<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet"%>
<%@page import="com.mollatech.axiom.nucleus.db.Units"%>
<%@include file="header.jsp" %>
<%    
    UnitsManagemet uMngt = new UnitsManagemet();
    Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
%>
<div class="container-fluid">
    <div id="auditTable">
        <h1 class="text-success">Operators Management</h1>
        <p>You can manage all your operators with requester status also pull audit.</p>
        <br>
        <div class="row-fluid">
            <div id="licenses_data_table">
                <table class="table table-striped">
                    <tr>
                        <td>No.</td>
                        <td>Name</td>
                        <td>Email</td>
                        <td>Mobile</td>
                        <td>Unit</td>
                        <td>Audit</td>
                        <td>Created</td>
                        <td>Last Access</td>
                    </tr>
                     <%
                            Channels channel1 = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                            OperatorsManagement smng = new OperatorsManagement();
                            String sessionID = (String) session.getAttribute("_apSessionID");
                            Operators[] arrOpR = smng.GetOperatorByTypeNunit(channel1.getChannelid(),operator.getUnits(), OperatorsManagement.REQUESTER);
                            String error = "NA";
                            int k = 0;
                            if(arrOpR != null){ 
                                for(int i = 0; i < arrOpR.length; i++){
                              if((arrOpR[i].getOperatorType()) == 1){
                              Units unitObj = uMngt.getUnitByUnitId(sessionID, channel1.getChannelid(), arrOpR[i].getUnits());
                              k++;
                        %>
                    <tr>
                       
                                    <td><%=i + 1%></td>
                                    <td><%=arrOpR[i].getName()%></td>
                                    <td><%=arrOpR[i].getEmailid()%></td>
                                    <td><%=arrOpR[i].getPhone()%></td>
                                    <td><%=unitObj.getUnitname()%></td>
                                    <td>
                                        <a href="#" class="btn btn-mini" onclick="loadEditRequesterOperatorDetails('<%=arrOpR[i].getOperatorid()%>')">Audit Download</a>
                                    </td>
                                    <td><%=sdf.format(arrOpR[i].getCreatedOn())%></td>
                                    <td><%= sdf.format(arrOpR[i].getLastAccessOn())%></td>
                        <%}}}else{%>
                                    <td><%=1%></td>
                                    <td><%=error%></td>
                                    <td><%=error%></td>
                                    <td><%=error%></td>
                                    <td><%=error%></td>
                                    <td><%=error%></td>
                                    <td><%=error%></td>
                                    <td><%=error%></td>
                        <%}if(k == 0){%>
                                    <td><%=1%></td>
                                    <td><%="No record found"%></td>
                                    <td><%="No record found"%></td>
                                    <td><%="No record found"%></td>
                                    <td><%="No record found"%></td>
                                    <td><%="No record found"%></td>
                                    <td><%="No record found"%></td>
                                    <td><%="No record found"%></td>
                        <%}%>
                    </tr>
                </table>
            </div>
        </div>
    </div>
 </div>
                        <div id="auditDownload" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idauditDownload"></div></h3>
        <h3 id="myModalLabel">Download Audit</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="testSMSPrimaryForm" name="testSMSPrimaryForm">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_oprnameR" name="_oprnameR"/>
                    <input type="hidden" id="_opridR" name="_opridR"/>
                    <div class="control-group">
                        <label class="control-label"  for="username">Start Date</label>
                        <div class="controls" align="left" >

                            <!--<span class="add-on">From:</span>-->   
                            <div id="Pushdatetimepicker1" class="input-append date">
                                <input id="_auditStartDate" name="_auditStartDate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth">

                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">End Date</label>
                        <div class="controls" align="left">
                            <!--<span class="add-on">Till:</span>-->   
                            <div id="Pushdatetimepicker2" class="input-append date">
                                <input id="_auditEndDate" name="_auditEndDate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth">

                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>                    
                </fieldset>
            </form>
        </div>
    </div>

    <div class="modal-footer">
        <div id="editoperator-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="searchAudit()" id="buttonEditOperatorSubmit">Show Audit</button>
    </div>
</div>


<script type="text/javascript">
    $(function() {
        $('#Pushdatetimepicker1').datepicker({
              format: 'dd/mm/yyyy',
            language: 'pt-BR'
        });
    });
    $(function() {
        $('#Pushdatetimepicker2').datepicker({
            format: 'dd/mm/yyyy',
            language: 'pt-BR'
        });
    });
    
//                            ChangeMediaType(0);
</script>
<%@include file="footer.jsp" %>
