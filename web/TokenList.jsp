<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet"%>
<%@page import="com.mollatech.axiom.nucleus.db.Units"%>
<%@include file="header.jsp" %>
<script src="./assets/js/units.js"></script>
<%
    String _sessionID = (String) session.getAttribute("_apSessionID");

    SessionManagement smObj = new SessionManagement();
    int iStatus = smObj.GetSessionStatus(_sessionID);
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
     OperatorsManagement oMngt = new OperatorsManagement();
    Units[] unitsObj = null;
    if (iStatus == 1) { //active
        smObj.UpdateSession(_sessionID);
        UnitsManagemet cmObj = new UnitsManagemet();
        unitsObj = cmObj.ListUnitss(_sessionID, channel.getChannelid());
        cmObj = null;
    }
%>
<div class="container-fluid">
    <h1 class="text-success">Tokens Management</h1>
    <p>To manage tokens repository. free token can shift from one unit to new unit.</p>
    <div class="row-fluid">
        <div id="licenses_data_table">
            <table class="table table-striped">
                <tr>
                    <td>No</td>
                    <td>Units Name</td>
                    <td>Manage</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tokens
<!--                        <div class="btn-group">
                         <a type="button" class="btn btn-success btn-mini" >&nbsp;Active</a>
                     <a type="button" class="btn btn-danger btn-mini" >Suspended</a>
                      <a  type="button" class="btn btn-info btn-mini" >&nbsp;Total</a>
                        </div>
                    </td>-->
<!--                    <td>In-Active Operators</td>
                    <td>All Operators</td>-->
                    <td>Created On</td>
                    <td>Last Updated On</td>
                    
                </tr>

                <%
                    out.flush();
//                    UnitsManagemet tmObj = new UnitsManagemet();
               if(unitsObj != null){
                    for (int i = 0; i < unitsObj.length; i++) {
                        
                      int iActiveCount = oMngt.getOperatorByUnitId(_sessionID, channel.getChannelid(), unitsObj[i].getUnitid(),OperatorsManagement.ACTIVE_STATUS);
                      int inActiveCount = oMngt.getOperatorByUnitId(_sessionID, channel.getChannelid(), unitsObj[i].getUnitid(),OperatorsManagement.SUSPEND_STATUS);
                      int iAllCount = oMngt.getOperatorByUnitId(_sessionID, channel.getChannelid(), unitsObj[i].getUnitid(),2);
                        
                        Units axcObj = unitsObj[i];
                         int iActivePr = 0;
                        if(iActiveCount !=0){
                         iActivePr = (iActiveCount/iAllCount) * 100;
                        }
                         int inActivePr = 0;
                         if(inActiveCount !=0){
                         inActivePr = (inActiveCount/iAllCount) * 100;
                         }
                        
                        SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm ");
//                        String TypeoF = null;
//                        if (axcObj.getType() == 1) {
//                            TypeoF = "Mobile";
//                        } else {
//                            TypeoF = "Email";
//                        }
                        
                        String strStatus = "Active";
                        if (axcObj.getStatus()==1) {
                            strStatus = "Active";
                        } else {
                            strStatus = "Suspended";
                        }
                        String userStatus = "user-status-value-" + i;
//                        ByteArrayInputStream bais = new ByteArrayInputStream(axcObj.getTemplatebody());
//                        String templatebody = (String) UtilityFunctions.deserializeFromObject(bais);
            
                               int iAllpr = 0;     
                        if(iAllCount != 0){
                            iAllpr = 100;
                        }

                %>
                <tr>
                    <td><%=(i + 1)%></td>
                    <td><%=axcObj.getUnitname() %></td>
                           
                     <%//if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
                    <td>
                        <div class="btn-group">
                            <button class="btn btn-mini" id="<%=userStatus%>"><%=strStatus%></button>
                            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                
                                 <li><a href="#"  onclick="changeUnitsStatus(<%=axcObj.getUnitid()%>, 1,'<%=userStatus%>')" >Mark as Active?</a></li>
                                 <li><a href="#" onclick="changeUnitsStatus(<%=axcObj.getUnitid()%>, 0,'<%=userStatus%>')" >Mark as Suspended?</a></li>
                                 <li class="divider"></li>
                                 <!--<li><a href="#" onclick="loadEditUnitDetails('<%=axcObj.getUnitname() %>','<%=axcObj.getUnitid()%>')">Edit Details</a></li>-->
                                  <li><a href="#" onclick="loadShiftUnitDetails('<%=axcObj.getUnitname() %>','<%=axcObj.getUnitid()%>')">Shift Free Tokens</a></li>
                                    <li><a href="#" onclick="loadShiftUnitDetails('<%=axcObj.getUnitname() %>','<%=axcObj.getUnitid()%>')">Assign Tokens</a></li>
                                 <!--<li><a href="#" onclick="removeUnits(<%=axcObj.getUnitid()%>)"><font color="red">Remove Unit?</font></a></li>-->
                               
                            </ul>
                        </div>
                    </td>
                    <% //}%>
                    
<!--                    <td><a href="./operators.jsp?_status=1&_unitId=<%=unitsObj[i].getUnitid()%>" class="btn btn-success" type="button"> <%=iActiveCount%> (<%=iActivePr %>%)</a></td>
                    <td><a href="./operators.jsp?_status=0&_unitId=<%=unitsObj[i].getUnitid()%>"  class="btn btn-danger" type="button"> <%=inActiveCount%> (<%=inActivePr %>%)</a></td>
                    <td><a href="./operators.jsp?_status=2&_unitId=<%=unitsObj[i].getUnitid()%>" class="btn btn-info" type="button"> <%=iAllCount%> (<%=iAllpr%>%)</a></td>-->
               <td> 
                <div class="btn-group">
                      <a href="./operators.jsp?_status=1&_unitId=<%=unitsObj[i].getUnitid()%>" type="button" class="btn btn-success btn-mini">Active :&nbsp;<%=iActiveCount%> (<%=iActivePr %>%)</a>
                      <a href="./operators.jsp?_status=0&_unitId=<%=unitsObj[i].getUnitid()%>" type="button" class="btn btn-danger btn-mini">In-Active :&nbsp;<%=inActiveCount%> (<%=inActivePr %>%)</a>
                      <a href="./operators.jsp?_status=2&_unitId=<%=unitsObj[i].getUnitid()%>" type="button" class="btn btn-info btn-mini">Free :&nbsp;<%=iAllCount%></a>
                </div>
               </td>
                <td><%=sdf.format(axcObj.getCreatedOn())%>
                <td><%=sdf.format(axcObj.getLastupOn())%></td>

                </tr>
                <%}}%>
            </table>
            <!--<p><a href="#addNewUnit" class="btn btn-primary" data-toggle="modal">Add New Unit&raquo;</a></p>-->
        </div>
    </div>
    <br>

    <script language="javascript">
        //listChannels();
    </script>
</div>
<!-- Modal -->
<div id="addNewUnit" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Add New Unit</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="AddNewUnitForm" name="AddNewUnitForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Name</label>
                        <div class="controls">
                            <input type="text" id="_unitName" name="_unitName" placeholder="set unique name for login" class="input-xlarge">
                        </div>
                    </div>
               
                    <div class="control-group">
                        <label class="control-label"  for="username">Status</label>
                        <div class="controls">
                            <select class="span4" name="_unitStatus" id="_unitStatus">
                                <option value="1">Active</option>
                                <option value="0">In-Active</option>
                            </select>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>



    <div class="modal-footer">
        <div id="add-new-unit-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <!--<div class="span3" id="add-new-unit-result"></div>-->
        <!--<button class="btn btn-primary" onclick="addNewUnit()" id="addnewUnitSubmitBut">Add New Unit</button>-->
    </div>
</div>
<div id="editUnit" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idEditUnitName"></div>Edit Unit</h3>
    </div>
<!--     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Add New Unit</h3>
    </div>-->
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="editUnitForm" name="editUnitForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Name</label>
                        <div class="controls">
                            <input type="text" id="_unitNameE" name="_unitNameE" placeholder="Unit Name" class="input-xlarge">
<!--                            <input type="hidden" id="_oprroleidE" name="_oprroleidE"/>
                            <input type="hidden" id="_oprstatusE" name="_oprstatusE"/>-->
                            <input type="hidden" id="_unitId" name="_unitId"/>
                            <input type="hidden" id="_oldunitNameE" name="_oldunitNameE"/>
                        </div>
                    </div>
                
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="editunit-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
       
        <button class="btn btn-primary" onclick="editUnit()" id="buttonEditUnitSubmit">Save Changes</button>
       
    </div>
</div>


<div id="shiftUnit" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idshiftUnitName"></div>Shift Tokens</h3>
    </div>
<!--     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Add New Unit</h3>
    </div>-->
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="shiftUnitForm" name="shiftUnitForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">From Unit</label>
                        <div class="controls">
                            <input type="text" readonly id="_oldunitNameS" name="_oldunitNameS" placeholder="Unit Name" class="input-xlarge">
<!--                            <input type="hidden" id="_oprroleidE" name="_oprroleidE"/>
                            <input type="hidden" id="_oprstatusE" name="_oprstatusE"/>-->
                            <input type="hidden" id="_unitIdS" name="_unitIdS"/>
                            <!--<input type="hidden" id="_oldunitNameS" name="_oldunitNameS"/>-->
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">To Unit</label>
                        <div class="controls">
                      <select class="span4" name="_unitS" id="_unitS">
                                <%
                                    UnitsManagemet uMngt = new UnitsManagemet();
                                    Units[] uList = uMngt.ListUnitss(sessionid, channel.getChannelid());
                                    if (uList != null) {
                                        for (int i = 0; i < uList.length; i++) {
                                     %>
                                <option value="<%=uList[i].getUnitid()%>"><%=uList[i].getUnitname()%></option>
                                <%
                                        }
                                    }
                                %>
                            </select>
                        </div>
                    </div>
                
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="shiftunit-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
       
        <button class="btn btn-primary" onclick="shiftUnit()" id="buttonShiftUnitSubmit">Save Changes</button>
       
    </div>
</div>
<%@include file="footer.jsp" %>