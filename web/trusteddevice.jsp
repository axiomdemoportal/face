<%@include file="header.jsp" %>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<script src="./assets/js/trustedDevice.js"></script>

<div class="container-fluid">
    <h1 class="text-success">Device Tracking and Hardware Profile</h1>
    <p>List of devices and its hardware profile associated with the user(s). You can manage the devices as trusted or not? In addition, details of hardware and software are also listed for managing inventory.</p>
    <p>
        <br>
    <h3>Search through name</h3>   
    <div class="input-append">
        <form id="searchDeviceForm" name="searchDeviceForm">
            <input type="text" id="_keyword" name="_keyword" placeholder="search Users using name, phone or email" class="span4"><span class="add-on"><i class="icon-search"></i></span>
            <a href="#" class="btn btn-success" onclick="searchDevices()">Search User(s)</a>
        </form>
    </div>
    <div id="device_table_main">
    </div>
</div>
</p>
<br>

<%@include file="footer.jsp" %>