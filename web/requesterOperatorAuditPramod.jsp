<%@page import="com.mollatech.axiom.nucleus.db.Audit"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.AuditManagement"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="java.text.SimpleDateFormat"%>


<%
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _startdate = request.getParameter("_startdate");
    String _enddate = request.getParameter("_enddate");
    String _oprID = request.getParameter("_opridR");
    String _oprName = request.getParameter("_oprName");
    DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    Date startDate = null;
    if (_startdate != null && !_startdate.isEmpty()) {
        startDate = (Date) formatter.parse(_startdate);
    }
    Date endDate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        endDate = (Date) formatter.parse(_enddate);
    }

    AuditManagement audit = new AuditManagement();
    String strmsg = "No Record Found";
    Audit[] arrAudit = audit.searchAuditObj(channel.getChannelid(), _oprID, startDate, endDate);
  SimpleDateFormat tz1 = new SimpleDateFormat("EEE dd MMM");
%>
<h3> Results from <%=tz1.format(startDate)%> to <%=tz1.format(endDate)%> for <%=_oprName%> </i></h3>
<div class="row-fluid">
    <div class="span6">
        <div class="control-group">                        
            <div class="span1">
                <div class="control-group form-inline">
                    <a href="#" class="btn btn-info" onclick="requesterReportCSV()" >
                        <i class="icon-white icon-chevron-down"></i> CSV</a>
                </div>
            </div>
            <div class="span1">
                <div class="control-group form-inline">
                    <a href="#" class="btn btn-info" onclick="requesterPDF()" >
                        <i class="icon-white icon-chevron-down"></i> PDF</a>
                </div>
            </div>
            <div class="span1">
                <div class="control-group form-inline">
                    <a href="#" class="btn btn-info" onclick="requesterTXT()" >
                        <i class="icon-white icon-chevron-down"></i> TEXT</a>
                </div>
            </div>
        </div>
    </div>
    <table class="table table-striped" >
        <tr>
            <td>No.</td>
            <td>Remote Access</td>
            <td>IP</td>
            <td>Operator</td>
            <td>Category</td>
            <td>Item Type</td>
            <td>Action</td>
            <td>Result</td>
            <td>New</td>
            <!--<td>Old</td>-->
            <td>Time</td>

        </tr>
        <%    if (arrAudit != null) {

                for (int i = 0; i < arrAudit.length; i++) {


        %>
        <tr>
            <td><%=i + 1%></td>
            <td><%=arrAudit[i].getRemoteaccesslogin()%></td>
            <td><%=arrAudit[i].getIpaddress()%></td>
            <td><%=arrAudit[i].getOperatorname()%></td>
            <td><%=arrAudit[i].getCategory()%></td>
            <td><%=arrAudit[i].getItemtype()%></td>
            <td><%=arrAudit[i].getAction()%></td>
            <td><%=arrAudit[i].getResult()%></td>
            <td><%=arrAudit[i].getNewvalue()%></td>
            <!--<td><%=arrAudit[i].getOldvalue()%></td>-->
            <td><%=arrAudit[i].getAuditedon()%></td>
        </tr>
        <%}
                } else {%>
        <tr>
            <td>1</td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <!--<td><%=strmsg%></td>-->
        </tr>
        <%}%>
    </table>
    <div id ="1">
      <div class="span6">
        <div class="control-group">                        
            <div class="span1">
                <div class="control-group form-inline">
                    <a href="#" class="btn btn-info" onclick="requesterReportCSV()" >
                        <i class="icon-white icon-chevron-down"></i> CSV</a>
                </div>
            </div>
            <div class="span1">
                <div class="control-group form-inline">
                    <a href="#" class="btn btn-info" onclick="requesterPDF()" >
                        <i class="icon-white icon-chevron-down"></i> PDF</a>
                </div>
            </div>
            <div class="span1">
                <div class="control-group form-inline">
                    <a href="#" class="btn btn-info" onclick="requesterTXT()" >
                        <i class="icon-white icon-chevron-down"></i> TEXT</a>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>


</div>
