<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>
<%@page import="com.mollatech.axiom.connector.communication.AXIOMStatus"%>
<%@page import="com.mollatech.axiom.nucleus.settings.SendNotification"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.TemplateUtils"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.db.Templates"%>
<%@page import="com.mollatech.axiom.nucleus.db.operation.TemplateNames"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%@page import="com.mollatech.axiom.nucleus.settings.TokenSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet"%>
<%@page import="com.mollatech.axiom.nucleus.db.Units"%>
<%@include file="header.jsp" %>
<!--<script src="./assets/js/jquery.js"></script>
<script src="./assets/js/bootbox.min.js" type="text/javascript"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">
<link rel="stylesheet" href="./assets/css/datepicker.css">
<script src="./assets/js/bootstrap-datepicker.js"></script>
<link href="./assets/css/bootstrap.css" rel="stylesheet">-->
<script src="assets/js/units.js" type="text/javascript"></script>
<div class="container-fluid">
    <h1 class="text-success">Unit Reports</h1>
    <h3>Make Your Own Report</h3>
    <input type="hidden" id="_changeStatus" name="_changeStatus">

    <div class="row-fluid">
        <div class="span12">
            <form id="unitReportform" name="unitReportform">
                <div class="control-group form-inline">                
                    <div class="input-append">

                        <span class="add-on">From:</span>   
                        <div id="datetimepicker1" class="input-append date">
                            <input id="userstartdate" name="userstartdate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth">
                            <span class="add-on">
                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                            </span>
                        </div>

                    </div>
                    <div class="input-append">
                        <span class="add-on">To:</span>   
                        <div id="datetimepicker2" class="input-append date">
                            <input id="userenddate" name="userenddate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth">
                            <span class="add-on">
                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                            </span>
                        </div>
                    </div>    
                    <div class="input-prepend">
                        <span class="add-on " >Enter </span><input id="_searchtext" name="_searchtext" class="" type="text" placeholder="Unit Name" data-bind="value: vm.ActualDoorSizeDepth" />
                    </div>
                    <i>For List only, select Status: </i> 
                    <select span="1"  name="_status" id="_status" >
                        <option value="1">All</option>
                        <option value="2">Active Only</option>
                        <option value="3">Suspended Only</option>
                    </select>
                    <a href="#" class="btn btn-success" onclick="getunitRepprts()">Generate Report</a>
                    <!--                <div class="btn-group">
                                        <button class="btn"><div id="_change-status-Axiom"></div></button>
                                        <button class="btn" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="ChangeStatusType(1)">Active Only</a></li>
                                            <li><a href="#" onclick="ChangeStatusType(0)">Suspended Only</a></li>
                                        </ul>
                                    </div>-->
                </div>
            </form>
        </div>
    </div>



    <div id="units_table_main">

    </div>

    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            });
        });
        $(function () {
            $('#datetimepicker2').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'

            });
        });

    </script>
</div>

<%@include file="footer.jsp" %>
