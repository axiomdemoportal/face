
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.MobileTrustManagment"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Geotrack"%>
<%@page import="com.mollatech.axiom.nucleus.db.Geofence"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.GeoLocationManagement"%>
<%@page import="com.mollatech.axiom.connector.user.TokenStatusDetails"%>
<%@page contentType="application/json" pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.util.Map" %>
<%@ page import="org.json.simple.JSONValue" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Trusteddevice"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.Otptokens"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TrustDeviceManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<!--<script src="./assets/js/trustedDevice.js"></script>-->
<%
    int REGISTER = 1;
    int LOGIN = 2;
    int TRANSACTION = 3;
    int CHANGEINPROFILE = 4;
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();
//    String userId = request.getParameter("_searchtext");
//    String _duration = request.getParameter("_duration");
//    GeoLocationManagement geoObj = new GeoLocationManagement();
//    Geotrack Tracks[] = null;
    SimpleDateFormat sdf = new SimpleDateFormat("d MMM,yyyy HH:mm ");
//    Tracks = geoObj.getTracks(sessionId, _channelId, userId, _duration);
    String _startdate = request.getParameter("_startdate");
    String _enddate = request.getParameter("_enddate");
    String _type = request.getParameter("_type");
    DateFormat formatter = new SimpleDateFormat("dd/mm/yyyy");
    Date startDate = null;
    if (_startdate != null && !_startdate.isEmpty()) {
        startDate = (Date) formatter.parse(_startdate);
    }
    Date endDate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        endDate = (Date) formatter.parse(_enddate);
    }
    GeoLocationManagement geoObj = new GeoLocationManagement();
    Geotrack[] Tracks = null;
    if (_type.equals("0")) {
        Tracks = geoObj.getGeoTracksByTxType(channel.getChannelid(), MobileTrustManagment.INVALIDIMAGE);
    } else {
        Tracks = geoObj.getGeoTracksByTxTypeDuration(channel.getChannelid(), MobileTrustManagment.INVALIDIMAGE, endDate, startDate);
    }

    if (Tracks != null) {

%>

<%        List listHowTos = new LinkedList();
        Map m2 = new HashMap();

        for (int i = 0; i < Tracks.length; i++) {
            String _txtype = "";
            if (Tracks[i].getTxtype() == REGISTER) {
                _txtype = "Register";
            } else if (Tracks[i].getTxtype() == LOGIN) {
                _txtype = "Login";
            } else if (Tracks[i].getTxtype() == TRANSACTION) {
                _txtype = "Transaction";
            } else if (Tracks[i].getTxtype() == CHANGEINPROFILE) {
                _txtype = "Change Profile";
            } else if (Tracks[i].getTxtype() == MobileTrustManagment.INVALIDIMAGE) {
                _txtype = "Honey Trap";
            } else if (Tracks[i].getTxtype() == MobileTrustManagment.VALIDIMAGE) {
                _txtype = "Image";
            }

//           Date d = Tracks[i].getExecutedOn();
            String zip = " ";
            if (Tracks[i].getZipcode() != null) {
                zip = Tracks[i].getZipcode();
            }

            m2.put("address", "City:" + Tracks[i].getCity());
            m2.put("_lat", Tracks[i].getLattitude());
            m2.put("_lng", Tracks[i].getLongitude());
            m2.put("_txtype", _txtype);
            m2.put("_city", Tracks[i].getCity());
            m2.put("_country", Tracks[i].getCountry());
            m2.put("_state", Tracks[i].getState());

            m2.put("_zipcode", zip);

            m2.put("_date", sdf.format(Tracks[i].getExecutedOn()));

            listHowTos.add(m2);
            m2 = new HashMap();

        }

        /*
         "
         "Lot 31363, Batu 5 , Jalan Klang Lama, 58000, KL","3.107340","101.678848
         "Lot PT 46120 Batu 6, Jalan Bukit Kemuning, Bukit Naga, 42540 Klang","3.004340","101.507072
         "Ground Floor, Wisma MBSA, Jalan Persiaran Perbandaran, 40450, Shah Alam","3.077400","101.519501
         "Ground Floor, Bangunan Affinbank, Lot P3.4, Jalan Persiaran Perbandaran, Section 14, 40000, Shah Alam ","3.073130","101.524010

         "Lot LG B1 (C) Lower Ground Floor, Kompleks Karamunsing, 88300, Kota Kinabalu, Sabah","5.975500","116.075500
         "Ground Floor, Wisma Felcra, Lot 4780, Jalan Rejang, Setapak Jaya, Kuala Lumpur ","3.190740","101.732620
         "5, Jalan Raja Chulan, 50450 Kuala Lumpur","3.151100","101.716103

         "Lot PT 7637, Jalan Batu Tiga Lama, 41300 Klang","3.058670","101.480324
         "KM 135, Lebuhraya Utara Selatan, Mukim Jorak, 84600 Muar, Johor ","2.122690","102.792511
         "Departure Hall, 5th Floor, Main Terminal Building, KLIA, Sepang, Selangor","2.754460","101.705017
         "Lot PTD 101051 & 101052, Jalan Permas 10/10, Bandar Baru Permas Jaya, Masai, 81750 Johor Bahru","1.494820","103.813011
         "Menara KBS, No 27 Persiaran Perdana Presint 4, Pusat Pentadbiran Kerajaan Persekutuan, 62570, Putrajaya.","2.954320","101.700012
         "Jalan 19/1-B, Sea Park, 46300 Petaling Jaya","3.113240","101.626648

         "Lot PT 6086, Jalan Kuching, 51200, Kuala Lumpur","3.188023","101.669991

         "Sg Besi, 57000, KL","3.119660","101.703537
         "Kem Sg Besi, 57000, Kuala Lumpur","3.065800","101.715698
         "Pengkalan TUDM, Sg Besi Army Cam, 50460 KL","3.119660","101.703537
         "Lot 5241 & 5242, 295, Setapak, 53300, Kuala Lumpur","3.198490","101.716087
         "Lot 3280 & 3281,No 98, Jalan Genting Klang, Setapak, 53300 Kuala Lumpur, 46150","3.193410","101.709862

         "Lot 13639, Jalan Kenanga, Off Jalan Damansara Highway, 47400, PJ","3.132733","101.611244
         "No. 100, Jalan Manggis, Kampong Pasir, 81200 Johor Bahru","1.498120","103.698868

         */
        out.clear();

        response.setContentType("application/json");
        String jsonString = JSONValue.toJSONString(listHowTos);
        out.print(jsonString);
        out.flush();

    }
%>

