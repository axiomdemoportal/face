<%@ page import="com.sun.management.UnixOperatingSystemMXBean" %>
<%@ page import="java.io.File" %>
<%@ page import="java.lang.management.ManagementFactory" %>
<%@ page import="java.lang.management.OperatingSystemMXBean" %>
<%@ page import="java.util.Set" %>
<%@ page import="java.util.Date" %>

<%
    out.println("Available processors (cores): "
            + Runtime.getRuntime().availableProcessors());

    out.println("<br>");

    out.println("Free memory (bytes): "
            + Runtime.getRuntime().freeMemory());
    out.println("<br>");
    long maxMemory = Runtime.getRuntime().maxMemory();

    out.println("Maximum memory (bytes): "
            + (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemory));
    out.println("<br>");

    out.println("Total memory available to JVM (bytes): "
            + Runtime.getRuntime().totalMemory());
    out.println("<br>");
    File[] roots = File.listRoots();

    for (File root : roots) {
        out.println("File system root: " + root.getAbsolutePath());
        out.println("<br>");
        out.println("Total space (bytes): " + root.getTotalSpace());
        out.println("<br>");
        out.println("Free space (bytes): " + root.getFreeSpace());
        out.println("<br>");
        out.println("Usable space (bytes): " + root.getUsableSpace());
    }

    Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
    Thread[] threadArray = threadSet.toArray(new Thread[threadSet.size()]);
    out.println("<br>");
    out.println("Number of threads:: " + threadSet.size());
    out.println("<br>");
    OperatingSystemMXBean os = ManagementFactory.getOperatingSystemMXBean();
    if (os instanceof UnixOperatingSystemMXBean) {
 out.println("<br>");
        out.println("Number of open files: " + ((UnixOperatingSystemMXBean) os).getOpenFileDescriptorCount());
 out.println("<br>");
        out.println("Number of maximum files: " + ((UnixOperatingSystemMXBean) os).getMaxFileDescriptorCount());
 out.println("<br>");
        out.println("CPU load: " + ((UnixOperatingSystemMXBean) os).getSystemCpuLoad() * 100);
 out.println("<br>");
        out.println("Process CPU load: " + ((UnixOperatingSystemMXBean) os).getProcessCpuLoad() * 100);
    }

    out.println("<br>");
    Date ddd = new Date();
    out.println("Date And Time:: " + ddd.toString());
%>
