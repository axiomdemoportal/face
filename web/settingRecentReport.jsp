<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SchedulerManagement"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.AxiomProtect"%>
<%@page import="com.mollatech.axiom.nucleus.settings.NucleusMonitorSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.Monitorsettings"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.axiom.nucleus.db.Monitortracking"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.MonitorSettingsManagement"%>
<script src="./assets/js/addSettings.js"></script>
<div class="tab-content">
    <h3>Recent Report..</h3>
    <div class="tab-pane active" id="primary">
        <%!
            public String ChechFor = "";
        %> 
        <%  Channels _apSChannelDetails = (Channels) session.getAttribute("_apSChannelDetails");
            String _settingName = request.getParameter("_settingName");
            int type = Integer.parseInt(request.getParameter("_type"));
            Map settingsMap = (Map) session.getAttribute("setting");
            Monitorsettings monitorsettings = null;
            monitorsettings = (Monitorsettings) settingsMap.get(_settingName);
            int types;
            NucleusMonitorSettings nms = null;
            if (monitorsettings != null) {
                byte[] obj = monitorsettings.getMonitorSettingEntry();
                byte[] f = AxiomProtect.AccessDataBytes(obj);
                ByteArrayInputStream bais = new ByteArrayInputStream(f);
                Object object = SchedulerManagement.deserializeFromObject(bais);
                //NucleusMonitorSettings nms = null;
                if (object instanceof NucleusMonitorSettings) {
                    nms = (NucleusMonitorSettings) object;
                }
            }
            MonitorSettingsManagement management = new MonitorSettingsManagement();
            Monitortracking[] monitortrackings = management.getMonitorTrackingByName(_apSChannelDetails.getChannelid(), _settingName);
        %>
        <div class="container-fluid">
            <div class="row-fluid">
                <div id="licenses_data_table">
                    <table class="table table-striped" id="table_main">
                        <tr>
                            <td>No.</td>
                            <td>Monitor Name</td>
                            <td>Monitor Type</td>
                            <td>Check For</td>
                            <td>Performance</td>
                            <td>Executed On</td>
                        </tr>
                        <%
                            if (monitortrackings != null) {
                                int x = 25;
                                if (monitortrackings.length < x) {
                                    x = monitortrackings.length;
                                }
                                for (int i = 0; i < x; i++) {
                        %>
                        <tr>
                            <td><%=i + 1%></td>
                            <td><%= _settingName%></td>
                            <td><%= getStringType(type, nms)%></td>
                            <td><%= ChechFor%></td>
                            <td><%= monitortrackings[i].getPerformance()%></td>
                            <td><%= monitortrackings[i].getExecutionStartOn()%></td>
                        </tr>
                        <% }
                        } else {%>
                        <tr>
                            <td>1</td>
                            <td>No Record Found</td>
                            <td>No Record Found</td>
                            <td>No Record Found</td>
                            <td>No Record Found</td>
                            <td>No Record Found</td>
                        </tr>
                        <% }
                        %>
                    </table>
                </div>
            </div>
        </div>
        <%! public String getStringType(int type, NucleusMonitorSettings nms) {

                String types = "";

                if (type == 1) {
                    types = "Web Site";
                    ChechFor = nms.getWebadd();
                } else if (type == 3) {
                    types = "Ping Monitor";
                    ChechFor = nms.getPinghost();
                }
                return types;
            }
        %>
    </div>
</div>