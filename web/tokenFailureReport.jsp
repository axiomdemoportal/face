<%@page import="com.mollatech.axiom.nucleus.db.Units"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet"%>
<%@page import="com.mollatech.axiom.nucleus.db.Otptokens"%>
<%@page import="java.text.Normalizer.Form"%>
<%@page import="com.itextpdf.text.Document"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.connector.user.TokenStatusDetails"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>

<%@include file="header.jsp" %>
<script src="./assets/js/jquery.js"></script>
<script src="./assets/js/otptokens.js"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">
<script src="./assets/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="./assets/css/datepicker.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<div class="container-fluid">
    <h1 class="text-success">Usage Report</h1>
    <h3>Make Your Own Report</h3>
    <div class="row-fluid">
        <div class="span12">
            <div class="control-group form-inline">
                <div class="input-prepend">
                    <span class="add-on">From:</span>   
                    <div id="datetimepicker1" class="input-append date">
                        <input id="startdate" name="startdate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                </div>
                <div class="input-prepend">
                    <!--<div class="well">-->
                    <span class="add-on">till:</span>   
                    <div id="datetimepicker2" class="input-append date">
                        <input id="enddate" name="enddate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                    <!--</div>-->
                </div>
                <select class="selectpicker" name="_changeCategory" id="_changeCategory"  class="span6">
                    <option value="Verify Password">User & Password</option>
                    <option value="VERIFY OTP">One Time Password</option>
                    <option  value="VERIFY SIGNATURE OTP">Signature One Time Password</option>
                    <option  value="ChallengeResponse Management">Challenge Response (Q&A)</option>
                    <option  value="PKI Sign Data">Digital Signature (using Digital Certificate)</option>
                    <option  value="OcrDocumentation">OCR Templatization</option>
                    <!--<option value="2">Secure Phrase (Image Based)</option>-->                    
                    <!--                    <option value="6">Trusted Device</option>
                                        <option value="7">Geo-location (Fence & Track)</option>
                                        <option value="8">Two Way Auth</option>-->
                </select>
                <div class="input-prepend">
                    <%if (oprObj.getRoleid() != 1) {//1 sysadmin
                            if (accessObj.viewOtpFailureReport == true) { %>
                    <button class="btn btn-success" onclick="generateOtpUsage()" id="addUserButton">Generate Report Now</button>
                    <%} else {%>
                    <button class="btn btn-success" onclick="InvalidRequestOTPToken('otpfailurereport')" id="addUserButton">Generate Report Now</button>
                    <%}
                    } else {%>
                    <button class="btn btn-success" onclick="generateOtpUsage()" id="addUserButton">Generate Report Now</button>

                    <%}%>
                </div>
                <div class="input-prepend">
                    <!--<button class="btn btn-success" id="refreshButton" onclick="RefreshTokenReport()" type="button">Refresh Report</button>-->
                    <%if (oprObj.getRoleid() != 1) {
                            if (accessObj != null && accessObj.viewOtpFailureReport == true) {%>
                    <!--                    <button class="btn btn-success" id="refreshButton" onclick="RefreshTokenUsageReport()" type="button">Refresh Report</button>-->
                    <%} else {%>
                    <button class="btn btn-success " onclick="InvalidRequestOTPToken('otpfailurereport')" type="button">Generate Report Now</button>
                    <%}
                    } else {%>
                    <!--                     <button class="btn btn-success" id="refreshButton" onclick="RefreshTokenUsageReport()" type="button">Refresh Report</button>-->
                    <%}%>
                </div>


            </div>
            <div id="users_table_main_MSG">
            </div>
        </div>
    </div>
    <div id="licenses_data_table">
    </div>

    <!--               <div class="tabbable" id="REPORT" id="usageOTPReport">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#otpcharts1" data-toggle="tab">Charts</a></li>
                <li><a href="#otpreport2" data-toggle="tab">Tabular List</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="otpcharts1">
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="control-group">
                                <div class="span4">
                                    <div id="graph" ></div>
    
                                </div>
                                <div  class="span8">
                                    <div id="graph1"></div>
    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    
                <div class="tab-pane" id="otpreport2">  
                    <div id="licenses_data_table">
                    </div>
                </div>
            </div>
        </div>-->


    <script>
        //document.getElementById("REPORT").style.display = 'none';
        //document.getElementById("refreshButton").style.display = 'none';
        $(function () {
            $('#datetimepicker1').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            });
        });
        $(function () {
            $('#datetimepicker2').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'

            });
        });
    </script>
</div>

<%@include file="footer.jsp" %>