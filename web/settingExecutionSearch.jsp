<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<script src="./assets/js/addSettings.js"></script>
<%! public String ChechFor = "";%> 
<%  Channels _apSChannelDetails = (Channels) session.getAttribute("_apSChannelDetails");
    String _settingName = request.getParameter("_settingName");
    int type = Integer.parseInt(request.getParameter("_type"));
    String _start = request.getParameter("_startdate");
    String _end = request.getParameter("_enddate");
%>
<div class="tabbable" style="margin-left: 15px; alignment-adjust: central">      
    <ul class="nav nav-tabs">
        <li class="active"><a onclick="chart('<%=_settingName%>', '<%=type%>', '<%=_start%>', '<%=_end%>')" data-toggle="tab">Historical report</a></li>
        <li><a onclick="textualreport('<%=_settingName%>', '<%=type%>', '<%=_start%>', '<%=_end%>')" data-toggle="tab">Tabular Report</a></li> 
        <li><a onclick="realtimes('<%=_settingName%>', '<%=type%>', '<%=_start%>', '<%=_end%>')" data-toggle="tab">Real Time Report</a></li> 
    </ul>                    
</div>
<div id="tabchart" ></div>
<div id="tabtextual" style="display: none"></div>
<div id="tabrealtime" style="display: none"></div>
<script>
    chart('<%=_settingName%>', '<%=type%>', '<%=_start%>', '<%=_end%>');
</script>