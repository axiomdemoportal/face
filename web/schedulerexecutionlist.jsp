<%@include file="header.jsp" %>
<script src="./assets/js/scheduler.js"></script>
<%@page import="com.mollatech.axiom.nucleus.settings.SchedulerSettingEntry"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.AxiomProtect"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SchedulerManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Schedulertracking"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String strmsg = "No Record Found";
    String _schdulerName = request.getParameter("_schdulerName");
    SchedulerManagement sMngt = new SchedulerManagement();
    Schedulertracking[] arrtracking = sMngt.getarrSchedulerTracking(channel.getChannelid(), _schdulerName);


%>

<div class="container-fluid">
<div class="row-fluid">
    <div id="licenses_data_table">
        <!--<h3>Execution List</h3>-->
        <h3 class="text-success">Execution List for <%=_schdulerName%></h3>
        <table class="table table-striped" >
            <tr>
                <td>No.</td>
                <td>Scheduler Name</td>
                <td>Type</td>
                <td>Preference</td>
                <td>Status</td>
                <td>Download Report</td>
                <td>Executed On</td>
            </tr>
            <%               if (arrtracking != null) {

                    for (int i = 0; i < arrtracking.length; i++) {

                        byte[] obj = arrtracking[i].getSchedulerSettingEntry();
                        ByteArrayInputStream bais = new ByteArrayInputStream(obj);
                        Object object = SchedulerManagement.deserializeFromObject(bais);

                        SchedulerSettingEntry schedulerSetting = null;
                        if (object instanceof SchedulerSettingEntry) {
                            schedulerSetting = (SchedulerSettingEntry) object;
                        }
                        String strgatewayType = "";
                        if (schedulerSetting.getType() == 0) {
                            strgatewayType = "ALL";
                        } else if (schedulerSetting.getType() == 1) {
                            strgatewayType = "SMS";
                        } else if (schedulerSetting.getType() == 2) {
                            strgatewayType = "USSD";
                        } else if (schedulerSetting.getType() == 3) {
                            strgatewayType = "VOICE";
                        } else if (schedulerSetting.getType() == 4) {
                            strgatewayType = "EMAIL";
                        } else if (schedulerSetting.getType() == 5) {
                            strgatewayType = "FAX";
                        } else if (schedulerSetting.getType() == 6) {
                            strgatewayType = "FACEBOOK";
                        } else if (schedulerSetting.getType() == 7) {
                            strgatewayType = "LINKEDIN";
                        } else if (schedulerSetting.getType() == 8) {
                            strgatewayType = "TWITTER";
                        } else if (schedulerSetting.getType() == 18) {
                            strgatewayType = "ANDROIDPUSH";
                        } else if (schedulerSetting.getType() == 19) {
                            strgatewayType = "IPHONEPUSH";
                        }

                        String strPreference = "";
                        if (schedulerSetting.getPrefrence() == 0) {
                            strPreference = "Primary and Secondary";
                        } else if (schedulerSetting.getPrefrence() == 1) {
                            strPreference = "Primary";
                        } else if (schedulerSetting.getPrefrence() == 2) {
                            strPreference = "Secondary";
                        }
                        String strgatewayStatus = "";
                        if (schedulerSetting.getGatewayStatus() == 99) {
                            strgatewayStatus = "ALL";
                        }
                        if (schedulerSetting.getGatewayStatus() == 0) {
                            strgatewayStatus = "SENT";
                        }
                        if (schedulerSetting.getGatewayStatus() == 2) {
                            strgatewayStatus = "PENDING";
                        }
                        if (schedulerSetting.getGatewayStatus() == -1) {
                            strgatewayStatus = "FAILED";
                        }
                        if (schedulerSetting.getGatewayStatus() == -5) {
                            strgatewayStatus = "BLOCKED";
                        }
                        String reportPath = arrtracking[i].getGeneratedReportFilePath();
                        String[] arrReportPath = null;
                        if (schedulerSetting.getReportType() == 99) {
                            arrReportPath = reportPath.split(",");
                        }


            %>
            <tr>
                <td><%=i + 1%></td>
                <td><%=arrtracking[i].getSchedulerName()%></td>
                <td><%=strgatewayType%></td>
                <td><%=strPreference%></td>
                <td><%=strgatewayStatus%></td>
                <td>
                    <% if (schedulerSetting.getReportType() == 99) {%>
                    <button class="btn btn-mini" onclick="GetSchedulerReport('<%=arrtracking[i].getSchedulerTrackingId() %>','<%=0%>','<%=0%>')" type="button"><i class="icon-file"></i>&nbsp; PDF </button>

                    <button class="btn btn-mini" onclick="GetSchedulerReport('<%=arrtracking[i].getSchedulerTrackingId()%>','<%=1%>','<%=0%>')" type="button"><i class="icon-file"></i>&nbsp; CSV </button>
                    <%} else if (schedulerSetting.getReportType() == 0) {%>

                    <button class="btn btn-mini" onclick="GetSchedulerReport('<%=arrtracking[i].getSchedulerTrackingId()%>','<%=0%>','<%=1%>')" type="button"><i class="icon-file"></i>&nbsp; PDF</button>
                    <%} else if (schedulerSetting.getReportType() == 1) {%>

                    <button class="btn btn-mini" onclick="GetSchedulerReport('<%=arrtracking[i].getSchedulerTrackingId()%>','<%=1%>','<%=1%>')" type="button"><i class="icon-file"></i>&nbsp; CSV</button>
                    <%}%>
                </td>

                <td><%=arrtracking[i].getExecutedOn() %></td>
            </tr>
            <%}
         } else {%>
            <tr>
                <td><%=strmsg%></td>
                <td><%=strmsg%></td>
                <td><%=strmsg%></td>
                <td><%=strmsg%></td>
                <td><%=strmsg%></td>
                <td><%=strmsg%></td>
                <td><%=strmsg%></td>

            </tr>
            <%}%>
        </table>
    </div>
           <td><a href="./scheduler.jsp" class="btn" type="button"> << Back </a></td>

</div>
</div>

<%@include file="footer.jsp" %>