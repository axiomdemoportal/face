<%@page import="java.util.Enumeration"%>
<%@page import="com.mollatech.axiom.nucleus.settings.OCRSettings"%>
<%@page import="java.util.Hashtable"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Templates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/ocrdetails.js"></script>
<div class="container-fluid">
    <h1 class="text-success">Templete OCR Configuration</h2>
        <hr>
        <div class="row-fluid">
            <form class="form-horizontal" id="otpsettingsform" name="otpsettingsform">

<!--                <input type="hidden" id="_OCRIp" name="_OCRIp">
                <input type="hidden" id="_OCRPort" name="_OCRPort">
                <input type="hidden" id="_PDFPath" name="_PDFPath">

                <input type="hidden" id="_SFTPUsername" name="_SFTPUsername">
                <input type="hidden" id="_SFTPPassword" name="_SFTPPassword">
                <input type="hidden" id="_SFTPIp" name="_SFTPIp">

                <input type="hidden" id="_SFTPPort" name="_SFTPPort">-->


                <div class="control-group">
                    <label class="control-label"  for="username">OCR Service</label>
                    <div class="controls">
                        <div> 
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; IP :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <div class="btn-group">
                                <input type="text" id="_ocrIp" name="_ocrIp">
                            </div>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Port :
                            <div class="btn-group">
                                <input type="text" id="_ocrPort" name="_ocrPort">
                            </div>                        

                        </div>
                    </div>
                </div>
                <!--<hr>-->
                <div class="control-group">
                    <!--<label class="control-label"  for="username">Out of Band Token</label>-->
                    <div class="controls">
                        <div> 
                            &nbsp;&nbsp;&nbsp;&nbsp; PDF upload path :
                            <div class="btn-group">
                                <input type="text" id="_pdfPath" name="_pdfPath">
                            </div>

                        </div>
                    </div>
                </div>
                <hr>

                <div class="control-group">
                    <label class="control-label"  for="username">SFTP Setting</label>
                    <div class="controls">
                        <div> 
                            &nbsp;&nbsp;&nbsp;&nbsp;  Username :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <div class="btn-group">
                                <input type="text" id="_sftpUsername" name="_sftpUsername">   
                            </div>
                            &nbsp;&nbsp;  Password :
                            <div class="btn-group">
                                <input type="password" id="_sftpPassword" name="_sftpPassword">   
                            </div>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div> 
                            &nbsp;&nbsp;&nbsp;&nbsp; IP ::&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                            <div class="btn-group">
                                <input type="text" id="_sftpIp" name="_sftpIp">   
                            </div>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Port :
                            <div class="btn-group">
                                <input type="text" id="_sftpPort" name="_sftpPort">   
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <!-- Submit -->
                <div class="control-group">
                    <div class="controls">
                        <div id="save-otp-settings-result"></div>
                        <% //if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) { %>                                
                        <!--<button class="btn btn-primary" onclick="editOCRSettings()" type="button">Update OCR Setting Now >> </button>-->
                        <button class="btn btn-primary" onclick="editOCRSettings()" type="button">Save OCR Setting Now >> </button>
                        <% //} %>
                    </div>
                </div>

            </form>
        </div>


        <script language="javascript" type="text/javascript">
            LoadOCRSettings();
            </script>
            <script>
                function strcmpTokens(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function Alert4Tokens(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}

                function editOCRSettings() {
    var s = './editocrsettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#otpsettingsform").serialize(),
        success: function(data) {
            if (strcmpTokens(data._result, "error") == 0) {
                $('#save-otp-settings-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpTokens(data._result, "success") == 0) {
//                $('#save-otp-settings-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}



function LoadOCRSettings() {
    var s = './loadocrsettings';

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {

            $('#_ocrIp').val(data._ocrIp);
            $('#_ocrPort').val(data._ocrPort);
            $('#_pdfPath').val(data._pdfPath);
            $('#_sftpUsername').val(data._sftpUsername);
            $('#_sftpPassword').val(data._sftpPassword);
            $('#_sftpIp').val(data._sftpIp);
            $('#_sftpPort').val(data._sftpPort);
     
        }
    });
}

                </script>
</div>


<%@include file="footer.jsp" %>