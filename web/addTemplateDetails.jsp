<%@page import="java.io.InputStream"%>
<%@page import="org.bouncycastle.util.encoders.Base64"%>
<!DOCTYPE html>
<html lang="en">
    <%@page import="java.awt.image.BufferedImage"%>
    <%@page import="java.io.ByteArrayOutputStream"%>
    <%@page import="java.io.File"%>"
    <%@page import="javax.imageio.ImageIO"%> 
    <%@page import="java.io.FileReader"%>
    <%@page import="java.io.BufferedReader"%>
    <%@page import="java.nio.channels.FileChannel"%>
    <%@page import="java.io.FileInputStream"%>
    <%@page import="org.apache.commons.io.FileUtils"%>
    <%@page import="java.io.File"%>
    <%@page import="org.json.JSONObject"%>
    <%@page import="java.io.ByteArrayInputStream"%>
    <%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
    <%@page import="com.mollatech.axiom.nucleus.db.DocumentTemplate"%>
    <%@page import="com.mollatech.axiom.nucleus.db.connector.management.DocsTemplatesManagement"%>

    <link href="assets/dococr/CSSJS/bootstrap.css" rel="stylesheet" type="text/css"/>
    <script src="assets/dococr/CSSJS/Js/bootstrap.js" type="text/javascript"></script>
    <script src="assets/dococr/CSSJS/Js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="assets/dococr/CSSJS/Js/mdb.js" type="text/javascript"></script>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <script src="assets/dococr/js/jquery.min.js" type="text/javascript"></script>
    <script src="assets/dococr/js/jquery.Jcrop.js" type="text/javascript"></script>
    <script src="assets/dococr/js/documentsTemplate.js" type="text/javascript"></script>

    <link href="assets/dococr/css/demo_files/main.css" rel="stylesheet" type="text/css"/>
    <link href="assets/dococr/css/demo_files/demos.css" rel="stylesheet" type="text/css"/>
    <link href="assets/dococr/css/jquery.Jcrop.css" rel="stylesheet" type="text/css"/>

    <div class="card text-center">
        <script type="text/javascript">

            jQuery(function ($) {

                var jcrop_api;

                $('#target').Jcrop({
                    onChange: showCoords,
                    onSelect: showCoords,
                    onRelease: clearCoords
                }, function () {
                    jcrop_api = this;
                });

                $('#coords').on('change', 'input', function (e) {
                    var x1 = $('#x1').val(),
                            x2 = $('#x2').val(),
                            y1 = $('#y1').val(),
                            y2 = $('#y2').val();
                    jcrop_api.setSelect([x1, y1, x2, y2]);
                });

            });

            // Simple event handler, called from onChange and onSelect
            // event handlers, as per the Jcrop invocation above
            function showCoords(c)
            {
                $('#x1').val(c.x);
                $('#y1').val(c.y);
                $('#x2').val(c.x2);
                $('#y2').val(c.y2);
                $('#w').val(c.w);
                $('#h').val(c.h);
            }
            ;

            function clearCoords()
            {
                $('#coords input').val('');
            }
            ;
        </script>
        <%
            String templatename = request.getParameter("templateName");
            String encodedImage = (String) request.getSession().getAttribute("preprocessImage");
            ////////PreprocessImage///////////////////////  
            byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(encodedImage);
            BufferedImage img = ImageIO.read(new ByteArrayInputStream(imageBytes));
            request.getSession().setAttribute("_finalImage", imageBytes);
            String width = Integer.toString(img.getWidth());
            String height = Integer.toString(img.getHeight());
        %>
        <div class="card-header">
            <!--Tabs-->
            <form id="coords"
                  class="coords"
                  onsubmit="return false;"
                  action="http://example.com/post.php">
                <div class="inline-labels">
                    <label>Field name :</label>
                    <input size="10" id="fieldname" class="form-control" name="fieldname" style="height: 30px !important" type="text" placeholder="Enter field name"/> 
                    <label>Data type :</label>
                    <select id="datatype" name="datatype"  style="width:150px;height: 30px !important">
                        <option value="num">Numeric</option>
                        <option value="alphanum">Alpha-numeric</option>
                        <option value="alpha">Alphabetic</option>
                    </select>
                    <select id="ocrt_type" name="ocrt_type"  style="width:150px;height: 30px !important">
                        <option value="ZON">zonal</option>
                        <option value="SCAN">scaned</option>
                    </select>
                    <label>X1 <input type="text" class="form-control" size="4" id="x1" name="x1" style="width: 50px;height: 30px !important"/></label>
                    <label>Y1 <input type="text" class="form-control" size="4" id="y1" name="y1" style="width: 50px;height: 30px !important"/></label>
                    <label>X2 <input type="text" class="form-control" size="4" id="x2" name="x2" style="width: 50px;height: 30px !important"/></label>
                    <label>Y2 <input type="text" class="form-control" size="4" id="y2" name="y2" style="width: 50px;height: 30px !important"/></label>
                    <label>W <input type="text" size="4" id="w" name="w" style="width: 50px;height: 30px !important"/></label>
                    
                    <!--<label>H <input type="text" size="4" id="h" name="h" style="width: 50px;height: 30px !important"/></label>-->
                    <input type="hidden" id="templatename" name="templatename" value="<%=templatename%>">
                    <button type="button" onclick="saveocrdata()" class="btn btn-success" style="vertical-align: super !important">Save</button>
                    <!--<button type="button" onclick="saveKeyocrPoint()" class="btn btn-success" style="vertical-align: super; !imporatant">Save</button>-->
                    <a href="./addJsonForTable.jsp?templatename=<%=templatename%>"><button type="button" class="btn btn-success" style="vertical-align: super !important" >Add table</button></a>
                    <a href="./TestResult.jsp"><button type="button" class="btn btn-success" style="vertical-align: super !important" >Test</button></a>
                    <a href="./home.jsp"><button type="button" class="btn btn-danger" style="vertical-align: super !important">Cancel</button></a>
                </div>
            </form>
        </div>
        <div class="card-body">
            <div div class="jc-demo-box col-md-12" style="overflow-y: auto;height:450px;border: black;">
                <div class="span12" >
                    <img style="width: '<%=width%>px';height: '<%=height%>px'" src="data:image/png;base64,<%=encodedImage%>" id="target" alt="[Jcrop Example]" />
                </div>
            </div>
        </div>
    </div>