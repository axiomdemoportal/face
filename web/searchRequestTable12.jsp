<%@page import="com.mollatech.axiom.nucleus.crypto.ChannelProfile"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.connector.access.controller.ApprovalSetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.Operators"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.AxiomProtect"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.db.Approvalsettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.AuthorizationManagement"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.connector.user.TokenStatusDetails"%>
<%@page import="com.mollatech.axiom.nucleus.db.Certificates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Pkitokens"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement"%>
<%--<%@include file="header.jsp" %>--%>
<script src="./assets/js/authorization.js"></script>
<!--<div class="container-fluid">-->
<%
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();
    AuthorizationManagement aObj = new AuthorizationManagement();

    SettingsManagement sMngmt = new SettingsManagement();
    OperatorsManagement opMngt = new OperatorsManagement();
    Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
    ChannelProfile chSettingObj = null;
    if (settingsObj != null) {
        chSettingObj = (ChannelProfile) settingsObj;
    }
    if (chSettingObj != null) {
        Date startDate = new Date();
        Approvalsettings[] arrexpiredRequest = aObj.getExpiredRequest(sessionId, _channelId, startDate, AuthorizationManagement.AUTORIZATION_PENDING_STATUS);

        if (arrexpiredRequest != null) {
            for (int i = 0; i < arrexpiredRequest.length; i++) {
                aObj.expiredAuthorizationRequest(sessionId, _channelId, arrexpiredRequest[i].getApprovalId());
            }
        }
    }

    String oprid = request.getParameter("_unitIDs");
    String _searchType = request.getParameter("_searchType");

    int type = 0;
    if (_searchType.isEmpty() == false) {
        type = Integer.parseInt(_searchType);
    }
//            int unitIDs = Integer.parseInt(arrEvents);

    Approvalsettings[] arrRequest = null;
    if (type == 0) {    //ALL 
        arrRequest = aObj.getALLPendingRequest(sessionId, _channelId, AuthorizationManagement.AUTORIZATION_PENDING_STATUS);
    } else {
        arrRequest = aObj.getPendingRequestv2(sessionId, channel.getChannelid(),
                type, AuthorizationManagement.AUTORIZATION_PENDING_STATUS, 0, oprid);
       
    }
    String strerr = "No Record Found";

%>

<div class="row-fluid">
    <div id="licenses_data_table">
        <h3>Search Result</h3>
        <table class="table table-striped" id="table_main">

            <tr>
                <td>No.</td>
                <td>Requester's Action</td>

                <td>User Name</td>
                <td>Marked By</td>

                <td>Action</td>
                <td>Created On</td>
                <td>Expired On</td>
                <!--<td>Updated On</td>-->
            </tr>
            <%  Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
//                System.out.println(operatorS.getOperatorType());
//                System.out.println(arrRequest);
//                System.out.println(arrRequest.length);

                if (operatorS.getOperatorType() == OperatorsManagement.AUTHORIZER)
                    if (arrRequest != null) {
//                                OperatorsManagement opMngt = new OperatorsManagement();
                        UserManagement uMngt = new UserManagement();
                        for (int i = 0; i < arrRequest.length; i++) {

                            byte[] obj = arrRequest[i].getApprovalSettingEntry();
                            byte[] f = AxiomProtect.AccessDataBytes(obj);
                            ByteArrayInputStream bais = new ByteArrayInputStream(f);
                            Object object = AuthorizationManagement.deserializeFromObject(bais);

                            ApprovalSetting approvalSetting = null;
                            if (object instanceof ApprovalSetting) {
                                approvalSetting = (ApprovalSetting) object;
                            }

                            String strStatus = "-";
                            if (arrRequest[i].getStatus() == 1) {
                                strStatus = "Active";
                            } else if (arrRequest[i].getStatus() == 2) {
                                strStatus = "Pending";
                            }

            %>

            <tr>
                <%if (approvalSetting != null) {

                        if (!approvalSetting.makerid.equals(operatorS.getOperatorid())) {

                            Operators oprbj = opMngt.getOperatorById(_channelId, approvalSetting.makerid);

                            int approve = 1;

                            if (chSettingObj.authorizationunit == ChannelProfile._AUTHARIZATIONBYSAMEUNIT) {
                                if (oprbj.getUnits() != operatorS.getUnits()) {
                                    approve = 0;
                                }
                            }

                            if (approve == 1) {
                                AuthUser u = uMngt.getUser(sessionId, _channelId, approvalSetting.userId);
                                String userName = "-";
                                if (u != null) {
                                    userName = u.getUserName();
                                }

                %>
                <td><%=(i + 1)%></td>
                <%if (approvalSetting.tokenSerialNo != null) {%>
                <td><%=approvalSetting.itemid%> serial no =<%=approvalSetting.tokenSerialNo%> </td>
                <%} else {%>
                <td><%=approvalSetting.itemid%> </td>
                <%}%>
                 <!--<td><%=strStatus%></td>-->
                <td><%=userName%></td>
                <td><%= oprbj.getName()%></td>
                <td>
                    <div class="btn-group">
                        <span id="<%=strStatus%>" class="label-success"><%=strStatus%></span>
                        <button class="btn btn-mini" id="<%=strStatus%>" ><%=strStatus%></button>                        
                        <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <%if (approvalSetting.action == AuthorizationManagement.ASSIGN_OTP_TOKEN) {%>
                            <li><a href="#" onclick="ApproveRequest('assignOtpTokenForm', 'assigntoken', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="assignOtpTokenForm+<%=arrRequest[i].getApprovalId()%>">
                                <input type="hidden" id="_category+<%=arrRequest[i].getApprovalId()%>" name="_category" value="<%=approvalSetting.tokenCategory%>">
                                <input type="hidden" id="_subcategory+<%=arrRequest[i].getApprovalId()%>" name="_subcategory" value="<%=approvalSetting.tokenSubCategory%>">
                                <input type="hidden" id="_serialnumber+<%=arrRequest[i].getApprovalId()%>" name="_serialnumber" value="<%=approvalSetting.tokenSerialNo%>">
                                <input type="hidden" id="_userid+<%=arrRequest[i].getApprovalId()%>" name="_userid" value="<%=approvalSetting.userId%>">
                            </form>
                            <%}
                                if (approvalSetting.action == AuthorizationManagement.CHANGE_OTP_TOKEN_TYPE) {%>
                            <li><a href="#" onclick="ApproveRequest('changeOtpTokenForm', 'changetokentype', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="changeOtpTokenForm">
                                <input type="hidden" id="_category" name="_category" value="<%=approvalSetting.tokenCategory%>">
                                <input type="hidden" id="_subcategory" name="_subcategory" value="<%=approvalSetting.tokenSubCategory%>">
                                <input type="hidden" id="_userid" name="_userid" value="<%=approvalSetting.userId%>">
                            </form>
                            <%}
                                if (approvalSetting.action == AuthorizationManagement.CHANGE_OTP_TOKEN_STATUS) {%>
                            <li><a href="#" onclick="ApproveRequest('changeOtpTokenStatusForm', 'changeotpstatus', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="changeOtpTokenStatusForm">
                                <input type="hidden" id="_category" name="_category" value="<%=approvalSetting.tokenCategory%>">
                                <input type="hidden" id="_subcategory" name="_subcategory" value="<%=approvalSetting.tokenSubCategory%>">
                                <input type="hidden" id="_userid" name="_userid" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_status" name="_status" value="<%=approvalSetting.tokenStatus%>">
                            </form>
                            <%}
                                if (approvalSetting.action == AuthorizationManagement.HW_OTP_UPLOAD) {%>
                            <li><a href="#" onclick="ApproveRequest('uploadTokenForm', 'hwotptokeuploadsubmit', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="uploadTokenForm">
                                <input type="hidden" id="_otpXmlFliePath" name="_otpXmlFliePath" value="<%=approvalSetting.xmlfilepath%>">
                                <input type="hidden" id="_otpPasswordFliePath" name="_otpPasswordFliePath" value="<%=approvalSetting.passwordfilepath%>">
                                <input type="hidden" id="_UnitId" name="_UnitId" value="<%=approvalSetting.units_id%>">
                            </form>
                            <%}
                                if (approvalSetting.action == AuthorizationManagement.OTP_TOKEN_REPLACE) {%>
                            <li><a href="#" onclick="ApproveRequest('replaceTokenForm', 'replaceToken', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="replaceTokenForm">
                                <input type="hidden" id="_userIDReplaceHW" name="_userIDReplaceHW" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_oldTokenSerialNo" name="_oldTokenSerialNo" value="<%=approvalSetting.tokenSerialNoOld%>">
                                <input type="hidden" id="_newTokenSerialNo" name="_newTokenSerialNo" value="<%=approvalSetting.tokenSerialNo%>">
                                <input type="hidden" id="_oldTokenStatus" name="_oldTokenStatus" value="<%=approvalSetting.tokenStatus%>">
                                <input type="hidden" id="_categoryReplaceHW" name="_categoryReplaceHW" value="<%=approvalSetting.tokenCategory%>">
                                <input type="hidden" id="_subcategoryReplaceHW" name="_subcategoryReplaceHW" value="<%=approvalSetting.tokenSubCategory%>">
                            </form>
                            <%}
                                if (approvalSetting.action == AuthorizationManagement.ASSIGN_CERTIFICATE) {%>
                            <li><a href="#" onclick="ApproveRequest('assignCertificateForm', 'generatecertificate', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="assignCertificateForm">
                                <input type="hidden" id="_userid" name="_userid" value="<%=approvalSetting.userId%>">
                            </form>
                            <%}
                                if (approvalSetting.action == AuthorizationManagement.SEND_CERTIFICATE) {%>
                            <li><a href="#" onclick="ApproveRequest('sendCertificateForm', 'sendcertfile', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="sendCertificateForm">
                                <input type="hidden" id="_userSENDCERT" name="_userSENDCERT" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_emailSENDCERT" name="_emailSENDCERT" value="<%=approvalSetting.sendCertTo%>">
                            </form>
                            <%}
                                if (approvalSetting.action == AuthorizationManagement.RENEW_CERTIFICATE) {%>
                            <li><a href="#" onclick="ApproveRequest('renewCertificateForm', 'renewcertificate', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="sendCertificateForm">
                                <input type="hidden" id="_userid" name="_userid" value="<%=approvalSetting.userId%>">
                            </form>
                            <%}
                                if (approvalSetting.action == AuthorizationManagement.REVOKE_CERTIFICATE) {%>
                            <li><a href="#" onclick="ApproveRequest('revokeCertificateForm', 'revokecertificate', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="revokeCertificateForm">
                                <input type="hidden" id="_userid" name="_userid" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_reason" name="_reason" value="<%=approvalSetting.cerRevokeReason%>">
                            </form>
                            <%}
                                if (approvalSetting.action == AuthorizationManagement.ASSIGN_HW_PKI_TOKEN) {%>
                            <li><a href="#" onclick="ApproveRequest('assignhwPkiForm', 'assignnewtoken', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="assignhwPkiForm">
                                <input type="hidden" id="_userid" name="_userid" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_category" name="_category" value="<%=approvalSetting.tokenCategory%>">
                                <input type="hidden" id="_ser" name="_ser" value="<%=approvalSetting.tokenSerialNo%>">
                            </form>
                            <%}
                                if (approvalSetting.action == AuthorizationManagement.PKI_TOKEN_STATUS) {%>
                            <li><a href="#" onclick="ApproveRequest('changePkitokenstatusForm', 'changepkitokenstatus', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="changePkitokenstatusForm">
                                <input type="hidden" id="_userid" name="_userid" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_category" name="_category" value="<%=approvalSetting.tokenCategory%>">
                                <input type="hidden" id="_status" name="_status" value="<%=approvalSetting.tokenStatus%>">
                            </form>
                            <%}
                                if (approvalSetting.action == AuthorizationManagement.PKI_SEND_PFX) {%>
                            <li><a href="#" onclick="ApproveRequest('sendPfxFileForm', 'sendpfxfile', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="sendPfxFileForm">
                                <input type="hidden" id="_userid" name="_userid" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_category" name="_category" value="<%=approvalSetting.tokenCategory%>">
                            </form>
                            <%}
                                if (approvalSetting.action == AuthorizationManagement.PKI_SEND_PFX_PASSWORD) {%>
                            <li><a href="#" onclick="ApproveRequest('sendPfxPasswordFileForm', 'sendpfxpassword', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="sendPfxPasswordFileForm">
                                <input type="hidden" id="_userid" name="_userid" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_category" name="_category" value="<%=approvalSetting.tokenCategory%>">
                            </form>
                            <%}
                                if (approvalSetting.action == AuthorizationManagement.PKI_SEND_REGCODE) {%>
                            <li><a href="#" onclick="ApproveRequest('sendRegCodeForm', 'sendpkitokenregistration', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="sendRegCodeForm">
                                <input type="hidden" id="_userid" name="_userid" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_category" name="_category" value="<%=approvalSetting.tokenCategory%>">
                            </form>
                            <%}
                                if (approvalSetting.action == AuthorizationManagement.TWO_WAY_AUTH_STATUS) {%>
                            <li><a href="#" onclick="ApproveRequest('twoWayAuthStatusForm', 'EditAuth', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="twoWayAuthStatusForm">
                                <input type="hidden" id="userid" name="userid" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="status" name="status" value="<%=approvalSetting.status1%>">
                                <input type="hidden" id="type" name="type" value="<%=approvalSetting.type%>">
                            </form>
                            <%}
                                if (approvalSetting.action == AuthorizationManagement.TWO_WAY_AUTH_REGCODE) {%>
                            <li><a href="#" onclick="ApproveRequest('twoWayAuthSendRegocdeForm', 'SendPushRegCode', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="twoWayAuthSendRegocdeForm">
                                <input type="hidden" id="userid" name="userid" value="<%=approvalSetting.userId%>">
                            </form>
                            <%}
                                if (approvalSetting.action == AuthorizationManagement.SYSTEM_CHANGEPASSWORD) {%>
                            <li><a href="#" onclick="ApproveRequest('changeOperatorPasswordForm', 'changeoprpassword', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="changeOperatorPasswordForm">
                                <input type="hidden" id="_oldPassword" name="_oldPassword" value="<%=approvalSetting.oldvalue%>">
                                <input type="hidden" id="_newPassword" name="_newPassword" value="<%=approvalSetting.password%>">
                                <input type="hidden" id="_confirmPassword" name="_confirmPassword" value="<%=approvalSetting.newValue%>">
                            </form>
                            <%}
                                if (approvalSetting.action == AuthorizationManagement.CHANNEL_ADD) {%>
                            <li><a href="#" onclick="ApproveRequest('addchannelForm', 'addchannel', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="addchannelForm">
                                <input type="hidden" id="_ch_name" name="_ch_name" value="<%=approvalSetting.chanel_name%>">
                                <input type="hidden" id="_ch_virtual_path" name="_ch_virtual_path" value="<%=approvalSetting.chanel_virtual_path%>">
                                <input type="hidden" id="_ch_status" name="_ch_status" value="<%=approvalSetting.chanel_status%>">
                                <input type="hidden" id="_op_name" name="_op_name" value="<%=approvalSetting.chanel_operator_name%>">
                                <input type="hidden" id="_op_email" name="_op_email" value="<%=approvalSetting.chanel_operator_email%>">
                                <input type="hidden" id="_op_phone" name="_op_phone" value="<%=approvalSetting.chanel_operator_phone%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.CHANNEL_EDIT) {%>
                            <li><a href="#" onclick="ApproveRequest('editchannelForm', 'editchannel', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="editchannelForm">
                                <input type="hidden" id="_ch_nameE" name="_ch_nameE" value="<%=approvalSetting.chanel_name%>">
                                <input type="hidden" id="_ch_virtual_pathE" name="_ch_virtual_pathE" value="<%=approvalSetting.chanel_virtual_path%>">
                                <input type="hidden" id="_ch_statusE" name="_ch_statusE" value="<%=approvalSetting.chanel_status%>">
                                <input type="hidden" id="_channelidE" name="_channelidE" value="<%=approvalSetting.chanel_channelid%>">
                                <input type="hidden" id="_rem_expiryminE" name="_rem_expiryminE" value="<%=approvalSetting.chanel_expirymin%>">
                                <input type="hidden" id="_rem_loginidE" name="_rem_loginidE" value="<%=approvalSetting.chanel_remote_loginid%>">
                                <input type="hidden" id="_rem_passwordE" name="_rem_passwordE" value="<%=approvalSetting.chanel_remote_password%>">
                                <input type="hidden" id="_rem_passwordEC" name="_rem_passwordEC" value="<%=approvalSetting.chanel_remote_password_confirm%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.CHANNEL_STATUS) {%>
                            <li><a href="#" onclick="ApproveRequest('channelStatusForm', 'changestatus', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="channelStatusForm">
                                <input type="hidden" id="_ch_status" name="_ch_status" value="<%=approvalSetting.chanel_status%>">
                                <input type="hidden" id="_channelid" name="_channelid" value="<%=approvalSetting.chanel_channelid%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.CHANNEL_REMOTE_ACCESS) {%>
                            <li><a href="#" onclick="ApproveRequest('channelRemoteAccessForm', 'changeremoteaccess', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="channelRemoteAccessForm">
                                <input type="hidden" id="_ch_rem_status" name="_ch_rem_status" value="<%=approvalSetting.chanel_status%>">
                                <input type="hidden" id="_channelid" name="_channelid" value="<%=approvalSetting.chanel_channelid%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.OPERATOR_ADD) {%>
                            <li><a href="#" onclick="ApproveRequest('addOperatorForm', 'addoperator', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="addOperatorForm">
                                <input type="hidden" id="_oprname" name="_oprname" value="<%=approvalSetting.operator_name%>">
                                <input type="hidden" id="_oprphone" name="_oprphone" value="<%=approvalSetting.operator_phone%>">
                                <input type="hidden" id="_opremail" name="_opremail" value="<%=approvalSetting.operator_email%>">
                                <input type="hidden" id="_oprrole" name="_oprrole" value="<%=approvalSetting.operator_role%>">
                                <input type="hidden" id="_units" name="_units" value="<%=approvalSetting.operator_units%>">
                                <input type="hidden" id="_operatorType" name="_operatorType" value="<%=approvalSetting.operator_type%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.OPERATOR_EDIT) {%>
                            <li><a href="#" onclick="ApproveRequest('editOperatorForm', 'editoperator', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="editOperatorForm">
                                <input type="hidden" id="_oprnameE" name="_oprnameE" value="<%=approvalSetting.operator_name%>">
                                <input type="hidden" id="_oprphoneE" name="_oprphoneE" value="<%=approvalSetting.operator_phone%>">
                                <input type="hidden" id="_opremailE" name="_opremailE" value="<%=approvalSetting.operator_email%>">
                                <input type="hidden" id="_oprroleidE" name="_oprroleidE" value="<%=approvalSetting.operator_role%>">
                                <input type="hidden" id="_oprstatusE" name="_oprstatusE" value="<%=approvalSetting.operator_status%>">
                                <input type="hidden" id="_unitsEDIT" name="_unitsEDIT" value="<%=approvalSetting.operator_units%>">
                                <input type="hidden" id="_operatorTypeE" name="_operatorTypeE" value="<%=approvalSetting.operator_type%>">
                                <input type="hidden" id="_opridE" name="_opridE" value="<%=approvalSetting.operator_id%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.OPERATOR_GRANT_ACCESS) {
                            %>
                            <li><a href="#" onclick="ApproveRequest('grantAccessOperatorForm', 'grantAccessOperator', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="grantAccessOperatorForm">
                                <input type="hidden" id="_oprAccessId" name="_oprAccessId" value="<%=approvalSetting.operator_id%>">
                                <input type="hidden" id="_accessStartDate" name="_accessStartDate" value="<%=approvalSetting.operator_access_startdate%>">
                                <input type="hidden" id="_accessEndDate" name="_accessEndDate" value="<%=approvalSetting.operator_access_enddate%>">
<!--                                        <input type="hidden" id="_oprAccessId" name="_oprAccessId" value="<%=1%>">
                                <input type="hidden" id="_accessStartDate" name="_accessStartDate" value="<%=1%>">
                                <input type="hidden" id="_accessEndDate" name="_accessEndDate" value="<%=1%>">-->
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.OPERATOR_ACCESS_TYPE) {
                            %>
                            <li><a href="#" onclick="ApproveRequest('grantAccessTypeForm', 'changeOperatorAccess', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="grantAccessOperatorForm">
                                <input type="hidden" id="_oprAccessId" name="_oprAccessId" value="<%=approvalSetting.operator_id%>">
                                <input type="hidden" id="_op_type" name="_op_type" value="<%=approvalSetting.operator_type%>">

                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.OPERATOR_STATUS) {%>
                            <li><a href="#" onclick="ApproveRequest('changeOperatorStatusForm', 'changeoprstatus', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="changeOperatorStatusForm">
                                <input type="hidden" id="_op_status" name="_op_status" value="<%=approvalSetting.operator_status%>">
                                <input type="hidden" id="_oprid" name="_oprid" value="<%=approvalSetting.operator_id%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.OPERATOR_ROLE) {%>
                            <li><a href="#" onclick="ApproveRequest('changeOperatorRoleForm', 'changeoprrole', '<%=arrRequest[i].getApprovalId()%>', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="changeOperatorRoleForm">
                                <input type="hidden" id="_oprid" name="_oprid" value="<%=approvalSetting.operator_id%>">
                                <input type="hidden" id="_roleId" name="_roleId" value="<%=approvalSetting.operator_role%>">
                                <input type="hidden" id="_roleName" name="_roleName" value="<%=approvalSetting.operator_role_name%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.OPERATOR_RESEND_PASSWORD) {%>
                            <li><a href="#" onclick="ApproveRequest('operatorResendForm', 'resendpassword', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="operatorResendForm">
                                <input type="hidden" id="_oprid" name="_oprid" value="<%=approvalSetting.operator_id%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.OPERATOR_SEND_RANDOM_PASSWORD) {%>
                            <li><a href="#" onclick="ApproveRequest('operatorSendRandomPasswordForm', 'sendrandompassword', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="operatorResendForm">
                                <input type="hidden" id="_oprid" name="_oprid" value="<%=approvalSetting.operator_id%>">
                                <input type="hidden" id="_send" name="_send" value="<%=approvalSetting.operator_send_password%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.UNIT_STATUS) {%>
                            <li><a href="#" onclick="ApproveRequest('unitStatusForm', 'changeUnitsStatus')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>', '<%=arrRequest[i].getApprovalId()%>')">Reject</a></li>
                            <form class="form-horizontal" id="unitStatusForm">
                                <input type="hidden" id="_unitId" name="_unitId" value="<%=approvalSetting.units_id%>">
                                <input type="hidden" id="_status" name="_status" value="<%=approvalSetting.units_status%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.UNIT_EDIT) {%>
                            <li><a href="#" onclick="ApproveRequest('unitEditForm', 'editUnit', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="unitEditForm">
                                <input type="hidden" id="_unitNameE" name="_unitNameE" value="<%=approvalSetting.units_name%>">
                                <input type="hidden" id="_unitId" name="_unitId" value="<%=approvalSetting.units_id%>">
                                <input type="hidden" id="_oldunitNameE" name="_oldunitNameE" value="<%=approvalSetting.units_name_old%>">
                                <input type="hidden" id="_thresholdLimitO" name="_thresholdLimitO" value="<%=approvalSetting.units_threshold_limit_old%>">
                                <input type="hidden" id="_thresholdLimitE" name="_thresholdLimitE" value="<%=approvalSetting.units_threshold_limit%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.UNIT_SHIFT_OPERATORS) {%>
                            <li><a href="#" onclick="ApproveRequest('unitShiftOperatotrsForm', 'shiftUnitsOperators', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="unitShiftOperatotrsForm">
                                <input type="hidden" id="_unitS" name="_unitS" value="<%=approvalSetting.units_id%>">
                                <input type="hidden" id="_unitIdS" name="_unitIdS" value="<%=approvalSetting.units_id_old%>">
                                <input type="hidden" id="_oldunitNameS" name="_oldunitNameS" value="<%=approvalSetting.units_name_old%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.UNIT_ASSIGN_TOKEN) {%>
                            <li><a href="#" onclick="ApproveRequest('unitAssignTokenForm', 'assignTokenToUnit', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="unitAssignTokenForm">
                                <input type="hidden" id="_unitIdA" name="_unitIdA" value="<%=approvalSetting.units_id%>">
                                <input type="hidden" id="_oldunitNameA" name="_oldunitNameA" value="<%=approvalSetting.units_name%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.UNIT_REMOVE) {%>
                            <li><a href="#" onclick="ApproveRequest('unitRemoveForm', 'removeUnit', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="unitRemoveForm">
                                <input type="hidden" id="_tid" name="_tid" value="<%=approvalSetting.units_id%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.UNIT_ADD) {%>
                            <li><a href="#" onclick="ApproveRequest('unitAddForm', 'addUnit', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="unitAddForm">
                                <input type="hidden" id="_unitName" name="_unitName" value="<%=approvalSetting.units_name%>">
                                <input type="hidden" id="_unitStatus" name="_unitStatus" value="<%=approvalSetting.units_status%>">
                                <input type="hidden" id="_thresholdLimit" name="_thresholdLimit" value="<%=approvalSetting.units_threshold_limit%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.USERGROUP_ADD) {%>
                            <li><a href="#" onclick="ApproveRequest('usergroupAddForm', 'AddGroup', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="usergroupAddForm">
                                <input type="hidden" id="_unitName" name="_unitName" value="<%=approvalSetting.units_name%>">
                                <input type="hidden" id="_unitStatus" name="_unitStatus" value="<%=approvalSetting.units_status%>">
                                <input type="hidden" id="_selectedType" name="_selectedType" value="<%=approvalSetting.selectedType%>">
                                <input type="hidden" id="_selectedCheck" name="_selectedCheck" value="<%=approvalSetting.selectedCheck%>">
                                <input type="hidden" id="_CountryName" name="_CountryName" value="<%=approvalSetting.CountryName%>">
                                <input type="hidden" id="_StateName" name="_StateName" value="<%=approvalSetting.StateName%>">
                                <input type="hidden" id="_cityName" name="_cityName" value="<%=approvalSetting.cityName%>">
                                <input type="hidden" id="_PinCode" name="_PinCode" value="<%=approvalSetting.PinCode%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.USERGROUP_EDIT) {%>
                            <li><a href="#" onclick="ApproveRequest('usergroupEditForm', 'EditGroup', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="usergroupEditForm">
                                <input type="hidden" id="_unitNameE" name="_unitNameE" value="<%=approvalSetting.units_name%>">
                                <input type="hidden" id="_unitStatusE" name="_unitStatusE" value="<%=approvalSetting.units_status%>">
                                <input type="hidden" id="_selectedTypeE" name="_selectedTypeE" value="<%=approvalSetting.selectedType%>">
                                <!--<input type="hidden" id="_selectedCheck" name="_selectedCheck" value="<%=approvalSetting.selectedCheck%>">-->
                                <input type="hidden" id="_CountryNameE" name="_CountryNameE" value="<%=approvalSetting.CountryName%>">
                                <input type="hidden" id="_StateName" name="_StateName" value="<%=approvalSetting.StateName%>">
                                <input type="hidden" id="_cityNameE" name="_cityNameE" value="<%=approvalSetting.cityName%>">
                                <input type="hidden" id="_PinCodeE" name="_PinCodeE" value="<%=approvalSetting.PinCode%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.USERGROUP_STATUS) {%>
                            <li><a href="#" onclick="ApproveRequest('usergroupStatusForm', 'ChangeGroupStatus', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="usergroupStatusForm">
                                <input type="hidden" id="_unitId" name="_unitId" value="<%=approvalSetting.units_id%>">
                                <input type="hidden" id="_status" name="_status" value="<%=approvalSetting.units_status%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.USERGROUP_SHIFT_USERS) {%>
                            <li><a href="#" onclick="ApproveRequest('usergroupShiftUsersForm', 'ShiftGroupUsers', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="usergroupShiftUsersForm">
                                <input type="hidden" id="_unitIdS" name="_unitIdS" value="<%=approvalSetting.units_id_old%>">
                                <input type="hidden" id="_oldGroupNameS" name="_oldGroupNameS" value="<%=approvalSetting.units_name%>">
                                <input type="hidden" id="_unitS" name="_unitS" value="<%=approvalSetting.units_id%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.USERGROUP_DELETE) {%>
                            <li><a href="#" onclick="ApproveRequest('usergroupDeleteForm', 'RemoveGroup', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="usergroupDeleteForm">
                                <input type="hidden" id="groupname" name="groupname" value="<%=approvalSetting.units_name%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.TEMPLATE_ADD_MOBILE) {%>
                            <li><a href="#" onclick="ApproveRequest('templateAddMobileForm', 'savetemplates', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="templateAddMobileForm">
                                <input type="hidden" id="_types" name="_types" value="1">
                                <input type="hidden" id="_templatename" name="_templatename" value="<%=approvalSetting.template_name%>">
                                <input type="hidden" id="_templatebody" name="_templatebody" value="<%=approvalSetting.template_body%>">
                                <input type="hidden" id="_templatesubject" name="_templatesubject" value="<%=approvalSetting.template_subject%>">
                                <input type="hidden" id="_templatevariables" name="_templatevariables" value="<%=approvalSetting.template_varibles%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.TEMPLATE_ADD_EMAIL) {%>
                            <li><a href="#" onclick="ApproveRequest('templateAddEmailForm', 'savetemplates', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="templateAddEmailForm">
                                <input type="hidden" id="_types" name="_types" value="2">
                                <input type="hidden" id="_templatenameS" name="_templatenameS" value="<%=approvalSetting.template_name%>">
                                <input type="hidden" id="emailCotents" name="emailCotents" value="<%=approvalSetting.template_body%>">
                                <input type="hidden" id="_templatesubjectS" name="_templatesubjectS" value="<%=approvalSetting.template_subject%>">
                                <input type="hidden" id="_templatevariablesS" name="_templatevariablesS" value="<%=approvalSetting.template_varibles%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.TEMPLATE_EDIT_EMAIL) {%>
                            <li><a href="#" onclick="ApproveRequest('templateEditEmailForm', 'editemailtemplate', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="templateEditEmailForm">
                                <input type="hidden" id="idEmailTemplateId" name="idEmailTemplateId" value="<%=approvalSetting.template_id%>">
                                <input type="hidden" id="_templateE_name" name="_templateE_name" value="<%=approvalSetting.template_name%>">
                                <input type="hidden" id="_templateE_subject" name="_templateE_subject" value="<%=approvalSetting.template_subject%>">
                                <input type="hidden" id="emailContents" name="emailContents" value="<%=approvalSetting.template_body%>">
                                <input type="hidden" id="_templateE_variable" name="_templateE_variable" value="<%=approvalSetting.template_varibles%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.TEMPLATE_EDIT_MOBILE) {%>
                            <li><a href="#" onclick="ApproveRequest('templateEditMobileForm', 'edittemplates', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="templateEditMobileForm">

                                <input type="hidden" id="idMessageTemplateId" name="idMessageTemplateId" value="<%=approvalSetting.template_id%>">
                                <input type="hidden" id="_templateM_name" name="_templateM_name" value="<%=approvalSetting.template_name%>">
                                <input type="hidden" id="_templateM_body" name="_templateM_body" value="<%=approvalSetting.template_body%>">
                                <input type="hidden" id="_templateM_variable" name="_templateM_variable" value="<%=approvalSetting.template_varibles%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.TEMPLATE_STATUS) {%>
                            <li><a href="#" onclick="ApproveRequest('templateStatusForm', 'changetemplatestatus', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="templateStatusForm">
                                <input type="hidden" id="_templateid" name="_templateid" value="<%=approvalSetting.template_id%>">
                                <input type="hidden" id="_status" name="_status" value="<%=approvalSetting.template_status%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.TEMPLATE_DELETE) {%>
                            <li><a href="#" onclick="ApproveRequest('templateRemoveForm', 'removetemplate', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="templateRemoveForm">
                                <input type="hidden" id="_tid" name="_tid" value="<%=approvalSetting.template_id%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.USER_ADD) {%>
                            <li><a href="#" onclick="ApproveRequest('userAddForm', 'adduser', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="userAddForm">
                                <input type="hidden" id="_Name" name="_Name" value="<%=approvalSetting.userName%>">
                                <input type="hidden" id="_Email" name="_Email" value="<%=approvalSetting.userEmail%>">
                                <input type="hidden" id="_Phone" name="_Phone" value="<%=approvalSetting.userPhone%>">
                                <input type="hidden" id="_groupid" name="_groupid" value="<%=approvalSetting.user_groupId%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.USER_EDIT) {%>
                            <li><a href="#" onclick="ApproveRequest('userEditForm', 'edituser', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="userEditForm">
                                <input type="hidden" id="_userIdE" name="_userIdE" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_NameE" name="_NameE" value="<%=approvalSetting.userName%>">
                                <input type="hidden" id="_EmailE" name="_EmailE" value="<%=approvalSetting.userEmail%>">
                                <input type="hidden" id="_PhoneE" name="_PhoneE" value="<%=approvalSetting.userPhone%>">
                                <input type="hidden" id="_groupid" name="_groupid" value="<%=approvalSetting.user_groupId%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.USER_STATUS) {%>
                            <li><a href="#" onclick="ApproveRequest('userStatusForm', 'changeuserstatus', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="userStatusForm">
                                <input type="hidden" id="_userid" name="_userid" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_status" name="_status" value="<%=approvalSetting.userStatus%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.USER_DELETE) {%>
                            <li><a href="#" onclick="ApproveRequest('userDeleteForm', 'removeuser', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="userDeleteForm">
                                <input type="hidden" id="_userid" name="_userid" value="<%=approvalSetting.userId%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.USER_UNLOCK_PASSWORD) {%>
                            <li><a href="#" onclick="ApproveRequest('userUnlockPasswordForm', 'unlockuserpassword', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="userUnlockPasswordForm">
                                <input type="hidden" id="_userid" name="_userid" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_status" name="_status" value="<%=approvalSetting.userStatus%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.USER_RESEND_PASSWORD) {%>
                            <li><a href="#" onclick="ApproveRequest('userResendPasswordForm', 'resetuserpassword', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="userResendPasswordForm">
                                <input type="hidden" id="_userid" name="_userid" value="<%=approvalSetting.userId%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.USER_RESET_PASSWORD) {%>
                            <li><a href="#" onclick="ApproveRequest('userResetPasswordForm', 'resetuserpassword', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="userResetPasswordForm">
                                <input type="hidden" id="_userid" name="_userid" value="<%=approvalSetting.userId%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.USER_SET_SEND_PASSWORD) {%>
                            <li><a href="#" onclick="ApproveRequest('userResetSendPasswordForm', 'setandresenduserpassword', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="userResetSendPasswordForm">
                                <input type="hidden" id="_userid" name="_userid" value="<%=approvalSetting.userId%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.USER_ERASE_MOBILE_TRUST) {%>
                            <li><a href="#" onclick="ApproveRequest('userEraseMobileTrustForm', 'eraseMobileTrustCredential', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="userEraseMobileTrustForm">
                                <input type="hidden" id="_userid" name="_userid" value="<%=approvalSetting.userId%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.CHALLENGE_RESPONSE_STATUS) {%>
                            <li><a href="#" onclick="ApproveRequest('challengeResponseForm', 'changeChallengestatus', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="challengeResponseForm">
                                <input type="hidden" id="_userid" name="_userid" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_status" name="_status" value="<%=approvalSetting.userStatus%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.CHALLENGE_RESPONSE_DELETE) {%>
                            <li><a href="#" onclick="ApproveRequest('challengeResponseDeleteForm', 'removeChallengeuser', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="challengeResponseDeleteForm">
                                <input type="hidden" id="_userid" name="_userid" value="<%=approvalSetting.userId%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.CHALLENGE_RESPONSE_ADDQUESTION) {%>
                            <li><a href="#" onclick="ApproveRequest('addQuestionForm', 'addQuestion', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="addQuestionForm">
                                <input type="hidden" id="_question" name="_question" value="<%=approvalSetting.challenge_question%>">
                                <input type="hidden" id="_ch_weightage" name="_ch_weightage" value="<%=approvalSetting.challenge_weightage%>">
                                <input type="hidden" id="_ch_status" name="_ch_status" value="<%=approvalSetting.status1%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.CHALLENGE_RESPONSE_EDITQUESTION) {%>
                            <li><a href="#" onclick="ApproveRequest('editQuestionForm', 'editQuestion', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="addQuestionForm">
                                <input type="hidden" id="_eqid" name="_eqid" value="<%=approvalSetting.challenge_questionId%>">
                                <input type="hidden" id="_equestion" name="_equestion" value="<%=approvalSetting.challenge_question%>">
                                <input type="hidden" id="_e_weightage" name="_e_weightage" value="<%=approvalSetting.challenge_weightage%>">
                                <input type="hidden" id="_e_status" name="_e_status" value="<%=approvalSetting.status1%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.SYSTEM_ADD_SCHEDULER) {%>
                            <li><a href="#" onclick="ApproveRequest('addReportSchedularForm', 'addscheduler', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="addReportSchedularForm">
                                <input type="hidden" id="_gatewayType" name="_gatewayType" value="<%=approvalSetting.type%>">
                                <input type="hidden" id="_schedulerStatus" name="_schedulerStatus" value="<%=approvalSetting.status%>">
                                <input type="hidden" id="_gatewayPreference" name="_gatewayPreference" value="<%=approvalSetting.prefernce%>">
                                <input type="hidden" id="_gatewayStatus" name="_gatewayStatus" value="<%=approvalSetting.status1%>">
                                <input type="hidden" id="_operatorRoles" name="_operatorRoles" value="<%=approvalSetting.operator_role%>">
                                <input type="hidden" id="_schedulerDuration" name="_schedulerDuration" value="<%=approvalSetting.duration%>">
                                <input type="hidden" id="_schedulername" name="_schedulername" value="<%=approvalSetting.name%>">
                                <input type="hidden" id="_tagsList" name="_tagsList" value="<%=approvalSetting.email%>">
                                <input type="hidden" id="_reportType" name="_reportType" value="<%=approvalSetting.reportType%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.SYSTEM_STATUS_SCHEDULER) {%>
                            <li><a href="#" onclick="ApproveRequest('changeStatusSchedularForm', 'changeschedulerstatus', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="changeschedulerstatus">
                                <input type="hidden" id="_status" name="_status" value="<%=approvalSetting.status%>">
                                <input type="hidden" id="_schedulerName" name="_schedulerName" value="<%=approvalSetting.name%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.SYSTEM_EDIT_SCHEDULER) {%>
                            <li><a href="#" onclick="ApproveRequest('editSchedularForm', 'editscheduler', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="editSchedularForm">
                                <input type="hidden" id="_gatewayType" name="_gatewayType" value="<%=approvalSetting.type%>">
                                <input type="hidden" id="_schedulerStatus" name="_schedulerStatus" value="<%=approvalSetting.status%>">
                                <input type="hidden" id="_gatewayPreference" name="_gatewayPreference" value="<%=approvalSetting.prefernce%>">
                                <input type="hidden" id="_gatewayStatus" name="_gatewayStatus" value="<%=approvalSetting.status1%>">
                                <input type="hidden" id="_operatorRoles" name="_operatorRoles" value="<%=approvalSetting.operator_role%>">
                                <input type="hidden" id="_schedulerDuration" name="_schedulerDuration" value="<%=approvalSetting.duration%>">
                                <input type="hidden" id="_schedulername" name="_schedulername" value="<%=approvalSetting.name%>">
                                <input type="hidden" id="_EditSchedulerContact0" name="_EditSchedulerContact0" value="<%=approvalSetting.email%>">
                                <input type="hidden" id="_reportType" name="_reportType" value="<%=approvalSetting.reportType%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.SYSTEM_DELETE_SCHEDULER) {%>
                            <li><a href="#" onclick="ApproveRequest('removeSchedularForm', 'removescheduler', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="removeSchedularForm">
                                <input type="hidden" id="_schedulerName" name="_schedulerName" value="<%=approvalSetting.name%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.SYSTEM_AUDIT_INTEGRITY) {%>
                            <li><a href="#" onclick="ApproveRequest('auditIntegrityForm', 'auditIntigrityTest', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="auditIntegrityForm">
                                <input type="hidden" id="_IntegrityID" name="_IntegrityID" value="<%=approvalSetting.template_id%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.TRUSTED_DEVICE_STATUS) {%>
                            <li><a href="#" onclick="ApproveRequest('trusedDeviceStatusForm', 'changedevicestatus', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="trusedDeviceStatusForm">
                                <input type="hidden" id="_userid" name="_userid" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_status" name="_status" value="<%=approvalSetting.status1%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.TRUSTED_DEVICE_DELETE) {%>
                            <li><a href="#" onclick="ApproveRequest('trusedDeviceRemoveForm', 'removeTrustDevice', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="trusedDeviceRemoveForm">
                                <input type="hidden" id="_userid" name="_userid" value="<%=approvalSetting.userId%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.GEOTRACKING_EDIT) {%>
                            <li><a href="#" onclick="ApproveRequest('geotrackingEditForm', 'editgeotracking', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="geotrackingEditForm">
                                <input type="hidden" id="_userID" name="_userID" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_homeCountry" name="_homeCountry" value="<%=approvalSetting.geolocation_homecountry%>">
                                <input type="hidden" id="_groupid" name="_groupid" value="<%=approvalSetting.geolocation_groupId%>">
                                <input type="hidden" id="_startdate" name="_startdate" value="<%=approvalSetting.geolocation_startdate%>">
                                <input type="hidden" id="_enddate" name="_enddate" value="<%=approvalSetting.geolocation_enddate%>">
                                <input type="hidden" id="_eroaming" name="_eroaming" value="<%=approvalSetting.geolocation_roaming%>">

                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.SYSTEMESSAGE_ADD) {%>
                            <li><a href="#" onclick="ApproveRequest('systemmessageAddForm', 'AddSystemMessage', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="systemmessageAddForm">
                                <input type="hidden" id="_messagebody" name="_messagebody" value="<%=approvalSetting.systemmessage_messagebody%>">
                                <input type="hidden" id="_alertto" name="_alertto" value="<%=approvalSetting.systemmessage_alertto%>">
                                <input type="hidden" id="_status" name="_status" value="<%=approvalSetting.systemmessage_status%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.SYSTEMESSAGE_DELETE) {%>
                            <li><a href="#" onclick="ApproveRequest('systemmessageDeleteForm', 'Deletemessagesettingentry', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="systemmessageDeleteForm">
                                <input type="hidden" id="_sid" name="_sid" value="<%=approvalSetting.systemmessage_id%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.SYSTEMESSAGE_STATUS) {%>
                            <li><a href="#" onclick="ApproveRequest('systemmessageStatusForm', 'ChangesystemmessageStatus', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="systemmessageStatusForm">
                                <input type="hidden" id="_messageno" name="_messageno" value="<%=approvalSetting.systemmessage_id%>">
                                <input type="hidden" id="_status" name="_status" value="<%=approvalSetting.systemmessage_status%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.IMAGE_DELETE) {%>
                            <li><a href="#" onclick="ApproveRequest('imageDeleteForm', 'removeSecurePhrase', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="imageDeleteForm">
                                <input type="hidden" id="_userid" name="_userid" value="<%=approvalSetting.userId%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.IMAGE_UNLOCK) {%>
                            <li><a href="#" onclick="ApproveRequest('imageunlockForm', 'unlockImage', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="imageunlockForm">
                                <input type="hidden" id="_userid" name="_userid" value="<%=approvalSetting.userId%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.SNMP_ADD) {%>
                            <li><a href="#" onclick="ApproveRequest('snmpAddForm', 'AddsnmpSettings', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="snmpAddForm">
                                <input type="hidden" id="_snmpip" name="_snmpip" value="<%=approvalSetting.ip%>">
                                <input type="hidden" id="_snmpport" name="_snmpport" value="<%=approvalSetting.port%>">
                                <input type="hidden" id="_level" name="_level" value="<%=approvalSetting.snmp_level%>">
                                <input type="hidden" id="_alive" name="_alive" value="<%=approvalSetting.snmp_alive%>">
                                <input type="hidden" id="_error" name="_error" value="<%=approvalSetting.snmp_error%>">
                                <input type="hidden" id="_warning" name="_warning" value="<%=approvalSetting.snmp_warning%>">
                                <input type="hidden" id="_status" name="_status" value="<%=approvalSetting.status1%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.SNMP_DELETE) {%>
                            <li><a href="#" onclick="ApproveRequest('snmpDeleteForm', 'deletesnmpsettingentry', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="snmpAddForm">
                                <input type="hidden" id="_sid" name="_sid" value="<%=approvalSetting.snmp_id%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.SNMP_EDIT) {%>
                            <li><a href="#" onclick="ApproveRequest('snmpEditForm', 'EditsnmpSettings', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="snmpEditForm">
                                <input type="hidden" id="_snmpipedit" name="_snmpipedit" value="<%=approvalSetting.ip%>">
                                <input type="hidden" id="_snmpportedit" name="_snmpportedit" value="<%=approvalSetting.port%>">
                                <input type="hidden" id="_leveledit" name="_leveledit" value="<%=approvalSetting.snmp_level%>">
                                <input type="hidden" id="_aliveedit" name="_aliveedit" value="<%=approvalSetting.snmp_alive%>">
                                <input type="hidden" id="_erroredit" name="_erroredit" value="<%=approvalSetting.snmp_error%>">
                                <input type="hidden" id="_warningedit" name="_warningedit" value="<%=approvalSetting.snmp_warning%>">
                                <input type="hidden" id="_statusedit" name="_statusedit" value="<%=approvalSetting.status1%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.SNMP_STATUS) {%>
                            <li><a href="#" onclick="ApproveRequest('snmpStatusForm', 'ChangesnmpStatus', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="snmpStatusForm">
                                <input type="hidden" id="_name" name="_name" value="<%=approvalSetting.snmp_name%>">
                                <input type="hidden" id="_status" name="_status" value="<%=approvalSetting.status1%>">
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.SMS_ADD) {%>
                            <li><a href="#" onclick="ApproveRequest('smsAddSettingForm', 'editsmssettings', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="smsAddSettingForm">
                                <% if (approvalSetting.prefernce == 1) {%>
                                <input type="hidden" id="_perference" name="_perference" value="<%=approvalSetting.prefernce%>">
                                <input type="hidden" id="_type" name="_type" value="<%=approvalSetting.type%>">
                                <input type="hidden" id="_className" name="_className" value="<%=approvalSetting.className%>">
                                <input type="hidden" id="_ip" name="_ip" value="<%=approvalSetting.ip%>">
                                <input type="hidden" id="_logConfirmation" name="_logConfirmation" value="<%=approvalSetting.logConfirmation1%>">
                                <input type="hidden" id="_password" name="_password" value="<%=approvalSetting.password%>">
                                <input type="hidden" id="_phoneNumber" name="_phoneNumber" value="<%=approvalSetting.phoneNo%>">
                                <input type="hidden" id="_port" name="_port" value="<%=approvalSetting.port%>">
                                <input type="hidden" id="_reserve1" name="_reserve1" value="<%=approvalSetting.reserve1%>">
                                <input type="hidden" id="_reserve2" name="_reserve2" value="<%=approvalSetting.reserve2%>">
                                <input type="hidden" id="_reserve3" name="_reserve3" value="<%=approvalSetting.reserve3%>">
                                <input type="hidden" id="_status" name="_status" value="<%=approvalSetting.status1%>">
                                <input type="hidden" id="_userId" name="_userId" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_autofailover" name="_autofailover" value="<%=approvalSetting.autofailover%>">
                                <input type="hidden" id="_retries" name="_retries" value="<%=approvalSetting.retrycount%>">
                                <input type="hidden" id="_retryduration" name="_retryduration" value="<%=approvalSetting.retryduration%>">
                                <input type="hidden" id="_messgeLength" name="_messgeLength" value="<%=approvalSetting.messgeLength%>">
                                <%}
                                    if (approvalSetting.prefernce == 2) {%>
                                <input type="hidden" id="_perference" name="_perference" value="<%=approvalSetting.prefernce%>">
                                <input type="hidden" id="_type" name="_type" value="<%=approvalSetting.type%>">
                                <input type="hidden" id="_classNameS" name="_classNameS" value="<%=approvalSetting.className%>">
                                <input type="hidden" id="_ipS" name="_ipS" value="<%=approvalSetting.ip%>">
                                <input type="hidden" id="_logConfirmationS" name="_logConfirmationS" value="<%=approvalSetting.logConfirmation1%>">
                                <input type="hidden" id="_passwordS" name="_passwordS" value="<%=approvalSetting.password%>">
                                <input type="hidden" id="_phoneNumberS" name="_phoneNumberS" value="<%=approvalSetting.phoneNo%>">
                                <input type="hidden" id="_portS" name="_portS" value="<%=approvalSetting.port%>">
                                <input type="hidden" id="_reserve1S" name="_reserve1S" value="<%=approvalSetting.reserve1%>">
                                <input type="hidden" id="_reserve2S" name="_reserve2S" value="<%=approvalSetting.reserve2%>">
                                <input type="hidden" id="_reserve3S" name="_reserve3S" value="<%=approvalSetting.reserve3%>">
                                <input type="hidden" id="_statusS" name="_statusS" value="<%=approvalSetting.status1%>">
                                <input type="hidden" id="_userIdS" name="_userIdS" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_autofailoverS" name="_autofailoverS" value="<%=approvalSetting.autofailover%>">
                                <input type="hidden" id="_retriesS" name="_retriesS" value="<%=approvalSetting.retrycount%>">
                                <input type="hidden" id="_retrydurationS" name="_retrydurationS" value="<%=approvalSetting.retryduration%>">
                                <input type="hidden" id="_messgeLengthS" name="_messgeLengthS" value="<%=approvalSetting.messgeLength%>">
                                <%}%>
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.USSD_ADD) {%>
                            <li><a href="#" onclick="ApproveRequest('ussdAddSettingForm', 'editussdsettings', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="ussdAddSettingForm">
                                <% if (approvalSetting.prefernce == 1) {%>
                                <input type="hidden" id="_perferenceUSSD" name="_perferenceUSSD" value="<%=approvalSetting.prefernce%>">
                                <input type="hidden" id="_typeUSSD" name="_typeUSSD" value="<%=approvalSetting.type%>">
                                <input type="hidden" id="_classNameUSSD" name="_classNameUSSD" value="<%=approvalSetting.className%>">
                                <input type="hidden" id="_ipUSSD" name="_ipUSSD" value="<%=approvalSetting.ip%>">
                                <input type="hidden" id="_logConfirmationUSSD" name="_logConfirmationUSSD" value="<%=approvalSetting.logConfirmation1%>">
                                <input type="hidden" id="_passwordUSSD" name="_passwordUSSD" value="<%=approvalSetting.password%>">
                                <input type="hidden" id="_phoneNumberUSSD" name="_phoneNumberUSSD" value="<%=approvalSetting.phoneNo%>">
                                <input type="hidden" id="_portUSSD" name="_portUSSD" value="<%=approvalSetting.port%>">
                                <input type="hidden" id="_reserve1USSD" name="_reserve1USSD" value="<%=approvalSetting.reserve1%>">
                                <input type="hidden" id="_reserve2USSD" name="_reserve2USSD" value="<%=approvalSetting.reserve2%>">
                                <input type="hidden" id="_reserve3USSD" name="_reserve3USSD" value="<%=approvalSetting.reserve3%>">
                                <input type="hidden" id="_statusUSSD" name="_statusUSSD" value="<%=approvalSetting.status1%>">
                                <input type="hidden" id="_userIdUSSD" name="_userIdUSSD" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_autofailoverUSSD" name="_autofailoverUSSD" value="<%=approvalSetting.autofailover%>">
                                <input type="hidden" id="_retriesUSSD" name="_retriesUSSD" value="<%=approvalSetting.retrycount%>">
                                <input type="hidden" id="_retrydurationUSSD" name="_retrydurationUSSD" value="<%=approvalSetting.retryduration%>">
                                <input type="hidden" id="_messgeLengthUSSD" name="_messgeLengthUSSD" value="<%=approvalSetting.messgeLength%>">
                                <%}
                                    if (approvalSetting.prefernce == 2) {%>
                                <input type="hidden" id="_perferenceUSSD" name="_perferenceUSSD" value="<%=approvalSetting.prefernce%>">
                                <input type="hidden" id="_typeUSSD" name="_typeUSSD" value="<%=approvalSetting.type%>">
                                <input type="hidden" id="_classNameUSSDS" name="_classNameUSSDS" value="<%=approvalSetting.className%>">
                                <input type="hidden" id="_ipUSSDS" name="_ipUSSDS" value="<%=approvalSetting.ip%>">
                                <input type="hidden" id="_logConfirmationUSSDS" name="_logConfirmationUSSDS" value="<%=approvalSetting.logConfirmation1%>">
                                <input type="hidden" id="_passwordUSSDS" name="_passwordUSSDS" value="<%=approvalSetting.password%>">
                                <input type="hidden" id="_phoneNumberUSSDS" name="_phoneNumberUSSDS" value="<%=approvalSetting.phoneNo%>">
                                <input type="hidden" id="_portUSSDS" name="_portUSSDS" value="<%=approvalSetting.port%>">
                                <input type="hidden" id="_reserve1USSDS" name="_reserve1USSDS" value="<%=approvalSetting.reserve1%>">
                                <input type="hidden" id="_reserve2USSDS" name="_reserve2USSDS" value="<%=approvalSetting.reserve2%>">
                                <input type="hidden" id="_reserve3USSDS" name="_reserve3USSDS" value="<%=approvalSetting.reserve3%>">
                                <input type="hidden" id="_statusUSSDS" name="_statusUSSDS" value="<%=approvalSetting.status1%>">
                                <input type="hidden" id="_userIdUSSDS" name="_userIdUSSDS" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_autofailoverUSSDS" name="_autofailoverUSSDS" value="<%=approvalSetting.autofailover%>">
                                <input type="hidden" id="_retriesUSSDS" name="_retriesUSSDS" value="<%=approvalSetting.retrycount%>">
                                <input type="hidden" id="_retrydurationUSSDS" name="_retrydurationUSSDS" value="<%=approvalSetting.retryduration%>">
                                <input type="hidden" id="_messgeLengthUSSDS" name="_messgeLengthUSSDS" value="<%=approvalSetting.messgeLength%>">
                                <%}%>
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.VOICE_ADD) {%>
                            <li><a href="#" onclick="ApproveRequest('ivrAddSettingForm', 'editivrsettings', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="ussdAddSettingForm">
                                <% if (approvalSetting.prefernce == 1) {%>
                                <input type="hidden" id="_perferenceIVR" name="_perferenceIVR" value="<%=approvalSetting.prefernce%>">
                                <input type="hidden" id="_typeIVR" name="_typeIVR" value="<%=approvalSetting.type%>">
                                <input type="hidden" id="_classNameIVR" name="_classNameIVR" value="<%=approvalSetting.className%>">
                                <input type="hidden" id="_ipIVR" name="_ipIVR" value="<%=approvalSetting.ip%>">
                                <input type="hidden" id="_logConfirmationIVR" name="_logConfirmationIVR" value="<%=approvalSetting.logConfirmation1%>">
                                <input type="hidden" id="_passwordIVR" name="_passwordIVR" value="<%=approvalSetting.password%>">
                                <input type="hidden" id="_phoneNumberIVR" name="_phoneNumberIVR" value="<%=approvalSetting.phoneNo%>">
                                <input type="hidden" id="_portIVR" name="_portIVR" value="<%=approvalSetting.port%>">
                                <input type="hidden" id="_reserve1IVR" name="_reserve1IVR" value="<%=approvalSetting.reserve1%>">
                                <input type="hidden" id="_reserve2IVR" name="_reserve2IVR" value="<%=approvalSetting.reserve2%>">
                                <input type="hidden" id="_reserve3IVR" name="_reserve3IVR" value="<%=approvalSetting.reserve3%>">
                                <input type="hidden" id="_statusIVR" name="_statusIVR" value="<%=approvalSetting.status1%>">
                                <input type="hidden" id="_userIdIVR" name="_userIdIVR" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_autofailoverIVR" name="_autofailoverIVR" value="<%=approvalSetting.autofailover%>">
                                <input type="hidden" id="_retriesIVR" name="_retriesIVR" value="<%=approvalSetting.retrycount%>">
                                <input type="hidden" id="_retrydurationIVR" name="_retrydurationIVR" value="<%=approvalSetting.retryduration%>">
                                <input type="hidden" id="_messgeLengthIVR" name="_messgeLengthIVR" value="<%=approvalSetting.messgeLength%>">
                                <%}
                                    if (approvalSetting.prefernce == 2) {%>
                                <input type="hidden" id="_perferenceIVR" name="_perferenceIVR" value="<%=approvalSetting.prefernce%>">
                                <input type="hidden" id="_typeIVR" name="_typeIVR" value="<%=approvalSetting.type%>">
                                <input type="hidden" id="_classNameIVRS" name="_classNameIVRS" value="<%=approvalSetting.className%>">
                                <input type="hidden" id="_ipIVRS" name="_ipIVRS" value="<%=approvalSetting.ip%>">
                                <input type="hidden" id="_logConfirmationIVRS" name="_logConfirmationIVRS" value="<%=approvalSetting.logConfirmation1%>">
                                <input type="hidden" id="_passwordIVRS" name="_passwordIVRS" value="<%=approvalSetting.password%>">
                                <input type="hidden" id="_phoneNumberIVRS" name="_phoneNumberIVRS" value="<%=approvalSetting.phoneNo%>">
                                <input type="hidden" id="_portIVRS" name="_portIVRS" value="<%=approvalSetting.port%>">
                                <input type="hidden" id="_reserve1IVRS" name="_reserve1IVRS" value="<%=approvalSetting.reserve1%>">
                                <input type="hidden" id="_reserve2IVRS" name="_reserve2IVRS" value="<%=approvalSetting.reserve2%>">
                                <input type="hidden" id="_reserve3IVRS" name="_reserve3IVRS" value="<%=approvalSetting.reserve3%>">
                                <input type="hidden" id="_statusIVRS" name="_statusIVRS" value="<%=approvalSetting.status1%>">
                                <input type="hidden" id="_userIdIVRS" name="_userIdIVRS" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_autofailoverIVRS" name="_autofailoverIVRS" value="<%=approvalSetting.autofailover%>">
                                <input type="hidden" id="_retriesIVRS" name="_retriesIVRS" value="<%=approvalSetting.retrycount%>">
                                <input type="hidden" id="_retrydurationIVRS" name="_retrydurationIVRS" value="<%=approvalSetting.retryduration%>">
                                <input type="hidden" id="_messgeLengthIVRS" name="_messgeLengthIVRS" value="<%=approvalSetting.messgeLength%>">
                                <%}%>
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.EMAIL_ADD) {%>
                            <li><a href="#" onclick="ApproveRequest('ivrAddSettingForm', 'editivrsettings', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="ussdAddSettingForm">
                                <% if (approvalSetting.prefernce == 1) {%>
                                <input type="hidden" id="_perferenceEMAIL" name="_perferenceEMAIL" value="<%=approvalSetting.prefernce%>">
                                <input type="hidden" id="_typeEMAIL" name="_typeEMAIL" value="<%=approvalSetting.type%>">
                                <input type="hidden" id="_classNameEMAIL" name="_classNameEMAIL" value="<%=approvalSetting.className%>">
                                <input type="hidden" id="_ipEMAIL" name="_ipEMAIL" value="<%=approvalSetting.ip%>">
                                <input type="hidden" id="_logConfirmationEMAIL" name="_logConfirmationEMAIL" value="<%=approvalSetting.logConfirmation1%>">
                                <input type="hidden" id="_passwordEMAIL" name="_passwordEMAIL" value="<%=approvalSetting.password%>">
                                <input type="hidden" id="_phoneNumberEMAIL" name="_phoneNumberEMAIL" value="<%=approvalSetting.phoneNo%>">
                                <input type="hidden" id="_portEMAIL" name="_portEMAIL" value="<%=approvalSetting.port%>">
                                <input type="hidden" id="_reserve1EMAIL" name="_reserve1EMAIL" value="<%=approvalSetting.reserve1%>">
                                <input type="hidden" id="_reserve2EMAIL" name="_reserve2EMAIL" value="<%=approvalSetting.reserve2%>">
                                <input type="hidden" id="_reserve3EMAIL" name="_reserve3EMAIL" value="<%=approvalSetting.reserve3%>">
                                <input type="hidden" id="_statusEMAIL" name="_statusEMAIL" value="<%=approvalSetting.status1%>">
                                <input type="hidden" id="_userIdEMAIL" name="_userIdEMAIL" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_autofailoverEMAIL" name="_autofailoverEMAIL" value="<%=approvalSetting.autofailover%>">
                                <input type="hidden" id="_retriesEMAIL" name="_retriesEMAIL" value="<%=approvalSetting.retrycount%>">
                                <input type="hidden" id="_retrydurationEMAIL" name="_retrydurationEMAIL" value="<%=approvalSetting.retryduration%>">
                                <input type="hidden" id="_messgeLengthEMAIL" name="_messgeLengthEMAIL" value="<%=approvalSetting.messgeLength%>">
                                <%}
                                    if (approvalSetting.prefernce == 2) {%>
                                <input type="hidden" id="_perferenceEMAIL" name="_perferenceEMAIL" value="<%=approvalSetting.prefernce%>">
                                <input type="hidden" id="_typeEMAIL" name="_typeEMAIL" value="<%=approvalSetting.type%>">
                                <input type="hidden" id="_classNameEMAILS" name="_classNameEMAILS" value="<%=approvalSetting.className%>">
                                <input type="hidden" id="_ipEMAILS" name="_ipEMAILS" value="<%=approvalSetting.ip%>">
                                <input type="hidden" id="_logConfirmationEMAILS" name="_logConfirmationEMAILS" value="<%=approvalSetting.logConfirmation1%>">
                                <input type="hidden" id="_passwordEMAILS" name="_passwordEMAILS" value="<%=approvalSetting.password%>">
                                <input type="hidden" id="_phoneNumberEMAILS" name="_phoneNumberEMAILS" value="<%=approvalSetting.phoneNo%>">
                                <input type="hidden" id="_portEMAILS" name="_portEMAILS" value="<%=approvalSetting.port%>">
                                <input type="hidden" id="_reserve1EMAILS" name="_reserve1EMAILS" value="<%=approvalSetting.reserve1%>">
                                <input type="hidden" id="_reserve2EMAILS" name="_reserve2EMAILS" value="<%=approvalSetting.reserve2%>">
                                <input type="hidden" id="_reserve3EMAILS" name="_reserve3EMAILS" value="<%=approvalSetting.reserve3%>">
                                <input type="hidden" id="_statusEMAILS" name="_statusEMAILS" value="<%=approvalSetting.status1%>">
                                <input type="hidden" id="_userIdEMAILS" name="_userIdEMAILS" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_autofailoverEMAILS" name="_autofailoverEMAILS" value="<%=approvalSetting.autofailover%>">
                                <input type="hidden" id="_retriesEMAILS" name="_retriesEMAILS" value="<%=approvalSetting.retrycount%>">
                                <input type="hidden" id="_retrydurationEMAILS" name="_retrydurationEMAILS" value="<%=approvalSetting.retryduration%>">
                                <input type="hidden" id="_messgeLengthEMAILS" name="_messgeLengthEMAILS" value="<%=approvalSetting.messgeLength%>">
                                <%}%>
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.BILLING_MANAGER_ADD) {%>
                            <li><a href="#" onclick="ApproveRequest('billingManagerAddSettingForm', 'editbillingmanagersettings', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="billingManagerAddSettingForm">
                                <% if (approvalSetting.prefernce == 1) {%>

                                <input type="hidden" id="_costperTxOOBOTP" name="_costperTxOOBOTP" value="<%=approvalSetting._costperTxOOBOTP%>">
                                <input type="hidden" id="_costOOBOTP" name="_costOOBOTP" value="<%=approvalSetting._costOOBOTP%>">
                                <input type="hidden" id="_txPerDayOOBOTP" name="_txPerDayOOBOTP" value="<%=approvalSetting._txPerDayOOBOTP%>">
                                <input type="hidden" id="_selectAlertBeforeOOBOTP" name="_selectAlertBeforeOOBOTP" value="<%=approvalSetting._selectAlertBeforeOOBOTP%>">
                                <input type="hidden" id="_selectBillingDurationOOBOTP" name="_selectBillingDurationOOBOTP" value="<%=approvalSetting._selectBillingDurationOOBOTP%>">
                                <input type="hidden" id="_selectBillTypeOOBOTP" name="_selectBillTypeOOBOTP" value="<%=approvalSetting._selectBillTypeOOBOTP%>">
                                <input type="hidden" id="_allowBillingManagerOOBOTP" name="_allowBillingManagerOOBOTP" value="<%=approvalSetting._allowBillingManagerOOBOTP%>">
                                <input type="hidden" id="_costCertificate" name="_costCertificate" value="<%=approvalSetting._costCertificate%>">
                                <input type="hidden" id="_selectAlertBeforeCertificate" name="_selectAlertBeforeCertificate" value="<%=approvalSetting._selectAlertBeforeCertificate%>">
                                <input type="hidden" id="_selectBillingDurationCertificate" name="_selectBillingDurationCertificate" value="<%=approvalSetting._selectBillingDurationCertificate%>">
                                <input type="hidden" id="_selectBillTypeCertificate" name="_selectBillTypeCertificate" value="<%=approvalSetting._selectBillTypeCertificate%>">
                                <input type="hidden" id="_allowBillingManagerCertificate" name="_allowBillingManagerCertificate" value="<%=approvalSetting._allowBillingManagerCertificate%>">
                                <input type="hidden" id="_costHWPKIOTP" name="_costHWPKIOTP" value="<%=approvalSetting._costHWPKIOTP%>">
                                <input type="hidden" id="_selectAlertBeforeHWPKIOTP" name="_selectAlertBeforeHWPKIOTP" value="<%=approvalSetting._selectAlertBeforeHWPKIOTP%>">
                                <input type="hidden" id="_selectBillingDurationHWPKIOTP" name="_selectBillingDurationHWPKIOTP" value="<%=approvalSetting._selectBillingDurationHWPKIOTP%>">
                                <input type="hidden" id="_selectBillTypeHWPKIOTP" name="_selectBillTypeHWPKIOTP" value="<%=approvalSetting._selectBillTypeHWPKIOTP%>">
                                <input type="hidden" id="_allowBillingManagerHWPKIOTP" name="_allowBillingManagerHWPKIOTP" value="<%=approvalSetting._allowBillingManagerHWPKIOTP%>">
                                <input type="hidden" id="_costSWPKIOTP" name="_costSWPKIOTP" value="<%=approvalSetting._costSWPKIOTP%>">
                                <input type="hidden" id="_selectAlertBeforeSWPKIOTP" name="_selectAlertBeforeSWPKIOTP" value="<%=approvalSetting._selectAlertBeforeSWPKIOTP%>">
                                <input type="hidden" id="_selectBillingDurationSWPKIOTP" name="_selectBillingDurationSWPKIOTP" value="<%=approvalSetting._selectBillingDurationSWPKIOTP%>">
                                <input type="hidden" id="_allowBillingManager" name="_allowBillingManager" value="<%=approvalSetting._allowBillingManager%>">
                                <input type="hidden" id="_allowRepetationCharacterB" name="_allowRepetationCharacterB" value="<%=approvalSetting._allowRepetationCharacterB%>">
                                <input type="hidden" id="_gatewayTypeB" name="_gatewayTypeB" value="<%=approvalSetting._gatewayTypeB%>">
                                <input type="hidden" id="_alertAttemptB" name="_alertAttemptB" value="<%=approvalSetting._alertAttemptB%>">
                                <input type="hidden" id="_allowBillingManagerSWOTP" name="_allowBillingManagerSWOTP" value="<%=approvalSetting._allowBillingManagerSWOTP%>">
                                <input type="hidden" id="_selectBillTypeSWOTP" name="_selectBillTypeSWOTP" value="<%=approvalSetting._selectBillTypeSWOTP%>">
                                <input type="hidden" id="_selectBillingDurationSWOTP" name="_selectBillingDurationSWOTP" value="<%=approvalSetting._selectBillingDurationSWOTP%>">
                                <input type="hidden" id="_selectAlertBeforeSWOTP" name="_selectAlertBeforeSWOTP" value="<%=approvalSetting._selectAlertBeforeSWOTP%>">
                                <input type="hidden" id="_costSWOTP" name="_costSWOTP" value="<%=approvalSetting._costSWOTP%>">
                                <input type="hidden" id="_allowBillingManagerHWOTP" name="_allowBillingManagerHWOTP" value="<%=approvalSetting._allowBillingManagerHWOTP%>">
                                <input type="hidden" id="_selectBillTypeHWOTP" name="_selectBillTypeHWOTP" value="<%=approvalSetting._selectBillTypeHWOTP%>">
                                <input type="hidden" id="_selectBillingDurationHWOTP" name="_selectBillingDurationHWOTP" value="<%=approvalSetting._selectBillingDurationHWOTP%>">
                                <input type="hidden" id="_selectAlertBeforeHWOTP" name="_selectAlertBeforeHWOTP" value="<%=approvalSetting._selectAlertBeforeHWOTP%>">
                                <input type="hidden" id="_costHWOTP" name="_costHWOTP" value="<%=approvalSetting._costHWOTP%>">
                                <input type="hidden" id="_allowBillingManagerSWPKIOTP" name="_allowBillingManagerSWPKIOTP" value="<%=approvalSetting._allowBillingManagerSWPKIOTP%>">
                                <input type="hidden" id="_selectBillTypeSWPKIOTP" name="_selectBillTypeSWPKIOTP" value="<%=approvalSetting._selectBillTypeSWPKIOTP%>">
                                <%}%>

                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.ADD_ANDROID_PUSH_SETTING) {%>
                            <li><a href="#" onclick="ApproveRequest('androidPushForm', 'editpushnotificationsetting', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="androidPushForm">
                                <% if (approvalSetting.prefernce == 1) {%>
                                <input type="hidden" id="_className" name="_className" value="<%=approvalSetting.className%>">
                                <input type="hidden" id="_ip" name="_ip" value="<%=approvalSetting.ip%>">
                                <input type="hidden" id="_logConfirmation" name="_logConfirmation" value="<%=approvalSetting.logConfirmation1%>">
                                <input type="hidden" id="_port" name="_port" value="<%=approvalSetting.port%>">
                                <input type="hidden" id="_reserve1" name="_reserve1" value="<%=approvalSetting.reserve1%>">
                                <input type="hidden" id="_reserve2" name="_reserve2" value="<%=approvalSetting.reserve2%>">
                                <input type="hidden" id="_reserve3" name="_reserve3" value="<%=approvalSetting.reserve3%>">
                                <input type="hidden" id="_status" name="_status" value="<%=approvalSetting.status1%>">
                                <input type="hidden" id="_autofailover" name="_autofailover" value="<%=approvalSetting.autofailover%>">
                                <input type="hidden" id="_retries" name="_retries" value="<%=approvalSetting.retrycount%>">
                                <input type="hidden" id="_retryduration" name="_retryduration" value="<%=approvalSetting.retryduration%>">
                                <input type="hidden" id="_retriesFromGoogle" name="_retriesFromGoogle" value="<%=approvalSetting.retriesFromGoogle%>">
                                <input type="hidden" id="_timetolive" name="_timetolive" value="<%=approvalSetting.timetolive%>">
                                <input type="hidden" id="_delayWhileIdle" name="_delayWhileIdle" value="<%=approvalSetting.delayWhileIdle%>">
                                <input type="hidden" id="_gcmurl" name="_gcmurl" value="<%=approvalSetting.gcmurl%>">
                                <input type="hidden" id="_apikey" name="_apikey" value="<%=approvalSetting.apikey%>">
                                <input type="hidden" id="_googleSenderKey" name="_googleSenderKey" value="<%=approvalSetting.googleSenderKey%>">
                                <%}%>
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.ADD_IPHONE_PUSH_SETTING) {%>
                            <li><a href="#" onclick="ApproveRequest('iphonePushForm', 'editiphonepushsettings', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="iphonePushForm">
                                <% if (approvalSetting.prefernce == 1) {%>
                                <input type="hidden" id="_classNameS" name="_classNameS" value="<%=approvalSetting.className%>">
                                <input type="hidden" id="_ipS" name="_ipS" value="<%=approvalSetting.ip%>">
                                <input type="hidden" id="_logConfirmationS" name="_logConfirmationS" value="<%=approvalSetting.logConfirmation1%>">
                                <input type="hidden" id="_portS" name="_portS" value="<%=approvalSetting.port%>">
                                <input type="hidden" id="_reserve1S" name="_reserve1S" value="<%=approvalSetting.reserve1%>">
                                <input type="hidden" id="_reserve2S" name="_reserve2S" value="<%=approvalSetting.reserve2%>">
                                <input type="hidden" id="_reserve3S" name="_reserve3S" value="<%=approvalSetting.reserve3%>">
                                <input type="hidden" id="_statusS" name="_statusS" value="<%=approvalSetting.status1%>">
                                <input type="hidden" id="_autofailover" name="_autofailover" value="<%=approvalSetting.autofailover%>">
                                <input type="hidden" id="_retriesS" name="_retriesS" value="<%=approvalSetting.retrycount%>">
                                <input type="hidden" id="_retrydurationS" name="_retrydurationS" value="<%=approvalSetting.retryduration%>">
                                <input type="hidden" id="_retriesFromGoogle" name="_retriesFromGoogle" value="<%=approvalSetting.retriesFromGoogle%>">
                                <input type="hidden" id="_bundleID" name="_bundleID" value="<%=approvalSetting.bundleID%>">
                                <input type="hidden" id="_delayWhileIdleS" name="_delayWhileIdleS" value="<%=approvalSetting.delayWhileIdle%>">
                                <input type="hidden" id="_password" name="_password" value="<%=approvalSetting.password%>">
                                <%}%>
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.ADD_USER_SOURCE_SETTING) {%>
                            <li><a href="#" onclick="ApproveRequest('addUserSourceSettingForm', 'editusersrcsettings', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="addUserSourceSettingForm">
                                <% if (approvalSetting.prefernce == 1) {%>
                                <input type="hidden" id="_userValidityDays" name="_userValidityDays" value="<%=approvalSetting.userValidityDays%>">
                                <input type="hidden" id="_algoAttrReserve4US" name="_algoAttrReserve4US" value="<%=approvalSetting.algoAttrReserve4US%>">
                                <input type="hidden" id="_algoAttrReserve3US" name="_algoAttrReserve3US" value="<%=approvalSetting.algoAttrReserve3US%>">
                                <input type="hidden" id="_algoAttrReserve2US" name="_algoAttrReserve2US" value="<%=approvalSetting.algoAttrReserve2US%>">
                                <input type="hidden" id="_algoAttrReserve1US" name="_algoAttrReserve1US" value="<%=approvalSetting.algoAttrReserve1US%>">
                                <input type="hidden" id="_passwordTypeUS" name="_passwordTypeUS" value="<%=approvalSetting.passwordTypeUS%>">
                                <input type="hidden" id="_algoTypeUS" name="_algoTypeUS" value="<%=approvalSetting.algoTypeUS%>">
                                <input type="hidden" id="_tableNameUS" name="_tableNameUS" value="<%=approvalSetting.table_name%>">
                                <input type="hidden" id="_databaseNameUS" name="_databaseNameUS" value="<%=approvalSetting.database_name%>">
                                <input type="hidden" id="_reserve3US" name="_reserve3US" value="<%=approvalSetting.reserve3%>">
                                <input type="hidden" id="_reserve2US" name="_reserve2US" value="<%=approvalSetting.reserve2%>">
                                <input type="hidden" id="_reserve1US" name="_reserve1US" value="<%=approvalSetting.reserve1%>">
                                <input type="hidden" id="_sslUS" name="_sslUS" value="<%=approvalSetting.ssl%>">
                                <input type="hidden" id="_classNameUS" name="_classNameUS" value="<%=approvalSetting.className%>">
                                <input type="hidden" id="_passwordUS" name="_passwordUS" value="<%=approvalSetting.password%>">
                                <input type="hidden" id="_userIdUS" name="_userIdUS" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_portUS" name="_portUS" value="<%=approvalSetting.port%>">
                                <input type="hidden" id="_ipUS" name="_ipUS" value="<%=approvalSetting.ip%>">
                                <input type="hidden" id="_statusUS" name="_statusUS" value="<%=approvalSetting.status1%>">
                                <%}%>
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.ADD_PASSWORD_POLICY_SETTING) {%>
                            <li><a href="#" onclick="ApproveRequest('addPasswordPolicySettingForm', 'editPassordPolicySettings', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="addUserSourceSettingForm">
                                <% if (approvalSetting.prefernce == 1) {%>
                                <input type="hidden" id="_userValidityDays" name="_userValidityDays" value="<%=approvalSetting.userValidityDays%>">
                                <input type="hidden" id="_allowAlertFor" name="_allowAlertFor" value="<%=approvalSetting.allowAlertFor%>">
                                <input type="hidden" id="_arrCommonPassword" name="_arrCommonPassword" value="<%=approvalSetting.arrCommonPassword%>">
                                <input type="hidden" id="_templateName" name="_templateName" value="<%=approvalSetting.templateName%>">
                                <input type="hidden" id="_alertAttempt" name="_alertAttempt" value="<%=approvalSetting.alertAttempt%>">
                                <input type="hidden" id="_gatewayType" name="_gatewayType" value="<%=approvalSetting.gatewayType%>">
                                <input type="hidden" id="_allowAlert" name="_allowAlert" value="<%=approvalSetting.allowAlert%>">
                                <input type="hidden" id="_blockCommonWords" name="_blockCommonWords" value="<%=approvalSetting.blockCommonWords%>">
                                <input type="hidden" id="_alertUserPassword" name="_alertUserPassword" value="<%=approvalSetting.alertUserPassword%>">
                                <input type="hidden" id="_genearationPassword" name="_genearationPassword" value="<%=approvalSetting.genearationPassword%>">
                                <input type="hidden" id="_allowRepetationCharacter" name="_allowRepetationCharacter" value="<%=approvalSetting.allowRepetationCharacter%>">
                                <input type="hidden" id="_passwordType" name="_passwordType" value="<%=approvalSetting.passwordType%>">
                                <input type="hidden" id="_crResetReissue" name="_crResetReissue" value="<%=approvalSetting.crResetReissue%>">
                                <input type="hidden" id="_epinResetReissue" name="_epinResetReissue" value="<%=approvalSetting.epinResetReissue%>">
                                <input type="hidden" id="_changePasswordAfterLogin" name="_changePasswordAfterLogin" value="<%=approvalSetting.changePasswordAfterLogin%>">
                                <input type="hidden" id="_oldPasswordReuseTime" name="_oldPasswordReuseTime" value="<%=approvalSetting.oldPasswordReuseTime%>">
                                <input type="hidden" id="_passwordInvalidAttempts" name="_passwordInvalidAttempts" value="<%=approvalSetting.passwordInvalidAttempts%>">
                                <input type="hidden" id="_enforceDormancyTime" name="_enforceDormancyTime" value="<%=approvalSetting.enforceDormancyTime%>">
                                <input type="hidden" id="_passwordIssueLimit" name="_passwordIssueLimit" value="<%=approvalSetting.passwordIssueLimit%>">
                                <input type="hidden" id="_passwordExpiryTime" name="_passwordExpiryTime" value="<%=approvalSetting.passwordExpiryTime%>">
                                <input type="hidden" id="_passwordLength" name="_passwordLength" value="<%=approvalSetting.passwordLength%>">

                                <%}%>
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.ADD_CHANNLE_PROFILE_SETTING) {%>
                            <li><a href="#" onclick="ApproveRequest('addChannelprofileSettingForm', 'editChannelprofilesettings', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="addChannelprofileSettingForm">
                                <% if (approvalSetting.prefernce == 1) {%>

                                <input type="hidden" id="_yDeviation" name="_yDeviation" value="<%=approvalSetting.yDeviation%>">
                                <input type="hidden" id="_xDeviation" name="_xDeviation" value="<%=approvalSetting.xDeviation%>">
                                <input type="hidden" id="_sweetSpotDeviation" name="_sweetSpotDeviation" value="<%=approvalSetting.sweetSpotDeviation%>">
                                <input type="hidden" id="_timetoampm" name="_timetoampm" value="<%=approvalSetting.timetoampm%>">
                                <input type="hidden" id="_totimerange" name="_totimerange" value="<%=approvalSetting.totimerange%>">
                                <input type="hidden" id="_timefromampm" name="_timefromampm" value="<%=approvalSetting.timefromampm%>">
                                <input type="hidden" id="_timerange" name="_timerange" value="<%=approvalSetting.timerange%>">
                                <input type="hidden" id="_dayrestriction" name="_dayrestriction" value="<%=approvalSetting.dayrestriction%>">
                                <input type="hidden" id="_serverRestriction" name="_serverRestriction" value="<%=approvalSetting.serverRestriction%>">
                                <input type="hidden" id="_multipleSession" name="_multipleSession" value="<%=approvalSetting.multipleSession%>">
                                <input type="hidden" id="_Authorizer_Units" name="_Authorizer_Units" value="<%=approvalSetting.Authorizer_Units%>">
                                <input type="hidden" id="_locationclassName" name="_locationclassName" value="<%=approvalSetting.locationclassName%>">
                                <input type="hidden" id="_ocraspecification" name="_ocraspecification" value="<%=approvalSetting.ocraspecification%>">
                                <input type="hidden" id="_certspecification" name="_certspecification" value="<%=approvalSetting.certspecification%>">
                                <input type="hidden" id="_otpspecification" name="_otpspecification" value="<%=approvalSetting.otpspecification%>">
                                <input type="hidden" id="_cleanupdays" name="_cleanupdays" value="<%=approvalSetting.cleanupdays%>">
                                <input type="hidden" id="_connectorstatus" name="_connectorstatus" value="<%=approvalSetting.connectorstatus%>">
                                <input type="hidden" id="_resetuser" name="_resetuser" value="<%=approvalSetting.resetuser%>">
                                <input type="hidden" id="_Authorizer_PeningDuration" name="_Authorizer_PeningDuration" value="<%=approvalSetting.Authorizer_PeningDuration%>">
                                <input type="hidden" id="_authorizer" name="_authorizer" value="<%=approvalSetting.authorizer%>">
                                <input type="hidden" id="_uploads" name="_uploads" value="<%=approvalSetting.uploads%>">
                                <input type="hidden" id="_rssarchive" name="_rssarchive" value="<%=approvalSetting.rssarchive%>">
                                <input type="hidden" id="_cleanuppath" name="_cleanuppath" value="<%=approvalSetting.cleanuppath%>">
                                <input type="hidden" id="_softwaretype" name="_softwaretype" value="<%=approvalSetting.softwaretype%>">
                                <input type="hidden" id="_alertmedia" name="_alertmedia" value="<%=approvalSetting.alertmedia%>">
                                <input type="hidden" id="_checkuser" name="_checkuser" value="<%=approvalSetting.checkuser%>">
                                <input type="hidden" id="_alertuser" name="_alertuser" value="<%=approvalSetting.alertuser%>">
                                <input type="hidden" id="_edituser" name="_edituser" value="<%=approvalSetting.edituser%>">
                                <input type="hidden" id="_deleteuser" name="_deleteuser" value="<%=approvalSetting.deleteuser%>">
                                <input type="hidden" id="_tokenload" name="_tokenload" value="<%=approvalSetting.tokenload%>">
                                <%}%>
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.ADD_TOKEN_SETTING) {%>
                            <li><a href="#" onclick="ApproveRequest('addOtpSettingForm', 'editotpsettings', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="addOtpSettingForm">
                                <% if (approvalSetting.prefernce == 1) {%>
                                <input type="hidden" id="_OTPLength" name="_OTPLength" value="<%=approvalSetting.OTPLength%>">
                                <input type="hidden" id="_SOTPLength" name="_SOTPLength" value="<%=approvalSetting.SOTPLength%>">
                                <input type="hidden" id="_OATHAlgoType" name="_OATHAlgoType" value="<%=approvalSetting.OATHAlgoType%>">
                                <input type="hidden" id="_SWOTPLength" name="_SWOTPLength" value="<%=approvalSetting.SWOTPLength%>">
                                <input type="hidden" id="_SWOATHAlgoType" name="_SWOATHAlgoType" value="<%=approvalSetting.SWOATHAlgoType%>">
                                <input type="hidden" id="_SWWebTokenExpiryTime" name="_SWWebTokenExpiryTime" value="<%=approvalSetting.SWWebTokenExpiryTime%>">
                                <input type="hidden" id="_OTPAttempts" name="_OTPAttempts" value="<%=approvalSetting.OTPAttempts%>">
                                <input type="hidden" id="_ValidationSteps" name="_ValidationSteps" value="<%=approvalSetting.ValidationSteps%>">
                                <input type="hidden" id="_duration" name="_duration" value="<%=approvalSetting.duration%>">
                                <input type="hidden" id="_MultipleToken" name="_MultipleToken" value="<%=approvalSetting.MultipleToken%>">
                                <input type="hidden" id="_SWWebTokenPinAttempt" name="_SWWebTokenPinAttempt" value="<%=approvalSetting.SWWebTokenPinAttempt%>">
                                <input type="hidden" id="_EnforceMasking" name="_EnforceMasking" value="<%=approvalSetting.EnforceMasking%>">
                                <input type="hidden" id="_autoUnlockAfter" name="_autoUnlockAfter" value="<%=approvalSetting.autoUnlockAfter%>">
                                <input type="hidden" id="_RegistrationExpiryTime" name="_RegistrationExpiryTime" value="<%=approvalSetting.RegistrationExpiryTime%>">
                                <input type="hidden" id="_allowAlertT" name="_allowAlertT" value="<%=approvalSetting.allowAlertT%>">
                                <input type="hidden" id="_gatewayType" name="_gatewayType" value="<%=approvalSetting.gatewayType%>">
                                <input type="hidden" id="_templateNameT" name="_templateNameT" value="<%=approvalSetting.templateNameT%>">
                                <input type="hidden" id="_otpExpiryAfterSession" name="_otpExpiryAfterSession" value="<%=approvalSetting.otpExpiryAfterSession%>">
                                <input type="hidden" id="_otpLockedAfter" name="_otpLockedAfter" value="<%=approvalSetting.otpLockedAfter%>">
                                <%}%>
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.ADD_CERTIFICATE_SETTING) {%>
                            <li><a href="#" onclick="ApproveRequest('addCASettingForm', 'editcasettings', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="addCASettingForm">
                                <% if (approvalSetting.prefernce == 1) {%>
                                <input type="hidden" id="_alertsource" name="_alertsource" value="<%=approvalSetting.alertsource%>">
                                <input type="hidden" id="_statuskyc" name="_statuskyc" value="<%=approvalSetting.status1%>">
                                <input type="hidden" id="_pulledtime" name="_pulledtime" value="<%=approvalSetting.pulledtime%>">
                                <input type="hidden" id="_tagsList" name="_tagsList" value="<%=approvalSetting.tagsList%>">
                                <input type="hidden" id="ocspurl" name="ocspurl" value="<%=approvalSetting.ocspurl%>">
                                <input type="hidden" id="_crlpassword" name="_crlpassword" value="<%=approvalSetting.crlpassword%>">
                                <input type="hidden" id="_crlusername" name="_crlusername" value="<%=approvalSetting.crlusername%>">
                                <input type="hidden" id="_crllink" name="_crllink" value="<%=approvalSetting.crllink%>">
                                <input type="hidden" id="_keyLength" name="_keyLength" value="<%=approvalSetting.keyLength%>">
                                <input type="hidden" id="_validityDays" name="_validityDays" value="<%=approvalSetting.validityDays%>">
                                <input type="hidden" id="_userId" name="_userId" value="<%=approvalSetting.userId%>">
                                <input type="hidden" id="_reserve3" name="_reserve3" value="<%=approvalSetting.reserve3%>">
                                <input type="hidden" id="_reserve2" name="_reserve2" value="<%=approvalSetting.reserve2%>">
                                <input type="hidden" id="_reserve1" name="_reserve1" value="<%=approvalSetting.reserve1%>">
                                <input type="hidden" id="_port" name="_port" value="<%=approvalSetting.port%>">
                                <input type="hidden" id="_password" name="_password" value="<%=approvalSetting.password%>">
                                <input type="hidden" id="_ip" name="_ip" value="<%=approvalSetting.ip%>">
                                <input type="hidden" id="_className" name="_className" value="<%=approvalSetting.className%>">

                                <%}%>
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.ADD_MOBILE_TRUST_SETTING) {%>
                            <li><a href="#" onclick="ApproveRequest('addMobileTrustSettingForm', 'editmobiletrustsetting', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="addMobileTrustSettingForm">
                                <% if (approvalSetting.prefernce == 1) {%>
                                <input type="hidden" id="_blackListedCountries" name="_blackListedCountries" value="<%=approvalSetting.blackListedCountries%>">
                                <input type="hidden" id="_isBlackListed" name="_isBlackListed" value="<%=approvalSetting.isBlackListed%>">
                                <input type="hidden" id="_type" name="_type" value="<%=approvalSetting.type%>">
                                <input type="hidden" id="_selfDestructURlM" name="_selfDestructURlM" value="<%=approvalSetting.selfDestructURlM%>">
                                <input type="hidden" id="_allowAlertForM" name="_allowAlertForM" value="<%=approvalSetting.allowAlertForM%>">
                                <input type="hidden" id="_templateName" name="_templateName" value="<%=approvalSetting.templateName%>">
                                <input type="hidden" id="_alertAttemptM" name="_alertAttemptM" value="<%=approvalSetting.alertAttemptM%>">
                                <input type="hidden" id="_gatewayTypeM" name="_gatewayTypeM" value="<%=approvalSetting.gatewayTypeM%>">
                                <input type="hidden" id="_allowAlertM" name="_allowAlertM" value="<%=approvalSetting.allowAlertM%>">
                                <input type="hidden" id="_geoFencing" name="_geoFencing" value="<%=approvalSetting.geoFencing%>">
                                <input type="hidden" id="_deviceTracking" name="_deviceTracking" value="<%=approvalSetting.deviceTracking%>">
                                <input type="hidden" id="_timeStamp" name="_timeStamp" value="<%=approvalSetting.timeStamp%>">
                                <input type="hidden" id="_selfDestructURl" name="_selfDestructURl" value="<%=approvalSetting.selfDestructURl%>">
                                <input type="hidden" id="_SelfDestructAttempts" name="_SelfDestructAttempts" value="<%=approvalSetting.SelfDestructAttempts%>">
                                <input type="hidden" id="_SelfDestructEnable" name="_SelfDestructEnable" value="<%=approvalSetting.SelfDestructEnable%>">
                                <input type="hidden" id="_SilentCall" name="_SilentCall" value="<%=approvalSetting.SilentCall%>">
                                <input type="hidden" id="_Backup" name="_Backup" value="<%=approvalSetting.Backup%>">
                                <input type="hidden" id="_ExpiryMin" name="_ExpiryMin" value="<%=approvalSetting.ExpiryMin%>">
                                <input type="hidden" id="_CountryName" name="_CountryName" value="<%=approvalSetting.CountryName%>">
                                <%}%>
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.ADD_RADIUS_SETTING) {%>
                            <li><a href="#" onclick="ApproveRequest('addRadiusSettingForm', 'editRadiusClient', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>', '<%= oprbj.getUnits()%>')">Reject</a></li>
                            <form class="form-horizontal" id="addMobileTrustSettingForm">
                                <% if (approvalSetting.prefernce == 1) {%>
                                <input type="hidden" id="_displayname" name="_displayname" value="<%=approvalSetting.displayname%>">
                                <input type="hidden" id="authenticationtype" name="authenticationtype" value="<%=approvalSetting.authenticationtype%>">
                                <input type="hidden" id="_rClientIP" name="_rClientIP" value="<%=approvalSetting.rClientIP%>">
                                <input type="hidden" id="_radiusClientSecretkey" name="_radiusClientSecretkey" value="<%=approvalSetting.radiusClientSecretkey%>">
                                <input type="hidden" id="_dayrestriction" name="_dayrestriction" value="<%=approvalSetting.dayrestriction%>">
                                <input type="hidden" id="_timerange" name="_timerange" value="<%=approvalSetting.timerange%>">
                                <input type="hidden" id="_timefromampm" name="_timefromampm" value="<%=approvalSetting.timefromampm%>">
                                <input type="hidden" id="_totimerange" name="_totimerange" value="<%=approvalSetting.totimerange%>">
                                <input type="hidden" id="_timetoampm" name="_timetoampm" value="<%=approvalSetting.timetoampm%>">

                                <%}%>
                            </form>
                            <%} else if (approvalSetting.action == AuthorizationManagement.ADD_EPIN_SETTING) {%>
                            <li><a href="#" onclick="ApproveRequest('addEpinSettingForm', 'editepinsettings', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                            <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>')">Reject</a></li>
                            <form class="form-horizontal" id="addMobileTrustSettingForm">
                                <% if (approvalSetting.prefernce == 1) {%>

                                <input type="hidden" id="_displayname" name="_displayname" value="<%=approvalSetting.displayname%>">
                                <input type="hidden" id="_weburl" name="_weburl" value="<%=approvalSetting.webeurl%>">
                                <input type="hidden" id="_weburlStatus" name="_weburlStatus" value="<%=approvalSetting.weburlStatus%>">
                                <input type="hidden" id="_ussdurl" name="_ussdurl" value="<%=approvalSetting.ussdurl%>">
                                <input type="hidden" id="_ussdurlStatus" name="_ussdurlStatus" value="<%=approvalSetting.ussdurlStatus%>">
                                <input type="hidden" id="_voiceurl" name="_voiceurl" value="<%=approvalSetting.voiceurl%>">
                                <input type="hidden" id="_voiceurlStatus" name="_voiceurlStatus" value="<%=approvalSetting.voiceurlStatus%>">
                                <input type="hidden" id="_smsurl" name="_smsurl" value="<%=approvalSetting.smsurl%>">
                                <input type="hidden" id="_smsurlStatus" name="_smsurlStatus" value="<%=approvalSetting.smsurlStatus%>">
                                <input type="hidden" id="_ChallengeResponseclassname" name="_ChallengeResponseclassname" value="<%=approvalSetting.ChallengeResponseclassname%>">
                                <input type="hidden" id="_ChallengeResponse" name="_ChallengeResponse" value="<%=approvalSetting.ChallengeResponse%>">
                                <input type="hidden" id="_PinSourceClassNameclassname" name="_PinSourceClassNameclassname" value="<%=approvalSetting.PinSourceClassNameclassname%>">
                                <input type="hidden" id="_PINSource" name="_PINSource" value="<%=approvalSetting.PINSource%>">
                                <input type="hidden" id="_smstext" name="_smstext" value="<%=approvalSetting.smstext%>">
                                <input type="hidden" id="_isAlertOperatorOnFailure" name="_isAlertOperatorOnFailure" value="<%=approvalSetting.isAlertOperatorOnFailure%>">
                                <input type="hidden" id="_expiryTime" name="_expiryTime" value="<%=approvalSetting.expiryTime%>">
                                <input type="hidden" id="_pinDeliveryType" name="_pinDeliveryType" value="<%=approvalSetting.pinDeliveryType%>">
                                <input type="hidden" id="_channelType" name="_channelType" value="<%=approvalSetting.channelType%>">
                                <input type="hidden" id="_channelType2" name="_channelType2" value="<%=approvalSetting.channelType2%>">
                                <input type="hidden" id="_splitduration" name="_splitduration" value="<%=approvalSetting.duration%>">
                                <input type="hidden" id="_dayRestriction" name="_dayRestriction" value="<%=approvalSetting.dayRestriction%>">
                                <input type="hidden" id="_timeFromRestriction" name="_timeFromRestriction" value="<%=approvalSetting.timeFromInHour%>">
                                <input type="hidden" id="_timeToRestriction" name="_timeToRestriction" value="<%=approvalSetting.timeToInHour%>">
                                <input type="hidden" id="_pinRequestCountDuration" name="_pinRequestCountDuration" value="<%=approvalSetting.pinRequestCountDuration%>">
                                <input type="hidden" id="_pinRequestCount" name="_pinRequestCount" value="<%=approvalSetting.pinRequestCount%>">
                                <input type="hidden" id="_operatorController" name="_operatorController" value="<%=approvalSetting.operatorController%>">

                                <%} else if (approvalSetting.action == AuthorizationManagement.WEBWATCH_ADD) {%>
                                <li><a href="#" onclick="ApproveRequest('addWebWatchForm', 'AddMSettings', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                                <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>')">Reject</a></li>
                                <form class="form-horizontal" id="addWebWatchForm">
                                    <input type="hidden" id="_name" name="_name" value="<%=approvalSetting.name%>">
                                    <input type="hidden" id="_status" name="_status" value="<%=approvalSetting.status%>">
                                    <input type="hidden" id="_edit" name="_edit" value="<%=approvalSetting.edituser%>">
                                    <input type="hidden" id="_selectedType" name="_selectedType" value="<%=approvalSetting.type%>">
                                    <input type="hidden" id="_frequency" name="_frequency" value="<%=approvalSetting.freequency%>">
                                    <input type="hidden" id="_timeout" name="_timeout" value="<%=approvalSetting.responseTime%>">
                                    <input type="hidden" id="_selectedSNMP" name="_selectedSNMP" value="<%=approvalSetting.snmp_name%>">
                                    <input type="hidden" id="_servicesalert" name="_servicesalert" value="<%=approvalSetting.monitors%>">
                                    <input type="hidden" id="_selectedAlert" name="_selectedAlert" value="<%=approvalSetting.alertmedia%>">
                                    <input type="hidden" id="_dropcount" name="_dropcount" value="<%=approvalSetting.dropcount%>">
                                    <input type="hidden" id="_selectedReoprt" name="_selectedReoprt" value="<%=approvalSetting.reportType%>">
                                    <input type="hidden" id="_templateName" name="_templateName" value="<%=approvalSetting.templateName%>">
                                    <input type="hidden" id="_templateName" name="_templateName" value="<%=approvalSetting.template_id%>">
                                    <input type="hidden" id="_webAddress" name="_webAddress" value="<%=approvalSetting.webadd%>">
                                    <input type="hidden" id="_imagescript" name="_imagescript" value="<%=approvalSetting.imagescript%>">
                                    <input type="hidden" id="_dnshost" name="_dnshost" value="<%=approvalSetting.dnshost%>">
                                    <input type="hidden" id="_dnsport" name="_dnsport" value="<%=approvalSetting.port%>">
                                    <input type="hidden" id="_dnsname" name="_dnsname" value="<%=approvalSetting.dnsname%>">
                                    <input type="hidden" id="_selectedLookup" name="_selectedLookup" value="<%=approvalSetting.selectedlookup%>">
                                    <input type="hidden" id="linktodns" name="linktodns" value="<%=approvalSetting.linktodns%>">
                                    <input type="hidden" id="_Porthost" name="_Porthost" value="<%=approvalSetting.porthost%>">
                                    <input type="hidden" id="_Portport" name="_Portport" value="<%=approvalSetting.portport%>">
                                    <input type="hidden" id="_Portcommand" name="_Portcommand" value="<%=approvalSetting.portcommand%>">
                                    <input type="hidden" id="_Pinghost" name="_Pinghost" value="<%=approvalSetting.Pinghost%>">
                                    <input type="hidden" id="_selectedprotocol" name="_selectedprotocol" value="<%=approvalSetting.selectedprotocol%>">
                                    <input type="hidden" id="_sslHost" name="_sslHost" value="<%=approvalSetting.sslHost%>">
                                    <input type="hidden" id="_sslPort" name="_sslPort" value="<%=approvalSetting.sslPort%>">
                                    <input type="hidden" id="linktossl" name="linktossl" value="<%=approvalSetting.linktossl%>">
                                    <input type="hidden" id="_notify" name="_notify" value="<%=approvalSetting.notify%>">
                                </form>
                                <%} else if (approvalSetting.action == AuthorizationManagement.WEBWATCH_CHANGESTATUS) {%>
                                <li><a href="#" onclick="ApproveRequest('changeStatusWebWatchForm', 'ChangeSettingStatus', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                                <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>')">Reject</a></li>
                                <form class="form-horizontal" id="changeStatusWebWatchForm">
                                    <input type="hidden" id="name" name="name" value="<%=approvalSetting.name%>">
                                    <input type="hidden" id="newstatus" name="newstatus" value="<%=approvalSetting.status%>">
                                    <input type="hidden" id="oldstatus" name="oldstatus" value="<%=approvalSetting.oldvalue%>">
                                </form>
                                <%} else if (approvalSetting.action == AuthorizationManagement.WEBWATCH_DELETE) {%>
                                <li><a href="#" onclick="ApproveRequest('RemoveMonitoring', 'RemoveMonitorSetting', '<%=arrRequest[i].getApprovalId()%>')">Approve</a></li>
                                <li><a href="#" onclick="removeAuthorization('<%= arrRequest[i].getApprovalId()%>')">Reject</a></li>
                                <form class="form-horizontal" id="changeStatusWebWatchForm">
                                    <input type="hidden" id="moniotorId" name="moniotorId" value="<%=approvalSetting.name%>">
                                </form>
                                <%}%>
                        </ul>
                    </div>
                </td>

<!--<td><%=arrRequest[i].getUpdatedOn()%></td>-->
                <%}%>
                <td><%=arrRequest[i].getCreatedOn()%></td>
                <td><%=arrRequest[i].getExpireOn()%></td>
            </tr>


            <%}
                        }
                    }
                }
            } else {%>
            <tr>
                <td><%=1%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>

            </tr>
            <%}%>


        </table>

    </div>
</div>
<!--</div>-->

<div id="requestReject" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Reject Request</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="requestRejectform" name="requestRejectform">
                <fieldset>
                    <input type="hidden" id="_approvalID" name="_approvalID" >
                    <input type="hidden" id="_unitId" name="_unitId" >
                    Description
                    <select class="span7" name="_description" id="_description">
                        <option value="INVALID_DATA">Invalid Data</option>
                        <option value="INVALID_REQUEST" >Invalid Request</option>
                        <option value="DUPLICATE_ENTRY" >Duplicate Entry</option>
                        <option value="INCORRECT_DATA" >Incorrect Data</option>
                        <option value="UNAUTHORIZED_REQUEST" >Unauthorized Request</option>
                        <option value="OTHER" >Other</option>
                    </select>
                    <!--                    <div class="control-group">
                                            <label class="control-label"  for="username">Description</label>
                                            <div class="controls">
                                                <textarea class="form-control" rows="3" id="_description" name="_description"></textarea>
                                            </div>
                                        </div>-->
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="rejectkyc-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="removeAuthorizationReq()" id="addUserButton">Apply Now>></button>
    </div>
</div>        



<%--<%@include file="footer.jsp" %>--%>