<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="com.mollatech.axiom.nucleus.db.Remotesignature"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.PDFSigningManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<!--<div class="container-fluid">-->
<%
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    String _channelId = channel.getChannelid();

    String _searchType = request.getParameter("_searchType");
    String _startdaters = request.getParameter("_startdaters");
    String _enddaters = request.getParameter("_enddaters");
    Date dstartdate = null;
    Date denddate = null;
    if (_startdaters != null && _enddaters != null && !_startdaters.equals("") && !_enddaters.equals("")) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        dstartdate = format.parse(_startdaters);
        denddate = format.parse(_enddaters);
    }
    int isearchType = Integer.parseInt(_searchType);
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm aaa");

    UserManagement usermngObj = new UserManagement();
    PDFSigningManagement pdfObj = new PDFSigningManagement();

    AuthUser Users[] = null;
    Remotesignature[] RemoteSign = null;
    if (isearchType == 1) {
        String _userName = request.getParameter("_userName");
        Users = usermngObj.SearchUsersByID(sessionId, _channelId, _userName);
        session.setAttribute("remoteSignReportUser", Users);
    } else if (isearchType == 0) {
        String _archiveId = request.getParameter("_userName");
        RemoteSign = pdfObj.getRemoteSignatureObjV2(_channelId, _archiveId, isearchType, dstartdate, denddate);
    }
    if (isearchType == 2) {
        String _reference = request.getParameter("_userName");
        RemoteSign = pdfObj.getRemoteSignatureObjV2(_channelId, _reference, isearchType, dstartdate, denddate);
    }
    session.setAttribute("remoteSignReport", RemoteSign);
    String strerr = "No Record Found";
%>

<div class="row-fluid">

    <div id="licenses_data_table">
        <input type="hidden" id="_isroaming" name="_isroaming"  >
        <table class="table table-striped" id="table_main">

            <tr>
                <td>No.</td>
                <td>Archive Id</td>
                <td>Reference Id</td>
                <td>User</td>
                <td>Type</td>
                <td>OTP Result</td>
                <td>OTP Verify Time</td>                
                <td>Signature</td>
                <td>Data/File</td>
                <td>Signing Result</td>
                <td>Signing Time</td>
<!--                <td>OTP Verification</td>-->
            </tr>
            <%
                if (Users != null) {
                    int count = 0;
                    for (int i = 0; i < Users.length; i++) {
                        Remotesignature[] rObj = pdfObj.getRemoteSignatureObjV2(channel.getChannelid(), Users[i].getUserId(), isearchType, dstartdate, denddate);

                        String signingDate = "NA";
                        String timeOfVerification = "NA";
                        if (rObj != null) {
                            for (int j = 0; j < rObj.length; j++) {
                                String strdoctype = "";
                                if (rObj[j].getDocType() == 1) {
                                    strdoctype = "Raw";
                                } else if (rObj[j].getDocType() == 2) {
                                    strdoctype = "PDF";
                                }

                                String strresult = "";
                                if (rObj[j].getOtpResult() == 0) {
                                    strresult = "Success";
                                } else if (rObj[j].getOtpResult() == -1) {
                                    strresult = "Failed";
                                } else if (rObj[j].getOtpResult() == -2) {
                                    strresult = "Expired";
                                }
                                String strresultsigning = "";
                                if (rObj[j].getResultOfSigning() == 0) {
                                    strresultsigning = "Success";
                                } else if (rObj[j].getResultOfSigning() == 1) {
                                    strresultsigning = "Failed";
                                }else {
                                    strresultsigning = "Failed";
                                }
//                     String signaturefile = rObj[i].getSignature();
//                     String datatosignfile = rObj[i].getDataToSign();
                                String datatosignfile = rObj[j].getDataToSign();
                                if (rObj[j].getTimeOfOtpVerification() != null) {

                                    timeOfVerification = sdf.format(rObj[j].getTimeOfOtpVerification());
                                }
                                if (rObj[j].getTimeOfSigning() != null) {
                                    signingDate = sdf.format(rObj[j].getTimeOfSigning());
                                }
                                String otptoVerify = "";
                                if (rObj[j].getOtp() != null) {
                                    otptoVerify = rObj[j].getOtp();
                                }
                                String useridForOtp = "";
                                if (rObj[j].getUserid() != null) {
                                    useridForOtp = rObj[j].getUserid();
                                }
            %>
            <tr>
                <td><%=++count%></td>
                <td><%=rObj[j].getArchiveid()%></td>
                <td><%=rObj[j].getReferenceid()%></td>
                <td><%= Users[i].getUserName()%></td>
                <td><%=strdoctype%></td>                
                <td><%=strresult%></td>
                <td><%=timeOfVerification%></td>                
                <%
                    String s = URLEncoder.encode(rObj[j].getArchiveid(), "UTF-8");
                %>
                <td><a href='./getremoteserversigning?archiveId=<%=s%>&type=1'>Download</a></td>
                <td><a href='./getremoteserversigning?archiveId=<%=s%>&type=2'>Download</a></td>
                <td><%=strresultsigning%></td>
                <td><%=signingDate%></td>
<!--                <td><button class="btn btn-success" onclick="verifyotpForReports('<%=useridForOtp%>', '<%=otptoVerify%>', '<%=datatosignfile%>', '<%=timeOfVerification%>')" type="button">Verify</button></td>-->
            </tr>
            <%}
                    }
                }
            } else if (RemoteSign != null) {

                for (int k = 0; k < RemoteSign.length; k++) {
            %>

            <%
                String signingDate = "NA";
                String timeOfVerification = "NA";
                AuthUser uObj = usermngObj.getUser(sessionId, _channelId, RemoteSign[k].getUserid());
                String signaturefile = RemoteSign[k].getSignature();
                String datatosignfile = RemoteSign[k].getDataToSign();
                String strdoctype = "";
                if (RemoteSign[k].getDocType() == 1) {
                    strdoctype = "Raw";
                } else if (RemoteSign[k].getDocType() == 2) {
                    strdoctype = "PDF";
                }

                String strresult = "";
                if (RemoteSign[k].getOtpResult() == 0) {
                    strresult = "Success";
                } else if (RemoteSign[k].getOtpResult() == -1) {
                    strresult = "Failed";
                } else if (RemoteSign[k].getOtpResult() == -2) {
                    strresult = "Expired";
                }
                String strresultsigning = "";
                if (RemoteSign[k].getResultOfSigning() == 0) {
                    strresultsigning = "Success";
                } else if (RemoteSign[k].getResultOfSigning() == 1) {
                    strresultsigning = "Failed";
                }else{
                    strresultsigning = "Failed";
                }
                if (RemoteSign[k].getTimeOfOtpVerification() != null) {
                    timeOfVerification = sdf.format(RemoteSign[k].getTimeOfOtpVerification());
                }
                if (RemoteSign[k].getTimeOfSigning() != null) {
                    signingDate = sdf.format(RemoteSign[k].getTimeOfSigning());
                }
                String otptoVerify = "";
                if (RemoteSign[k].getOtp() != null) {
                    otptoVerify = sdf.format(RemoteSign[k].getOtp());
                }
                String useridForOtp = "";
                if (RemoteSign[k].getUserid() != null) {
                    useridForOtp = RemoteSign[k].getUserid();
                }

            %>

            <tr>
                <td><%=(k + 1)%></td>
                <td><%=RemoteSign[k].getArchiveid()%></td>
                <td><%=RemoteSign[k].getReferenceid()%></td>
                <td><%=uObj.getUserName()%></td>
                <td><%=strdoctype%></td>
                <td><%=strresult%></td>
                <td><%=timeOfVerification%></td>                
                <%
                    String s = URLEncoder.encode(RemoteSign[k].getArchiveid(), "UTF-8");
                    String r = URLEncoder.encode(RemoteSign[k].getReferenceid(), "UTF-8");
                %>
                <td><a href='./getremoteserversigning?archiveId=<%=s%>&type=1'>Download</a></td>
                <td><a href='./getremoteserversigning?archiveId=<%=s%>&type=2'>Download</a></td>                
                <td><%=strresultsigning%></td>
                <td><%=signingDate%></td>

<!--           <td><a onclick="verifyotpForReports('<%=RemoteSign[k].getUserid()%>','<%=RemoteSign[k].getOtp()%>','<%=RemoteSign[k].getDataToSign()%>','<%=RemoteSign[k].getTimeOfSigning()%>')">Download</a></td>-->
<!--                <td><button class="btn btn-success" onclick="verifyotpForReports('<%=useridForOtp%>', '<%=otptoVerify%>', '<%=datatosignfile%>', '<%=timeOfVerification%>')" type="button">Verify</button></td>-->
                <%}
                } else {%>
            <tr>
                <td><%=1%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>    
                <td><%= strerr%></td> 
                <%}%>
            </tr>         
        </table>
        <div class="row-fluid">
            <div class="span6">
                <div class="control-group">                        
                    <div class="span1">
                        <div class="control-group form-inline">
                            <a href="#" class="btn btn-info" onclick="downloadRSSReport(1, '<%=_startdaters%>', '<%=_enddaters%>', '<%=_searchType%>')" >
                                CSV</a>
                        </div>
                    </div>
                    <div class="span1">
                        <div class="control-group form-inline">
                            <a href="#" class="btn btn-info" onclick="downloadRSSReport(0, '<%=_startdaters%>', '<%=_enddaters%>', '<%=_searchType%>')" >
                                PDF</a>
                        </div>
                    </div>
                    <div class="span1">
                        <div class="control-group form-inline">
                            <a href="#" class="btn btn-info" onclick="downloadRSSReport(2, '<%=_startdaters%>', '<%=_enddaters%>', '<%=_searchType%>')" >
                                TEXT</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>     
    </div>
</div>
<!--</div>-->





<%--<%@include file="footer.jsp" %>--%>