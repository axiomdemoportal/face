<%@page import="com.mollatech.axiom.nucleus.db.Interactionsexecutions"%>
<%@page import="com.mollatech.axiom.nucleus.db.operation.SurveyQueAndOptions"%>
<%@page import="com.mollatech.axiom.nucleus.db.operation.Survey"%>
<%@page import="com.mollatech.dictum.management.SurveyManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Interactions"%>
<%@include file="header.jsp" %>
<script src="./assets/js/interactions.js"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">
<div class="container-fluid">
    <h2>Archives Executions</h2>
    <p>List of executed interactions, their results...</p>
    <br>
    <div class="row-fluid">
        <div id="licenses_data_table">
            <table class="table table-striped">
                <tr>
                    <td>No.</td>
                    <td>Name</td>
                    <td>Status</td>
                    <td>Show Reports</td>
                    <td>Manage</td>
                    <td>Created On</td>
                    <td>Last Updated On</td>
                    <td>Expired On</td>
                </tr>

                <%
                    Channels _apSChannelDetails = (Channels) session.getAttribute("_apSChannelDetails");
                    String _irid = (String) request.getParameter("_irid");
                    SurveyManagement surmngObj = new SurveyManagement();
                    Interactionsexecutions[] irexeList = surmngObj.getInteractionsExecutionList(Integer.valueOf(_irid).intValue(), _apSChannelDetails.getChannelid());

                    if (irexeList != null)
                        for (int i = 0; i < irexeList.length; i++) {
                            Interactionsexecutions irexeObj = irexeList[i];


                            String nameIRE = irexeObj.getInteractionName();
                            int iID = irexeObj.getInteractionexecutionid();


                            java.util.Date dLA = irexeObj.getLastUpdateOn();
                            java.util.Date dCR = irexeObj.getCreatedOn();
                            java.util.Date dEX = irexeObj.getExpirydatetime();


                            int iOprStatus = irexeObj.getStatus();
                            String strStatus;
                            if (iOprStatus == 1) {
                                strStatus = "Running...";
                            } else {
                                strStatus = "Stopped.";
                            }

                            SimpleDateFormat sdf = new SimpleDateFormat("d MMM,yyyy HH:mm ");

                            String uidiv4OprStatus = "irexe-status-value-" + i;

                %>
                <tr>
                    <td><%=i + 1%></td>
                    <td><%=nameIRE%></td>
                    <td><span class="label label-success"><%=strStatus%></span></td>                                        
                    <td><a href="#" class="btn btn-mini" onclick="InteractionReport('<%=irexeObj.getInteractionid()%>','<%=irexeObj.getInteractionexecutionid()%>')">View Report</a></td>
                    <td>
                        <div class="btn-group">
                            <button class="btn btn-mini" id="<%=uidiv4OprStatus%>">Manage</button>
                            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeInteractionExecutionStatus('<%=iID%>',1,'<%=uidiv4OprStatus%>')">Stop Execution?</a></li>
                                <li class="divider"></li>
                                <li><a href="#" onclick="ChangeInteractionExecutionStatus('<%=iID%>',2,'<%=uidiv4OprStatus%>')">Remove Forever?</a></li>
                            </ul>
                        </div>
                    </td>
                    <td><%=sdf.format(dCR)%></td>
                    <td><%=sdf.format(dLA)%></td>
                    <td><%=sdf.format(dEX)%></td>
                </tr>
                <%}%>
            </table>


        </div>
    </div>
    <br>
    <p><a href="InteractionsList.jsp" class="btn"><< List Interactions</a></p>
    <script language="javascript">
        //listChannels();
    </script>
</div>

<%@include file="footer.jsp" %>
