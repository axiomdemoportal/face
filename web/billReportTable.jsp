<%@page import="com.mollatech.axiom.nucleus.settings.BillingManagerSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.connector.access.controller.AccessMatrixSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.Operators"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Billmanager"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.BillingManagment"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.MobileTrustManagment"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Geotrack"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.GeoLocationManagement"%>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Random" %>
<%    
    int REGISTER = 1;
    int LOGIN = 2;
    int TRANSACTION = 3;
    int CHANGEINPROFILE = 4;

    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    String _startdate = request.getParameter("_startdate");
    String _enddate = request.getParameter("_enddate");
    String _type = request.getParameter("_type");
    DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    Date startDate = null;
    if (_startdate != null && !_startdate.isEmpty()) {
        startDate = (Date) formatter.parse(_startdate);
    }
    Date endDate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        endDate = (Date) formatter.parse(_enddate);
    }
    int itype = -1;
    if (_type != null) {
        itype = Integer.parseInt(_type);
    }

    BillingManagment billMngt = new BillingManagment();
    UserManagement userMngt = new UserManagement();
    

    String _reportType = request.getParameter("_reportType");
    Billmanager[] arrBill = null;
    String strType1 = "-";
    if (_reportType.equals("1")) {
        strType1 = "subbillreportdownload";
        arrBill = billMngt.getBillRecordsByType(channel.getChannelid(), itype,startDate,endDate);
    } else {
        strType1 = "txbillreportdownload";
        arrBill = billMngt.getBillRecordsByTypePerTx(channel.getChannelid(), itype,  startDate,endDate);
    }
    String strerr = "No Records Found";
    Operators oprObjI = (Operators) session.getAttribute("_apOprDetail");
    AccessMatrixSettings accessObjN = (AccessMatrixSettings) session.getAttribute("_apAccessEntry");
    
    
    SettingsManagement sMngmt = new SettingsManagement();
    Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), sMngmt.BILLING_MANAGER_SETTING, sMngmt.PREFERENCE_ONE);
    BillingManagerSettings billingObj = (BillingManagerSettings) settingsObj;
    String strCurrencyType = billingObj.strCurrencyType;
            

%>

<h3>Search Results</h3>
<div class="row-fluid">
    <div class="span6">
        <%if (_reportType.equals("1")) {%>
        <div class="control-group">                        
            <div class="span1">
                <div class="control-group form-inline">
                    <%if (oprObjI.getRoleid() != 1) {//1 sysadmin
                            if (accessObjN.downloadsubscriptionBaseBillingReport == true) {%>
                    <a href="#"  class="btn btn-info" onclick="geoBillReportCSV('<%=itype%>', '<%=_reportType%>')" >
                        <%} else {%>
                        <a href="#"  class="btn btn-info" onclick="InvalidRequestBillReport('<%= strType1%>')" >
                            <%}
                            } else {%>
                            <a href="#"  class="btn btn-info" onclick="geoBillReportCSV('<%=itype%>', '<%=_reportType%>')" >
                                <%}%>
                                <!--<a href="#"  class="btn btn-info" onclick="tokenFailureReportCSV()" >-->
                                <i class="icon-white icon-chevron-down"></i> CSV</a>
                            </div>
                            </div>
                            <div class="span1">
                                <div class="control-group form-inline">
                                    <%if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                            if (accessObjN.downloadsubscriptionBaseBillingReport == true) {%>
                                    <a href="#"  class="btn btn-info" onclick="geoBillReportPDF('<%=itype%>', '<%=_reportType%>')" >
                                        <%} else {%>
                                        <a href="#"  class="btn btn-info" onclick="InvalidRequestBillReport('<%= strType1%>')" >
                                            <%}
                                            } else {%>
                                            <a href="#"  class="btn btn-info" onclick="geoBillReportPDF('<%=itype%>', '<%=_reportType%>')" >
                                                <%}%>
                                                <i class="icon-white icon-chevron-down"></i> PDF</a>
                                            </div>
                                            </div>
                                            <div class="span1">
                                                <div class="control-group form-inline">
                                                    <%if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                            if (accessObjN.downloadsubscriptionBaseBillingReport == true) {%>
                                                    <a href="#"  class="btn btn-info" onclick="geoBillReportTXT('<%=itype%>', '<%=_reportType%>')" >
                                                        <%} else {%>
                                                        <a href="#"  class="btn btn-info" onclick="InvalidRequestBillReport('<%= strType1%>')" >
                                                            <%}
                                                            } else {%>
                                                            <a href="#"  class="btn btn-info" onclick="geoBillReportTXT('<%=itype%>', '<%=_reportType%>')" >
                                                                <%}%>
                                                                <!--<a href="#" class="btn btn-info" onclick="tokenFailureReportTXT()" >-->
                                                                <i class="icon-white icon-chevron-down"></i> TXT</a>
                                                            </div>
                                                            </div>


                                                            </div>
                                                            <%} else { %>
                                                            <div class="control-group">                        
                                                                <div class="span1">
                                                                    <div class="control-group form-inline">
                                                                        <%if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                if (accessObjN.downloadtranscationBaseBillingReport == true) {%>
                                                                        <a href="#"  class="btn btn-info" onclick="geoBillReportCSV('<%=itype%>', '<%=_reportType%>')" >
                                                                            <%} else {%>
                                                                            <a href="#"  class="btn btn-info" onclick="InvalidRequestBillReport('<%= strType1%>')" >
                                                                                <%}
                                                                                } else {%>
                                                                                <a href="#"  class="btn btn-info" onclick="geoBillReportCSV('<%=itype%>', '<%=_reportType%>')" >
                                                                                    <%}%>
                                                                                    <!--<a href="#"  class="btn btn-info" onclick="tokenFailureReportCSV()" >-->
                                                                                    <i class="icon-white icon-chevron-down"></i> CSV</a>
                                                                                </div>
                                                                                </div>
                                                                                <div class="span1">
                                                                                    <div class="control-group form-inline">
                                                                                        <%if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                if (accessObjN.downloadtranscationBaseBillingReport == true) {%>
                                                                                        <a href="#"  class="btn btn-info" onclick="geoBillReportPDF('<%=itype%>', '<%=_reportType%>')" >
                                                                                            <%} else {%>
                                                                                            <a href="#"  class="btn btn-info" onclick="InvalidRequestBillReport('<%= strType1%>')" >
                                                                                                <%}
                                                                                                } else {%>
                                                                                                <a href="#"  class="btn btn-info" onclick="geoBillReportPDF('<%=itype%>', '<%=_reportType%>')" >
                                                                                                    <%}%>
                                                                                                    <i class="icon-white icon-chevron-down"></i> PDF</a>
                                                                                                </div>
                                                                                                </div>
                                                                                                <div class="span1">
                                                                                                    <div class="control-group form-inline">
                                                                                                        <%if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                                if (accessObjN.downloadtranscationBaseBillingReport == true) {%>
                                                                                                        <a href="#"  class="btn btn-info" onclick="geoBillReportTXT('<%=itype%>', '<%=_reportType%>')" >
                                                                                                            <%} else {%>
                                                                                                            <a href="#"  class="btn btn-info" onclick="InvalidRequestBillReport('<%= strType1%>')" >
                                                                                                                <%}
                                                                                                                } else {%>
                                                                                                                <a href="#"  class="btn btn-info" onclick="geoBillReportTXT('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                    <%}%>
                                                                                                                    <!--<a href="#" class="btn btn-info" onclick="tokenFailureReportTXT()" >-->
                                                                                                                    <i class="icon-white icon-chevron-down"></i> TXT</a>
                                                                                                                </div>
                                                                                                                </div>


                                                                                                                </div>
                                                                                                                <!--                                                            <div class="control-group">                        
                                                                                                                                                                                <div class="span1">
                                                                                                                                                                                    <div class="control-group form-inline">
                                                                                                                <%  if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                                        if (accessObjN.downloadtranscationBaseBillingReport == true) {%>
                                                                                                                <a href="#"  class="btn btn-info" onclick="geoBillReportCSV('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                <%} else {%>
                                                                                                                <a href="#"  class="btn btn-info" onclick="InvalidRequestBillReport('<%= strType1%>')" >
                                                                                                                <%}
                                                                                                                } else {%>
                                                                                                                <a href="#"  class="btn btn-info" onclick="geoBillReportCSV('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                <%}%>
                                                                                                                <i class="icon-white icon-chevron-down"></i> CSV</a>
                                                                                                            </div>
                                                                                                            </div>
                                                                                                            <div class="span1">
                                                                                                                <div class="control-group form-inline">
                                                                                                                <%if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                                        if (accessObjN.downloadtranscationBaseBillingReport == true) {%>
                                                                                                                <a href="#"  class="btn btn-info" onclick="geoBillReportPDF('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                <%} else {%>
                                                                                                                <a href="#"  class="btn btn-info" onclick="InvalidRequestBillReport('<%= strType1%>')" >
                                                                                                                <%}%>
                                                                                                                <a href="#"  class="btn btn-info" onclick="geoBillReportPDF('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                <%}%>
                                                                                                                <i class="icon-white icon-chevron-down"></i> PDF</a>
                                                                                                            </div>
                                                                                                            </div>
                                                                                                            <div class="span1">
                                                                                                                <div class="control-group form-inline">
                                                                                                                <%if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                                        if (accessObjN.downloadtranscationBaseBillingReport == true) {%>
                                                                                                                <a href="#"  class="btn btn-info" onclick="geoBillReportTXT('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                <%} else {%>
                                                                                                                <a href="#"  class="btn btn-info" onclick="InvalidRequestBillReport('<%= strType1%>')" >
                                                                                                                <%}
                                                                                                                } else {%>
                                                                                                                <a href="#"  class="btn btn-info" onclick="geoBillReportTXT('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                <%}%>
                                                                                                                <a href="#" class="btn btn-info" onclick="tokenFailureReportTXT()" >
                                                                                                                <i class="icon-white icon-chevron-down"></i> TXT</a>
                                                                                                            </div>
                                                                                                            </div>


                                                                                                            </div>-->
                                                                                                                <%}%>
                                                                                                                </div>
                                                                                                                </div>
                                                                                                                <div id="licenses_data_table">

                                                                                                                    <input type="hidden" id="_tracking" name="_tracking"  >
                                                                                                                    <table class="table table-striped" id="table_main">
                                                                                                                        <tr>
                                                                                                                            <td>No.</td>
                                                                                                                            <td>Name</td>
                                                                                                                            <td>Bill Type</td>
                                                                                                                            <td>Bill Generation</td>
                                                                                                                            <td>Cost</td>
                                                                                                                            <td>Type</td>
                                                                                                                            <td>Next Bill Date</td>
                                                                                                                            <td>Dated</td>
                                                                                                                        </tr>
                                                                                                                        <%           
        
                                                                                                                            if (arrBill != null) {
                                                                                                                                for (int i = 0; i < arrBill.length; i++) {
                                                                                                                                    SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                                                                                                                    String start = df2.format(arrBill[i].getCreatedOn());
                                                                                                                                    //                    SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                                                                                                                    String nextBillDate = "--";
                                                                                                                                    if (arrBill[i].getNexBillDate() != null) {
                                                                                                                                        nextBillDate = df2.format(arrBill[i].getNexBillDate());
                                                                                                                                    }
                                                                                                                                    AuthUser user = userMngt.getUser(sessionId, channel.getChannelid(), arrBill[i].getUserid());

                                                                                                                                    String strType = "--";
                                                                                                                                    if (arrBill[i].getCategory() == BillingManagment.SOFTWARE_TOKEN) {
                                                                                                                                        strType = "Software OTP Token";
                                                                                                                                    } else if (arrBill[i].getCategory() == BillingManagment.HARDWARE_TOKEN) {
                                                                                                                                        strType = "Hardware OTP Token";
                                                                                                                                    } else if (arrBill[i].getCategory() == BillingManagment.OOB_TOKEN) {
                                                                                                                                        strType = "OOB OTP Token";
                                                                                                                                    } else if (arrBill[i].getCategory() == BillingManagment.SW_PKI_TOKEN) {
                                                                                                                                        strType = "Software PKI Token";
                                                                                                                                    } else if (arrBill[i].getCategory() == BillingManagment.HW_PKI_TOKEN) {
                                                                                                                                        strType = "Hardware PKI Token";
                                                                                                                                    } else if (arrBill[i].getCategory() == BillingManagment.CERTIFICATE) {
                                                                                                                                        strType = "Digital Certificate";
                                                                                                                                    }

                                                                                                                                    String billType = "--";
                                                                                                                                    if (arrBill[i].getBillType() == 1) {
                                                                                                                                        billType = "Bill PEr Transaction";
                                                                                                                                    } else if (arrBill[i].getBillType() == 2) {
                                                                                                                                        billType = "Subscription";
                                                                                                                                    } else if (arrBill[i].getBillType() == 3) {
                                                                                                                                        billType = "One Time";
                                                                                                                                    }
                                                                                                                                    String billGeneration = "" + arrBill[i].getNexBillDate();
//                                                                                                                                    if (arrBill[i].getNexBillDate()!= null) {
//                                                                                                                                        if (arrBill[i].getBillType() == 1) {
//                                                                                                                                            billGeneration = "Monthly";
//                                                                                                                                        } else if (arrBill[i].getBillType() == 2) {
//                                                                                                                                            billGeneration = "Yearly";
//                                                                                                                                        } else if (arrBill[i].getBillType() == 3) {
//                                                                                                                                            billGeneration = "Quarterly";
//                                                                                                                                        }
//                                                                                                                                    }

                                                                                                                                    String name = "--";
                                                                                                                                    if (user != null) {
                                                                                                                                        name = user.userName;
                                                                                                                                    }

                                                                                                                        %>
                                                                                                                        <tr>
                                                                                                                            <td><%=(i + 1)%></td>
                                                                                                                            <td><%=name%></td>
                                                                                                                            <td><%=billType%></td>
                                                                                                                            <td><%= billGeneration%></td>
                                                                                                                            <td><%=strCurrencyType%> <%=arrBill[i].getCost()%></td>
                                                                                                                            <td><%= strType%></td>
                                                                                                                            <td><%=nextBillDate%></td>
                                                                                                                            <td><%=start%></td>
                                                                                                                        </tr>
                                                                                                                        <% }
                                                                                                                        } else {%>
                                                                                                                        <tr>
                                                                                                                            <td><%=1%></td>
                                                                                                                            <td><%= strerr%></td>
                                                                                                                            <td><%= strerr%></td>
                                                                                                                            <td><%= strerr%></td>
                                                                                                                            <td><%= strerr%></td>
                                                                                                                            <td><%= strerr%></td>
                                                                                                                            <td><%= strerr%></td>
                                                                                                                            <td><%= strerr%></td>                                                                                                                           
                                                                                                                            <%}%>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </div>
                                                                                                                <div class="row-fluid">
                                                                                                                    <div class="span6">
                                                                                                                        <%  if (_reportType.equals("1")) {%>
                                                                                                                        <div class="control-group">                        
                                                                                                                            <div class="span1">
                                                                                                                                <div class="control-group form-inline">
                                                                                                                                    <%
                                                                                                                                        //Operators oprObjI = (Operators) session.getAttribute("_apOprDetail");
                                                                                                                                        // AccessMatrixSettings accessObjN = (AccessMatrixSettings) session.getAttribute("_apAccessEntry");
                                                                                                                                        if (oprObjI.getRoleid() != 1) {//1 sysadmin

                                                                                                                                            if (accessObjN.downloadsubscriptionBaseBillingReport == true) {%>
                                                                                                                                    <a href="#"  class="btn btn-info" onclick="geoBillReportCSV('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                                        <%} else {%>
                                                                                                                                        <a href="#"  class="btn btn-info" onclick="InvalidRequestBillReport('<%= strType1%>')" >
                                                                                                                                            <%}
                                                                                                                                            } else {%>
                                                                                                                                            <a href="#"  class="btn btn-info" onclick="geoBillReportCSV('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                                                <%}%>
                                                                                                                                                <!--<a href="#"  class="btn btn-info" onclick="tokenFailureReportCSV()" >-->
                                                                                                                                                <i class="icon-white icon-chevron-down"></i> CSV</a>
                                                                                                                                            </div>
                                                                                                                                            </div>
                                                                                                                                            <div class="span1">
                                                                                                                                                <div class="control-group form-inline">
                                                                                                                                                    <%if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                                                                            if (accessObjN.downloadsubscriptionBaseBillingReport == true) {%>
                                                                                                                                                    <a href="#"  class="btn btn-info" onclick="geoBillReportPDF('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                                                        <%} else {%>
                                                                                                                                                        <a href="#"  class="btn btn-info" onclick="InvalidRequestBillReport('<%= strType1%>')" >
                                                                                                                                                            <%}
                                                                                                                                                            } else {%>
                                                                                                                                                            <a href="#"  class="btn btn-info" onclick="geoBillReportPDF('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                                                                <%}%>
                                                                                                                                                                <!--<a href="#" class="btn btn-info" onclick="tokenFailureReportPDF()" >-->
                                                                                                                                                                <i class="icon-white icon-chevron-down"></i> PDF</a>
                                                                                                                                                            </div>
                                                                                                                                                            </div>
                                                                                                                                                            <div class="span1">
                                                                                                                                                                <div class="control-group form-inline">
                                                                                                                                                                    <%if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                                                                                            if (accessObjN.downloadsubscriptionBaseBillingReport == true) {%>
                                                                                                                                                                    <a href="#"  class="btn btn-info" onclick="geoBillReportTXT('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                                                                        <%} else {%>
                                                                                                                                                                        <a href="#"  class="btn btn-info" onclick="InvalidRequestBillReport('<%= strType1%>')" >
                                                                                                                                                                            <%}
                                                                                                                                                                            } else {%>
                                                                                                                                                                            <a href="#"  class="btn btn-info" onclick="geoBillReportTXT('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                                                                                <%}%>
                                                                                                                                                                                <!--<a href="#" class="btn btn-info" onclick="tokenFailureReportTXT()" >-->
                                                                                                                                                                                <i class="icon-white icon-chevron-down"></i> TXT</a>
                                                                                                                                                                            </div>
                                                                                                                                                                            </div>


                                                                                                                                                                            </div>
                                                                                                                                                                            <%} else { %>
                                                                                                                                                                            <div class="control-group">                        
                                                                                                                                                                                <div class="span1">
                                                                                                                                                                                    <div class="control-group form-inline">
                                                                                                                                                                                        <%if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                                                                                                                if (accessObjN.downloadtranscationBaseBillingReport == true) {%>
                                                                                                                                                                                        <a href="#"  class="btn btn-info" onclick="geoBillReportCSV('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                                                                                            <%} else {%>
                                                                                                                                                                                            <a href="#"  class="btn btn-info" onclick="InvalidRequestBillReport('<%= strType1%>')" >
                                                                                                                                                                                                <%}
                                                                                                                                                                                                } else {%>
                                                                                                                                                                                                <a href="#"  class="btn btn-info" onclick="geoBillReportCSV('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                                                                                                    <%}%>
                                                                                                                                                                                                    <!--<a href="#"  class="btn btn-info" onclick="tokenFailureReportCSV()" >-->
                                                                                                                                                                                                    <i class="icon-white icon-chevron-down"></i> CSV</a>
                                                                                                                                                                                                </div>
                                                                                                                                                                                                </div>
                                                                                                                                                                                                <div class="span1">
                                                                                                                                                                                                    <div class="control-group form-inline">
                                                                                                                                                                                                        <%if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                                                                                                                                if (accessObjN.downloadtranscationBaseBillingReport == true) {%>
                                                                                                                                                                                                        <a href="#"  class="btn btn-info" onclick="geoBillReportPDF('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                                                                                                        <%} else {%>
                                                                                                                                                                                                        <a href="#"  class="btn btn-info" onclick="InvalidRequestBillReport('<%= strType1%>')" >
                                                                                                                                                                                                        <%}
                                                                                                                                                                                                        } else {%>
                                                                                                                                                                                                        <a href="#"  class="btn btn-info" onclick="geoBillReportPDF('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                                                                                                        <%}%>
                                                                                                                                                                                                        <i class="icon-white icon-chevron-down"></i> PDF</a>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="span1">
                                                                                                                                                                                                        <div class="control-group form-inline">
                                                                                                                                                                                                        <%if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                                                                                                                                if (accessObjN.downloadtranscationBaseBillingReport == true) {%>
                                                                                                                                                                                                        <a href="#"  class="btn btn-info" onclick="geoBillReportTXT('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                                                                                                        <%} else {%>
                                                                                                                                                                                                        <a href="#"  class="btn btn-info" onclick="InvalidRequestBillReport('<%= strType1%>')" >
                                                                                                                                                                                                        <%}
                                                                                                                                                                                                        } else {%>
                                                                                                                                                                                                        <a href="#"  class="btn btn-info" onclick="geoBillReportTXT('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                                                                                                        <%}%>
                                                                                                                                                                                                        <!--<a href="#" class="btn btn-info" onclick="tokenFailureReportTXT()" >-->
                                                                                                                                                                                                        <i class="icon-white icon-chevron-down"></i> TXT</a>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        </div>


                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <!--                                                                                                                                                                            <div class="control-group">                        
                                                                                                                                                                                                        <div class="span1">
                                                                                                                                                                                                        <div class="control-group form-inline">
                                                                                                                                                                                                        <%
                                                                                                                                                                                                        //Operators oprObjI = (Operators) session.getAttribute("_apOprDetail");
                                                                                                                                                                                                            // AccessMatrixSettings accessObjN = (AccessMatrixSettings) session.getAttribute("_apAccessEntry");
                                                                                                                                                                                                            if (oprObjI.getRoleid() != 1) {//1 sysadmin

                                                                                                                                                                                                                if (accessObjN.downloadtranscationBaseBillingReport == true) {%>
                                                                                                                                                                                                        <a href="#"  class="btn btn-info" onclick="geoBillReportCSV('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                                                                                                        <%} else {%>
                                                                                                                                                                                                        <a href="#"  class="btn btn-info" onclick="InvalidRequestBillReport('<%= strType1%>')" >
                                                                                                                                                                                                        <%}
                                                                                                                                                                                                        } else {%>
                                                                                                                                                                                                        <a href="#"  class="btn btn-info" onclick="geoBillReportCSV('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                                                                                                        <%}%>
                                                                                                                                                                                                        <a href="#"  class="btn btn-info" onclick="tokenFailureReportCSV()" >
                                                                                                                                                                                                        <i class="icon-white icon-chevron-down"></i> CSV</a>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="span1">
                                                                                                                                                                                                        <div class="control-group form-inline">
                                                                                                                                                                                                        <%if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                                                                                                                                if (accessObjN.downloadtranscationBaseBillingReport == true) {%>
                                                                                                                                                                                                        <a href="#"  class="btn btn-info" onclick="geoBillReportPDF('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                                                                                                        <%} else {%>
                                                                                                                                                                                                        <a href="#"  class="btn btn-info" onclick="InvalidRequestBillReport('<%= strType1%>')" >
                                                                                                                                                                                                        <%}
                                                                                                                                                                                                        %>
                                                                                                                                                                                                        <a href="#"  class="btn btn-info" onclick="geoBillReportPDF('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                                                                                                        <%}%>
                                                                                                                                                                                                        <a href="#" class="btn btn-info" onclick="tokenFailureReportPDF()" >
                                                                                                                                                                                                        <i class="icon-white icon-chevron-down"></i> PDF</a>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div class="span1">
                                                                                                                                                                                                        <div class="control-group form-inline">
                                                                                                                                                                                                        <%if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                                                                                                                                if (accessObjN.downloadtranscationBaseBillingReport == true) {%>
                                                                                                                                                                                                        <a href="#"  class="btn btn-info" onclick="geoBillReportTXT('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                                                                                                        <%} else {%>
                                                                                                                                                                                                        <a href="#"  class="btn btn-info" onclick="InvalidRequestBillReport('<%= strType1%>')" >
                                                                                                                                                                                                        <%}
                                                                                                                                                                                                        } else {%>
                                                                                                                                                                                                        <a href="#"  class="btn btn-info" onclick="geoBillReportTXT('<%=itype%>', '<%=_reportType%>')" >
                                                                                                                                                                                                        <%}%>
                                                                                                                                                                                                        <a href="#" class="btn btn-info" onclick="tokenFailureReportTXT()" >
                                                                                                                                                                                                        <i class="icon-white icon-chevron-down"></i> TXT</a>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        </div>
                                                
                                                
                                                                                                                                                                                                        </div>-->
                                                                                                                                                                                                        <%}%>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        </div> 

                                                                                                                                                                                                        <script language="javascript">
//    TransactionMapHoneyTrap(<%=1%>);
                                                                                                                                                                                                        </script>
                                                                                                                                                                                                        <%--<%@include file="footer.jsp" %>--%>