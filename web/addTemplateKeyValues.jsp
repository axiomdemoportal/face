<!DOCTYPE html>
<html lang="en">
    <%@page import="java.awt.image.BufferedImage"%>
    <%@page import="java.io.ByteArrayOutputStream"%>
    <%@page import="java.io.File"%>"
    <%@page import="javax.imageio.ImageIO"%> 
    <%@page import="java.io.FileReader"%>
    <%@page import="java.io.BufferedReader"%>
    <%@page import="java.nio.channels.FileChannel"%>
    <%@page import="java.io.FileInputStream"%>
    <%@page import="org.apache.commons.io.FileUtils"%>
    <%@page import="java.io.File"%>
    <%@page import="org.json.JSONObject"%>
    <%@page import="java.io.ByteArrayInputStream"%>
    <%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
    <%@page import="com.mollatech.axiom.nucleus.db.DocumentTemplate"%>
    <%@page import="com.mollatech.axiom.nucleus.db.connector.management.DocsTemplatesManagement"%>
    <%@page import="java.io.IOException"%>
    <%@page import="java.io.FileOutputStream"%>
    <%@page import="java.io.OutputStreamWriter"%>
    <%@page import="java.io.BufferedWriter"%>
    <%@page import="java.io.Writer"%>
    <%@page import="org.apache.commons.io.IOUtils"%>
    <%@page import="java.io.InputStream"%>
    <%@page import="com.mollatech.axiom.ocr.ResizeImage"%>
    <%@page import="com.mollatech.axiom.document.PreprocessImage"%>
    <%@page import="org.bouncycastle.util.encoders.Base64"%>
    <link href="assets/dococr/CSSJS/bootstrap.css" rel="stylesheet" type="text/css"/>
    <script src="assets/dococr/CSSJS/Js/bootstrap.js" type="text/javascript"></script>
    <script src="assets/dococr/CSSJS/Js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="assets/dococr/CSSJS/Js/mdb.js" type="text/javascript"></script>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <script src="assets/dococr/js/jquery.min.js" type="text/javascript"></script>
    <script src="assets/dococr/js/jquery.Jcrop.js" type="text/javascript"></script>
    <script src="assets/dococr/js/documentsTemplate.js" type="text/javascript"></script>
    <link href="assets/dococr/css/demo_files/main.css" rel="stylesheet" type="text/css"/>
    <link href="assets/dococr/css/demo_files/demos.css" rel="stylesheet" type="text/css"/>
    <link href="assets/dococr/css/jquery.Jcrop.css" rel="stylesheet" type="text/css"/>
    <%
        String templatename = request.getParameter("fileName");
        String encodedImage = (String) request.getSession().getAttribute("preprocessImage");
        ////////PreprocessImage///////////////////////  
        byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(encodedImage);
        BufferedImage img = ImageIO.read(new ByteArrayInputStream(imageBytes));
        request.getSession().setAttribute("_finalImage", imageBytes);
        System.out.println("encodedImage == " + encodedImage);
        String width = Integer.toString(img.getWidth());
        String height = Integer.toString(img.getHeight());
        System.out.println("Width: " + width);
        System.out.println("Height: " + height);
    %>
    <div class="card text-center">
        <script type="text/javascript">
            jQuery(function ($) {
                var jcrop_api;
                $('#target').Jcrop({
                    onChange: showCoords,
                    onSelect: showCoords,
                    onRelease: clearCoords
                }, function () {
                    jcrop_api = this;
                });
                $('#coords').on('change', 'input', function (e) {
                    var x1 = $('#x1').val(),
                            x2 = $('#x2').val(),
                            y1 = $('#y1').val(),
                            y2 = $('#y2').val();
                    jcrop_api.setSelect([x1, y1, x2, y2]);
                });

            });
            function showCoords(c)
            {
                $('#x1').val(c.x);
                $('#y1').val(c.y);
                $('#x2').val(c.x2);
                $('#y2').val(c.y2);
                $('#w').val(c.w);
                $('#h').val(c.h);
            };
            function clearCoords()
            {
                $('#coords input').val('');
            };
        </script>
        <div class="card-header">
            <!--Tabs-->
            <form id="coords"
                  class="coords"
                  onsubmit="return false;"
                  action="http://example.com/post.php">
                <label  style="color: blue">Select Key Co-ordinates and Template Name</label>
                <div class="inline-labels">
<!--                    <input type="hidden" id="widthpara" name="widthpara" value="<%=width%>">
                    <input type="hidden" id="heightpara" name="heightpara" value="<%=height%>">-->
                    <label>Enter Template name :</label>
                    <input size="10" id="templateDocsName" name="templateDocsName" style="height: 30px !important" type="text" placeholder="Enter Template Name"/> 
                    <label>Field name :</label>
                    <input size="10" id="fieldname" name="fieldname" style="height: 30px !important" type="text" placeholder="Enter field name"/> 
                    <label>X1 <input type="text" size="4" id="x1" name="x1" style="width: 50px;height: 30px !important"/></label>
                    <label>Y1 <input type="text" size="4" id="y1" name="y1" style="width: 50px;height: 30px !important"/></label>
                    <label>X2 <input type="text" size="4" id="x2" name="x2" style="width: 50px;height: 30px !important"/></label>
                    <label>Y2 <input type="text" size="4" id="y2" name="y2" style="width: 50px;height: 30px !important"/></label>
                    <label>W <input type="text" size="4" id="w" name="w" style="width: 50px;height: 30px !important"/></label>
                    <label>H <input type="text" size="4" id="h" name="h" style="width: 50px;height: 30px !important"/></label>
                    <input type="hidden" id="templatename" name="templatename" value="<%=templatename%>">
                    <button type="button" onclick="saveKeyocrPoint('<%=templatename%>')" class="btn btn-success" style="vertical-align: super !important">Save</button>
                    <a href="./home.jsp"><button type="button" class="btn btn-danger" style="vertical-align: super !important">Cancel</button></a>
                </div>
            </form>
        </div>
        <div class="card-body">
            <div div class="jc-demo-box col-md-12" style="overflow-y: auto;height:450px;border: black;">
                <div class="span12" >
                    <img  style="width: '<%=width%>px';height: '<%=height%>px'"  src="data:image/png;base64,<%=encodedImage%>" id="target" alt="[Jcrop Example]" />
                </div>
            </div>
        </div>
    </div>