<%@include file="header.jsp" %>
<script src="./assets/js/otptokens.js"></script>

<%
    SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm ");
    java.util.Date dLA = new java.util.Date();

%>
<div class="container-fluid">
    <h2>OTP Token Management</h2>
    <p>List of users and their OTP token management.</p>
    <p>
        <br>
    <div class="input-append">
       <form id="searchUserForm" name="searchUserForm">
               <input type="text" id="_keyword" name="_keyword" placeholder="search Users using name, phone or email" class="span4"><span class="add-on"><i class="icon-search"></i></span>
               <a href="#" class="btn btn-success" onclick="searchOtpUsers()">Search User(s)</a>
        </form>
    </div>
    <div id="users_table_main">
    </div>
</p>

<div id="ResyncSoftware" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Resync OTP Token</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="ResyncSoftwareform">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_userIDS" name="_userIDS" >
                    <div class="control-group">
                        <label class="control-label"  for="username">First OTP</label>
                        <div class="controls">
                            <input type="text" id="_FirstOtp" name="_FirstOtp" placeholder="2210D03840000A" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Second OTP</label>
                        <div class="controls">
                            <input type="text" id="_SecondOtp" name="_SecondOtp" placeholder="2210D03840000A" class="input-xlarge">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="ResyncSoftware-result"></div>
        <button class="btn btn-primary" onclick="resyncsoftware()" id="buttonResyncSoftware" type="button">Submit</button>
    </div>
</div>

<div id="HardwareRegistration" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idEditPushMaperName"></div></h3>
        <h3 id="myModalLabel">Assign Hardware Token</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="HardwareRegistrationform">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_userIDHR" name="_userIDHR" >
                    <div class="control-group">
                        <label class="control-label"  for="username">Serial Number</label>
                        <div class="controls">
                            <input type="text" id="_registrationCode" name="_registrationCode" placeholder="2210D03840000A" class="input-xlarge">
                        </div>
                    </div>
               </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="HardwareRegistration-result"></div>
        <button class="btn btn-primary" onclick="assignhardwaretoken()" id="buttonHardwareRegistration" type="button">Submit</button>
    </div>
</div>        

<script language="javascript">
    //listChannels();
</script>

<%@include file="footer.jsp" %>