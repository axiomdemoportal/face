<%@include file="header.jsp" %>
<script src="./assets/js/otptokens.js"></script>

<%
    SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm ");
    java.util.Date dLA = new java.util.Date();

%>
<div class="container-fluid">
    <div id="auditTable">
        <h1 class="text-success">OTP Token Management</h1>
        <p>You can manage users and their associated One Time Password tokens in the system.</p>
        <h3>Search Users</h3>   
        <div class="input-append">
            <form id="searchUserForm" name="searchUserForm">
                <input type="text" id="_keyword" name="_keyword" placeholder="Search using name, phone or email" class="span4"><span class="add-on"><i class="icon-search"></i></span>
                <a href="#" class="btn btn-success" onclick="searchOtpUsers()">Search Now</a>
            </form>
        </div>
        <div id="users_table_main">
        </div>
        </p>

        <p>To Check Hardware Token Secret Validity <a href="#CheckSecret" class="btn" data-toggle="modal">Click Here >> </a></p>
    </div>
    <div id="CheckSecret" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3 id="myModalLabel">Hardware Token Secret Validity</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="HWTOKENSECRETVALIDITY">
                    <fieldset>
                        <!-- Name -->
                        <div class="control-group">
                            <label class="control-label"  for="username">Serial Number</label>
                            <div class="controls">
                                <input type="text" id="_serialno" name="_serialno" placeholder="2345xxxxx" class="input-xlarge">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <div id="check-secret-result"></div>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button class="btn btn-primary" onclick="CheckSecretHWToken()" id="checkSecretButton">Check Now >></button>
        </div>
    </div>

    <div id="ResyncSoftware" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3 id="myModalLabel">Resync Software Token</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="ResyncSoftwareform">
                    <fieldset>
                        <!-- Name -->
                        <input type="hidden" id="_userIDS" name="_userIDS" >
                        <input type="hidden" id="_categoryS" name="_categoryS" >
                        <input type="hidden" id="_srno" name="_srno" >
                        <div class="control-group">
                            <label class="control-label"  for="username">First OTP</label>
                            <div class="controls">
                                <input type="text" id="_FirstOtp" name="_FirstOtp" placeholder="2210D03840000A" class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Second OTP</label>
                            <div class="controls">
                                <input type="text" id="_SecondOtp" name="_SecondOtp" placeholder="2210D03840000A" class="input-xlarge">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <div id="ResyncSoftware-result"></div>
            <button class="btn btn-primary" onclick="resyncsoftware()" id="buttonResyncSoftware" type="button">Submit</button>
        </div>
    </div>


    <div id="ResyncHardware" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3 id="myModalLabel">Resync Hardware Token</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="ResyncHardwareform">
                    <fieldset>
                        <!-- Name -->
                        <input type="hidden" id="_userIDHW" name="_userIDHW" >
                        <input type="hidden" id="_categoryHW" name="_categoryHW" value="2">
                        <input type="hidden" id="_srnoHW" name="_srnoHW" >
                        <div class="control-group">
                            <label class="control-label"  for="username">First OTP</label>
                            <div class="controls">
                                <input type="text" id="_FirstOtp" name="_FirstOtp" placeholder="2210D03840000A" class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Second OTP</label>
                            <div class="controls">
                                <input type="text" id="_SecondOtp" name="_SecondOtp" placeholder="2210D03840000A" class="input-xlarge">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <div id="ResyncHardware-result"></div>
            <button class="btn btn-primary" onclick="resynchardware()" id="buttonResyncHardware" type="button">Submit</button>
        </div>
    </div>

    <div id="HardRegistration" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3><div id="idEditPushMaperName"></div></h3>
            <h3 id="myModalLabel">Assign Hardware Token</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="HardwareRegistrationform">
                    <fieldset>
                        <!-- Name -->
                        <!--<input type="hidden" id="_userIDHR" name="_userIDHR" >-->
                        <input type="hidden" id="_userid" name="_userid" >
                        <input type="hidden" id="_category" name="_category" >
                        <input type="hidden" id="_subcategory" name="_subcategory" >
                        <div class="control-group">
                            <label class="control-label"  for="username">Serial Number</label>
                            <div class="controls">
                                <input type="text" id="_serialnumber" name="_serialnumber" placeholder="e.g 123231212" class="input-xlarge">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <div id="HardwareRegistration-result"></div>
            <button class="btn btn-primary" onclick="assignhardwaretoken()" id="buttonHardwareRegistration" type="button">Submit</button>
        </div>
    </div>        

    <script language="javascript">
        //listChannels();
    </script>

    <div id="verifyOTP" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3 id="myModalLabel">Verify One Time Password</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="verifyOTPForm" name="verifyOTPForm">
                    <fieldset>
                        <input type="hidden" id="_userIDOV" name="_userIDOV" >
                        <input type="hidden" id="_usertypeIDS" name="_usertypeIDS">
                        <div class="control-group">
                            <label class="control-label"  for="username">Enter One Time Password to Verify:</label>
                            <div class="controls">
                                <input type="text" id="_oobotp" name="_oobotp" placeholder="Enter OTP" class="input-medium">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <div class="span3" id="verify-otp-result"></div>
            <button class="btn btn-primary" onclick="verifyOTP()" id="addnewOperatorSubmitBut">Verify OTP</button>
        </div>
    </div>

    <div id="ReplaceHardware" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3 id="myModalLabel">Replace Hardware Token</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="ReplaceHardwareform">
                    <fieldset>
                        <!-- Name -->
                        <input type="hidden" id="_userIDReplaceHW" name="_userIDReplaceHW" >
                        <input type="hidden" id="_categoryReplaceHW" name="_categoryReplaceHW" >
                        <input type="hidden" id="_subcategoryReplaceHW" name="_subcategoryReplaceHW" >
                        <!--<input type="hidden" id="_srnoHW" name="_srnoHW" >-->
                        <div class="control-group">
                            <label class="control-label"  for="username">Old Token Serial No</label>
                            <div class="controls">
                                <input type="text" id="_oldTokenSerialNo" name="_oldTokenSerialNo" placeholder="2210D03840000A" class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Old Token Marked As</label>
                            <div class="controls">
                                <select name="_oldTokenStatus" id="_oldTokenStatus" class="span3">
                                    <option value="-5">Lost</option>                                             
                                    <option value="-10">Free</option>
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">New Token Serial No</label>
                            <div class="controls">
                                <input type="text" id="_newTokenSerialNo" name="_newTokenSerialNo" placeholder="2210D03840000A" class="input-xlarge">
                            </div>
                        </div>

                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <div id="ReplaceHardware-result"></div>
            <button class="btn btn-primary" onclick="replacehardware()" id="buttonResyncHardware" type="button">Submit</button>
        </div>
    </div>

    <div id="userTokenauditDownload" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3><div id="idauditDownload"></div></h3>
            <h3 id="myModalLabel">Download Audit</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="testSMSPrimaryForm" name="testSMSPrimaryForm">
                    <fieldset>
                        <!-- Name -->
                        <input type="hidden" id="_auditUserID" name="_auditUserID"/>
                        <input type="hidden" id="_auditUserName" name="_auditUserName"/>
                        <div class="control-group">
                            <label class="control-label"  for="username">Start Date</label>
                            <div class="controls" align="left" >

                                <!--<span class="add-on">From:</span>-->   
                                <div id="Pushdatetimepicker1" class="input-append date">
                                    <input id="_auditStartDate" name="_auditStartDate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">End Date</label>
                            <div class="controls" align="left">
                                <!--<span class="add-on">Till:</span>-->   
                                <div id="Pushdatetimepicker2" class="input-append date">
                                    <input id="_auditEndDate" name="_auditEndDate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>                    
                    </fieldset>
                </form>
            </div>
        </div>

        <div class="modal-footer">
            <div id="editoperator-result"></div>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button class="btn btn-primary" onclick="searchUserTokenAudit()" id="buttonEditOperatorSubmit">Show Audit</button>
        </div>
    </div>
    <script>
        $(function () {
            $('#Pushdatetimepicker1').datepicker({
                format: 'dd/MM/yyyy',
                language: 'pt-BR'
            });
        });
        $(function () {
            $('#Pushdatetimepicker2').datepicker({
                format: 'dd/MM/yyyy',
                language: 'pt-BR'
            });
        });
    </script>

    <%@include file="footer.jsp" %>