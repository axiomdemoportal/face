<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.axiom.nucleus.db.Usergroups"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserGroupsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Webresource"%>
<%@include file="header.jsp" %>
<script src="./assets/js/ajaxfileupload.js"></script>
<script src="./assets/js/bootstrap-fileupload.js"></script> 
<link href="./assets/css/bootstrap.css" rel="stylesheet">
<link href="./assets/css/bootstrap-fileupload.css" rel="stylesheet">
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<script src="./assets/js/usermanagement.js"></script>
<%    String _sessionID = (String) session.getAttribute("_apSessionID");
    SessionManagement smObj = new SessionManagement();
    int iStatus = smObj.GetSessionStatus(_sessionID);
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    OperatorsManagement oMngt = new OperatorsManagement();
%>
<div class="container-fluid" >
    <h1 class="text-success">Edit Web Resource</h1>
    <div class="row-fluid">
        <form class="form-horizontal" id="editResourceForm">
            <fieldset>
                <%
                    Webresource res = (Webresource) session.getAttribute("_res");
                    JSONObject json = new JSONObject(res.getSsofields());
                %>
                <!-- Name -->
                <input type="hidden" value="<%=res.getResourceid()%>" id="_resid" name="_resid">
                <div class="control-group">
                    <label class="control-label"  for="resourcename">Name</label>
                    <div class="controls">
                        <input type="text" id="_Name" name="_Name" value="<%=res.getResourcename()%>" placeholder="resource name" class="input-xlarge">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"  for="weburl">URL</label>
                    <div class="controls">
                        <input type="text" id="_Weburl" name="_Weburl" value="<%=res.getWeburl()%>" placeholder="Website URL" class="input-xlarge">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"  for="status">Status</label>
                    <div class="controls"  value="<%=res.getStatus()%>" class="input-large">                               
                        <select name="_Status" id="_Status">
                            <option value="active">Active</option>
                            <option value="Suspended">Suspended</option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"  for="username">Select File:</label>                                    
                    <div class="controls fileupload fileupload-new" data-provides="fileupload">
                        <div class="input-append">
                            <div class="uneditable-input span4"><i class="icon-file fileupload-exists"></i> 
                                <span class="fileupload-preview"></span>
                            </div>
                            <span class="btn btn-file" >
                                <span class="fileupload-new">Select file</span>
                                <span class="fileupload-exists">Change</span>
                                <input type="file" id="fileImageToUpload" name="fileImageToUpload" />
                            </span>
                            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                            <a href="#"class="btn btn-success" id="buttonUploadImage"  onclick="UploadImageFile()" type="button">Upload Now</a>   
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"  for="group">Group</label>
                    <div class="controls"  class="input-large">                               
                        <select name="_groupName" id="_groupName"  multiple class="span3">
                            <%UserGroupsManagement ugmngt = new UserGroupsManagement();
                                Usergroups[] ugroup = null;
                                ugroup = ugmngt.ListGroups(sessionid, channel.getChannelid());
                                String str[] = res.getGroups().split(",");
                                for (int i = 0; i < ugroup.length; i++) {
                                    for (int j = 0; j < str.length; j++) {
                                        if (ugroup[i].getGroupname().equals(str[j])) {%>
                            <option value="<%=ugroup[i].getGroupname()%>" selected><%=ugroup[i].getGroupname()%></option>
                            <%
                                        break;
                                    }
                                }

                            %>
                            <option value="<%=ugroup[i].getGroupname()%>"><%=ugroup[i].getGroupname()%></option>
                            <%}%>
                        </select>
                    </div>
                </div>
                <table class="table table-striped" id="table_main">
                    <tr>
                        <th>Field 1</th>
                        <th>Field 2</th>
                        <th>Field 3</th>
                        <th>Field 4</th>
                        <th>Field 5</th>
                        <th>Field 6</th>
                        <th>Field 7</th>
                        <th>Field 8</th>
                        <th>Field 9</th>
                        <th>Field 10</th>
                    </tr>
                    <tr>
                        <td><input type="text" id="_field1" name="_field1" placeholder="key" <%if (json != null) {
                                if (json.get("_field1") != null) {%>value="<%=json.get("_field1")%>"<%}
                            }%> class="span10"></td>
                        <td><input type="text" id="_field2" name="_field2" placeholder="key" <%if (json != null) {
                                if (json.get("_field1") != null) {%>value="<%=json.get("_field2")%>"<%}
                            }%> class="span10"></td>
                        <td><input type="text" id="_field3" name="_field3" placeholder="key" <%if (json != null) {
                                if (json.get("_field1") != null) {%>value="<%=json.get("_field3")%>"<%}
                            }%> class="span10"></td>
                        <td><input type="text" id="_field4" name="_field4" placeholder="key" <%if (json != null) {
                                if (json.get("_field1") != null) {%>value="<%=json.get("_field4")%>"<%}
                            }%> class="span10"></td>
                        <td><input type="text" id="_field5" name="_field5" placeholder="key" <%if (json != null) {
                                if (json.get("_field1") != null) {%>value="<%=json.get("_field5")%>"<%}
                            }%> class="span10"></td>
                        <td><input type="text" id="_field6" name="_field6" placeholder="key" <%if (json != null) {
                                if (json.get("_field1") != null) {%>value="<%=json.get("_field6")%>"<%}
                            }%> class="span10"></td>
                        <td><input type="text" id="_field7" name="_field7" placeholder="key" <%if (json != null) {
                                if (json.get("_field1") != null) {%>value="<%=json.get("_field7")%>"<%}
                            }%> class="span10"></td>
                        <td><input type="text" id="_field8" name="_field8" placeholder="key" <%if (json != null) {
                                if (json.get("_field1") != null) {%>value="<%=json.get("_field8")%>"<%}
                            }%> class="span10"></td>
                        <td><input type="text" id="_field9" name="_field9" placeholder="key" <%if (json != null) {
                                if (json.get("_field1") != null) {%>value="<%=json.get("_field9")%>"<%}
                            }%> class="span10"></td>
                        <td><input type="text" id="_field10" name="_field10" placeholder="key" <%if (json != null) {
                                if (json.get("_field1") != null) {%>value="<%=json.get("_field10")%>"<%}
                            }%> class="span10"></td>
                    </tr>
                </table>
            </fieldset>
        </form>
        <button class="btn btn-primary" onclick="editresource()" id="editResourceButton">Save Resource</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true" onclick="editback()">Back</button>           
    </div>
    <!--<div id="addResource-result"></div>-->
</div>
</div>
<script>
    $(document).ready(function() {
        $("#_groupName").select2();
    });
</script>

<!--edit details-->
<!--</div>-->
<%@include file="footer.jsp" %>