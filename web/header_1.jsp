<%@page import="com.mollatech.axiom.connector.access.controller.AccessMatrixSettings"%>
<%@page import="java.util.Properties"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.PropsFileUtil"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.ecopin.management.EPINManagement"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.LoadSettings"%>
<%@ page import="com.mollatech.axiom.nucleus.db.Operators" %>
<%@ page import="com.mollatech.axiom.nucleus.db.Channels" %>
<%@ page import="com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement" %>
<%@ page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement" %>
<%@ page import="com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement" %>
<%@ page import="com.mollatech.axiom.nucleus.db.connector.management.RemoteAccessManagement" %>
<%@ page import="com.mollatech.axiom.nucleus.db.operation.AxiomChannel" %>
<%@ page import="com.mollatech.axiom.nucleus.db.operation.AxiomOperator" %>
<%@ page import="com.mollatech.axiom.nucleus.db.Accesses" %>
<%@ page import="com.mollatech.axiom.nucleus.db.Roles" %>
<%@ page import="org.hibernate.Session" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%
    String strContextPath = this.getServletContext().getContextPath();
    String status = (String) session.getAttribute("_apOprAuth");
    if (status == null || status.compareTo("yes") != 0) {
//        String nextJSP = "./index.jsp";
        String nextJSP = "/index.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
        dispatcher.forward(request, response);
        return;
    }
    SimpleDateFormat tzTimeHeader = new SimpleDateFormat("EEE dd/MMM/yy-HH:mm");
    String completeTimeHeader = tzTimeHeader.format(new Date());

    Operators oprObj = (Operators) session.getAttribute("_apOprDetail");
    String name = oprObj.getName();
    AccessMatrixSettings accessObj = (AccessMatrixSettings) session.getAttribute("_apAccessEntry");
    OperatorsManagement oprmngObj = new OperatorsManagement();
    Roles[] roles = oprmngObj.getAllRoles(oprObj.getChannelid());

    String strProductType = LoadSettings.g_sSettings.getProperty("product.type");

    int iPRODUCT = (new Integer(strProductType)).intValue();
    //int DICTUM = 1;
    //int ECOPIN = 2;
    int AXIOM = 3;
    // iPRODUCT = 2;

    String strOperatorRole = (String) session.getAttribute("_apOprRole");
    int iOperatorRole = Integer.valueOf(strOperatorRole).intValue();

    EPINManagement epinmngObj = new EPINManagement();
    String sessionid = (String) session.getAttribute("_apSessionID");
    int iFailedRequests = epinmngObj.GetCountOfEpintracker(sessionid, oprObj.getChannelid(), -3); //failed
    int iPendingRequests = epinmngObj.GetCountOfEpintracker(sessionid, oprObj.getChannelid(), 2); //pending

    String FailedRequests = "";
    String PendingRequests = "";

    if (iFailedRequests != 0) {
        FailedRequests = "<span class=\"label label-warning\">" + iFailedRequests + "</span>";
    }

    if (iPendingRequests != 0) {
        PendingRequests = "<span class=\"label label-warning\">" + iPendingRequests + "</span>";
    }

    //nilesh  option enable disable 16-3-15
//    String sep = System.getProperty("file.separator");
//    String usrhome = System.getProperty("catalina.home");
//    if (usrhome == null) {
//        usrhome = System.getenv("catalina.home");
//    }
    Properties navigationSettings = null;
//    if (usrhome != null) {
//        usrhome += sep + "axiomv2-settings" + sep + "EnableOptions.conf";
//        PropsFileUtil p1 = new PropsFileUtil();
//        if (p1.LoadFile(usrhome) == true) {
//            navigationSettings = p1.properties;
//        }
//    }
    //nilesh

    //added by supriya
//System.out.println("hi nilesh in header!!!");
%>
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta name="google-translate-customization" content="26e4d928eae0659b-61f8bcc48ec8af9a-g74ee54aac640948a-11"></meta>
        <title><%=strContextPath%> Management Portal For <%=name%></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="./assets/css/bootstrap.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 60px;
                padding-bottom: 40px;
            }
        </style>
        <link href="./assets/css/bootstrap-responsive.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="../assets/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="./assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="./assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="./assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="./assets/ico/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="./assets/ico/favicon.png">
        <link rel="stylesheet" href="./assets/css/datepicker.css">
        <link rel="stylesheet" href="./assets/css/jquery.sidr.dark.css">

        <script src="./assets/js/jquery.js"></script>
        <script src="./assets/js/bootstrap-transition.js"></script>
        <script src="./assets/js/bootstrap-alert.js"></script>
        <script src="./assets/js/bootstrap-modal.js"></script>
        <script src="./assets/js/bootstrap-dropdown.js"></script>
        <script src="./assets/js/bootstrap-scrollspy.js"></script>
        <script src="./assets/js/bootstrap-tab.js"></script>
        <script src="./assets/js/bootstrap-tooltip.js"></script>
        <script src="./assets/js/bootstrap-popover.js"></script>
        <script src="./assets/js/bootstrap-button.js"></script>
        <script src="./assets/js/bootstrap-collapse.js"></script>
        <script src="./assets/js/bootstrap-carousel.js"></script>
        <script src="./assets/js/bootstrap-typeahead.js"></script>
        <script src="./assets/js/bootstrap-datepicker.js"></script>


        <script src="./assets/js/json_sans_eval.js"></script>
        <script src="./assets/js/bootbox.min.js"></script>
        <script src="./assets/js/channels.js"></script>
        <script src="./assets/js/operators.js"></script>

        <script src="./assets/js/jquery.sidr.min.js"></script>


    </head>

    <body>
        <%if (oprObj.getRoleid() != 1 && accessObj != null) {//not sysadmin%>
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="brand" href="home.jsp"><%=strContextPath%></a>
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <% if (accessObj.listChannel == true || accessObj.listOperator == true
                                        || accessObj.listUnits == true || accessObj.ListUsers == true
                                        || accessObj.ListUsersGroup == true || accessObj.listTemplate == true) {%> 
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Channels Management<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <% if (accessObj.listChannel == true) {%>  
                                    <li><a href="channels.jsp"><i class="icon-file"></i>Channels</a></li>                                    
                                    <!--<li class="divider"></li>-->
                                    <%}%>
                                    <% if (accessObj.listOperator == true) {%>  
                                    <li><a href="operatorsmain.jsp"><i class="icon-file"></i>Operators</a></li>
                                    <!--<li class="divider"></li>-->
                                    <%}%>
                                    <% if (accessObj.listUnits == true) {%>  
                                    <li><a href="UnitsList.jsp"><i class="icon-file"></i>Units</a></li>
                                    <!--<li class="divider"></li>-->
                                    <%}%>
                                    <% if (accessObj.ListUsersGroup == true) {%>  
                                    <li><a href="UsersGroup.jsp"><i class="icon-file"></i>Users Group</a></li>
                                        <%}%>
                                        <%if (accessObj.listTemplate == true) {%>
                                    <!--<li class="divider"></li>-->
                                    <li class="dropdown-submenu">
                                        <a tabindex="-1" href="#"><i class="icon-file"></i>Message Templates</a>
                                        <ul class="dropdown-menu">
                                            <%if (accessObj.addTemplate == true) {%>
                                            <li><a href="TemplateNew.jsp"><i class="icon-file"></i>Add New Template</a></li>                                    
                                            <li class="divider"></li>
                                                <%}%>
                                            <li><a href="TemplateMobileList.jsp"><i class="icon-file"></i>Mobile Messages</a></li>                                    
                                            <li><a href="TemplateEmailList.jsp"><i class="icon-file"></i>Email Messages</a></li>
                                        </ul>                                            
                                    </li>
                                    <li><a href="ErrorMessageList.jsp"><i class="icon-file"></i>Error Messages</a></li>
                                        <%}%>
                                </ul>
                            </li>
                            <% }%>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Maker/Checker<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <%if (oprObj.getOperatorType() == 2) {//2 == authorizer%> 
                                    <li><a  href="searchArchiveList.jsp">Authorizer Archive Requests</a></li> 
                                    <li><a href="searchPendingList.jsp"> Authorizer Pending Requests</a></li>
                                        <%}else{%>
                                    <li><a href="requesterOperators.jsp">Requester Operators</a></li>
                                    <li><a   href="searchArchiveListRequester.jsp">Requester Archived Requests</a></li>
                                    <li><a   href="PendingListForRequster.jsp">Requester Pending Requests</a></li>
                                        <%}%>
                                </ul>
                            </li>

                            <% if( 1 == 2){
                                if (accessObj.authorizerArchiveRequest == true || accessObj.authorizerPendingRequest == true
                                        || accessObj.requesterArchiveRequest == true || accessObj.requesterPendingRequest == true
                                        || accessObj.requesterOperatorList == true) {%> 
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Maker/Checker<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <% //if (oprObj.getOperatorType() == 2) {//authorizer
                                        if (accessObj.authorizerArchiveRequest == true) {%>
                                    <li><a  href="searchArchiveList.jsp">Authorizer Archive Requests</a></li> 
                                        <%}%>
                                        <%if (accessObj.authorizerPendingRequest == true) {%>
                                    <li><a href="searchPendingList.jsp"> Authorizer Pending Requests</a></li>
                                        <%}%>
                                        <%if (accessObj.requesterOperatorList == true) {%>
                                    <li><a href="requesterOperators.jsp">Requester Operators</a></li>
                                        <%}%>
                                        <%if (accessObj.requesterArchiveRequest == true) {%>
                                    <li><a   href="searchArchiveListRequester.jsp">Requester Archived Requests</a></li>
                                        <%}%>
                                        <%if (accessObj.requesterPendingRequest == true) {%>
                                    <li><a   href="PendingListForRequster.jsp">Requester Pending Requests</a></li>
                                        <%}
                                            //                                    }%>
                                </ul>
                            </li>
                            <%}}%>


                            <%if (accessObj.listSmsGateway == true || accessObj.listUSSDGateway || accessObj.listVOICEGateway == true || accessObj.listEMAILGateway == true
                                 || accessObj.listBillingManager == true || accessObj.listUserSourceSettings == true
                                        || accessObj.listPasswordPolicySettings == true || accessObj.listChannelProfileSettings == true
                                        || accessObj.listOtpTokensSettings == true || accessObj.listCertificateSettings == true
                                        || accessObj.listMobileTrustSettings == true || accessObj.listRadiusConfigSettings == true
                                        || accessObj.listImageSettings == true
                                
                                    
                                
                                
                                
                                    ){%>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Configuration<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <%  if (accessObj.listSmsGateway == true || accessObj.listUSSDGateway
                                                || accessObj.listVOICEGateway == true || accessObj.listEMAILGateway == true) {%>
                                    <li><a href="inventoryReports.jsp?_type=7"><i class="icon-file"></i>Gateway</a></li>
                                        <%}%>
                                        <% if (accessObj.listBillingManager == true || accessObj.listUserSourceSettings == true
                                                    || accessObj.listPasswordPolicySettings == true || accessObj.listChannelProfileSettings == true
                                                    || accessObj.listOtpTokensSettings == true || accessObj.listCertificateSettings == true
                                                    || accessObj.listMobileTrustSettings == true || accessObj.listRadiusConfigSettings == true
                                                    || accessObj.listImageSettings == true) {%>
                                    <li><a href="inventoryReports.jsp?_type=8"><i class="icon-file"></i>System Settings</a></li>
                                        <%}%>
                                </ul>
                            </li>
                            <%}%>

                            <%if (accessObj.ListUsers == true || accessObj.listImageManagement == true
                                        || accessObj.listOOBSMSToken == true
                                        || accessObj.listOOBEMAILToken == true || accessObj.listOOBVOICEToken == true
                                        || accessObj.listOOBUSSDToken == true || accessObj.listSWMOBILEToken == true
                                        || accessObj.listSWWEBToken == true || accessObj.listSWPCToken == true
                                        || accessObj.listHWOTPToken == true || accessObj.listCerticate || accessObj.listHWPKIToken == true
                                        || accessObj.listSWPKIToken == true || accessObj.listChallengeResponse == true
                                        || accessObj.listTrustedDevice == true || accessObj.listGeoLocation == true
                                        || accessObj.listWebWatch == true || accessObj.listWebResource == true) {%>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Security & User Management<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="searchResource.jsp"><i class="icon-user"></i>Resource Management</a></li>
                                        <%
                                            accessObj.listWebWatch = false;
                                            if (accessObj.listWebWatch == true) {%>
                                    <li><a href="listSettings.jsp"><i class="icon-user"></i>Web Watch</a></li>
                                        <%}%>
                                        <%
                                            accessObj.listWebResource = false;
                                            if (accessObj.listWebResource == true) {%>
                                    <li><a href="resources.jsp"><i class="icon-cog"></i>Web Resource</a></li>
                                        <%}
                                            accessObj.listSnmpManagement = false;
                                            if (accessObj.listSnmpManagement == true) {%>
                                    <li><a href="SNMPaddsettings.jsp"><i class="icon-cog"></i>SNMP Alerts</a></li>
                                        <%}

                                            accessObj.listESIGNERManagement = false;
                                            if (accessObj.listESIGNERManagement == true) {
                                        %>

                                    <li class="dropdown-submenu">
                                        <a tabindex="-1" href="#"><i class="icon-file"></i>E-PDF Signing</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="epdfsign.jsp"><i class="icon-file"></i>Sign Request</a></li>                                    
                                            <li><a href="signcategory.jsp"><i class="icon-file"></i>Document Category</a></li>
                                        </ul>
                                    </li>
                                    <%}%>

                                </ul>
                            </li>
                            <%}%>
                            <%if (accessObj.listUserReport == true || accessObj.listMessageReport == true
                                        || accessObj.listHoneyTrapReport == true || accessObj.listOtpReport == true
                                        || accessObj.listCertificateReport == true || accessObj.listPkiReport == true
                                        || accessObj.listEpinSystemReport == true || accessObj.listEpinUserSpecificReport == true
                                        || accessObj.listRemoteSigningReport == true || accessObj.listtranscationReport == true
                                        || accessObj.listOtpFailureReport == true || accessObj.listsubscriptionBaseBillingReport == true
                                        || accessObj.listtranscationBaseBillingReport == true || accessObj.listMessageReport == true
                                        || accessObj.listHoneyTrapReport == true) {%>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <%if (accessObj.listUserReport == true || accessObj.listMessageReport == true
                                                || accessObj.listHoneyTrapReport == true || accessObj.listOtpReport == true
                                                || accessObj.listCertificateReport == true || accessObj.listPkiReport == true
                                                || accessObj.listEpinSystemReport == true || accessObj.listEpinUserSpecificReport == true
                                                || accessObj.listRemoteSigningReport == true || accessObj.listtranscationReport == true
                                                || accessObj.downloadAuditTrail == true || accessObj.downloadSessionAuditTrail == true) {%>
                                    <li><a href="inventoryReports.jsp?_type=1"><i class="icon-file"></i>Inventory Reports</a></li>
                                    <li><a href="inventoryReports.jsp?_type=2"><i class="icon-file"></i>Usage Reports</a></li>
                                        <%}%>
                                        <%if (accessObj.listtranscationBaseBillingReport == true || accessObj.listsubscriptionBaseBillingReport == true) {%>
                                    <li><a href="inventoryReports.jsp?_type=3"><i class="icon-file"></i>Billing Reports</a></li>
                                        <%}%>
                                        <%if (accessObj.listMessageReport == true) {%>
                                    <li><a href="inventoryReports.jsp?_type=4"><i class="icon-file"></i>Message Reports</a></li>
                                        <%}%>
                                        <%if (accessObj.listHoneyTrapReport == true) {%>
                                    <!--<li><a href="inventoryReports.jsp?_type=5"><i class="icon-file"></i>Miscellaneous Reports</a></li>-->
                                    <%}%>
                                    <%if (accessObj.downloadAuditTrail == true || accessObj.downloadSessionAuditTrail == true) {%>
                                    <li><a href="inventoryReports.jsp?_type=6"><i class="icon-file"></i>Audit Reports</a></li>
                                        <%}%>
                                </ul>
                            </li>

                            <%}%>

                            <%if (accessObj.listSystemMessage == true || accessObj.listhwOTPTokenUpload == true
                                        || accessObj.listhwPKITokenUpload == true || accessObj.listChallengeResponse == true
                                        || accessObj.connectorStatus == true || accessObj.auditIntegrity == true
                                        || accessObj.listReportSchedulerManagement == true) {%>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <%if (accessObj.listSystemMessage == true) {%>
                                    <li><a href="ListSystemMessage.jsp"><i class="icon-cog"></i>System Alert(Operators)</a></li>
                                        <%}%>
                                        <%if (accessObj.listhwOTPTokenUpload == true || accessObj.listhwPKITokenUpload == true) {%>
                                    <li><a href="hwotpupload.jsp"><i class="icon-user"></i>Hardware Token Uploader</a></li>        
                                        <%}%>
                                        <%if (accessObj.listChallengeResponse == true) {%>
                                    <li><a href="QuestionsList.jsp"><i class="icon-file"></i>Challenge Response Questions List</a></li>
                                        <%}%>
                                        <%if (accessObj.connectorStatus == true) {%>
                                    <li><a href="connectorstatuslist.jsp" ><i class="icon-cog"></i>Connectors Status</a></li>
                                        <%}%>
                                        <%if (accessObj.auditIntegrity == true) {%>
                                    <li><a href="#CheckAuditIntegrity" data-toggle="modal"><i class="icon-cog"></i>Audit Integrity</a></li>
                                        <%}
                                            if (accessObj.listReportSchedulerManagement == true) {%>
                                    <li><a href="scheduler.jsp" data-toggle="modal"><i class="icon-cog"></i>Report Scheduler</a></li>
                                        <%}%>

                                </ul>
                            </li>
                            <%}%>
                            <%if (accessObj.listlicenseDetails == true || accessObj.changePassword == true
                                        || accessObj.changeDBPassword == true || accessObj.changeSecurityCredentials == true) {%>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">System Admin<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <!--<li><a href="addAccessRole.jsp"><i class="icon-cog"></i>Access Role</a></li>-->
                                    <%if (accessObj.listlicenseDetails == true) {%>
                                    <li><a href="showlicenseI.jsp"><i class="icon-user"></i>License Details</a></li>
                                        <%}%>
                                        <%if (accessObj.changePassword == true) {%>
                                    <li><a href="#ChangePassword" data-toggle="modal" ><i class="icon-cog"></i>Change Password</a></li>
                                        <%}%>
                                        <%if (accessObj.changeDBPassword == true) {%>
                                    <li><a href="initdbform.jsp" ><i class="icon-cog"></i>Change DB Details</a></li>
                                        <%}%> 
                                        <%if (accessObj.changeSecurityCredentials == true) {%>
                                    <li><a href="reinitialize.jsp"><i class="icon-user"></i>Change Security Credentials</a></li>
                                        <%}%>
                                </ul>
                            </li>
                            <%}%>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Requests<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="users.jsp"><i class="icon-user"></i>User & Password</a></li>
                                    <li><a href="ecopinpending.jsp">Pending Approvals <%=PendingRequests%></a></li>                                    
                                    <li><a href="ecopinfailed.jsp">Failed Deliveries <%=FailedRequests%></a></li>
                                </ul>
                            </li>




                        </ul>

                        <%if (navigationSettings == null) {
                                // if(g_subjecttemplateSettings.getProperty("axiom.change.password").equals("true")){%>
                        <div class="pull-right">
                            <ul class="nav pull-right">                                
                                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><%=completeTimeHeader%> Welcome, <%=name%><b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <%if (accessObj.downloadLogs == true) {%>
                                        <li><a href="#downloadzippedlog" data-toggle="modal"><i class="icon-file"></i>Download Logs</a></li>
                                            <%}%>
                                            <%if (accessObj.downloadCleanupLogs == true) {%>
                                        <li><a href="#" onclick="DownloadCleanupLog()"><i class="icon-file"></i>Download Clean-up Logs</a></li>
                                            <%}%>

                                        <li class="divider"></li>
                                        <li><a href="signout.jsp"><i class="icon-off"></i>Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <%}%>

                    </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <%} else {%>
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="brand" href="home.jsp"><%=strContextPath%></a>

                    <!--                    <a class="brand" id="ansn" onClick="window.print()"><i class="icon-print"></i></a>-->

                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <%
                                if (iOperatorRole == OperatorsManagement.Approvar || iOperatorRole == OperatorsManagement.Requester) {
                                    //                            if(0==1){
                                    if (iOperatorRole == OperatorsManagement.Approvar) {
                            %> 
                            <a class="brand" href="requesterOperators.jsp">Operators</a>
                            <li><a  href="archiveRequestListAuthorizer.jsp">Archive Requests</a></li>
                            <li><a  href="PendingListForApproval.jsp">Pending Requests</a></li>
                                <%}%>
                            <a class="brand" href="otptokens.jsp">One Time Password Tokens</a>
                            <li><a  class="brand" href="archiveRequestList.jsp">Archived Requests</a></li>
                            <li><a  class="brand" href="PendingListForRequster.jsp">Pending Requests</a></li>

                            <%} else {%>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Channels Management<b class="caret"></b></a>
                                <ul class="dropdown-menu">

                                    <li><a href="channels.jsp"><i class="icon-file"></i>Channels</a></li>                                    
                                    <!--<li class="divider"></li>-->
                                    <li><a href="operatorsmain.jsp"><i class="icon-file"></i>Operators</a></li>
                                    <!--<li class="divider"></li>-->
                                    <li><a href="UnitsList.jsp"><i class="icon-file"></i>Units</a></li>
                                    <!--<li class="divider"></li>-->
                                    <li><a href="UsersGroup.jsp"><i class="icon-file"></i>Users Group</a></li>
                                    <!--<li class="divider"></li>-->
                                    <li class="dropdown-submenu">
                                        <a tabindex="-1" href="#"><i class="icon-file"></i>Templates</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="TemplateNew.jsp"><i class="icon-file"></i>Add New Template</a></li>                                    
                                            <li class="divider"></li>
                                            <li><a href="TemplateMobileList.jsp"><i class="icon-file"></i>Mobile Messages</a></li>                                    
                                            <li><a href="TemplateEmailList.jsp"><i class="icon-file"></i>Email Messages</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>


                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Configuration<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="inventoryReports.jsp?_type=7"><i class="icon-file"></i>Gateway</a></li>
                                    <li><a href="inventoryReports.jsp?_type=8"><i class="icon-file"></i>System Settings</a></li>
                                    <!--                                    <li><a href="smsgateway.jsp"><i class="icon-cog"></i>SMS Gateways</a></li>
                                                                        <li><a href="ussdgateway.jsp"><i class="icon-cog"></i>USSD Gateways</a></li>
                                                                        <li><a href="voicegateway.jsp"><i class="icon-cog"></i>Voice Gateways</a></li>
                                                                        <li><a href="emailgateway.jsp"><i class="icon-cog"></i>Email Servers</a></li>
                                                                        <li><a href="BillingManager.jsp"><i class="icon-cog"></i>Billing Manager</a></li>-->


                                    <li><a href="epinsetting.jsp"><i class="icon-hdd"></i>E-PIN Settings</a></li>      


                                </ul>
                            </li>



                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Security & User Management<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="searchResource.jsp"><i class="icon-user"></i>Resource Management</a></li>
                                    <!--                                    <li><a href="users.jsp"><i class="icon-user"></i>User & Password</a></li>
                                                                        <li><a href="userSecurePhrase.jsp"><i class="icon-user"></i>Secure Phase (Image Based)</a></li>
                                                                        <li><a href="otptokens.jsp"><i class="icon-user"></i>One Time Password Tokens</a></li>
                                                                        <li><a href="pkitokens.jsp"><i class="icon-user"></i>Digital Certificates And Tokens</a></li> 
                                                                        <li><a href="ChallengeResponse.jsp"><i class="icon-user"></i>Challenge Response (Q&A)</a></li> 
                                                                        <li><a href="trusteddevice.jsp"><i class="icon-user"></i>Trusted Device (via Mobile)</a></li>
                                                                        <li><a href="geotrackingmain.jsp"><i class="icon-user"></i>Geo-location (Fence & Track)</a></li>
                                                                        <li><a href="Twowayauth.jsp"><i class="icon-user"></i>TwoWayAuth Management</a></li>-->
                                    <li><a href="listSettings.jsp"><i class="icon-user"></i>Web Watch</a></li>
                                    <li><a href="resources.jsp"><i class="icon-cog"></i>Mobile Resource Management</a></li>
                                    <!--<li><a href="epdfsignhome.jsp"><i class="icon-user"></i>ePdfSigning</a></li>-->
                                    <li><a href="SNMPaddsettings.jsp"><i class="icon-cog"></i>SNMP System</a></li>
                                    <li><a href="ssoApplicationMapping.jsp"><i class="icon-user"></i>Single Sign On Mapping</a></li>
                                    <li class="dropdown-submenu">
                                        <a tabindex="-1" href="#"><i class="icon-file"></i>E-PDF Signing</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="epdfsign.jsp"><i class="icon-file"></i>Sign Request</a></li>                                    
                                            <li><a href="signcategory.jsp"><i class="icon-file"></i>Document Category</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="inventoryReports.jsp?_type=1"><i class="icon-file"></i>Inventory Reports</a></li>
                                    <li><a href="inventoryReports.jsp?_type=2"><i class="icon-file"></i>Usage Reports</a></li>
                                    <li><a href="inventoryReports.jsp?_type=3"><i class="icon-file"></i>Billing Reports</a></li>
                                    <li><a href="inventoryReports.jsp?_type=4"><i class="icon-file"></i>Message Reports</a></li>
                                    <!--<li><a href="inventoryReports.jsp?_type=5"><i class="icon-file"></i>Miscellaneous Reports</a></li>-->
                                    <li><a href="inventoryReports.jsp?_type=6"><i class="icon-file"></i>Audit Reports</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="ListSystemMessage.jsp"><i class="icon-cog"></i>System Alert(Operators)</a></li>

                                    <li><a href="hwotpupload.jsp"><i class="icon-user"></i>Hardware Token Uploader</a></li>        
                                    <li><a href="QuestionsList.jsp"><i class="icon-file"></i>Challenge Response Questions List</a></li>
                                    <li><a href="connectorstatuslist.jsp" ><i class="icon-cog"></i>Connectors Status</a></li>
                                    <li><a href="#CheckAuditIntegrity" data-toggle="modal"><i class="icon-cog"></i>Audit Integrity</a></li>
                                    <li><a href="scheduler.jsp"><i class="icon-file"></i>Report Scheduler's</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">System Admin<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="addAccessRole.jsp"><i class="icon-cog"></i>Access Role</a></li>
                                    <li><a href="showlicenseI.jsp"><i class="icon-user"></i>License Details</a></li>
                                    <li><a href="#ChangePassword" data-toggle="modal" ><i class="icon-cog"></i>Change Password</a></li>
                                    <li><a href="kycpendingrequest.jsp" ><i class="icon-cog"></i>KYC Request</a></li>
                                    <li><a href="initdbform.jsp" ><i class="icon-cog"></i>Change Database Password</a></li>
                                    <li><a href="reinitialize.jsp"><i class="icon-user"></i>Change Security Credentials</a></li>
                                </ul>
                            </li>





                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Requests<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="users.jsp"><i class="icon-user"></i>User & Password</a></li>
                                    <li><a href="ecopinpending.jsp">Pending Approvals <%=PendingRequests%></a></li>                                    
                                    <li><a href="ecopinfailed.jsp">Failed Deliveries <%=FailedRequests%></a></li>
                                </ul>
                            </li>

                            <%
                                }%>
                        </ul>

                        <%if (navigationSettings == null) {
                                // if(g_subjecttemplateSettings.getProperty("axiom.change.password").equals("true")){%>
                        <div class="pull-right">
                            <ul class="nav pull-right">                                
                                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"> Welcome, <%=name%><b class="caret"></b></a>
                                    <ul class="dropdown-menu">

                                        <!--                                        <li><a href="#ChangePassword" data-toggle="modal" ><i class="icon-cog"></i>Change Password</a></li>
                                                                                <li class="divider"></li>
                                                                                <li><a href="signout.jsp"><i class="icon-off"></i>Logout</a></li>-->
                                        <!--                                        <li><a href="showlicenseI.jsp"><i class="icon-user"></i>License Details</a></li>-->
                                        <!--                                        <li><a href="addAccessRole.jsp"><i class="icon-cog"></i>Access Role</a></li>-->
                                        <!--                                        <li><a href="ListSystemMessage.jsp"><i class="icon-cog"></i>System Message</a></li>-->
                                        <!--<li><a href="SNMPaddsettings.jsp"><i class="icon-cog"></i>SNMP System</a></li>-->
                                        <!--                                        <li><a href="hwotpupload.jsp"><i class="icon-user"></i>Hardware Token Uploader</a></li>                                        -->
                                        <!--                                        <li><a href="QuestionsList.jsp"><i class="icon-file"></i>Challenge Response Questions List</a></li>-->
                                        <!--<li><a href="signpdf.jsp"><i class="icon-user"></i>Sign PDF Document</a></li>-->
                                        <!--                                        <li><a href="reinitialize.jsp"><i class="icon-user"></i>Change Security Credentials</a></li>-->
                                        <!--                                        <li><a href="scheduler.jsp"><i class="icon-file"></i>Schedulers</a></li>-->
                                        <!--                                        <li><a href="channelaudit.jsp"><i class="icon-user"></i>Download Audit Trail</a></li>
                                                                                <li><a href="sessionaudit.jsp"><i class="icon-file"></i>Session Audit Trail</a></li>  
                                                                                <li><a href="#CheckAuditIntegrity" data-toggle="modal"><i class="icon-cog"></i>Audit Integrity</a></li>-->
                                        <!--<li><a href="#" onclick="DownloadZippedLog()"><i class="icon-file"></i>Download Logs</a></li>-->
                                        <li><a href="#downloadzippedlog" data-toggle="modal"><i class="icon-file"></i>Download Logs</a></li>
                                        <li><a href="#" onclick="DownloadCleanupLog()"><i class="icon-file"></i>Download Clean-up Logs</a></li>
                                        <!--                                        <li><a href="connectorstatuslist.jsp" ><i class="icon-cog"></i>Connectors Status</a></li>-->

                                        <!--                                        <li><a href="#ChangePassword" data-toggle="modal" ><i class="icon-cog"></i>Change Password</a></li>-->
                                        <!--                                        <li><a href="initdbform.jsp" ><i class="icon-cog"></i>Change Database Password</a></li>-->
                                        <li class="divider"></li>
                                        <li><a href="signout.jsp"><i class="icon-off"></i>Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <%}%>
                    </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <%}%>
        <!--   check here  -->


        <div id="downloadzippedlog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                <h3 id="myModalLabel">Download Logs</h3>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <form class="form-horizontal" id="downloadzippedlogForm" name="downloadzippedlogForm">
                        <div class="span12">
                            <div class="control-group" >
                                <div class="input-prepend">
                                    <span class="add-on">Select Date</span>   
                                    <div id="downloadzippedlog1" class="input-append date" algin = "center">
                                        <input id="_logsDate" name="_logsDate"  type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <div class="modal-footer">

                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <!--<div class="span3" id="add-new-scheduler-result"></div>-->
                <button class="btn btn-primary" onclick="DownloadZippedLogByDay()" id="addnewSchedulerSubmitBut">Download logs</button>
            </div>
        </div>                                        

        <script>
            $(document).ready(function () {
                $('#left-menu').sidr({
                    name: 'sidr-left',
                    side: 'left' // By default
                });

            });
            $(document).ready(function () {
                $('#simple-menu').sidr();
            });

            $(document).ready(function () {
                $("#downloadzippedlog1").datepicker({dateFormat: "dd-MM-yyyy"}).val()
            });
        </script>
        <%
//            String startOrstop = (String) session.getAttribute("startOrstop");
//            if (startOrstop == null) {
//                startOrstop = "";
//    }
//    if (startOrstop.equals("1") || startOrstop.equals("")) {%>                                    
        <!--        <script>
                    setInterval(function() {
                        checkValidMonitor()
                    }, 50000);
                </script>-->
        <% //}%>
