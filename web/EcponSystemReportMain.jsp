<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Epintracker"%>
<%@page import="com.mollatech.ecopin.management.EPINManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/ecopin/ecopin.js"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/css/datepicker.css">
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">

<div class="container-fluid">
    <h1 class="text-success">E-PIN Reports</h1>
    <h3>Make your own report</h3>
    <!--/ <input type="hidden" id="_changeStatusType" name="_changeStatusType">-->
    <div class="row-fluid">
        <div class="span12">
            <div class="control-group form-inline">

                <div class="input-prepend">

                    <!--<div class="well">-->
                    <span class="add-on">From:</span>   
                    <div id="datetimepicker1" class="input-append date">
                        <input id="startdate" name="startdate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                    <!--</div>-->
                </div>
                <div class="input-prepend">
                    <!--<div class="well">-->
                    <span class="add-on">till:</span>   
                    <div id="datetimepicker2" class="input-append date">
                        <input id="enddate" name="enddate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                    <!--</div>-->
                </div>
                <!--<button class="btn btn-success" onclick="EcopinSystemReport()" type="button">Generate Report</button>-->
                <%if (oprObj.getRoleid() != 1) {
                        if (accessObj != null && accessObj.viewEpinSystemReport == true) {%>
                <button class="btn btn-success" onclick="EcopinSystemReport()" id="addUserButton">Filter</button>
                <%} else {%>
                <button class="btn btn-success " onclick="InvalidRequestEpin('epinreport')" type="button">Generate Report Now</button>
                <%}
                } else {%>
                <button class="btn btn-success" onclick="EcopinSystemReport()" id="addUserButton">Filter</button>
                <%}%>

            </div>
            <div id="SystemReport">

            </div>
        </div>

    </div>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            });
        });
        $(function () {
            $('#datetimepicker2').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            });
        });
    </script>
</div>

<%@include file="footer.jsp" %>
