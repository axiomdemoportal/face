<%@include file="header.jsp" %>
<script src="./assets/js/ivrgateway.js"></script>
<div class="container-fluid">
    <h1 class="text-success">Voice(IVR) Gateway Configuration</h1>
    <p>To facilitate mobile based notification, Voice(IVR) Gateway(s) connection needs to be configured. For auto fail-over, please set the Secondary Gateway too. </p>
    <div class="tabbable">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#primary" data-toggle="tab">Primary Voice(IVR) Gateway</a></li>
            <li><a href="#secondary" data-toggle="tab">Secondary Voice(IVR) Gateway</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="primary">
                <div class="row-fluid">
                    <form class="form-horizontal" id="voiceprimaryform" name="voiceprimaryform">

                        <div id="legend">
                            <legend class="">Primary Voice(IVR) Gateway Configuration</legend>
                        </div>

                        <div class="control-group">
                            <label class="control-label"  for="username">Status </label>
                            <div class="controls">
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_status-primary-ivr"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ChangeActiveStatusIVR(1,1)">Mark as Active?</a></li>
                                        <li><a href="#" onclick="ChangeActiveStatusIVR(1,0)">Mark as Suspended?</a></li>
                                    </ul>
                                </div>
                                with Auto Fail-Over to Secondary Gateway
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_autofailover-primary-ivr"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ChangeFailOverIVR(1)">Mark as Enabled?</a></li>
                                        <li><a href="#" onclick="ChangeFailOverIVR(0)">Mark as Disabled?</a></li>
                                    </ul>
                                </div>
                            </div>
                            
                        </div>

                        <input type="hidden" id="_statusIVR" name="_statusIVR">
                        <input type="hidden" id="_perferenceIVR" name="_perferenceIVR" value="1">
                        <input type="hidden" id="_typeIVR" name="_typeIVR" value="3">
                        <input type="hidden" id="_autofailoverIVR" name="_autofailoverIVR">
                        <input type="hidden" id="_retriesIVR" name="_retriesIVR">
                        <input type="hidden" id="_retrydurationIVR" name="_retrydurationIVR">

                        <div class="control-group">
                            <label class="control-label"  for="username">Retry Count</label>
                            <div class="controls">
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_retries-primary-ivr"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ChangeRetryIVR(1,2)">2 Retries</a></li>
                                        <li><a href="#" onclick="ChangeRetryIVR(1,3)">3 Retries</a></li>
                                        <li><a href="#" onclick="ChangeRetryIVR(1,5)">5 Retries</a></li>
                                    </ul>
                                </div>
                                with wait time between retries as
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_retryduration-primary-ivr"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ChangeRetryDurationIVR(1,10)">10 seconds</a></li>
                                        <li><a href="#" onclick="ChangeRetryDurationIVR(1,30)">30 seconds</a></li>
                                        <li><a href="#" onclick="ChangeRetryDurationIVR(1,60)">60 seconds</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <hr>

                        <div class="control-group">
                            <label class="control-label"  for="username">Host : Port </label>
                            <div class="controls">
                                <input type="text" id="_ipIVR" name="_ipIVR" placeholder="example localhost/127.0.0.1" class="span3">
                                : <input type="text" id="_portIVR" name="_portIVR" placeholder="443" class="span1">

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Authenticate using </label>
                            <div class="controls">
                                <input type="text" id="_userIdIVR" name="_userIdIVR" placeholder="enter user id for authenticaiton " class="span3">
                                : <input type="password" id="_passwordIVR" name="_passwordIVR" placeholder="leave blank is no authentication" class="span3">
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label"  for="username">Registered Phone</label>
                            <div class="controls">
                                <input type="text" id="_phoneNumberIVR" name="_phoneNumberIVR" placeholder="display number while sending" class="input-xlarge">                                
                            </div>
                        </div>
                        
                        <hr>
                        
                        <div class="control-group">
                            <label class="control-label"  for="username">Additional Attributes</label>
                            <div class="controls">
                                <input type="text" id="_reserve1IVR" name="_reserve1IVR" placeholder="additional details >> name1=value1" class="span3">
                                : <input type="text" id="_reserve2IVR" name="_reserve2IVR" placeholder="additional details >> name1=value1" class="span3">
                                : <input type="text" id="_reserve3IVR" name="_reserve3IVR" placeholder="additional details >> name1=value1" class="span3">
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label"  for="username">Implementation Class</label>
                            <div class="controls">
                                <input type="text" id="_classNameIVR" name="_classNameIVR" placeholder="complete class name including package" class="input-xlarge">
                            </div>
                        </div>
                        
                        <!-- Submit -->
                        <div class="control-group">
                            <div class="controls">
                                <div id="save-ivr-gateway-primary-result"></div>
                                <%// if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) { %>
                                <button class="btn btn-primary" onclick="EditIVRSetting(1)" type="button">Save Setting Now >> </button>
                                <% //} %>
                                <button class="btn" onclick="LoadTestIVRConnectionUI(1)" type="button">Test Connection >> </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <div class="tab-pane" id="secondary">
                <div class="row-fluid">
                    <form class="form-horizontal" id="ivrsecondaryform" name="ivrsecondaryform" >
                        <fieldset>
                            <div id="legend">
                                <legend class="">Secondary IVR Gateway Configuration</legend>
                            </div>
                            <div class="control-group">
                                <label class="control-label"  for="username">Status </label>
                                <div class="controls">
                                    <div class="btn-group">
                                        <button class="btn btn-small"><div id="_status-secondary-ivr"></div></button>
                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="ChangeActiveStatusIVR(2,1)">Mark as Active?</a></li>
                                            <li><a href="#" onclick="ChangeActiveStatusIVR(2,0)">Mark as Suspended?</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" id="_statusIVRS" name="_statusIVRS">
                            <input type="hidden" id="_retriesIVRS" name="_retriesIVRS">
                            <input type="hidden" id="_retrydurationIVRS" name="_retrydurationIVRS">
                            <input type="hidden" id="_perferenceIVR" name="_perferenceIVR" value="2">
                            <input type="hidden" id="_typeIVR" name="_typeIVR" value="3">

                            <div class="control-group">
                                <label class="control-label"  for="username">Retry Count</label>
                                <div class="controls">
                                    <div class="btn-group">
                                        <button class="btn btn-small"><div id="_retries-secondary-ivr"></div></button>
                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="ChangeRetryIVR(2,2)">2 Retries</a></li>
                                            <li><a href="#" onclick="ChangeRetryIVR(2,3)">3 Retries</a></li>
                                            <li><a href="#" onclick="ChangeRetryIVR(2,5)">5 Retries</a></li>
                                        </ul>
                                    </div>
                                    with wait time between retries as
                                    <div class="btn-group">
                                        <button class="btn btn-small"><div id="_retryduration-secondary-ivr"></div></button>
                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="ChangeRetryDurationIVR(2,10)">10 seconds</a></li>
                                            <li><a href="#" onclick="ChangeRetryDurationIVR(2,30)">30 seconds</a></li>
                                            <li><a href="#" onclick="ChangeRetryDurationIVR(2,60)">60 seconds</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <hr>

                            
                            <div class="control-group">
                                <label class="control-label"  for="username">Host:Port </label>
                                <div class="controls">
                                    <input type="text" id="_ipIVRS" name="_ipIVRS" placeholder="example localhost/127.0.0.1" class="span3">
                                    : <input type="text" id="_portIVRS" name="_portIVRS" placeholder="443" class="span2">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"  for="username">Authenticate using</label>
                                <div class="controls">
                                    <input type="text" id="_userIdIVRS" name="_userIdIVRS" placeholder="leave blank is no authentication" class="span3">
                                    : <input type="password" id="_passwordIVRS" name="_passwordIVRS" placeholder="leave blank is no authentication" class="span3">
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label"  for="username">Registered Phone</label>
                                <div class="controls">
                                    <input type="text" id="_phoneNumberIVRS" name="_phoneNumberIVRS" placeholder="display number while sending" class="span3">
                                    
                                </div>
                            </div>
                            <hr>
                            
                            <div class="control-group">
                                <label class="control-label"  for="username">Additional Attributes</label>
                                <div class="controls">
                                    <input type="text" id="_reserve1IVRS" name="_reserve1IVRS" placeholder="additional details >> name1=value1" class="span3">
                                    : <input type="text" id="_reserve2IVRS" name="_reserve2IVRS" placeholder="additional details >> name1=value1" class="span3">
                                    : <input type="text" id="_reserve3IVRS" name="_reserve3IVRS" placeholder="additional details >> name1=value1" class="span3">
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label"  for="username">Implementation Class</label>
                                <div class="controls">
                                    <input type="text" id="_classNameIVRS" name="_classNameIVRS" placeholder="complete class name including package" class="input-xlarge">
                                </div>
                            </div>
                            <!-- Submit -->
                            <div class="control-group">
                                <div class="controls">
                                    <% //if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) { %>
                                    <button class="btn btn-primary"  onclick="EditIVRSetting(2)" type="button">Save Setting Now >> </button>
                                    <%// } %>
                                    <button class="btn" onclick="LoadTestIVRConnectionUI(2)" type="button">Test Connection >> </button>
                                    <div id="save-ivr-gateway-secondary-result"></div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <script language="javascript" type="text/javascript">
        LoadIVRSetting(1);
        LoadIVRSetting(2);
    </script>
</div>




<div id="testIVRPrimary" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Test Primary Gateway</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="testIVRPrimaryForm" name="testIVRPrimaryForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Test Message</label>
                        <div class="controls">
                            <input type="text" id="_testmsg" name="_testmsg" placeholder="this is the sample message to be sent out..." class="input-xlarge">
                            <input type="hidden" id="_type" name="_type" value="3">
                            <input type="hidden" id="_preference" name="_preference" value="1">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Phone</label>
                        <div class="controls">
                            <input type="text" id="_testphone" name="_testphone" placeholder="set phone for notification" class="input-xlarge">
                        </div>
                    </div>                    
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div class="span3" id="test-ivr-primary-configuration-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="testconnectionIVRprimary()" id="testIVRPrimaryBut">Send Message Now</button>
    </div>
</div>

<div id="testIVRSecondary" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Test Secondary Gateway</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="testIVRSecondaryForm" name="testIVRSecondaryForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Test Message</label>
                        <div class="controls">
                            <input type="text" id="_testmsgS" name="_testmsgS" placeholder="this is the sample message to be sent out..." class="input-xlarge">
                            <input type="hidden" id="_type" name="_type" value="3">
                            <input type="hidden" id="_preference" name="_preference" value="2">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Phone</label>
                        <div class="controls">
                            <input type="text" id="_testphoneS" name="_testphoneS" placeholder="set phone for notification" class="input-xlarge">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div class="span3" id="test-ivr-secondary-configuration-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="testconnectionIVRsecondary()" id="testIVRSecondaryBut">Send Message Now</button>
    </div>
</div>

<%@include file="footer.jsp" %>