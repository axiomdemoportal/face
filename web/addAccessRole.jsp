<%@include file="header.jsp" %>
<script src="./assets/js/accessMatrix.js"></script>
<div class="container-fluid">
    <h1 class="text-success">Access Role Matrix Management</h1>

    <br>
    <table class="table table-striped">
        <tr>
            <TD ><FONT size=3 ><B>No.</B></FONT></TD>
            <TD ><FONT size=3 ><B>Role</B></FONT></TD>
            <TD ><FONT size=3 ><B>Access Rights</B></FONT></TD>
            <TD ><FONT size=3 ><B>Download</B></FONT></TD>
            <TD ><FONT size=3 ><B>Manage</B></FONT></TD>
            <TD ><FONT size=3 ><B>Created On</B></FONT></TD>
            <TD ><FONT size=3 ><B>Updated On</B></FONT></TD>
        </tr>

        <%
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            OperatorsManagement opMngt = new OperatorsManagement();
            Roles[] arrRoles = opMngt.getAllRoles(channel.getChannelid());
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
            String strerr = "No Record Found";
            if (arrRoles != null) {
                for (int i = 0; i < arrRoles.length; i++) {

                    String userStatus = "-";

                    if (arrRoles[i].getStatus() == OperatorsManagement.ACTIVE_STATUS) {
                        userStatus = "Active";
                    } else if (arrRoles[i].getStatus() == OperatorsManagement.SUSPEND_STATUS) {
                        userStatus = "Suspended";
                    }
                    String useriStatus = userStatus + i;
        %>

        <tr>

            <td><%=i + 1%></td>
            <td><%=arrRoles[i].getName()%></td>
            <td>
                <!--<a href="./roleAccessRightNew.jsp?_roleId=<%=arrRoles[i].getRoleid()%>"class="btn btn-mini" id="mobileTemplateBody-<%=i%>"  rel="popover" data-html="true">Click to View</a>-->
                <!--<a href="./roleAccessRightN.jsp?_roleId=<%=arrRoles[i].getRoleid()%>"class="btn btn-mini" id="mobileTemplateBody-<%=i%>"  rel="popover" data-html="true">Click to View</a>-->
                <%if (oprObj.getRoleid() != 1) {%>
                <%if (accessObj != null && (accessObj.viewaccessMatrix == true)) {%>
                <a href="./roleAccessRightN_1.jsp?_roleId=<%=arrRoles[i].getRoleid()%>&_type=ROLE"class="btn btn-mini" id="mobileTemplateBody-<%=i%>"  rel="popover" data-html="true">Click to View</a>
                <%} else {%>
                <button class="btn btn-mini" onclick="InvalidRequest('rolereports')" type="button">Click to View</button>
                <%}
                } else {%>
                <a href="./roleAccessRightN_1.jsp?_roleId=<%=arrRoles[i].getRoleid()%>&_type=ROLE"class="btn btn-mini" id="mobileTemplateBody-<%=i%>"  rel="popover" data-html="true">Click to View</a>
                <%}%>
            </td>
            <td><button class="btn btn-mini" onclick="downlaoAccessMatrix('1', '<%=arrRoles[i].getRoleid()%>')" type="button">PDF</button>
            <button class="btn btn-mini" onclick="downlaoAccessMatrix('0', '<%=arrRoles[i].getRoleid()%>')" type="button">CSV</button>
            <button class="btn btn-mini" onclick="downlaoAccessMatrix('2', '<%=arrRoles[i].getRoleid()%>')" type="button">TXT</button></td>
            <td>
                <div class="btn-group">
                    <button class="btn btn-mini" id="<%=useriStatus%>"><%=userStatus%></button>
                    <!--<button class="btn btn-mini">Manage</button>-->
                    <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <% //if (accessObj.editaccessMatrix == true) {%>
                        <li><a href="#"  onclick="ChangeRoleStatus('<%=arrRoles[i].getRoleid()%>', 1, '<%=useriStatus%>')" >Mark as Active?</a></li>
                            <% //}%>
                            <% //if (accessObj.editaccessMatrix == true) {%>
                        <li><a href="#" onclick="ChangeRoleStatus('<%=arrRoles[i].getRoleid()%>', 0, '<%=useriStatus%>')" >Mark as Suspended?</a></li>
                            <% //}%>
                        <li class="divider"></li>
                            <% //if (accessObj.editaccessMatrix == true) {%>
                        <li><a href="#" onclick="loadRolesDetails('<%=arrRoles[i].getName()%>', '<%=arrRoles[i].getStatus()%>', '<%=arrRoles[i].getRoleid()%>')">Edit</a></li>
                            <% //}%>
                            <% //if (accessObj.removeaccessMatrix == true) {%>
                        <li><a href="#" onclick="deleteRole('<%=arrRoles[i].getRoleid()%>')"><font color="red">Remove?</font></a></li>
                                <% //}%>
                    </ul>
                </div>

            </td>
            <TD ><%=sdf.format(arrRoles[i].getCreatedOn())%></TD>
            <td ><%=sdf.format(arrRoles[i].getLastAccessOn())%></td>

        </tr>
        <%
            }
        } else {

        %>

        <td><%=strerr%></td>
        <td><%=strerr%></td>
        <td><%=strerr%></td>
        <td><%=strerr%></td>
        <td><%=strerr%></td>
        <td><%=strerr%></td>

        <%}%>
    </table>


    <!--</div>-->
    <!--</div>-->
    <br>
    <table>
        <tr>
            <td>
                <button class="btn btn-primary" onclick="downlaodAllAccessMatrix(1)" id="addnewRoleSubmitBut">Download PDF</button>
                <button class="btn btn-primary" onclick="downlaodAllAccessMatrix(0)" id="addnewRoleSubmitBut">Download CSV</button>
                <button class="btn btn-primary" onclick="downlaodAllAccessMatrix(2)" id="addnewRoleSubmitBut">Download TXT</button>
                <%if (accessObj.addaccessMatrix == true) {%>
                
                <a href="#addNewRole" class="btn btn-primary" data-toggle="modal">Add new Role&raquo;</a>
                <%}%>
            </td>
        </tr>
    </table>

    <script language="javascript">
        //listChannels();
    </script>
</div>

<div id="addNewRole" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Add New Role</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="AddNewRoleForm" name="AddNewRoleForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Name</label>
                        <div class="controls">
                            <input type="text" id="_rolename" name="_rolename" placeholder="set role name" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Status</label>
                        <div class="controls">
                            <select name="_rolestatus" id="_rolestatus" class="input-large">
                                <option value="1">Active</option>
                                <option value="0">Suspended</option>
                            </select>   
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>

    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <div class="span3" id="add-new-role-result"></div>

        <button class="btn btn-primary" onclick="addRole()" id="addnewRoleSubmitBut">Add New Role</button>

    </div>
</div>

<div id="EditRole" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Edit Role</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="EditRoleForm" name="EditRoleForm">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_roleIDE" name="_roleIDE">
                    <div class="control-group">
                        <label class="control-label"  for="username">Name</label>
                        <div class="controls">
                            <input type="text" id="_rolenameE" name="_rolenameE" placeholder="set role name" class="input-xlarge">
                        </div>
                    </div>
                    <!--                      <div class="control-group">
                                            <label class="control-label"  for="username">Status</label>
                                            <div class="controls">
                                                <select name="_rolestatusE" id="_rolestatusE" class="input-large">
                                                    <option value="1">Active</option>
                                                    <option value="0">Suspended</option>
                                                </select>   
                                            </div>
                                        </div>-->
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <div class="span3" id="edit-new-role-result"></div>
        <button class="btn btn-primary" onclick="EditRole()" id="editRoleSubmitBut">Edit Role</button>
    </div>
</div>

<%@include file="footer.jsp" %>

