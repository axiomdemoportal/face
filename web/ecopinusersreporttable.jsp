<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Epintracker"%>
<%@page import="com.mollatech.ecopin.management.EPINManagement"%>
<%@page import="com.mollatech.axiom.nucleus.settings.SendNotification"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="java.text.SimpleDateFormat"%>


<%
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
     String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    String _searchtext = request.getParameter("_searchtext");
    String _startdate = request.getParameter("_startdate");
    String _enddate = request.getParameter("_enddate");
    DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    Date startDate = null;
    if (_startdate != null && !_startdate.isEmpty()) {
        startDate = (Date) formatter.parse(_startdate);
    }
    Date endDate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        endDate = (Date) formatter.parse(_enddate);
    }
    EPINManagement eMngt = new EPINManagement();
    UserManagement uMngt = new UserManagement();
 
    Epintracker[] epinobj = eMngt.searchEPINUserObj(channel.getChannelid(),_searchtext, endDate, startDate);
    
    
      Epintracker[] unqueUserObj = eMngt.searchuniqueEPINUserObj(epinobj);
      
      int pinrequestCount = 0;
      int usedPinCount = 0;
       pinrequestCount = eMngt.getRequestPinCount(channel.getChannelid(),_searchtext, endDate, startDate);
       usedPinCount = eMngt.getUsedPinCount(channel.getChannelid(),_searchtext, endDate, startDate);
      System.out.print(unqueUserObj.length);
    
    
 %>

    <div class="tab-content">
          <div class="row-fluid">
              <div id="licenses_data_table">
                <table class="table table-striped" id="table_main">
                          <tr>
                                <td>No.</td>
                                <td>User Name</td>
                                <td>Phone</td>
                                <td>Email</td>
                                <td>Epin Requests</td>
                                <td>Epin Used</td>
                                 <td>Show Detail Reports</td>
                          </tr>
                            <% if (unqueUserObj != null) {
                                    for (int i = 0; i < unqueUserObj.length;i++) {
                                        AuthUser user = uMngt.getUser(sessionId, channel.getChannelid(), unqueUserObj[i].getUserid());
                                        if(user != null){
                                             pinrequestCount = eMngt.getRequestPinCount(channel.getChannelid(),user.getUserId(), endDate, startDate);
                                             usedPinCount = eMngt.getUsedPinCount(channel.getChannelid(),user.getUserId(), endDate, startDate);
                            %>
                            <tr>
                                <td><%=(i + 1)%></td>
                                <td><%=user.getUserName()%></td> 
                                <td><%=user.getPhoneNo() %></td> 
                                <td><%=user.getEmail() %></td> 
                                <td><%=pinrequestCount %></td> 
                                <td><%=usedPinCount %></td> 
                                <td> <a href="#" class="btn btn-mini" onclick="EcopinReport('<%=user.getUserId() %>','<%=_enddate %>','<%=_startdate %>')">View Report</a></td>                    
                            </tr>
                            <%}}
                            } else {%>
                            <tr>
                            <td><%=1%></td>
                            <td>No Records Found</td>
                            <td>No Records Found</td>
                            <td>No Records Found</td> 
                            <td>No Records Found</td> 
                             <%}%>
                             </tr>
                        </table>
               </div>
            </div>
       
    </div>

    <br><br>