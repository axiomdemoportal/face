<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="com.mollatech.axiom.nucleus.db.Remotesignature"%>
<%--<%@page import="com.mollatech.axiom.nucleus.db.Users"%>--%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.PDFSigningManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<!--<div class="container-fluid">-->
<%
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    String _channelId = channel.getChannelid();

    String _searchType = request.getParameter("_searchType");
    
    int isearchType = Integer.parseInt(_searchType);
    
    
    UserManagement usermngObj = new UserManagement();
    PDFSigningManagement pdfObj = new PDFSigningManagement();

    AuthUser Users[] = null;
     Remotesignature[] RemoteSign = null;
    if(isearchType == 1){
        String _userName = request.getParameter("_userName");
    Users = usermngObj.SearchUsers(sessionId, _channelId, _userName);
    }else if(isearchType == 0){
        String _archiveId = request.getParameter("_userName");
    
      RemoteSign = pdfObj.getRemoteSignatureObj(_channelId, _archiveId, isearchType);
    }if(isearchType == 2){
         String _reference = request.getParameter("_userName");
           RemoteSign = pdfObj.getRemoteSignatureObj(_channelId, _reference, isearchType);
    }
    String strerr = "No Record Found";
  
%>

<div class="row-fluid">

    <div id="licenses_data_table">
        <input type="hidden" id="_isroaming" name="_isroaming"  >
        <table class="table table-striped" id="table_main">

            <tr>
                <td>No.</td>
                <td>Archive Id</td>
                <td>Reference Id</td>
                <td>User</td>
                <td>Type</td>
                <td>OTP Result</td>
                <td>OTP Verify Time</td>                
                <td>Signature</td>
                <td>Data/File</td>
                <td>Signing Result</td>
                <td>Signing Time</td>
            </tr>
            <%
                  if (Users != null) {
                for (int i = 0; i < Users.length; i++) {
                    Remotesignature[] rObj = pdfObj.getRemoteSignatureObj(channel.getChannelid(), Users[i].getUserId(),isearchType);

                    
                    if(rObj != null){
                        
                        for(int j =0;j<rObj.length;j++){
                            
                        
                        String strdoctype = "";
                        if(rObj[j].getDocType() == 1 ){
                            strdoctype = "Raw";
                        }else if(rObj[j].getDocType() == 2){
                            strdoctype = "PDF";
                        }
                        
                         String strresult = "";
                        if(rObj[j].getOtpResult() == 0 ){
                            strresult = "Success";
                        }else if(rObj[j].getOtpResult()== -1){
                            strresult = "Failed";
                        }else if(rObj[j].getOtpResult()== -2){
                            strresult = "Expired";
                        }
                         String strresultsigning = "";
                        if(rObj[j].getResultOfSigning() == 0 ){
                            strresultsigning = "Success";
                        }else if(rObj[j].getResultOfSigning()== 1){
                            strresultsigning = "Failed";
                        }
//                        String signaturefile = rObj[i].getSignature();
//                     String datatosignfile = rObj[i].getDataToSign();
                        
                        
            %>
            <tr>
                <td><%=(j + 1)%></td>
                <td><%=rObj[j].getArchiveid()%></td>
                <td><%=rObj[j].getReferenceid()%></td>
                <td><%= Users[0].getUserName()%></td>
                <td><%=strdoctype %></td>                
                <td><%=strresult %></td>
                <td><%=rObj[j].getTimeOfOtpVerification()%></td>                
                <% 
                    String s = URLEncoder.encode(rObj[j].getArchiveid(),"UTF-8");                  
                %>
                <td><a href='./getremoteserversigning?archiveId=<%=s%>&type=1'>Download</a></td>
                <td><a href='./getremoteserversigning?archiveId=<%=s%>&type=2'>Download</a></td>
                  <td><%=strresultsigning%></td>
                  <td><%=rObj[j].getTimeOfSigning()%></td>
            </tr>
            <%}}}} else if(RemoteSign != null){
            
            for(int k=0;k<RemoteSign.length;k++){
            %>

          <%
             AuthUser uObj =  usermngObj.getUser(sessionId, _channelId, RemoteSign[k].getUserid());
              String signaturefile = RemoteSign[k].getSignature();
              String datatosignfile = RemoteSign[k].getDataToSign();
              String strdoctype = "";
                        if(RemoteSign[k].getDocType() == 1 ){
                            strdoctype = "Raw";
                        }else if(RemoteSign[k].getDocType() == 2){
                            strdoctype = "PDF";
                        }
                        
                         String strresult = "";
                        if(RemoteSign[k].getOtpResult() == 0 ){
                            strresult = "Success";
                        }else if(RemoteSign[k].getOtpResult()== -1){
                            strresult = "Failed";
                        }else if(RemoteSign[k].getOtpResult()== -2){
                            strresult = "Expired";
                        }
                         String strresultsigning = "";
                        if(RemoteSign[k].getResultOfSigning() == 0 ){
                            strresultsigning = "Success";
                        }else if(RemoteSign[k].getResultOfSigning()== 1){
                            strresultsigning = "Failed";
                        }
          %>
            
            <tr>
                <td><%=( k + 1 )%></td>
                <td><%=RemoteSign[k].getArchiveid() %></td>
                <td><%=RemoteSign[k].getReferenceid()%></td>
                <td><%= uObj.getUserName() %></td>
                <td><%=strdoctype%></td>
                 <td><%=strresult%></td>
                <td><%=RemoteSign[k].getTimeOfOtpVerification()%></td>
                
                <% 
                    String s = URLEncoder.encode(RemoteSign[k].getArchiveid(),"UTF-8");  
                    String r = URLEncoder.encode(RemoteSign[k].getReferenceid(),"UTF-8");
                %>
                <td><a href='./getremoteserversigning?archiveId=<%=s%>&type=1'>Download</a></td>
                <td><a href='./getremoteserversigning?archiveId=<%=s%>&type=2'>Download</a></td>                
                <td><%=strresultsigning%></td>
                  <td><%=RemoteSign[k].getTimeOfSigning()%></td>
               
                <%}}else{%>
                 <tr>
                <td><%=1%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                 <td><%= strerr%></td>
                  <td><%= strerr%></td>
               
                <%}%>
            </tr>
         

        </table>
    </div>
</div>
<!--</div>-->





<%--<%@include file="footer.jsp" %>--%>