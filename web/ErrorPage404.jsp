<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isErrorPage="true" %>
<% response.setStatus(404);%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>HTTP 404</title>
    </head>
    <body>
        <div class="wrapper row2">
            <div id="container" class="clear">
                <section id="fof" class="clear">
                    <div class="hgroup clear">
                        <h1>HTTP 404</h1>
                        <h2><span>Page Not Found !!!</span></h2>
                    </div>
                    <p>You Request Could Not Be Complete, Please Try Again...</p>
                 
                </section>
            </div>
        </div>
    </body>
</html>