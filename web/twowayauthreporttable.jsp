<%@page import="org.json.JSONObject"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TxManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Txdetails"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TwowayauthManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Twowayauth"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">
<link rel="stylesheet" href="./assets/css/datepicker.css">
<script src="./assets/js/bootstrap-datepicker.js"></script>
<script src="./assets/js/twowayauth.js"></script>
<script src="./assets/js/bootstrap-popover.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();

    String _startdate = request.getParameter("_startdate");
    String _enddate = request.getParameter("_enddate");
    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
    Date startDate = null;
    if (_startdate != null && !_startdate.isEmpty()) {
        startDate = (Date) formatter.parse(_startdate);
        startDate.setHours(0);
        startDate.setMinutes(0);
        startDate.setSeconds(0);
    }
    Date endDate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        endDate = (Date) formatter.parse(_enddate);
        endDate.setHours(23);
        endDate.setMinutes(59);
        endDate.setSeconds(59);
    }
    Txdetails[] txdetailses = new TxManagement().getAllTxdetailsByDate(sessionId, _channelId, startDate, endDate);
    session.setAttribute("txdetailses", txdetailses);
    String strmsg = "No Record Found";
%>
<!--<div class="container-fluid">-->
<div class="tabbable" id="REPORT">
    <ul class="nav nav-tabs" id="twoWayReportTab">
        <li class="active"><a href="#twowaycharts" data-toggle="tab">Charts</a></li>
        <li><a href="#twowayreport" data-toggle="tab">Tabular List</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="twowaycharts">
            <div class="row-fluid">
                <div class="span12">
                    <div class="control-group">
                        <div class="span4">
                            <div id="twoWayAuthgraph" ></div>
                        </div>
                        <div  class="span8">
                            <div id="twoWayAuthgraph1"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-pane" id="twowayreport">  
            <!--                <div id="auth_table_main">
                            </div>-->

            <div class="row-fluid">
                <div class="span12">
                    <table class="table table-striped" >
                        <tr>
                            <td>No.</td>
                            <td>User Name</td>
                            <td>Tx. Id</td>
                            <td>Type</td>
                            <td>Device Id</td>
                            <td>Message</td>
                            <td>Status</td>
                            <td>Question Asked</td>
                            <td>QnA Status</td>
                            <td>Created On</td>
                            <td>Updated On</td>
                            <td>Expired On</td>
                        </tr>
                        <%    if (txdetailses != null) {
                                Map map = new HashMap();
                                for (int i = 0; i < txdetailses.length; i++) {
                                    String type = "NA";
                                    String deviceId = "NA";
                                    String strStatus = "APPROVED";
                                    String questionAsked = "NA";
                                    String updatedOn = "NA";
                                    String QnAStatus = "APPROVED";
                                    if (txdetailses[i].getType() != null && txdetailses[i].getType() == TxManagement.SMS) {
                                        type = "SMS";
                                    } else if (txdetailses[i].getType() != null && txdetailses[i].getType() == TxManagement.VOICE) {
                                        type = "VOICE";
                                    } else if (txdetailses[i].getType() != null && txdetailses[i].getType() == TxManagement.MISSEDCALL) {
                                        type = "MISSEDCALL";
                                    } else if (txdetailses[i].getType() != null && txdetailses[i].getType() == TxManagement.PUSH) {
                                        type = "PUSH";
                                    }
                                    if (txdetailses[i].getDeviceid() != null) {
                                        deviceId = txdetailses[i].getDeviceid();
                                    }
                                    if (txdetailses[i].getStatus() == TxManagement.CANCELED) {
                                        strStatus = "CANCELED";
                                    } else if (txdetailses[i].getStatus() == TxManagement.DENIED) {
                                        strStatus = "DENIED";
                                    } else if (txdetailses[i].getStatus() == TxManagement.EXPIRED) {
                                        strStatus = "EXPIRED";
                                    } else if (txdetailses[i].getStatus() == TxManagement.PENDING) {
                                        strStatus = "PENDING";
                                    } else if (txdetailses[i].getStatus() == TxManagement.RESPONDED) {
                                        strStatus = "RESPONDED";
                                    }
                                    if (txdetailses[i].getQuestion() != null) {
                                        questionAsked = "YES";
                                    }
                                    if (txdetailses[i].getQnAStatus() == TxManagement.CANCELED) {
                                        QnAStatus = "CANCELED";
                                    } else if (txdetailses[i].getQnAStatus() == TxManagement.DENIED) {
                                        QnAStatus = "DENIED";
                                    } else if (txdetailses[i].getQnAStatus() == TxManagement.EXPIRED) {
                                        QnAStatus = "EXPIRED";
                                    } else if (txdetailses[i].getQnAStatus() == TxManagement.PENDING) {
                                        QnAStatus = "PENDING";
                                    } else if (txdetailses[i].getQnAStatus() == TxManagement.RESPONDED) {
                                        QnAStatus = "RESPONDED";
                                    }
                                    if (txdetailses[i].getUpdatedOn() != null) {
                                        updatedOn = sdf.format(txdetailses[i].getUpdatedOn());

                                    }
                                    if (questionAsked.equals("NA")) {
                                        QnAStatus = "NA";
                                    }
                                    AuthUser user = null;
                                    if (map.isEmpty()) {
                                        user = new UserManagement().getUser(_channelId, txdetailses[i].getUserid());
                                        if (user != null) {
                                            map.put(txdetailses[i].getUserid(), user);
                                        }
                                    } else {
                                        if (map.get(txdetailses[i].getUserid()) == null) {
                                            user = new UserManagement().getUser(_channelId, txdetailses[i].getUserid());
                                            if (user != null) {
                                                map.put(txdetailses[i].getUserid(), user);
                                            }
                                        } else {
                                            user = (AuthUser) map.get(txdetailses[i].getUserid());
                                        }
                                    }
                                    String mes = "NA";
                                    String os = "NA";
                                    String browser = "NA";
                                    String location = "NA";
                                    String time = "NA";
                                    String ip = "NA";
                                    String txmessage = txdetailses[i].getTxmsg();
                                    if (txmessage.startsWith("{")) {
                                        JSONObject jsonObj = new JSONObject(txmessage);
                                        if (jsonObj.has("_os")) {
                                            os = jsonObj.getString("_os");
                                        }
                                        if (jsonObj.has("_browser")) {
                                            browser = jsonObj.getString("_browser");
                                        }
                                        if (jsonObj.has("_location")) {
                                            location = jsonObj.getString("_location");
                                        }
                                        if (jsonObj.has("_time")) {
                                            time = jsonObj.getString("_time");
                                        }
                                        if (jsonObj.has("_ip")) {
                                            ip = jsonObj.getString("_ip");
                                        }
                                        txmessage = "you are trying to login to system from IP: " + ip + " with Location: " + location + " browser Name: " + browser + " OS: " + os + " At Time: " + time;
                                    }
                        %> 
                        <tr>
                            <td><%=i + 1%></td>
                            <%if (user != null) {%>
                            <td><%=user.getUserName()%></td>
                            <%} else {%>
                            <td>NA</td>
                            <%}%>
                            <td><%=txdetailses[i].getTransactionId()%></td>
                            <td><%=type%></td>
                            <td><%=deviceId%></td>
                            <td>
                                <a href="#/" class="btn btn-mini" id="twoWayUsagRepMsgs-<%=i + 1%>"  rel="popover" data-html="true">Click to View</a>
                                <script>
                                    $(function ()
                                    {
                                        $("#twoWayUsagRepMsgs-<%=i + 1%>").popover({title: 'Message', content: "<%=txmessage%>"});
                                    });
                                </script>
                            </td>
                            <td><%=strStatus%></td>
                            <td><%=questionAsked%></td>
                            <td><%=QnAStatus%></td>
                            <td><%=sdf.format(txdetailses[i].getCreatedOn())%></td>
                            <td><%= updatedOn%></td>
                            <td><%=sdf.format(txdetailses[i].getExpiredOn())%></td>
                        </tr>
                        <%
                            }
                        } else {%>
                        <tr>
                            <td><%=strmsg%></td>
                            <td><%=strmsg%></td>
                            <td><%=strmsg%></td>
                            <td><%=strmsg%></td>
                            <td><%=strmsg%></td>
                            <td><%=strmsg%></td>
                            <td><%=strmsg%></td>
                            <td><%=strmsg%></td>
                            <td><%=strmsg%></td>
                            <td><%=strmsg%></td>
                            <td><%=strmsg%></td>

                        </tr>
                        <%}%>
                    </table>

                    <div class="row-fluid">   
                        <div class="span6">

                            <div class="control-group">                        
                                <div class="span1">
                                    <div class="control-group form-inline">
                                        <a href="#" class="btn btn-info" onclick="downloadauthReport(1, '<%=_startdate%>', '<%=_enddate%>')" >
                                            CSV</a>
                                    </div>
                                </div>
                                <div class="span1">
                                    <div class="control-group form-inline">
                                        <a href="#" class="btn btn-info" onclick="downloadauthReport(0, '<%=_startdate%>', '<%=_enddate%>')" >
                                            PDF</a>
                                    </div>
                                </div>
                                <div class="span1">
                                    <div class="control-group form-inline">
                                        <a href="#" class="btn btn-info" onclick="downloadauthReport(2, '<%=_startdate%>', '<%=_enddate%>')" >
                                            TEXT</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>