
<%@page import="com.mollatech.axiom.nucleus.settings.PINDeliverySetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.mollatech.ecopin.management.EPINManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Epintracker"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/epinsetting.js"></script>
<div class="container-fluid">
    <h1 class="text-success">Pending EPIN Trackers</h1>
    <p>EPIN PENDING REQUEST </p>
    <br>

    <div class="row-fluid">
        <div id="licenses_data_table">
            <table class="table table-striped">
                <tr>
                    <td>No.</td>
                    <td>Name</td>
                    <td>Mobile</td>
                    <td>Email</td>
                    <td>Operators status</td>
                    <td>Operator</td>
                    <td>Manage</td>
                    <td>Expiry In Mins</td>
                    <td>Request Date Time</td>

                </tr>
                <%  
                    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
                    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                    String _channelId = channel.getChannelid();

                    Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
                    String operatorId = operatorS.getOperatorid();
                    UserManagement usermngObj = new UserManagement();
                    AuthUser Users[] = null;
                    Epintracker tracks[] = null;
                    AuthUser User = null;
                    final int OPERATOR_CONTROLLED = 1;

                    final int PENDING = 2;
                    EPINManagement epinObj = new EPINManagement();
                    tracks = epinObj.GetEpintracker(sessionId, _channelId, PENDING);

                    if (tracks != null) {

                        for (int i = 0; i < tracks.length; i++) {

                            SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm ");
                            java.util.Date requestDate = new java.util.Date();
                            requestDate = tracks[i].getRequestdatetime();
                            User = usermngObj.getUser(sessionId, _channelId, tracks[i].getUserid());
                            int Mins = tracks[i].getExpiryInMins();

                            //String ExpiryMins = String.valueOf(Mins).concat("MINS");
                            int iOprStatus = tracks[i].getStatus();
                            String strStatus;
                            if (iOprStatus == 1) {
                                strStatus = "APPROVAL";
                            } else if (iOprStatus == 2) {
                                strStatus = "PENDING";
                            } else if (iOprStatus == -2) {
                                strStatus = "REJECTED";

                            } else {
                                strStatus = "EXPIRED";
                            }

                            String uidiv4OprStatus = "epin-status-value-" + i;
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(requestDate);
                            cal.add(Calendar.MINUTE, Mins);

                            Date expiryDate = cal.getTime();

                            OperatorsManagement opObj = new OperatorsManagement();

                            Operators ApprovedOperator = opObj.getOperatorById(channel.getChannelid(), tracks[i].getOperatorid1());
                            String opname = "No Operator Present";
                            if (ApprovedOperator != null) {
                                opname = ApprovedOperator.getName();
                            }
                            String operStatus = "Not Avialabale";
                            if (tracks[i].getOperatorsstatus() == EPINManagement.SINGLE_OPERATOR_CONTROLLED && tracks[i].getStatus() == EPINManagement.PENDING) {
                                operStatus = "Waiting for Operator";
                                 opname = "No Operator Present";
                            } else if (tracks[i].getOperatorsstatus() == EPINManagement.PENDING_FOR_OPERATORS && tracks[i].getStatus() == EPINManagement.PENDING) {
                                operStatus = "Waiting for 1st Operator";
                                 opname = "No Operator Present";
                            } else if (tracks[i].getOperatorsstatus() == EPINManagement.PENDING_FOR_2nd_OPERATORS && tracks[i].getStatus() == EPINManagement.PENDING) {
                                operStatus = "Waiting for 2nd Operator";
                            }
                            int check = -1;
                            if (ApprovedOperator != null) {
                            if(ApprovedOperator.getOperatorid().equals(operatorId) != true && tracks[i].getOperatorsstatus()!= EPINManagement.PENDING_FOR_2nd_OPERATORS){
                            check =1;
                            }else if(ApprovedOperator.getOperatorid().equals(operatorId) != true){
                                check = 1;
                            }
                            }else if(ApprovedOperator == null){
                                check =1;
                            }
                            if(check == 1){
                            

                %>


  <!--<td><%=User.getUserId()%></td>-->
                <td><%=(i + 1)%></td>
                <td><%=User.getUserName()%></td>
                <td><%=User.getPhoneNo()%></td>
                <td><%=User.getEmail()%></td>
                <td><%=operStatus%></td>
                <td><%=opname%></td>
                <td>
                    <div class="btn-group">
                        <button class="btn btn-mini" id="<%=uidiv4OprStatus%>"><%=strStatus%></button>
                        <!--<button class="btn btn-mini" >Manage</button>-->
                        <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="ChangeEpinStatus('<%=tracks[i].getEpintrackerid()%>', '<%=User.getUserId()%>', 1)">Approved??</a></li>
                            <li><a href="#" onclick="loadepinDetails('<%=tracks[i].getEpintrackerid()%>', '<%=User.getUserId()%>', -2)">Rejected??</a></li>

                        </ul>
                    </div>
                </td>
                <td><%=sdf.format(expiryDate)%></td>
                <td><%=sdf.format(requestDate)%></td>
                </tr>
                <%}}%>

            </table>

            <%  } else if (tracks == null) {%>
            <h3>No users found...</h3>
            <%}%>
        </div>
    </div>
    <br>
</div>
<div id="rejected" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="rejected">Reason For Rejection</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="rejectedForm" name="rejectedForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Reason</label>
                        <div class="controls">
                            <input type="text" id="_reason" name="_reason" placeholder="Reason to Reject" class="input-xlarge">
                        </div>
                    </div>

                    <input type="hidden" id="_epinid" name="_epinid" >
                    <input type="hidden" id="_userid" name="_userid">
                    <input type="hidden" id="_e_status" name="_e_status">

                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="rejected-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="changeepin()" id="buttonChangeEpin">Reject</button>
    </div>
</div>            

<%@include file="footer.jsp" %>
