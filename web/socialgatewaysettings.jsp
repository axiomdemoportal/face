<%@include file="header.jsp" %>
<script src="./assets/js/social.js"></script>
<div class="container-fluid">
    <h1 class="text-success">Social Media Configuration</h1>
    <p>To facilitate mobile based notification, Social Gateway(s) connection needs to be configured. </p>
    
        <div class="tab-content">
            <div class="tab-pane active" id="social">
                <div class="row-fluid">
                    <form class="form-horizontal" id="socialform" name="socialform">

                        
                        <div class="control-group">
                            <label class="control-label"  for="username">Status </label>
                            <div class="controls">
                                <div>
                                    <div class="btn-group">

                                        <button class="btn btn-small"><div id="_status-primary-social"></div></button>
                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="ChangeActiveStatusSocial(1, 1)">Mark as Active?</a></li>
                                            <li><a href="#" onclick="ChangeActiveStatusSocial(1, 0)">Mark as Suspended?</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" id="_status" name="_status">
                        <input type="hidden" id="_perference" name="_perference" value="1">
                        <input type="hidden" id="_type" name="_type" value="17">
                        <input type="hidden" id="_retries" name="_retries">
                        <input type="hidden" id="_retryduration" name="_retryduration">

                        <div class="control-group">
                            <label class="control-label"  for="username">Retry Count</label>
                            <div class="controls">
                                <div>
                                    <div class="btn-group">
                                        <button class="btn btn-small"><div id="_retries-primary-social"></div></button>
                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="ChangeRetrySocial(1, 2)">2 Retries</a></li>
                                            <li><a href="#" onclick="ChangeRetrySocial(1, 3)">3 Retries</a></li>
                                            <li><a href="#" onclick="ChangeRetrySocial(1, 5)">5 Retries</a></li>
                                        </ul>
                                    </div>
                                    with wait time between retries as      
                                    <div class="btn-group">
                                        <button class="btn btn-small"><div id="_retryduration-primary-social"></div></button>
                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="ChangeRetryDurationSocial(1, 10)">10 seconds</a></li>
                                            <li><a href="#" onclick="ChangeRetryDurationSocial(1, 30)">30 seconds</a></li>
                                            <li><a href="#" onclick="ChangeRetryDurationSocial(1, 60)">60 seconds</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
<!--                        <div class="control-group">
                            <label class="control-label"  for="username">Host:Port</label>
                            <div class="controls">
                                <input type="text" id="_ip" name="_ip" placeholder="example localhost/127.0.0.1" class="span3">
                                : <input type="text" id="_port" name="_port" placeholder="443" class="span1">
                            </div>
                        </div>-->


                        <div class="control-group">
                            <label class="control-label"  for="username">Facebook AppID:Secret</label>
                            <div class="controls">
                                <input type="text" id="_facebookappid" name="_facebookappid" placeholder="Facebook App ID" class="span3">
                                : <input type="text" id="_facebookappsecret" name="_facebookappsecret" placeholder="Facebook App Secret" class="span3">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">LinkedIn Key:Secret</label>
                            <div class="controls">
                                <input type="text" id="_linkedinkey" name="_linkedinkey" placeholder="Linkedin App Key" class="span3">
                                : <input type="text" id="_linkedinsecret" name="_linkedinsecret" placeholder="Linkedin App Secret" class="span3">
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label"  for="username">LinkedIn Token:Secret</label>
                            <div class="controls">
                                <input type="text" id="_linkedintoken" name="_linkedintoken" placeholder="Linkedin App Access Token" class="span3">
                                : <input type="text" id="_linkedinaccesssecret" name="_linkedinaccesssecret" placeholder="Linkedin App Access Secret" class="span3">
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label"  for="username">Twitter Key:Secret</label>
                            <div class="controls">
                                <input type="text" id="_twitterkey" name="_twitterkey" placeholder="Twitter Consumer Key" class="span3">
                                : <input type="text" id="_twittersecret" name="_twittersecret" placeholder="Twitter Token Key" class="span3">
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label"  for="username">Facebook SITE URL:</label>
                            <div class="controls">
                                <input type="text" id="_facebookURL" name="_facebookURL" placeholder="Site URL" class="span3">

                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label"  for="username">LinkedIn WEB URL:Redirect URL:</label>
                            <div class="controls">
                                <input type="text" id="_linkedinURL" name="_linkedinURL" placeholder="LinkedIn Web URL" class="span3">
                                :<input type="text" id="_linkedinAccessURL" name="_linkedinAccessURL" placeholder="Access Redirect URL" class="span3">

                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label"  for="username">Twitter Web URL:CallBack URL</label>
                            <div class="controls">
                                <input type="text" id="_twitterURL" name="_twitterURL" placeholder="Twitter Web URL" class="span3">
                                :<input type="text" id="_twitterCallbackURL" name="_twitterCallbackURL" placeholder="Twitter Callback URL" class="span3">
                            </div>
                        </div>


                        <hr>
                        <div class="control-group">
                            <label class="control-label"  for="username">Additional Attributes</label>
                            <div class="controls">
                                <input type="text" id="_reserve1" name="_reserve1" placeholder="name1=value1" class="span3">
                                : <input type="text" id="_reserve2" name="_reserve2" placeholder="any setting" class="span3">
                                : <input type="text" id="_reserve3" name="_reserve3" placeholder="parameters" class="span3">
                            </div>
                        </div>

                        <!-- Submit -->
                        <div class="control-group">
                            <div class="controls">
                                <div id="save-social-gateway-primary-result"></div>
                                <%// if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
                                <button class="btn btn-primary" onclick="EditSocialSetting(1)" type="button">Save Setting Now >> </button>
                                <%//}%>
                                <!--<button class="btn" onclick="LoadTestSocialConnectionUI(1)" type="button">Test Connection >> </button>-->
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    
    <script language="javascript" type="text/javascript">
                                                LoadSocialSetting(1);
//                                                LoadSMSSetting(2);
    </script>
</div>
<%@include file="footer.jsp" %>