
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.common.utils.UtilityFunctions"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.settings.ContactTagsSetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Templates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/dictum/bulkmsg.js"></script>
<div class="container-fluid">
    <h2>Send Bulk Message over Facebook </h2>
    <p>You can send facebook notification to large set of contacts at different s</p>
    <br>
    <div class="row-fluid">
        <form class="form-horizontal" id="bulkmsg" name="bulkmsg">
            <fieldset>
                <%
                    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
                    Channels channelOnj = (Channels) session.getAttribute("_apSChannelDetails");
//                    TagsManagement tagmngObj = new TagsManagement();
//                    Tags[] tagsObj = tagmngObj.getAllTags(channelOnj.getChannelid());

                    //SettingsManagement smng = new SettingsManagement();
                    //ContactTagsSetting tagssettingObj = (ContactTagsSetting) smng.getSetting(sessionId, channelOnj.getChannelid(), SettingsManagement.CONTACT_TAGS, SettingsManagement.PREFERENCE_ONE);

                    //String[] tags = tagssettingObj.getTags();


                    TemplateManagement tempObj = new TemplateManagement();
                    Templates[] templates = tempObj.Listtemplates(sessionId, channelOnj.getChannelid());

                %>


                            
                  <div class="control-group">
                    <label class="control-label"  for="sendingOption">Send As</label>
                    <div class="controls">
                        <select name="_sendingOption" id="_sendingOption">
                            <option value=".." >..</option>                                             
                            <option value="1" >Private Message</option>
                            <option value="2" >Public Message</option>
                           
                        </select>
                    </div>
                </div>                


                <div class="control-group">
                    <label class="control-label"  for="toNumber">Choose Template</label>
                    <div class="controls">
                        <select class="selectpicker" name="_templateID" id="_templateID" onchange="LoadTemplateData()">
                            <option value=".." >..</option>                                             

                            <% for (int j = 0; j < templates.length; j++) {

                                    Templates t = templates[j];
                                    int iType = t.getType();

                                    if (iType == 2) {
                                        continue;
                                    }

                                    ByteArrayInputStream bais = new ByteArrayInputStream(t.getTemplatebody());
                                    String templatebody = (String) UtilityFunctions.deserializeFromObject(bais);




                            %>
                            <option value="<%=templatebody%>" ><%=t.getTemplatename()%></option>                                             
                            <%
                                }%>
                        </select>

                    </div>
                </div>        


                <div class="control-group">
                    <label class="control-label"  for="speed">Speed</label>
                    <div class="controls">
                        <select name="_speed" id="_speed">
                            <option value=".." >..</option>                                             
                            <option value="1" >Slow</option>
                            <option value="3" >Normal</option>
                            <option value="10" >Fast</option>                            
                            <option value="30" >Hyper Fast</option>                            
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"  for="messagebody">Message</label>
                    <div class="controls">
                        <textarea id="_messagebody" name="_messagebody" placeholder="Message Body" class="span9" cols="60" rows="3"></textarea>
                        <br>Attention: #name#,#date#,#datetime#,#email# and #phone# are going to get replaced with real values.
                    </div>
                </div>

                <!-- Submit -->
                <div class="control-group">
                    <div class="controls">
                        <button class="btn btn-primary btn-large" onclick="notificationblast()" type="button">Send Bulk Notifications Now >></button>
                        <div id="bulk-sms-gateway-result"></div>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>


    <script language="javascript" type="text/javascript">
                            ChangeSpeed(1);
                            //  ChangeFormat(1);
                            ChangeSending(1);
    </script>

    <%@include file="footer.jsp" %>