<%@include file="header.jsp" %>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<script src="./assets/js/securephrase.js"></script>

<div class="container-fluid" id="secureAuditTable">
    <h1 class="text-success">User Secure Phrase Management</h1>
    <p>List of users in the system. Their password management can be managed from this interface.</p>
    <h3>Search Users</h3>   
    <div class="input-append">
        <form id="userSecurePhrase" name="userSecurePhrase">
            <input type="text" id="_keyword" name="_keyword" placeholder="Search using name, phone, email..." class="span4"><span class="add-on"><i class="icon-search"></i></span>
            <a href="#" class="btn btn-success" onclick="searchUsersPhrases()">Search Now</a>
        </form>
    </div>


    <div id="users_secure_phrase_table_main">
    </div>
</p>
<br>
<div id="securePhraseAuditDowload" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idauditDownload"></div></h3>
        <h3 id="myModalLabel">Download Audit</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="securePhraseForm" name="securePhraseForm">
                <input type="hidden" id="_userIDSecure" name="_userIDSecure"/>
                <input type="hidden" id="_userNameSecure" name="_userNameSecure"/>
                <fieldset>
                    <!-- Name -->


                    <div class="control-group">
                        <label class="control-label"  for="username">Start Date</label>
                        <div class="controls" align="left" >

                            <!--<span class="add-on">From:</span>-->   
                            <div id="Pushdatetimepicker1" class="input-append date">
                                <input id="_auditStartDate" name="_auditStartDate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">End Date</label>
                        <div class="controls" align="left">
                            <!--<span class="add-on">Till:</span>-->   
                            <div id="Pushdatetimepicker2" class="input-append date">
                                <input id="_auditEndDate" name="_auditEndDate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>                    
                </fieldset>
            </form>
        </div>
    </div>

    <div class="modal-footer">
        <div id="editoperator-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="searchSecurePhraseAudit()" id="buttonEditOperatorSubmit">Show Audit</button>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#Pushdatetimepicker1').datepicker({
            format: 'dd/MM/yyyy',
            language: 'pt-BR'
        });
    });
    $(function () {
        $('#Pushdatetimepicker2').datepicker({
            format: 'dd/MM/yyyy',
            language: 'pt-BR'
        });
    });
//                            ChangeMediaType(0);
</script>
</div>

<%@include file="footer.jsp" %>