<%@page import="java.util.Calendar"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.bouncycastle.util.encoders.Base64"%>
<%@page import="com.mollatech.axiom.nucleus.db.Audit"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.AuditManagement"%>
<%@include file="header.jsp" %>

<div class="container-fluid">
    <div id="auditTable">
        <h1 class="text-success">Audit</h1>
        <p>Search result for audit integrity test. </p>
        <br>
        <div class="row-fluid">
            <div id="licenses_data_table">
                <table class="table table-striped">
                    <tr>
                        <td>No.</td>
                        <td>Integrity Id</td>
                        <td>Integrity Status</td>
                        <td>Remote Access</td>
                        <td>IP</td>
                        <td>Operator</td>
                        <td>Category</td>
                        <td>Item Type</td>
                        <td>Action</td>
                        <td>Result</td>
                        <!--<td>New</td>-->
                        <td>Time</td>
                    </tr>
                    <%    AuditManagement audit = new AuditManagement();
                        Channels _apSChannelDetails = (Channels) session.getAttribute("_apSChannelDetails");
                        String _IntegrityID = request.getParameter("_IntegrityID");
                        String _status = request.getParameter("_status");
                        Audit auditObj = audit.getAuditrailByIntegrityId(_apSChannelDetails.getChannelid(), _IntegrityID);
                        if (auditObj != null) {

                            String strStatus = "Failed";
                            if (_status.equals("1")) {
                                strStatus = "Success";
                            }
                    %>
                   <tr>
                        <td>1</td>
                        <td><%=auditObj.getIntegritycheck()%></td>
                        <td><%=strStatus%></td>
                        <td><%=auditObj.getRemoteaccesslogin()%></td>
                        <td><%=auditObj.getIpaddress()%></td>
                        <td><%=auditObj.getOperatorname()%></td>
                        <td><%=auditObj.getCategory()%></td>
                        <td><%=auditObj.getItemtype()%></td>
                        <td><%=auditObj.getAction()%></td>
                        <td><%=auditObj.getResult()%></td>
                        <td><%=auditObj.getAuditedon()%></td>
                    </tr>
                    <%} else {%>
                    <tr>
                        No record found!!!
                    </tr>
                    <%}%>
                </table>


            </div>
        </div>
        <br>

        <script language="javascript">
            //listChannels();
        </script>
    </div>
</div>



<%@include file="footer.jsp" %>
