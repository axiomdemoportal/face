<%@page import="com.mollatech.axiom.nucleus.db.connector.management.GroupManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Countrylist"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.LocationManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserGroupsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Usergroups"%>
<%@page import="com.mollatech.axiom.nucleus.settings.SendNotification"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/units.js"></script>
<script src="./assets/js/mobiletrust.js"></script>
<script src="./assets/js/ajaxfileupload.js"></script>
<script src="./assets/js/bootstrap-fileupload.js"></script> 
<link href="./assets/css/bootstrap.css" rel="stylesheet">
<link href="./assets/css/bootstrap-fileupload.css" rel="stylesheet">
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>


<!--<link href="./assets/js/select2/select2.css" rel="stylesheet"/>-->
<!--<script src="./assets/js/select2/select2.js"></script>-->
<!--<script src="./assets/js/globalsettingv2.js"></script>-->
<script src="./ckeditor/ckeditor.js"></script>
<script src="./ckeditor/config.js"></script>
<script src="./ckeditor/styles.js"></script>

<%    String _sessionID = (String) session.getAttribute("_apSessionID");

    SessionManagement smObj = new SessionManagement();
    TemplateManagement tempObj = new TemplateManagement();
//     OperatorsManagement s = new OperatorsManagement();
    int iStatus = smObj.GetSessionStatus(_sessionID);
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    OperatorsManagement oMngt = new OperatorsManagement();
    OTPTokenManagement otpMngt = new OTPTokenManagement(channel.getChannelid());
    SettingsManagement sMngt = new SettingsManagement();
    SendNotification send = new SendNotification();
    Usergroups[] groupsObj = null;
    if (iStatus == 1) { //active
        smObj.UpdateSession(_sessionID);
        UserGroupsManagement cmObj = new UserGroupsManagement();
        groupsObj = cmObj.ListGroups(_sessionID, channel.getChannelid());
        cmObj = null;
    }
%>
<div class="container-fluid">
    <h1 class="text-success">User Groups</h1>
    <div class="row-fluid">
        <div id="licenses_data_table">
            <table class="display responsive wrap" id="UsersGroupTable">
                <thead>
                    <tr>
                        <td><b>No</td>
                        <td><b>Group Name</td>
                        <td><b>Manage</td>
                        <td><b>Make Default</td>
                        <td><b>Total Users</td>
                        <td><b>Country</td>
                        <td><b>State</td>
                        <td><b>Created On</td>
                        <td><b>Last Updated On</td>

                    </tr>
                </thead>

                <%
                    out.flush();
//                    UnitsManagemet tmObj = new UnitsManagemet();
                    if (groupsObj != null) {
                        for (int i = 0; i < groupsObj.length; i++) {

                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            String strStatus = "Active";
                            if (groupsObj[i].getStatus() == 1) {
                                strStatus = "Active";
                            } else {
                                strStatus = "Suspended";
                            }
                            String userStatus = "user-status-value-" + i;

                            UserManagement uManagement = new UserManagement();
                            AuthUser[] aUser = uManagement.getAllUserByGroup(sessionid, channel.getChannelid(), groupsObj[i].getGroupid());
                            int countOfUser = 0;
                            if (aUser != null) {
                                countOfUser = aUser.length;
                            }

                            LocationManagement lManagement = new LocationManagement();
                            String conuntry = "Not Available";
                            String state = "Not Available";
                            String createdOn = "Not Available";
                            String updatedOn = "Not Available";

                            if (groupsObj[i] != null) {

                                if (groupsObj[i].getCountry() != null && groupsObj[i].getCountry().equals("") == false) {
                                    Countrylist cList = lManagement.getCountriesByid(Integer.parseInt(groupsObj[i].getCountry()));
                                    conuntry = cList.getCountyName();
                                }
                                if (groupsObj[i].getState() != null && groupsObj[i].getState().equals("") == false) {
                                    state = groupsObj[i].getState();
                                }
                                createdOn = sdf.format(groupsObj[i].getCreatedOn());
                                updatedOn = sdf.format(groupsObj[i].getLastupOn());
                            }


                %>
                <input type="hidden" id="_isBlackListed" name="_isBlackListed">
                <tr>
                    <td><%=(i + 1)%></td>
                    <td><%=groupsObj[i].getGroupname()%></td>

                    <%//if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
                    <td>
                        <div class="btn-group">
                            <button class="btn btn-mini" id="<%=userStatus%>"><%=strStatus%></button>
                            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">

                                <li><a href="#"  onclick="changeGroupStatus('<%=groupsObj[i].getGroupname()%>', 1, '<%=userStatus%>')" >Mark as Active?</a></li>
                                <li><a href="#" onclick="changeGroupStatus('<%=groupsObj[i].getGroupname()%>', 0, '<%=userStatus%>')" >Mark as Suspended?</a></li>
                                <li class="divider"></li>
                                <li><a href="#" onclick="loadEditGroupDetails('<%=groupsObj[i].getGroupname()%>')">Edit Details</a></li>
                                <li><a href="#" onclick="loadShiftGroupDetails('<%=groupsObj[i].getGroupname()%>', '<%=groupsObj[i].getGroupid()%>')">Shift Group's Users</a></li>
                                <!--<li><a href="#" onclick="restrictLocation('<%=groupsObj[i].getGroupname()%>', '<%=groupsObj[i].getGroupid()%>')">Restrict Location</a></li>-->
                                <!--<li><a href="#" onclick="loadShiftUnitDetails('<%=groupsObj[i].getGroupname()%>','<%=groupsObj[i].getGroupid()%>')">Shift Unit's Operator</a></li>-->
                                <li><a href="#" onclick="removeGroup('<%=groupsObj[i].getGroupname()%>')"><font color="red">Remove Group?</font></a></li>

                            </ul>
                        </div>
                    </td>
                    <%// }%>
                    <%
                        if (groupsObj[i].getDefaultFlag() == 1) { %>
                    <td><a href="#" class="btn btn-mini btn-info" >Default Group</a></td>
                    <% } else {
                    %>
                    <td><a href="#" class="btn btn-mini " onclick="MakeItDefault('<%=groupsObj[i].getGroupname()%>', '<%=groupsObj[i].getGroupid()%>')">Make Default</a></td>
                    <%
                        }
                    %>

                    <td><%=countOfUser%></td>
                    <td><%=conuntry%></td>
                    <td><%=state%></td>
                    <td><%=createdOn%>
                    <td><%=updatedOn%></td>

                </tr>
                <%}
                    }%>
            </table>
            <p><a href="#addNewUnit" class="btn btn-primary" data-toggle="modal">Add New Group&raquo;</a></p>
        </div>
    </div>
    <br>

    <script language="javascript">
        //listChannels();
    </script>
</div>

<!-- Modal -->
<div id="addNewUnit" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Add New Group</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="AddNewUnitForm" name="AddNewUnitForm">

                <!-- Name -->
                <div class="control-group">
                    <label class="control-label"  for="username">Name</label>
                    <div class="controls">
                        <input type="text" id="_unitName" name="_unitName" placeholder="set unique name for login" class="input-xlarge">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"  for="username">Status</label>
                    <div class="controls">
                        <select class="span4" name="_unitStatus" id="_unitStatus">
                            <option value="1">Active</option>
                            <option value="0">Suspended</option>
                        </select>
                    </div>
                </div>
                <div id="monitors">
                    <div class="control-group">
                        <label class="control-label"  for="username">Restriction Level</label>
                        <div class="controls">
                            <select name="_selectedType" id="_selectedType" class="span6">
                                <option value='No Restriction' id="No Restriction"> No Restriction </option>
                                <option value='Country' id="Country" > Country </option>
                                <option value='State' id="State"> State</option>
                                <!--<option value='3' > Server Monitor </option>-->
                                <option value='City' id="City"> City </option>
                                <option value='Pincode' id="Pincode"> Pincode </option>

                            </select>
                        </div>
                    </div>
                </div>
                <!--                <div id="CheckThrough">
                                    <div class="control-group">
                                        <label class="control-label"  for="username">Check Through</label>
                                        <div class="controls">
                                            <select name="_selectedCheck" id="_selectedCheck" class="span4">
                                                <option value='Allow' id="Allow"> Allow </option>
                                                <option value='Disallow' id="Disallow" > Dis-Allow </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>-->

                <div class="control-group" id="country">
                    <label class="control-label"  for="username">Home Country</label>
                    <div class="controls">
                        <select class="span10" name="_CountryName" id="_CountryName">
                            <%
                                //getAll conotries here 
                                LocationManagement lManagement = new LocationManagement();
                                int countryCode = -1;
                                Countrylist[] countryList = lManagement.getAllCountries();
                                if (countryList != null) {
                                    for (int j = 0; j < countryList.length; j++) {
                                        String strcode = countryList[j].getCountyName();
                                        countryCode = countryList[j].getCountryid();
                            %>
                            <option value="<%=countryCode%>"><%=strcode%></option>

                            <%}
                                }%>
                        </select>
                    </div>
                </div>
                <div name="_StateName" id="_StateName">                           
                </div>
                <div id="_City">
                    <div class="control-group">
                        <label class="control-label"  for="username">Cities </label>
                        <div class="controls">
                            <textarea name="_cityName" id="_cityName" class="span10"  >
                                                          ,                         
                            </textarea>

                        </div>
                    </div>
                </div>
                <div id="_Pin">
                    <div class="control-group">
                        <label class="control-label"  for="username">Pin codes </label>
                        <div class="controls">
                            <textarea name="_PinCode" id="_PinCode" class="span10"  >
                                                          ,                         
                            </textarea>

                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="add-new-unit-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <!--<div class="span3" id="add-new-unit-result"></div>-->
        <button class="btn btn-primary" onclick="addGroup()" id="addnewUnitSubmitBut">Add New Group</button>
    </div>
</div>

<div id="editGroups"></div>
<div id="shiftGroups" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idshiftUnitName"></div>Shift Group's Users</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="shiftGroupForm" name="shiftGroupForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Old Group Name</label>
                        <div class="controls">
                            <input type="text" readonly id="_oldGroupNameS" name="_oldGroupNameS" placeholder="Unit Name" class="input-xlarge">
                            <!--                            <input type="hidden" id="_oprroleidE" name="_oprroleidE"/>
                                                        <input type="hidden" id="_oprstatusE" name="_oprstatusE"/>-->
                            <input type="hidden" id="_unitIdS" name="_unitIdS"/>
                            <!--<input type="hidden" id="_oldunitNameS" name="_oldunitNameS"/>-->
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">New Group Name</label>
                        <div class="controls">
                            <select class="span4" name="_unitS" id="_unitS">
                                <%
                                    GroupManagement uMngt = new GroupManagement();
                                    Usergroups[] uList = uMngt.ListGroups(sessionid, channel.getChannelid());
                                    if (uList != null) {
                                        for (int i = 0; i < uList.length; i++) {
                                %>
                                <option value="<%=uList[i].getGroupid()%>"><%=uList[i].getGroupname()%></option>
                                <%
                                        }
                                    }
                                %>
                            </select>
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="shiftunit-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>

        <button class="btn btn-primary" onclick="shiftGroup()" id="buttonShiftUnitSubmit">Save Changes</button>

    </div>
</div>

<script>
    $(document).ready(function () {
        document.getElementById("_StateName").style.display = 'none';
        document.getElementById("country").style.display = 'none';
        document.getElementById("_City").style.display = 'none';
        document.getElementById("_Pin").style.display = 'none';
    });
    $(document).ready(function () {
        $("#_cityName").select2({
            tags: [],
            dropdownCss: {display: 'none'},
            tokenSeparators: [","]
        });
    });
    $(document).ready(function () {
        $("#_PinCode").select2({
            tags: [],
            dropdownCss: {display: 'none'},
            tokenSeparators: [","]
        });
    });
    $(document).ready(function () {
//  var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
        $('#_CountryName').change(function () {
            var selectedValue = $(this).val();
            // alert(selectedValue);
            var s = './stateList.jsp?_countryId=' + selectedValue;
            $.ajax({
                type: 'GET',
                url: s,
                success: function (data) {

                    $('#_StateName').html(data);
                }
            });
        });
    });
    $('#_selectedTypeE').change(function () {
        // alert('hi');
        var selectedValue = $(this).val();
        if (selectedValue === 'No Restriction')
        {
            document.getElementById("countryE").style.display = 'none';
            document.getElementById("_StateNameE").style.display = 'none';
            document.getElementById("_CityE").style.display = 'none';
            document.getElementById("_PinE").style.display = 'none';
        } else if (selectedValue === 'Country')
        {
            document.getElementById("countryE").style.display = 'block';
            document.getElementById("_StateNameE").style.display = 'none';
            document.getElementById("_CityE").style.display = 'none';
            document.getElementById("_PinE").style.display = 'none';
        } else if (selectedValue === 'State')
        {
            document.getElementById("countryE").style.display = 'block';
            document.getElementById("_StateNameE").style.display = 'block';
            document.getElementById("_CityE").style.display = 'none';
            document.getElementById("_PinE").style.display = 'none';
        } else if (selectedValue === 'City')
        {
            document.getElementById("countryE").style.display = 'block';
            document.getElementById("_StateNameE").style.display = 'block';
            document.getElementById("_CityE").style.display = 'block';
            document.getElementById("_PinE").style.display = 'none';
        } else if (selectedValue === 'Pincode')
        {
            document.getElementById("countryE").style.display = 'block';
            document.getElementById("_StateNameE").style.display = 'block';
            document.getElementById("_PinE").style.display = 'block';
            document.getElementById("_CityE").style.display = 'none'
        }

    });

</script>

<script>
    $(document).ready(function () {
        $('#UsersGroupTable').DataTable({
            responsive: true
        });
    });
</script>

<%@include file="footer.jsp" %>