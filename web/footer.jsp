<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%
    Date dFooter = new Date();
    SimpleDateFormat sdfFooter = new SimpleDateFormat("yyyy");
    String strYYYY = sdfFooter.format(dFooter);

    long LongTime = dFooter.getTime() / 1000;
    SimpleDateFormat tz = new SimpleDateFormat("EEE dd MMM yyyy HH:mm:ss zzz ");
    String completeTimewithLocalTZ = tz.format(dFooter);
%>

<hr>
<script src="./assets/js/footer.js"></script>
<div id="ChangePassword" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�(close)</button>
        <h3 id="myModalLabel">Change Password</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="ChangePasswordform">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_userIDS" name="_userIDS" >
                    <div class="control-group">
                        <label class="control-label"  for="username">Current Password</label>
                        <div class="controls">
                            <input type="password" id="_oldPassword" name="_oldPassword" placeholder="Current Password" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">New Password</label>
                        <div class="controls">
                            <input type="password" id="_newPassword" name="_newPassword" placeholder="New Password" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Confirm Password</label>
                        <div class="controls">
                            <input type="password" id="_confirmPassword" name="_confirmPassword" placeholder="Confirm Password" class="input-xlarge">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="Password-result"></div>
        <button class="btn btn-primary" onclick="changeoperatorpassword()" id="passwordChange" type="button">Change</button>
    </div>
</div>
<div id="CheckAuditIntegrity" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�(close)</button>
        <h3 id="myModalLabel">Audit Integrity</h3>
    </div>
    <div class="row-fluid">    <div class="modal-body">

            <form class="form-horizontal" id="CheckIntegrityform">
                <fieldset>
                    <!-- Name -->
                    <!--<input type="hidden" id="_userIDS" name="_userIDS" >-->
                    <div class="control-group">
                        <label class="control-label"  for="username">Integrity ID</label>
                        <div class="controls">
                            <input type="text" id="_IntegrityID" name="_IntegrityID" placeholder="Integrity Id" class="input-xlarge">
                            <!--                                    <textarea id="txtareaauditRecord" name="txtareaauditRecord" cols="20" rows="4" style="display:none"></textarea>-->
                        </div>               <div id="txtareaauditRecord" style="display:none">
                            <br>
                            <label class="control-label"> Session Id </label>
                            <div class="controls"><input type="text" class="input-xlarge" id="sessionId" placeholder="NA"></div>
                            <br>    
                            <label class="control-label">channel Id</label>
                            <div class="controls"><input type="text" class="input-xlarge" id="channelId" placeholder="NA">
                            </div> 
                            <br> 
                            <label class="control-label">Operator Id</label>
                            <div class="controls">
                                <input type="text" class="input-xlarge" id="OperatorId" placeholder="NA">
                            </div>

                            <br> <label class="control-label">Ip Address</label>
                            <div class="controls">
                                <input type="text" class="input-xlarge" id="IpAddress" placeholder="NA">
                            </div>
                            <br> <label class="control-label">Channel Name</label>
                            <div class="controls">
                                <input type="text" class="input-xlarge" id="ChannelName" placeholder="NA">
                            </div>
                            <br> <label class="control-label">Remoteaccesslogin</label>
                            <div class="controls">
                                <input type="text" class="input-xlarge" id="Remoteaccesslogin" placeholder="NA">
                            </div>
                            <br> <label class="control-label">Time</label>
                            <div class="controls">
                                <input type="text" id="Time" class="input-xlarge" placeholder="NA">
                            </div>
                            <br> <label class="control-label">Action</label>
                            <div class="controls">
                                <input type="text" id="Action" class="input-xlarge" placeholder="NA">
                            </div>
                            <br> <label class="control-label">Result</label>
                            <div class="controls"><input type="text" id="Result" class="input-xlarge" placeholder="NA">
                            </div>
                            <br> <label class="control-label">Resultcode</label>
                            <div class="controls">
                                <input type="text" class="input-xlarge" id="Resultcode" placeholder="NA">
                            </div>
                            <br> <label class="control-label">Category</label>
                            <div class="controls"><input type="text" class="input-xlarge" id="Category" placeholder="NA">
                            </div>
                            <br> <label class="control-label">New value</label>
                            <div class="controls">
                                <input type="text" class="input-xlarge" id="Newvalue" placeholder="NA">
                            </div>
                            <br> <label class="control-label">Old value</label>
                            <div class="controls">
                                <input type="text" class="input-xlarge" id="Oldvalue" placeholder="NA">
                            </div>
                            <br> <label class="control-label">Itemtype</label>
                            <div class="controls">
                                <input type="text" class="input-xlarge" id="Itemtype" placeholder="NA">
                            </div>
                            <br> <label class="control-label">Itemid</label>
                            <div class="controls">
                                <input type="text" class="input-xlarge" id="Itemid" placeholder="NA">
                            </div>


                        </div>
                        <!--                            <textarea id="txtareaauditRecord" name="txtareaauditRecord" cols="20" rows="4" style="display:none"></textarea>-->

                    </div>

                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="Integrity-result"></div>
        <button class="btn btn-primary" onclick="checkAuditIntigrity()" id="IntegrityButton" type="button">Check Now>></button>
    </div>
</div>
<script>
    setInterval(function () {
        checkValidSession()
    }, 15000);
    setInterval(function () {
        checkServer()
    }, 15000);
</script>
<!--<button onclick="geoFindMe()">hit</button>-->
<footer>
    <div align="center">
        <a class="brand" id="ansn" onClick="window.print()"><i class="icon-print"></i></a>
        <p><small> &copy; Molla Technologies 2009-<%=strYYYY%></small></p>
        <p>Local Date and Time::<%=completeTimewithLocalTZ%> (<%=LongTime%>)</p>                
    </div>
</footer>
<!--    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-73101570-1', 'auto');
  ga('send', 'pageview');

</script>-->
</body>
</html>


