<%@page import="com.mollatech.axiom.nucleus.db.DocumentTemplate"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.DocsTemplatesManagement"%>
<%@include file="header.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script src="./assets/js/template.js"></script>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <%
            String startDate = request.getParameter("startdate1");
            String endDate = request.getParameter("enddate1");
            String templateName = request.getParameter("_templateName");
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            DocsTemplatesManagement docsObj = new DocsTemplatesManagement();
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date _startDate = formatter.parse(startDate);
            Date _endDate = formatter.parse(endDate);
            OperatorsManagement opObj = new OperatorsManagement();
            DocumentTemplate[] tempObj = docsObj.getTemplateByDateNname(_startDate, _endDate, templateName);

        %>
        <div class="container-fluid">
            <div class="row-fluid">
                <form id="templateData" name="templateData">
                    <table class="table table-bordered table-striped text-center" id="table_main">
                        <thead >
                            <tr>
                                <th>No.</th>
                                <th>Template Name</th>
                                <th>Created By</th>
                                <th>Created On</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <%                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                            if (tempObj != null) {
                                for (int i = 0; i < tempObj.length; i++) {
                                    Operators operatorObj = opObj.getOperatorById(channel.getChannelid(), tempObj[i].getOperatorId());

                        %>
                        <tr>
                            <td><%=i + 1%></td>
                            <td><%=tempObj[i].getTemplateName()%></td>
                            <td><%=operatorObj.getName()%></td>
                            <td><%=sdf.format(tempObj[i].getCreatedOn())%></td>
                            <td text-center>
                                <a href='editTemplate.jsp?templateName=<%=tempObj[i].getTemplateName()%>'>
                                <button type="button" class="btn btn-primary btn-xs">Edit</button>
                                 </a>
                                <a href='displayTemplate.jsp?templateName=<%=tempObj[i].getTemplateName()%>'>
                                <button type="button" class="btn btn-primary btn-xs">show</button>
                                </a>
                                <button type="button" onclick="deleteTemplate('<%=tempObj[i].getTemplateId()%>')" class="btn btn-primary btn-xs">delete</button>
                            </td>
                        </tr>
                        <%}
                            }%>
                    </table>
                </form>
            </div>
        </div>
    </body>
    <script>
        
        
    </script>
</html>
