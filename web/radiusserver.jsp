<%@include file="header.jsp" %>
<div class="container-fluid">
    <h2>RADIUS Server Configuration</h2>
    <p>To facilitate RADIUS support for authentication, AXIOM Protect exposes RADIUS interface that can be used for RADIUS compliant systems like VPN etc.</p>
    <br>
    <div class="row-fluid">
        <form class="form-horizontal" id="primaryform">
            <fieldset>
                <div id="legend">
                    <legend class=""><input type="checkbox" id="_accountEnabled" value="option1"> Accounting Details </legend>
                </div>

                <!-- Name -->
                <div class="control-group">
                    <label class="control-label"  for="username">Host/IP : Port </label>
                    <div class="controls">
                        <input type="text" id="_accountIp" name="_accountIp" placeholder="example localhost/127.0.0.1" class="input-xlarge">
                        : <input type="text" id="_accountPort" name="_accountPort" placeholder="443" class="span2">
                    </div>
                </div>

                <div id="legend">
                    <legend class=""><input type="checkbox" id="_authEnabled" name="_authEnabled" value="option1"> Authentication Details </legend>
                </div>

                <!-- Name -->
                <div class="control-group">
                    <label class="control-label"  for="username">Host/IP : Port </label>
                    <div class="controls">
                        <input type="text" id="_authIp" name="_authIp" placeholder="example localhost/127.0.0.1" class="input-xlarge">
                        : <input type="text" id="_authPort" name="_authPort" placeholder="443" class="span2">
                    </div>
                </div>

                <div id="legend">
                    <legend class=""> Password Validation</legend>
                </div>

                <div class="control-group">
                    <label class="control-label" for="password">Validation Source</label>
                    <div class="controls">
                        <select class="span4" name="expiry_month" id="expiry_month">
                            <option value="ldap">Use LDAP/Active Directory as Soruce</option>
                            <option value="axiom">Use AXIOM User Repository as Soruce</option>
                        </select>
                    </div>
                </div>


                <div class="control-group">
                    <label class="control-label"  for="username">LDAP Host/IP</label>
                    <div class="controls">
                        <input type="text" id="_ldapServerIp" name="_ldapServerIp" placeholder="example localhost/127.0.0.1" class="input-xlarge">
                        : <input type="text" id="_ldapServerPort" name="_ldapServerPort" placeholder="443" class="span2">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"  for="username">LDAP Search Path</label>
                    <div class="controls">
                        <input type="text" id="_ldapSearchPath" name="_ldapSearchPath" placeholder="leave blank is no authentication" class="input-xlarge">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"  for="username">AXIOM Repository Host/IP</label>
                    <div class="controls">
                        <input type="text" id="_axiomServerIp" name="_axiomServerIp" placeholder="example localhost/127.0.0.1" class="input-xlarge">
                        : <input type="text" id="_axiomServerPort" name="_axiomServerPort" placeholder="443" class="span2">
                    </div>
                </div>


                <div id="legend">
                    <legend class=""> RADIUS Client</legend>
                </div>
                <div class="control-group">
                    <label class="control-label"  for="username">Client Authentication Type</label>
                    <div class="controls">
                        <select class="span4" name="expiry_month" id="expiry_month">
                            <option value="password">Password Only</option>
                            <option value="otp">OTP Only</option>
                            <option value="both">Password+OTP </option>
                            <option value="followup">Password followed by OTP </option>
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"  for="username">Radius Client</label>
                    <div class="controls">
                        <input type="text" id="_radiusClientIp" name="_radiusClientIp" placeholder="example localhost/127.0.0.1" class="input-xlarge">                        
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="password">Client Shared Secret </label>
                    <div class="controls">
                        <input type="password" id="_radiusClientSecretkey" name="_radiusClientSecretkey" placeholder="leave blank is no authentication" class="input-xlarge">
                    </div>
                </div>

                <!-- Submit -->
                <div class="control-group">
                    <div class="controls">
                        <button class="btn btn-primary" onclick="editradiussettings()" type="button">Save Setting Now >> </button>                        
                    </div>
                </div>

            </fieldset>
        </form>
    </div>

</div>

<script src="./assets/js/jquery.js"></script>
    <script src="./assets/js/bootstrap-transition.js"></script>
    <script src="./assets/js/bootstrap-alert.js"></script>
    <script src="./assets/js/bootstrap-modal.js"></script>
    <script src="./assets/js/bootstrap-dropdown.js"></script>
    <script src="./assets/js/bootstrap-scrollspy.js"></script>
    <script src="./assets/js/bootstrap-tab.js"></script>
    <script src="./assets/js/bootstrap-tooltip.js"></script>
    <script src="./assets/js/bootstrap-popover.js"></script>
    <script src="./assets/js/bootstrap-button.js"></script>
    <script src="./assets/js/bootstrap-collapse.js"></script>
    <script src="./assets/js/bootstrap-carousel.js"></script>
    <script src="./assets/js/bootstrap-typeahead.js"></script>
    <script src="./assets/js/login.js"></script>
    
<script src="./assets/js/radiusserver.js"></script>

<script type="text/javascript">
    radiussettings()
</script>


<%@include file="footer.jsp" %>