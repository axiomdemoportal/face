<%@page import="com.mollatech.axiom.nucleus.db.Operators"%>
<%@page import="com.mollatech.axiom.connector.access.controller.AccessMatrixSettings"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Audit"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.AuditManagement"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.MobileTrustManagment"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Geotrack"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.GeoLocationManagement"%>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Random" %>

<%    int REGISTER = 1;
    int LOGIN = 2;
    int TRANSACTION = 3;
    int CHANGEINPROFILE = 4;

    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    String _startdate = request.getParameter("_startdate");
    String _enddate = request.getParameter("_enddate");
//    String _type = request.getParameter("_type");
    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    Date startDate = null;
    if (_startdate != null && !_startdate.isEmpty()) {
        startDate = (Date) formatter.parse(_startdate);
    }
    Date endDate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        endDate = (Date) formatter.parse(_enddate);
    }
//    GeoLocationManagement geoObj = new GeoLocationManagement();
//    Geotrack[] Tracks = geoObj.getGeoTracksByTxTypeDuration(channel.getChannelid(), MobileTrustManagment.INVALIDIMAGE, endDate, startDate);

//    _type = "VERIFY OTP";
//    String itemType1 = "Verify SOTP";
    String _type = request.getParameter("_changeCategory");
    String itemType1 = null;

    AuditManagement auditObj = new AuditManagement();
    UserManagement user = new UserManagement();
    Audit[] arraudit;
    if(_type.equalsIgnoreCase("OcrDocumentation")){
        arraudit = auditObj.searchAuditObjTokenFailureForHdfc(channel.getChannelid(), _type, itemType1, startDate, endDate);
    }else{
     arraudit = auditObj.searchAuditObjTokenFailure(channel.getChannelid(), _type, itemType1, startDate, endDate);
    }
     String strerr = "No Records Found";

//    HttpSession session = request.getSession(true);
//    session.setAttribute("_honeyTrapStartDate", startDate);
//     session.setAttribute("_honeyTrapEndDate", endDate);

%>

<h3>Search Results</h3>

<div class="tabbable" id="REPORT">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#otpcharts1" data-toggle="tab">Charts</a></li>
        <li><a href="#otpreport2" data-toggle="tab">Tabular List</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="otpcharts1">
            <div class="row-fluid">
                <div class="span12">
                    <div class="control-group">
                        <div class="span4">
                            <div id="graph" ></div>

                        </div>
                        <div  class="span8">
                            <div id="graph1"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="otpreport2">  
            <div id="licenses_data_table">                           
                <div class="row-fluid">
                    <div class="span6">
                        <div class="control-group">                        
                            <div class="span1">
                                <div class="control-group form-inline">
                                    <%Operators oprObjI = (Operators) session.getAttribute("_apOprDetail");
                                        AccessMatrixSettings accessObjN = (AccessMatrixSettings) session.getAttribute("_apAccessEntry");
                                        if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                            if (accessObjN.downloadOtpFailureReport == true) {%>
                                    <a href="#"  class="btn btn-info" onclick="tokenFailureReportCSV('<%=_type%>')" >
                                        <%} else {%>
                                        <a href="#"  class="btn btn-info" onclick="InvalidRequestOTPToken('otpfailurereportdownload')" >
                                            <%}
                                            } else {%>
                                            <a href="#"  class="btn btn-info" onclick="tokenFailureReportCSV('<%=_type%>')" >
                                                <%}%>
                                                <!--<a href="#"  class="btn btn-info" onclick="tokenFailureReportCSV()" >-->
                                                <i class="icon-white icon-chevron-down"></i> CSV</a>
                                            </div>
                                            </div>
                                            <div class="span1">
                                                <div class="control-group form-inline">
                                                    <%if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                            if (accessObjN.downloadOtpFailureReport == true) {%>
                                                    <a href="#"  class="btn btn-info" onclick="tokenFailureReportPDF('<%=_type%>')" >
                                                        <%} else {%>
                                                        <a href="#"  class="btn btn-info" onclick="InvalidRequestOTPToken('otpfailurereportdownload')" >
                                                            <%}
                                                            } else {%>
                                                            <a href="#"  class="btn btn-info" onclick="tokenFailureReportPDF('<%=_type%>')" >
                                                                <%}%>
                                                                <!--<a href="#" class="btn btn-info" onclick="tokenFailureReportPDF()" >-->
                                                                <i class="icon-white icon-chevron-down"></i> PDF</a>
                                                            </div>
                                                            </div>
                                                            <div class="span1">
                                                                <div class="control-group form-inline">
                                                                    <%if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                            if (accessObjN.downloadOtpFailureReport == true) {%>
                                                                    <a href="#"  class="btn btn-info" onclick="tokenFailureReportTXT('<%=_type%>')" >
                                                                        <%} else {%>
                                                                        <a href="#"  class="btn btn-info" onclick="InvalidRequestOTPToken('otpfailurereportdownload')" >
                                                                            <%}
                                                                            } else {%>
                                                                            <a href="#"  class="btn btn-info" onclick="tokenFailureReportTXT('<%=_type%>')" >
                                                                                <%}%>
                                                                                <!--<a href="#" class="btn btn-info" onclick="tokenFailureReportTXT()" >-->
                                                                                <i class="icon-white icon-chevron-down"></i> TXT</a>
                                                                            </div>
                                                                            </div>


                                                                            </div>
                                                                            </div>
                                                                            </div>
                                                                            <div id="licenses_data_table">

                                                                                <input type="hidden" id="_tracking" name="_tracking"  >
                                                                                <table class="table table-striped" id="table_main">
                                                                                    <tr>
                                                                                        <td>No.</td>
                                                                                        <td>Name</td>
                                                                                        <td>Session ID</td>
                                                                                        <td>Channel ID</td>
                                                                                        <td>Result Code</td>
                                                                                        <td>Result</td>
                                                                                        <td>Dated</td>
                                                                                    </tr>
                                                                                    <%            if (arraudit != null) {
                                                                                            for (int i = 0; i < arraudit.length; i++) {
                                                                                                SimpleDateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                                                                                                String start = df2.format(arraudit[i].getAuditedon());

                                                                                                //                    String strtype = "";
                                                                                                //                    if (arraudit[i].getResultcode() == -9) {
                                                                                                //                        strtype = "Expired";
                                                                                                //                    } else if (arraudit[i].getResultcode() == -8) {
                                                                                                //                        strtype = "Consumed";
                                                                                                //                    } else if (arraudit[i].getResultcode() == -17) {
                                                                                                //                        strtype = "not found";
                                                                                                //                    } else if (arraudit[i].getResultcode() == -22) {
                                                                                                //                        strtype = "Locked";
                                                                                                //                    } else if (arraudit[i].getResultcode() == -1) {
                                                                                                //                        strtype = "Error";
                                                                                                //                    }
                                                                                                AuthUser aUser = user.getUser(channel.getChannelid(), arraudit[i].getItemid());
                                                                                                String username = "User Not Found";
                                                                                                if (aUser != null) {
                                                                                                    username = aUser.getUserName();
                                                                                                }
                                                                                    %>
                                                                                    <tr>
                                                                                        <td><%=(i + 1)%></td>
                                                                                        <td><%=username%></td>
                                                                                        <td><%=arraudit[i].getSessionId()%></td>
                                                                                        <td><%=arraudit[i].getChannelid()%></td>
                                                                                        <td><%=arraudit[i].getResultcode()%></td>
                                                                                        <td><%=arraudit[i].getResult()%></td>
                                                                                        <td><%=start%></td>
                                                                                    </tr>
                                                                                    <%
                                                                                        }
                                                                                    } else {%>
                                                                                    <tr>
                                                                                        <td><%=1%></td>
                                                                                        <td><%= strerr%></td>
                                                                                        <td><%= strerr%></td>
                                                                                        <td><%= strerr%></td>
                                                                                        <td><%= strerr%></td>
                                                                                        <td><%= strerr%></td>
                                                                                        <td><%= strerr%></td>
                                                                                        <%}%>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                            <div class="row-fluid">
                                                                                <div class="span6">

                                                                                    <div class="control-group">                        
                                                                                        <div class="span1">
                                                                                            <div class="control-group form-inline">
                                                                                                <% if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                        if (accessObjN.downloadOtpFailureReport == true) {%>
                                                                                                <a href="#"  class="btn btn-info" onclick="tokenFailureReportCSV('<%=_type%>')" >
                                                                                                    <%} else {%>
                                                                                                    <a href="#"  class="btn btn-info" onclick="InvalidRequestOTPToken('otpfailurereportdownload')" >
                                                                                                        <%}
                                                                                                        } else {%>
                                                                                                        <a href="#"  class="btn btn-info" onclick="tokenFailureReportCSV('<%=_type%>')" >
                                                                                                            <%}%>
                                                                                                            <!--<a href="#"  class="btn btn-info" onclick="tokenFailureReportCSV()" >-->
                                                                                                            <i class="icon-white icon-chevron-down"></i> CSV</a>
                                                                                                        </div>
                                                                                                        </div>
                                                                                                        <div class="span1">
                                                                                                            <div class="control-group form-inline">
                                                                                                                <%if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                                        if (accessObjN.downloadOtpFailureReport == true) {%>
                                                                                                                <a href="#"  class="btn btn-info" onclick="tokenFailureReportPDF('<%=_type%>')" >
                                                                                                                    <%} else {%>
                                                                                                                    <a href="#"  class="btn btn-info" onclick="InvalidRequestOTPToken('otpfailurereportdownload')" >
                                                                                                                        <%}
                                                                                                                        } else {%>
                                                                                                                        <a href="#"  class="btn btn-info" onclick="tokenFailureReportPDF('<%=_type%>')" >
                                                                                                                            <%}%>
                                                                                                                            <!--<a href="#" class="btn btn-info" onclick="tokenFailureReportPDF()" >-->
                                                                                                                            <i class="icon-white icon-chevron-down"></i> PDF</a>
                                                                                                                        </div>
                                                                                                                        </div>
                                                                                                                        <div class="span1">
                                                                                                                            <div class="control-group form-inline">
                                                                                                                                <%if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                                                        if (accessObjN.downloadOtpFailureReport == true) {%>
                                                                                                                                <a href="#"  class="btn btn-info" onclick="tokenFailureReportTXT('<%=_type%>')" >
                                                                                                                                    <%} else {%>
                                                                                                                                    <a href="#"  class="btn btn-info" onclick="InvalidRequestOTPToken('otpfailurereportdownload')" >
                                                                                                                                        <%}
                                                                                                                                        } else {%>
                                                                                                                                        <a href="#"  class="btn btn-info" onclick="tokenFailureReportTXT('<%=_type%>')" >
                                                                                                                                            <%}%>
                                                                                                                                            <!--<a href="#" class="btn btn-info" onclick="tokenFailureReportTXT()" >-->
                                                                                                                                            <i class="icon-white icon-chevron-down"></i> TXT</a>
                                                                                                                                        </div>
                                                                                                                                        </div>


                                                                                                                                        </div>
                                                                                                                                        </div>
                                                                                                                                        </div> 
                                                                                                                                        </div>
                                                                                                                                        </div>
                                                                                                                                        </div>
                                                                                                                                        </div>
                                                                                                                                        <%--<%@include file="footer.jsp" %>--%>