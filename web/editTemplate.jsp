<%@page import="org.bouncycastle.util.encoders.Base64"%>
<!DOCTYPE html>
<html lang="en">
    <%@page import="java.util.Iterator"%>
    
    <%@page import="java.awt.image.BufferedImage"%>
    <%@page import="java.io.ByteArrayOutputStream"%>
    <%@page import="java.io.File"%>"
    <%@page import="javax.imageio.ImageIO"%> 
    <%@page import="java.io.FileReader"%>
    <%@page import="java.io.BufferedReader"%>
    <%@page import="java.nio.channels.FileChannel"%>
    <%@page import="java.io.FileInputStream"%>
    <%@page import="org.apache.commons.io.FileUtils"%>
    <%@page import="java.io.File"%>
    <%@page import="org.json.JSONObject"%>
    <%@page import="java.io.ByteArrayInputStream"%>
    <%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
    <%@page import="com.mollatech.axiom.nucleus.db.DocumentTemplate"%>
    <%@page import="com.mollatech.axiom.nucleus.db.connector.management.DocsTemplatesManagement"%>

    <link href="assets/OCR/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <script src="assets/dococr/CSSJS/Js/mdb.js" type="text/javascript"></script>
    <script src="assets/dococr/js/jquery.Jcrop.js" type="text/javascript"></script>
    <script src="assets/dococr/js/documentsTemplate.js" type="text/javascript"></script>
    <link href="assets/dococr/css/demo_files/main.css" rel="stylesheet" type="text/css"/>
    <link href="assets/dococr/css/demo_files/demos.css" rel="stylesheet" type="text/css"/>
    <link href="assets/dococr/css/jquery.Jcrop.css" rel="stylesheet" type="text/css"/>
    <div class="card text-center">
        <script type="text/javascript">

            jQuery(function ($) {

                var jcrop_api;

                $('#target').Jcrop({
                    onChange: showCoords,
                    onSelect: showCoords,
                    onRelease: clearCoords
                }, function () {
                    jcrop_api = this;
                });

                $('#coords').on('change', 'input', function (e) {
                    var x1 = $('#x1').val(),
                            x2 = $('#x2').val(),
                            y1 = $('#y1').val(),
                            y2 = $('#y2').val();
                    jcrop_api.setSelect([x1, y1, x2, y2]);
                });
            });

            // Simple event handler, called from onChange and onSelect
            // event handlers, as per the Jcrop invocation above
            function showCoords(c)
            {
                $('#x1').val(c.x);
                $('#y1').val(c.y);
                $('#x2').val(c.x2);
                $('#y2').val(c.y2);
                $('#w').val(c.w);
                $('#h').val(c.h);

            }
            ;

            function clearCoords()
            {
                $('#coords input').val('');
            }
            ;



        </script>
        <%
            String templatename = request.getParameter("templateName");
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            DocsTemplatesManagement docObj = new DocsTemplatesManagement();
            DocumentTemplate docsDetails = docObj.editDocumentDetails(templatename, channel.getChannelid());
            //////////////////////Found Image size/////////////////////
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] image = docsDetails.getProcessImage();
            //String base64image = Base64.getEncoder().encodeToString(image);
            String base64image = "";
        %>

        <h1 class="text-success">Update Template</h1>
        <div class="card-header">
            <!--Tabs-->
            <form id="coords"
                  class="coords"
                  onsubmit="return false;"
                  action="">
                <div class="inline-labels">

                    <%if (docsDetails != null && docsDetails.getTemplateDetails() != null) {
                            docsDetails.getTemplateDetails();
                            JSONObject jsonObj = new JSONObject(docsDetails.getTemplateDetails());
                            Iterator<?> keys = jsonObj.keys();

                    %>
                    <div id="update">
                        <!--For Update-->

                        <select id="datatype" name="datatype"  style="width:150px;height: 30px !important">
                            <option value="num">Numeric</option>
                            <option value="alphanum">Alpha-numeric</option>
                            <option value="alpha">Alphabetic</option>
                        </select>
                        <select name="_cordinateName" id="_cordinateName" onchange="myFunction('<%=templatename%>')">
                            <option value="-1" >Select Co-ordinate</option> 
                            <%                           
                                while (keys.hasNext()) {
                                    String key = (String) keys.next();
                                    System.out.println("object == " + key);
                            %>
                            <option value="<%=key%>" ><%=key%></option> 
                            <%
                                    if (jsonObj.get(key) instanceof JSONObject) {
                                        System.out.println("Shailendra == " + jsonObj.get(key));
                                        jsonObj.get(key).toString();
                                        JSONObject jObj = new JSONObject(jsonObj.get(key).toString());
                                        jObj.getString("x1");
                                        System.out.println("=========>" + jObj.getString("x1"));
                                    }
                                }
                            %>
                            <option value="2" >Edit All</option>
                        </select>
                        <label>X1 <input type="text" size="4" id="x1" name="x1" style="width: 50px;height: 30px !important"/></label>
                        <label>Y1 <input type="text" size="4" id="y1" name="y1" style="width: 50px;height: 30px !important"/></label>
                        <label>X2 <input type="text" size="4" id="x2" name="x2" style="width: 50px;height: 30px !important"/></label>
                        <label>Y2 <input type="text" size="4" id="y2" name="y2" style="width: 50px;height: 30px !important"/></label>
                        <label>W <input type="text" size="4" id="w" name="w" style="width: 50px;height: 30px !important"/></label>
                        <!--<label>H <input type="text" size="4" id="h" name="h" style="width: 50px;height: 30px !important"/></label>-->
                        <input type="hidden" id="templatename" name="templatename" value="<%=templatename%>">
                        <button type="button" onclick="updateTemplate()" style="vertical-align: super !important" class="btn btn-success">Update</button>
                        <button type="button" onclick="addfield()" style="vertical-align: super !important" class="btn btn-success">Add New Field</button>
                        <a href="./home.jsp"><button type="button" onclick="deleteField('<%=templatename%>')" class="btn btn-danger" style="vertical-align: super !important">Delete</button></a>
                        <a href="./home.jsp"><button type="button" style="vertical-align: super !important" class="btn btn-danger">Home</button></a>

                    </div>
                    <div id="addfield" name="addfield" style="display: none">
                        <!--Add new field-->
                        <input size="10" id="newfieldname" name="newfieldname" style="height: 30px !important" type="text" placeholder="Enter field name"/> 
                        <select id="datatype" name="datatype"  style="width:150px;height: 30px !important">
                            <option value="num">Numeric</option>
                            <option value="alphanum">Alpha-numeric</option>
                            <option value="alpha">Alphabetic</option>
                        </select>
                        <label>X1 <input type="text" size="4" id="x1" name="x1" style="width: 50px;height: 30px !important"/></label>
                        <label>Y1 <input type="text" size="4" id="y1" name="y1" style="width: 50px;height: 30px !important"/></label>
                        <label>X2 <input type="text" size="4" id="x2" name="x2" style="width: 50px;height: 30px !important"/></label>
                        <label>Y2 <input type="text" size="4" id="y2" name="y2" style="width: 50px;height: 30px !important"/></label>
                        <label>W <input type="text" size="4" id="w" name="w" style="width: 50px;height: 30px !important"/></label>
                        <input type="hidden" id="templatename" name="templatename" value="<%=templatename%>">
                        <button type="button" onclick="addNewFieldInTemplate('<%=templatename%>')" style="vertical-align: super !important" class="btn btn-success">Save</button>
                        <a href="./home.jsp"><button type="button" class="btn btn-danger" style="vertical-align: super !important">Cancel</button></a>
                    </div>
                    <%}%>
                </div>
            </form>
        </div>
        <div class="card-body">
            <div div class="jc-demo-box col-md-12" style="overflow-y: auto;height:450px;border: black;">
                <div class="span12" >
                    <img src="data:image/png;base64,<%=base64image%>" id="target" alt="[Jcrop Example]" />
                </div>
            </div>
        </div>
        <div class="container">

            <!-- The Modal -->


        </div>
    </div>
    <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Modal Heading</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    Modal body..
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    <script>
        function updateTemplate() {
            var s = './EditTemplate';
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                data: $('#coords').serialize(),
                success: function (data) {
                    alert(data._message);
                    document.getElementById("x1").value = "";
                    document.getElementById("x2").value = "";
                    document.getElementById("y1").value = "";
                    document.getElementById("y2").value = "";
                    document.getElementById("w").value = "";
                }
            });
        }
        function myFunction(templateName) {
            var _cordinateName = document.getElementById("_cordinateName").value;
            if (_cordinateName === "2") {

            }
            if (_cordinateName !== "-1") {
                var s = './SetCordinateValues?_templateName=' + templateName + "&_cordinateName=" + _cordinateName;
                $.ajax({
                    type: 'GET',
                    url: s,
                    dataType: 'json',
                    success: function (data) {
                        document.getElementById("x1").value = data.x1;
                        document.getElementById("x2").value = data.x2;
                        document.getElementById("y1").value = data.y1;
                        document.getElementById("y2").value = data.y2;
                        document.getElementById("w").value = data.w;
                        document.getElementById("datatype").value = data._datatype;
                    }
                });
            }
        }

        function addNewFieldInTemplate(templateName) {
            var s = './UpdateNewField?templateName='+templateName;
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
                data: $('#coords').serialize(),
                success: function (data) {
                    alert(data._message);
                    document.getElementById("x1").value = "";
                    document.getElementById("x2").value = "";
                    document.getElementById("y1").value = "";
                    document.getElementById("y2").value = "";
                    document.getElementById("newfieldname").value = "";
                }
            });
        }

        function deleteField(templateName) {
            var _cordinateName = document.getElementById("_cordinateName").value;
            if (_cordinateName === "-1") {
                alert("Please select co-ordinate");
            } else {
                var s = './DeleteTemplateField?templateName=' + templateName;
                $.ajax({
                    type: 'POST',
                    url: s,
                    async: false,
                    dataType: 'json',
                    data: $('#coords').serialize(),
                    success: function (data) {
                        alert(data._message);
                    }
                });
            }
        }
        function addfield() {
            $('#update').hide();
            $("#update").remove();
            $('#addfield').show();
        }
    </script>