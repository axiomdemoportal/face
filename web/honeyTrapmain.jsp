<%@page import="com.mollatech.axiom.nucleus.db.Geotrack"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.MobileTrustManagment"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.GeoLocationManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>
<%@page import="com.mollatech.axiom.nucleus.settings.GlobalChannelSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/jquery.js"></script>
<script src="./assets/js/geotracking.js"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/bootstrap-datepicker.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/css/datepicker.css">
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="./assets/js/json_sans_eval.js"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>


<div class="container-fluid">
    <h1 class="text-success">Honey Trap Report</h1>
    <!--<h3>Make Your Own Report</h3>-->
    <br>
    <input type="hidden" id="_changeType" name="_changeType" value="0">
    <div class="row-fluid">
        <div class="span12">
            <div class="control-group form-inline">

                <div class="input-prepend">

                    <!--<div class="well">-->
                    <span class="add-on">From:</span>   
                    <div id="datetimepicker1" class="input-append date">
                        <input id="startdate" name="startdate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                    <!--</div>-->
                </div>
                <div class="input-prepend">
                    <!--<div class="well">-->
                    <span class="add-on">till:</span>   
                    <div id="datetimepicker2" class="input-append date">
                        <input id="enddate" name="enddate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                    <!--</div>-->
                </div>
                <!--<button class="btn btn-success" onclick="searchGeoTrackByType()" id="addUserButton">Generate Report Now</button>-->

                <%if (oprObj.getRoleid() != 1) {//1 sysadmin
                        if (accessObj != null && accessObj.viewHoneyTrapReport == true) {%>
                <button class="btn btn-success" onclick="searchGeoTrackByType()" id="addUserButton">Generate Report Now</button>
                <%} else {%>
                <button class="btn btn-success " onclick="InvalidRequestHoneyTrap('honeytrapreport')" type="button">Generate Report</button>
                <%}
                } else {%>
                <button class="btn btn-success" onclick="searchGeoTrackByType()" id="addUserButton">Generate Report Now</button>
                <%}%>
            </div>
            <div id="users_table_main_MSG">
            </div>
        </div>
    </div>
    <%        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        int limit = 20;
        int TRADITIONAL = 1;

        GeoLocationManagement geoObj = new GeoLocationManagement();

        Geotrack[] Tracks = geoObj.getGeoTracksByTxType(channel.getChannelid(), MobileTrustManagment.INVALIDIMAGE);

        String strerr = "No Records Found";
    %>

    <!--<div class="container-fluid">-->
    <div id="map_transaction" style="width:100%; height: 400px"></div>
    <!--</div>--> 
    <div class="row-fluid">
        <div id="licenses_data_table">

            <table class="table table-striped" id="table_main">
                <tr>
                    <td>No.</td>
                    <td>IP Address</td>
                    <td>Lattitude</td>
                    <td>Longitude</td>
                    <td>City</td>
                    <td>State</td>
                    <td>Country</td>
                    <td>Zip code</td>
                    <td>Type</td>
                    <td>Dated</td>
                </tr>
                <%
                    if (Tracks != null) {
                        for (int i = 0; i < Tracks.length; i++) {
                            SimpleDateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            String start = df2.format(Tracks[i].getExecutedOn());

                            String strtype = "";
                            if (Tracks[i].getTxtype() == MobileTrustManagment.REGISTER) {
                                strtype = "Register";
                            } else if (Tracks[i].getTxtype() == MobileTrustManagment.LOGIN) {
                                strtype = "Login";
                            } else if (Tracks[i].getTxtype() == MobileTrustManagment.TRANSACTION) {
                                strtype = "Transaction";
                            } else if (Tracks[i].getTxtype() == MobileTrustManagment.CHANGEINPROFILE) {
                                strtype = "Change Profile";
                            } else if (Tracks[i].getTxtype() == MobileTrustManagment.VALIDIMAGE) {
                                strtype = "Valid Image";
                            } else if (Tracks[i].getTxtype() == MobileTrustManagment.INVALIDIMAGE) {
                                strtype = "Trap";
                            }
                %>
                <tr>
                    <td><%=(i + 1)%></td>
                    <td><%=Tracks[i].getIp()%></td>
                    <td><%=Tracks[i].getLattitude()%></td>
                    <td><%=Tracks[i].getLongitude()%></td>
                    <td><%=Tracks[i].getCity()%></td>
                    <td><%=Tracks[i].getState()%></td>
                    <td><%=Tracks[i].getCountry()%></td>
                    <td><%=Tracks[i].getZipcode()%></td>
                    <td><%=strtype%></td>
                    <td><%=start%></td>
                </tr>
                <%
                    }
                } else {%>
                <tr>
                    <td><%=1%></td>
                    <td><%= strerr%></td>
                    <td><%= strerr%></td>
                    <td><%= strerr%></td>
                    <td><%= strerr%></td>
                    <td><%= strerr%></td>
                    <td><%= strerr%></td>
                    <td><%= strerr%></td>
                    <td><%= strerr%></td>
                    <td><%= strerr%></td>
                    <%}%>
                </tr>
            </table>
        </div>
    </div>

    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datepicker({
                format: 'dd/MM/yyyy',
                language: 'pt-BR'
            });
        });
        $(function () {
            $('#datetimepicker2').datepicker({
                format: 'dd/MM/yyyy',
                language: 'pt-BR'

            });
        });
//        ChangeMediaType(0);

    </script>
    <script language="javascript">
        TransactionMapHoneyTrap(<%=0%>);
    </script>



</div>

<%@include file="footer.jsp" %>