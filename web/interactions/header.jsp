<%@ page import="com.mollatech.axiom.nucleus.db.Operators" %>
<%@ page import="com.mollatech.axiom.nucleus.db.Channels" %>
<%@ page import="com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement" %>
<%@ page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement" %>
<%@ page import="com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement" %>
<%@ page import="com.mollatech.axiom.nucleus.db.connector.management.RemoteAccessManagement" %>
<%@ page import="com.mollatech.axiom.nucleus.db.operation.AxiomChannel" %>
<%@ page import="com.mollatech.axiom.nucleus.db.operation.AxiomOperator" %>
<%@ page import="com.mollatech.axiom.nucleus.db.Accesses" %>
<%@ page import="com.mollatech.axiom.nucleus.db.Roles" %>
<%@ page import="org.hibernate.Session" %>
<%@ page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="google-translate-customization" content="26e4d928eae0659b-61f8bcc48ec8af9a-g74ee54aac640948a-11"></meta>
        <title>Interactions</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="../assets/css/bootstrap.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 60px;
                padding-bottom: 40px;
            }
        </style>
        <link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="../assets/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="../assets/ico/favicon.png">


        <!-- Le javascript
================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="../assets/js/jquery.js"></script>
        <script src="../assets/js/bootstrap-transition.js"></script>
        <script src="../assets/js/bootstrap-alert.js"></script>
        <script src="../assets/js/bootstrap-modal.js"></script>
        <script src="../assets/js/bootstrap-dropdown.js"></script>
        <script src="../assets/js/bootstrap-scrollspy.js"></script>
        <script src="../assets/js/bootstrap-tab.js"></script>
        <script src="../assets/js/bootstrap-tooltip.js"></script>
        <script src="../assets/js/bootstrap-popover.js"></script>
        <script src="../assets/js/bootstrap-button.js"></script>
        <script src="../assets/js/bootstrap-collapse.js"></script>
        <script src="../assets/js/bootstrap-carousel.js"></script>
        <script src="../assets/js/bootstrap-typeahead.js"></script>
        <script src="../assets/js/json_sans_eval.js"></script>
        <script src="../assets/js/bootbox.min.js"></script>
        <script src="../assets/js/interactions/interactions.js"></script>
            


    </head>

    <body>

<!--        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="brand" href="#">Web Form for User Validation!!!</a>
                </div>
            </div>
        </div>-->
