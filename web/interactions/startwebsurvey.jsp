<%@page import="java.util.Date"%>
<%@include file="header.jsp" %>
<%@page import="com.mollatech.dictum.management.SurveyManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.ChannelsUtils"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil"%>
<%@page import="com.mollatech.axiom.nucleus.db.operation.Survey"%>
<%@page import="com.mollatech.axiom.nucleus.db.Interactionresponse"%>
<%@page import="com.mollatech.axiom.nucleus.db.Interactionsexecutions"%>
<%@page import="com.mollatech.axiom.connector.communication.AXIOMStatus"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script src="../assets/js/interactions/interactions.js"></script>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Web Interaction</title>
    </head>


    <div id="users_table_main">

    </div>


    <%        String interactionid = request.getParameter("interactionid");
        String from = request.getParameter("contactDetails");
        String userid = request.getParameter("name");

        try {
            int interactionId = 0;
            if (interactionid != null) {
                interactionId = Integer.parseInt(interactionid);
            }
            AXIOMStatus status = null;
            Interactionsexecutions interaction = null;
            Interactionsexecutions[] interactionsList = null;
            Interactionresponse interactionresponse = null;
            String message = null;

            Survey survey = null;
            //        if (strInteractionid != null) {
            //            interactionId = Integer.parseInt(strInteractionid);
            //        }

            String _channelName = this.getServletContext().getContextPath();
            _channelName = _channelName.replaceAll("/", "");

            SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
            Session sChannel = suChannel.openSession();
            ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
            
            Channels channel = cUtil.getChannel(_channelName);
            
            
            
    %> 
    <div class="modal-footer" id="closesurveyid">
        <button class="btn btn-sm btn-danger" onclick="closesurveyonclick('<%=channel.getChannelid()%>', '<%=interactionId%>', '<%=from%>')" id="closebuttonid">Close Survey</button>
    </div> 

    <%
        
        if (channel == null) {
            sChannel.close();
            suChannel.close();
            return;
        }
        sChannel.close();
        suChannel.close();
        //end of addition

        SurveyManagement sManagement = new SurveyManagement();
        interaction = sManagement.getInteractionExecutions(interactionId);
        if(interaction != null){
            if (interaction.getStatus() == SurveyManagement.RUNNING) {
                interaction = interaction;
            }
        }
        
        //  interaction = sManagement.getInteractionExecutions(interactionId);
        session = request.getSession(true);
        session.setAttribute("interactionid", interactionid);
        if (interaction.getStatus() == SurveyManagement.RUNNING) {
            survey = sManagement.getSurveyQuestionsByIRExecution(interaction);

    %>   
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container-fluid">
                <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="brand" href="#"><%=interaction.getInteractionName()%></a>
            </div>
        </div>
    </div>


    <!--        <div class="hero-unit" align="left">
                <h3>Greeting <i>"<%=survey.greetings.toString()%>"</i></h3>
                <form action="websurveyQA.jsp" method="POST" >
                    <h3><input type="radio" name="firstoption" id="firstoptionone"><i><%=survey.greetingsTags[0].toString() + ")"%></i><i><%=survey.greetingOptions[0].toString()%></i></h3>
                    <h3><input type="radio" name="firstoption" id="firstoptiontwo"><i><%=survey.greetingsTags[1].toString() + ")"%></i><i><%=survey.greetingOptions[1].toString()%></i></h3>
                    <input type="hidden" id="Body" name="Body">
                    <input type="hidden" id="From" name="From">
                    <input type="submit" class="btn-primary" onclick="prepsave(<%=survey.greetingsTags[0].toString()%>,<%=survey.greetingsTags[0].toString()%>,<%=from%>)">
                </form>
            </div>-->

    <div class="hero-unit" align="left">
        <h3>Greeting <i>"<%=survey.greetings.toString()%>"</i></h3>
        <form action="websurveyQA.jsp" method="POST" >
            <h3><input type="radio" name="firstoption" id="firstoptionone"><i><%=survey.greetingsTags[0] + ")"%></i><i><%=survey.greetingOptions[0]%></i></h3>
            <h3><input type="radio" name="firstoption" id="firstoptiontwo"><i><%=survey.greetingsTags[1] + ")"%></i><i><%=survey.greetingOptions[1]%></i></h3>
            <input type="hidden" id="Body" name="Body">
            <input type="hidden" id="From" name="From">
            <input type="submit" class="btn-primary" onclick="prepsave('<%=survey.greetingsTags[0]%>', '<%=survey.greetingsTags[1]%>', '<%=from%>')">

        </form>
    </div>
            <script>
//             window.onbeforeunload = function('<%=channel.getChannelid()%>', '<%=interactionId%>','<%=from%>') {
//                 
//            return "Are you sure you want to close the window?";
//            return "Are you sure you want to close the window?";
//
//            var s = '../closeSurvey?From='+<%=from%>+'&irId='+<%=interactionId%>+'channelid='+<%=channel.getChannelid()%>;
//            $.ajax({
//                type: 'POST',
//                url: s,
//                dataType: 'json',
//                data: $("#closesurveyid").serialize(),
//                success: function(data) {
//                    if (strcmpSurveys(data._result, "error") == 0) {
//                        Alert4Surveys("<span><font color=red>" + data._message + "</font></span>");
//                        window.setTimeout(RefreshSurveysList, 2000);
//                    }
//                    else if (strcmpSurveys(data._result, "success") == 0) {
//
//
//                    }
//                }
//            });
//
//        }
 window.onbeforeunload = function() {
                 
            return "Are you sure you want to close the window?";
            return "Are you sure you want to close the window?";

           

        }
                
            </script>       
    <%

                //   session.setAttribute("From", from);
                // sManagement.addInteractionResponse(channel.getChannelid(), from, interactionId, userid, SurveyManagement.WEB, survey.greetings, survey.offer, survey.questionAndOptions[0].question, survey.questionAndOptions[1].question, survey.questionAndOptions[2].question, survey.questionAndOptions[3].question, survey.questionAndOptions[4].question, new Date());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }
    %>
    <body>
    </body>
    <script>
        
//        window.onbeforeunload = function('<%=from%>') {
////                return "Are you sure you want to close the window?";
//            return "Are you sure you want to close the window?";
//
//            var s = '../closeSurvey';
//            $.ajax({
//                type: 'POST',
//                url: s,
//                dataType: 'json',
//                data: $("#closesurveyid").serialize(),
//                success: function(data) {
//                    if (strcmpSurveys(data._result, "error") == 0) {
//                        Alert4Surveys("<span><font color=red>" + data._message + "</font></span>");
//                        window.setTimeout(RefreshSurveysList, 2000);
//                    }
//                    else if (strcmpSurveys(data._result, "success") == 0) {
//
//
//                    }
//                }
//            });
//
//        }
    </script>



</html>
<%@include file="footer.jsp" %>