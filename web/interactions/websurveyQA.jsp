<%@page import="java.util.Date"%>
<%@include file="header.jsp" %>
<%@page import="com.mollatech.dictum.management.SurveyManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.ChannelsUtils"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil"%>
<%@page import="com.mollatech.axiom.nucleus.db.operation.Survey"%>
<%@page import="com.mollatech.axiom.nucleus.settings.SendNotification"%>
<%@page import="com.mollatech.axiom.nucleus.db.Interactionresponse"%>
<%@page import="com.mollatech.axiom.nucleus.db.Interactionsexecutions"%>
<%@page import="com.mollatech.axiom.connector.communication.AXIOMStatus"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%    try {
        String from = request.getParameter("From");
        String body = request.getParameter("Body");

        String strInteractionid = (String) request.getSession().getAttribute("interactionid");

        int interactionId = 0;
        AXIOMStatus status = null;
        Interactionsexecutions interaction = null;
        Interactionsexecutions[] interactionsList = null;
        Interactionresponse interactionresponse = null;
        String message = null;

        Survey survey = null;
        if (strInteractionid != null) {
            interactionId = Integer.parseInt(strInteractionid);
        }

        String _channelName = this.getServletContext().getContextPath();
        _channelName = _channelName.replaceAll("/", "");

        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);

        Channels channel = cUtil.getChannel(_channelName);
        if (channel == null) {
            sChannel.close();
            suChannel.close();
            return;
        }

        body = body.trim();

        sChannel.close();
        suChannel.close();
        //end of addition

        String channelId = channel.getChannelid();
        SurveyManagement sManagement = new SurveyManagement();
//    interaction = sManagement.getInteractionExecutions(interactionId);
//
//    for (int i = 0; i < interactionsList.length; i++) {
//        if (interaction.getStatus() == SurveyManagement.RUNNING) {
//
//        }
//    }
//        interactionsList = sManagement.getInteractionsExecutionList(channel.getChannelid());
//        for (int i = 0; i < interactionsList.length; i++) {
//            if (interactionsList[i].getStatus() == SurveyManagement.RUNNING) {
//                interaction = interactionsList[i];
//                break;
//            }
//        }
        
        
        interaction = sManagement.getInteractionExecutions(interactionId);
        if(interaction != null){
            if (interaction.getStatus() == SurveyManagement.RUNNING) {
                interaction = interaction;
            }
        }

        Date d = new Date();
        long difference = interaction.getExpirydatetime().getTime() - d.getTime();
        if (difference <= 0L) {
            sManagement.changeStatus(interaction.getInteractionid(), channelId, SurveyManagement.CLOSED);
        }

%>

<div class="modal-footer" id="closesurveyid">
    <button class="btn btn-sm btn-danger" onclick="closesurveyonclick('<%=channel.getChannelid()%>', '<%=interactionId%>', '<%=from%>')" id="closebuttonid">Close Survey</button>
</div> 
<script>
//    window.onbeforeunload = function() {
//        return "Are you sure you want to close the window?";
//        return "Are you sure you want to close the window?";
//        
//    }

</script>    


<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container-fluid">
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="brand" href="#"><%=interaction.getInteractionName()%></a>
        </div>
    </div>
</div>

<%
    interactionresponse = sManagement.getInteractionsResponse(channelId, from, interaction.getInteractionexecutionid());

    survey = sManagement.getSurveyQuestionsByIRExecution(interaction);
    if (interactionresponse != null && survey != null) {
        if (body.equalsIgnoreCase(survey.greetingsTags[1])) {
            sManagement.changeInteractionResponse(channelId, interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SurveyManagement.WEB, interactionresponse.getGreetings(), SurveyManagement.NO, interactionresponse.getOffer(), interactionresponse.getQuestion1(), 0, interactionresponse.getQuestion2(), 0, interactionresponse.getQuestion3(), 0, interactionresponse.getQuestion4(), 0, interactionresponse.getQuestion5(), 0, 0, SurveyManagement.CLOSED);
%>
<div class="hero-unit" align="left">
    <h3><i>"<%=interactionresponse.getOffer()%>"</i></h3>

</div>

<%
} else if (body.equalsIgnoreCase(survey.greetingsTags[0])) {
    // sManagement.changeInteractionResponse(channelId, from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), YES, interactionresponse.getOffer(), NO, interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 0);

    if (interactionresponse.getQuestion1() == null) {
        //user not found
    } else {

%>


<div class="hero-unit" align="left">
    <h3>Question 1: <i>"<%=interactionresponse.getQuestion1()%>"</i></h3>
    <form action="" method="POST">
        <input type="hidden" id="Body" name="Body">
        <input type="hidden" id="From" name="From">
        <h3><input type="radio" name="firstquestionoption" id="firstquestionone"><i><%=survey.questionAndOptions[0].optionTags[0] + ")"%></i><i><%=survey.questionAndOptions[0].options[0]%></i></h3>
        <h3><input type="radio" name="firstquestionoption" id="firstquestiontwo"><i><%=survey.questionAndOptions[0].optionTags[1] + ")"%></i><i><%=survey.questionAndOptions[0].options[1]%></i></h3>
        <h3><input type="radio" name="firstquestionoption" id="firstquestionthree"><i><%=survey.questionAndOptions[0].optionTags[2] + ")"%></i><i><%=survey.questionAndOptions[0].options[2]%></i></h3>
        <input type="hidden" id="questiononeanswer" name="questiononeanswer">

        <input type="submit" class="btn-primary" onclick="questionone('<%=survey.questionAndOptions[0].optionTags[0]%>', '<%=survey.questionAndOptions[0].optionTags[1]%>', '<%=survey.questionAndOptions[0].optionTags[2]%>', '<%=from%>')">
    </form>
</div>
<script>

    function Refreshwebpage() {
        window.location.href = "./websurveyQA.jsp";
    }
    function questionone(check1, check2, check3, from) {
        alert(check1);
        alert(check2);
        if (document.getElementById('firstquestionone').checked) {
            alert("firstoption");
            document.getElementById("questiononeanswer").value = check1;
            document.getElementById("Body").value = check1;
            document.getElementById("From").value = from;
//             window.location.reload();
        }
        else if (document.getElementById('firstquestiontwo').checked) {

            document.getElementById("questiononeanswer").value = check2;
            document.getElementById("Body").value = check2;
            document.getElementById("From").value = from;
//            window.setTimeout(Refreshwebpage, 2000);
        }
        else if (document.getElementById('firstquestionthree').checked) {

            document.getElementById("questiononeanswer").value = check3;
            document.getElementById("Body").value = check3;
            document.getElementById("From").value = from;
//            window.setTimeout(Refreshwebpage, 2000);

        }
        return true;
    }
</script> 


<%

            sManagement.changeInteractionResponse(channelId, interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SurveyManagement.WEB, interactionresponse.getGreetings(), SurveyManagement.YES, interactionresponse.getOffer(), interactionresponse.getQuestion1(), 0, interactionresponse.getQuestion2(), 0, interactionresponse.getQuestion3(), 0, interactionresponse.getQuestion4(), 0, interactionresponse.getQuestion5(), 0, 1, SurveyManagement.RUNNING);

        }
    }
    int iCurrent = interactionresponse.getIcurrent();
    if (iCurrent < survey.questionAndOptions.length) {
        if (body.equalsIgnoreCase(survey.questionAndOptions[iCurrent].optionTags[0])) {

            if (interactionresponse.getIcurrent() == 1) {
                // sManagement.changeInteractionResponse(channelId, from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), YES, interactionresponse.getOffer(), interactionresponse.getGreetingsresponse(), interactionresponse.getQuestion1(), A, interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 0);

                if (interactionresponse.getQuestion2() == null) {
                    //user not found
                } else {%>

<div class="hero-unit" align="left">
    <h3>Question 2: <i>"<%=interactionresponse.getQuestion2()%>"</i></h3>
    <form action="websurveyQA.jsp" method="POST" >
        <input type="hidden" id="Body" name="Body">
        <input type="hidden" id="From" name="From">
        <h3><input type="radio" name="secondquestionoption" id="secondquestionone"><i><%=survey.questionAndOptions[1].optionTags[0] + ")"%></i><i><%=survey.questionAndOptions[1].options[0]%></i></h3>
        <h3><input type="radio" name="secondquestionoption" id="secondquestiontwo"><i><%=survey.questionAndOptions[1].optionTags[1] + ")"%></i><i><%=survey.questionAndOptions[1].options[1]%></i></h3>
        <h3><input type="radio" name="secondquestionoption" id="secondquestionthree"><i><%=survey.questionAndOptions[1].optionTags[2] + ")"%></i><i><%=survey.questionAndOptions[1].options[2]%></i></h3>
        <input type="hidden" id="questiontwoanswer" name="questiontwoanswer">
        <input type="submit" class="btn-primary" onclick="questiontwo('<%=survey.questionAndOptions[1].optionTags[0]%>', '<%=survey.questionAndOptions[1].optionTags[1]%>', '<%=survey.questionAndOptions[1].optionTags[2]%>', '<%=from%>')">
    </form>
</div>
<script>
    function questiontwo(check1, check2, check3, from) {

        if (document.getElementById('secondquestionone').checked) {

            document.getElementById("questiontwoanswer").value = check1;
            document.getElementById("Body").value = check1;
            document.getElementById("From").value = from;
        }
        else if (document.getElementById('secondquestiontwo').checked) {

            document.getElementById("questiontwoanswer").value = check2;
            document.getElementById("Body").value = check2;
            document.getElementById("From").value = from;
        } else if (document.getElementById('secondquestionthree').checked) {

            document.getElementById("questiontwoanswer").value = check3;
            document.getElementById("Body").value = check3;
            document.getElementById("From").value = from;
        }

    }
</script> 



<%

        sManagement.changeInteractionResponse(channelId, interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SurveyManagement.WEB, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), SurveyManagement.OPTION1, interactionresponse.getQuestion2(), 0, interactionresponse.getQuestion3(), 0, interactionresponse.getQuestion4(), 0, interactionresponse.getQuestion5(), 0, 2, SurveyManagement.RUNNING);

    }
} else if (interactionresponse.getIcurrent() == 2) {
    // sManagement.changeInteractionResponse(channelId, from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), NO, interactionresponse.getOffer(), interactionresponse.getGreetingsresponse(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), A, interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 0);

    if (interactionresponse.getQuestion3() == null) {
        //user not found
    } else {%>

<div class="hero-unit" align="left">
    <h3>Question 3: <i>"<%=interactionresponse.getQuestion3()%>"</i></h3>
    <form action="websurveyQA.jsp" method="POST" >
        <input type="hidden" id="Body" name="Body">
        <input type="hidden" id="From" name="From">
        <h3><input type="radio" name="threequestionoption" id="threequestionone"><i><%=survey.questionAndOptions[2].optionTags[0] + ")"%></i><i><%=survey.questionAndOptions[2].options[0]%></i></h3>
        <h3><input type="radio" name="threequestionoption" id="threequestiontwo"><i><%=survey.questionAndOptions[2].optionTags[1] + ")"%></i><i><%=survey.questionAndOptions[2].options[1]%></i></h3>
        <h3><input type="radio" name="threequestionoption" id="threequestionthree"><i><%=survey.questionAndOptions[2].optionTags[2] + ")"%></i><i><%=survey.questionAndOptions[2].options[2]%></i></h3>
        <input type="hidden" id="questionthreeanswer" name="questionthreeanswer">
        <input type="submit" class="btn-primary" onclick="questionone('<%=survey.questionAndOptions[2].optionTags[0]%>', '<%=survey.questionAndOptions[2].optionTags[1]%>', '<%=survey.questionAndOptions[2].optionTags[2]%>', '<%=from%>')">
    </form>
</div>
<script>
    function questionthree(check1, check2, check3, from) {
        if (document.getElementById('threequestionone').checked) {
            document.getElementById("questionthreeanswer").value = check1;
            document.getElementById("Body").value = check1;
            document.getElementById("From").value = from;
        }
        else if (document.getElementById('threequestiontwo').checked) {
            document.getElementById("questionthreeanswer").value = check2;
            document.getElementById("Body").value = check2;
            document.getElementById("From").value = from;
        } else if (document.getElementById('threequestionthree').checked) {

            document.getElementById("questionthreeanswer").value = check3;
            document.getElementById("Body").value = check3;
            document.getElementById("From").value = from;
        }


    }
</script> 



<%

        sManagement.changeInteractionResponse(channelId, interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SurveyManagement.WEB, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), SurveyManagement.OPTION1, interactionresponse.getQuestion3(), 0, interactionresponse.getQuestion4(), 0, interactionresponse.getQuestion5(), 0, 3, SurveyManagement.RUNNING);

    }
} else if (interactionresponse.getIcurrent() == 3) {

    if (interactionresponse.getQuestion4() == null) {
        //user not found
    } else {%>

<div class="hero-unit" align="left">
    <h3>Question 4: <i>"<%=interactionresponse.getQuestion4()%>"</i></h3>
    <form action="websurveyQA.jsp" method="POST" >
        <input type="hidden" id="Body" name="Body">
        <input type="hidden" id="From" name="From">
        <h3><input type="radio" name="fourthquestionoption" id="fourthquestionone"><i><%=survey.questionAndOptions[3].optionTags[0] + ")"%></i><i><%=survey.questionAndOptions[3].options[0]%></i></h3>
        <h3><input type="radio" name="fourthquestionoption" id="fourthquestiontwo"><i><%=survey.questionAndOptions[3].optionTags[1] + ")"%></i><i><%=survey.questionAndOptions[3].options[1]%></i></h3>
        <h3><input type="radio" name="fourthquestionoption" id="fourthquestionthree"><i><%=survey.questionAndOptions[3].optionTags[2] + ")"%></i><i><%=survey.questionAndOptions[3].options[2]%></i></h3>
        <input type="hidden" id="questionfouranswer" name="questionfouranswer">
        <input type="submit" class="btn-primary" onclick="questionfour('<%=survey.questionAndOptions[3].optionTags[0]%>', '<%=survey.questionAndOptions[3].optionTags[1]%>', '<%=survey.questionAndOptions[3].optionTags[2]%>', '<%=from%>')">
    </form>
</div>
<script>
    function questionfour(check1, check2, check3, from) {

        if (document.getElementById('fourthquestionone').checked) {

            document.getElementById("questionfouranswer").value = check1;
            document.getElementById("Body").value = check1;
            document.getElementById("From").value = from;
        }
        else if (document.getElementById('fourthquestiontwo').checked) {

            document.getElementById("questionfouranswer").value = check2;
            document.getElementById("Body").value = check2;
            document.getElementById("From").value = from;
        }
        else if (document.getElementById('fourthquestionthree').checked) {

            document.getElementById("questionfouranswer").value = check3;
            document.getElementById("Body").value = check3;
            document.getElementById("From").value = from;
        }

    }
</script> 


<%
        sManagement.changeInteractionResponse(channelId, interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SurveyManagement.WEB, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), SurveyManagement.OPTION1, interactionresponse.getQuestion4(), 0, interactionresponse.getQuestion5(), 0, 4, SurveyManagement.RUNNING);

    }
} else if (interactionresponse.getIcurrent() == 4) {

    if (interactionresponse.getQuestion5() == null) {
        //user not found
    } else {
%>
<div class="hero-unit" align="left">
    <h3>Question 5: <i>"<%=interactionresponse.getQuestion5()%>"</i></h3>
    <form action="websurveyQA.jsp" method="POST" >
        <input type="hidden" id="Body" name="Body">
        <input type="hidden" id="From" name="From">
        <h3><input type="radio" name="fifthquestionoption" id="fifthquestionone"><i><%=survey.questionAndOptions[4].optionTags[0] + ")"%></i><i><%=survey.questionAndOptions[4].options[0]%></i></h3>
        <h3><input type="radio" name="fifthquestionoption" id="fifthquestiontwo"><i><%=survey.questionAndOptions[4].optionTags[1] + ")"%></i><i><%=survey.questionAndOptions[4].options[1]%></i></h3>
        <h3><input type="radio" name="fifthquestionoption" id="fifthquestionthree"><i><%=survey.questionAndOptions[4].optionTags[2] + ")"%></i><i><%=survey.questionAndOptions[4].options[2]%></i></h3>
        <input type="hidden" id="questionfiveanswer" name="questionfiveanswer">
        <input type="submit" class="btn-primary" onclick="questionfive('<%=survey.questionAndOptions[4].optionTags[0]%>', '<%=survey.questionAndOptions[4].optionTags[1]%>', '<%=survey.questionAndOptions[4].optionTags[2]%>', '<%=from%>')">
    </form>
</div>
<script>
    function questionfive(check1, check2, check3, from) {

        if (document.getElementById('fifthquestionone').checked) {

            document.getElementById("questionfiveanswer").value = check1;
            document.getElementById("Body").value = check1;
            document.getElementById("From").value = from;
        }
        else if (document.getElementById('fifthquestiontwo').checked) {

            document.getElementById("questionfiveanswer").value = check2;
            document.getElementById("Body").value = check2;
            document.getElementById("From").value = from;
        } else if (document.getElementById('fifthquestionthree').checked) {

            document.getElementById("questionfiveanswer").value = check3;
            document.getElementById("Body").value = check3;
            document.getElementById("From").value = from;
        }


    }
</script> 


<%
            sManagement.changeInteractionResponse(channelId, interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SurveyManagement.WEB, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), SurveyManagement.OPTION1, interactionresponse.getQuestion5(), 0, 5, SurveyManagement.RUNNING);

        }
    } else if (interactionresponse.getIcurrent() == 5) {
        sManagement.changeInteractionResponse(channelId, interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SurveyManagement.WEB, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), SurveyManagement.OPTION1, interactionresponse.getIcurrent(), SurveyManagement.CLOSED);

    }
} else if (body.equalsIgnoreCase(survey.questionAndOptions[iCurrent].optionTags[1])) {

    if (interactionresponse.getIcurrent() == 1) {
        // sManagement.changeInteractionResponse(channelId, from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), YES, interactionresponse.getOffer(), interactionresponse.getGreetingsresponse(), interactionresponse.getQuestion1(), A, interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 0);

        if (interactionresponse.getQuestion1() == null) {
            //user not found
        } else {%>
<div class="hero-unit" align="left">
    <h3>Question 2: <i>"<%=interactionresponse.getQuestion2()%>"</i></h3>
    <form action="websurveyQA.jsp" method="POST" >
        <input type="hidden" id="Body" name="Body">
        <input type="hidden" id="From" name="From">
        <h3><input type="radio" name="sixthquestionoption" id="sixthquestionone"><i><%=survey.questionAndOptions[1].optionTags[0] + ")"%></i><i><%=survey.questionAndOptions[1].options[0]%></i></h3>
        <h3><input type="radio" name="sixthquestionoption" id="sixthquestiontwo"><i><%=survey.questionAndOptions[1].optionTags[1] + ")"%></i><i><%=survey.questionAndOptions[1].options[1]%></i></h3>
        <h3><input type="radio" name="sixthquestionoption" id="sixthquestionthree"><i><%=survey.questionAndOptions[1].optionTags[2] + ")"%></i><i><%=survey.questionAndOptions[1].options[2]%></i></h3>
        <input type="hidden" id="questionsixthanswer" name="questionsixthanswer">
        <input type="submit" class="btn-primary" onclick="questionsix('<%=survey.questionAndOptions[1].optionTags[0]%>', '<%=survey.questionAndOptions[1].optionTags[1]%>', '<%=survey.questionAndOptions[1].optionTags[2]%>', '<%=from%>')">
    </form>
</div>
<script>
    function questionsix(check1, check2, check3, from) {

        if (document.getElementById('sixthquestionone').checked) {

            document.getElementById("questionsixthanswer").value = check1;
            document.getElementById("Body").value = check1;
            document.getElementById("From").value = from;
        }
        else if (document.getElementById('sixthquestiontwo').checked) {

            document.getElementById("questionsixthanswer").value = check2;
            document.getElementById("Body").value = check2;
            document.getElementById("From").value = from;
        } else if (document.getElementById('sixthquestionthree').checked) {

            document.getElementById("questionsixthanswer").value = check3;
            document.getElementById("Body").value = check3;
            document.getElementById("From").value = from;
        }


    }
</script> 

<%

        sManagement.changeInteractionResponse(channelId, interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SurveyManagement.WEB, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), SurveyManagement.OPTION2, interactionresponse.getQuestion2(), 0, interactionresponse.getQuestion3(), 0, interactionresponse.getQuestion4(), 0, interactionresponse.getQuestion5(), 0, 2, SurveyManagement.RUNNING);

    }
} else if (interactionresponse.getIcurrent() == 2) {
    // sManagement.changeInteractionResponse(channelId, from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), NO, interactionresponse.getOffer(), interactionresponse.getGreetingsresponse(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), A, interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 0);

    if (interactionresponse.getQuestion1() == null) {
        //user not found
    } else {%>
<div class="hero-unit" align="left">
    <h3>Question 3: <i>"<%=interactionresponse.getQuestion3()%>"</i></h3>
    <form action="websurveyQA.jsp" method="POST" >
        <input type="hidden" id="Body" name="Body">
        <input type="hidden" id="From" name="From">
        <h3><input type="radio" name="seventhquestionoption" id="seventhquestionone"><i><%=survey.questionAndOptions[2].optionTags[0] + ")"%></i><i><%=survey.questionAndOptions[2].options[0]%></i></h3>
        <h3><input type="radio" name="seventhquestionoption" id="seventhquestiontwo"><i><%=survey.questionAndOptions[2].optionTags[1] + ")"%></i><i><%=survey.questionAndOptions[2].options[1]%></i></h3>
        <h3><input type="radio" name="seventhquestionoption" id="seventhquestionthree"><i><%=survey.questionAndOptions[2].optionTags[2] + ")"%></i><i><%=survey.questionAndOptions[2].options[2]%></i></h3>
        <input type="hidden" id="questionsevenanswer" name="questionsevenanswer">
        <input type="submit" class="btn-primary" onclick="questionseven('<%=survey.questionAndOptions[2].optionTags[0]%>', '<%=survey.questionAndOptions[2].optionTags[1]%>', '<%=survey.questionAndOptions[2].optionTags[2]%>', '<%=from%>')">
    </form>
</div>
<script>
    function questionseven(check1, check2, check3, from) {

        if (document.getElementById('seventhquestionone').checked) {
            document.getElementById("questionsevenanswer").value = check1;
            document.getElementById("Body").value = check1;
            document.getElementById("From").value = from;
        }
        else if (document.getElementById('seventhquestiontwo').checked) {
            document.getElementById("questionsevenanswer").value = check2;
            document.getElementById("Body").value = check2;
            document.getElementById("From").value = from;
        } else if (document.getElementById('seventhquestionthree').checked) {

            document.getElementById("questionsevenanswer").value = check3;
            document.getElementById("Body").value = check3;
            document.getElementById("From").value = from;
        }


    }
</script> 

<%

//                        message = "Question 3\n" + interactionresponse.getQuestion3()
//                                + "\n" + survey.questionAndOptions[2].optionTags[0] + " "
//                                + survey.questionAndOptions[2].options[0]
//                                + "\n" + survey.questionAndOptions[2].optionTags[1] + " "
//                                + "\n" + survey.questionAndOptions[2].options[1]
//                                + "\n" + survey.questionAndOptions[2].optionTags[2] + " "
//                                + "\n" + survey.questionAndOptions[2].options[2];
//                     
        sManagement.changeInteractionResponse(channelId, interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SurveyManagement.WEB, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), SurveyManagement.OPTION2, interactionresponse.getQuestion3(), 0, interactionresponse.getQuestion4(), 0, interactionresponse.getQuestion5(), 0, 3, SurveyManagement.RUNNING);

    }
} else if (interactionresponse.getIcurrent() == 3) {

    if (interactionresponse.getQuestion1() == null) {
        //user not found
    } else {
%>
<div class="hero-unit" align="left">
    <h3>Question 4: <i>"<%=interactionresponse.getQuestion4()%>"</i></h3>
    <form action="websurveyQA.jsp" method="POST" >
        <input type="hidden" id="Body" name="Body">
        <input type="hidden" id="From" name="From">
        <h3><input type="radio" name="eigthquestionoption" id="eigthquestionone"><i><%=survey.questionAndOptions[3].optionTags[0] + ")"%></i><i><%=survey.questionAndOptions[3].options[0]%></i></h3>
        <h3><input type="radio" name="eigthquestionoption" id="eigthquestiontwo"><i><%=survey.questionAndOptions[3].optionTags[1] + ")"%></i><i><%=survey.questionAndOptions[3].options[1]%></i></h3>
        <h3><input type="radio" name="eigthquestionoption" id="eigthquestionthree"><i><%=survey.questionAndOptions[3].optionTags[2] + ")"%></i><i><%=survey.questionAndOptions[3].options[2]%></i></h3>
        <input type="hidden" id="questioneightanswer" name="questioneightanswer">
        <input type="submit" class="btn-primary" onclick="questioneight('<%=survey.questionAndOptions[3].optionTags[0]%>', '<%=survey.questionAndOptions[3].optionTags[1]%>', '<%=survey.questionAndOptions[3].optionTags[2]%>', '<%=from%>')">
    </form>
</div>
<script>
    function questioneight(check1, check2, check3, from) {

        if (document.getElementById('eigthquestionone').checked) {

            document.getElementById("questioneightanswer").value = check1;
            document.getElementById("Body").value = check1;
            document.getElementById("From").value = from;
        }
        else if (document.getElementById('eigthquestiontwo').checked) {

            document.getElementById("questioneightanswer").value = check2;
            document.getElementById("Body").value = check2;
            document.getElementById("From").value = from;
        } else if (document.getElementById('eigthquestionthree').checked) {

            document.getElementById("questioneightanswer").value = check3;
            document.getElementById("Body").value = check3;
            document.getElementById("From").value = from;
        }

    }
</script> 
<%
//
//                        message = "Question 4\n" + interactionresponse.getQuestion4()
//                                + "\n" + survey.questionAndOptions[3].optionTags[0] + " "
//                                + survey.questionAndOptions[3].options[0]
//                                + "\n" + survey.questionAndOptions[3].optionTags[1] + " "
//                                + survey.questionAndOptions[3].options[1]
//                                + "\n" + survey.questionAndOptions[3].optionTags[2] + " "
//                                + survey.questionAndOptions[3].options[2];
//                       
        sManagement.changeInteractionResponse(channelId, interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SurveyManagement.WEB, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), SurveyManagement.OPTION2, interactionresponse.getQuestion4(), 0, interactionresponse.getQuestion5(), 0, 4, SurveyManagement.RUNNING);

    }
} else if (interactionresponse.getIcurrent() == 4) {

    if (interactionresponse.getQuestion1() == null) {
        //user not found
    } else {

%>
<div class="hero-unit" align="left">
    <h3>Question 1: <i>"<%=interactionresponse.getQuestion5()%>"</i></h3>
    <form action="websurveyQA.jsp" method="POST" >
        <input type="hidden" id="Body" name="Body">
        <input type="hidden" id="From" name="From">
        <h3><input type="radio" name="ninthquestionoption" id="ninthquestionone"><i><%=survey.questionAndOptions[4].optionTags[0] + ")"%></i><i><%=survey.questionAndOptions[4].options[0]%></i></h3>
        <h3><input type="radio" name="ninthquestionoption" id="ninthquestiontwo"><i><%=survey.questionAndOptions[4].optionTags[1] + ")"%></i><i><%=survey.questionAndOptions[4].options[1]%></i></h3>
        <h3><input type="radio" name="ninthquestionoption" id="ninthquestionthree"><i><%=survey.questionAndOptions[4].optionTags[2] + ")"%></i><i><%=survey.questionAndOptions[4].options[2]%></i></h3>
        <input type="hidden" id="questionnineanswer" name="questionnineanswer">
        <input type="submit" class="btn-primary" onclick="questionnine('<%=survey.questionAndOptions[4].optionTags[0]%>', '<%=survey.questionAndOptions[4].optionTags[1]%>', '<%=survey.questionAndOptions[4].optionTags[2]%>', '<%=from%>')">
    </form>
</div>
<script>
    function questionnine(check1, check2, check3, from) {

        if (document.getElementById('ninthquestionone').checked) {
            document.getElementById("questionnineanswer").value = check1;
            document.getElementById("Body").value = check1;
            document.getElementById("From").value = from;
        }
        else if (document.getElementById('ninthquestiontwo').checked) {
            document.getElementById("questionnineanswer").value = check2;
            document.getElementById("Body").value = check2;
            document.getElementById("From").value = from;
        } else if (document.getElementById('ninthquestionthree').checked) {
            document.getElementById("questionnineanswer").value = check3;
            document.getElementById("Body").value = check3;
            document.getElementById("From").value = from;
        }

    }
</script>
<%

//                        message = "Question 5\n" + interactionresponse.getQuestion5()
//                                + "\n" + survey.questionAndOptions[4].optionTags[0] + " "
//                                + survey.questionAndOptions[4].options[0]
//                                + "\n" + survey.questionAndOptions[4].optionTags[1] + " "
//                                + survey.questionAndOptions[4].options[1]
//                                + "\n" + survey.questionAndOptions[4].optionTags[2] + " "
//                                + survey.questionAndOptions[4].options[2];
//                     
            sManagement.changeInteractionResponse(channelId, interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SurveyManagement.WEB, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), SurveyManagement.OPTION2, interactionresponse.getQuestion5(), 0, 5, SurveyManagement.RUNNING);

        }
    } else if (interactionresponse.getIcurrent() == 5) {
        sManagement.changeInteractionResponse(channelId, interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SurveyManagement.WEB, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), SurveyManagement.OPTION2, interactionresponse.getIcurrent(), SurveyManagement.CLOSED);

    }
} else if (body.equalsIgnoreCase(survey.questionAndOptions[iCurrent].optionTags[2])) {

    if (interactionresponse.getIcurrent() == 1) {
        // sManagement.changeInteractionResponse(channelId, from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), YES, interactionresponse.getOffer(), interactionresponse.getGreetingsresponse(), interactionresponse.getQuestion1(), A, interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 0);

        if (interactionresponse.getQuestion1() == null) {
            //user not found
        } else {

%>
<div class="hero-unit" align="left">
    <h3>Question 2: <i>"<%=interactionresponse.getQuestion2()%>"</i></h3>
    <form action="websurveyQA.jsp" method="POST" >
        <input type="hidden" id="Body" name="Body">
        <input type="hidden" id="From" name="From">
        <h3><input type="radio" name="tenthquestionoption" id="tenthquestionone"><i><%=survey.questionAndOptions[1].optionTags[0] + ")"%></i><i><%=survey.questionAndOptions[1].options[0]%></i></h3>
        <h3><input type="radio" name="tenthquestionoption" id="tenthquestiontwo"><i><%=survey.questionAndOptions[1].optionTags[1] + ")"%></i><i><%=survey.questionAndOptions[1].options[1]%></i></h3>
        <h3><input type="radio" name="tenthquestionoption" id="tenthquestionthree"><i><%=survey.questionAndOptions[1].optionTags[2] + ")"%></i><i><%=survey.questionAndOptions[1].options[2]%></i></h3>
        <input type="hidden" id="questiontenthanswer" name="questiontenthanswer">
        <input type="submit" class="btn-primary" onclick="questiontenth('<%=survey.questionAndOptions[1].optionTags[0]%>', '<%=survey.questionAndOptions[1].optionTags[1]%>', '<%=survey.questionAndOptions[1].optionTags[2]%>', '<%=from%>')">
    </form>
</div>
<script>
    function questiontenth(check1, check2, check3, from) {

        if (document.getElementById('tenthquestionone').checked) {
            document.getElementById("questiontenthanswer").value = check1;
            document.getElementById("Body").value = check1;
            document.getElementById("From").value = from;
        }
        else if (document.getElementById('tenthquestiontwo').checked) {

            document.getElementById("Bquestiontenthanswerody").value = check2;
            document.getElementById("Body").value = check2;
            document.getElementById("From").value = from;
        } else if (document.getElementById('tenthquestionthree').checked) {

            document.getElementById("questiontenthanswer").value = check3;
            document.getElementById("Body").value = check3;
            document.getElementById("From").value = from;
        }

    }
</script> 
<%

//                        message = "Question 2\n" + interactionresponse.getQuestion2()
//                                + "\n" + survey.questionAndOptions[1].optionTags[0] + " "
//                                + survey.questionAndOptions[1].options[0]
//                                + "\n" + survey.questionAndOptions[1].optionTags[1] + " "
//                                + survey.questionAndOptions[1].options[1]
//                                + "\n" + survey.questionAndOptions[1].optionTags[2] + " "
//                                + survey.questionAndOptions[1].options[2];
//                       
        sManagement.changeInteractionResponse(channelId, interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SurveyManagement.WEB, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), SurveyManagement.OPTION3, interactionresponse.getQuestion2(), 0, interactionresponse.getQuestion3(), 0, interactionresponse.getQuestion4(), 0, interactionresponse.getQuestion5(), 0, 2, SurveyManagement.RUNNING);

    }
} else if (interactionresponse.getIcurrent() == 2) {
    // sManagement.changeInteractionResponse(channelId, from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), NO, interactionresponse.getOffer(), interactionresponse.getGreetingsresponse(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), A, interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 0);

    if (interactionresponse.getQuestion1() == null) {
        //user not found
    } else {

%>
<div class="hero-unit" align="left">
    <h3>Question 3: <i>"<%=interactionresponse.getQuestion3()%>"</i></h3>
    <form action="websurveyQA.jsp" method="POST" >
        <input type="hidden" id="Body" name="Body">
        <input type="hidden" id="From" name="From">
        <h3><input type="radio" name="eleventhquestionoption" id="eleventhquestionone"><i><%=survey.questionAndOptions[2].optionTags[0] + ")"%></i><i><%=survey.questionAndOptions[2].options[0]%></i></h3>
        <h3><input type="radio" name="eleventhquestionoption" id="eleventhquestiontwo"><i><%=survey.questionAndOptions[2].optionTags[1] + ")"%></i><i><%=survey.questionAndOptions[2].options[1]%></i></h3>
        <h3><input type="radio" name="eleventhquestionoption" id="eleventhquestionthree"><i><%=survey.questionAndOptions[2].optionTags[2] + ")"%></i><i><%=survey.questionAndOptions[2].options[2]%></i></h3>
        <input type="hidden" id="questionelevenanswer" name="questionelevenanswer">
        <input type="submit" class="btn-primary" onclick="questioneleven('<%=survey.questionAndOptions[2].optionTags[0]%>', '<%=survey.questionAndOptions[2].optionTags[1]%>', '<%=survey.questionAndOptions[2].optionTags[2]%>', '<%=from%>')">
    </form>
</div>
<script>
    function questioneleven(check1, check2, check3, from) {

        if (document.getElementById('eleventhquestionone').checked) {

            document.getElementById("questionelevenanswer").value = check1;
            document.getElementById("Body").value = check1;
            document.getElementById("From").value = from;
        } else if (document.getElementById('eleventhquestiontwo').checked) {

            document.getElementById("questionelevenanswer").value = check2;
            document.getElementById("Body").value = check2;
            document.getElementById("From").value = from;
        } else if (document.getElementById('eleventhquestionthree').checked) {

            document.getElementById("questionelevenanswer").value = check3;
            document.getElementById("Body").value = check3;
            document.getElementById("From").value = from;
        }

    }
</script> 
<%

//                        message = "Question 3\n" + interactionresponse.getQuestion3()
//                                + "\n" + survey.questionAndOptions[2].optionTags[0] + " "
//                                + survey.questionAndOptions[2].options[0]
//                                + "\n" + survey.questionAndOptions[2].optionTags[1] + " "
//                                + "\n" + survey.questionAndOptions[2].options[1]
//                                + "\n" + survey.questionAndOptions[2].optionTags[2] + " "
//                                + "\n" + survey.questionAndOptions[2].options[2];
//                       
        sManagement.changeInteractionResponse(channelId, interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SurveyManagement.WEB, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), SurveyManagement.OPTION3, interactionresponse.getQuestion3(), 0, interactionresponse.getQuestion4(), 0, interactionresponse.getQuestion5(), 0, 3, SurveyManagement.RUNNING);

    }
} else if (interactionresponse.getIcurrent() == 3) {

    if (interactionresponse.getQuestion1() == null) {
        //user not found
    } else {

%>
<div class="hero-unit" align="left">
    <h3>Question 4: <i>"<%=interactionresponse.getQuestion4()%>"</i></h3>
    <form action="websurveyQA.jsp" method="POST" >
        <input type="hidden" id="Body" name="Body">
        <input type="hidden" id="From" name="From">
        <h3><input type="radio" name="twelthquestionoption" id="twelthquestionone"><i><%=survey.questionAndOptions[3].optionTags[0] + ")"%></i><i><%=survey.questionAndOptions[3].options[0]%></i></h3>
        <h3><input type="radio" name="twelthquestionoption" id="twelthquestiontwo"><i><%=survey.questionAndOptions[3].optionTags[1] + ")"%></i><i><%=survey.questionAndOptions[3].options[1]%></i></h3>
        <h3><input type="radio" name="twelthquestionoption" id="twelthquestionthree"><i><%=survey.questionAndOptions[3].optionTags[2] + ")"%></i><i><%=survey.questionAndOptions[3].options[2]%></i></h3>
        <input type="hidden" id="questiontweleanswer" name="questiontweleanswer">
        <input type="submit" class="btn-primary" onclick="questiontwele('<%=survey.questionAndOptions[3].optionTags[0]%>', '<%=survey.questionAndOptions[3].optionTags[1]%>', '<%=survey.questionAndOptions[3].optionTags[2]%>', '<%=from%>')">
    </form>
</div>
<script>
    function questiontwele(check1, check2, check3, from) {

        if (document.getElementById('twelthquestionone').checked) {

            document.getElementById("questiontweleanswer").value = check1;
            document.getElementById("Body").value = check1;
            document.getElementById("From").value = from;
        }
        else if (document.getElementById('twelthquestiontwo').checked) {

            document.getElementById("questiontweleanswer").value = check2;
            document.getElementById("Body").value = check2;
            document.getElementById("From").value = from;
        } else if (document.getElementById('twelthquestionthree').checked) {

            document.getElementById("questiontweleanswer").value = check3;
            document.getElementById("Body").value = check3;
            document.getElementById("From").value = from;
        }

    }
</script> 
<%
//                        message = "Question 4\n" + interactionresponse.getQuestion4()
//                                + "\n" + survey.questionAndOptions[3].optionTags[0] + " "
//                                + survey.questionAndOptions[3].options[0]
//                                + "\n" + survey.questionAndOptions[3].optionTags[1] + " "
//                                + survey.questionAndOptions[3].options[1]
//                                + "\n" + survey.questionAndOptions[3].optionTags[2] + " "
//                                + survey.questionAndOptions[3].options[2];
//                        
        sManagement.changeInteractionResponse(channelId, interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SurveyManagement.WEB, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), SurveyManagement.OPTION3, interactionresponse.getQuestion4(), 0, interactionresponse.getQuestion5(), 0, 4, SurveyManagement.RUNNING);

    }
} else if (interactionresponse.getIcurrent() == 4) {

    if (interactionresponse.getQuestion1() == null) {
        //user not found
    } else {
%>
<div class="hero-unit" align="left">
    <h3>Question 5: <i>"<%=interactionresponse.getQuestion5()%>"</i></h3>
    <form action="websurveyQA.jsp" method="POST" >
        <input type="hidden" id="Body" name="Body">
        <input type="hidden" id="From" name="From">
        <h3><input type="radio" name="thirteenquestionoption" id="thirteenquestionone"><i><%=survey.questionAndOptions[4].optionTags[0] + ")"%></i><i><%=survey.questionAndOptions[4].options[0]%></i></h3>
        <h3><input type="radio" name="thirteenquestionoption" id="thirteenquestiontwo"><i><%=survey.questionAndOptions[4].optionTags[1] + ")"%></i><i><%=survey.questionAndOptions[4].options[1]%></i></h3>
        <h3><input type="radio" name="thirteenquestionoption" id="thirteenquestionthree"><i><%=survey.questionAndOptions[4].optionTags[2] + ")"%></i><i><%=survey.questionAndOptions[4].options[2]%></i></h3>
        <input type="hidden" id="questionthirteenanswer" name="questionthirteenanswer">
        <input type="submit" class="btn-primary" onclick="questionthriteen('<%=survey.questionAndOptions[4].optionTags[0]%>', '<%=survey.questionAndOptions[4].optionTags[1]%>', '<%=survey.questionAndOptions[4].optionTags[2]%>', '<%=from%>')">
    </form>
</div>
<script>
    function questionthriteen(check1, check2, check3, from) {

        if (document.getElementById('thirteenquestionone').checked) {

            document.getElementById("questionthirteenanswer").value = check1;
            document.getElementById("Body").value = check1;
            document.getElementById("From").value = from;
        }
        else if (document.getElementById('thirteenquestiontwo').checked) {

            document.getElementById("questionthirteenanswer").value = check2;
            document.getElementById("Body").value = check2;
            document.getElementById("From").value = from;
        } else if (document.getElementById('thirteenquestionthree').checked) {

            document.getElementById("questionthirteenanswer").value = check3;
            document.getElementById("Body").value = check3;
            document.getElementById("From").value = from;
        }

    }
</script> 
<%

//                        message = "Question 5\n" + interactionresponse.getQuestion5()
//                                + "\n" + survey.questionAndOptions[4].optionTags[0] + " "
//                                + survey.questionAndOptions[4].options[0]
//                                + "\n" + survey.questionAndOptions[4].optionTags[1] + " "
//                                + survey.questionAndOptions[4].options[1]
//                                + "\n" + survey.questionAndOptions[4].optionTags[2] + " "
//                                + survey.questionAndOptions[4].options[2];
//                        
                sManagement.changeInteractionResponse(channelId, interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SurveyManagement.WEB, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(),
                        interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), SurveyManagement.OPTION3, interactionresponse.getQuestion5(), 0, 5, SurveyManagement.RUNNING);

            }
        }
    }
} else if (interactionresponse.getIcurrent() == 5) {
    sManagement.changeInteractionResponse(channelId, interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SurveyManagement.WEB, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), SurveyManagement.OPTION3, interactionresponse.getIcurrent(), SurveyManagement.CLOSED);
%>

<div class="hero-unit" align="left">
    <h3><i>"<%=interactionresponse.getOffer()%>"</i></h3>

</div>

<%

            }
        }
    } catch (Exception ex) {
        ex.printStackTrace();

    }

%>
<script>
    window.onbeforeunload = function() {
//                return "Are you sure you want to close the window?";
    return "Are you sure you want to close the window?";
            var s = '../closeSurvey';
            $.ajax({
            type: 'POST',
                    url: s,
                    dataType: 'json',
                    data: $("#closesurveyid").serialize(),
                    success: function(data) {
                    if (strcmpSurveys(data._result, "error") == 0) {
                    Alert4Surveys("<span><font color=red>" + data._message + "</font></span>");
                            window.setTimeout(RefreshSurveysList, 2000);
                    }
                    else if (strcmpSurveys(data._result, "success") == 0) {


                    }
                    }
            });
    }
</script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

    </body>
</html>

<%@include file="footer.jsp" %>