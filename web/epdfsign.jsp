<%@page import="com.mollatech.axiom.nucleus.db.Doccategory"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CategoryManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Signingrequest"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SignRequestManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/epdfsigning.js"></script>
<%    String sessionId = (String) session.getAttribute("_apSessionID");
    SessionManagement smObj = new SessionManagement();
    int iStatus = smObj.GetSessionStatus(sessionId);
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String channelId = channel.getChannelid();
    SignRequestManagement srm = new SignRequestManagement();
    Signingrequest[] sr = srm.ListEpdfdoc(sessionId, channelId);
    CategoryManagement cm = new CategoryManagement();
    SimpleDateFormat sdf = new SimpleDateFormat("EEE d-M-yyyy,HH:mm ");
%>
<div class="container-fluid">
    <h1 class="text-success">E-Signing Request Management</h1>
    <p>This feature supports for automated signing of Documents </p>
    <h2>Search Resource</h2>
    <legend class=""></legend>   
    <div class="input-append">
        <input type="text" class="span3" id="_keys" name="_keys" placeholder="Search Using Document name,Category.."/>
        <select id="_keysType" name="_keysType" class="span2">
            <option disabled selected value="-1">Search By</option>
            <option value="1">Document Name</option>
            <option value="2">Document Category</option>
        </select>    
        <!--<input type="date"  class="datepicker span1" id="_startdate" name="_startdate"/>-->
        <div class="input-prepend">
            <span class="add-on">From:</span>   
            <div id="datetimepicker1" class="input-append date">
                <input class="span2" id="_startdate" name="_startdate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                <span class="add-on">
                    <i data-time-icon="icon icon-time" data-date-icon="icon icon-calendar"></i>
                </span>
            </div>
        </div>
        <!--<input type="date" class="datepicker span1" id="_enddate" name="_enddate"/>-->
        <div class="input-prepend">
            <span class="add-on">Till:</span>   
            <div id="datetimepicker2" class="input-append date">
                <input class="span2" id="_enddate" name="_enddate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                </span>
            </div>
        </div>
        <a class="btn btn-success" onclick="searchEsignDocuments()">Search</a>
    </div>
    <div id="EsignSearchRecords">
        <h3>Recent E-Signing Requests</h3>         
        <p><a href="addpdfsignpage.jsp" class="btn btn-primary">Add New Request</a></p>
        <table class="display responsive wrap" id="PdfSignTable">
            <thead>
                <tr>
                    <th><b>Sr No</th>
                    <th><b>Name</th>
                    <th><b>Category</th>
                    <th><b>Status</th>
                    <th><b><b>Details</th>            
                                <th><b>Note</th>
                                <th><b>Manage</th>
                                <th><b>Download</th>
                                <th><b>Uploaded On</th>
                                <th><b>Published On</th>
                                <th><b>Last Updated On</th>                
                                </tr>
                                </thead>


                                <% if (sr != null) {
                                        int Length = 0;
                                        if (sr.length > 10) {
                                            Length = 10;
                                        } else {
                                            Length = sr.length;
                                        }
                                        for (int i = 0; i < Length; i++) {
                                            String strStatus = "Active";
                                            if (sr[i].getStatus() == 1) {
                                                strStatus = "Active";
                                            } else {
                                                strStatus = "Suspended";
                                            }
                                            String docStatus = "user-status-value-" + i;
                                            int catid = sr[i].getDoccategory();
                                            Doccategory dc = cm.getCategoryDetail(sessionId, channelId, sr[i].getDoccategory());
                                %>
                                <tr>
                                    <td><%=i + 1%></td>
                                    <td><%=sr[i].getDocname()%></td>
                                    <td><%=dc.getCategoryname()%></td>
                                    <td>
                                        <%if (sr[i].getStatus() == 1) {%>
                                        <div class="label label-orange">Active</div>
                                        <%} else if (sr[i].getStatus() == 0) {%>
                                        <button class="btn btn-mini" id="starttracking"  href="#" onclick="starttracking(<%= sr[i].getSrid()%>)" > &nbsp; &nbsp; Start  &nbsp; &nbsp;</button>
                                        <!--<div class="label label-Default">Suspended</div>-->
                                        <%} else if (sr[i].getStatus() == -1) {%>
                                        <div class="label label-important">Expired</div>
                                        <%} else if (sr[i].getStatus() == 2) {%>
                                        <div class="label label-success">Completed</div>
                                        <%} else {%>
                                        <div class="label label-important">Rejected</div>
                                        <%}%>
                                    </td>
                                    <td>
                                        <a href="./epdfsignTracking.jsp?_docId=<%=sr[i].getSrid()%>" class="btn btn-mini">
                                            View Details
                                        </a>
                                    </td>

                                    <td>
                                        <a href="#" class="btn btn-mini" id="documentNote-<%=i%>" data-toggle="popover" data-trigger="focus"  rel="popover" data-html="true">
                                            Click to View
                                        </a>
                                        <script>
                                            $(function ()
                                            {
                                                $("#documentNote-<%=i%>").popover({title: 'Token Details', content: "<%=sr[i].getNote()%>"});
                                            });
                                            $("a[rel=popover]")
                                                    .click(function (e) {
                                                        e.preventDefault();
                                                    });

                                        </script>
                                    </td>
                                    <td>
                                        <div class="btn-group"> 
                                            <button class="btn btn-mini" id="" <%if (sr[i].getStatus() == 0) {%> <%} else {%> disabled <%}%> >Manage</button>
                                            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown" <%if (sr[i].getStatus() == 0) {%> <%} else {%> disabled <%}%> ><span class="caret"></span></button>
                                            <ul class="dropdown-menu">     
                        <!--                        <li><a href="#" onclick="changepdfdocStatus(<%= sr[i].getSrid()%>, 1, '<%=docStatus%>')">Mark as Active?</a></li>
                                                <li><a href="#" onclick="changepdfdocStatus(<%= sr[i].getSrid()%>, 0, '<%=docStatus%>')">Mark as Suspended?</a></li>
                                                <li class="divider"></li>-->
                                                <li><a href="./editpdfsignpage.jsp?_docId=<%= sr[i].getSrid()%>" >Edit Details</a></li>
                                                <li><a onclick="removeEpdfdoc(<%=sr[i].getSrid()%>)"><font color="red">Remove?</font></a></li>
                                            </ul> 
                                        </div>
                                    </td>
                                    <td> 
                                        <a href="./DownloadEpdf?_docId=<%=sr[i].getSrid()%>&_type=1" class="btn btn-mini" target='_blank'>Latest PDF </a>
                                    </td>
                                    <td><%= sdf.format(sr[i].getUploadedon())%></td>
                                    <td><%= sdf.format(sr[i].getPublishon())%></td>
                                    <td><%= sdf.format(sr[i].getLastupdate())%></td>
                                    <!--            <td> 
                                    <% if ((sr[i].getStatus()) == 2 || (sr[i].getStatus()) == 1) {%>
                                     <button class="btn btn-mini" id="starttracking"  href="#"  disabled="true" > &nbsp; &nbsp; Start  &nbsp; &nbsp;</button>
                                    <% } else {%>
                                    <button class="btn btn-mini" id="starttracking"  href="#" onclick="starttracking('<%= sr[i].getSrid()%>')" > &nbsp; &nbsp; Start  &nbsp; &nbsp;</button>
                                    <%}%>
                                </td>-->
                                </tr> 
                                <%}
                                } else {%>
                                <tr><td>No record found</td><td>No record found</td><td>No record found</td><td>No record found</td><td>No record found</td><td>No record found</td><td>No record found</td><td>No record found</td><td>No record found</td><td>No record found</td><td>No record found</td></tr>
                                <%}%>
                                </table>
                                <br>
                                </div>
                                <p><a href="addpdfsignpage.jsp" class="btn btn-primary">Add New Request</a></p>
                                <!--</div>-->
                                </div>
                                <script>

                                    $(function () {
                                        $('#datetimepicker1').datepicker({
                                            format: 'dd/MM/yyyy',
                                            language: 'pt-BR'
                                        });
                                    });
                                    $(function () {
                                        $('#datetimepicker2').datepicker({
                                            format: 'dd/MM/yyyy',
                                            language: 'pt-BR'
                                        });
                                    });
                                </script>

                                <script>
                                    $(document).ready(function () {
                                        $('#PdfSignTable').DataTable({
                                            responsive: true
                                        });
                                    });
                                </script>




                                <%@include file="footer.jsp" %>