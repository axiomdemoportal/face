<%@include file="header.jsp" %>
<script src="./assets/js/ssomapping.js"></script>
<%
    String keyword = request.getParameter("_ssokeyword");
%>
<div class="container-fluid">
    <h1 class="text-success">SSO Application Management</h1>
    <!--<p>List of users in the system. Their two way authentication management can be managed from this interface.</p>-->
    <h3>Search Users</h3>   
    <div class="input-append">
        <form id="twowayauthForm" name="twowayauthForm">
            <%if (keyword != null) {%>
            <input type="text" id="_ssokeyword" name="_ssokeyword" placeholder="Search using name, phone, email..." class="span4" value="<%=keyword%>"><span class="add-on"><i class="icon-search"></i></span>
                <%} else {%>
            <input type="text" id="_ssokeyword" name="_ssokeyword" placeholder="Search using name, phone, email..." class="span4"><span class="add-on"><i class="icon-search"></i></span>
                <%}%>
            <a href="#" class="btn btn-success" onclick="searchSSOUsers()">Search Now</a>
        </form>
    </div>
    <div id="sso_table_main">
    </div>
    <%if (keyword != null) {
    %>
    <script>
        searchSSOUsers();
    </script>
    <%
        }%>
</div>
<%@include file="footer.jsp" %>