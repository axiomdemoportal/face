<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.operation.Survey"%>
<%@page import="com.mollatech.axiom.nucleus.db.Interactionsexecutions"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.Interactionresponse"%>
<%@page import="com.mollatech.dictum.management.SurveyManagement"%>
<script src="./assets/js/interactions.js"></script>
<%
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String interactionId = request.getParameter("_interactionID");

    String _QNo = request.getParameter("_QNo");
    String _interactionExecutionID = request.getParameter("_interactionExecutionID");
    int iInteractionId = -9999;
    if (interactionId != null) {
        iInteractionId = Integer.parseInt(interactionId);
    }
    int iinteractionExecutionID = -9999;
    if (_interactionExecutionID != null) {
        iinteractionExecutionID = Integer.parseInt(_interactionExecutionID);
    }
    int iQNO = -9999;
    if (_QNo != null) {
        iQNO = Integer.parseInt(_QNo);
    }

    SurveyManagement survey = new SurveyManagement();
    Interactionresponse[] irObj = survey.getIRObjByDate(channel.getChannelid(), iInteractionId, iQNO);

    Interactionsexecutions iresponseObj = survey.getInteractionExecutions(iInteractionId);
    Survey surveyObj = survey.getSurveyQuestionsByIRExecution(iresponseObj);

%>

<div class="tab-pane" id="msgreport">   
    <div class="row-fluid">
        <div class="span6">
            <div class="control-group">                        
                <div class="span3">
                    <div class="control-group form-inline">
                        <a href="#" class="btn btn-info" onclick="interactionReportCSV('<%=iInteractionId%>', '<%=iinteractionExecutionID%>', '<%=iQNO%>')" >
                            <i class="icon-white icon-chevron-down"></i>Download CSV</a>
                    </div>
                </div>
                <div class="span3">
                    <div class="control-group form-inline">
                        <a href="#" class="btn btn-info" onclick="interactionReportPDF('<%=iInteractionId%>', '<%=iinteractionExecutionID%>', '<%=iQNO%>')" >
                            <i class="icon-white icon-chevron-down"></i>Download PDF</a>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-striped" id="table_main">

            <tr>
                <td>No.</td>
                <td>Name</td>
                <td>Contact No</td>
                <td>Answer</td>
                <td>Proocess Date</td>
                <td>Proocess Time</td>
            </tr>
            <% if (irObj != null) {
                    DateFormat f = new SimpleDateFormat("dd/mm/yyyy hh:mm:ss a");
                    DateFormat date = new SimpleDateFormat("dd/mm/yyyy");
                    DateFormat time = new SimpleDateFormat("hh:mm:ss a");
                    for (int i = 0; i < irObj.length; i++) {

                        Date processtime = irObj[i].getProcessTime();

                        String dateObj = date.format(processtime);
                        String timeObj = time.format(processtime);

            %>
            <tr>
                <% //for (int j = 0; j < 5; j++) {%>
                <td><%=(i + 1)%></td>
                <td><%=irObj[i].getUsername()%></td>
                <td><%=irObj[i].getContactno()%></td>
                <%if (iQNO == 0) {
                        String options = "Not Answered";
                        if (irObj[i].getGreetingsresponse() != null) {
                            if (irObj[i].getGreetingsresponse() == 1) {
                                options = surveyObj.greetingOptions[0];
                            } else if (irObj[i].getGreetingsresponse() == 2) {
                                options = surveyObj.greetingOptions[1];
                            }
                        }
                %>

                <td><%= options%></td>

                <%}%>
                <%if (iQNO == 1) {
                        String options = "Not Answered";
                        if (irObj[i].getAns1() != null && irObj[i].getAns1() != 0) {
                            int ians = irObj[i].getAns1();
                            if (ians == 1) {
                                options = surveyObj.questionAndOptions[0].options[0];//option 0
                            } else if (ians == 2) {
                                options = surveyObj.questionAndOptions[0].options[1];//option 0
                            } else if (ians == 3) {
                                options = surveyObj.questionAndOptions[0].options[2];//option 0
                            }
                        }
                %>

                <td><%=options%></td>
                <%}%>
                <%if (iQNO == 2) {
                        String options = "Not Answered";
                        if (irObj[i].getAns2() != null && irObj[i].getAns2() != 0) {
                            int ians = irObj[i].getAns2();
                            if (ians == 1) {
                                options = surveyObj.questionAndOptions[1].options[0];//option 0
                            } else if (ians == 2) {
                                options = surveyObj.questionAndOptions[1].options[1];//option 0
                            } else if (ians == 3) {
                                options = surveyObj.questionAndOptions[1].options[2];//option 0
                            }
                        }
                %>

                <td><%=options%></td>
                <%}%>
                <%if (iQNO == 3) {
                        String options = "Not Answered";
                        if (irObj[i].getAns3() != null && irObj[i].getAns3() != 0) {
                            int ians = irObj[i].getAns3();
                            if (ians == 1) {
                                options = surveyObj.questionAndOptions[2].options[0];//option 0
                            } else if (ians == 2) {
                                options = surveyObj.questionAndOptions[2].options[1];//option 0
                            } else if (ians == 3) {
                                options = surveyObj.questionAndOptions[2].options[2];//option 0
                            }
//                            options = surveyObj.questionAndOptions[2].options[irObj[i].getAns3()];
                        }
                %>

                <td><%=options%></td>
                <%}%>
                <%if (iQNO == 4) {
                        String options = "Not Answered";
                        if (irObj[i].getAns4() != null && irObj[i].getAns4() != 0) {
                            int ians = irObj[i].getAns4();
                            if (ians == 1) {
                                options = surveyObj.questionAndOptions[3].options[0];//option 0
                            } else if (ians == 2) {
                                options = surveyObj.questionAndOptions[3].options[1];//option 0
                            } else if (ians == 3) {
                                options = surveyObj.questionAndOptions[3].options[2];//option 0
                            }
//                            options = surveyObj.questionAndOptions[3].options[irObj[i].getAns4()];
                        }
                %>

                <td><%=options%></td>
                <%}%>
                <%if (iQNO == 5) {
                        String options = "Not Answered";
                        if (irObj[i].getAns5() != null && irObj[i].getAns5() != 0) {
                            int ians = irObj[i].getAns5();
                            if (ians == 1) {
                                options = surveyObj.questionAndOptions[4].options[0];//option 0
                            } else if (ians == 2) {
                                options = surveyObj.questionAndOptions[4].options[1];//option 0
                            } else if (ians == 3) {
                                options = surveyObj.questionAndOptions[4].options[2];//option 0
                            }
//                            options = surveyObj.questionAndOptions[4].options[irObj[i].getAns5()];
                        }
                %>

                <td><%=options%></td>
                <%}%>

<!--                <td><%=irObj[i].getProcessTime()%></td>-->
                <td><%=dateObj%></td>
                <td><%=timeObj%></td>

            </tr>
            <%}
//                }
            } else {%>
            <tr>
                <td><%=1%></td>
                <td>No Records Found</td>
                <td>No Records Found</td>
                <td>No Records Found</td> 
                <td>No Records Found</td>
                <td>No Records Found</td>

                <%}%>
            </tr>
        </table>

        <div class="span6">
            <div class="control-group">                        
                <div class="span3">
                    <div class="control-group form-inline">
                        <a href="#" class="btn btn-info" onclick="interactionReportCSV('<%=iInteractionId%>', '<%=iinteractionExecutionID%>', '<%=iQNO%>')" >
                            <i class="icon-white icon-chevron-down"></i>Download CSV</a>
                    </div>
                </div>
                <div class="span3">
                    <div class="control-group form-inline">
                        <a href="#" class="btn btn-info" onclick="interactionReportPDF('<%=iInteractionId%>', '<%=iinteractionExecutionID%>', '<%=iQNO%>')" >
                            <i class="icon-white icon-chevron-down"></i>Download PDF</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<br><br>