<%@page import="com.mollatech.axiom.nucleus.settings.GlobalChannelSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.settings.SendNotification"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="java.text.SimpleDateFormat"%>


<%
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    String _searchtext = request.getParameter("_searchtext");
//    String _startdate = request.getParameter("_startdate");
//    String _enddate = request.getParameter("_enddate");
    String _startdate = request.getParameter("_startdate");
    String _enddate = request.getParameter("_enddate");

    String _type = request.getParameter("_type");

    DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    Date startDate = null;
    if (_startdate != null && !_startdate.isEmpty()) {
        startDate = (Date) formatter.parse(_startdate);
    }
    Date endDate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        endDate = (Date) formatter.parse(_enddate);
    }
    BulkMSGManagement bMngt = new BulkMSGManagement();

    int type = Integer.parseInt(_type);
    //System.out.println("The message type is " + type);
    Channellogs[] clogobj = bMngt.searchMsgObj(channel.getChannelid(), type, _searchtext, endDate, startDate);

    String start = formatter.format(startDate);
    String end = formatter.format(endDate);

//    String typeSC = "None";
//    if (type == SendNotification.FACEBOOK) {
//        typeSC = "FACEBOOK";
//    } else if (type == SendNotification.LINKEDIN) {
//        typeSC = "LINKEDIN";
//    } else if (type == SendNotification.TWITTER) {
//        typeSC = "TWITTER";
//    }
    final int PUSH = 6;
%>
<h3> Results from <%=start%> to <%=end%> for Push Notification type sent to "<%=_searchtext%>"</i></h3>

<div class="tabbable">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#msgcharts" data-toggle="tab">Charts</a></li>
        <li><a href="#msgreport" data-toggle="tab">Tabular List</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="msgcharts">
            <div class="row-fluid">
                <div class="span12">
                    <div class="control-group">
                        <div class="span4">
                            <div id="MsgReportgraphPush" ></div>

                        </div>
                        <div class="span7">
                            <div id="MsgReportgraphPush1"></div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="tab-pane" id="msgreport">   
            <div class="row-fluid">
                <div class="span6">
                    <div class="control-group">                        
                        <div class="span3">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="pushReportCSV()" >
                                    <i class="icon-white icon-chevron-down"></i>Download CSV</a>
                            </div>
                        </div>
                        <div class="span3">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="pushReportPDF()" >
                                    <i class="icon-white icon-chevron-down"></i>Download PDF</a>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped" id="table_main">

                    <tr>
                        <td>No.</td>
                        <td>Message ID</td>
                        <td>Type</td>
                        <td>Delivered To</td>                                
                        <td>Status</td>
                        <td>Cost</td>
                        <td>Sent On</td>
                        <td>Update On</td>
                    </tr>
                    <% if (clogobj != null) {
                            for (int i = 0; i < clogobj.length; i++) {
//                                String strType = "";
                                String strSource = "";
                                final int PENDING = 2;
                                final int SENT = 0;
                                final int BLOCKED = -5;

                                //final int FAILD = -1;
                                String strStatus = "";

                                String strType = "Android Push";
                                if (clogobj[i].getType() == 19) {
                                    strType = "I-Phone Push";
                                }
//                                

                                if (clogobj[i].getSourceid() == 1) {
                                    strSource = "BULK MSG";
                                } else if (clogobj[i].getSourceid() == 2) {
                                    strSource = "BULK MSG";
                                } else if (clogobj[i].getSourceid() == 3) {
                                    strSource = "BULK MSG";
                                } else if (clogobj[i].getSourceid() == 4) {
                                    strSource = "BULK MSG";
                                }

                                if (clogobj[i].getStatus() == PENDING) {
                                    strStatus = "PENDING";
                                } else if (clogobj[i].getStatus() == SENT) {
                                    strStatus = "SENT";
                                } else if (clogobj[i].getStatus() == BLOCKED) {
                                    strStatus = "BLOCKED";
                                } else {
                                    strStatus = "FAILED";
                                }

                                String strMSGID = clogobj[i].getMsgid();
                                if (strMSGID == null || strMSGID.isEmpty() == true) {
                                    strMSGID = "<span class='label label-Default'>" + "Not Available from Sender" + "</span>";
                                }

                                String social = clogobj[i].getSocial();

                                String device = "";
                                if (social != null && social.isEmpty() == false) {
                                    device = social;
                                }

//                                  float fcost = 0.0f; 
//                               SettingsManagement sMngt = new SettingsManagement();
//                                Object globalobj = sMngt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.GlobalSettings, 1);
//
////                                float fcost = 0.0f;
////                                
//                                if (globalobj != null) {
//                                    GlobalChannelSettings gObj = (GlobalChannelSettings) globalobj;
//                                    if (clogobj[i].getType() == PUSH) {
//                                        fcost = gObj.pushsettingobj.pushcost;
//                                    }
//                                }

                    %>
                    <tr>
                        <td><%=(i + 1)%></td>
                        <td><%=strMSGID%></td>
                       
                        <td><span class="label label-info"><%=strType%></span></td>
                       
                        <td><%=clogobj[i].getEmail()%></td> 
                        <td><%=strStatus%></td> 
                        <%if(clogobj[i].getCost() != null){%>
                        <td><%=clogobj[i].getCost()%></td> 
                        <%}else{%>
                        <td><%=0.0%></td> 
                        <%}%>
                        <td><%=clogobj[i].getSentUtctime()%></td>
                        <td><%=clogobj[i].getLastupdateUtctime()%></td>  
                    </tr>
                    <%}
                    } else {%>
                    <td><%=1%></td>
                    <td>No Records Found</td>
                    <td>No Records Found</td>
                    <td>No Records Found</td>
                    <td>No Records Found</td> 
                    <td>No Records Found</td>
                    <td>No Records Found</td>
                    <td>No Records Found</td>
                    <%}%>

                </table>

                <div class="span6">
                    <div class="control-group">                        
                        <div class="span3">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="pushReportCSV()" >
                                    <i class="icon-white icon-chevron-down"></i>Download CSV</a>
                            </div>
                        </div>
                        <div class="span3">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="pushReportPDF()" >
                                    <i class="icon-white icon-chevron-down"></i>Download PDF</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br><br>