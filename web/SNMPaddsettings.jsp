<%-- 
    Document   : SNMPaddsettings
    Created on : 1 Nov, 2014, 9:49:48 AM
    Author     : bluebricks6
--%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SNMPReceiverManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Snmpreceivertracking"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SchedulerManagement"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.AxiomProtect"%>
<%--<%@page import="com.mollatech.axiom.nucleus.settings.RecieverpdfSettingEntry"%>--%>
<%@page import="com.mollatech.axiom.nucleus.db.Snmpreceiversettings"%>
<%@page import="java.util.List"%>
<%@include file="header.jsp" %>

<script src="./assets/js/snmpsettings.js"></script>
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>

<div class="container-fluid">
    <h1 class="text-success">System Monitoring Settings</h1>
    <p>To facilitate SNMP </p>
    <br>
    <div class="row-fluid">
        <div id="licenses_data_table">
            <table class="table table-striped">
                <tr>
                    <TD ><FONT size=3 ><B>No.</B></FONT></TD>
                    <TD ><FONT size=3 ><B>IP</B></FONT></TD>
                    <TD ><FONT size=3 ><B>PORT</B></FONT></TD>
                    <TD ><FONT size=3 ><B>Manage</B></FONT></TD>
                    <TD ><FONT size=3 ><B>Executions</B></FONT></TD>
                    <TD ><FONT size=3 ><B>LEVEL</B></FONT></TD>
                    <TD ><FONT size=3 ><B>ALIVE</B></FONT></TD>
                    <TD ><FONT size=3 ><B>ERROR</B></FONT></TD>
                    <TD ><FONT size=3 ><B>WARNING</B></FONT></TD>
                    <TD ><FONT size=3 ><B>Created On</B></FONT></TD>
                    <TD ><FONT size=3 ><B>Updated On</B></FONT></TD>
                </tr>
                <!--<tr>-->
                    <%                    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                        SNMPReceiverManagement m = new SNMPReceiverManagement();
                        Snmpreceiversettings[] list1 = m.listsettingentry();

                        String strerr = "No Record Found";
                        if (list1 != null) {
                            for (int i = 0; i < list1.length; i++) {

                                Snmpreceivertracking[] arr = m.getarrReceiverTracking(channel.getChannelid(), list1[i].getSnmpIp());

                                int executionCount = 0;
                                if (arr != null) {
                                    executionCount = arr.length;
                                }

                                String strStatus = "Active";
                                if (list1[i].getSnmpStatus() == 1) {
                                    strStatus = "Active";
                                } else {
                                    strStatus = "Suspended";
                                }
                                String userStatus = "user-status-value-" + i;


                    %>
                <tr>
                    <td><%=i + 1%></td>
                    <td><%=list1[i].getSnmpIp()%></td>
                    <td><%=list1[i].getSnmpPort()%></td>
                    <td>
                        <div class="btn-group">
                            <button class="btn btn-mini" id="<%=userStatus%>"><%=strStatus%></button>
                            <!--<button class="btn btn-mini">Manage</button>-->
                            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#"  onclick="ChangesnmpSettingStatus('<%=list1[i].getSnmpIp()%>', 1, '<%=userStatus%>')" >Mark as Active?</a></li>
                                <li><a href="#" onclick="ChangesnmpSettingStatus('<%=list1[i].getSnmpIp()%>', 0, '<%=userStatus%>')" >Mark as Suspended?</a></li>
                                <li class="divider"></li>
                                <li><a href="#" onclick="loadEditsnmpDetails('<%=list1[i].getSnmpId()%>')">Edit</a></li>
                                <li><a href="#" onclick="deletesnmpsettingentry('<%=list1[i].getSnmpId()%>')"><font color="red">Remove?</font></a></li>

                            </ul>
                        </div>

                        <%

                        %>
                    </td>
                    <td ><%=executionCount%></td>
                    <td><%=list1[i].getSnmpLevel()%></td>
                    <td><%=list1[i].getSnmpAlive()%></td>
                    <td><%=list1[i].getSnmpError()%></td>
                    <td><%=list1[i].getSnmpWarning()%></td>
                    <TD ><%=list1[i].getCreatedOn()%></TD>
                    <td ><%=list1[i].getUpdatedOn()%></td>
                </tr>
                <%
                    }
                } else {

                %>

                <td><%=strerr%></td>
                <td><%=strerr%></td>
                <td><%=strerr%></td>
                <td><%=strerr%></td>
                <td><%=strerr%></td>
                <td><%=strerr%></td>
                <td><%=strerr%></td>
                <td><%=strerr%></td>
                <td><%=strerr%></td>
                <%}%>
            </table>


        </div>
    </div>
    <br>
    <p><a href="#addOperator" class="btn btn-primary" data-toggle="modal">Add New Snmp Server&raquo;</a></p>
    <script language="javascript">
        //listChannels();
    </script>
</div>



<div id="addOperator" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Add SNMP Server</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="AddNewOperatorForm" name="AddNewOperatorForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">IP</label>
                        <div class="controls">
                            <input type="text" id="_snmpip" name="_snmpip" placeholder="set ip" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Port</label>
                        <div class="controls">
                            <input type="text" id="_snmpport" name="_snmpport" placeholder="set port" class="input-small">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Level</label>
                        <div class="controls">

                            <select name="_level" id="_level" class="input-large">
                                <option value="Error">Error</option>
                                <option value="Info">Info</option>
                                <option value="Warning">Warning</option>
                            </select>   
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Alive</label>
                        <div class="controls">
                            <input type="text" id="_alive" name="_alive" placeholder="set alive after" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Error</label>
                        <div class="controls">

                            <select name="_error" id="_error" class="input-large">
                                <option value="Enable">Enable</option>
                                <option value="Disable">Disable</option>
                            </select>   
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Warning</label>
                        <div class="controls">

                            <select name="_warning" id="_warning" class="input-large">
                                <option value="CPU">CPU</option>
                                <option value="RAM">RAM</option>
                            </select>   
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Status</label>
                        <div class="controls">

                            <select name="_status" id="_status" class="input-large">
                                <option value="1">Active</option>
                                <option value="2">Suspended</option>
                            </select>   
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>

    </div>


    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <div class="span3" id="add-new-operator-result"></div>
        <button class="btn btn-primary" onclick="addsnmpSetttings1()" id="addnewOperatorSubmitBut">Create</button>
    </div>
</div>

<div id="editDetails" class=" modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel1">Enter new details to edit</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="editEmployeeForm" name="editEmployeeForm">
<!--                <input type="" readonly id="_snmpipedit" name="_snmpipedit"  >-->
                <fieldset>
                    <div class="control-group">
                        <label class="control-label"  for="username">IP</label>
                        <div class="controls">
                            <input type="text" readonly id="_snmpipedit" name="_snmpipedit" placeholder="set ip" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Port</label>
                        <div class="controls">
                            <input type="text" id="_snmpportedit" name="_snmpportedit" placeholder="set port" class="input-small">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Level</label>
                        <div class="controls">

                            <select name="_leveledit" id="_leveledit" class="input-large">
                                <option value="Error">Error</option>
                                <option value="Info">Info</option>
                                <option value="Warning">Warning</option>
                            </select>   
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Alive</label>
                        <div class="controls">
                            <input type="text" id="_aliveedit" name="_aliveedit" placeholder="set alive after" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Error</label>
                        <div class="controls">

                            <select name="_erroredit" id="_erroredit" class="input-large">
                                <option value="Enable">Enable</option>
                                <option value="Disable">Disable</option>
                            </select>   
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Warning</label>
                        <div class="controls">

                            <select name="_warningedit" id="_warningedit" class="input-large">
                                <option value="CPU">CPU</option>
                                <option value="RAM">RAM</option>
                            </select>   
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Status</label>
                        <div class="controls">

                            <select name="_statusedit" id="_statusedit" class="input-large">
                                <option value="1">Active</option>
                                <option value="2">Suspended</option>
                            </select>   
                        </div>
                    </div>
                </fieldset>
            </form>

        </div>
    </div>  
    <div class="modal-footer">
          <div id="editoperator-result"></div>
          <button class="btn btn-primary" onclick="editsnmpserverdetails()" id="editUserButton">Save</button>
        <button class="btn" data-dismiss="modal" area-hidden="true">Cancel</button>
    </div>
</div>
<%@include file="footer.jsp" %>

