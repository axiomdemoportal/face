<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="com.mollatech.axiom.nucleus.db.Roles"%>
<%@page import="com.mollatech.axiom.nucleus.db.operation.AxiomOperator"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Operators"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<script src="./assets/js/addSettings.js"></script>
<table class="table table-striped" id="table_main">
    <tr>
        <td><input type="checkbox" id="selectall" onclick="checkAll()"></td>
        <td>Sr. No</td>
        <td>Name</td>
        <td>Phone</td>
        <td>Email</td>
        <td>Role</td>
    </tr>
    <%
        int count = 1;
        Channels _apSChannelDetails = (Channels) session.getAttribute("_apSChannelDetails");
        OperatorsManagement oManagement = new OperatorsManagement();
        String _sessionID = (String) session.getAttribute("_apSessionID");
        Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        SessionManagement smObj = new SessionManagement();
        int iStatus = smObj.GetSessionStatus(_sessionID);
        AxiomOperator[] axiomoperatorObj = null;
        String _selectedRole = request.getParameter("_selectedRole");
        String array[] = _selectedRole.split(",");
        String edit = request.getParameter("edit");
        String gname = request.getParameter("gname");
        Map keymap = (Map) request.getSession().getAttribute("keymap");
        Map valuemap = null;
        Map operators = new HashMap();
        String list = "";
        String operatorList = "";
        if (edit != null && gname != null) {
            valuemap = (Map) keymap.get(gname);
            operatorList = (String) valuemap.get("operatorsList");
        }
        Roles roleObj = oManagement.getRoleByRoleId(_apSChannelDetails.getChannelid(), operator.getRoleid());
        if (iStatus == 1) { //active
            axiomoperatorObj = oManagement.ListOperatorsInternal(_apSChannelDetails.getChannelid());
        }
        if (array.length != 0) {
            for (int j = 0; j < array.length; j++)
                for (int i = 0; i < axiomoperatorObj.length; i++) {
                    AxiomOperator axoprObj = axiomoperatorObj[i];
                    int iOprStatus = axoprObj.getiStatus();
                    if (iOprStatus == 1 && axoprObj.getStrRoleName().equalsIgnoreCase(array[j]) || _selectedRole.equalsIgnoreCase("all")) {
                        String oprId = URLEncoder.encode(axoprObj.getStrOperatorid());
//                        if (!operatorList.equals(""))
                        list = list + axoprObj.getStrName() + ",";
                        if (operatorList.contains(axoprObj.getStrName())) {%>
    <td> <input type="checkbox" name='_sel'  id='_sel<%=axoprObj.getStrName()%>' onchange="Updatemonitor('<%=axoprObj.getStrName()%>')" checked></td>
    <td><%= count++%></td>
    <script>
        Updatemonitor('<%=axoprObj.getStrName()%>');
    </script>
    <% } else {
    %>
    <tr>

        <td> <input type="checkbox" name='_sel'  id='_sel<%=axoprObj.getStrName()%>' onchange="Updatemonitor('<%=axoprObj.getStrName()%>')"></td>
        <td><%= count++%></td>
        <% }%>
        <td><%= axoprObj.getStrName()%></td>
        <td><%= axoprObj.getStrPhone()%></td>
        <td><%= axoprObj.getStrEmail()%></td>
        <td><%= axoprObj.getStrRoleName()%></td>
    </tr>
    <%
                    }
                }
        }
        operators.put("opr", list);
        session.setAttribute("oprator", operators);
    %>
    <form id="deleteContactListForm" name="deleteContactListForm">
        <input type="hidden" name="_operatorsList" id="_operatorsList">
    </form>
</table>

<script>
function checkAll() {
   var arrMarkMail = document.getElementsByName("_sel");
   for (var i = 0; i < arrMarkMail.length; i++) {
      arrMarkMail[i].checked = true;
   }
}
</script>