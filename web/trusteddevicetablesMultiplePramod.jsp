<%@page import="com.mollatech.axiom.connector.user.TokenStatusDetails"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Trusteddevice"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.Otptokens"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TrustDeviceManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<script src="./assets/js/trustedDevice.js"></script>
<%
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();
    String _searchtext = request.getParameter("_searchtext");
    UserManagement usermngObj = new UserManagement();
    AuthUser Users[] = null;
    Users = usermngObj.SearchUsers(sessionId, _channelId, _searchtext);
    if(Users==null)
    {
       Users = usermngObj.SearchUsersByID(sessionId, _channelId, _searchtext);  
    }
    if (Users != null) {

%>
<h3>Results for <i>"<%=_searchtext%>"</i> in Trusted Device</h3>

<table class="table table-striped" id="table_main">

    <tr>
        <td>No.</td>
        <td>UserId</td>
        <td>Name</td>
        <td>Email</td>
        <td>Device ID</td>        
        <td>Manage</td>
        <td>Audit</td>
        <td>Attempts Trail</td>
        <td>OS Details</td> 
        <td>Created On</td>
        <td>Last Access On</td>

    </tr>

    <%
        int k=0;
        if (Users != null)
            for (int i = 0; i < Users.length; i++) {

                java.util.Date dCreatedOn = new java.util.Date(Users[i].lCreatedOn);
                java.util.Date dLastUpdated = new java.util.Date(Users[i].lLastAccessOn);
                SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm ");
                String strStatus = null;
                Trusteddevice[] arrdeviceDetails = null;
                Trusteddevice deviceDetails = null;
                TrustDeviceManagement tObj = new TrustDeviceManagement();

//                deviceDetails = tObj.getTrusteddevice(sessionId, _channelId, Users[i].getUserId());
                arrdeviceDetails = tObj.getTrusteddevices(sessionId, _channelId, Users[i].getUserId());

                if (arrdeviceDetails != null) {
                    for (int j = 0; j < arrdeviceDetails.length; j++) {
                        k++;
                        deviceDetails = arrdeviceDetails[j];
                        dCreatedOn = deviceDetails.getCreatedOn();
                        dLastUpdated = deviceDetails.getLastupdatedOn();
                        if ((deviceDetails.getStatus() != 0) == true) {
                            strStatus = "Active";
                        } else {
                            strStatus = "Suspended";
                        }

                        String uidivStatus = "operator-status-value-" + i;

    %>
    <tr id="user_search_<%=Users[i].getUserId()%>">
        <td><%=k%></td>
         <td><a href="#" class="btn btn-mini" onclick="viewUserID('<%=Users[i].getUserId()%>')" >View ID</a></td>
        <td><%=Users[i].getUserName()%></td>
        <td><%=Users[i].getEmail()%></td>
        <!--<td><%=deviceDetails.getAxiomid()%></td>-->
        <td><%=deviceDetails.getDeviceid()%></td>

        <td>
            <div class="btn-group">
                <button class="btn btn-mini" id="<%=uidivStatus%>"><%=strStatus%></button>
                <%-- <button class="btn btn-mini" id="<%=userStatus%>">Manage</button> --%>
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="changeDeviceStatus(1, '<%=Users[i].getUserId()%>', '<%=uidivStatus%>')">Mark as Active?</a></li>
                    <li><a href="#" onclick="changeDeviceStatus(-2, '<%=Users[i].getUserId()%>', '<%=uidivStatus%>')">Mark as Suspended?</a></li>
                    <li><a href="#" onclick="removeTrustedDevice('<%=Users[i].getUserId()%>','<%=deviceDetails.getDeviceno()%>')">Remove?</a></li>
                </ul>
            </div>
        </td>
        <td>
            <div class="btn-group">
                <button class="btn btn-mini">Audit for</button>
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="DeviceAudits('<%=Users[i].getUserId()%>', 'MOBILETRUST', '1day')">Today</a></li>
                    <li><a href="#" onclick="DeviceAudits('<%=Users[i].getUserId()%>', 'MOBILETRUST', '7days')">Last 7 days</a></li>
                    <li class="divider"></li>
                    <li><a href="#" onclick="DeviceAudits('<%=Users[i].getUserId()%>', 'MOBILETRUST', '1month')">Last 30 days</a></li>
                    <li><a href="#" onclick="DeviceAudits('<%=Users[i].getUserId()%>', 'MOBILETRUST', '2months')">Last 2 months</a></li>
                    <li><a href="#" onclick="DeviceAudits('<%=Users[i].getUserId()%>', 'MOBILETRUST', '3months')">Last 3 months</a></li>
                    <li><a href="#" onclick="DeviceAudits('<%=Users[i].getUserId()%>', 'MOBILETRUST', '6months')">Last 6 months</a></li>
                </ul>
            </div>
        </td>
        <td>
            <div class="btn-group">
                <button class="btn btn-mini">Destroyed Clients</button>
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="ClientPasswordTrail('<%=Users[i].getUserId()%>', '1day')">Today</a></li>
                    <li><a href="#" onclick="ClientPasswordTrail('<%=Users[i].getUserId()%>', '7days')">Last 7 days</a></li>
                    <li class="divider"></li>
                    <li><a href="#" onclick="ClientPasswordTrail('<%=Users[i].getUserId()%>', '1month')">Last 30 days</a></li>
                    <li><a href="#" onclick="ClientPasswordTrail('<%=Users[i].getUserId()%>', '2months')">Last 2 months</a></li>
                    <li><a href="#" onclick="ClientPasswordTrail('<%=Users[i].getUserId()%>', '3months')">Last 3 months</a></li>
                    <li><a href="#" onclick="ClientPasswordTrail('<%=Users[i].getUserId()%>', '6months')">Last 6 months</a></li>
                </ul>
            </div>
        </td>

        <td><%=deviceDetails.getOsdetails()%></td>
<!--        <td>
            <a href="#" class="btn btn-mini" id="mobileTemplateBody-<%=i%>"  rel="popover" data-html="true">Click to View</a>
            <script>
                $(function()
                {
                    $("#mobileTemplateBody-<%=i%>").popover({title: 'OS Details', content: "<%=deviceDetails.getOsdetails()%>"});
                });
            </script>

        </td>-->
        <td><%=sdf.format(dCreatedOn)%></td>
        <td><%=sdf.format(dLastUpdated)%></td>
    </tr>
    <% }
            }
        }%>
</table>
<%} else if (Users == null) {%>

<h3>No users found...</h3>

<%}%>
<br><br>
