
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TwowayauthManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Twowayauth"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();
    String _searchtext = request.getParameter("_searchtext");
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
    UserManagement usermngObj = new UserManagement();
    AuthUser Users[] = null;
    Users = usermngObj.SearchUsers(sessionId, _channelId, _searchtext);
    String start = "NA", update = "NA";
    String status = "Disable";
    String type;
    JSONObject json = null;
%>
<script src="./assets/js/usermanagement.js"></script>
<table class="display responsive wrap" id="table_main">
    <thead>

        <tr>
            <th><b>No.</th>
            <th>UserId</th>
            <th><b>Name</th>
            <th><b>Email</th>
            <th><b>SMS</th>
            <th><b>Voice Callback</th>
            <th><b>Voice Missed Call</th>
            <th><b>Push</th>
            <th><b>Audit</th>
            <th><b>Created On</th>
            <th><b>Last Updated On</th>
        </tr>

    </thead>
    <%if (Users != null) {
            for (int j = 0; j < Users.length; j++) {
                start = "NA";
                update = "NA";
                json = null;
                Twowayauth twowayauth = new TwowayauthManagement().getAuthDetailsByUserId(_channelId, Users[j].getUserId(), sessionId);
                if (twowayauth != null) {
                    type = twowayauth.getType();
                    json = new JSONObject(type);
                    start = sdf.format(twowayauth.getCreationdatetime());
                    update = sdf.format(twowayauth.getLastaccessdatetime());

                }
    %>
    <tr>
        <td><%= j + 1%></td>
        <td><a href="#" class="btn btn-mini" onclick="viewUserID('<%=Users[j].getUserId()%>')" >View ID</a></td>
        <td><%= Users[j].getUserName()%></td>
        <td><%= Users[j].getEmail()%></td>

        <td>
            <div class="btn-group">
                <%
                    status = "Suspended";
                    if (json != null) {
                        status = json.getString("SMS");
                    }%>
                <button class="btn btn-mini" id="SMStype"><%=status%></button>
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="ChangeAuthType(1, 1, '<%=_searchtext%>', '<%=Users[j].getUserId()%>')">Active</a></li>
                    <li><a href="#" onclick="ChangeAuthType(1, 0, '<%=_searchtext%>', '<%=Users[j].getUserId()%>')">Suspended</a></li>
                </ul>
            </div>
        </td>
        <td><div class="btn-group">
                <%
                    status = "Suspended";
                    if (json != null) {
                        status = json.getString("Callback");
                    }%>
                <button class="btn btn-mini" id="Callbacktype" ><%=status%></button>
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="ChangeAuthType(2, 1, '<%=_searchtext%>', '<%=Users[j].getUserId()%>')">Active</a></li>
                    <li><a href="#" onclick="ChangeAuthType(2, 0, '<%=_searchtext%>', '<%=Users[j].getUserId()%>')">Suspended</a></li>

                </ul>
            </div></td>
        <td><div class="btn-group">
                <%
                    status = "Suspended";
                    if (json != null) {
                        status = json.getString("Missed");
                    }%>
                <button class="btn btn-mini" id="Missedtype"><%=status%></button>
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="ChangeAuthType(3, 1, '<%=_searchtext%>', '<%=Users[j].getUserId()%>')">Active</a></li>
                    <li><a href="#" onclick="ChangeAuthType(3, 0, '<%=_searchtext%>', '<%=Users[j].getUserId()%>')">Suspended</a></li>

                </ul>
            </div></td>
        <td><div class="btn-group">
                <%
                    status = "Suspended";
                    if (json != null) {
                        status = json.getString("Push");
                    }%>
                <button class="btn btn-mini" id="Pushtype"><%=status%></button>
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="ChangeAuthType(4, 1, '<%=_searchtext%>', '<%=Users[j].getUserId()%>')">Active</a></li>
                    <li><a href="#" onclick="ChangeAuthType(4, 0, '<%=_searchtext%>', '<%=Users[j].getUserId()%>')">Suspended</a></li>
                    <li><a href="#" onclick="sendPushRegcode('<%=Users[j].getUserId()%>', '<%=_searchtext%>')">(Re) Send Reg Code</a></li>

                </ul>
            </div></td>
        <td>
            <a href="#/" class="btn btn-mini" <%if (twowayauth == null) {%> onclick="return false" disabled <%} else {%>onclick="downloadTwowayAuthAudit('<%=Users[j].getUserId()%>', '<%=Users[j].getUserName()%>')"<%}%>>Audit Download</a>
        </td>
        <td><%= start%></td>
        <td><%= update%></td>
    </tr>
    <%}
    } else {%>
    <tr>
        <td>No Record Found</td>
        <td>No Record Found</td>
        <td>No Record Found</td>
        <td>No Record Found</td>
        <td>No Record Found</td>
        <td>No Record Found</td>
        <td>No Record Found</td>
        <td>No Record Found</td>
        <td>No Record Found</td>
        <td>No Record Found</td>
    </tr>
    <%}%>
</table>

<script>
    $(document).ready(function () {
        $('#table_main').DataTable({
            responsive: true
        });
    });
</script>