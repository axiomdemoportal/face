<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Templates"%>
<%@page import="com.mollatech.axiom.nucleus.db.Pushmessagemappers"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.Pushmessages"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.PushMessageManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.operation.TemplateVariables"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/pushmappers.js"></script>
<%    String _sessionID = (String) session.getAttribute("_apSessionID");
    SessionManagement smObj = new SessionManagement();
    int iStatus = smObj.GetSessionStatus(_sessionID);
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String strTID = request.getParameter("_cid");
    int _callerid = Integer.valueOf(strTID);
    Pushmessagemappers[] PushmessagemappersObj = null;
    Pushmessages pmcObj = null;
    if (iStatus == 1) { //active
        smObj.UpdateSession(_sessionID);
        PushMessageManagement cmObj = new PushMessageManagement();
        PushmessagemappersObj = cmObj.ListPushmappers(_sessionID, channel.getChannelid(), _callerid);
        pmcObj = cmObj.getPushmessage(_sessionID, channel.getChannelid(), _callerid);
        cmObj = null;

    }
%>
<div class="container-fluid">
    <h1 class="text-success">Messages under caller "<%=pmcObj.getCallerName()%>"</h1>    
    <p>List of Messages that will be accepted from Caller system. You can create any number its template and implementation module can be managed here.</p>

    <br>
    <div class="row-fluid">
        <div id="licenses_data_table">
            <table class="table table-striped">
                <tr>
                    <td>No.</td>
                    <td>Message Identifier</td>
                    <td>Message Type</td>
                    <td>Associated Template</td>
                    <td>Manage</td>
                    <td>Send Via</td>
                    <td>Implementation Class</td>
                </tr>
                <%
                    out.flush();
                    PushMessageManagement tmObj = new PushMessageManagement();
                    if (PushmessagemappersObj != null)
                        for (int i = 0; i < PushmessagemappersObj.length; i++) {
                            Pushmessagemappers axcObj = PushmessagemappersObj[i];
                            SimpleDateFormat sdf = new SimpleDateFormat("d MMM,yyyy HH:mm ");
                            int tid = axcObj.getTemplateid();
                            Templates templates = null;
                            TemplateManagement tmp = new TemplateManagement();
                            templates = tmp.LoadTemplate(_sessionID, channel.getChannelid(), tid);
                            String Type = "";
                            String sendvia = null;
                            if (axcObj.getSendvia() == 1) {
                                sendvia = "SMS";
                            } else if (axcObj.getSendvia() == 2) {
                                sendvia = "VOICE";
                            } else if (axcObj.getSendvia() == 3) {
                                sendvia = "USSD";
                            } else if (axcObj.getSendvia() == 4) {
                                sendvia = "EMAIL";

                            }
                            if (templates != null) {
                                if (templates.getType() == 1) {
                                    Type = "Mobile";
                                } else if (templates.getType() == 2) {
                                    Type = "Email";
                                }
                            }
                            String tempName = "";
                            if (templates != null) {
                                tempName = templates.getTemplatename();
                            }
                %>
                <tr>
                    <td><%=(i + 1)%></td>
                    <td><%=axcObj.getMessageformat()%></td>
                    <td><%=Type%></td>
                    <td><%=tempName%></td>
                    <td>
                        <div class="btn-group">
                            <button class="btn btn-mini">Manage</button>
                            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#editPushMappers" onclick="loadEditPushmapperDetails('<%=axcObj.getMapperid()%>')" data-toggle="modal">Edit Message</a></li>
                                <li><a href="#" onclick="removePushmappers(<%=axcObj.getMapperid()%>,<%=axcObj.getCallerid()%>)"><font color="red">Remove Message?</font></a></li>
                            </ul>
                        </div>
                    </td>
                    <td><%=sendvia%></td>
                    <td><%=axcObj.getClassname()%></td>
                </tr>
                <%}%>
            </table>
        </div>
    </div>
    <br>
    <p>

        <a href="./FXpushmessages.jsp" role="button" class="btn" data-toggle="modal"><< Back</a>
        <a href="#addPushMappers" role="button" class="btn btn-primary" data-toggle="modal">Add New Message >></a>
    </p>

</div>
<!-- Modal -->
<div id="addPushMappers" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Add New FX Message under "<%=pmcObj.getCallerName()%>" </h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="addISOMessageForm">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_CallerId" name="_CallerId" value=<%=strTID%> >
                    <div class="control-group">
                        <label class="control-label"  for="username">Message Identifier</label>
                        <div class="controls">
                            <input type="text" id="_Messageformat" name="_Messageformat" placeholder="e.g. 2210D03840000A008000000000004100800019 BITMAP for ISO8583" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Send Via</label>
                        <div class="controls">
                            <select class="span4" name="_templateType" id="_templateType">
                                <option value="1">SMS</option>
                                <option value="2">Voice</option>
                                <option value="3">USSD</option>
                                <option value="4">Email</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Message Template</label>
                        <div class="controls">
                            <!--<input type="text" id="_templateName" name="_templateName" placeholder="Transaction" class="input-xlarge">-->
                            <input
                                type="text"
                                class="span9 basicTypeahead"
                                autocomplete="off"
                                data-provide="typeahead"
                                name="_templateName" 
                                id="_templateName"
                                placeholder="type template name here"
                                />
                        </div>
                    </div>
                    <hr>
                    <div class="control-group">
                        <label class="control-label"  for="username">Implementation</label>
                        <div class="controls">
                            <input type="text" id="_templateClass" name="_templateClass" placeholder="e.g. com.mollatech.xyz" class="input-xlarge">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="addPushMappers-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="addpushmapper()" id="buttonaddPushMappers">Add New Message</button>
    </div>
</div>
<div id="editPushMappers" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idEditPushMaperName"></div></h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="editISOMessageForm">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_CallerId" name="_CallerId" value=<%=strTID%> >
                    <input type="hidden" id="_MapperId" name="_MapperId" >
                    <div class="control-group">
                        <label class="control-label"  for="username">Message Identifier</label>
                        <div class="controls">
                            <input type="text" id="_MessageformatE" name="_MessageformatE" placeholder="2210D03840000A008000000000004100800019" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Send Via</label>
                        <div class="controls">
                            <select class="span4" name="_templateTypeE" id="_templateTypeE">
                                <option value="1">SMS</option>
                                <option value="2">Voice</option>
                                <option value="3">USSD</option>
                                <option value="4">Email</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Associated Template</label>
                        <div class="controls">
                            <input
                                type="text"
                                class="span9 basicTypeahead"
                                autocomplete="off"
                                data-provide="typeahead"
                                name="_templateNameE" id="_templateNameE"
                                />
                            <!--<input type="text" id="_templateNameE" name="_templateNameE" placeholder="Transaction" class="input-xlarge">-->
                        </div>
                    </div>
                    <hr>
                    <div class="control-group">
                        <label class="control-label"  for="username">Implementation Class</label>
                        <div class="controls">
                            <input type="text" id="_templateClassE" name="_templateClassE" placeholder="TransactionExecution" class="input-xlarge">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="editPushMappers-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="editpushmappers()" id="buttonEditPushMappers" type="button">Save PushMessage</button>
    </div>
</div>
<script type="text/javascript">
    search();
</script>                  

<%@include file="footer.jsp" %>