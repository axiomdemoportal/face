<%@page import="com.mollatech.axiom.nucleus.db.operation.AxiomQuestionsAndAnswers"%>
<%@page import="com.mollatech.axiom.nucleus.db.Questionsandanswers"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.ChallengeResponsemanagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.QuestionsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Questions"%>
<%@page import="com.mollatech.axiom.common.utils.UtilityFunctions"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/questions.js"></script>
<%
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    String userID = request.getParameter("_userID");
    
    ChallengeResponsemanagement cManagement = new ChallengeResponsemanagement();
    Questionsandanswers qanda=cManagement.getRegisterUser(sessionId, channel.getChannelid(), userID);
    Questions[] questionsObj = null;
    AxiomQuestionsAndAnswers ques=null;  
    int stat = -1;
    
     ques=cManagement.getUserQuestionsandAnswers(sessionId, channel.getChannelid(), userID);
        if(ques!=null)
        {
        
%>
<div class="container-fluid">
    <h1 class="text-success">Users Questions List </h1>
    <div class="row-fluid">
        <div id="licenses_data_table">
            <table class="table table-striped">
                <tr>
                    <td>No</td>
                    <td>Question</td>
                    <td>Status</td>
                 </tr>

                <%
                    out.flush();
                   
                    for (int i = 0; i < ques.webQAndA.length; i++) {
                        QuestionsManagement qManagement = new QuestionsManagement ();
                       Questions q = qManagement.getQuestionById(sessionId, channel.getChannelid(), ques.webQAndA[i].questionid); 
//                        Questions axcObj = questionsObj[i];
                        String strStatus = "SUSPENDED";
                        if(q.getStatus()== 1){
                            strStatus = "ACTIVE";
                        }
                %>
                <tr>
                    <td><%=(i + 1)%></td>
                    <td><%=ques.webQAndA[i].question%></td>
                    <td><%=strStatus%></td>
                 </tr>
                <%}%>
            </table>
            <% 
    } else if(ques == null) { %>
    <h3>No Questions found...</h3>
    <%}%>
        </div>
    </div>
    <br>

    <script language="javascript">
                            //listChannels();
    </script>
</div>
<%@include file="footer.jsp" %>