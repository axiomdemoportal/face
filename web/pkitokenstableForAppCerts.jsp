<%@page import="com.mollatech.axiom.nucleus.db.Certificates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Pkitokens"%>
<%@page import="com.mollatech.axiom.connector.user.TokenStatusDetails"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.Otptokens"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<script src="./assets/js/pkitokens.js"></script>
<script src="./assets/js/caconnector.js"></script>
<script src="./assets/js/ajaxfileupload.js"></script>
<script src="./assets/js/bootstrap-fileupload.js"></script> 
<link href="./assets/css/bootstrap.css" rel="stylesheet">
<link href="./assets/css/bootstrap-fileupload.css" rel="stylesheet">
<script src="./assets/js/otptokens.js"></script>
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<script src="./assets/js/globalsetting.js"></script>
<%

    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();
    String _searchtext = request.getParameter("_searchtext");
    UserManagement usermngObj = new UserManagement();
    AuthUser Users[] = null;
%>
<h3>Results for <i>"<%=_searchtext%>"</i> in Digital Certificate and Tokens</h3>
    <table class="table table-striped" id="table_main">
        <tr>
            <td>Serial</td>
            <td>Domain Name</td>
            <td>Certificate</td>
            <td>Manage</td>
            <td>Audit Trail</td>        
            <td>Issued On</td>
            <td>Expires On</td>
        </tr>
        <%
            Certificates[] certifts = null;
            String strLabelSW = "label";    //unassigned
            String strLabelHW = "label";    //unassigned
            java.util.Date dCreatedOn = null;
            java.util.Date dLastUpdated = null;
            SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm ");
            String strStatus = null;
            TokenStatusDetails pkiDetails[] = null;
            PKITokenManagement pkiObj = new PKITokenManagement();
            String softPkistatus = "Unassigned";
            String hardPkistatus = "Unassigned";
            String serialNumberhard = "------";
            CertificateManagement CObj = new CertificateManagement();

            certifts = CObj.getCertificateListByDomain(sessionId, _channelId, _searchtext);
            if(certifts!=null){
            for (int i = 0; i < certifts.length; i++) {
                if (certifts[i] != null && certifts[i].getCertTyep() != null) {
                    String userStatus = "user-status-value-" + certifts[i].getStatus();
                    String Certistatus = null;
                 if (certifts[i] != null) {
                        if (certifts[i].getStatus()== CertificateManagement.CERT_STATUS_ACTIVE) {
                            Certistatus = "Valid & Active";
                        } else if (certifts[i].getStatus() == CertificateManagement.CERT_STATUS_REVOKED) {
                            Certistatus = "Revoked";
                        } else if (certifts[i].getStatus() == CertificateManagement.CERT_STATUS_EXPIRED) {
                            Certistatus = "Expired";
                        }

                        dCreatedOn = certifts[i].getCreationdatetime();
                        dLastUpdated = certifts[i].getExpirydatetime();
                    } else {
                        Certistatus = "Not Issued";
                    }

                    String sotpStatus = "sotp-status-value-" + 1;

                    String hotpStatus = "hotp-status-value-" + 1;

                    String hotpType = "hotp-type-value-" + 1;

                    String issuedOn = "NA";
                    if (dCreatedOn != null) {
                        issuedOn = sdf.format(dCreatedOn);
                    }

                    String ExpireOn = "NA";
                    if (dLastUpdated != null) {
                        ExpireOn = sdf.format(dLastUpdated);
                    }
                


        %>
           
               
        <tr id="user_search_<%=certifts[i].getSrno()%>">
            <td><%=certifts[i].getSrno()%></td>
            <td><%=certifts[i].getUserid()%></td>        
            <%if (certifts[i] == null || certifts[i].getStatus() != CertificateManagement.CERT_STATUS_ACTIVE) {%>
            <td><a href="#" onclick="generatecertificate('<%=certifts[i].getUserid()%>')" class="btn btn-mini btn-primary">Issue Now</a></td>
            <% } else {%> 
            <td><a href="#" onclick="loadCertificateDetails('<%=certifts[i].getUserid()%>')" class="btn btn-mini">View Certificate</a></td>
            <% }%> 
            <td>
                <div class="btn-group">
                    <button class="btn btn-mini">Manage</button>
                    <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                    <ul class="dropdown-menu">
    <!--                    <li><a href="#" onclick="loadsendcertfile('<%=certifts[i].getUserid()%>')">Share Certificate(via Email)</a></li>-->
                        <li class="divider"></li>                    
                        <li><a href="#" onclick="renewcertificate('<%=certifts[i].getUserid()%>')">Renew Certificate</a></li>                    
                        <li><a href="#" onclick="loadrevoke('<%=certifts[i].getUserid()%>')"><font color="red">Revoke Certificate</font></a></li>
                        <li><a href="#" onclick="loadsendcertfile1('<%=certifts[i].getUserid()%>')">(Re)send PFX File (via email)</a></li>
   <!--                    <li><a href="#" onclick="sendpfxpassword('<%=_searchtext%>')">(Re)send PFX Password (via mobile)</a></li>-->
                    </ul>
                </div>
            </td>

            <td>
                <a href="#" class="btn btn-mini" onclick="loadUserPKITokenAuditDetails('<%=certifts[i].getUserid()%>', '<%=certifts[i].getUserid()%>')">Audit Download</a>
            </td>
            <td><%=certifts[i].getCreationdatetime()%></td>
            <td><%=certifts[i].getExpirydatetime()%></td>
             </tr>
           <%}else{%>
           <tr>
            <td><span class="label label-inverse">Not Available</span></td>
            <td><span class="label label-inverse">Not Available</span></td>
            <td><span class="label label-inverse">Not Available</span></td>
            <td><span class="label label-inverse">Not Available</span></td>
            <td>--</td>
            <td>--</td>
            <td>--</td>
            </tr>
            <%}}}%>
    </table>

  


    
