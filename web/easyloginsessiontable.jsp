<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.util.concurrent.TimeUnit"%>
<!--<script src="./assets/js/jquery.js"></script>
<script src="./assets/js/certificate.js"></script>-->
<script src="./assets/js/easyloginreport.js"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<!--<script src="./assets/js/bootstrap.min.js"></script>-->
<!--<link rel="stylesheet" href="./assets/css/bootstrap.min.css">-->
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">
<link rel="stylesheet" href="./assets/css/datepicker.css">
<script src="./assets/js/bootstrap-datepicker.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@page import="com.mollatech.axiom.nucleus.db.ApEasyloginsession"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.EasyLoginSessionManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.axiom.nucleus.db.ApEasylogin"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.EasyLoginManagement"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();

    String _startdate = request.getParameter("_startdate");
    String _enddate = request.getParameter("_enddate");
    String _searchtext = request.getParameter("_searchStatus");
//    String _user = request.getParameter("_user");
    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    DateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
    Date startdate = null;
    Integer status = 3;
    if (_searchtext != null && !_searchtext.equalsIgnoreCase("3")) {
        status = Integer.parseInt(_searchtext);
    }
    if (_startdate != null && !_startdate.isEmpty()) {
        startdate = (Date) formatter.parse(_startdate);
    }
    Date enddate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        enddate = (Date) formatter.parse(_enddate);
    }
    if (startdate != null && startdate.equals(enddate)) {
        enddate = new Date(startdate.getTime() + TimeUnit.DAYS.toMillis(1));
    }
    ApEasyloginsession[] txdetailses = new EasyLoginSessionManagement().getAllEasyLoginSessionByDate(sessionId, _channelId, startdate, enddate, status);
    session.setAttribute("easyloginSessiondetailses", txdetailses);
    String strmsg = "No Record Found";
%>
<div class="container-fluid">
    <table class="table table-striped" >
        <thead>            
        <th align="center">No.</th>
        <th align="center">User Name</th>
        <th align="center">App IP</th>
        <th align="center">Status</th>
        <th align="center">Message</th>
        <th align="center">Accepted from</th>
        <th align="center">Requested from</th>
        <th align="center">Location</th>       
        <th align="center">Created On</th>            
        </thead>
        <%    if (txdetailses != null) {
                Map map = new HashMap();
                Map map2 = new HashMap();
                String city = null, state = null, country = null, pincode = null;
                for (int i = 0; i < txdetailses.length; i++) {
                    String errmessage = "NA";
                    String latitude = "NA";
                    String longitude = "NA";
                    String strStatus = "NA";
                    String createdOn = "NA";
                    String browser = "NA";
                    String agent = "NA";
                    String location = "NA";
                    String userName = "NA";
                    String deviceName = "NA";
                    String deviceOS = "NA";
                    String deviceVersion = "NA";
                    String deviceId = "NA";
                    String[] deviceDetail = null;
                    if (txdetailses[i].getBrowser() != null) {
                        browser = txdetailses[i].getBrowser();
                    }
                    if (txdetailses[i].getAgent() != null) {
                        agent = txdetailses[i].getAgent();
                    }
                    if (txdetailses[i].getLattitude() != null) {
                        latitude = txdetailses[i].getLattitude();
                    }
                    if (txdetailses[i].getLongitude() != null) {
                        longitude = txdetailses[i].getLongitude();
                    }
                    if (txdetailses[i].getStatus() == EasyLoginSessionManagement.EAPPROVED_STATUS) {
                        strStatus = "Approved";
                    } else if (txdetailses[i].getStatus() == EasyLoginSessionManagement.EEXPIRED_STATUS) {
                        strStatus = "Expired";
                    } else if (txdetailses[i].getStatus() == EasyLoginSessionManagement.EREJECTED_STATUS) {
                        strStatus = "Rejected";
                    }
                    if (txdetailses[i].getErrormsg() != null && !txdetailses[i].getErrormsg().isEmpty()) {
                        errmessage = txdetailses[i].getErrormsg();
                    }
                    if (txdetailses[i].getCreatedon() != null) {
                        createdOn = format.format(txdetailses[i].getCreatedon());
                    }

                    AuthUser user = null;
                    ApEasylogin easyLoginObj = null;
                    if (map.isEmpty()) {
                        //user = new UserManagement().getUser(_channelId, txdetailses[i].getUserid());
                        easyLoginObj = new EasyLoginManagement().getEasyLoginByLoginId(sessionId, _channelId, txdetailses[i].getEasyloginId());
                        map.put(txdetailses[i].getEasyloginId(), easyLoginObj);
                    } else {
                        if (map.get(txdetailses[i].getEasyloginId()) == null) {
                            easyLoginObj = new EasyLoginManagement().getEasyLoginByLoginId(sessionId, _channelId, txdetailses[i].getEasyloginId());
                            map.put(txdetailses[i].getEasyloginId(), easyLoginObj);
                        } else {
                            easyLoginObj = (ApEasylogin) map.get(txdetailses[i].getEasyloginId());
                        }
                    }
                    if (easyLoginObj != null) {
                        deviceDetail = easyLoginObj.getDeviceinfo().split(":");
                        deviceId = easyLoginObj.getDeviceid();
                        if (map2.isEmpty()) {
                            user = new UserManagement().getUser(_channelId, easyLoginObj.getUserid());
                            map.put(easyLoginObj.getUserid(), user);
                        } else {
                            if (map2.get(easyLoginObj.getUserid()) == null) {
                                user = new UserManagement().getUser(_channelId, easyLoginObj.getUserid());
                                map.put(easyLoginObj.getUserid(), user);
                            } else {
                                user = (AuthUser) map.get(easyLoginObj.getUserid());
                            }
                        }

                    }
                    if (user != null) {
                        userName = user.userName;
                    }
                    if (deviceDetail != null) {
                        if (deviceDetail.length >= 1) {
                            deviceOS = deviceDetail[0];
                        }
                        if (deviceDetail.length >= 2) {
                            deviceName = deviceDetail[1];
                        }
                        if (deviceDetail.length >= 3) {
                            deviceVersion = deviceDetail[2];
                        }
                    }
                    if (txdetailses[i].getLocation() != null) {
                        location = txdetailses[i].getLocation();
                        JSONObject json_location = new JSONObject(location);
                        JSONArray array = new JSONArray(json_location.getString("address_components"));
                        location = json_location.getString("formatted_address");
                        JSONObject temp = null;
                        for (int a = 0; a < array.length(); a++) {
                            temp = array.getJSONObject(a);
//                            System.out.println(temp);                            
                            System.out.println(temp.getString("types"));
                            if (temp.getString("types").contains("administrative_area_level_2")) {
                                city = temp.getString("long_name");
                            }
                            if (temp.getString("types").contains("administrative_area_level_1")) {
                                state = temp.getString("long_name");
                            }
//                            if (temp.getString("types").contains("country")) {
//                                country = temp.getString("short_name");
//                            }
//                            if (temp.getString("types").contains("postal_code")) {
//                                pincode = temp.getString("long_name");
//                            }
                        }
                        if (city != null) {
                            city = city + ", ";
                        }
                        if (state != null) {
                            city += state;
                        }
//                        if (country != null) {
//                            location += country + ",";
//                        }
//                        if (pincode != null) {
//                            location += pincode + ",";
//                        }

                    }
        %>
        <tr>
            <td align="center"><%=i + 1%></td>
            <td align="center"><%=userName%></td>
            <td align="center"><%=txdetailses[i].getAppIp()%></td>
            <td align="center">
                <%if (txdetailses[i].getStatus() == 0) {%>
                <span class=" label label-info"><%=strStatus%></span>
                <%}
                    if (txdetailses[i].getStatus() == 1) {%>
                <span class="label label-success"><%=strStatus%></span>
                <%}
                    if (txdetailses[i].getStatus() == -1) {%>
                <span class="label label-orange"><%=strStatus%></span>
                <%}
                    if (txdetailses[i].getStatus() == -2) {%>
                <span class="label label-warning"><%=strStatus%></span>
                <%}%>
            </td>
            <td align="center"><%=errmessage%></td>
<!--            <td><%=browser%></td>-->
<!--            <td><%=agent%></td>-->

            <td align="center">

                <a href="#/" class="btn btn-mini" id="deviceInfoMsgs-<%=i%>"  data-toggle="popover" data-trigger="focus" rel="popover" data-html="true">  
                    <% if (deviceOS.equalsIgnoreCase("Android")) {%>
<!--                    <img src="assets/img/android_os.png" alt="Android" style="width:30px;height:30px;border:0">-->
                    Android
                </a>
                    <%} else if (deviceOS.equalsIgnoreCase("ios")) {%>
<!--                <img src="assets/img/apple_os.png.png" alt="IOS" style="width:30px;height:30px;border:0">-->
                    iOS
                </a>
                <%} else {%>                
<!--                <img src="assets/img/android_os.png" alt="NA" style="width:30px;height:30px;border:0"></a>-->
                    Android
                <%}%>
                <script>
                    $(function ()
                    {
                        $("#deviceInfoMsgs-<%=i%>").popover({title: 'Device Info', content: "Device Name : <%=deviceName%> <br/> Device OS : <%=deviceOS%> <br/> Device Version : <%=deviceVersion%> <br/> Device Id : <%=deviceId%>"});
                    });
                </script>

            </td>

            <td align="center">
                <%if (browser.equalsIgnoreCase("chrome")) {%>
                <a href="#/" class="btn btn-mini" id="agent-<%=i%>" data-toggle="popover" data-trigger="focus" rel="popover" data-html="true">
<!--                    <img src="assets/img/chrome.png" alt="chrome" style="width:30px;height:30px;border:0"/>-->
                    Chrome
                </a>
                <script>
                    $(function ()
                    {
                        $("#agent-<%=i%>").popover({title: 'Browser Detail', content: " Browser : <%=browser%></br> Browser Info : <%=agent%>"});
                    });
                </script>
                <%}%>
                <%if (browser.equalsIgnoreCase("firefox")) {%>
                <a href="#/" class="btn btn-mini" id="agent-<%=i%>" data-toggle="popover" data-trigger="focus" rel="popover" data-html="true">
<!--                    <img src="assets/img/chrome.png" alt="chrome" style="width:42px;height:42px;border:0"/>-->
                    Firefox
                </a>
                <script>
                    $(function ()
                    {
                        $("#agent-<%=i%>").popover({title: 'Browser Detail', content: " Browser : <%=browser%></br> Browser Info : <%=agent%>"});
                    });
                </script>
                <%}%>
            </td>
            <td align="center">
                <a href="#/" class="btn btn-mini" id="locationMsgs-<%=i%>" data-toggle="popover" data-trigger="focus" rel="popover" data-html="true"><%= city%>
                </a>
                <script>
                    $(function ()
                    {
                        $("#locationMsgs-<%=i%>").popover({title: 'Location', content: "<%=location%>"});
                    });
                </script>
            </td>
<!--            <td><%=latitude%></td>
            <td><%=longitude%></td>-->
            <td align="center"><%=createdOn%></td>

        </tr>
        <%
            }
        } else {%>
        <tr>
            <td align="center"><%=strmsg%></td>
            <td align="center"><%=strmsg%></td>
            <td align="center"><%=strmsg%></td>
            <td align="center"><%=strmsg%></td>
            <td align="center"><%=strmsg%></td>
            <td align="center"><%=strmsg%></td>
            <td align="center"><%=strmsg%></td>
            <td align="center"><%=strmsg%></td>
<!--            <td><%=strmsg%></td>
            -->            <td align="center"><%=strmsg%></td>
        </tr>
        <%}%>
    </table>

    <div class="row-fluid">   
        <div class="span6">

            <div class="control-group">                        
                <div class="span1">
                    <div class="control-group form-inline">
                        <a href="#" class="btn btn-info" onclick="downloadeasysessionReport(1, '<%=_startdate%>', '<%=_enddate%>')" >
                            CSV</a>
                    </div>
                </div>
                <div class="span1">
                    <div class="control-group form-inline">
                        <a href="#" class="btn btn-info" onclick="downloadeasysessionReport(0, '<%=_startdate%>', '<%=_enddate%>')" >
                            PDF</a>
                    </div>
                </div>
                <div class="span1">
                    <div class="control-group form-inline">
                        <a href="#" class="btn btn-info" onclick="downloadeasysessionReport(2, '<%=_startdate%>', '<%=_enddate%>')" >
                            TEXT</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>