<%@page import="com.mollatech.axiom.connector.mobiletrust.Location"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.MobileTrustManagment"%>
<%@page import="com.mollatech.axiom.nucleus.settings.MobileTrustSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CategoryManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Doccategory"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Users"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserGroupsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SignRequestManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Signingrequest"%>
<%@page import="com.mollatech.axiom.nucleus.db.Srtracking"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SignReqTrackingManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/epdfsigning.js"></script>

<%    String sessionId = (String) session.getAttribute("_apSessionID");
    SessionManagement smObj = new SessionManagement();
    int iStatus = smObj.GetSessionStatus(sessionId);
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _docId = request.getParameter("_docId");
    int docId = -1;
    if (_docId != null) {
        docId = Integer.parseInt(_docId);
    }
    SignRequestManagement srm = new SignRequestManagement();
    Signingrequest sr = srm.getEpdfbyId(sessionId, channel.getChannelid(), docId);
    String docName = sr.getDocname();
    SimpleDateFormat sdf = new SimpleDateFormat("EEE d-M-yyyy,HH:mm ");
%>
<div class="container-fluid">
    <h1 class="text-success">Tracking Detail of ' <%= docName%> ' Document</h1>
    <p>This feature supports Tracking for automated signing of documents. </p>
    <br>
    <table class="table table-striped">
        <tr>
            <th>Sr No</th>
            <th>User Name</th> 
            <th>Tracking Status</th>            
            <th>Manage</th>
            <th>Download pdf</th>
            <th>City</th>
            <th>State</th>
            <th>Country</th>
            <th>Started On</th>
            <th>Ended On</th>
            <th>Expired On</th>
        </tr>
        <%
            UserManagement um = new UserManagement();
            SignReqTrackingManagement srtm = new SignReqTrackingManagement();
            Srtracking[] srtrac = srtm.getAllSRTrackingDetailsbydocId(sessionId, channel.getChannelid(), docId);
            if (srtrac != null) {
                for (int i = 0; i < srtrac.length; i++) {
                    AuthUser user = um.getUser(sessionId, channel.getChannelid(), srtrac[i].getUserid());
                    String docStatus = "user-status-value-" + i;
        %>
        <tr>
            <td><%=(i + 1)%></td>          
            <td><%= user.getUserName()%></td>          
            <td>
                <%if (srtrac[i].getStatus() == 0) {%>
                <div class="label label-info"> 
                    Active
                </div>
                <%} else if (srtrac[i].getStatus() == -1) {%>
                <div class="label label-Default"> 
                    Pending    
                </div>
                <%} else if (srtrac[i].getStatus() == -2) {%>
                <div class="label label-Default"> 
                    Suspended    
                </div>
                <%} else if (srtrac[i].getStatus() == 2) {%>
                <div class="label label-important"> 
                    Rejected
                </div>
                <%} else if (srtrac[i].getStatus() == 1) {%>
                <div class="label label-success"> 
                    Approved
                </div>
                <%} else if (srtrac[i].getStatus() == 3) {%>
                <div class="label label-success"> 
                    Completed
                </div>
                <%} else if (srtrac[i].getStatus() == -3) {%>
                <div class="label label-important"> 
                    Expired
                </div>
                <%}%>
            </td>
            <td>
                <div class="btn-group"> 
                    <button class="btn btn-mini" id="">Manage</button>
                    <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                    <ul class="dropdown-menu">            
                        <% if (srtrac[i].getStatus() == -3) {
                        %>
                        <li><a href="#" onclick="changeTrackingStatus(<%= srtrac[i].getTrackingid()%>, 0, '<%=docStatus%>',<%= srtrac[i].getDocid()%>)">Mark as Active?</a></li>
                        <!--<li><a href="#" onclick="changeTrackingStatus(<%= srtrac[i].getTrackingid()%>, -2, '<%=docStatus%>',<%= srtrac[i].getDocid()%>)">Mark as Suspended?</a></li>-->
                        <!--<li><a href="#" onclick="changeTrackingStatus(<%= srtrac[i].getTrackingid()%>, 3, '<%=docStatus%>',<%= srtrac[i].getDocid()%>)">Mark as Completed?</a></li>-->
                        <li class="divider"></li>
                            <%}%>
                        <li><a href="#" onclick="sendNotification(<%= srtrac[i].getTrackingid()%>, 3, '<%=docStatus%>',<%= srtrac[i].getDocid()%>)">Send Notification</a></li>
                    </ul> </div>
            </td>
            <td>
                <a href="./DownloadEpdf?_docId=<%=srtrac[i].getTrackingid()%>&_type=2" class="btn btn-mini" target='_blank'>Download PDF </a>
            </td>
            <%
                MobileTrustManagment mtm = new MobileTrustManagment();
                Location location = mtm.getLocationByLongAndLat(srtrac[i].getLongitude(), srtrac[i].getLattitude());
            %>
            <% if (location != null) {%>
            <td>                
                <%=location.city%>
            </td>
            <td>
                <%= location.state%>
            </td>
            <td>
                <%= location.country%>
            </td>
            <%} else {%>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <%}%>
            <td>
                <%if (srtrac[i].getStartedonon() != null) {%>
                <%=sdf.format(srtrac[i].getStartedonon())%>
                <%} else {%>
                Not Yet Started <%}%>                
            </td>
            <td>
                <%if (srtrac[i].getEndon() != null) {%>
                <%=sdf.format(srtrac[i].getEndon())%>
                <%} else {%>
                Not yet<%}%>
            </td>
            <td>
                <%if (srtrac[i].getExpiredon() != null) {%>
                <%=sdf.format(srtrac[i].getExpiredon())%>
                <%} else {%>Not yet Started<%}%>
            </td>    
        </tr>
        <%}
        } else {%>
        <tr><td>No Record Found</td><td>No Record Found</td><td>No Record Found</td><td>No Record Found</td><td>No Record Found</td><td>No Record Found</td><td>No Record Found</td><td>No Record Found</td><td>No Record Found</td><td>No Record Found</td><td>No Record Found</td></tr>
        <%}%>
    </table>
    <p><a class="btn btn-primary" onclick="backTracking()">Back</a></p>

    <br>
    <!--<p><a href="addpdfsignpage.jsp" class="btn btn-primary">Add Pdf Sign</a></p>-->
    <!--</div>-->
</div>
<%@include file="footer.jsp" %>