<%@page import="com.mollatech.axiom.nucleus.db.Systemmessagereceivertracking"%>
<%@page import="com.mollatech.axiom.nucleus.db.Systemmessagesettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SystemMessageManagement"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.mollatech.axiom.nucleus.settings.PasswordPolicySetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.dictum.management.ContactManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/systemMessage.js"></script>
<%   
     try{
      String _sessionID = (String) session.getAttribute("_apSessionID");
      Operators operartorObj = (Operators) request.getSession().getAttribute("_apOprDetail");
    SessionManagement smObj = new SessionManagement();
    int iStatus = smObj.GetSessionStatus(_sessionID);
    AxiomChannel[] channelsObj = null;
    if (iStatus == 1) { //active
        smObj.UpdateSession(_sessionID);
        ChannelManagement cmObj = new ChannelManagement();
        channelsObj = cmObj.ListChannelsInternal();
        cmObj = null;
    }

//    if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() < 4) {
//        return;
//    }
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");

     SettingsManagement sMngt = new SettingsManagement();
    PasswordPolicySetting passObj = null; Object obj = sMngt.getSetting(_sessionID, channel.getChannelid(), SettingsManagement.PASSWORD_POLICY_SETTING, 
            SettingsManagement.PREFERENCE_ONE);
      String resP = (String)request.getSession().getAttribute("_passewordExpiry");
                    if(resP == null){
                    if (obj != null) {
                        if (obj instanceof PasswordPolicySetting) {
                            passObj = (PasswordPolicySetting) obj;
                                                   if (operartorObj.getRoleid() != 0) {
                                if (passObj.passwordExpiryTime != 99) {//password never expire
                                    Date passupdateDate = operartorObj.getPasswordupdatedOn();
                                    Calendar current = Calendar.getInstance();
                                    Calendar current1 = Calendar.getInstance();
                                    if (passupdateDate != null) {
                                        current.setTime(passupdateDate);
                                        current.add(Calendar.DATE, passObj.passwordExpiryTime);
                                        current.add(Calendar.DATE,-7);
                                        Date d = new Date();
                                        current1.setTime(d);
                                        Date currentDate = current1.getTime();
                                        Date expireDate = current.getTime();
                                        if (currentDate.after(expireDate)) {
                                            session.setAttribute("_passewordExpiry", "yes");
                                            resP = "yes";
                                          
                                        }
                                    }

                                }
                            }
                        }
                    }
                    }
       String resL = (String)request.getSession().getAttribute("_lastLoginOn");
       if(resL == null){
          resL = "yes";
       }
       
        Date _lastlogintime = (Date)request.getSession().getAttribute("_lastlogintime");
        
       
       if(resL.equals("yes")){
        SimpleDateFormat tzL = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
        String completeTimewithLocalTZL = tzL.format(_lastlogintime);
        session.setAttribute("_lastLoginOn", completeTimewithLocalTZL);
       }

%>

<div class="container-fluid">
    <h1 class="text-success">Channels Management</h1>
    <p>List of all channels and their management. Please be careful as it will impact the system usage from other entities.</p>
    <br>
    <%
                if(resP == null){
                    resP = "no";
                }
                 if(resP.equals("yes")){
                  
                  session.setAttribute("_passewordExpiry", "No");
    %>
    <div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  <strong>Warning!</strong> Your password is going to expire, Please Change your password!!!
  </div>
    <%}%>
     <%
                 
                 if(resL.equals("yes")){
                     String resL1 = (String)request.getSession().getAttribute("_lastLoginOn");
                 
                   session.setAttribute("_lastLoginOn", "No");
    %>
    <div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  <strong>Warning!</strong> Last logged on <%=resL1%>!!!
  </div>
    <%}%>
   
    <%        
        //added by supriya
        SystemMessageManagement m = new SystemMessageManagement();
        int opid = operartorObj.getRoleid();
        Systemmessagesettings[] list1 = m.listmessagesettingentrybyAlertTo(String.valueOf(opid));
        if (list1 != null) {
            for (int i = 0; i < list1.length; i++) {
                Systemmessagereceivertracking arr = m.getarrmessageTracking(channel.getChannelid(), list1[i].getMessageId(), list1[i].getAlertTo());
                if (arr == null) {
                    String message = list1[i].getMessage();
    %>
    <div class="alert alert-warning alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" onclick="addmessagetracking('<%=list1[i].getMessageId()%>', '<%=list1[i].getAlertTo()%>')"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <strong>System Alert Message!</strong><%=message%>!!!
    </div>

    <%
                }
            }
        }
    %>
    
    <div class="row-fluid">
        <div id="licenses_data_table">
            <table class="table table-striped">
                <tr>
                    <td>No.</td>
                    <td>Name</td>
                    <td>VPath</td>
                    <td>Manage</td>
                    <td>Remote Access</td>
                    <td>Sessions</td>
                    <td>Operators</td>
                    <%                       
                       String strProductType1 = LoadSettings.g_sSettings.getProperty("product.type");
                        int iPRODUCT1 = (new Integer(strProductType1)).intValue();
                        if (iPRODUCT1 == 3) {
                    %>

                    <td>Users</td>
                    <td>OTP Tokens</td>
                    <td>PKI Tokens</td>
                    <td>Certificates</td>
                    <%} else if (iPRODUCT1 == 1) {
                    %>                            
                    <td>Contacts</td>
                    <%
                        }

                    %>
                    <td>Created On</td>
                    <td>Updated On</td>                    
                </tr>

                <%                    out.flush();
                    ChannelManagement cmObj = new ChannelManagement();

                    UserManagement umObj = new UserManagement();

                    for (int i = 0;
                            i < channelsObj.length;
                            i++) {
                        AxiomChannel axcObj = channelsObj[i];
                        int iChStatus = axcObj.getiStatus();
                        String strStatus;
                        if (iChStatus == 1) {
                            strStatus = "Active";
                        } else {
                            strStatus = "Suspended";
                        }

                        RemoteAccessManagement rmObj = new RemoteAccessManagement();
                        boolean bRAEnabled = rmObj.IsRemoteAccessEnabled(axcObj.getStrChannelid());

                        java.util.Date dCreatedOn = new java.util.Date(axcObj.utcCreatedOn);
                        java.util.Date dLastUpdated = new java.util.Date(axcObj.lastUpdateOn);

                        String uidiv4RemoteAccess = "channel-ra-value-" + i;
                        String strRemoteAccessStatus = "Enabled";
                        if (bRAEnabled == false) {
                            strRemoteAccessStatus = "Disabled";
                        }

                        String uidiv4ChStatus = "channel-status-value-" + i;

                        SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm ");

                        int userCount = 0;
                        int otptokenCount = 0;
                        int certCount = 0;
                        int pkiCount = 0;
                        int contactsCount = 0;

                        if (iPRODUCT1 == 3) { //axiom
                            userCount = umObj.getCountOfUsers(axcObj.getStrChannelid());
                            OTPTokenManagement otpmObj = new OTPTokenManagement(axcObj.getStrChannelid());
                            otptokenCount = otpmObj.getOTPTokenCount(axcObj.getStrChannelid());

                            PKITokenManagement pkimngObj = new PKITokenManagement();
                            pkiCount = pkimngObj.getPKITokenCount(axcObj.getStrChannelid());

                            CertificateManagement certmngObj = new CertificateManagement();
                            certCount = certmngObj.getCertificateCount(axcObj.getStrChannelid());
                        } else if (iPRODUCT1 == 1) {
                            ContactManagement cntmngObj = new ContactManagement();
                            contactsCount = cntmngObj.getAllContactCountOfChannel(axcObj.getStrChannelid());
                        }


                %>
                <tr>
                    <td><%=i + 1%></td>
                    <td><%=axcObj.getStrName()%></td>
                    <td><%=axcObj.getStrVirtualPath()%></td>
                    <td>
                        <div class="btn-group">
                            <button class="btn btn-mini" id="<%=uidiv4ChStatus%>"><%=strStatus%></button>
                            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeChannelStatus('<%=axcObj.getStrChannelid()%>', 1, '<%=uidiv4ChStatus%>')">Mark as Active?</a></li>
                                <li><a href="#" onclick="ChangeChannelStatus('<%=axcObj.getStrChannelid()%>', 0, '<%=uidiv4ChStatus%>')">Mark as Suspended?</a></li>
                                <li class="divider"></li>
                                <li><a href="#" onclick="ChangeChannelRemoteAccess('<%=axcObj.getStrChannelid()%>', 1, '<%=uidiv4RemoteAccess%>')" >Enable Remote Access?</a></li>
                                <li><a href="#" onclick="ChangeChannelRemoteAccess('<%=axcObj.getStrChannelid()%>', 0, '<%=uidiv4RemoteAccess%>')" >Disable Remove Access?</a></li>
                                <li class="divider"></li>
                                <li><a href="./channelsettings.jsp" data-toggle="modal">View Source Details</a></li>
                                <li><a href="#" onclick="loadEditChannelDetails('<%=axcObj.getStrChannelid()%>')" >Edit Channel</a></li>
                            </ul>
                        </div>
                    </td>
                    <td><div id="<%=uidiv4RemoteAccess%>"><%=strRemoteAccessStatus%></div></td>
                    <td><%=cmObj.getTotalSessions(axcObj.getStrChannelid())%></td>
                    <td><%=cmObj.getOperatorCount(axcObj.getStrChannelid())%></td>

                    <%
                        if (iPRODUCT1 == 3) {
                    %>

                    <td><%=userCount%></td>
                    <td><%=otptokenCount%></td>
                    <td><%=pkiCount%></td>
                    <td><%=certCount%></td>
                    <%} else if (iPRODUCT1 == 1) {
                    %>                            
                    <td><%=contactsCount%></td>
                    <%
                        }

                    %>





                    <td><%=sdf.format(dCreatedOn)%></td>
                    <td><%=sdf.format(dLastUpdated)%></td>
                </tr>
                <%}%>
            </table>


        </div>
    </div>
    <br>
    <p>
        <a href="#addChannel" role="button" class="btn btn-primary" data-toggle="modal">Add New Channel&raquo;</a>
    </p>

    <script language="javascript">
        //listChannels();
    </script>
</div>


<!-- Modal -->
<div id="addChannel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Add New Channel</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="addchannelForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Channel Name</label>
                        <div class="controls">
                            <input type="text" id="_ch_name" name="_ch_name" placeholder="Display name for future reference" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Virtual Path</label>
                        <div class="controls">
                            <input type="text" id="_ch_virtual_path" name="_ch_virtual_path" placeholder="short no space phrase to create virtual folder" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Status</label>
                        <div class="controls">
                            <select class="span4" name="_ch_status" id="_ch_status">
                                <option value="1">Active</option>
                                <option value="0">Suspended</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <div class="control-group">
                        <label class="control-label"  for="username">Administrator Name</label>
                        <div class="controls">
                            <input type="text" id="_op_name" name="_op_name" placeholder="channel administrator name (login id)" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Email Id</label>
                        <div class="controls">
                            <input type="text" id="_op_email" name="_op_email" placeholder="administrator's email id for notifications" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Phone</label>
                        <div class="controls">
                            <input type="text" id="_op_phone" name="_op_phone" placeholder="administrator's password" class="span4">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>



    </div>
    <div class="modal-footer">
        <div id="addchannel-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="addchannel()" id="buttonAddChannel">Create New Channel</button>
    </div>
</div>

<div id="editChannel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3>Edit Channel <div id="idEditChannelName"></div></h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="editchannelForm" name="editchannelForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Channel Name</label>
                        <div class="controls">
                            <input type="text" id="_ch_nameE" name="_ch_nameE" readonly placeholder="new channel name" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Virtual Path</label>
                        <div class="controls">
                            <input type="text" id="_ch_virtual_pathE" name="_ch_virtual_pathE" readonly placeholder="new virtual path" class="input-xlarge">
                        </div>
                    </div>
                    <hr>
                    <div class="control-group">
                        <label class="control-label"  for="username">Remote Access Login</label>
                        <div class="controls">
                            <input type="text" id="_rem_loginidE" name="_rem_loginidE" placeholder="remote access login id" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Remote Access Password</label>
                        <div class="controls">
                            <input type="password" id="_rem_passwordE" name="_rem_passwordE" placeholder="password " class="span4">
                            : <input type="password" id="_rem_passwordEC" name="_rem_passwordEC" placeholder="confirm" class="span4">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Session Expiry </label>
                        <div class="controls">
                            <input type="text" id="_rem_expiryminE" name="_rem_expiryminE" placeholder="eg.30" class="span2"> (in mins)
                            <input type="hidden" id="_channelidE" name="_channelidE" >
                            <input type="hidden" id="_ch_statusE" name="_ch_statusE" >
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="editchannel-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="editChannel()" id="buttonEditChannel">Save Changes</button>
    </div>
</div>
<%}catch(Exception ex){
    ex.printStackTrace();
}%>
<%@include file="footer.jsp" %>