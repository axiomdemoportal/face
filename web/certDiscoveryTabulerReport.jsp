
<%@include file="header.jsp"%>
   <!--pagination-->
<!--   <script src="assets/js/jquery.js" type="text/javascript"></script>-->
<!--      <script src="./assets/js/jquery.dataTables.min.js"></script>
        <script src="assets/js/dataTables.responsive.min.js"></script>
        <link  rel="stylesheet" href="assets/css/jquery.dataTables.min.css">
        <link  rel="stylesheet" href="assets/css/responsive.dataTables.min.css">-->
<script src="assets/js/certdiscovery.js" type="text/javascript"></script>
<script type="text/javascript">
    var ca=certDonutChartCA();
         
         var arry = new Array();
         arry.push("All");
          for(i = 0; i < ca.length; i++) {
               
                   arry.push(ca[i].label)
    
    }
//    console.log("Arrrrray"+arry);

  //alert(JSON.stringify(ca));
    
    
    function certDonutChartCA() {
    var s = './certDiscoveryChartforCA';
   
    var jsonData = $.ajax({
        url: s,        
        dataType: "json",
        async: false
    }).responseText;

    var a= jsonParse(jsonData);
    //alert(JSON.stringify(a));

    return a;
   
}
     function configureDropDownLists(ddl1,ddl2) {
         var CertificateAuthorities = arry;
        
        
    var SSLEndpointNotices = ['All','Beast', 'Weak Cipher Suites', 'SSLv2.0 Protocol Enable','SSLv3.0 Protocol Enable','Heartbleed','Freak Attack'];
    var ExpiringCertificates = ['All','0 Days', '7 Days', '30 Days','45 Days','60 Days','90 Days'];
    var CertificateNotices = ['All','Weak Keys<2048', 'Certificate Name Mismatch', 'Weak Hashing Algorithm','ShA1 Violation'];

    switch (ddl1.value) {
        case 'SSL Endpoint Notices':
            ddl2.options.length = 0;
            for (i = 0; i < SSLEndpointNotices.length; i++) {
                createOption(ddl2, SSLEndpointNotices[i], SSLEndpointNotices[i]);
            }
            break;
        case 'Expiring Certificates':
            ddl2.options.length = 0; 
        for (i = 0; i < ExpiringCertificates.length; i++) {
            createOption(ddl2, ExpiringCertificates[i], ExpiringCertificates[i]);
            }
            break;
        case 'Certificate Notices':
            ddl2.options.length = 0;
            for (i = 0; i < CertificateNotices.length; i++) {
                createOption(ddl2, CertificateNotices[i], CertificateNotices[i]);
            }
            break;
            case 'Certificate Authorities':
            ddl2.options.length = 0;
            for (i = 0; i < CertificateAuthorities.length; i++) {
                createOption(ddl2, CertificateAuthorities[i], CertificateAuthorities[i]);
            }
             break;
            default:
                ddl2.options.length = 0;
               
            break;
    }

}

    function createOption(ddl, text, value) {
        var opt = document.createElement('option');
        opt.value = value;
        opt.text = text;
        ddl.options.add(opt);
    }
</script>
<h1 class="text-success"> &nbsp;Certificate Discovery Reports</h1>
<br>

        <p></p>
<!--        <h3> Type</h3> -->


    <div class="input-append">
      <div class="container-fluid" id="certDiscoveryForm1" name="certDiscoveryForm1">

<select span="1" name="_Type" id="ddl" onchange="configureDropDownLists(this,document.getElementById('ddl2'))">
<option value="categoryempty" disabled selected hidden>Select Category</option>
<option value="Certificate Authorities">Certificate Authorities(CA's)</option>
<option value="SSL Endpoint Notices">SSL Endpoint Notices</option>
<option value="Expiring Certificates">Expiring Certificates</option>
<option value="Certificate Notices">Certificate Notices</option>
</select>

<select span="2" id="ddl2">
    <option value="Criteriaempty" disabled selected hidden>Select Criteria</option>
</select>
      
    <button span="3" class="btn btn-success" onclick="SearchCert1();" id="SearchTblCert">Search Now</button>
  <br>
                <br>
      </div>
</div>

     <div id="cert_table_main1">
        </div>


    <%@include file="footer.jsp" %>