/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.settings.OCRSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 *
 * @author mollatech1
 */
public class editocrsettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editocrsettings.class.getName());

    final String itemtype = "SETTINGS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //      PrintRequestParameters(request);
        log.info("Servlet started");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String OperatorID = operatorS.getOperatorid();

        String _channelId = channel.getChannelid();
        SettingsManagement sMngmt = new SettingsManagement();
        int retValue = -1;

        String result = "success";
        String message = "OCR Settings Update Successful!!!";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        
            log.debug("channel :: " + channel.getName());
            log.debug("getChannelid :: " + channel.getChannelid());
            log.debug("operatorS :: " + operatorS.getOperatorid());
            log.debug("sessionId :: " + sessionId);
            log.debug("remoteaccesslogin :: " + remoteaccesslogin);

        // String _preference = request.getParameter("_perference");
        int _iPreference = 1;

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_HARDWARE) != 0
                && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_OOB) != 0
                && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
            result = "error";
            message = "This feature is not available in this license!!!";

            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }

            out.print(json);
            out.flush();
            return;
        }

        int _type = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.OCR;
        //  String strType = String.valueOf(_type);
        Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _type, _iPreference);

        OCRSettings tokenObj = null;
        boolean bAddSetting = false;

        if (settingsObj == null) {
            tokenObj = new OCRSettings();
            tokenObj.setChannelId(_channelId);
            //  emailObj.setChannelId(_channelId);
            //emailObj.setPreference(_iPreference);
            bAddSetting = true;
        } else {
            tokenObj = (OCRSettings) settingsObj;
        }
        if (_type == com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.OCR) {
            if (_iPreference == com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE) {
                //OOBEmailChannelSettings emailObj = new OOBEmailChannelSettings();
                
               String ocrIp = request.getParameter("_ocrIp");
               String _ocrPort = request.getParameter("_ocrPort");
               String _pdfPath = request.getParameter("_pdfPath");
               String _sftpUsername = request.getParameter("_sftpUsername");
               String _sftpPassword = request.getParameter("_sftpPassword");
               String _sftpIp = request.getParameter("_sftpIp");
               String _sftpPort = request.getParameter("_sftpPort");
               
               

                tokenObj.setOcrIp(ocrIp);
                tokenObj.setOcrPort(_ocrPort);
                tokenObj.setpdfPath(_pdfPath);
                tokenObj.setSftpUsername(_sftpUsername);
                tokenObj.setSftpPassword(_sftpPassword);
                tokenObj.setSftpIp(_sftpIp);
                tokenObj.setSftpPort(_sftpPort);
               

            } else {

            }
        }
        AuditManagement audit = new AuditManagement();
        if (bAddSetting == true) {
            retValue = sMngmt.addSetting(sessionId, _channelId, _type, _iPreference, tokenObj);
            log.debug("editotpsettings::addSetting" +retValue);
            String resultString = "ERROR";
            if (retValue == 0) {
                resultString = "SUCCESS";
                //audit.setIP("127.0.0.1");

                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add OCR Setting", resultString, retValue, "Setting Management",
                        "", "OCR IP " + tokenObj.getOcrIp() + " OCR Port" + tokenObj.getOcrPort()
                        + "Pdf upload path" + tokenObj.getpdfPath()+ "SFTP Username" + tokenObj.getSftpUsername()                        + "SW SOTP Token Length" + tokenObj.getpdfPath() + "SW Algorithm Type =" + tokenObj.getSftpIp()
                        + "SFTP password " + tokenObj.getSftpPassword()
                        + "SFTP IP" + tokenObj.getSftpIp()
                        + "SFTP Port" + tokenObj.getSftpPort(),
                        itemtype, _channelId);
            } else if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add OCR Setting", resultString, retValue, "Setting Management",
                        "", "Failed to add OCR Settings",
                        itemtype, _channelId);
           }

        } else {
            OCRSettings oldtokenObj = (OCRSettings) sMngmt.getSetting(sessionId, _channelId, _type, _iPreference);
            retValue = sMngmt.changeSetting(sessionId, _channelId, _type, _iPreference, settingsObj, tokenObj);
            log.debug("retValue :: " +retValue);
                
            String resultString = "ERROR";

            if (retValue == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add OCR Setting", resultString, retValue, "Setting Management",
                        "", "OCR IP " + tokenObj.getOcrIp() + " OCR Port" + tokenObj.getOcrPort()
                        + "Pdf upload path" + tokenObj.getpdfPath()+ "SFTP Username" + tokenObj.getSftpUsername()                        + "SW SOTP Token Length" + tokenObj.getpdfPath() + "SW Algorithm Type =" + tokenObj.getSftpIp()
                        + "SFTP password " + tokenObj.getSftpPassword()
                        + "SFTP IP" + tokenObj.getSftpIp()
                        + "SFTP Port" + tokenObj.getSftpPort(),
                        itemtype, _channelId);

            } else if (retValue != 0) {

                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Change OTP Token Setting", resultString, retValue, "Setting Management",
                        "OCR Ip" + oldtokenObj.getOcrIp() + "OCR Port" + oldtokenObj.getOcrPort()
                        + "PDF Path" + oldtokenObj.getpdfPath() + "SFTP Username" + oldtokenObj.getSftpUsername()
                        + "SFTP Password" + oldtokenObj.getSftpPassword() + "SFTP IP =" + oldtokenObj.getSftpIp()
                        + "SFTP Port =" + oldtokenObj.getSftpPort(),
                        "Failed TO Edit Setting", itemtype, _channelId);
            }

        }

        if (retValue != 0) {
            result = "error";
            message = "OTP Settings Update Failed!!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
