<%@include file="header.jsp" %>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%    //String strChID = 
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();

%>

<script src="./assets/js/audit.js"></script>

<div class="container-fluid">
    <h2 class="text-success">Complete Audit Trail</h2>    
    <p>
        <br>
        <!--<h3>Search Contacts through name,phone or email</h3>-->   
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="testUSSDSecondaryForm" name="testUSSDSecondaryForm">
                <fieldset>
                    <div class="control-group" >
                        <div class="controls">
                            <span class="add-on">Start Date </span>   
                            <div id="channelaudit" class="input-append date" algin = "center">
                                <input id="_channelDate" name="_channelDate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth">
                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="control-group" >
                        <div class="controls">
                            <span class="add-on">End Date </span>   
                            <div id="channelsaudit" class="input-append date" algin = "center">
                                <input id="_channelSDate" name="_channeSlDate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth">
                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <!-- Name -->

                    <div class="control-group">
                        <!--<label class="control-label"  for="username">Enter ChannelId :</label>-->
                        <div class="controls">
                            <input type="hidden" id="_channeliD" name="_channeliD"  class="span4" value="<%=_channelId%>">
                            <!--<input type="text" id="_testmsgS" name="_testmsgS" placeholder="this is the sample message to be sent out..." class="input-xlarge">-->
                            <div>
                                <a href="#" class="btn btn-success btn-large" onclick="ChannelAudits()">Download PDF</a>

                                <a href="#" class="btn btn-success btn-large" onclick="ChannelAuditsCSV()">Download CSV</a>
                            </div>

                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>

    <br>

    <script>

        $(function () {
            $("#channelaudit").datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            }).val();
            $("#channelsaudit").datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            }).val();
        });
    </script>

    <%@include file="footer.jsp" %>