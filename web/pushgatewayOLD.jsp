<%@include file="header.jsp" %>
<script src="./assets/js/ajaxfileupload.js"></script>
<script src="./assets/js/bootstrap-fileupload.js"></script> 
<link href="./assets/css/bootstrap.css" rel="stylesheet">
<link href="./assets/css/bootstrap-fileupload.css" rel="stylesheet">
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<script src="./assets/js/pushgateway.js"></script>
<div class="container-fluid">
    <h1 class="text-success">Push MSG Gateway Configuration</h1>
    <p>To facilitate push notification, Push MSG Gateway(s) connection needs to be configured. For i-Phone, please set the Secondary Gateway too. </p>
    <div class="tabbable">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#primary" data-toggle="tab">Android Push MSG Gateway</a></li>
            <li><a href="#secondary" data-toggle="tab">Apple Push MSG Gateway</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="primary">
                <div class="row-fluid">
                    <form class="form-horizontal" id="PushMsgprimaryform" name="PushMsgprimaryform">

                        <div id="legend">
                            <legend class="">Android Push MSG Gateway Configuration</legend>
                        </div>

                        <div class="control-group">
                            <label class="control-label"  for="username">Status </label>
                            <div class="controls">
                                <div>
                                    <div class="btn-group">

                                        <button class="btn btn-small"><div id="_status-primary-PushMsg"></div></button>
                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="ChangeActiveStatusPushMsg(1, 1)">Mark as Active?</a></li>
                                            <li><a href="#" onclick="ChangeActiveStatusPushMsg(1, 0)">Mark as Suspended?</a></li>
                                        </ul>
                                    </div>
<!--                                    with Auto Fail-Over to Secondary Gateway 
                                    <div class="btn-group">

                                        <button class="btn btn-small"><div id="_autofailover-primary-PushMsg"></div></button>
                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="ChangeFailOverPushMsg(1)">Mark as Enabled?</a></li>
                                            <li><a href="#" onclick="ChangeFailOverPushMsg(0)">Mark as Disabled?</a></li>
                                        </ul>
                                    </div>-->

                                </div>
                            </div>
                        </div>

                        <input type="hidden" id="_status" name="_status">
                        <input type="hidden" id="_perference" name="_perference" value="1">
                        <input type="hidden" id="_type" name="_type" value="18">
                        <input type="hidden" id="_autofailover" name="_autofailover">
                        <input type="hidden" id="_retries" name="_retries">
                        <input type="hidden" id="_retryduration" name="_retryduration">

                        <input type="hidden" id="_retriesFromGoogle" name="_retriesFromGoogle">
                        <input type="hidden" id="_timetolive" name="_timetolive">
                        <input type="hidden" id="_delayWhileIdle" name="_delayWhileIdle">



                        <div class="control-group">
                            <label class="control-label"  for="username">Retry Count</label>
                            <div class="controls">
                                <div>
                                    <div class="btn-group">
                                        <button class="btn btn-small"><div id="_retries-primary-PushMsg"></div></button>
                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="ChangeRetryPushMsg(1, 2)">2 Retries</a></li>
                                            <li><a href="#" onclick="ChangeRetryPushMsg(1, 3)">3 Retries</a></li>
                                            <li><a href="#" onclick="ChangeRetryPushMsg(1, 5)">5 Retries</a></li>
                                        </ul>
                                    </div>
                                    with wait time between retries as      
                                    <div class="btn-group">
                                        <button class="btn btn-small"><div id="_retryduration-primary-PushMsg"></div></button>
                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="ChangeRetryDurationPushMsg(1, 10)">10 seconds</a></li>
                                            <li><a href="#" onclick="ChangeRetryDurationPushMsg(1, 30)">30 seconds</a></li>
                                            <li><a href="#" onclick="ChangeRetryDurationPushMsg(1, 60)">60 seconds</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr>
                        <div class="control-group">
                            <label class="control-label"  for="username"> Delay While Idle</label>
                            <div class="controls">
                                <div>
                                    <div class="btn-group">
                                        <button class="btn btn-small"><div id="_delayWhileIdle-primary-PushMsg"></div></button>
                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="ChangeDelayWhileIdel(1, 1)">Active</a></li>
                                            <li><a href="#" onclick="ChangeDelayWhileIdel(1, 0)">Suspended</a></li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label"  for="username">Time To Live</label>
                            <div class="controls">
                                <div>
                                    <div class="btn-group">
                                        <button class="btn btn-small"><div id="_timetolive-primary-PushMsg"></div></button>
                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="ChangeTimeToLive(1, 10)">10 Seconds</a></li>
                                            <li><a href="#" onclick="ChangeTimeToLive(1, 20)">20 Seconds</a></li>
                                            <li><a href="#" onclick="ChangeTimeToLive(1, 30)">30 Seconds</a></li>
                                        </ul>
                                    </div>
                                    Retry From Google      
                                    <div class="btn-group">
                                        <button class="btn btn-small"><div id="_retriesFromGoogle-primary-PushMsg"></div></button>
                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="ChangeRetryFromGoogle(1, 2)">2 Retries</a></li>
                                            <li><a href="#" onclick="ChangeRetryFromGoogle(1, 3)">3 Retries</a></li>
                                            <li><a href="#" onclick="ChangeRetryFromGoogle(1, 5)">5 Retries</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="control-group">
                            <label class="control-label"  for="username">Host:Port</label>
                            <div class="controls">
                                <input type="text" id="_ip" name="_ip" placeholder="example localhost/127.0.0.1" class="span3">
                                : <input type="text" id="_port" name="_port" placeholder="443" class="span1">
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label"  for="username">GCM URL </label>
                            <div class="controls">
                                <input type="text" id="_gcmurl" name="_gcmurl" placeholder="Enter GCM Url" class="input-xlarge">
                                Api Key: <input type="text" id="_apikey" name="_apikey" placeholder="Enter API Key" class="input-xlarge">
                            </div>
                        </div>


                        <div class="control-group">
                            <label class="control-label"  for="username">Google Sender Key</label>
                            <div class="controls">
                                <input type="text" id="_googleSenderKey" name="_googleSenderKey" placeholder="Enter Google Sender Key" class="span3">                                
                            </div>
                        </div>
                        <hr>
                        <div class="control-group">
                            <label class="control-label"  for="username">Additional Attributes</label>
                            <div class="controls">
                                <input type="text" id="_reserve1" name="_reserve1" placeholder="name1=value1" class="span3">
                                : <input type="text" id="_reserve2" name="_reserve2" placeholder="any setting" class="span3">
                                : <input type="text" id="_reserve3" name="_reserve3" placeholder="parameters" class="span3">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Implementation Class</label>
                            <div class="controls">
                                <input type="text" id="_className" name="_className" placeholder="complete class name including package" class="span6">
                            </div>
                        </div>

                        <!-- Submit -->
                        <div class="control-group">
                            <div class="controls">
                                <div id="save-PushMsg-gateway-primary-result"></div>
                                <% // if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
                                <button class="btn btn-primary" onclick="EditPushMsgSetting(1)" type="button">Save Setting Now >> </button>
                                <%//}%>
                                <button class="btn" onclick="LoadTestPushMsgConnectionUI(1)" type="button">Test Connection >> </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <div class="tab-pane" id="secondary">
                <div class="row-fluid">
                    <form class="form-horizontal" id="PushMsgecondaryform" name="PushMsgecondaryform" >
                        <fieldset>
                            <div id="legend">
                                <legend class="">Apple Push MSG Gateway Configuration</legend>
                            </div>

                            <div class="control-group">
                                <label class="control-label"  for="username">Status </label>
                                <div class="controls">
                                    <div>
                                        <div class="btn-group">

                                            <button class="btn btn-small"><div id="_statusS-secondary-PushMsg"></div></button>
                                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#" onclick="ChangeActiveStatusPushMsg(2, 1)">Mark as Active?</a></li>
                                                <li><a href="#" onclick="ChangeActiveStatusPushMsg(2, 0)">Mark as Suspended?</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" id="_statusS" name="_statusS">
                            <input type="hidden" id="_perference" name="_perference" value="2">
                            <input type="hidden" id="_typeS" name="_typeS" value="19">
                            <input type="hidden" id="_autofailoverS" name="_autofailoverS">
                            <input type="hidden" id="_retriesS" name="_retriesS">
                            <input type="hidden" id="_retrydurationS" name="_retrydurationS">

                            <input type="hidden" id="_retriesFromGoogleS" name="_retriesFromGoogleS">
                            <input type="hidden" id="_timetoliveS" name="_timetoliveS">
                            <input type="hidden" id="_delayWhileIdleS" name="_delayWhileIdleS">



                            <div class="control-group">
                                <label class="control-label"  for="username">Retry Count</label>
                                <div class="controls">
                                    <div>
                                        <div class="btn-group">
                                            <button class="btn btn-small"><div id="_retriesS-secondary-PushMsg"></div></button>
                                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#" onclick="ChangeRetryPushMsg(2, 2)">2 Retries</a></li>
                                                <li><a href="#" onclick="ChangeRetryPushMsg(2, 3)">3 Retries</a></li>
                                                <li><a href="#" onclick="ChangeRetryPushMsg(2, 5)">5 Retries</a></li>
                                            </ul>
                                        </div>
                                        with wait time between retries as      
                                        <div class="btn-group">
                                            <button class="btn btn-small"><div id="_retrydurationS-secondary-PushMsg"></div></button>
                                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#" onclick="ChangeRetryDurationPushMsg(2, 10)">10 seconds</a></li>
                                                <li><a href="#" onclick="ChangeRetryDurationPushMsg(2, 30)">30 seconds</a></li>
                                                <li><a href="#" onclick="ChangeRetryDurationPushMsg(2, 60)">60 seconds</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <div class="control-group">
                                <label class="control-label"  for="username">Application Type</label>
                                <div class="controls">
                                    <div>
                                        <div class="btn-group">
                                            <button class="btn btn-small"><div id="_delayWhileIdleS-secondary-PushMsg"></div></button>
                                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#" onclick="ChangeDelayWhileIdel(2, 1)">Production</a></li>
                                                <li><a href="#" onclick="ChangeDelayWhileIdel(2, 0)">Sandbox</a></li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                              <div class="control-group">
                            <label class="control-label"  for="username">Host:Port</label>
                            <div class="controls">
                                <input type="text" id="_ipS" name="_ipS" placeholder="example localhost/127.0.0.1" class="span3">
                                : <input type="text" id="_portS" name="_portS" placeholder="443" class="span1">
                            </div>
                        </div>


                            <div class="control-group">
                                <label class="control-label"  for="username">Bundle Id</label>
                                <div class="controls">
                                    <input type="text" id="_bundleID" name="_bundleID" placeholder="Enter Bundle Id" class="span3">                                
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="controls-row" id="licenses">
                                    <div class="row-fluid">
                                        <!--<form class="form-horizontal" id="uploadXMLFormEAD" name="uploadXMLFormEAD">-->
                                            <fieldset>
                                                <div class="control-group">
                                                    <label class="control-label"  for="username">Select File:</label>                                    
                                                    <div class="controls fileupload fileupload-new" data-provides="fileupload">
                                                        <div class="input-append">
                                                            <div class="uneditable-input span4"><i class="icon-file fileupload-exists"></i> 
                                                                <span class="fileupload-preview"></span>
                                                            </div>
                                                            <span class="btn btn-file" >
                                                                <span class="fileupload-new">Select file</span>
                                                                <span class="fileupload-exists">Change</span>
                                                                <input type="file" id="fileXMLToUploadEAD" name="fileXMLToUploadEAD"/>
                                                            </span>
                                                            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                        </div>
                                                    </div>
                                  
                                                </div>
                                           
                                                <input type="hidden" id="_perference" name="_perference" value="2">

                                                <div class="control-group">
                                                    <label class="control-label"  for="username">Enter Password</label>                                    
                                                    <div class="controls">
                                                        <input class="span3" type="password" id="_certpassowrd" name="_certpassowrd" placeholder="Enter Password To Import " >
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <div class="controls">
                                                        <button class="btn btn-success" id="buttonUploadEAD"  onclick="CertificateFileUpload()" >Upload Now>></button>
                                                                <div id="uploadStatus"></div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        <!--</form>-->
                                    </div>
                                    <!-- Submit -->
                                </div>

                            </div>

                            <hr>
                            <div class="control-group">
                                <label class="control-label"  for="username">Additional Attributes</label>
                                <div class="controls">
                                    <input type="text" id="_reserve1S" name="_reserve1S" placeholder="name1=value1" class="span3">
                                    : <input type="text" id="_reserve2S" name="_reserve2S" placeholder="any setting" class="span3">
                                    : <input type="text" id="_reserve3S" name="_reserve3S" placeholder="parameters" class="span3">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"  for="username">Implementation Class</label>
                                <div class="controls">
                                    <input type="text" id="_classNameS" name="_classNameS" placeholder="complete class name including package" class="span6">
                                </div>
                            </div>

                            <!-- Submit -->
                            <div class="control-group">
                                <div class="controls">
                                    <div id="save-PushMsg-gateway-secondary-result"></div>
                                    <%// if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
                                    <button class="btn btn-primary" onclick="EditPushMsgSetting(2)" type="button">Save Setting Now >> </button>
                                    <%//}%>
                                    <button class="btn" onclick="LoadTestPushMsgConnectionUI(2)" type="button">Test Connection >> </button>
                                </div>
                            </div>
                            <fieldset>
                                </form>
                                </div>
                                </div>
                                </div>
                                </div>


                                <script language="javascript" type="text/javascript">
                                    LoadPushMsgSetting(1);
                                    LoadPushMsgSetting(2);
                                </script>
                                </div>




                                <div id="testPushMsgPrimary" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                        <h3 id="myModalLabel">Test Primary Gateway</h3>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row-fluid">
                                            <form class="form-horizontal" id="testPushMsgPrimaryForm" name="testPushMsgPrimaryForm">
                                                <fieldset>
                                                    <!-- Name -->
                                                    <div class="control-group">
                                                        <label class="control-label"  for="username">Test Message</label>
                                                        <div class="controls">
                                                            <input type="text" id="_testmsg" name="_testmsg" placeholder="sample message to be sent out..." class="input-xlarge">
                                                            <input type="hidden" id="_type" name="_type" value="18">
                                                            <input type="hidden" id="_preference" name="_preference" value="1">
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label"  for="username">Email Id</label>
                                                        <div class="controls">
                                                            <input type="text" id="_testphone" name="_testphone" placeholder="Enter emailID" class="input-xlarge">
                                                        </div>
                                                    </div>                    
                                                </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="span3" id="test-sms-primary-configuration-result"></div>
                                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                        <button class="btn btn-primary" onclick="testconnectionPushMsgprimary()" id="testSMSPrimaryBut">Send Message Now</button>
                                    </div>
                                </div>

                                <div id="testPushMsgSecondary" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                        <h3 id="myModalLabel">Test Secondary Gateway</h3>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row-fluid">
                                            <form class="form-horizontal" id="testPushMsgSecondaryForm" name="testPushMsgSecondaryForm">
                                                <fieldset>
                                                    <!-- Name -->
                                                    <div class="control-group">
                                                        <label class="control-label"  for="username">Test Message</label>
                                                        <div class="controls">
                                                            <input type="text" id="_testmsgS" name="_testmsgS" placeholder="this is the sample message to be sent out..." class="input-xlarge">
                                                            <input type="hidden" id="_type" name="_type" value="19">
                                                            <input type="hidden" id="_preference" name="_preference" value="2">
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <label class="control-label"  for="username">Phone</label>
                                                        <div class="controls">
                                                            <input type="text" id="_testphoneS" name="_testphoneS" placeholder="Enter emailId" class="input-xlarge">
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="span3" id="test-sms-secondary-configuration-result"></div>
                                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                        <button class="btn btn-primary" onclick="testconnectionPushMsgsecondary()" id="testPushMsgSecondaryBut">Send Message Now</button>
                                    </div>
                                </div>

                                <%@include file="footer.jsp" %>