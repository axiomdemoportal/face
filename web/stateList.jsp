<%-- 
    Document   : stateList
    Created on : 12 Jan, 2015, 12:09:36 PM
    Author     : Manoj Sherkhane
--%>

<%@page import="com.mollatech.axiom.nucleus.db.connector.management.LocationManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Statelist"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

        <div class="control-group">
            <label class="control-label"  for="username">States</label>
            <div class="controls">
                <select class="span10" name="_StateName" id="_StateName">
                    <%
                        String countryId =request.getParameter("_countryId");
                        
                        int iCountryId = Integer.parseInt(countryId);
                        //getAll States here 
                        LocationManagement lManagement = new LocationManagement();
                        Statelist[] stateList = lManagement.getAllStateListByCountry(iCountryId);
                        for (int j = 0; j < stateList.length; j++) {
                            String statecode = stateList[j].getStateName();
                    %>
                    <option value="<%=statecode%>"><%=statecode%></option>
                    <%}%>
                </select>
            </div>
        </div>
  