<%@include file="header.jsp" %>
<script src="./assets/js/pdfsigning.js"></script>
<script src="./assets/js/json_sans_eval.js"></script>
<script src="./assets/js/bootbox.min.js"></script>
<script src="./assets/js/ajaxfileupload.js"></script>
<script src="./assets/js/bootstrap-fileupload.js"></script>   
<link href="./assets/css/bootstrap.css" rel="stylesheet">
<link href="./assets/css/bootstrap-fileupload.css" rel="stylesheet">
<style type="text/css">
    body {
        padding-top: 60px;
        padding-bottom: 40px;
    }
</style>
<div class="container-fluid">
    <h2>Sign PDF</h2>
    <br>
    <div class="row-fluid">
        <form class="form-horizontal" id="PDFSigning" name="PDFSigning">
            <fieldset>
                <div class="control-group">
                    <label class="control-label"  for="hwserialno">Enter Email</label>
                    <div class="controls">
                        <input type="text" id="_emailId" name="_emailId" placeholder="Email ID" class="input-xlarge"/>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"  for="otp">OTP</label>
                    <div class="controls">
                        <input type="text" id="_otp" name="_otp" placeholder="Enter OTP" class="input-xlarge"/>
                    </div>
                </div>
                <input type="hidden" id="_filename" name="_filename">

                <div class="controls-row" id="licenses">
                    <div class="row-fluid">
                        <form class="form-horizontal" id="uploadXMLFormEAD" name="uploadXMLFormEAD">
                            <fieldset>

                                <div class="control-group">
                                    <label class="control-label"  for="username">Select File:</label>                                    
                                    <div class="controls fileupload fileupload-new" data-provides="fileupload">
                                        <div class="input-append">
                                            <div class="uneditable-input span4"><i class="icon-file fileupload-exists"></i> 
                                                <span class="fileupload-preview"></span>
                                            </div>
                                            <span class="btn btn-file" >
                                                <span class="fileupload-new">Select file</span>
                                                <span class="fileupload-exists">Change</span>
                                                <input type="file" id="fileXMLToUploadEAD" name="fileXMLToUploadEAD"/>
                                            </span>
                                            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        </div>
                                        <button class="btn btn-success" id="buttonUploadEAD"  onclick="PDFUpload()" >Upload Now>></button>
                                    </div>

                                </div>
                                <!--<button class="btn btn-success" id="buttonUploadEAD"  onclick="PDFUpload()" >Upload Now>></button>-->
                            </fieldset>
                        </form>

                    </div>
                    <!-- Submit -->
                </div>

                <div class="control-group">
                    <div class="controls">
                        <button class="btn btn-primary btn-large" onclick="signPDF()" type="button">Sign PDF Now >></button>
                        <div id="sign-pdf-result"></div>
                        <div class="span3" id="download-signed-pdf"></div>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>

    <%@include file="footer.jsp" %>
