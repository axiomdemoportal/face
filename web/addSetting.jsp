<%@page import="com.mollatech.axiom.nucleus.settings.WebSealSetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.MonitorSettingsManagement"%>
<%@page import="com.mollatech.axiom.common.utils.UtilityFunctions"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.AxiomProtect"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SchedulerManagement"%>
<%@page import="com.mollatech.axiom.nucleus.settings.NucleusMonitorSettings"%>
<%@page import="java.io.ObjectInputStream"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.db.Monitorsettings"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.axiom.nucleus.settings.ContactTagsSetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Templates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%@include file="header.jsp" %>
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<script src="./assets/js/addSettings.js"></script>


<%
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");

    Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
    String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");

    String operatorId = operator.getOperatorid();
    SettingsManagement sMngmt = new SettingsManagement();
    Map keymap = null;
    String key = "";
    Monitorsettings monitorsettings = null;
    Map settingsMap = (Map) session.getAttribute("setting");
    String Name = request.getParameter("Name");
    String uniqueId = "";
    String _imagescript = "";
    if (Name != null) {
        monitorsettings = (Monitorsettings) settingsMap.get(Name);
        uniqueId = monitorsettings.getWebUniqueId();
    }
    Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.MONITORGROUPS, SettingsManagement.PREFERENCE_ONE);
    if (settingsObj != null) {
        keymap = (Map) settingsObj;
        session.setAttribute("keymap", keymap);
        Iterator i = keymap.entrySet().iterator();
        while (i.hasNext()) {
            Map.Entry e = (Map.Entry) i.next();
            key = key + e.getKey().toString() + ",";
        }
    }
    String[] keys = {};
    if (!key.equals("")) {
        key = key.substring(0, key.length() - 1);
        keys = key.split(",");
    }

    WebSealSetting setting = (WebSealSetting) sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.WEBSEAL_SETTINGS, SettingsManagement.PREFERENCE_ONE);

%>
<div class="container-fluid">
    <h1 class="text-success">Add Web Watch Settings</h1>
    <p>add Web Watch settings for sending alerts</p>
    <hr>
    <form class="form-horizontal" id="AddNewSettingsForm" name="AddNewSettingsForm">
        <fieldset>
            <div class="control-group">

                <label class="control-label"  for="username">Unique Name</label>
                <div class="controls">
                    <% if (Name != null) {%>
                    <input type="text" id="_name" name="_name" value="<%=Name%>" readonly="">
                    <%} else {%>
                    <input type="text" id="_name" name="_name">
                    <%}%>
                    Set Status
                    <select  name="_status" id="_status" >
                        <option value="0" id="0">Suspended</option>
                        <option value="1" id="1" >Active</option>

                        <%
                            if (monitorsettings != null) {
                        %>
                        <script>
                            var h = document.getElementById('_status');
                            var val = h.options['<%= monitorsettings.getMonitorStatus()%>'].selected = 'true';

                        </script> 
                        <% }
                        %>
                    </select>
                </div>
            </div>

            <hr>
            <h3 class="text-success">Monitor Details</h3>
            <div class="control-group">
                <label class="control-label"  for="username">Monitor Type</label>
                <div class="controls">
                    <select name="_selectedType" id="_selectedType" class="span4"  >
                        <option value="0" id="0"> .. </option>                                             
                        <option value='1' id="1"> Web Site </option>
                        <option value='4' id="4"> DNS Monitor </option>
                        <option value='6' id="6"> Port Monitor </option>
                        <option value='9' id="9"> Ping Monitor </option>
                        <option value='10' id="10"> SSL Certificate Monitor </option>
                    </select>
                </div>
            </div>
            <!-- Select -->
            <div id="website">
                <div class="control-group" >
                    <label class="control-label"  for="username">Web Site Address</label>
                    <div class="controls">
                        <input type="text" id="_webAddress" name="_webAddress" placeholder="Web Site Address" class="input-xxlarge" <%if (Name == null) {%>onblur="getUniqueId(<%if (setting == null) { %>''<%} else {%>'<%=setting.webSealURL%>'<%}%>)" <%}%>>
                        <input type="hidden" id="_UniqueIdVal" name="_UniqueIdVal" value="<%=uniqueId%>">
                        <input type="hidden" id="_webSealURL" name="_webSealURL" <%if (setting == null) { %>value=""<%} else {%> value="<%=setting.webSealURL%>"<%}%>>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"  for="username">Script for Image</label>
                    <div class="controls">
                        <textarea id="_imagescript" name="_imagescript" class="span8" cols="90" rows="7" readOnly></textarea>
                    </div>
                </div>
            </div>
            <div id="DNS">
                <div class="control-group" >
                    <label class="control-label"  for="username">Host:Port:Name</label>
                    <div class="controls">
                        <input type="text" id="_dnshost" name="_dnshost" placeholder="DNS Host" class="input-medium">
                        : <input type="text" id="_dnsport" name="_dnsport" placeholder="DNS Port" class="input-medium">
                        : <input type="text" id="_dnsname" name="_dnsname" placeholder="Domain Name" class="input-medium">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"  for="username">Lookup Type</label>
                    <div class="controls">
                        <select name="_selectedLookup" id="_selectedLookup">
                            <option value="a" id="a">A</option>
                            <option value="all" id="all">ALL</option>
                            <option value="ns" id="ns">NS</option>
                            <option value="mx" id="mx">MX</option>
                        </select>
                    </div>
                </div>
                <div class="control-group" >
                    <label class="control-label"  for="username">Link To website</label>
                    <div class="controls">
                        <select  name="linkfordns" id="linkfordns" >
                            <option value="yes" id="yes">yes</option>
                            <option value="no" id="no" >no</option>

                        </select>
                    </div>
                </div>

                <div id="dnslink">
                    <div class="control-group">
                        <label class="control-label"  for="username">Link To</label>
                        <div class="controls">
                            <select name="linktodns" id="linktodns">
                                <%
                                    NucleusMonitorSettings nms1 = null;
                                    MonitorSettingsManagement m = new MonitorSettingsManagement();
                                    Monitorsettings[] Monitorsettings = m.listwebsites(1);
                                    if (Monitorsettings != null) {
                                        for (int i = 0; i < Monitorsettings.length; i++) {
                                            byte[] obj = Monitorsettings[i].getMonitorSettingEntry();
                                            byte[] f = AxiomProtect.AccessDataBytes(obj);
                                            ByteArrayInputStream bais = new ByteArrayInputStream(f);
                                            Object object = SchedulerManagement.deserializeFromObject(bais);

                                            if (object instanceof NucleusMonitorSettings) {
                                                nms1 = (NucleusMonitorSettings) object;
                                            }

                                            //nms1 = (NucleusMonitorSettings) object;
                                            if (nms1 != null) {
                                                _imagescript = nms1.getWebseal();
                                %>
                                <option value="<%=nms1.getWebadd()%>"><%=nms1.getWebadd()%></option>
                                <script>

                                </script>
                                <%
                                            }
                                        }
                                    }
                                %>
                            </select>

                        </div>
                    </div>
                </div>      
            </div>

            <div id="portmonitor">
                <div class="control-group" >
                    <label class="control-label"  for="username">Port Host:Port</label>
                    <div class="controls">
                        <input type="text" id="_Porthost" name="_Porthost" placeholder="Port Host" class="input-medium">
                        :<input type="text" id="_Portport" name="_Portport" placeholder="Port" class="input-medium">
                    </div>
                </div>
                <div class="control-group" >
                    <label class="control-label"  for="username">Command</label>
                    <div class="controls">
                        <input type="text" id="_Portcommand" name="_Portcommand" placeholder="Command" class="input-xlarge">
                    </div>
                </div>
            </div>


            <div id="ping">
                <div class="control-group" >
                    <label class="control-label"  for="username">Ping Host</label>
                    <div class="controls">
                        <input type="text" id="_Pinghost" name="_Pinghost" placeholder="Ping Host" class="input-xlarge">
                    </div>
                </div>
            </div>

            <div id="ssl">
                <div class="control-group" >
                    <label class="control-label"  for="username">SSL Host Name </label>
                    <div class="controls">
                        <select name="_selectedprotocol" id="_selectedprotocol" class="input-small">
                            <option value="https" id="https">HTTPS</option>
                            <option value="smtps" id="smtps">SMTPS</option>
                            <option value="pops" id="pops">POPS</option>
                            <option value="imaps" id="imaps">IMAPS</option>
                            <option value="ftps" id="ftps">FTPS</option>
                            <option value="custom" id="custom">CUSTOM</option>
                        </select>
                        <input type="text" id="_sslHost" name="_sslHost" placeholder="SSL Host" class="input-xlarge">
                        :<input type="text" id="_sslPort" name="_sslPort" placeholder="SSL Port" class="input-mini">
                    </div>
                </div>
                <div class="control-group" >
                    <label class="control-label"  for="username">Notify me before certificate expires </label>
                    <div class="controls">
                        <input type="text" id="_notify" name="_notify" placeholder="days" class="input-mini"> &nbsp;&nbsp; days.
                    </div>
                </div>
                <div class="control-group" >
                    <label class="control-label"  for="username">Link To website</label>
                    <div class="controls">
                        <select  name="linkforssl" id="linkforssl" >
                            <option value="yes" id="yes">yes</option>
                            <option value="no" id="no" >no</option>


                        </select>
                    </div>
                </div>

                <div id="ssllink">
                    <div class="control-group">
                        <label class="control-label"  for="username">Link To</label>
                        <div class="controls">
                            <select name="linktossl" id="linktossl">
                                <%
                                    if (Monitorsettings != null) {
                                        for (int i = 0; i < Monitorsettings.length; i++) {
                                            byte[] obj = Monitorsettings[i].getMonitorSettingEntry();
                                            byte[] f = AxiomProtect.AccessDataBytes(obj);
                                            ByteArrayInputStream bais = new ByteArrayInputStream(f);
                                            Object object = SchedulerManagement.deserializeFromObject(bais);

                                            if (object instanceof NucleusMonitorSettings) {
                                                nms1 = (NucleusMonitorSettings) object;
                                            }

                                            //nms1 = (NucleusMonitorSettings) object;
                                            if (nms1 != null) {


                                %>

                                <option value="<%=nms1.getWebadd()%>"><%=nms1.getWebadd()%></option>

                                <%}
                                        }
                                    }
                                %>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <%
                NucleusMonitorSettings nms = null;
                if (monitorsettings != null) {

                    byte[] obj = monitorsettings.getMonitorSettingEntry();
                    byte[] f = AxiomProtect.AccessDataBytes(obj);
                    ByteArrayInputStream bais = new ByteArrayInputStream(f);
                    Object object = SchedulerManagement.deserializeFromObject(bais);

                    if (object instanceof NucleusMonitorSettings) {
                        nms = (NucleusMonitorSettings) object;
                    }
                    if (nms1 != null) {
                        //nms = (NucleusMonitorSettings) object;
%>
            <script>
//                $('#_selectedType').attr("disabled", true);
                $("#_selectedType").find('option[value=' +<%= monitorsettings.getMonitorType()%> + ']').prop("selected", true);
            </script> 
            <%
                if (monitorsettings.getMonitorType() == 1) {%>
            <script>
                document.getElementById("DNS").style.display = 'none';
                document.getElementById("ssl").style.display = 'none';
                document.getElementById("ping").style.display = 'none';
                document.getElementById("portmonitor").style.display = 'none';
                var ele = document.getElementById("website");
                ele.style.display = "block";
                $('#_webAddress').val('<%=nms.getWebadd()%>');
                var _imageScript = "";
                <%for (int i = 0; i < nms.getWebseal().split("\n").length - 1; i++) {
  
                %>
                _imageScript += '<%=nms.getWebseal().split("\n")[i].replace("\n", "").replace("\r", "")%>\n';
                <%}%>
                $('#_imagescript').val(_imageScript + '</scr' + 'ipt>');

            </script>
            <%} else if (monitorsettings.getMonitorType() == 4) {%>
            <script>
                document.getElementById("website").style.display = 'none';
                document.getElementById("ssl").style.display = 'none';
                document.getElementById("ping").style.display = 'none';
                document.getElementById("portmonitor").style.display = 'none';
                var ele = document.getElementById("DNS");
                ele.style.display = "block";
                $('#_dnshost').val('<%= nms.getDnshost()%>');
                $('#_dnsport').val('<%= nms.getDnsport()%>');
                $('#_dnsname').val('<%= nms.getDnsname()%>');
                $('#linktodns').val('<%= nms.getLinktodns()%>');
                $('#linkfordns').val('<%= nms.getLinkselectfordns()%>');

                var ds = document.getElementById('_selectedLookup');
                ds.options['<%= nms.getSelectedLookup()%>'].selected = 'true';
                <%
                    if (nms.getLinkselectfordns().equals("no")) {
                %>
                document.getElementById("dnslink").style.display = 'none';
                <%
                    }
                %>

            </script>
            <%} else if (monitorsettings.getMonitorType() == 6) {%>
            <script>
                document.getElementById("website").style.display = 'none';
                document.getElementById("DNS").style.display = 'none';
                document.getElementById("ping").style.display = 'none';
                document.getElementById("ssl").style.display = 'none';
                var ele = document.getElementById("portmonitor");
                ele.style.display = "block";
                $('#_Porthost').val('<%= nms.getPorthost()%>');
                $('#_Portport').val('<%= nms.getPortport()%>');
                $('#_Portcommand').val('<%= nms.getPortcommand()%>');
            </script>
            <%} else if (monitorsettings.getMonitorType() == 9) {%>
            <script>
                document.getElementById("website").style.display = 'none';
                document.getElementById("DNS").style.display = 'none';
                document.getElementById("ssl").style.display = 'none';
                document.getElementById("portmonitor").style.display = 'none';
                var ele = document.getElementById("ping");
                ele.style.display = "block";
                $('#_Pinghost').val('<%= nms.getPinghost()%>');
            </script>
            <%} else if (monitorsettings.getMonitorType() == 10) {%>
            <script>
                document.getElementById("website").style.display = 'none';
                document.getElementById("DNS").style.display = 'none';
                document.getElementById("ping").style.display = 'none';
                document.getElementById("portmonitor").style.display = 'none';
                var ele = document.getElementById("ssl");
                ele.style.display = "block";
                $('#_sslHost').val('<%= nms.getSslHost()%>');
                $('#_sslPort').val('<%= nms.getSslPort()%>');
                $('#_notify').val('<%= nms.getNotify()%>');
                $('#linktossl').val('<%= nms.getLinktodns()%>');
                $('#linkforssl').val('<%= nms.getLinkselectfordns()%>');
                var ds = document.getElementById('_selectedprotocol');
                ds.options['<%= nms.getSelectedprotocol()%>'].selected = 'true';
                <%
                    if (nms.getLinkselectforssl().equals("no")) {
                %>
                document.getElementById("ssllink").style.display = 'none';
                <%
                    }
                %>
            </script>
            <%}
                }

            } else {%>
            <script>
                document.getElementById("website").style.display = 'none';
                document.getElementById("DNS").style.display = 'none';
                document.getElementById("portmonitor").style.display = 'none';
                document.getElementById("ping").style.display = 'none';
                document.getElementById("ssl").style.display = 'none';
            </script>    
            <%  }
            %>

            <!-- Select -->
            <div class="control-group" >
                <label class="control-label"  for="username">Check Frequency</label>
                <div class="controls">
                    <select id="_frequency" name="_frequency" >
                        <option value="1min" id="1min">1 Min</option>
                        <option value="5min" id="5min">5 Min</option>
                        <option value="10min" id="10min">10 Min</option>
                        <option value="15min" id="15min">15 Min</option>
                        <option value="30min" id="30min">30 Min</option>
                        <option value="1hr" id="1hr">1 hr</option>
                        <option value="2hr" id="2hr">2 hr</option>
                        <option value="3hr" id="3hr">3 hr</option>
                        <option value="6hr" id="6hr">6 hr</option>
                        <option value="1day" id="1day">1 day</option>

                    </select>
                    <%
                        if (nms != null) {%>
                    <script>
                        var e = document.getElementById('_frequency');
                        e.options['<%=  nms.getFrequency()%>'].selected = 'true';

                    </script>   
                    <%}
                    %>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"  for="username">Response Time</label>
                <div class="controls">
                    <input type="text" id="_timeout" name="_timeout" placeholder="Response Time" class="input-medium">
                </div>
                <%
                    if (nms != null) {%>
                <script>
                    $('#_timeout').val('<%= nms.getTimeout()%>')
                </script>
                <% }
                %>
            </div>
            <hr>
            <h3 class="text-success">Alert To</h3>
            <div class="control-group">
                <label class="control-label"  for="username">Alert To</label>
                <div class="controls">
                    <select name="_selectedAlert" id="_selectedAlert" class="input-xxlarge" multiple>
                        <%    if (keys.length != 0) {
                                for (int i = 0; i < keys.length; i++) {%>
                        <option value="<%= keys[i]%>" ><%= keys[i]%></option>
                        <%  }
                            }%>
                    </select>
                    <%
                        if (nms != null) {
                            String[] array = nms.getSelectedalert().split(",");
                            for (int i = 0; i < array.length; i++) {
                    %>
                    <script>

                        $("#_selectedAlert").find('option[value=<%= array[i]%>]').prop("selected", true);

                    </script> 
                    <%}
                        }
                    %>

                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="listGroup.jsp" class="btn btn-default" >List Group </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    After drop count
                    <%
                        if (nms != null) {

                    %>
                    <input type="text" id="_dropcount" name="_dropcount" class="input-small" value="<%=nms.getDropcount()%>">
                    <%
                    } else {
                    %>
                    <input type="text" id="_dropcount" name="_dropcount" class="input-small" >
                    <%}
                    %>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label"  for="username">  Using Template</label>
                <div class="controls">
                    <select class="selectpicker" name="_templateName" id="_templateName"  class="span3">
                        <%
                            TemplateManagement tempObj = new TemplateManagement();
                            channel = (Channels) session.getAttribute("_apSChannelDetails");
                            sessionId = (String) session.getAttribute("_apSessionID");

                            Templates[] templates = tempObj.Listtemplates(sessionId, channel.getChannelid());

                            for (int j = 0; j < templates.length; j++) {

                                Templates t = templates[j];
                                int iType = t.getType();
                                // if(iType == 2){
                        %><%if (iType == 2 || iType == 1) {%>
                        <option  value="<%=t.getTemplatename()%>" id="<%=t.getTemplatename()%>"><%=t.getTemplatename()%></option>       
                        <% }%>
                        <%}%>
                    </select>

                    <button class="btn btn-link"  onclick="forwarTemplateRequestformonitors()" type="button">View / Edit(new window opens)</button>

                </div>
            </div>
            <div class="control-group">
                <label class="control-label"  for="username">With Report Type</label>
                <div class="controls">
                    <select name="_selectedReoprt" id="_selectedReoprt" class="input-medium" >
                        <option value="0" id="0">PDF and CSV</option>
                        <option value="1" id="1">PDF</option>
                        <option value="2" id="2">CSV</option>
                    </select>
                    <%
                        if (monitorsettings != null) {%>
                    <script>
                        var k = document.getElementById('_selectedReoprt');
                        k.options['<%= monitorsettings.getReportType()%>'].selected = 'true';
                        var tp = document.getElementById('_templateName');
                        tp.options['<%= nms.getTemplateId()%>'].selected = 'true';

                    </script> 
                    <% }
                    %>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <a href="./listSettings.jsp" class="btn" type="button"> << Back </a>
                    <%
                        if (Name != null) {%>
                    <button class="btn btn-primary" onclick="addMSettings(1)" type="button">Save Settings >> </button> 
                    <%} else {%>
                    <button class="btn btn-primary" onclick="addMSettings(0)" type="button">Save Settings >> </button> 
                    <%  }
                    %>


                </div>
            </div>

        </fieldset>
    </form>
</div>

<script>
    //    $(document).ready(function() {
    //        $("#_selectedType").select2()
    //    });

    $(document).ready(function () {
        $('#linkfordns').change(function () {
            if ($("#linkfordns").val() == "yes")
            {
                $('#dnslink').show();
            }
            else
                $("#dnslink").hide();
        });
    });
    $(document).ready(function () {
        $('#linkforssl').change(function () {
            if ($("#linkforssl").val() == "yes")
            {
                $('#ssllink').show();
            }
            else
                $("#ssllink").hide();
        });
    });
    $(document).ready(function () {
        $("#_selectedAlert").select2()
    });
    $(document).ready(function () {
        $("#_tags").select2()
    });
    $(document).ready(function () {
        $("#_operators").select2()
    });
    $(document).ready(function () {
        $("#_operatorsgrp").select2()
    });



//    document.getElementById("ssllink").style.display = 'none';
//    document.getElementById("dnslink").style.display = 'none';
    function doSomething(id, urlGet) {
        var value = document.getElementById(id).value;
        //alert(value);
        scriptforimage(urlGet, value);
    }

</script>


<%@include file="footer.jsp" %>