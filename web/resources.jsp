<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserGroupsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Usergroups"%>
<%@include file="header.jsp" %>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<script src="./assets/js/usermanagement.js"></script>
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<%
    String keyword = request.getParameter("_keyword");
%>
<div class="container-fluid" >
    <div id="auditTable">
        <h1 class="text-success">Web Resource Management</h1>
        <p>List of Mobile Resources in the system. Their management can be managed from this interface.</p>
        <h3>Search Resources</h3>   
        <div class="input-append">
            <form id="searchUserForm" name="searchResourceForm">
                <%if (keyword != null) {%>
                <input type="text" id="_keyword" name="_keyword" placeholder="Search using  resource name..." class="span4" value="<%=keyword%>"><span class="add-on"><i class="icon-search"></i></span>
                    <%} else {%>
                <input type="text" id="_keyword" name="_keyword" placeholder="Search using  resource name..." class="span4"><span class="add-on"><i class="icon-search"></i></span>
                    <%}%>               
                <a href="#" class="btn btn-success" onclick="searchResources()">Search Now</a>
            </form>
        </div>

        <%        Channels _apSChannelDetails = (Channels) session.getAttribute("_apSChannelDetails");
            OperatorsManagement oManagement = new OperatorsManagement();
            String _sessionID = (String) session.getAttribute("_apSessionID");
            Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        %>  

        <div id="resource_table_main">
            </p>
            <br>
        </div>
        <%if (keyword != null) {
        %>
        <script>
            searchResources();
        </script>
        <%
        }%>
        <p><a href="#addNewResource" class="btn btn-primary" data-toggle="modal" onclick="addresource()">Add New Resources&raquo;</a></p>


        <script>
            $(function () {
                $('#Pushdatetimepicker1').datepicker({
                    format: 'dd/MM/yyyy',
                    language: 'pt-BR'
                });
            });
            $(function () {
                $('#Pushdatetimepicker2').datepicker({
                    format: 'dd/MM/yyyy',
                    language: 'pt-BR'
                });
            });

        </script>

        <%@include file="footer.jsp" %>