<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Interactionresponse"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.Interactions"%>
<%@page import="com.mollatech.dictum.management.SurveyManagement"%>
<div class="container-fluid">
   
    <%
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
     String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    
    String _userId = request.getParameter("_userID");
    String _startdate = request.getParameter("_startDate");
    String _enddate = request.getParameter("_endDate");
    DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    Date startDate = null;
    if (_startdate != null && !_startdate.isEmpty()) {
        startDate = (Date) formatter.parse(_startdate);
    }
    Date endDate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        endDate = (Date) formatter.parse(_enddate);
    }
    
    UserManagement userObj = new UserManagement();
    AuthUser user = userObj.getUser(sessionId, channel.getChannelid(), _userId);

    %>
      <h3 class="text-success">Reports generated for user <%=user.getUserName() %> for duration <%=_startdate %> to <%=_enddate %> </h3>
    
    
    <div class="tabbable">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#PolicyCheck" data-toggle="tab">Policy Check</a></li>
            <li ><a href="#QAValidation" data-toggle="tab">QA Validation</a></li>
            <li ><a href="#Delivery" data-toggle="tab">Delivery</a></li>
            <li><a href="#OperatorControlled" data-toggle="tab">Operator Controlled</a></li>
             <li><a href="#DualOperatorControlled" data-toggle="tab">Dual Operator Controlled</a></li>
            <li><a href="#ecopintable" data-toggle="tab">Download Report</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="PolicyCheck">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="tab-pane active" id="msgcharts">
                            <p><h3><em><strong>Policy Check</strong></em></h3></p>
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">
                                        <div class="span4">
                                            <div id="PolicyCheckDonutChart" ></div>
                                            <!--Donut Chart-->
                                        </div>
                                        <div class="span7">
                                            <div id="PolicyCheckBarChart"></div>
                                            <!--Bar Chart-->   
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
            <div class="tab-pane active" id="QAValidation">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="tab-pane active" id="msgcharts">
                          
                            <p><h3><em><strong>QA-Validation</strong></em></h3></p>
                    
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">
                                        <div class="span4">
                                            <div id="QAValidationDonutChart" ></div>
                                            <!--Donut Chart-->
                                        </div>
                                        <div class="span7">
                                            <div id="QAValidationBarChart"></div>
                                            <!--Bar Chart-->   
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <div class="tab-pane active" id="Delivery">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="tab-pane active" id="msgcharts">
                           <p><h3><em><strong>Delivery</strong></em></h3></p>
                            
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">
                                        <div class="span4">
                                            <div id="DeliveryDonutChart" ></div>
                                            <!--Donut Chart-->
                                        </div>
                                        <div class="span7">
                                            <div id="DeliveryBarChart"></div>
                                            <!--Bar Chart-->   
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <div class="tab-pane active" id="OperatorControlled">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="tab-pane active" id="msgcharts">
                           <p><h3><em><strong>Operator Controlled</strong></em></h3></p>
                           
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">
                                        <div class="span4">
                                            <div id="OperatorControlledDonutChart" ></div>
                                            <!--Donut Chart-->
                                        </div>
                                        <div class="span7">
                                            <div id="OperatorControlledBarChart"></div>
                                            <!--Bar Chart-->   
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
             <div class="tab-pane active" id="DualOperatorControlled">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="tab-pane active" id="msgcharts">
                           <p><h3><em><strong>Dual Operator Controlled</strong></em></h3></p>
                           
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">
                                        <div class="span4">
                                            <div id="DualOperatorControlledDonutChart" ></div>
                                            <!--Donut Chart-->
                                        </div>
                                        <div class="span7">
                                            <div id="DualOperatorControlledBarChart"></div>
                                            <!--Bar Chart-->   
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
                
                <div class="tab-pane active" id="ecopintable">
                    <div id="ecopinusertable">
                        
                    </div>
               </div>

        </div>
    </div>



</div>
<script>
//      document.getElementById("question1").style.display = 'none';
document.getElementById("hidelable").style.display = 'none';

</script>
<%--<%@include file="footer.jsp" %>--%>