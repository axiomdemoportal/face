<%@page import="com.mollatech.axiom.nucleus.settings.ContactTagsSetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Hashtable"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.dictum.management.ContactManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Contacts"%>
<%
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();
    String _contactid = request.getParameter("_contactId");
    int iContactID = Integer.valueOf(_contactid);

    SettingsManagement smng = new SettingsManagement();
    ContactTagsSetting tagsObj = (ContactTagsSetting) smng.getSetting(sessionId, channel.getChannelid(), SettingsManagement.CONTACT_TAGS, SettingsManagement.PREFERENCE_ONE);

    //TagsManagement tagmngObj = new TagsManagement();
    //Tags[] tagsObj = tagmngObj.getAllTags(_channelId);

    ContactManagement contact = new ContactManagement();
    Contacts con = contact.getContact(sessionId, _channelId, iContactID);
    //String strTagName = "";
    String[] strTags = con.getTags().split(",");

    out.clear();
    
    String name = null;
    if(con.getName() != null ){
        name = con.getName();
    }
    String email = null;
    if(con.getEmailid() != null ){
        email = con.getEmailid();
    }
     String phone = null;
    if(con.getPhone() != null ){
        phone = con.getPhone();
    }
     String faxNo = null;
    if(con.getFaxno() != null ){
        faxNo = con.getFaxno();
    }
    
           
%>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
    <h3 id="myModalLabel">Edit "<%=con.getName()%>"</h3>
</div>
<div class="modal-body">
    <div class="row-fluid">
        <form class="form-horizontal" id="editContactForm" name="editContactForm">
            <fieldset>
                <div class="control-group">
                    <label class="control-label"  for="username">Name</label>
                    <div class="controls">
                        <input type="text" id="_nameEditContact" name="_nameEditContact" placeholder="set unique name for login" value="<%=name %>" class="input-xlarge">
                        <input type="hidden" id="_contactidEditContact" name="_contactidEditContact" value="<%=_contactid%>" >
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"  for="username">Email Id</label>
                    <div class="controls">
                        <input type="text" id="_emailEditContact" name="_emailEditContact" placeholder="set emailid for notification" value="<%=email %>" class="input-xlarge">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"  for="username">Phone</label>
                    <div class="controls">
                        <input type="text" id="_phoneEditContact" name="_phoneEditContact" placeholder="set phone for notification" value="<%=phone%>" class="input-xlarge">
                    </div>
                </div>
                    
                    <div class="control-group">
                        <label class="control-label"  for="username">Fax No</label>
                        <div class="controls">
                            <input type="text" id="_faxEditContact" name="_faxEditContact" placeholder="set fax number for notification" value="<%=faxNo %>" class="input-xlarge">
                        </div>
                    </div>



                <div class="control-group">
                    <label class="control-label"  for="username">Tag(s)</label>
                    <div class="controls">
                        <select  name="_tagIDEditContact" id="_tagIDEditContact"  class="span9" multiple>

                            <%

                                Hashtable tagsAvailable = new Hashtable();
                                String[] tags = tagsObj.getTags();
                                
                                
                                for ( int i=0;i<tags.length;i++) { 
                                    tagsAvailable.put(tags[i], tags[i]);
                                }
                                
                                for ( int i=0;i<strTags.length;i++) { 
                                    tagsAvailable.remove(strTags[i]);
                                }
                                
                                for ( int i=0;i<strTags.length;i++) { 
                                    %>
                                    <option value="<%=strTags[i]%>" selected><%=strTags[i]%></option>                                             
                                    <%                                                                       
                                }
                                
                                
                                Enumeration tagids = tagsAvailable.keys();
                                
                                while (tagids.hasMoreElements()) {
                                    String tagid = (String) tagids.nextElement();
                                    String tagname = (String) tagsAvailable.get(tagid);
                                    %>
                                        <option value="<%=tagname%>"><%=tagname%></option>                                             
                                    <%                                
                                }
                                  %>  
                        </select>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
</div>
<div class="modal-footer">
    <div class="span3" id="edit-contact-result"></div>
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button class="btn btn-primary" onclick="editContact()" id="addEditContactSubmitBut">Save Changes>></button>
</div>





