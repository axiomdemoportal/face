
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Otptokens"%>
<%@page import="com.mollatech.axiom.nucleus.db.Operators"%>
<%@page import="com.mollatech.axiom.connector.access.controller.AccessMatrixSettings"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<%
    final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;
    final int LOCKED_STATUS = -1;
    final int REMOVE_STATUS = -99;
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _searchtext = request.getParameter("_searchtext");
    String _changeStatus = request.getParameter("_status");
    int istatus = Integer.parseInt("99");
    UserManagement usermngObj = new UserManagement();
    AuthUser user[] = null;
    AuthUser userDetails = null;
    Otptokens otptokenObj = null;
    otptokenObj = new OTPTokenManagement(channel.getChannelid()).getOtpObjBySerialNo(sessionId,_searchtext);
    if(otptokenObj == null){
    user = usermngObj.SearchUserByStatus(sessionId, channel.getChannelid(), _searchtext, istatus);
    }
    
    if(user == null&&otptokenObj==null){
        userDetails = usermngObj.getUser(sessionId, channel.getChannelid(), _searchtext);
        if(userDetails != null){
        user = new AuthUser[1];
        user[0] = userDetails;
        }
    }
    else if(otptokenObj!=null){
        user = usermngObj.SearchUsersByID(sessionId, channel.getChannelid(), otptokenObj.getUserid());
    }
    
    session.setAttribute("userDetailsObject", user);
    String strerr = "No Record Found";
%>
<style>
    .innerr {
        display: inline-block;
        vertical-align: middle;
        background: yellow;
        padding: 3px 5px;
    }
    .block4 {
        /*        //color:#fff;background-color:#337ab7;*/
        box-shadow: 5px 5px 5px #888888;
        line-height: 90px;
        width:250px;
        margin-left:10px;
        margin-top: 20px;
    }
    .inner4 {
        line-height: normal; /* Reset line-height for the child. */
        background: none;
    }
    #square {
        width: 900px;
        height: 900px;
        background: red;
    }
    div.relative1 {
        margin: 1%;
        position: relative;
        height: 100%;
        color : black;
        border: 2px solid lightblue;
        box-shadow: 5px 5px 5px #888888;
        line-height: 100%;     
    } 

    div.absolute {
        position: absolute;
        top: 5%;
        bottom: 10%;
        left: 27%;
        width: 73%;
        height: 70%;
        font-size:100%;
    }
    .footerLink{
        margin-top: 20%;
        margin-left: 0%;
        text-align: center;
        line-height: 160%;              
        background-color:darkslategrey;                   
    }
    .footerMargin{
        margin-bottom: 0%; 
        color: white;
    }
    .slick-next {
        border: 1px solid lightblue;
        width: 20%;
        height: 40%;
        text-align: center;
        margin: 5%;
        margin-top: 7%;
        background-color:lightblue;

    }
    .slick-next:after {
        display: inline-block;
        vertical-align: middle;
        content: "";
        height: 135%;
    }
</style>

<h3>Searched Results for <i>"<%=_searchtext%>"</i></h3>
<% if(user!=null && user.length==1){%>
<table width="20%"> 
        <%}else{%>
<table width="100%">   
    <%}%>
    
    <%
        if (user != null) {
            
            for (int i = 0; i < user.length; i++) {
                String strStatus = null;
                if (user[i].getStatePassword() == ACTIVE_STATUS) {
                    strStatus = "ACTIVE";
                } else if (user[i].getStatePassword() == LOCKED_STATUS) {
                    strStatus = "LOCKED";
                } else if (user[i].getStatePassword() == SUSPEND_STATUS) {
                    strStatus = "SUSPEND";
                } else if (user[i].getStatePassword() == REMOVE_STATUS) {
                    strStatus = "REMOVED";
                }
                
    %>
    <% if(i!=0 && i % 5 == 0) {%>
    <tr>
        <%}%>
    <td height="absolute"><!--
        -->            <a href="showUserDetails1.jsp?search=<%=user[i].userId%>" target="_blank" style="line-height: 160%">
                <div class="relative1">
                    <div class="slick-next">
                        <span class="fa fa fa-user" style="font-size: 280%"></span>
                    </div><!--
-->                    <div class="absolute">                                 
                        <h5 class="title" style="text-align:left; text-height: 70%">UserId : <%=user[i].userId%> </h5>                           
                        <p class="intro">
                            <b>User name&nbsp;&nbsp;&nbsp;: </b><%=user[i].userName%><br>
                            <b>Email&nbsp;&nbsp;&nbsp;: </b><%=user[i].email%> <br>
                            <b>Phone&nbsp;: </b><%=user[i].phoneNo%><br>
                            <b>Status&nbsp;: </b><%=strStatus%>
                        </p>                          
                    </div><!--
-->                    <div class="footerLink">
                        <p class="footerMargin">View Details</p>
                    </div> 
                </div>
            </a><!--
-->        </td>
    <% if(i!=0 && i % 40 == 0) {%>
    </tr>
        <%}%>
    <%
            }
        }else{
    %>
    <tr>
        <td><h3>No record found...</h3></td> 
    </tr>
    <% }%>
</table>
    <br><br>
    <div id="tab-of-user">
    <ul class="nav nav-tabs" >
        <li class="active"><a href="#usercharts" data-toggle="tab">Charts</a></li>
        <li><a href="#userreport" data-toggle="tab">Tabular List</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="usercharts">
            <div class="row-fluid">
                <div class="span12">
                    <div class="control-group">
                        <div class="span4">
                            <div id="UserReportgraph" ></div>

                        </div>
                        <div  class="span8">
                            <div id="UserReportgraph1"></div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="tab-pane" id="userreport">
        </div>
    </div>
    </div>
    <script>
        $(document).ready(function(){
            document.getElementById("tab-of-user").style.display = 'none';
        });
        
        function showTabs(){
           document.getElementById("tab-of-user").style.display = 'block'; 
        }
    </script>