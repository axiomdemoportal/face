<%@include file="header.jsp" %>
<script src="./assets/js/accessMatrix.js"></script>
<%@page import="com.mollatech.axiom.connector.access.controller.AccessMatrixSettings"%>
<%@page import="com.mollatech.axiom.common.utils.UtilityFunctions"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.AxiomProtect"%>
<%@page import="java.io.ByteArrayInputStream"%>

<%@page import="com.mollatech.axiom.nucleus.db.Roles"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>

 <head>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.3.js"></script>
<script src="assets/js/htmlToPdf/jspdf.js" type="text/javascript"></script>
<script src="assets/js/htmlToPdf/from_html.js" type="text/javascript"></script>
<script src="assets/js/htmlToPdf/split_text_to_size.js" type="text/javascript"></script>
<script src="assets/js/htmlToPdf/standard_fonts_metrics.js" type="text/javascript"></script>
<script src="assets/js/htmlToPdf/cell.js" type="text/javascript"></script>
<script src="assets/js/htmlToPdf/FileSaver.js" type="text/javascript"></script>
<script type="text/javascript">
        $(document).ready(function() {

            $("#exportpdf").click(function() {
                var pdf = new jsPDF('p', 'pt', 'ledger');
                // source can be HTML-formatted string, or a reference
                // to an actual DOM element from which the text will be scraped.
                source = $('#SystemManagement')[0];

                // we support special element handlers. Register them with jQuery-style 
                // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
                // There is no support for any other type of selectors 
                // (class, of compound) at this time.
                specialElementHandlers = {
                    // element with id of "bypass" - jQuery style selector
                    '#bypassme' : function(element, renderer) {
                        // true = "handled elsewhere, bypass text extraction"
                        return true
                    }
                };
                margins = {
                    top : 80,
                    bottom : 60,
                    left : 60,
                    width : 522
                };
                // all coords and widths are in jsPDF instance's declared units
                // 'inches' in this case
                pdf.fromHTML(source, // HTML string or DOM elem ref.
                margins.left, // x coord
                margins.top, { // y coord
                    'width' : margins.width, // max width of content on PDF
                    'elementHandlers' : specialElementHandlers
                },

                function(dispose) {
                    // dispose: object with X, Y of the last line add to the PDF 
                    //          this allow the insertion of new lines after html
                    pdf.save('fileNameOfGeneretedPdf.pdf');
                }, margins);
            });

        });
    </script>
 </head>

<%    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
//    Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
    String _sessionID = (String) session.getAttribute("_apSessionID");
    OperatorsManagement opMngt = new OperatorsManagement();
    String roleId = request.getParameter("_roleId");
    int roleid = Integer.parseInt(roleId);
    if(roleId == null){
        roleId = "-1";
    }
    String _accesstype = request.getParameter("_type");
    String _operatorID = request.getParameter("_operatorID");
    Roles roleObj = null;
    int iRoleID = -1;
    if (!_accesstype.equals("OPERATOR")) {
        iRoleID = Integer.parseInt(roleId);
        roleObj = opMngt.getRoleByRoleId(channel.getChannelid(), iRoleID);
    }

    OperatorsManagement opsMngt = new OperatorsManagement();
    OperatorsManagement opDetails = new OperatorsManagement();
   Roles role;
   role = opDetails.getRoleById(channel.getChannelid(), roleid);
%>

<div class="container-fluid">
    <%if (roleObj != null) {%>
    <h1 class="text-success">Role Access for <%=roleObj.getName()%></h1>
    <%} else if (_operatorID != null) {%>
    <h1 class="text-success">Role Access for <%=_operatorID%></h1>
    <%}else{%>
    <h1 class="text-success">Role Access Management</h1>
    <%}%>

    <div class="tabbable">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#SystemManagement" data-toggle="tab">System Management</a></li>
            <li><a href="#SystemConfiguration" data-toggle="tab">System Configuration</a></li>
            <li><a href="#SecurityManagment" data-toggle="tab">Security Management</a></li>
            <li><a href="#SystemReport" data-toggle="tab">System Reports</a></li>
            <li><a href="#SystemSpecific" data-toggle="tab">System Specific</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="SystemManagement">
                <table class="table table-bordered table-hover table-striped" style="width: 600px;">

                    <%
                        // byte[] accessByte = roleObj.getAccessentry();
                        AccessMatrixSettings accessSettings = null;
                        if (!_accesstype.equals("OPERATOR")) {
                            if (roleObj != null) {
                                
                                byte[] accessByte = roleObj.getAccessentry();
                                ByteArrayInputStream bais = null;
                                Object object = null;
                                if (accessByte != null) {
                                    bais = new ByteArrayInputStream(AxiomProtect.AccessDataBytes(accessByte));
                                    object = UtilityFunctions.deserializeFromObject(bais);
                                }
                                if (object != null) {
                                    accessSettings = (AccessMatrixSettings) object;
                                } else {
                                    accessSettings = new AccessMatrixSettings();
                                }
                            }
                        } else {
                            
                            Operators operator = opsMngt.getOperatorByName(_sessionID, channel.getChannelid(), _operatorID);
                            byte[] accessByte = operator.getAccessentry();
                            ByteArrayInputStream bais = null;
                            Object object = null;
                            if (accessByte != null) {
                                bais = new ByteArrayInputStream(AxiomProtect.AccessDataBytes(accessByte));
                                object = UtilityFunctions.deserializeFromObject(bais);
                            }
                            if (object != null) {
                                accessSettings = (AccessMatrixSettings) object;
                            } else {
                                accessSettings = new AccessMatrixSettings();
                            }
                        }

                        if (accessSettings == null) {
                            accessSettings = new AccessMatrixSettings();
                        }
                    %>
                    <tr>
                        <td>Sr. No. </td>
                        <td>Type</td>
                        <td>List</td>
                        <td>Add</td>
                        <td>Edit</td>
                        <td>Delete</td>

                    </tr>
                    <tr>
                        <td>1 </td>
                        <td>Channels Management</td>
                        <%if (accessSettings.listChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selChannelLists" name="_selChannelLists"  onchange="accessList('ChannelLists')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"  id="_selChannelLists" name="_selChannelLists"  onchange="accessList('ChannelLists')"></td>
                            <%}%>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannels" name="_selAddChannels"  onchange="accessList('AddChannels')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannels" name="_selAddChannels"  onchange="accessList('AddChannels')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannels" name="_selEditChannels"  onchange="accessList('EditChannels')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannels" name="_selEditChannels"  onchange="accessList('EditChannels')"></td>
                            <%}%>
                        <td>Not Available</td>

                    </tr>
                    <tr>
                        <td>2 </td>
                        <td>Operators Management</td>
                        <%if (accessSettings.listOperator == true) { %>
                        <td> <input type="checkbox" checked id="_selListOperator" name="_selListOperator"  onchange="accessList('ListOperator')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"  id="_selListOperator" name="_selListOperator"  onchange="accessList('ListOperator')"></td>
                            <%}%>
                            <%if (accessSettings.addOperator == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddOperator" name="_selAddOperator"  value="true" onchange="accessList('AddOperator')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddOperator" name="_selAddOperator"  value="false" onchange="accessList('AddOperator')"></td>
                            <%}%>
                            <%if (accessSettings.editOperator == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditOperator" name="_selEditOperator"  value="true" onchange="accessList('EditOperator')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditOperator" name="_selEditOperator"  value="false" onchange="accessList('EditOperator')"></td>
                            <%}%>
                            <%if(role.getName().equals("sysadmin")){%>
                        <td>Not Available</td>
                        <%}else{%>
                        <td>Not Available</td>
                        <%}%>
                        

                    </tr>
                    <tr>
                        <td>3 </td>
                        <td>Units Management</td>
                        <%if (accessSettings.listUnits == true) { %>
                        <td> <input type="checkbox"  checked id="_selListUnits" name="_selListUnits"  value="true" onchange="accessList('ListUnits')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListUnits" name="_selListUnits"  value="false" onchange="accessList('ListUnits')"></td>
                            <%}%>
                            <%if (accessSettings.addUnits == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddUnits" name="_selAddUnits"  value="true" onchange="accessList('AddUnits')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddUnits" name="_selAddUnits"  value="false" onchange="accessList('AddUnits')"></td>
                            <%}%>
                            <%if (accessSettings.editUnits == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditUnits" name="_selEditUnits"  value="true" onchange="accessList('EditUnits')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditUnits" name="_selEditUnits"  value="false" onchange="accessList('EditUnits')"></td>
                            <%}%>
                            <%if (accessSettings.deleteUnits == true) { %>
                        <td> <input type="checkbox"  checked id="_selDeleteUnits" name="_selDeleteUnits"  value="true" onchange="accessList('DeleteUnits')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selDeleteUnits" name="_selDeleteUnits"  value="false" onchange="accessList('DeleteUnits')"></td>
                            <%}%>
                    </tr>
                    <tr>
                        <td>4 </td>
                        <td>Templates Management</td>
                        <%if (accessSettings.listTemplate == true) { %>
                        <td> <input type="checkbox"  checked id="_selListTemplate" name="_selListTemplate"  value="true" onchange="accessList('ListTemplate')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListTemplate" name="_selListTemplate"  value="false" onchange="accessList('ListTemplate')"></td>
                            <%}%>

                        <%if (accessSettings.addTemplate == true) { %>
                        <!--
                        <td> <input type="checkbox"  checked id="_selAddTemplate" name="_selAddTemplate"  value="true" onchange="accessList('AddTemplate')"></td>
                        -->
                        <td>Not Available</td>
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selAddTemplate" name="_selAddTemplate"  value="false" onchange="accessList('AddTemplate')"></td>
                        -->
                        <td>Not Available</td>
                            <%}%>
                            <%if (accessSettings.editTemplate == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditTemplate" name="_selEditTemplate"  value="true" onchange="accessList('EditTemplate')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditTemplate" name="_selEditTemplate"  value="false" onchange="accessList('EditTemplate')"></td>
                            <%}%>
                            <%if (accessSettings.removeTemplate == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveTemplate" name="_selRemoveTemplate"  value="true" onchange="accessList('RemoveTemplate')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveTemplate" name="_selRemoveTemplate"  value="false" onchange="accessList('RemoveTemplate')"></td>
                            <%}%>
                    </tr>
                    <tr>
                        <td>5 </td>
                        <td>User Management (Password Authentication)</td>
                        <%if (accessSettings.ListUsers == true) { %>
                        <td> <input type="checkbox"  checked id="_selUserLists" name="_selUserLists"  value="true" onchange="accessList('UserLists')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selUserLists" name="_selUserLists"  value="false" onchange="accessList('UserLists')"></td>
                            <%}%>
                            <%if (accessSettings.addUser == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddUsers" name="_selAddUsers"  value="true" onchange="accessList('AddUsers')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddUsers" name="_selAddUsers"  value="false" onchange="accessList('AddUsers')"></td>
                            <%}%>
                            <%if (accessSettings.editUser == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditUsers" name="_selEditUsers"  value="true" onchange="accessList('EditUsers')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditUsers" name="_selEditUsers"  value="false" onchange="accessList('EditUsers')"></td>
                            <%}%>
                            <%if (accessSettings.removeUser == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveUsers" name="_selRemoveUsers"  value="true" onchange="accessList('RemoveUsers')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveUsers" name="_selRemoveUsers"  value="false" onchange="accessList('RemoveUsers')"></td>
                            <%}%>
                    </tr>
                    <tr>
                        <td>6 </td>
                        <td>User Group Management</td>
                        <%if (accessSettings.ListUsersGroup == true) { %>
                        <td> <input type="checkbox"  checked id="_selListUserGroup" name="_selListUserGroup"  value="true" onchange="accessList('ListUserGroup')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListUserGroup" name="_selListUserGroup"  value="false" onchange="accessList('ListUserGroup')"></td>
                            <%}%>
                            <%if (accessSettings.addUserGroup == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddUserGroup" name="_selAddUserGroup"  value="true" onchange="accessList('AddUserGroup')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddUserGroup" name="_selAddUserGroup"  value="false" onchange="accessList('AddUserGroup')"></td>
                            <%}%>
                            <%if (accessSettings.editUserGroup == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditUserGroup" name="_selEditUserGroup"  value="true" onchange="accessList('EditUserGroup')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditUserGroup" name="_selEditUserGroup"  value="false" onchange="accessList('EditUserGroup')"></td>
                            <%}%>
                            <%if (accessSettings.removeUserGroup == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveUserGroup" name="_selRemoveUserGroup"  value="true" onchange="accessList('RemoveUserGroup')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveUserGroup" name="_selRemoveUserGroup"  value="false" onchange="accessList('RemoveUserGroup')"></td>
                            <%}%>
                    </tr>

                    <tr>
                        <td> 7</td>
                        <td>Challenge Response Management (Q&A Authentication)</td>
                        <%if (accessSettings.listChallengeResponse == true) { %>
                        <td> <input type="checkbox"  checked id="_selListChallengeResponse" name="_selListChallengeResponse"  value="true" onchange="accessList('ListChallengeResponse')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListChallengeResponse" name="_selListChallengeResponse"  value="false" onchange="accessList('ListChallengeResponse')"></td>
                            <%}%>
                            <%if (accessSettings.addChallengeResponse == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddChallengeResponse" name="_selAddChallengeResponse"  value="true" onchange="accessList('AddChallengeResponse')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddChallengeResponse" name="_selAddChallengeResponse"  value="false" onchange="accessList('AddChallengeResponse')"></td>
                            <%}%>
                            <%if (accessSettings.editChallengeResponse == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChallengeResponse" name="_selEditChallengeResponse"  value="true" onchange="accessList('EditChallengeResponse')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChallengeResponse" name="_selEditChallengeResponse"  value="false" onchange="accessList('EditChallengeResponse')"></td>
                            <%}%>
                            <%if (accessSettings.removeChallengeResponse == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveChallengeResponse" name="_selRemoveChallengeResponse"  value="true" onchange="accessList('RemoveChallengeResponse')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveChallengeResponse" name="_selRemoveChallengeResponse"  value="false" onchange="accessList('RemoveChallengeResponse')"></td>
                            <%}%>
                    </tr>
                    <tr>
                        <td> 8</td>
                        <td>Trusted Device Management(Device ID Authentication)</td>
                        <%if (accessSettings.listTrustedDevice == true) { %>
                        <td> <input type="checkbox"  checked id="_selListTrustedDevice" name="_selListTrustedDevice"  value="true" onchange="accessList('ListTrustedDevice')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListTrustedDevice" name="_selListTrustedDevice"  value="false" onchange="accessList('ListTrustedDevice')"></td>
                            <%}%>
                            <%if (accessSettings.addTrustedDevice == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddTrustedDevice" name="_selAddTrustedDevice"  value="true" onchange="accessList('AddTrustedDevice')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddTrustedDevice" name="_selAddTrustedDevice"  value="false" onchange="accessList('AddTrustedDevice')"></td>
                            <%}%>
                            <%if (accessSettings.editTrustedDevice == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditTrustedDevice" name="_selEditTrustedDevice"  value="true" onchange="accessList('EditTrustedDevice')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditTrustedDevice" name="_selEditTrustedDevice"  value="false" onchange="accessList('EditTrustedDevice')"></td>
                            <%}%>
                            <%if (accessSettings.removeDestroyClients == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveTrustedDevice" name="_selRemoveTrustedDevice"  value="true" onchange="accessList('RemoveTrustedDevice')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveTrustedDevice" name="_selRemoveTrustedDevice"  value="false" onchange="accessList('RemoveTrustedDevice')"></td>
                            <%}%>

                    </tr>
                    <tr>
                        <td> 9</td>
                        <td>Geo-location Management(Geo Fencing And Tracking Authentication)</td>
                        <%if (accessSettings.listGeoLocation == true) { %>
                        <td> <input type="checkbox"  checked id="_selListGeoLocation" name="_selListGeoLocation"  value="true" onchange="accessList('ListGeoLocation')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListGeoLocation" name="_selListGeoLocation"  value="false" onchange="accessList('ListGeoLocation')"></td>
                            <%}%>
                            <%if (accessSettings.addGeoLocation == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddGeoLocation" name="_selAddGeoLocation"  value="true" onchange="accessList('AddGeoLocation')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddGeoLocation" name="_selAddGeoLocation"  value="false" onchange="accessList('AddGeoLocation')"></td>
                            <%}%>
                            <%if (accessSettings.editGeoLocation == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditGeoLocation" name="_selEditGeoLocation"  value="true" onchange="accessList('EditGeoLocation')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditGeoLocation" name="_selEditGeoLocation"  value="false" onchange="accessList('EditGeoLocation')"></td>
                            <%}%>
                        <td>Not Available</td>
                            <%if (accessSettings.removeGeoLocation == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selRemoveGeoLocation" name="_selRemoveGeoLocation"  value="true" onchange="accessList('RemoveGeoLocation')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selRemoveGeoLocation" name="_selRemoveGeoLocation"  value="false" onchange="accessList('RemoveGeoLocation')"></td>-->
                            <%}%>
                    </tr>
                    <tr>
                        <td> 10</td>
                        <td>System Alert's(for Operators)</td>
                        <%if (accessSettings.listSystemMessage == true) { %>
                        <td> <input type="checkbox"  checked id="_selListSystemMessage" name="_selListSystemMessage"  value="true" onchange="accessList('ListSystemMessage')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListSystemMessage" name="_selListSystemMessage"  value="false" onchange="accessList('ListSystemMessage')"></td>
                            <%}%>
                            <%if (accessSettings.addSystemMessage == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddSystemMessage" name="_selAddSystemMessage"  value="true" onchange="accessList('AddSystemMessage')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddSystemMessage" name="_selAddSystemMessage"  value="false" onchange="accessList('AddSystemMessage')"></td>
                            <%}%>
                            <%if (accessSettings.editSystemMessage == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditSystemMessage" name="_selEditSystemMessage"  value="true" onchange="accessList('EditSystemMessage')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditSystemMessage" name="_selEditSystemMessage"  value="false" onchange="accessList('EditSystemMessage')"></td>
                            <%}%>
                            <%if (accessSettings.removeSystemMessage == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveSystemMessage" name="_selRemoveSystemMessage"  value="true" onchange="accessList('RemoveSystemMessage')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveSystemMessage" name="_selRemoveSystemMessage"  value="false" onchange="accessList('RemoveSystemMessage')"></td>
                            <%}%>

                    </tr>
                    <tr>
                        <td> 11</td>
                        <td>Secure Phrase Management (Dynamic Image With Sweet Spot)</td>
                        <%if (accessSettings.listImageManagement == true) { %>
                        <td> <input type="checkbox"  checked id="_selListImageManagement" name="_selListImageManagement"  value="true" onchange="accessList('ListImageManagement')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListImageManagement" name="_selListImageManagement"  value="false" onchange="accessList('ListImageManagement')"></td>
                            <%}%>
                            <%if (accessSettings.addImageManagement == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddImageManagement" name="_selAddImageManagement"  value="true" onchange="accessList('AddImageManagement')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddImageManagement" name="_selAddImageManagement"  value="false" onchange="accessList('AddImageManagement')"></td>
                            <%}%>
                            <%if (accessSettings.editImageManagement == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditImageManagement" name="_selEditImageManagement"  value="true" onchange="accessList('EditImageManagement')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditImageManagement" name="_selEditImageManagement"  value="false" onchange="accessList('EditImageManagement')"></td>
                            <%}%>
                            <%if (accessSettings.removeImageManagement == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveImageManagement" name="_selRemoveImageManagement"  value="true" onchange="accessList('RemoveImageManagement')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveImageManagement" name="_selRemoveImageManagement"  value="false" onchange="accessList('RemoveImageManagement')"></td>
                            <%}%>

                    </tr>
                    
                    <% if ( 1 == 2 )  { %>  
                    <tr>
                        <td> 11</td>
                        <td>SNMP Management</td>
                        <%if (accessSettings.listSnmpManagement == true) { %>
                        <td> <input type="checkbox"  checked id="_selListSnmpManagement" name="_selListSnmpManagement"  value="true" onchange="accessList('ListSnmpManagement')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListSnmpManagement" name="_selListSnmpManagement"  value="false" onchange="accessList('ListSnmpManagement')"></td>
                            <%}%>
                            <%if (accessSettings.addSnmpManagement == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddSnmpManagement" name="_selAddSnmpManagement"  value="true" onchange="accessList('AddSnmpManagement')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddSnmpManagement" name="_selAddSnmpManagement"  value="false" onchange="accessList('AddSnmpManagement')"></td>
                            <%}%>
                            <%if (accessSettings.editSnmpManagement == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditSnmpManagement" name="_selEditSnmpManagement"  value="true" onchange="accessList('EditSnmpManagement')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditSnmpManagement" name="_selEditSnmpManagement"  value="false" onchange="accessList('EditSnmpManagement')"></td>
                            <%}%>
                            <%if (accessSettings.removeSnmpManagement == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveSnmpManagement" name="_selRemoveSnmpManagement"  value="true" onchange="accessList('RemoveSnmpManagement')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveSnmpManagement" name="_selRemoveSnmpManagement"  value="false" onchange="accessList('RemoveSnmpManagement')"></td>
                            <%}%>

                    </tr>
                    <%  } %> 
<!--                    <tr>
                        <td> 12</td>
                        <td>Report Scheduler Management (for SMS,Email Messages etc) </td>
                        <%if (accessSettings.listReportSchedulerManagement == true) { %>
                        <td> <input type="checkbox"  checked id="_selListReportSchedulerManagement" name="_selListReportSchedulerManagement"  value="true" onchange="accessList('ListReportSchedulerManagement')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListReportSchedulerManagement" name="_selListReportSchedulerManagement"  value="false" onchange="accessList('ListReportSchedulerManagement')"></td>
                            <%}%>
                            <%if (accessSettings.addReportSchedulerManagement == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddReportSchedulerManagement" name="_selAddReportSchedulerManagement"  value="true" onchange="accessList('AddReportSchedulerManagement')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddReportSchedulerManagement" name="_selAddReportSchedulerManagement"  value="false" onchange="accessList('AddReportSchedulerManagement')"></td>
                            <%}%>
                            <%if (accessSettings.editReportSchedulerManagement == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditReportSchedulerManagement" name="_selEditReportSchedulerManagement"  value="true" onchange="accessList('EditReportSchedulerManagement')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditReportSchedulerManagement" name="_selEditReportSchedulerManagement"  value="false" onchange="accessList('EditReportSchedulerManagement')"></td>
                            <%}%>
                            <%if (accessSettings.removeReportSchedulerManagement == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveReportSchedulerManagement" name="_selRemoveReportSchedulerManagement"  value="true" onchange="accessList('RemoveReportSchedulerManagement')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveReportSchedulerManagement" name="_selRemoveReportSchedulerManagement"  value="false" onchange="accessList('RemoveReportSchedulerManagement')"></td>
                            <%}%>

                    </tr>-->
                    <tr>
                        <td> 12</td>
                        <td>Two-Way Authentication Management (Out of Band Channel Two Way Authentication)</td>
                        <%if (accessSettings.listTwoWayAuthManagement == true) { %>
                        <td> <input type="checkbox"  checked id="_selListTwoWayAuthManagement" name="_selListTwoWayAuthManagement"  value="true" onchange="accessList('ListTwoWayAuthManagement')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListTwoWayAuthManagement" name="_selListTwoWayAuthManagement"  value="false" onchange="accessList('ListTwoWayAuthManagement')"></td>
                            <%}%>
                            <%if (accessSettings.addTwoWayAuthManagement == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddTwoWayAuthManagement" name="_selAddTwoWayAuthManagement"  value="true" onchange="accessList('AddTwoWayAuthManagement')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddTwoWayAuthManagement" name="_selAddTwoWayAuthManagement"  value="false" onchange="accessList('AddTwoWayAuthManagement')"></td>
                            <%}%>
                            <%if (accessSettings.editTwoWayAuthManagement == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditTwoWayAuthManagement" name="_selEditTwoWayAuthManagement"  value="true" onchange="accessList('EditTwoWayAuthManagement')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditTwoWayAuthManagement" name="_selEditTwoWayAuthManagement"  value="false" onchange="accessList('EditTwoWayAuthManagement')"></td>
                            <%}%>
                            <%if (accessSettings.removeTwoWayAuthManagement == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveTwoWayAuthManagement" name="_selRemoveTwoWayAuthManagement"  value="true" onchange="accessList('RemoveTwoWayAuthManagement')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveTwoWayAuthManagement" name="_selRemoveTwoWayAuthManagement"  value="false" onchange="accessList('RemoveTwoWayAuthManagement')"></td>
                            <%}%>

                    </tr>
                    
                      <tr>
                        <td> 13</td>
                        <td>Web Watch & Seal Management</td>
                        <%if (accessSettings.listWebWatch == true) { %>
                        <td> <input type="checkbox"  checked id="_selListWebWatch" name="_selListWebWatch"  value="true" onchange="accessList('ListWebWatch')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListWebWatch" name="_selListWebWatch"  value="false" onchange="accessList('ListWebWatch')"></td>
                            <%}%>
                            <%if (accessSettings.addWebWatch == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddWebWatch" name="_selAddWebWatch"  value="true" onchange="accessList('AddWebWatch')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddWebWatch" name="_selAddWebWatch"  value="false" onchange="accessList('AddWebWatch')"></td>
                            <%}%>
                            <%if (accessSettings.editWebWatch == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditWebWatch" name="_selEditWebWatch"  value="true" onchange="accessList('EditWebWatch')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditWebWatch" name="_selEditWebWatch"  value="false" onchange="accessList('EditWebWatch')"></td>
                            <%}%>
                            <%if (accessSettings.removeWebWatch == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveWebWatch" name="_selRemoveWebWatch"  value="true" onchange="accessList('RemoveWebWatch')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveWebWatch" name="_selRemoveWebWatch"  value="false" onchange="accessList('RemoveWebWatch')"></td>
                            <%}%>

                    </tr>
                    
                     <tr>
                        <td> 14</td>
                        <td>Web Resource Management (Secure Mobile Browser/Single Sign On)</td>
                        <%if (accessSettings.listWebResource == true) { %>
                        <td> <input type="checkbox"  checked id="_selListWebResource" name="_selListWebResource"  value="true" onchange="accessList('ListWebResource')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListWebResource" name="_selListWebResource"  value="false" onchange="accessList('ListWebResource')"></td>
                            <%}%>
                            <%if (accessSettings.addWebResource == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddWebResource" name="_selAddWebResource"  value="true" onchange="accessList('AddWebResource')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddWebResource" name="_selAddWebResource"  value="false" onchange="accessList('AddWebResource')"></td>
                            <%}%>
                            <%if (accessSettings.editWebResource == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditWebResource" name="_selEditWebResource"  value="true" onchange="accessList('EditWebResource')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditWebResource" name="_selEditWebResource"  value="false" onchange="accessList('EditWebResource')"></td>
                            <%}%>
                            <%if (accessSettings.removeWebResource == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveWebResource" name="_selRemoveWebResource"  value="true" onchange="accessList('RemoveWebResource')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveWebResource" name="_selRemoveWebResource"  value="false" onchange="accessList('RemoveWebResource')"></td>
                            <%}%>
                    </tr>
                     <tr>
                        <td>15</td>
                        <td>Error Messages</td>
                        
                        <%if (accessSettings.listErrorMessages == true) { %>
                        <td> <input type="checkbox"  checked id="_sellistErrorMessages" name="_sellistErrorMessages"  value="true" onchange="accessList('listErrorMessages')"></td>
                            <%} else {%>
                     <td> <input type="checkbox"   id="_sellistErrorMessages" name="_sellistErrorMessages"  value="false" onchange="accessList('listErrorMessages')"></td>
                            <%}%>
                       <td>Not Available</td>
                            <%if (accessSettings.editErrorMessages == true) { %>
                        <td> <input type="checkbox"  checked id="_seleditErrorMessages" name="_seleditErrorMessages"  value="true" onchange="accessList('editErrorMessages')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seleditErrorMessages" name="_seleditErrorMessages"  value="false" onchange="accessList('editErrorMessages')"></td>
                            <%}%>
                        <td> Not Available</td>
                          </tr>
                   
                       <tr>
                           <td>16</td>
                        <td>Access Matrix</td>
                        
                       <%if (accessSettings.viewaccessMatrix == true) { %>
                        <td> <input type="checkbox"  checked id="_selviewAccessMatrix" name="_selviewAccessMatrix"  value="true" onchange="accessList('viewAccessMatrix')"></td>
                            <%} else {%>
                         <td> <input type="checkbox"   id="_selviewAccessMatrix" name="_selviewAccessMatrix"  value="false" onchange="accessList('viewAccessMatrix')"></td>
                            <%}%>
                        <%if (accessSettings.addaccessMatrix == true) { %>
                        <td> <input type="checkbox"  checked id="_seladdAccessMatrix" name="_seladdAccessMatrix"  value="true" onchange="accessList('addAccessMatrix')"></td>
                            <%} else {%>
                         <td> <input type="checkbox"   id="_seladdAccessMatrix" name="_seladdAccessMatrix"  value="false" onchange="accessList('addAccessMatrix')"></td>
                            <%}%>
                            <%if (accessSettings.editaccessMatrix == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditAccessMatrix" name="_selEditAccessMatrix"  value="true" onchange="accessList('EditAccessMatrix')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditAccessMatrix" name="_selEditAccessMatrix"  value="false" onchange="accessList('EditAccessMatrix')"></td>
                            <%}%>
                          <%if (accessSettings.removeaccessMatrix == true) { %>
                        <td> <input type="checkbox"  checked id="_selremoveAccessMatrix" name="_selremoveAccessMatrix"  value="true" onchange="accessList('removeAccessMatrix')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selremoveAccessMatrix" name="_selremoveAccessMatrix"  value="false" onchange="accessList('removeAccessMatrix')"></td>
                            <%}%>
                    </tr>
                          
                          <tr>
                        <td></td>
                        <%if (oprObj.getRoleid() != 1) {%>
                        <%if (accessObj != null && (accessObj.editaccessMatrix == true)) {%>
                        <td><p><button class="btn btn-primary" onclick="saveMatrix('<%=roleId%>', '<%=OperatorsManagement.MatrixChannel%>', '<%=_operatorID%>')">Save&raquo;</button></p></td>
                        <%}else{%>
                        <td><p><button class="btn btn-primary" onclick="InvalidRequest('rolereports')">Save&raquo;</button></p></td>
                        <%}}else{%>
                        <td><p><button class="btn btn-primary" onclick="saveMatrix('<%=roleId%>', '<%=OperatorsManagement.MatrixChannel%>', '<%=_operatorID%>')">Save&raquo;</button></p></td>
                        <%}%>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                    </tr>
                </table>
            </div>
            <div class="tab-pane" id="SystemConfiguration">
                <table class="table table-bordered table-hover table-striped" style="width: 600px;">

                    <tr>
                        <td>Sr. No. </td>
                        <td>Type</td>
                        <td>List</td>
                        <td>Add/Change</td>
<!--                        <td>Edit</td>
                        <td>Delete</td>-->
                    </tr>
                    <tr>
                        <td>1 </td>
                        <td>SMS Gateway Configuration</td>
                        <%if (accessSettings.listSmsGateway == true) { %>
                        <td> <input type="checkbox"  checked id="_selListSmsGateway" name="_selListSmsGateway"  value="true" onchange="accessList('ListSmsGateway')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListSmsGateway" name="_selListSmsGateway"  value="false" onchange="accessList('ListSmsGateway')"></td>
                            <%}%>
                            <%if (accessSettings.addSmsGateway == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selAddSmsGateway" name="_selAddSmsGateway"  value="true" onchange="accessList('AddSmsGateway')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selAddSmsGateway" name="_selAddSmsGateway"  value="false" onchange="accessList('AddSmsGateway')"></td>-->
                            <%}%>
                            <%if (accessSettings.editSmsGateway == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditSmsGateway" name="_selEditSmsGateway"  value="true" onchange="accessList('EditSmsGateway')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditSmsGateway" name="_selEditSmsGateway"  value="false" onchange="accessList('EditSmsGateway')"></td>
                            <%}%>
                            <%if (accessSettings.removeSmsGateway == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selRemoveSmsGateway" name="_selRemoveSmsGateway"  value="true" onchange="accessList('RemoveSmsGateway')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selRemoveSmsGateway" name="_selRemoveSmsGateway"  value="false" onchange="accessList('RemoveSmsGateway')"></td>-->
                            <%}%>

                    </tr>
                    <tr>
                        <td>2 </td>
                        <td>USSD Gateway Configuration</td>
                        <%if (accessSettings.listUSSDGateway == true) { %>
                        <td> <input type="checkbox"  checked id="_selListUSSDGateway" name="_selListUSSDGateway"  value="true" onchange="accessList('ListUSSDGateway')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListUSSDGateway" name="_selListUSSDGateway"  value="false" onchange="accessList('ListUSSDGateway')"></td>
                            <%}%>
                            <%if (accessSettings.addUSSDGateway == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selAddUSSDGateway" name="_selAddUSSDGateway"  value="true" onchange="accessList('AddUSSDGateway')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selAddUSSDGateway" name="_selAddUSSDGateway"  value="false" onchange="accessList('AddUSSDGateway')"></td>-->
                            <%}%>
                            <%if (accessSettings.editUSSDGateway == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditUSSDGateway" name="_selEditUSSDGateway"  value="true" onchange="accessList('EditUSSDGateway')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditUSSDGateway" name="_selEditUSSDGateway"  value="false" onchange="accessList('EditUSSDGateway')"></td>
                            <%}%>
                            <%if (accessSettings.removeUSSDGateway == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selRemoveUSSDGateway" name="_selRemoveUSSDGateway"  value="true" onchange="accessList('RemoveUSSDGateway')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selRemoveUSSDGateway" name="_selRemoveUSSDGateway"  value="false" onchange="accessList('RemoveUSSDGateway')"></td>-->
                            <%}%>
                    </tr>
                    <tr>
                        <td>3 </td>
                        <td>VOICE Gateway Configuration</td>
                        <%if (accessSettings.listVOICEGateway == true) { %>
                        <td> <input type="checkbox"  checked id="_selListVOICEGateway" name="_selListVOICEGateway"  value="true" onchange="accessList('ListVOICEGateway')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListVOICEGateway" name="_selListVOICEGateway"  value="false" onchange="accessList('ListVOICEGateway')"></td>
                            <%}%>
                            <%if (accessSettings.addVOICEGateway == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selAddVOICEGateway" name="_selAddVOICEGateway"  value="true" onchange="accessList('AddVOICEGateway')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selAddVOICEGateway" name="_selAddVOICEGateway"  value="false" onchange="accessList('AddVOICEGateway')"></td>-->
                            <%}%>
                            <%if (accessSettings.editVOICEGateway == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditVOICEGateway" name="_selEditVOICEGateway"  value="true" onchange="accessList('EditVOICEGateway')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditVOICEGateway" name="_selEditVOICEGateway"  value="false" onchange="accessList('EditVOICEGateway')"></td>
                            <%}%>
                            <%if (accessSettings.removeVOICEGateway == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selRemoveVOICEGateway" name="_selRemoveVOICEGateway"  value="true" onchange="accessList('RemoveVOICEGateway')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selRemoveVOICEGateway" name="_selRemoveVOICEGateway"  value="false" onchange="accessList('RemoveVOICEGateway')"></td>-->
                            <%}%>

                    </tr>
                    <tr>
                        <td>4 </td>
                        <td>EMAIL(SMTP) Gateway Configuration</td>
                        <%if (accessSettings.listEMAILGateway == true) { %>
                        <td> <input type="checkbox"  checked id="_selListEMAILGateway" name="_selListEMAILGateway"  value="true" onchange="accessList('ListEMAILGateway')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListEMAILGateway" name="_selListEMAILGateway"  value="false" onchange="accessList('ListEMAILGateway')"></td>
                            <%}%>
                            <%if (accessSettings.addEMAILGateway == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selAddEMAILGateway" name="_selAddEMAILGateway"  value="true" onchange="accessList('AddEMAILGateway')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selAddEMAILGateway" name="_selAddEMAILGateway"  value="false" onchange="accessList('AddEMAILGateway')"></td>-->
                            <%}%>
                            <%if (accessSettings.editEMAILGateway == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditEMAILGateway" name="_selEditEMAILGateway"  value="true" onchange="accessList('EditEMAILGateway')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditEMAILGateway" name="_selEditEMAILGateway"  value="false" onchange="accessList('EditEMAILGateway')"></td>
                            <%}%>
                            <%if (accessSettings.removeEMAILGateway == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selRemoveEMAILGateway" name="_selRemoveEMAILGateway"  value="true" onchange="accessList('RemoveEMAILGateway')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selRemoveEMAILGateway" name="_selRemoveEMAILGateway"  value="false" onchange="accessList('RemoveEMAILGateway')"></td>-->
                            <%}%>
                    </tr>
<!--                    <tr>
                        <td>5 </td>
                        <td>Billing Manager</td>
                        <%if (accessSettings.listBillingManager == true) { %>
                        <td> <input type="checkbox"  checked id="_selListBillingManager" name="_selListBillingManager"  value="true" onchange="accessList('ListBillingManager')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListBillingManager" name="_selListBillingManager"  value="false" onchange="accessList('ListBillingManager')"></td>
                            <%}%>
                            <%if (accessSettings.addBillingManager == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddBillingManager" name="_selAddBillingManager"  value="true" onchange="accessList('AddBillingManager')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddBillingManager" name="_selAddBillingManager"  value="false" onchange="accessList('AddBillingManager')"></td>
                            <%}%>
                            <%if (accessSettings.editBillingManager == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditBillingManagerGateway" name="_selEditBillingManagerGateway"  value="true" onchange="accessList('EditBillingManagerGateway')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditBillingManagerGateway" name="_selEditBillingManagerGateway"  value="false" onchange="accessList('EditBillingManagerGateway')"></td>
                            <%}%>
                            <%if (accessSettings.removeBillingManager == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveBillingManagerGateway" name="_selRemoveBillingManagerGateway"  value="true" onchange="accessList('RemoveBillingManagerGateway')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveBillingManagerGateway" name="_selRemoveBillingManagerGateway"  value="false" onchange="accessList('RemoveBillingManagerGateway')"></td>
                            <%}%>
                    </tr>-->
                    <tr>
                        <td>5 </td>
                        <td>Push Notification Gateway Configuration</td>
                        <%if (accessSettings.listPushGateway == true) { %>
                        <td> <input type="checkbox"  checked id="_selListPushGatewayGateway" name="_selListPushGatewayGateway"  value="true" onchange="accessList('ListPushGatewayGateway')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListPushGatewayGateway" name="_selListPushGatewayGateway"  value="false" onchange="accessList('ListPushGatewayGateway')"></td>
                            <%}%>
                            <%if (accessSettings.addPushGateway == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selAddPushGatewayGateway" name="_selAddPushGatewayGateway"  value="true" onchange="accessList('AddPushGatewayGateway')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selAddPushGatewayGateway" name="_selAddPushGatewayGateway"  value="false" onchange="accessList('AddPushGatewayGateway')"></td>-->
                            <%}%>
                            <%if (accessSettings.editPushGateway == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditPushGatewayGateway" name="_selEditPushGatewayGateway"  value="true" onchange="accessList('EditPushGatewayGateway')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditPushGatewayGateway" name="_selEditPushGatewayGateway"  value="false" onchange="accessList('EditPushGatewayGateway')"></td>
                            <%}%>
                            <%if (accessSettings.removePushGateway == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selRemovePushGatewayGateway" name="_selRemovePushGatewayGateway"  value="true" onchange="accessList('RemovePushGatewayGateway')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selRemovePushGatewayGateway" name="_selRemovePushGatewayGateway"  value="false" onchange="accessList('RemovePushGatewayGateway')"></td>-->
                            <%}%>
                    </tr>
                    <tr>
                        <td>6 </td>
                        <td>User Source Configuration</td>
                        <%if (accessSettings.listUserSourceSettings == true) { %>
                        <td> <input type="checkbox"  checked id="_selListUserSourceSettings" name="_selListUserSourceSettings"  value="true" onchange="accessList('ListUserSourceSettings')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListUserSourceSettings" name="_selListUserSourceSettings"  value="false" onchange="accessList('ListUserSourceSettings')"></td>
                            <%}%>
                            <%if (accessSettings.addUserSourceSettings == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selAddUserSourceSettings" name="_selAddUserSourceSettings"  value="true" onchange="accessList('AddUserSourceSettings')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selAddUserSourceSettings" name="_selAddUserSourceSettings"  value="false" onchange="accessList('AddUserSourceSettings')"></td>-->
                            <%}%>
                            <%if (accessSettings.editUserSourceSettings == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditUserSourceSettings" name="_selEditUserSourceSettings"  value="true" onchange="accessList('EditUserSourceSettings')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditUserSourceSettings" name="_selEditUserSourceSettings"  value="false" onchange="accessList('EditUserSourceSettings')"></td>
                            <%}%>
                            <%if (accessSettings.removeUserSourceSettings == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selRemoveUserSourceSettings" name="_selRemoveUserSourceSettings"  value="true" onchange="accessList('RemoveUserSourceSettings')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selRemoveUserSourceSettings" name="_selRemoveUserSourceSettings"  value="false" onchange="accessList('RemoveUserSourceSettings')"></td>-->
                            <%}%>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>Password Policy Configuration</td>
                        <%if (accessSettings.listPasswordPolicySettings == true) { %>
                        <td> <input type="checkbox"  checked id="_selListPasswordPolicySettings" name="_selListPasswordPolicySettings"  value="true" onchange="accessList('ListPasswordPolicySettings')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListPasswordPolicySettings" name="_selListPasswordPolicySettings"  value="false" onchange="accessList('ListPasswordPolicySettings')"></td>
                            <%}%>
                            <%if (accessSettings.addPasswordPolicySettings == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selAddPasswordPolicySettings" name="_selAddPasswordPolicySettings"  value="true" onchange="accessList('AddPasswordPolicySettings')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selAddPasswordPolicySettings" name="_selAddPasswordPolicySettings"  value="false" onchange="accessList('AddPasswordPolicySettings')"></td>-->
                            <%}%>
                            <%if (accessSettings.editPasswordPolicySettings == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditPasswordPolicySettings" name="_selEditPasswordPolicySettings"  value="true" onchange="accessList('EditPasswordPolicySettings')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditPasswordPolicySettings" name="_selEditPasswordPolicySettings"  value="false" onchange="accessList('EditPasswordPolicySettings')"></td>
                            <%}%>
                            <%if (accessSettings.removePasswordPolicySettings == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selRemovePasswordPolicySettings" name="_selRemovePasswordPolicySettings"  value="true" onchange="accessList('RemovePasswordPolicySettings')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selRemovePasswordPolicySettings" name="_selRemovePasswordPolicySettings"  value="false" onchange="accessList('RemovePasswordPolicySettings')"></td>-->
                            <%}%>
                    </tr>
                    <tr>
                        <td>8 </td>
                        <td>Channel Profile Configuration</td>
                        <%if (accessSettings.listChannelProfileSettings == true) { %>
                        <td> <input type="checkbox"  checked id="_selListChannelProfileSettings" name="_selListChannelProfileSettings"  value="true" onchange="accessList('ListChannelProfileSettings')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListChannelProfileSettings" name="_selListChannelProfileSettings"  value="false" onchange="accessList('ListChannelProfileSettings')"></td>
                            <%}%>
                            <%if (accessSettings.addChannelProfileSettings == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selAddChannelProfileSettings" name="_selAddChannelProfileSettings"  value="true" onchange="accessList('AddChannelProfileSettings')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selAddChannelProfileSettings" name="_selAddChannelProfileSettings"  value="false" onchange="accessList('AddChannelProfileSettings')"></td>-->
                            <%}%>
                            <%if (accessSettings.editChannelProfileSettings == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannelProfileSettings" name="_selEditChannelProfileSettings"  value="true" onchange="accessList('EditChannelProfileSettings')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannelProfileSettings" name="_selEditChannelProfileSettings"  value="false" onchange="accessList('EditChannelProfileSettings')"></td>
                            <%}%>
                            <%if (accessSettings.removeChannelProfileSettings == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selRemoveChannelProfileSettings" name="_selRemoveChannelProfileSettings"  value="true" onchange="accessList('RemoveChannelProfileSettings')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selRemoveChannelProfileSettings" name="_selRemoveChannelProfileSettings"  value="false" onchange="accessList('RemoveChannelProfileSettings')"></td>-->
                            <%}%>
                    </tr>
                    <tr>
                        <td>9 </td>
                        <td>One Time Password Token</td>
                        <%if (accessSettings.listOtpTokensSettings == true) { %>
                        <td> <input type="checkbox"  checked id="_selListOtpTokensSettings" name="_selListOtpTokensSettings"  value="true" onchange="accessList('ListOtpTokensSettings')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListOtpTokensSettings" name="_selListOtpTokensSettings"  value="false" onchange="accessList('ListOtpTokensSettings')"></td>
                            <%}%>
                            <%if (accessSettings.addOtpTokensSettings == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selAddOtpTokensSettings" name="_selAddOtpTokensSettings"  value="true" onchange="accessList('AddOtpTokensSettings')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selAddOtpTokensSettings" name="_selAddOtpTokensSettings"  value="false" onchange="accessList('AddOtpTokensSettings')"></td>-->
                            <%}%>
                            <%if (accessSettings.editOtpTokensSettings == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditOtpTokensSettings" name="_selEditOtpTokensSettings"  value="true" onchange="accessList('EditOtpTokensSettings')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditOtpTokensSettings" name="_selEditOtpTokensSettings"  value="false" onchange="accessList('EditOtpTokensSettings')"></td>
                            <%}%>
                            <%if (accessSettings.removeOtpTokensSettings == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selRemoveOtpTokensSettings" name="_selRemoveOtpTokensSettings"  value="true" onchange="accessList('RemoveOtpTokensSettings')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selRemoveOtpTokensSettings" name="_selRemoveOtpTokensSettings"  value="false" onchange="accessList('RemoveOtpTokensSettings')"></td>-->
                            <%}%>
                    </tr>
                    <tr>
                        <td>10 </td>
                        <td>Digital Certificate Authority Connector</td>
                        <%if (accessSettings.listCertificateSettings == true) { %>
                        <td> <input type="checkbox"  checked id="_selListCertificateSettings" name="_selListCertificateSettings"  value="true" onchange="accessList('ListCertificateSettings')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListCertificateSettings" name="_selListCertificateSettings"  value="false" onchange="accessList('ListCertificateSettings')"></td>
                            <%}%>
                            <%if (accessSettings.addCertificateSettings == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selAddCertificateSettings" name="_selAddCertificateSettings"  value="true" onchange="accessList('AddCertificateSettings')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selAddCertificateSettings" name="_selAddCertificateSettings"  value="false" onchange="accessList('AddCertificateSettings')"></td>-->
                            <%}%>
                            <%if (accessSettings.editCertificateSettings == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditCertificateSettings" name="_selEditCertificateSettings"  value="true" onchange="accessList('EditCertificateSettings')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditCertificateSettings" name="_selEditCertificateSettings"  value="false" onchange="accessList('EditCertificateSettings')"></td>
                            <%}%>
                            <%if (accessSettings.removeCertificateSettings == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selRemoveCertificateSettings" name="_selRemoveCertificateSettings"  value="true" onchange="accessList('RemoveCertificateSettings')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selRemoveCertificateSettings" name="_selRemoveCertificateSettings"  value="false" onchange="accessList('RemoveCertificateSettings')"></td>-->
                            <%}%>
                    </tr>
                    <tr>
                        <td>11 </td>
                        <td>Mobile Trust Settings</td>
                        <%if (accessSettings.listMobileTrustSettings == true) { %>
                        <td> <input type="checkbox"  checked id="_selListMobileTrustSettings" name="_selListMobileTrustSettings"  value="true" onchange="accessList('ListMobileTrustSettings')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListMobileTrustSettings" name="_selListMobileTrustSettings"  value="false" onchange="accessList('ListMobileTrustSettings')"></td>
                            <%}%>
                            <%if (accessSettings.addMobileTrustSettings == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selAddMobileTrustSettings" name="_selAddMobileTrustSettings"  value="true" onchange="accessList('AddMobileTrustSettings')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selAddMobileTrustSettings" name="_selAddMobileTrustSettings"  value="false" onchange="accessList('AddMobileTrustSettings')"></td>-->
                            <%}%>
                            <%if (accessSettings.editMobileTrustSettings == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditMobileTrustSettings" name="_selEditMobileTrustSettings"  value="true" onchange="accessList('EditMobileTrustSettings')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditMobileTrustSettings" name="_selEditMobileTrustSettings"  value="false" onchange="accessList('EditMobileTrustSettings')"></td>
                            <%}%>
                            <%if (accessSettings.removeMobileTrustSettings == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selRemoveMobileTrustSettings" name="_selRemoveMobileTrustSettings"  value="true" onchange="accessList('RemoveMobileTrustSettings')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selRemoveMobileTrustSettings" name="_selRemoveMobileTrustSettings"  value="false" onchange="accessList('RemoveMobileTrustSettings')"></td>-->
                            <%}%>
                    </tr>
                    <tr>
                        <td>12 </td>
                        <td>RADIUS Settings</td>
                        <%if (accessSettings.listRadiusConfigSettings == true) { %>
                        <td> <input type="checkbox"  checked id="_selListRadiusConfigSettings" name="_selListRadiusConfigSettings"  value="true" onchange="accessList('ListRadiusConfigSettings')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListRadiusConfigSettings" name="_selListRadiusConfigSettings"  value="false" onchange="accessList('ListRadiusConfigSettings')"></td>
                            <%}%>
                            <%if (accessSettings.addRadiusConfigSettings == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selAddRadiusConfigSettings" name="_selAddRadiusConfigSettings"  value="true" onchange="accessList('AddRadiusConfigSettings')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selAddRadiusConfigSettings" name="_selAddRadiusConfigSettings"  value="false" onchange="accessList('AddRadiusConfigSettings')"></td>-->
                            <%}%>
                            <%if (accessSettings.editRadiusConfigSettings == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditRadiusConfigSettings" name="_selEditRadiusConfigSettings"  value="true" onchange="accessList('EditRadiusConfigSettings')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditRadiusConfigSettings" name="_selEditRadiusConfigSettings"  value="false" onchange="accessList('EditRadiusConfigSettings')"></td>
                            <%}%>
                            <%if (accessSettings.removeRadiusConfigSettings == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selRemoveRadiusConfigSettings" name="_selRemoveRadiusConfigSettings"  value="true" onchange="accessList('RemoveRadiusConfigSettings')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selRemoveRadiusConfigSettings" name="_selRemoveRadiusConfigSettings"  value="false" onchange="accessList('RemoveRadiusConfigSettings')"></td>-->
                            <%}%>
                    </tr>
                    <tr>
                        <td>13 </td>
                        <td>Global Settings</td>
                        <%if (accessSettings.listGlobalSettings == true) { %>
                        <td> <input type="checkbox"  checked id="_selListGlobalSettings" name="_selListGlobalSettings"  value="true" onchange="accessList('ListGlobalSettings')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListGlobalSettings" name="_selListGlobalSettings"  value="false" onchange="accessList('ListGlobalSettings')"></td>
                            <%}%>
                            <%if (accessSettings.addGlobalSettings == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selAddGlobalSettings" name="_selAddGlobalSettings"  value="true" onchange="accessList('AddGlobalSettings')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selAddGlobalSettings" name="_selAddGlobalSettings"  value="false" onchange="accessList('AddGlobalSettings')"></td>-->
                            <%}%>
                            <%if (accessSettings.editGlobalSettings == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditGlobalSettings" name="_selEditGlobalSettings"  value="true" onchange="accessList('EditGlobalSettings')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditGlobalSettings" name="_selEditGlobalSettings"  value="false" onchange="accessList('EditGlobalSettings')"></td>
                            <%}%>
                            <%if (accessSettings.removeGlobalSettings == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selRemoveGlobalSettings" name="_selRemoveGlobalSettings"  value="true" onchange="accessList('RemoveGlobalSettings')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selRemoveGlobalSettings" name="_selRemoveGlobalSettings"  value="false" onchange="accessList('RemoveGlobalSettings')"></td>-->
                            <%}%>
                    </tr>
                    <tr>
                        <td>14</td>
                        <td>Secure Phrase Configuration (Dynamic Image)</td>
                        <%if (accessSettings.listImageSettings == true) { %>
                        <td> <input type="checkbox"  checked id="_selListImageSettings" name="_selListImageSettings"  value="true" onchange="accessList('ListImageSettings')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListImageSettings" name="_selListImageSettings"  value="false" onchange="accessList('ListImageSettings')"></td>
                            <%}%>
                            <%if (accessSettings.addImageConfigSettings == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selAddImageConfigSettings" name="_selImageConfigSettings"  value="true" onchange="accessList('AddImageConfigSettings')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selAddImageConfigSettings" name="_selAddEpinConfigSettings"  value="false" onchange="accessList('AddImageConfigSettings')"></td>-->
                            <%}%>
                            <%if (accessSettings.editImageConfigSettings == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditImageConfigSettings" name="_selEditImageConfigSettings"  value="true" onchange="accessList('EditImageConfigSettings')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditImageConfigSettings" name="_selEditImageConfigSettings"  value="false" onchange="accessList('EditImageConfigSettings')"></td>
                            <%}%>
                            <%if (accessSettings.removeImageConfigSettings == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selRemoveImageConfigSettings" name="_selRemoveImageConfigSettings"  value="true" onchange="accessList('RemoveImageConfigSettings')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selRemoveImageConfigSettings" name="_selRemoveImageConfigSettings"  value="false" onchange="accessList('RemoveImageConfigSettings')"></td>-->
                            <%}%>
                    </tr>
                    <% if ( 1 == 2) { %>
                    <tr>
                        <td>15 </td>
                        <td>EPIN Settings</td>
                        <%if (accessSettings.listEpinConfigSettings == true) { %>
                        <td> <input type="checkbox"  checked id="_selListEpinConfigSettings" name="_selListEpinConfigSettings"  value="true" onchange="accessList('ListEpinConfigSettings')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListEpinConfigSettings" name="_selListEpinConfigSettings"  value="false" onchange="accessList('ListEpinConfigSettings')"></td>
                            <%}%>
                            <%if (accessSettings.addEpinConfigSettings == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selAddEpinConfigSettings" name="_selAddEpinConfigSettings"  value="true" onchange="accessList('AddEpinConfigSettings')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selAddEpinConfigSettings" name="_selAddEpinConfigSettings"  value="false" onchange="accessList('AddEpinConfigSettings')"></td>-->
                            <%}%>
                            <%if (accessSettings.editEpinConfigSettings == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditEpinConfigSettings" name="_selEditEpinConfigSettings"  value="true" onchange="accessList('EditEpinConfigSettings')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditEpinConfigSettings" name="_selEditEpinConfigSettings"  value="false" onchange="accessList('EditEpinConfigSettings')"></td>
                            <%}%>
                            <%if (accessSettings.removeEpinConfigSettings == true) { %>
                        <!--<td> <input type="checkbox"  checked id="_selRemoveEpinConfigSettings" name="_selRemoveEpinConfigSettings"  value="true" onchange="accessList('RemoveEpinConfigSettings')"></td>-->
                            <%} else {%>
                        <!--<td> <input type="checkbox"   id="_selRemoveEpinConfigSettings" name="_selRemoveEpinConfigSettings"  value="false" onchange="accessList('RemoveEpinConfigSettings')"></td>-->
                            <%}%>
                    </tr>
                    <%}%>
<!--                    <tr>
                        <td>16 </td>
                        <td>FAX Gateway Configuration</td>
                        <%if (accessSettings.listFAXGateway == true) { %>
                        <td> <input type="checkbox"  checked id="_selListFax" name="_selListFax"  value="true" onchange="accessList('ListEMAILGateway')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selListFax" name="_selListFax"  value="false" onchange="accessList('ListEMAILGateway')"></td>
                            <%}%>
                            <%if (accessSettings.addFAXGateway == true) { %>
                       <td> <input type="checkbox"  checked id="_selAddFax" name="_selAddFax"  value="true" onchange="accessList('AddEMAILGateway')"></td>
                            <%} else {%>
                       <td> <input type="checkbox"   id="_selAddFax" name="_selAddFax"  value="false" onchange="accessList('AddEMAILGateway')"></td>
                            <%}%>
                            <%if (accessSettings.editFAXGateway == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditFax" name="_selEditFax"  value="true" onchange="accessList('EditEMAILGateway')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditFax" name="_selEditFax"  value="false" onchange="accessList('EditEMAILGateway')"></td>
                            <%}%>
                            <%if (accessSettings.removeFAXGateway == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveFax" name="_selRemoveFax"  value="true" onchange="accessList('RemoveEMAILGateway')"></td>
                            <%} else {%>
                       <td> <input type="checkbox"   id="_selRemoveFax" name="_selRemoveFax"  value="false" onchange="accessList('RemoveEMAILGateway')"></td>
                            <%}%>
                    </tr>-->
                    
                    <tr>
                        <td> </td>
                        <td></td>
                        <%if (oprObj.getRoleid() != 1) {%>
                        <%if (accessObj != null && (accessObj.editaccessMatrix == true)) {%>
                        <td><p><button class="btn btn-primary" onclick="saveMatrix('<%=roleId%>','<%=OperatorsManagement.MatrixSystemConfigurations%>','<%=_operatorID%>')">Save&raquo;</button></p></td>
                        <%}else{%>
                        <td><p><button class="btn btn-primary" onclick="InvalidRequest('rolereports')">Save&raquo;</button></p></td>
                        <%}}else{%>
                        <td><p><button class="btn btn-primary" onclick="saveMatrix('<%=roleId%>','<%=OperatorsManagement.MatrixSystemConfigurations%>','<%=_operatorID%>')">Save&raquo;</button></p></td>
                        <%}%>
<!--                        <td><p><button class="btn btn-primary" onclick="saveMatrix('<%=roleId%>','<%=OperatorsManagement.MatrixSystemConfigurations%>','<%=_operatorID%>')">Save&raquo;</button></p></td>-->
                        <td> </td>
                        
                        <td> </td>
                    </tr>
                </table>
            </div>

<!--            <div class="tab-pane" id="SecurityManagment">
                <table class="table table-bordered  table-striped" style="width: 600px;">

                    <tr>
                        <td>Sr. No. </td>
                        <td>Type</td>
                        <td>List</td>
                        <td>Add</td>
                        <td>Edit</td>
                        <td>Delete</td>
                    </tr>
                    <tr>
                        <td>1 </td>
                        <td>Out of Band One Time Password Tokens</td>
                        <td></td><td></td><td></td><td></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>SMS Token</td>
                        <%if (accessSettings.listOOBSMSToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selOOBSMSTokenLIST" name="_selOOBSMSTokenLIST"  value="true" onchange="accessList('OOBSMSTokenLIST')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selOOBSMSTokenLIST" name="_selOOBSMSTokenLIST"  value="false" onchange="accessList('OOBSMSTokenLIST')"></td>
                            <%}%>
                            <%if (accessSettings.addOOBSMSToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddOOBSMSToken" name="_selAddOOBSMSToken"  value="true" onchange="accessList('AddOOBSMSToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddOOBSMSToken" name="_selAddOOBSMSToken"  value="false" onchange="accessList('AddOOBSMSToken')"></td>
                            <%}%>
                            <%if (accessSettings.editOOBSMSToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditOOBSMSToken" name="_selEditOOBSMSToken"  value="true" onchange="accessList('EditOOBSMSToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditOOBSMSToken" name="_selEditOOBSMSToken"  value="false" onchange="accessList('EditOOBSMSToken')"></td>
                            <%}%>
                            <%if (accessSettings.removeOOBSMSToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveOOBSMSToken" name="_selRemoveOOBSMSToken"  value="true" onchange="accessList('RemoveOOBSMSToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveOOBSMSToken" name="_selRemoveOOBSMSToken"  value="false" onchange="accessList('RemoveOOBSMSToken')"></td>
                            <%}%>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>VOICE Token</td>
                        <%if (accessSettings.listOOBVOICEToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selOOBVOICETokenLIST" name="_selOOBVOICETokenLIST"  value="true" onchange="accessList('OOBVOICETokenLIST')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selOOBVOICETokenLIST" name="_selOOBVOICETokenLIST"  value="false" onchange="accessList('OOBVOICETokenLIST')"></td>
                            <%}%>
                            <%if (accessSettings.addOOBVOICEToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddOOBVOICEToken" name="_selAddOOBVOICEToken"  value="true" onchange="accessList('AddOOBVOICEToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddOOBVOICEToken" name="_selAddOOBVOICEToken"  value="false" onchange="accessList('AddOOBVOICEToken')"></td>
                            <%}%>
                            <%if (accessSettings.editOOBVOICEToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditOOBVOICEToken" name="_selEditOOBVOICEToken"  value="true" onchange="accessList('EditOOBVOICEToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditOOBVOICEToken" name="_selEditOOBVOICEToken"  value="false" onchange="accessList('EditOOBVOICEToken')"></td>
                            <%}%>
                            <%if (accessSettings.removeOOBVOICEToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveOOBVOICEToken" name="_selRemoveOOBVOICEToken"  value="true" onchange="accessList('RemoveOOBVOICEToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveOOBVOICEToken" name="_selRemoveOOBVOICEToken"  value="false" onchange="accessList('RemoveOOBVOICEToken')"></td>
                            <%}%>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>USSD Token</td>
                        <%if (accessSettings.listOOBUSSDToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selOOBUSSDTokenLIST" name="_selOOBUSSDTokenLIST"  value="true" onchange="accessList('OOBUSSDTokenLIST')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selOOBUSSDTokenLIST" name="_selOOBUSSDTokenLIST"  value="false" onchange="accessList('OOBUSSDTokenLIST')"></td>
                            <%}%>
                            <%if (accessSettings.addOOBUSSDToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddOOBUSSDToken" name="_selAddOOBUSSDToken"  value="true" onchange="accessList('AddOOBUSSDToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddOOBUSSDToken" name="_selAddOOBUSSDToken"  value="false" onchange="accessList('AddOOBUSSDToken')"></td>
                            <%}%>
                            <%if (accessSettings.editOOBUSSDToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditOOBUSSDToken" name="_selEditOOBUSSDToken"  value="true" onchange="accessList('EditOOBUSSDToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditOOBUSSDToken" name="_selEditOOBUSSDToken"  value="false" onchange="accessList('EditOOBUSSDToken')"></td>
                            <%}%>
                            <%if (accessSettings.removeOOBUSSDToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveOOBUSSDToken" name="_selRemoveOOBUSSDToken"  value="true" onchange="accessList('RemoveOOBUSSDToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveOOBUSSDToken" name="_selRemoveOOBUSSDToken"  value="false" onchange="accessList('RemoveOOBUSSDToken')"></td>
                            <%}%>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>EMAIL Token</td>
                        <%if (accessSettings.listOOBEMAILToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selOOBEMAILTokenLIST" name="_selOOBEMAILTokenLIST"  value="true" onchange="accessList('OOBEMAILTokenLIST')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selOOBEMAILTokenLIST" name="_selOOBEMAILTokenLIST"  value="false" onchange="accessList('OOBEMAILTokenLIST')"></td>
                            <%}%>
                            <%if (accessSettings.addOOBEMAILToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddOOBEMAILToken" name="_selAddOOBEMAILToken"  value="true" onchange="accessList('AddOOBEMAILToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddOOBEMAILToken" name="_selAddOOBEMAILToken"  value="false" onchange="accessList('AddOOBEMAILToken')"></td>
                            <%}%>
                            <%if (accessSettings.editOOBEMAILToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditOOBEMAILToken" name="_selEditOOBEMAILToken"  value="true" onchange="accessList('EditOOBEMAILToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditOOBEMAILToken" name="_selEditOOBEMAILToken"  value="false" onchange="accessList('EditOOBEMAILToken')"></td>
                            <%}%>
                            <%if (accessSettings.removeOOBEMAILToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveOOBEMAILToken" name="_selRemoveOOBEMAILToken"  value="true" onchange="accessList('RemoveOOBEMAILToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveOOBEMAILToken" name="_selRemoveOOBEMAILToken"  value="false" onchange="accessList('RemoveOOBEMAILToken')"></td>
                            <%}%>
                    </tr>
                    
                    <tr>
                        <td>2 </td>
                        <td>Software One Time Password Tokens</td>
                        <td></td><td></td><td></td><td></td>
                    </tr>
                     <tr>
                        <td> </td>
                        <td>Mobile Token</td>
                        <%if (accessSettings.listSWMOBILEToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selSWMOBILETokenLIST" name="_selSWMOBILETokenLIST"  value="true" onchange="accessList('SWMOBILETokenLIST')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selSWMOBILETokenLIST" name="_selSWMOBILETokenLIST"  value="false" onchange="accessList('SWMOBILETokenLIST')"></td>
                            <%}%>
                            <%if (accessSettings.addSWMOBILEToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddSWMOBILEToken" name="_selAddSWMOBILEToken"  value="true" onchange="accessList('AddSWMOBILEToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddSWMOBILEToken" name="_selAddSWMOBILEToken"  value="false" onchange="accessList('AddSWMOBILEToken')"></td>
                            <%}%>
                            <%if (accessSettings.editSWMOBILEToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditSWMOBILEToken" name="_selEditSWMOBILEToken"  value="true" onchange="accessList('EditSWMOBILEToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditSWMOBILEToken" name="_selEditSWMOBILEToken"  value="false" onchange="accessList('EditSWMOBILEToken')"></td>
                            <%}%>
                            <%if (accessSettings.removeSWMOBILEToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveSWMOBILEToken" name="_selRemoveSWMOBILEToken"  value="true" onchange="accessList('RemoveSWMOBILEToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveSWMOBILEToken" name="_selRemoveSWMOBILEToken"  value="false" onchange="accessList('RemoveSWMOBILEToken')"></td>
                            <%}%>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>Web Token</td>
                        <%if (accessSettings.listSWWEBToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selSWWEBTokenLIST" name="_selSWWEBTokenLIST"  value="true" onchange="accessList('SWWEBTokenLIST')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selSWWEBTokenLIST" name="_selSWWEBTokenLIST"  value="false" onchange="accessList('SWWEBTokenLIST')"></td>
                            <%}%>
                            <%if (accessSettings.addSWWEBToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddSWWEBToken" name="_selAddSWWEBToken"  value="true" onchange="accessList('AddSWWEBToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddSWWEBToken" name="_selAddSWWEBToken"  value="false" onchange="accessList('AddSWWEBToken')"></td>
                            <%}%>
                            <%if (accessSettings.editSWWEBToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditSWWEBToken" name="_selEditSWWEBToken"  value="true" onchange="accessList('EditSWWEBToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditSWWEBToken" name="_selEditSWWEBToken"  value="false" onchange="accessList('EditSWWEBToken')"></td>
                            <%}%>
                            <%if (accessSettings.removeSWWEBToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveSWWEBToken" name="_selRemoveSWWEBToken"  value="true" onchange="accessList('RemoveSWWEBToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveSWWEBToken" name="_selRemoveSWWEBToken"  value="false" onchange="accessList('RemoveSWWEBToken')"></td>
                            <%}%>
                    </tr>

                    <tr>
                        <td> </td>
                        <td>WEB Token</td>
                        <%if (accessSettings.listSWWEBToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selSWWEBToken" name="_selSWWEBToken"  value="true" onchange="accessList('SWWEBToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selSWWEBToken" name="_selSWWEBToken"  value="false" onchange="accessList('SWWEBToken')"></td>
                            <%}%>
                            <%if (accessSettings.addSWWEBToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddSWWEBToken" name="_selAddSWWEBToken"  value="true" onchange="accessList('AddSWWEBToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddSWWEBToken" name="_selAddSWWEBToken"  value="false" onchange="accessList('AddSWWEBToken')"></td>
                            <%}%>
                            <%if (accessSettings.editSWWEBToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditSWWEBToken" name="_selEditSWWEBToken"  value="true" onchange="accessList('EditSWWEBToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditSWWEBToken" name="_selEditSWWEBToken"  value="false" onchange="accessList('EditSWWEBToken')"></td>
                            <%}%>
                            <%if (accessSettings.removeSWWEBToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveSWWEBToken" name="_selRemoveSWWEBToken"  value="true" onchange="accessList('RemoveSWWEBToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveSWWEBToken" name="_selRemoveSWWEBToken"  value="false" onchange="accessList('RemoveSWWEBToken')"></td>
                            <%}%>
                    </tr>
                    <tr>
                        <td> </td>
                        <td></td>
                        <%if (accessSettings.listSWPCToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selSWPCTokenLIST" name="_selSWPCTokenLIST"  value="true" onchange="accessList('SWPCTokenLIST')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selSWPCTokenLIST" name="_selSWPCTokenLIST"  value="false" onchange="accessList('SWPCTokenLIST')"></td>
                            <%}%>
                            <%if (accessSettings.addSWPCToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddSWPCToken" name="_selAddSWPCToken"  value="true" onchange="accessList('AddSWPCToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddSWPCToken" name="_selAddSWPCToken"  value="false" onchange="accessList('AddSWPCToken')"></td>
                            <%}%>
                            <%if (accessSettings.editSWPCToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditSWPCToken" name="_selEditSWPCToken"  value="true" onchange="accessList('EditSWPCToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditSWPCToken" name="_selEditSWPCToken"  value="false" onchange="accessList('EditSWPCToken')"></td>
                            <%}%>
                            <%if (accessSettings.removeSWPCToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveSWPCToken" name="_selRemoveSWPCToken"  value="true" onchange="accessList('RemoveSWPCToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveSWPCToken" name="_selRemoveSWPCToken"  value="false" onchange="accessList('RemoveSWPCToken')"></td>
                            <%}%>
                    </tr>
                    <tr>
                        <td>3 </td>
                        <td>Hardware One Time Password Tokens</td>
                        <td></td><td></td><td></td><td></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>Hardware Token</td>
                        <%if (accessSettings.listHWOTPToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selHWOTPTokenLIST" name="_selHWOTPTokenLIST"  value="true" onchange="accessList('HWOTPTokenLIST')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selHWOTPTokenLIST" name="_selHWOTPTokenLIST"  value="false" onchange="accessList('HWOTPTokenLIST')"></td>
                            <%}%>
                            <%if (accessSettings.addHWOTPToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddHWOTPToken" name="_selAddHWOTPToken"  value="true" onchange="accessList('AddHWOTPToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddHWOTPToken" name="_selAddHWOTPToken"  value="false" onchange="accessList('AddHWOTPToken')"></td>
                            <%}%>
                            <%if (accessSettings.editHWOTPToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditHWOTPToken" name="_selEditHWOTPToken"  value="true" onchange="accessList('EditHWOTPToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditHWOTPToken" name="_selEditHWOTPToken"  value="false" onchange="accessList('EditHWOTPToken')"></td>
                            <%}%>
                            <%if (accessSettings.removeHWOTPToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveHWOTPToken" name="_selRemoveHWOTPToken"  value="true" onchange="accessList('RemoveHWOTPToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveHWOTPToken" name="_selRemoveHWOTPToken"  value="false" onchange="accessList('RemoveHWOTPToken')"></td>
                            <%}%>
                    </tr>
                    <tr>
                        <td>4 </td>
                        <td>PKI Tokens (for Digital Certificate)</td>
                        <td></td><td></td><td></td><td></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>Certificate</td>
                        <%if (accessSettings.listCerticate == true) { %>
                        <td> <input type="checkbox"  checked id="_selCerticateLIST" name="_selCerticateLIST"  value="true" onchange="accessList('CerticateLIST')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selCerticateLIST" name="_selCerticateLIST"  value="false" onchange="accessList('CerticateLIST')"></td>
                            <%}%>
                            <%if (accessSettings.addCerticate == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddCerticate" name="_selAddCerticate"  value="true" onchange="accessList('AddCerticate')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddCerticate" name="_selAddCerticate"  value="false" onchange="accessList('AddCerticate')"></td>
                            <%}%>
                            <%if (accessSettings.editCerticate == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditCerticate" name="_selEditCerticate"  value="true" onchange="accessList('EditCerticate')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditCerticate" name="_selEditCerticate"  value="false" onchange="accessList('EditCerticate')"></td>
                            <%}%>
                            <%if (accessSettings.removeCerticate == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveCerticate" name="_selRemoveCerticate"  value="true" onchange="accessList('RemoveCerticate')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveCerticate" name="_selRemoveCerticate"  value="false" onchange="accessList('RemoveCerticate')"></td>
                            <%}%>
                    </tr>

                    <tr>
                        <td> </td>
                        <td>Hardware PKI</td>
                        <%if (accessSettings.listHWPKIToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selHWPKITokenLIST" name="_selHWPKITokenLIST"  value="true" onchange="accessList('HWPKITokenLIST')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selHWPKITokenLIST" name="_selHWPKITokenLIST"  value="false" onchange="accessList('HWPKITokenLIST')"></td>
                            <%}%>
                            <%if (accessSettings.addHWPKIToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddHWPKIToken" name="_selAddHWPKIToken"  value="true" onchange="accessList('AddHWPKIToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddHWPKIToken" name="_selAddHWPKIToken"  value="false" onchange="accessList('AddHWPKIToken')"></td>
                            <%}%>
                            <%if (accessSettings.editHWPKIToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditHWPKIToken" name="_selEditHWPKIToken"  value="true" onchange="accessList('EditHWPKIToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditHWPKIToken" name="_selEditHWPKIToken"  value="false" onchange="accessList('EditHWPKIToken')"></td>
                            <%}%>
                            <%if (accessSettings.removeHWPKIToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveHWPKIToken" name="_selRemoveHWPKIToken"  value="true" onchange="accessList('RemoveHWPKIToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveHWPKIToken" name="_selRemoveHWPKIToken"  value="false" onchange="accessList('RemoveHWPKIToken')"></td>
                            <%}%>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>Software PKI</td>
                        <%if (accessSettings.listSWPKIToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selSWPKITokenLIST" name="_selSWPKITokenLIST"  value="true" onchange="accessList('SWPKITokenLIST')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selSWPKITokenLIST" name="_selSWPKITokenLIST"  value="false" onchange="accessList('SWPKITokenLIST')"></td>
                            <%}%>
                            <%if (accessSettings.addSWPKIToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddSWPKIToken" name="_selAddSWPKIToken"  value="true" onchange="accessList('AddSWPKIToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddSWPKIToken" name="_selAddSWPKIToken"  value="false" onchange="accessList('AddSWPKIToken')"></td>
                            <%}%>
                            <%if (accessSettings.editSWPKIToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditSWPKIToken" name="_selEditSWPKIToken"  value="true" onchange="accessList('EditSWPKIToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditSWPKIToken" name="_selEditSWPKIToken"  value="false" onchange="accessList('EditSWPKIToken')"></td>
                            <%}%>
                            <%if (accessSettings.removeSWPKIToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoveSWPKIToken" name="_selRemoveSWPKIToken"  value="true" onchange="accessList('RemoveSWPKIToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoveSWPKIToken" name="_selRemoveSWPKIToken"  value="false" onchange="accessList('RemoveSWPKIToken')"></td>
                            <%}%>
                    </tr>


                    <tr>
                        <td> </td>
                        <td><p><button class="btn btn-primary" onclick="saveMatrix('<%=roleId%>','<%=OperatorsManagement.MatrixSystemSecurity%>','<%=_operatorID%>')">Save&raquo;</button></p></td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                    </tr>
                </table>


            </div>-->
<!--Shailendra-->
 <div class="tab-pane" id="SecurityManagment">
                <table class="table table-bordered  table-striped" style="width: 600px;">

                    <tr>
                        <td>Sr. No. </td>
                        <td>Type</td>
                        <td>Yes</td>
                        <!--<td>No</td>-->
                    </tr>
                    <tr>
                        <td>1 </td>
                        <td>Out of Band One Time Password Tokens</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>Assign Token</td>
                        <%if (accessSettings.addoobAssignToken == true) { %>
                        <td> <input type="checkbox"  checked id="_seladdOOBAssignToken" name="_seladdOOBAssignToken"  value="true" onchange="accessList('addOOBAssignToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seladdOOBAssignToken" name="_seladdOOBAssignToken"  value="false" onchange="accessList('addOOBAssignToken')"></td>
                            <%}%>
                    </tr>
                    <tr>
                       <td> </td>
                        <td>Change Token type</td>
                        <%if (accessSettings.addoobChangetokenType == true) { %>
                        <td> <input type="checkbox"  checked id="_seladdChangeTokenType" name="_seladdChangeTokenType"  value="true" onchange="accessList('addChangeTokenType')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seladdChangeTokenType" name="_seladdChangeTokenType"  value="false" onchange="accessList('addChangeTokenType')"></td>
                            <%}%>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>Mark As Active</td>
                        <%if (accessSettings.addoobMarkAsActive == true) { %>
                        <td> <input type="checkbox"  checked id="_seladdOOBmarkActive" name="_seladdOOBmarkActive"  value="true" onchange="accessList('addOOBmarkActive')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seladdOOBmarkActive" name="_seladdOOBmarkActive"  value="false" onchange="accessList('addOOBmarkActive')"></td>
                            <%}%>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>Mark As Suspended</td>
                        <%if (accessSettings.addoobMarkAsSuspended == true) { %>
                        <td> <input type="checkbox"  checked id="_selOOBmarksuspend" name="_selOOBmarksuspend"  value="true" onchange="accessList('OOBmarksuspend')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selOOBmarksuspend" name="_selOOBmarksuspend"  value="false" onchange="accessList('OOBmarksuspend')"></td>
                            <%}%>
                    </tr>
<!--                    <tr>
                        <td> </td>
                        <td>Verify one time password</td>
                        <% //if (accessSettings.addoobVerifyOTP == true) { %>
                        <td> <input type="checkbox"  checked id="_selOOBverifyOtp" name="_selOOBverifyOtp"  value="true" onchange="accessList('OOBverifyOtp')"></td>
                            <% //} else {%>
                        <td> <input type="checkbox"   id="_selOOBverifyOtp" name="_selOOBverifyOtp"  value="false" onchange="accessList('OOBverifyOtp')"></td>
                            <% //}%>
                            <% //if (accessSettings.removeoobVerifyOTP == true) { %>
                        <td> <input type="checkbox"  checked id="_selremoveVerifyOtp" name="_selremoveVerifyOtp"  value="true" onchange="accessList('removeVerifyOtp')"></td>
                            <% //} else {%>
                        <td> <input type="checkbox"   id="_selremoveVerifyOtp" name="_selremoveVerifyOtp"  value="false" onchange="accessList('removeVerifyOtp')"></td>
                            <% //}%>
                    </tr>-->
                    <tr>
                        <td> </td>
                        <td>Un-assign Token</td>
                        <%if (accessSettings.addoobUnAssignToken == true) { %>
                        <td> <input type="checkbox"  checked id="_selOOBunassignToken" name="_selOOBunassignToken"  value="true" onchange="accessList('OOBunassignToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selOOBunassignToken" name="_selOOBunassignToken"  value="false" onchange="accessList('OOBunassignToken')"></td>
                            <%}%>
                    </tr>
                    
                    <tr>
                        <td>2 </td>
                        <td>Software One Time Password Tokens</td>
                        <td></td>
                        <!--<td></td>-->
                    </tr>
                     <tr>
                        <td> </td>
                        <td>Assign Token</td>
                        <%if (accessSettings.addsoftAssignToken == true) { %>
                        <td> <input type="checkbox"  checked id="_seladdSoftAssignToken" name="_seladdSoftAssignToken"  value="true" onchange="accessList('addSoftAssignToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seladdSoftAssignToken" name="_seladdSoftAssignToken"  value="false" onchange="accessList('addSoftAssignToken')"></td>
                            <%}%>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>Re(Send) Registration code</td>
                        <%if (accessSettings.addsoftResendActCode == true) { %>
                        <td> <input type="checkbox"  checked id="_seladdSoftResendCode" name="_seladdSoftResendCode"  value="true" onchange="accessList('addSoftResendCode')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seladdSoftResendCode" name="_seladdSoftResendCode"  value="false" onchange="accessList('addSoftResendCode')"></td>
                            <%}%>
                    </tr>

                    <tr>
                        <td> </td>
                        <td>Mark As Active</td>
                        <%if (accessSettings.addsoftMarkAsActive == true) { %>
                        <td> <input type="checkbox"  checked id="_seladdSWmarkActive" name="_seladdSWmarkActive"  value="true" onchange="accessList('addSWmarkActive')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seladdSWmarkActive" name="_seladdSWmarkActive"  value="false" onchange="accessList('addSWmarkActive')"></td>
                            <%}%>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>Mark As Suspended</td>
                        <%if (accessSettings.addsoftMarkSuspende == true) { %>
                        <td> <input type="checkbox"  checked id="_seladdSoftMarkActive" name="_seladdSoftMarkActive"  value="true" onchange="accessList('addSoftMarkActive')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seladdSoftMarkActive" name="_seladdSoftMarkActive"  value="false" onchange="accessList('addSoftMarkActive')"></td>
                            <%}%>
                    </tr>
<!--                    <tr>
                        <td> </td>
                        <td>Verify one time password</td>
                        <% //if (accessSettings.addsoftVOneTimePass == true) { %>
                        <td> <input type="checkbox"  checked id="_seladdSoftOTP" name="_seladdSoftOTP"  value="true" onchange="accessList('addSoftOTP')"></td>
                            <% //} else {%>
                        <td> <input type="checkbox"   id="_seladdSoftOTP" name="_seladdSoftOTP"  value="false" onchange="accessList('addSoftOTP')"></td>
                            <% //}%>
                            <% //if (accessSettings.remsoftVOneTimePass == true) { %>
                        <td> <input type="checkbox"  checked id="_selremoveSoftOTP" name="_selremoveSoftOTP"  value="true" onchange="accessList('removeSoftOTP')"></td>
                            <% //} else {%>
                        <td> <input type="checkbox"   id="_selremoveSoftOTP" name="_selremoveSoftOTP"  value="false" onchange="accessList('removeSoftOTP')"></td>
                            <% //}%>
                    </tr>-->
                    <tr>
                        <td> </td>
                        <td>Un-assign Token</td>
                        <%if (accessSettings.addsoftUnAssignToken == true) { %>
                        <td> <input type="checkbox"  checked id="_seladdSWUnAssignTok" name="_seladdSWUnAssignTok"  value="true" onchange="accessList('addSWUnAssignTok')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seladdSWUnAssignTok" name="_seladdSWUnAssignTok"  value="false" onchange="accessList('addSWUnAssignTok')"></td>
                            <%}%>
                    </tr>
                    
                    <tr>
                        <td>3 </td>
                        <td>Hardware One Time Password Tokens</td>
                        <td></td>
                        <!--<td></td>-->
                    </tr>
                    <tr>
                        <td> </td>
                        <td>Assign Token</td>
                        <%if (accessSettings.addhardAssignToken == true) { %>
                        <td> <input type="checkbox"  checked id="_seladdHassignToken" name="_seladdHassignToken"  value="true" onchange="accessList('addHassignToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seladdHassignToken" name="_seladdHassignToken"  value="false" onchange="accessList('addHassignToken')"></td>
                            <%}%>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>Mark As Active</td>
                        <%if (accessSettings.addhardMarkAsActive == true) { %>
                        <td> <input type="checkbox"  checked id="_selHWmarkActive" name="_selHWmarkActive"  value="true" onchange="accessList('HWmarkActive')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selHWmarkActive" name="_selHWmarkActive"  value="false" onchange="accessList('HWmarkActive')"></td>
                            <%}%>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>Mark As Suspended</td>
                        <%if (accessSettings.addHWMarkAsSuspended == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddHWsuspend" name="_selAddHWsuspend"  value="true" onchange="accessList('AddHWsuspend')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selAddHWsuspend" name="_selAddHWsuspend"  value="false" onchange="accessList('AddHWsuspend')"></td>
                            <%}%>
                    </tr>
<!--                    <tr>
                        <td> </td>
                        <td>Verify One Time password</td>
                        <% //if (accessSettings.addhardVerifyOTP == true) { %>
                        <td> <input type="checkbox"  checked id="_selAddHWverOTP" name="_selAddHWverOTP"  value="true" onchange="accessList('AddHWverOTP')"></td>
                            <% //} else {%>
                        <td> <input type="checkbox"   id="_selAddHWverOTP" name="_selAddHWverOTP"  value="false" onchange="accessList('AddHWverOTP')"></td>
                            <% //}%>
                            <% //if (accessSettings.remhardVerifyOTP == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemHWverOTP" name="_selRemHWverOTP"  value="true" onchange="accessList('RemHWverOTP')"></td>
                            <% //} else {%>
                        <td> <input type="checkbox"   id="_selRemHWverOTP" name="_selRemHWverOTP"  value="false" onchange="accessList('RemHWverOTP')"></td>
                            <% //}%>
                    </tr>-->
                    <tr>
                        <td> </td>
                        <td>Resync Token</td>
                        <%if (accessSettings.addHWresyncToken == true) { %>
                        <td> <input type="checkbox"  checked id="_seladdHWrec" name="_seladdHWrec"  value="true" onchange="accessList('addHWrec')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seladdHWrec" name="_seladdHWrec"  value="false" onchange="accessList('addHWrec')"></td>
                            <%}%>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>Replace Token</td>
                        <%if (accessSettings.addHWReplaceToken == true) { %>
                        <td> <input type="checkbox"  checked id="_seladdHWrepToken" name="_seladdHWrepToken"  value="true" onchange="accessList('addHWrepToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seladdHWrepToken" name="_seladdHWrepToken"  value="false" onchange="accessList('addHWrepToken')"></td>
                            <%}%>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>PUK Code</td>
                        <%if (accessSettings.addHWPukCode == true) { %>
                        <td> <input type="checkbox"  checked id="_seladdHWpukToken" name="_seladdHWpukToken"  value="true" onchange="accessList('addHWpukToken')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seladdHWpukToken" name="_seladdHWpukToken"  value="false" onchange="accessList('addHWpukToken')"></td>
                            <%}%>
                </tr>
                    <tr>
                        <td> </td>
                        <td>Mark As lost</td>
                        <%if (accessSettings.addHWMarkAsLost == true) { %>
                        <td> <input type="checkbox"  checked id="_addHWmarkLost" name="_addHWmarkLost"  value="true" onchange="accessList('addHWmarkLost')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seladdHWmarkLost" name="_seladdHWmarkLost"  value="false" onchange="accessList('addHWmarkLost')"></td>
                            <%}%>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>Un-assign Token</td>
                        <%if (accessSettings.addHwUnAssignToken == true) { %>
                        <td> <input type="checkbox"  checked id="_seladdHWunassign" name="_seladdHWunassign"  value="true" onchange="accessList('addHWunassign')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seladdHWunassign" name="_seladdHWunassign"  value="false" onchange="accessList('addHWunassign')"></td>
                            <%}%>
                    </tr>
                    <tr>
                        <td> </td>
                        <%if (oprObj.getRoleid() != 1) {%>
                        <%if (accessObj != null && (accessObj.editaccessMatrix == true)) {%>
                        <td><p><button class="btn btn-primary" onclick="saveMatrix('<%=roleId%>','<%=OperatorsManagement.MatrixSystemSecurity%>','<%=_operatorID%>')">Save&raquo;</button></p></td>
                        <%}else{%>
                        <td><p><button class="btn btn-primary" onclick="InvalidRequest('rolereports')">Save&raquo;</button></p></td>
                        <%}}else{%>
                        <td><p><button class="btn btn-primary" onclick="saveMatrix('<%=roleId%>','<%=OperatorsManagement.MatrixSystemSecurity%>','<%=_operatorID%>')">Save&raquo;</button></p></td>
                        <%}%>
<!--                        <td><p><button class="btn btn-primary" onclick="saveMatrix('<%=roleId%>','<%=OperatorsManagement.MatrixSystemSecurity%>','<%=_operatorID%>')">Save&raquo;</button></p></td>-->
                        <td> </td>
                    </tr>
                </table>


            </div>
<!--End Shailendra changes-->
            <div class="tab-pane" id="SystemReport">
                <table class="table table-bordered  table-striped" style="width: 600px;">
                    <tr>
                        <td>Sr. No. </td>
                        <td>Name</td>
                        <td>List</td>
                        <td>View</td>
                        <td>Download</td>
                    </tr>
                    <tr>
                        <td>1 </td>
                        <td>Users Report</td>
                        <%if (accessSettings.listUserReport == true) { %>
                        <td> <input type="checkbox"  checked id="_sellistUserReport" name="_sellistUserReport"  value="true" onchange="accessList('listUserReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_sellistUserReport" name="_sellistUserReport"  value="false" onchange="accessList('listUserReport')"></td>
                            <%}%>
                            <%if (accessSettings.viewUserReport == true) { %>
                        <td> <input type="checkbox"  checked id="_selviewUserReport" name="_selviewUserReport"  value="true" onchange="accessList('viewUserReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selviewUserReport" name="_selviewUserReport"  value="false" onchange="accessList('viewUserReport')"></td>
                            <%}%>
                            <%if (accessSettings.downloadUserReport == true) { %>
                        <td> <input type="checkbox"  checked id="_seldownloadUserReport" name="_seldownloadUserReport"  value="true" onchange="accessList('downloadUserReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seldownloadUserReport" name="_seldownloadUserReport"  value="false" onchange="accessList('downloadUserReport')"></td>
                            <%}%>

                    </tr>
                    <tr>
                        <td>2 </td>
                        <td>Messages Report</td>
                        <%if (accessSettings.listMessageReport == true) { %>
                        <td> <input type="checkbox"  checked id="_selMessageReportList" name="_selMessageReportList"  value="true" onchange="accessList('MessageReportList')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selMessageReportList" name="_selMessageReportList"  value="false" onchange="accessList('MessageReportList')"></td>
                            <%}%>
                            <%if (accessSettings.viewMessageReport == true) { %>
                        <td> <input type="checkbox"  checked id="_selviewMessageReport" name="_selviewMessageReport"  value="true" onchange="accessList('viewMessageReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selviewMessageReport" name="_selviewMessageReport"  value="false" onchange="accessList('viewMessageReport')"></td>
                            <%}%>
                            <%if (accessSettings.downloadMessageReport == true) { %>
                        <td> <input type="checkbox"  checked id="_seldownloadMessageReport" name="_seldownloadMessageReport"  value="true" onchange="accessList('downloadMessageReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seldownloadMessageReport" name="_seldownloadMessageReport"  value="false" onchange="accessList('downloadMessageReport')"></td>
                            <%}%>

                    </tr>
                    <tr>
                        <td>3 </td>
                        <td>One Time Password Tokens Report</td>
                        <%if (accessSettings.listOtpReport == true) { %>
                        <td> <input type="checkbox"  checked id="_selOtpReportList" name="_selOtpReportList"  value="true" onchange="accessList('OtpReportList')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selOtpReportList" name="_selOtpReportList"  value="false" onchange="accessList('OtpReportList')"></td>
                            <%}%>
                            <%if (accessSettings.viewOtpReport == true) { %>
                        <td> <input type="checkbox"  checked id="_selviewOtpReport" name="_selviewOtpReport"  value="true" onchange="accessList('viewOtpReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selviewOtpReport" name="_selviewOtpReport"  value="false" onchange="accessList('viewOtpReport')"></td>
                            <%}%>
                            <%if (accessSettings.downloadOtpReport == true) { %>
                        <td> <input type="checkbox"  checked id="_seldownloadOtpReport" name="_seldownloadOtpReport"  value="true" onchange="accessList('downloadOtpReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seldownloadOtpReport" name="_seldownloadOtpReport"  value="false" onchange="accessList('downloadOtpReport')"></td>
                            <%}%>

                    </tr>
                    <tr>
                        <td>4 </td>
                        <td>One Time Password Tokens Failure Report</td>
                        <%if (accessSettings.listOtpFailureReport == true) { %>
                        <td> <input type="checkbox"  checked id="_selOtpFailureReportList" name="_selOtpFailureReportList"  value="true" onchange="accessList('OtpFailureReportList')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selOtpFailureReportList" name="_selOtpFailureReportList"  value="false" onchange="accessList('OtpFailureReportList')"></td>
                            <%}%>
                            <%if (accessSettings.viewOtpFailureReport == true) { %>
                        <td> <input type="checkbox"  checked id="_selviewOtpFailureReport" name="_selviewOtpFailureReport"  value="true" onchange="accessList('viewOtpFailureReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selviewOtpFailureReport" name="_selviewOtpFailureReport"  value="false" onchange="accessList('viewOtpFailureReport')"></td>
                            <%}%>
                            <%if (accessSettings.downloadOtpFailureReport == true) { %>
                        <td> <input type="checkbox"  checked id="_seldownloadOtpFailureReport" name="_seldownloadOtpFailureReport"  value="true" onchange="accessList('downloadOtpFailureReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seldownloadOtpFailureReport" name="_seldownloadOtpFailureReport"  value="false" onchange="accessList('downloadOtpFailureReport')"></td>
                            <%}%>

                    </tr>
                    <tr>
                        <td>5 </td>
                        <td>PKI Report</td>
                        <%if (accessSettings.listPkiReport == true) { %>
                        <td> <input type="checkbox"  checked id="_selPkiReportList" name="_selPkiReportList"  value="true" onchange="accessList('PkiReportList')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selPkiReportList" name="_selPkiReportList"  value="false" onchange="accessList('PkiReportList')"></td>
                            <%}%>
                            <%if (accessSettings.viewPkiReport == true) { %>
                        <td> <input type="checkbox"  checked id="_selviewPkiReport" name="_selviewPkiReport"  value="true" onchange="accessList('viewPkiReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selviewPkiReport" name="_selviewPkiReport"  value="false" onchange="accessList('viewPkiReport')"></td>
                            <%}%>
                            <%if (accessSettings.downloadPkiReport == true) { %>
                        <td> <input type="checkbox"  checked id="_seldownloadPkiReport" name="_seldownloadPkiReport"  value="true" onchange="accessList('downloadPkiReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seldownloadPkiReport" name="_seldownloadPkiReport"  value="false" onchange="accessList('downloadPkiReport')"></td>
                            <%}%>

                    </tr>
                    
                   
                    <tr>
                        <td>6 </td>
                        <td>Remote Signature Report</td>
                        <%if (accessSettings.listRemoteSigningReport == true) { %>
                        <td> <input type="checkbox"  checked id="_selRemoteSigningReportList" name="_selRemoteSigningReportList"  value="true" onchange="accessList('RemoteSigningReportList')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selRemoteSigningReportList" name="_selRemoteSigningReportList"  value="false" onchange="accessList('RemoteSigningReportList')"></td>
                            <%}%>
                            <%if (accessSettings.viewRemoteSigningReport == true) { %>
                        <td> <input type="checkbox"  checked id="_selviewRemoteSigningReport" name="_selviewRemoteSigningReport"  value="true" onchange="accessList('viewRemoteSigningReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selviewRemoteSigningReport" name="_selviewRemoteSigningReport"  value="false" onchange="accessList('viewRemoteSigningReport')"></td>
                            <%}%>
                            <%if (accessSettings.downloadRemoteSigningReport == true) { %>
                        <td> <input type="checkbox"  checked id="_seldownloadRemoteSigningReport" name="_seldownloadRemoteSigningReport"  value="true" onchange="accessList('downloadRemoteSigningReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seldownloadRemoteSigningReport" name="_seldownloadRemoteSigningReport"  value="false" onchange="accessList('downloadRemoteSigningReport')"></td>
                            <%}%>

                    </tr>
<!--                    <tr>
                        <td>7 </td>
                        <td>Billing Report(Subscription)</td>
                        <%if (accessSettings.listsubscriptionBaseBillingReport == true) { %>
                        <td> <input type="checkbox"  checked id="_selsubscriptionBaseBillingReportList" name="_selsubscriptionBaseBillingReportList"  value="true" onchange="accessList('subscriptionBaseBillingReportList')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selsubscriptionBaseBillingReportList" name="_selsubscriptionBaseBillingReportList"  value="false" onchange="accessList('subscriptionBaseBillingReportList')"></td>
                            <%}%>
                            <%if (accessSettings.viewsubscriptionBaseBillingReport == true) { %>
                        <td> <input type="checkbox"  checked id="_selviewsubscriptionBaseBillingReport" name="_selviewsubscriptionBaseBillingReport"  value="true" onchange="accessList('viewsubscriptionBaseBillingReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selviewsubscriptionBaseBillingReport" name="_selviewsubscriptionBaseBillingReport"  value="false" onchange="accessList('viewsubscriptionBaseBillingReport')"></td>
                            <%}%>
                            <%if (accessSettings.downloadsubscriptionBaseBillingReport == true) { %>
                        <td> <input type="checkbox"  checked id="_seldownloadsubscriptionBaseBillingReport" name="_seldownloadsubscriptionBaseBillingReport"  value="true" onchange="accessList('downloadsubscriptionBaseBillingReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seldownloadsubscriptionBaseBillingReport" name="_seldownloadsubscriptionBaseBillingReport"  value="false" onchange="accessList('downloadsubscriptionBaseBillingReport')"></td>
                            <%}%>

                    </tr>
                    <tr>
                        <td>8 </td>
                        <td>Billing Report(/Transaction)</td>
                        <%if (accessSettings.listtranscationBaseBillingReport == true) { %>
                        <td> <input type="checkbox"  checked id="_seltranscationBaseBillingReportList" name="_seltranscationBaseBillingReportList"  value="true" onchange="accessList('transcationBaseBillingReportList')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seltranscationBaseBillingReportList" name="_seltranscationBaseBillingReportList"  value="false" onchange="accessList('transcationBaseBillingReportList')"></td>
                            <%}%>
                            <%if (accessSettings.viewtranscationBaseBillingReport == true) { %>
                        <td> <input type="checkbox"  checked id="_selviewtranscationBaseBillingReport" name="_selviewtranscationBaseBillingReport"  value="true" onchange="accessList('viewtranscationBaseBillingReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selviewtranscationBaseBillingReport" name="_selviewtranscationBaseBillingReport"  value="false" onchange="accessList('viewtranscationBaseBillingReport')"></td>
                            <%}%>
                            <%if (accessSettings.downloadtranscationBaseBillingReport == true) { %>
                        <td> <input type="checkbox"  checked id="_seldownloadtranscationBaseBillingReport" name="_seldownloadtranscationBaseBillingReport"  value="true" onchange="accessList('downloadtranscationBaseBillingReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seldownloadtranscationBaseBillingReport" name="_seldownloadtranscationBaseBillingReport"  value="false" onchange="accessList('downloadtranscationBaseBillingReport')"></td>
                            <%}%>

                    </tr>-->
                      <tr>
                        <td>7 </td>
                        <td>Transaction Report</td>
                        <%if (accessSettings.listtranscationReport == true) { %>
                        <td> <input type="checkbox"  checked id="_seltranscationReportList" name="_seltranscationReportList"  value="true" onchange="accessList('transcationReportList')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seltranscationReportList" name="_seltranscationReportList"  value="false" onchange="accessList('transcationReportList')"></td>
                            <%}%>
                            <%if (accessSettings.viewtranscationReport == true) { %>
                        <td> <input type="checkbox"  checked id="_selviewtranscationReport" name="_selviewtranscationReport"  value="true" onchange="accessList('viewtranscationReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selviewtranscationReport" name="_selviewtranscationReport"  value="false" onchange="accessList('viewtranscationReport')"></td>
                            <%}%>
                            <%if (accessSettings.downloadtranscationReport == true) { %>
                        <td> <input type="checkbox"  checked id="_seldownloadtranscationReport" name="_seldownloadtranscationReport"  value="true" onchange="accessList('downloadtranscationReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seldownloadtranscationReport" name="_seldownloadtranscationReport"  value="false" onchange="accessList('downloadtranscationReport')"></td>
                            <%}%>

                    </tr>
                     <tr>
                        <td>8</td>
                        <td>Honey Trap Report</td>
                        <%if (accessSettings.listHoneyTrapReport == true) { %>
                        <td> <input type="checkbox"  checked id="_selHoneyTrapReportList" name="_selHoneyTrapReportList"  value="true" onchange="accessList('HoneyTrapReportList')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selHoneyTrapReportList" name="_selHoneyTrapReportList"  value="false" onchange="accessList('HoneyTrapReportList')"></td>
                            <%}%>
                            <%if (accessSettings.viewHoneyTrapReport == true) { %>
                        <td> <input type="checkbox"  checked id="_selviewHoneyTrapReport" name="_selviewHoneyTrapReport"  value="true" onchange="accessList('viewHoneyTrapReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selviewHoneyTrapReport" name="_selviewHoneyTrapReport"  value="false" onchange="accessList('viewHoneyTrapReport')"></td>
                            <%}%>
                            <%if (accessSettings.downloadHoneyTrapReport == true) { %>
                        <td> <input type="checkbox"  checked id="_seldownloadHoneyTrapReport" name="_seldownloadHoneyTrapReport"  value="true" onchange="accessList('downloadHoneyTrapReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seldownloadHoneyTrapReport" name="_seldownloadHoneyTrapReport"  value="false" onchange="accessList('downloadHoneyTrapReport')"></td>
                            <%}%>

                    </tr>
                    
                    <% if ( 1 == 2) { %>
                    
                     <tr>
                        <td>9 </td>
                        <td>EPIN Report(system specific)Report</td>
                        <%if (accessSettings.listEpinSystemReport == true) { %>
                        <td> <input type="checkbox"  checked id="_selEpinSystemReportList" name="_selEpinSystemReportList"  value="true" onchange="accessList('EpinSystemReportList')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEpinSystemReportList" name="_selEpinSystemReportList"  value="false" onchange="accessList('EpinSystemReportList')"></td>
                            <%}%>
                            <%if (accessSettings.viewEpinSystemReport == true) { %>
                        <td> <input type="checkbox"  checked id="_selviewEpinSystemReport" name="_selviewEpinSystemReport"  value="true" onchange="accessList('viewEpinSystemReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selviewEpinSystemReport" name="_selviewEpinSystemReport"  value="false" onchange="accessList('viewEpinSystemReport')"></td>
                            <%}%>
                            <%if (accessSettings.downloadEpinSystemReport == true) { %>
                        <td> <input type="checkbox"  checked id="_seldownloadEpinSystemReport" name="_seldownloadEpinSystemReport"  value="true" onchange="accessList('downloadEpinSystemReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seldownloadEpinSystemReport" name="_seldownloadEpinSystemReport"  value="false" onchange="accessList('downloadEpinSystemReport')"></td>
                            <%}%>

                    </tr>
                    <tr>
                        <td>7 </td>
                        <td>EPIN Report(user specific)</td>
                        <%if (accessSettings.listEpinUserSpecificReport == true) { %>
                        <td> <input type="checkbox"  checked id="_selEpinUserSpecificReportList" name="_selEpinUserSpecificReportList"  value="true" onchange="accessList('EpinUserSpecificReportList')"></td>
                            <%}else{%>
                        <td> <input type="checkbox"   id="_selEpinUserSpecificReportList" name="_selEpinUserSpecificReportList"  value="false" onchange="accessList('EpinUserSpecificReportList')"></td>
                            <%}%>
                            <%if (accessSettings.viewEpinUserSpecificReport == true) { %>
                        <td> <input type="checkbox"  checked id="_selviewEpinUserSpecificReport" name="_selviewEpinUserSpecificReport"  value="true" onchange="accessList('viewEpinUserSpecificReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selviewEpinUserSpecificReport" name="_selviewEpinUserSpecificReport"  value="false" onchange="accessList('viewEpinUserSpecificReport')"></td>
                            <%}%>
                            <%if (accessSettings.downloadEpinUserSpecificReport == true) { %>
                        <td> <input type="checkbox"  checked id="_seldownloadEpinUserSpecificReport" name="_seldownloadEpinUserSpecificReport"  value="true" onchange="accessList('downloadEpinUserSpecificReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seldownloadEpinUserSpecificReport" name="_seldownloadEpinUserSpecificReport"  value="false" onchange="accessList('downloadEpinUserSpecificReport')"></td>
                            <%}%>

                    </tr>
                    <%}%>
                    <tr>
                        <td>9</td>
                        <td>Easy Check-In Report</td>
                        
                        <%if (accessSettings.listEasyCheckinReport == true) { %>
                        <td> <input type="checkbox"  checked id="_sellistEasyCheckinReport" name="_sellistEasyCheckinReport"  value="true" onchange="accessList('listEasyCheckinReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_sellistEasyCheckinReport" name="_sellistEasyCheckinReport"  value="false" onchange="accessList('listEasyCheckinReport')"></td>
                            <%}%>
                            <%if (accessSettings.viewEasyCheckinReport == true) { %>
                        <td> <input type="checkbox"  checked id="_selviewEasyCheckinReport" name="_selviewEasyCheckinReport"  value="true" onchange="accessList('viewEasyCheckinReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selviewEasyCheckinReport" name="_selviewEasyCheckinReport"  value="false" onchange="accessList('viewEasyCheckinReport')"></td>
                            <%}%>
                            <%if (accessSettings.downloadasyCheckinReport == true) { %>
                        <td> <input type="checkbox"  checked id="_seldownloadEasyCheckinReport" name="_seldownloadasyCheckinReport"  value="true" onchange="accessList('downloadasyCheckinReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seldownloadasyCheckinReport" name="_seldownloadasyCheckinReport"  value="false" onchange="accessList('downloadasyCheckinReport')"></td>
                            <%}%>

                    </tr>
                     <tr>
                        <td>10</td>
                        <td>Certificate Reports</td>
                        <%if (accessSettings.listCertificateReport == true) { %>
                        <td> <input type="checkbox"  checked id="_sellistCertificateReport" name="_sellistCertificateReport"  value="true" onchange="accessList('listCertificateReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_sellistCertificateReport" name="_sellistCertificateReport"  value="false" onchange="accessList('listCertificateReport')"></td>
                            <%}%>
                            <%if (accessSettings.viewCertificateReport == true) { %>
                        <td> <input type="checkbox"  checked id="_selviewCertificateReport" name="_selviewCertificateReport"  value="true" onchange="accessList('viewCertificateReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selviewCertificateReport" name="_selviewCertificateReport"  value="false" onchange="accessList('viewCertificateReport')"></td>
                            <%}%>
                            <%if (accessSettings.downloadCertificateReport == true) { %>
                        <td> <input type="checkbox"  checked id="_seldownloadCertificateReport" name="_seldownloadCertificateReport"  value="true" onchange="accessList('downloadCertificateReport')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seldownloadCertificateReport" name="_seldownloadCertificateReport"  value="false" onchange="accessList('downloadCertificateReport')"></td>
                            <%}%>

                    </tr>
                    
                    <tr>
                        <td>11</td>
                        <td>Two-Way Auth Reports</td>
                        <%if (accessSettings.listTwoAuthRports == true) { %>
                        <td> <input type="checkbox"  checked id="_sellistTwoAuthRports" name="_sellistTwoAuthRports"  value="true" onchange="accessList('listTwoAuthRports')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_sellistTwoAuthRports" name="_sellistTwoAuthRports"  value="false" onchange="accessList('listTwoAuthRports')"></td>
                            <%}%>
                            <%if (accessSettings.viewTwoAuthRports == true) { %>
                        <td> <input type="checkbox"  checked id="_selviewTwoAuthRports" name="_selviewTwoAuthRports"  value="true" onchange="accessList('viewTwoAuthRports')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selviewTwoAuthRports" name="_selviewTwoAuthRports"  value="false" onchange="accessList('viewTwoAuthRports')"></td>
                            <%}%>
                            <%if (accessSettings.downloadTwoAuthRports == true) { %>
                        <td> <input type="checkbox"  checked id="_seldownloadTwoAuthRports" name="_seldownloadTwoAuthRports"  value="true" onchange="accessList('downloadTwoAuthRports')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seldownloadTwoAuthRports" name="_seldownloadTwoAuthRports"  value="false" onchange="accessList('downloadTwoAuthRports')"></td>
                            <%}%>

                    </tr>
                    <tr>
                        <td>12</td>
                        <td>Roles Reports</td>
                        <%if (accessSettings.listRoleRports == true) { %>
                        <td> <input type="checkbox"  checked id="_sellistRoleRports" name="_sellistRoleRports"  value="true" onchange="accessList('listRoleRports')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_sellistRoleRports" name="_sellistRoleRports"  value="false" onchange="accessList('listRoleRports')"></td>
                            <%}%>
                            <%if (accessSettings.viewRoleRports == true) { %>
                        <td> <input type="checkbox"  checked id="_selviewRoleRports" name="_selviewRoleRports"  value="true" onchange="accessList('viewRoleRports')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selviewRoleRports" name="_selviewRoleRports"  value="false" onchange="accessList('viewRoleRports')"></td>
                            <%}%>
                            <%if (accessSettings.downloadRoleRports == true) { %>
                        <td> <input type="checkbox"  checked id="_seldownloadRoleRports" name="_seldownloadRoleRports"  value="true" onchange="accessList('downloadRoleRports')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seldownloadRoleRports" name="_seldownloadRoleRports"  value="false" onchange="accessList('downloadRoleRports')"></td>
                            <%}%>

                    </tr>
                    <tr>
                        <td>13</td>
                        <td>Unit Reports</td>
                        <%if (accessSettings.listUnitRports == true) { %>
                        <td> <input type="checkbox"  checked id="_sellistUnitRports" name="_sellistUnitRports"  value="true" onchange="accessList('listUnitRports')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_sellistUnitRports" name="_sellistUnitRports"  value="false" onchange="accessList('listUnitRports')"></td>
                            <%}%>
                            <%if (accessSettings.viewUnitRports == true) { %>
                        <td> <input type="checkbox"  checked id="_selviewUnitRports" name="_selviewUnitRports"  value="true" onchange="accessList('viewUnitRports')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selviewUnitRports" name="_selviewUnitRports"  value="false" onchange="accessList('viewUnitRports')"></td>
                            <%}%>
                            <%if (accessSettings.downloadUnitReports == true) { %>
                        <td> <input type="checkbox"  checked id="_seldownloadUnitRports" name="_seldownloadUnitReports"  value="true" onchange="accessList('downloadUnitReports')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_seldownloadUnitReports" name="_seldownloadUnitRports"  value="false" onchange="accessList('downloadUnitReports')"></td>
                            <%}%>

                    </tr>
                    <tr>
                        <td> </td>
                        <%if (oprObj.getRoleid() != 1) {%>
                        <%if (accessObj != null && (accessObj.editaccessMatrix == true)) {%>
                        <td><p><button class="btn btn-primary" onclick="saveMatrix('<%=roleId%>','<%=OperatorsManagement.MatrixSystemReport%>','<%=_operatorID%>')">Save&raquo;</button></p></td>
                        <%}else{%>
                        <td><p><button class="btn btn-primary" onclick="InvalidRequest('rolereports')">Save&raquo;</button></p></td>
                        <%}}else{%>
                        <td><p><button class="btn btn-primary" onclick="saveMatrix('<%=roleId%>','<%=OperatorsManagement.MatrixSystemReport%>','<%=_operatorID%>')">Save&raquo;</button></p></td>
                        <%}%>
<!--                        <td><p><button class="btn btn-primary" onclick="saveMatrix('<%=roleId%>','<%=OperatorsManagement.MatrixSystemReport%>','<%=_operatorID%>')">Save&raquo;</button></p></td>-->
                        <td> </td>
                        <td> </td>
                        <td> </td>
                    </tr>
                </table>
            </div>

            <div class="tab-pane" id="SystemSpecific">
                <table>
                    <tr>
                        <td>
                            <table class="table table-bordered table-hover table-striped" style="width: 400px;">
                                <tr>
                                    <td>Sr. No. </td>
                                    <td>Type</td>
                                    <td>List</td>
                                </tr>
                                <tr><td>1 </td>
                                    <td>License Details</td>
                                    <%if (accessSettings.listlicenseDetails == true) { %>
                                    <td> <input type="checkbox"  checked id="_sellistlicenseDetails" name="_sellistlicenseDetails"  value="true" onchange="accessList('listlicenseDetails')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_sellistlicenseDetails" name="_sellistlicenseDetails"  value="false" onchange="accessList('listlicenseDetails')"></td>
                                        <%}%>
                                </tr>
                                <tr>
                                    <td>2 </td>
                                    <td>Hardware OTP Token File Uploader</td>
                                    <%if (accessSettings.listhwOTPTokenUpload == true) { %>
                                    <td> <input type="checkbox"  checked id="_sellisthwOTPTokenUpload" name="_sellisthwOTPTokenUpload"  value="true" onchange="accessList('listhwOTPTokenUpload')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_sellisthwOTPTokenUpload" name="_sellisthwOTPTokenUpload"  value="false" onchange="accessList('listhwOTPTokenUpload')"></td>
                                        <%}%>
                                </tr>
                                <tr>
                                    <td>3 </td>
                                    <td>Hardware PKI Token File Uploader</td>
                                    <%if (accessSettings.listhwPKITokenUpload == true) { %>
                                    <td> <input type="checkbox"  checked id="_sellisthwPKITokenUpload" name="_sellisthwPKITokenUpload"  value="true" onchange="accessList('listhwPKITokenUpload')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_sellisthwPKITokenUpload" name="_sellisthwPKITokenUpload"  value="false" onchange="accessList('listhwPKITokenUpload')"></td>
                                        <%}%>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>E-PDF Signing</td>
                                    <%if (accessSettings.signPDFDOC == true) { %>
                                    <td> <input type="checkbox"  checked id="_selsignPDFDOC" name="_selsignPDFDOC"  value="true" onchange="accessList('signPDFDOC')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selsignPDFDOC" name="_selsignPDFDOC"  value="false" onchange="accessList('signPDFDOC')"></td>
                                        <%}%>
                                </tr>
<!--                                <tr>
                                    <td>4 </td>
                                    <td><font color="red"Change Security Credentials</font></td>
                                    <%if (accessSettings.changeSecurityCredentials == true) { %>
                                    <td> <input type="checkbox"  checked id="_selchangeSecurityCredentials" name="_selchangeSecurityCredentials"  value="true" onchange="accessList('changeSecurityCredentials')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selchangeSecurityCredentials" name="_selchangeSecurityCredentials"  value="false" onchange="accessList('changeSecurityCredentials')"></td>
                                        <%}%>
                                </tr>-->
                                <tr>
                                    <td>5 </td>
                                        <td>Download System Audit Trail</td>
                                    <%if (accessSettings.downloadAuditTrail == true) { %>
                                    <td> <input type="checkbox"  checked id="_seldownloadAuditTrail" name="_seldownloadAuditTrail"  value="true" onchange="accessList('downloadAuditTrail')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_seldownloadAuditTrail" name="_seldownloadAuditTrail"  value="false" onchange="accessList('downloadAuditTrail')"></td>
                                        <%}%>
                                </tr>
                                <tr>
                                    <td>6 </td>
                                    <td>Session Audit Trail And Kill </td>
                                    <%if (accessSettings.downloadSessionAuditTrail == true) { %>
                                    <td> <input type="checkbox"  checked id="_seldownloadSessionAuditTrail" name="_seldownloadSessionAuditTrail"  value="true" onchange="accessList('downloadSessionAuditTrail')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_seldownloadSessionAuditTrail" name="_seldownloadSessionAuditTrail"  value="false" onchange="accessList('downloadSessionAuditTrail')"></td>
                                        <%}%>
                                </tr>
<!--                                <tr>
                                    <td>7</td>
                                    <td>Kill Session Id</td>
                                    <%if (accessSettings.killSessionID == true) { %>
                                    <td> <input type="checkbox"  checked id="_selkillSessionID" name="_selkillSessionID"  value="true" onchange="accessList('killSessionID')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selkillSessionID" name="_selkillSessionID"  value="false" onchange="accessList('killSessionID')"></td>
                                        <%}%>
                                </tr>-->
                                <tr>
                                    <td>7 </td>
                                    <td>Download Logs</td>
                                    <%if (accessSettings.downloadLogs == true) { %>
                                    <td> <input type="checkbox"  checked id="_seldownloadLogs" name="_seldownloadLogs"  value="true" onchange="accessList('downloadLogs')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_seldownloadLogs" name="_seldownloadLogs"  value="false" onchange="accessList('downloadLogs')"></td>
                                        <%}%>
                                </tr>
                                <% if ( 1 == 2 ) { %>
                                <tr>
                                    <td>8 </td>
                                    <td>Download Cleanup Logs</td>
                                    <%if (accessSettings.downloadCleanupLogs == true) { %>
                                    <td> <input type="checkbox"  checked id="_seldownloadCleanupLogs" name="_seldownloadCleanupLogs"  value="true" onchange="accessList('downloadCleanupLogs')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_seldownloadCleanupLogs" name="_seldownloadCleanupLogs"  value="false" onchange="accessList('downloadCleanupLogs')"></td>
                                        <%}%>
                                </tr>
                                <%  } %>
<!--                                <tr>
                                    <td>9 </td>
                                    <td>Connector Status</td>
                                    <%if (accessSettings.connectorStatus == true) { %>
                                    <td> <input type="checkbox"  checked id="_selconnectorStatus" name="_selconnectorStatus"  value="true" onchange="accessList('connectorStatus')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selconnectorStatus" name="_selconnectorStatus"  value="false" onchange="accessList('connectorStatus')"></td>
                                        <%}%>
                                </tr>-->
                                <tr> <td>9</td>
                                    <td>Audit Integrity</td>
                                    <%if (accessSettings.auditIntegrity == true) { %>
                                    <td> <input type="checkbox"  checked id="_selauditIntegrity" name="_selauditIntegrity"  value="true" onchange="accessList('auditIntegrity')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selauditIntegrity" name="_selauditIntegrity"  value="false" onchange="accessList('auditIntegrity')"></td>
                                        <%}%>
                                </tr>

                                <tr> <td>10 </td>
                                    <td>Change Password</td>
                                    <%if (accessSettings.changePassword == true) { %>
                                    <td> <input type="checkbox"  checked id="_selchangePassword" name="_selchangePassword"  value="true" onchange="accessList('changePassword')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selchangePassword" name="_selchangePassword"  value="false" onchange="accessList('changePassword')"></td>
                                        <%}%>
                                </tr>
<!--                                <tr> <td>12 </td>
                                    <td>Requester Archive list</td>
                                    <%if (accessSettings.requesterArchiveRequest == true) { %>
                                    <td> <input type="checkbox"  checked id="_selrequesterArchiveRequest" name="_selrequesterArchiveRequest"  value="true" onchange="accessList('requesterArchiveRequest')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selrequesterArchiveRequest" name="_selrequesterArchiveRequest"  value="false" onchange="accessList('requesterArchiveRequest')"></td>
                                        <%}%>
                                </tr>
                                <tr> <td>13 </td>
                                    <td>Requester Pending List</td>
                                    <%if (accessSettings.requesterPendingRequest == true) { %>
                                    <td> <input type="checkbox"  checked id="_selrequesterPendingRequest" name="_selrequesterPendingRequest"  value="true" onchange="accessList('requesterPendingRequest')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selrequesterPendingRequest" name="_selrequesterPendingRequest"  value="false" onchange="accessList('requesterPendingRequest')"></td>
                                        <%}%>
                                </tr>
                                <tr> <td>14 </td>
                                    <td>Authorizer Archive List</td>
                                    <%if (accessSettings.authorizerArchiveRequest == true) { %>
                                    <td> <input type="checkbox"  checked id="_selauthorizerArchiveRequest" name="_selauthorizerArchiveRequest"  value="true" onchange="accessList('authorizerArchiveRequest')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selauthorizerArchiveRequest" name="_selauthorizerArchiveRequest"  value="false" onchange="accessList('authorizerArchiveRequest')"></td>
                                        <%}%>
                                </tr>
                                <tr> <td>15 </td>
                                    <td>Authorizer Pending List</td>
                                    <%if (accessSettings.authorizerPendingRequest == true) { %>
                                    <td> <input type="checkbox"  checked id="_selauthorizerPendingRequest" name="_selauthorizerPendingRequest"  value="true" onchange="accessList('authorizerPendingRequest')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selauthorizerPendingRequest" name="_selauthorizerPendingRequest"  value="false" onchange="accessList('authorizerPendingRequest')"></td>
                                        <%}%>
                                </tr>
                                <tr> <td>16 </td>
                                    <td>Requester Operator List</td>
                                    <%if (accessSettings.requesterOperatorList == true) { %>
                                    <td> <input type="checkbox"  checked id="_selrequesterOperatorList" name="_selrequesterOperatorList"  value="true" onchange="accessList('requesterOperatorList')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selrequesterOperatorList" name="_selrequesterOperatorList"  value="false" onchange="accessList('requesterOperatorList')"></td>
                                        <%}%>
                                </tr>-->
<!--                                <tr> <td>18 </td>
                                    <td>Requester Operator List</td>
                                    <%if (accessSettings.kycRequests == true) { %>
                                    <td> <input type="checkbox"  checked id="_selkycRequests" name="_selkycRequests"  value="true" onchange="accessList('kycRequests')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selkycRequests" name="_selkycRequests"  value="false" onchange="accessList('kycRequests')"></td>
                                        <%}%>
                                </tr>-->
                                <tr>
                                    <td> </td>
                                    <%if (oprObj.getRoleid() != 1) {%>
                        <%if (accessObj != null && (accessObj.editaccessMatrix == true)) {%>
                        <td><p><button class="btn btn-primary" onclick="saveMatrix('<%=roleId%>','<%=OperatorsManagement.MatrixSystemSpecific%>','<%=_operatorID%>')">Save&raquo;</button></p></td>
                        <%}else{%>
                        <td><p><button class="btn btn-primary" onclick="InvalidRequest('rolereports')">Save&raquo;</button></p></td>
                        <%}}else{%>
                        <td><p><button class="btn btn-primary" onclick="saveMatrix('<%=roleId%>','<%=OperatorsManagement.MatrixSystemSpecific%>','<%=_operatorID%>')">Save&raquo;</button></p></td>
                        <%}%>
<!--                                    <td><p><button class="btn btn-primary" onclick="saveMatrix('<%=roleId%>','<%=OperatorsManagement.MatrixSystemSpecific%>','<%=_operatorID%>')">Save&raquo;</button></p></td>-->
                                    <td> </td>
                                </tr>
                            </table>
                        </td>
<!--                        <td>
                            <table class="table table-bordered table-hover table-striped" style="width: 400px;">
                                <tr>
                                    <td>Sr. No. </td>
                                    <td>Type</td>
                                    <td>List</td>
                                </tr>
                                <tr> <td>11 </td>
                                    <td>Audit Integrity</td>
                                    <%if (accessSettings.auditIntegrity == true) { %>
                                    <td> <input type="checkbox"  checked id="_selauditIntegrity" name="_selauditIntegrity"  value="true" onchange="accessList('auditIntegrity')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selauditIntegrity" name="_selauditIntegrity"  value="false" onchange="accessList('auditIntegrity')"></td>
                                        <%}%>
                                </tr>

                                <tr> <td>12 </td>
                                    <td>Change Password</td>
                                    <%if (accessSettings.changePassword == true) { %>
                                    <td> <input type="checkbox"  checked id="_selchangePassword" name="_selchangePassword"  value="true" onchange="accessList('changePassword')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selchangePassword" name="_selchangePassword"  value="false" onchange="accessList('changePassword')"></td>
                                        <%}%>
                                </tr>
                                <tr> <td>13 </td>
                                    <td>Requester Archive list</td>
                                    <%if (accessSettings.requesterArchiveRequest == true) { %>
                                    <td> <input type="checkbox"  checked id="_selrequesterArchiveRequest" name="_selrequesterArchiveRequest"  value="true" onchange="accessList('requesterArchiveRequest')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selrequesterArchiveRequest" name="_selrequesterArchiveRequest"  value="false" onchange="accessList('requesterArchiveRequest')"></td>
                                        <%}%>
                                </tr>
                                <tr> <td>14 </td>
                                    <td>Requester Pending List</td>
                                    <%if (accessSettings.requesterPendingRequest == true) { %>
                                    <td> <input type="checkbox"  checked id="_selrequesterPendingRequest" name="_selrequesterPendingRequest"  value="true" onchange="accessList('requesterPendingRequest')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selrequesterPendingRequest" name="_selrequesterPendingRequest"  value="false" onchange="accessList('requesterPendingRequest')"></td>
                                        <%}%>
                                </tr>
                                <tr> <td>15 </td>
                                    <td>Authorizer Archive List</td>
                                    <%if (accessSettings.authorizerArchiveRequest == true) { %>
                                    <td> <input type="checkbox"  checked id="_selauthorizerArchiveRequest" name="_selauthorizerArchiveRequest"  value="true" onchange="accessList('authorizerArchiveRequest')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selauthorizerArchiveRequest" name="_selauthorizerArchiveRequest"  value="false" onchange="accessList('authorizerArchiveRequest')"></td>
                                        <%}%>
                                </tr>
                                <tr> <td>16 </td>
                                    <td>Authorizer Pending List</td>
                                    <%if (accessSettings.authorizerPendingRequest == true) { %>
                                    <td> <input type="checkbox"  checked id="_selauthorizerPendingRequest" name="_selauthorizerPendingRequest"  value="true" onchange="accessList('authorizerPendingRequest')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selauthorizerPendingRequest" name="_selauthorizerPendingRequest"  value="false" onchange="accessList('authorizerPendingRequest')"></td>
                                        <%}%>
                                </tr>
                                <tr> <td>17 </td>
                                    <td>Requester Operator List</td>
                                    <%if (accessSettings.requesterOperatorList == true) { %>
                                    <td> <input type="checkbox"  checked id="_selrequesterOperatorList" name="_selrequesterOperatorList"  value="true" onchange="accessList('requesterOperatorList')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selrequesterOperatorList" name="_selrequesterOperatorList"  value="false" onchange="accessList('requesterOperatorList')"></td>
                                        <%}%>
                                </tr>
                                <tr> <td>18 </td>
                                    <td>Requester Operator List</td>
                                    <%if (accessSettings.kycRequests == true) { %>
                                    <td> <input type="checkbox"  checked id="_selkycRequests" name="_selkycRequests"  value="true" onchange="accessList('kycRequests')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selkycRequests" name="_selkycRequests"  value="false" onchange="accessList('kycRequests')"></td>
                                        <%}%>
                                </tr>
                                <tr></tr>
                                <tr></tr>
                                <tr></tr>
                                <tr>
                                    <td> </td>
                                    <td><p><button class="btn btn-primary" onclick="saveMatrix('<%=roleId%>',<%=OperatorsManagement.MatrixSystemSpecific%>,<%=_accesstype%>)">Save&raquo;</button></p></td>
                                    <td> </td>
                                </tr>
                            </table>

                        </td>-->
                    </tr>
                </table>
            </div>            





            <div class="tab-pane" id="SystemSpecific1">
                <table>
                    <tr>
                        <td>
                            <table class="table table-bordered table-hover table-striped" style="width: 400px;">
                                <tr>
                                    <td>Sr. No. </td>
                                    <td>Type</td>
                                    <td>List</td>
                                    <td>Add</td>
                                    <td>Edit</td>
                                    <td>Delete</td>
                                </tr>


                                <tr><td>20 </td>
                                    <td>Report Scheduler</td>
                                    <%if (accessSettings.downloadAuditTrail == true) { %>
                                    <td> <input type="checkbox"  checked id="_selDownloadAuditTrail" name="_selDownloadAuditTrail"  value="true" onchange="accessList('DownloadAuditTrail')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selDownloadAuditTrail" name="_selDownloadAuditTrail"  value="false" onchange="accessList('DownloadAuditTrail')"></td>
                                        <%}%>
                                    <td> <input type="checkbox"   id="_selDownloadAuditTrail" name="_selDownloadAuditTrail"  value="false" onchange="accessList('DownloadAuditTrail')"></td>
                                    <td> <input type="checkbox"   id="_selDownloadAuditTrail" name="_selDownloadAuditTrail"  value="false" onchange="accessList('DownloadAuditTrail')"></td>
                                    <td> <input type="checkbox"   id="_selDownloadAuditTrail" name="_selDownloadAuditTrail"  value="false" onchange="accessList('DownloadAuditTrail')"></td>
                                </tr>

                                <tr><td>21 </td>
                                    <td>Download Session Audit</td>
                                    <%if (accessSettings.downloadSessionAuditTrail == true) { %>
                                    <td> <input type="checkbox"  checked id="_selDownloadSessionAuditTrail" name="_selDownloadSessionAuditTrail"  value="true" onchange="accessList('DownloadSessionAuditTrail')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selDownloadSessionAuditTrail" name="_selDownloadSessionAuditTrail"  value="false" onchange="accessList('DownloadSessionAuditTrail')"></td>
                                        <%}%>
                                    <td> <input type="checkbox"   id="_selDownloadSessionAuditTrail" name="_selDownloadSessionAuditTrail"  value="false" onchange="accessList('DownloadSessionAuditTrail')"></td>
                                    <td> <input type="checkbox"   id="_selDownloadSessionAuditTrail" name="_selDownloadSessionAuditTrail"  value="false" onchange="accessList('DownloadSessionAuditTrail')"></td>
                                    <td> <input type="checkbox"   id="_selDownloadSessionAuditTrail" name="_selDownloadSessionAuditTrail"  value="false" onchange="accessList('DownloadSessionAuditTrail')"></td>
                                </tr>

                                <tr><td>22 </td>
                                    <td>Kill Session</td>
                                    <%if (accessSettings.killSessionID == true) { %>
                                    <td> <input type="checkbox"  checked id="_selKillSessionID" name="_selKillSessionID"  value="true" onchange="accessList('KillSessionID')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selKillSessionID" name="_selKillSessionID"  value="false" onchange="accessList('KillSessionID')"></td>
                                        <%}%>
                                    <td> <input type="checkbox"   id="_selKillSessionID" name="_selKillSessionID"  value="false" onchange="accessList('KillSessionID')"></td>
                                    <td> <input type="checkbox"   id="_selKillSessionID" name="_selKillSessionID"  value="false" onchange="accessList('KillSessionID')"></td>
                                    <td> <input type="checkbox"   id="_selKillSessionID" name="_selKillSessionID"  value="false" onchange="accessList('KillSessionID')"></td>
                                </tr>
                                <tr><td>23 </td>
                                    <td>Download Logs</td>
                                    <%if (accessSettings.downloadLogs == true) { %>
                                    <td> <input type="checkbox"  checked id="_selDownloadLogs" name="_selDownloadLogs"  value="true" onchange="accessList('DownloadLogs')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selDownloadLogs" name="_selDownloadLogs"  value="false" onchange="accessList('DownloadLogs')"></td>
                                        <%}%>
                                    <td> <input type="checkbox"   id="_selDownloadLogs" name="_selDownloadLogs"  value="false" onchange="accessList('DownloadLogs')"></td>
                                    <td> <input type="checkbox"   id="_selDownloadLogs" name="_selDownloadLogs"  value="false" onchange="accessList('DownloadLogs')"></td>
                                    <td> <input type="checkbox"   id="_selDownloadLogs" name="_selDownloadLogs"  value="false" onchange="accessList('DownloadLogs')"></td>
                                </tr>
                                <% if ( 1 == 2 ) { %>
                                <tr><td>24 </td>
                                    <td>Download Cleanup logs</td>
                                    <%if (accessSettings.downloadCleanupLogs == true) { %>
                                    <td> <input type="checkbox"  checked id="_selDownloadCleanupLogs" name="_selDownloadCleanupLogs"  value="true" onchange="accessList('DownloadCleanupLogs')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selDownloadCleanupLogs" name="_selDownloadCleanupLogs"  value="false" onchange="accessList('DownloadCleanupLogs')"></td>
                                        <%}%>
                                    <td> <input type="checkbox"   id="_selDownloadCleanupLogs" name="_selDownloadCleanupLogs"  value="false" onchange="accessList('DownloadCleanupLogs')"></td>
                                    <td> <input type="checkbox"   id="_selDownloadCleanupLogs" name="_selDownloadCleanupLogs"  value="false" onchange="accessList('DownloadCleanupLogs')"></td>
                                    <td> <input type="checkbox"   id="_selDownloadCleanupLogs" name="_selDownloadCleanupLogs"  value="false" onchange="accessList('DownloadCleanupLogs')"></td>
                                </tr>
                                <% } %>
                                <tr><td>24 </td>
                                    <td>Connector Status</td>
                                    <%if (accessSettings.connectorStatus == true) { %>
                                    <td> <input type="checkbox"  checked id="_selConnectorStatus" name="_selConnectorStatus"  value="true" onchange="accessList('ConnectorStatus')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selConnectorStatus" name="_selConnectorStatus"  value="false" onchange="accessList('ConnectorStatus')"></td>
                                        <%}%>
                                    <td> <input type="checkbox"   id="_selConnectorStatus" name="_selConnectorStatus"  value="false" onchange="accessList('ConnectorStatus')"></td>
                                    <td> <input type="checkbox"   id="_selConnectorStatus" name="_selConnectorStatus"  value="false" onchange="accessList('ConnectorStatus')"></td>
                                    <td> <input type="checkbox"   id="_selConnectorStatus" name="_selConnectorStatus"  value="false" onchange="accessList('ConnectorStatus')"></td>
                                </tr>
                                <tr><td>25 </td>
                                    <td>Audit Integrity</td>
                                    <%if (accessSettings.auditIntegrity == true) { %>
                                    <td> <input type="checkbox"  checked id="_selAuditIntegrity" name="_selAuditIntegrity"  value="true" onchange="accessList('AuditIntegrity')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selAuditIntegrity" name="_selAuditIntegrity"  value="false" onchange="accessList('AuditIntegrity')"></td>
                                        <%}%>
                                    <td> <input type="checkbox"   id="_selAuditIntegrity" name="_selAuditIntegrity"  value="false" onchange="accessList('AuditIntegrity')"></td>
                                    <td> <input type="checkbox"   id="_selAuditIntegrity" name="_selAuditIntegrity"  value="false" onchange="accessList('AuditIntegrity')"></td>
                                    <td> <input type="checkbox"   id="_selAuditIntegrity" name="_selAuditIntegrity"  value="false" onchange="accessList('AuditIntegrity')"></td>
                                </tr>

                                <tr><td>26 </td>
                                    <td>Change Password</td>
                                    <%if (accessSettings.changePassword == true) { %>
                                    <td> <input type="checkbox"  checked id="_selChangePassword" name="_selChangePassword"  value="true" onchange="accessList('ChangePassword')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selChangePassword" name="_selChangePassword"  value="false" onchange="accessList('ChangePassword')"></td>
                                        <%}%>
                                    <td> <input type="checkbox"   id="_selChangePassword" name="_selChangePassword"  value="false" onchange="accessList('ChangePassword')"></td>
                                    <td> <input type="checkbox"   id="_selChangePassword" name="_selChangePassword"  value="false" onchange="accessList('ChangePassword')"></td>
                                    <td> <input type="checkbox"   id="_selChangePassword" name="_selChangePassword"  value="false" onchange="accessList('ChangePassword')"></td>
                                </tr>

                                <tr>
                                    <td> </td>
                                    <td><p><button class="btn btn-primary" onclick="saveMatrix('<%=roleId%>',<%=OperatorsManagement.MatrixLogout%>,<%=_accesstype%>)">Save&raquo;</button></p></td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>

                                </tr>
                            </table>
                        </td>
                        <td>
                            <table class="table table-bordered table-hover table-striped" style="width: 400px;">
                                <tr>
                                    <td>Sr. No. </td>
                                    <td>Type</td>
                                    <td>List</td>
                                    <td>Add</td>
                                    <td>Edit</td>
                                    <td>Delete</td>
                                </tr>


                                <tr><td>29 </td>
                                    <td>Requester Archive Request</td>
                                    <%if (accessSettings.requesterArchiveRequest == true) { %>
                                    <td> <input type="checkbox"  checked id="_selRequesterArchiveRequest" name="_selRequesterArchiveRequest"  value="true" onchange="accessList('RequesterArchiveRequest')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selRequesterArchiveRequest" name="_selRequesterArchiveRequest"  value="false" onchange="accessList('RequesterArchiveRequest')"></td>
                                        <%}%>
                                    <td> <input type="checkbox"   id="_selRequesterArchiveRequest" name="_selRequesterArchiveRequest"  value="false" onchange="accessList('RequesterArchiveRequest')"></td>
                                    <td> <input type="checkbox"   id="_selRequesterArchiveRequest" name="_selRequesterArchiveRequest"  value="false" onchange="accessList('RequesterArchiveRequest')"></td>
                                    <td> <input type="checkbox"   id="_selRequesterArchiveRequest" name="_selRequesterArchiveRequest"  value="false" onchange="accessList('RequesterArchiveRequest')"></td>
                                </tr>
                                <tr><td>30 </td>
                                    <td>Requester Pensding Request</td>
                                    <%if (accessSettings.requesterPendingRequest == true) { %>
                                    <td> <input type="checkbox"  checked id="_selRequesterPendingRequest" name="_selRequesterPendingRequest"  value="true" onchange="accessList('RequesterPendingRequest')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selRequesterPendingRequest" name="_selRequesterPendingRequest"  value="false" onchange="accessList('RequesterPendingRequest')"></td>
                                        <%}%>
                                    <td> <input type="checkbox"   id="_selRequesterPendingRequest" name="_selRequesterPendingRequest"  value="false" onchange="accessList('RequesterPendingRequest')"></td>
                                    <td> <input type="checkbox"   id="_selRequesterPendingRequest" name="_selRequesterPendingRequest"  value="false" onchange="accessList('RequesterPendingRequest')"></td>
                                    <td> <input type="checkbox"   id="_selRequesterPendingRequest" name="_selRequesterPendingRequest"  value="false" onchange="accessList('RequesterPendingRequest')"></td>
                                </tr>
                                <tr><td>31 </td>
                                    <td>Authorizer Pensding Request</td>
                                    <%if (accessSettings.authorizerArchiveRequest == true) { %>
                                    <td> <input type="checkbox"  checked id="_selAuthorizerArchiveRequest" name="_selAuthorizerArchiveRequest"  value="true" onchange="accessList('AuthorizerArchiveRequest')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selAuthorizerArchiveRequest" name="_selAuthorizerArchiveRequest"  value="false" onchange="accessList('AuthorizerArchiveRequest')"></td>
                                        <%}%>
                                    <td> <input type="checkbox"   id="_selAuthorizerArchiveRequest" name="_selAuthorizerArchiveRequest"  value="false" onchange="accessList('AuthorizerArchiveRequest')"></td>
                                    <td> <input type="checkbox"   id="_selAuthorizerArchiveRequest" name="_selAuthorizerArchiveRequest"  value="false" onchange="accessList('AuthorizerArchiveRequest')"></td>
                                    <td> <input type="checkbox"   id="_selAuthorizerArchiveRequest" name="_selAuthorizerArchiveRequest"  value="false" onchange="accessList('AuthorizerArchiveRequest')"></td>
                                </tr>
                                <tr><td>32 </td>
                                    <td>Authorizer Archive Request</td>
                                    <%if (accessSettings.authorizerArchiveRequest == true) { %>
                                    <td> <input type="checkbox"  checked id="_selAuthorizerArchiveRequest" name="_selAuthorizerArchiveRequest"  value="true" onchange="accessList('AuthorizerArchiveRequest')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selAuthorizerArchiveRequest" name="_selAuthorizerArchiveRequest"  value="false" onchange="accessList('AuthorizerArchiveRequest')"></td>
                                        <%}%>
                                    <td> <input type="checkbox"   id="_selAuthorizerArchiveRequest" name="_selAuthorizerArchiveRequest"  value="false" onchange="accessList('AuthorizerArchiveRequest')"></td>
                                    <td> <input type="checkbox"   id="_selAuthorizerArchiveRequest" name="_selAuthorizerArchiveRequest"  value="false" onchange="accessList('AuthorizerArchiveRequest')"></td>
                                    <td> <input type="checkbox"   id="_selAuthorizerArchiveRequest" name="_selAuthorizerArchiveRequest"  value="false" onchange="accessList('AuthorizerArchiveRequest')"></td>
                                </tr>
                                <tr><td>33 </td>
                                    <td>Authorizer Pending Request</td>
                                    <%if (accessSettings.authorizerPendingRequest == true) { %>
                                    <td> <input type="checkbox"  checked id="_selAuthorizerPendingRequest" name="_selAuthorizerPendingRequest"  value="true" onchange="accessList('AuthorizerPendingRequest')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selAuthorizerPendingRequest" name="_selAuthorizerPendingRequest"  value="false" onchange="accessList('AuthorizerPendingRequest')"></td>
                                        <%}%>
                                    <td> <input type="checkbox"   id="_selAuthorizerPendingRequest" name="_selAuthorizerPendingRequest"  value="false" onchange="accessList('AuthorizerPendingRequest')"></td>
                                    <td> <input type="checkbox"   id="_selAuthorizerPendingRequest" name="_selAuthorizerPendingRequest"  value="false" onchange="accessList('AuthorizerPendingRequest')"></td>
                                    <td> <input type="checkbox"   id="_selAuthorizerPendingRequest" name="_selAuthorizerPendingRequest"  value="false" onchange="accessList('AuthorizerPendingRequest')"></td>
                                </tr>
                                <tr><td>34</td>
                                    <td>Requester Operator list</td>
                                    <%if (accessSettings.requesterOperatorList == true) { %>
                                    <td> <input type="checkbox"  checked id="_selRequesterOperatorList" name="_selRequesterOperatorList"  value="true" onchange="accessList('RequesterOperatorList')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selRequesterOperatorList" name="_selRequesterOperatorList"  value="false" onchange="accessList('RequesterOperatorList')"></td>
                                        <%}%>
                                    <td> <input type="checkbox"   id="_selRequesterOperatorList" name="_selRequesterOperatorList"  value="false" onchange="accessList('RequesterOperatorList')"></td>
                                    <td> <input type="checkbox"   id="_selRequesterOperatorList" name="_selRequesterOperatorList"  value="false" onchange="accessList('RequesterOperatorList')"></td>
                                    <td> <input type="checkbox"   id="_selRequesterOperatorList" name="_selRequesterOperatorList"  value="false" onchange="accessList('RequesterOperatorList')"></td>
                                </tr>
                                <tr><td>35 </td>
                                    <td>Kyc Request</td>
                                    <%if (accessSettings.kycRequests == true) { %>
                              <td> <input type="checkbox"  checked id="_selKycRequests" name="_selKycRequests"  value="true" onchange="accessList('KycRequests')"></td>
                                        <%} else {%>
                                    <td> <input type="checkbox"   id="_selKycRequests" name="_selKycRequests"  value="false" onchange="accessList('KycRequests')"></td>
                                        <%}%>
                                    <td> <input type="checkbox"   id="_selKycRequests" name="_selKycRequests"  value="false" onchange="accessList('KycRequests')"></td>
                                    <td> <input type="checkbox"   id="_selKycRequests" name="_selKycRequests"  value="false" onchange="accessList('KycRequests')"></td>
                                    <td> <input type="checkbox"   id="_selKycRequests" name="_selKycRequests"  value="false" onchange="accessList('KycRequests')"></td>
                                </tr>
                                <tr>
                                    <td> </td>
                                    <td><p><button class="btn btn-primary" onclick="saveMatrix('<%=roleId%>',<%=OperatorsManagement.MatrixLogout%>,<%=_accesstype%>)">Save&raquo;</button></p></td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>



    </div>
    <form id="deleteListForm" name="deleteListForm">
        <input type="hidden" name="_deleteList" id="_deleteList" value=",">
    </form>
    <form id="addListForm" name="addListForm">
        <input type="hidden" name="_addList" id="_addList" value=",">
    </form>


    <%@include file="footer.jsp" %>