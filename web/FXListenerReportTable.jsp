
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.ISOManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Pushmessagemappers"%>
<%@page import="com.mollatech.axiom.nucleus.db.Pushmessages"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.PushMessageManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Isologs"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.IsoUtils"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="java.text.SimpleDateFormat"%>


<%
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _sessionID = (String) session.getAttribute("_apSessionID");
    String _pushmessageSearch = request.getParameter("_pushmessageSearch");
    String _pushmappersSearch = request.getParameter("_pushmappersSearch");
    String _startdate = request.getParameter("_startdate");
    String _enddate = request.getParameter("_enddate");

    DateFormat formatter = new SimpleDateFormat("dd/mm/yyyy");
    Date startDate = null;
    if (_startdate != null && !_startdate.isEmpty()) {
        startDate = (Date) formatter.parse(_startdate);
    }
    Date endDate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        endDate = (Date) formatter.parse(_enddate);
    }

    PushMessageManagement pManagement = new PushMessageManagement();

    Pushmessages pObj = pManagement.getPushmessagebyname(_sessionID, channel.getChannelid(), _pushmessageSearch);
    ISOManagement iso = new ISOManagement();

    Isologs[] clogobj = iso.getisologsbycallerID(channel.getChannelid(), pObj.getCallerid(), _pushmappersSearch, startDate, endDate);

    String start = null;
    if (startDate != null) {
        start = formatter.format(startDate);
    }
    String end = null;
    if (endDate != null) {
        end = formatter.format(endDate);
    }

    String typeSC = "None";


%>
<h3> Results from <%=start%> to <%=end%> for Caller "<%=_pushmessageSearch%>"</i></h3>

<div class="tabbable">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#msgcharts" data-toggle="tab">Charts</a></li>
        <li><a href="#msgreport" data-toggle="tab">Tabular List</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="msgcharts">
            <div class="row-fluid">
                <div class="span12">
                    <div class="control-group">
                        <div class="span4">
                            <div id="MsgReportgraph" ></div>
                            Donut Chart
                        </div>
                        <div class="span7">
                            <div id="MsgReportgraph1"></div>
                            Bar Chart   
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="tab-pane" id="msgreport">   
            <div class="row-fluid">
                <div class="span6">
                    <div class="control-group">                        
                        <div class="span3">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="fxmsgReportCSV()" >
                                    <i class="icon-white icon-chevron-down"></i>Download CSV</a>
                            </div>
                        </div>
                        <div class="span3">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="fxmsgReportPDF()" >
                                    <i class="icon-white icon-chevron-down"></i>Download PDF</a>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped" id="table_main">
                    <tr>
                        <td>No.</td>
                        <td>Caller</td>
                        <td>Caller IP</td>
                        <td>Indicator</td>
                        <td>Sent On</td>                        
                        <td>Sent Via</td>                    
                        <td>Date</td>
                        <td>Response to Caller</td>
                    </tr>
                    <% if (clogobj != null) {
                            for (int i = 0; i < clogobj.length; i++) {

                                String strType = "";

                                String strStatus = "";

                                if (clogobj[i].getSentvia() == 1) {
                                    strType = "SMS";
                                } else if (clogobj[i].getSentvia() == 2) {
                                    strType = "VOICE";
                                } else if (clogobj[i].getSentvia() == 3) {
                                    strType = "USSD";
                                } else if (clogobj[i].getSentvia() == 4) {
                                    strType = "EMAIL";
                                } else {
                                    strType = "FAX";
                                }

//                                if (clogobj[i].getResponsevalue() == 1) {
//                                    strType = "SUCCESS";
//                                } else if (clogobj[i].getResponsevalue() == -1) {
//                                    strType = "FAILURE";
//                                }
//                                if (clogobj[i].getResponsevalue() == 1) {
//                                    strStatus = "SUCCESS";
//                                } else if (clogobj[i].getResponsevalue() == -1) {
//                                    strStatus = "FAILURE";
//                                }
//
//                                String strMSGID = clogobj[i].getIsobitmap();
//                                if (strMSGID == null || strMSGID.isEmpty() == true) {
//                                    strMSGID = "<span class='label label-Default'>" + "Not Available" + "</span>";
//                                }

                    %>
                    <tr>
                        <td><%=(i + 1)%></td>
                        <td><%=clogobj[i].getCallername()%></td>
                        <td><%=clogobj[i].getClientaddress()%></td>
                        <td><%=clogobj[i].getIsobitmap()%></td>
                        <td><%=clogobj[i].getSentOn()%></td>                        
                        <td><%=strType%></td>
                        <td><%=clogobj[i].getCreatedOn()%></td>
                        <td><%=clogobj[i].getResponsesent()%></td>

                    </tr>
                    <%}
                    } else {%>
                    <tr>
                        <td><%=1%></td>
                        <td>No Records Found</td>
                        <td>No Records Found</td> 
                        <td>No Records Found</td>
                        <td>No Records Found</td>
                        <td>No Records Found</td>
                        <td>No Records Found</td>
                        <td>No Records Found</td>
                        <%}%>
                    </tr>
                </table>



            </div>
        </div>
    </div>

    <br><br>