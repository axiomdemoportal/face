<%@page import="com.mollatech.axiom.nucleus.db.Operators"%>
<%@page import="com.mollatech.axiom.connector.access.controller.AccessMatrixSettings"%>
<%@page import="com.mollatech.axiom.nucleus.settings.SendNotification"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="java.text.SimpleDateFormat"%>


<%
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _searchtext = request.getParameter("_searchtext");
    String _startdate = request.getParameter("_startdate");
    String _enddate = request.getParameter("_enddate");
    String _type = request.getParameter("_type");

    if (_type == null || _type.isEmpty() == true) {
        _type = "" + SendNotification.SMS;
    }

    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
    Date startDate = null;
    if (_startdate != null && !_startdate.isEmpty()) {
        startDate = (Date) formatter.parse(_startdate);
    }
    Date endDate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        endDate = (Date) formatter.parse(_enddate);
    }
    BulkMSGManagement bMngt = new BulkMSGManagement();

    int type = Integer.parseInt(_type);
    Channellogs[] clogobj = bMngt.searchMsgObj(channel.getChannelid(), type, _searchtext, startDate, endDate);
    String start = null;
    if (startDate != null) {
        start = formatter.format(startDate);
    }
    String end = null;
    if (endDate != null) {
        end = formatter.format(endDate);
    }

    DateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy");

    String typeSC = "None";
    if (type == SendNotification.SMS) {
        typeSC = "SMS";
    } else if (type == SendNotification.USSD) {
        typeSC = "USSD";
    } else if (type == SendNotification.VOICE) {
        typeSC = "VOICE";
    } else if (type == SendNotification.EMAIL) {
        typeSC = "EMAIL";
    } else if (type == SendNotification.FAX) {
        typeSC = "FAX";
    }

%>
<h3> Results from <%=start%> to <%=end%> for <%=typeSC%> type sent to "<%=_searchtext%>"</i></h3>

<div class="tabbable">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#msgcharts" data-toggle="tab">Charts</a></li>
        <li><a href="#msgreport" data-toggle="tab">Tabular List</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="msgcharts">
            <div class="row-fluid">
                <div class="span12">
                    <div class="control-group">
                        <div class="span4">
                            <div id="MsgReportgraph" ></div>
                            Donut Chart
                        </div>
                        <div class="span7">
                            <div id="MsgReportgraph1"></div>
                            Bar Chart   
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="tab-pane" id="msgreport">   
            <div class="row-fluid">
                <div class="span6">
                    <div class="control-group">                        
                        <div class="span1">
                            <div class="control-group form-inline">
                                <!--                                <a href="#" class="btn btn-info" onclick="msgReportCSV()" >
                                                                    <i class="icon-white icon-chevron-down"></i>Download CSV</a>-->
                                <%Operators oprObjI = (Operators) session.getAttribute("_apOprDetail");
                                    AccessMatrixSettings accessObjN = (AccessMatrixSettings) session.getAttribute("_apAccessEntry");
                                    if (oprObjI.getRoleid() != 1) {//1 sysadmin
//                                AccessMatrixSettings accessObjN = (AccessMatrixSettings) session.getAttribute("_apAccessEntry");
                                        if (accessObjN != null && accessObjN.downloadMessageReport == true) {%>
                                <a href="#" class="btn btn-info" onclick="msgReportCSV()" >
                                    <%} else {%>
                                    <a href="#" class="btn btn-info" onclick="InvalidRequestMSG('msgreportdownload')" >
                                        <%}
                                        } else {%>
                                        <a href="#" class="btn btn-info" onclick="msgReportCSV()" >
                                            <%}%>
                                            <i class="icon-white icon-chevron-down"></i> CSV</a>
                                        </div>
                                        </div>
                                        <div class="span1">
                                            <div class="control-group form-inline">
                                                <!--                                <a href="#" class="btn btn-info" onclick="msgReportPDF()" >
                                                                                    <i class="icon-white icon-chevron-down"></i>Download PDF</a>-->
                                                <%    if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                        if (accessObjN != null && accessObjN.downloadMessageReport == true) {%>
                                                <a href="#" class="btn btn-info" onclick="msgReportPDF()" >
                                                    <%} else {%>
                                                    <a href="#" class="btn btn-info" onclick="InvalidRequestMSG('msgreportdownload')" >
                                                        <%}
                                                        } else {%>
                                                        <a href="#" class="btn btn-info" onclick="msgReportPDF()" >
                                                            <%}%>
                                                            <i class="icon-white icon-chevron-down"></i> PDF</a>
                                                        </div>
                                                        </div>
                                                        <div class="span1">
                                                            <div class="control-group form-inline">
                                                                <!--                                <a href="#" class="btn btn-info" onclick="msgReportTXT()" >
                                                                                                    <i class="icon-white icon-chevron-down"></i>Download TXT</a>-->
                                                                <%    if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                        if (accessObjN != null && accessObjN.downloadMessageReport == true) {%>
                                                                <a href="#" class="btn btn-info" onclick="msgReportTXT()" >
                                                                    <%} else {%>
                                                                    <a href="#" class="btn btn-info" onclick="InvalidRequestMSG('msgreportdownload')" >
                                                                        <%}
                                                                        } else {%>
                                                                        <a href="#" class="btn btn-info" onclick="msgReportTXT()" >
                                                                            <%}%>
                                                                            <i class="icon-white icon-chevron-down"></i> TXT</a>
                                                                        </div>
                                                                        </div>
                                                                        </div>
                                                                        </div>
                                                                        <table class="table table-striped" id="table_main">

                                                                            <tr>
                                                                                <td>No.</td>
                                                                                <td>Message ID</td>
                                                                                <td>Type</td>
                                                                                <%if (type == 1 || type == 2 || type == 3) {%>
                                                                                <td>Phone</td>
                                                                                <%} else if (type == 4) {%>
                                                                                <td>Email</td>
                                                                                <%} else if (type == 5) {%>
                                                                                <td>Fax No</td>
                                                                                <%}%>
                                                                                <td>Retries</td>
                                                                                <td>Gateway</td>
                                                                                <td>Status</td>
                                                                                <td>Cost</td>
                                                                                <td>Sent On</td>
                                                                                <td>Update On</td>
                                                                            </tr>
                                                                            <% if (clogobj != null) {
                                                                                    for (int i = 0; i < clogobj.length; i++) {
                                                                                        String strType = "";
                                                                                        String strSource = "";
                                                                                        //                                        final int PENDING = 0;
                                                                                        //                                        final int SENT = 1;
                                                                                        //                                        final int FAILD = -1;

                                                                                        final int SENT = 0;
                                                                                        final int PENDING = 2;
                                                                                        final int FAILED = -1;
                                                                                        final int BLOCKED = -5;

                                                                                        String strStatus = "";

                                                                                        if (clogobj[i].getType() == 1) {
                                                                                            strType = "SMS";
                                                                                        } else if (clogobj[i].getType() == 2) {
                                                                                            strType = "VOICE";
                                                                                        } else if (clogobj[i].getType() == 3) {
                                                                                            strType = "USSD";
                                                                                        } else if (clogobj[i].getType() == 4) {
                                                                                            strType = "EMAIL";
                                                                                        } else if (clogobj[i].getType() == 5) {
                                                                                            strType = "FAX";
                                                                                        } else if (clogobj[i].getType() == 18) {
                                                                                            strType = "Android Push";

                                                                                        } else if (clogobj[i].getType() == 19) {
                                                                                            strType = "APNS Push";

                                                                                        } else if (clogobj[i].getType() == 33) {
                                                                                            strType = "Web Push";

                                                                                        }

                                                                                        if (clogobj[i].getSourceid() == 1) {
                                                                                            strSource = "BULK MSG";
                                                                                        } else if (clogobj[i].getSourceid() == 2) {
                                                                                            strSource = "BULK MSG";
                                                                                        } else if (clogobj[i].getSourceid() == 3) {
                                                                                            strSource = "BULK MSG";
                                                                                        } else if (clogobj[i].getSourceid() == 4) {
                                                                                            strSource = "BULK MSG";
                                                                                        }
                                                                                        if (clogobj[i].getStatus() == PENDING) {
                                                                                            strStatus = "PENDING";
                                                                                        } else if (clogobj[i].getStatus() == SENT) {
                                                                                            strStatus = "SENT";
                                                                                        } else if (clogobj[i].getStatus() == FAILED) {
                                                                                            strStatus = "FAILED";
                                                                                        } else if (clogobj[i].getStatus() == BLOCKED) {
                                                                                            strStatus = "BLOCKED";
                                                                                        }

                                                                                        String strMSGID = clogobj[i].getMsgid();
                                                                                        if (strMSGID == null || strMSGID.isEmpty() == true) {
                                                                                            strMSGID = "<span class='label label-Default'>" + "Not Available" + "</span>";
                                                                                        }

                                                                            %>
                                                                            <tr>
                                                                                <td><%=(i + 1)%></td>
                                                                                <td><%=strMSGID%></td>
                                                                                <td><%=strType%></td>
                                                                                <%if (type == 1 || type == 2 || type == 3) {%>
                                                                                <td><%=clogobj[i].getPhone()%></td>
                                                                                <%} else if (type == 4) {%>
                                                                                <td><%=clogobj[i].getEmail()%></td> 
                                                                                <%} else if (type == 5) {%>
                                                                                <td><%=clogobj[i].getPhone()%></td> 
                                                                                <%}%>
                                                                                <td><%=clogobj[i].getRetrycount()%></td>                        
                                                                                <%
                                                                                    String GatewayOrder = "Primary";
                                                                                    if (clogobj[i].getPreference() == 2) {
                                                                                        GatewayOrder = "Secondary";
                                                                                    } else if (clogobj[i].getPreference() == 3) {
                                                                                        GatewayOrder = "Ternary";
                                                                                    }
                                                                                %>
                                                                                <td><%=GatewayOrder%></td>                        
                                                                                <td><%=strStatus%></td> 
                                                                                <%if (clogobj[i].getCost() != null) {%>
                                                                                <td><%=clogobj[i].getCost()%></td> 
                                                                                <%} else {%>
                                                                                <td><%=0.0%></td> 
                                                                                <%}%>
                                                                                <td><%=sdf.format(clogobj[i].getSentUtctime())%></td>

                                                                                <td><%=sdf.format(clogobj[i].getLastupdateUtctime())%></td>                          
                                                                            </tr>
                                                                            <%}
                                                                            } else {%>
                                                                            <tr>
                                                                                <td><%=1%></td>
                                                                                <td>No Records Found</td>
                                                                                <td>No Records Found</td>

                                                                                <td>No Records Found</td> 

                                                                                <td>No Records Found</td>
                                                                                <td>No Records Found</td>
                                                                                <td>No Records Found</td>
                                                                                <td>No Records Found</td>
                                                                                <td>No Records Found</td>
                                                                                <td>No Records Found</td>


                                                                                <%}%>
                                                                            </tr>
                                                                        </table>

                                                                        <div id="dkdkd">
                                                                            <div class="span6">
                                                                                <!--                        <div class="control-group">                        
                                                                                                            <div class="span3">
                                                                                                                <div class="control-group form-inline">
                                                                                                                    <a href="#" class="btn btn-info" onclick="msgReportCSV()" >
                                                                                                                        <i class="icon-white icon-chevron-down"></i>Download CSV</a>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="span3">
                                                                                                                <div class="control-group form-inline">
                                                                                                                    <a href="#" class="btn btn-info" onclick="msgReportPDF()" >
                                                                                                                        <i class="icon-white icon-chevron-down"></i>Download PDF</a>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="span3">
                                                                                                                <div class="control-group form-inline">
                                                                                                                    <a href="#" class="btn btn-info" onclick="msgReportTXT()" >
                                                                                                                        <i class="icon-white icon-chevron-down"></i>Download TXT</a>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>-->
                                                                                <div class="control-group">                        
                                                                                    <div class="span1">
                                                                                        <div class="control-group form-inline">
                                                                                            <!--                                <a href="#" class="btn btn-info" onclick="msgReportCSV()" >
                                                                                                                                <i class="icon-white icon-chevron-down"></i>Download CSV</a>-->
                                                                                            <% //Operators oprObjI = (Operators) session.getAttribute("_apOprDetail");
                                                                                                //                                    AccessMatrixSettings accessObjN = (AccessMatrixSettings) session.getAttribute("_apAccessEntry");
                                                                                                if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                    //                                AccessMatrixSettings accessObjN = (AccessMatrixSettings) session.getAttribute("_apAccessEntry");
                                                                                                    if (accessObjN != null && accessObjN.downloadMessageReport == true) {%>
                                                                                            <a href="#" class="btn btn-info" onclick="msgReportCSV()" >
                                                                                                <%} else {%>
                                                                                                <a href="#" class="btn btn-info" onclick="InvalidRequestMSG('msgreportdownload')" >
                                                                                                    <%}
                                                                                                    } else {%>
                                                                                                    <a href="#" class="btn btn-info" onclick="msgReportCSV()" >
                                                                                                        <%}%>
                                                                                                        <i class="icon-white icon-chevron-down"></i> CSV</a>
                                                                                                    </div>
                                                                                                    </div>
                                                                                                    <div class="span1">
                                                                                                        <div class="control-group form-inline">
                                                                                                            <!--                                <a href="#" class="btn btn-info" onclick="msgReportPDF()" >
                                                                                                                                                <i class="icon-white icon-chevron-down"></i>Download PDF</a>-->
                                                                                                            <%    if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                                    if (accessObjN != null && accessObjN.downloadMessageReport == true) {%>
                                                                                                            <a href="#" class="btn btn-info" onclick="msgReportPDF()" >
                                                                                                                <%} else {%>
                                                                                                                <a href="#" class="btn btn-info" onclick="InvalidRequestMSG('msgreportdownload')" >
                                                                                                                    <%}
                                                                                                                    } else {%>
                                                                                                                    <a href="#" class="btn btn-info" onclick="msgReportPDF()" >
                                                                                                                        <%}%>
                                                                                                                        <i class="icon-white icon-chevron-down"></i> PDF</a>
                                                                                                                    </div>
                                                                                                                    </div>
                                                                                                                    <div class="span1">
                                                                                                                        <div class="control-group form-inline">
                                                                                                                            <!--                                <a href="#" class="btn btn-info" onclick="msgReportTXT()" >
                                                                                                                                                                <i class="icon-white icon-chevron-down"></i>Download TXT</a>-->
                                                                                                                            <%    if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                                                    if (accessObjN != null && accessObjN.downloadMessageReport == true) {%>
                                                                                                                            <a href="#" class="btn btn-info" onclick="msgReportTXT()" >
                                                                                                                                <%} else {%>
                                                                                                                                <a href="#" class="btn btn-info" onclick="InvalidRequestMSG('msgreportdownload')" >
                                                                                                                                    <%}
                                                                                                                                    } else {%>
                                                                                                                                    <a href="#" class="btn btn-info" onclick="msgReportTXT()" >
                                                                                                                                        <%}%>
                                                                                                                                        <i class="icon-white icon-chevron-down"></i> TXT</a>
                                                                                                                                    </div>
                                                                                                                                    </div>
                                                                                                                                    </div>
                                                                                                                                    </div>
                                                                                                                                    </div>
                                                                                                                                    </div>

                                                                                                                                    </div>

                                                                                                                                    </div>

                                                                                                                                    </div>


                                                                                                                                    <br><br>