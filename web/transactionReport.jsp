<%@include file="header.jsp" %>
<!DOCTYPE html>
<html>
    <script src="./assets/js/usermanagement.js"></script>
    <body>
        <div class="container-fluid">
            <h1 class="text-success">Transaction Details</h1>

            <hr>
            <form class="form-horizontal" id="TransactionSearch" name="TransactionSearch">
                <fieldset>

                    <div class="control-group">
                        <label class="control-label"  for="_mname">User Id</label>
                        <div class="controls">
                            <input type="text" id="_uidtx" name="_uidtx" class="input-large">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            Type
                            <select  name="_typeTx" id="_typeTx">
                                <option value="" selected>Select</option>
                                <option value="1">Push</option>
                                <option value="2">SMS</option>
                                <option value="3">Voice Callback</option>
                                <option value="4">Voice Missed</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label"  for="_mname">Status</label>
                        <div class="controls">

                            <select  name="_statustx" id="_statustx">
                                <option value="" selected>Select</option>
                                <option value="-1">Expired</option>
                                <option value="0">Responded</option>
                                <option value="1">Canceled</option>
                                <option value="2">Pending</option>
                                <option value="3">Approved</option>
                                <option value="4">Denied</option>


                            </select>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Transaction Id
                            <input type="text" id="_txid" name="_txid" class="input-large">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label"  for="_emsg">From:Till</label>
                        <div class="controls">
                            <div id="datetimepicker1" class="input-append date">
                                <input id="startdatetx" name="startdatetx" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <div id="datetimepicker2" class="input-append date">
                                <input id="enddatetx" name="enddatetx" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label"  for="username"></label>
                        <div class="controls">
                            <%if (oprObj.getRoleid() != 1) {
                                     if (accessObj.viewtranscationReport == true) { %>
                            <button class="btn btn-primary" onclick="generateTxRepo()" type="button">Search</button>
                            <%} else {%>
                            <button class="btn btn-success " onclick="InvalidRequest('transactionreport')" type="button">Generate Report Now</button>
                            <%}
                            } else {%>
                            <button class="btn btn-primary" onclick="generateTxRepo()" type="button">Search</button>
                            <%}%>
                        </div>
                    </div>

                </fieldset>

            </form>
        </div>
    </body>

</html>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker2').datepicker({
            format: 'dd/MM/yyyy',
            language: 'pt-BR'
        });
    });
    $(function () {
        $('#datetimepicker1').datepicker({
            format: 'dd/MM/yyyy',
            language: 'pt-BR'
        });
    });
//    ChangeMediaType(0);
</script>
<%@include file="footer.jsp" %>