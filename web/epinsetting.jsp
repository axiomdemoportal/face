<%@include file="header.jsp" %>
<script src="./assets/js/epinsetting.js"></script>
<div class="container-fluid">
    <h2 class="text-success">E-PIN Settings</h2>
    <p>To facilitate automated Electronic PIN/Password Request, Generation and Delivery along with rules enforcement.</p>
    <br>

    <div class="row-fluid">
        <form class="form-horizontal" id="epinsettingsform" name="epinsettingsform">

            <input type="hidden" id="_pinDeliveryType" name="_pinDeliveryType">
            <input type="hidden" id="_channelType" name="_channelType">
            <input type="hidden" id="_channelType2" name="_channelType2">
            <input type="hidden" id="_dayRestriction" name="_dayRestriction">
            <input type="hidden" id="_timeFromRestriction" name="_timeFromRestriction">
            <input type="hidden" id="_timeToRestriction" name="_timeToRestriction">
            <input type="hidden" id="_splitduration" name="_splitduration">
            <input type="hidden" id="_expiryTime" name="_expiryTime">
            <input type="hidden" id="_pinRequestCountDuration" name="_pinRequestCountDuration">
            <input type="hidden" id="_pinRequestCount" name="_pinRequestCount">
            <input type="hidden" id="_operatorController" name="_operatorController">


            <input type="hidden" id="_isAlertOperatorOnFailure" name="_isAlertOperatorOnFailure">
            <input type="hidden" id="_PINSource" name="_PINSource">
            <input type="hidden" id="_PINLength" name="_PINLength">
            <input type="hidden" id="_PINValidity" name="_PINValidity">
            
              <input type="hidden" id="_ChallengeResponse" name="_ChallengeResponse">
             <input type="hidden" id="_smsurlStatus" name="_smsurlStatus">
             <input type="hidden" id="_voiceurlStatus" name="_voiceurlStatus">
             <input type="hidden" id="_ussdurlStatus" name="_ussdurlStatus">
             <input type="hidden" id="_weburlStatus" name="_weburlStatus">

            <div class="control-group">
                <label class="control-label"  for="username">Delivery Type</label>
                <div class="controls">
                    <div> 
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_pinDeliveryType_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeDeliveryTypeEPIN(1, 'Single Channel Delivery')">Single Channel Delivery</a></li>
                                <li><a href="#" onclick="ChangeDeliveryTypeEPIN(2, 'Dual Channel Delivery')">Dual Channel Delivery</a></li>
                                <li><a href="#" onclick="ChangeDeliveryTypeEPIN(3, 'Single Channel Split Delivery')">Single Channel Split Delivery</a></li>
                                <li><a href="#" onclick="ChangeDeliveryTypeEPIN(4, 'Dual Channel Split Delivery')">Dual Channel Split Delivery</a></li>
                            </ul>
                        </div>
                        with 1st channel as

                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_channelType_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeChannelEPIN(1, 1, 'SMS Channel')">SMS Channel</a></li>
                                <li><a href="#" onclick="ChangeChannelEPIN(1, 2, 'USSD Channel')">USSD Channel</a></li>
                                <li><a href="#" onclick="ChangeChannelEPIN(1, 3, 'Voice Channel')">Voice Channel</a></li>
                                <li><a href="#" onclick="ChangeChannelEPIN(1, 4, 'Email Channel')">Email Channel</a></li>
                                <li><a href="#" onclick="ChangeChannelEPIN(1, 5, 'PIN Mailer')">PIN Mailer</a></li>
                            </ul>
                        </div>
                        and 2nd channel as
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_channelType2_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeChannelEPIN(2, 1, 'SMS Channel')">SMS Channel</a></li>
                                <li><a href="#" onclick="ChangeChannelEPIN(2, 2, 'USSD Channel')">USSD Channel</a></li>
                                <li><a href="#" onclick="ChangeChannelEPIN(2, 3, 'Voice Channel')">Voice Channel</a></li>
                                <li><a href="#" onclick="ChangeChannelEPIN(2, 4, 'Email Channel')">Email Channel</a></li>
                                <li><a href="#" onclick="ChangeChannelEPIN(2, 5, 'PIN Mailer')">PIN Mailer</a></li>
                            </ul>
                        </div>
                        with split duration as
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_splitduration_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeSplitDurationEPIN(2, '2 mins')">2 mins</a></li>
                                <li><a href="#" onclick="ChangeSplitDurationEPIN(5, '5 mins')">5 mins</a></li>
                                <li><a href="#" onclick="ChangeSplitDurationEPIN(10, '10 mins')">10 mins</a></li>
                                <li><a href="#" onclick="ChangeSplitDurationEPIN(15, '15 mins')">15 mins</a></li>
                                <li><a href="#" onclick="ChangeSplitDurationEPIN(30, '30 mins')">30 mins</a></li>                            
                            </ul>                        
                        </div>
                    </div>
                </div>
            </div>
            <hr>


            <div class="control-group">
                <label class="control-label"  for="username">Day Restriction</label>
                <div class="controls">
                    <div>
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_dayRestriction_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeDayRestrictionEPIN(1, 'Week days only')">Week days only</a></li>
                                <li><a href="#" onclick="ChangeDayRestrictionEPIN(2, 'Whole Week (including Weekend)')">Whole Week (including Weekend)</a></li>
                            </ul>
                        </div>
                        time between 
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_timeFromRestriction_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeTimeRestrictionEPIN(1, 7, '7AM')">7am</a></li>
                                <li><a href="#" onclick="ChangeTimeRestrictionEPIN(1, 8, '8AM')">8am</a></li>
                                <li><a href="#" onclick="ChangeTimeRestrictionEPIN(1, 9, '9AM')">9am</a></li>
                                <li><a href="#" onclick="ChangeTimeRestrictionEPIN(1, 10, '10AM')">10am</a></li>
                                <li><a href="#" onclick="ChangeTimeRestrictionEPIN(1, 11, '11AM')">11am</a></li>
                                <li><a href="#" onclick="ChangeTimeRestrictionEPIN(1, 0, 'Any')">Any</a></li>
                            </ul>                        
                        </div>
                        to 
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_timeToRestriction_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeTimeRestrictionEPIN(2, 12, '12PM')">12pm</a></li>
                                <li><a href="#" onclick="ChangeTimeRestrictionEPIN(2, 13, '1PM')">1pm</a></li>
                                <li><a href="#" onclick="ChangeTimeRestrictionEPIN(2, 14, '2PM')">2pm</a></li>
                                <li><a href="#" onclick="ChangeTimeRestrictionEPIN(2, 15, '3PM')">3pm</a></li>
                                <li><a href="#" onclick="ChangeTimeRestrictionEPIN(2, 16, '4PM')">4pm</a></li>
                                <li><a href="#" onclick="ChangeTimeRestrictionEPIN(2, 17, '5PM')">5pm</a></li>
                                <li><a href="#" onclick="ChangeTimeRestrictionEPIN(2, 18, '6PM')">6pm</a></li>
                                <li><a href="#" onclick="ChangeTimeRestrictionEPIN(2, 19, '7PM')">7pm</a></li>
                                <li><a href="#" onclick="ChangeTimeRestrictionEPIN(2, 24, 'Any')">Any</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <hr>

            <div class="control-group">
                <label class="control-label"  for="username">Request Threshold</label>
                <div class="controls">
                    <div>
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_pinRequestCount_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangePinRequestCountEPIN(1, '1 request')">1 request</a></li>
                                <li><a href="#" onclick="ChangePinRequestCountEPIN(3, '3 requests')">3 requests</a></li>
                                <li><a href="#" onclick="ChangePinRequestCountEPIN(5, '5 requests')">5 requests</a></li>
                                <li><a href="#" onclick="ChangePinRequestCountEPIN(10, '10 requests')">10 requests</a></li>
                                <li><a href="#" onclick="ChangePinRequestCountEPIN(-1, 'No Limit')">No Limit</a></li>                            
                            </ul>
                        </div>
                        in duration of
                        <div class="btn-group">
                            in a 
                            <button class="btn btn-small"><div id="_pinRequestCountDuration_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangePinRequestCountDurationEPIN(1, 'Day')">Day</a></li>
                                <li><a href="#" onclick="ChangePinRequestCountDurationEPIN(2, 'Week')">Week</a></li>
                                <li><a href="#" onclick="ChangePinRequestCountDurationEPIN(3, 'Month')">Month</a></li>
                            </ul>                        
                        </div>
                        with request expiry time limit as 
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_expiryTime_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeExpiryTimeLimitEPIN(1, '1 min')">1 min</a></li>
                                <li><a href="#" onclick="ChangeExpiryTimeLimitEPIN(2, '2 mins')">2 mins</a></li>
                                <li><a href="#" onclick="ChangeExpiryTimeLimitEPIN(5, '5 mins')">5 mins</a></li>
                                <li><a href="#" onclick="ChangeExpiryTimeLimitEPIN(10, '10 mins')">10 mins</a></li>
                            </ul>                        
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="control-group">
                <label class="control-label"  for="username">Operator Approval?</label>
                <div class="controls">
                    <div>
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_operatorController_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeOperatorControllerEPIN(0, 'No, Not Required')">No, Not Required</a></li>                            
                                <li><a href="#" onclick="ChangeOperatorControllerEPIN(1, 'Yes, Single Operator Required')">Yes, Single Operator Required</a></li> 
                                 <li><a href="#" onclick="ChangeOperatorControllerEPIN(2, 'Yes, Dual Operator Required')">Yes, Dual Operator Required</a></li> 
                            </ul>
                        </div>
                        in addition, inform operators in case of failure   
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_isAlertOperatorOnFailure_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeAlertOperatorOnFailureEPIN(false, 'No, Do not inform on EPIN Failure.')">No, Do not inform on EPIN Failure.</a></li>                            
                                <li><a href="#" onclick="ChangeAlertOperatorOnFailureEPIN(true, 'Yes, Do inform on EPIN Failure')">Yes, Do inform on EPIN Failure</a></li>                            
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
            <hr>

            <div class="control-group">
                <label class="control-label"  for="username">Other module</label>
                <div class="controls">
                    <div>
                        Epin Generation
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_PINSource_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangePINFormEPIN(1, 'Internally by Axiom')">Internally by Axiom</a></li>
                                <li><a href="#" onclick="ChangePINFormEPIN(2, 'External By Third party Source')">External By Third party Source</a></li>
                            </ul>
                            
                        </div>
                        , Challenge Response Module
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_ChallengeResponse_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeChallengeResponseForm(1, 'Internally by Axiom')">Internally by Axiom</a></li>
                                <li><a href="#" onclick="ChangeChallengeResponseForm(2, 'External By Third party Source')">External By Third party Source</a></li>
                            </ul>
                        </div>
                        , SMS Identifier
                          <input type="text" id="_smstext" name="_smstext" placeholder="eg. PIN,ACTIVE,EPIN" class="span1">                             
<!--                        <div id="hideclassTexxtBox">
                              <input type="text" id="_PinSourceClassNameclassname" name="_PinSourceClassNameclassname" placeholder="Enter complete class name including package" class="input-xlarge">
                         </div> -->
                    </div>
                       
                </div>
            </div>
             <div class="control-group" id="hidePinSourcetxtbox">
                <label class="control-label"  for="username">Enter Class Name</label>
                <div class="controls">
                     <input type="text" id="_PinSourceClassNameclassname" name="_PinSourceClassNameclassname" placeholder="Enter complete class name including package" class="input-xlarge">
                </div>
            </div>
            <!--<hr>-->
            <!--<hr>-->

             <div class="control-group" id="hideChallengeResponsetxtbox">
                <label class="control-label"  for="username">Enter Class Name</label>
                <div class="controls">
                      <input type="text" id="_ChallengeResponseclassname" name="_ChallengeResponseclassname" placeholder="Enter complete class name including package" class="input-xlarge">
                </div>
            </div>
            <hr>
<!--            <div class="control-group">
                <label class="control-label"  for="username">SMS Text</label>
                <div class="controls">
                    <input type="text" id="_smstext" name="_smstext" placeholder="eg. PIN,ACTIVE,EPIN" class="span3">                             
                </div>
            </div>-->
<!--
            <hr>-->

            <div class="control-group">
                <label class="control-label"  for="username">Response URL</label>
                <div class="controls">
                    SMS
                    <input type="text" id="_smsurl" name="_smsurl" placeholder="Enter SMS Response URL" class="input-xlarge" readonly="true">                             
                    mark as
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_smsurlStatus_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="ChangeSMSResponseURLStatus(1, 'Active')">Active</a></li>
                            <li><a href="#" onclick="ChangeSMSResponseURLStatus(0, 'In-Active')">In-Active</a></li>
                        </ul>
                    </div>
                     VOICE
                    <input type="text" id="_voiceurl" name="_voiceurl" placeholder="Enter VOICE Response URL" class="input-xlarge" readonly="true">                             
                   mark as
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_voiceurlStatus_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="ChangeVOICEResponseURLStatus(1, 'Active')">Active</a></li>
                            <li><a href="#" onclick="ChangeVOICEResponseURLStatus(0,'In-Active')">In-Active</a></li>
                        </ul>
                    </div>
                </div>
            </div>
<!--            <div class="control-group">
                <label class="control-label"  for="username">Voice Response URL</label>
                <div class="controls">
                    <input type="text" id="_voiceurl" name="_voiceurl" placeholder="Enter VOICE Response URL" class="input-xlarge" readonly="true">                             
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_voiceurlStatus_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="ChangeVOICEResponseURLStatus(1, 'Active')">Active</a></li>
                            <li><a href="#" onclick="ChangeVOICEResponseURLStatus(0,'In-Active')">In-Active</a></li>
                        </ul>
                    </div>
                </div>
            </div>-->
            
             <div class="control-group">
                <!--<label class="control-label"  for="username">USSD Response URL</label>-->
                <div class="controls">
                    USSD
                    <input type="text" id="_ussdurl" name="_ussdurl" placeholder="Enter USSD Response URL" class="input-xlarge" readonly="true" >                             
                   mark as
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_ussdurlStatus_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="ChangeUSSDResponseURLStatus(1, 'Active')">Active</a></li>
                            <li><a href="#" onclick="ChangeUSSDResponseURLStatus(0, 'In-Active')">In-Active</a></li>
                        </ul>
                    </div>
                    WEB
                    <input type="text" id="_weburl" name="_weburl" placeholder="Enter WEB Response URL" class="input-xlarge" readonly="true" >                             
                    mark as
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_weburlStatus_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="ChangeWEBResponseURLStatus(1, 'Active')">Active</a></li>
                            <li><a href="#" onclick="ChangeWEBResponseURLStatus(0, 'In-Active')">In-Active</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            
<!--             <div class="control-group">
                <label class="control-label"  for="username">WEB Response URL</label>
                <div class="controls">
                    <input type="text" id="_weburl" name="_weburl" placeholder="Enter WEB Response URL" class="input-xlarge" readonly="true" >                             
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_weburlStatus_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="ChangeWEBResponseURLStatus(1, 'Active')">Active</a></li>
                            <li><a href="#" onclick="ChangeWEBResponseURLStatus(0, 'In-Active')">In-Active</a></li>
                        </ul>
                    </div>
                </div>
            </div>-->

            <hr>
            <!--            <div class="control-group">
                            <label class="control-label"  for="username">Implementation Class</label>
                            <div class="controls">
                                <input type="text" id="_classname" name="_classname" placeholder="complete class name including package" class="input-xlarge">
                            </div>
                        </div>-->
            <!-- Submit -->
            
<!--            <div class="control-group">
                <div class="controls">
                    <div id="save-epin-settings-result"></div>
                    <button class="btn btn-primary" onclick="editEPINSettings()" type="button">Save PIN Setting Now >> </button>
                </div>
            </div>-->
  <%//if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
            <div class="control-group">
                <div class="controls">
                    <div id="save-epin-settings-result"></div>
                    <button class="btn btn-primary" onclick="editEPINSettings()" type="button">Save PIN Setting Now >> </button>
                </div>
            </div>

            <%//}%>

        </form>
    </div>


    <script language="javascript" type="text/javascript">
        LoadEPINSettings();
    </script>
</div>


<%@include file="footer.jsp" %>