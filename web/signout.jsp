<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.Operators"%>
<%@ page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String sessionid = (String)session.getAttribute("_apSessionID");
  //  System.out.println("Sessionid from signout.jsp "+sessionid);
     Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
  //  System.out.println("operatorS from signout.jsp "+operatorS);
     Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    OperatorsManagement oManagement = new OperatorsManagement();
    if(operatorS!= null && channel!=null ){
    oManagement.ChangeLoginStatus(channel.getChannelid(), operatorS.getOperatorid(), OperatorsManagement.LOGIN_INACTIVE);
    }SessionManagement sm = new SessionManagement();
    sm.CloseSession(sessionid);
    session.invalidate();
%>
<jsp:forward page="index.jsp" />

