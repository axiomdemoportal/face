<%@include file="header.jsp" %>
<script src="./assets/js/globalsetting.js"></script>
<div class="container-fluid">
    <h1 class="text-success">Global Settings</h1>
    <p>To facilitate mobile and email messages in bulk, you can define your own speed for 4 levels provided.</p>
    <br>
    <div class="row-fluid">
        <form class="form-horizontal" id="globalsettingsform" name="globalsettingsform">
            
            <input type="hidden" id="_slowPaceMobile" name="_slowPaceMobile">
            <input type="hidden" id="_normalPaceMobile" name="_normalPaceMobile">
            <input type="hidden" id="_fastPaceMobile" name="_fastPaceMobile">
            <input type="hidden" id="_hyperPaceMobile" name="_hyperPaceMobile">
            <input type="hidden" id="_slowPaceEmail" name="_slowPaceEmail">
            <input type="hidden" id="_normalPaceEmail" name="_normalPaceEmail">
            <input type="hidden" id="_fastPaceEmail" name="_fastPaceEmail">
            <input type="hidden" id="_hyperPaceEmail" name="_hyperPaceEmail">
              <input type="hidden" id="_voiceSetting" name="_voiceSetting" value="1">
              <input type="hidden" id="_weigtage" name="_weigtage">
            <input type="hidden" id="_answersattempts" name="_answersattempts">
            
            
              
              <div class="control-group">

                <label class="control-label"  for="username">Challenge Response Settings: </label>

                <div class="controls">
                    <div> 
                        Percentage:  
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_weigtage_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="SetPercentage(75, '75 Percent')">75 percent</a></li>
                                <li><a href="#" onclick="SetPercentage(80, '80 Percent')">80 percent</a></li>
                                <li><a href="#" onclick="SetPercentage(90, '90 Percent')">90 percent</a></li>
                                <li><a href="#" onclick="SetPercentage(100, '100 Percent')">100 percent</a></li>                                
                            </ul>
                        </div>
                        Attempts 
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_answersattempts_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="setAttempts(3, '3 attempts')">3 attempts</a></li>
                                <li><a href="#" onclick="setAttempts(5, '5 attempts')">5 attempts</a></li>
                                <li><a href="#" onclick="setAttempts(7, '7 attempts')">7 attempts</a></li>

                            </ul>
                        </div>

                    </div>
                </div>
            </div>
            <hr>
            
            
            

            <div class="control-group">
                <label class="control-label"  for="username">Mobile Settings: </label>
                <div class="controls">
                    <div> 
                        Slow pace:  
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_slowPaceMobile_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="SetPACE(1,1,'#_slowPaceMobile_div','1 message/second')">1 message/second</a></li>
                                <li><a href="#" onclick="SetPACE(1,3,'#_slowPaceMobile_div','3 messages/second')">3 messages/second</a></li>
                                <li><a href="#" onclick="SetPACE(1,5,'#_slowPaceMobile_div','5 messages/second')">5 messages/second</a></li>
                            </ul>
                        </div>
                        , Normal Pace: 
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_normalPaceMobile_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="SetPACE(2,6,'#_normalPaceMobile_div','6 messages/second')">6 messages/second</a></li>
                                <li><a href="#" onclick="SetPACE(2,8,'#_normalPaceMobile_div','8 messages/second')">8 messages/second</a></li>
                                <li><a href="#" onclick="SetPACE(2,10,'#_normalPaceMobile_div','10 messages/second')">10 messages/second</a></li>
                            </ul>
                        </div>
                        , Fast Pace: 
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_fastPaceMobile_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="SetPACE(3,15,'#_fastPaceMobile_div','15 messages/second')">15 messages/second</a></li>
                                <li><a href="#" onclick="SetPACE(3,20,'#_fastPaceMobile_div','20 messages/second')">20 messages/second</a></li>
                                <li><a href="#" onclick="SetPACE(3,25,'#_fastPaceMobile_div','25 messages/second')">25 messages/second</a></li>
                                <li><a href="#" onclick="SetPACE(3,30,'#_fastPaceMobile_div','30 messages/second')">30 messages/second</a></li>                                
                            </ul>
                        </div>
                        and Hyper Pace: 
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_hyperPaceMobile_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="SetPACE(4,35,'#_hyperPaceMobile_div','35 messages/second')">35 messages/second</a></li>
                                <li><a href="#" onclick="SetPACE(4,50,'#_hyperPaceMobile_div','50 messages/second')">50 messages/second</a></li>
                                <li><a href="#" onclick="SetPACE(4,75,'#_hyperPaceMobile_div','75 messages/second')">75 messages/second</a></li>
                                <li><a href="#" onclick="SetPACE(4,100,'#_hyperPaceMobile_div','100 messages/second')">100 messages/second</a></li>                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <hr>

            <div class="control-group">
                <label class="control-label"  for="username">Email Settings:</label>
                <div class="controls">
                    <div> 
                        Slow pace:  
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_slowPaceEmail_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="SetPACEEmail(1,1,'#_slowPaceEmail_div','1 message/second')">1 message/second</a></li>
                                <li><a href="#" onclick="SetPACEEmail(1,3,'#_slowPaceEmail_div','3 messages/second')">3 messages/second</a></li>
                                <li><a href="#" onclick="SetPACEEmail(1,5,'#_slowPaceEmail_div','5 messages/second')">5 messages/second</a></li>
                            </ul>
                        </div>
                        , Normal Pace: 
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_normalPaceEmail_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="SetPACEEmail(2,6,'#_normalPaceEmail_div','6 messages/second')">6 messages/second</a></li>
                                <li><a href="#" onclick="SetPACEEmail(2,8,'#_normalPaceEmail_div','8 messages/second')">8 messages/second</a></li>
                                <li><a href="#" onclick="SetPACEEmail(2,10,'#_normalPaceEmail_div','10 messages/second')">10 messages/second</a></li>
                            </ul>
                        </div>
                        , Fast Pace: 
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_fastPaceEmail_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="SetPACEEmail(3,15,'#_fastPaceEmail_div','15 message/second')">15 messages/second</a></li>
                                <li><a href="#" onclick="SetPACEEmail(3,20,'#_fastPaceEmail_div','20 message/second')">20 messages/second</a></li>
                                <li><a href="#" onclick="SetPACEEmail(3,25,'#_fastPaceEmail_div','25 message/second')">25 messages/second</a></li>
                                <li><a href="#" onclick="SetPACEEmail(3,30,'#_fastPaceEmail_div','30 message/second')">30 messages/second</a></li>                                
                            </ul>
                        </div>
                        and Hyper Pace: 
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_hyperPaceEmail_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="SetPACEEmail(4,35,'#_hyperPaceEmail_div','35 message/second')">35 messages/second</a></li>
                                <li><a href="#" onclick="SetPACEEmail(4,50,'#_hyperPaceEmail_div','50 messages/second')">50 messages/second</a></li>
                                <li><a href="#" onclick="SetPACEEmail(4,75,'#_hyperPaceEmail_div','75 messages/second')">75 messages/second</a></li>
                                <li><a href="#" onclick="SetPACEEmail(4,100,'#_hyperPaceEmail_div','100 messages/second')">100 messages/second</a></li>                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <input type="hidden" id="_FXListner" name="_FXListner" placeholder="e.g. 3333" class="span1" value="2222">
            <input type="hidden" id="_vduration" name="_vduration" placeholder="e.g. 30" class="span1" value="30">
            <!--
            <div class="control-group">
                <label class="control-label"  for="username">FX Message Listener:</label>
                <div class="controls">
                    Run at port <input type="text" id="_FXListner" name="_FXListner" placeholder="e.g. 3333" class="span1" value="2222">
                </div>
            </div>
            <hr>
             <div class="control-group">
                <label class="control-label"  for="username">Voice Settings:</label>
                <div class="controls">
                    Voice Message Duration(in Sec) <input type="text" id="_vduration" name="_vduration" placeholder="e.g. 30" class="span1">
                   Replay or Not:
                    <div class="btn-group">
                            <button class="btn btn-small"><div id="_voiceSetting_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="SetVoiceSetting(1,'#_voiceSetting_div','yes')">Yes</a></li>
                                <li><a href="#" onclick="SetVoiceSetting(2,'#_voiceSetting_div','no')">No</a></li>
                              
                            </ul>
                   </div>
                </div>                
            </div>
            <hr>
            -->
            <div class="control-group">
                <div class="controls">
                    <div id="save-global-settings-result"></div>
                    <button class="btn btn-primary" onclick="editGlobalSettings()" type="button">Save GLobal Settings Now >> </button>
                </div>
            </div>

        </form>
    </div>


    <script language="javascript" type="text/javascript">
        LoadGlobalSettings();        
    </script>
</div>


<%@include file="footer.jsp" %>