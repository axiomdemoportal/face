<%@include file="header.jsp" %>
<script src="./assets/js/addSettings.js"></script>
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<%    String values = request.getParameter("val");

    if (values != null) {
        session.setAttribute("startOrstop", values);
        if (values.equals("0")) {
%>
<script>

    bootbox.confirm("<span><font color=blue><h2>All Monitors Stopped Successfully!!!!</h2></font></span>", function(result) {
        fresh();
    });

</script>
<% } else {%>
<script>

    bootbox.confirm("<span><font color=blue><h2>All Monitors Started Successfully!!!!</h2></font></span>", function(result) {
        fresh();
    });

</script>
<% }
    }
%>
<div class="container-fluid">
    <h1 class="text-success">Web Watch & Seal Management</h1>
    <p>This is used to add settings of Web Watch.</p>

    <a href="listSettings.jsp?val=0" class="btn btn-primary" >Suspend All</a>
    <a href="listSettings.jsp?val=1" class="btn btn-primary" >Activate All</a>
    <div id="settings_table_main1">
    </div>
    <a href="addSetting.jsp" class="btn btn-primary" >Add New Watch</a>

    <script>
        listSettings();
    </script>
</div>
<%@include file="footer.jsp" %>