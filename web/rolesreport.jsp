
<%@page import="com.mollatech.axiom.nucleus.db.Audit"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.AuditManagement"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.mollatech.axiom.nucleus.db.Units"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet"%>
<%@page import="com.mollatech.axiom.nucleus.db.operation.AxiomOperator"%>
<%@page import="com.mollatech.axiom.nucleus.db.Roles"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.RolesUtils"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Operators"%>
<%@page import="com.mollatech.axiom.connector.access.controller.AccessMatrixSettings"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<script src="assets/js/usermanagement.js" type="text/javascript"></script>
<script src="./assets/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="./assets/css/jquery.dataTables.min.css">
<script src="assets/DataTable/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>

<%
    final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;
    final int LOCKED_STATUS = -1;
    final int REMOVE_STATUS = -99;
    final int ROLEID = -999;
    List<String> creatBy = new ArrayList<String>();
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _searchtext = request.getParameter("_searchtext");
    String _changeStatus = request.getParameter("_status");
    String _fromDate = request.getParameter("_fromDate");
    String _toDate = request.getParameter("_toDate");
    Date dstartdate = null;
    Date denddate = null;
    UnitsManagemet unitInfo = new UnitsManagemet();
    List<String> unitnamelist = new ArrayList<String>();
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    if (_fromDate != null && _toDate != null && !_fromDate.equals("") && !_toDate.equals("")) {

        dstartdate = sdf.parse(_fromDate);
        denddate = sdf.parse(_toDate);
        denddate.setHours(23);
        denddate.setMinutes(59);
        denddate.setSeconds(59);
    }
    if (!_toDate.equals("") && _toDate != null) {
        denddate = sdf.parse(_toDate);
        denddate.setHours(23);
        denddate.setMinutes(59);
        denddate.setSeconds(59);
    }
    int istatus = Integer.parseInt(_changeStatus);
//    UserManagement usermngObj = new UserManagement();
//    AuthUser user[] = null;
    //user = usermngObj.SearchUserByStatus(sessionId, channel.getChannelid(), _searchtext, istatus);
//    user = usermngObj.SearchUserByStatusBetDate(sessionId, channel.getChannelid(), _searchtext, istatus,dstartdate,denddate);
//    session.setAttribute("inventoryUserReport", user);
//    String strerr = "No Record Found";
    OperatorsManagement opDetails = new OperatorsManagement();
    AxiomOperator[] oprData = null;
    Roles role = null;
    if (_searchtext.equalsIgnoreCase("All") && !_fromDate.isEmpty() && !_toDate.isEmpty()) {
        _searchtext = "All";
        oprData = opDetails.GetOperatorsUsingDatesnRole(sessionId, channel.getChannelid(), dstartdate, denddate, ROLEID, 1);
        session.setAttribute("userRoleReport", oprData);
    }else if(_searchtext.equalsIgnoreCase("All") && _fromDate.isEmpty() && _toDate.isEmpty()){
        oprData = opDetails.ListOperators(sessionId, channel.getChannelid());
        session.setAttribute("userRoleReport", oprData);
    }else if(_searchtext.equalsIgnoreCase("All") && _fromDate.isEmpty() && !_toDate.isEmpty()){
        oprData = opDetails.GetOperatorsUsingDatesnRole(sessionId, channel.getChannelid(), dstartdate, denddate, ROLEID, 1);
        session.setAttribute("userRoleReport", oprData);
    }else {
        role = opDetails.getRoleByName(channel.getChannelid(), _searchtext);
    }
    if (role != null && _fromDate != "" && _toDate != "") {
        oprData = opDetails.GetOperatorsUsingDatesnRole(sessionId, channel.getChannelid(), dstartdate, denddate, role.getRoleid(), 1);
        session.setAttribute("userRoleReport", oprData);
    } else if (role != null && _fromDate == "" && _toDate == "") {
        oprData = opDetails.GetOperatorByRoleId(channel.getChannelid(), role.getRoleid());
        session.setAttribute("userRoleReport", oprData);
    } else if (role != null && _fromDate.isEmpty() && !_toDate.isEmpty()) {
        oprData = opDetails.GetOperatorByEndDate(sessionId, channel.getChannelid(), denddate, role.getRoleid());
        session.setAttribute("userRoleReport", oprData);
    } else {
        session.setAttribute("userRoleReport", oprData);
    }
//    Operators [] oprData = opDetails.getOperatorsByDate(sessionId, channel.getChannelid(), _searchtext,dstartdate,denddate);
%>
<h3>Searched Results for <i>"<%=_searchtext%>"</i></h3>

<div class="tabbable">
    <ul class="nav nav-tabs" id="userReportTab">
        <li class="active"><a href="#userreport" data-toggle="tab">Tabular List</a></li>
        <!--<li><a href="#userreport" data-toggle="tab">Tabular List</a></li>-->
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="userreport">            
            <div class="row-fluid">
                <div class="span6">
                    <div class="control-group">                        
                        <div class="span1">
                            <div class="control-group form-inline">
                                <%Operators oprObjI = (Operators) session.getAttribute("_apOprDetail");
                                    AccessMatrixSettings accessObjN = (AccessMatrixSettings) session.getAttribute("_apAccessEntry");
                                    if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                        if (accessObjN != null && accessObjN.downloadRoleRports == true) {%>
                                <a href="#" class="btn btn-info" onclick="UserRoleReportDownload(1)" >
                                    <%} else {%>
                                    <a href="#" class="btn btn-info" onclick="InvalidRequest('UserRoleReportDownload')" >
                                        <%}
                                        } else {%>
                                        <a href="#" class="btn btn-info" onclick="UserRoleReportDownload(1)" >
                                            <%}%>
                                            <i class="icon-white icon-chevron-down"></i> CSV</a>
                                        </div>
                                        </div>
                                        <div class="span1">
                                            <div class="control-group form-inline">
                                                <%    if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                        if (accessObjN != null && accessObjN.downloadRoleRports == true) {%>
                                                <a href="#" class="btn btn-info" onclick="UserRoleReportDownload(0)" >
                                                    <%} else {%>
                                                    <a href="#" class="btn btn-info" onclick="InvalidRequest('UserRoleReportDownload')" >
                                                        <%}
                                                        } else {%>
                                                        <a href="#" class="btn btn-info" onclick="UserRoleReportDownload(0)" >
                                                            <%}%>
                                                            <i class="icon-white icon-chevron-down"></i> PDF</a>
                                                        </div>
                                                        </div>
                                                        <div class="span1">
                                                            <div class="control-group form-inline">
                                                                <!--<a href="#" class="btn btn-info" onclick="userReportTXT()" >-->
                                                                <!--<i class="icon-white icon-chevron-down"></i>Download TXT</a>-->
                                                                <%    if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                        if (accessObjN != null && accessObjN.downloadRoleRports == true) {%>
                                                                <a href="#" class="btn btn-info" onclick="UserRoleReportDownload(2)" >
                                                                    <%} else {%>
                                                                    <a href="#" class="btn btn-info" onclick="InvalidRequest('UserRoleReportDownload')" >
                                                                        <%}
                                                                        } else {%>
                                                                        <a href="#" class="btn btn-info" onclick="UserRoleReportDownload(2)" >
                                                                            <%}%>
                                                                            <i class="icon-white icon-chevron-down"></i> TXT</a>
                                                                        </div>
                                                                        </div>

                                                                        </div>
                                                                        </div>
                                                                        </div>
                                                                        <table class="display responsive wrap" id="table_main">
                                                                            <thead>
                                                                            <th>No.</th>
                                                                            <th>Name</th>
                                                                            <th>Email</th>
                                                                            <th>Phone no</th>
                                                                            <th>Unit</th>
                                                                            <th>Status</th>
                                                                            <th>Role</th>
                                                                            <th>Created On</th>
                                                                            <th>Created By</th>
                                                                            </thead>

                                                                            <%
                                                                                String auditAction = "Add Operator";
                                                                                String createdBy = "NA";
                                                                                AuditManagement audit = new AuditManagement();
                                                                                Audit[] auditDetails;
                                                                                auditDetails = new AuditManagement().getAuditrailByaction(auditAction);
                                                                                String strerr = "No Record found";
                                                                                if (oprData != null) {

                                                                                    for (int i = 0; i < oprData.length; i++) {
                                                                                        String accFrom = "NA";
                                                                                        String expOn = "NA";
                                                                                        Date expOn1 = oprData[i].getAccessTill();
                                                                                        SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy");
                                                                                        if (expOn1 != null) {
                                                                                            expOn = df2.format(expOn1);
                                                                                        }
                                                                                        Date date1 = oprData[i].getAccessStartFrom();
                                                                                        Units unit = unitInfo.getUnitByUnitId(sessionId, channel.getChannelid(), oprData[i].getiUnit());
                                                                                        unitnamelist.add(unit.getUnitname());
                                                                                        if (date1 != null) {
                                                                                            accFrom = df2.format(date1);
                                                                                        }
                                                                                        String strStatus = null;
                                                                                        if (oprData[i].getiStatus() == ACTIVE_STATUS) {
                                                                                            strStatus = "ACTIVE";
                                                                                        } else if (oprData[i].getiStatus() == LOCKED_STATUS) {
                                                                                            strStatus = "LOCKED";
                                                                                        } else if (oprData[i].getiStatus() == SUSPEND_STATUS) {
                                                                                            strStatus = "SUSPEND";
                                                                                        } else if (oprData[i].getiStatus() == REMOVE_STATUS) {
                                                                                            strStatus = "REMOVED";
                                                                                        }

                                                                                        Date cdate = new Date(oprData[i].getUtcCreatedOn());
                                                                                        String createdOn = df2.format(cdate);
//                                                                                        for (int j = 0; j < auditDetails.length; j++) {
//                                                                                            System.out.println(auditDetails[j].getNewvalue());
//                                                                                        }
                                                                                        if (auditDetails != null) {
                                                                                            for (int k = 0; k < auditDetails.length; k++) {
                                                                                                if (auditDetails[k].getNewvalue() != null && auditDetails[k].getNewvalue() != "") {
                                                                                                    String action = auditDetails[k].getNewvalue();
                                                                                                    String[] diffAction = action.split(",");
                                                                                                    if (action.contains("Email=" + oprData[i].getStrEmail())) {
                                                                                                        OperatorsManagement mgt = new OperatorsManagement();
                                                                                                        Operators op = new Operators();
                                                                                                        op = mgt.getOperatorById(channel.getChannelid(), auditDetails[k].getOperatorid());
                                                                                                        createdBy = op.getName();
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        if (createdBy == null || createdBy == "") {
                                                                                            createdBy = "NA";
                                                                                        }
                                                                                        creatBy.add(createdBy);
                                                                            %>
                                                                            <tr>
                                                                                <td><%=(i + 1)%></td>
                                                                                <%if (oprData[i].getStrName() != null) {%>
                                                                                <td><%= oprData[i].getStrName()%></td>
                                                                                <%} else {%>
                                                                                <td><%="NA"%></td>
                                                                                <%}%>
                                                                                <%if (oprData[i].getStrEmail() != null) {%>
                                                                                <td><%= oprData[i].getStrEmail()%></td>
                                                                                <%} else {%>
                                                                                <td><%="NA"%></td>
                                                                                <%}%>
                                                                                <%if (oprData[i].getStrPhone() != null) {%>
                                                                                <td><%=oprData[i].getStrPhone()%></td>
                                                                                <%} else {%>
                                                                                <td><%="NA"%></td>
                                                                                <%}%>
                                                                                <td><%=unit.getUnitname()%></td>
                                                                                <td><%=strStatus%></td>
                                                                                <%if (oprData[i].getStrRoleName() != null) {%>
                                                                                <td><%=oprData[i].getStrRoleName()%></td>
                                                                                <%} else {%>
                                                                                <td><%="NA"%></td>
                                                                                <%}%>
                                                                                <td><%=createdOn%></td>
                                                                                <td><%=createdBy%></td>

                                                                            </tr>
                                                                            <%}
                                                                                session.setAttribute("createdByList", creatBy);
                                                                            } else {%>
                                                                            <tr>
                                                                                <td><%=1%></td>
                                                                                <td><%= strerr%></td>
                                                                                <td><%= strerr%></td>
                                                                                <td><%= strerr%></td>
                                                                                <td><%= strerr%></td>
                                                                                <td><%= strerr%></td>
                                                                                <td><%= strerr%></td>
                                                                                <td><%= strerr%></td>
                                                                                <td><%= strerr%></td>

                                                                            </tr>
                                                                            <%}
                                                                                session.setAttribute("createdByList", creatBy);
                                                                                session.setAttribute("usersUnitReport", unitnamelist);%>
                                                                        </table>

                                                                        <script>
                                                                            $(document).ready(function () {
                                                                                $('#table_main').DataTable({
                                                                                    responsive: true
                                                                                });
                                                                            });
                                                                        </script>

                                                                        <div class="row-fluid">
                                                                            <div class="span6">
                                                                                <div class="control-group">                        
                                                                                    <div class="span1">
                                                                                        <div class="control-group form-inline">
                                                                                            <%  if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                    if (accessObjN != null && accessObjN.downloadRoleRports == true) {%>
                                                                                            <a href="#" class="btn btn-info" onclick="UserRoleReportDownload(1)" >
                                                                                                <%} else {%>
                                                                                                <a href="#" class="btn btn-info" onclick="InvalidRequest('UserRoleReportDownload')" >
                                                                                                    <%}
                                                                                                    } else {%>
                                                                                                    <a href="#" class="btn btn-info" onclick="UserRoleReportDownload(1)" >
                                                                                                        <%}%>
                                                                                                        <i class="icon-white icon-chevron-down"></i> CSV</a>
                                                                                                    </div>
                                                                                                    </div>
                                                                                                    <div class="span1">
                                                                                                        <div class="control-group form-inline">
                                                                                                            <%    if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                                    if (accessObjN != null && accessObjN.downloadRoleRports == true) {%>
                                                                                                            <a href="#" class="btn btn-info" onclick="UserRoleReportDownload(0)" >
                                                                                                                <%} else {%>
                                                                                                                <a href="#" class="btn btn-info" onclick="InvalidRequest('UserRoleReportDownload')" >
                                                                                                                    <%}
                                                                                                                    } else {%>
                                                                                                                    <a href="#" class="btn btn-info" onclick="UserRoleReportDownload(0)" >
                                                                                                                        <%}%>
                                                                                                                        <i class="icon-white icon-chevron-down"></i> PDF</a>
                                                                                                                    </div>
                                                                                                                    </div>
                                                                                                                    <div class="span1">
                                                                                                                        <div class="control-group form-inline">
                                                                                                                            <!--                                    <a href="#" class="btn btn-info" onclick="userReportTXT()" >
                                                                                                                                                                    <i class="icon-white icon-chevron-down"></i>Download TXT</a>-->
                                                                                                                            <%    if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                                                    if (accessObjN != null && accessObjN.downloadRoleRports == true) {%>
                                                                                                                            <a href="#" class="btn btn-info" onclick="UserRoleReportDownload(2)" >
                                                                                                                                <%} else {%>
                                                                                                                                <a href="#" class="btn btn-info" onclick="InvalidRequest('UserRoleReportDownload')" >
                                                                                                                                    <%}
                                                                                                                                    } else {%>
                                                                                                                                    <a href="#" class="btn btn-info" onclick="UserRoleReportDownload(2)" >
                                                                                                                                        <%}%>
                                                                                                                                        <i class="icon-white icon-chevron-down"></i> TXT</a>
                                                                                                                                    </div>
                                                                                                                                    </div>

                                                                                                                                    </div>
                                                                                                                                    </div>
                                                                                                                                    </div>

                                                                                                                                    </div>    
                                                                                                                                    </div>

                                                                                                                                    </div>
                                                                                                                                    <br>
                                                                                                                                    <br>
