<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Operators"%>
<%@page import="com.mollatech.axiom.connector.access.controller.AccessMatrixSettings"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Certificates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();
    String _status = request.getParameter("_changeCertStatus");
    int iStatus = -9999;
    if (_status != null && !_status.isEmpty()) {
        iStatus = Integer.parseInt(_status);
    }
    String _startdate = request.getParameter("_startdate");
    String _enddate = request.getParameter("_enddate");
     DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    Date startDate = null;
    if (_startdate != null && !_startdate.isEmpty()) {
        startDate = (Date) formatter.parse(_startdate);
    }
    Date endDate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        endDate = (Date) formatter.parse(_enddate);
    }

    CertificateManagement cObj = new CertificateManagement();
    Certificates[] arrCobj = null;
    if (iStatus == -1) {
        arrCobj = cObj.getExpireSoonCertificate(_channelId,startDate,endDate);
    } else {
        arrCobj = cObj.getCertObjByDate(sessionId, _channelId, iStatus,endDate,startDate);
    }

    String strerr = "No Record Found";
    int Active = 1;
    int Revoked = -5;
    int Expire = -10;
    int ExpireSoon = -1;
    String strStatus = null;
    if (iStatus == Active) {
        strStatus = "Active";
    } else if (iStatus == Expire) {
        strStatus = "Expire";
    } else if (iStatus == Revoked) {
        strStatus = "Revoked";
    } else if (iStatus == ExpireSoon) {
        strStatus = "Expire Soon";
    }

%>
<h3>Searched Results for Certificate Status = <i>"<%=strStatus%>"</i></h3>

<div class="tabbable">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#certcharts" data-toggle="tab">Charts</a></li>
        <li><a href="#certreport" data-toggle="tab">Tabular List</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="certcharts">
            <div class="row-fluid">
                <div class="span12">
                    <div class="control-group">
                        <div class="span4">
                            <div id="certdonutchart" ></div>
                            Donut Chart
                        </div>
                        <div  class="span8">
                            <div id="certbarchart"></div>
                            Bar Chart
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="tab-pane" id="certreport">   
            <div class="row-fluid">
                <div class="span6">
                    <div class="control-group">                        
                        <div class="span1">
                            <div class="control-group form-inline">
                                <%Operators oprObjI = (Operators) session.getAttribute("_apOprDetail");
                                    AccessMatrixSettings accessObjN = (AccessMatrixSettings) session.getAttribute("_apAccessEntry");
                                    if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                        if (accessObjN != null && accessObjN.downloadCertificateReport == true) {%>
                                <a href="#" class="btn btn-info" onclick="CertificateReport()" >
                                    <%} else {%>
                                    <a href="#" class="btn btn-info" onclick="InvalidRequestOTPToken('otpreportdownload')" >
                                        <%}
                                        } else {%>
                                        <a href="#" class="btn btn-info" onclick="CertificateReport()" >
                                            <%}%>

                                            <i class="icon-white icon-chevron-down"></i> CSV</a>
                                        </div>
                                        </div>
                                        <div class="span1">
                                            <div class="control-group form-inline">
                                                <% if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                        if (accessObjN != null && accessObjN.downloadCertificateReport == true) {%>
                                                <a href="#" class="btn btn-info" onclick="CertificateReportpdf()" >
                                                    <%} else {%>
                                                    <a href="#" class="btn btn-info" onclick="InvalidRequestCertificate('certreportdownload')" >
                                                        <%}
                                                        } else {%>
                                                        <a href="#" class="btn btn-info" onclick="CertificateReportpdf()" >
                                                            <%}%>

                                                            <i class="icon-white icon-chevron-down"></i> PDF</a>
                                                        </div>
                                                        </div>
                                                             <div class="span1">
                                            <div class="control-group form-inline">
                                                <% if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                        if (accessObjN != null && accessObjN.downloadCertificateReport == true) {%>
                                                <a href="#" class="btn btn-info" onclick="CertificateReportTXT()" >
                                                    <%} else {%>
                                                    <a href="#" class="btn btn-info" onclick="InvalidRequestCertificate('certreportdownload')" >
                                                        <%}
                                                        } else {%>
                                                        <a href="#" class="btn btn-info" onclick="CertificateReportTXT()" >
                                                            <%}%>

                                                            <i class="icon-white icon-chevron-down"></i> TEXT</a>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        </div>

                                                        <table class="table table-striped" id="table_main">
                                                            <tr>
                                                                <td>No.</td>
                                                                <td>User Id</td>
                                                                <td>User Name</td>
                                                                <td>Certificate ID</td>  
                                                                <td>Status</td>  
                                                                <td>Issued On</td>
                                                                <td>Expires On</td>
                                                            </tr>
                                                            <%
                                                                int CERT_STATUS_ACTIVE = 1;
                                                                int CERT_STATUS_EXPIRED = -10;
                                                                int CERT_STATUS_REVOKED = -5;
                                                                int GOING_TO_EXPIRED = -1;
                                                                int SHOW_ALL = 2;

                                                                if (arrCobj != null) {
                                                                    UserManagement uObj = new UserManagement();
                                                                    for (int i = 0; i < arrCobj.length; i++) {

                                                                        AuthUser userObj = uObj.getUser(sessionId, _channelId, arrCobj[i].getUserid());
                                                                        String strCertStatus = null;
                                                                        if (arrCobj[i].getStatus() == CERT_STATUS_ACTIVE) {
                                                                            strCertStatus = "Active Certificate";
                                                                        } else if (arrCobj[i].getStatus() == CERT_STATUS_REVOKED) {
                                                                            strCertStatus = "Revoked Certificate";
                                                                        } else if (arrCobj[i].getStatus() == CERT_STATUS_EXPIRED) {
                                                                            strCertStatus = "Expired Certificate";
                                                                        } else if (arrCobj[i].getStatus() == GOING_TO_EXPIRED) {
                                                                            strCertStatus = "Expire Soon Certificate";
                                                                        }
                                                            %>

                                                            <tr>
                                                                <td><%=(i + 1)%></td>
                                                                <%if(userObj != null){%>
                                                                <td><%=userObj.getUserId()%></td>
                                                                <%}else{%>
                                                                 <td><%="NA"%></td> 
                                                                <%}%>
                                                                <%if(userObj != null){%>
                                                                <td><%=userObj.getUserName()%></td>
                                                                <%}else{%>
                                                                 <td><%="NA"%></td> 
                                                                <%}%>
                                                                <td><%=arrCobj[i].getCertid()%></td>
                                                                <td><%=strCertStatus%></td>
                                                                <td><%=formatter.format(arrCobj[i].getCreationdatetime())%></td>
                                                                <td><%=formatter.format(arrCobj[i].getExpirydatetime())%></td>
                                                            </tr>


                                                            <%}} else {%>
                                                            <tr>
                                                                <td><%=1%></td>
                                                                <td><%= strerr%></td>
                                                                <td><%= strerr%></td>
                                                                <td><%= strerr%></td>
                                                                <td><%= strerr%></td>
                                                                <td><%= strerr%></td>
                                                                <td><%= strerr%></td>

                                                            </tr>
                                                            <%}%>


                                                        </table>

                                                        <div class="row-fluid">
                                                            <div class="span6">
                                                                <div class="control-group">                        
                                                                    <div class="span1">
                                                                        <div class="control-group form-inline">
                                                                            <% if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                        if (accessObjN != null && accessObjN.downloadCertificateReport == true) {%>
                                                                            <a href="#" class="btn btn-info" onclick="CertificateReport()" >
                                                                                <%} else {%>
                                                                                <a href="#" class="btn btn-info" onclick="InvalidRequestOTPToken('otpreportdownload')" >
                                                                                    <%}
                                        } else {%>
                                                                                    <a href="#" class="btn btn-info" onclick="CertificateReport()" >
                                                                                        <%}%>

                                                                                        <i class="icon-white icon-chevron-down"></i> CSV</a>
                                                                                    </div>
                                                                                    </div>
                                                                                    <div class="span1">
                                                                                        <div class="control-group form-inline">
                                                                                            <% if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                        if (accessObjN != null && accessObjN.downloadCertificateReport == true) {%>
                                                                                            <a href="#" class="btn btn-info" onclick="CertificateReportpdf()" >
                                                                                                <%} else {%>
                                                                                                <a href="#" class="btn btn-info" onclick="InvalidRequestCertificate('certreportdownload')" >
                                                                                                    <%}
                                                        } else {%>
                                                                                                    <a href="#" class="btn btn-info" onclick="CertificateReportpdf()" >
                                                                                                        <%}%>

                                                                                                        <i class="icon-white icon-chevron-down"></i> PDF</a>
                                                                                                    </div>
                                                                                                    </div>
                                                                                                         <div class="span1">
                                            <div class="control-group form-inline">
                                                <% if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                        if (accessObjN != null && accessObjN.downloadCertificateReport == true) {%>
                                                <a href="#" class="btn btn-info" onclick="CertificateReportTXT()" >
                                                    <%} else {%>
                                                    <a href="#" class="btn btn-info" onclick="InvalidRequestCertificate('certreportdownload')" >
                                                        <%}
                                                        } else {%>
                                                        <a href="#" class="btn btn-info" onclick="CertificateReportTXT()" >
                                                            <%}%>

                                                            <i class="icon-white icon-chevron-down"></i> TEXT</a>
                                                        </div>
                                                        </div>
                                                                                                    </div>
                                                                                                    </div>
                                                                                                    </div>

                                                                                                    <br><br>
                                                                                                    </div>
                                                                                                    </div>
                                                                                                    </div>

                                                                                                    <br><br>
                                                                                                   