<%@page import="com.mollatech.axiom.nucleus.db.Units"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet"%>
<%@page import="com.mollatech.axiom.nucleus.db.Otptokens"%>
<%@page import="java.text.Normalizer.Form"%>
<%@page import="com.itextpdf.text.Document"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.connector.user.TokenStatusDetails"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>

<%@include file="header.jsp" %>
<!--<script src="./assets/js/jquery.js"></script>-->
<script src="./assets/js/otptokens.js"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">

<div class="container-fluid">
    <h1 class="text-success">One Time Password Tokens Reports</h1>
    <h3>Make Your Own Report</h3>
    <div class="row-fluid">
        <div class="span12">
            <div class="control-group form-inline">
                From Date :   
                <input id="otpstartdate" name="otpstartdate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth" style="width: 10%">
                To : 
                <input id="otpenddate" name="otpenddate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth" style="width: 10%">
                Token Type :
                <select class="selectpicker" name="_changeCategory" id="_changeCategory" style="width: 15%" >
                    <option  value="1">Software OTP Token</option>       
                    <option  value="2">Hardware OTP Token</option>       
                    <option  value="3">Out Of Band OTP Token</option>       
                </select>
                Token Status :
                <select class="selectpicker" name="_changeOTPStatus" id="_changeOTPStatus" style="width: 10%">
                    <option  value="2">All</option>       
                    <option  value="0">Assigned </option>       
                    <option  value="1">Active </option>       
                    <option  value="-1">Locked </option>       
                    <option  value="-10">Unassigned </option>       
                    <option  value="-2">Suspended </option>       
                    <option  value="-5">Lost  <i>(For Hardware Only)</i></option>       
                    <option  value="-10">Free And Available  <i>(For Hardware Only)</i></option>       
                </select>
                Units :

                <select class="selectpicker" name="_UnitId" id="_UnitId" style="width:10%">

                    <%   Channels channel = (Channels) session.getAttribute("_apSChannelDetails");
                        String sessionId = (String) session.getAttribute("_apSessionID");
                        UnitsManagemet unitObj = new UnitsManagemet();
                        Units[] un = unitObj.ListUnitss(sessionId, channel.getChannelid());
                        if (un != null) {
                            for (int j = 0; j < un.length; j++) {

                    %>
                    <option  value="<%=un[j].getUnitid()%>"><%=un[j].getUnitname()%></option>       
                    <%}
                    %>
                    <option  value="-1">All Units</option>       
                    <%
                            } else {%>
                    <option  value="0">No Units Found</option>   
                    <%}%>
                </select>

                <div class="input-prepend">
                    <!--<button class="btn btn-success" id="generatereportButton" onclick="generateOtpTablev2()" type="button">Filter</button>-->
                    <%if (oprObj.getRoleid() != 1) {
                            if (accessObj != null && accessObj.viewOtpReport == true) {%>
                    <button class="btn btn-success" onclick="generateOtpTablev2()" id="generatereportButton">Generate Report Now</button>
                    <%} else {%>
                    <button class="btn btn-success " onclick="InvalidRequestOTPToken('otpreport')" type="button">Generate Report Now</button>
                    <%}
                    } else {%>
                    <button class="btn btn-success" onclick="generateOtpTablev2()" id="generatereportButton">Generate Report Now</button>
                    <%}%>
                </div>               
            </div>
        </div>
    </div>

    <!--    <div class="tabbable" id="REPORT">
            <ul class="nav nav-tabs" id="otpTokenReport">
                <li class="active"><a href="#otpcharts" data-toggle="tab">Charts</a></li>
                <li><a href="#otpreport" data-toggle="tab">Tabular List</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="otpcharts">
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="control-group">
                                <div class="span4">
                                    <div id="otpReportgraph" ></div>
    
                                </div>
                                <div  class="span8">
                                    <div id="otpReportgraph1"></div>
    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->

    <!--            <div class="tab-pane" id="otpreport">  -->
    <div id="licenses_data_table">
    </div>
    <!--            </div>-->
</div>
<!--    </div>-->


<script type="text/javascript">
    ChangeOtpCategory(1);
    ChangeOtpStateType(2);
    //        generateOtpTable()

    $(function () {
        $('#otpstartdate').datepicker({
            format: 'dd/mm/yyyy',
            language: 'pt-BR'
        });
    });
    $(function () {
        $('#otpenddate').datepicker({
            format: 'dd/mm/yyyy',
            language: 'pt-BR'

        });
    });

</script>
<script>

//        document.getElementById("_UnitId").style.display = 'none';
    //  document.getElementById("REPORT").style.display = 'none';
    //document.getElementById("refreshButton").style.display = 'none';
</script>
</div>
<%@include file="footer.jsp" %>