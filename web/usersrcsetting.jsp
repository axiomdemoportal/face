<%@include file="header.jsp" %>
<script src="./assets/js/usersource.js"></script>
<div class="container-fluid">
    <h1 class="text-success">User Source Configuration</h1>
    <p>To facilitate internal/external user repository for user, password listing and more importantly associating OTP token, PKI token and Digital Certificate to these users. Please configure user repository.</p>
    <br>
    <div class="row-fluid">
        <form class="form-horizontal" id="usersourceform" name="usersourceform">
            <div class="control-group">
           <label class="control-label"  for="username">Database type:Host:Port </label>
                <div class="controls">
                    <!-- Changes by abhishek-->
                    <select class="span2" name="_databaseTypeUS" id="_databaseTypeUS">
                        <option value="Mysql">Mysql</option>
                        <option value="Oracle">Oracle</option>
                        <option value="LDAP">LDAP</option>
                    </select> 
<!--                </div>
                <div class="controls">-->
                    <input type="text" id="_ipUS" name="_ipUS" placeholder="example localhost/127.0.0.1" class="input-xlarge">
                    : <input type="text" id="_portUS" name="_portUS" placeholder="443" class="span1">
                    with 
                    <select class="span2" name="_sslUS" id="_sslUS">
                        <option value="true">Yes, SSL Enabled?</option>
                        <option value="false">No, SSL Disabled?</option>
                    </select>
                    
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="password">Source Details</label>
                <div class="controls">
                    <input type="text" id="_databaseNameUS" name="_databaseNameUS" placeholder="database name" class="span2">
                    : <input type="text" id="_tableNameUS" name="_tableNameUS" placeholder="table name " class="span2">
                    authenticated with <input type="text" id="_userIdUS" name="_userIdUS" placeholder=" userid" class="span2">
                    : <input type="password" id="_passwordUS" name="_passwordUS" placeholder="password" class="span2">
                </div>
            </div> 
            <div class="control-group">
                <label class="control-label"  for="username">Additional Attributes</label>
                <div class="controls">
                    <input type="text" id="_reserve1US" name="_reserve1US" placeholder="name1=value1" class="span2">
                    : <input type="text" id="_reserve2US" name="_reserve2US" placeholder="name2=value2" class="span2">
                    : <input type="text" id="_reserve3US" name="_reserve3US" placeholder="name3=value3" class="span2">
                    (if needed)
                </div>
            </div>
            <div id="legend">
                <legend class="">Password Policy Details</legend>
            </div>

            <div class="control-group">
                <label class="control-label" for="password">Password Type</label>
                <div class="controls">
                    <select class="span3" name="_passwordTypeUS" id="_passwordTypeUS">
                        <option value=1>Encrypted Password</option>
                        <option value=2>Hashed Password</option>
                        <option value=3>Plain Password</option>
                    </select>
                    with algorithm as 
                    <select class="span3" name="_algoTypeUS" id="_algoTypeUS">
                        <option value=1>AES/CBC(Encryption)</option>
                        <option value=4>3DES (Encryption)</option>
                        <option value=6>SHA1 (Hashing)</option>
                        <option value=7>SHA256 (Hashing)</option>                            
                    </select>
                    User Validity
                    <select class="span5" name="_userValidityDays" id="_userValidityDays" style="width:20%">
                        <option value="1825" >5 Years</option>
                        <option value="1460" >4 Years</option>
                        <option value="1095" >3 Years</option>
                        <option value="730" >2 Years</option>
                        <option value="365" >1 Year</option>
                    </select>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label"  for="username">Algorithm Attributes</label>
                <div class="controls">
                    <input type="text" id="_algoAttrReserve1US" name="_algoAttrReserve1US" placeholder="name1=value1" class="span2">
                    : <input type="text" id="_algoAttrReserve2US" name="_algoAttrReserve2US" placeholder="name2=value2" class="span2">
                    : <input type="text" id="_algoAttrReserve3US" name="_algoAttrReserve3US" placeholder="name3=value3" class="span2">
                    : <input type="text" id="_algoAttrReserve4US" name="_algoAttrReserve4US" placeholder="name4=value4" class="span2">
                    (if needed)

                </div>


            </div>



            <input type="hidden" id="_statusUS" name="_statusUS" >

            <hr>


            <div class="control-group">
                <label class="control-label"  for="username">Implementation Class</label>
                <div class="controls">
                    <input type="text" id="_classNameUS" name="_classNameUS" placeholder="complete class name including package" class="input-xlarge">
                </div>
            </div>
            <!-- Submit -->
            <div class="control-group">
                <div class="controls">
                    <div id="save-user-source-result"></div>
                    <%// if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) { %>                                
                    <button class="btn btn-primary" onclick="EditUserSourceSetting()" type="button">Save  Now >> </button>
                    <%// } %>
                    <button class="btn " onclick="testConnectionUS()" type="button">Test Connection </button>
                </div>
            </div>

        </form>
    </div>


    <script language="javascript" type="text/javascript">
        LoadUserSourceSetting();
    </script>
</div>


<%@include file="footer.jsp" %>