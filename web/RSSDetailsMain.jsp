<%@include file="header.jsp" %>
<script src="./assets/js/remotesigning.js"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>


<div class="container-fluid">
    <h1 class="text-success">Remote Server Signing</h2>
        <br>
        <h3>Search  By </h3>

        <form id="searchDeviceForm" name="searchDeviceForm">
            <label class="control-label"  for="_emsg">From<span style="margin-left:18% ">Till</span></label>
            <div id="datetimepicker1" class="input-append date">
                <input id="startdaters" name="startdaters" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                </span>
            </div>
            &nbsp;&nbsp;&nbsp;
            <div id="datetimepicker2" class="input-append date">
                <input id="enddaters" name="enddaters" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                </span>
            </div>

            <div class="input-append">

                <input type="hidden" id="_searchType" name="_searchType">            
                <div class="btn-group">             
                    Select <button class="btn btn-small"><div id="_searchType_div"></div></button> and enter
                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="#" onclick="ChangeSearchType(1)">User ID </a></li>
                        <li><a href="#" onclick="ChangeSearchType(0)">Archive ID </a></li>
                        <li><a href="#" onclick="ChangeSearchType(2)">Reference ID </a></li>
                    </ul>
                </div>
                <input type="text" id="_userName" name="_userName" placeholder="search Users using User ID, Archive ID or Reference ID" class="span3"><span class="add-on"><i class="icon-search"></i></span>
                    <%if (oprObj.getRoleid() != 1) {
                            if (accessObj.viewRemoteSigningReport == true) { %>
                <a href="#" class="btn btn-success" onclick="searchRemotSigningRecords()">Search</a>
                <%} else {%>
                <a href="#" class="btn btn-success" onclick="InvalidRequestRemoteSign('remotesigningreport')">Search</a>
                <%}
                } else {%>
                <a href="#" class="btn btn-success" onclick="searchRemotSigningRecords()">Search</a>
                <%}%>
        </form>
</div>

<div id="roaming_table_main">
</div>
</div>
<script type ="text/javascript">
    ChangeSearchType(1);
</script>
</p>
<br>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker2').datepicker({
            format: 'dd/mm/yyyy',
            language: 'pt-BR'
        });
    });
    $(function () {
        $('#datetimepicker1').datepicker({
            format: 'dd/mm/yyyy',
            language: 'pt-BR'
        });
    });
</script>
<%@include file="footer.jsp" %>