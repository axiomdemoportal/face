<%@include file="header.jsp" %>
<script src="./assets/js/jquery.js"></script>
<script src="./assets/js/usermanagement.js"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">
<script src="assets/js/simplifiedUserSearch.js" type="text/javascript"></script>
<div class="container-fluid">
    <h3 class="text-success">Simplified Users Search</h3>
<!--    <h3>Make Your Own Report</h3>-->
    <input type="hidden" id="_changeStatus" name="_changeStatus">

    <div class="row-fluid">
        <div class="span12">
            <div class="control-group form-inline">

                <div class="input-prepend">
                    <span class="add-on " >Enter </span><input id="_searchtext" name="_searchtext" class="" type="text" placeholder="name/phone/email/userid/serial number" data-bind="value: vm.ActualDoorSizeDepth" />
                </div>
                <div class="input-append">                    
                    <button class="btn btn-success " onclick="searchSimplifiedUserReport()" type="button">Search Now</button>                   
                </div>
            </div>
        </div>
    </div>
    <div id="users_data_tabs">
        
    </div>
<script type="text/javascript" src="assets/plugins/jquery-1.12.3.min.js"></script>                                                                   
<script type="text/javascript" src="assets/plugins/jquery-match-height/jquery.matchHeight-min.js"></script>
<!--<script type="text/javascript" src="assets/js/main.js"></script>-->
<%@include file="footer.jsp" %>