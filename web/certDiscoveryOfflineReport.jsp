<%-- 
    Document   : certDiscoveryOfflineReport
    Created on : 27 Dec, 2016, 10:46:47 AM
    Author     : bluebricks
--%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.axiom.nucleus.db.ApCertDiscovery"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CertDiscoveryManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.certDiscovery.utils.IPAddress"%>
<%@page import="com.mollatech.certDiscovery.utils.InstallCert"%>
<%@page import="com.mollatech.certDiscovery.utils.CertDetails"%>
<%@page import="java.util.*"%>
<%--<%@include file="header.jsp" %>--%>
<!--pagination-->
<script src="./assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/dataTables.responsive.min.js"></script>
<link  rel="stylesheet" href="assets/css/jquery.dataTables.min.css">
<link  rel="stylesheet" href="assets/css/responsive.dataTables.min.css">
<!--JS FOR BOOTSTRAP BUTTON SYMBOL -->

<!--<link href="assets/css/bootstrap3.3.7/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<script src="assets/js/js2.1.1/jquery.min.js" type="text/javascript"></script>
<script src="assets/js/js2.1.1/bootstrap.min.js" type="text/javascript"></script>-->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->


<div class="row-fluid">
    <div id="licenses_data_table">
        <%
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            String _channelId = channel.getChannelid();
            String ips = request.getParameter("IP");
            String ports = request.getParameter("port");
            List ip = new ArrayList();
            if (ips.contains("/")) {

                String[] str = ips.split("/");
                List<String> list = IPAddress.getIPAddressesOffline(str[0]);
                ip.addAll(list);
            } else {
                ip.add(ips);
            }

            //
            List port = new ArrayList();
            if (ports.contains(",")) {

                String[] str = ports.split("\\s*,\\s*");

                List<String> items = Arrays.asList(str);
                port.addAll(items);
            } else {
                port.add(ports);
            }
        %>
        <!--table table-striped  display responsive wrap table-bordered table-hover-->
        <table class="display responsive wrap table-bordered table-striped table-hover" id="table_main11">
            <thead>
                <tr>
                    <td><b>No</b></td>
                    <td><b>Hostname/IP Address</td>
                    <td><b>Port</td>
                    <td><b>Subject </b></td>
                    <td><b>Issuer  </b></td>
                    <td><b>Issued Date</b></td>
                    <td><b>Expiry Date</b></td>
                    <td><b>Details</b></td>
                    <td><b>Delete</b></td>
                </tr>
            </thead>
            <!--            <tbody>-->

            <%  String strerr = "No Record Found";
                int c = 0;
                String PortIpDetails = null;
                String IPPortDetails = null;
                List<String> ip2 = ip;
                List<String> port3 = port;
                for (String ip1 : ip2) {
                    for (String port1 : port3) {
                        JSONObject detail1 = new JSONObject();
                        try {
                            detail1.put("_Port", port1);
                            detail1.put("ip", ip1);

                        } catch (Exception e) {

                        }
                        PortIpDetails = detail1.toString();

                        JSONObject detail2 = new JSONObject();
                        try {
                            detail2.put("ip", ip1);
                            detail2.put("_Port", port1);

                        } catch (Exception e) {

                        }
                        IPPortDetails = detail2.toString();

                        CertDiscoveryManagement cMgnt = new CertDiscoveryManagement();
                        ApCertDiscovery[] apCertDiscovery = cMgnt.getCertDiscoveryDetails();
                        if (apCertDiscovery != null) {
                            int l = apCertDiscovery.length;
                            for (int i = 0; i < l; i++) {
                                if (apCertDiscovery[i].getPortIpDetails().equals(PortIpDetails) || apCertDiscovery[i].getPortIpDetails().equals(IPPortDetails)) {
                                    c++;
                                    Date Expirydatetime = apCertDiscovery[i].getExpirydatetime();

                                    String Details = apCertDiscovery[i].getSubject();
                                    JSONObject PortIp = new JSONObject(PortIpDetails);
                                    String Port = (String) PortIp.get("_Port");
                                    String Ip = (String) PortIp.get("ip");
                                    JSONObject moduledetails = new JSONObject(Details);

                                    String _Subject = (String) moduledetails.get("_Subject");
                                    String _Issuer = (String) moduledetails.get("_Issuer");
                                    String _SHA1 = (String) moduledetails.get("_SHA1");
                                    String _MD5 = (String) moduledetails.get("_MD5");
                                    String _IssuedDate = (String) moduledetails.get("_IssuedDate");
                                    String _expDate = (String) moduledetails.get("_ExpiryDate");
                                    String _Type = (String) moduledetails.get("_Type");
                                    String regcode = apCertDiscovery[i].getCid();

                                    i = i + 1;

            %>
            <tr>
                <td><%=c%></td>
                <td><%= Ip%></td>
                <td><%= Port%></td>
                <td><%= _Subject%></td>
                <td><%= _Issuer%></td>
                <td><%= _IssuedDate%></td>
                <td><%= _expDate%></td>

                <td>
                    <div id="certDelete" name="certDelete"><input type="hidden" name="PortIpDetails" id="PortIpDetails" value="<%=Ip%>">  </input>
                        <input type="hidden" name="IPPortDetails" id="IPPortDetails" value="<%=Port%>">  </input></div>
                    <div class="btn-group">
                        <a href="#certDetail<%=i%>" class="btn btn-mini" style="appearance: button" data-toggle="modal" >Details</a>
                    </div>
                    <div id="certDetail<%=i%>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                            <h3 class="text text-success" id="myModalLabel"><div id="idcertDetails"></div></h3>
                        </div>
                        <div class="modal-body">
                            <div class="row-fluid">
                                <form class="form-horizontal" id="deleteCertForm">
                                    <fieldset>
                                        <table class="table table-striped   table-bordered table-hover" id="tablemain">
                                            <tr>
                                                <td>Subject : </td>
                                                <td id="_Subject<%=i%>"><%= _Subject%></td>
                                            </tr>
                                            <tr>
                                                <td>Issuer : </td>
                                                <td id="_Issuer<%=i%>"><%= _Issuer%></td>
                                            </tr>
                                            <tr>
                                                <td>SHA1 : </td>
                                                <td id="_SHA1<%=i%>"><%= _SHA1%></td>
                                            </tr>
                                            <tr>
                                                <td>MD5 : </td>
                                                <td id="_MD5<%=i%>"><%= _MD5%></td>
                                            </tr>
                                            <tr>
                                                <td>Expiry Date : </td>
                                                <td id="_ExpiryDate<%=i%>"><%= _expDate%></td>
                                            </tr>
                                            <tr>
                                                <td>Issued Date : </td>
                                                <td id="_IssuedDate<%=i%>"><%=_IssuedDate%></td>
                                            </tr>
                                            <tr>
                                                <td>Type : </td>
                                                <td id="_Type<%=i%>"><%= _Type%></td>
                                            </tr>

                                        </table>


                                    </fieldset>

                                </form>     


                            </div>
                        </div>
                                                    <div class="modal-footer">
                        
                                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                        
                                                    </div>
                    </div>
                </td>
                <td> <div class="btn-group">
                        <button  class="btn btn-mini" onclick="deletecert('<%=Ip%>', '<%=Port%>')" id="addCertButton"><span class="icon-trash"></span></button>

<!--                            <button  class="btn btn-mini" onclick="deletecert('<%=Ip%>', '<%=Port%>')" id="addCertButton"><span class="icon-trash"></span></button>-->
                    </div></td>
            </tr>
            <% } else {%>
            <%                   
                    }
                }

            } else {%>
            <tr>
                <td><%=strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                                <td><%= strerr%></td>
            </tr>
            <% }
                    }
                }
                if (c == 0) {%>
            <tr>
                <td><%=strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
            </tr>
            <%}

            %> 
        </table>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#table_main11').DataTable({
            responsive: true
        });
    });
</script>
<!--Script for paging-->





<!--<div>
    <div id="deleteCert-result"></div>
    <button onclick="myFunction()" type="button" class="btn btn-primary">Print <span class="glyphicon glyphicon-print"></span></button>
    <script>
        function myFunction() {
            window.print();
        }
    </script>
</div>-->
<!--<script language="javascript">
</script>-->
<!--<script>
    $(document).ready(function () {
        $('#table_main1').DataTable({
            responsive: true
        });
    });
</script>-->