<!DOCTYPE html>
<html lang="en">
    <%@page import="java.awt.image.BufferedImage"%>
    <%@page import="java.io.ByteArrayOutputStream"%>
    <%@page import="java.io.File"%>"
    <%@page import="javax.imageio.ImageIO"%> 
    <%@page import="java.io.FileReader"%>
    <%@page import="java.io.BufferedReader"%>
    <%@page import="java.nio.channels.FileChannel"%>
    <%@page import="java.io.FileInputStream"%>
    <%@page import="org.apache.commons.io.FileUtils"%>
    <%@page import="java.io.File"%>
    <%@page import="org.json.JSONObject"%>
    <%@page import="java.io.ByteArrayInputStream"%>
    <%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
    <%@page import="com.mollatech.axiom.nucleus.db.DocumentTemplate"%>
    <%@page import="com.mollatech.axiom.nucleus.db.connector.management.DocsTemplatesManagement"%>
    <%@page import="java.io.IOException"%>
    <%@page import="java.io.FileOutputStream"%>
    <%@page import="java.io.OutputStreamWriter"%>
    <%@page import="java.io.BufferedWriter"%>
    <%@page import="java.io.Writer"%>
    <%@page import="org.apache.commons.io.IOUtils"%>
    <%@page import="java.io.InputStream"%>
    <%@page import="com.mollatech.axiom.ocr.ResizeImage"%>
    <%@page import="com.mollatech.axiom.document.PreprocessImage"%>
    <%@page import="org.bouncycastle.util.encoders.Base64"%>
    <%@include file="header.jsp" %>
    <script src="assets/dococr/js/documentsTemplate.js" type="text/javascript"></script>
    <%
        String templatename = request.getParameter("templatename");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        DocsTemplatesManagement docObj = new DocsTemplatesManagement();
        DocumentTemplate docsDetails = docObj.editDocumentDetails(templatename, channel.getChannelid());
        byte[] docsImage = docsDetails.getProcessImage();
        String base64image = new String(Base64.encode(docsImage));
    %>
    <style>
        form div {
            padding: x; /*default div padding in the form e.g. 5px 0 5px 0*/
            margin: y; /*default div padding in the form e.g. 5px 0 5px 0*/
        }
        .divForText { /*For Text line only*/
            padding: a;
            margin: b;
        }
        .divForLabelInput{ /*For Text and Input line */
            padding: c;
            margin: d;
        }
        .divForInput{ /*For Input line only*/
            padding: e;
            margin: f;
        }
    </style>
    <form id="jsondata" name="jsondata">
        <div id="container" style="width:100%;">                                   
            <div id="left" class="panel panel-default" style=";overflow-y: auto; height:600px;float:left; width:70%;">
                <img src="data:image/png;base64,<%=base64image%>"/> 
            </div>                     
            <div class="text-center" class="panel panel-default" id="right" style="float:right; width:30%;"> 
                <br><br><br>
                <h1 class="text-center text-success">Save Table data</h1><hr> 
                <input type="text" id="_columnName" name="_columnName" placeholder="Enter column name"> 
                <button type="button" onclick="addTableColumn('<%=templatename%>')" class="btn btn-primary" style="vertical-align: super !important" >Add Column</button>
                <p>Once column adding done then add row values</p>
                <button type="button" class="btn btn-success" onclick="showcolumn('<%=templatename%>')" style="vertical-align: super !important;width:150px" >Show</button>
                <!--<button type="button" class="btn btn-success" onclick="showcolumn('<%=templatename%>')" style="vertical-align: super !important;width:150px" >Edit row</button>-->
                <br> <br> <br>
                <div class="divForText" id="dynamic" name="dynamic"ss></div>
                <br>
                <button type="button" id="savetable" class="btn btn-success" onclick="savestructure('<%=templatename%>')" style="vertical-align: super !important;width:300px;display:none;">Save Structure</button>
            </div>                   
        </div> 
    </form>
    <script>




        function allAlert(msg) {
            bootbox.alert("<h2>" + msg + "</h2>", function (result) {
                if (result === false) {
                } else {
                    //end here
                }
            });

        }
        function strcmpUsers(a, b)
        {
            return (a < b ? -1 : (a > b ? 1 : 0));
        }

        function addTableColumn(templatename) {
            var columnValue = document.getElementById("_columnName").value;
            var msg = "Please insert column value";
            if (columnValue === null || columnValue === "") {
                allAlert("<span><font color=red>" + msg + "</font></span>");
                return;
            }
            var s = './addColumnFromTable?templatename=' + templatename;
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
                data: $('#jsondata').serialize(),
                success: function (data) {
                    if (strcmpUsers(data._result, "error") === 0) {
                        allAlert("<span><font color=red>" + data._message + "</font></span>");
                    } else if (strcmpUsers(data._result, "success") === 0) {
                        allAlert("<span><font color=blue>" + data._message + "</font></span>");
                        document.getElementById("_columnName").value = "";
                    }

                },
                error: function (data) {
                    JSON.stringify(data);
                }
            });
        }


        function showcolumn(templatename) {
            var s = './GetColumn?templatename=' + templatename;
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
                success: function (data) {
                    if (strcmpUsers(data._result, "success") === 0) {
                        var columnname = data._columns;
                        var textfield = document.getElementById("dynamic");
                        $('#dynamic').empty();
                        console.log(columnname[0] + "==" + columnname[1] + " == " + columnname.length);
                        var length1 = data._length;
                        for (var i = 0; i < length1; i++) {
                            var column = document.createElement("input");
                            column.setAttribute("id", "column" + i);
                            column.setAttribute("name", "column" + i);
                            column.value = columnname[i];
                            var linebreak = document.createElement("br");
                            var row = document.createElement("input");
                            row.setAttribute("id", "row" + i);
                            row.setAttribute("name", "row" + i);
                            textfield.appendChild(column);
                            textfield.appendChild(row);
                            textfield.appendChild(linebreak);
                            textfield.appendChild(linebreak);
                            textfield.appendChild(linebreak);
                        }
                        $('#savetable').show();
                        document.getElementById("_columnName").value = "";
                    }
                    if (strcmpUsers(data._result, "error") === 0) {
                        allAlert("<span><font color=red>" + data._message + "</font></span>");
                    }
                },
                error: function (data) {
//                    alert("hello2" + JSON.stringify(data));
                    JSON.stringify(data);
                }
            });
        }

        function savestructure(templatename) {
            var s = './AddTableStructure?templatename=' + templatename;
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
                data: $('#jsondata').serialize(),
                success: function (data) {
                    if (strcmpUsers(data._result, "error") === 0) {
                        allAlert("<span><font color=red>" + data._message + "</font></span>");
                    } else if (strcmpUsers(data._result, "success") === 0) {
                        allAlert("<span><font color=blue>" + data._message + "</font></span>");
                    }
                },
                error: function (data) {
//                    alert("hello2" + JSON.stringify(data));
                    JSON.stringify(data);
                }
            });
        }

    </script>