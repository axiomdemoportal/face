<%@include file="header.jsp" %>
<script src="./assets/js/geotracking.js"></script>

<div class="container-fluid">
    <h1 class="text-success">Geo Management and Tracking</h1>
    <p>This feature is to share user's geo-locations for all their transactions executed. You can manipulate user's home and foreign roaming along with visual map details.</p>
    <p>
    <br>
    <h3>Search  By name</h3>   
    <div class="input-append">
        <form id="searchDeviceForm" name="searchDeviceForm">
            <input type="text" id="_keyword" name="_keyword" placeholder="search Users using name, phone or email" class="span4"><span class="add-on"><i class="icon-search"></i></span>
            <a href="#" class="btn btn-success" onclick="searchRoaming()">Search User(s)</a>
        </form>
    </div>
    <div id="roaming_table_main">
    </div>

</p>
<br>

<%@include file="footer.jsp" %>