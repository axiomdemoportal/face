<%@page import="com.mollatech.axiom.nucleus.db.Audit"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.AuditManagement"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="java.text.SimpleDateFormat"%>


<%
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _startdate = request.getParameter("_startdate");
    String _enddate = request.getParameter("_enddate");
    String _auditUserID = request.getParameter("_auditUserID");
    String _itemType = request.getParameter("_itemType");
    String _auditUserName = request.getParameter("_auditUserName");
    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy MM:ss");
    Date startDate = null;
    if (_startdate != null && !_startdate.isEmpty()) {
        startDate = (Date) formatter.parse(_startdate);
    }
    Date endDate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        endDate = (Date) formatter.parse(_enddate);
    }

    AuditManagement audit = new AuditManagement();
    String strmsg = "No Record Found";
    Audit[] arrAudit = audit.searchAuditObj(channel.getChannelid(), _auditUserID, _itemType, startDate, endDate);
    SimpleDateFormat tz1 = new SimpleDateFormat("EEE dd MMM");
%>
<h3> Results from <%=tz1.format(startDate)%> to <%=tz1.format(endDate)%> for <%=_auditUserName%> </i></h3>
<div class="row-fluid">
    <div class="span6">
        <div class="control-group">                        
            <div class="span1">
                <div class="control-group form-inline">
                    <a href="#" class="btn btn-info" onclick="userReportCSV('<%=_itemType%>', '<%=_startdate%>', '<%=_enddate%>', '<%=_auditUserID%>')" >
                        <i class="icon-white icon-chevron-down"></i> CSV</a>
                </div>
            </div>
            <div class="span1">
                <div class="control-group form-inline">
                    <a href="#" class="btn btn-info" onclick="userReportPDF('<%=_itemType%>', '<%=_startdate%>', '<%=_enddate%>', '<%=_auditUserID%>')" >
                        <i class="icon-white icon-chevron-down"></i> PDF</a>
                </div>
            </div>
            <div class="span1">
                <div class="control-group form-inline">
                    <a href="#" class="btn btn-info" onclick="userReportTXT('<%=_itemType%>', '<%=_startdate%>', '<%=_enddate%>', '<%=_auditUserID%>')" >
                        <i class="icon-white icon-chevron-down"></i> TEXT</a>
                </div>
            </div>
        </div>
    </div>
    <table class="table table-striped" >
        <tr>
            <td>No.</td>
            <td>Remote Access</td>
            <td>IP</td>
            <td>Operator</td>
            <td>Category</td>
            <td>Item Type</td>
            <td>Action</td>
            <td>Result</td>

            <!--<td>Old</td>-->
            <td>Time</td>
            <td>New</td>

        </tr>
        <%    if (arrAudit != null) {

                for (int i = 0; i < arrAudit.length; i++) {
                    DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
                    Date time = (arrAudit[i].getAuditedon());
                    String exactTime = time.getHours()+ ":" + time.getMinutes()+":"+time.getSeconds();

        %>
        <tr>
            <td><%=i + 1%></td>
            <td><%=arrAudit[i].getRemoteaccesslogin()%></td>
            <td><%=arrAudit[i].getIpaddress()%></td>
            <td><%=arrAudit[i].getOperatorname()%></td>
            <td><%=arrAudit[i].getCategory()%></td>
            <td><%=arrAudit[i].getItemtype()%></td>
            <td><%=arrAudit[i].getAction()%></td>
            <td><%=arrAudit[i].getResult()%></td>
            <td><%=sdf.format(time)%></td>
            <td><%=arrAudit[i].getNewvalue()%></td>
            <!--<td><%=arrAudit[i].getOldvalue()%></td>-->

        </tr>
        <%}
        } else {%>
        <tr>
            <td>1</td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <!--<td><%=strmsg%></td>-->
        </tr>
        <%}%>
    </table>
    <div class="row-fluid">
        <div class="span6">
            <!--        <div class="control-group">                        
                        <div class="span1">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="tokenFailureReportCSV()" >
                                    <i class="icon-white icon-chevron-down"></i> CSV</a>
                            </div>
                        </div>
                        <div class="span1">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="tokenFailureReportPDF()" >
                                    <i class="icon-white icon-chevron-down"></i> PDF</a>
                            </div>
                        </div>
            
                        <div class="span1">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="tokenFailureReportTXT()" >
                                    <i class="icon-white icon-chevron-down"></i> TXT</a>
                            </div>
                        </div>
                    </div>-->
            <div class="control-group">                        
                <div class="span1">
                    <div class="control-group form-inline">
                        <a href="#" class="btn btn-info" onclick="userReportCSV('<%=_itemType%>', '<%=_startdate%>', '<%=_enddate%>', '<%=_auditUserID%>')" >
                            <i class="icon-white icon-chevron-down"></i> CSV</a>
                    </div>
                </div>
                <div class="span1">
                    <div class="control-group form-inline">
                        <a href="#" class="btn btn-info" onclick="userReportPDF('<%=_itemType%>', '<%=_startdate%>', '<%=_enddate%>', '<%=_auditUserID%>')" >
                            <i class="icon-white icon-chevron-down"></i> PDF</a>
                    </div>
                </div>
                <div class="span1">
                    <div class="control-group form-inline">
                        <a href="#" class="btn btn-info" onclick="userReportTXT('<%=_itemType%>', '<%=_startdate%>', '<%=_enddate%>', '<%=_auditUserID%>')" >
                            <i class="icon-white icon-chevron-down"></i> TEXT</a>
                    </div>
                </div>


            </div>
        </div>
    </div> 

</div>


