<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.dictum.management.ContactManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Contacts"%>
<%
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();
    String _type = request.getParameter("_type");
    String _keyword = request.getParameter("_keyword");

    //TagsManagement tagmngObj = new TagsManagement();
    //Tags[] tagsObj = tagmngObj.getAllTags(_channelId);



    Contacts[] contacts = null;

    ContactManagement contact = new ContactManagement();

    if (_type.compareTo("2") == 0) {
        contacts = contact.SearchContactsByTag(sessionId, _channelId, _keyword);

    } else if (_type.compareTo("1") == 0) {
        contacts = contact.SearchContactsByType(sessionId, _channelId, _keyword);
    }
%>
<h3>Searched Results for <i>"<%=_keyword%>"</i></h3>
<div id="DeleteMultipleContacts" ><p><a href="#" class="btn btn-mini" onclick="deleteContactList()">Delete Contacts</a></p></div>
<div id="ChangeContactStatus" ><p><a href="#" class="btn btn-mini" onclick="ChangeContactStatus()">Change Status</a></p></div>


<table class="table table-striped" id="table_main">
    <tr><td>Select</td>
        <td>No</td>
        <td>Name</td>
        <td>Phone</td>
        <td>Email</td>
        <td>Fax</td>
        <td>Tag</td>
          <td>Status</td>
        <td>Manage</td>
        <td>Created On</td>
        <td>Modified On</td>
    </tr>
    <%
        if (contacts != null){
            for (int i = 0; i < contacts.length; i++) {
                Contacts conObj = contacts[i];

                String strTagName = "";
                String[] strTags = conObj.getTags().split(",");

                /*for(int j=0;j<strTags.length;j++){
                 for ( int k=0;k<tagsObj.length;k++){                    
                 if(tagsObj[k].getTagid().compareTo(strTags[j]) == 0 ){
                 strTagName = tagsObj[k].getTagname();
                 break;                                
                 }
                 }
                 }*/




    %>
    <tr id="contact_search_<%=conObj.getContactid()%>">
        <td> <input type="checkbox" id="_sel<%=conObj.getContactid()%>" name="_sel<%=conObj.getContactid()%>" value="false" onchange="UpdateDeleteSelectContactList(<%=conObj.getContactid()%>)"></td>
        <td><%=(i + 1)%></td>
        <td><%=conObj.getName()%></td>
        <td><%=conObj.getPhone()%></td>
        <td><%=conObj.getEmailid()%></td>
        <td><%=conObj.getFaxno()%></td>        
        <td>
            <%

                for (int j = 0; j < strTags.length; j++) {

            %>
            <span class="label label-info"><%=strTags[j]%></span>
            <%

                }


            %>
        </td>
          <%
               
                if(conObj.getStatus() == -1){%>
         <td><span class="label label-success">Suspended</span></td>   
         <%}else{%>
          <td><span class="label label-important">Active</span></td>   
         <%}%>
        <td> 
            <div class="btn-group">
                <button class="btn btn-mini">Manage</button>
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="deleteContact(<%=conObj.getContactid()%>)" >Delete Contact</a></li>
                    <li><a href="#"  onclick="getContact(<%=conObj.getContactid()%>)">Edit Contact</a></li>                    
                </ul>
            </div>
        </td>
        <td><%=conObj.getCreatedon()%></td>
        <td><%=conObj.getModifiedon()%></td>
        <!--</td>-->
    </tr>
    <% }}%>
</table>
<BR><BR>
<form id="deleteContactListForm" name="deleteContactListForm">
    <input type="hidden" name="_deleteContactsList" id="_deleteContactsList" value=",">
</form>
<script>
    document.getElementById("DeleteMultipleContacts").style.display = 'none';
    document.getElementById("ChangeContactStatus").style.display = 'none';
</script>
