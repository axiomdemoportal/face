<%@page import="com.mollatech.axiom.connector.access.controller.AccessMatrixSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.Roles"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Operators"%>
<%
    Operators operatorS1 = (Operators) request.getSession().getAttribute("_apOprDetail");
    Operators oprObj1 = (Operators) session.getAttribute("_apOprDetail");
    OperatorsManagement oprmngObj1 = new OperatorsManagement();
    if(oprObj1 == null){
        return;
    }
    Roles[] roles1 = oprmngObj1.getAllRoles(oprObj1.getChannelid());
    Roles adminRole=oprmngObj1.getRoleById(oprObj1.getChannelid(), oprObj1.getRoleid());
    String strRole = "";
    for (int j = 0; j < roles1.length; j++) {
        if (operatorS1.getRoleid() == roles1[j].getRoleid()) {
            strRole = roles1[j].getName();
            session.setAttribute("_apOprRoleName", strRole);
            break;
        }
    }

    int iOperatorRole1 = 0;

    session.setAttribute("_apOprRole", "" + oprObj1.getRoleid());
    AccessMatrixSettings accessObj1 = (AccessMatrixSettings) session.getAttribute("_apAccessEntry");
    
    if (adminRole.getName().equals("sysadmin")) {
%>
<jsp:include page="channels.jsp" /> 
<%
} else if (oprObj1.getRoleid() != 1) {

   if (accessObj1.listChannel == true) { %>
<jsp:include page="channels.jsp" /> 
<%} else  { %>
<jsp:include page="channels.jsp" /> 
<%}
    }%>
