<%@page import="com.mollatech.axiom.nucleus.settings.PluginSetting"%>
<%@page import="com.mollatech.axiom.nucleus.settings.IPConfigFilter"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>

<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@include file="header.jsp" %>
<%
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");

    SettingsManagement smng = new SettingsManagement();
    PluginSetting fileObj = (PluginSetting) smng.getSetting(sessionId, channel.getChannelid(), SettingsManagement.PLUGIN_SETTING, SettingsManagement.PREFERENCE_ONE);

%>

<link href="./assets/css/bootstrap-fileupload.css" rel="stylesheet"/>
<script src="./assets/js/bootstrap-fileupload.js"></script>
<link href="./assets/js/select2/select2.css" rel="stylesheet">
<script src="./assets/js/select2/select2.js"></script>
<script src="./assets/js/ajaxfileupload.js"></script>
<script src="./assets/js/pluginsettings.js"></script>

<meta charset="utf-8">
<title><%=strContextPath%> Management Portal For You</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
<link href="./assets/css/bootstrap.css" rel="stylesheet">
<div class="container-fluid">
    <h2>Caller IP Restriction</h2>
    <p>You can enforce IP restriction where only selected IPs shall be allowed to use the system.</p>
    <br>

    <div class="row-fluid">


        <div class="control-group">
            <label class="control-label"  for="username">File List:</label>
            <div class="controls">
                <textarea name="_fileNames" id="_fileNames" style="width:100%">
                    <%
                        String[] strKeywords;
                        int iStatus = 1;
                        if (fileObj == null) {
                            strKeywords = new String[1];
                            strKeywords[0] = "";

                        } else {
                            strKeywords = fileObj.getFileName();
                            // iStatus = ifObj.getStatus();
                        }
                        if (strKeywords != null)
                            for (int j = 0; j < strKeywords.length; j++) {
                    %>        
                    <%=strKeywords[j]%>,
                    <%
                        }
                    %>
                </textarea>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <div id="save-plugin-settings-result"></div>
                <button class="btn btn-primary" onclick="SavePluginFilerList()" type="button" id="SaveIpFilteringButton">Save Changes Now >> </button>
            </div>
        </div>

        <form class="form-horizontal" id="UploadFileForm" name="UploadFileForm">
            <div class="control-group">
                <label class="control-label"  for="username">Select File</label>                                    
                <div class="fileupload fileupload-new" data-provides="fileupload">
                    <div class="input-append">
                        <div class="uneditable-input span4"><i class="icon-file fileupload-exists"></i> 
                            <span class="fileupload-preview"></span>
                        </div>
                        <span class="btn btn-file">
                            <span class="fileupload-new">Select file</span>
                            <span class="fileupload-exists">Change</span>
                            <input type="file" id="licensefile" name="licensefile"/>
                        </span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                    </div>
                </div>
            </div>       
            <hr>
            <div class="control-group">
                <div class="controls">
                    <div id="save-epin-settings-result"></div>
                    <button class="btn btn-primary" onclick="UploadNow()" type="button" id="SavePluginButton">Upload Plugin >> </button>
                </div> 
            </div>
        </form>
    </div>


    <script language="javascript" type="text/javascript">
                        //LoadGlobalSettings();        
    </script>
</div>


<script>
    //$(document).ready(function() { $("#_keywords").select2() }); 

    $(document).ready(function() {
        $("#_fileNames").select2({
            tags: [],
            dropdownCss: {display: 'none'},
            tokenSeparators: [","]
        });
        
        
    });
</script>

<%@include file="footer.jsp" %>