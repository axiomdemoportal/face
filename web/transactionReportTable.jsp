<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Txdetails"%>
<%@include file="header.jsp" %>
<%
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();
    Txdetails[] txdetailses = (Txdetails[]) session.getAttribute("txdetailses");
    String updated = "Not Available";
%>
<html>
    <script src="./assets/js/usermanagement.js"></script>
    <div class="container-fluid">
        <body>

            <h1 class="text-success">Transaction Details</h1>
            <div id='Settings_table'>
                <table class="table table-striped" id="table_main">

                    <tr>
                        <td>No.</td>
                        <td>Name</td>
                        <td>Email</td>
                        <td>Type</td>
                        <td>Status</td>
                        <td>Transaction Id</td>
                        <td>Created On</td>
                        <td>Updated On</td>

                    </tr>
                    <%
                        int count = 0;
                        for (int j = 0; j < txdetailses.length; j++) {
                            AuthUser user = new UserManagement().getUser(sessionId, _channelId, txdetailses[j].getUserid());
                            if ( user == null )
                                continue;
                            count ++;
                            
                    %> <tr>
                        <td><%=count%></td>
                        <td><%=user.userName%></td>
                        <td><%=user.email%></td>
                        <%
                            String strType = "";
                            if (txdetailses[j].getType()!=null && txdetailses[j].getType() == 1) {
                                strType = "Push";
                            } else if (txdetailses[j].getType()!=null && txdetailses[j].getType() == 2) {
                                strType = "SMS";
                            } else if (txdetailses[j].getType()!=null && txdetailses[j].getType() == 3) {
                                strType = "Voice Callback";
                            } else if (txdetailses[j].getType()!=null && txdetailses[j].getType() == 4) {
                                strType = "Voice Missed Call";
                            }

                        %>
                        <td><%=strType%></td>

                        <%
                            String strstatus = "";
                            if (txdetailses[j].getStatus() == 2) {
                                strstatus = "Pending";
                            } else if (txdetailses[j].getStatus() == -1) {
                                strstatus = "Expired";
                            } else if (txdetailses[j].getStatus() == 0) {
                                strstatus = "Responded";
                            } else if (txdetailses[j].getStatus() == 1) {
                                strstatus = "Canceled";
                            } else if (txdetailses[j].getStatus() == 3) {
                                strstatus = "Approved";
                            } else if (txdetailses[j].getStatus() == 4) {
                                strstatus = "Denied";
                            }

                        %>


                        <td><%=strstatus%></td>
                        <td><%=txdetailses[j].getTransactionId()%></td>
                        <td><%=txdetailses[j].getCreatedOn()%></td>
                        <%
                            updated = "Not Available";
                            if (txdetailses[j].getUpdatedOn() != null) {
                                updated = txdetailses[j].getUpdatedOn().toString();
                            }
                        %>
                        <td><%=updated%></td>

                    </tr><%   }
                    %>
                </table>
            </div>
            <div class="span9">
                <div class="control-group">
                    <div class="span6">
                        <div class="control-group form-inline">
                            <a href="#" class="btn btn-success" onclick="txReport(1)" >
                                <i class="icon-white icon-chevron-down"></i>Download CSV</a>

                            <a href="#" class="btn btn-success" onclick="txReport(0)" >
                                <i class="icon-white icon-chevron-down"></i>Download PDF</a>
                        </div>
                    </div>
                  </div>
            </div>
        </body>
    </div>
</html>
<%@include file="footer.jsp" %>