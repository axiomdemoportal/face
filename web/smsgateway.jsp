<%@include file="header.jsp" %>
<script src="./assets/js/smsgateway.js"></script>
<div class="container-fluid">
    <h1 class="text-success">SMS Gateway Configuration</h1>
    <p>To facilitate mobile based notification, SMS Gateway(s) connection needs to be configured. For auto fail-over, please set the Secondary Gateway too. </p>
    <div class="tabbable">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#primary" data-toggle="tab">Primary SMS Gateway</a></li>
            <li><a href="#secondary" data-toggle="tab">Secondary SMS Gateway</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="primary">
                <div class="row-fluid">
                    <form class="form-horizontal" id="smsprimaryform" name="smsprimaryform">

                        <div id="legend">
                            <legend class="">Primary SMS Gateway Configuration</legend>
                        </div>

                        <div class="control-group">
                            <label class="control-label"  for="username">Status </label>
                            <div class="controls">
                                <div>
                                    <div class="btn-group">

                                        <button class="btn btn-small"><div id="_status-primary-sms"></div></button>
                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="ChangeActiveStatusSMS(1,1)">Mark as Active?</a></li>
                                            <li><a href="#" onclick="ChangeActiveStatusSMS(1,0)">Mark as Suspended?</a></li>
                                        </ul>
                                    </div>
                                    with Auto Fail-Over to Secondary Gateway 
                                    <div class="btn-group">

                                        <button class="btn btn-small"><div id="_autofailover-primary-sms"></div></button>
                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="ChangeFailOverSMS(1)">Mark as Enabled?</a></li>
                                            <li><a href="#" onclick="ChangeFailOverSMS(0)">Mark as Disabled?</a></li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <input type="hidden" id="_status" name="_status">
                        <input type="hidden" id="_perference" name="_perference" value="1">
                        <input type="hidden" id="_type" name="_type" value="1">
                        <input type="hidden" id="_autofailover" name="_autofailover">
                        <input type="hidden" id="_retries" name="_retries">
                        <input type="hidden" id="_retryduration" name="_retryduration">
                        <input type="hidden" id="_messgeLength" name="_messgeLength">

                        <div class="control-group">
                            <label class="control-label"  for="username">Retry Count</label>
                            <div class="controls">
                                <div>
                                    <div class="btn-group">
                                        <button class="btn btn-small"><div id="_retries-primary-sms"></div></button>
                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                             <li><a href="#" onclick="ChangeRetrySMS(1,0)">Disable</a></li>
                                             <li><a href="#" onclick="ChangeRetrySMS(1,1)">1 Retries</a></li>
                                            <li><a href="#" onclick="ChangeRetrySMS(1,2)">2 Retries</a></li>
                                            <li><a href="#" onclick="ChangeRetrySMS(1,3)">3 Retries</a></li>
                                            <li><a href="#" onclick="ChangeRetrySMS(1,5)">5 Retries</a></li>
                                        </ul>
                                    </div>
                                    with wait time between retries as      
                                    <div class="btn-group">
                                        <button class="btn btn-small"><div id="_retryduration-primary-sms"></div></button>
                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="ChangeRetryDurationSMS(1,10)">10 seconds</a></li>
                                            <li><a href="#" onclick="ChangeRetryDurationSMS(1,30)">30 seconds</a></li>
                                            <li><a href="#" onclick="ChangeRetryDurationSMS(1,60)">60 seconds</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="control-group">
                            <label class="control-label"  for="username">Allow Message length(>160)</label>
                            <div class="controls">
                                <div>
                                    <div class="btn-group">
                                        <button class="btn btn-small"><div id="_messgeLength-primary-sms"></div></button>
                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="MessageLengthSMS(1,0)">Yes</a></li>
                                            <li><a href="#" onclick="MessageLengthSMS(1,1)">No</a></li>

                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>



                        <div class="control-group">
                            <label class="control-label"  for="username">Host:Port</label>
                            <div class="controls">
                                <input type="text" id="_ip" name="_ip" placeholder="example localhost/127.0.0.1" class="clean-input signatureParam">
                                : <input type="text" id="_port" name="_port" placeholder="443" class="span1">
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label"  for="username">Authenticate using</label>
                            <div class="controls">
                                <input type="text" id="_userId" name="_userId" placeholder="account id for authenticaiton " class="input-xlarge">
                                : <input type="password" id="_password" name="_password" placeholder="password for  authentication" class="input-xlarge">
                            </div>
                        </div>


                        <div class="control-group">
                            <label class="control-label"  for="username">Registered Phone</label>
                            <div class="controls">
                                <input type="text" id="_phoneNumber" name="_phoneNumber" placeholder="display number while sending" class="span3">                                
                            </div>
                        </div>
                        <hr>
                        <div class="control-group">
                            <label class="control-label"  for="username">Additional Attributes</label>
                            <div class="controls">
                                <input type="text" id="_reserve1" name="_reserve1" placeholder="name1=value1" class="span3">
                                : <input type="text" id="_reserve2" name="_reserve2" placeholder="any setting" class="span3">
                                : <input type="text" id="_reserve3" name="_reserve3" placeholder="parameters" class="span3">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Implementation Class</label>
                            <div class="controls">
                                <input type="text" id="_className" name="_className" placeholder="complete class name including package" class="span6">
                            </div>
                        </div>

                        <!-- Submit -->
                        <div class="control-group">
                            <div class="controls">
                                <div id="save-sms-gateway-primary-result"></div>
                                <%// if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
                                <button class="btn btn-primary" onclick="EditSMSSetting(1)" type="button">Save Setting Now >> </button>
                                <%//}%>
                                <button class="btn" onclick="LoadTestSMSConnectionUI(1)" type="button">Test Connection >> </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <div class="tab-pane" id="secondary">
                <div class="row-fluid">
                    <form class="form-horizontal" id="smsecondaryform" name="smsecondaryform" >
                        <fieldset>
                            <div id="legend">
                                <legend class="">Secondary SMS Gateway Configuration</legend>
                            </div>
                            <div class="control-group">
                                <label class="control-label"  for="username">Status </label>
                                <div class="controls">
                                    <div class="btn-group">
                                        <button class="btn btn-small"><div id="_status-secondary-sms"></div></button>
                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="ChangeActiveStatusSMS(2,1)">Mark as Active?</a></li>
                                            <li><a href="#" onclick="ChangeActiveStatusSMS(2,0)">Mark as Suspended?</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" id="_statusS" name="_statusS">
                            <input type="hidden" id="_retriesS" name="_retriesS">
                            <input type="hidden" id="_retrydurationS" name="_retrydurationS">
                            <input type="hidden" id="_perference" name="_perference" value="2">
                            <input type="hidden" id="_type" name="_type" value="1">
                            <input type="hidden" id="_messgeLengthS" name="_messgeLengthS">

                            <div class="control-group">
                                <label class="control-label"  for="username">Retry Count</label>
                                <div class="controls">
                                    <div class="btn-group">
                                        <button class="btn btn-small"><div id="_retries-secondary-sms"></div></button>
                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="ChangeRetrySMS(2,0)">Disable</a></li>
                                            <li><a href="#" onclick="ChangeRetrySMS(2,1)">1 Retries</a></li>
                                            <li><a href="#" onclick="ChangeRetrySMS(2,2)">2 Retries</a></li>
                                            <li><a href="#" onclick="ChangeRetrySMS(2,3)">3 Retries</a></li>
                                            <li><a href="#" onclick="ChangeRetrySMS(2,5)">5 Retries</a></li>
                                        </ul>
                                    </div>
                                    with wait time between retries as    
                                    <div class="btn-group">
                                        <button class="btn btn-small"><div id="_retryduration-secondary-sms"></div></button>
                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="ChangeRetryDurationSMS(2,10)">10 seconds</a></li>
                                            <li><a href="#" onclick="ChangeRetryDurationSMS(2,30)">30 seconds</a></li>
                                            <li><a href="#" onclick="ChangeRetryDurationSMS(2,60)">60 seconds</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"  for="username">Allow Message length(>160)</label>
                                <div class="controls">
                                    <div>
                                        <div class="btn-group">
                                            <button class="btn btn-small"><div id="_messgeLength-secondary-sms"></div></button>
                                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#" onclick="MessageLengthSMS(2,0)">Yes</a></li>
                                                <li><a href="#" onclick="MessageLengthSMS(2,1)">No</a></li>

                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <hr>

                            <div class="control-group">
                                <label class="control-label"  for="username">Host:Port </label>
                                <div class="controls">
                                    <input type="text" id="_ipS" name="_ipS" placeholder="example localhost/127.0.0.1" class="span2">
                                    : <input type="text" id="_portS" name="_portS" placeholder="443" class="span1">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"  for="username">Authenticate using </label>
                                <div class="controls">
                                    <input type="text" id="_userIdS" name="_userIdS" placeholder="leave blank is no authentication" class="span3">
                                    : <input type="password" id="_passwordS" name="_passwordS" placeholder="leave blank is no authentication" class="span3">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"  for="username">Registered Phone</label>
                                <div class="controls">
                                    <input type="text" id="_phoneNumberS" name="_phoneNumberS" placeholder="display number while sending" class="span3">                                    
                                </div>
                            </div>
                            <hr>
                            <div class="control-group">
                                <label class="control-label"  for="username">Additional Attributes</label>
                                <div class="controls">
                                    <input type="text" id="_reserve1S" name="_reserve1S" placeholder="additional details >> name1=value1" class="span3">
                                    : <input type="text" id="_reserve2S" name="_reserve2S" placeholder="additional details >> name1=value1" class="span3">
                                    : <input type="text" id="_reserve3S" name="_reserve3S" placeholder="additional details >> name1=value1" class="span3">
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label"  for="username">Implementation Class</label>
                                <div class="controls">
                                    <input type="text" id="_classNameS" name="_classNameS" placeholder="complete class name including package" class="input-xlarge">
                                </div>
                            </div>
                            <!-- Submit -->
                            <div class="control-group">
                                <div class="controls">
                                    <%// if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
                                    <button class="btn btn-primary"  onclick="EditSMSSetting(2)" type="button">Save Setting Now >> </button>
                                    <%//}%>
                                    <button class="btn" onclick="LoadTestSMSConnectionUI(2)" type="button">Test Connection >> </button>
                                    <div id="save-sms-gateway-secondary-result"></div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <script language="javascript" type="text/javascript">
        LoadSMSSetting(1);
        LoadSMSSetting(2);
    </script>
</div>




<div id="testSMSPrimary" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Test Primary Gateway</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="testSMSPrimaryForm" name="testSMSPrimaryForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Test Message</label>
                        <div class="controls">
                            <input type="text" id="_testmsg" name="_testmsg" placeholder="sample message to be sent out..." class="input-xlarge">
                            <input type="hidden" id="_type" name="_type" value="1">
                            <input type="hidden" id="_preference" name="_preference" value="1">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Phone</label>
                        <div class="controls">
                            <input type="text" id="_testphone" name="_testphone" placeholder="phone starts with + " class="input-xlarge">
                        </div>
                    </div>                    
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div class="span3" id="test-sms-primary-configuration-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="testconnectionSMSprimary()" id="testSMSPrimaryBut">Send Message Now</button>
    </div>
</div>

<div id="testSMSSecondary" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Test Secondary Gateway</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="testSMSSecondaryForm" name="testSMSSecondaryForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Test Message</label>
                        <div class="controls">
                            <input type="text" id="_testmsgS" name="_testmsgS" placeholder="this is the sample message to be sent out..." class="input-xlarge">
                            <input type="hidden" id="_type" name="_type" value="1">
                            <input type="hidden" id="_preference" name="_preference" value="2">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Phone</label>
                        <div class="controls">
                            <input type="text" id="_testphoneS" name="_testphoneS" placeholder="set phone for notification" class="input-xlarge">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div class="span3" id="test-sms-secondary-configuration-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="testconnectionSMSsecondary()" id="testSMSSecondaryBut">Send Message Now</button>
    </div>
</div>

<%@include file="footer.jsp" %>