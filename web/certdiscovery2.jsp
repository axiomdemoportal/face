<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserGroupsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Usergroups"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/certdiscovery.js"></script>
<script src="./assets/js/otptokens.js"></script>
<script src="./assets/js/pkitokens.js"></script>
<script src="./assets/js/challengeresponse.js"></script>
<script src="./assets/js/trustedDevice.js"></script>
<script src="./assets/js/geotracking.js"></script>
<script src="./assets/js/usermanagement.js"></script>
<script src="./assets/js/securephrase.js"></script>
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->


<div class="container-fluid" >
    <div id="auditTable">
        <h1 class="text-success"> <b>Certificate Details</b></h1>
        <p></p>
        <h3>Search Certificates</h3>   
        <div class="input-append">
            <div class="container-fluid" id="certDiscoveryForm" name="certDiscoveryForm">
                <h6>Enter Hostname or Ip Address and SubnetMask(eg.192.168.0/24)  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enter port(eg.8443,9443)</h6>
                <input type="text" id="_IP" name="_IP" placeholder="192.168.0/24" class="span4">&nbsp;&nbsp;&nbsp;&nbsp;
               
                <input type="text" id="_port" name="_port" placeholder="8443">     &nbsp;     
                                <select span="2" id="SearchCert">
                <option value="" disabled selected hidden>Select Report Type</option>
                <option value="Online">Online</option>
                 <option value="Offline">Offline</option>
              </select>
                &nbsp;
             
                <button  class="btn btn-success" span="2"  onclick="certDiscovery()" id="searchCertButton">Search Now</button>
                <br>
                <br>
            </div>
        </div>
        

        <div id="cert_table_main">
        </div>
    </div>
</div>

       


    <%@include file="footer.jsp" %>