<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@include file="header.jsp" %>
<div class="container-fluid">
    <h1 class="text-success"> Signature Verification</h1>

    <%            
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        OperatorsManagement opMngt = new OperatorsManagement();
    %>
    <script>
        function strcmpChallenge(a, b)
        {
            return (a < b ? -1 : (a > b ? 1 : 0));
        }

        function verify()
        {
            var signData = document.getElementById("sigverified").value;
            alert(signData);
            var jsondata = {"sigverified": signData};
            $.ajax({
                type: 'POST',
                url: "./VerifySignature",
                dataType: 'json',
                data: jsondata,
                success: function(data) {
                    if (strcmpChallenge(data._result, "error") == 0) {

                        document.getElementById("_result").innerHTML = " Verification result: Error";
                    }
                    else if (strcmpChallenge(data._result, "success") == 0) {
                        alert("Signature verified Successfully");
                        document.getElementById("_result").innerHTML = "Verification result: Success";
                    }
                }
            });
        }
    </script>
</div>

<title>PKCS7 Signature Verification</title>
<div class="row-fluid">
    <form id="frmVerifySign" action="VerifySignature"> 
        <table style="width:100%">
            <tr>
                <td style="text-align:center;width: 50%" >
                    Verifying Signature(PKCS7 Signature)<br/>
                    <textarea name="sigverified" id="sigverified" rows="10" cols="300" style="width:80%">
MIAGCSqGSIb3DQEHAqCAMIACAQExCzAJBgUrDgMCGgUAMIAGCSqGSIb3DQEHAaCAJIAEF3NvbWUgYnl0ZXMgdG8gYmUgc2lnbmVkAAAAAAAAoIAwggOSMIICeqADAgECAgRWZ2euMA0GCSqGSIb3DQEBCwUAMIGKMSIwIAYJKoZIhvcNAQkBFhNtYW5vakBtb2xsYXRlY2guY29tMQswCQYDVQQGEwJJTjELMAkGA1UECAwCTUgxDTALBgNVBAcMBFB1bmUxEzARBgNVBAoMCkJsdWVCcmlja3MxETAPBgNVBAsMCEluZm9UZWNoMRMwEQYDVQQDDApCbHVlQnJpY2tzMB4XDTE1MTIwODIzMjkzMloXDTE2MTIwODIzMjkzMlowgYoxIjAgBgkqhkiG9w0BCQEWE21hbm9qQG1vbGxhdGVjaC5jb20xCzAJBgNVBAYTAklOMQswCQYDVQQIDAJNSDENMAsGA1UEBwwEUHVuZTETMBEGA1UECgwKQmx1ZUJyaWNrczERMA8GA1UECwwISW5mb1RlY2gxEzARBgNVBAMMCkJsdWVCcmlja3MwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDe7WVkhjNA8/vkdZPenlPayL2GL7QKysK3wTsyCcsLoVgPWaQam+T6ipSqdv3yzN3dSu+6ieNAlqdrc39WnWD+HnZLudg9KpIug/JaR0q3+7qqCc8xAxFloCUPcsOUE1/pCLR252dKPOJ4GzQK580O/z9Cooxy950z+XiOxpJoOm0HTqz9Q4qU1SIVV/XozJL+YQHGHFCcU2he0VYwH6lMz2ayxjbmKQD3mt/TaQEp042yQSDCTjALb0p3f2FC9oJrXoNWn/Gs52FD/L8yeQUO2Bin6ZdWxlY36KMXX1am2vmkmLzE1xzvXA/+HpFeYx9mU1JhTCeC+1vvOxDHtZ/nAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAGikUhXAN4xhf7CvPaIrJ8Xu5di+0VMlfkMbEKsEpBwVXPR394steTNnNwuDJ/njjeH2xD3MsaqY/HOC+2J1Jx+Hi4P/A66OOZdKq2jniwt+gA1g057Xqh/4FSDta1zqio+5oDiMW79rp14WukrYk31rOBnOpHslJcHlIzVFhFVZgAUyMyKVfObSgJQhSqVdSqrBcM+loXZUztYb2DVkcpAcuTZaY1o/ixtBGxFMGxJJnsd1sFeIJm4JDv33TPsYbsOdqqrpPQHMrDSWseVY3fN7qGyMpU/luIjOOiFjuEWXhShFK0AwVvycPh+HrQExXkJDWnBb3IZSqNschxJ2FGYAADGCAhowggIWAgEBMIGTMIGKMSIwIAYJKoZIhvcNAQkBFhNtYW5vakBtb2xsYXRlY2guY29tMQswCQYDVQQGEwJJTjELMAkGA1UECAwCTUgxDTALBgNVBAcMBFB1bmUxEzARBgNVBAoMCkJsdWVCcmlja3MxETAPBgNVBAsMCEluZm9UZWNoMRMwEQYDVQQDDApCbHVlQnJpY2tzAgRWZ2euMAkGBSsOAwIaBQCgXTAYBgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcBMBwGCSqGSIb3DQEJBTEPFw0xNjAxMTIwODA1NTFaMCMGCSqGSIb3DQEJBDEWBBRBj6kU9lZtTLl4wzCD852XqhU1jzANBgkqhkiG9w0BAQEFAASCAQBmEjI3kM3hlO+hKJ2e9dNpFnNOYJDciReqJ2y9XcHIAfDNyHDk1AU4Rk/mlJVXdWszQ6R/l9IPq35tCecXzlD0BaOeYprgsK4qNGzSbQQLPWUKbsLv//XvZpaHlJPIQOmFJVwHHnMVrbVX8vfjS4rvLEgtYcP0YkGHKn/a4I0DqFhpAqY7ERS/ylFGfHAKjhmUaOiRIPCWCz5xFwWr/OiCvzS3IFzlsmVPStwL9cE/TdQwopl2E2vy8NIbJMUc93IlDJDRK7WxHS9oL6kb5/giQw5/Fr3KKxnxCTU37dM94Jz7Q//9CvIrQkxRH+/QTWN9+JS/04UPfWvwltIIPAYkAAAAAAAA

                    </textarea><br/>
                    <!--Text message to be verified.<br/>
                    <input type="text" name="msgverified" size="100" value="aaa"/><br/>
                    Signer's Public Key Certificate.<br/>
                    <textarea name="cert" rows="10" cols="65">
                    -----BEGIN CERTIFICATE-----
                    MIIBvTCCASYCCQD55fNzc0WF7TANBgkqhkiG9w0BAQUFADAjMQswCQYDVQQGEwJK
                    UDEUMBIGA1UEChMLMDAtVEVTVC1SU0EwHhcNMTAwNTI4MDIwODUxWhcNMjAwNTI1
                    MDIwODUxWjAjMQswCQYDVQQGEwJKUDEUMBIGA1UEChMLMDAtVEVTVC1SU0EwgZ8w
                    DQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBANGEYXtfgDRlWUSDn3haY4NVVQiKI9Cz
                    Thoua9+DxJuiseyzmBBe7Roh1RPqdvmtOHmEPbJ+kXZYhbozzPRbFGHCJyBfCLzQ
                    fVos9/qUQ88u83b0SFA2MGmQWQAlRtLy66EkR4rDRwTj2DzR4EEXgEKpIvo8VBs/
                    3+sHLF3ESgAhAgMBAAEwDQYJKoZIhvcNAQEFBQADgYEAEZ6mXFFq3AzfaqWHmCy1
                    ARjlauYAa8ZmUFnLm0emg9dkVBJ63aEqARhtok6bDQDzSJxiLpCEF6G4b/Nv/M/M
                    LyhP+OoOTmETMegAVQMq71choVJyOFE5BtQa6M/lCHEOya5QUfoRF2HF9EjRF44K
                    3OK+u3ivTSj3zwjtpudY5Xo=
                    -----END CERTIFICATE-----
                    </textarea>-->
                </td>
                <td style="width:50%;text-align:left">
                    <p><a href="#addNewRole" class="btn btn-primary" onclick="verify()" >Verify Now &uarr;</a></p>
                </td>
            <br/>

            </tr>
        </table>
</div>
<div>
    <div id="_result"  name="_result" class="text-success">
    </div>
</div>

</form>

<%@include file="footer.jsp" %>

