<%@page import="com.mollatech.axiom.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Webresource"%>
<%@page import="com.mollatech.axiom.nucleus.db.Usergroups"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserGroupsManagement"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();
    String _searchtext = request.getParameter("_keyword");
    ResourceManagement rmngtObj = new ResourceManagement();
    Webresource resource = new Webresource();
    Webresource Res[] = null;
    Res = rmngtObj.SearchResourcs(sessionId, _channelId, _searchtext);
//    Users = usermngObj.SearchUsers(sessionId, _channelId, _searchtext);
//     Res = usermngObj.SearchResourcs(sessionId, _channelId, _searchtext);

    if (Res != null) {
%>
<h3>Results for <i>"<%=_searchtext%>"</i></h3>

<table class="display responsive wrap" id="table_main">
    <thead>
        <tr>
            <th>No.</th>
            <th>Name</th>
            <th>Web URL</th>
            <th>Image</th>
            <th>Groups</th>
            <th>Status</th>        
            <th>Created On</th>
            <th>Last Access On</th>
        </tr>
    </thead>
    <%}
        if (Res != null) {
            for (int i = 0; i < Res.length; i++) {
                java.util.Date dCreatedOn = Res[i].getCreatedOn();
                java.util.Date dLastUpdated = Res[i].getLastupOn();
                SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm");
                String strStatus = "Inactive";
                if (Res[i].getStatus() == 1) {
                    strStatus = "Active";
                } else if (Res[i].getStatus() == 0) {
                    strStatus = "Suspended";
                }
                String userStatus = "user-status-value-" + i;
    %>
    <tr id="user_search_<%=Res[i].getResourceid()%>">
        <td><%=(i + 1)%></td>
        <td><%=Res[i].getResourcename()%></td>
        <td><%=Res[i].getWeburl()%></td>
        <td>
            <img id="my_image" src="./DownloadImage?_resid=<%=Res[i].getResourceid()%>"  height="25" width="30"/>            
        </td>
        <td><%

            String glist = Res[i].getGroups();
            String[] grouplist = glist.split("\\s*,\\s*");
            for (int j = 0; j < grouplist.length; j++) {
            %>
            <span class="label label-info"><%=grouplist[j]%></span>
            <%
                }

            %>
        </td>
        <td>
            <div class="btn-group">
                <button class="btn btn-mini" id="<%=userStatus%>"><%=strStatus%></button>
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#"  onclick="changestat(1, <%=Res[i].getResourceid()%>, '<%=userStatus%>', '<%=_searchtext%>')" >Mark as Active?</a></li>
                    <li><a href="#" onclick="changestat(0,<%=Res[i].getResourceid()%>, '<%=userStatus%>', '<%=_searchtext%>')" >Mark as Suspended?</a></li>
                    <li class="divider"></li>
                    <li><a href="#editUser" onclick="loadResourceDetails('<%=Res[i].getResourceid()%>')" data-toggle="modal">Edit Details</a></li>
                    <li><a href="#" onclick="removeResource('<%=Res[i].getResourceid()%>')" data-toggle="modal"><font color="red">Remove?</font></a></li>
                </ul>
            </div>
        </td>
        <td><%=sdf.format(dCreatedOn)%></td>
        <td><%=sdf.format(dLastUpdated)%></td>
    </tr>
    <% } %>        
</table>
<%  } else if (Res == null) { %>
<h3>No Record found...</h3>
<%}%>
<br><br>
<script>
    $(document).ready(function () {
        $('#table_main').DataTable({
            responsive: true
        });
    });
</script>