<%@page import="com.mollatech.axiom.nucleus.settings.BillingManagerSettings"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Hashtable"%>
<%@page import="com.mollatech.axiom.nucleus.settings.PasswordPolicySetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Templates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/billingManager.js"></script>

<div class="container-fluid">
    <h1 class="text-success">Billing Manager</h1>
    <p>To facilitate billing enforcement, you can use these settings. The features like assign, reset, change etc will follow these settings.</p>
    <div class="row-fluid">
        <form class="form-horizontal" id="billingManagersettingsform" name="billingManagersettingsform">

            <input type="hidden" id="_allowBillingManager" name="_allowBillingManager">
            <!--<input type="hidden" id="_currancyType" name="_currancyType">-->
            <!--swotp-->
            <input type="hidden" id="_allowBillingManagerSWOTP" name="_allowBillingManagerSWOTP">
            <input type="hidden" id="_selectBillTypeSWOTP" name="_selectBillTypeSWOTP">
            <input type="hidden" id="_selectBillingDurationSWOTP" name="_selectBillingDurationSWOTP">
            <input type="hidden" id="_selectAlertBeforeSWOTP" name="_selectAlertBeforeSWOTP">
            <!--hwotp-->
            <input type="hidden" id="_allowBillingManagerHWOTP" name="_allowBillingManagerHWOTP">
            <input type="hidden" id="_selectBillTypeHWOTP" name="_selectBillTypeHWOTP">
            <input type="hidden" id="_selectBillingDurationHWOTP" name="_selectBillingDurationHWOTP">
            <input type="hidden" id="_selectAlertBeforeHWOTP" name="_selectAlertBeforeHWOTP">
            <!--swPKIotp-->
            <input type="hidden" id="_allowBillingManagerSWPKIOTP" name="_allowBillingManagerSWPKIOTP">
            <input type="hidden" id="_selectBillTypeSWPKIOTP" name="_selectBillTypeSWPKIOTP">
            <input type="hidden" id="_selectBillingDurationSWPKIOTP" name="_selectBillingDurationSWPKIOTP">
            <input type="hidden" id="_selectAlertBeforeSWPKIOTP" name="_selectAlertBeforeSWPKIOTP">
            <!--HWPKIOTP-->
            <input type="hidden" id="_allowBillingManagerHWPKIOTP" name="_allowBillingManagerHWPKIOTP">
            <input type="hidden" id="_selectBillTypeHWPKIOTP" name="_selectBillTypeHWPKIOTP">
            <input type="hidden" id="_selectBillingDurationHWPKIOTP" name="_selectBillingDurationHWPKIOTP">
            <input type="hidden" id="_selectAlertBeforeHWPKIOTP" name="_selectAlertBeforeHWPKIOTP">
            <!--CERTIFICATE-->
            <input type="hidden" id="_allowBillingManagerCertificate" name="_allowBillingManagerCertificate">
            <input type="hidden" id="_selectBillTypeCertificate" name="_selectBillTypeCertificate">
            <input type="hidden" id="_selectBillingDurationCertificate" name="_selectBillingDurationCertificate">
            <input type="hidden" id="_selectAlertBeforeCertificate" name="_selectAlertBeforeCertificate">
            <!--alert-->
            <input type="hidden" id="_allowRepetationCharacterB" name="_allowRepetationCharacterB" value="Disable">
            <input type="hidden" id="_gatewayTypeB" name="_gatewayTypeB" value="Email">
            <input type="hidden" id="_alertAttemptB" name="_alertAttemptB" value="3">
            
            <input type="hidden" name="_templateName" id="_templateName" value="email.bill.reminder.alert">

            <!--OOBOTP-->
            <input type="hidden" id="_allowBillingManagerOOBOTP" name="_allowBillingManagerOOBOTP">
            <input type="hidden" id="_selectBillTypeOOBOTP" name="_selectBillTypeOOBOTP">
            <input type="hidden" id="_selectBillingDurationOOBOTP" name="_selectBillingDurationOOBOTP">
            <input type="hidden" id="_selectAlertBeforeOOBOTP" name="_selectAlertBeforeOOBOTP">
            <!--<input type="hidden" id="_txPerDayOOBOTP" name="_txPerDayOOBOTP">-->
            <hr>
            <div class="control-group">
                <label class="control-label"  for="username">Enable Billing</label>
                <div class="controls">
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_allowBillingManager_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="allowBillingManager(1, '#_allowBillingManager_div', 'Enable')">Enable</a></li>
                            <li><a href="#" onclick="allowBillingManager(0, '#_allowBillingManager_div', 'Disable')">Disable</a></li>
                        </ul>
                    </div>
                    Currency
                    <input type="text" id="_currancyType" name="_currancyType" placeholder="USD/AUD/INR/MYR" class="span2">
                    <!--                    <div class="btn-group">
                                            <button class="btn btn-small"><div id="_currancyType_div"></div></button>
                                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#" onclick="selectCurrency(1, '#_currancyType_div', 'Doller')">Doller</a></li>
                                                <li><a href="#" onclick="selectCurrency(2, '#_currancyType_div', 'INR')">INR</a></li>
                                                <li><a href="#" onclick="selectCurrency(3, '#_currancyType_div', 'YEN')">YEN</a></li>
                                                <li><a href="#" onclick="selectCurrency(4, '#_currancyType_div', 'EURO')">EURO</a></li>
                                            </ul>
                                        </div>-->

                </div>

            </div>  

            <div class="control-group">
                <label class="control-label"  for="username">  Software OTP token  </label>
                <div class="controls">
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_allowBillingManagerSWOTP_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="allowBillingManagerSWOTP(1, '#_allowBillingManagerSWOTP_div', 'Enable')">Enable</a></li>
                            <li><a href="#" onclick="allowBillingManagerSWOTP(2, '#_allowBillingManagerSWOTP_div', 'Disable')">Disable</a></li>
                        </ul>
                    </div>
                    Bill Type
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_selectBillTypeSWOTP_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <!--<li><a href="#" onclick="allowRepeatationOfCharacter1(1, '#_allowRepetationCharacter1_div', 'Enable')">Bill/Transaction</a></li>-->
                            <li><a href="#" onclick="selectBillTypeSWOTP(2, '#_selectBillTypeSWOTP_div', 'Subscription')">Subscription</a></li>
                            <li><a href="#" onclick="selectBillTypeSWOTP(3, '#_selectBillTypeSWOTP_div', 'One Time')">One Time</a></li>
                        </ul>
                    </div>
                    Duration
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_selectBillingDurationSWOTP_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <!--<li><a href="#" onclick="selectBillingDurationSWOTP(1, '#_selectBillingDurationSWOTP_div', 'Monthly')">Monthly</a></li>-->
                            <!--<li><a href="#" onclick="selectBillingDurationSWOTP(3, '#_selectBillingDurationSWOTP_div', 'Quarterly')">Quarterly</a></li>-->
                            <li><a href="#" onclick="selectBillingDurationSWOTP(2, '#_selectBillingDurationSWOTP_div', 'Yearly')">Yearly</a></li>

                        </ul>
                    </div>
                    Alert Before
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_selectAlertBeforeSWOTP_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="selectAlertBeforeSWOTP(1, '#_selectAlertBeforeSWOTP_div', '1 Week')">1 Week</a></li>
                            <li><a href="#" onclick="selectAlertBeforeSWOTP(2, '#_selectAlertBeforeSWOTP_div', '2 Weeks')">2 weeks</a></li>
                            <li><a href="#" onclick="selectAlertBeforeSWOTP(3, '#_selectAlertBeforeSWOTP_div', '3 Weeks')">3 weeks</a></li>
                        </ul>
                    </div>
                    cost
                    <input type="text" id="_costSWOTP" name="_costSWOTP" placeholder="cost" class="span1">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"  for="username">  Hardware OTP Token  </label>
                <div class="controls">
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_allowBillingManagerHWOTP_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="allowBillingManagerHWOTP(1, '#_allowBillingManagerHWOTP_div', 'Enable')">Enable</a></li>
                            <li><a href="#" onclick="allowBillingManagerHWOTP(2, '#_allowBillingManagerHWOTP_div', 'Disable')">Disable</a></li>
                        </ul>
                    </div>
                    Bill Type
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_selectBillTypeHWOTP_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <!--<li><a href="#" onclick="allowRepeatationOfCharacter1(1, '#_allowRepetationCharacter1_div', 'Enable')">Bill/Transaction</a></li>-->
                            <!--<li><a href="#" onclick="selectBillTypeHWOTP(2, '#_selectBillTypeHWOTP_div', 'Subscription')">Subscription</a></li>-->
                            <li><a href="#" onclick="selectBillTypeHWOTP(3, '#_selectBillTypeHWOTP_div', 'One Time')">One Time</a></li>
                        </ul>
                    </div>
                    Duration
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_selectBillingDurationHWOTP_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <!--<li><a href="#" onclick="selectBillingDurationHWOTP(1, '#_selectBillingDurationHWOTP_div', 'Monthly')">Monthly</a></li>-->
                            <!--<li><a href="#" onclick="selectBillingDurationHWOTP(3, '#_selectBillingDurationHWOTP_div', 'Quarterly')">Quarterly</a></li>-->
                            <li><a href="#" onclick="selectBillingDurationHWOTP(2, '#_selectBillingDurationHWOTP_div', 'Yearly')">Yearly</a></li>
                            
                        </ul>
                    </div>
                    Alert Before
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_selectAlertBeforeHWOTP_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="selectAlertBeforeHWOTP(1, '#_selectAlertBeforeHWOTP_div', '1 Week')">1 Week</a></li>
                            <li><a href="#" onclick="selectAlertBeforeHWOTP(2, '#_selectAlertBeforeHWOTP_div', '2 Weeks')">2 weeks</a></li>
                            <li><a href="#" onclick="selectAlertBeforeHWOTP(3, '#_selectAlertBeforeHWOTP_div', '3 Weeks')">3 weeks</a></li>
                        </ul>
                    </div>
                    cost
                    <input type="text" id="_costHWOTP" name="_costHWOTP" placeholder="cost" class="span1">



                </div>
            </div>

            <div class="control-group">
                <label class="control-label"  for="username"> Software PKI Token </label>
                <div class="controls">
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_allowBillingManagerSWPKIOTP_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="allowBillingManagerSWPKIOTP(1, '#_allowBillingManagerSWPKIOTP_div', 'Enable')">Enable</a></li>
                            <li><a href="#" onclick="allowBillingManagerSWPKIOTP(2, '#_allowBillingManagerSWPKIOTP_div', 'Disable')">Disable</a></li>
                        </ul>
                    </div>
                    Bill Type
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_selectBillTypeSWPKIOTP_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <!--<li><a href="#" onclick="allowRepeatationOfCharacter1(1, '#_allowRepetationCharacter1_div', 'Enable')">Bill/Transaction</a></li>-->
                            <li><a href="#" onclick="selectBillTypeSWPKIOTP(2, '#_selectBillTypeSWPKIOTP_div', 'Subscription')">Subscription</a></li>
                            <li><a href="#" onclick="selectBillTypeSWPKIOTP(3, '#_selectBillTypeSWPKIOTP_div', 'One Time')">One Time</a></li>
                        </ul>
                    </div>
                    Duration
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_selectBillingDurationSWPKIOTP_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <!--<li><a href="#" onclick="selectBillingDurationSWPKIOTP(1, '#_selectBillingDurationSWPKIOTP_div', 'Monthly')">Monthly</a></li>-->
                            <li><a href="#" onclick="selectBillingDurationSWPKIOTP(2, '#_selectBillingDurationSWPKIOTP_div', 'Yearly')">Yearly</a></li>
                            <!--<li><a href="#" onclick="selectBillingDurationSWPKIOTP(3, '#_selectBillingDurationSWPKIOTP_div', 'Quarterly')">Quarterly</a></li>-->
                        </ul>
                    </div>
                    Alert Before
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_selectAlertBeforeSWPKIOTP_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="selectAlertBeforeSWPKIOTP(1, '#_selectAlertBeforeSWPKIOTP_div', '1 Week')">1 Week</a></li>
                            <li><a href="#" onclick="selectAlertBeforeSWPKIOTP(2, '#_selectAlertBeforeSWPKIOTP_div', '2 Weeks')">2 weeks</a></li>
                            <li><a href="#" onclick="selectAlertBeforeSWPKIOTP(3, '#_selectAlertBeforeSWPKIOTP_div', '3 Weeks')">3 weeks</a></li>
                        </ul>
                    </div>
                    cost
                    <input type="text" id="_costSWPKIOTP" name="_costSWPKIOTP" placeholder="cost" class="span1">



                </div>
            </div>

            <div class="control-group">
                <label class="control-label"  for="username"> Hadware PKI Token  </label>
                <div class="controls">
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_allowBillingManagerHWPKIOTP_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="allowBillingManagerHWPKIOTP(1, '#_allowBillingManagerHWPKIOTP_div', 'Enable')">Enable</a></li>
                            <li><a href="#" onclick="allowBillingManagerHWPKIOTP(2, '#_allowBillingManagerHWPKIOTP_div', 'Disable')">Disable</a></li>
                        </ul>
                    </div>
                    Bill Type
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_selectBillTypeHWPKIOTP_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <!--<li><a href="#" onclick="allowRepeatationOfCharacter1(1, '#_allowRepetationCharacter1_div', 'Enable')">Bill/Transaction</a></li>-->
                            <!--<li><a href="#" onclick="selectBillTypeHWPKIOTP(2, '#_selectBillTypeHWPKIOTP_div', 'Subscription')">Subscription</a></li>-->
                            <li><a href="#" onclick="selectBillTypeHWPKIOTP(3, '#_selectBillTypeHWPKIOTP_div', 'One Time')">One Time</a></li>
                        </ul>
                    </div>
                    Duration
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_selectBillingDurationHWPKIOTP_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <!--<li><a href="#" onclick="selectBillingDurationHWPKIOTP(1, '#_selectBillingDurationHWPKIOTP_div', 'Monthly')">Monthly</a></li>-->
                            <li><a href="#" onclick="selectBillingDurationHWPKIOTP(2, '#_selectBillingDurationHWPKIOTP_div', 'Yearly')">Yearly</a></li>
                            <!--<li><a href="#" onclick="selectBillingDurationHWPKIOTP(3, '#_selectBillingDurationHWPKIOTP_div', 'Quarterly')">Quarterly</a></li>-->
                        </ul>
                    </div>
                    Alert Before
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_selectAlertBeforeHWPKIOTP_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="selectAlertBeforeHWPKIOTP(1, '#_selectAlertBeforeHWPKIOTP_div', '1 Week')">1 Week</a></li>
                            <li><a href="#" onclick="selectAlertBeforeHWPKIOTP(2, '#_selectAlertBeforeHWPKIOTP_div', '2 Weeks')">2 weeks</a></li>
                            <li><a href="#" onclick="selectAlertBeforeHWPKIOTP(3, '#_selectAlertBeforeHWPKIOTP_div', '3 Weeks')">3 weeks</a></li>
                        </ul>
                    </div>
                    cost
                    <input type="text" id="_costHWPKIOTP" name="_costHWPKIOTP" placeholder="cost" class="span1">



                </div>
            </div>

            <div class="control-group">
                <label class="control-label"  for="username"> Certificate  </label>
                <div class="controls">
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_allowBillingManagerCertificate_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="allowBillingManagerCertificate(1, '#_allowBillingManagerCertificate_div', 'Enable')">Enable</a></li>
                            <li><a href="#" onclick="allowBillingManagerCertificate(2, '#_allowBillingManagerCertificate_div', 'Disable')">Disable</a></li>
                        </ul>
                    </div>
                    Bill Type
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_selectBillTypeCertificate_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <!--<li><a href="#" onclick="allowRepeatationOfCharacter1(1, '#_allowRepetationCharacter1_div', 'Enable')">Bill/Transaction</a></li>-->
                            <!--<li><a href="#" onclick="selectBillTypeCertificate(2, '#_selectBillTypeCertificate_div', 'Subscription')">Subscription</a></li>-->
                            <li><a href="#" onclick="selectBillTypeCertificate(3, '#_selectBillTypeCertificate_div', 'One Time')">One Time</a></li>
                        </ul>
                    </div>
                    Duration
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_selectBillingDurationCertificate_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <!--<li><a href="#" onclick="selectBillingDurationCertificate(1, '#_selectBillingDurationCertificate_div', 'Monthly')">Monthly</a></li>-->
                            <li><a href="#" onclick="selectBillingDurationCertificate(2, '#_selectBillingDurationCertificate_div', 'Yearly')">Yearly</a></li>
                            <!--<li><a href="#" onclick="selectBillingDurationCertificate(3, '#_selectBillingDurationCertificate_div', 'Quarterly')">Quarterly</a></li>-->
                        </ul>
                    </div>
                    Alert Before
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_selectAlertBeforeCertificate_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="selectAlertBeforeCertificate(1, '#_selectAlertBeforeCertificate_div', '1 Week')">1 Week</a></li>
                            <li><a href="#" onclick="selectAlertBeforeCertificate(2, '#_selectAlertBeforeCertificate_div', '2 Weeks')">2 weeks</a></li>
                            <li><a href="#" onclick="selectAlertBeforeCertificate(3, '#_selectAlertBeforeCertificate_div', '3 Weeks')">3 weeks</a></li>
                        </ul>
                    </div>
                    cost
                    <input type="text" id="_costCertificate" name="_costCertificate" placeholder="cost" class="span1">



                </div>
            </div>

            <div class="control-group">
                <label class="control-label"  for="username"> Out Of Band Token  </label>
                <div class="controls">
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_allowBillingManagerOOBOTP_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="allowBillingManagerOOBOTP(1, '#_allowBillingManagerOOBOTP_div', 'Enable')">Enable</a></li>
                            <li><a href="#" onclick="allowBillingManagerOOBOTP(2, '#_allowBillingManagerOOBOTP_div', 'Disable')">Disable</a></li>
                        </ul>
                    </div>
                    Bill Type
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_selectBillTypeOOBOTP_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="selectBillTypeOOBOTP(1, '#_selectBillTypeOOBOTP_div', 'Bill/Transaction')">Bill/Transaction</a></li>
                            <!--<li><a href="#" onclick="selectBillTypeOOBOTP(2, '#_selectBillTypeOOBOTP_div', 'Subscription')">Subscription</a></li>-->
                            <!--<li><a href="#" onclick="selectBillTypeOOBOTP(3, '#_selectBillTypeOOBOTP_div', 'One Time')">One Time</a></li>-->
                        </ul>
                    </div>
                    Duration
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_selectBillingDurationOOBOTP_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="selectBillingDurationOOBOTP(1, '#_selectBillingDurationOOBOTP_div', 'Monthly')">Monthly</a></li>
                            <!--<li><a href="#" onclick="selectBillingDurationOOBOTP(3, '#_selectBillingDurationOOBOTP_div', 'Quarterly')">Quarterly</a></li>-->
                            <!--<li><a href="#" onclick="selectBillingDurationOOBOTP(2, '#_selectBillingDurationOOBOTP_div', 'Yearly')">Yearly</a></li>-->

                        </ul>
                    </div>
                    Alert Before
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_selectAlertBeforeOOBOTP_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="selectAlertBeforeOOBOTP(1, '#_selectAlertBeforeOOBOTP_div', '1 Week')">1 Week</a></li>
                            <li><a href="#" onclick="selectAlertBeforeOOBOTP(2, '#_selectAlertBeforeOOBOTP_div', '2 weeks')">2 weeks</a></li>
                            <!--<li><a href="#" onclick="selectAlertBeforeOOBOTP(3, '#_selectAlertBeforeOOBOTP_div', '3 weeks')">3 weeks</a></li>-->
                        </ul>
                    </div>
                    cost
                    <input type="text" id="_costOOBOTP" name="_costOOBOTP" placeholder="cost" class="span1">

                    Cost per Transaction
                    <input type="text" id="_costperTxOOBOTP" name="_costperTxOOBOTP" placeholder="cost" class="span1">
                    <!--                    Threshold Cost
                                        <input type="text" id="_thresholdCostOOBOTP" name="_thresholdCostOOBOTP" placeholder="Threshold cost" class="span1">-->
                    Transactions per Day(0 - for unlimited)
                    <input type="text" id="_txPerDayOOBOTP" name="_txPerDayOOBOTP" placeholder="cost" class="span1">
                    <!--                    <div class="btn-group">
                                            <button class="btn btn-small"><div id="_txPerDayOOBOTP_div"></div></button>
                                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#" onclick="selectTXperDayOOBOTP(3, '#_txPerDayOOBOTP_div', '3 Tx/Day')">3 Tx/Day</a></li>
                                                <li><a href="#" onclick="selectTXperDayOOBOTP(5, '#_txPerDayOOBOTP_div', '5 Tx/Day')">5 Tx/Day</a></li>
                                                <li><a href="#" onclick="selectTXperDayOOBOTP(7, '#_txPerDayOOBOTP_div', '7 Tx/Day')">7 Tx/Day</a></li>
                                            </ul>
                                        </div>-->



                </div>
            </div>

            <!--<label  for="username">Alert Settings:</label>-->
            <% if ( 1 == 2 ) { %>
            <div class="control-group">
                <label class="control-label"  for="username">Alert </label>
                <div class="controls">

                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_allowRepetationCharacterB_div">Enable</div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="allowRepeatationOfCharacterB(1, '#_allowRepetationCharacterB_div', 'Enable')">Enable</a></li>
                            <li><a href="#" onclick="allowRepeatationOfCharacterB(2, '#_allowRepetationCharacterB_div', 'Disable')">Disable</a></li>
                        </ul>
                    </div>
                    Via
                    
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_gatewayTypeB_div">EMAIL</div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <!--<li><a href="#" onclick="AlertViaB(1, '#_gatewayTypeB_div', 'SMS')">SMS</a></li>-->
<!--                            <li><a href="#" onclick="AlertViaB(2, '#_gatewayTypeB_div', 'USSD')">USSD</a></li>
                            <li><a href="#" onclick="AlertViaB(3, '#_gatewayTypeB_div', 'VOICE')">VOICE</a></li>-->
                            <!--<li><a href="#" onclick="AlertViaB(4, '#_gatewayTypeB_div', 'EMAIL')">EMAIL</a></li>-->
<!--                            <li><a href="#" onclick="AlertViaB(5, '#_gatewayTypeB_div', 'FAX')">FAX</a></li>
                            <li><a href="#" onclick="AlertViaB(18, '#_gatewayTypeB_div', 'ANDROIDPUSH')">ANDROIDPUSH</a></li>
                            <li><a href="#" onclick="AlertViaB(19, '#_gatewayTypeB_div', 'IPHONEPUSH')">IPHONEPUSH</a></li>-->


                        </ul>
                    </div>
                    With Attempts:
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_alertAttemptB_div">3 Attempts</div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="AlertAttemptsB(1, '#_alertAttemptB_div', '1 Attempt')">1 Attempt</a></li>
                            <li><a href="#" onclick="AlertAttemptsB(2, '#_alertAttemptB_div', '2 Attempts')">2 Attempts</a></li>
                            <li><a href="#" onclick="AlertAttemptsB(3, '#_alertAttemptB_div', '3 Attempts')">3 Attempts</a></li>
                        </ul>
                    </div>

                    Using Template : <b>email.bill.reminder.alert</b>
                </div>
            </div>
            <% } %>
            
            <% //if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>

            <div class="control-group">
                <div class="controls">
                    <div id="save-billing-settings-result"></div>
                    <button class="btn btn-primary" onclick="editBillingManagerSettings()" type="button">Save Setting Now >> </button>
                </div>
            </div>
            <%//}%>
        </form>
        <!--</div>-->
    </div>

    <script language="javascript" type="text/javascript">
        LoadBillingManagerSettings();
    </script>
</div>


<%@include file="footer.jsp" %>