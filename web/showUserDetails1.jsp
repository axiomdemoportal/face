<%@include file="header.jsp" %>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SecurePhraseManagment"%>
<%@page import="com.mollatech.axiom.nucleus.db.Securephrase"%>
<%@page import="com.mollatech.axiom.nucleus.settings.ImageSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.Questionsandanswers"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.ChallengeResponsemanagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Certificates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TrustDeviceManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Trusteddevice"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TwowayauthManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Twowayauth"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.axiom.nucleus.db.Statelist"%>
<%@page import="com.mollatech.axiom.nucleus.db.Usergroups"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserGroupsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.LocationManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Countrylist"%>
<%@page import="com.mollatech.axiom.nucleus.db.Geofence"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.MobileTrustManagment"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.GeoLocationManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Otptokens"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.connector.user.TokenStatusDetails"%>
<%@page import="com.mollatech.axiom.nucleus.settings.TokenSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>

<!--<script src="./assets/js/jquery.js"></script>-->
<script src="./assets/js/usermanagement.js"></script>
<!--<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">-->
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<script src="./assets/js/resourcemanagment.js"></script>
<script src="./assets/js/otptokens.js"></script>
<script src="./assets/js/pkitokens.js"></script>
<script src="./assets/js/challengeresponse.js"></script>
<script src="./assets/js/trustedDevice.js"></script>
<script src="./assets/js/geotracking.js"></script>
<script src="./assets/js/securephrase.js"></script>
<script src="./assets/js/caconnector.js"></script>
<script src="./assets/js/ajaxfileupload.js"></script>
<script src="./assets/js/bootstrap-fileupload.js"></script> 
<script src="assets/js/simplifiedUserSearch.js" type="text/javascript"></script>
<!--<link href="./assets/css/bootstrap.css" rel="stylesheet">-->
<!--<script src="assets/js/simplyUserSarch.js" type="text/javascript"></script>-->
<link href="./assets/css/bootstrap-fileupload.css" rel="stylesheet">
<script src="./assets/js/globalsetting.js"></script>
<!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">-->
<!--<script src="./assets/js/bootstrap-modal.js"></script>-->
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<style>
    .innerr {
        display: inline-block;
        vertical-align: middle;
        background: yellow;
        padding: 3px 5px;
    }
    .block4 {
        /*        //color:#fff;background-color:#337ab7;*/
        box-shadow: 5px 5px 5px #888888;
        line-height: 90px;
        width:250px;
        margin-left:10px;
        margin-top: 20px;
    }
    .inner4 {
        line-height: normal; /* Reset line-height for the child. */
        background: none;
    }
    #square {
        width: 900px;
        height: 900px;
        background: red;
    }
    div.relative1 {
        margin: 1%;
        position: relative;
        height: 100%;
        color : black;
        border: 2px solid lightblue;
        box-shadow: 5px 5px 5px #888888;
        line-height: 100%;     
    } 

    div.absolute {
        position: absolute;
        top: 5%;
        bottom: 10%;
        left: 27%;
        width: 73%;
        height: 70%;
        font-size:100%;
    }
    .footerLink{
        margin-top: 20%;
        margin-left: 0%;
        text-align: center;
        line-height: 160%;              
        background-color:darkslategrey;                   
    }
    .footerMargin{
        margin-bottom: 0%; 
        color: white;
    }
    .slick-next {
        border: 1px solid lightblue;
        width: 20%;
        height: 40%;
        text-align: center;
        margin: 5%;
        margin-top: 7%;
        background-color:lightblue;

    }
    .slick-next:after {
        display: inline-block;
        vertical-align: middle;
        content: "";
        height: 135%;
    }
</style>

<%    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    //AuthUser userObj = (AuthUser) request.getSession().getAttribute("userDetailsObject");
    String _searchUser = request.getParameter("search");

    SettingsManagement setObj = new SettingsManagement();
    Object settingsObj = setObj.getSetting(sessionId, channel.getChannelid(), SettingsManagement.Token, SettingsManagement.PREFERENCE_ONE);
    UserManagement usermngObj = new UserManagement();
    AuthUser userObj = usermngObj.getUser(channel.getChannelid(), _searchUser);
    TokenSettings tokenObj = null;
    boolean bAddSetting = false;

    if (settingsObj != null) {
        tokenObj = (TokenSettings) settingsObj;
    }
    int subtype = -1;
    int subtypeSwtoken = -1;

    String strerr = "No Record Found";
%>
<div class="container-fluid">
    <table>
        <tr>
            <td><b style="font-size: 150%"> User Id</b>
                <span style="font-size: 150%">:</span>
                <span style="font-size: 150%"><%=userObj.getUserId()%></span>
                &nbsp;
                <b style="font-size: 150%"> Name</b>
                <span style="font-size: 150%">:</span>
                <span style="font-size: 150%"><%=userObj.getUserName()%></span>
                &nbsp;
                <b style="font-size: 150%">Phone</b>
                <span style="font-size: 150%">:</span>
                <span style="font-size: 150%"><%=userObj.getPhoneNo()%></span>
                &nbsp;
                <b style="font-size: 150%">Email</b>
                <span style="font-size: 150%">:</span>
                <span style="font-size: 150%"><%=userObj.getEmail()%></span></td>
        </tr>
    </table>
    <br>
    <ul class="nav nav-tabs" >
        <li class="active"><a href="#userotp" data-toggle="tab">One Time Password</a></li>
        <li><a href="#usercert" data-toggle="tab">Certificate</a></li>
        <!--        <li><a href="#usergeo" data-toggle="tab">Geo</a></li>
                <li><a href="#userdevice" data-toggle="tab">Device</a></li>
                <li><a href="#userqa" data-toggle="tab">Q&A</a></li>
                <li><a href="#usertwowayauth" data-toggle="tab">Two way Auth</a></li>
                        <li><a href="#userepin" data-toggle="tab">EPIN</a></li>
                <li><a href="#userimage" data-toggle="tab">Image</a></li>-->
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="userotp">
            <%
                if (userObj != null) {
                    String strLabelOOB = "label";    //unassigned
                    String strLabelSW = "label";    //unassigned
                    String strLabelHW = "label";    //unassigned
                    boolean flag = false;
                    java.util.Date dLastUpdated = new java.util.Date(userObj.lLastAccessOn);
                    java.util.Date dsoftUpdated = new java.util.Date(userObj.lLastAccessOn);
                    java.util.Date dsoftCreated = new java.util.Date(userObj.lLastAccessOn);
                    java.util.Date dhardUpdated = new java.util.Date(userObj.lLastAccessOn);
                    java.util.Date dhardCreated = new java.util.Date(userObj.lLastAccessOn);
                    java.util.Date dOOBUpdated = new java.util.Date(userObj.lLastAccessOn);
                    java.util.Date dOOBCreated = new java.util.Date(userObj.lLastAccessOn);
//                java.util.Date dCreatedOn = new java.util.Date(Users[i].lCreatedOn);
//                java.util.Date dLastUpdated = new java.util.Date(Users[i].lLastAccessOn);
                    SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm ");
                    String strStatus = null;
                    TokenStatusDetails tokendetails[] = null;

                    String softstatus = "Unassigned";
                    String hardstatus = "Unassigned";
                    String OOBstatus = "Unassigned";
                    String OOBtype = "---";
                    String SOBtype = "---";
                    String serialNumbersoft = "------";
                    String serialNumberhard = "------";
                    String serialNumberoob = "NA";
                    int attempt = 0;
                    int attemptS = 0;
                    int attemptH = 0;
                    int attemptO = 0;

                    OTPTokenManagement OtpObj = new OTPTokenManagement(channel.getChannelid());

                    String pukcode = null;
                    tokendetails = OtpObj.getTokenList(sessionId, channel.getChannelid(), userObj.getUserId());
                    TokenStatusDetails tokenDetailsOfOOB = null;
                    TokenStatusDetails tokenDetailsOfSoft = null;
                    TokenStatusDetails tokenDetailsOfHard = null;
                    if (tokendetails != null) {
                        for (int j = 0; j < tokendetails.length; j++) {
                            if (tokendetails[j].Catrgory == OTPTokenManagement.SOFTWARE_TOKEN) {
                                tokenDetailsOfSoft = tokendetails[j];
                                if (tokenDetailsOfSoft.serialnumber != null) {
                                    serialNumbersoft = tokenDetailsOfSoft.serialnumber;
                                }
                                dsoftUpdated = new java.util.Date(tokenDetailsOfSoft.lastaccessOn);
                                dsoftCreated = new java.util.Date(tokenDetailsOfSoft.createOn);
                            } else if (tokendetails[j].Catrgory == OTPTokenManagement.HARDWARE_TOKEN) {
                                tokenDetailsOfHard = tokendetails[j];
//                           
                                if (tokenDetailsOfHard.serialnumber != null) {
                                    serialNumberhard = tokenDetailsOfHard.serialnumber;
                                }
                                dhardUpdated = new java.util.Date(tokenDetailsOfHard.lastaccessOn);
                                dhardCreated = new java.util.Date(tokenDetailsOfHard.createOn);
                            } else if (tokendetails[j].Catrgory == OTPTokenManagement.OOB_TOKEN) {
                                tokenDetailsOfOOB = tokendetails[j];
//                            
                                if (tokenDetailsOfOOB.serialnumber != null) {
                                    serialNumberoob = tokenDetailsOfOOB.serialnumber;
                                }
                                dOOBUpdated = new java.util.Date(tokenDetailsOfOOB.lastaccessOn);
                                dOOBCreated = new java.util.Date(tokenDetailsOfOOB.createOn);
                            }

                            if (dsoftUpdated.after(dhardUpdated) && dsoftUpdated.after(dOOBUpdated)) {
                                dLastUpdated = dsoftUpdated;
                            } else if (dhardUpdated.after(dsoftUpdated) && dhardUpdated.after(dOOBUpdated)) {
                                dLastUpdated = dhardUpdated;
                            } else if (dOOBUpdated.after(dsoftUpdated) && dsoftUpdated.after(dhardUpdated)) {
                                dLastUpdated = dOOBUpdated;
                            }
                            if (tokenDetailsOfOOB != null) {
                                attemptO = tokenDetailsOfOOB.Attempts;
                            }
                            if (tokenDetailsOfSoft != null) {
                                attemptS = tokenDetailsOfSoft.Attempts;
                            }
                            if (tokenDetailsOfHard != null) {
                                attemptH = tokenDetailsOfHard.Attempts;
                            }
                            attempt = attemptO + attemptS + attemptH;
                        }
                        if (tokenDetailsOfSoft != null) {
                            if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                                strLabelSW += " label-success";   //active
                                softstatus = "Active";
                            } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
                                strLabelSW += " label-warning";   //locked
                                softstatus = "Locked";
                            } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                                strLabelSW += " label-info";   //assigned                
                                softstatus = "Assigned";
                            } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                                softstatus = "Unassigned";

                            } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
                                strLabelSW += " label-important";   //suspended
                                softstatus = "suspended";
                            }
                            if (tokenDetailsOfSoft.SubCategory == OTPTokenManagement.SW_WEB_TOKEN) {
                                SOBtype = "WEB(" + serialNumbersoft + ")";
                                subtypeSwtoken = 1;
                            }
                            if (tokenDetailsOfSoft.SubCategory == OTPTokenManagement.SW_MOBILE_TOKEN) {
                                SOBtype = "MOBILE(" + serialNumbersoft + ")";
                                subtypeSwtoken = 2;
                            }
                            if (tokenDetailsOfSoft.SubCategory == OTPTokenManagement.SW_PC_TOKEN) {
                                subtypeSwtoken = 3;
                                SOBtype = "PC(" + serialNumbersoft + ")";
                            }
                        }
                        if (tokenDetailsOfHard != null) {
                            if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                                hardstatus = "Active";
                                strLabelHW += " label-success";   //active
                            }
                            if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
                                hardstatus = "Locked";
                                strLabelHW += " label-warning";   //locked
                            }
                            if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                                hardstatus = "Assigned";
                                strLabelHW += " label-info";   //assigned                
                            }
                            if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                                hardstatus = "Unassigned";
                                flag = true;
                            }
                            if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
                                hardstatus = "suspended";
                                strLabelHW += " label-important";   //suspended
                            }
                            if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_LOST) {
                                hardstatus = "Lost";
                                strLabelHW += " label-important";   //suspended
                            }
                            Otptokens otpObj = OtpObj.getOtpObjBySerialNo(sessionId, tokenDetailsOfHard.serialnumber);
                            if (otpObj != null && tokenObj != null) {
                                pukcode = OtpObj.generatePUK(otpObj.getSecret(), tokenObj.getPukCodeLengthHW(), tokenObj.getPukCodeValidityHW());
                            }
                        }
                        if (tokenDetailsOfOOB != null) {
                            if (tokenDetailsOfOOB.Status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                                strLabelOOB += " label-success";   //active
                                OOBstatus = "Active";
                            }
                            if (tokenDetailsOfOOB.Status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
                                strLabelOOB += " label-warning";   //locked
                                OOBstatus = "Locked";
                            }
                            if (tokenDetailsOfOOB.Status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                                strLabelOOB += " label-info";   //assigned                
                                OOBstatus = "Assigned";
                            }
                            if (tokenDetailsOfOOB.Status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                                OOBstatus = "Unassigned";
                            }
                            if (tokenDetailsOfOOB.Status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
                                strLabelOOB += " label-important";   //suspended
                                OOBstatus = "suspended";
                            }
                            if (tokenDetailsOfOOB.SubCategory == OTPTokenManagement.OOB__SMS_TOKEN) {
                                OOBtype = "SMS";
                                subtype = 1;
                            }
                            if (tokenDetailsOfOOB.SubCategory == OTPTokenManagement.OOB__VOICE_TOKEN) {
                                OOBtype = "VOICE";
                                subtype = 2;
                            }
                            if (tokenDetailsOfOOB.SubCategory == OTPTokenManagement.OOB__USSD_TOKEN) {
                                subtype = 3;
                                OOBtype = "USSD";
                            }
                            if (tokenDetailsOfOOB.SubCategory == OTPTokenManagement.OOB__EMAIL_TOKEN) {
                                OOBtype = "EMAIL";
                                subtype = 4;
                            }
                        }
                    }
                    String userStatus = "user-status-value-";
                    String otpType = "otp-type-value-";
                    String otpStatus = "otp-status-value-";
                    String sotpStatus = "sotp-status-value-";
                    String hotpStatus = "hotp-status-value-";
                    String sotpType = "sotp-type-value-";
                    String hotpType = "hotp-type-value-";
            %>                
            <div class="tab-content">
                <ul class="nav nav-pills">
                    <li class="active"><a href="#oob-pills" data-toggle="tab">Out Of Band Token</a></li>                   
                    <li><a href="#sofware-pills" data-toggle="tab">Software Token</a></li>                    
                    <li><a href="#hardware-pills" data-toggle="tab">Hardware Token</a></li>
                </ul>
                <div class="tab-pane fade in active" id="oob-pills">
                    <div class="relative1">
                        <div class="container-fluid">
                            <form class="form-horizontal" id="primaryform" name="primaryform">
                                <div id="legend">
                                    <legend>  Out Of Band Token </legend> 
                                </div>                        
                                <div class="control-group">                                    
                                    <label class="control-label"  for="username">Serial No </label>
                                    <div class="controls">                                   
                                        <%=serialNumberoob%>#  Status As #<span id="<%=otpStatus%>" class="<%=strLabelOOB%>"><%=OOBstatus%></span># with attempt #<%=attemptO%>
                                    </div>                                    
                                </div>                        
                                <div class="control-group">
                                    <label class="control-label"  for="username">Created On</label>
                                    <div class="controls">
                                        <% if (dOOBCreated != null && dLastUpdated != null) {%>
                                        <p><%=sdf.format(dOOBCreated)%> Last Access On <%=sdf.format(dLastUpdated)%></p>
                                        <% } else {%>
                                        <p><%="Not-Available"%> Last Access On <%="Not-Available"%></p>
                                        <% }%>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label"  for="username">Assign Token</label>                                        
                                    <div class="controls">                                            
                                        <a href="#" onclick="assigntokenOOBandSW('<%=userObj.getUserId()%>', 3, 1, '<%=otpType%>', '<%=otpStatus%>')" class="btn btn-success btn-mini">SMS Token</a></li>
                                        <a href="#" onclick="assigntokenOOBandSW('<%=userObj.getUserId()%>', 3, 2, '<%=otpType%>', '<%=otpStatus%>')" class="btn btn-info btn-mini">Voice Token</a></li>
                                        <a href="#" onclick="assigntokenOOBandSW('<%=userObj.getUserId()%>', 3, 3, '<%=otpType%>', '<%=otpStatus%>')" class="btn btn-warning btn-mini">USSD Token</a></li>
                                        <a href="#" onclick="assigntokenOOBandSW('<%=userObj.getUserId()%>', 3, 4, '<%=otpType%>', '<%=otpStatus%>')" class="btn btn-danger btn-mini">Email Token</a></li>
                                    </div>
                                </div>                                                                                        
                                <div class="control-group">
                                    <label class="control-label"  for="username">Change Token Type</label>                                        
                                    <div class="controls">
                                        <a href="#" onclick="changetokenOOBandSW('<%=userObj.getUserId()%>', 3, 1, '<%=otpType%>')" class="btn btn-success btn-mini">SMS Token</a>
                                        <a href="#" onclick="changetokenOOBandSW('<%=userObj.getUserId()%>', 3, 2, '<%=otpType%>')" class="btn btn-info btn-mini">Voice Token</a>
                                        <a href="#" onclick="changetokenOOBandSW('<%=userObj.getUserId()%>', 3, 3, '<%=otpType%>')" class="btn btn-warning btn-mini">USSD Token</a>
                                        <a href="#" onclick="changetokenOOBandSW('<%=userObj.getUserId()%>', 3, 4, '<%=otpType%>')" class="btn btn-danger btn-mini">Email Token</a>
                                    </div>
                                </div>                                                                                        
                                <div class="control-group">
                                    <label class="control-label"  for="username">Change Status</label>                                        
                                    <div class="controls">
                                        <a href="#" onclick="changeotpstatus(1, '<%=userObj.getUserId()%>', 3, '<%=subtype%>', '<%=otpStatus%>')" class="btn btn-success btn-mini">Mark as Active</a>                    
                                        <a href="#" onclick="loadotpstatusSimplified(-2, '<%=userObj.getUserId()%>', 3, '<%=subtype%>', '<%=otpStatus%>')" class="btn btn-warning btn-mini">Mark as Suspended</a>
                                        <a href="#" onclick="changeotpstatus(-10, '<%=userObj.getUserId()%>', 3, '<%=subtype%>', '<%=otpStatus%>')" class="btn btn-danger btn-mini">Un-Assign Token</a>
                                    </div>
                                </div>                                        
                                <div class="control-group">
                                    <label class="control-label"  for="username">Manage</label>                                        
                                    <div class="controls">
                                        <a href="#" onclick="LoadTestOTPUISimplified('<%=userObj.getUserId()%>', 3)" class="btn btn-success btn-mini">Verify One Time Password</a>    
                                    </div>
                                </div>
                                <!--                    <div class="control-group">
                                                        <label class="control-label"  for="username">Unassign Token</label>                                        
                                                        <div class="controls">                                            
                                                            <a href="#" onclick="changeotpstatus(-10, '<%=userObj.getUserId()%>', 3, '<%=subtype%>', '<%=otpStatus%>')" class="btn btn-success btn-mini">Un-Assign Token</a>
                                                        </div>
                                                    </div>-->
                            </form>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="sofware-pills">
                    <div class="relative1">
                        <div class="container-fluid">                                
                            <form class="form-horizontal" id="primaryform" name="primaryform">
                                <div id="legend">
                                    <legend>  Software Token </legend> 
                                </div>
                                <div class="control-group">                                    
                                    <label class="control-label"  for="username">Serial No </label>
                                    <div class="controls">                                   
                                        <%=SOBtype%>#  Status As #<span id="<%=sotpStatus%>" class="<%=strLabelSW%>"><%=softstatus%></span># with attempt #<%=attemptS%>                                            
                                    </div>                                    
                                </div>
                                <div class="control-group">
                                    <label class="control-label"  for="username">Created On</label>
                                    <div class="controls">
                                        <% if (dsoftCreated != null && dsoftUpdated != null) {%>
                                        <p><%=sdf.format(dsoftCreated)%> Last Access On <%=sdf.format(dsoftUpdated)%></p>
                                        <% } else {%>
                                        <p><%="Not-Available"%> Last Access On <%="Not-Available"%></p>
                                        <% }%>
                                    </div>
                                </div>                                                                    
                                <div class="control-group">
                                    <label class="control-label"  for="username">Assign Token</label>
                                    <div class="controls">                                          
                                        <a href="#" onclick="assigntokenOOBandSW('<%=userObj.getUserId()%>', 1, 1, '<%=sotpType%>', '<%=sotpStatus%>')" class="btn btn-success btn-mini">Web Token</a>
                                        <a href="#" onclick="assigntokenOOBandSW('<%=userObj.getUserId()%>', 1, 2, '<%=sotpType%>', '<%=sotpStatus%>')" class="btn btn-info btn-mini">Mobile Token</a>
        <!--                                <a href="#" onclick="assigntokenOOBandSW('<%=userObj.getUserId()%>', 1, 3, '<%=sotpType%>', '<%=sotpStatus%>')" class="btn btn-danger btn-mini">PC Token</a>                                           -->

                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label"  for="username">Change Status</label>
                                    <div class="controls">
                                        <a href="#" onclick="changeotpstatus(1, '<%=userObj.getUserId()%>', 1, '<%=subtypeSwtoken%>', '<%=sotpStatus%>')" class="btn btn-success btn-mini">Mark as Active</a>
                                        <a href="#" onclick="loadotpstatusSimplified(-2, '<%=userObj.getUserId()%>', 1, '<%=subtypeSwtoken%>', '<%=sotpStatus%>')" class="btn btn-info btn-mini">Mark as Suspended</a>                                          
                                        <a href="#" onclick="changeotpstatus(-10, '<%=userObj.getUserId()%>', 1, '<%=subtypeSwtoken%>', '<%=sotpStatus%>')"class="btn btn-danger btn-mini">Un-Assign Token</a>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label"  for="username">Manage</label>
                                    <div class="controls">
                                        <a href="#" onclick="sendregistration('<%=userObj.getUserId()%>', 1, '<%=subtypeSwtoken%>')" class="btn btn-success btn-mini">(Re)send Registration Code</a>                                                               
                                        <a href="#" onclick="LoadTestOTPUISimplified('<%=userObj.getUserId()%>', 1)" class="btn btn-info btn-mini">Verify One Time Password</a>                                           
                                    </div>
                                </div>
                                <!--                    <div class="control-group">
                                                        <label class="control-label"  for="username">Unassign Token</label>
                                                        <div class="controls">                                          
                                                            <a href="#" onclick="changeotpstatus(-10, '<%=userObj.getUserId()%>', 1, '<%=subtypeSwtoken%>', '<%=sotpStatus%>')"class="btn btn-success btn-mini">Un-Assign Token</a>                                         
                                                        </div>
                                                    </div>-->
                            </form>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="hardware-pills">
                    <div class="relative1">
                        <div class="container-fluid">
                            <form class="form-horizontal" id="primaryform" name="primaryform">
                                <div id="legend">
                                    <legend>  Hardware Token </legend> 
                                </div>
                                <div class="control-group">
                                    <label class="control-label"  for="username">Serial No </label>
                                    <div class="controls">
                                        <%=serialNumberhard%>#  Status As #
                                        <%
                                            if (hardstatus.equalsIgnoreCase("Unassigned")) {
                                        %>
                                        <span id="<%=hotpStatus%>" class="<%=strLabelHW%>"><%=hardstatus%></span> <a href="#" onclick="loadregisterHW('<%=userObj.getUserId()%>', 2, 1)" class="btn btn-success btn-mini">Assign Token</a> # with attempt #<%=attemptH%>          
                                        <%} else {%>
                                        <span id="<%=hotpStatus%>" class="<%=strLabelHW%>"><%=hardstatus%></span> # with attempt #<%=attemptH%>
                                        <%}%>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label"  for="username">Created On</label>
                                    <div class="controls">
                                        <% if (dhardCreated != null && dhardUpdated != null) {%>
                                        <p><%=sdf.format(dhardCreated)%> Last Access On <%=sdf.format(dhardUpdated)%></p>
                                        <% } else {%>
                                        <p><%="Not-Available"%> Last Access On <%="Not-Available"%></p>
                                        <% }%>
                                    </div>
                                </div>
                                <!--                                        <div class="control-group">
                                                                            <label class="control-label"  for="username">Assign Token</label>
                                                                            <div class="controls">
                                                                                <a href="#" onclick="loadregisterHW('<%=userObj.getUserId()%>', 2, 1)" class="btn btn-success btn-mini">Assign Token</a>                                            
                                                                            </div>
                                                                        </div>-->
                                <div class="control-group">
                                    <label class="control-label"  for="username">Change Status</label>
                                    <div class="controls">
                                        <a href="#" onclick="changeotpstatus(1, '<%=userObj.getUserId()%>', 2, 1, '<%=hotpStatus%>')" class="btn btn-success btn-mini">Mark as Active</a>                                            
                                        <a href="#" onclick="loadotpstatusSimplified(-2, '<%=userObj.getUserId()%>', 2, 1, '<%=hotpStatus%>')" class="btn btn-danger btn-mini">Mark as Suspended</a>
                                        <a href="#" onclick="losttoken(-5, '<%=userObj.getUserId()%>', 2, 1, '<%=hotpStatus%>')" class="btn btn-info btn-mini">Mark as Lost</a>                                            
                                        <a href="#" onclick="changeotpstatus(-10, '<%=userObj.getUserId()%>', 2, 1, '<%=hotpStatus%>')" class="btn btn-warning btn-mini">Un-Assign Token</a>                                           
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label"  for="username">Manage</label>
                                    <div class="controls">
                                        <a href="#"  onclick="LoadTestOTPUISimplified('<%=userObj.getUserId()%>', 2)" class="btn btn-success btn-mini">Verify One Time Password</a>                                            
                                        <a href="#ResyncHardware" onclick="loadresyncHWSimplified('<%=userObj.getUserId()%>', '<%=serialNumberhard%>', 2)" class="btn btn-info btn-mini">Resync Token</a>
                                        <a href="#" data-toggle="modal" onclick="LoadTestOTPForReplaceSimplified('<%=userObj.getUserId()%>', 2, 1)" class="btn btn-danger btn-mini">Replace Token</a>
                                        <a href="#" onclick="loadPUKHWSimplified('<%=userObj.getUserId()%>', '<%=pukcode%>', '<%=tokenObj.getPukCodeValidityHW()%>')" class="btn btn-warning btn-mini">PUK Code</a>
                                    </div>
                                </div>
                            </form>        
                        </div>
                    </div>
                </div>

                <div id="legend">
                    <legend>  Audit Management </legend> 
                </div>
                <div class="control-group">
                    <form class="form-horizontal" id="primaryform" name="primaryform">
                        <label class="control-label"  for="username">Audit </label>
                        <input type="hidden" id="_otpauditUserType" name="_otpauditUserType"/>
                        <input type="hidden" id="_otpauditUserID1" name="_otpauditUserID1" value="<%=userObj.userId%>"/>
                        <input type="hidden" id="_otpauditUserName1" name="_otpauditUserName1" value="<%=userObj.userName%>"/>
                        <div class="controls">                            
                            <div class="input-prepend">
                                <span class="add-on">From:</span>   
                                <div id="datetimepicker111" class="input-append date">
                                    <input id="startdateuser" name="startdateuser" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="input-prepend">
                                <span class="add-on">till:</span>   
                                <div id="datetimepicker222" class="input-append date">
                                    <input id="enddateser" name="enddateser" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <script type="text/javascript">
                                $(function () {
                                    $('#datetimepicker111').datepicker({
                                        format: 'dd/MM/yyyy',
                                        language: 'pt-BR'
                                    });
                                });
                                $(function () {
                                    $('#datetimepicker222').datepicker({
                                        format: 'dd/MM/yyyy',
                                        language: 'pt-BR'

                                    });
                                });
                            </script>
                            Report Format   
                            <div class="btn-group">
                                <button class="btn btn-mini" id="_userreportType-div">Select Type</button>
                                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="ChangeReportUserSimplified(0)">PDF</a></li>
                                    <li><a href="#" onclick="ChangeReportUserSimplified(1)">CSV</a></li>
                                    <li><a href="#" onclick="ChangeReportUserSimplified(2)">TEXT</a></li>
                                </ul>
                            </div>
                            <a href="#" class="btn btn-success btn-mini" onclick="downloadUserTokenAudit()">Download Report</a> 
                    </form>
                </div>                
            </div><br><br><br><br><br><br>
        </div><br>        
    </div>
    <%}%>
    <div class="tab-pane" id="usercert">
        <%
            if (userObj != null) {
                Certificates certifts = null;
                String strLabelSW = "label";    //unassigned
                String strLabelHW = "label";    //unassigned
                java.util.Date dCreatedOn = null;
                java.util.Date dLastUpdated = null;
                SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm ");
                String strStatus = null;
                TokenStatusDetails pkiDetails[] = null;
                PKITokenManagement pkiObj = new PKITokenManagement();
                String softPkistatus = "Unassigned";
                String hardPkistatus = "Unassigned";
                String serialNumberhard = "------";
                CertificateManagement CObj = new CertificateManagement();
                certifts = CObj.getCertificate(sessionId, channel.getChannelid(), userObj.getUserId());
                pkiDetails = pkiObj.getTokenList(sessionId, channel.getChannelid(), userObj.getUserId());
                if (pkiDetails != null) {
                    TokenStatusDetails tokenDetailsOfSoft = null;
                    TokenStatusDetails tokenDetailsOfHard = null;
                    for (int j = 0; j < pkiDetails.length; j++) {
                        if (pkiDetails[j].Catrgory == OTPTokenManagement.SOFTWARE_TOKEN) {
                            tokenDetailsOfSoft = pkiDetails[j];
                        } else if (pkiDetails[j].Catrgory == OTPTokenManagement.HARDWARE_TOKEN) {
                            tokenDetailsOfHard = pkiDetails[j];
                        }
                    }
                    if (tokenDetailsOfSoft != null) {
                        if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                            strLabelSW += " label-success";   //active
                            softPkistatus = "Active";
                        } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                            strLabelSW += " label-info";   //assigned                
                            softPkistatus = "Assigned";
                        } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                            softPkistatus = "Unassigned";
                        } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
                            softPkistatus = "suspended";
                        }
                    }
                    if (tokenDetailsOfHard != null) {
                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                            hardPkistatus = "Active";
                            strLabelHW += " label-success";   //active
                        }
                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                            hardPkistatus = "Assigned";
                            strLabelHW += " label-info";   //assigned                
                        }
                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                            hardPkistatus = "Unassigned";
                        }
                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_LOST) {
                            hardPkistatus = "LOST";
                            strLabelHW += " label-important";   //assigned                
                        }
                        if (tokenDetailsOfHard.serialnumber != null) {
                            serialNumberhard = tokenDetailsOfHard.serialnumber;
                        }
                    }
                }

                String userStatus = "user-status-value-";
                String Certistatus = null;

                if (certifts != null) {
                    if (certifts.getStatus() == CertificateManagement.CERT_STATUS_ACTIVE) {
                        Certistatus = "Valid & Active";
                    } else if (certifts.getStatus() == CertificateManagement.CERT_STATUS_REVOKED) {
                        Certistatus = "Revoked";
                    } else if (certifts.getStatus() == CertificateManagement.CERT_STATUS_EXPIRED) {
                        Certistatus = "Expired";
                    }
                    dCreatedOn = certifts.getCreationdatetime();
                    dLastUpdated = certifts.getExpirydatetime();
                } else {
                    Certistatus = "Not Issued";
                }
                String sotpStatus = "sotp-status-value-";
                String hotpStatus = "hotp-status-value-";
                String hotpType = "hotp-type-value-";
                String issuedOn = "NA";
                if (dCreatedOn != null) {
                    issuedOn = sdf.format(dCreatedOn);
                }
                String ExpireOn = "NA";
                if (dLastUpdated != null) {
                    ExpireOn = sdf.format(dLastUpdated);
                }
        %>               
        <div class="relative1">
            <div class="container-fluid">
                <form class="form-horizontal" id="primaryform" name="primaryform">
                    <div id="legend">
                        <legend>Digital Certificates And Tokens</legend> 
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Certificate</label>
                        <div class="controls">                    
                            <% if (certifts == null || certifts.getStatus() != CertificateManagement.CERT_STATUS_ACTIVE) {%>             
                            <a href="#" onclick="generatecertificate('<%= userObj.getUserId()%>')" class="btn btn-mini btn-primary">Issue Now</a>
                            <% } else {%>
                            <a href="#" onclick="loadCertificateDetails('<%=userObj.getUserId()%>')" class="btn btn-mini btn-success">View Certificate</a>
                            <% }%> 
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Status</label>
                        <div class="controls">                    
                            Issued On# <%=issuedOn%> ,Expire On# <%=ExpireOn%>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Upload Document</label>
                        <div class="controls">
                            <a href="#" onclick="uploadCustomerDetails('<%=userObj.getUserId()%>')" class="btn btn-mini btn-success" >Upload Now</a>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Manage</label>
                        <div class="controls">
                            <a href="#" onclick="loadsendcertfile('<%=userObj.getUserId()%>')" class="btn btn-mini btn-success">Share Certificate(via Email)</a>
                            <a href="#" onclick="renewcertificate('<%=userObj.getUserId()%>')" class="btn btn-mini btn-info">Renew Certificate</a>
                            <a href="#" onclick="loadrevoke('<%=userObj.getUserId()%>')" class="btn btn-mini btn-warning">Revoke Certificate</a>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Hardware PKI Token</label>
                        <div class="controls">
                            Serial No. <%=serialNumberhard%>#  Status As #
                            <%
                                if (hardPkistatus.equalsIgnoreCase("Unassigned")) {
                            %>
                            <span id="<%=hotpStatus%>" class="<%=strLabelHW%>"><%=hardPkistatus%></span> <a href="#" onclick="loadUsersID('<%=userObj.getUserId()%>')" class="btn btn-mini btn-success">Assign Token</a>          
                            <%} else {%>
                            <span id="<%=hotpStatus%>" class="<%=strLabelHW%>"><%=hardPkistatus%></span> <a href="#" onclick="changepkitokenstatus(1, '<%=userObj.getUserId()%>', 2, '<%=hotpStatus%>')" class="btn btn-mini btn-success">Activate Token</a> 
                            <%}%>
                            <a href="#" onclick="sendpfxfile('<%=userObj.getUserId()%>')" class="btn btn-mini btn-info">(Re)send PFX File (via email)</a>
                            <a href="#" onclick="sendpfxpassword('<%=userObj.getUserId()%>')" class="btn btn-mini btn-warning">(Re)send PFX Password (via mobile)</a>
                            <a href="#" onclick="changepkitokenstatus(-10, '<%=userObj.getUserId()%>', 2, '<%=hotpStatus%>')" class="btn btn-mini btn-danger" >Un-Assign Token</a>
                            <a href="#" onclick="changepkitokenstatus(-5, '<%=userObj.getUserId()%>', 2, '<%=hotpStatus%>')" class="btn btn-mini btn-danger">Mark as Lost</a>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Software PKI Token</label>
                        <div class="controls">
    <!--                        Serial No. <%=serialNumberhard%>#  Status As #-->
                            <%
                                if (hardPkistatus.equalsIgnoreCase("Unassigned")) {
                            %>
                            <span id="<%=sotpStatus%>" class="<%=strLabelSW%>"><%=softPkistatus%></span> <a href="#" onclick="loadUsersID('<%=userObj.getUserId()%>')" class="btn btn-mini btn-success">Assign Token</a>          
                            <%} else {%>
                            <span id="<%=sotpStatus%>" class="<%=strLabelSW%>"><%=softPkistatus%></span> <a href="#" onclick="changepkitokenstatus(1, '<%=userObj.getUserId()%>', 2, '<%=hotpStatus%>')"  class="btn btn-mini btn-success">Activate Token</a> 
                            <%}%>
                            <a href="#" onclick="sendpfxfile('<%=userObj.getUserId()%>')" class="btn btn-mini btn-info">(Re)send PFX File (via email)</a>
                            <a href="#" onclick="sendpfxpassword('<%=userObj.getUserId()%>')" class="btn btn-mini btn-warning">(Re)send PFX Password (via mobile)</a>
                            <a href="#" onclick="changepkitokenstatus(-10, '<%=userObj.getUserId()%>', 2, '<%=hotpStatus%>')" class="btn btn-mini btn-danger">Un-Assign Token</a>
                            <a href="#" onclick="changepkitokenstatus(-5, '<%=userObj.getUserId()%>', 2, '<%=hotpStatus%>')" class="btn btn-mini btn-danger">Mark as Lost</a>
                        </div>
                    </div>
                    <div id="legend">
                        <legend>Audit Management</legend> 
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Audit</label>                    
                        <form class="form-horizontal" id="primaryform" name="primaryform">                
                            <input type="hidden" id="_certreportType" name="_certreportType"/>
                            <input type="hidden" id="_otpauditUserID2" name="_otpauditUserID2" value="<%=userObj.userId%>"/>
                            <input type="hidden" id="_otpauditUserName2" name="_otpauditUserName2" value="<%=userObj.userName%>"/>
                            <div class="controls">                            
                                <div class="input-prepend">
                                    <span class="add-on">From:</span>   
                                    <div id="datetimepicker1111" class="input-append date">
                                        <input id="certstartdate" name="certstartdate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="input-prepend">
                                    <span class="add-on">till:</span>   
                                    <div id="datetimepicker2222" class="input-append date">
                                        <input id="certenddate" name="certenddate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    $(function () {
                                        $('#datetimepicker1111').datepicker({
                                            format: 'dd/MM/yyyy',
                                            language: 'pt-BR'
                                        });
                                    });
                                    $(function () {
                                        $('#datetimepicker2222').datepicker({
                                            format: 'dd/MM/yyyy',
                                            language: 'pt-BR'

                                        });
                                    });
                                </script>
                                Report Format   
                                <div class="btn-group">
                                    <button class="btn  btn-mini" id="_certreportType-div">Select Type</button>
                                    <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ChangeReportUserSimplifiedCert(0)">PDF</a></li>
                                        <li><a href="#" onclick="ChangeReportUserSimplifiedCert(1)">CSV</a></li>
                                        <li><a href="#" onclick="ChangeReportUserSimplifiedCert(2)">TEXT</a></li>
                                    </ul>
                                </div>
                                <a href="#" class="btn btn-success btn-mini" onclick="downloadUserCertAudit()">Download Report</a> 
                        </form>
                    </div>                
            </div>

            <%} else if (userObj == null) {%>
            <%}%>
            </form>
        </div>
    </div>
</div>
<!-- modal for change home group -->            
<div id="changehome" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Select Home Group</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">

            <form class="form-horizontal" id="edithomecountryForm" name="edithomecountryForm">

                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_userID" name="_userID">

                    <div class="control-group">
                        <label class="control-label"  for="username">Group</label>
                        <div class="controls">

                            <select class="span10" name="_groupid" id="_groupid">
                                <%
                                    int groupid = -1;
                                    int countryid = -1;
                                    UserGroupsManagement uMgmt = new UserGroupsManagement();
                                    Usergroups[] uList = uMgmt.ListGroups(sessionId, channel.getChannelid());
                                    if (uList
                                            != null) {
                                        for (int i = 0; i < uList.length; i++) {
                                            groupid = uList[i].getGroupid();
                                            if (uList[i].getCountry() != null && uList[i].getCountry().equals("") == false) {
                                                countryid = Integer.parseInt(uList[i].getCountry());
                                            }
                                %>

                                <option value="<%=uList[i].getGroupid()%>"><%=uList[i].getGroupname()%></option>
                                <%
                                        }
                                    }
                                %>
                            </select>
                        </div>
                    </div>            
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="editgeotrack-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="editgeotracking()()" id="buttonEditGeotrack" type="button">Change Home Country</button>
    </div>
</div>
<!-- change home group model end -->
<!-- modal for edit roaming setting -->
<div id="editroamingsetting" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Enable Roaming</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="editRoamingSettingForm">
                <%LocationManagement lManagement = new LocationManagement();
                    Countrylist cList[] = lManagement.getAllCountries();
                    Statelist sList[] = null;
                    if (countryid
                            != -1) {
                        sList = lManagement.getAllStateListByCountry(countryid);
                    }

                %>

                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_roamingID" name="_roamingID"  >
                    <input type="hidden" id="_userID1" name="_userID1"  >
                    <div class="control-group">
                        <label class="control-label"  for="username">Enable Roaming</label>
                        <div class="controls">
                            <select class="span8" name="_eroaming" id="_eroaming">
                                <option value="2">DISABLE ROAMING</option>
                                <option value="1">ENABLE COUNTRY LEVEL</option>
                                <option value="3">ENABLE STATE LEVEL</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group" id="fContires">
                        <label class="control-label"  for="username">Foreign Country</label>
                        <div class="controls">

                            <select  name="_foreignCountry" id="_foreignCountry"  class="span9" multiple>
                                <% for (int j = 0;
                                            j < cList.length;
                                            j++) {
                                %>
                                <option data-content="<span class='label label-success'><%=cList[j].getCountyName()%></span>"value="<%=cList[j].getCountryid()%>"><%=cList[j].getCountyName()%></option>                                             
                                <%
                                    }%>
                            </select>
                        </div>
                    </div>
                    <% if (sList
                                != null) {
                    %>                    <div class="control-group" id="states">
                        <label class="control-label"  for="username">Roaming States</label>
                        <div class="controls">

                            <select  name="stateList" id="stateList"  class="span9" multiple>
                                <%
                                    for (int j = 0; j < sList.length; j++) {
                                %>
                                <option data-content="<span class='label label-success'><%=sList[j].getStateName()%></span>"value="<%=sList[j].getStateName()%>"><%=sList[j].getStateName()%></option>                                             
                                <%
                                    }
                                %>
                            </select>
                        </div>
                    </div>
                    <%
                        }
                    %>
                    <div class="control-group" id="sDate">
                        <label class="control-label"  for="username">Start Date :</label>
                        <div class="controls">
                            <div class="input-prepend">
                                <div id="datetimepicker1" class="input-append date">
                                    <input id="_startdate" name="_startdate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                    <span class="add-on" >
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="control-group" id="eDate">
                        <label class="control-label"  for="username">End Date :</label>
                        <div class="controls">
                            <div class="input-prepend">
                                <!--<div class="well">-->
                                <!--<span class="add-on">Till :</span>-->   
                                <div id="datetimepicker2" class="input-append date">
                                    <input id="_enddate" name="_enddate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                                <!--</div>-->
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="editroaming-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="editroamingsetting()" id="buttonEditRoaming" type="button">Save Roaming Setting</button>
    </div>
</div>
<!-- end of modal for edit roaming setting -->

<!-- modal for send certificate -->
<div id="SendCertificate" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idSendCertificate"></div></h3>
        <h3 id="myModalLabel">Send Certificate To</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="SendCertificateform">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_userSENDCERT" name="_userSENDCERT" >
                    <div class="control-group">
                        <label class="control-label"  for="username">Recipient's Emailid</label>
                        <div class="controls">
                            <input type="text" id="_emailSENDCERT" name="_emailSENDCERT" placeholder="email id " class="input-xlarge">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="SendCertificate-result"></div>
        <button class="btn btn-primary" onclick="sendcertificatefile()" id="buttonSendCertificate" type="button">Submit</button>
    </div>
</div>
<!-- modal for send certificate ends -->

<!-- modal for revoke certifiacte start -->
<div id="RevokeCertificate" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idEditPushMaperName"></div></h3>
        <h3 id="myModalLabel">Reason To Revoke</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="RevokeCertificateform">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_userCR" name="_userCR" >

                    <div class="control-group">
                        <label class="control-label"  for="username">Reason</label>
                        <div class="controls">
                            <input type="text" id="_reason" name="_reason" placeholder="Any reason?" class="input-xlarge">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="HardwarePkiToken-result"></div>
        <button class="btn btn-primary" onclick="revokecertificate()" id="buttonHardwareRegistration" type="button">Submit</button>
    </div>
</div>
<!-- modal for revoke certificate ends -->

<!-- modal for hardware pki token start -->
<div id="HardwarePkiToken" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idEditPushMaperName"></div></h3>
        <h3 id="myModalLabel">Assign Hardware Token</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="HardwarePkiTokenform">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_userIDHR" name="_userIDHR" >

                    <div class="control-group">
                        <label class="control-label"  for="username">Serial Number</label>
                        <div class="controls">
                            <input type="text" id="_registrationCode" name="_registrationCode" placeholder="2210D03840000A" class="input-xlarge">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="HardwarePkiToken-result"></div>
        <button class="btn btn-primary" onclick="assignhardwaretokens(2)" id="buttonHardwareRegistration" type="button">Submit</button>
    </div>
</div>
<!-- modal for assign hardware token ends -->

<!-- modal for certificate details -->
<div id="certificateDetails" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Certificate Details</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="certform">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label"  for="username">Serial Number</label>
                        <div class="controls">
                            <input type="text" id="_srnoCertifiacte" name="_srnoCertifiacte" class="span4">   
                            & Algorithm: <input type="text" id="_algoname" name="_algoname" class="span4">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">User Details</label>
                        <div class="controls">
                            <textarea id="_subjectdn" name="_subjectdn" style="width:95%"></textarea>
                            <!--<input type="text" id="_subjectdn" name="_subjectdn"  class="input-xlarge">-->
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Issuer Details</label>
                        <div class="controls">
                            <textarea id="_issuerdn" name="_issuerdn" style="width:95%"></textarea>
                            <!--<input type="text" id="_issuerdn" name="_issuerdn"  class="input-xlarge">-->
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Issued On</label>
                        <div class="controls">
                            <input type="text" id="_notbefore" name="_notbefore"  class="input-xlarge">
                        </div>
                    </div>                    
                    <div class="control-group">
                        <label class="control-label"  for="username">Valid till</label>
                        <div class="controls">
                            <input type="text" id="_notafter" name="_notafter"  class="input-xlarge">
                        </div>
                    </div>                    
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
</div>
<!-- modal for certificate details end -->

<!-- modal for cert upload -->
<div id="kycupload" class="modal hide fade" tabindex="-1" role="dialog" style="width: 650px;"   aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Upload Documents</h3>
        <p>Please Compress all documents in zip and upload here.</p>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="kycuploadForm">
                <fieldset>
                    <input type="hidden" id="_usersid" name="_usersid" >

                    <div class="control-group">
                        <label class="control-label"  for="username">Select File:</label>                                    
                        <div class="controls fileupload fileupload-new" data-provides="fileupload">
                            <div class="input-append">
                                <div class="uneditable-input span3"><i class="icon-file fileupload-exists"></i> 
                                    <span class="fileupload-preview"></span>
                                </div>
                                <span class="btn btn-file" >
                                    <span class="fileupload-new"><i class='icon-file'></i></span>
                                    <span class="fileupload-exists"><i class='icon-file'></i></span>
                                    <input type="file" id="filekycupload" name="filekycupload"/>
                                </span>
                                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><i class='icon-trash'></i></a>
                                <button class="btn btn-success" id="buttonkycUpload"  onclick="UploadkycFile()">Upload</button>
                            </div>
                        </div>

                    </div>
                    <br>
                </fieldset>
            </form>
        </div>
    </div>
    <!--    <div class="modal-footer">
            <div id="addUser-result"></div>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button class="btn btn-primary" onclick="adduser()" id="addUserButton">Save Details Now>></button>
        </div>-->
</div> 


<div id="userPkiTokenauditDownload" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idauditDownload"></div></h3>
        <h3 id="myModalLabel">Download Audit</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="testSMSPrimaryForm" name="testSMSPrimaryForm">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_auditUserIDPKI" name="_auditUserIDPKI"/>
                    <input type="hidden" id="_auditUserNamePKI" name="_auditUserNamePKI"/>
                    <div class="control-group">
                        <label class="control-label"  for="username">Start Date</label>
                        <div class="controls" align="left" >

                            <!--<span class="add-on">From:</span>-->   
                            <div id="PushdatetimepickerPKI" class="input-append date">
                                <input id="_auditStartDatePKI" name="_auditStartDatePKI" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">End Date</label>
                        <div class="controls" align="left">
                            <!--<span class="add-on">Till:</span>-->   
                            <div id="PushdatetimepickerPKI1" class="input-append date">
                                <input id="_auditEndDatePKI" name="_auditEndDatePKI" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>                    
                </fieldset>
            </form>
        </div>
    </div>
    <script>
        $(function () {
            $('#PushdatetimepickerPKI').datepicker({
                format: 'dd/MM/yyyy',
                language: 'pt-BR'
            });
        });
        $(function () {
            $('#PushdatetimepickerPKI1').datepicker({
                format: 'dd/MM/yyyy',
                language: 'pt-BR'
            });
        });
    </script>
    <div class="modal-footer">
        <div id="editoperator-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="searchUsersPKITokenAuditPKI()" id="buttonEditOperatorSubmit">Show Audit</button>
    </div>
</div>

<div id="suspendTokenSimplified" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Reject Request</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="suspendTokenform" name="suspendTokenform">
                <fieldset>
                    <input type="hidden" id="_userstatus" name="_userstatus" >
                    <input type="hidden" id="_userID" name="_userID" >
                    <input type="hidden" id="_usercategory" name="_usercategory" >
                    <input type="hidden" id="_usersubcategory" name="_usersubcategory" >
                    <input type="hidden" id="uidiv" name="uidiv" >
                    <div class="control-group">
                        <label class="control-label"  for="username">Description</label>
                        <div class="controls">
                            <textarea class="form-control" rows="3" id="_suspendreason" name="_suspendreason"></textarea>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="rejectkyc-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="suspendToken()" id="addUserButton">Suspend Now>></button>
    </div>
</div>

<div id="verifyOTPSimplified" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Verify One Time Password</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="verifyOTPForm" name="verifyOTPForm">
                <fieldset>
                    <input type="hidden" id="_userIDOV" name="_userIDOV" >
                    <input type="hidden" id="_usertypeIDS" name="_usertypeIDS">
                    <div class="control-group">
                        <label class="control-label"  for="username">Enter One Time Password to Verify:</label>
                        <div class="controls">
                            <input type="text" id="_oobotp" name="_oobotp" placeholder="Enter OTP" class="input-medium">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <div class="span3" id="verify-otp-result"></div>
        <button class="btn btn-primary" onclick="verifyOTP()" id="addnewOperatorSubmitBut">Verify OTP</button>
    </div>
</div>

<div id="HardRegistration" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idEditPushMaperName"></div></h3>
        <h3 id="myModalLabel">Assign Hardware Token</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="HardwareRegistrationform">
                <fieldset>
                    <!-- Name -->
                    <!--<input type="hidden" id="_userIDHR" name="_userIDHR" >-->
                    <input type="hidden" id="_userid" name="_userid" >
                    <input type="hidden" id="_category" name="_category" >
                    <input type="hidden" id="_subcategory" name="_subcategory" >
                    <div class="control-group">
                        <label class="control-label"  for="username">Serial Number</label>
                        <div class="controls">
                            <input type="text" id="_serialnumber" name="_serialnumber" placeholder="e.g 123231212" class="input-xlarge">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="HardwareRegistration-result"></div>
        <button class="btn btn-primary" onclick="assignhardwaretoken1()" id="buttonHardwareRegistration" type="button">Submit</button>
    </div>
</div>

<div id="ResyncHardwareSimplified" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Resync Hardware Token</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="ResyncHardwareform">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_userIDHW" name="_userIDHW" >
                    <input type="hidden" id="_categoryHW" name="_categoryHW" value="2">
                    <input type="hidden" id="_srnoHW" name="_srnoHW" >
                    <div class="control-group">
                        <label class="control-label"  for="username">First OTP</label>
                        <div class="controls">
                            <input type="text" id="_FirstOtp" name="_FirstOtp" placeholder="2210D03840000A" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Second OTP</label>
                        <div class="controls">
                            <input type="text" id="_SecondOtp" name="_SecondOtp" placeholder="2210D03840000A" class="input-xlarge">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="ResyncHardware-result"></div>
        <button class="btn btn-primary" onclick="resynchardware()" id="buttonResyncHardware" type="button">Submit</button>
    </div>
</div>

<div id="ReplaceHardwareSimplified" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Replace Hardware Token</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="ReplaceHardwareform">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_userIDReplaceHW" name="_userIDReplaceHW" >
                    <input type="hidden" id="_categoryReplaceHW" name="_categoryReplaceHW" >
                    <input type="hidden" id="_subcategoryReplaceHW" name="_subcategoryReplaceHW" >
                    <!--<input type="hidden" id="_srnoHW" name="_srnoHW" >-->
                    <div class="control-group">
                        <label class="control-label"  for="username">Old Token Serial No</label>
                        <div class="controls">
                            <input type="text" id="_oldTokenSerialNo" name="_oldTokenSerialNo" placeholder="2210D03840000A" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Old Token Marked As</label>
                        <div class="controls">
                            <select name="_oldTokenStatus" id="_oldTokenStatus" class="span3">
                                <option value="-5">Lost</option>                                             
                                <option value="-10">Free</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">New Token Serial No</label>
                        <div class="controls">
                            <input type="text" id="_newTokenSerialNo" name="_newTokenSerialNo" placeholder="2210D03840000A" class="input-xlarge">
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="ReplaceHardware-result"></div>
        <button class="btn btn-primary" onclick="replacehardware()" id="buttonResyncHardware" type="button">Submit</button>
    </div>
</div>
<div id="PUKCODESimplified" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idEditPushMaperName"></div></h3>
        <h3 id="myModalLabel">PUK Code</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="pukform">
                <fieldset>
                    <!-- Name -->
                    <!--<input type="hidden" id="_userIDHR" name="_userIDHR" >-->
                    <input type="hidden" id="_useridPUK" name="_useridPUK" >
                    <input type="hidden" id="_pukExpiry" name="_pukExpiry" >
                    <!--                        <input type="hidden" id="_subcategoryPUK" name="_subcategoryPUK" >
                                            <input type="hidden" id="_pukcode" name="_pukcode" >-->
                    <div class="control-group">
                        <label class="control-label"  for="username">PUK Code</label>
                        <div class="controls">
                            <input type="text" id="_pukcode" name="_pukcode" placeholder="e.g 123231212"  readonly class="input-xlarge">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="puk-result"></div>
        <button class="btn btn-primary" onclick="sendPUKCode('EMAIL')" id="buttonPUK" type="button">Send via Email</button>
        <button class="btn btn-primary" onclick="sendPUKCode('SMS')" id="buttonPUK" type="button">Send via SMS</button>
    </div>
</div>
<%@include file="footer.jsp" %>


