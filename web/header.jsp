<%@page import="com.mollatech.axiom.nucleus.crypto.AxiomProtect"%>
<%@page import="com.mollatech.axiom.connector.access.controller.AccessMatrixSettings"%>
<%@page import="java.util.Properties"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.PropsFileUtil"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.ecopin.management.EPINManagement"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.LoadSettings"%>
<%@ page import="com.mollatech.axiom.nucleus.db.Operators" %>
<%@ page import="com.mollatech.axiom.nucleus.db.Channels" %>
<%@ page import="com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement" %>
<%@ page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement" %>
<%@ page import="com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement" %>
<%@ page import="com.mollatech.axiom.nucleus.db.connector.management.RemoteAccessManagement" %>
<%@ page import="com.mollatech.axiom.nucleus.db.operation.AxiomChannel" %>
<%@ page import="com.mollatech.axiom.nucleus.db.operation.AxiomOperator" %>
<%@ page import="com.mollatech.axiom.nucleus.db.Accesses" %>
<%@ page import="com.mollatech.axiom.nucleus.db.Roles" %>
<%@ page import="org.hibernate.Session" %>
<%@ page import="java.text.SimpleDateFormat" %>

<%
    String strContextPath = this.getServletContext().getContextPath();
    String status = (String) session.getAttribute("_apOprAuth");
    if (status == null || status.compareTo("yes") != 0) {
        String nextJSP = "/index.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
        dispatcher.forward(request, response);
        return;
    }

    //SimpleDateFormat tzTimeHeader = new SimpleDateFormat("EEE dd/MMM/yy-HH:mm");
    SimpleDateFormat tzTimeHeader = new SimpleDateFormat("dd/MMM HH:mm");
    String completeTimeHeader = tzTimeHeader.format(new Date());

    Operators oprObj = (Operators) session.getAttribute("_apOprDetail");
    String name = oprObj.getName();

    AccessMatrixSettings accessObj = (AccessMatrixSettings) session.getAttribute("_apAccessEntry");
    //System.out.println(" AccessMatrixSettings is not avialable in session");
    OperatorsManagement oprmngObj = new OperatorsManagement();
    Roles[] roles = oprmngObj.getAllRoles(oprObj.getChannelid());
    Roles adminRole = oprmngObj.getRoleById(oprObj.getChannelid(), oprObj.getRoleid());
    String strOperatorRole = (String) session.getAttribute("_apOprRole");
    int iOperatorRole = Integer.valueOf(strOperatorRole).intValue();

    EPINManagement epinmngObj = new EPINManagement();
    String sessionid = (String) session.getAttribute("_apSessionID");
    int iFailedRequests = epinmngObj.GetCountOfEpintracker(sessionid, oprObj.getChannelid(), -3); //failed
    int iPendingRequests = epinmngObj.GetCountOfEpintracker(sessionid, oprObj.getChannelid(), 2); //pending

    //if (oprObj.getRoleid() == 1) {  //this is sysadmin
    if (adminRole.getName().equals("sysadmin")) {
        accessObj.listChannel = true;
        accessObj.addChannel = true;
        accessObj.editChannel = true;
//2, Operators :
        accessObj.listOperator = true;
        accessObj.addOperator = true;
        accessObj.editOperator = true;
        // accessObj.removeOperator=true;

//3  Units :
        accessObj.listUnits = true;
        accessObj.addUnits = true;
        accessObj.editUnits = true;
        accessObj.deleteUnits = true;

//    4. Templates :
        accessObj.listTemplate = true;
        accessObj.addTemplate = true;
        accessObj.editTemplate = true;
        accessObj.removeTemplate = true;

        //5. User Password :
        accessObj.ListUsers = true;
        accessObj.addUser = true;
        accessObj.editUser = true;
        accessObj.removeUser = true;
        //5. User Group :
        accessObj.ListUsersGroup = true;
        accessObj.addUserGroup = true;
        accessObj.editUserGroup = true;
        accessObj.statusUserGroup = true;
        accessObj.removeUserGroup = true;

        //6. Challenge Response
        accessObj.listChallengeResponse = true;
        accessObj.addChallengeResponse = true;
        accessObj.editChallengeResponse = true;
        accessObj.removeChallengeResponse = true;

        //7. Trusted Device
        accessObj.listTrustedDevice = true;
        accessObj.addTrustedDevice = true;
        accessObj.editTrustedDevice = true;
        accessObj.removeDestroyClients = true;

        //10. Geo - location
        accessObj.listGeoLocation = true;
        accessObj.addGeoLocation = true;
        accessObj.editGeoLocation = true;
        accessObj.removeGeoLocation = true;

        //11. System message
        accessObj.listSystemMessage = true;
        accessObj.addSystemMessage = true;
        accessObj.editSystemMessage = true;
        accessObj.removeSystemMessage = true;
        //12. Secure Phrase mngt 
        accessObj.listImageManagement = true;
        accessObj.addImageManagement = true;
        accessObj.editImageManagement = true;
        accessObj.removeImageManagement = true;

//13. SNMP Phrase mngt 
        accessObj.listSnmpManagement = true;
        accessObj.addSnmpManagement = true;
        accessObj.editSnmpManagement = true;
        accessObj.removeSnmpManagement = true;

        //added by vikram - 7jan2016
        //E-SOIGNER
        accessObj.listESIGNERManagement = true;
        accessObj.addESIGNERManagement = true;
        accessObj.editESIGNERManagement = true;
        accessObj.removeESIGNERManagement = true;

        accessObj.listWebSealManagement = true;
        accessObj.addWebSealManagement = true;
        accessObj.editWebSealManagement = true;
        accessObj.removeWebSealManagement = true;
        //end of addition

        //14. Report Shceduler mngt 
        accessObj.listReportSchedulerManagement = true;
        accessObj.addReportSchedulerManagement = true;
        accessObj.editReportSchedulerManagement = true;
        accessObj.removeReportSchedulerManagement = true;
        //15. Report Shceduler mngt 
        accessObj.listTwoWayAuthManagement = true;
        accessObj.addTwoWayAuthManagement = true;
        accessObj.editTwoWayAuthManagement = true;
        accessObj.removeTwoWayAuthManagement = true;

        //15. Report Shceduler mngt 
        accessObj.listWebWatch = true;
        accessObj.addWebWatch = true;
        accessObj.editWebWatch = true;
        accessObj.removeWebWatch = true;
        accessObj.viewTwoAuthRports = true;

        //16. web resource mngt 
        accessObj.listWebResource = true;
        accessObj.addWebResource = true;
        accessObj.editWebResource = true;
        accessObj.removeWebResource = true;
//12. Configurations
        accessObj.listSmsGateway = true;
        accessObj.addSmsGateway = true;
        accessObj.editSmsGateway = true;
        accessObj.removeSmsGateway = true;

        accessObj.listUSSDGateway = true;
        accessObj.addUSSDGateway = true;
        accessObj.editUSSDGateway = true;
        accessObj.removeUSSDGateway = true;

        accessObj.listVOICEGateway = true;
        accessObj.addVOICEGateway = true;
        accessObj.editVOICEGateway = true;
        accessObj.removeVOICEGateway = true;

        accessObj.listEMAILGateway = true;
        accessObj.addEMAILGateway = true;
        accessObj.editEMAILGateway = true;
        accessObj.removeEMAILGateway = true;

        accessObj.listFAXGateway = true;
        accessObj.addFAXGateway = true;
        accessObj.editFAXGateway = true;
        accessObj.removeFAXGateway = true;

        accessObj.listBillingManager = true;
        accessObj.addBillingManager = true;
        accessObj.editBillingManager = true;
        accessObj.removeBillingManager = true;

        accessObj.listPushGateway = true;
        accessObj.addPushGateway = true;
        accessObj.editPushGateway = true;
        accessObj.removePushGateway = true;
        accessObj.listUserSourceSettings = true;
        accessObj.addUserSourceSettings = true;
        accessObj.editUserSourceSettings = true;
        accessObj.removeUserSourceSettings = true;

        accessObj.listPasswordPolicySettings = true;
        accessObj.addPasswordPolicySettings = true;
        accessObj.editPasswordPolicySettings = true;
        accessObj.removePasswordPolicySettings = true;

        accessObj.listChannelProfileSettings = true;
        accessObj.addChannelProfileSettings = true;
        accessObj.editChannelProfileSettings = true;
        accessObj.removeChannelProfileSettings = true;

        accessObj.listOtpTokensSettings = true;
        accessObj.addOtpTokensSettings = true;
        accessObj.editOtpTokensSettings = true;
        accessObj.removeOtpTokensSettings = true;

        accessObj.listCertificateSettings = true;
        accessObj.addCertificateSettings = true;
        accessObj.editCertificateSettings = true;
        accessObj.removeCertificateSettings = true;

        accessObj.listMobileTrustSettings = true;
        accessObj.addMobileTrustSettings = true;
        accessObj.editMobileTrustSettings = true;
        accessObj.removeMobileTrustSettings = true;

        accessObj.listRadiusConfigSettings = true;
        accessObj.addRadiusConfigSettings = true;
        accessObj.editRadiusConfigSettings = true;
        accessObj.removeRadiusConfigSettings = true;

        accessObj.listGlobalSettings = true;
        accessObj.addGlobalSettings = true;
        accessObj.editGlobalSettings = true;
        accessObj.removeGlobalSettings = true;

        accessObj.listEpinConfigSettings = true;
        accessObj.addEpinConfigSettings = true;
        accessObj.editEpinConfigSettings = true;
        accessObj.removeEpinConfigSettings = true;

        accessObj.listImageSettings = true;
        accessObj.addImageConfigSettings = true;
        accessObj.editImageConfigSettings = true;
        accessObj.removeImageConfigSettings = true;

//7. OTP tokens
        // accessObj.OOBToken;
        accessObj.listOOBSMSToken = true;
        accessObj.addOOBSMSToken = true;
        accessObj.editOOBSMSToken = true;
        accessObj.removeOOBSMSToken = true;

        accessObj.listOOBVOICEToken = true;
        accessObj.addOOBVOICEToken = true;
        accessObj.editOOBVOICEToken = true;
        accessObj.removeOOBVOICEToken = true;

        accessObj.listOOBUSSDToken = true;
        accessObj.addOOBUSSDToken = true;
        accessObj.editOOBUSSDToken = true;
        accessObj.removeOOBUSSDToken = true;

        accessObj.listOOBEMAILToken = true;
        accessObj.addOOBEMAILToken = true;
        accessObj.editOOBEMAILToken = true;
        accessObj.removeOOBEMAILToken = true;

//   - S/W Tokens 
        accessObj.listSWMOBILEToken = true;
        accessObj.addSWMOBILEToken = true;
        accessObj.editSWMOBILEToken = true;
        accessObj.removeSWMOBILEToken = true;

        accessObj.listSWWEBToken = true;
        accessObj.addSWWEBToken = true;
        accessObj.editSWWEBToken = true;
        accessObj.removeSWWEBToken = true;

        accessObj.listSWPCToken = true;
        accessObj.addSWPCToken = true;
        accessObj.editSWPCToken = true;
        accessObj.removeSWPCToken = true;

//    - H/W Tokens 
        accessObj.listHWOTPToken = true;
        accessObj.addHWOTPToken = true;
        accessObj.editHWOTPToken = true;
        accessObj.removeHWOTPToken = true;

        accessObj.listCerticate = true;
        accessObj.addCerticate = true;
        accessObj.editCerticate = true;
        accessObj.removeCerticate = true;

        accessObj.listHWPKIToken = true;
        accessObj.addHWPKIToken = true;
        accessObj.editHWPKIToken = true;
        accessObj.removeHWPKIToken = true;

        accessObj.listSWPKIToken = true;
        accessObj.addSWPKIToken = true;
        accessObj.editSWPKIToken = true;
        accessObj.removeSWPKIToken = true;

//12. Reports
        accessObj.listUserReport = true;
        accessObj.viewUserReport = true;
        accessObj.downloadUserReport = true;

        accessObj.listMessageReport = true;
        accessObj.viewMessageReport = true;
        accessObj.downloadMessageReport = true;

        accessObj.listOtpReport = true;
        accessObj.viewOtpReport = true;
        accessObj.downloadOtpReport = true;

        accessObj.listOtpFailureReport = true;
        accessObj.viewOtpFailureReport = true;
        accessObj.downloadOtpFailureReport = true;

        accessObj.listPkiReport = true;
        accessObj.viewPkiReport = true;
        accessObj.downloadPkiReport = true;

        accessObj.listEpinSystemReport = true;
        accessObj.viewEpinSystemReport = true;
        accessObj.downloadEpinSystemReport = true;

        accessObj.listEpinUserSpecificReport = true;
        accessObj.viewEpinUserSpecificReport = true;
        accessObj.downloadEpinUserSpecificReport = true;

        accessObj.listRemoteSigningReport = true;
        accessObj.viewRemoteSigningReport = true;
        accessObj.downloadRemoteSigningReport = true;

        accessObj.listsubscriptionBaseBillingReport = true;
        accessObj.viewsubscriptionBaseBillingReport = true;
        accessObj.downloadsubscriptionBaseBillingReport = true;

        accessObj.listtranscationBaseBillingReport = true;
        accessObj.viewtranscationBaseBillingReport = true;
        accessObj.downloadtranscationBaseBillingReport = true;

        accessObj.listtranscationReport = true;
        accessObj.viewtranscationReport = true;
        accessObj.downloadtranscationReport = true;

        accessObj.listHoneyTrapReport = true;
        accessObj.viewHoneyTrapReport = true;
        accessObj.downloadHoneyTrapReport = true;

        accessObj.listCertificateReport = true;
        accessObj.viewCertificateReport = true;
        accessObj.downloadCertificateReport = true;

//13. Logout
//    license
        accessObj.listlicenseDetails = true;
//     token upload
        accessObj.listhwOTPTokenUpload = true;
        accessObj.listhwPKITokenUpload = true;
        //        sign pdf
        accessObj.signPDFDOC = true;
//        change security credentials
        accessObj.changeSecurityCredentials = true;
//        audit trail
        accessObj.downloadAuditTrail = true;
        accessObj.downloadSessionAuditTrail = true;
        accessObj.killSessionID = true;
//        Download Logs
        accessObj.downloadLogs = true;
        accessObj.downloadCleanupLogs = true;
        accessObj.connectorStatus = true;
//        audit
        accessObj.auditIntegrity = true;
//    accessObj.addreportScheduler;
//        change password
        accessObj.changePassword = true;
        accessObj.changeDBPassword = true;
        //Requester
        accessObj.requesterArchiveRequest = true;
        accessObj.requesterPendingRequest = true;
        //Authorizer
        accessObj.authorizerArchiveRequest = true;
        accessObj.authorizerPendingRequest = true;
        accessObj.requesterOperatorList = true;
        accessObj.kycRequests = true;
        accessObj.listErrorMessages = true;
        accessObj.listTwoAuthRports = true;
        accessObj.listEasyCheckinReport = true;
        accessObj.viewEasyCheckinReport = true;
        accessObj.downloadasyCheckinReport = true;

        //for access Matrix Managemebt
        accessObj.viewaccessMatrix = true;
        accessObj.editaccessMatrix = true;
        accessObj.removeaccessMatrix = true;
        accessObj.addaccessMatrix = true;

        //accessObj.listTwoWayAuthManagement = true;
        //accessObj.twoWayAuthDualChannel = true;
    }

    String FailedRequests = "";
    String PendingRequests = "";

    if (iFailedRequests != 0) {
        FailedRequests = "<span class=\"label label-warning\">" + iFailedRequests + "</span>";
    }

    if (iPendingRequests != 0) {
        PendingRequests = "<span class=\"label label-warning\">" + iPendingRequests + "</span>";
    }
    Properties navigationSettings = null;
%>
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta name="google-translate-customization" content="26e4d928eae0659b-61f8bcc48ec8af9a-g74ee54aac640948a-11"></meta>
        <title><%=strContextPath%> Management Portal For <%=name%></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="./assets/css/bootstrap.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 60px;
                padding-bottom: 40px;
            }
            /*            body {
                            margin: 0;    
                            background: url('assets/img/molla_logo.jpg');                
                            background-size: 400px 200px;
                            background-repeat: no-repeat;     
                            background-position: center;
                            background-attachment: fixed;            
                            display: compact;
                        }*/
        </style>
        <link href="./assets/css/bootstrap-responsive.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="../assets/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="./assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="./assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="./assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="./assets/ico/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="./assets/ico/favicon.png">
        <link rel="stylesheet" href="./assets/css/datepicker.css">
        <link rel="stylesheet" href="./assets/css/jquery.sidr.dark.css">

        <script src="./assets/js/jquery.js"></script>
        <script src="./assets/js/bootstrap-transition.js"></script>
        <script src="./assets/js/bootstrap-alert.js"></script>
        <script src="./assets/js/bootstrap-modal.js"></script>
        <script src="./assets/js/bootstrap-dropdown.js"></script>
        <script src="./assets/js/bootstrap-scrollspy.js"></script>
        <script src="./assets/js/bootstrap-tab.js"></script>
        <script src="./assets/js/bootstrap-tooltip.js"></script>
        <script src="./assets/js/bootstrap-popover.js"></script>
        <script src="./assets/js/bootstrap-button.js"></script>
        <script src="./assets/js/bootstrap-collapse.js"></script>
        <script src="./assets/js/bootstrap-carousel.js"></script>
        <script src="./assets/js/bootstrap-typeahead.js"></script>
        <script src="./assets/js/bootstrap-datepicker.js"></script>
        <script src="./assets/js/json_sans_eval.js"></script>
        <script src="./assets/js/bootbox.min.js"></script>
        <script src="./assets/js/channels.js"></script>
        <script src="./assets/js/operators.js"></script>
        <script src="./assets/js/jquery.sidr.min.js"></script>

        <!--pagination-->
        <script src="./assets/js/jquery.dataTables.min.js"></script>
        <script src="assets/js/dataTables.responsive.min.js"></script>
        <link  rel="stylesheet" href="assets/css/jquery.dataTables.min.css">
        <link  rel="stylesheet" href="assets/css/responsive.dataTables.min.css">


    </head>
    <body>
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="brand" href="home.jsp"><%=strContextPath%></a>
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <% if (accessObj.listChannel == true || accessObj.listOperator == true
                                        || accessObj.listUnits == true || accessObj.ListUsers == true
                                        || accessObj.ListUsersGroup == true || accessObj.listTemplate == true) {%> 
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Channels Management<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <% if (accessObj.listChannel == true) {%>  
                                    <li><a href="channels.jsp"><i class="icon-file"></i>Channels</a></li>                                    
                                    <!--<li class="divider"></li>-->
                                    <%}%>
                                    <% if (accessObj.listOperator == true) {%>  
                                    <li><a href="operatorsmain.jsp"><i class="icon-file"></i>Operators</a></li>
                                    <!--<li class="divider"></li>-->
                                    <%}%>
                                    <% if (accessObj.listUnits == true) {%>  
                                    <li><a href="UnitsList.jsp"><i class="icon-file"></i>Units</a></li>
                                    <!--<li class="divider"></li>-->
                                    <%}%>
                                    <% if (accessObj.ListUsersGroup == true) {%>  
                                    <li><a href="UsersGroup.jsp"><i class="icon-file"></i>Users Group</a></li>
                                        <%}%>
                                        <%if (accessObj.listTemplate == true) {%>
                                    <!--<li class="divider"></li>-->
                                    <li class="dropdown-submenu">
                                        <a tabindex="-1" href="#"><i class="icon-file"></i>Message Templates</a>
                                        <ul class="dropdown-menu">
                                            <%if (accessObj.addTemplate == true) {%>
                                            <!--<li><a href="TemplateNew.jsp"><i class="icon-file"></i>Add New Template</a></li>                                    
                                            <li class="divider"></li>
                                            -->
                                            <%}%>
                                            <li><a href="TemplateMobileList.jsp"><i class="icon-file"></i>Mobile Templates</a></li>                                    
                                            <li><a href="TemplateEmailList.jsp"><i class="icon-file"></i>Email Templates</a></li>
                                        </ul>                                            
                                    </li>
                                    <%if (accessObj.listErrorMessages == true) {%>
                                    <li><a href="ErrorMessageList.jsp"><i class="icon-file"></i>Error Messages</a></li>
                                        <%}%>                                   
                                        <%}%>
                                </ul>
                            </li>
                            <% } %>

                            <%if (!name.equals("sysadmin")) {%>
                            <%if (oprObj.getOperatorType() == 2) {//2 == authorizer%> 
                            <li>
                                <a href="Authoperator.jsp">Operators</a>  
                            </li>
                            <%}%>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Maker/Checker<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <%if (oprObj.getOperatorType() == 2) {//2 == authorizer%> 
                                    <li><a  href="searchArchiveList.jsp">Authorizer Archive Requests</a></li> 
                                    <li><a href="searchPendingList.jsp"> Authorizer Pending Requests</a></li>
                                        <%}else{%>
                                    <li><a href="requesterOperators.jsp">Requester Operators</a></li>
                                    <li><a   href="searchArchiveListRequester.jsp">Requester Archived Requests</a></li>
                                    <li><a   href="PendingListForRequster.jsp">Requester Pending Requests</a></li>
                                        <%}%>
                                </ul>
                            </li>
                            <%}%>



                            <%if (accessObj.listSmsGateway == true || accessObj.listUSSDGateway 
                                 || accessObj.listVOICEGateway == true
                                        || accessObj.listEMAILGateway == true
                                        || accessObj.listBillingManager == true
                                        || accessObj.listUserSourceSettings == true
                                        || accessObj.listPasswordPolicySettings == true || accessObj.listChannelProfileSettings == true
                                        || accessObj.listOtpTokensSettings == true || accessObj.listCertificateSettings == true
                                        || accessObj.listMobileTrustSettings == true
                                        || accessObj.listRadiusConfigSettings == true
                                        || accessObj.listImageSettings == true                                    
                                    
                                
                                    ){%>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Configuration<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <%  if (accessObj.listSmsGateway == true || accessObj.listUSSDGateway
                                                || accessObj.listVOICEGateway == true || accessObj.listEMAILGateway == true) {%>
                                    <li><a href="inventoryReports.jsp?_type=7"><i class="icon-file"></i>Gateways</a></li>
                                        <%}%>
                                        <% if (accessObj.listBillingManager == true || accessObj.listUserSourceSettings == true
                                                    || accessObj.listPasswordPolicySettings == true || accessObj.listChannelProfileSettings == true
                                                    || accessObj.listOtpTokensSettings == true || accessObj.listCertificateSettings == true
                                                    || accessObj.listMobileTrustSettings == true || accessObj.listRadiusConfigSettings == true
                                                    || accessObj.listImageSettings == true) {%>
                                    <li><a href="inventoryReports.jsp?_type=8"><i class="icon-file"></i>System Settings</a></li>
                                        <%}%>
                                </ul>
                            </li>
                            <%}%>

                            <%if (accessObj.ListUsers == true || accessObj.listImageManagement == true
                                        || accessObj.listOOBSMSToken == true
                                        || accessObj.listOOBEMAILToken == true || accessObj.listOOBVOICEToken == true
                                        || accessObj.listOOBUSSDToken == true || accessObj.listSWMOBILEToken == true
                                        || accessObj.listSWWEBToken == true || accessObj.listSWPCToken == true
                                        || accessObj.listHWOTPToken == true || accessObj.listCerticate || accessObj.listHWPKIToken == true
                                        || accessObj.listSWPKIToken == true || accessObj.listChallengeResponse == true
                                        || accessObj.listTrustedDevice == true || accessObj.listGeoLocation == true
                                        || accessObj.listWebWatch == true || accessObj.listWebResource == true) {%>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Security & User Management<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="searchResource.jsp"><i class="icon-user"></i>Resource Management</a></li>
                                        <%

                                            if (accessObj.listWebWatch == true) {%>
                                    <!--                                    <li><a href="listSettings.jsp"><i class="icon-user"></i>Web Watch & Seal Management</a></li>-->
                                    <%}%>
                                    <%

                                        if (accessObj.listWebResource == true) {%>
                                    <li><a href="resources.jsp"><i class="icon-cog"></i>Web Resource Management</a></li>
                                        <%}

                                            if (accessObj.listWebResource == true) {%>
                                    <li><a href="PushWatch.jsp"><i class="icon-cog"></i>PushDevice Management</a></li>
                                        <%}

                                            if (accessObj.listSnmpManagement == true) {%>
                                    <!--<li><a href="SNMPaddsettings.jsp"><i class="icon-cog"></i>SNMP Alerts</a></li>-->
                                    <%}

                                        if (accessObj.listESIGNERManagement == true) {
                                    %>
                                    <!--                                    <li><a href="easyloginReport.jsp"><i class="icon-cog"></i>Easy CheckIn</a></li>
                                                                        <li class="dropdown-submenu">
                                                                            <a tabindex="-1" href="#"><i class="icon-file"></i>EPDF Signer Management</a>
                                                                            <ul class="dropdown-menu">
                                                                                <li><a href="epdfsign.jsp"><i class="icon-file"></i>New Sign Request</a></li>                                    
                                                                                <li><a href="signcategory.jsp"><i class="icon-file"></i>Document Categories</a></li>
                                                                            </ul>
                                                                        </li>-->
                                    <%}%>


                                </ul>
                            </li>
                            <%}%>

                            <%if (accessObj.listUserReport == true || accessObj.listMessageReport == true
                                        || accessObj.listHoneyTrapReport == true || accessObj.listOtpReport == true
                                        || accessObj.listCertificateReport == true || accessObj.listPkiReport == true
                                        || accessObj.listEpinSystemReport == true || accessObj.listEpinUserSpecificReport == true
                                        || accessObj.listRemoteSigningReport == true || accessObj.listtranscationReport == true
                                        || accessObj.listOtpFailureReport == true || accessObj.listsubscriptionBaseBillingReport == true
                                        || accessObj.listtranscationBaseBillingReport == true || accessObj.listMessageReport == true
                                        || accessObj.listHoneyTrapReport == true || accessObj.listRoleRports == true
                                        || accessObj.listUnitRports == true) {%>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <%if (accessObj.listUserReport == true || accessObj.listMessageReport == true
                                                || accessObj.listHoneyTrapReport == true || accessObj.listOtpReport == true
                                                || accessObj.listCertificateReport == true || accessObj.listPkiReport == true
                                                || accessObj.listEpinSystemReport == true || accessObj.listEpinUserSpecificReport == true
                                                || accessObj.listRemoteSigningReport == true || accessObj.listtranscationReport == true
                                                || accessObj.downloadAuditTrail == true || accessObj.downloadSessionAuditTrail == true || accessObj.listRoleRports == true
                                                || accessObj.listUnitRports == true) {%>
                                    <li><a href="inventoryReports.jsp?_type=1"><i class="icon-file"></i>Inventory Reports</a></li>
                                    <li><a href="inventoryReports.jsp?_type=2"><i class="icon-file"></i>Usage Reports</a></li>
                                        <%}%>
                                        <%if (accessObj.listtranscationBaseBillingReport == true || accessObj.listsubscriptionBaseBillingReport == true) {%>
                                    <!--                                    <li><a href="inventoryReports.jsp?_type=3"><i class="icon-file"></i>Billing Reports</a></li>-->
                                    <%}%>
                                    <%if (accessObj.listMessageReport == true) {%>
                                    <li><a href="inventoryReports.jsp?_type=4"><i class="icon-file"></i>Message Reports</a></li>
                                        <%}%>
                                        <%if (accessObj.listHoneyTrapReport == true) {%>
                                    <!--<li><a href="inventoryReports.jsp?_type=5"><i class="icon-file"></i>Miscellaneous Reports</a></li>-->
                                    <%}%>
                                    <%if (accessObj.downloadAuditTrail == true || accessObj.downloadSessionAuditTrail == true) {%>
                                    <li><a href="inventoryReports.jsp?_type=6"><i class="icon-file"></i>Audit Reports</a></li>
                                        <%}%>
                                </ul>
                            </li>

                            <%}%>

                            <%if (accessObj.listSystemMessage == true || accessObj.listhwOTPTokenUpload == true
                                        || accessObj.listhwPKITokenUpload == true || accessObj.listChallengeResponse == true
                                        || accessObj.connectorStatus == true || accessObj.auditIntegrity == true
                                        || accessObj.listReportSchedulerManagement == true) {%>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Administration<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <%if (accessObj.listSystemMessage == true) {%>
                                    <!--                                    <li><a href="ListSystemMessage.jsp"><i class="icon-cog"></i>System Alert(Operators)</a></li>-->
                                    <%}%>
                                    <%if (accessObj.listhwOTPTokenUpload == true || accessObj.listhwPKITokenUpload == true) {%>
                                    <li><a href="hwotpupload.jsp"><i class="icon-user"></i>Hardware Token Uploader</a></li>        
                                        <%}%>
                                    <!--                                      Challenge Response Questions List moved in configuration>> System settings-->
                                    <%if (accessObj.listChallengeResponse == true) {%>
                                    <!--                                    <li><a href="QuestionsList.jsp"><i class="icon-file"></i>Challenge Response Questions List</a></li>-->
                                    <%}%>
                                    <%if (accessObj.connectorStatus == true) {%>
                                    <!--                                    <li><a href="connectorstatuslist.jsp" ><i class="icon-cog"></i>Connectors Statuses</a></li>-->
                                    <%}%>
                                    <%if (accessObj.auditIntegrity == true) {%>
                                    <li><a href="#CheckAuditIntegrity" data-toggle="modal"><i class="icon-cog"></i>Audit Integrity</a></li>
                                        <%}
                                            if (accessObj.listReportSchedulerManagement == true) {%>
                                    <!--                                    <li><a href="scheduler.jsp"><i class="icon-cog"></i>Report Scheduler Management</a></li>-->
                                    <%}%>

                                </ul>
                            </li>
                            <%}%>
                            <%if (accessObj.listlicenseDetails == true || accessObj.changePassword == true
                                        || accessObj.changeDBPassword == true || accessObj.changeSecurityCredentials == true) {%>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">System Administration<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <%if (accessObj.viewaccessMatrix == true) {%>
                                    <li><a href="addAccessRole.jsp"><i class="icon-cog"></i>Access Role Matrix</a></li>
                                        <% } %>
                                        <%if (accessObj.kycRequests == true) {%>
                                    <!--                                    <li><a href="kycpendingrequest.jsp" ><i class="icon-cog"></i>"Know Your Customer" Certificate Request</a></li>-->
                                    <%}%>
                                    <%if (accessObj.listlicenseDetails == true) {%>
                                    <li><a href="showlicenseI.jsp"><i class="icon-user"></i>License Details</a></li>
                                        <%}%>
                                        <%if (accessObj.changePassword == true) {%>
                                    <li><a href="#ChangePassword" data-toggle="modal" ><i class="icon-cog"></i>Change My Password</a></li>
                                        <%}%>
                                        <%if (accessObj.changeDBPassword == true) {%>
                                    <li><a href="initdbform.jsp" ><i class="icon-cog"></i>Change Database Details</a></li>
                                        <%}%> 
                                        <%if (accessObj.changeSecurityCredentials == true) {%>
                                    <li><a href="reinitialize.jsp"><i class="icon-user"></i>Change Security Credentials</a></li>
                                        <%}%>

                                </ul>
                            </li>


                            <%}%>
                            <ul class="nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">OCR<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="ocrdocumentation.jsp"><i class="icon-file"></i>Template Ocr</a></li>
                                    <li><a href="templatereport.jsp"><i class="icon-file"></i>Template Details</a></li>
                                    <li><a href="TestResult.jsp"><i class="icon-file"></i>Test Template</a></li>
                                </ul> 
                            </li>
                        </ul>

                            <% if (1 == 2) { //this is for EPIN and needs to be sorted out
//                                accessObj.listEpinSystemReport = true;
//                                accessObj.viewEpinSystemReport = true;
//                                accessObj.downloadEpinSystemReport = true;
//
//                                accessObj.listEpinUserSpecificReport = true;
//                                accessObj.viewEpinUserSpecificReport = true;
//                                accessObj.downloadEpinUserSpecificReport = true;
                            %>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Requests<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="users.jsp"><i class="icon-user"></i>User & Password</a></li>
                                    <li><a href="ecopinpending.jsp">Pending Approvals <%=PendingRequests%></a></li>                                    
                                    <li><a href="ecopinfailed.jsp">Failed Deliveries <%=FailedRequests%></a></li>
                                </ul>
                            </li>

                            <% }%>



                        </ul>

                        <%if (navigationSettings == null) {
                                // if(g_subjecttemplateSettings.getProperty("axiom.change.password").equals("true")){%>
                        <div class="pull-right">
                            <ul class="nav pull-right">                                
                                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><%=completeTimeHeader%>, Hi <%=name%><b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <%if (accessObj.downloadLogs == true) {%>
                                        <li><a href="#downloadzippedlog" data-toggle="modal"><i class="icon-file"></i>Download Logs</a></li>
                                            <%}%>
                                            <%if (accessObj.downloadCleanupLogs == true) {%>
                                        <!--<li><a href="#" onclick="DownloadCleanupLog()"><i class="icon-file"></i>Download Clean-up Logs</a></li>-->
                                        <%}%>

                                        <li class="divider"></li>
                                        <li><a href="signout.jsp"><i class="icon-off"></i>Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <%}%>

                    </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>

        <div id="downloadzippedlog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">\D7</button>
                <h3 id="myModalLabel">Download Logs</h3>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <form class="form-horizontal" id="downloadzippedlogForm" name="downloadzippedlogForm">
                        <div class="span12">
                            <div class="control-group" >
                                <div class="input-prepend">
                                    <span class="add-on">Select Date</span>   
                                    <div id="downloadzippedlog1" class="input-append date" algin = "center">
                                        <input id="_logsDate" name="_logsDate"  type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <div class="modal-footer">

                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <!--<div class="span3" id="add-new-scheduler-result"></div>-->
                <button class="btn btn-primary" onclick="DownloadZippedLogByDay()" id="addnewSchedulerSubmitBut">Download logs</button>
            </div>
        </div>                                        

        <script>
            $(document).ready(function () {
                $('#left-menu').sidr({
                    name: 'sidr-left',
                    side: 'left' // By default
                });

            });
            $(document).ready(function () {
                $('#simple-menu').sidr();
            });

            $(document).ready(function () {
                $("#downloadzippedlog1").datepicker({
                    format: 'dd/mm/yyyy',
                    language: 'pt-BR'
                });
            });
        </script>