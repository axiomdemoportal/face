<%@page import="com.mollatech.axiom.nucleus.db.Operators"%>
<%@page import="com.mollatech.axiom.connector.access.controller.AccessMatrixSettings"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Audit"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.AuditManagement"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.MobileTrustManagment"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Geotrack"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.GeoLocationManagement"%>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Random" %>

<%

    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _startdate = request.getParameter("_startdate");
    String _enddate = request.getParameter("_enddate");
    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    Date startDate = null;
    if (_startdate != null && !_startdate.isEmpty()) {
        startDate = (Date) formatter.parse(_startdate);
    }
    Date endDate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        endDate = (Date) formatter.parse(_enddate);
    }
    String _type = request.getParameter("_category");
    String itemType1 = "OcrDocumentation";
    int type = Integer.parseInt(_type);
    AuditManagement auditObj = new AuditManagement();
    UserManagement user = new UserManagement();
    Audit[] arraudit;
    arraudit = auditObj.searchAuditForSuccessFail(channel.getChannelid(), itemType1, startDate, endDate,type);
    String strerr = "No Records Found";
%>

<h3>Search Results</h3>

<div class="tabbable" id="REPORT">
            <div id="licenses_data_table">
                <div id="licenses_data_table">
                    <table class="table table-striped" id="table_main">
                        <tr>
                            <td>No.</td>
                            <td>Name</td>
                            <td>Session ID</td>
                            <td>Channel ID</td>
                            <td>Result Code</td>
                            <td>Result</td>
                            <td>Dated</td>
                        </tr>
                        <%            if (arraudit != null) {
                                for (int i = 0; i < arraudit.length; i++) {
                                    SimpleDateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                                    String start = df2.format(arraudit[i].getAuditedon());
                                    AuthUser aUser = user.getUser(channel.getChannelid(), arraudit[i].getItemid());
                                    String username = "User Not Found";
                                    if (aUser != null) {
                                        username = aUser.getUserName();
                                    }
                        %>
                        <tr>
                            <td><%=(i + 1)%></td>
                            <td><%=username%></td>
                            <td><%=arraudit[i].getSessionId()%></td>
                            <td><%=arraudit[i].getChannelid()%></td>
                            <td><%=arraudit[i].getResultcode()%></td>
                            <td><%=arraudit[i].getResult()%></td>
                            <td><%=start%></td>
                        </tr>
                        <%
                            }
                        } else {%>
                        <tr>
                            <td><%=1%></td>
                            <td><%= strerr%></td>
                            <td><%= strerr%></td>
                            <td><%= strerr%></td>
                            <td><%= strerr%></td>
                            <td><%= strerr%></td>
                            <td><%= strerr%></td>
                            <%}%>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

<%--<%@include file="footer.jsp" %>--%>