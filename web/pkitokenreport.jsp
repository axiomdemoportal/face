<%@page import="java.util.Date"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.connector.user.TokenStatusDetails"%>
<%@page import="com.mollatech.axiom.nucleus.db.Certificates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Pkitokens"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/jquery.js"></script>
<script src="./assets/js/pkitokens.js"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">
<link rel="stylesheet" href="./assets/css/datepicker.css">
<script src="./assets/js/bootstrap-datepicker.js"></script>
<input type="hidden" id="_changePkiCategory" name="_changePkiCategory" value="0">
<input type="hidden" id="_changePkiStatus" name="_changePkiStatus" value="0">

<div class="container-fluid">
    <h1 class="text-success">PKI Tokens Reports</h2>
        <h3>Make Your Own Report</h3>

        <div class="row-fluid">
            <div class="span12">
                <div class="control-group form-inline">

                    From Date :   
                    <input id="pkistartdate" name="pkistartdate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth" style="width: 10%">
                    To : 
                    <input id="pkienddate" name="pkienddate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth" style="width: 10%">


                    <div class="btn-group">

                        <button class="btn"><div id="_change_pki_category_Axiom"></div></button>
                        <button class="btn" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="ChangePkiCategory(1)">Mobile Pki Token</a></li>
                            <li><a href="#" onclick="ChangePkiCategory(2)">Hardware Pki Token</a></li>

                        </ul>
                    </div>
                    <div class="input-prepend">
                        <!--<button class="btn btn-success" onclick="generatePkiTable()" type="button">Filter</button>-->
                        <%if (oprObj.getRoleid() != 1) {
                                if (accessObj != null && accessObj.viewPkiReport == true) {%>
                        <button class="btn btn-success" onclick="generatePkiTable()" id="addUserButton">Filter</button>
                        <%} else {%>
                        <button class="btn btn-success " onclick="InvalidRequestPKIToken('pkireport')" type="button">Generate Report Now</button>
                        <%}
                        } else {%>
                        <button class="btn btn-success" onclick="generatePkiTable()" id="addUserButton">Filter</button>
                        <%}%>
                    </div>
                    (For Table List only, select Status:  
                    <div class="btn-group">
                        <button class="btn"><div id="_change_Pki_Status_Axiom"></div></button>
                        <button class="btn" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="ChangePkiStateType(0)">Assigned State</a></li>
                            <li><a href="#" onclick="ChangePkiStateType(1)">Active State</a></li>

                            <li><a href="#" onclick="ChangePkiStateType(-1)">Locked State</a></li>                        
                            <!--<li><a href="#" onclick="ChangePkiStateType(-10)">Unassigned State</a></li>-->
                            <li><a href="#" onclick="ChangePkiStateType(-2)">Suspended State</a></li>
                            <li><a href="#" onclick="ChangePkiStateType(-5)">Lost State <i>(For Hardware Only)</i></a></li>
                            <li><a href="#" onclick="ChangePkiStateType(-10)">Free And Available State <i>(For Hardware Only)</i></a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
        <script>
            $(function () {
                $('#pkistartdate').datepicker({
                    format: 'dd/mm/yyyy',
                    language: 'pt-BR'
                });
            });
            $(function () {
                $('#pkienddate').datepicker({
                    format: 'dd/mm/yyyy',
                    language: 'pt-BR'

                });
            });
        </script>


        <%        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            String _channelId = channel.getChannelid();
            PKITokenManagement pObj = new PKITokenManagement();
            int limit = 50;
            Pkitokens[] oObjToken = pObj.getALLPKIToenObj(_channelId, limit);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            String strerr = "No Record Found";
        %>

        <div class="row-fluid">
            <div id="licenses_data_table">
                <h3>Recent Additions</h3>
                <table class="table table-striped" id="table_main">

                    <tr>
                        <td>No.</td>
                        <td>User Name</td>
                        <td>Mobile</td>
                        <td>Email</td>
                        <td>Serial Number</td>

                        <td>Category</td>

                        <td>Status</td>
                        <td>Last Access On</td>
                    </tr>
                    <%
                        String strcategory = "-";
                        String strsubcategory = "-";
                        String strstatus = "-";
                        if (oObjToken != null) {
                            for (int i = 0; i < oObjToken.length; i++) {

                                String uID = oObjToken[i].getUserid();

                                UserManagement uObj = new UserManagement();
                                AuthUser[] Users = uObj.SearchUsersByID(sessionId, _channelId, uID);

                                if (oObjToken[i].getCategory() == OTPTokenManagement.SOFTWARE_TOKEN) {
                                    strcategory = "S/W Token";

                                    if (oObjToken[i].getStatus() == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                                        strstatus = "Active";
                                    } else if (oObjToken[i].getStatus() == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
                                        strstatus = "Locked";
                                    } else if (oObjToken[i].getStatus() == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                                        strstatus = "Assign";
                                    } else if (oObjToken[i].getStatus() == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                                        strstatus = "Un-Assign";
                                    } else if (oObjToken[i].getStatus() == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
                                        strstatus = "Suspended";
                                    }
                                } else if (oObjToken[i].getCategory() == OTPTokenManagement.HARDWARE_TOKEN) {
                                    strcategory = "H/W Token";

                                    //status
                                    if (oObjToken[i].getStatus() == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                                        strstatus = "Active";
                                    } else if (oObjToken[i].getStatus() == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
                                        strstatus = "Locked";
                                    } else if (oObjToken[i].getStatus() == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                                        strstatus = "Assign";
                                    } else if (oObjToken[i].getStatus() == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                                        strstatus = "Un-Assign";
                                    } else if (oObjToken[i].getStatus() == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
                                        strstatus = "Suspended";
                                    } else if (oObjToken[i].getStatus() == OTPTokenManagement.TOKEN_STATUS_LOST) {
                                        strstatus = "Lost";
                                    } else if (oObjToken[i].getStatus() == OTPTokenManagement.TOKEN_STATUS_FREE) {
                                        strstatus = "Free";
                                    }

                                } else if (oObjToken[i].getCategory() == OTPTokenManagement.OOB_TOKEN) {
                                    strcategory = "OOB Token";

                                    if (oObjToken[i].getStatus() == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                                        strstatus = "Active";
                                    } else if (oObjToken[i].getStatus() == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
                                        strstatus = "Locked";
                                    } else if (oObjToken[i].getStatus() == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                                        strstatus = "Assign";
                                    } else if (oObjToken[i].getStatus() == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                                        strstatus = "Un-Assign";
                                    } else if (oObjToken[i].getStatus() == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
                                        strstatus = "Suspended";
                                    }

                                }

                    %>

                    <tr>
                        <%if (Users != null) {%>
                        <td><%=(i + 1)%></td>
                        <td><%=Users[0].getUserName()%></td>
                        <td><%=Users[0].getPhoneNo()%></td>
                        <td><%=Users[0].getEmail()%></td>
                        <td><%= oObjToken[i].getSrno()%></td>
                        <td><%=strcategory%></td>
                        <td><%=strstatus%></td>
                        <td><%= sdf.format(oObjToken[i].getLastaccessdatetime())%></td>
                        <%}%>
                    </tr>


                    <%}
                    } else {%>
                    <tr>
                        <td><%=1%></td>
                        <td><%= strerr%></td>
                        <td><%= strerr%></td>
                        <td><%= strerr%></td>
                        <td><%= strerr%></td>
                        <td><%= strerr%></td>
                        <td><%= strerr%></td>
                        <td><%= strerr%></td>

                    </tr>
                    <%}%>


                </table>


            </div>
        </div>
        <script type="text/javascript">
            ChangePkiCategory(1);
            ChangePkiStateType(0);
            //        generateOtpTable()

        </script>
</div>
<%@include file="footer.jsp" %>