<%@page import="com.mollatech.axiom.nucleus.db.connector.management.MobileTrustManagment"%>
<%@page import="com.mollatech.axiom.nucleus.settings.MobileTrustSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Geotrack"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.GeoLocationManagement"%>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Random" %>
<%@include file="header.jsp" %>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="./assets/js/json_sans_eval.js"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script src="./assets/js/geotracking.js"></script>
<%
    int OTP=6;
//    int REGISTER = 5;
//    int LOGIN = 2;
//    int TRANSACTION = 3;
//    int CHANGEINPROFILE = 4;
    int GENERATEAUTHENTICATIONPACKAGE = 5;
    
     int REGISTER = 1;
    int LOGIN = 2;
    int TRANSACTION = 3;
    int CHANGEINPROFILE = 4;
    
    
    
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    String userID = request.getParameter("_userID");
    String duration = request.getParameter("_duration");
    UserManagement uObj = new UserManagement();
    GeoLocationManagement geoObj = new GeoLocationManagement();
    Geotrack Tracks[] = null;
    Tracks = geoObj.getTracks(sessionId, channel.getChannelid(), userID, duration);
    AuthUser[] Users = uObj.SearchUsersByID(sessionId, channel.getChannelid(), userID);
    String strerr = "No Records Found";
%>
<div class="container-fluid">
    <h3>Transaction Places for User <i>"<%=Users[0].getUserName()%>"</i></h3>
    <div id="map_transaction" style="width:100%; height: 400px"></div>
</div>    
     <hr>
<div class="container-fluid">
    
    <div class="row-fluid">
                <div class="span6">
                    <div class="control-group">                        
                        <div class="span1">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="geoTrackReport('<%=Users[0].userId%>','<%=duration%>')" >
                                    <i class="icon-white icon-chevron-down"></i> CSV</a>
                            </div>
                        </div>
                        <div class="span1">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="geoTrackpdf('<%=Users[0].userId%>','<%=duration%>')" >
                                    <i class="icon-white icon-chevron-down"></i> PDF</a>
                            </div>
                        </div>
                         <div class="span1">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="geoTrackTxt('<%=Users[0].userId%>','<%=duration%>')" >
                                    <i class="icon-white icon-chevron-down"></i> TEXT</a>
                            </div>
                        </div>

                    </div>
                </div>
    </div>
    <div id="licenses_data_table">
        <input type="hidden" id="_tracking" name="_tracking"  >
        <table class="table table-striped" id="table_main">
            <tr>
                <td>No.</td>
                <td>IP Address</td>
                <td>Lattitude</td>
                <td>Longitude</td>
                <td>City</td>
                <td>State</td>
                <td>Country</td>
                <td>Zip code</td>
                <td>Type</td>
                <td>Dated</td>
            </tr>
            <%
                if (Tracks != null) {
                    for (int i = 0; i < Tracks.length; i++) {
                        SimpleDateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                        String start = df2.format(Tracks[i].getExecutedOn());
                        String strtype = "";
                        if (Tracks[i].getTxtype() == REGISTER) {
                            strtype = "Register";
                        } else if (Tracks[i].getTxtype() == LOGIN) {
                            strtype = "Login";
                        } else if (Tracks[i].getTxtype() == TRANSACTION) {
                            strtype = "Transaction";
                        } else if (Tracks[i].getTxtype() == CHANGEINPROFILE) {
                            strtype = "Change Profile";
                        } else if (Tracks[i].getTxtype() == MobileTrustManagment.INVALIDIMAGE) {
                           strtype = "Honey Trap";
                        }else if (Tracks[i].getTxtype() == GENERATEAUTHENTICATIONPACKAGE) {
                           strtype = "Login(AuthPackage)";
                        }
                        
            %>
            <tr>
                <td><%=(i + 1)%></td>
                <td><%=Tracks[i].getIp()%></td>
                <td><%=Tracks[i].getLattitude()%></td>
                <td><%=Tracks[i].getLongitude()%></td>
                <td><%=Tracks[i].getCity()%></td>
                <td><%=Tracks[i].getState()%></td>
                <td><%=Tracks[i].getCountry()%></td>
                <td><%=Tracks[i].getZipcode()%></td>
                <td><%=strtype%></td>
                <td><%=start%></td>
            </tr>
            <%
                }
            } else {%>
            <tr>
                <td><%=1%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <%}%>
            </tr>
        </table>
    </div>
           <div class="row-fluid">
                <div class="span6">
                    <div class="control-group">                        
                        <div class="span1">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="geoTrackReport('<%=Users[0].userId%>','<%=duration%>')" >
                                    <i class="icon-white icon-chevron-down"></i> CSV</a>
                            </div>
                        </div>
                        <div class="span1">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="geoTrackpdf('<%=Users[0].userId%>','<%=duration%>')" >
                                    <i class="icon-white icon-chevron-down"></i> PDF</a>
                            </div>
                        </div>
                          <div class="span1">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="geoTrackTxt('<%=Users[0].userId%>','<%=duration%>')" >
                                    <i class="icon-white icon-chevron-down"></i> TEXT</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div> 
            
</div>           
<script language="javascript">
    TransactionMap('<%=userID%>', '<%=duration%>');
</script>
<%@include file="footer.jsp" %>