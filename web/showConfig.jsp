
<html lang="en">
    <head>
        <link rel="shortcut icon"
              href="assets/siteIcon.ico"/>
        <!--<link rel="shortcut icon" href="assets/siteIcon.ico"/>-->
        <meta charset="utf-8">
        <meta http-equiv="X-Frame-Options" content="deny">
        <title> Update Dbsetting Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta http-equiv="X-Frame-Options" content="deny">
        <!-- Le styles -->
        <link href="./assets/css/bootstrap.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 40px;
                padding-bottom: 40px;
                background-color: #f5f5f5;
            }

            .form-signin {
                max-width: 300px;
                padding: 19px 29px 29px;
                margin: 0 auto 20px;
                background-color: #fff;
                border: 1px solid #e5e5e5;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
            }
            .form-signin .form-signin-heading,
            .form-signin .checkbox {
                margin-bottom: 10px;
            }
            .form-signin input[type="text"],
            .form-signin input[type="password"] {
                font-size: 16px;
                height: auto;
                margin-bottom: 15px;
                padding: 7px 9px;
            }

        </style>
        <link href="./assets/css/bootstrap-responsive.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="./assets/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="./assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="./assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="./assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="./assets/ico/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="./assets/shield_256.png">
    </head>

    <body>

        <div class="container">

            <div class="hero-unit" align="center">
                <h1>UPDATE DB SETTING FILE </h1>
                
                <br>
                <form class="form-signin" method="POST" action="#" id="loginForm" name="loginForm">
                    <p>
                    <div class="text" align="center" id="login-result" ></div>
                   
                    <input type="text" id="_name"  align="right" name="_name"  class="input-block-level" placeholder="Enter Path">
                   
                    <input type="password" id="_passwd" align="right" name="_passwd" class="input-block-level" placeholder="Enter password">  <br>
                    <button class="btn btn-large btn-primary" onclick="DbsettingLogin()" type="button" >Log in</button>
                    <button class="btn btn-large btn-primary" onclick="RefreshLogoutDBsetting()" type="button" >Cancel</button>
                   
                    
                </form>
            </div>
        </div> <!-- /container -->

       
       

        <footer>
           
        </footer>

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="./assets/js/jquery.js"></script>
        <script src="./assets/js/bootstrap-transition.js"></script>
        <script src="./assets/js/bootstrap-alert.js"></script>
        <script src="./assets/js/bootstrap-modal.js"></script>
        <script src="./assets/js/bootstrap-dropdown.js"></script>
        <script src="./assets/js/bootstrap-scrollspy.js"></script>
        <script src="./assets/js/bootstrap-tab.js"></script>
        <script src="./assets/js/bootstrap-tooltip.js"></script>
        <script src="./assets/js/bootstrap-popover.js"></script>
        <script src="./assets/js/bootstrap-button.js"></script>
        <script src="./assets/js/bootstrap-collapse.js"></script>
        <script src="./assets/js/bootstrap-carousel.js"></script>
        <script src="./assets/js/bootstrap-typeahead.js"></script>
        <script src="./assets/js/login.js"></script>

        <link rel="stylesheet" href="./assets/css/datepicker.css">
        <link rel="stylesheet" href="./assets/css/jquery.sidr.dark.css">

        <script src="./assets/js/json_sans_eval.js"></script>
        <script src="./assets/js/bootbox.min.js"></script>





        <script language="javascript">
//                        TellTimezone();
        </script>
    </body>
</html>
