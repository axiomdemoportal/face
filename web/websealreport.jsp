<%@page import="com.mollatech.axiom.nucleus.db.Monitorsettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.MonitorSettingsManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">
<link rel="stylesheet" href="./assets/css/datepicker.css">
<script src="./assets/js/bootstrap-datepicker.js"></script>
<script src="./assets/js/webseal.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<div class="container-fluid">
    <h2 class="text-success">Web seal Report</h2>   
    <div class="row-fluid">
        <div class="span12">                
            <div class="form-group input-append">
                <!--                <div class="input-prepend">-->
                <!--                    <div class="well">-->
                <span class="add-on">From:</span>   
                <div id="datetimepicker1" class="input-append date">
                    <input id="startdate" name="startdate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth">
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
                <!--                    </div>-->
            </div>
            <div class="input-append">
                <!--                    <div class="well">-->
                <span class="add-on">to:</span>   
                <div id="datetimepicker2" class="input-append date">
                    <input id="enddate" name="enddate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth">
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
                <span style="margin-left: 10px">
                    <select class="selectpicker " name="_UnitId" id="_UnitId">

                        <%   Channels channel = (Channels) session.getAttribute("_apSChannelDetails");
                            String sessionId = (String) session.getAttribute("_apSessionID");
                            MonitorSettingsManagement unitObj = new MonitorSettingsManagement();
                            Monitorsettings[] un = unitObj.getMonitorTrackingByNameURL(channel.getChannelid());
                            if (un != null) {
                                for (int j = 0; j < un.length; j++) {

                        %>
                        <option  value="<%=un[j].getMonitorId()%>"><%=un[j].getMonitorName()%></option>       

                        <%}
                        } else {%>
                        <option  value="0">No Units Found</option>   
                        <%}%>
                    </select>
                </span>
                <div class="input-append">                
                    <span style="margin-left: 10px"><button class="btn btn-success" onclick="generateWebSealTable()" type="button" id="generatereportButton">Search</button></span>
                    <!--                    <span style="margin-left: 10px"><button class="btn btn-success" id="refreshButton" onclick="RefreshAuthReport()" type="button" >Refresh Report</button></span>-->
                </div>   
                <!--                    </div>-->

            </div>
            <div class="input-append">

            </div>

        </div>
    </div>

    <!-- New added -->
    <div class="tabbable" id="REPORT">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#otpcharts" data-toggle="tab">Charts</a></li>
            <li><a href="#otpreport" data-toggle="tab">Tabular List</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="otpcharts">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="control-group">
                            <div class="span12">
                                <div id="graphDount" ></div>
                            </div>
                            <!--                            <div  class="span10">
                                                            <div id="graphBar"></div>
                            
                                                        </div>-->
                        </div>
                    </div>
                    <div class="span12">
                        <div class="control-group">
                            <div id="graphBar"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane" id="otpreport">  
                <div id="webseal_table_main">
                </div>
            </div>
        </div>
    </div>

</div>
<!--    <div id="auth_table_main" style="margin-top: 50px"></div>-->
<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datepicker({
            format: 'dd/mm/yyyy',
            language: 'pt-BR'
        });
    });
    $(function () {
        $('#datetimepicker2').datepicker({
            format: 'dd/mm/yyyy',
            language: 'pt-BR'

        });
    });

</script>
<script>
    document.getElementById("REPORT").style.display = 'none';
    //document.getElementById("refreshButton").style.display = 'none';
</script>
<%@include file="footer.jsp" %>