<%@page import="com.mollatech.axiom.nucleus.db.connector.management.LocationManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Countrylist"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.Usergroups"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.GroupManagement"%>
<%
    String sessionId = (String) session.getAttribute("_apSessionID");
    String restrictLevel = "";
    String allow = "Disallow";
    int CountryCod = -1;
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _unitName = request.getParameter("_unitName");
    GroupManagement management = new GroupManagement();
    Usergroups usergroups = management.getGroupByGroupName(sessionId, channel.getChannelid(), _unitName);

    if (usergroups != null) {
        if (!usergroups.getCountry().equals("")) {
            CountryCod = Integer.parseInt(usergroups.getCountry());
        }
        if (usergroups.getRestrictionLevel() == 1) {
            restrictLevel = "No Restriction";
        } else if (usergroups.getRestrictionLevel() == 2) {
            restrictLevel = "Country";
        } else if (usergroups.getRestrictionLevel() == 3) {
            restrictLevel = "State";
        } else if (usergroups.getRestrictionLevel() == 4) {
            restrictLevel = "City";
        } else if (usergroups.getRestrictionLevel() == 5) {
            restrictLevel = "Pincode";
        }
        if (usergroups.getIsEnabled() == 1) {
            allow = "Allow";
        }
%>

<div id="editGroup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>-->
        <h3 id="myModalLabel">Edit Group</h3>
    </div>
    <div class="modal-body">

        <div class="row-fluid">
            <form class="form-horizontal" id="editGroupForm" name="editGroupForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Name</label>
                        <div class="controls">
                            <input type="text" id="_unitNameE" name="_unitNameE" value="<%= _unitName%>" class="input-xlarge" readonly>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label"  for="username">Status</label>
                        <div class="controls">
                            <select class="span4" name="_unitStatusE" id="_unitStatusE">
                                <option value="0">Suspended</option>
                                <option value="1" selected>Active</option>

                            </select>
                        </div>
                    </div>

                    <div id="monitors">
                        <div class="control-group">
                            <label class="control-label"  for="username">Restriction Level</label>
                            <div class="controls">
                                <select name="_selectedTypeE" id="_selectedTypeE" class="span6">
                                    <option value='No Restriction' id="No Restriction"> No Restriction </option>
                                    <option value='Country' id="Country" > Country </option>
                                    <option value='State' id="State"> State</option>
                                    <option value='City' id="City"> City </option>
                                    <option value='Pincode' id="Pincode"> Pincode </option>

                                </select>
                                <script>
                                    var r = document.getElementById('_selectedTypeE');
                                    r.options['<%=restrictLevel%>'].selected = 'true';

                                </script>
                                <%if (usergroups.getRestrictionLevel() == 1) {%>
                                <script>
                                    document.getElementById("countryE").style.display = 'none';
                                    document.getElementById("_StateNameE").style.display = 'none';
                                    document.getElementById("_CityE").style.display = 'none';
                                    document.getElementById("_PinE").style.display = 'none';</script>
                                    <%} else if (usergroups.getRestrictionLevel() == 2) { %>
                                <script>document.getElementById("countryE").style.display = 'block';
                                    document.getElementById("_StateNameE").style.display = 'none';
                                    document.getElementById("_CityE").style.display = 'none';
                                    document.getElementById("_PinE").style.display = 'none';
                                </script>
                                <%} else if (usergroups.getRestrictionLevel() == 3) {%>
                                <script>document.getElementById("countryE").style.display = 'block';
                                    document.getElementById("_StateNameE").style.display = 'block';
                                    document.getElementById("_CityE").style.display = 'none';
                                    document.getElementById("_PinE").style.display = 'none';
                                </script>
                                <%} else if (usergroups.getRestrictionLevel() == 4) {%>
                                <script>document.getElementById("countryE").style.display = 'block';
                                    document.getElementById("_StateNameE").style.display = 'block';
                                    document.getElementById("_CityE").style.display = 'block';
                                    document.getElementById("_PinE").style.display = 'none';</script>
                                    <%} else if (usergroups.getRestrictionLevel() == 5) {%>
                                <script>document.getElementById("countryE").style.display = 'block';
                                    document.getElementById("_StateNameE").style.display = 'block';
                                    document.getElementById("_PinE").style.display = 'block';
                                    document.getElementById("_CityE").style.display = 'none';</script>
                                    <%}%>
                            </div>
                        </div>
                    </div>
<!--                    <div id="CheckThrough">
                        <div class="control-group">
                            <label class="control-label"  for="username">Check Through</label>
                            <div class="controls">
                                <select name="_selectedCheckE" id="_selectedCheckE" class="span4">
                                    <option value='Allow' id="Allow"> Allow </option>
                                    <option value='Disallow' id="Disallow" > Dis-Allow </option>
                                </select>
                            </div>
                        </div>
                    </div>-->
                    <div class="control-group" id="countryE">
                        <label class="control-label"  for="username">Home Country</label>
                        <div class="controls">
                            <select class="span10" name="_CountryNameE" id="_CountryNameE">
                                <%//getAll conotries here 
                                    LocationManagement lManagement = new LocationManagement();
                                    int countryCode = -1;
                                    Countrylist[] countryList = lManagement.getAllCountries();
                                    if (countryList != null) {
                                        for (int j = 0; j < countryList.length; j++) {
                                            String strcode = countryList[j].getCountyName();
                                            countryCode = countryList[j].getCountryid();
                                            if (CountryCod == countryCode) {%>
                                <option value="<%=countryCode%>" selected ><%=strcode%></option>
                                <%} else {%>
                                <option value="<%=countryCode%>" ><%=strcode%></option>
                                <%}

                                        }
                                    }%>
                            </select>

                        </div>
                    </div>
                    <% if (usergroups.getRestrictionLevel() >= 3) {%>
                    <script>
                        var s = './stateList.jsp?_countryId=<%=CountryCod%>&stateString=<%=usergroups.getState()%>';
                        $.ajax({
                            type: 'GET',
                            url: s,
                            success: function (data) {
                                $('#_StateNameE').html(data);
                            }});
                    </script>
                    <%}%>
                    <div name="_StateNameE" id="_StateNameE">                           
                    </div>
                    <div id="_CityE">
                        <div class="control-group">
                            <label class="control-label"  for="username">Cities </label>
                            <div class="controls">
                                <textarea name="_cityNameE" id="_cityNameE" class="input-large"  >
                                    <%
                                        String[] cities = usergroups.getListOfLocationOrPincodes().split(",");
                                        for (int i = 0; i < cities.length; i++) {
                                    %> <%= cities[i]%>,
                                    <%}%>
                                </textarea>

                            </div>
                        </div>
                    </div>
                    <div id="_PinE">
                        <div class="control-group">
                            <label class="control-label"  for="username">Pin codes </label>
                            <div class="controls">
                                <textarea name="_PinCodeE" id="_PinCodeE" class="span10"  >
                                    <%
                                        String[] pins = usergroups.getListOfLocationOrPincodes().split(",");
                                        for (int i = 0; i < pins.length; i++) {
                                    %> <%= pins[i]%>,
                                    <%}%>
                                </textarea>

                            </div>
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
        <%}%>
    </div>
    <div class="modal-footer">
        <div id="add-new-unit-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <!--<div class="span3" id="add-new-unit-result"></div>-->
        <button class="btn btn-primary" onclick="editGroup()" id="editnewUnitSubmitBut">Save Changes</button>
    </div>
</div>
<script>
    var k = document.getElementById('_unitStatusE');
    k.options['<%=usergroups.getStatus()%>'].selected = 'true';

//    var c = document.getElementById('_selectedCheckE');
//    c=1;
//    c.options['<%=allow%>'].selected = 'true';
    $(document).ready(function () {
        $("#_cityNameE").select2({
            tags: [],
            dropdownCss: {display: 'none'},
            tokenSeparators: [","]
        });
    });
    $(document).ready(function () {
        $("#_PinCodeE").select2({
            tags: [],
            dropdownCss: {display: 'none'},
            tokenSeparators: [","]
        });
    });
    $(document).ready(function () {
        //  var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
        $('#_CountryNameE').change(function () {
            var selectedValue = $(this).val();
           // alert(selectedValue);
            var s = './stateList.jsp?_countryId=' + selectedValue;
            $.ajax({
                type: 'GET',
                url: s,
                success: function (data) {
                    $('#_StateNameE').html(data);
                }
            });
        });
    });
    $(document).ready(function () {
        $('#_selectedTypeE').change(function () {
            var selectedValue = $(this).val();
            if (selectedValue === 'No Restriction')
            {
                document.getElementById("countryE").style.display = 'none';
                document.getElementById("_StateNameE").style.display = 'none';
                document.getElementById("_CityE").style.display = 'none';
                document.getElementById("_PinE").style.display = 'none';
            }
            else if (selectedValue === 'Country')
            {
                document.getElementById("countryE").style.display = 'block';
                document.getElementById("_StateNameE").style.display = 'none';
                document.getElementById("_CityE").style.display = 'none';
                document.getElementById("_PinE").style.display = 'none';
            }
            else if (selectedValue === 'State')
            {
                document.getElementById("countryE").style.display = 'block';
                document.getElementById("_StateNameE").style.display = 'block';
                document.getElementById("_CityE").style.display = 'none';
                document.getElementById("_PinE").style.display = 'none';
            }
            else if (selectedValue === 'City')
            {
                document.getElementById("countryE").style.display = 'block';
                document.getElementById("_StateNameE").style.display = 'block';
                document.getElementById("_CityE").style.display = 'block';
                document.getElementById("_PinE").style.display = 'none';
            }
            else if (selectedValue === 'Pincode')
            {
                document.getElementById("countryE").style.display = 'block';
                document.getElementById("_StateNameE").style.display = 'block';
                document.getElementById("_PinE").style.display = 'block';
                document.getElementById("_CityE").style.display = 'none';
            }

        });
    });
</script>