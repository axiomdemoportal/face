<%-- 
    Document   : PushWatch
    Created on : 4 Jan, 2017, 11:09:40 AM
    Author     : mayuri
--%>
<%@page import="com.mollatech.axiom.nucleus.db.Registerdevicepush"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.PushNotificationDeviceManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.GroupManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Countrylist"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.LocationManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserGroupsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Usergroups"%>
<%@page import="com.mollatech.axiom.nucleus.settings.SendNotification"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserGroupsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Usergroups"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/ajaxfileupload.js"></script>
<script src="./assets/js/bootstrap-fileupload.js"></script> 
<link href="./assets/css/bootstrap.css" rel="stylesheet">
<link href="./assets/css/bootstrap-fileupload.css" rel="stylesheet">
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<script src="./assets/js/pushgateway.js"></script>

<%    String _sessionID = (String) session.getAttribute("_apSessionID");

    SessionManagement smObj = new SessionManagement();
    TemplateManagement tempObj = new TemplateManagement();
//     OperatorsManagement s = new OperatorsManagement();
    int iStatus = smObj.GetSessionStatus(_sessionID);
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    OperatorsManagement oMngt = new OperatorsManagement();
    OTPTokenManagement otpMngt = new OTPTokenManagement(channel.getChannelid());
    SettingsManagement sMngt = new SettingsManagement();
    SendNotification send = new SendNotification();
    Usergroups[] groupsObj = null;
    if (iStatus == 1) { //active
        smObj.UpdateSession(_sessionID);
        UserGroupsManagement cmObj = new UserGroupsManagement();
        groupsObj = cmObj.ListGroups(_sessionID, channel.getChannelid());
        cmObj = null;
    }
    
        PushNotificationDeviceManagement pushNotificationDeviceManagement = new PushNotificationDeviceManagement();
            Registerdevicepush[] regAndroid = null;
            Registerdevicepush[] regAndIOS = null;
            Registerdevicepush[] regPush = null;
            String androidDevCount="0";
            String iodDeviceCount="0";
            String webDeviceCount="0";
            Usergroups[] userGroup=null;
            try {
                regAndroid = pushNotificationDeviceManagement.GetRegisterPushDeviceByType(channel.getChannelid(), SendNotification.ANDROIDPUSH);
                regAndIOS = pushNotificationDeviceManagement.GetRegisterPushDeviceByType(channel.getChannelid(), SendNotification.IPHONEPUSH);
                regPush = pushNotificationDeviceManagement.GetRegisterPushDeviceByType(channel.getChannelid(), SendNotification.WEBPUSH);
                UserGroupsManagement userGroups = new UserGroupsManagement();
               userGroup = userGroups.getAllGroup(_sessionID, channel.getChannelid());
              
             if(regAndroid!=null)
             {
             androidDevCount=""+regAndroid.length;
             }
             if(regAndIOS!=null)
             {
              iodDeviceCount=""+regAndIOS.length;
             }
              if(regPush!=null)
             {
              webDeviceCount=""+regPush.length;
             }
             
            }catch(Exception ex)
            {
            ex.addSuppressed(ex);
            }






%>

<div style="margin-left: 40px;" class="container-fluid" >


    <h1 class="text-success">Push Device Management</h1>
    <div class="tabbable">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#import" data-toggle="tab">Import Device Token</a></li>
            <li><a href="#customize" data-toggle="tab">Send Customize Message</a></li>
        <li><a href="#export" data-toggle="tab">Export Device Tokens</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="import">
                <div class="row-fluid">
                    <div id="legend">
                        <legend class="">Import Device Token</legend>
                    </div>

                    <div class="input-append">
                        <div>
                            <label class="control-label"  for="username">Select Push Type</label>
                            <select span="2" id="_type">
                                <option value="" disabled selected hidden>Select Push Type</option>
                                <option value="18">Android</option>
                                <option value="19">IOS</option>
                                <option value="33">Web Push</option>
                            </select>
                        </div>
                        <br><br>
                        <label class="control-label"  for="username">Select Group</label>
                        <div>
                            <select span="2" id="_groupId" name="_groupId">
                                <option value="0"  selected >All Groups</option>
                                
                                <% for (int idx = 0; idx < groupsObj.length; idx++) {
                                        Object GroupNmae = groupsObj[idx].getGroupname();
                                        
                                %>
                               <option value=<%=groupsObj[idx].getGroupid()%> id=<%=idx%>> <%=GroupNmae%> </option>
                                <%}%>
                            </select>
                        </div>
                            <br/>
                          <div class="control-group">
                <label class="control-label"  for="username">Upload file with following format as .txt extension </label>      
                <br/> 
                <textarea class="span7" rows="2" placeholder="{useridentity}={devicetoken}" readonly="true"></textarea>

                          </div>
                            
                        <br>
                       
                        <div class="control-group">
                            <div class="controls-row" id="licenses">
                                <div class="row-fluid">
                                    <!--<form class="form-horizontal" id="uploadXMLFormEAD" name="uploadXMLFormEAD">-->
                                    <fieldset>

                                        <div class="control-group">
                                            <label class="control-label"  for="username">Select File:</label>                                    
                                            <div class="controls fileupload fileupload-new" data-provides="fileupload">
                                                <div class="input-append">
                                                    <div class="uneditable-input span4"><i class="icon-file fileupload-exists"></i> 
                                                        <span class="fileupload-preview"></span>
                                                    </div>
                                                    <span class="btn btn-file" >
                                                        <span class="fileupload-new">Select file</span>
                                                        <span class="fileupload-exists">Change</span>
                                                        <input type="file" id="fileXMLToUploadEADpush" name="fileXMLToUploadEADpush"/>
                                                    </span>
                                                    <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                </div>
                                            </div>

                                        </div>

                                        <input type="hidden" id="_perference" name="_perference" value="2">

                                        <!--                        <div class="control-group">
                                                                    <label class="control-label"  for="username">Enter Password</label>                                    
                                                                    <div class="controls">
                                                                        <input class="span3" type="password" id="_certpassowrd" name="_certpassowrd" placeholder="Enter Password To Import " >
                                                                    </div>
                                                                </div>-->





                                        <div class="control-group">
                                            <div class="controls">
                                                <div id="save-PushMsg-gateway-ternory-result"></div>
                                                <button class="btn btn-success" id="buttonUploadEADPush"  onclick="PushwatchFileUpload()" type="button">Upload Now>></button>&nbsp;&nbsp;&nbsp;&nbsp; 
                                                &nbsp;&nbsp;&nbsp;&nbsp;   
                                                <button class="btn btn-primary" onclick="PushwatchSaveSettings()" id="savePushWatchSecondaryBut" type="button">Import>></button>

                                                <div id="uploadStatus"></div>
                                            </div>



                                        </div>
                                </div>
                                </fieldset>
                                <!--</form>-->
                            </div>
                            <!-- Submit -->
                        </div>

                    </div>
                </div>


            </div>
            <div class="tab-pane" id="customize">
                <div class="row-fluid">
                   
                        <div id="legend">
                            <legend class="">Send Customize Message</legend>
                        </div>

                        <div class="input-append">
                             <div>
                                <label  for="devices">Select Devices</label> 
                            </div>
                            <div>
                                <select span="2" id="_Pushtype">
                                    <option value="0"  selected >All Devices</option>
                                    <option value="18">Android</option>
                                    <option value="19">IOS</option>
                                    <option value="33">Web Push</option>
                                </select>
                            </div>
                            <br><br>
                            <div>
                                <select span="2" id="_groupname">
                                    <option value="0"  selected >All Groups </option>
                                    <% for (int idx = 0; idx < groupsObj.length; idx++) {
                                            Object GroupNmae = groupsObj[idx].getGroupname();
                                            Object groupId = groupsObj[idx].getGroupid();
                                    %>
                                 <option value=<%=groupId%> id=<%=idx%>> <%=GroupNmae%> </option>
                                    <%}%>
                                </select>
                            </div>
                            <br>
                            <br>

                            <div>
                                <label  for="username">Enter Message:</label> 
                            </div>
                        </div>
                                <div>
                                <input type="text" id="_Pushmsg" name="_Pushmsg" placeholder="Enter Message:" class="input-xxlarge">

                            </div>
                        </div>
                        <br><br>
                        <div class="control-group">
                            <div id="test-PushMsg-primary-configuration-result"></div>
                            <button class="btn btn-success" id="buttonSendPush"  onclick="sendCustomizePush()" type="button">Send Now>></button>
                            <div id="uploadStatus"></div>
                        </div>

                        </fieldset>
                </div>
                                <div class="tab-pane" id="export">
                     <div class="row-fluid">
                        <div id="legend">
                            <legend class="">Export Device Tokens</legend>
                        </div>
                     <div class="input-append">
                            <div>
                                <label  for="devices">Select Devices</label> 
                            </div>
                              <div>
                                <select span="2" id="_Pushtypeexp">
                                    <option value="0"  selected >All Devices</option>
                                    <option value="18">Android</option>
                                    <option value="19">IOS</option>
                                    <option value="33">Web Push</option>
                                </select>
                                  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                 <select span="2" id="_groupnameExp" name="_groupnameExp">
                                <option value=""  selected >All Groups</option>
                                    <% for (int idx = 0; idx < groupsObj.length; idx++) {
                                       Object GroupNmae = groupsObj[idx].getGroupname();
                                    %>
                                    <option value='<%=groupsObj[idx].getGroupid()%>'> <%=GroupNmae%> </option>
                                    <%}%>
                                </select>
                              <button class="btn btn-success" id="buttonSendPush"  onclick="exportDeviceToken()" type="button">Export>></button>
                            </div>
                               
                                
                        
<!--                         <div>
                              <div>
                                <label  for="deviceCount">Device Token Count By Type</label> 
                              </div>
                             <div id="deviceTokenByType" name='deviceTokenByType'>
                                 <table id='deviceTokenTable' name='deviceTokenTable'>
                                     
                                     
                                 </table>
                             </div>
                             
                         </div>-->
                         
<!--                            <br><br>
                            <div>
                                
                            </div>-->
                           <br/>
                        </div>
                                 <div id="deviceTokenByType" name='deviceTokenByType'>
                                     Device Count<br/>
                                             <table class="table table-striped" id="table_main"><tr>
                                                            <td>
                                                    Android 
                                                     </td>
                                                            <td>
                                                         IOS
                                                     </td>
                                                            <td>
                                                         WebPush
                                                     </td>
                                         </tr>
                                         <tr>
                                             
                                             
                                             <td><%=androidDevCount%></td>
                                               <td><%=iodDeviceCount%></td>
                                              <td><%=webDeviceCount%></td>
                                                  
                                                   
                                             
                                         </tr>
                                             </table>
                                </div>
                                   <div id="deviceTokenByGroup" name='deviceTokenByGroup'>
                                     Device Count By Group<br/>
                                             <table class="table table-striped" id="table_main"><tr>
                                                        <td>
                                                    GroupName 
                                                     </td>
                                                     <td>
                                                    DeviceCount
                                                     </td>
                                                        
                                         </tr>
                                    <%
                                    
                                      for (int i = 0; i < userGroup.length; i++) {
                    regPush = pushNotificationDeviceManagement.getRegisterPushDeviceByGroup(channel.getChannelid(), userGroup[i].getGroupid());
                    if (regPush != null) {%>
                        <tr>
                                                        <td>
                                                    <%=userGroup[i].getGroupname()%> 
                                                     </td>
                                                     <td>
                                                    <%=regPush.length%>
                                                     </td>
                                                        
                                         </tr>
                                  <% } else {%>
                       
                             <tr>
                                                        <td>
                                                    <%=userGroup[i].getGroupname()%> 
                                                     </td>
                                                     <td>
                                                    0
                                                     </td>
                                                        
                                         </tr>
                   <%}%>
                                   <%}%>
                                         
                                             </table>
                                </div>
                                              
                                
                        </div>
                        <br><br>
                        <div class="control-group">
                            <div id="test-PushMsg-primary-configuration-result"></div>
<!--                            <button class="btn btn-success" id="buttonSendPush"  onclick="exportDeviceToken()" type="button">Export>></button>-->
<!--                           <button class="btn btn-success" id="buttonSendPush"  onclick="getDeviceCounts()" type="button">Show Counts>></button>-->
                            <div id="uploadStatus"></div>
                        </div>

                        </fieldset>
                </div>   
                                
                                
            </div>
        </div>
        </form>
        <!--</form>-->
    </div>
    <!-- Submit -->
</div>

</div>
</div>


</div>
</div>





<%@include file="footer.jsp" %>