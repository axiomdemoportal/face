<%@include file="header.jsp" %>
<script src="./assets/js/initdb.js"></script>
<div class="container-fluid">
    <h1 class="text-success">Change Database Details</h1>
    <p><font color="red">Please ensure the userid and password is changed rightly. Please restart the system after saving this change.</font></p>
    <hr>
    <br>
    <div class="row-fluid">
        <form class="form-horizontal" method="POST" action="#" id="InitDBForm" name="InitDBForm">
            <div class="control-group">
                <label class="control-label"  for="username">Database</label>
                <div class="controls">
                    <select name="_dbType" id="_dbType">
                        <!--<option value="JavaDB" selected>Internal DB (HA not supported)</option>-->                            
                        <option value="Mysql">MySQL Database</option>                                             
                        <option value="Oracle">Oracle Database</option>
                        <!--<option value="DB2">IBM DB2 Database</option>-->                                                                         
                        <!--<option value="MSSQL">Microsoft SQL Database</option>-->                                                                         
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"  for="username">Ip</label>
                <div class="controls">
                    <input type="text" id="_ip"  name="_ip" placeholder="Enter IP/Host">
                </div>
            </div>
            
            <div class="control-group">
                <label class="control-label"  for="username">Port</label>
                <div class="controls">
                    <input type="text" id="_p"  name="_p"   placeholder="Enter Port">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"  for="username">Database Name</label>
                <div class="controls">
                    <input type="text" id="_dbName" name="_dbName" placeholder="Enter Database Name">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"  for="username">User Id</label>
                <div class="controls">
                    <input type="text" id="_u"  name="_u"   placeholder="Enter User id">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"  for="username">Password</label>
                <div class="controls">
                    <input type="password" id="_pd" name="_pd"  placeholder="Enter password">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"  for="username">Confirm Password</label>
                <div class="controls">
                   <input type="password" id="_c" name="_c"  placeholder="Confirm password">
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <div id="login-result"></div>
                    <button class="btn btn-small btn-primary" onclick="DBInit()" type="button" id="INITDBbutton" >Save Database Details</button>
                    
                </div>
            </div>
        </form>

    </div>


    <%@include file="footer.jsp" %>

