

<%@page import="com.mollatech.axiom.nucleus.db.connector.management.GroupManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.MobileTrustManagment"%>
<%@page import="com.mollatech.axiom.nucleus.db.Statelist"%>
<%@page import="com.mollatech.axiom.nucleus.db.Countrylist"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.LocationManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Usergroups"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserGroupsManagement"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.face.common.CountryCodes"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Geofence"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.GeoLocationManagement"%>

<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>

<%
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    String _channelId = channel.getChannelid();
    GeoLocationManagement oObj = new GeoLocationManagement();
    LocationManagement lManagement = new LocationManagement();
    String _searchtext = request.getParameter("_searchtext");
    UserManagement usermngObj = new UserManagement();
    AuthUser Users[] = null;
    Users = usermngObj.SearchUsers(sessionId, _channelId, _searchtext);
    if (Users == null) {
        Users = usermngObj.SearchUsersByID(sessionId, _channelId, _searchtext);
    }

    String strerr = "No Record Found";
    String err = "Not Set";
    if (Users != null) {
%>

<h3>Results for <i>"<%=_searchtext%>"</i> in Geo-location (Fence & Track)</h3>

<div class="row-fluid">

    <div id="licenses_data_table">
        <input type="hidden" id="_isroaming" name="_isroaming"  >
        <table class="table table-striped" id="table_main">

            <tr>
                <td>No.</td>
                <td>UserId</td>
                <td>User Name</td>
                <td>User Group</td>
                <td>Manage</td>
                <td>Roaming</td>
                <td>Home Country</td>
                <td>Foreign Country</td>
                <td>Roaming States</td>
                <td>Geo Track</td>
                <td>Roaming Start Date</td>
                <td>Roaming End Date</td>

            </tr>
            <%

                UserGroupsManagement ugObj = new UserGroupsManagement();

                for (int i = 0; i < Users.length; i++) {
                    Geofence gObj = oObj.getGeoFenceObjByUserId(channel.getChannelid(),
                            Users[i].getUserId());
                    if (gObj != null) {
                        String strRoaming = "No";

                        int roamingState = gObj.getRoamingAllowed();
                        if (roamingState == MobileTrustManagment.COUNTRY_ROAMING_ENABLE) {
                            //if ((gObj.getRoamingAllowed()) == true) {
                            strRoaming = "Yes(COUNTRY)";
                        } else if (roamingState == MobileTrustManagment.STATE_ROAMING_ENABLE) {
                            strRoaming = "Yes(STATE)";
                        }
                        SimpleDateFormat df2 = new SimpleDateFormat("dd/mm/yyyy");
                        String start = "NA", end = "NA";
                        if (gObj.getStartDateForRoming() != null) {
                            start = df2.format(gObj.getStartDateForRoming());
                        }
                        if (gObj.getEndDateForRoming() != null) {
                            end = df2.format(gObj.getEndDateForRoming());
                        }

                        Countrylist conList = null;
                        if (gObj.getHomeCountry() != null && gObj.getHomeCountry().isEmpty() == false) {
                            conList = lManagement.getCountriesByid(Integer.parseInt(gObj.getHomeCountry()));
                        }

                        int igid = Users[i].getGroupid();

                        Usergroups usergroupObj = ugObj.getGroupByGroupId(sessionId, _channelId, igid);
                        String groupName = "NA";
                        if (usergroupObj != null) {
                            groupName = usergroupObj.getGroupname();
                        }

            %>
            <tr>
                <td><%=(i + 1)%></td>
                <td><a href="#" class="btn btn-mini" onclick="viewUserID('<%=Users[i].getUserId()%>')" >View ID</a></td>
                <td><%= Users[i].getUserName()%></td>
                <td><%= groupName%></td>
                <td>
                    <div class="btn-group">
                        <button class="btn btn-mini">Manage</button>
                        <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="loadRoamingDetails('<%=gObj.getUserid()%>')">Change Home Group</a></li>
                            <li><a href="#" onclick="enableRoamingDetails('<%=gObj.getUserid()%>',<%=igid%>)">Enable Roaming</a></li>
                            <li><a href="#" onclick="disableroamingsetting('<%=gObj.getUserid()%>', 2)">Disable Roaming</a></li>
                        </ul>
                    </div>
                </td>
                <td><%=strRoaming%></td>
                <%if (conList != null && conList.getCountyName().isEmpty() == false) {%>
                <td><%=conList.getCountyName()%></td>
                <%} else {%>
                <td><%=err%></td>
                <%}%>

                <%if (gObj.getForeignCountry() != null && gObj.getForeignCountry().length() > 0) {
                        String[] foreignCountryList = gObj.getForeignCountry().split(",");
                        Countrylist foreignCountry = null;
                        String resultCountry = "";
                        for (int q = 0; q < foreignCountryList.length; q++) {
                            LocationManagement lmObj = new LocationManagement();
                            foreignCountry = lmObj.getCountriesByid(Integer.parseInt(foreignCountryList[q]));
                            resultCountry += foreignCountry.getCountyName() + ", ";
                        }


                %>
                <td><%=resultCountry%></td>
                <%} else {%>
                <td><%=err%></td>
                <%}%>
                <%if (gObj.getOtherStates() != null && gObj.getOtherStates().length() > 0) {%>
                <td><%=gObj.getOtherStates()%></td>
                <%} else {%>
                <td><%=err%></td>
                <%}%>
                <td>
                    <div class="btn-group">
                        <button class="btn btn-mini">Geo Track For</button>
                        <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="./GeoTracks.jsp?_userID=<%=gObj.getUserid()%>+&_duration=1day" data-toggle="modal">Today</a></li>
                            <li><a href="./GeoTracks.jsp?_userID=<%=gObj.getUserid()%>+&_duration=7days" data-toggle="modal">Last 7 days</a></li>
                            <li><a href="./GeoTracks.jsp?_userID=<%=gObj.getUserid()%>+&_duration=1month" data-toggle="modal">Last 1 Month</a></li>
                            <li><a href="./GeoTracks.jsp?_userID=<%=gObj.getUserid()%>+&_duration=2months" data-toggle="modal">Last 2 Months</a></li>
                            <li><a href="./GeoTracks.jsp?_userID=<%=gObj.getUserid()%>+&_duration=3months" data-toggle="modal">Last 3 Months</a></li>
                        </ul>
                    </div>
                </td>

                <td><%=start%></td>
                <td><%=end%></td>

            </tr>
            <%}
                }
            } else {%>
            <h3>No users found...</h3>
            <%}%>


        </table>
    </div>
</div>
<!--</div>-->

<div id="changehome" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Select Home Group</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">

            <form class="form-horizontal" id="edithomecountryForm" name="edithomecountryForm">

                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_userID" name="_userID">

                    <div class="control-group">
                        <label class="control-label"  for="username">Group</label>
                        <div class="controls">

                            <select class="span10" name="_groupid" id="_groupid">
                                <%

                                    int groupid = -1;
                                    int countryid = -1;

                                    UserGroupsManagement uMgmt = new UserGroupsManagement();
                                    Usergroups[] uList = uMgmt.ListGroups(sessionId, channel.getChannelid());
                                    if (uList != null) {
                                        for (int i = 0; i < uList.length; i++) {
                                            //groupid = uList[i].getGroupid();
//                                            if (uList[i].getCountry() != null && uList[i].getCountry().equals("") == false) {
//                                                countryid = Integer.parseInt(uList[i].getCountry());
//                                            }
                                %>

                                <option value="<%=uList[i].getGroupid()%>"><%=uList[i].getGroupname()%></option>
                                <%
                                        }
                                    }
                                %>
                            </select>
                        </div>
                    </div>            





                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="editgeotrack-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="editgeotracking()()" id="buttonEditGeotrack" type="button">Change Home Group</button>
    </div>
</div>
<div id="editroamingsetting" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Enable Roaming</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="editRoamingSettingForm">
                <input type="hidden" name="_userIDEditRS" id="_userIDEditRS"/>                
                <input type="hidden" name="_groupidEditRS" id="_groupidEditRS"/>
                <input type="hidden" name="_homeCountryEditRS" id="_homeCountryEditRS"/>


                <%
                    Countrylist cList[] = lManagement.getAllCountries();
                    Statelist sList[] = null;
                    if (countryid != -1) {
                        sList = lManagement.getAllStateListByCountry(countryid);
                    }

                %>

                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_roamingID" name="_roamingID"  >
                    <input type="hidden" id="_userID1" name="_userID1"  >
                    <div class="control-group">
                        <label class="control-label"  for="username">Enable Roaming</label>
                        <div class="controls">
                            <select class="span8" name="_eroaming" id="_eroaming">
                                <option value="2">Disable Roaming</option>
                                <option value="1">Enable Country Level Roaming</option>
                                <option value="3">Enable State Level Roaming (with home country)</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group" id="fContires">
                        <label class="control-label"  for="username">Foreign Country</label>
                        <div class="controls">

                            <select  name="_foreignCountryEditRS" id="_foreignCountryEditRS"  class="span9" multiple>
                                <% for (int j = 0; j < cList.length; j++) {
                                %>
                                <option data-content="<span class='label label-success'><%=cList[j].getCountyName()%></span>"value="<%=cList[j].getCountryid()%>"><%=cList[j].getCountyName()%></option>                                             
                                <%
                                    }%>
                            </select>
                        </div>
                    </div>
                    <% if (sList != null) {
                    %>                    <div class="control-group" id="states">
                        <label class="control-label"  for="username">Roaming States</label>
                        <div class="controls">

                            <select  name="stateListeditRS" id="stateListeditRS"  class="span9" multiple>
                                <%
                                    for (int j = 0; j < sList.length; j++) {
                                %>
                                <option data-content="<span class='label label-success'><%=sList[j].getStateName()%></span>"value="<%=sList[j].getStateName()%>"><%=sList[j].getStateName()%></option>                                             
                                <%
                                    }
                                %>
                            </select>
                        </div>
                    </div>
                    <%
                    } else {
                    %>
                    <div class="control-group" id="states"></div>

                    <% }%>
                    <div class="control-group" id="sDate">
                        <label class="control-label"  for="username">Start Date :</label>
                        <div class="controls">
                            <div class="input-prepend">
                                <div id="datetimepicker1" class="input-append date">
                                    <input id="_startdateEditRS" name="_startdateEditRS" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                    <span class="add-on" >
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="control-group" id="eDate">
                        <label class="control-label"  for="username">End Date :</label>
                        <div class="controls">
                            <div class="input-prepend">
                                <!--<div class="well">-->
                                <!--<span class="add-on">Till :</span>-->   
                                <div id="datetimepicker2" class="input-append date">
                                    <input id="_enddateEditRS" name="_enddateEditRS" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                                <!--</div>-->
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="editroaming-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="editroamingsetting()" id="buttonEditRoaming" type="button">Save Roaming Setting</button>
    </div>
</div>


<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datepicker({
            format: 'dd/mm/yyyy',
            language: 'pt-BR'
        });
    });
    $(function () {
        $('#datetimepicker2').datepicker({
            format: 'dd/mm/yyyy',
            language: 'pt-BR'
        });
    });

    $(document).ready(function () {
        $("#_foreignCountry").select2()
    });
    $(document).ready(function () {
        $("#stateList").select2()
    });
    //                                    ChangeMediaType(0);



    $('#_eroaming').change(function () {
        // alert('hi');
        var selectedValue = $(this).val();

        if (selectedValue === '1') //country 
        {

            document.getElementById("fContires").style.display = "block";
            document.getElementById("states").style.display = "none";
            document.getElementById("sDate").style.display = "block";
            document.getElementById("eDate").style.display = "block";


        } else if (selectedValue === '2') // 
        {
            document.getElementById("fContires").style.display = "none";
            document.getElementById("states").style.display = "none";
            document.getElementById("sDate").style.display = "none";
            document.getElementById("eDate").style.display = "none";

        } else if (selectedValue === '3')
        {
            document.getElementById("fContires").style.display = "none";
            document.getElementById("states").style.display = "block";
            document.getElementById("sDate").style.display = "block";
            document.getElementById("eDate").style.display = "block";
        }


    });
</script>                               

<%--<%@include file="footer.jsp" %>--%>