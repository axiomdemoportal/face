<%@include file="header.jsp" %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">    
<link rel="shortcut icon" href="favicon.ico">  
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<!-- Global CSS -->

<!-- Plugins CSS -->    
<!--<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.css">-->
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<!--<link rel="stylesheet" href="assets/plugins/elegant_font/css/style.css">-->
<style>
    .innerr {
        display: inline-block;
        vertical-align: middle;
        background: yellow;
        padding: 3px 5px;
    }
    .block4 {
        /*        //color:#fff;background-color:#337ab7;*/
        box-shadow: 5px 5px 5px #888888;
        line-height: 90px;
        width:250px;
        margin-left:10px;
        margin-top: 20px;
    }
    .inner4 {
        line-height: normal; /* Reset line-height for the child. */
        background: none;
    }
    #square {
        width: 900px;
        height: 900px;
        background: red;
    }
    div.relative1 {
        margin: 1%;
        position: relative;
        height: 100%;
        color : black;
        border: 2px solid lightblue;
        box-shadow: 5px 5px 5px #888888;
        line-height: 100%;     
    } 

    div.absolute {
        position: absolute;
        top: 5%;
        bottom: 10%;
        left: 27%;
        width: 73%;
        height: 70%;
        font-size:100%;
    }
    .footerLink{
        margin-top: 20%;
        margin-left: 0%;
        text-align: center;
        line-height: 160%;              
        background-color:darkslategrey;                   
    }
    .footerMargin{
          margin-bottom: 0%; 
          color: white;
    }
    .slick-next {
        border: 1px solid lightblue;
        width: 20%;
        height: 40%;
        text-align: center;
        margin: 5%;
        margin-top: 7%;
        background-color:lightblue;

    }
        .slick-next:after {
            display: inline-block;
            vertical-align: middle;
            content: "";
            height: 135%;
        }
</style>
<script src="./assets/js/emailgateway.js"></script>
<%if (oprObj.getRoleid() != 1) {
//not sysadmin%>
<div class="container-fluid">
    <%   
        String _type = request.getParameter("_type");
        if (_type.equals("1")) {
    %>
    <div class="row-fluid" >
        <h1 class="text-success">Inventory Reports</h1>
        <p>list of reports</p>
        <ul class="thumbnails">
            <%if (accessObj.listUserReport) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="usersreport.jsp" >
                        <img src="./assets/img/user.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Users Reports</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listOtpReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="otptokensreport.jsp" >
                        <img src="./assets/img/otp6.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>OTP Tokens Reports</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listCertificateReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="certificatereport.jsp" >
                        <img src="./assets/img/certificate3.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Certificate Reports</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listPkiReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="pkitokenreport.jsp" >
                        <img src="./assets/img/pki1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>PKI Tokens Reports</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listEpinSystemReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="EcponSystemReportMain.jsp" >
                        <img src="./assets/img/epin1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>E-PIN Reports</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listtranscationReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="transactionReport.jsp" >
                        <img src="./assets/img/transaction5.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Transaction Details</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listRemoteSigningReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="RSSDetailsMain.jsp" >
                        <img src="./assets/img/transaction5.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Remote Signing Report</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>

        </ul>
    </div>


    <%} else if (_type.equals("2")) {%>
    <div class="row-fluid" >
        <h1 class="text-success">Usage Reports</h1>

        <ul class="thumbnails">
            <%if (accessObj.listOtpFailureReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="tokenFailureReport.jsp" >
                        <img src="./assets/img/user.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Usage Report</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>            
            <%}%>
            <%if (accessObj.listHoneyTrapReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="honeyTrapmain.jsp" >
                        <img src="./assets/img/honeytrap1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Honey Trap   Report</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
            <%
            //accessObj.listTwoWayAuthManagement = true;
            if (accessObj.listTwoWayAuthManagement == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="twoWayAuthDualChannel.jsp" >
                        <img src="./assets/img/TwoWayAuthReport.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Two Way (Dual Channel) Authentication Report</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
            <%
            //accessObj.listTwoWayAuthManagement = true;
            if (accessObj.listWebSealManagement == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="websealreport.jsp" >
                        <img src="./assets/img/secure-site.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>WebSeal Usage Report</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
        </ul>
    </div>

    <%} else if (_type.equals("3")) {%>
    <div class="row-fluid" >
        <h1 class="text-success">Billing Reports</h1>

        <ul class="thumbnails">
            <%if (accessObj.listsubscriptionBaseBillingReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="billReportmain.jsp" >
                        <img src="./assets/img/bill7.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Billing Report (Subscription)</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listtranscationBaseBillingReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="billReportmainPerTx.jsp" >
                        <img src="./assets/img/trans1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Billing Report (/Tx) </h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
        </ul>
    </div>

    <%} else if (_type.equals("4")) {%>
    <div class="row-fluid" >
        <h1 class="text-success">Message Reports</h1>

        <ul class="thumbnails">
            <%if (accessObj.listMessageReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="msgreportmain.jsp" >
                        <img src="./assets/img/sms2.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Message Report</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listMessageReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="msgcostreport.jsp" >
                        <img src="./assets/img/trans1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Summary Cost Report </h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
        </ul>
    </div>

    <%} else if (_type.equals("6")) {%>
    <div class="row-fluid" >
        <h1 class="text-success">Audit Reports</h1>

        <ul class="thumbnails">
            <%if (accessObj.downloadAuditTrail == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="channelaudit.jsp" >
                        <img src="./assets/img/ca1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Complete Audit Trail</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.downloadSessionAuditTrail == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="sessionaudit.jsp" >
                        <img src="./assets/img/ca1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Session ID Audit </h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.auditIntegrity == true) { %>
            <!--            <li class="span2">
                            <div class="thumbnail">
                                <a href="#CheckAuditIntegrity" >
                                    <img src="./assets/img/ca1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                                </a>
                                <div class="caption">
                                    <h5>Audit Integrity </h5>
                                    <h3>Check audit integrity</h3>
                                </div>
                            </div>
                        </li>-->
            <%}%>
        </ul>
    </div>

    <%} else if (_type.equals("7")) {%>
    <div class="row-fluid" >
        <h1 class="text-success">Gateway Configuration</h1>       
        <ul class="thumbnails">
            <%if (accessObj.listSmsGateway == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="smsgateway.jsp" >
                        <img src="./assets/img/sms2.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>SMS Gateway Configuration</h5>                        
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listUSSDGateway == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="ussdgateway.jsp" >
                        <img src="./assets/img/ussd8.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>USSD Gateway Configuration</h5>                        
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listVOICEGateway == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="voicegateway.jsp" >
                        <img src="./assets/img/voice11.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>VOICE Gateway Configuration</h5>

                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listEMAILGateway == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="emailgateway.jsp" >
                        <img src="./assets/img/email14.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Email Gateway Configuration</h5>

                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listFAXGateway == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="faxgateway.jsp" >
                        <img src="./assets/img/fax4.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>FAX Gateway Configuration</h5>

                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listPushGateway == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="pushgateway.jsp" >
                        <img src="./assets/img/push5.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Push Notification Gateway Configuration</h5>

                    </div>
                </div>
            </li>
            <%}%>

        </ul>
    </div>


    <%} else if (_type.equals("8")) {%>
    <div class="row-fluid" >
        <h1 class="text-success">System Configuration</h1>

        <ul class="thumbnails">
            <%if (accessObj.listPasswordPolicySettings == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="PasswordPolicySetting.jsp" >
                        <img src="./assets/img/pp4.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Password Policy Settings</h5>

                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listChannelProfileSettings == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="channelProfile.jsp" >
                        <img src="./assets/img/cp2.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Channel Profile Settings</h5>

                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listCertificateSettings == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="caconnector.jsp" >
                        <img src="./assets/img/certificate3.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Certificate Authority (CA) Connector Configuration</h5>

                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listMobileTrustSettings == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="mobiletrustsettings.jsp" >
                        <img src="./assets/img/mobiletrust4.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Trust Settings</h5>

                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listRadiusConfigSettings == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="radiusserver2.jsp" >
                        <img src="./assets/img/image1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Radius General and Client Setting Management</h5>

                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listUserSourceSettings == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="usersrcsetting.jsp" >
                        <img src="./assets/img/user.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>User Source Configuration</h5>

                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listGlobalSettings == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="globalsettingv2.jsp" >
                        <img src="./assets/img/global5.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Global Configuration</h5>

                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listEpinConfigSettings == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="epinsetting.jsp" >
                        <img src="./assets/img/epin1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>E-PIN Settings</h5>

                    </div>
                </div>
            </li>            
            <%}%>
            <%if (accessObj.listImageSettings == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="imagesettings.jsp" >
                        <img src="./assets/img/image1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Secure Phrase Configuration</h5>
                        <p>Secure Phrase Configuration.</p>
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listBillingManager == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="BillingManager.jsp" >
                        <img src="./assets/img/bill7.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Billing Manager</h5>

                    </div>
                </div>
            </li>
            <%}if (accessObj.listOtpTokensSettings == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="tokensettings.jsp" >
                        <img src="./assets/img/otp6.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>OTP Token Configuration</h5>

                    </div>
                </div>
            </li>
            <%}%>

        </ul>
    </div>


    <%}%>


</div>
<%} else 
{ // sysadmin
%>
<div class="container-fluid">
    <%   
        String _type = request.getParameter("_type");
//        _type = "6";
        if (_type.equals("1")) {
    %>
<!--    <div class="row-fluid" >
        <h1 class="text-success">Inventory Reports</h1>
        <p>list of reports</p>
        <ul class="thumbnails">

            <li class="span2">
                <div class="thumbnail">
                    <a href="usersreport.jsp" >
                        <img src="./assets/img/user.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Users Reports</h5>
                        <h3>Make Your Own Report</h3>
                    </div>
                </div>
            </li>
            <li class="span2">
                <div class="thumbnail">
                    <a href="otptokensreport.jsp" >
                        <img src="./assets/img/otp6.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>OTP Tokens Reports</h5>
                        <h3>Make Your Own Report</h3>
                    </div>
                </div>
            </li>
            <li class="span2">
                <div class="thumbnail">
                    <a href="certificatereport.jsp" >
                        <img src="./assets/img/certificate3.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Certificate Reports</h5>
                        <h3>Make Your Own Report</h3>
                    </div>
                </div>
            </li>
            <li class="span2">
                <div class="thumbnail">
                    <a href="pkitokenreport.jsp" >
                        <img src="./assets/img/pki1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>PKI Tokens Reports</h5>
                        <h3>Make Your Own Report</h3>
                    </div>
                </div>
            </li>
            <%if (accessObj.listEpinSystemReport == true) 
                    if ( 1 == 0) 
                { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="EcponSystemReportMain.jsp" >
                        <img src="./assets/img/epin1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>E-PIN Reports</h5>
                        <h3>Make Your Own Report</h3>
                    </div>
                </div>
            </li>
            <% } %> 
            <%if (accessObj.editTwoWayAuthManagement == true)  { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="transactionReport.jsp" >
                        <img src="./assets/img/transaction5.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Transaction Details</h5>
                        <h3>Make Your Own Report</h3>
                    </div>
                </div>
            </li>
            <li class="span2">
                <div class="thumbnail">
                    <a href="transactionReport.jsp" >
                        <img src="./assets/img/transaction5.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Transaction Details</h5>
                        <h3>Make Your Own Report</h3>
                    </div>
                </div>
            </li>
            <% } %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="RSSDetailsMain.jsp" >
                        <img src="./assets/img/transaction5.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Remote Signing Report</h5>

                    </div>
                </div>
            </li>

        </ul>
    </div>-->
            <div class="container-fluid" >
        <h1 class="text-success">Inventory Reports</h1>
        <table width="100%">
            <tr>
                <td>
                    <a href="usersreport.jsp">
                    <div class="relative1">                        
                        <div class="slick-next">
                            <span class="fa fa-user" style="font-size: 280%"></span>
                        </div>
                        <div class="absolute">                                 
                            <h4 class="title">User Report</h4>                           
                            <p class="intro">Using this Interface you can get details of user</p>                          
                        </div>
                        <div class="footerLink">
                            <p class="footerMargin">User Report</p>
                        </div>
                    </div>
                    </a>
                </td>
                <td>
                    <a href="otptokensreport.jsp">
                    <div class="relative1" style="margin-left: 10px;border: 2px solid #dff0d8;">                       
                        <div class="slick-next bg-success" style="border: 1px solid #dff0d8;background: #dff0d8 ">
                            <span class="fa fa-compass" style="font-size: 280%"></span>
                        </div>
                        <div class="absolute">                                 
                            <h4 class="title">OTP Tokens Reports</h4>                           
                            <p class="intro">Using this Interface you can get OTP Tokens details</p>                          
                        </div>
                        <div class="footerLink">
                            <p class="footerMargin">OTP Tokens Reports</p>
                        </div>
                    </div>
                    </a>
                </td>
                <td>
                    <a href="certificatereport.jsp">
                    <div class="relative1" style="margin-left: 10px;border: 2px solid #f2dede;">                            
                        <div class="slick-next" style="border: 1px solid #f2dede;background: #f2dede ">
                            <span class="fa fa-copy" style="font-size: 280%"></span>
                        </div>
                        <div class="absolute">                                 
                            <h4 class="title">Certificate Reports</h4>                           
                            <p class="intro">Using this Interface you can get Certificate details</p>                          
                        </div>
                        <div class="footerLink">
                            <p class="footerMargin">Certificate Reports</p>
                        </div>
                    </div>
                    </a>
                </td>
                <td>
                    <a href="pkitokenreport.jsp">
                    <div class="relative1" style="margin-left: 10px;border: 2px solid #f7ecb5;">                            
                        <div class="slick-next bg-danger" style="border: 1px solid #f7ecb5;background: #f7ecb5 ">
                            <span class="fa fa fa-line-chart" style="font-size: 280%"></span>
                        </div>
                        <div class="absolute">                                 
                            <h4 class="title">PKI Tokens Reports</h4>                           
                            <p class="intro">Using this Interface you can get PKI tokens details</p>                          
                        </div>
                        <div class="footerLink">
                            <p class="footerMargin">PKI Tokens Reports</p>
                        </div>
                    </div>
                    </a>
                </td>
            </tr>
            <tr>
		<%if (accessObj.listEpinSystemReport == true) 
                    if ( 1 == 0) 
                { %>
                <td>
                    <a href="EcponSystemReportMain.jsp">
                    <div class="relative1" style="margin-top: 15px">                    
                        <div class="slick-next">
                            <span class="fa fa fa-mobile-phone" style="font-size: 280%"></span>
                        </div>
                        <div class="absolute">                                 
                            <h4 class="title">E-PIN Reports</h4>                           
                            <p class="intro">Using this Interface you can manipulate your E-PIN Configuration</p>                          
                        </div>
                        <div class="footerLink">
                            <p class="footerMargin">E-PIN Reports</p>
                        </div>
                    </div> 
                    </a>
                </td>
		<% } %> 
           	<%if (accessObj.editTwoWayAuthManagement == true)  { %>
                <td>
                    <a href="transactionReport.jsp">
                    <div class="relative1" style="margin-left: 10px;border: 2px solid #dff0d8; margin-top: 15px">
                        <div class="slick-next" style="border: 1px solid #dff0d8;background: #dff0d8 ">
                            <span class="fa fa-line-chart" style="font-size: 280%"></span>
                        </div>
                        <div class="absolute">                                 
                            <h4 class="title">Transaction Details</h4>                           
                            <p class="intro">Using this Interface you can get transaction details</p>                          
                        </div>
                        <div class="footerLink">
                            <p class="footerMargin">Transaction Details</p>
                        </div>
                    </div>
                    </a>
                </td>
		<% } %> 
                <td width="25%">
                    <a href="RSSDetailsMain.jsp">
                    <div class="relative1" style="margin-left: 10px;border: 2px solid #f2dede; margin-top: 15px">                            
                        <div class="slick-next" style="border: 1px solid #f2dede;background: #f2dede ">
                            <span class="fa fa-gears" style="font-size: 280%"></span>
                        </div>
                        <div class="absolute">                                 
                            <h4 class="title">Remote Signing Report</h4>                           
                            <p class="intro">Using this Interface you can get remote signing details</p>                          
                        </div>
                        <div class="footerLink">
                            <p class="footerMargin">Remote Signing Report</p>
                        </div>
                    </div>
                    </a>
                </td>
            </tr>
        </table>
    </div>

    <%} else if (_type.equals("2")) {%>
    <div class="container-fluid" >
        <h1 class="text-success">Usage Reports</h1>
<!--        <p>list of reports</p>
        <ul class="thumbnails">
            <li class="span2">
                <div class="thumbnail">
                    <a href="tokenFailureReport.jsp" >
                        <img src="./assets/img/user.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Usage Report</h5>

                    </div>
                </div>
            </li>
            <%if (accessObj.listHoneyTrapReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="honeyTrapmain.jsp" >
                        <img src="./assets/img/honeytrap1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Honey Trap Report</h5>

                    </div>
                </div>
            </li>
            <%  } %>
            <%if (accessObj.editWebWatch == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="websealSetting.jsp" >
                        <img src="./assets/img/secure-site.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Web Seal Configuration</h5>

                    </div>
                </div>
            </li>
              <li class="span2">
            <div class="thumbnail">
                <a href="easyloginsession.jsp" >
                    <img src="./assets/img/mobile_auth.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                </a>
                <div class="caption">
                    <h5>Easy CheckIn Report</h5>
                    <h3>Make Your Own Report</h3>
                </div>
            </div>
        </li>
    </div>
            <%  } %>
        </ul>-->
            <table width="60%">
            <tr>
                <td width="32%" >
                     <a href="tokenFailureReport.jsp">
                    <div class="relative1">                        
                        <div class="slick-next">
                            <span class="fa fa-user" style="font-size: 280%"></span>
                        </div>
                        <div class="absolute">                                 
                            <h4 class="title">Usage Report</h4>                           
                            <p class="intro">Using this Interface you can get details token usage</p>                          
                        </div>
                        <div class="footerLink">
                           <p class="footerMargin">Usage Report</p>
                        </div>
                    </div>
                    </a>
                </td >
		 <%if (accessObj.listHoneyTrapReport == true) { %>
                <td width="33%">
                    <a href="honeyTrapmain.jsp">
                    <div class="relative1" style="margin-left: 10px;border: 2px solid #dff0d8;">                       
                        <div class="slick-next bg-success" style="border: 1px solid #dff0d8;background: #dff0d8 ">
                            <span class="fa fa-compass" style="font-size: 280%"></span>
                        </div>
                        <div class="absolute">                                 
                            <h4 class="title">Honey Trap Report</h4>                           
                            <p class="intro">Using this Interface you can get deails honey trap</p>                          
                        </div>
                        <div class="footerLink">
                            <p class="footerMargin">Honey Trap Report</p>
                        </div>
                    </div>
                    </a>
                </td>
		<%  } %>
		<%if (accessObj.editWebWatch == true) { %>
                <td width="33%">
                    <a href="websealSetting.jsp">
                    <div class="relative1" style="margin-left: 10px;border: 2px solid #f2dede;">                            
                        <div class="slick-next" style="border: 1px solid #f2dede;background: #f2dede ">
                            <span class="fa fa-copy" style="font-size: 280%"></span>
                        </div>
                        <div class="absolute">                                 
                            <h4 class="title">Web Seal Configuration</h4>                           
                            <p class="intro">Using this Interface you can get deails of web seal</p>                          
                        </div>
                        <div class="footerLink">
                            <p class="footerMargin">Web Seal Configuration</p>
                        </div>
                    </div>
                    </a>
                </td>
                <td >
<!--                    <div class="relative1" style="margin-left: 10px;border: 2px solid #f7ecb5;">                            
                        <div class="slick-next bg-danger" style="border: 1px solid #f7ecb5;background: #f7ecb5 ">
                            <span class="fa fa fa-line-chart" style="font-size: 280%"></span>
                        </div>
                        <div class="absolute">                                 
                            <h4 class="title">Easy CheckIn Report</h4>                           
                            <p class="intro">Using this Interface you can get deails of easy checkin</p>                          
                        </div>
                        <div class="footerLink">
                            <a href="easyloginsession.jsp"><p class="footerMargin">Easy CheckIn Report</p></a>
                        </div>
                    </div>-->
                </td>
		 <%  } %>
            </tr>
        </table>
    </div>

    <%} else if (_type.equals("3")) {%>
   <div class="container-fluid" >
        <h1 class="text-success">Billing Reports</h1>
<!--        <p>list of reports</p>
        <ul class="thumbnails">
            <li class="span2">
                <div class="thumbnail">
                    <a href="billReportmain.jsp" >
                        <img src="./assets/img/bill7.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Billing Report (Subscription)</h5>

                    </div>
                </div>
            </li>
            <li class="span2">
                <div class="thumbnail">
                    <a href="billReportmainPerTx.jsp" >
                        <img src="./assets/img/trans1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Billing Report (/Tx) </h5>

                    </div>
                </div>
            </li>
        </ul>
    </div>-->
    <table width="50%">
            <tr>
                <td width="10%">
                    <a href="billReportmain.jsp">
                    <div class="relative1">                        
                        <div class="slick-next">
                            <span class="fa fa-credit-card" style="font-size: 280%"></span>
                        </div>
                        <div class="absolute">                                 
                            <h4 class="title">Billing Report (Subscription)</h4>                           
                            <p class="intro">Using this Interface you can get subscription details</p>                          
                        </div>
                        <div class="footerLink">
                            <p class="footerMargin">Billing Report</p>
                        </div>
                    </div>
                    </a>
                </td>
                <td width="10%">
                    <a href="billReportmainPerTx.jsp">
                    <div class="relative1" style="margin-left: 10px;border: 2px solid #dff0d8;">                       
                        <div class="slick-next bg-success" style="border: 1px solid #dff0d8;background: #dff0d8 ">
                            <span class="fa fa-bar-chart" style="font-size: 280%"></span>
                        </div>
                        <div class="absolute">                                 
                            <h4 class="title">Billing Report (/Tx)</h4>                           
                            <p class="intro">Using this Interface you can get transcation details</p>                          
                        </div>
                        <div class="footerLink">
                            <p class="footerMargin">Billing Report (/Tx)</p>
                        </div>
                    </div>
                    </a>
                </td>
            </tr>
        </table>
    </div>

    <%} else if (_type.equals("4")) {%>
    <div class="container-fluid" >
        <h1 class="text-success">Message Reports</h1>
<!--        <p>list of reports</p>
        <ul class="thumbnails">
            <li class="span2">
                <div class="thumbnail">
                    <a href="msgreportmain.jsp" >
                        <img src="./assets/img/sms2.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Message Report</h5>
                        <h3>Make Your Own Report</h3>
                    </div>
                </div>
            </li>
            <li class="span2">
                <div class="thumbnail">
                    <a href="msgcostreport.jsp" >
                        <img src="./assets/img/trans1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Summary Cost Report </h5>
                        <h3>Make Your Own Report</h3>
                    </div>
                </div>
            </li>
        </ul>-->
        <table width="50%">
            <tr>
                <td width="10%">
			<a href="msgreportmain.jsp">                    
		  <div class="relative1">                        
                        <div class="slick-next">
                            <span class="fa fa-envelope" style="font-size: 280%"></span>
                        </div>
                        <div class="absolute">                                 
                            <h4 class="title">Message Report</h4>                           
                            <p class="intro">Using this Interface you can get details of usage of email,sms etc</p>                          
                        </div>
                        <div class="footerLink">
                            <p class="footerMargin">Message Report</p>
                        </div>
                    </div>
			</a>
                </td>
                <td width="10%">
		    <a href="msgcostreport.jsp">
                    <div class="relative1" style="margin-left: 10px;border: 2px solid #dff0d8;">                       
                        <div class="slick-next bg-success" style="border: 1px solid #dff0d8;background: #dff0d8 ">
                            <span class="fa fa-file-text-o" style="font-size: 280%"></span>
                        </div>
                        <div class="absolute">                                 
                            <h4 class="title">Summary Cost Report</h4>                           
                            <p class="intro">Using this Interface you can get details summary cost</p>                          
                        </div>
                        <div class="footerLink">
                           <p class="footerMargin">Summary Cost Report</p>
                        </div>
                    </div>
		    </a>
                </td>
            </tr>
        </table>
    </div>

    <%} else if (_type.equals("6")) {%>
    <div class="row-fluid" >
        <h1 class="text-success">Audit Reports</h1>
 <!--         <p>list of reports</p>
      <ul class="thumbnails">
            <li class="span2">
                <div class="thumbnail">
                    <a href="channelaudit.jsp" >
                        <img src="./assets/img/ca1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Complete Audit Trail</h5>
                        <h3>Make Your Own Report</h3>
                    </div>
                </div>
            </li>
            <li class="span2">
                <div class="thumbnail">
                    <a href="sessionaudit.jsp" >
                        <img src="./assets/img/ca1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Session ID Audit </h5>
                        <h3>Make Your Own Report</h3>
                    </div>
                </div>
            </li>
                        <li class="span2">
                            <div class="thumbnail">
                                <a href="#CheckAuditIntegrity" >
                                    <img src="./assets/img/ca1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                                </a>
                                <div class="caption">
                                    <h5>Audit Integrity </h5>
                                    <h3>Check audit integrity</h3>
                                </div>
                            </div>
                        </li>
        </ul>-->
        <table width="50%">
            <tr>
                <td width="10%">
			<a href="channelaudit.jsp">                    
		  <div class="relative1">                        
                        <div class="slick-next">
                            <span class="fa fa-newspaper-o" style="font-size: 280%"></span>
                        </div>
                        <div class="absolute">                                 
                            <h4 class="title">Complete Audit Trail</h4>                           
                            <p class="intro">Using this Interface you can get details of audit</p>                          
                        </div>
                        <div class="footerLink">
                            <p class="footerMargin">Complete Audit Trail</p>
                        </div>
                    </div>
			</a>
                </td>
                <td width="10%">
		    <a href="sessionaudit.jsp">
                    <div class="relative1" style="margin-left: 10px;border: 2px solid #dff0d8;">                       
                        <div class="slick-next bg-success" style="border: 1px solid #dff0d8;background: #dff0d8 ">
                            <span class="fa fa-history" style="font-size: 280%"></span>
                        </div>
                        <div class="absolute">                                 
                            <h4 class="title">Session ID Audit</h4>                           
                            <p class="intro">Using this Interface you can get details of Session ID Audit</p>                          
                        </div>
                        <div class="footerLink">
                           <p class="footerMargin">Session ID Audit</p>
                        </div>
                    </div>
		    </a>
                </td>
            </tr>
        </table>
    </div>

    <%} else if (_type.equals("7")) {%>
<div class="container-fluid">
        <h2 class="text-success">Gateway Configuration</h2>             
        <table width="100%">
            <tr>
                <td>
                    <a href="smsgateway.jsp">
                    <div class="relative1">                        
                        <div class="slick-next">
                            <span class="fa fa fa-mobile-phone" style="font-size: 280%"></span>
                        </div>
                        <div class="absolute">                                 
                            <h4 class="title">SMS Gateway Configuration</h4>                           
                            <p class="intro">Using this Interface you can manipulate your SMS Gateway Configuration</p>                          
                        </div>
                        <div class="footerLink">
                            <p class="footerMargin">Manage Your SMS Gateway Configuration    </p>
                        </div>
                    </div>
                    </a>
                </td>
                <td>
                     <a href="ussdgateway.jsp">
                    <div class="relative1" style="margin-left: 10px;border: 2px solid #dff0d8;">                       
                        <div class="slick-next bg-success" style="border: 1px solid #dff0d8;background: #dff0d8 ">
                            <span class="fa fa fa-tablet" style="font-size: 280%"></span>
                        </div>
                        <div class="absolute">                                 
                            <h4 class="title">USSD Gateway Configuration</h4>                           
                            <p class="intro">Using this Interface you can manipulate your USSD Gateway Configuration</p>                          
                        </div>
                        <div class="footerLink">
                           <p class="footerMargin">Manage Your USSD Gateway Configuration    </p>
                        </div>
                    </div>
                    </a>
                </td>
                <td>
                    <a href="voicegateway.jsp">
                    <div class="relative1" style="margin-left: 10px;border: 2px solid #f2dede;">                            
                        <div class="slick-next" style="border: 1px solid #f2dede;background: #f2dede ">
                            <span class="fa fa-phone" style="font-size: 280%"></span>
                        </div>
                        <div class="absolute">                                 
                            <h4 class="title">VOICE Gateway Configuration</h4>                           
                            <p class="intro">Using this Interface you can manipulate your VOICE Gateway Configuration</p>                          
                        </div>
                        <div class="footerLink">
                            <p class="footerMargin">Manage Your VOICE Gateway Configuration    </p>
                        </div>
                    </div>
                    </a>
                </td>
                <td>
                    <a href="emailgateway.jsp">
                    <div class="relative1" style="margin-left: 10px;border: 2px solid #f7ecb5;">                            
                        <div class="slick-next bg-danger" style="border: 1px solid #f7ecb5;background: #f7ecb5 ">
                            <span class="fa fa-envelope-o" style="font-size: 280%"></span>
                        </div>
                        <div class="absolute">                                 
                            <h4 class="title">EMAIL Gateway Configuration</h4>                           
                            <p class="intro">Using this Interface you can manipulate your EMAIL Gateway Configuration</p>                          
                        </div>
                        <div class="footerLink">
                            <p class="footerMargin">Manage Your EMAIL Gateway Configuration    </p>
                        </div>
                    </div>
                    </a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="faxgateway.jsp">
                    <div class="relative1" style="margin-top: 15px">
                        <div class="slick-next">
                            <span class="fa fa-fax" style="font-size: 40px"></span>
                        </div>
                        <div class="absolute">                                 
                            <h4 class="title">FAX Gateway Configuration</h4>                           
                            <p class="intro">Using this Interface you can manipulate your FAX Gateway Configuration</p>                          
                        </div>
                        <div class="footerLink">
                            <p class="footerMargin">Manage Your FAX Gateway Configuration    </p>
                        </div>
                    </div>
                    </a>
                </td> 
                <td>
                    <a href="pushgateway.jsp">
                    <div class="relative1" style="margin-left: 10px;border: 2px solid #dff0d8; margin-top: 15px">                                                
                        <div class="slick-next" style="border: 1px solid #dff0d8;background: #dff0d8 ">
                            <span class="fa fa-commenting-o" style="font-size: 280%"></span>
                        </div>
                        <div class="absolute">                                 
                            <h4 class="title">Push Notification Configuration</h4>                           
                            <p class="intro">Using this Interface you can manipulate your Push Notification Configuration</p>                          
                        </div>
                        <div class="footerLink">
                            <p class="footerMargin">Manage Your Push Notification Config</p>
                        </div>
                    </div>
                    </a>
                </td>
            </tr>
        </table>
    </div>



    <%} else if (_type.equals("8")) {%>
<div class="container-fluid">
    <h2 class="text-success">System Configuration</h2>

    <table width="100%">       
        <tr>
              <td height="absolute">
                <a href="channelProfile.jsp"  style="line-height: 160%">
                <div class="relative1">
                    <div class="slick-next">
                        <span class="fa fa fa-sitemap" style="font-size: 280%"></span>
                    </div>
                    <div class="absolute">                                 
                        <h4 class="title" style="text-align:left; text-height: 70%">Channel Profile Settings</h4>                           
                        <p class="intro">Using this Interface you can manipulate your Channel Profile Settings</p>                          
                    </div>
                    <div class="footerLink">
                        <p class="footerMargin">Manage Your Channel Profile Settings</p>
                    </div> 
                </div>
                </a>
            </td>
            <td height="absolute">
                <a href="PasswordPolicySetting.jsp" style="line-height: 160%">
                <div class="relative1" style="border:solid #dff0d8;">                    
                    <div class="slick-next" style="border:solid #dff0d8; background:#dff0d8 ">
                        <span class="fa fa-lock" style="font-size: 280%"></span>
                    </div>
                    <div class="absolute">                                 
                        <h4 class="title"  style="text-align:left; text-height: 70%">Password Policy Settings</h4>                           
                        <p class="intro">Using this Interface you can manipulate your Password Policy Configuration</p>                          
                    </div>
                    <div class="footerLink">
                        <p class="footerMargin">Manage Your Password Policy</p>
                    </div>
                </div>
                </a>
            </td>                      
            <td height="absolute">
                <a href="caconnector.jsp">
                <div class="relative1" style="border:solid #f2dede;">                            
                    <div class="slick-next" style="border: solid #f2dede;background: #f2dede ">
                        <span class="fa fa-copy" style="font-size: 280%"></span>
                    </div>
                    <div class="absolute">                                 
                        <h4 class="title">Certificate Authority Configuration</h4>                           
                        <p class="intro">Using this Interface you can manipulate your Certificate Authority Configuration</p>                          
                    </div>
                    <div class="footerLink">
                        <p class="footerMargin">Manage Your Certificate Authority </p>
                    </div>
                </div>
                </a>
            </td>
            <td height="absolute">
                <a href="mobiletrustsettings.jsp">
                <div class="relative1" style="border: solid #f7ecb5;">                            
                    <div class="slick-next bg-danger" style="border:solid #f7ecb5;background: #f7ecb5 ">
                        <span class="fa fa-chain" style="font-size: 280%"></span>
                    </div>
                    <div class="absolute">                                 
                        <h4 class="title">Trust Settings</h4>                           
                        <p class="intro">To facilitate multi layered security</p>                          
                    </div>
                    <div class="footerLink">
                        <p class="footerMargin">Manage Your Trust Settings    </p>
                    </div>
                </div>
                </a>
            </td>
        </tr>
        <tr>
            <%if (accessObj.listEpinSystemReport == true)
                if ( 1 == 0 ) { %>
               
                <td height="absolute">
                <a href="epinsetting.jsp">
                    <div class="relative1">                    
                    <div class="slick-next">
                        <span class="fa fa fa-mobile-phone" style="font-size: 280%"></span>
                    </div>
                    <div class="absolute">                                 
                        <h4 class="title">E-PIN Settings</h4>                           
                        <p class="intro">Using this Interface you can manipulate your E-PIN Configuration</p>                          
                    </div>
                    <div class="footerLink">
                        <p class="footerMargin">Manage Your E-PIN Configuration    </p>
                    </div>
                </div>
                </a>
            </td>
            <%}%>
            <td height="absolute">
                <a href="usersrcsetting.jsp">
                <div class="relative1" style="border: solid #dff0d8">
                    <div class="slick-next" style="border: 1px solid #dff0d8;background: #dff0d8 ">
                        <span class="fa fa fa-group" style="font-size: 280%"></span>
                    </div>
                    <div class="absolute">                                 
                        <h4 class="title">User Source Settings</h4>                           
                        <p class="intro">Using this Interface you can manipulate your User Source Settings</p>                          
                    </div>
                    <div class="footerLink">
                        <p class="footerMargin">Manage Your User Source Settings    </p>
                    </div>
                </div>
                </a>
            </td>  
            <td height="absolute">
                <a href="radiusserver2.jsp">
                <div class="relative1" style="border:solid #f2dede">                            
                    <div class="slick-next" style="border: 1px solid #f2dede;background: #f2dede ">
                        <span class="fa fa-gears" style="font-size: 280%"></span>
                    </div>
                    <div class="absolute">                                 
                        <h4 class="title">Radius and Client Management</h4>                           
                        <p class="intro">Using this Interface you can manipulate your Radius and Client Management</p>                          
                    </div>
                    <div class="footerLink">
                        <p class="footerMargin">Manage Your Radius and Client</p>
                    </div>
                </div>
                </a>
            </td>
            <td height="absolute">
                <a href="globalsettingv2.jsp">
                <div class="relative1" style="border: solid #f7ecb5">                            
                    <div class="slick-next bg-danger" style="border: 1px solid #f7ecb5;background: #f7ecb5 ">
                        <span class="fa fa-globe" style="font-size: 280%"></span>
                    </div>
                    <div class="absolute">                                 
                        <h4 class="title">Global Configuration</h4>                           
                        <p class="intro">To facilitate multi layered security</p>                          
                    </div>
                    <div class="footerLink">
                        <p class="footerMargin">Manage Your Global Configuration </p>
                    </div>
                </div>
                </a>
            </td>    
        </tr>

        <tr>                           
            <td height="absolute">
                <a href="imagesettings.jsp">
                <div class="relative1">                    
                    <div class="slick-next">
                        <span class="fa fa-shield" style="font-size: 280%"></span>
                    </div>
                    <div class="absolute">                                 
                        <h4 class="title">Secure Phrase</h4>                           
                        <p class="intro">Using this Interface you can manipulate your Secure Phrase</p>                          
                    </div>
                    <div class="footerLink">
                        <p class="footerMargin">Manage Your Secure Phrase    </p>
                    </div>
                </div> 
                </a>
            </td>
            <%if (accessObj.editOtpTokensSettings == true) { %>
            <td height="absolute">
                <a href="BillingManager.jsp">
                <div class="relative1" style="border: solid #dff0d8">
                    <div class="slick-next bg-success" style="border: 1px solid #dff0d8;background: #dff0d8 ">
                        <span class="fa fa-credit-card" style="font-size: 280%"></span>
                    </div>
                    <div class="absolute">                                 
                        <h4 class="title">Billing Manager</h4>                           
                        <p class="intro">Using this Interface you can manipulate your Billing Manager</p>                          
                    </div>
                    <div class="footerLink">
                        <p class="footerMargin">Manage Your User Billing Manager    </p>
                    </div>
                </div>
                </a>
            </td>
            <%  } %>
            <%if (accessObj.editOtpTokensSettings == true) { %>
            <td height="absolute">
                <a href="tokensettings.jsp">
                <div class="relative1" style="border:solid #f2dede">                            
                    <div class="slick-next bg-danger" style="border: 1px solid #f2dede;background: #f2dede">
                        <span class="fa fa-compass" style="font-size: 280%"></span>
                    </div>
                    <div class="absolute">                                 
                        <h4 class="title">OTP Token Configuration</h4>                           
                        <p class="intro">To facilitate multi layered security</p>                          
                    </div>
                    <div class="footerLink">
                        <p class="footerMargin">Manage Your multi layered security</p>
                    </div>
                </div>
                </a>
            </td>    
            <%  } %>
        </tr>
        <%}%> 
    </table>
</div>
    <%  } %>    
<script type="text/javascript" src="assets/plugins/jquery-1.12.3.min.js"></script>                                                                   
<script type="text/javascript" src="assets/plugins/jquery-match-height/jquery.matchHeight-min.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script>
<%@include file="footer.jsp" %>

