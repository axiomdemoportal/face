<%@page import="com.mollatech.axiom.connector.communication.AXIOMStatus"%>
<%@page import="com.mollatech.axiom.nucleus.settings.SendNotification"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.TemplateUtils"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.db.Templates"%>
<%@page import="com.mollatech.axiom.nucleus.db.operation.TemplateNames"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%@page import="com.mollatech.axiom.nucleus.settings.TokenSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet"%>
<%@page import="com.mollatech.axiom.nucleus.db.Units"%>
<%@include file="header.jsp" %>
<script src="./assets/js/units.js"></script>
<script src="./assets/js/ajaxfileupload.js"></script>
<script src="./assets/js/bootstrap-fileupload.js"></script> 
<link href="./assets/css/bootstrap.css" rel="stylesheet">
<link href="./assets/css/bootstrap-fileupload.css" rel="stylesheet">
<%  String _sessionID = (String) session.getAttribute("_apSessionID");
    SessionManagement smObj = new SessionManagement();
    TemplateManagement tempObj = new TemplateManagement();
//     OperatorsManagement s = new OperatorsManagement();
    int iStatus = smObj.GetSessionStatus(_sessionID);
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    OperatorsManagement oMngt = new OperatorsManagement();
    OTPTokenManagement otpMngt = new OTPTokenManagement(channel.getChannelid());
    SettingsManagement sMngt = new SettingsManagement();
    SendNotification send = new SendNotification();
    Units[] unitsObj = null;
    if (iStatus == 1) { //active
        smObj.UpdateSession(_sessionID);
        UnitsManagemet cmObj = new UnitsManagemet();
        unitsObj = cmObj.ListUnitss(_sessionID, channel.getChannelid());
        cmObj = null;
    }
%>
<div class="container-fluid">
    <h1 class="text-success">Units Management</h1>
    <div class="row-fluid">
        <div id="licenses_data_table">
            <table class="display responsive wrap" id="UnitListTable">
                <thead>

                    <tr>
                        <td><b>No</td>
                        <td><b>Units Name</td>
                        <td><b>Manage</td>
                        <td><b>Created On</td>
                        <td><b>Last Updated On</td>
                        <td><b>Operators</td>
                        <td><b>Hardware OTP Tokens</td>
                        <td><b>Threshold Limit</td>                    
                    </tr>
                </thead>


                <%
                    out.flush();
//                    UnitsManagemet tmObj = new UnitsManagemet();
                    if (unitsObj != null) {
                        for (int i = 0; i < unitsObj.length; i++) {

                            int iActiveCount = oMngt.getOperatorByUnitId(_sessionID, channel.getChannelid(), unitsObj[i].getUnitid(), OperatorsManagement.ACTIVE_STATUS);
                            int inActiveCount = oMngt.getOperatorByUnitId(_sessionID, channel.getChannelid(), unitsObj[i].getUnitid(), OperatorsManagement.SUSPEND_STATUS);
                            int lockedOperatorCount = oMngt.getOperatorByUnitId(_sessionID, channel.getChannelid(), unitsObj[i].getUnitid(), OperatorsManagement.LOCKED_STATUS);
                            int deletedOperatorCount = oMngt.getOperatorByUnitId(_sessionID, channel.getChannelid(), unitsObj[i].getUnitid(), OperatorsManagement.REMOVE_STATUS);

                            int iAllCount = oMngt.getOperatorByUnitId(_sessionID, channel.getChannelid(), unitsObj[i].getUnitid(), 2);

                            int iActiveHwToken = otpMngt.getHwTokenUnit(_sessionID, channel.getChannelid(), OTPTokenManagement.HARDWARE_TOKEN,
                                    OTPTokenManagement.TOKEN_STATUS_ACTIVE, unitsObj[i].getUnitid());
//                               int iall = otpMngt.getHwTokenUnit(_sessionID,channel.getChannelid(),OTPTokenManagement.HARDWARE_TOKEN,
//                                   OTPTokenManagement.TOKEN_STATUS_ALL,unitsObj[i].getUnitid());
                            int iassign = otpMngt.getHwTokenUnit(_sessionID, channel.getChannelid(), OTPTokenManagement.HARDWARE_TOKEN,
                                    OTPTokenManagement.TOKEN_STATUS_ASSIGNED, unitsObj[i].getUnitid());
                            int ifree = otpMngt.getHwTokenUnit(_sessionID, channel.getChannelid(), OTPTokenManagement.HARDWARE_TOKEN,
                                    OTPTokenManagement.TOKEN_STATUS_FREE, unitsObj[i].getUnitid());
                            int ilocked = otpMngt.getHwTokenUnit(_sessionID, channel.getChannelid(), OTPTokenManagement.HARDWARE_TOKEN,
                                    OTPTokenManagement.TOKEN_STATUS_LOCKEd, unitsObj[i].getUnitid());
                            int ilost = otpMngt.getHwTokenUnit(_sessionID, channel.getChannelid(), OTPTokenManagement.HARDWARE_TOKEN,
                                    OTPTokenManagement.TOKEN_STATUS_LOST, unitsObj[i].getUnitid());
                            int isuspended = otpMngt.getHwTokenUnit(_sessionID, channel.getChannelid(), OTPTokenManagement.HARDWARE_TOKEN,
                                    OTPTokenManagement.TOKEN_STATUS_SUSPENDED, unitsObj[i].getUnitid());
//                             int iunassign = otpMngt.getHwTokenUnit(_sessionID,channel.getChannelid(),OTPTokenManagement.HARDWARE_TOKEN,
//                                   OTPTokenManagement.TOKEN_STATUS_UNASSIGNED,unitsObj[i].getUnitid());
                            int iall = iActiveHwToken + iassign + ifree + ilocked + ilost + isuspended;

                            Object ipobj = sMngt.getSettingInner(channel.getChannelid(), SettingsManagement.Token, 1);
                            if (ipobj != null) {
                                TokenSettings tObj = (TokenSettings) ipobj;
                                if (tObj.isTokenAlert() == true) {
                                    if (unitsObj[i].getThrsholdLimit() <= ifree) {
                                        Templates templatesObj = tempObj.LoadbyName(_sessionID, channel.getChannelid(), TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                                        if (templatesObj.getStatus() == tempObj.ACTIVE_STATUS) {
                                            ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                            String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                            String strsubject = templatesObj.getSubject();

                                            Operators[] aOperator = oMngt.arrGetOperatorByUnitId(_sessionID, channel.getChannelid(), unitsObj[i].getUnitid());
                                            if (aOperator != null) {
                                                String[] emailList = new String[aOperator.length - 1];
                                                for (int j = 1; j < aOperator.length; j++) {
                                                    emailList[j - 1] = aOperator[j].getEmailid();

                                                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
                                                    if (strmessageBody != null) {
                                                        // Date date = new Date();
                                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[j].getName());
                                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[j].getEmailid());
                                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                                        strmessageBody = strmessageBody.replaceAll("#filterword#", request.getRemoteAddr());
                                                    }
                                                }

                                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                            }

                                        }
                                    }
                                }
                            }

                            Units axcObj = unitsObj[i];
                            int iActivePr = 0;
                            if (iActiveCount != 0) {
                                iActivePr = (iActiveCount / iAllCount) * 100;
                            }
                            int inActivePr = 0;
                            if (inActiveCount != 0) {
                                inActivePr = (inActiveCount / iAllCount) * 100;
                            }

                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            String strStatus = "Active";
                            if (axcObj.getStatus() == 1) {
                                strStatus = "Active";
                            } else {
                                strStatus = "Suspended";
                            }
                            String userStatus = "user-status-value-" + i;
                            int iAllpr = 0;
                            if (iAllCount != 0) {
                                iAllpr = 100;
                            }

                %>
                <tr>
                    <td><%=(i + 1)%></td>
                    <td><%=axcObj.getUnitname()%></td>


                    <td>
                        <div class="btn-group">
                            <button class="btn btn-mini" id="<%=userStatus%>"><%=strStatus%></button>
                            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#"  onclick="changeUnitsStatus(<%=axcObj.getUnitid()%>, 1, '<%=userStatus%>')" >Mark as Active?</a></li>
                                <li><a href="#" onclick="changeUnitsStatus(<%=axcObj.getUnitid()%>, 0, '<%=userStatus%>')" >Mark as Suspended?</a></li>
                                <li class="divider"></li>
                                <li><a href="#" onclick="loadEditUnitDetails('<%=axcObj.getUnitname()%>', '<%=axcObj.getUnitid()%>', '<%=axcObj.getThrsholdLimit()%>')">Edit Details</a></li>
                                <li><a href="#" onclick="loadShiftUnitDetails('<%=axcObj.getUnitname()%>', '<%=axcObj.getUnitid()%>')">Shift Unit's Operator</a></li>                                
                                <li><a href="#" onclick="loadAssignTokens('<%=axcObj.getUnitname()%>', '<%=axcObj.getUnitid()%>')">Assign Token's</a></li>
                                <li><a href="#" onclick="loadShiftTokensTokens('<%=axcObj.getUnitname()%>', '<%=axcObj.getUnitid()%>')">Shift Free tokens between units</a></li>
                                <li><a href="#" onclick="removeUnits(<%=axcObj.getUnitid()%>)"><font color="red">Remove Unit?</font></a></li>

                            </ul>
                        </div>
                    </td>

                    <td><%=sdf.format(axcObj.getCreatedOn())%></td>
                    <td><%=sdf.format(axcObj.getLastupOn())%></td>


                    <td> 

                        <span class="label label-success">Active(<%=iActiveCount%>)</span>
                        <span class="label label-danger">Suspended(<%=inActiveCount%>)</span>
                        <span class="label label-primary">Permanently Removed(<%=deletedOperatorCount%>)</span>
                        <span class="label label-primary">Locked (<%=lockedOperatorCount%>)</span>
                        <span class="label label-primary">Total (<%=iAllCount%>)</span>
                    </td>
                    <td> 

                        <span class="label label-primary">Assigned:<%=iassign%></span>
                        <span class="label label-success">Active:<%=iActiveHwToken%></span>
                        <span class="label label-danger">Suspended:<%=isuspended%></span>
                        <span class="label label-warming">Locked:<%=ilocked%></span>
                        <span class="label label-danger">Lost:<%=ilost%></span>
                        <span class="label label-info">Free:<%=ifree%></span>
                        <span class="label label-default">Total:<%=iall%></span>

                    </td>
                    <td><%=axcObj.getThrsholdLimit()%>


                </tr>
                <%}
                    }%>
            </table>
            <p><a href="#addNewUnit" class="btn btn-primary" data-toggle="modal">Add New Unit&raquo;</a></p>
        </div>
    </div>
    <br>

    <script language="javascript">
        //listChannels();
    </script>


    <script>
        $(document).ready(function () {
            $('#UnitListTable').DataTable({
                responsive: true
            });
        });
    </script>

</div>
<!-- Modal -->
<div id="addNewUnit" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Add New Unit</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="AddNewUnitForm" name="AddNewUnitForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Name</label>
                        <div class="controls">
                            <input type="text" id="_unitName" name="_unitName" onblur="enabelAdd()" placeholder="set unique name for login" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Threshold Limit</label>
                        <div class="controls">
                            <input type="text" id="_thresholdLimit" name="_thresholdLimit" placeholder="Count" class="input-small">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Status</label>
                        <div class="controls">
                            <select class="span4" name="_unitStatus" id="_unitStatus">
                                <option value="1">Active</option>
                                <option value="0">Suspended</option>
                            </select>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>



    <div class="modal-footer">
        <div id="add-new-unit-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <!--<div class="span3" id="add-new-unit-result"></div>-->
        <button class="btn btn-primary" onclick="addNewUnit()" id="addnewUnitSubmitBut">Add New Unit</button>
        <script>
            doSequence();
        </script>
    </div>
</div>
<div id="editUnit" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idEditUnitName"></div>Edit Unit</h3>
    </div>
    <!--     <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3 id="myModalLabel">Add New Unit</h3>
        </div>-->
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="editUnitForm" name="editUnitForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Name</label>
                        <div class="controls">
                            <input type="text" id="_unitNameE" name="_unitNameE" placeholder="Unit Name" class="input-xlarge">
                            <!--                            <input type="hidden" id="_oprroleidE" name="_oprroleidE"/>
                                                        <input type="hidden" id="_oprstatusE" name="_oprstatusE"/>-->
                            <input type="hidden" id="_unitId" name="_unitId"/>
                            <input type="hidden" id="_oldunitNameE" name="_oldunitNameE"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Threshold Limit</label>
                        <div class="controls">
                            <input type="hidden" id="_thresholdLimitO" name="_thresholdLimitO"/>
                            <input type="text" id="_thresholdLimitE" name="_thresholdLimitE" placeholder="Count" class="input-small">
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="editunit-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>

        <button class="btn btn-primary" onclick="editUnit()" id="buttonEditUnitSubmit">Save Changes</button>

    </div>
</div>


<div id="shiftUnit" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idshiftUnitName"></div>Shift Unit's Operators</h3>
    </div>
    <!--     <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3 id="myModalLabel">Add New Unit</h3>
        </div>-->
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="shiftUnitForm" name="shiftUnitForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Old Unit Name</label>
                        <div class="controls">
                            <input type="text" readonly id="_oldunitNameS" name="_oldunitNameS" placeholder="Unit Name" class="input-xlarge">
                            <!--                            <input type="hidden" id="_oprroleidE" name="_oprroleidE"/>
                                                        <input type="hidden" id="_oprstatusE" name="_oprstatusE"/>-->
                            <input type="hidden" id="_unitIdS" name="_unitIdS"/>
                            <!--<input type="hidden" id="_oldunitNameS" name="_oldunitNameS"/>-->
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">New Unit Name</label>
                        <div class="controls">
                            <select class="span4" name="_unitS" id="_unitS">
                                <%
                                    UnitsManagemet uMngt = new UnitsManagemet();
                                    Units[] uList = uMngt.ListUnitss(sessionid, channel.getChannelid());
                                    if (uList != null) {
                                        for (int i = 0; i < uList.length; i++) {
                                %>
                                <option value="<%=uList[i].getUnitid()%>"><%=uList[i].getUnitname()%></option>
                                <%
                                        }
                                    }
                                %>
                            </select>
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="shiftunit-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>

        <button class="btn btn-primary" onclick="shiftUnit()" id="buttonShiftUnitSubmit">Save Changes</button>

    </div>
</div>


<div id="AssignTokens" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" width='70%' aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idshiftUnitName"></div>Assign Tokens</h3>
    </div>

    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="AssignTokenForm" name="AssignTokenForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Unit Name</label>
                        <div class="controls">
                            <input type="text" readonly id="_oldunitNameA" name="_oldunitNameA" placeholder="Unit Name" class="input-xlarge">
                            <input type="hidden" id="_unitIdA" name="_unitIdA"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Select File:</label>                                    
                        <div class="controls fileupload fileupload-new" data-provides="fileupload">
                            <div class="input-append">
                                <div class="uneditable-input span2"><i class="icon-file fileupload-exists"></i> 
                                    <span class="fileupload-preview"></span>
                                </div>
                                <span class="btn btn-file" >
                                    <!--                                    <span class="fileupload-new">Select file</span>
                                                                        <span class="fileupload-exists">Change</span>-->
                                    <span class="fileupload-new"><i class='icon-file'></i></span>
                                    <span class="fileupload-exists"><i class='icon-file'></i></span>
                                    <input type="file" id="fileFreeTokenToUploadEAD" name="fileFreeTokenToUploadEAD"/>
                                </span>
                                <!--<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>-->

                                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><i class='icon-trash'></i></a>
                                <button class="btn btn-success" id="buttonUploadFreeTokens"  onclick="UploadFreeTokensFile()" >Upload>></button>
                            </div>
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="assign-unit-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>

        <button class="btn btn-primary" onclick="AssignTokenToUnit()" id="buttonAssignTokenUnitSubmit">Assign Tokens</button>

    </div>
</div>

<div id="ShiftFreeHWTokens" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" width='70%' aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idshiftUnitName"></div>Shift Free Tokens</h3>
    </div>

    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="ShiftTokenForm" name="ShiftTokenForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Unit Name</label>
                        <div class="controls">
                            <input type="text" readonly id="_oldunitNameT" name="_oldunitNameT" placeholder="Unit Name" class="input-xlarge">
                            <input type="hidden" id="_unitIdT" name="_unitIdT"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">New Unit Name</label>
                        <div class="controls">
                            <select class="span4" name="_unitIdNameT" id="_unitIdNameT">
                                <%
                                    if (uList != null) {
                                        for (int i = 0; i < uList.length; i++) {
                                %>
                                <option value="<%=uList[i].getUnitid()%>"><%=uList[i].getUnitname()%></option>
                                <%
                                        }
                                    }
                                %>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Select File:</label>                                    
                        <div class="controls fileupload fileupload-new" data-provides="fileupload">
                            <div class="input-append">
                                <div class="uneditable-input span2"><i class="icon-file fileupload-exists"></i> 
                                    <span class="fileupload-preview"></span>
                                </div>
                                <span class="btn btn-file" >
                                    <!--                                    <span class="fileupload-new">Select file</span>
                                                                        <span class="fileupload-exists">Change</span>-->
                                    <span class="fileupload-new"><i class='icon-file'></i></span>
                                    <span class="fileupload-exists"><i class='icon-file'></i></span>
                                    <input type="file" id="fileFreeTokenToUploadEADS" name="fileFreeTokenToUploadEADS"/>
                                </span>
                                <!--<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>-->

                                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><i class='icon-trash'></i></a>
                                <button class="btn btn-success" id="buttonUploadFreeTokensS"  onclick="UploadSifftTokensFile()" >Upload>></button>
                            </div>
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="assign-unit-resultS"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>

        <button class="btn btn-primary" onclick="ShiftTokenToUnit()" id="buttonAssignTokenUnitSubmitS">Assign Tokens</button>

    </div>
</div>
<%@include file="footer.jsp" %>