<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Operators"%>
<%@page import="com.mollatech.axiom.connector.access.controller.AccessMatrixSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.Certificates"%>
<%@page import="java.security.Certificate"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.connector.user.TokenStatusDetails"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Pkitokens"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%
    int SOFTWARE_TOKEN = 1;
    int HARDWARE_TOKEN = 2;
    int OOB_TOKEN = 3;
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();
    String _category = request.getParameter("_changePkiCategory");
    String _changeStatus = request.getParameter("_changePkiStatus");

    String _startDate = request.getParameter("_startDate");
    String _endDate = request.getParameter("_endDate");
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    Date sDate = null;
    Date eDate = null;
    if (_startDate != null && !_startDate.isEmpty()) {
        sDate = formatter.parse(_startDate);
    }
    if (_endDate != null && !_endDate.isEmpty()) {
        eDate = formatter.parse(_endDate);
    }

    int icategory = Integer.parseInt(_category);
    int istatus = Integer.parseInt(_changeStatus);
    PKITokenManagement pObj = new PKITokenManagement();

    Pkitokens[] pkiObj = pObj.searchOtpObjByStatusV2(_channelId, icategory, istatus, sDate, eDate);

    String strerr = "No Record Found";

    String strtoken = null;
    if (icategory == SOFTWARE_TOKEN) {
        strtoken = "Mobile PKI Token";
    } else if (icategory == HARDWARE_TOKEN) {
        strtoken = "Hardware PKI Token";
    }
    int ALL = OTPTokenManagement.TOKEN_STATUS_ALL;
    int SUSPEND = OTPTokenManagement.TOKEN_STATUS_SUSPENDED;
    int ASSIGN = OTPTokenManagement.TOKEN_STATUS_ASSIGNED;
    int LOCKED = OTPTokenManagement.TOKEN_STATUS_LOCKEd;
    int ACTIVE = OTPTokenManagement.TOKEN_STATUS_ACTIVE;
    int FREE = OTPTokenManagement.TOKEN_STATUS_FREE;
    int LOST = OTPTokenManagement.TOKEN_STATUS_LOST;

    String status = null;

    if (istatus == ALL) {
        status = "All States";
    } else if (istatus == SUSPEND) {
        status = "Suspended";
    } else if (istatus == ASSIGN) {
        status = "Assigned";
    } else if (istatus == LOCKED) {
        status = "Locked";
    } else if (istatus == ACTIVE) {
        status = "Active";
    } else if (istatus == FREE) {
        status = "Free";
    } else if (istatus == LOST) {
        status = "Lost";
    }


%>

<h3>Searched Results for Type is <i>"<%=strtoken%>" and State is "<%=status%>"</i></h3>

<div class="tabbable">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#pkicharts" data-toggle="tab">Charts</a></li>
        <li><a href="#pkireport" data-toggle="tab">Tabular List</a></li>
    </ul>
    <div class="tab-content">

        <div class="tab-pane active" id="pkicharts">
            <div class="row-fluid">
                <div class="span12">
                    <div class="control-group">
                        <div class="span4">
                            <div id="pkidonutchart" ></div>
                            Donut Chart
                        </div>
                        <div  class="span8">
                            <div id="pkibarchart"></div>
                            Bar Chart
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="tab-pane" id="pkireport">     
            <div class="row-fluid">
                <div class="span8">
                    <div class="span2">
                        <div class="control-group form-inline">
                            <%Operators oprObjI = (Operators) session.getAttribute("_apOprDetail");
                                AccessMatrixSettings accessObjN = (AccessMatrixSettings) session.getAttribute("_apAccessEntry");
                                if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                    if (accessObjN != null && accessObjN.downloadPkiReport == true) {%>
                            <a href="#" class="btn btn-info" onclick="PKIReport()" >
                                <%} else {%>
                                <a href="#" class="btn btn-info" onclick="InvalidRequestPKIToken('pkireportdownload')" >
                                    <%}
                                    } else {%>
                                    <a href="#" class="btn btn-info" onclick="PKIReport()" >
                                        <%}%>
                                        Download CSV</a>                             
                                    </div>
                                    </div>
                                    <div class="span2">
                                        <div class="control-group form-inline">
                                            <% if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                    if (accessObjN != null && accessObjN.downloadPkiReport == true) {%>
                                            <a href="#" class="btn btn-info" onclick="PKIReportpdf()" >
                                                <%} else {%>
                                                <a href="#" class="btn btn-info" onclick="InvalidRequestPKIToken('pkireportdownload')" >
                                                    <%}
                                                    } else {%>
                                                    <a href="#" class="btn btn-info" onclick="PKIReportpdf()" >
                                                        <%}%>
                                                        Download PDF</a>
                                                    </div>
                                                    </div>
                                                    <div class="span2">
                                                        <div class=" control-group form-inline">
                                                            <% if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                    if (accessObjN != null && accessObjN.downloadPkiReport == true) {%>
                                                            <a href="#" class="btn btn-info" onclick="PKIReportTXT()" >
                                                                <%} else {%>
                                                                <a href="#" class="btn btn-info" onclick="InvalidRequestPKIToken('pkireportdownload')" >
                                                                    <%}
                                                                    } else {%>
                                                                    <a href="#" class="btn btn-info" onclick="PKIReportTXT()" >
                                                                        <%}%>

                                                                        Download TXT</a>
                                                                    </div>
                                                                    </div>                
                                                                    </div>
                                                                    </div>



                                                                    <table class="table table-striped" id="table_main">
                                                                        <tr>
                                                                            <td>No.</td>
                                                                            <td>User Name</td>
                                                                            <td>Mobile</td>
                                                                            <td>Email</td>
                                                                            <td>Serial Number</td>
                                                                            <%if (icategory == HARDWARE_TOKEN) {%>
                                                                            <td>Hardware PKI Token</td>
                                                                            <%} else if (icategory == SOFTWARE_TOKEN) {%>
                                                                            <td>Mobile PKI Token</td>  
                                                                            <%}%>

                                                                            <td>Expires On</td>
                                                                        </tr>
                                                                        <%
                                                                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                                                                            if (pkiObj != null) {
                                                                                for (int i = 0; i < pkiObj.length; i++) {
                                                                                    int iStatus = pkiObj[i].getStatus();
                                                                                    if (icategory != HARDWARE_TOKEN) {
                                                                                        if (iStatus != OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                                                                                            String uID = pkiObj[i].getUserid();
                                                                                            UserManagement uObj = new UserManagement();
                                                                                            //AuthUser[] Users = uObj.SearchUsersByID(sessionId, _channelId, uID);
                                                                                            AuthUser Users = new UserManagement().getUser(sessionId, _channelId, uID);
                                                                                            String softstatus = "Unassigned";
                                                                                            String hardstatus = "Unassigned";
                                                                                            String OOBstatus = "Unassigned";
                                                                                            String OOBtype = "---";
                                                                                            String strLabelHW = "label";    //unassigned
                                                                                            String strLabelOOB = "label";    //unassigned
                                                                                            String strLabelSW = "label";    //unassigned
                                                                                            TokenStatusDetails tokenDetailsOfSoft = null;
                                                                                            TokenStatusDetails tokendetails[] = null;
                                                                                            TokenStatusDetails tokenDetailsOfOOB = null;
                                                                                            TokenStatusDetails tokenDetailsOfHard = null;
                                                                                            tokendetails = pObj.getTokenList(sessionId, _channelId, uID);
                                                                                            if (tokendetails != null) {
                                                                                                for (int j = 0; j < tokendetails.length; j++) {
                                                                                                    if (tokendetails[j].Catrgory == OTPTokenManagement.SOFTWARE_TOKEN) {
                                                                                                        tokenDetailsOfSoft = tokendetails[j];
                                                                                                    } else if (tokendetails[j].Catrgory == OTPTokenManagement.HARDWARE_TOKEN) {
                                                                                                        tokenDetailsOfHard = tokendetails[j];
                                                                                                    }
                                                                                                }
                                                                                                if (tokenDetailsOfSoft != null) {
                                                                                                    //
                                                                                                    if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                                                                                                        strLabelSW += " label-success";   //active
                                                                                                        softstatus = "Active";
                                                                                                    } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
                                                                                                        strLabelSW += " label-warning";   //locked
                                                                                                        softstatus = "Locked";
                                                                                                    } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                                                                                                        strLabelSW += " label-info";   //assigned                
                                                                                                        softstatus = "Assigned";

                                                                                                    } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                                                                                                        softstatus = "Unassigned";

                                                                                                    } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
                                                                                                        strLabelSW += " label-important";   //suspended
                                                                                                        softstatus = "suspended";

                                                                                                    }
                                                                                                }

                                                                                                if (tokenDetailsOfHard != null) {

                                                                                                    if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                                                                                                        hardstatus = "Active";
                                                                                                        strLabelHW += " label-success";   //active
                                                                                                    }

                                                                                                    if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_LOST) {
                                                                                                        hardstatus = "Lost";
                                                                                                        strLabelHW += " label-important";   //suspended

                                                                                                    }

                                                                                                    if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
                                                                                                        hardstatus = "Locked";
                                                                                                        strLabelHW += " label-warning";   //locked

                                                                                                    }
                                                                                                    if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                                                                                                        hardstatus = "Assigned";
                                                                                                        strLabelHW += " label-info";   //assigned                

                                                                                                    }
                                                                                                    if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                                                                                                        hardstatus = "Unassigned";

                                                                                                    }
                                                                                                    if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
                                                                                                        hardstatus = "suspended";
                                                                                                        strLabelHW += " label-important";   //suspended
                                                                                                    }
                                                                                                    if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_FREE) {
                                                                                                        hardstatus = "Free";
                                                                                                        strLabelHW += " label-important";   //suspended

                                                                                                    }
                                                                                                }
                                                                                            }
                                                                        %>


                                                                        <tr>
                                                                            <td><%=(i + 1)%></td> 
                                                                            <%if (Users != null) {%>
                                                                            <td><%=Users.getUserName()%></td>
                                                                            <td><%=Users.getPhoneNo()%></td>
                                                                            <td><%=Users.getEmail()%></td>
                                                                            <%} else {%>
                                                                            <td><span class='label label-Default'>"Not Available"</span></td>
                                                                            <td><span class='label label-Default'>"Not Available"</span></td>
                                                                            <td><span class='label label-Default'>"Not Available"</span></td>
                                                                            <%}%>

                                                                            <td><%=pkiObj[i].getSrno()%></td>
                                                                            <%if (icategory == SOFTWARE_TOKEN) {%>
                                                                            <td><%= softstatus%></td>
                                                                            <%} else if (icategory == HARDWARE_TOKEN) {%>
                                                                            <td><%= hardstatus%></td>
                                                                            <%}%>
                                                                            <td><%= sdf.format(pkiObj[i].getLastaccessdatetime())%></td>
                                                                        </tr>


                                                                        <%}
                                                                        } else if (icategory == HARDWARE_TOKEN) {
                                                                            //                     if (iStatus != OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                                                                            String uID = pkiObj[i].getUserid();
                                                                            UserManagement uObj = new UserManagement();
                                                                            AuthUser[] Users = uObj.SearchUsersByID(sessionId, _channelId, uID);
                                                                            String softstatus = "Unassigned";
                                                                            String hardstatus = "Unassigned";
                                                                            String OOBstatus = "Unassigned";
                                                                            String OOBtype = "---";
                                                                            String strLabelHW = "label";    //unassigned
                                                                            String strLabelOOB = "label";    //unassigned
                                                                            String strLabelSW = "label";    //unassigned
                                                                            TokenStatusDetails tokenDetailsOfSoft = null;
                                                                            TokenStatusDetails tokendetails[] = null;
                                                                            TokenStatusDetails tokenDetailsOfOOB = null;
                                                                            TokenStatusDetails tokenDetailsOfHard = null;
                                                                            tokendetails = pObj.getTokenList(sessionId, _channelId, uID);
                                                                            if (tokendetails != null) {
                                                                                for (int j = 0; j < tokendetails.length; j++) {
                                                                                    if (tokendetails[j].Catrgory == OTPTokenManagement.SOFTWARE_TOKEN) {
                                                                                        tokenDetailsOfSoft = tokendetails[j];
                                                                                    } else if (tokendetails[j].Catrgory == OTPTokenManagement.HARDWARE_TOKEN) {
                                                                                        tokenDetailsOfHard = tokendetails[j];
                                                                                    }
                                                                                }
                                                                                if (tokenDetailsOfSoft != null) {
                                                                                    //
                                                                                    if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                                                                                        strLabelSW += " label-success";   //active
                                                                                        softstatus = "Active";
                                                                                    } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
                                                                                        strLabelSW += " label-warning";   //locked
                                                                                        softstatus = "Locked";
                                                                                    } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                                                                                        strLabelSW += " label-info";   //assigned                
                                                                                        softstatus = "Assigned";

                                                                                    } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                                                                                        softstatus = "Unassigned";

                                                                                    } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
                                                                                        strLabelSW += " label-important";   //suspended
                                                                                        softstatus = "suspended";

                                                                                    }
                                                                                }

                                                                                if (tokenDetailsOfHard != null) {

                                                                                    if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                                                                                        hardstatus = "Active";
                                                                                        strLabelHW += " label-success";   //active
                                                                                    }

                                                                                    if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_LOST) {
                                                                                        hardstatus = "Lost";
                                                                                        strLabelHW += " label-important";   //suspended

                                                                                    }

                                                                                    if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
                                                                                        hardstatus = "Locked";
                                                                                        strLabelHW += " label-warning";   //locked

                                                                                    }
                                                                                    if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                                                                                        hardstatus = "Assigned";
                                                                                        strLabelHW += " label-info";   //assigned                

                                                                                    }
                                                                                    if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                                                                                        hardstatus = "Unassigned";

                                                                                    }
                                                                                    if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
                                                                                        hardstatus = "suspended";
                                                                                        strLabelHW += " label-important";   //suspended
                                                                                    }
                                                                                    if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_FREE) {
                                                                                        hardstatus = "Free";
                                                                                        strLabelHW += " label-important";   //suspended

                                                                                    }
                                                                                }

                                                                            }
                                                                        %>


                                                                        <tr>
                                                                            <td><%=(i + 1)%></td> 
                                                                            <%if (Users != null) {%>
                                                                            <td><%=Users[0].getUserName()%></td>
                                                                            <td><%=Users[0].getPhoneNo()%></td>
                                                                            <td><%=Users[0].getEmail()%></td>
                                                                            <%} else {%>
                                                                            <td><span class='label label-Default'>"Not Available"</span></td>
                                                                            <td><span class='label label-Default'>"Not Available"</span></td>
                                                                            <td><span class='label label-Default'>"Not Available"</span></td>
                                                                            <%}%>

                                                                            <%if (pkiObj[i].getSrno() != null) {%>
                                                                            <td><%=pkiObj[i].getSrno()%></td>
                                                                            <%} else {%>
                                                                            <td><span class='label label-Default'>"Not Available"</span></td>
                                                                            <%}%>

                                                                            <%if (icategory == SOFTWARE_TOKEN) {%>
                                                                            <td><%= softstatus%></td>
                                                                            <%} else if (icategory == HARDWARE_TOKEN) {%>
                                                                            <td><%= hardstatus%></td>
                                                                            <%}%>
                                                                            <td><%=sdf.format(pkiObj[i].getLastaccessdatetime())%></td>
                                                                        </tr>


                                                                        <%}
                                                                            }

                                                                        } else {%>
                                                                        <td><%=1%></td>
                                                                        <td><%= strerr%></td>
                                                                        <td><%= strerr%></td>
                                                                        <td><%= strerr%></td>
                                                                        <td><%= strerr%></td>
                                                                        <!--<td><%= strerr%></td>-->
                                                                        <%if (icategory == SOFTWARE_TOKEN) {%>
                                                                        <td><%= strerr%></td>
                                                                        <%} else if (icategory == HARDWARE_TOKEN) {%>
                                                                        <td><%= strerr%></td>
                                                                        <%}%>
                                                                        <td><%= strerr%></td>
                                                                        <%}%>

                                                                    </table>


                                                                    <div class="row-fluid">
                                                                        <div class="span8">
                                                                            <div class="span2">
                                                                                <div class="control-group form-inline">

                                                                                    <%   if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                            if (accessObjN != null && accessObjN.downloadPkiReport == true) {%>
                                                                                    <a href="#" class="btn btn-info" onclick="PKIReport()" >
                                                                                        <%} else {%>
                                                                                        <a href="#" class="btn btn-info" onclick="InvalidRequestPKIToken('pkireportdownload')" >
                                                                                            <%}
                                                                                            } else {%>
                                                                                            <a href="#" class="btn btn-info" onclick="PKIReport()" >
                                                                                                <%}%>
                                                                                                Download CSV</a>

                                                                                            </div>
                                                                                            </div>
                                                                                            <div class="span2">
                                                                                                <div class="control-group form-inline">                              
                                                                                                    <% if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                            if (accessObjN != null && accessObjN.downloadPkiReport == true) {%>
                                                                                                    <a href="#" class="btn btn-info" onclick="PKIReportpdf()" >
                                                                                                        <%} else {%>
                                                                                                        <a href="#" class="btn btn-info" onclick="InvalidRequestPKIToken('pkireportdownload')" >
                                                                                                            <%}
                                                                                                            } else {%>
                                                                                                            <a href="#" class="btn btn-info" onclick="PKIReportpdf()" >
                                                                                                                <%}%>
                                                                                                                Download PDF</a>
                                                                                                            </div>
                                                                                                            </div>

                                                                                                            <div class="span2">
                                                                                                                <div class="control-group form-inline">
                                                                                                                    <% if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                                            if (accessObjN != null && accessObjN.downloadPkiReport == true) {%>
                                                                                                                    <a href="#" class="btn btn-info" onclick="PKIReportTXT()" >
                                                                                                                        <%} else {%>
                                                                                                                        <a href="#" class="btn btn-info" onclick="InvalidRequestPKIToken('pkireportdownload')" >
                                                                                                                            <%}
                                                                                                                            } else {%>
                                                                                                                            <a href="#" class="btn btn-info" onclick="PKIReportTXT()" >
                                                                                                                                <%}%>Download TXT</a>
                                                                                                                            </div>
                                                                                                                            </div>
                                                                                                                            </div>
                                                                                                                            </div>
                                                                                                                            </div>
                                                                                                                            </div>
                                                                                                                            <br><br>

