<%@page import="com.mollatech.axiom.nucleus.db.Units"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet"%>
<%@page import="com.mollatech.axiom.nucleus.db.Otptokens"%>
<%@page import="java.text.Normalizer.Form"%>
<%@page import="com.itextpdf.text.Document"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.connector.user.TokenStatusDetails"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>

<%@include file="header.jsp" %>
<script src="./assets/js/jquery.js"></script>
<script src="./assets/js/otptokens.js"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">
<script src="./assets/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="./assets/css/datepicker.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<div class="container-fluid">
    <h1 class="text-success">Summary Report</h1>
    <h3>Make Your Own Report</h3>
    <div class="row-fluid">
        <div class="span12">
            <div class="control-group form-inline">
                <div class="input-prepend">
                    <span class="add-on">From:</span>   
                    <div id="datetimepicker1" class="input-append date">
                        <input id="startdate" name="startdate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                </div>
                <div class="input-prepend">
                    <!--<div class="well">-->
                    <span class="add-on">till:</span>   
                    <div id="datetimepicker2" class="input-append date">
                        <input id="enddate" name="enddate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                    <!--</div>-->
                </div>
                <select class="selectpicker" name="_category" id="_category"  class="span6">
                    <option value="0">Success</option>
                    <option value="1">Failed</option>
                </select>
                <div class="input-prepend">
                    <button class="btn btn-success" onclick="generatesummaryReport()" id="addUserButton">Generate Report Now</button>
                </div>


            </div>
            <div id="summary_table_main_MSG">
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $('#datetimepicker1').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            });
        });
        $(function () {
        $('#datetimepicker2').datepicker({
        format: 'dd/mm/yyyy',
                language: 'pt-BR'

        });  });
    </script>
    
</div>
<script>
             function generatesummaryReport() {

                if (document.getElementById('startdate').value.length === 0 || document.getElementById('enddate').value.length === 0) {
                Alert4Tokens("<span><font color=red>Date Range is not selected!!!</font></span>");
                        return;
                }
                var val1 = document.getElementById('startdate').value;
                var val2 = document.getElementById('enddate').value;
                var val3 = document.getElementById('_category').value
                $('#summary_table_main_MSG').html("<h3>Loading....</h3>");
                        var s = './summaryReportTable.jsp?_category=' + val3 + "&_startdate=" + val1 + "&_enddate=" + val2;
                        $.ajax({
                        type: 'GET',
                                url: s,
                                success: function(data) {
                                $('#summary_table_main_MSG').html(data);
                                }
                        } );
                    }
</script>
<%@include file="footer.jsp" %>