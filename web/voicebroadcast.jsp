
<%@page import="com.mollatech.axiom.common.utils.UtilityFunctions"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.settings.ContactTagsSetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Templates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%--<%@page import="com.mollatech.axiom.nucleus.db.Tags"%>
<%@page import="com.mollatech.dictum.management.TagsManagement"%>--%>
<%@include file="header.jsp" %>
<script src="./assets/js/dictum/bulkmsg.js"></script>
<div class="container-fluid">
    <h2>BroadCast Voice Call </h2>
    <p>You can send voice messages to large set of contacts</p>
    <br>
    <div class="row-fluid">
        <form class="form-horizontal" id="voicebroadcast" name="voicebroadcast">
            <fieldset>
                <%
                    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
                    Channels channelOnj = (Channels) session.getAttribute("_apSChannelDetails");
//                    TagsManagement tagmngObj = new TagsManagement();
//                    Tags[] tagsObj = tagmngObj.getAllTags(channelOnj.getChannelid());

                    SettingsManagement smng = new SettingsManagement();
                    ContactTagsSetting tagssettingObj = (ContactTagsSetting) smng.getSetting(sessionId, channelOnj.getChannelid(), SettingsManagement.CONTACT_TAGS, SettingsManagement.PREFERENCE_ONE);

                    String[] tags = tagssettingObj.getTags();


                    String url = (String) session.getAttribute("_url");
                
//                    TemplateManagement tempObj = new TemplateManagement();
//                    Templates[] templates = tempObj.Listtemplates(sessionId, channelOnj.getChannelid());

                %>

                <div class="control-group">
                    <label class="control-label"  for="toNumber">Select TAG</label>
                    <div class="controls">
                        <select class="selectpicker" name="_tagID" id="_tagID">
                            <option value=".." >..</option>                                             

                            <% for (int j = 0; j < tags.length; j++) {%>

                            <option data-content="<span class='label label-success'><%=j%></span>" value="<%=tags[j]%>"><%=tags[j]%></option>                                             
                            <%}
                            %>
                        </select>

                    </div>
                </div>





<!--                <div class="control-group">
                    <label class="control-label"  for="speed">Send as </label>
                    <div class="controls">
                        <select name="_type" id="_type">
                            <option value=".." >..</option>                                             
                            <option value="1" >SMS Message</option>
                            <option value="2" >USSD Message</option>
                            <option value="3" >Voice Message</option>                            
                        </select>
                        with speed       
                        <select name="_speed" id="_speed">
                            <option value=".." >..</option>                                             
                            <option value="1" >Slow</option>
                            <option value="3" >Normal</option>
                            <option value="10" >Fast</option>                            
                            <option value="30" >Hyper Fast</option>                            
                        </select>
                    </div>
                </div>-->

<!--                <div class="control-group">
                    <label class="control-label"  for="messagebody">Message</label>
                    <div class="controls">
                        <textarea id="_messagebody" name="_messagebody" placeholder="Message Body" class="span9" cols="60" rows="3"></textarea>
                        <br>Attention: #name#,#date#,#datetime#,#email# and #phone# are going to get replaced with real values.
                    </div>
                </div>-->

                <!-- Submit -->
                <div class="control-group">
                    <div class="controls">
                        <button class="btn btn-primary btn-large" onclick="BroadCast(<%=url%>)" type="button">Send Bulk Messages Now >></button>
                        <div id="bulk-sms-gateway-result"></div>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>


    <script language="javascript" type="text/javascript">
        ChangeSpeed(1);
        //  ChangeFormat(1);
    </script>

    <%@include file="footer.jsp" %>