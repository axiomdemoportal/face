<%@page import="java.net.URLEncoder"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.Templates"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.operation.TemplateVariables"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/template.js"></script>
<%
    String _sessionID = (String) session.getAttribute("_apSessionID");

    SessionManagement smObj = new SessionManagement();
    int iStatus = smObj.GetSessionStatus(_sessionID);
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");

    Templates[] templatesObj = null;
    if (iStatus == 1) { //active
        smObj.UpdateSession(_sessionID);
        TemplateManagement cmObj = new TemplateManagement();
        templatesObj = cmObj.ListTypeTemplates(_sessionID, channel.getChannelid(), 2);
        cmObj = null;
    }
%>
<div class="container-fluid">
    <h1 class="text-success">Email Templates</h1>
    <div class="row-fluid">
        <div id="licenses_data_table">
            <table class="display responsive wrap" id="EmailListTable">
                <thead>
                    <tr>
                        <td><b>No</td>
                        <td><b>Unique Name</td>
                        <td><b>Subject</td>
                        <!--<td>Variables</td>-->
                        <td><b>Manage</td>
                        <%//if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
                        <td><b>Status</td>
                        <%//}%>
                        <td><b>Created On</td>
                        <td><b>Last Updated On</td>                    
                    </tr>
                </thead>

                <%
                    out.flush();
                    TemplateManagement tmObj = new TemplateManagement();

                    for (int i = 0; i < templatesObj.length; i++) {
                        Templates axcObj = templatesObj[i];

                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                        String TypeoF = null;
                        if (axcObj.getType() == 1) {
                            TypeoF = "Mobile";
                        } else {
                            TypeoF = "Email";
                        }

                        String strStatus = "Active";
                        if (axcObj.getStatus() == 1) {
                            strStatus = "Active";
                        } else {
                            strStatus = "Suspended";
                        }
                        String userStatus = "user-status-value-" + i;
                %>
                <tr>
                    <td><%=(i + 1)%></td>
                    <td><%=axcObj.getTemplatename()%></td>
                    <td>
                        <!--<a href="#" class="btn btn-mini" id="emailTemplateSubject-<%=i%>"  rel="popover" data-html="true">Click to View</a>-->
                        <a href="#" class="btn btn-mini" id="emailTemplateSubject-<%=i%>" data-toggle="popover" data-trigger="focus"  rel="popover" data-html="true">Click to View</a> 
                        <script>
                            $(function ()
                            {
                                $("#emailTemplateSubject-<%=i%>").popover({title: 'Subject', content: "<%=axcObj.getSubject()%>"});
                            });
                            $("a[rel=popover]")
                                    .click(function (e) {
                                        e.preventDefault();
                                    });
                        </script>

                    </td>
                    <!--<td>
                        <a href="#" class="btn btn-mini" id="emailTemplate-<%=i%>"  rel="popover" data-html="true">Click to View</a>
                        <script>
                            $(function ()
                            { $("#emailTemplate-<%=i%>").popover({title: 'Variables', content: "<%=axcObj.getTemplatevariables()%>"});
                            });
                        </script>
                    </td>-->
                    <td>
                        <div class="btn-group">
                            <button class="btn btn-mini">Manage</button>
                            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="./TemplateEmailEdit.jsp?_tid=<%=axcObj.getTemplateid()%>" data-toggle="modal">Edit Message Template</a></li>
                                    <%//if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >=3 ) {%>
    <!--                            <li><a href="#" onclick="removeEmailTemplate(<%=axcObj.getTemplateid()%>)"><font color="red">Remove Templates?</font></a></li>-->
                                <%// } %>
                            </ul>
                        </div>
                    </td>   
                    <%//if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
                    <td>
                        <div class="btn-group">
                            <button class="btn btn-mini" id="<%=userStatus%>"><%=strStatus%></button>
                            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#"  onclick="changetemplatestatus(<%=axcObj.getTemplateid()%>, 1, '<%=userStatus%>')" >Mark as Active?</a></li>
                                <li><a href="#" onclick="changetemplatestatus(<%=axcObj.getTemplateid()%>, 0, '<%=userStatus%>')" >Mark as Suspended?</a></li>
                            </ul>
                        </div>
                    </td>
                    <%// }%>

                    <td><%=sdf.format(axcObj.getCreatedOn())%>
                    <td><%=sdf.format(axcObj.getLastupDatedOn())%></td>

                </tr>
                <%}%>
            </table>
        </div>
    </div>
    <br>

    <script language="javascript">
        //listChannels();
    </script>

    <script>
        $(document).ready(function () {
            $('#EmailListTable').DataTable({
                responsive: true
            });
        });
    </script>


</div>
<!-- Modal -->

<%@include file="footer.jsp" %>