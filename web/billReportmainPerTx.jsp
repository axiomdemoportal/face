<%@page import="com.mollatech.axiom.nucleus.db.connector.management.BillingManagment"%>
<%@page import="com.mollatech.axiom.nucleus.db.Geotrack"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.MobileTrustManagment"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.GeoLocationManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>
<%@page import="com.mollatech.axiom.nucleus.settings.GlobalChannelSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/jquery.js"></script>
<script src="./assets/js/billreport.js"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/bootstrap-datepicker.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/css/datepicker.css">
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">


<div class="container-fluid">
    <h1 class="text-success">Billing Report</h1>
    <!--<h3>Make Your Own Report</h3>-->
    <br>
    <div class="row-fluid">
        <div class="span12">
            <div class="control-group form-inline">

                <div class="input-prepend">

                    <!--<div class="well">-->
                    <span class="add-on">From:</span>   
                    <div id="datetimepicker1" class="input-append date">
                        <input id="startdate" name="startdate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                    <!--</div>-->
                </div>
                <div class="input-prepend">
                    <!--<div class="well">-->
                    <span class="add-on">till:</span>   
                    <div id="datetimepicker2" class="input-append date">
                        <input id="enddate" name="enddate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                    <!--</div>-->
                </div>
                <select class="selectpicker" name="_changeCategory" id="_changeCategory"  class="span3">
                    <option  value="<%=0%>">All</option>  
                    <option  value="<%=BillingManagment.OOB_TOKEN%>">Out Of Band One Time Password Token</option>   
                    <option  value="<%=BillingManagment.SOFTWARE_TOKEN%>">Software One Time Password Token</option>       
                    <option  value="<%=BillingManagment.HARDWARE_TOKEN%>">Hardware One Time Password Token</option>                           
                    <option  value="<%=BillingManagment.SW_PKI_TOKEN%>">Software PKI Token</option>   
                    <option  value="<%=BillingManagment.HW_PKI_TOKEN%>">Hardware PKI Token</option>   
                    <option  value="<%=BillingManagment.CERTIFICATE%>">Digital Certificate</option> 

                </select>
                <!--<button class="btn btn-success" onclick="searchBillTrackByType(2)" id="addUserButton">Generate Report Now</button>-->
                <%if (oprObj.getRoleid() != 1) {
                        if (accessObj != null && accessObj.viewtranscationBaseBillingReport == true) {%>
                <button class="btn btn-success" onclick="searchBillTrackByTypeV1(2)" id="addUserButton">Generate Report Now</button>
                <%} else {%>
                <button class="btn btn-success " onclick="InvalidRequestBillReport('billreporttx')" type="button">Generate Report Now</button>
                <%}
                } else {%>
                <button class="btn btn-success" onclick="searchBillTrackByTypeV1(2)" id="addUserButton">Generate Report Now</button>
                <%}%>
            </div>
            <div id="licenses_data_table">
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datepicker({
                format: 'dd/MM/yyyy',
                language: 'pt-BR'
            });
        });
        $(function () {
            $('#datetimepicker2').datepicker({
                format: 'dd/MM/yyyy',
                language: 'pt-BR'

            });
        });
//        ChangeMediaType(0);

    </script>




</div>

<%@include file="footer.jsp" %>