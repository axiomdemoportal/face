


<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Properties"%>



<div id="dbsetting-result"></div>

<div class="container-fluid">
    <h1 class="text-success"> DB Setting Details</h1>
   
     <form class="form-signin" method="POST" action="#" id="SaveForm" name="SaveForm">
    <table class="table table-striped">

        <%    //String _sessionID = (String) session.getAttribute("_apSessionID");
            Properties prop = (Properties) session.getAttribute("propObj");
             String str= (String) session.getAttribute("filepath");
            Enumeration enamObj = prop.propertyNames();
            int i=1;
            while (enamObj.hasMoreElements()) {
                
                String key = (String) enamObj.nextElement();
                String value = (String) prop.getProperty(key);


        %>
        
        
        <tr>
            <td>
                <b><label><%=key%></label></b>
            </td>
            <td>
                <input type="text" name="<%= key%>" value="<%=value%>"/>
            </td> 
        </tr>

        <%
        i=i+1;
            }
            
        %>  

    </table>
         
            <input type="hidden" name="path" id="path" value="<%=str%>">
            <button class="btn btn-large btn-primary" onclick="savedbsetting()" type="button" >SAVE CHANGES</button>
     </form>
             
            <a href="index.jsp">back</a>
</div>   



            
            <script src="./assets/js/jquery.js"></script>
        <script src="./assets/js/bootstrap-transition.js"></script>
        <script src="./assets/js/bootstrap-alert.js"></script>
        <script src="./assets/js/bootstrap-modal.js"></script>
        <script src="./assets/js/bootstrap-dropdown.js"></script>
        <script src="./assets/js/bootstrap-scrollspy.js"></script>
        <script src="./assets/js/bootstrap-tab.js"></script>
        <script src="./assets/js/bootstrap-tooltip.js"></script>
        <script src="./assets/js/bootstrap-popover.js"></script>
        <script src="./assets/js/bootstrap-button.js"></script>
        <script src="./assets/js/bootstrap-collapse.js"></script>
        <script src="./assets/js/bootstrap-carousel.js"></script>
        <script src="./assets/js/bootstrap-typeahead.js"></script>
        <script src="./assets/js/login.js"></script>

        <link rel="stylesheet" href="./assets/css/datepicker.css">
        <link rel="stylesheet" href="./assets/css/jquery.sidr.dark.css">

        <script src="./assets/js/json_sans_eval.js"></script>
        <script src="./assets/js/bootbox.min.js"></script>


        <script language="javascript">
//                        TellTimezone();
        </script>
    
