<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%@include file="header.jsp" %>
<%

//    if ( Integer.valueOf((String)session.getAttribute("_apOprRole")).intValue() < 3) {
//        return;
//    }    

    String _sessionID = (String) session.getAttribute("_apSessionID");

    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    SessionManagement smObj = new SessionManagement();
    int iStatus = smObj.GetSessionStatus(_sessionID);

    AxiomChannel channelsObj = null;
    if (iStatus == 1) { //active
        smObj.UpdateSession(_sessionID);
        ChannelManagement cmObj = new ChannelManagement();
        channelsObj = cmObj.GetChannel(_sessionID, channel.getName());
        cmObj = null;
    }
    
    
%>
<%
    out.flush();
    ChannelManagement cmObj = new ChannelManagement();

    UserManagement umObj = new UserManagement();


    AxiomChannel axcObj = channelsObj;

    int iChStatus = axcObj.getiStatus();
    String strStatus;
    if (iChStatus == 1) {
        strStatus = "Active";
    } else {
        strStatus = "Suspended";
    }

    RemoteAccessManagement rmObj = new RemoteAccessManagement();
    boolean bRAEnabled = rmObj.IsRemoteAccessEnabled(axcObj.getStrChannelid());

    java.util.Date dCreatedOn = new java.util.Date(axcObj.utcCreatedOn);
    java.util.Date dLastUpdated = new java.util.Date(axcObj.lastUpdateOn);

    String uidiv4RemoteAccess = "channel-ra-value-";
    String strRemoteAccessStatus = "Enabled";
    if (bRAEnabled == false) {
        strRemoteAccessStatus = "Disabled";
    }

    String uidiv4ChStatus = "channel-status-value-";

    SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm ");

    int userCount = 0;
    int otptokenCount = 0;
    userCount = umObj.getCountOfUsers(axcObj.getStrChannelid());
        OTPTokenManagement otpmObj = new OTPTokenManagement(axcObj.getStrChannelid());
    otptokenCount = otpmObj.getOTPTokenCount(axcObj.getStrChannelid());
    
    int pkiCount=0;
    PKITokenManagement pkimngObj = new PKITokenManagement();                                
    pkiCount = pkimngObj.getPKITokenCount(axcObj.getStrChannelid());
    int certCount=0;
    CertificateManagement certmngObj = new CertificateManagement();
    certCount = certmngObj.getCertificateCount(axcObj.getStrChannelid());

    
%>

<div class="container-fluid">
    <h1> /<%=axcObj.getStrName()%> </h1>
    <hr>
    <h4>1. Channel Id: <%= axcObj.getStrChannelid()%></h4>
    <h4>2. Interface Path: "/<%= axcObj.getStrVirtualPath()%>"</h4>
    <% if (axcObj.getStrVirtualPath().compareToIgnoreCase("face") != 0 ) { %> 
    <h4>3. Web Service: "/<%= axcObj.getStrVirtualPath()%>core/AxiomCoreInterfaceImpl?wsdl"</h4>
    <% } else { %>
    <h4>3. Web Service: "/core/AxiomCoreInterfaceImpl?wsdl"</h4>
    <% } %>
<!--    <h4>4. Remote Access: <%= strRemoteAccessStatus%>, change to <div class="btn-group">
            <button class="btn btn-mini" id="<%=uidiv4RemoteAccess%>"><%=strRemoteAccessStatus%></button>
            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
            <ul class="dropdown-menu">
                <li><a href="#" onclick="ChangeChannelRemoteAccess('<%=axcObj.getStrChannelid()%>',1,'<%=uidiv4RemoteAccess%>')" >Enable Remote Access?</a></li>
                <li><a href="#" onclick="ChangeChannelRemoteAccess('<%=axcObj.getStrChannelid()%>',0,'<%=uidiv4RemoteAccess%>')" >Disable Remove Access?</a></li>
            </ul>
        </div></h4>-->
    <h4>5. Total Sessions till date are <%=cmObj.getTotalSessions(axcObj.getStrChannelid())%></h4>
    <h4>6. Total Operators are  <%=cmObj.getOperatorCount(axcObj.getStrChannelid())%></h4>
    <hr>
    <h4>7. Total Users till date are <%=userCount%></h4>
    <h4>8. Total OTP Tokens issued are <%=otptokenCount%></h4>
    <h4>9. Total PKI Tokens issued are <%=pkiCount%></h4>
    <h4>10. Total Certificates issued are <%=certCount%></h4>
    <hr>
    <h4>Channel Creation Date is <%=sdf.format(dCreatedOn)%></h4>
    <h4>Last Updated Date is <%=sdf.format(dLastUpdated)%></h4>
</div>

<%
      Object _apIsAccess = session.getAttribute("_apIsAccess");
     // boolean _bapIsAccess = true;
//      if(_apIsAccess != null){
//          _bapIsAccess = _apIsAccess;
//      }
//      _apIsAccess = "5";
if(_apIsAccess != null){
    _apIsAccess = null;
    session.removeAttribute("_apIsAccess");
%>
<script>
    Alert4Channel("<span><font color=red>" + "Invalid Request!!!" + "</font></span>");
</script>
<%}
%>


<%@include file="footer.jsp" %>