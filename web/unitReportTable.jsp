
<%@page import="com.mollatech.axiom.nucleus.db.operation.AxiomOperator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.mollatech.axiom.nucleus.db.operation.UnitReport"%>
<%@page import="java.util.List"%>
<%@page import="com.mollatech.axiom.connector.access.controller.AccessMatrixSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.Audit"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.AuditManagement"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.LoadSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.Operators"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.connector.communication.AXIOMStatus"%>
<%@page import="com.mollatech.axiom.nucleus.settings.SendNotification"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.TemplateUtils"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.db.Templates"%>
<%@page import="com.mollatech.axiom.nucleus.db.operation.TemplateNames"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%@page import="com.mollatech.axiom.nucleus.settings.TokenSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet"%>
<%@page import="com.mollatech.axiom.nucleus.db.Units"%>
<script src="assets/js/usermanagement.js" type="text/javascript"></script>
<script src="./assets/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="./assets/css/jquery.dataTables.min.css">
<script src="assets/DataTable/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="assets/js/operators.js" type="text/javascript"></script>
<%  String _sessionID = (String) session.getAttribute("_apSessionID");
    SessionManagement smObj = new SessionManagement();
    TemplateManagement tempObj = new TemplateManagement();
//     OperatorsManagement s = new OperatorsManagement();
    int iStatus = smObj.GetSessionStatus(_sessionID);
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    String _searchtext = request.getParameter("_searchtext");
    String startDate = request.getParameter("startdate");
    Date sDate = null;
    if(!startDate.equals(null) && !startDate.isEmpty()){
    sDate = new SimpleDateFormat("dd/MM/yyyy").parse(startDate);
     sDate.setHours(00);
    sDate.setMinutes(00);
    sDate.setSeconds(00);
    }
    String enddate = request.getParameter("enddate");
    Date eDate = new SimpleDateFormat("dd/MM/yyyy").parse(enddate);
//    String _status = request.getParameter("status");
//    int status = Integer.parseInt(_status);
    OperatorsManagement oMngt = new OperatorsManagement();
    OTPTokenManagement otpMngt = new OTPTokenManagement(channel.getChannelid());
    SettingsManagement sMngt = new SettingsManagement();
    SendNotification send = new SendNotification();
    String strerr = "No record found.";
    Units [] unitsObj = null;
//    Date sDate = new Date(startDate);
//    Date eDate = new Date(enddate);
   
    eDate.setHours(23);
    eDate.setMinutes(59);
    eDate.setSeconds(59);
    if (iStatus == 1) { //active
        smObj.UpdateSession(_sessionID);
        UnitsManagemet cmObj = new UnitsManagemet();
        unitsObj = cmObj.getAllnUnitByName(_sessionID, channel.getChannelid(), _searchtext);
        cmObj = null;
    }
%>
<div id="_unitReportTable">
    <h3>Searched Results for <i>"<%=_searchtext%>"</i></h3>

    <div class="tabbable">
        <ul class="nav nav-tabs" id="userReportTab">
            <li class="active"><a href="#userreport" data-toggle="tab">Tabular List</a></li>
            <!--<li><a href="#userreport" data-toggle="tab">Tabular List</a></li>-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="userreport">            
                <div class="row-fluid">
                    <div class="span6">
                        <div class="control-group">                        
                            <div class="span1">
                                <div class="control-group form-inline">
                                    <%Operators oprObjI = (Operators) session.getAttribute("_apOprDetail");
                                        AccessMatrixSettings accessObjN = (AccessMatrixSettings) session.getAttribute("_apAccessEntry");%>
                                    <%if (oprObjI.getRoleid() != 1) {%> 
                                    <%if (accessObjN != null && accessObjN.downloadUnitReports == true) {%>
                                    <a href="#" class="btn btn-info" onclick="UnitRoleReportDownload(1)">
                                        <%} else {%>
                                        <a href="#" class="btn btn-info" onclick="InvalidRequest('unitroledown')" >

                                            <%}
                                            } else {%>
                                            <a href="#" class="btn btn-info" onclick="UnitRoleReportDownload(1)" >
                                                <%}%>
                                                <i class="icon-white icon-chevron-down"></i> CSV</a>
                                            </div>
                                            </div>
                                            <div class="span1">
                                                <div class="control-group form-inline">
                                                    <%if (oprObjI.getRoleid() != 1) {%> 
                                                    <%if (accessObjN != null && accessObjN.downloadUnitReports == true) {%>
                                                    <a href="#" class="btn btn-info" onclick="UnitRoleReportDownload(0)" >
                                                        <%} else {%>
                                                        <a href="#" class="btn btn-info" onclick="InvalidRequest('unitroledown')" >
                                                            <%}
                                                            } else {%>

                                                            <a href="#" class="btn btn-info" onclick="UnitRoleReportDownload(0)" >
                                                                <%}%>

                                                                <i class="icon-white icon-chevron-down"></i> PDF</a>
                                                            </div>
                                                            </div>
                                                            <div class="span1">
                                                                <div class="control-group form-inline">
                                                                    <%if (oprObjI.getRoleid() != 1) {%> 
                                                                    <%if (accessObjN != null && accessObjN.downloadUnitReports == true) {%>
                                                                    <a href="#" class="btn btn-info" onclick="UnitRoleReportDownload(2)" >
                                                                        <%} else {%>
                                                                        <a href="#" class="btn btn-info" onclick="InvalidRequest('unitroledown')" >
                                                                            <%}
                                                                            } else {%>
                                                                            <a href="#" class="btn btn-info" onclick="UnitRoleReportDownload(2)" >
                                                                                <%}%>
                                                                                <i class="icon-white icon-chevron-down"></i> TXT</a>
                                                                            </div>
                                                                            </div>

                                                                            </div>
                                                                            </div>
                                                                            </div>
                                                                            <table class="table table-striped" id="table_unit">
                                                                                <thead>
                                                                                <th><b>No</th>
                                                                                <th><b>Name</th>
                                                                                <th><b>Email</th>
                                                                                <th><b>Phone No</th>
                                                                                <th><b>Unit</th>
                                                                                <th><b>Status</th>
                                                                                <th><b>Role</th>
                                                                                <th><b>Attempts</th> 
                                                                                <th><b>Access From</th>
                                                                                <th><b>Audit</th>
                                                                                <th><b>Expires</th>
                                                                                <th><b>Created On</th>
                                                                                </thead>

                                                                                <%
                                                                                    int l = 0;
                                                                                    DateFormat format1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                                                                                    List<UnitReport> dataList = new ArrayList<UnitReport>();
                                                                                    if (unitsObj != null) {
                                                                                       for(int k = 0; k < unitsObj.length; k++){
                                                                                        OperatorsManagement op = new OperatorsManagement();
                                                                                        AxiomOperator[] operator = null;
                                                                                        operator = op.getOperatorByUnitAndNameAndDate(_sessionID, channel.getChannelid(), unitsObj[k].getUnitid(), sDate, eDate);
                                                                                        
                                                                                        if (operator != null) {
                                                                                            for (int i = 0; i < operator.length; i++) {
                                                                                                
                                                                                                String access = "NA";
                                                                                                String expire = "NA";
                                                                                                String create = "NA";
                                                                                                UnitReport report = new UnitReport();
                                                                                                int iOprStatus = operator[i].getiStatus();
                                                                                                int iOprAccessType = operator[i].getiAccessType();
                                                                                                String strStatus = null;
                                                                                                if (iOprStatus == 1) {
                                                                                                    strStatus = "Active";
                                                                                                } else if (iOprStatus == -1) {
                                                                                                    strStatus = "Locked";
                                                                                                } else if (iOprStatus == 0) {
                                                                                                    strStatus = "Suspended";
                                                                                                } else if (iOprStatus == -99) {
                                                                                                    strStatus = "Deleted";
                                                                                                }

                                                                                                String strOprAccessType = "Not Set";
                                                                                                if (iOprAccessType == 1) {
                                                                                                    strOprAccessType = "Operator";
                                                                                                } else {
                                                                                                    strOprAccessType = "Role";
                                                                                                }
                                                                                                if ((operator[i].getAccessStartFrom()) != null) {
                                                                                                    //                                DateFormat format1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                                                                                                    access = format1.format(operator[i].getAccessStartFrom());
                                                                                                }
                                                                                                if ((operator[i].getAccessTill()) != null) {
                                                                                                    //                                DateFormat format1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                                                                                                    expire = format1.format(operator[i].getAccessTill());
                                                                                                }
                                                                                                if (String.valueOf(operator[i].getUtcCreatedOn()) != null) {
                                                                                                    //                                DateFormat format1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                                                                                                    create = format1.format(operator[i].getUtcCreatedOn());
                                                                                                }
                                                                                                report.operator_name = (operator[i].getStrName());
                                                                                                report.email = operator[i].getStrEmail();
                                                                                                report.phone = operator[i].getStrPhone();
                                                                                                report.unit = unitsObj[k].getUnitname();
                                                                                                report.manage = strStatus;
                                                                                                report.role = operator[i].getStrRoleName();
                                                                                                report.attempts = String.valueOf(operator[i].getiWrongAttempts());
                                                                                                report.accessfrom = access;
                                                                                                report.expire = expire;
                                                                                                report.createdOn = create;
                                                                                                dataList.add(report);
                                                                                                l++;
                                                                                %>
                                                                                <tr>
                                                                                    <td><%=l%></td>
                                                                                    <td><%=operator[i].getStrName()%></td>
                                                                                    <td><%=operator[i].getStrEmail()%></td>
                                                                                    <td><%=operator[i].getStrPhone()%></td>
                                                                                    <td><%=unitsObj[k].getUnitname()%></td>
                                                                                    <td><%=strStatus%></td>
                                                                                    <td><%=operator[i].getStrRoleName()%></td>
                                                                                    <td><%=operator[i].getiWrongAttempts()%></td>
                                                                                    <td><%=access%></td>
                                                                                    <td>
                                                                                        <a href="#" class="btn btn-mini" onclick="searchForUnit('<%=startDate%>', '<%=enddate%>', '<%=operator[i].getStrName()%>', '<%=operator[i].getStrOperatorid()%>')" id ="buttonEditOperatorSubmit">Show Audit</a>
                                                                                    </td>
                                                                                    <td><%=expire%></td>
                                                                                    <td><%=create%></td>
                                                                                </tr>
                                                                                <%

                                                                                    session.setAttribute("unitOprRole", dataList);
                                                                                %>
                                                                                <%}}}}
                                                                                 else {
                                                                                    UnitReport report = new UnitReport();
                                                                                    dataList.add(report);
                                                                                    session.setAttribute("unitOprRole", dataList);
                                                                                %><tr>
                                                                                    <td><%=1%></td>
                                                                                    <td><%= strerr%></td>
                                                                                    <td><%= strerr%></td>
                                                                                    <td><%= strerr%></td>
                                                                                    <td><%= strerr%></td>
                                                                                    <td><%= strerr%></td>
                                                                                    <td><%= strerr%></td>
                                                                                    <td><%= strerr%></td>
                                                                                    <td><%= strerr%></td>
                                                                                    <td><%= strerr%></td>
                                                                                    <td><%= strerr%></td>
                                                                                    <td><%= strerr%></td>
                                                                                </tr><%
                                                                                    }%>
//                                                                
                                                                            </table>
                                                                            <script>
                                                                                $(document).ready(function () {
                                                                                    $('#table_unit').DataTable({
                                                                                        responsive: true
                                                                                    });
                                                                                });
                             
                                                                            </script>
                                                                            <div class="row-fluid">
                    <div class="span6">
                        <div class="control-group">                        
                            <div class="span1">
                                <div class="control-group form-inline">
                                   <%if (oprObjI.getRoleid() != 1) {%> 
                                    <%if (accessObjN != null && accessObjN.downloadUnitReports == true) {%>
                                    <a href="#" class="btn btn-info" onclick="UnitRoleReportDownload(1)">
                                        <%} else {%>
                                        <a href="#" class="btn btn-info" onclick="InvalidRequest('unitroledown')" >

                                            <%}
                                            } else {%>
                                            <a href="#" class="btn btn-info" onclick="UnitRoleReportDownload(1)" >
                                                <%}%>
                                                <i class="icon-white icon-chevron-down"></i> CSV</a>
                                            </div>
                                            </div>
                                            <div class="span1">
                                                <div class="control-group form-inline">
                                                    <%if (oprObjI.getRoleid() != 1) {%> 
                                                    <%if (accessObjN != null && accessObjN.downloadUnitReports == true) {%>
                                                    <a href="#" class="btn btn-info" onclick="UnitRoleReportDownload(0)" >
                                                        <%} else {%>
                                                        <a href="#" class="btn btn-info" onclick="InvalidRequest('unitroledown')" >
                                                            <%}
                                                            } else {%>

                                                            <a href="#" class="btn btn-info" onclick="UnitRoleReportDownload(0)" >
                                                                <%}%>

                                                                <i class="icon-white icon-chevron-down"></i> PDF</a>
                                                            </div>
                                                            </div>
                                                            <div class="span1">
                                                                <div class="control-group form-inline">
                                                                    <%if (oprObjI.getRoleid() != 1) {%> 
                                                                    <%if (accessObjN != null && accessObjN.downloadUnitReports == true) {%>
                                                                    <a href="#" class="btn btn-info" onclick="UnitRoleReportDownload(2)" >
                                                                        <%} else {%>
                                                                        <a href="#" class="btn btn-info" onclick="InvalidRequest('unitroledown')" >
                                                                            <%}
                                                                            } else {%>
                                                                            <a href="#" class="btn btn-info" onclick="UnitRoleReportDownload(2)" >
                                                                                <%}%>
                                                                                <i class="icon-white icon-chevron-down"></i> TXT</a>
                                                                            </div>
                                                                            </div>

                                                                            </div>
                                                                            </div>
                                                                            </div>
            </div></div></div></div>                                 