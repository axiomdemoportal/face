<%@page import="com.mollatech.axiom.nucleus.settings.AxiomRadiusClient"%>
<%@page import="com.mollatech.axiom.nucleus.settings.AxiomRadiusConfiguration"%>
<%@page import="com.mollatech.axiom.nucleus.settings.RadiusServerSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/radiusserver.js"></script>
<div class="container-fluid">
    <h1 class="text-success">Radius General and Client Setting Management</h1>
    <p>To facilitate axiom based radius.... </p>
    <div class="tabbable">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#primary" data-toggle="tab">Radius General Settings</a></li>
            <li><a href="#secondary" data-toggle="tab">Radius Clients</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="primary">
                <div class="container-fluid">
                    <h2>RADIUS Server Configuration</h2>
                    <p>To facilitate RADIUS support for authentication, AXIOM Protect exposes RADIUS interface that can be used for RADIUS compliant systems like VPN etc.</p>
                    <br>
                    <div class="row-fluid">
                        <form class="form-horizontal" id="primaryform">
                            <fieldset>
                                <div id="legend">
                                    <legend class=""><input type="checkbox" id="_accountEnabled" name="_accountEnabled" > Accounting Details </legend>
                                </div>

                                <!-- Name -->
                                <div class="control-group">
                                    <label class="control-label"  for="username">Host/IP : Port </label>
                                    <div class="controls">
                                        <input type="text" id="_accountIp" name="_accountIp" placeholder="example localhost/127.0.0.1" class="input-xlarge">
                                        : <input type="text" id="_accountPort" name="_accountPort" placeholder="443" class="span2">
                                    </div>
                                </div>

                                <div id="legend">
                                    <legend class=""><input type="checkbox" id="_authEnabled" name="_authEnabled"> Authentication Details </legend>
                                </div>

                                <!-- Name -->
                                <div class="control-group">
                                    <label class="control-label"  for="username">Host/IP : Port </label>
                                    <div class="controls">
                                        <input type="text" id="_authIp" name="_authIp" placeholder="example localhost/127.0.0.1" class="input-xlarge">
                                        : <input type="text" id="_authPort" name="_authPort" placeholder="443" class="span2">
                                    </div>
                                </div>

                                <div id="legend">
                                    <legend class=""> Password Validation</legend>
                                </div>

                                <div class="control-group">
                                    <label class="control-label"  for="username">Validation Source</label>
                                    <div class="controls">
                                        <select class="span4" name="_ldapValidate" id="_ldapValidate">
                                            <option value="ldap">Use LDAP/Active Directory as Source</option>
                                            <option value="axiom">Use AXIOM User Repository as Source</option>
                                        </select>
                                    </div>
                                </div> 


                                <div class="control-group">
                                    <label class="control-label"  for="username">LDAP Host/IP</label>
                                    <div class="controls">
                                        <input type="text" id="_ldapServerIp" name="_ldapServerIp" placeholder="example localhost/127.0.0.1" class="input-xlarge">
                                        : <input type="text" id="_ldapServerPort" name="_ldapServerPort" placeholder="443" class="span2">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label"  for="username">LDAP Search Path</label>
                                    <div class="controls">
                                        <input type="text" id="_ldapSearchPath" name="_ldapSearchPath" placeholder="leave blank is no authentication" class="input-xlarge">
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label"  for="username">LDAP Server UserName/Password</label>
                                    <div class="controls">
                                        <input type="text" id="_ldapUsername" name="_ldapUsername" placeholder="example localhost" class="input-xlarge">
                                        : <input type="password" id="_ldapPassword" name="_ldapPassword" placeholder="password!@#$" class="span2">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="controls">
                                        <button class="btn btn-primary" onclick="editradiussettings()" type="button">Save Setting Now >> </button>                        
                                    </div>
                                </div>

                            </fieldset>
                        </form>
                    </div>
                </div>
                <script language="javascript" type="text/javascript">
                    LoadRadiusSetting(1);
                  
                </script>
            </div>
            <div class="tab-pane" id="secondary">
                <div class="row-fluid">
                    <table class="table table-striped" id="table_main">

                        <tr>
                            <td>No.</td>
                            <td>Display Name</td>
                            <td>Type</td>
                            <td>Status</td>
                            <td>Manage</td>
                            <td>Created On</td>
                            <td>Last Updated On</td>
                        </tr>
                        <%                            
                            Channels _apSChannelDetails = (Channels) session.getAttribute("_apSChannelDetails");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            String _sessionID = (String) session.getAttribute("_apSessionID");
                            int iType = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.Radius;
                            int iPreference = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE;
                            SettingsManagement sMngmt = new SettingsManagement();
                            AxiomRadiusConfiguration RadiusServerObj = null;

                            //   String strType = String.valueOf(iType);
                            Object settingsObj = sMngmt.getSetting(_sessionID, _apSChannelDetails.getChannelid(), iType, iPreference);

                            if (settingsObj != null) {
                                RadiusServerObj = (AxiomRadiusConfiguration) settingsObj;

                                AxiomRadiusClient[] radiusClient = null;
                                radiusClient = RadiusServerObj.getRadiusClient();
                                if (radiusClient != null) {

                                    for (int i = 0; i < radiusClient.length; i++) {

                                        AxiomRadiusClient nradiusClient = radiusClient[i];

                                        int iOprStatus = nradiusClient.getStatus();
                                        String strStatus;
                                        if (iOprStatus == 1) {
                                            strStatus = "Active";
                                        } else {
                                            strStatus = "Suspended";
                                        }
                                        SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm ");

                                        String uidiv4OprStatus = "radius-status-value-" + i;

                                        SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yy");

                        %>      
                        <tr>
                            <td><%=i + 1%></td>
                            <td><%=nradiusClient.getRadiusClientDisplayname()%></td>
                            <td><%=nradiusClient.getRadiusClientAuthtype()%></td>
                            <td>
                                <div class="btn-group">
                                    <button class="btn btn-mini" id="<%=uidiv4OprStatus%>"><%=strStatus%></button>
                                    <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ChangeradiusClientStatus(1,<%=i + 1%>,'<%=uidiv4OprStatus%>')">Mark as Active?</a></li>
                                        <li><a href="#" onclick="ChangeradiusClientStatus(0,<%=i + 1%>,'<%=uidiv4OprStatus%>')">Mark as Suspended?</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button class="btn btn-mini" >Manage</button>
                                    <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="removeRadiusClient(<%=i + 1%>)">Remove?</a></li>
                                        <li><a href="#" onclick="loadClientDetails(<%=i + 1%>, '<%=nradiusClient.getRadiusClientDisplayname()%>', '<%=nradiusClient.getRadiusClientAuthtype()%>', '<%=nradiusClient.getRadiusClientIp()%>', '<%=nradiusClient.getRadiusClientSecretkey()%>')" data-toggle="modal">Edit Details</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td><%=sdf.format(nradiusClient.getCreationDate())%></td>
                            <td><%=sdf.format(nradiusClient.getLastUpdateDate())%></td>
                        </tr>

                        <%}
                                }
                            }%>
                    </table>

                    <form class="form-horizontal" id="smsecondaryform" name="smsecondaryform" >
                        <div class="control-group">
                            <div class="controls">
                                <button href="#addNewRadiusClient" class="btn btn-primary" data-toggle="modal">Add New Radius Client&raquo;</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div id="addNewRadiusClient" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                    <h3 id="myModalLabel">Add New Radius Client</h3>
                </div>
                <div class="modal-body">
                    <div class="row-fluid">
                        <form class="form-horizontal" id="RadiusClientForm">
                            <fieldset>
                                <!-- Name -->
                                <div class="control-group">
                                    <label class="control-label"  for="username">Display Name</label>
                                    <div class="controls">
                                        <input type="text" id="_displayname" name="_displayname" placeholder="VPN First.." class="input-xlarge">                        
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label"  for="username">Client Authentication Type</label>
                                    <div class="controls">
                                        <select class="span9" name="authenticationtype" id="authenticationtype">
                                            <option value="password" >Password Only</option>
                                            <option value="otp" >One Time Password (OTP) Only </option>
                                            <option value="both" >OTP And Password</option>
                                            <option value="followup" >Password followed by OTP</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label"  for="username">Radius Client IP</label>
                                    <div class="controls">
                                        <input type="text" id="_rClientIP" name="_rClientIP" placeholder="example localhost/127.0.0.1" class="input-xlarge">                        
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="username">Client Shared Secret </label>
                                    <div class="controls">
                                        <input type="password" id="_radiusClientSecretkey" name="_radiusClientSecretkey" placeholder="leave blank is no authentication" class="input-xlarge">
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <div id="addNewRadiusClient-result"></div>
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button class="btn btn-primary" onclick="addNewRadiusClient()" id="addUserButton">Create Radius Client</button>
                </div>
            </div>
            <div id="editRadiusClient" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                    <h3 id="myModalLabel">edit Radius Client</h3>
                </div>
                <div class="modal-body">
                    <div class="row-fluid">
                        <form class="form-horizontal" id="editRadiusClientForm">
                            <fieldset>
                                <input type="hidden" id="srno" name="srno">
                                <div class="control-group">
                                    <label class="control-label"  for="username">Display Name</label>
                                    <div class="controls">
                                        <input type="text" id="_eclientdisplayname" name="_eclientdisplayname" placeholder="VPN First.." class="input-xlarge">                        
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label"  for="username">Client Authentication Type</label>
                                    <div class="controls">
                                        <select class="span9" name="eauthenticationtype" id="eauthenticationtype">
                                            <option value="password" >Password Only</option>
                                            <option value="otp" >One Time Password (OTP) Only </option>
                                            <option value="both" >OTP And Password</option>
                                            <option value="followup" >Password followed by OTP</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label"  for="username">Radius Client IP</label>
                                    <div class="controls">
                                        <input type="text" id="_eradiusClientIp" name="_eradiusClientIp" placeholder="example localhost/127.0.0.1" class="input-xlarge">                        
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="username">Client Shared Secret </label>
                                    <div class="controls">
                                        <input type="username" id="_eradiusClientSecretkey" name="_eradiusClientSecretkey" placeholder="leave blank is no authentication" class="input-xlarge">
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <div id="editRadiusClient-result"></div>
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button class="btn btn-primary" onclick="editRadiusClient()" id="addUserButton">Edit Radius Client</button>
                </div>
            </div>
        </div>
    </div>

</div>

<%@include file="footer.jsp" %>