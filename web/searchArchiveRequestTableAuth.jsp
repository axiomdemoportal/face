<%@page import="com.mollatech.axiom.v2.face.handler.accessmatrix.ApprovalPendingRequestComparator"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.apache.commons.lang.ArrayUtils"%>
<%@page import="com.mollatech.axiom.connector.access.controller.ApprovalSetting"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Operators"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.AxiomProtect"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.db.Approvalsettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.AuthorizationManagement"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.connector.user.TokenStatusDetails"%>
<%@page import="com.mollatech.axiom.nucleus.db.Certificates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Pkitokens"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement"%>
<%--<%@include file="header.jsp" %>--%>
<script src="./assets/js/authorization.js"></script>
<!--<div class="container-fluid">-->
<%
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
    String _channelId = channel.getChannelid();
    AuthorizationManagement aObj = new AuthorizationManagement();
    Date startDate12 = new Date();
    Calendar current = Calendar.getInstance();
    current.setTime(startDate12);
    String arrEvents = request.getParameter("_unitIDs");
    String starTimeUnit = request.getParameter("_startAtTime");
    String endTimeUnit = request.getParameter("_endAtTime");
    String _searchType = request.getParameter("_searchType");
    String _reqstatusA = request.getParameter("_statusA");
    int type = 0;
    if (_searchType.isEmpty() == false) {
        type = Integer.parseInt(_searchType);
    }
    Approvalsettings[] arrRequest = null;
    ArrayList<Approvalsettings> listAuthReq = null;
    if (type == 0) {
//        if(operatorS.getOperatorType()==OperatorsManagement.AUTHORIZER)
        Approvalsettings[] arrRequestAutherizer = aObj.getALLArchiveRequestAuthorizer(sessionId, operatorS.getOperatorid(), _channelId);
//        else
        Approvalsettings[] arrRequestRequester = aObj.getALLArchiveRequestAuthorizerRequester(sessionId, operatorS.getOperatorid(), _channelId);
        arrRequest = (Approvalsettings[]) ArrayUtils.addAll(arrRequestAutherizer, arrRequestRequester);
        listAuthReq = new ArrayList<Approvalsettings>(Arrays.asList(arrRequest));

        Collections.sort(listAuthReq, new ApprovalPendingRequestComparator());
    } else {

        int reqStatus = Integer.parseInt(_reqstatusA);
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date startDate = null;
        if (starTimeUnit != null && !starTimeUnit.isEmpty()) {
            startDate = (Date) formatter.parse(starTimeUnit);
        }
        Date endDate = null;
        if (endTimeUnit != null && !endTimeUnit.isEmpty()) {
            endDate = (Date) formatter.parse(endTimeUnit);
        }
//        if(operatorS.getOperatorType()==OperatorsManagement.AUTHORIZER)
        Approvalsettings[] arrRequestAuth = aObj.getArchiveRequest(sessionId, channel.getChannelid(), operatorS.getOperatorid(), startDate, endDate,
                reqStatus);
//        else
        Approvalsettings[] arrRequestReq = aObj.getArchiveRequestv2(sessionId, channel.getChannelid(), operatorS.getOperatorid(), startDate, endDate,
                reqStatus);
        arrRequest = (Approvalsettings[]) ArrayUtils.addAll(arrRequestAuth, arrRequestReq);
        listAuthReq = new ArrayList<Approvalsettings>(Arrays.asList(arrRequest));

        Collections.sort(listAuthReq, new ApprovalPendingRequestComparator());

    }

    String strerr = "No Record Found";
    //  SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy HH:mm ");
    DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
%>

<div class="row-fluid">
    <div class="row-fluid">
        <div class="span6">
            <div class="span2">
                <div class="control-group form-inline">
                    <a href="#" class="btn btn-info" onclick="authoriserReportCSV('<%=arrEvents%>', '<%=_searchType%>', '<%=_reqstatusA%>', '<%=starTimeUnit%>', '<%=endTimeUnit%>', '<%=OperatorsManagement.AUTHORIZER%>')" >
                        <i class="icon-white icon-chevron-down"></i>Download CSV</a>
                </div>
            </div>
            <div class="span2">
                <div class="control-group form-inline">
                    <a href="#" class="btn btn-info" onclick="authoriserReportPDF('<%=arrEvents%>', '<%=_searchType%>', '<%=_reqstatusA%>', '<%=starTimeUnit%>', '<%=endTimeUnit%>', '<%=OperatorsManagement.AUTHORIZER%>')" >
                        <i class="icon-white icon-chevron-down"></i>Download PDF</a>
                </div>
            </div>
            <div class="span2">
                <div class="control-group form-inline">
                    <a href="#" class="btn btn-info" onclick="authoriserReportTXT('<%=arrEvents%>', '<%=_searchType%>', '<%=_reqstatusA%>', '<%=starTimeUnit%>', '<%=endTimeUnit%>', '<%=OperatorsManagement.AUTHORIZER%>')" >
                        <i class="icon-white icon-chevron-down"></i>Download TXT</a>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="row-fluid">
                <div class="span6">
                    <div class="control-group">                        
                        <div class="span3">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="OTPReport()" >
                                    <i class="icon-white icon-chevron-down"></i>Download CSV</a>
                            </div>
                        </div>
                        <div class="span3">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="OTPReportpdf()" >
                                    <i class="icon-white icon-chevron-down"></i>Download PDF</a>
                            </div>
                        </div>
    
    
                    </div>
                </div>
            </div>-->
    <div id="licenses_data_table">
        <!--        <h3>Recent Additions</h3>-->

        <table class="table table-striped" id="table_main">

            <tr>
                <td>No.</td>
                <td>Action</td>
                <td>Old Value</td>
                <td>New Value</td>
                <td>Marked By</td>
                <td>User Name</td>
                <td>Action</td>
                <!--                <td>old Value</td>
                                <td>new Value</td>-->
                <td>Reason</td>
                <td>Created On</td>
                <td>Expired On</td>
            </tr>
            <%                    if (listAuthReq != null) {
                    OperatorsManagement opMngt = new OperatorsManagement();
                    UserManagement uMngt = new UserManagement();
                    for (int i = 0; i < listAuthReq.size(); i++) {
                        byte[] obj = listAuthReq.get(i).getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obj);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = AuthorizationManagement.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        String strStatus = "-";
                        if (arrRequest[i].getStatus() == 1) {
                            strStatus = "Active";
                        } else if (arrRequest[i].getStatus() == 2) {
                            strStatus = "Pending";
                        }
                        String userName = "-";
                        if (approvalSetting != null) {
//                                if(operatorS.getOperatorid().equals(approvalSetting.makerid)){
                            AuthUser u = null;
                            if (approvalSetting.userId != null) {
                                u = uMngt.getUser(sessionId, _channelId, approvalSetting.userId);
                            }

                            if (u != null) {
                                userName = u.getUserName();
                            }

                            String strAuthStatus = "-";
                            if (arrRequest[i].getStatus() == AuthorizationManagement.AUTORIZATION_APPROVE_STATUS) {
                                strAuthStatus = "Approve";
                            } else if (arrRequest[i].getStatus() == AuthorizationManagement.AUTORIZATION_REJECT_STATUS) {
                                strAuthStatus = "Reject";
                            } else if (arrRequest[i].getStatus() == AuthorizationManagement.AUTORIZATION_EXPIRE_STATUS) {
                                strAuthStatus = "Expired";
                            }

                            String markername = "-";
                            boolean print = true;
                            if (arrRequest[i].getMarkerOperatorid() != null) {
                                Operators oprbj = opMngt.getOperatorById(_channelId, arrRequest[i].getMarkerOperatorid());
                                if (oprbj != null) {

                                    markername = oprbj.getName();

                                    if (type != 0) {
                                        if (type == 1) { // BY Units
                                            int unitIDs = Integer.parseInt(arrEvents);

                                            if (unitIDs != oprbj.getUnits()) {
                                                print = false;
                                            }
                                        } else if (type == 2) { // By operatorId
                                            if (!arrEvents.equals(oprbj.getOperatorid())) {
                                                print = false;
                                            }
                                        }
                                    }
                                }
                            }

                            String reason = "-";
                            if (approvalSetting.reason != null) {
                                reason = approvalSetting.reason;
                            }
                            
                            byte[] obj1 = arrRequest[i].getApprovalSettingEntry();
                            byte[] f1 = AxiomProtect.AccessDataBytes(obj1);
                            ByteArrayInputStream bais1 = new ByteArrayInputStream(f1);
                            Object object1 = AuthorizationManagement.deserializeFromObject(bais1);
                            ApprovalSetting approvalSetting1 = null;
                            String old = "NA",newval = "NA";
                            if (object instanceof ApprovalSetting) {
                                approvalSetting1 = (ApprovalSetting) object1;
                                if(approvalSetting1.oldvalue != null && approvalSetting1.oldvalue != ""){
                                old = approvalSetting1.oldvalue;
                                }
                                if(approvalSetting1.newValue != null && approvalSetting1.newValue != ""){
                                newval = approvalSetting1.newValue;
                                }
                            }
            %>
            <%if (print == true) {%>
            <tr>
                <td><%=(i + 1)%></td>
                <%if (approvalSetting.tokenSerialNo != null) {%>
                <td><%=approvalSetting.itemid%> serial no =<%=approvalSetting.tokenSerialNo%> </td>
                <%} else {%>
                <td><%=approvalSetting.itemid%> </td>
                <%}%>
                <td><%=old%></td>
                <td><%=newval%></td>
                <td><%= markername%></td>
                <td><%=userName%></td>
                <td><%=strAuthStatus%></td>
                <td><%=reason%></td>
                <td><%=sdf.format(arrRequest[i].getCreatedOn())%></td>
                <!--<td><%=arrRequest[i].getExpireOn()%></td>-->
                <td><%=sdf.format(arrRequest[i].getExpireOn())%></td>
                <% //}%>
            </tr>



            <%}}}} else {%>
            <tr>
                <td><%=1%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
            </tr>
            <%}%>


        </table>


    </div>
</div>
<div class="row-fluid">
    <div class="span6">
        <div class="span2">
            <div class="control-group form-inline">
                <a href="#" class="btn btn-info" onclick="authoriserReportCSV('<%=arrEvents%>', '<%=_searchType%>', '<%=_reqstatusA%>', '<%=starTimeUnit%>', '<%=endTimeUnit%>', '<%=OperatorsManagement.AUTHORIZER%>')" >
                    <i class="icon-white icon-chevron-down"></i>Download CSV</a>
            </div>
        </div>
        <div class="span2">
            <div class="control-group form-inline">
                <a href="#" class="btn btn-info" onclick="authoriserReportPDF('<%=arrEvents%>', '<%=_searchType%>', '<%=_reqstatusA%>', '<%=starTimeUnit%>', '<%=endTimeUnit%>', '<%=OperatorsManagement.AUTHORIZER%>')" >
                    <i class="icon-white icon-chevron-down"></i>Download PDF</a>
            </div>
        </div>
        <div class="span2">
            <div class="control-group form-inline">
                <a href="#" class="btn btn-info" onclick="authoriserReportTXT('<%=arrEvents%>', '<%=_searchType%>', '<%=_reqstatusA%>', '<%=starTimeUnit%>', '<%=endTimeUnit%>', '<%=OperatorsManagement.AUTHORIZER%>')" >
                    <i class="icon-white icon-chevron-down"></i>Download Text</a>
            </div>
        </div>
    </div>
</div>

<!--</div>-->





<%--<%@include file="footer.jsp" %>--%>