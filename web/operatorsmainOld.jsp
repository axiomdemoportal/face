<%@page import="com.mollatech.axiom.nucleus.db.Units"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserGroupsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Usergroups"%>
<%@include file="header.jsp" %>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<!--<script src="./assets/js/usermanagement.js"></script>-->
<%
    UnitsManagemet uMngt = new UnitsManagemet();
    Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");

    String operatorsource = LoadSettings.g_sSettings.getProperty("axiom.operator.source");
    if (operatorsource == null) {
        operatorsource = "MYSQL";
    }
%>
<div class="container-fluid" >
    <div id="auditTable">
        <h1 class="text-success">Operators Management</h1>
        <p>You can manage all your operators, their roles and credentials management. Any operator added will have access rights based on one of the either, Role Based Or Operator based. Role based is coming from Access Role Rights matrix applicable for all operators in that defined role. Operator based is specific to that operator (essentially modified Role based access right matrix).</p>
        <h3>Search Operators</h3>   
        <div class="input-append">
            <form id="searchUserForm" name="searchUserForm">
                <input type="text" id="_keyword" name="_keyword" placeholder="Search using name, phone, email..." class="span3"/>
                <select class="units" name="_unitID" id="_unitID">
                    <option value="-1" selected="selected">All Units</option>
                    <%
                        Units[] uList = uMngt.ListUnitss(sessionid, operator.getChannelid());
                        if (uList != null) {
                            for (int i = 0; i < uList.length; i++) {
                                if (uList[i].getStatus() == 1) {//only active
%>
                    <option value="<%=uList[i].getUnitid()%>"><%=uList[i].getUnitname()%></option>
                    <%
                            }
                        }
                    %>

                    <%
                        }
                    %>
                </select>
                <select class="span2" name="_operatorStatus" id="_operatorStatus">
                    <option value="1">Active</option>
                    <option value="0">Suspend</option>
                    <option value="-1">Locked</option>
                    <option value="-99">Permanently Removed</option>

                </select>   
                <a href="#" class="btn btn-success" onclick="searchOperators()">Search Now</a>
            </form>
            <script>
                $('select[name=_unitID]').val(-1);
//                  doSequence("select.units option");
//                
//                    $('select[name=_unitID]').val(-1);
            </script>
        </div>
        <div id="users_table_main">
        </div>
        </p>
        <br>
        <p><a href="#addOperator" class="btn btn-primary" data-toggle="modal">Add New Operator&raquo;</a></p>
        <%if (operatorsource.equals("LDAP")) {%>
        <p><a href="./searchoperator.jsp" class="btn btn-success" >Add Ldap Operator&raquo;</a></p>
        <%}%>
        <!--<p><a href="#addNewUser" class="btn btn-primary" data-toggle="modal">Add New User&raquo;</a></p>-->
    </div>
</div>

<div id="addOperator" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Add New Operator</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="AddNewOperatorForm" name="AddNewOperatorForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Name</label>
                        <div class="controls">
                            <input type="text" id="_oprname" name="_oprname" placeholder="set unique name for login" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Email Id</label>
                        <div class="controls">
                            <input type="text" id="_opremail" name="_opremail" placeholder="set emailid for notification" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Phone</label>
                        <div class="controls">
                            <input type="text" id="_oprphone" name="_oprphone" placeholder="set phone for notification" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Assign Role</label>
                        <div class="controls">
                            <select class="span4" name="_oprrole" id="_oprrole">
                                <%   for (int i = 0; i < roles.length; i++) {
//                                        if (roleObj.getName().equals(OperatorsManagement.admin) && !roles[i].getName().equals(OperatorsManagement.sysadmin)) {
%>
                                                                                <!--<option value="<%=roles[i].getRoleid()%>"><%=roles[i].getName()%></option>-->
                                <%
//                                } else if (roleObj.getName().equals(OperatorsManagement.sysadmin)) {
                                    if (!operator.getName().equals("sysadmin")) {
                                        if (!roles[i].getName().equals("sysadmin")) {
                                %>                           
                                <option value="<%=roles[i].getRoleid()%>"><%=roles[i].getName()%></option>
                                <%
                                    }
                                } else {%>
                                <option value="<%=roles[i].getRoleid()%>"><%=roles[i].getName()%></option>
                                <%}
                                    }
                                    //                                    }
                                %>
                            </select>

                            <select class="span4" name="_operatorType" id="_operatorType">
                                <option  value="1">Requester</option>       
                                <option  value="2">Authorizer</option>  
                            </select>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Unit</label>
                        <div class="controls">
                            <select class="span4" name="_units" id="_units">
                                <%
//                                    Units[] uList = uMngt.ListUnitss(sessionid, _apSChannelDetails.getChannelid());
                                    if (uList != null) {
                                        for (int i = 0; i < uList.length; i++) {
                                            if (uList[i].getStatus() == 1) {//only active
%>
                                <option value="<%=uList[i].getUnitid()%>"><%=uList[i].getUnitname()%></option>
                                <%
                                            }
                                        }
                                    }
                                %>
                            </select>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>

    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideModal()">Close</button>
        <div class="span3" id="add-new-operator-result"></div>
        <button class="btn btn-primary" onclick="addOperator()" id="addnewOperatorSubmitBut">Add New Operator</button>

    </div>
</div>

<div id="editOperator" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3>Edit Operator<div id="idEditOperatorName"></div></h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="editOperatorForm" name="editOperatorForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Name</label>
                        <div class="controls">
                            <input type="text" id="_oprnameE" name="_oprnameE" placeholder="Operator Name" class="input-xlarge">
                            <input type="hidden" id="_oprroleidE" name="_oprroleidE"/>
                            <input type="hidden" id="_oprstatusE" name="_oprstatusE"/>
                            <input type="hidden" id="_opridE" name="_opridE"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Email</label>
                        <div class="controls">
                            <input type="text" id="_opremailE" name="_opremailE" placeholder="Operator Emailid" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Phone</label>
                        <div class="controls">
                            <input type="text" id="_oprphoneE" name="_oprphoneE" placeholder="Operator phone" class="input-xlarge">
                        </div>
                    </div>

                    <div class="control-group" >
                        <label class="control-label"  for="username"> Type</label>
                        <div class="controls">
                            <select class="span4" name="_operatorTypeE" id="_operatorTypeE">
                                <option  value="1">Requester</option>       
                                <option  value="2">Asuthorizer</option>  
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <!--                        <label class="control-label"  for="username"> Type</label>-->
                        <div class="controls">
                            <select class="span4" name="_operatorTypeE"   id="_operatorTypeE" style="display:none">
                                <option  value="1">Requester</option>       
                                <option  value="2">Authorizer</option>  
                            </select>
                            Edit Unit
                            <select class="span4" name="_unitsEDIT" id="_unitsEDIT">
                                <%
//                                UnitsManagemet uMngt = new UnitsManagemet();
//                                   Units[] uList = uMngt.ListUnitss(sessionid,_apSChannelDetails.getChannelid());
                                    if (uList != null) {
                                        for (int i = 0; i < uList.length; i++) {
                                            if (uList[i].getStatus() == 1) {//only active
%>
                                <option value="<%=uList[i].getUnitid()%>"><%=uList[i].getUnitname()%></option>
                                <%
                                            }
                                        }
                                    }
                                %>
                            </select>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="editoperator-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <%String edituser = LoadSettings.g_sSettings.getProperty("user.edit");
            if (edituser.equals("true")) {
        %>
        <button class="btn btn-primary" onclick="editoperator()" id="buttonEditOperatorSubmit">Save Changes</button>
        <%}%>
    </div>
</div>

<div id="auditDownload" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idauditDownload"></div></h3>
        <h3 id="myModalLabel">Download Audit</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="testSMSPrimaryForm" name="testSMSPrimaryForm">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_oprnameR" name="_oprnameR"/>
                    <input type="hidden" id="_opridR" name="_opridR"/>
                    <div class="control-group">
                        <label class="control-label"  for="username">Start Date</label>
                        <div class="controls" align="left" >

                            <!--<span class="add-on">From:</span>-->   
                            <div id="Pushdatetimepicker1" class="input-append date">
                                <input id="_auditStartDate" name="_auditStartDate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">End Date</label>
                        <div class="controls" align="left">
                            <!--<span class="add-on">Till:</span>-->   
                            <div id="Pushdatetimepicker2" class="input-append date">
                                <input id="_auditEndDate" name="_auditEndDate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>                    
                </fieldset>
            </form>
        </div>
    </div>

    <div class="modal-footer">
        <div id="editoperator-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="searchAudit()" id="buttonEditOperatorSubmit">Show Audit</button>
    </div>
</div>

<div id="operatorAccess" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idauditDownload"></div></h3>
        <h3 id="myModalLabel">Grant Access</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="operatorAcessForm" name="operatorAcessForm">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_oprAccessId" name="_oprAccessId"/>
                    <div class="control-group">
                        <label class="control-label"  for="username">Start Date</label>
                        <div class="controls" align="left">
                            <!--<span class="add-on">Till:</span>-->   
                            <div id="Pushdatetimepicker4" class="input-append date">
                                <input id="_accessStartDate" name="_accessStartDate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div> 
                    <div class="control-group">
                        <label class="control-label"  for="username">End Date</label>
                        <div class="controls" align="left">
                            <!--<span class="add-on">Till:</span>-->   
                            <div id="Pushdatetimepicker3" class="input-append date">
                                <input id="_accessEndDate" name="_accessEndDate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>                    
                </fieldset>
            </form>
        </div>
    </div>

    <div class="modal-footer">
        <div id="editoperator-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="grantAccess()" id="buttonEditOperatorSubmit">Grant Access</button>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#Pushdatetimepicker1').datepicker({
            format: 'dd/mm/yyyy',
            language: 'pt-BR'
        });
    });
    $(function () {
        $('#Pushdatetimepicker2').datepicker({
            format: 'dd/mm/yyyy',
            language: 'pt-BR'
        });
    });
    $(function () {
        $('#Pushdatetimepicker3').datepicker({
            format: 'dd/mm/yyyy',
            language: 'pt-BR'
        });
    });
    $(function () {
        $('#Pushdatetimepicker4').datepicker({
            format: 'dd/mm/yyyy',
            language: 'pt-BR'
        });
    });
//                            ChangeMediaType(0);
</script>

<%@include file="footer.jsp" %>