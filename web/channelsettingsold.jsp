<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/channels.js"></script>
<%    //if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() < 3) {
      //  return;
    //}

    String _sessionID = (String) session.getAttribute("_apSessionID");

    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    SessionManagement smObj = new SessionManagement();
    int iStatus = smObj.GetSessionStatus(_sessionID);

    AxiomChannel channelsObj = null;
    if (iStatus == 1) { //active
        smObj.UpdateSession(_sessionID);
        ChannelManagement cmObj = new ChannelManagement();
        channelsObj = cmObj.GetChannel(_sessionID, channel.getName());
        cmObj = null;
    }


%>
<%    out.flush();
    ChannelManagement cmObj = new ChannelManagement();

    UserManagement umObj = new UserManagement();

    AxiomChannel axcObj = channelsObj;

    int iChStatus = axcObj.getiStatus();
    String strStatus;
    if (iChStatus == 1) {
        strStatus = "Active";
    } else {
        strStatus = "Suspended";
    }

    RemoteAccessManagement rmObj = new RemoteAccessManagement();
    boolean bRAEnabled = rmObj.IsRemoteAccessEnabled(axcObj.getStrChannelid());

    java.util.Date dCreatedOn = new java.util.Date(axcObj.utcCreatedOn);
    java.util.Date dLastUpdated = new java.util.Date(axcObj.lastUpdateOn);

    String uidiv4RemoteAccess = "channel-ra-value-";
    String strRemoteAccessStatus = "Enabled";
    if (bRAEnabled == false) {
        strRemoteAccessStatus = "Disabled";
    }

    String strSourcedetails = "Internal Axiom";
    if (bRAEnabled == true) {
        strRemoteAccessStatus = "External LDAP";
    }

    String uidiv4ChStatus = "channel-status-value-";

    SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm ");

    int userCount = 0;
    int otptokenCount = 0;
    userCount = umObj.getCountOfUsers(axcObj.getStrChannelid());
    OTPTokenManagement otpmObj = new OTPTokenManagement(axcObj.getStrChannelid());
    otptokenCount = otpmObj.getOTPTokenCount(axcObj.getStrChannelid());

    int pkiCount = 0;
    PKITokenManagement pkimngObj = new PKITokenManagement();
    pkiCount = pkimngObj.getPKITokenCount(axcObj.getStrChannelid());
    int certCount = 0;
    CertificateManagement certmngObj = new CertificateManagement();
    certCount = certmngObj.getCertificateCount(axcObj.getStrChannelid());


%>

<div class="container-fluid">
    <h1> /<%=axcObj.getStrName()%> </h1>
    <hr>
    <h4>1. Channel Id: <%= axcObj.getStrChannelid()%></h4>
    <h4>2. Interface Path: "/<%= axcObj.getStrVirtualPath()%>"</h4>
    <% if (axcObj.getStrVirtualPath().compareToIgnoreCase("face") != 0) {%> 
    <h4>3. Web Service: "/<%= axcObj.getStrVirtualPath()%>core/AxiomCoreInterfaceImpl?wsdl"</h4>
    <% } else { %>
    <h4>3. Web Service: "/core/AxiomCoreInterfaceImpl?wsdl"</h4>
    <% }%>



    <form class="form-horizontal" id="datasourceform">
        <fieldset>
            <input type="hidden" id="_datasource" name="_datasource">
          <div class="control-group">
              
                <label class="control-label"  for="username">Operator Data Source</label>
                <div class="controls">
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_data-source"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="ChangeSource(1)">Internal Data Source </a></li>
                            <li><a href="#" onclick="ChangeSource(0)">External Data Source</a></li>
                            
                        </ul>
                    </div>
                </div>
            </div> 

            <div class="control-group">
                <label class="control-label"  for="username">Host:Port</label>
                <div class="controls">
                    <input type="text" id="_ipSource" name="_ipSource" placeholder="example localhost/127.0.0.1" class="input-xlarge">
                    : <input type="text" id="_portSource" name="_portSource" placeholder="443" class="span1">
                    with 
                    <select class="span2" name="_sslSource" id="_sslSource">
                        <option value="true">Yes, SSL Enabled?</option>
                        <option value="false">No, SSL Disabled?</option>
                    </select>
                </div>
            </div> 

            <div class="control-group">
                <label class="control-label" for="password">Source Details</label>
                <div class="controls">
                    <input type="text" id="_databaseNameSource" name="_databaseNameSource" placeholder="database name" class="span2">
                    : <input type="text" id="_tableNameSource" name="_tableNameSource" placeholder="table name " class="span2">
                    authenticated with <input type="text" id="_userIdSource" name="_userIdSource" placeholder=" userid" class="span2">
                    : <input type="password" id="_passwordSource" name="_passwordSource" placeholder="password" class="span2">
                </div>
            </div> 


            <div class="control-group">
                <div class="controls">
                    <button class="btn btn-small" onclick="editChannelsettings()" type="button">Save External Source Settings >> </button>                        
                </div>
            </div> 

        </fieldset>
    </form>

    <h4>5. Total Sessions till date are <%=cmObj.getTotalSessions(axcObj.getStrChannelid())%></h4>
    <h4>6. Total Operators are  <%=cmObj.getOperatorCount(axcObj.getStrChannelid())%></h4>
    <hr>
    <h4>7. Total Users till date are <%=userCount%></h4>
    <h4>8. Total OTP Tokens issued are <%=otptokenCount%></h4>
    <h4>9. Total PKI Tokens issued are <%=pkiCount%></h4>
    <h4>10. Total Certificates issued are <%=certCount%></h4>
    <hr>
    <h4>Channel Creation Date is <%=sdf.format(dCreatedOn)%></h4>
    <h4>Last Updated Date is <%=sdf.format(dLastUpdated)%></h4>
    <script language="javascript" type="text/javascript">
        loadchannelsettings();
    </script>
</div>
<%@include file="footer.jsp" %>