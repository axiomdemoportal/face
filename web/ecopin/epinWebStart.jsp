<%@page import="com.mollatech.ecopin.management.EPINManagement"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.RemoteAccessUtils"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.ChannelsUtils"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.RemoteAccessManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.operation.AxiomChannel"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%@include file="./header.jsp" %>
<%
 String _channelName = this.getServletContext().getContextPath();
    _channelName = _channelName.replaceAll("/", "");

    SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
    Session sChannel = suChannel.openSession();
    ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);

    SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
    Session sRemoteAcess = suRemoteAcess.openSession();
    RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);

    Channels channel1 = cUtil.getChannel(_channelName);
    if (channel1 == null) {
        //out.print("empty channel name");
        //out.flush();
        sRemoteAcess.close();
        suRemoteAcess.close();
        sChannel.close();
        suChannel.close();
        JSONObject json = new JSONObject();
        String result = "error";
        String message = "empty channel name!!!!";
        json.put("_result", result);
        json.put("_message", message);
        out.print(json);
        return;
    }

    String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel1.getChannelid());
    if (credentialInfo == null) {
        //out.print("GetRemoteAccessCredentials() failed");
        //out.flush();
        sRemoteAcess.close();
        suRemoteAcess.close();
        sChannel.close();
        suChannel.close();
        JSONObject json = new JSONObject();
        String result = "error";
        String message = "GetRemoteAccessCredentials() failed";
        json.put("_result", result);
        json.put("_message", message);
        out.print(json);
        return;
    }

    request.getSession().setAttribute("_apSessionRemoteAccessLoginID", credentialInfo[0]);

    SessionManagement sManagement = new SessionManagement();
    String sessionId1 = sManagement.OpenSession(channel1.getChannelid(), credentialInfo[0], credentialInfo[1],request.getSession().getId());

    if (sessionId1 == null) {
        //out.print("OpenSession()");
        //out.flush();
        sRemoteAcess.close();
        suRemoteAcess.close();
        sChannel.close();
        suChannel.close();
        JSONObject json = new JSONObject();
        String result = "error";
        String message = "OpenSession() failed";
        json.put("_result", result);
        json.put("_message", message);
        out.print(json);
        return;
    }

    sRemoteAcess.close();
    suRemoteAcess.close();
    sChannel.close();
    suChannel.close();

    request.getSession().setAttribute("_apSessionID", sessionId1);
    request.getSession().setAttribute("_apSChannelDetails", channel1);
    
    EPINManagement eManagement = new EPINManagement();
    int result = eManagement.IsChannelURLACtive(sessionId1, channel1.getChannelid(), EPINManagement.WEB);
    if(result != 0){
       JSONObject json = new JSONObject();
        String strResult = "error";
        String message = "Web Epin Url is disabled!!!";
        json.put("_result", strResult);
        json.put("_message", message);
        out.print(json);
    }
%>

<form class="form-actions" id="epinform" name="epinform" method="GET" action="epinWebAskQA.jsp">
            <div class="control-group" align="center">
                <label class="control-label"  for="username">Emaild Id</label>
                <div class="controls">
                    <input type="text" id="_op_email" name="_op_email" placeholder="administrator's email id for notifications" class="input-xlarge">
                </div>
                <div class="controls">
                    <button class="btn btn-primary" id="buttonpin" type="submit">Apply for EPIN</button>
                </div>
            </div>
            <div id="epin-result"></div>
        </form>
   


<%@include file="footer.jsp" %>