<%@page import="com.mollatech.axiom.connector.epin.QuestionsAndAnswers"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.ecopin.handler.epinSMS"%>
<%@page import="com.mollatech.axiom.v2.face.handler.editsettings"%>
<%@page import="com.mollatech.axiom.nucleus.settings.PINDeliverySetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.RemoteAccessUtils"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.ChannelsUtils"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.axiom.nucleus.db.operation.AxiomQuestionsAndAnswers"%>
<%@page import="com.mollatech.axiom.connector.user.QuestionAndAnswer"%>
<%@page import="com.mollatech.axiom.nucleus.db.Epinsessions"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.RemoteAccessManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.operation.AxiomChannel"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%@page import="com.mollatech.ecopin.management.EPINManagement"%>
<%@include file="header.jsp" %>
<%    //response.setContentType("application/json");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    String _user_email = request.getParameter("_op_email");

    //]] sManagement = new SessionManagement();
    //String channelId = "b6BK5gdwIWqSue/8VSJOuuIiFNc=";
    String channelId = channel.getChannelid();
    SettingsManagement setManagement = new SettingsManagement();

    EPINManagement eManagement = new EPINManagement();
    int countOfCorrectAns = 0;
    AuthUser aUser = null;

    Epinsessions eSessions = null;
    String eSessionId = null;

    if (aUser == null) {

        //added
        Epinsessions[] eSessionsList = eManagement.getEpinSessionsByType(_user_email, channelId, epinSMS.EMAILID);
        if (eSessionsList != null) {
            for (int i = 0; i < eSessionsList.length; i++) {
                if (eSessionsList[i].getStatus() == epinSMS.STOP || eSessionsList[i].getStatus() == epinSMS.EXPIRED) {
                    //continue;
                } else {
                    eSessions = eSessionsList[i];
                    eSessionId = eSessions.getEpinsessionid();
                    break;
                }
            }
        }
        Date d = new Date();

        //end of addition
        //eSessions = eManagement.getEpinSessionsByType(_user_email, channelId, 3);
        if (eSessions != null) {
            long difference = eSessions.getExpirydatetime().getTime() - d.getTime();
            if (difference <= 0L) {
                eManagement.updateEPINSession(eSessions.getEpinsessionid(), eSessions.getUserid(), eSessions.getChannelid(), eSessions.getPhone(), eSessions.getPhone(), null, EPINManagement.EXPIRED);
                JSONObject json = new JSONObject();
                String result = "error";
                String message = "Epin Session has expired!!!";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                return;
            }
        }

        UserManagement uMamanagement = new UserManagement();
        aUser = uMamanagement.CheckUserByType(sessionId, channelId, _user_email, 3);
        
        if(aUser == null){
             JSONObject json = new JSONObject();
                String result = "error";
                String message = "";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                return;
        }

        /*else if (eSessions != null) {
         if (eSessions != null && eSessions.getStatus() == -1 || eSessions != null && eSessions.getStatus() == 2) {
         sManagement.UpdateSession(eSessions.getSessionId());
         }
         aUser = eManagement.CheckUserByType(sessionId, channelId, _user_email, 3);
         }*/
    }

    //delete from here
//        PINDeliverySetting p = new PINDeliverySetting();
//        p.timeToInHour = 9;
//        p.timeFromInHour =23;
//        p.pinRequestCountDuration= 10;
//        p.pinRequestCount = 5;
//        p.pinDeliveryType= 1;
//        p.operatorController = false;
//        p.isAlertOperatorOnFailure = true;
//        p.expiryTime = 5;
//        p.duration = 5;
//        p.dayRestriction = 2;
//        p.classname = "com.mollatech.internal.handler.user.source.AxiomUser";
//        p.channelType2 = 4;
//        p.channelType = 1;
//        p.PINForm = 1;
//        p.PINLength = 6;
//        p.PINValidity = 5;
//     
//        SettingsManagement sMan = new SettingsManagement();
//        sMan.addSetting(sessionId, channel.getChannelid(), SettingsManagement.Epin, 1, p);
//    
//    
    //to here
    QuestionAndAnswer[] queArray = eManagement.GetValidationQuestionsAndAnswers(sessionId, channelId, aUser.userId);
    QuestionsAndAnswers QandAObj = new QuestionsAndAnswers(queArray);
    PINDeliverySetting ePin = (PINDeliverySetting) setManagement.getSetting(sessionId, channelId, editsettings.EPIN, 1);

    int retValue = eManagement.EnforcePINPolicy(sessionId, null, aUser.userId, channelId, aUser.phoneNo, aUser.email, new Date(), ePin);
    if (retValue == 0) {

        if (eSessions == null) {
            if (QandAObj.questionsAndAnswerses == null || QandAObj.questionsAndAnswerses.length <= 0) {
                //user not found
                JSONObject json = new JSONObject();
                String result = "error";
                String message = "user not found 1 failed";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                return;
            } else {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.MINUTE, ePin.expiryTime);
                Date expiryDate = cal.getTime();
                session.setAttribute("_apUserObj", aUser);
                eSessionId = eManagement.addEPINSession(aUser.userId, channelId, aUser.phoneNo, aUser.email, QandAObj, new Date(), expiryDate, sessionId);
                request.getSession().setAttribute("_apEPINSessionId", eSessionId);
            }
        }
        /*else if (eSessions != null) {
         if (QandAObj.questionsAndAnswerses == null || QandAObj.questionsAndAnswerses.length <= 0) {
         JSONObject json = new JSONObject();
         String result = "error";
         String message = "user not found 2 failed";
         json.put("_result", result);
         json.put("_message", message);
         out.print(json);
         return;
         } else {
         session.setAttribute("_apUserObj", aUser);
         eManagement.enableExpirySession(eSessions.getUserid(), channelId, eSessions.getPhone(), eSessions.getEmailid(), QandAObj, 0, new Date(), new Date());
         }
         }*/
%>
<form class="form-actions" id="epinform" name="epinform">
    <div id='epin-result'></div>
    <div class='control-group'align='center'>
        <label class='control-label'  for='username'><h3>Questions And Answer</h3>
        </label>
    </div>
    <%  for (int i = 0; i < QandAObj.questionsAndAnswerses.length; i++) {%>

    <div class='control-group' align='center'>
        <label class='control-label'  for='username'><h2><%=(i + 1) + ": " + QandAObj.questionsAndAnswerses[i].question%>
            </h2></label>
        <div class='controls'>
            <input type='text' id='answers<%=i%>' name='answers<%=i%>' placeholder='Answer to validate' class='input-xlarge'>
        </div>
    </div>

    <%  }%>                
    <div class='controls' align='center'>
        <button class='btn btn-primary' onclick='getEpin()' id='buttonpin' type='button'>Genrate EPIN</button>
    </div>
</form>
<%

    } else {

        eManagement.AddEPINTracker(channelId, null, null, EPINManagement.NO_OPERATOR_CONTROLLED, aUser.userId, aUser.phoneNo, aUser.email, 4, -3, new Date(), null, ePin.expiryTime, null);

        JSONObject json = new JSONObject();
        String result = "error";
        String message = "wrong emailid!!!";
        json.put("_result", result);
        json.put("_message", message);
        out.print(json);
    }
%>



<%@include file="footer.jsp" %>