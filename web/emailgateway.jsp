<%@include file="header.jsp" %>
<script src="./assets/js/emailgateway.js"></script>
<div class="container-fluid">
    <h1 class="text-success">Email SMTP Gateway Configuration</h1>
    <p>To facilitate mobile based notification, Email SMTP Gateway(s) connection needs to be configured. For auto fail-over, please set the Secondary Gateway too. </p>
    <div class="tabbable">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#primary" data-toggle="tab">Primary Email SMTP Gateway</a></li>
            <li><a href="#secondary" data-toggle="tab">Secondary Email SMTP Gateway</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="primary">
                <div class="row-fluid">
                    <form class="form-horizontal" id="emailprimaryform" name="emailprimaryform">

                        <div id="legend">
                            <legend class="">Primary Email SMTP Gateway Configuration</legend>
                        </div>

                        <div class="control-group">
                            <label class="control-label"  for="username">Status </label>
                            <div class="controls">
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_status-primary-EMAIL"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ChangeActiveStatusEMAIL(1,1)">Mark as Active?</a></li>
                                        <li><a href="#" onclick="ChangeActiveStatusEMAIL(1,0)">Mark as Suspended?</a></li>
                                    </ul>
                                </div>
                                with Auto Fail-Over to Secondary Gateway
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_autofailover-primary-EMAIL"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ChangeFailOverEMAIL(1)">Mark as Enabled?</a></li>
                                        <li><a href="#" onclick="ChangeFailOverEMAIL(0)">Mark as Disabled?</a></li>
                                    </ul>
                                </div>
                            </div>
                            
                        </div>

                        <input type="hidden" id="_statusEMAIL" name="_statusEMAIL">
                        <input type="hidden" id="_perferenceEMAIL" name="_perferenceEMAIL" value="1">
                        <input type="hidden" id="_typeEMAIL" name="_typeEMAIL" value="4">
                        <input type="hidden" id="_autofailoverEMAIL" name="_autofailoverEMAIL">
                        <input type="hidden" id="_retriesEMAIL" name="_retriesEMAIL">
                        <input type="hidden" id="_retrydurationEMAIL" name="_retrydurationEMAIL">

                        <div class="control-group">
                            <label class="control-label"  for="username">Retry Count</label>
                            <div class="controls">
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_retries-primary-EMAIL"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ChangeRetryEMAIL(1,2)">2 Retries</a></li>
                                        <li><a href="#" onclick="ChangeRetryEMAIL(1,3)">3 Retries</a></li>
                                        <li><a href="#" onclick="ChangeRetryEMAIL(1,5)">5 Retries</a></li>
                                    </ul>
                                </div>
                                with wait time between retries as
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_retryduration-primary-EMAIL"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ChangeRetryDurationEMAIL(1,10)">10 seconds</a></li>
                                        <li><a href="#" onclick="ChangeRetryDurationEMAIL(1,30)">30 seconds</a></li>
                                        <li><a href="#" onclick="ChangeRetryDurationEMAIL(1,60)">60 seconds</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <hr>

                        <div class="control-group">
                            <label class="control-label"  for="username">Host : Port </label>
                            <div class="controls">
                                <input type="text" id="_ipEMAIL" name="_ipEMAIL" placeholder="example localhost/127.0.0.1" class="span3">
                                : <input type="text" id="_portEMAIL" name="_portEMAIL" placeholder="443" class="span1">
                                with <input type="checkbox" id="_sslEMAIL" name="_sslEMAIL" value="sslEnabled"> SSL enabled?
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Authenticate using </label>
                            <div class="controls">
                                <input type="text" id="_userIdEMAIL" name="_userIdEMAIL" placeholder="enter user id for authenticaiton " class="span2">
                                : <input type="password" id="_passwordEMAIL" name="_passwordEMAIL" placeholder="leave blank is no authentication" class="span2">
                                with  <input type="checkbox" name="_authRequiredEMAIL" id="_authRequiredEMAIL" value="authEnabled"> Authentication Enabled?
                                
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Sender Details</label>
                            <div class="controls">
                                <input type="text" id="_fromNameEMAIL" name="_fromNameEMAIL" placeholder="Sender's Name" class="span2">
                                : <input type="text" id="_fromEmailEMAIL" name="_fromEmailEMAIL" placeholder="Sender's Emailid" class="span3">
                            </div>
                        </div>
                        <hr>
                        <div class="control-group">
                                <label class="control-label"  for="username">Additional Attributes</label>
                                <div class="controls">
                                    <input type="text" id="_reserve1EMAIL" name="_reserve1EMAIL" placeholder="additional details >> name1=value1" class="span3">
                                    : <input type="text" id="_reserve2EMAIL" name="_reserve2EMAIL" placeholder="additional details >> name1=value1" class="span3">
                                    : <input type="text" id="_reserve3EMAIL" name="_reserve3EMAIL" placeholder="additional details >> name1=value1" class="span3">
                                </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Implementation Class</label>
                            <div class="controls">
                                <input type="text" id="_classNameEMAIL" name="_classNameEMAIL" placeholder="complete class name including package" class="input-xlarge">
                            </div>
                        </div>
                        
                        <!-- Submit -->
                        <div class="control-group">
                            <div class="controls">
                                <div id="save-EMAIL-gateway-primary-result"></div>
                                <% //if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) { %>
                                <button class="btn btn-primary" onclick="EditEMAILSetting(1)" type="button">Save Setting Now >> </button>
                                <%// } %>
                                <button class="btn" onclick="LoadTestEMAILConnectionUI(1)" type="button">Test Connection >> </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <div class="tab-pane" id="secondary">
                <div class="row-fluid">
                       <form class="form-horizontal" id="emailsecondaryform" name="emailsecondaryform">

                        <div id="legend">
                            <legend class="">Secondary Email SMTP Gateway Configuration</legend>
                        </div>

                        <div class="control-group">
                            <label class="control-label"  for="username">Status </label>
                            <div class="controls">
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_status-secondary-EMAIL"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ChangeActiveStatusEMAIL(2,1)">Mark as Active?</a></li>
                                        <li><a href="#" onclick="ChangeActiveStatusEMAIL(2,0)">Mark as Suspended?</a></li>
                                    </ul>
                                </div>
                                <!--with Auto Fail-Over to Secondary Gateway
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_autofailover-secondary-EMAIL"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ChangeFailOverEMAIL(2,1)">Mark as Enabled?</a></li>
                                        <li><a href="#" onclick="ChangeFailOverEMAIL(2,0)">Mark as Disabled?</a></li>
                                    </ul>
                                </div>
                                -->
                            </div>
                            
                        </div>

                        <input type="hidden" id="_statusEMAILS" name="_statusEMAILS">
                        <input type="hidden" id="_perferenceEMAIL" name="_perferenceEMAIL" value="2">
                        <input type="hidden" id="_typeEMAIL" name="_typeEMAIL" value="4">
                        <input type="hidden" id="_autofailoverEMAILS" name="_autofailoverEMAILS">
                        <input type="hidden" id="_retriesEMAILS" name="_retriesEMAILS">
                        <input type="hidden" id="_retrydurationEMAILS" name="_retrydurationEMAILS">

                        <div class="control-group">
                            <label class="control-label"  for="username">Retry Count</label>
                            <div class="controls">
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_retries-secondary-EMAIL"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ChangeRetryEMAIL(2,2)">2 Retries</a></li>
                                        <li><a href="#" onclick="ChangeRetryEMAIL(2,3)">3 Retries</a></li>
                                        <li><a href="#" onclick="ChangeRetryEMAIL(2,5)">5 Retries</a></li>
                                    </ul>
                                </div>
                                with wait time between retries as
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_retryduration-secondary-EMAIL"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ChangeRetryDurationEMAIL(2,10)">10 seconds</a></li>
                                        <li><a href="#" onclick="ChangeRetryDurationEMAIL(2,30)">30 seconds</a></li>
                                        <li><a href="#" onclick="ChangeRetryDurationEMAIL(2,60)">60 seconds</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <hr>

                        <div class="control-group">
                            <label class="control-label"  for="username">Host : Port </label>
                            <div class="controls">
                                <input type="text" id="_ipEMAILS" name="_ipEMAILS" placeholder="example localhost/127.0.0.1" class="span3">
                                : <input type="text" id="_portEMAILS" name="_portEMAILS" placeholder="443" class="span1">
                                with <input type="checkbox" id="_sslEMAILS" name="_sslEMAILS" value="sslEnabled"> SSL enabled?
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Authenticate using </label>
                            <div class="controls">
                                <input type="text" id="_userIdEMAILS" name="_userIdEMAILS" placeholder="enter user id for authenticaiton " class="span2">
                                : <input type="password" id="_passwordEMAILS" name="_passwordEMAILS" placeholder="leave blank is no authentication" class="span2">
                                with  <input type="checkbox" name="_authRequiredEMAILS" id="_authRequiredEMAILS" value="authEnabled"> Authentication Enabled?
                                
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Sender Details</label>
                            <div class="controls">
                                <input type="text" id="_fromNameEMAILS" name="_fromNameEMAILS" placeholder="Sender's Name" class="span2">
                                : <input type="text" id="_fromEmailEMAILS" name="_fromEmailEMAILS" placeholder="Sender's Emailid" class="span3">
                            </div>
                        </div>
                        <hr>
                        <div class="control-group">
                                <label class="control-label"  for="username">Additional Attributes</label>
                                <div class="controls">
                                    <input type="text" id="_reserve1EMAILS" name="_reserve1EMAILS" placeholder="additional details >> name1=value1" class="span3">
                                    : <input type="text" id="_reserve2EMAILS" name="_reserve2EMAILS" placeholder="additional details >> name1=value1" class="span3">
                                    : <input type="text" id="_reserve3EMAILS" name="_reserve3EMAILS" placeholder="additional details >> name1=value1" class="span3">
                                </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Implementation Class</label>
                            <div class="controls">
                                <input type="text" id="_classNameEMAILS" name="_classNameEMAILS" placeholder="complete class name including package" class="input-xlarge">
                            </div>
                        </div>
                        
                        <!-- Submit -->
                        <div class="control-group">
                            <div class="controls">
                                <div id="save-EMAIL-gateway-primary-result"></div>
                                <%// if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) { %>
                                <button class="btn btn-primary" onclick="EditEMAILSetting(2)" type="button">Save Setting Now >> </button>
                                <%//}%>
                                <button class="btn" onclick="LoadTestEMAILConnectionUI(2)" type="button">Test Connection >> </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>


    <script language="javascript" type="text/javascript">
        LoadEMAILSetting(1);
        LoadEMAILSetting(2);
    </script>
</div>




<div id="testEMAILPrimary" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Test Primary Gateway</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="testEMAILPrimaryForm" name="testEMAILPrimaryForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Test Message</label>
                        <div class="controls">
                             <input type="text" id="_testmsg" name="_testmsg" placeholder="this is the sample email to be sent out..." class="input-xlarge">
                            <input type="hidden" id="_type" name="_type" value="4">
                            <input type="hidden" id="_preference" name="_preference" value="1">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Test Email ID</label>
                        <div class="controls">
                            <input type="text" id="_testphone" name="_testphone" placeholder="set email id to send message" class="input-xlarge">
                        </div>
                    </div>                    
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div class="span3" id="test-EMAIL-primary-configuration-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="testconnectionEMAILprimary()" id="testEMAILPrimaryBut">Send Message Now</button>
    </div>
</div>

<div id="testEMAILSecondary" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Test Secondary Gateway</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="testEMAILSecondaryForm" name="testEMAILSecondaryForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Test Message</label>
                        <div class="controls">
                            <input type="text" id="_testmsgS" name="_testmsgS" placeholder="this is the sample email to be sent out..." class="input-xlarge">
                            <input type="hidden" id="_type" name="_type" value="4">
                            <input type="hidden" id="_preference" name="_preference" value="2">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Test Email ID</label>
                        <div class="controls">
                            <input type="text" id="_testphoneS" name="_testphoneS" placeholder="set email id to send message" class="input-xlarge">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div class="span3" id="test-EMAIL-secondary-configuration-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="testconnectionEMAILsecondary()" id="testEMAILSecondaryBut">Send Message Now</button>
    </div>
</div>

<%@include file="footer.jsp" %>