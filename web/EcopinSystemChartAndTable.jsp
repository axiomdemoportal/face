<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Interactionresponse"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.Interactions"%>
<%@page import="com.mollatech.dictum.management.SurveyManagement"%>
<div class="container-fluid">
   
    <%
//        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
//     String sessionId = (String) request.getSession().getAttribute("_apSessionID");
//    
//    String _userId = request.getParameter("_userID");
    String _startdate = request.getParameter("_startDate");
    String _enddate = request.getParameter("_endDate");
    DateFormat formatter = new SimpleDateFormat("dd/mm/yyyy");
    Date startDate = null;
    if (_startdate != null && !_startdate.isEmpty()) {
        startDate = (Date) formatter.parse(_startdate);
    }
    Date endDate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        endDate = (Date) formatter.parse(_enddate);
    }
    

    %>
      <!--<h3 class="text-success">System Reports generated for duration <%=_startdate %> to <%=_enddate %> </h3>-->
    
    
    <div class="tabbable">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#SystemPolicyCheck" data-toggle="tab">Policy Check</a></li>
            <li ><a href="#SystemQAValidation" data-toggle="tab">QA Validation</a></li>
            <li ><a href="#SystemDelivery" data-toggle="tab">Delivery</a></li>
            <li><a href="#SystemOperator1Controlled" data-toggle="tab">Operator Controlled</a></li>
            <li><a href="#SystemDualOperatorControlled" data-toggle="tab">Dual Operator Controlled</a></li>
            <li><a href="#Systemecopintable" data-toggle="tab">Download Report</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="SystemPolicyCheck">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="tab-pane active" id="msgcharts">
                            <p><h3><em><strong>Policy Check</strong></em></h3></p>
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">
                                        <div class="span4">
                                            <div id="SystemPolicyCheckDonutChart" ></div>
                                            <!--Donut Chart-->
                                        </div>
                                        <div class="span7">
                                            <div id="SystemPolicyCheckBarChart"></div>
                                            <!--Bar Chart-->   
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
            <div class="tab-pane active" id="SystemQAValidation">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="tab-pane active" id="msgcharts">
                          
                            <p><h3><em><strong>QA-Validation</strong></em></h3></p>
                    
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">
                                        <div class="span4">
                                            <div id="SystemQAValidationDonutChart" ></div>
                                            <!--Donut Chart-->
                                        </div>
                                        <div class="span7">
                                            <div id="SystemQAValidationBarChart"></div>
                                            <!--Bar Chart-->   
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <div class="tab-pane active" id="SystemDelivery">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="tab-pane active" id="msgcharts">
                           <p><h3><em><strong>Delivery</strong></em></h3></p>
                            
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">
                                        <div class="span4">
                                            <div id="SystemDeliveryDonutChart" ></div>
                                            <!--Donut Chart-->
                                        </div>
                                        <div class="span7">
                                            <div id="SystemDeliveryBarChart"></div>
                                            <!--Bar Chart-->   
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <div class="tab-pane active" id="SystemOperator1Controlled">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="tab-pane active" id="msgcharts">
                           <p><h3><em><strong>Operator Controlled</strong></em></h3></p>
                           
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">
                                        <div class="span4">
                                            <div id="SystemOperator1ControlledDonutChart" ></div>
                                            <!--Donut Chart-->
                                        </div>
                                        <div class="span7">
                                            <div id="SystemOperator1ControlledBarChart"></div>
                                            <!--Bar Chart-->   
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
             <div class="tab-pane active" id="SystemDualOperatorControlled">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="tab-pane active" id="msgcharts">
                           <p><h3><em><strong>Dual Operator Controlled</strong></em></h3></p>
                           
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">
                                        <div class="span4">
                                            <div id="SystemDualOperatorControlledDonutChart" ></div>
                                            <!--Donut Chart-->
                                        </div>
                                        <div class="span7">
                                            <div id="SystemDualOperatorControlledBarChart"></div>
                                            <!--Bar Chart-->   
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
                
                <div class="tab-pane active" id="Systemecopintable">
                    <div id="SystemecopinReporttable">
                        
                    </div>
               </div>

        </div>
    </div>



</div>
<script>
//      document.getElementById("question1").style.display = 'none';
//document.getElementById("hidelable").style.display = 'none';

</script>
<%--<%@include file="footer.jsp" %>--%>