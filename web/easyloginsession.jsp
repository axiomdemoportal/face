<%@include file="header.jsp" %>
<!--<script src="./assets/js/jquery.js"></script>
<script src="./assets/js/certificate.js"></script>-->
<script src="./assets/js/easyloginreport.js"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<!--<script src="./assets/js/bootstrap.min.js"></script>-->
<!--<link rel="stylesheet" href="./assets/css/bootstrap.min.css">-->
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">
<link rel="stylesheet" href="./assets/css/datepicker.css">
<script src="./assets/js/bootstrap-datepicker.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<div class="container-fluid">
    <h2 class="text-success">Easy CheckIn Session Report</h2>   
    <div class="row-fluid">
        <div class="span12">                
            <div class="input-append">
                <!--                <div class="input-prepend">-->
                <!--                    <div class="well">-->
                <span class="add-on">From:</span>   
                <div id="datetimepicker1" class="input-append date">
                    <input id="sessionstartdate" name="sessionstartdate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth">
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>

            </div>
            <div class="input-append">
                <!--                    <div class="well">-->
                <span class="add-on">to:</span>   
                <div id="datetimepicker2" class="input-append date">
                    <input id="sessionenddate" name="sessionenddate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth">
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
                <div class="input-append">
                    <select span="1"  name="_statusName" id="_statusName" >
                        <option value="3">Select</option>
                        <option value="0">NA</option>
                        <option value="-1">Rejected</option>
                        <option value="-2">Expired</option>
                        <option value="1">Approved</option>                        
                    </select>
                    <span><button class="btn btn-success" onclick="generateSessionEasyLoginTable()" type="button" id="generatereportButton">Search</button></span>
                    <!--                    <span style="margin-left: 10px"><button class="btn btn-success" id="refreshButton" onclick="RefreshAuthReport()" type="button" >Refresh Report</button></span>-->
                </div>   
                <!--                    </div>-->

            </div>


        </div>
    </div>

    <!-- New added -->
    <div class="tabbable" id="SESSIONREPORT">
        <ul class="nav nav-tabs" id="sessioneasylogintab">
            <li class="active"><a href="#sessioneasyloginchart" data-toggle="tab">Charts</a></li>
            <li><a href="#otpreport" data-toggle="tab">Tabular List</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="sessioneasyloginchart">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="control-group">
                            <div class="span4">
                                <div id="sessiongraph" ></div>

                            </div>
                            <div  class="span8">
                                <div id="sessiongraph1"></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane" id="otpreport">  
                <div id="easyLoginSession_table_main">
                </div>
            </div>
        </div>
    </div>

</div>
<!--    <div id="auth_table_main" style="margin-top: 50px"></div>-->
<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datepicker({
            format: 'dd/mm/yyyy',
            language: 'pt-BR'
        });
    });
    $(function () {
        $('#datetimepicker2').datepicker({
            format: 'dd/mm/yyyy',
            language: 'pt-BR'

        });
    });

</script>
<script>
    document.getElementById("SESSIONREPORT").style.display = 'none';
    //document.getElementById("refreshButton").style.display = 'none';
</script>
<%@include file="footer.jsp" %>