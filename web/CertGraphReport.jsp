
<%-- 
    Document   : newcertDiscovery
    Created on : 14 Nov, 2016, 10:26:34 AM
    Author     : bluebricks
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<!--<html>
    <head>-->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@page import="com.mollatech.axiom.nucleus.db.ApCertDiscovery"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CertDiscoveryManagement"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Certificates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/jquery.js"></script>
<script src="./assets/js/certificate.js"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">
<link rel="stylesheet" href="./assets/css/datepicker.css">
<script src="./assets/js/bootstrap-datepicker.js"></script>
<!--<script src="assets/js/certdiscovery.js" type="text/javascript"></script>-->
<!--js for flot_Donut chart-->
<script src="assets/js/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="assets/js/json_sans_eval.js" type="text/javascript"></script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="/js/flot/excanvas.min.js"></script><![endif]-->
<script src="assets/js/jquery.flot.min.js" type="text/javascript"></script>
<script src="assets/js/jquery.flot.time.js" type="text/javascript"></script>
<script src="assets/js/jshashtable-2.1.js" type="text/javascript"></script>

<script src="assets/js/jquery.numberformatter-1.2.3.min.js" type="text/javascript"></script>
<script src="assets/js/jquery.flot.pie.min.js" type="text/javascript"></script>
<script src="assets/js/jquery.flot.axislabels.js" type="text/javascript"></script>
<script src="assets/js/canvasjs.min.js" type="text/javascript"></script>
<%
    String _startdate = request.getParameter("_startdate");
    String _enddate = request.getParameter("_enddate");
    DateFormat formatter = new SimpleDateFormat("dd/mm/yyyy");
    Date startDate = null;
    if (_startdate != null && !_startdate.isEmpty()) {
        startDate = (Date) formatter.parse(_startdate);
    }
    Date endDate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        endDate = (Date) formatter.parse(_enddate);
    }

    CertDiscoveryManagement cObj = new CertDiscoveryManagement();
    ApCertDiscovery[] arrCobj = null;
    arrCobj = cObj.getExpireSoonCertificate();

    String strerr = "No Record Found";

    String strStatus = null;

    strStatus = "Expire Soon (Within 30 days)";


%>
<!--    </head>-->
<style type="text/css">
    #left {
        width:600px;
        height:350px;
        float:left;
        background:white;
    }

    #right {
        /*            width:400px;*/
        width: 600px;
        height:350px;
        float:right;
        background:white;
    }
    #right11 {
        width:600px;
        height:250px;
        float:right;
        background:white;
    }
    #left11 {
        width:600px;
        height:100%;
        float:left;
        background:white;
    }

    #righ {
        width:600px;
        height:100%;
        float:right;
        background:white;
    }

    #blue_box {
        position: relative;
        background:white;
        width:100%;
        height:370px;
        padding: 3px;
    }
    #blue_box11 {
        position: relative;
        background:white;
        width:100%;
        height:40%;
        padding: 3px;
    }
    .divider{
        position:absolute;
        /*            left:405px;*/
        left: 599px;
        top:3%;
        height:350px;
        bottom:10%;
        border-left:1px solid black;
    }
    .divider1{
        position:absolute;
        /*            left:860px;*/
        left: 960px;
        top:3%;
        height:350px;
        bottom:10%;
        border-left:1px solid black;
    }
    .divider2{
        position:absolute;
        left:599px;
        top:9px;
        height:300px;
        border-left:1px solid black;
    }
</style>
<script>
    function start() {
        function1();
        function2();

    }
</script>
<!-- Piwik -->
<script type="text/javascript">
    var _paq = _paq || [];
    // tracker methods like "setCustomDimension" should be called before "trackPageView"
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function () {
        var u = "//localhost/piwik/";
        _paq.push(['setTrackerUrl', u + 'piwik.php']);
        _paq.push(['setSiteId', '1']);
        var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
        g.type = 'text/javascript';
        g.async = true;
        g.defer = true;
        g.src = u + 'piwik.js';
        s.parentNode.insertBefore(g, s);
    })();
</script>
<!-- End Piwik Code -->

<body onload="start()" >
    <div id="maindiv">

        <h1 class="text-success">Certificate Discovery Reports</h1>  
        <hr>
        <div id="blue_box">
            <div id="left">

                <h3>Certificate Authorities(CA's)</h3>
                <script>


                    var dataSet = certDonutChartCA();


                    function certDonutChartCA() {
                        var s = './certDiscoveryChartforCA';
                        var jsonData = $.ajax({
                            url: s,
                            dataType: "json",
                            async: false
                        }).responseText;

                        var a = jsonParse(jsonData);
//alert(JSON.stringify(a));

                        return a;

                    }

                    var options2 = {
                        series: {
                            pie: {
                                show: true,
                                innerRadius: 0.5,
                                label: {
                                    show: true,
//                 radius: 2 / 3,
                                    formatter: function (label, series) {
                                        return '<div style="font-size:9pt;padding:1px;color:black;">' + label + '<br/>' + series.data[0][1] + '</div>';
                                    },
                                    threshold: 0.1
                                }

                            }
                        },

                    };

                    $.fn.showMemo = function () {
                        $(this).bind("plothover", function (event, pos, item) {
                            if (!item) {
                                return;
                            }
                            console.log(item.series.data)
                            var html = [];
                            var valy = item.series.value;
                            var percent = parseFloat(item.series.percent).toFixed(2);

                            html.push("<div style=\"border:1px solid grey;background-color:",
                                    item.series.color,
                                    "\">",
                                    "<span style=\"color:white\">",
                                    item.series.label,
                                    " : ",
                                    data,
                                    $.formatNumber(item.series.data[0][1], {format: "#,###", locale: "us"}),
                                    " (", valy, "%)",
                                    "</span>",
                                    "</div>");
                            $("#flot-memo").html(html.join(''));
                        });
                    }
                    $(document).ready(function () {
                        $.plot($("#flot-placeholder2"), dataSet, options2);
                        $("#flot-placeholder").showMemo();
                    });
// for Values in persentage use this
//$.fn.showMemo = function () {
//    $(this).bind("plothover", function (event, pos, item) {
//        if (!item) { return; }
//        console.log(item.series.data)
//        var html = [];
//        var valy= item.series.value;
//        var percent = parseFloat(item.series.percent).toFixed(2);        
//
//        html.push("<div style=\"border:1px solid grey;background-color:",
//             item.series.color,
//             "\">",
//             "<span style=\"color:white\">",
//             item.series.label,
//             " : ",
//             data,
//             $.formatNumber(item.series.data[0][1], { format: "#,###", locale: "us" }),
//             " (", valy, "%)",
//             "</span>", 
//             "</div>");
//        $("#flot-memo").html(html.join(''));
//    });
//}

                </script>
                <div id="flot-placeholder2" style="width:500px;height:300px;margin:0 auto"></div>
                <!--                            Donut Chart-->
            </div>


            <div class="divider"></div>
            <div id="right">

                <h3>Expiring Certificates</h3>
                <div id="bar-example" style="width:400px;height:330px;margin:0 auto"></div>
                <script>
                    function certDiscoveryBarChart() {
                        var s = './certDiscoveryBarChart';
// var s = './certbarchart?_startdate=' + val3 + '&_enddate=' + val4;
                        var jsonData = $.ajax({
                            url: s,
                            dataType: "json",
                            async: false
                        }).responseText;

                        var myJsonObj = jsonParse(jsonData);

                        return myJsonObj;
                    }
                </script>
                <script>
//Bar Chart
                    var day_data1 = null;
                    day_data1 = certDiscoveryBarChart();
//            alert(JSON.stringify(day_data1));
                    Morris.Bar({
                        element: 'bar-example',
//                element: 'certbarchart11',
                        data: day_data1,
//                barSizeRatio:0.5,
                        xkey: 'label',
                        ykeys: ['value'],
                        labels: ['value'],
                        xLabelAngle: 40,
                        barColors: function (row, series, type) {
                            console.log("--> " + row.label, series, type);
                            if (row.label === "0 Days")
                                return "#AD1D28";
                            else if (row.label === "7 Days")
                                return "#DEBB27";
                            else if (row.label === "30 Days")
                                return "#7D0096";
                            else if (row.label === "45 Days")
                                return "#005CDE";
                            else if (row.label === "60 Days")
                                return "#ff5bd7";
                            else if (row.label === "90 Days")
                                return "#00A36A";
                        }
                    });


                </script>
                <!--                            Bar Chart-->
            </div>
            <!--            <div class="divider1"></div>-->

        </div>
        <hr>
        <div id="blue_box11">
            <div id="left11">
                <h3> SSL Endpoint Notices </h3>
                <div id="chartContainer" style="height: 250px; width: 500px;">

                    <script>
                        function certDiscoveryHorizontalBarChart() {
                            var s = './SSLEndpointNotice';
                            // var s = './certbarchart?_startdate=' + val3 + '&_enddate=' + val4;
                            var jsonData = $.ajax({
                                url: s,
                                dataType: "json",
                                async: false
                            }).responseText;
                            var myJsonObj = jsonParse(jsonData);

                            return myJsonObj;
                        }
                    </script>

                    <script type="text/javascript">
                        var data3 = certDiscoveryHorizontalBarChart();
                        //  window.onload = function () {
                        function function1() {
                            var chart = new CanvasJS.Chart("chartContainer", {
                                title: {
                                    // text: "SSL Endpoint Notices",
                                    fontFamily: "Verdana",
                                    fontColor: "Black",
                                    fontSize: 16

                                },
                                animationEnabled: true,
                                axisY: {
                                    tickThickness: 0,
                                    lineThickness: 0,
                                    valueFormatString: " ",
                                    gridThickness: 0
                                },
                                axisX: {
                                    tickThickness: 0,
                                    lineThickness: 0,
                                    labelFontSize: 16,
                                    labelFontColor: "Black"

                                },

                                data: [
                                    {
                                        indexLabelFontSize: 16,
                                        toolTipContent: "<span style='\"'color: {color};'\"'><strong>{indexLabel}</strong></span><span style='\"'font-size: 15px; color:black '\"'><strong>{y}</strong></span>",
                                        indexLabelPlacement: "inside",
                                        indexLabelFontColor: "Black",
                                        indexLabelFontWeight: 100,
                                        indexLabelFontFamily: "Verdana",
                                        color: "#62C9C3",
                                        type: "bar",
                                        dataPoints: data3
                                                //                        [
                                                //                    { y: 0, label: "0", indexLabel: "Video" },
                                                //                    { y: 0, label: "25", indexLabel: "Dining" },
                                                //                    { y: 33, label: "33", indexLabel: "Entertainment" },
                                                //                    { y: 36, label: "36", indexLabel: "News" },
                                                //                    { y: 42, label: "42", indexLabel: "Music" },
                                                //                    { y: 49, label: "49", indexLabel: "Social Networking" },
                                                //                    { y: 50, label: "50", indexLabel: "Maps/ Search" },
                                                //                    { y: 55, label: "55", indexLabel: "Weather" },
                                                //                    { y: 61, label: "61", indexLabel: "Games" }
                                                //
                                                //
                                                //                ]
                                    }
                                ]
                            });
                            chart.render();
                        }
                    </script>

                </div>
            </div>
            <div class="divider2"></div>
            <div id="right11">
                <h3>Certificate Notices </h3>
                <div id="chartContainer1" style="height: 100%; width: 500px;">

                    <script>
                        function certDiscoveryHorizontalBarChart1() {
                            var s = './certificateNotice';
                            // var s = './certbarchart?_startdate=' + val3 + '&_enddate=' + val4;
                            var jsonData = $.ajax({
                                url: s,
                                dataType: "json",
                                async: false
                            }).responseText;
                            var myJsonObj = jsonParse(jsonData);

                            return myJsonObj;
                        }
                    </script>

                    <script type="text/javascript">
                        var d = certDiscoveryHorizontalBarChart1();
                        //  alert(JSON.stringify(d));
                        // window.onload = function () {
                        function function2() {
                            var chart = new CanvasJS.Chart("chartContainer1", {
                                title: {
                                    // text: "SSL Endpoint Notices",
                                    fontFamily: "Verdana",
                                    fontColor: "Black",
                                    fontSize: 16

                                },
                                animationEnabled: true,
                                axisY: {
                                    tickThickness: 0,
                                    lineThickness: 0,
                                    valueFormatString: " ",
                                    gridThickness: 0
                                },
                                axisX: {
                                    tickThickness: 0,
                                    lineThickness: 0,
                                    labelFontSize: 16,
                                    labelFontColor: "Black"

                                },

                                data: [
                                    {
                                        indexLabelFontSize: 16,
                                        toolTipContent: "<span style='\"'color: {color};'\"'><strong>{indexLabel}</strong></span><span style='\"'font-size: 15px; color:black '\"'><strong>{y}</strong></span>",
                                        indexLabelPlacement: "inside",
                                        indexLabelFontColor: "Black",
                                        indexLabelFontWeight: 100,
                                        indexLabelFontFamily: "Verdana",
                                        color: "#62C9C3",
                                        type: "bar",
                                        dataPoints: d
                                                //                        [
                                                //                    { y: 21, label: "21", indexLabel: "Video" },
                                                //                    { y: 25, label: "25", indexLabel: "Dining" },
                                                //                    { y: 33, label: "33", indexLabel: "Entertainment" },
                                                //                    { y: 36, label: "36", indexLabel: "News" },
                                                //                    { y: 42, label: "42", indexLabel: "Music" },
                                                //                    { y: 49, label: "49", indexLabel: "Social Networking" },
                                                //                    { y: 50, label: "50", indexLabel: "Maps/ Search" },
                                                //                    { y: 55, label: "55", indexLabel: "Weather" },
                                                //                    { y: 61, label: "61", indexLabel: "Games" }
                                                //
                                                //
                                                //                ]
                                    }
                                ]
                            });
                            chart.render();
                        }
                    </script>

                </div>
            </div>

        </div>

    </div>
    <div>
        <div style="height: 90px;"></div>
        <%@include file="footer.jsp" %>
    </div>


