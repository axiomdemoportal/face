<%@page import="com.mollatech.axiom.nucleus.settings.IPConfigFilter"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>

<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@include file="header.jsp" %>
<%
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");

    SettingsManagement smng = new SettingsManagement();
    IPConfigFilter ifObj = (IPConfigFilter) smng.getSetting(sessionId, channel.getChannelid(), SettingsManagement.IP_FILTER, SettingsManagement.PREFERENCE_ONE);

    int alertstatus = 1;

%>

<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<script src="./assets/js/ipfilter.js"></script>
<div class="container-fluid">
    <h1 class="text-success">IP Restriction</h1>
    <p>You can enforce ip restriction where only selected IPs shall be allowed to use the web services exposed.</p>
    <br>

    <div class="row-fluid">
        <form class="form-horizontal" id="ipfilterform" name="ipfilterform">

            <div class="control-group">
                <label class="control-label"  for="username">Allowed List:</label>
                <div class="controls">
                    <textarea name="_ip" id="_ip" style="width:100%">
                        <%
                            String[] strKeywords;
                            int iStatus = 1;
                            if (ifObj == null) {
                                strKeywords = new String[1];
                                strKeywords[0] = "127.0.0.1";

                            } else {
                                strKeywords = ifObj.getIP();
                                iStatus = ifObj.getStatus();
                                alertstatus = ifObj.getAlertstatus();
                            }
                            if (strKeywords != null)
                                for (int j = 0; j < strKeywords.length; j++) {
                        %>        
                        <%=strKeywords[j]%>,
                        <%
                            }
                        %>
                    </textarea>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"  for="username">Enabled IP Restriction?</label>
                <div class="controls">
                    <select class="span3" name="_statusIP" id="_statusIP">
                        <%SettingsManagement s = new SettingsManagement();
                            IPConfigFilter ipConfig = (IPConfigFilter) s.getSetting(sessionId, channel.getChannelid(), com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.IP_FILTER, 1);
                            if (ipConfig != null) {
                                if (ipConfig.getStatus() == 0) {%> 
                        <option value="0" selected >Enforce Restriction.</option>
                        <option value="1">Allow All?</option>
                        <% } else {
                        %>
                        <option value="0"  >Enforce Restriction.</option>
                        <option value="1" selected>Allow All?</option>
                        <%                            }
                        } else {
                        %><option value="0"  >Enforce Restriction.</option>
                        <option value="1" selected>Allow All?</option>


                        <%                                        }%>

                    </select>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label"  for="username">IP Blocked Action</label>
                <div class="controls">
                    <select class="span4" name="_alertOperator" id="_alertOperator">
                        <% if (alertstatus == 0) {%> 
                        <option value="0" selected>Yes, Alert Administrator</option>
                        <option value="1">No, Do Not Alert Administrator</option>
                        <% } else {
                        %>
                        <option value="0" >Yes, Alert Administrator</option>
                        <option value="1" selected>No, Do Not Alert Administrator</option>
                        <%                            }
                        %>

                    </select>
                </div>
            </div>

            <hr>
            <div class="control-group">
                <div class="controls">
                    <div id="save-epin-settings-result"></div>
                    <button class="btn btn-primary" onclick="SaveIpFilerList()" type="button" id="SaveIpFilteringButton">Save Changes Now >> </button>
                </div>
            </div>

        </form>
    </div>


    <script language="javascript" type="text/javascript">
        //LoadGlobalSettings();        
    </script>
</div>


<script>    
    //$(document).ready(function() { $("#_keywords").select2() }); 
    
    $(document).ready(function() { 
        $("#_ip").select2({
            tags:[],
            dropdownCss: {display: 'none'},
            tokenSeparators: [","]
        });                   
    }); 
</script>

<%@include file="footer.jsp" %>