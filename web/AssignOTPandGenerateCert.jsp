<%@include file="header.jsp" %>
<script src="./assets/js/certificate.js"></script>
<div class="container-fluid">
    <h2>Assign Token and Generate Certificate</h2>
    <br>
    <div class="row-fluid">
        <form class="form-horizontal" id="generatecertificate" name="generatecertificate">
            <fieldset>
                
                <div class="control-group">
                    <label class="control-label"  for="Email">Email ID</label>
                    <div class="controls">
                        <input type="text" id="_emailid" name="_emailid" placeholder="Enter Email Id" class="input-xlarge"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"  for="hwserialno">H/W Token Serial No</label>
                    <div class="controls">
                        <input type="text" id="_hwserialno" name="_hwserialno" placeholder="H/W Serial No" class="input-xlarge"/>
                    </div>
                </div>
                

<!--                <div class="control-group">
                    <label class="control-label"  for="otp">OTP</label>
                    <div class="controls">
                        <input type="text" id="_otp" name="_otp" placeholder="Enter OTP" class="input-xlarge"/>
                    </div>
                </div>-->
              
                <div class="control-group">
                    <div class="controls">
                        <button class="btn btn-primary btn-large" onclick="generatecert()" type="button">Generate Certificate Now >></button>
                        <div id="generate-certificate-result"></div>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>

    <%@include file="footer.jsp" %>
