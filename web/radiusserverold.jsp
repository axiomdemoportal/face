<%@include file="header.jsp" %>
<div class="container-fluid">
    <h2>RADIUS Server Configuration</h2>
    <p>To facilitate RADIUS support for authentication, AXIOM Protect exposes RADIUS interface that can be used for RADIUS compliant systems like VPN etc.</p>
    <br>
    <div class="row-fluid">
        <form class="form-horizontal">
            <fieldset>
                <div id="legend">
                    <legend class=""><input type="checkbox" id="save_card" value="option1"> Accounting Details </legend>
                </div>

                <!-- Name -->
                <div class="control-group">
                    <label class="control-label"  for="username">Host/IP : Port </label>
                    <div class="controls">
                        <input type="text" id="_p1host" name="_p1host" placeholder="example localhost/127.0.0.1" class="input-xlarge">
                        : <input type="text" id="_p1host" name="_p1host" placeholder="443" class="span2">
                    </div>
                </div>

                <div id="legend">
                    <legend class=""><input type="checkbox" id="save_card" value="option1"> Authentication Details </legend>
                </div>

                <!-- Name -->
                <div class="control-group">
                    <label class="control-label"  for="username">Host/IP : Port </label>
                    <div class="controls">
                        <input type="text" id="_p1host" name="_p1host" placeholder="example localhost/127.0.0.1" class="input-xlarge">
                        : <input type="text" id="_p1host" name="_p1host" placeholder="443" class="span2">
                    </div>
                </div>

                <div id="legend">
                    <legend class=""> Password Validation</legend>
                </div>

                <div class="control-group">
                    <label class="control-label" for="password">Validation Source</label>
                    <div class="controls">
                        <select class="span4" name="expiry_month" id="expiry_month">
                            <option value="ldap">Use LDAP/Active Directory as Soruce</option>
                            <option value="axiom">Use AXIOM User Repository as Soruce</option>
                        </select>
                    </div>
                </div>


                <div class="control-group">
                    <label class="control-label"  for="username">LDAP Host/IP</label>
                    <div class="controls">
                        <input type="text" id="_p1host" name="_p1host" placeholder="example localhost/127.0.0.1" class="input-xlarge">
                        : <input type="text" id="_p1host" name="_p1host" placeholder="443" class="span2">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"  for="username">LDAP Search Path</label>
                    <div class="controls">
                        <input type="text" id="_p1host" name="_p1host" placeholder="leave blank is no authentication" class="input-xlarge">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"  for="username">AXIOM Repository Host/IP</label>
                    <div class="controls">
                        <input type="text" id="_p1host" name="_p1host" placeholder="example localhost/127.0.0.1" class="input-xlarge">
                        : <input type="text" id="_p1host" name="_p1host" placeholder="443" class="span2">
                    </div>
                </div>


                <div id="legend">
                    <legend class=""> RADIUS Client</legend>
                </div>
                <div class="control-group">
                    <label class="control-label"  for="username">Client Authentication Type</label>
                    <div class="controls">
                        <select class="span4" name="expiry_month" id="expiry_month">
                            <option value="password">Password Only</option>
                            <option value="otp">OTP Only</option>
                            <option value="both">Password+OTP </option>
                            <option value="followup">Password followed by OTP </option>
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"  for="username">Radius Client</label>
                    <div class="controls">
                        <input type="text" id="_p1host" name="_p1host" placeholder="example localhost/127.0.0.1" class="input-xlarge">                        
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="password">Client Shared Secret </label>
                    <div class="controls">
                        <input type="password" id="_p1host" name="_p1host" placeholder="leave blank is no authentication" class="input-xlarge">
                    </div>
                </div>

                <!-- Submit -->
                <div class="control-group">
                    <div class="controls">
                        <button class="btn btn-primary">Save Setting Now >> </button>                        
                    </div>
                </div>

            </fieldset>
        </form>
    </div>

</div>

<%@include file="footer.jsp" %>