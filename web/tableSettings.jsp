<%@page import="com.mollatech.axiom.nucleus.db.connector.management.MonitorSettingsManagement"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.axiom.nucleus.db.Monitorsettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<script src="./assets/js/addSettings.js"></script>
<table class="display responsive wrap" id="SettingTable">
    <thead>
    <tr>
        <td><b>Sr. No</td>
        <td><b>Monitor Name</td>
        <td><b>Monitor Type</td>
        <td><b>Manage</td>
        <td><b>View Report</td>
        <td><b>Created On</td>
        <td><b>Updated On</td>
</tr>
    </thead>
    <%

        Channels _apSChannelDetails = (Channels) session.getAttribute("_apSChannelDetails");
        OperatorsManagement oManagement = new OperatorsManagement();
        String _sessionID = (String) session.getAttribute("_apSessionID");
        MonitorSettingsManagement management = new MonitorSettingsManagement();
        Monitorsettings[] monitorsettingses = {};
        Map settingsMap=new HashMap();
        monitorsettingses = management.listAllsettings(_sessionID);
        String type = "", status = "";
        if (monitorsettingses!=null) {
            for (int i = 0; i < monitorsettingses.length; i++) {
                settingsMap.put(monitorsettingses[i].getMonitorName(), monitorsettingses[i]);
    %>
            
    <tr>
        <td><%= i + 1%></td>
        <td><%= monitorsettingses[i].getMonitorName()%></td>
        <%
            if (monitorsettingses[i].getMonitorType() == 1) {
                type = "Web Site";
            } else if (monitorsettingses[i].getMonitorType() == 2) {
                type = "FTP Server Monitor";
            } else if (monitorsettingses[i].getMonitorType() == 3) {
                type = "FTP RTT monitor";
            } else if (monitorsettingses[i].getMonitorType() == 4) {
                type = "DNS Monitor";
            } else if (monitorsettingses[i].getMonitorType() == 5) {
                type = "Mail Server RTT";
            } else if (monitorsettingses[i].getMonitorType() == 6) {
                type = "Port Monitor";
            } else if (monitorsettingses[i].getMonitorType() == 7) {
                type = "POP Service Monitor";
            } else if (monitorsettingses[i].getMonitorType() == 8) {
                type = "SMTP Service Monitor";
            } else if (monitorsettingses[i].getMonitorType() == 9) {
                type = "Ping Monitor";
            } else if (monitorsettingses[i].getMonitorType() == 10) {
                type = "SSL Certificate Monitor";
            }

        %>
        <td><%= type%></td>
        <%
            if (monitorsettingses[i].getMonitorStatus() == 1) {
                status = "Active";
            } else {
                status = "Suspended";
            }
        %>
        <!--<td><%= status%></td>-->
        <td><div class="btn-group">
                <button class="btn btn-mini" id="<%=monitorsettingses[i].getMonitorStatus()%>"><%=status%></button>
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="ChangeMonitorStatus('<%=monitorsettingses[i].getMonitorName()%>', 1, '<%=monitorsettingses[i].getMonitorStatus()%>')">Mark as Active?</a></li>
                    <li><a href="#" onclick="ChangeMonitorStatus('<%=monitorsettingses[i].getMonitorName()%>', 0, '<%=monitorsettingses[i].getMonitorStatus()%>')">Mark as Suspended?</a></li>
                    <li class="divider"></li>
                    <li><a href="./addSetting.jsp?Name=<%=monitorsettingses[i].getMonitorName()%>" onclick="">Edit Details</a></li>
                    <li><a href="#" onclick="removeMonitorSetting('<%=monitorsettingses[i].getMonitorId()%>')" data-toggle="modal"><font color="red">Remove?</font></a></li>
                </ul>
            </div></td>

           <td><a href="./settingExecution.jsp?_settingName=<%= monitorsettingses[i].getMonitorName()%>&type=<%= monitorsettingses[i].getMonitorType() %>" class="btn btn-mini" data-toggle="modal">Show Report</a></td>

        <td><%= monitorsettingses[i].getCreatedOn()%></td>
        <td><%= monitorsettingses[i].getUpdatedOn()%></td>
    </tr>
    <%}
    } else {%>
    <tr>
        <td>No Record Found</td>
        <td>No Record Found</td>
        <td>No Record Found</td>
        <td>No Record Found</td>
        <td>No Record Found</td>
        <td>No Record Found</td>
        <td>No Record Found</td>
    </tr>
    <%  }
        session.setAttribute("setting", settingsMap);
    %>
</table>

<script> 
    $(document).ready(function () { 
        $('#SettingTable').DataTable({ 
            responsive: true 
        }); 
    }); 
</script>