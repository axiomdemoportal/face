<%@page import="java.util.Enumeration"%>
<%@page import="com.mollatech.axiom.nucleus.settings.TokenSettings"%>
<%@page import="java.util.Hashtable"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Templates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/imagesettings.js"></script>
<div class="container-fluid">
    <h1 class="text-success">Image Authentication Configuration</h2>
        <p>Image Authentication settings.</p>
        <hr>
        <div class="row-fluid">
            <form class="form-horizontal" id="imagesettingsform" name="imagesettingsform">

            <div class="control-group">
                    <!--<label class="control-label"  for="username">System Configuration</label>-->
                    <label class="control-label"  for="username">Sweet Spot</label>
                    <div class="controls">
                        <select class="span5" name="_sweetSpot" id="_sweetSpot" style="width:20%">
                            <option value="1" >Enable</option>
                            <option value="0" >Disable</option>
                        </select>
                    <!--</div>-->
                 
                     <!--<div class="controls">-->
                        <select class="span5" name="_honeyTrap" id="_honeyTrap" style="width:20%;display:none">
                            <option value="1" >Enable</option>
                            <option value="0" >Disable</option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <!--<label class="control-label"  for="username">System Configuration</label>-->
                    <label class="control-label"  for="username">Sweet Spot Deviation</label>
                    <div class="controls">
                        <select class="span5" name="_sweetSpotDeviation" id="_sweetSpotDeviation" style="width:20%">
                            <option value="1" >Enable</option>
                            <option value="0" >Disable</option>
                        </select>
                        X - Co-ordinate 
                        <select class="span5" name="_xDeviation" id="_xDeviation" style="width:10%">
                            <option value="1" selected >1 Point</option>
                            <option value="2" >2 Point</option>
                            <option value="3" >3 Point</option>
                            <option value="4" >4 Point</option>
                            <option value="5" >5 Point</option>
                        </select>
                        Y - Co-ordinate 
                        <select class="span3" name="_yDeviation" id="_yDeviation" style="width:10%">
                            <option value="1" selected >1 Point</option>
                            <option value="2" >2 Point</option>
                            <option value="3" >3 Point</option>
                            <option value="4" >4 Point</option>
                            <option value="5" >5 Point</option>
                        </select>

                    </div>

                </div>
                <div class="control-group">
                    <!--<label class="control-label"  for="username">System Configuration</label>-->
                    <label class="control-label"  for="username">Geolocation</label>
                    <div class="controls">
                        <select class="span5" name="_geoCheck" id="_geoCheck" style="width:20%">
                            <option value="1" >Enable</option>
                            <option value="0" >Disable</option>
                        </select>
                        Device Profile
                        <select class="span5" name="_deviceProfile" id="_deviceProfile" style="width:20%">
                            <option value="1" >Enable</option>
                            <option value="0" >Disable</option>
                        </select>
                    </div>
                </div>
                <!--<hr>-->
                <div class="control-group">
                    <div class="controls">
                         <div id="save-image-setting-result"></div>
                        <div>
                             <% //if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
                            <a href="#" class="btn btn-primary" onclick="editImageSettings()">Save Settings</a>
                            <%//}%>
                        </div>

                    </div>
                </div>

            </form>
        </div>
        <script language="javascript" type="text/javascript">
            LoadImageSettings();
        </script>
</div>
<%@include file="footer.jsp" %>