<%@page import="com.mollatech.axiom.nucleus.settings.ContactTagsSetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Templates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%@include file="header.jsp" %>
<script src="assets/ocr/pdfsettings.js" type="text/javascript"></script>
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<script src="./assets/js/bootstrap.timepicker.min.js"></script>
<link href="./assets/css/bootstrap-timepicker.min.css" rel="stylesheet"/>
<link type="text/css" href="css/bootstrap.min.css" />
<link href="./assets/css/bootstrap-responsive.min.css" rel="stylesheet"/>
<link href="./assets/css/bootstrap-timepicker.css" rel="stylesheet"/>
<script type="text/javascript" src="./assets/js/bootbox.min.js"></script>
<script src="./assets/js/bootstrap-timepicker.js"></script>
<script src="./assets/js/ajaxfileupload.js"></script>
<script src="./assets/js/bootstrap-fileupload.js"></script> 
<link href="./assets/css/bootstrap.css" rel="stylesheet">
<link href="./assets/css/bootstrap-fileupload.css" rel="stylesheet">
<html>
    <div class="container-fluid">
        <h1 class="text-success">OCR Templatization</h1>
        <hr>
        <form class="form-horizontal" id="AddNewSchedulerForm" name="AddNewSchedulerForm">
            <fieldset>

                <div class="control-group">
                    <label class="control-label"  for="username">Select Pdf</label>                                    
                    <div class="controls fileupload fileupload-new" data-provides="fileupload">
                        <div class="input-append">
                            <div class="uneditable-input span4"><i class="icon-file fileupload-exists"></i> 
                                <span class="fileupload-preview"></span>
                            </div>
                            <span class="btn btn-file" >
                                <span class="fileupload-new">Select file</span>
                                <span class="fileupload-exists">Change</span>
                                <input type="file" id="filetemplatetoupload" name="filetemplatetoupload"/>
                            </span>
                            <a href="./ocrdocumentation.jsp" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                            <button class="btn btn-success" id="buttonUploadpdf"  onclick="OcrDocsUpload()" >Upload Now>></button>
                        </div>
                    </div>
                </div>
                <div>
                    <p>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        Upload pdf file for ocr document.
                    </p>
                </div>
            </fieldset>
        </form>
    </div>  
</html>
