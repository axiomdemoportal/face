<%@page import="java.util.concurrent.TimeUnit"%>
<%@include file="header.jsp" %>
<!--<script src="./assets/js/jquery.js"></script>
<script src="./assets/js/certificate.js"></script>-->
<script src="./assets/js/easyloginreport.js"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<!--<script src="./assets/js/bootstrap.min.js"></script>-->
<!--<link rel="stylesheet" href="./assets/css/bootstrap.min.css">-->
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">
<link rel="stylesheet" href="./assets/css/datepicker.css">
<script src="./assets/js/bootstrap-datepicker.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@page import="com.mollatech.axiom.nucleus.db.ApEasyloginsession"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.EasyLoginSessionManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.axiom.nucleus.db.ApEasylogin"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.EasyLoginManagement"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();

    String _startdate = request.getParameter("startdate");
    String _enddate = request.getParameter("enddate");
    String _searchtext = request.getParameter("_easyloginId");
    String _user = request.getParameter("_user");
    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    DateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
    Date startdate = null;
    if (_startdate != null && !_startdate.isEmpty()) {
        startdate = (Date) formatter.parse(_startdate);
    }
    Date enddate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        enddate = (Date) formatter.parse(_enddate);
    }
    if (startdate != null && startdate.equals(enddate)) {
        enddate = new Date(startdate.getTime() + TimeUnit.DAYS.toMillis(1));
    }
    ApEasyloginsession[] txdetailses = new EasyLoginSessionManagement().getAllEasyLoginSessionByUser(sessionId, _channelId, _searchtext, startdate, enddate);
    session.setAttribute("easyloginSessiondetailses", txdetailses);
    String strmsg = "No Record Found";
%>
<div class="container-fluid">
    <h1 class="text-success">Session Detail of ' <%=_user%> '</h1>
    <br>
    <table class="table table-striped" >
        <thead style="text-align: center">
            <tr>
                <th>No.</th>
                <th>App IP</th>
                <th>Browser</th>
                <th>Browser Info</th>
                <th>Location</th>
                <th>Latitude</th>
                <th>Longitude</th>
                <th>Message</th>
                <th>Status</th>
                <th>Created On</th>
            </tr>
        </thead>
        <%    if (txdetailses != null) {
                Map map = new HashMap();
                for (int i = 0; i < txdetailses.length; i++) {
                    String errmessage = "NA";
                    String latitude = "NA";
                    String longitude = "NA";
                    String strStatus = "NA";
                    String createdOn = "NA";
                    String browser = "NA";
                    String agent = "NA";
                    String location = "NA";

                    if (txdetailses[i].getBrowser() != null) {
                        browser = txdetailses[i].getBrowser();
                    }
                    if (txdetailses[i].getAgent() != null) {
                        agent = txdetailses[i].getAgent();
                    }
                    if (txdetailses[i].getLattitude() != null) {
                        latitude = txdetailses[i].getLattitude();
                    }
                    if (txdetailses[i].getLongitude() != null) {
                        longitude = txdetailses[i].getLongitude();
                    }
                    if (txdetailses[i].getStatus() == EasyLoginSessionManagement.EAPPROVED_STATUS) {
                        strStatus = "Approved";
                    } else if (txdetailses[i].getStatus() == EasyLoginSessionManagement.EEXPIRED_STATUS) {
                        strStatus = "Expired";
                    } else if (txdetailses[i].getStatus() == EasyLoginSessionManagement.EREJECTED_STATUS) {
                        strStatus = "Rejected";
                    }
                    if (txdetailses[i].getErrormsg() != null && !txdetailses[i].getErrormsg().isEmpty()) {
                        errmessage = txdetailses[i].getErrormsg();
                    }
                    if (txdetailses[i].getCreatedon() != null) {
                        createdOn = format.format(txdetailses[i].getCreatedon());
                    }

                    AuthUser user = null;
//                    if (map.isEmpty()) {
//                        user = new UserManagement().getUser(_channelId, txdetailses[i].getUserid());
//                        map.put(txdetailses[i].getUserid(), user);
//                    } else {
//                        if (map.get(txdetailses[i].getUserid()) == null) {
//                            user = new UserManagement().getUser(_channelId, txdetailses[i].getUserid());
//                            map.put(txdetailses[i].getUserid(), user);
//                        } else {
//                            user = (AuthUser) map.get(txdetailses[i].getUserid());
//                        }
//                    }
//                    if (txdetailses[i].getDeviceinfo() != null) {
//                        deviceDetail = txdetailses[i].getDeviceinfo().split(":");
//                    }
                    if (txdetailses[i].getLocation() != null) {
                        location = txdetailses[i].getLocation();
                    }
        %>
        <tr>
            <td><%=i + 1%></td>
            <td><%=txdetailses[i].getAppIp()%></td>
            <td><%=browser%></td>
            <td><%=agent%></td>
            <td>
                <a href="#/" class="btn btn-mini" id="locationMsgs-<%=i%>"  rel="popover" data-html="true">Location</a>
                <script>
                    $(function ()
                    {
                        $("#locationMsgs-<%=i%>").popover({title: 'Location', content: "<%=location%>"});
                    });
                </script>
            </td>
            <td><%=latitude%></td>
            <td><%=longitude%></td>
            <td><%=errmessage%></td>
            <td>
                <%if (txdetailses[i].getStatus() == 0) {%>
                <span class=" label label-info"><%=strStatus%></span>
                <%}
                    if (txdetailses[i].getStatus() == 1) {%>
                <span class="label label-success"><%=strStatus%></span>
                <%}
                    if (txdetailses[i].getStatus() == -1) {%>
                <span class="label label-orange"><%=strStatus%></span>
                <%}
                    if (txdetailses[i].getStatus() == -2) {%>
                <span class="label label-warning"><%=strStatus%></span>
                <%}%>
            </td>
            <td><%=createdOn%></td>

        </tr>
        <%
            }
        } else {%>
        <tr>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
        </tr>
        <%}%>
    </table>

    <!--    <div class="row-fluid">   
            <div class="span6">
    
                <div class="control-group">                        
                    <div class="span1">
                        <div class="control-group form-inline">
                            <a href="#" class="btn btn-info" onclick="downloadauthReport(1, '<%=_startdate%>', '<%=_enddate%>')" >
                                CSV</a>
                        </div>
                    </div>
                    <div class="span1">
                        <div class="control-group form-inline">
                            <a href="#" class="btn btn-info" onclick="downloadauthReport(0, '<%=_startdate%>', '<%=_enddate%>')" >
                                PDF</a>
                        </div>
                    </div>
                    <div class="span1">
                        <div class="control-group form-inline">
                            <a href="#" class="btn btn-info" onclick="downloadauthReport(2, '<%=_startdate%>', '<%=_enddate%>')" >
                                TEXT</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
</div>
<%@include file="footer.jsp" %>