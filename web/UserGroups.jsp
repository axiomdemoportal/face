<%@page import="com.mollatech.axiom.nucleus.db.Countrylist"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.LocationManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserGroupsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Usergroups"%>
<%@page import="com.mollatech.axiom.connector.communication.AXIOMStatus"%>
<%@page import="com.mollatech.axiom.nucleus.settings.SendNotification"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.TemplateUtils"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.db.Templates"%>
<%@page import="com.mollatech.axiom.nucleus.db.operation.TemplateNames"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%@page import="com.mollatech.axiom.nucleus.settings.TokenSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet"%>
<%@page import="com.mollatech.axiom.nucleus.db.Units"%>
<%@include file="header.jsp" %>
<script src="./assets/js/units.js"></script>
<script src="./assets/js/mobiletrust.js"></script>
<script src="./assets/js/ajaxfileupload.js"></script>
<script src="./assets/js/bootstrap-fileupload.js"></script> 
<link href="./assets/css/bootstrap.css" rel="stylesheet">
<link href="./assets/css/bootstrap-fileupload.css" rel="stylesheet">

<script>

    $(document).ready(function () {
        //  var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
        $('#_CountryName').change(function () {
            var selectedValue = $(this).val();
            // alert(selectedValue);

            var s = './stateList.jsp?_countryId=' + selectedValue;

            $.ajax({
                type: 'GET',
                url: s,
                success: function (data) {

                    $('#_StateName').html(data);

                }
            });
        });
    });

</script>


<%    String _sessionID = (String) session.getAttribute("_apSessionID");

    SessionManagement smObj = new SessionManagement();
    TemplateManagement tempObj = new TemplateManagement();
//     OperatorsManagement s = new OperatorsManagement();
    int iStatus = smObj.GetSessionStatus(_sessionID);
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    OperatorsManagement oMngt = new OperatorsManagement();
    OTPTokenManagement otpMngt = new OTPTokenManagement(channel.getChannelid());
    SettingsManagement sMngt = new SettingsManagement();
    SendNotification send = new SendNotification();
    Usergroups[] groupsObj = null;
    if (iStatus == 1) { //active
        smObj.UpdateSession(_sessionID);
        UserGroupsManagement cmObj = new UserGroupsManagement();
        groupsObj = cmObj.ListGroups(_sessionID, channel.getChannelid());
        cmObj = null;
    }
%>
<div class="container-fluid">
    <h1 class="text-success">User Groups</h1>
    <div class="row-fluid">
        <div id="licenses_data_table">
            <table class="table table-striped">
                <tr>
                    <td>No</td>
                    <td>Group Name</td>
                    <td>Manage</td>
                    <td>Total Users</td>
                    <td>Country</td>
                    <td>State</td>
                    <td>Created On</td>
                    <td>Last Updated On</td>

                </tr>

                <%
                    out.flush();
//                    UnitsManagemet tmObj = new UnitsManagemet();
                    if (groupsObj != null) {
                        for (int i = 0; i < groupsObj.length; i++) {

                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            String strStatus = "Active";
                            if (groupsObj[i].getStatus() == 1) {
                                strStatus = "Active";
                            } else {
                                strStatus = "Suspended";
                            }
                            String userStatus = "user-status-value-" + i;

                            UserManagement uManagement = new UserManagement();
                            AuthUser[] aUser = uManagement.getAllUserByGroup(sessionid, channel.getChannelid(), groupsObj[i].getGroupid());
                            int countOfUser = 0;
                            if (aUser != null) {
                                countOfUser = aUser.length;
                            }

                %>
                <input type="hidden" id="_isBlackListed" name="_isBlackListed">
                <tr>
                    <td><%=(i + 1)%></td>
                    <td><%=groupsObj[i].getGroupname()%></td>

                    <%//if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
                    <td>
                        <div class="btn-group">
                            <button class="btn btn-mini" id="<%=userStatus%>"><%=strStatus%></button>
                            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">

                                <li><a href="#"  onclick="changeUnitsStatus(<%=groupsObj[i].getGroupid()%>, 1, '<%=userStatus%>')" >Mark as Active?</a></li>
                                <li><a href="#" onclick="changeUnitsStatus(<%=groupsObj[i].getGroupid()%>, 0, '<%=userStatus%>')" >Mark as Suspended?</a></li>
                                <li class="divider"></li>
                                <li><a href="#" onclick="loadEditUnitDetails('<%=groupsObj[i].getGroupname()%>', '<%=groupsObj[i].getGroupid()%>')">Edit Details</a></li>
                                <li><a href="#" onclick="loadShiftUnitDetails('<%=groupsObj[i].getGroupname()%>', '<%=groupsObj[i].getGroupid()%>')">Shift Group's Users</a></li>
                                <li><a href="#" onclick="restrictLocation('<%=groupsObj[i].getGroupname()%>', '<%=groupsObj[i].getGroupid()%>')">Restrict Location</a></li>
                                <!--<li><a href="#" onclick="loadShiftUnitDetails('<%=groupsObj[i].getGroupname()%>','<%=groupsObj[i].getGroupid()%>')">Shift Unit's Operator</a></li>-->
                                <li><a href="#" onclick="removeUnits(<%=groupsObj[i].getGroupname()%>)"><font color="red">Remove Group?</font></a></li>

                            </ul>
                        </div>
                    </td>
                    <%// }%>
                    <td><%=countOfUser%></td>
                    <td><%=groupsObj[i].getCountry()%></td>
                    <td><%=groupsObj[i].getState()%></td>
                    <td><%=sdf.format(groupsObj[i].getCreatedOn())%>
                    <td><%=sdf.format(groupsObj[i].getLastupOn())%></td>

                </tr>
                <%}
                    }%>
            </table>
            <p><a href="#addNewUnit" class="btn btn-primary" data-toggle="modal">Add New Group&raquo;</a></p>
        </div>
    </div>
    <br>

    <script language="javascript">
        //listChannels();
    </script>
</div>
<!-- Modal -->
<div id="addNewUnit" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Add New Group</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="AddNewUnitForm" name="AddNewUnitForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Name</label>
                        <div class="controls">
                            <input type="text" id="_unitName" name="_unitName" placeholder="set unique name for login" class="input-xlarge">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label"  for="username">Status</label>
                        <div class="controls">
                            <select class="span4" name="_unitStatus" id="_unitStatus">
                                <option value="1">Active</option>
                                <option value="0">In-Active</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Restriction Level</label>
                        <div class="controls">

                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_block_Countries"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="SetBlackListedCountries(5, '#_block_Countries', 'No Restriction')">No Restriction</a></li>
                                    <li><a href="#" onclick="SetBlackListedCountries(1, '#_block_Countries', 'Country')">Country</a></li>
                                    <li><a href="#" onclick="SetBlackListedCountries(2, '#_block_Countries', 'State')">State</a></li>
                                    <li><a href="#" onclick="SetBlackListedCountries(3, '#_block_Countries', 'City')">City</a></li>
                                    <li><a href="#" onclick="SetBlackListedCountries(4, '#_block_Countries', 'Pincode')">Pincode</a></li>


                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Home Country</label>
                        <div class="controls">
                            <select class="span4" name="_CountryName" id="_CountryName">
                                <%
                                    //getAll conotries here 
                                    LocationManagement lManagement = new LocationManagement();
                                    int countryCode = -1;
                                    Countrylist[] countryList = lManagement.getAllCountries();
                                    if (countryList != null) {
                                        for (int j = 0; j < countryList.length; j++) {
                                            String strcode = countryList[j].getCountyName();
                                            countryCode = countryList[j].getCountryid();
                                %>
                                <option value="<%=countryCode%>"><%=strcode%></option>

                                <%}
                                    }%>
                            </select>
                        </div>
                    </div>
                    <div name="_StateName" id="_StateName">                           
                    </div>






                </fieldset>
            </form>
        </div>
    </div>



    <div class="modal-footer">
        <div id="add-new-unit-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <!--<div class="span3" id="add-new-unit-result"></div>-->
        <button class="btn btn-primary" onclick="addNewUnit()" id="addnewUnitSubmitBut">Add New Group</button>
    </div>
</div>
<div id="editUnit" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idEditUnitName"></div>Edit Group</h3>
    </div>
    <!--     <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3 id="myModalLabel">Add New Unit</h3>
        </div>-->
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="editUnitForm" name="editUnitForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Name</label>
                        <div class="controls">
                            <input type="text" id="_unitNameE" name="_unitNameE" placeholder="Unit Name" class="input-xlarge">
                            <!--                            <input type="hidden" id="_oprroleidE" name="_oprroleidE"/>
                                                        <input type="hidden" id="_oprstatusE" name="_oprstatusE"/>-->
                            <input type="hidden" id="_unitId" name="_unitId"/>
                            <input type="hidden" id="_oldunitNameE" name="_oldunitNameE"/>
                        </div>
                    </div>


                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="editunit-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>

        <button class="btn btn-primary" onclick="editUnit()" id="buttonEditUnitSubmit">Save Changes</button>

    </div>
</div>


<div id="shiftUnit" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idshiftUnitName">Shift Group's Users</div></h3>
    </div>

    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="shiftUnitForm" name="shiftUnitForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Old Group Name</label>
                        <div class="controls">
                            <input type="text" readonly id="_oldunitNameS" name="_oldunitNameS" placeholder="Unit Name" class="input-xlarge">
                            <!--                            <input type="hidden" id="_oprroleidE" name="_oprroleidE"/>
                                                        <input type="hidden" id="_oprstatusE" name="_oprstatusE"/>-->
                            <input type="hidden" id="_unitIdS" name="_unitIdS"/>
                            <!--<input type="hidden" id="_oldunitNameS" name="_oldunitNameS"/>-->
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">New Group Name</label>
                        <div class="controls">
                            <select class="span4" name="_unitS" id="_unitS">
                                <%
                                    UserGroupsManagement uMngt = new UserGroupsManagement();
                                    Usergroups[] uList = uMngt.ListGroups(sessionid, channel.getChannelid());
                                    if (uList != null) {
                                        for (int i = 0; i < uList.length; i++) {
                                %>
                                <option value="<%=uList[i].getGroupid()%>"><%=uList[i].getGroupname()%></option>
                                <%
                                        }
                                    }
                                %>
                            </select>
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="shiftunit-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>

        <button class="btn btn-primary" onclick="shiftUnit()" id="buttonShiftUnitSubmit">Save Changes</button>

    </div>
</div>



<%@include file="footer.jsp" %>