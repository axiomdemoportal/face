<%@page import="com.mollatech.axiom.nucleus.db.Certificates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Pkitokens"%>
<%@page import="com.mollatech.axiom.connector.user.TokenStatusDetails"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.Otptokens"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<script src="./assets/js/pkitokens.js"></script>
<script src="./assets/js/caconnector.js"></script>
<script src="./assets/js/ajaxfileupload.js"></script>
<script src="./assets/js/bootstrap-fileupload.js"></script> 
<link href="./assets/css/bootstrap.css" rel="stylesheet">
<link href="./assets/css/bootstrap-fileupload.css" rel="stylesheet">
<script src="./assets/js/otptokens.js"></script>
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<script src="./assets/js/globalsetting.js"></script>
<%

    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();
    String _searchtext = request.getParameter("_searchtext");
    UserManagement usermngObj = new UserManagement();
    AuthUser Users[] = null;
    Users = usermngObj.SearchUsers(sessionId, _channelId, _searchtext);
    if(Users!=null)
    {
    Users = usermngObj.SearchUsersByID(sessionId, _channelId, _searchtext);   
    }
    if (Users != null) {
%>
<h3>Results for <i>"<%=_searchtext%>"</i> in Digital Certificate and Tokens</h3>

<table class="table table-striped" id="table_main">

    <tr>
        <td>No.</td>
        <td>UserId</td>
        <td>User Name</td>
        <td>Certificate</td>
        <td>Upload Documents</td>
        <td>Manage</td>
        <td>Hardware PKI Token</td>
        <td>Software PKI Token</td>  
        <td>Audit Trail</td>        
        <td>Issued On</td>
        <td>Expires On</td>
    </tr>

    <%
            for (int i = 0; i < Users.length; i++) {

                Certificates certifts = null;

                String strLabelSW = "label";    //unassigned
                String strLabelHW = "label";    //unassigned

                java.util.Date dCreatedOn = null;
                java.util.Date dLastUpdated = null;

                SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm ");
                String strStatus = null;
                TokenStatusDetails pkiDetails[] = null;
                PKITokenManagement pkiObj = new PKITokenManagement();
                String softPkistatus = "Unassigned";
                String hardPkistatus = "Unassigned";

                String serialNumberhard = "------";

                CertificateManagement CObj = new CertificateManagement();
                certifts = CObj.getCertificate(sessionId, _channelId, Users[i].getUserId());

                pkiDetails = pkiObj.getTokenList(sessionId, _channelId, Users[i].getUserId());

                if (pkiDetails != null) {

                    TokenStatusDetails tokenDetailsOfSoft = null;
                    TokenStatusDetails tokenDetailsOfHard = null;

                    for (int j = 0; j < pkiDetails.length; j++) {
                        if (pkiDetails[j].Catrgory == OTPTokenManagement.SOFTWARE_TOKEN) {
                            tokenDetailsOfSoft = pkiDetails[j];
                        } else if (pkiDetails[j].Catrgory == OTPTokenManagement.HARDWARE_TOKEN) {
                            tokenDetailsOfHard = pkiDetails[j];
                        }
                    }
                    if (tokenDetailsOfSoft != null) {

                        if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                            strLabelSW += " label-success";   //active
                            softPkistatus = "Active";
                        } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                            strLabelSW += " label-info";   //assigned                
                            softPkistatus = "Assigned";
                        } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                            softPkistatus = "Unassigned";
                        }else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
                            softPkistatus = "suspended";
                          }
                    }
                    if (tokenDetailsOfHard != null) {

                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                            hardPkistatus = "Active";
                            strLabelHW += " label-success";   //active

                        }

                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                            hardPkistatus = "Assigned";
                            strLabelHW += " label-info";   //assigned                

                        }
                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                            hardPkistatus = "Unassigned";

                        }

                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_LOST) {
                            hardPkistatus = "LOST";
                            strLabelHW += " label-important";   //assigned                

                        }
                        if (tokenDetailsOfHard.serialnumber != null) {
                            serialNumberhard = tokenDetailsOfHard.serialnumber;

                        }

                    }

                }

                String userStatus = "user-status-value-" + i;
                String Certistatus = null;

                if (certifts != null) {
                    if (certifts.getStatus() == CertificateManagement.CERT_STATUS_ACTIVE) {
                        Certistatus = "Valid & Active";
                    } else if (certifts.getStatus() == CertificateManagement.CERT_STATUS_REVOKED) {
                        Certistatus = "Revoked";
                    } else if (certifts.getStatus() == CertificateManagement.CERT_STATUS_EXPIRED) {
                        Certistatus = "Expired";
                    }

                    dCreatedOn = certifts.getCreationdatetime();
                    dLastUpdated = certifts.getExpirydatetime();
                } else {
                    Certistatus = "Not Issued";
                }

                String sotpStatus = "sotp-status-value-" + i;

                String hotpStatus = "hotp-status-value-" + i;

                String hotpType = "hotp-type-value-" + i;

                String issuedOn = "NA";
                if (dCreatedOn != null) {
                    issuedOn = sdf.format(dCreatedOn);
                }

                String ExpireOn = "NA";
                if (dLastUpdated != null) {
                    ExpireOn = sdf.format(dLastUpdated);
                }


    %>
    <tr id="user_search_<%=Users[i].getUserId()%>">
        <td><%=(i + 1)%></td>
         <td><a href="#" class="btn btn-mini" onclick="viewUserID('<%=Users[i].getUserId()%>')" >View ID</a></td>
        <td><%=Users[i].getUserName()%></td>        
        <%if (0 == 0) { %>
        <%if (certifts == null || certifts.getStatus() != CertificateManagement.CERT_STATUS_ACTIVE) {%>
        <td><a href="#" onclick="generatecertificate('<%=Users[i].getUserId()%>')" class="btn btn-mini btn-primary">Issue Now</a></td>
        <% } else {%> 
        <td><a href="#" onclick="loadCertificateDetails('<%=Users[i].getUserId()%>')" class="btn btn-mini">View Certificate</a></td>
        <% }%> 
        <td>
            <button href="#" onclick="uploadCustomerDetails('<%=Users[i].getUserId()%>')" class="btn btn-mini" >Upload Now >></button>
        </td>
        <!--        <td>
                    <a class="btn btn-mini" href="./PendingListForRequster.jsp" data-toggle="modal" id="buttonISOdetails">View List</a></button>
                </td>-->
        <td>
            <div class="btn-group">
                <button class="btn btn-mini">Manage</button>
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="loadsendcertfile('<%=Users[i].getUserId()%>')">Share Certificate(via Email)</a></li>
                    <li class="divider"></li>                    
                    <li><a href="#" onclick="renewcertificate('<%=Users[i].getUserId()%>')">Renew Certificate</a></li>                    
                    <li><a href="#" onclick="loadrevoke('<%=Users[i].getUserId()%>')"><font color="red">Revoke Certificate</font></a></li>
                </ul>
            </div>
        </td>
        <td>
            <div class="btn-group">
                <span class="<%=strLabelHW%>" id="<%=hotpStatus%>"><%=hardPkistatus%></span>
                <button class="btn btn-mini" id="<%=hotpType%>"><%=serialNumberhard%></button>
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#"   onclick="loadUsersID('<%=Users[i].getUserId()%>')">Assign Token</a></li>
                    <!--<li><a href="#" onclick="assignewtoken('<%=Users[i].getUserId()%>','HARDWARE_TOKEN')">Assign New Token</a></li>-->
                    <li><a href="#" onclick="changepkitokenstatus(1, '<%=Users[i].getUserId()%>', 2, '<%=hotpStatus%>')">Activate Token</a></li>
                    <li class="divider"></li>
                    <li><a href="#" onclick="sendpfxfile('<%=Users[i].getUserId()%>')">(Re)send PFX File (via email)</a></li>
                    <li><a href="#" onclick="sendpfxpassword('<%=Users[i].getUserId()%>')">(Re)send PFX Password (via mobile)</a></li>
                    <li class="divider"></li>
                    <li><a href="#" onclick="changepkitokenstatus(-10, '<%=Users[i].getUserId()%>', 2, '<%=hotpStatus%>')" ><font color="red">Un-Assign Token</font></a></li>
                    <li><a href="#" onclick="changepkitokenstatus(-5, '<%=Users[i].getUserId()%>', 2, '<%=hotpStatus%>')" ><font color="red">Mark as Lost</font></a></li>
                </ul>
            </div>
        </td>
<!--    <div id="kycupload" class="modal hide fade" tabindex="-1" role="dialog" style="width: 650px;"   aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3 id="myModalLabel">Upload Documents</h3>
            <p>Please Compress all documents in zip and upload here.</p>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="kycuploadForm">
                    <fieldset>
                         <input type="hidden" id="_usersid" name="_usersid" >
                        
                        <div class="control-group">
                            <label class="control-label"  for="username">Select File:</label>                                    
                            <div class="controls fileupload fileupload-new" data-provides="fileupload">
                                <div class="input-append">
                                    <div class="uneditable-input span4"><i class="icon-file fileupload-exists"></i> 
                                        <span class="fileupload-preview"></span>
                                    </div>
                                    <span class="btn btn-file" >
                                        <span class="fileupload-new">Select file</span>
                                        <span class="fileupload-exists">Change</span>
                                        <input type="file" id="filekycupload" name="filekycupload"/>
                                    </span>
                                    <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                    <button class="btn btn-success" id="buttonkycUpload"  onclick="UploadkycFile()">Upload Now>></button>
                                </div>
                            </div>

                        </div>
                        <br>
                                            <div class="control-group">
                                                <label class="control-label"  for="username">Phone</label>
                                                <div class="controls">
                                                    <input type="text" id="_Phone" name="_Phone" placeholder="phone with country code" class="input-xlarge">
                                                </div>
                                            </div>

                    </fieldset>
                </form>
            </div>
        </div>
            <div class="modal-footer">
                <div id="addUser-result"></div>
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <button class="btn btn-primary" onclick="adduser()" id="addUserButton">Save Details Now>></button>
            </div>
    </div> -->

    <td>
        <div class="btn-group">
            <span class="<%=strLabelSW%>" id="<%=sotpStatus%>"><%=softPkistatus%></span>
            <button class="btn btn-mini">Mobile</button>
            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
            <ul class="dropdown-menu">
                <li><a href="#" onclick="assignewtoken('<%=Users[i].getUserId()%>', 1, '<%=sotpStatus%>')">Assign Token</a></li>
                <li><a href="#" onclick="sendregistrationcode('<%=Users[i].getUserId()%>', 1)">(Re)send Registration Code (via Mobile)</a></li>
                <li class="divider"></li>
                <li><a href="#" onclick="changepkitokenstatus(1, '<%=Users[i].getUserId()%>', 1, '<%=sotpStatus%>')">Active Token</a></li>                            
                <li><a href="#" onclick="changepkitokenstatus(-2, '<%=Users[i].getUserId()%>', 1, '<%=sotpStatus%>')">Suspend Token</a></li>                            

                <li class="divider"></li>
                <li><a href="#" onclick="changepkitokenstatus(-10, '<%=Users[i].getUserId()%>', 1, '<%=sotpStatus%>')"><font color="red">Un-Assign Token</font></a></li>                            
            </ul>
        </div>
    </td>

    <td>
            <a href="#" class="btn btn-mini" onclick="loadUserPKITokenAuditDetails('<%=Users[i].getUserId()%>','<%=Users[i].getUserName() %>')">Audit Download</a>
        </td>
<!--    <td>
        <div class="btn-group">
            <button class="btn btn-mini">Audit for</button>
            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
            <ul class="dropdown-menu">
                <li><a href="#" onclick="PKIAudits('<%=Users[i].getUserId()%>', 'PKITOKEN', '1day')">Today</a></li>
                <li><a href="#" onclick="PKIAudits('<%=Users[i].getUserId()%>', 'PKITOKEN', '7days')">Last 7 days</a></li>
                <li class="divider"></li>
                <li><a href="#" onclick="PKIAudits('<%=Users[i].getUserId()%>', 'PKITOKEN', '1month')">Last 30 days</a></li>
                <li><a href="#" onclick="PKIAudits('<%=Users[i].getUserId()%>', 'PKITOKEN', '2months')">Last 2 months</a></li>
                <li><a href="#" onclick="PKIAudits('<%=Users[i].getUserId()%>', 'PKITOKEN', '3months')">Last 3 months</a></li>
                <li><a href="#" onclick="PKIAudits('<%=Users[i].getUserId()%>', 'PKITOKEN', '6months')">Last 6 months</a></li>
            </ul>
        </div>
    </td>-->
    <td><%=issuedOn%></td>
    <td><%=ExpireOn%></td>
    <% } else { %>
    <td><span class="label label-inverse">Not Available</span></td>
    <td><span class="label label-inverse">Not Available</span></td>
    <td><span class="label label-inverse">Not Available</span></td>
    <td><span class="label label-inverse">Not Available</span></td>
    <td>--</td>
    <td>--</td>
    <td>--</td>

    <% } %>

</tr>
<%
        }%>
</table>

<%  } else if (Users == null) { %>
<h3>No users found...</h3>
<%}%>



<br><br>
