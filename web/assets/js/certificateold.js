function Alert4Cert(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function (result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}
function strcmpCert(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function ChangeCertCategory(value) {
    if (value === 1) {
        $('#_changeCertCategory').val("1");
        $('#_change_Certificate_category_Axiom').html("Certificate");
    }
}

function ChangeCertStateType(value) {
    if (value === 1) {
        $('#_changeCertStatus').val("1");
        $('#_change_Certificate_Status_Axiom').html("Active");
    } else if (value === -10) {
        $('#_changeCertStatus').val("-10");
        $('#_change_Certificate_Status_Axiom').html("Expired");
    } else if (value === -5) {
        $('#_changeCertStatus').val("-5");
        $('#_change_Certificate_Status_Axiom').html("Revoked");
    } else if (value === -1) {
        $('#_changeCertStatus').val("-1");
        $('#_change_Certificate_Status_Axiom').html("Expiring soon");
    }
}



function CertificateReport() {
    var val3 = encodeURIComponent(document.getElementById('startdate').value);
    var val4 = encodeURIComponent(document.getElementById('enddate').value);
    var val1 = document.getElementById('_changeCertStatus').value;
    var s = './certreport?_changeCertStatus=' + val1 + "&_reporttype=" + 1 + '&_enddate=' + val4 + '&_startdate=' + val3+'&_reporttype='+1;
    window.location.href = s;
    return false;
}

function CertificateReportpdf() {
    var val3 = encodeURIComponent(document.getElementById('startdate').value);
    var val4 = encodeURIComponent(document.getElementById('enddate').value);
    var val1 = document.getElementById('_changeCertStatus').value;
    var s = './certreport?_changeCertStatus=' + val1 + "&_reporttype=" + 0 + '&_enddate=' + val4 + '&_startdate=' + val3+'&_reporttype='+0;
    window.location.href = s;
    return false;
}
function CertificateReportTXT() {
    var val3 = encodeURIComponent(document.getElementById('startdate').value);
    var val4 = encodeURIComponent(document.getElementById('enddate').value);
    var val1 = document.getElementById('_changeCertStatus').value;
    var s = './certreport?_changeCertStatus=' + val1 + "&_reporttype=" + 2 + '&_enddate=' + val4 + '&_startdate=' + val3+'&_reporttype='+2;
    window.location.href = s;
    return false;
}
function generatecert() {
    var s = './assigntokenandgeneratecert';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#generatecertificate").serialize(),
        success: function (data) {
            if (strcmpCert(data._result, "error") === 0) {
                $('#generate-certificate-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Cert("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpCert(data._result, "success") === 0) {
                $('#generate-certificate-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Cert("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function generateCertificateTable() {
    var val = document.getElementById('_changeCertCategory').value;
    var val1 = document.getElementById('_changeCertStatus').value;
    var val3 = encodeURIComponent(document.getElementById('startdate').value);
    var val4 = encodeURIComponent(document.getElementById('enddate').value);
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    var todaysDate = mm + '/' + dd + '/' + yyyy;
    var s = './certificatesreporttable.jsp?_changeCertStatus=' + val1 + '&_startdate=' + val3 + '&_enddate=' + val4;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#licenses_data_table').html(data);
            var day_data = null;
            day_data = certDonutChart(val1);
            Morris.Donut({
                element: 'certdonutchart',
                data: day_data
            });
            //Bar Chart
            var day_data1 = null;
            day_data1 = certBarChart(val1);
            Morris.Bar({
                element: 'certbarchart',
                data: day_data1,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function (type) {
                    if (type === 'bar') {

                        return '#0066CC';
                    }
                    else {

                        return '#0066CC';
                    }
                }
            });

        }
//    $('#certdonutchart').empty();
//    $('#certbarchart').empty();
        //donut


    });

}

function certBarChart() {
    var s = './certbarchart';
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}

function certDonutChart() {
    var s = './certdonutchart';
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}
function InvalidRequestCertificate(id) {
    var s = './reportResponse?_requestID=' + id;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpTokens(data._result, "error") == 0) {
                Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpTokens(data._result, "success") == 0) {
                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}