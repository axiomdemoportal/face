function strcmpImage(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function Alert4Image(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}


function LoadImageSettings() {
    var s = './loadImageSettings';

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            $('#_sweetSpot').val(data._sweetSpot);
            $('#_sweetSpotDeviation').val(data._sweetSpotDeviation);
            $('#_xDeviation').val(data._xDeviation);
            $('#_yDeviation').val(data._yDeviation);
            $('#_geoCheck').val(data._geoCheck);
            $('#_deviceProfile').val(data._deviceProfile);
                $('#_honeyTrap').val(data._honeyTrap);
        }
    });
}

function editImageSettings() {
    var s = './editImageSettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#imagesettingsform").serialize(),
        success: function(data) {
            if (strcmpImage(data._result, "error") == 0) {
                $('#save-image-setting-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Image("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpImage(data._result, "success") == 0) {
                Alert4Image("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

