function strcmpTokens(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function Alert4Tokens(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}

function RefreshTokenList() {
    window.location.href = "./otptokens.jsp";
}



function generateRolesReport()
{
    
   var fromDate=document.getElementById("datetimepicker1").value;
   var toDate=document.getElementById("datetimepicker2").value;
   var status=document.getElementById("_changeOTPStatus").value;
   var keyword=document.getElementById("txtrolename").value;
  
     var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');

    if (keyword.length < 1) {
        //Alert4Tokens("Keyword should be meaningful and/or more than 3 characters");
        Alert4Tokens("Search keyword cannot be blank!!!");
        return;
    }

    var s = './rolesTable.jsp?_searchtext=' +encodeURI(keyword)+"&fromDate="+encodeURI(fromDate)+"&toDate="+encodeURI(toDate)+"&status="+encodeURI(status);
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            pleaseWaitDiv.modal('hide');
            $('#users_table_main').html(data);
        }
    });
    

}
//added by Abhiskek
function generateOtpUsage() {

    if (document.getElementById('startdate').value.length == 0 || document.getElementById('enddate').value.length == 0) {
        Alert4Tokens("<span><font color=red>Date Range is not selected!!!</font></span>");
        return;
    }
    var val1 = encodeURIComponent(document.getElementById('startdate').value);
    var val2 = encodeURIComponent(document.getElementById('enddate').value);
    var val3 = document.getElementById('_changeCategory').value;

    var val3 = document.getElementById('_changeCategory').value;
    
    if(val3 === "ChallengeResponse Management"){
        generateChallengeResponseTable(val1, val2);   
        return;
    }    
    
    $('#licenses_data_table').html("<h3>Loading....</h3>");
    var s = './tokenFailureReportTable.jsp?_changeCategory=' + val3 + "&_startdate=" + val1 + "&_enddate=" + val2;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#licenses_data_table').html(data);
            
            var day_data1= null;
            day_data1 = BarChartDemoUsage(val1, val2, val3);
            Morris.Bar({
                element: 'graph1',
                data: day_data1,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {
                        return '#0066CC';
                    }
                    else {
                        return '#0066CC';
                    }
                }
            });
            
            var day_data = null;
            day_data = DonutChartUsage(val1, val2, val3);

            Morris.Donut({
                element: 'graph',
                data: day_data
            });
    
        }
    });    
}
function DonutChartUsage(val, val1, val2) {
    var s = './usagedonutchart?_startdate=' + val + "&_enddate=" + val1 + "&_changeCategory=" + val2;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}


function BarChartDemoUsage(val, val1, val2) {
    var s = './usagedonutchart?_startdate=' + val + "&_enddate=" + val1 + "&_changeCategory=" + val2;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}


function generateChallengeResponseTable(val1, val2) {
    var val3 = val1;
    var val4 = val2;

    if (document.getElementById('startdate').value.length == 0 || document.getElementById('enddate').value.length == 0) {
         Alert4Cert("<span><font color=red>Date Range is not selected!!!</font></span>");
        return;
    }

    $('#licenses_data_table').html("<h3>Loading....</h3>");
    var s = './challengeResponseQnATable.jsp?&_startdate=' + val3 + '&_enddate=' + val4 +"&_itemType=CHALLENGERESPONSE";
    
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#licenses_data_table').html(data);
            
            var day_data = BarChartCA(val3, val4);
            Morris.Bar({
                element: 'graph1',
                data: day_data,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function (type) {
                    if (type === 'bar') {
                        return '#0066CC';
                    }
                    else {
                        return '#0066CC';
                    }
                }
            });
    
            var day_data = DonutChartCA(val3, val4);
            Morris.Donut({
                element: 'graph',
                data: day_data
            });
        }
        
    });
}








//function changeotpstatus(status, userid,_category,_subcategory) {
//    var s = './changeotpstatus?_status=' + status + '&_userid=' + encodeURIComponent(userid) +"&_category="+_category+ "&_subcategory="+_subcategory;
//    $.ajax({
//        type: 'GET',
//        url: s,
//        dataType: 'json',
//        success: function(data) {
//            if (strcmpTokens(data._result, "error") == 0) {
//                Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
//            }
//            else if (strcmpTokens(data._result, "success") == 0) {
//                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
//            //window.setTimeout(RefreshTokenList, 2000);
//            }
//        }
//    });
//}

function losttoken(status, userid, _category, _subcategory, uidiv) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        }
        else {
            var s = './losttoken?_status=' + status + '&_userid=' + encodeURIComponent(userid) + "&_category=" + _category + "&_subcategory=" + _subcategory;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpTokens(data._result, "error") == 0) {
                        Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpTokens(data._result, "success") == 0) {
                        Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                        //window.setTimeout(RefreshTokenList, 2000);

                        var uiToChange = '#' + uidiv;
                        $(uiToChange).html("Unassigned");
//                          $(uidivsn).html("");

//                        $(uiToChange).html(data._value);
                    }
                }
            });
        }
    });
}

function loadotpstatus(status, userid, _category, _subcategory, uidiv) {
    $('#_userstatus').val(status);
    $('#_userID').val(userid);
    $('#_usercategory').val(_category);
    $('#_usersubcategory').val(_subcategory);
    $('#uidiv').val(uidiv);
   // $("#suspendToken").modal();
   suspendToken();
}
function suspendToken() {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        }
        else {
            var status = document.getElementById('_userstatus').value;
            var userid = document.getElementById('_userID').value;
            var _category = document.getElementById('_usercategory').value;
            var _subcategory = document.getElementById('_usersubcategory').value;
            var _suspendreason = document.getElementById('_suspendreason').value;
            var uidiv = document.getElementById('uidiv').value;

            var s = './changeotpstatus?_status=' + status + '&_userid=' + encodeURIComponent(userid)
                    + "&_category=" + _category + "&_subcategory=" + _subcategory + "&_subcategory=" + _subcategory + '&_suspendreason=' + _suspendreason;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpTokens(data._result, "error") == 0) {
                        Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpTokens(data._result, "success") == 0) {
                        Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                        //window.setTimeout(RefreshTokenList, 2000);
                        var uiToChange = '#' + uidiv;
                        $(uiToChange).html(data._value);
                        $('#_suspendreason').val("");
                        $("#suspendToken").modal('hide');
                    }
                }
            });
        }
    });
}

function changeotpstatus(status, userid, _category, _subcategory, uidiv,serialno) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        }
        else {
            var s = './changeotpstatus?_status=' + status + '&_userid=' + encodeURIComponent(userid) + "&_category=" + _category + "&_subcategory=" + _subcategory+"&_serialno="+serialno;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpTokens(data._result, "error") == 0) {
                        Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpTokens(data._result, "success") == 0) {
                        Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                        //window.setTimeout(RefreshTokenList, 2000);
                        var uiToChange = '#' + uidiv;
                        $(uiToChange).html(data._value);
                    }
                }
            });
        }
    });
}

function changesoftstatus(status, userid) {
    var s = './changesoftwarestatus?_status=' + status + '&_userid=' + encodeURIComponent(userid);
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpTokens(data._result, "error") == 0) {
                Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpTokens(data._result, "success") == 0) {
                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                window.setTimeout(RefreshTokenList, 2000);

            }
        }
    });
}


function loadresyncSW(userid, _srno, _category) {
    $("#_userIDS").val(userid);
    $("#_srno").val(_srno);
    $("#_categoryS").val(_category);
    $("#ResyncSoftware").modal();
}

function loadregisterHW(userid, _category, _subcategory) {
    $("#_userid").val(userid);
    $("#_category").val(_category);
    $("#_subcategory").val(_subcategory);
    $("#HardRegistration").modal();
}


function resyncsoftware() {
    var s = './resynctoken';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#ResyncSoftwareform").serialize(),
        success: function(data) {
            if (strcmpTokens(data._result, "error") == 0) {
                $('#ResyncSoftware-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#buttonResyncSoftware').attr("disabled", false);
            }
            else if (strcmpTokens(data._result, "success") == 0) {
                $('#ResyncSoftware-result').html("<span><font color=blue>" + data._message + "</font></span>");
                $('#buttonResyncSoftware').attr("disabled", true);

            }
        }
    });
}

function loadresyncHW(userid, _srno, _category) {
    $("#_userIDHW").val(userid);
    $("#_srnoHW").val(_srno);
    $("#_categoryHW").val(_category);
    $("#ResyncHardware").modal();
}

function resynchardware() {
    var s = './resynctoken';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#ResyncHardwareform").serialize(),
        success: function(data) {
            if (strcmpTokens(data._result, "error") == 0) {
                $('#ResyncHardware-result').html("<span><font color=red>" + data._message+"    Please close!!!" + "</font></span></small>");
//                $('#buttonResyncHardware').attr("disabled", false);
            }
            else if (strcmpTokens(data._result, "success") == 0) {
                $('#ResyncHardware-result').html("<span><font color=blue>" + data._message +"  Please close!!"+ "</font></span>");
//                $('#buttonResyncHardware').attr("disabled", true);
            }
        }
    });
}
function replacehardware() {
    var s = './replaceToken';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#ReplaceHardwareform").serialize(),
        success: function(data) {
            if (strcmpTokens(data._result, "error") == 0) {
                $('#ReplaceHardware-result').html("<span><font color=red>" + data._message+"   Please close!!" + "</font></span></small>");
//                $('#buttonResyncHardware').attr("disabled", false);
            }
            else if (strcmpTokens(data._result, "success") == 0) {
                $('#ReplaceHardware-result').html("<span><font color=blue>" + data._message + "  Please close!!" + "</font></span>");
                $('#_userIDReplaceHW').val("");
                $('#_categoryReplaceHW').val("");
                $('#_subcategoryReplaceHW').val("");
                $('#_oldTokenSerialNo').val("");
                $('#_newTokenSerialNo').val("");

//                $('#buttonResyncHardware').attr("disabled", true);
            }
        }
    });
}







//function assigntokenOOBandSW(userid,_category,_subcategory, uidiv,udiv) {
//    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
//        if (result == false) {
//        } else {
//            var s = './assigntoken?_userid=' + encodeURIComponent(userid) + "&_category="+_category+ "&_subcategory="+_subcategory;
//            $.ajax({
//                type: 'GET',
//                url: s,
//                dataType: 'json',
//                success: function(data) {
//                    if (strcmpTokens(data._result, "error") == 0) {
//                        Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
//                    }
//                    else if (strcmpTokens(data._result, "success") == 0) {
//                        Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
//                        //window.setTimeout(RefreshTokenList, 2000);
//                        var uiToChange = '#' + uidiv;
//                        $(uiToChange).html(data._value);
//                        var uToChange='#' + udiv;
//                        $(uToChange).html(data._status);
//                    }
//                }
//            });
//        }
//    });
//}


function assigntokenOOBandSW(userid, _category, _subcategory, uidiv, udiv, serialNo) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './assigntoken?_userid=' + encodeURIComponent(userid) + "&_category=" + _category + "&_subcategory=" + _subcategory +"&_serialnumber="+serialNo;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpTokens(data._result, "error") == 0) {
                        Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpTokens(data._result, "success") == 0) {
                        Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                        //window.setTimeout(RefreshTokenList, 2000);
                        var uiToChange = '#' + uidiv;
                        $(uiToChange).html(data._value);
                        var uToChange = '#' + udiv;
                        $(uToChange).html(data._status);
                    }
                }
            });
        }
    });
}


//function changetokenOOBandSW(userid,_category,_subcategory, uidiv) {
//    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
//        if (result == false) {
//        } else {
//            var s = './changetokentype?_userid=' + encodeURIComponent(userid) + "&_category="+_category+ "&_subcategory="+_subcategory;
//            $.ajax({
//                type: 'GET',
//                url: s,
//                dataType: 'json',
//                success: function(data) {
//                    if (strcmpTokens(data._result,"error") == 0) {
//                        Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
//                    }
//                    else if (strcmpTokens(data._result,"success") == 0) {
//                        Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
//                        //window.setTimeout(RefreshTokenList, 2000);
//                        var uiToChange = '#' + uidiv;
//                        $(uiToChange).html(data._value);
//                    }
//                }
//            });
//        }
//    });
//}

function changetokenOOBandSW(userid, _category, _subcategory, uidiv) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './changetokentype?_userid=' + encodeURIComponent(userid) + "&_category=" + _category + "&_subcategory=" + _subcategory;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpTokens(data._result, "error") == 0) {
                        Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpTokens(data._result, "success") == 0) {
                        Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                        //window.setTimeout(RefreshTokenList, 2000);
                        var uiToChange = '#' + uidiv;
                        $(uiToChange).html(data._value);
                    }
                }
            });
        }
    });
}



function sendregistration(userid, _category, _subcategory) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            //var s = './sendregistration?_userid=' + userid;
            var s = './sendregistration?_userid=' + encodeURIComponent(userid) + "&_category=" + _category + "&_subcategory=" + _subcategory;

            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpTokens(data._result, "success") == 0) {
                        Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                    } else {
                        Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}

function assignsoftwaretoken(token, userid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './assignsoftwaretoken?_token=' + token + '&_userid=' + encodeURIComponent(userid);
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpTokens(data._result, "error") == 0) {
                        Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpTokens(data._result, "success") == 0) {
                        Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshTokenList, 2000);

                    }
                }
            });
        }
    });
}

function sendPUKCode(type) {
    var s = './sendPukCode?_sendVia=' + type;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#pukform").serialize(),
        success: function(data) {
            if (strcmpTokens(data._result, "error") == 0) {
                Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpTokens(data._result, "success") == 0) {
                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                $("#PUKCODE").modal('hide');
            }
        }
    });

}
function assignhardwaretoken1() {
    var s = './assigntoken';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#HardwareRegistrationform").serialize(),
        success: function(data) {
            if (strcmpTokens(data._result, "error") == 0) {
                Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
                $('#_serialnumber').val("");
            }
            else if (strcmpTokens(data._result, "success") == 0) {
                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                //window.setTimeout(RefreshTokenList, 2000);
                $("#HardRegistration").modal('hide');
                searchOtpUsers();
            }
        }
    });

}
function assignhardwaretoken() {
    var s = './assigntoken';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#HardwareRegistrationform").serialize(),
        success: function(data) {
            if (strcmpTokens(data._result, "error") == 0) {
                Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
                $('#_serialnumber').val("");
            }
            else if (strcmpTokens(data._result, "success") == 0) {
                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                //window.setTimeout(RefreshTokenList, 2000);
                $("#HardRegistration").modal('hide');
                searchOtpUsers();
            }
        }
    });

}

function loadDetails(_userid, _category, _subcategory) {
    var s = './getpushmapper?_mapperid=' + encodeURIComponent(_mid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpPushmappers(data._result, "success") == 0) {

                $("#_userid").val(data._mapperID);
                $("#_category").val(data._messagefomat);
                $("#_subcategory").val(data._templateName);
                $("#_templateTypeE").val(data._templateType);
                $("#_templateClassE").val(data._templateClass);
                $('#editPushMappers-result').html("");
                $("#editPushMappers").modal();
            } else {
                Alert4Pushmappers("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}



function changehardstatus(status, userid) {
    var s = './changehardwarestatus?_status=' + status + '&_userid=' + encodeURIComponent(userid);
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpTokens(data._result, "error") == 0) {
                Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpTokens(data._result, "success") == 0) {
                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                window.setTimeout(RefreshTokenList, 2000);
            }
        }
    });
}



function searchOtpUsers() {
    var val = document.getElementById('_keyword').value;
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');

    if (val.length < 1) {
        //Alert4Tokens("Keyword should be meaningful and/or more than 3 characters");
        Alert4Tokens("Search keyword cannot be blank!!!");
        return;
    }

    var s = './otptables.jsp?_searchtext=' + val;
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            pleaseWaitDiv.modal('hide');
            $('#users_table_main').html(data);
        }
    });
}


function OtpAudits(_userid, _itemtype, _duration) {

    var s = './otpaudit?_userid=' + encodeURIComponent(_userid) + "&_itemType=" + _itemtype + "&_duration=" + _duration + "&_format=pdf";
    window.location.href = s;
    return false;
}


function ChangeOtpCategory(value) {
    //1 for enabled
    //0 for disabled
    if (value === 1) {
        $('#_changeCategory').val("1");
        $('#_change_category_Axiom').html("SOFTWARE OTP TOKEN");
    } else if (value === 2) {
        $('#_changeCategory').val("2");
        $('#_change_category_Axiom').html("HARDWARE OTP TOKEN");
    } else if (value === 3) {
        $('#_changeCategory').val("3");
        $('#_change_category_Axiom').html("OUT OF BAND OTP TOKEN");
    }
}


//function generateOtpTable() {
//    var val = document.getElementById('_changeCategory').value;
//    var val1 = document.getElementById('_changeOTPStatus').value;
//     var s = './otptokensreporttable.jsp?_changeCategory=' + val +"&_changeOTPStatus="+val1;
//    $.ajax({
//        type: 'GET',
//        url: s,
//        success: function(data) {
//            $('#search_otp_main').html(data);
//        }
//    });
//   
//}
//
//function OTPReport(){
//     var val = document.getElementById('_changeCategory').value;
//    var val1 = document.getElementById('_changeOTPStatus').value;
//    var s = './otptokensreport?_changeCategory='+val +"&_changeOTPStatus="+val1+"&_reporttype="+1;
//    window.location.href = s;
//    return false;
//}
// 
//function OTPReportpdf(){
//     var val = document.getElementById('_changeCategory').value;
//    var val1 = document.getElementById('_changeOTPStatus').value;
//    var s = './otptokensreport?_changeCategory='+val +"&_changeOTPStatus="+val1+"&_reporttype="+0;
//    window.location.href = s;
//    return false;
//}

function ChangeOtpStateType(value) {

    if (value === 1) {
        $('#_changeOTPStatus').val("1");
        $('#_change_OtpStatus_Axiom').html("Active State");
    } else if (value === -1) {
        $('#_changeOTPStatus').val("-1");
        $('#_change_OtpStatus_Axiom').html("Locked State");
    } else if (value === 0) {
        $('#_changeOTPStatus').val("0");
        $('#_change_OtpStatus_Axiom').html("Assigned State");
    } else if (value === -5) {
        $('#_changeOTPStatus').val("-5");
        $('#_change_OtpStatus_Axiom').html("Lost State");
    } else if (value === -2) {
        $('#_changeOTPStatus').val("-2");
        $('#_change_OtpStatus_Axiom').html("Suspended State");
    } else if (value === 2) {
        $('#_changeOTPStatus').val("2");
        $('#_change_OtpStatus_Axiom').html("Show All States");
    } else if (value === -10) {
        $('#_changeOTPStatus').val("-10");
        $('#_change_OtpStatus_Axiom').html("Free And Available State");
    }

}


function DonutChart(val, val1, val2) {
    var s = './otpdonutchart?_changeCategory=' + val + "&_changeOTPStatus=" + val1 + "&_UnitId=" + val2;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}

function BarChartDemo(val, val1, val2) {
    var s = './otpbarchart?_changeCategory=' + val + "&_changeOTPStatus=" + val1 + "&_UnitId=" + val2;
    ;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}


function WebTokenExpiry(value) {
    if (value === 5) {
        $('#_SWWebTokenExpiryTime').val("5");
        $('#_SWWebTokenExpiryTime_div').html("5 Minutes");
    } else if (value === 10) {
        $('#_SWWebTokenExpiryTime').val("10");
        $('#_SWWebTokenExpiryTime_div').html("10 Minutes");
    } else if (value === 15) {
        $('#_SWWebTokenExpiryTime').val("15");
        $('#_SWWebTokenExpiryTime_div').html("15 Minutes");
    } else if (value === 20) {
        $('#_SWWebTokenExpiryTime').val("20");
        $('#_SWWebTokenExpiryTime_div').html("20 Minutes");
    }
}

function WebTokenPinAttempt(value) {
    if (value === 3) {
        $('#_SWWebTokenPinAttempt').val("3");
        $('#_SWWebTokenPinAttempt_div').html("3 Attempts");
    } else if (value === 5) {
        $('#_SWWebTokenPinAttempt').val("5");
        $('#_SWWebTokenPinAttempt_div').html("5 Attempts");
    } else if (value === 7) {
        $('#_SWWebTokenPinAttempt').val("7");
        $('#_SWWebTokenPinAttempt_div').html("7 Attempts");
    }
}
function UnlockAfter(value) {
    if (value === 3) {
        $('#_autoUnlockAfter').val("3");
        $('#_autoUnlockAfter_div').html("3 hours");
    } else if (value === 6) {
        $('#_autoUnlockAfter').val("6");
        $('#_autoUnlockAfter_div').html("6 hours");
    } else if (value === 9) {
        $('#_autoUnlockAfter').val("9");
        $('#_autoUnlockAfter_div').html("9 hours");
    } else if (value === 12) {
        $('#_autoUnlockAfter').val("12");
        $('#_autoUnlockAfter_div').html("12 hours");
    } else if (value === 24) {
        $('#_autoUnlockAfter').val("24");
        $('#_autoUnlockAfter_div').html("24 hours");
    }
     else if (value === 43800) {
        $('#_autoUnlockAfter').val("43800");
        $('#_autoUnlockAfter_div').html("Disabled");
    }
}

function UnlockOOBAfter(value) {
   
    if (value === 3) {
        $('#_autoUnlockOOBAfter').val("3");
        $('#_autoUnlockOOBAfter_div').html("3 hours");
    } else if (value === 6) {
        $('#_autoUnlockOOBAfter').val("6");
        $('#_autoUnlockOOBAfter_div').html("6 hours");
    } else if (value === 9) {
        $('#_autoUnlockOOBAfter').val("9");
        $('#_autoUnlockOOBAfter_div').html("9 hours");
    } else if (value === 12) {
        $('#_autoUnlockOOBAfter').val("12");
        $('#_autoUnlockOOBAfter_div').html("12 hours");
    } else if (value === 24) {
        $('#_autoUnlockOOBAfter').val("24");
        $('#_autoUnlockOOBAfter_div').html("24 hours");
    }
    else if (value === 43800) {
        $('#_autoUnlockOOBAfter').val("43800");
        $('#_autoUnlockOOBAfter_div').html("Disabled");
    }
}
function UnlockOOBAfter(value) {
   
    if (value === 3) {
        $('#_autoUnlockOOBAfter').val("3");
        $('#_autoUnlockOOBAfter_div').html("3 hours");
    } else if (value === 6) {
        $('#_autoUnlockOOBAfter').val("6");
        $('#_autoUnlockOOBAfter_div').html("6 hours");
    } else if (value === 9) {
        $('#_autoUnlockOOBAfter').val("9");
        $('#_autoUnlockOOBAfter_div').html("9 hours");
    } else if (value === 12) {
        $('#_autoUnlockOOBAfter').val("12");
        $('#_autoUnlockOOBAfter_div').html("12 hours");
    } else if (value === 24) {
        $('#_autoUnlockOOBAfter').val("24");
        $('#_autoUnlockOOBAfter_div').html("24 hours");
    }else if (value === 43800) {
        $('#_autoUnlockOOBAfter').val("43800");
        $('#_autoUnlockOOBAfter_div').html("Disabled");
    }
}
function UnlockSWAfter(value) {
    if (value === 3) {
        $('#_autoUnlockSWAfter').val("3");
        $('#_autoUnlockSWAfter_div').html("3 hours");
    } else if (value === 6) {
        $('#_autoUnlockSWAfter').val("6");
        $('#_autoUnlockSWAfter_div').html("6 hours");
    } else if (value === 9) {
        $('#_autoUnlockSWAfter').val("9");
        $('#_autoUnlockSWAfter_div').html("9 hours");
    } else if (value === 12) {
        $('#_autoUnlockSWAfter').val("12");
        $('#_autoUnlockSWAfter_div').html("12 hours");
    } else if (value === 24) {
        $('#_autoUnlockSWAfter').val("24");
        $('#_autoUnlockSWAfter_div').html("24 hours");
    } else if (value === 43800) {
        $('#_autoUnlockSWAfter').val("43800");
        $('#_autoUnlockSWAfter_div').html("Disable");
    }
}
function UnlockHWAfter(value) {
    if (value === 3) {
        $('#_autoUnlockHWAfter').val("3");
        $('#_autoUnlockHWAfter_div').html("3 hours");
    } else if (value === 6) {
        $('#_autoUnlockHWAfter').val("6");
        $('#_autoUnlockHWAfter_div').html("6 hours");
    } else if (value === 9) {
        $('#_autoUnlockHWAfter').val("9");
        $('#_autoUnlockHWAfter_div').html("9 hours");
    } else if (value === 12) {
        $('#_autoUnlockHWAfter').val("12");
        $('#_autoUnlockHWAfter_div').html("12 hours");
    } else if (value === 24) {
        $('#_autoUnlockHWAfter').val("24");
        $('#_autoUnlockHWAfter_div').html("24 hours");
    } else if (value === 43800) {
        $('#_autoUnlockHWAfter').val("43800");
        $('#_autoUnlockHWAfter_div').html("Disable");
    }
}
//function generateOtpTable() {
//    var val = document.getElementById('_changeCategory').value;
//    var val1 = document.getElementById('_changeOTPStatus').value;
//    
//    $('#graph').empty();
//    $('#graph1').empty();
//    
//    var day_data = DonutChart(val,val1);
//    Morris.Donut({
//        element: 'graph',
//        data: day_data
//
//    });
//        
//    var day_data = BarChartDemo(val,val1);
//    Morris.Bar({
//        element: 'graph1',
//        data: day_data,
//        xkey: 'label',
//        ykeys: ['value'],
//        labels: ['value'],
//        barColors: function(type) {
//            if (type === 'bar') {
//
//                return '#0066CC';
//            }
//            else {
//
//                return '#0066CC';
//            }
//        }
//    });
//    var s = './otptokensreporttable.jsp?_changeCategory=' + val + "&_changeOTPStatus=" + val1;
//    $.ajax({
//        type: 'GET',
//        url: s,
//        success: function(data) {
//            $('#licenses_data_table').html(data);
//        }
//    });
//
//}

function OTPReport() {
    var val = document.getElementById('_changeCategory').value;
    var val1 = document.getElementById('_changeOTPStatus').value;
    var val2 = document.getElementById('_UnitId').value;
    var s = './otptokensreport?_changeCategory=' + val + "&_changeOTPStatus=" + val1 + "&_reporttype=" + 1 + "&_UnitId=" + val2;
    window.location.href = s;
    return false;
}

function OTPReportpdf() {
    var val = document.getElementById('_changeCategory').value;
    var val1 = document.getElementById('_changeOTPStatus').value;
    var val2 = document.getElementById('_UnitId').value;
    var s = './otptokensreport?_changeCategory=' + val + "&_changeOTPStatus=" + val1 + "&_reporttype=" + 0 + "&_UnitId=" + val2;
    window.location.href = s;
    return false;
}

function OTPReportTXT() {
    var val = document.getElementById('_changeCategory').value;
    var val1 = document.getElementById('_changeOTPStatus').value;
    var val2 = document.getElementById('_UnitId').value;
    var s = './otptokensreport?_changeCategory=' + val + "&_changeOTPStatus=" + val1 + "&_reporttype=" + 2 + "&_UnitId=" + val2;
    window.location.href = s;
    return false;
}

function OTPLength(value) {
    if (value === 6) {
        $('#_OTPLength').val("6");
        $('#_OTPLength_div').html("6 Digits");
    } else if (value === 7) {
        $('#_OTPLength').val("7");
        $('#_OTPLength_div').html("7 Digits");
    } else if (value === 8) {
        $('#_OTPLength').val("8");
        $('#_OTPLength_div').html("8 Digits");
    }
}


function SOTPLength(value) {
    if (value === 6) {
        $('#_SOTPLength').val("6");
        $('#_SOTPLength_div').html("6 Digits");
    } else if (value === 7) {
        $('#_SOTPLength').val("7");
        $('#_SOTPLength_div').html("7 Digits");
    } else if (value === 8) {
        $('#_SOTPLength').val("8");
        $('#_SOTPLength_div').html("8 Digits");
    }
}

function OATHAlgo(value) {
    if (value === 1) {
        $('#_OATHAlgoType').val("1");
        $('#_OATHAlgoType_div').html("Event Based");
    } else if (value === 2) {
        $('#_OATHAlgoType').val("2");
        $('#_OATHAlgoType_div').html("Time Based");
    }
}

//sw otp 

function SWOTPLength(value) {
    if (value === 6) {
        $('#_SWOTPLength').val("6");
        $('#_SWOTPLength_div').html("6 Digits");
    } else if (value === 7) {
        $('#_SWOTPLength').val("7");
        $('#_SWOTPLength_div').html("7 Digits");
    } else if (value === 8) {
        $('#_SWOTPLength').val("8");
        $('#_SWOTPLength_div').html("8 Digits");
    }
}


function SWSOTPLength(value) {
    if (value === 6) {
        $('#_SWSOTPLength').val("6");
        $('#_SWSOTPLength_div').html("6 Digits");
    } else if (value === 7) {
        $('#_SWSOTPLength').val("7");
        $('#_SWSOTPLength_div').html("7 Digits");
    } else if (value === 8) {
        $('#_SWSOTPLength').val("8");
        $('#_SWSOTPLength_div').html("8 Digits");
    }
}

function SWOATHAlgo(value) {
    if (value === 1) {
        $('#_SWOATHAlgoType').val("1");
        $('#_SWOATHAlgoType_div').html("Event Based");
    } else if (value === 2) {
        $('#_SWOATHAlgoType').val("2");
        $('#_SWOATHAlgoType_div').html("Time Based");
    }
}

//HW OTP
function HWOTPLength(value) {
    if (value === 6) {
        $('#_HWOTPLength').val("6");
        $('#_HWOTPLength_div').html("6 Digits");
    } else if (value === 7) {
        $('#_HWOTPLength').val("7");
        $('#_HWOTPLength_div').html("7 Digits");
    } else if (value === 8) {
        $('#_HWOTPLength').val("8");
        $('#_HWOTPLength_div').html("8 Digits");
    }
}


function HWSOTPLength(value) {
    if (value === 6) {
        $('#_HWSOTPLength').val("6");
        $('#_HWSOTPLength_div').html("6 Digits");
    } else if (value === 7) {
        $('#_HWSOTPLength').val("7");
        $('#_HWSOTPLength_div').html("7 Digits");
    } else if (value === 8) {
        $('#_HWSOTPLength').val("8");
        $('#_HWSOTPLength_div').html("8 Digits");
    }
}

function HWOATHAlgo(value) {
    if (value === 1) {
        $('#_HWOATHAlgoType').val("1");
        $('#_HWOATHAlgoType_div').html("Event Based");
    } else if (value === 2) {
        $('#_HWOATHAlgoType').val("2");
        $('#_HWOATHAlgoType_div').html("Time Based");
    }
}

//OTP TOken Setting

function OTPAttempts(value) {
    if (value === 3) {
        $('#_OTPAttempts').val("3");
        $('#_OTPAttempts_div').html("3 times");
    } else if (value === 5) {
        $('#_OTPAttempts').val("5");
        $('#_OTPAttempts_div').html("5 times");
    } else if (value === 7) {
        $('#_OTPAttempts').val("7");
        $('#_OTPAttempts_div').html("7 times");
    }
}


function ValidationSteps(value) {
    if (value === 1) {
        $('#_ValidationSteps').val("1");
        $('#_ValidationSteps_div').html("1 step");
    } else if (value === 3) {
        $('#_ValidationSteps').val("3");
        $('#_ValidationSteps_div').html("3 steps");
    } else if (value === 5) {
        $('#_ValidationSteps').val("5");
        $('#_ValidationSteps_div').html("5 steps");
    } else if (value === 7) {
        $('#_ValidationSteps').val("7");
        $('#_ValidationSteps_div').html("7 steps");
    } else if (value === 10) {
        $('#_ValidationSteps').val("10");
        $('#_ValidationSteps_div').html("10 steps");
    }
}

function TimeDuration(value) {
    if (value === 10) {
        $('#_duration').val("10");
        $('#_duration_div').html("10 seconds");
    } else if (value === 30) {
        $('#_duration').val("30");
        $('#_duration_div').html("30 seconds");
    } else if (value === 60) {
        $('#_duration').val("60");
        $('#_duration_div').html("60 seconds");
    } else if (value === 120) {
        $('#_duration').val("120");
        $('#_duration_div').html("120 seconds");
    } else if (value === 180) {
        $('#_duration').val("180");
        $('#_duration_div').html("180 seconds");
    }
}

function MultipleTokens(value) {
    if (value == true) {
        $('#_MultipleToken').val("true");
        $('#_MultipleToken_div').html("Yes, reset attempts for all tokens.");
    } else if (value == false) {
        $('#_MultipleToken').val("false");
        $('#_MultipleToken_div').html("No, do not reset attempts for all tokens. ");
    }
}


function EnforceMasking(value) {
    if (value == true) {
        $('#_EnforceMasking').val(true);
        $('#_EnforceMasking_div').html("Yes, Apply Masking.");
    } else if (value == false) {
        $('#_EnforceMasking').val(false);
        $('#_EnforceMasking_div').html("No, Disable Masking.");
    }
}



function RegistrationExpiryTime(value) {
    if (value === 3) {
        $('#_RegistrationExpiryTime').val("3");
        $('#_RegistrationExpiryTime_div').html("3 minutes");
    } else if (value === 5) {
        $('#_RegistrationExpiryTime').val("5");
        $('#_RegistrationExpiryTime_div').html("5 minutes");
    } else if (value === 10) {
        $('#_RegistrationExpiryTime').val("10");
        $('#_RegistrationExpiryTime_div').html("10 minutes");
    } else if (value === 30) {
        $('#_RegistrationExpiryTime').val("30");
        $('#_RegistrationExpiryTime_div').html("30 minutes");
    }
}



function editOTPSettings() {
    var s = './editotpsettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#otpsettingsform").serialize(),
        success: function(data) {
            if (strcmpTokens(data._result, "error") == 0) {
                $('#save-otp-settings-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpTokens(data._result, "success") == 0) {
//                $('#save-otp-settings-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}


function LoadOTPSettings() {
    var s = './loadotpsettings';

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {

            $('#_autoUnlockAfter').val(data._autoUnlockAfter);
            if (data._autoUnlockAfter === 3)
                UnlockAfter(3);
            else if (data._autoUnlockAfter === 6)
                UnlockAfter(6);
            else if (data._autoUnlockAfter === 9)
                UnlockAfter(9);
            else if (data._autoUnlockAfter === 12)
                UnlockAfter(12);
            else if (data._autoUnlockAfter === 24)
                UnlockAfter(24);

            $('#_autoUnlockOOBAfter').val(data._autoUnlockOOBAfter);
            if (data._autoUnlockOOBAfter === 3)
                UnlockOOBAfter(3);
            else if (data._autoUnlockOOBAfter === 6)
                UnlockOOBAfter(6);
            else if (data._autoUnlockOOBAfter === 9)
                UnlockOOBAfter(9);
            else if (data._autoUnlockOOBAfter === 12)
                UnlockOOBAfter(12);
            else if (data._autoUnlockOOBAfter === 24)
                UnlockOOBAfter(24);
             else if (data._autoUnlockOOBAfter === 43800)
                UnlockOOBAfter(43800);

            $('#_autoUnlockSWAfter').val(data._autoUnlockSWAfter);
            if (data._autoUnlockSWAfter === 3)
                UnlockSWAfter(3);
            else if (data._autoUnlockSWAfter === 6)
                UnlockSWAfter(6);
            else if (data._autoUnlockSWAfter === 9)
                UnlockSWAfter(9);
            else if (data._autoUnlockSWAfter === 12)
                UnlockSWAfter(12);
            else if (data._autoUnlockSWAfter === 24)
                UnlockSWAfter(24);

            $('#_autoUnlockHWAfter').val(data._autoUnlockHWAfter);
            if (data._autoUnlockHWAfter === 3)
                UnlockHWAfter(3);
            else if (data._autoUnlockHWAfter === 6)
                UnlockHWAfter(6);
            else if (data._autoUnlockHWAfter === 9)
                UnlockHWAfter(9);
            else if (data._autoUnlockHWAfter === 12)
                UnlockHWAfter(12);
            else if (data._autoUnlockHWAfter === 24)
                UnlockHWAfter(24);



            $('#_OTPLength').val(data._OTPLength);
            if (data._OTPLength === 6)
                OTPLength(6);
            else if (data._OTPLength === 7)
                OTPLength(7);
            else if (data._OTPLength === 8)
                OTPLength(8);


            $('#_SOTPLength').val(data._SOTPLength);
            if (data._SOTPLength === 6)
                SOTPLength(6);
            else if (data._SOTPLength === 7)
                SOTPLength(7);
            else if (data._SOTPLength === 8)
                SOTPLength(8);

            $('#_OATHAlgoType').val(data._OATHAlgoType);
            if (data._OATHAlgoType === 1)
                OATHAlgo(1);
            else if (data._OATHAlgoType === 2)
                OATHAlgo(2);

            //sw

            $('#_SWOTPLength').val(data._SWOTPLength);
            if (data._SWOTPLength === 6)
                SWOTPLength(6);
            else if (data._SWOTPLength === 7)
                SWOTPLength(7);
            else if (data._SWOTPLength === 8)
                SWOTPLength(8);


            $('#_SWSOTPLength').val(data._SWSOTPLength);
            if (data._SWSOTPLength === 6)
                SWSOTPLength(6);
            else if (data._SWSOTPLength === 7)
                SWSOTPLength(7);
            else if (data._SWSOTPLength === 8)
                SWSOTPLength(8);

            $('#_SWOATHAlgoType').val(data._SWOATHAlgoType);
            if (data._SWOATHAlgoType === 1)
                SWOATHAlgo(1);
            else if (data._SWOATHAlgoType === 2)
                SWOATHAlgo(2);

            //hw
//            alert(data._HWOTPLength);
            $('#_HWOTPLength').val(data._HWOTPLength);
            if (data._HWOTPLength === 6)
                HWOTPLength(6);
            else if (data._HWOTPLength === 7)
                HWOTPLength(7);
            else if (data._HWOTPLength === 8)
                HWOTPLength(8);


            $('#_HWSOTPLength').val(data._HWSOTPLength);
            if (data._HWSOTPLength === 6)
                HWSOTPLength(6);
            else if (data._HWSOTPLength === 7)
                HWSOTPLength(7);
            else if (data._HWSOTPLength === 8)
                HWSOTPLength(8);

            $('#_HWOATHAlgoType').val(data._HWOATHAlgoType);
            if (data._HWOATHAlgoType === 1)
                HWOATHAlgo(1);
            else if (data._HWOATHAlgoType === 2)
                HWOATHAlgo(2);
            //attempt  

            $('#_OTPAttempts').val(data._OTPAttempts);
            if (data._OTPAttempts === 3)
                OTPAttempts(3);
            else if (data._OTPAttempts === 5)
                OTPAttempts(5);
            else if (data._OTPAttempts === 7)
                OTPAttempts(7);


            $('#_ValidationSteps').val(data._ValidationSteps);
            if (data._ValidationSteps === 1)
                ValidationSteps(1);
            else if (data._ValidationSteps === 3)
                ValidationSteps(3);
            else if (data._ValidationSteps === 5)
                ValidationSteps(5);
            else if (data._ValidationSteps === 7)
                ValidationSteps(7);
            else if (data._ValidationSteps === 10)
                ValidationSteps(10);

            $('#_duration').val(data._duration);
            if (data._duration === 10)
                TimeDuration(10);
            else if (data._duration === 30)
                TimeDuration(30);
            else if (data._duration === 60)
                TimeDuration(60);
            else if (data._duration === 120)
                TimeDuration(120);
            else if (data._duration === 180)
                TimeDuration(180);

            $('#_MultipleToken').val(data._MultipleToken);
            if (data._MultipleToken === true)
                MultipleTokens(true);
            else if (data._MultipleToken === false)
                MultipleTokens(false);

            $('#_RegistrationExpiryTime').val(data._RegistrationExpiryTime);
            //changed for text box
//            if (data._RegistrationExpiryTime === 3)
//                RegistrationExpiryTime(3);
//            else if (data._RegistrationExpiryTime === 5)
//                RegistrationExpiryTime(5);
//            else if (data._RegistrationExpiryTime === 10)
//                RegistrationExpiryTime(10);
//            else if (data._RegistrationExpiryTime === 30)
//                RegistrationExpiryTime(30);


            $('#_EnforceMasking').val(data._EnforceMasking);
            if (data._EnforceMasking === true)
                EnforceMasking(true);
            else if (data._EnforceMasking === false)
                EnforceMasking(false);

//              alert(data._thresholdCount);
            //$('#_thresholdCount').val(data._thresholdCount);
            $('#_allowAlertT').val(data._allowAlertT);
            if (data._allowAlertT == true)
                allowAlertT(1, '#_allowAlertT_div', 'Enable');
            else if (data._allowAlertT == false)
                allowAlertT(2, '#_allowAlertT_div', 'Disable');

            $('#_otpExpiryAfterSession').val(data._otpExpiryAfterSession);
            if (data._otpExpiryAfterSession == true)
                tokenExpiryAfterSession(1, '#_otpExpiryAfterSession_div', 'Enable');
            else if (data._otpExpiryAfterSession == false)
                tokenExpiryAfterSession(0, '#_otpExpiryAfterSession_div', 'Disable');

            $('#_otpLockedAfter').val(data._otpLockedAfter);
            if (data._otpLockedAfter == 5)
                tokenExpiryAfterXDays(5, '#_otpLockedAfter_div', '5 Days');
            else if (data._otpLockedAfter == 10)
                tokenExpiryAfterXDays(10, '#_otpLockedAfter_div', '10 Days');
            else if (data._otpLockedAfter == 15)
                tokenExpiryAfterXDays(15, '#_otpLockedAfter_div', '15 Days');
            else if (data._otpLockedAfter == 30)
                tokenExpiryAfterXDays(30, '#_otpLockedAfter_div', '30 Days');

//             alert(data._otpExpiryAfterSessionSW);
            $('#_otpExpiryAfterSessionSW').val(data._otpExpiryAfterSessionSW);
            if (data._otpExpiryAfterSessionSW == true)
                tokenExpiryAfterSessionSW(1, '#_otpExpiryAfterSessionSW_div', 'Enable');
            else if (data._otpExpiryAfterSessionSW == false)
                tokenExpiryAfterSessionSW(0, '#_otpExpiryAfterSessionSW_div', 'Disable');

            $('#_otpLockedAfterSW').val(data._otpLockedAfterSW);
            if (data._otpLockedAfterSW == 5)
                tokenExpiryAfterXDaysSW(5, '#_otpLockedAfterSW_div', '5 Days');
            else if (data._otpLockedAfterSW == 10)
                tokenExpiryAfterXDaysSW(10, '#_otpLockedAfterSW_div', '10 Days');
            else if (data._otpLockedAfterSW == 15)
                tokenExpiryAfterXDaysSW(15, '#_otpLockedAfterSW_div', '15 Days');
            else if (data._otpLockedAfterSW == 30)
                tokenExpiryAfterXDaysSW(30, '#_otpLockedAfterSW_div', '30 Days');

            $('#_otpExpiryAfterSessionHW').val(data._otpExpiryAfterSessionHW);
            if (data._otpExpiryAfterSessionHW == true)
                tokenExpiryAfterSessionHW(1, '#_otpExpiryAfterSessionHW_div', 'Enable');
            else if (data._otpExpiryAfterSessionHW == false)
                tokenExpiryAfterSessionHW(0, '#_otpExpiryAfterSessionHW_div', 'Disable');

            $('#_otpLockedAfterHW').val(data._otpLockedAfterHW);
            if (data._otpLockedAfterHW == 5)
                tokenExpiryAfterXDaysHW(5, '#_otpLockedAfterHW_div', '5 Days');
            else if (data._otpLockedAfterHW == 10)
                tokenExpiryAfterXDaysHW(10, '#_otpLockedAfterHW_div', '10 Days');
            else if (data._otpLockedAfterHW == 15)
                tokenExpiryAfterXDaysHW(15, '#_otpLockedAfterHW_div', '15 Days');
            else if (data._otpLockedAfterHW == 30)
                tokenExpiryAfterXDaysHW(30, '#_otpLockedAfterHW_div', '30 Days');


            $('#_gatewayType').val(data._gatewayTypeT);
            if (data._gatewayTypeT == 1)
                AlertViaT(1, '#_gatewayTypeT_div', 'SMS');
            else if (data._gatewayTypeT == 2)
                AlertViaT(2, '#_gatewayTypeT_div', 'USSD');
            else if (data._gatewayTypeT == 3)
                AlertViaT(3, '#_gatewayTypeT_div', 'VOICE');
            else if (data._gatewayTypeT == 4)
                AlertViaT(4, '#_gatewayTypeT_div', 'EMAIL');
            else if (data._gatewayTypeT == 5)
                AlertViaT(5, '#_gatewayTypeT_div', 'FAX');
            else if (data._gatewayTypeT == 18)
                AlertViaT(18, '#_gatewayTypeT_div', 'ANDROIDPUSH');
            else if (data._gatewayTypeT == 19)
                AlertViaT(19, '#_gatewayTypeT_div', 'IPHONEPUSH');


            $('#_SWWebTokenExpiryTime').val(data._SWWebTokenExpiryTime);
            if (data._SWWebTokenExpiryTime === 5)
                WebTokenExpiry(5);
            else if (data._SWWebTokenExpiryTime === 10)
                WebTokenExpiry(10);
            else if (data._SWWebTokenExpiryTime === 15)
                WebTokenExpiry(15);
            else if (data._SWWebTokenExpiryTime === 20)
                WebTokenExpiry(20);



            $('#_SWWebTokenPinAttempt').val(data._SWWebTokenPinAttempt);
            if (data._SWWebTokenPinAttempt === 3)
                WebTokenPinAttempt(3);
            else if (data._SWWebTokenPinAttempt === 5)
                WebTokenPinAttempt(5);
            else if (data._SWWebTokenPinAttempt === 7)
                WebTokenPinAttempt(7);
            setuashingalog(data._hashalgo,'#_hashingalgo_div',data._hashalgo);
            $('#_pukCodeLengthHW').val(data._pukCodeLengthHW);
            if (data._pukCodeLengthHW == 4)
                pukCodeLengthHW(4, '#_pukCodeLengthHW_div', '4 Digits');
            else if (data._pukCodeLengthHW == 6)
                pukCodeLengthHW(6, '#_pukCodeLengthHW_div', '6 Digits');
            else if (data._pukCodeLengthHW == 8)
                pukCodeLengthHW(8, '#_pukCodeLengthHW_div', '8 Digits');

            $('#_pukCodeValidityHW').val(data._pukCodeValidityHW);
            if (data._pukCodeValidityHW == 60)
                pukCodeValidityHW(60, '#_pukCodeValidityHW_div', '1 Minutes');
            else if (data._pukCodeValidityHW == 180)
                pukCodeValidityHW(180, '#_pukCodeValidityHW_div', '3 Minutes');
            else if (data._pukCodeValidityHW == 300)
                pukCodeValidityHW(300, '#_pukCodeValidityHW_div', '5 Minutes');
            else if (data._pukCodeValidityHW == 600)
                pukCodeValidityHW(600, '#_pukCodeValidityHW_div', '10 Minutes');
            else if (data._pukCodeValidityHW == 1200)
                pukCodeValidityHW(1200, '#_pukCodeValidityHW_div', '20 Minutes');
            else if (data._pukCodeValidityHW == 1800)
                pukCodeValidityHW(1800, '#_pukCodeValidityHW_div', '30 Minutes');
        }
    });
}

function generateOtpTable() {
    var val = document.getElementById('_changeCategory').value;
    var val1 = document.getElementById('_changeOTPStatus').value;

    var s = './otptokensreporttable.jsp?_changeCategory=' + val + "&_changeOTPStatus=" + val1;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#licenses_data_table').html(data);

            var day_data = DonutChart(val, val1);
            Morris.Donut({
                element: 'graph',
                data: day_data

            });

            var day_data = BarChartDemo(val, val1);
            Morris.Bar({
                element: 'graph1',
                data: day_data,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {

                        return '#0066CC';
                    }
                    else {

                        return '#0066CC';
                    }
                }
            });

        }
        //    $('#graph').empty();
        //    $('#graph1').empty();
        //    
    });

}

function UploadTokensFileXML()
{
    $.ajaxFileUpload
            (
                    {
                        type: 'POST',
                        url: './hwotptokeuploadsubmit',
                        fileElementId: 'fileXMLToUploadEAD',
                        dataType: 'json',
                        success: function(data, status)
                        {

                            if (strcmpTokens(data._result, "error") == 0) {
                                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");

                            }
                            else if (strcmpTokens(data._result, "success") == 0) {
                                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");

                            }
                            $('#download-Entries').html("<a href=./failedtoaddtokenentries?_Format=" + data._iformat + ">Download Fail Entry File</a>");
                        },
                        error: function(data, status, e)
                        {
                            Alert4Tokens(e);
                        }
                    }
            )
    return false;
}

function uploadlic() {
    var ele = document.getElementById("uploadXMLImageEAD");
    var text = document.getElementById("displayXMLTextEAD");
    if (ele.style.display == "block") {
        ele.style.display = "none";
        text.innerHTML = "click here";
    }
    else {
        ele.style.display = "block";
        text.innerHTML = "hide";
    }
}
function HardwareOTPTokenUpload() {
    
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var s = './hwotptokeuploadsubmit';
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#uploadXMLFormEAD").serialize(),
        
        success: function(data) {
            pleaseWaitDiv.modal('hide');
            if (strcmpTokens(data._result, "error") == 0) {
//                $('#save-otp-tokens').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpTokens(data._result, "success") == 0) {
//                $('#save-otp-tokens').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");

            }
            if (strcmpTokens(data._failed, "0") != 0 || strcmpTokens(data._success, "0") != 0) {
                document.getElementById("result-div").style.visibility = "visible";
                if (strcmpTokens(data._failed, "0") != 0)
                {
                    Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
                    document.getElementById("download-FailEntries").style.visibility = "visible";
//                document.getElementById("download-SuccessEntries").style.visibility = "visible";
                    //document.getElementById("download-FailEntries").style.background = "#0077b3";
                }
                if (strcmpTokens(data._success, "0") != 0)
                {
                    document.getElementById("download-SuccessEntries").style.visibility = "visible";
                    //document.getElementById("download-SuccessEntries").style.background = "#0077b3";
                }
                else if (strcmpTokens(data._failed, "0") != 0 && strcmpTokens(data._success, "0") != 0)
                {
                    document.getElementById("download-FailEntries").style.visibility = "visible";
                    document.getElementById("download-SuccessEntries").style.visibility = "visible";
                    //document.getElementById("download-FailEntries").style.background = "#0077b3";
                    //document.getElementById("download-SuccessEntries").style.background = "#0077b3";
                }
//            pleaseWaitDiv.modal('hide');
            }
        },
        error: function(data, status, e)
        {
            alert(e);
        }
    });
}

//function HardwareOTPTokenUpload() {
//
//    $('#buttonUploadEAD').attr("disabled", true);
//
//    var s = './hwotptokeuploadsubmit?_password=' + document.getElementById('_xmlpassowrd').value;
//    $.ajaxFileUpload({
//        type: 'POST',
//        fileElementId: 'fileXMLToUploadEAD',
//        url: s,
//        dataType: 'json',
//        success: function(data, status) {
//
//            if (strcmpTokens(data._result, "error") == 0) {
//                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
//
//
//            }
//            else if (strcmpTokens(data._result, "success") == 0) {
//                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
//
//
//            }
//
//            if (strcmpTokens(data._failed, "0") != 0)
//            {
//                document.getElementById("download-FailEntries").style.visibility = "visible";
////                document.getElementById("download-SuccessEntries").style.visibility = "visible";
//                //document.getElementById("download-FailEntries").style.background = "#0077b3";
//            }
//            if (strcmpTokens(data._success, "0") != 0)
//            {
//                document.getElementById("download-SuccessEntries").style.visibility = "visible";
//                //document.getElementById("download-SuccessEntries").style.background = "#0077b3";
//            }
//            else if (strcmpTokens(data._failed, "0") != 0 && strcmpTokens(data._success, "0") != 0)
//            {
//                document.getElementById("download-FailEntries").style.visibility = "visible";
//                document.getElementById("download-SuccessEntries").style.visibility = "visible";
//                //document.getElementById("download-FailEntries").style.background = "#0077b3";
//                //document.getElementById("download-SuccessEntries").style.background = "#0077b3";
//            }
//
//            //$('#download-FailEntries').html("<a href=./failedtoaddtokenentries?_Format=" + data._iformat + ">Download Fail Entry File</a>");
//            //$('#download-SuccessEntries').html("<a href=./Successaddtokenentries?_Format=" + data._iformat + ">Download Success Entry File</a>");
//        },
//        error: function(data, status, e)
//        {
//            alert(e);
//        }
//    });
//}

function SuccessEntriesDownload() {
    //var val = document.getElementById('_changeCategory').value;
    //var val1 = document.getElementById('_changeOTPStatus').value;
//    alert("SuccessEntriesDownload");
    var s = './Successaddtokenentries';//?_Format=' + _format ;//+ "&_changeOTPStatus=" + val1 + "&_reporttype=" + 0;
    window.location.href = s;
    return false;
}

function FailedEntriesDownload() {
    //var val = document.getElementById('_changeCategory').value;
    //var val1 = document.getElementById('_changeOTPStatus').value;
//    alert("FailedEntriesDownload");
    var s = './failedtoaddtokenentries';//?_Format=' + _format ;//+ "&_changeOTPStatus=" + val1 + "&_reporttype=" + 0;
    window.location.href = s;
    return false;
}

function HardwarePKITokenUpload() {
    var val = document.getElementById('_UnitIdPKI').value;
    //alert("ghfdgshfdgf"+val);
    $('#buttonUploadPKITOKENEAD').attr("disabled", true);

    var s = './pkitokensfileuload?_UnitIdPKI=' + encodeURIComponent(val);
    $.ajaxFileUpload({
        type: 'POST',
        fileElementId: 'PKITOKENfileUploadEAD',
        url: s,
        dataType: 'json',
        success: function(data, status) {

            if (strcmpTokens(data._result, "error") == 0) {
                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");


            }
            else if (strcmpTokens(data._result, "success") == 0) {
                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");


            }

            if (strcmpTokens(data._failed, "0") != 0)
            {
                document.getElementById("download-FailEntries").style.visibility = "visible";
//                document.getElementById("download-SuccessEntries").style.visibility = "visible";
                //document.getElementById("download-FailEntries").style.background = "#0077b3";
            }
            if (strcmpTokens(data._success, "0") != 0)
            {
                document.getElementById("download-SuccessEntries").style.visibility = "visible";
                //document.getElementById("download-SuccessEntries").style.background = "#0077b3";
            }
            else if (strcmpTokens(data._failed, "0") != 0 && strcmpTokens(data._success, "0") != 0)
            {
                document.getElementById("download-FailEntries").style.visibility = "visible";
                document.getElementById("download-SuccessEntries").style.visibility = "visible";
                //document.getElementById("download-FailEntries").style.background = "#0077b3";
                //document.getElementById("download-SuccessEntries").style.background = "#0077b3";
            }

            //$('#download-FailEntries').html("<a href=./failedtoaddtokenentries?_Format=" + data._iformat + ">Download Fail Entry File</a>");
            //$('#download-SuccessEntries').html("<a href=./Successaddtokenentries?_Format=" + data._iformat + ">Download Success Entry File</a>");
        },
        error: function(data, status, e)
        {
          //  alert("Error:::"+e);
        }
    });
}

function LoadTestOTPUI(_uid, type) {
    $('#_userIDOV').val(_uid);
    $('#_usertypeIDS').val(type);
    $("#verifyOTP").modal();
}

function LoadTestOTPForReplace(_uid, category, subcategory) {
    $('#_userIDReplaceHW').val(_uid);
    $('#_categoryReplaceHW').val(category);
    $('#_subcategoryReplaceHW').val(subcategory);
    $("#ReplaceHardware").modal();
}

function verifyOTP() {
    var s = './verifyOTP';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#verifyOTPForm").serialize(),
        success: function(data) {
            if (strcmpTokens(data._result, "error") == 0) {
                Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
                $('#_oobotp').val("");
            }
            else if (strcmpTokens(data._result, "success") == 0) {
                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                $('#_oobotp').val("");
            }
        }
    });
}

function UploadXMLFile() {
    $('#save-otp-tokens').attr("disabled", false);
    $('#buttonUploadXMLEAD').attr("disabled", true);
    var s = './uploadOTPXmlFile?_type=' + 1;
    $.ajaxFileUpload({
        fileElementId: 'fileXMLToUploadEAD',
        url: s,
        dataType: 'json',
        success: function(data, status) {

            if (strcmpTokens(data.result, "error") == 0) {
                Alert4Tokens("<span><font color=red>" + data.message + "</font></span>");
            }
            else if (strcmpTokens(data.result, "success") == 0) {
                Alert4Tokens("<span><font color=blue>" + data.message + "</font></span>");
                $('#buttonUploadPASSEAD').attr("disabled", false);
            }
        },
        error: function(data, status, e)
        {
            alert(e);
        }
    });
}


function UploadPasswordFile() {
    $('#buttonUploadPASSEAD').attr("disabled", true);
    var s = './uploadOTPXmlFile?_type=' + 2;
    $.ajaxFileUpload({
        fileElementId: 'fileXMLToUploadEADPASSWORD',
        url: s,
        dataType: 'json',
        success: function(data, status) {

            if (strcmpTokens(data.result, "error") == 0) {
                Alert4Tokens("<span><font color=red>" + data.message + "</font></span>");

            }
            else if (strcmpTokens(data.result, "success") == 0) {
                Alert4Tokens("<span><font color=blue>" + data.message + "</font></span>");
                $('#save-otp-tokens').attr("disabled", false);
            }
        },
        error: function(data, status, e)
        {
            alert(e);
        }
    });
}

function InitialState() {
    $('#buttonUploadPASSEAD').attr("disabled", true);
    $('#save-otp-tokens').attr("disabled", true);
}

function CheckSecretHWToken() {

    $('#checkSecretButton').attr("disabled", true);
    var s = './checkTokenSecrete';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#HWTOKENSECRETVALIDITY").serialize(),
        success: function(data) {
            if (strcmpTokens(data._result, "error") == 0) {
                $('#check-secret-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#checkSecretButton').attr("disabled", false);
            }
            else if (strcmpTokens(data._result, "success") == 0) {
//                alert("hi");
                $('#check-secret-result').html("<span><font color=blue>" + data._message + "</font></span>");
                $('#checkSecretButton').attr("disabled", true);
//                $('#_serialno').val("");
//                $("#CheckSecret").modal('hide');  
            }
        }
    });
}





function RefreshTokenReport() {
    //window.location.href = "./otptokensreport.jsp";
    $('#licenses_data_table').html("");
}
function RefreshTokenUsageReport() {
//    alert('s0');
    window.location.href = "./tokenFailureReport.jsp";
}


$(document).ready(function() {
    $('#_changeCategory').change(function() {
        var selectedValue = $(this).val();
//        alert(selectedValue);
//        var servletUrl = 'pushmapperselect?value=' + selectedValue;
//        alert(selectedValue);
        if (selectedValue == 2) {
          //  var ele = document.getElementById("_UnitId");
            //ele.style.display = "block";
        }
        if (selectedValue == 0) {
        }
    });
});


function allowAlertT(value, _div, disp) {
    $('#_allowAlertT').val(value);
    $(_div).html(disp);

}

function tokenExpiryAfterSession(value, _div, disp) {
    $('#_otpExpiryAfterSession').val(value);
    $(_div).html(disp);

}
function tokenExpiryAfterXDays(value, _div, disp) {
    $('#_otpLockedAfter').val(value);
    $(_div).html(disp);

}
function tokenExpiryAfterSessionSW(value, _div, disp) {
    $('#_otpExpiryAfterSessionSW').val(value);
    $(_div).html(disp);

}
function tokenExpiryAfterXDaysSW(value, _div, disp) {
    $('#_otpLockedAfterSW').val(value);
    $(_div).html(disp);

}
function tokenExpiryAfterSessionHW(value, _div, disp) {
    $('#_otpExpiryAfterSessionHW').val(value);
    $(_div).html(disp);

}
function tokenExpiryAfterXDaysHW(value, _div, disp) {
    $('#_otpLockedAfterHW').val(value);
    $(_div).html(disp);

}

function pukCodeLengthHW(value, _div, disp) {
//    alert(value);
    $('#_pukCodeLengthHW').val(value);
    $(_div).html(disp);

}

function pukCodeValidityHW(value, _div, disp) {
    $('#_pukCodeValidityHW').val(value);
    $(_div).html(disp);

}
function setuashingalog(value, _div, disp) {
    $('#_hashingalgo').val(value);
    $(_div).html(disp);
}



//function allowAlertForT(value, _div, disp) {
//    $('#_allowAlertForT').val(value);
//    $(_div).html(disp);
//
//}
function AlertViaT(value, _div, disp) {

    $('#_gatewayType').val(value);
    $(_div).html(disp);

}

function forwarTemplateRequestforAlertT() {
    var val = document.getElementById('_templateNameT').value;
    var s = './getTemplateType.jsp?_tid=' + val;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#AddNewSchedulerForm").serialize(),
        success: function(data) {
            if (strcmpTokens(data._result, "1") == 0) {
//                alert("hidknc");
                var s = './TemplateMobileEdit.jsp?_greetingsType=1&_tid=' + val;
                window.open(s);
//                window.location.href = s;
//                return false;
            } else if (strcmpTokens(data._result, "2") == 0) {
                var s = './TemplateEmailEdit.jsp?_greetingsType=1&_tid=' + val;
                window.open(s);
//                window.location.href = s;
//                return false;
            }
            s
        }
    });
}

function loadPUKHW(userid, _pukcode, pukexpiry) {
    $("#_useridPUK").val(userid);
//    $("#_categoryPUK").val(_category);
//    $("#_subcategoryPUK").val(_subcategory);
    $("#_pukcode").val(_pukcode);
    $("#_pukExpiry").val(pukexpiry);
    $("#PUKCODE").modal();
}
//function loadPUK(pukcode) {
//    bootbox.confirm("<h2><font color=blue>PUK Code :" + pukcode+ "</font></h2>", function (result) {
//        if (result == false) {
//        } else {
//              
//        }
//    });
//}

// bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
//        if (result === false) {
//        } else {
//            //var s = './sendregistration?_userid=' + userid;
//            var s = './sendregistration?_userid=' + encodeURIComponent(userid) + "&_category=" + _category + "&_subcategory=" + _subcategory;
//
//            $.ajax({
//                type: 'GET',
//                url: s,
//                dataType: 'json',
//                success: function(data) {
//                    if (strcmpTokens(data._result, "success") == 0) {
//                        Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
//                    } else {
//                        Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
//                    }
//                }
//            });
//        }
//    });

function loadUserTokenAuditDetails(_userId, _userName) {
    $("#_auditUserID1").val(_userId);
    $("#_auditUserName1").val(_userName);
    $("#userTokenauditDownload").modal();
}

function searchUserTokenAudit() {
    var val1 = encodeURIComponent(document.getElementById('_auditStartDate1').value);
    var val2 = encodeURIComponent(document.getElementById('_auditEndDate1').value);
    var val4 = encodeURIComponent(document.getElementById('_auditUserID1').value);
    var val5 = document.getElementById('_auditUserName1').value;
//    alert(val1 +" "+ val2);

    if (document.getElementById('_auditStartDate1').value.length == 0 || document.getElementById('_auditEndDate1').value.length == 0) {
        Alert4Tokens("Date Range is not selected!!!");
        return;
    }
    var s = './userPasswordAudit.jsp?_startdate=' + val1 + "&_enddate=" + val2 + "&_auditUserID=" + val4 + "&_auditUserName=" + val5 + "&_itemType=OTPTOKENS";
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
//            $('#licenses_data_table').html(data);
            $('#auditTable').html(data);
            $("#userTokenauditDownload").modal('hide');

        }
    });
}

//function userReportCSV(_itemType) {
//    alert("h");
//    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
//    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
//    var val4 = encodeURIComponent(document.getElementById('_auditUserID').value);
//    var s = './getuseraudits?_startdate=' + val1 + "&_enddate=" + val2 + "&_auditUserID=" + val4 + "&_itemType=" + _itemType + "&_format=1";
//    window.location.href = s;
//    return false;
//}
//
//function userReportPDF(_itemType) {
//    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
//    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
//    var val4 = encodeURIComponent(document.getElementById('_auditUserID').value);
//    var s = './getuseraudits?_startdate=' + val1 + "&_enddate=" + val2 + "&_auditUserID=" + val4 + "&_itemType=" + _itemType + "&_format=0";
//    window.location.href = s;
//    return false;
//}
//
//function userReportTXT(_itemType) {
//    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
//    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
//    var val4 = encodeURIComponent(document.getElementById('_auditUserID').value);
//    var s = './getuseraudits?_startdate=' + val1 + "&_enddate=" + val2 + "&_auditUserID=" + val4 + "&_itemType=" + _itemType + "&_format=2";
//    window.location.href = s;
//    return false;
//}

function searchTokenReport() {
    var val1 = encodeURIComponent(document.getElementById('startdate').value);
    var val2 = encodeURIComponent(document.getElementById('enddate').value);
    var val3 = document.getElementById('_changeType').value;
    var val4 = document.getElementById('_changeCategory').value;
    var s = './tokenFailureReportTable.jsp?_startdate=' + val1 + "&_enddate=" + val2 + "&_type=" + val3 + "&_changeCategory=" + val4;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#licenses_data_table').html(data);
        }});

}

function tokenFailureReportCSV(val) {
    var val1 = encodeURIComponent(document.getElementById('startdate').value);
    var val2 = encodeURIComponent(document.getElementById('enddate').value);
    var s = './tokenfailurereport?_startdate=' + val1 + "&_enddate=" + val2 + "&_type=" + val + "&_reporttype=1";
    window.location.href = s;
    return false;
}

function tokenFailureReportPDF(val) {
//    alert(val);
    var val1 = encodeURIComponent(document.getElementById('startdate').value);
    var val2 = encodeURIComponent(document.getElementById('enddate').value);
    var s = './tokenfailurereport?_startdate=' + val1 + "&_enddate=" + val2 + "&_type=" + val + "&_reporttype=0";
    window.location.href = s;
    return false;
}

function tokenFailureReportTXT(val) {
    var val1 = encodeURIComponent(document.getElementById('startdate').value);
    var val2 = encodeURIComponent(document.getElementById('enddate').value);
    var s = './tokenfailurereport?_startdate=' + val1 + "&_enddate=" + val2 + "&_type=" + val + "&_reporttype=2";
    window.location.href = s;
    return false;
}

function InvalidRequestOTPToken(id) {
    var s = './reportResponse?_requestID=' + id;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpTokens(data._result, "error") == 0) {
                Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpTokens(data._result, "success") == 0) {
                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}





function downloadCAReport(type, startdate, enddate) {
    var s = './DownloadCAAudit?_reporttype=' + type + "&_startDate=" + startdate + "&_endDate=" + enddate ;
    window.location.href = s;
    return false;
}

function DonutChartCA(val3, val4) {
    var s = './CAdonutchart?_startDate=' + val3 + "&_endDate=" + val4 +"&_itemType=CHALLENGERESPONSE";
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}
function BarChartCA(val3, val4) {
    var s = './CAbarchart?_startDate=' + val3 + "&_endDate=" + val4 +"&_itemType=CHALLENGERESPONSE"; 
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}


function generateOtpTablev2() {
    var val = document.getElementById('_changeCategory').value;
    var val1 = document.getElementById('_changeOTPStatus').value;
    var val2 = document.getElementById('_UnitId').value;
    
    var otpstartdate = document.getElementById('otpstartdate').value;
    var otpenddate = document.getElementById('otpenddate').value;
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
   // $('#licenses_data_table').html("<h3>Loading....</h3>");
    pleaseWaitDiv.modal();
    var s = './otptokensreporttable.jsp?_changeCategory=' + val + "&_changeOTPStatus=" + val1 + "&_UnitId=" + val2 + "&_otpstartdate=" +otpstartdate +"&_otpenddate=" +otpenddate;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#licenses_data_table').html(data);
        
            var day_data1 = null;
            day_data1 = BarChartDemo(val, val1, val2,otpstartdate,otpenddate);
            Morris.Bar({
                element: 'otpReportgraph1',
                data: day_data1,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {
                        return '#0066CC';
                    }
                    else {
                        return '#0066CC';
                    }
                }
            });
            
            var day_data = null;
            day_data = DonutChart(val, val1, val2,otpstartdate,otpenddate);
               Morris.Donut({
                element: 'otpReportgraph',
                data: day_data
            });
        
            pleaseWaitDiv.modal('hide');
        }
        
        
    });
}

function sendregistrationv2(userid, category, subcategory, _type) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            //var s = './sendregistration?_userid=' + userid;
            var s = './sendregistration?_userid=' + encodeURIComponent(userid) + "&_category=" + category + "&_subcategory=" + subcategory + "&_type=" + _type;

            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpTokens(data._result, "success") === 0) {
                        Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                    } else {
                        Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}



