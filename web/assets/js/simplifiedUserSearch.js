/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function loadotpstatusSimplified(status, userid, _category, _subcategory, uidiv) {
    $('#_userstatus').val(status);
    $('#_userID').val(userid);
    $('#_usercategory').val(_category);
    $('#_usersubcategory').val(_subcategory);
    $('#uidiv').val(uidiv);
    $("#suspendTokenSimplified").modal('show');
}

// new function for search user
function searchSimplifiedUserReport() {

    var valK = document.getElementById('_searchtext').value;


//    if (valK.length <= 3) {
//
//        Alert4Users("<h2><font color=red>Keyword should be meaningful and/or more than 3 characters</font></h2>");
//        return;
//    }

    var val = document.getElementById('_searchtext').value;
    var val1 = document.getElementById('_changeStatus').value;

    var s = './userDataTabs.jsp?_searchtext=' + val + "&_status=" + val1;

    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#users_data_tabs').html(data);
            //$("#UserReportgraph").empty();
            //$("#UserReportgraph1").empty();

        }
    });
}

function LoadTestOTPUISimplified(_uid, type) {
    $('#_userIDOV').val(_uid);
    $('#_usertypeIDS').val(type);
    $("#verifyOTPSimplified").modal('show');
}

function loadresyncHWSimplified(userid, _srno, _category) {
    $("#_userIDHW").val(userid);
    $("#_srnoHW").val(_srno);
    $("#_categoryHW").val(_category);
    $("#ResyncHardwareSimplified").modal();
}

function LoadTestOTPForReplaceSimplified(_uid, category, subcategory) {
    $('#_userIDReplaceHW').val(_uid);
    $('#_categoryReplaceHW').val(category);
    $('#_subcategoryReplaceHW').val(subcategory);
    $("#ReplaceHardwareSimplified").modal();
}

function loadPUKHWSimplified(userid,_pukcode, pukexpiry) {
    $("#_useridPUK").val(userid);
//    $("#_categoryPUK").val(_category);
//    $("#_subcategoryPUK").val(_subcategory);
    $("#_pukcode").val(_pukcode);
    $("#_pukExpiry").val(pukexpiry);
    $("#PUKCODESimplified").modal();
}


function uploadCustomerDetailsSimplified(_uid) {
//    alert(_uid);
     $('#_usersid').val(_uid);
     $("#kycuploadSimplified").modal();
}

function ChangeReportUserSimplified(value) {
    //1 for enabled
    //0 for disabled
    if (value === 1) {
        $('#_otpauditUserType').val("1");
        $('#_userreportType-div').html("CSV");
    } else if (value === 0) {
        $('#_otpauditUserType').val("0");
        $('#_userreportType-div').html("PDF");
    } else if (value === 2) {
        $('#_otpauditUserType').val("2");
        $('#_userreportType-div').html("TEXT");
    }
  }
function Alert4UserSimplified(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function (result) {
        if (result == false) {
        } else {
            //end here
        }
    });
} 
function downloadUserTokenAudit() {
    var val1 = encodeURIComponent(document.getElementById('startdateuser').value);
    var val2 = encodeURIComponent(document.getElementById('enddateser').value);
    var val4 = encodeURIComponent(document.getElementById('_otpauditUserID1').value);
    var val6 = encodeURIComponent(document.getElementById('_otpauditUserType').value);
    var val5 = document.getElementById('_otpauditUserName1').value;
    //alert(val6 +" "+ val6);

    if (document.getElementById('startdateuser').value.length == 0 || document.getElementById('enddateser').value.length == 0) {
        Alert4UserSimplified("Date Range is not selected!!!");
        return;
    }
    if (document.getElementById('_otpauditUserType').value.length == 0) {
        Alert4UserSimplified("Please select file format to download!!!");
        return;
    }      

    var s = './getuseraudits?_startdate=' + val1 + "&_enddate=" + val2 + "&_auditUserID=" + val4 + "&_auditUserName=" + val5 + "&_itemType=OTPTOKENS" +"&_format="+ val6;
    window.location.href = s;
    return false;
}

function downloadUserCertAudit() {
    var val1 = encodeURIComponent(document.getElementById('certstartdate').value);
    var val2 = encodeURIComponent(document.getElementById('certenddate').value);
    var val4 = encodeURIComponent(document.getElementById('_otpauditUserID2').value);
    var val6 = encodeURIComponent(document.getElementById('_certreportType').value);
    var val5 = document.getElementById('_otpauditUserName2').value;
    //alert(val6 +" "+ val6);

    if (document.getElementById('certstartdate').value.length == 0 || document.getElementById('certenddate').value.length == 0) {
        Alert4UserSimplified("Date Range is not selected!!!");
        return;
    }
    if (document.getElementById('_certreportType').value.length == 0) {
        Alert4UserSimplified("Please select file format to download!!!");
        return;
    }  
    var s = './getuseraudits?_startdate=' + val1 + "&_enddate=" + val2 + "&_auditUserID=" + val4 + "&_auditUserName=" + val5 + "&_itemType=PKITOKEN" +"&_format="+ val6;
    window.location.href = s;
    return false;
}

function ChangeReportUserSimplifiedCert(value) {
    //1 for enabled
    //0 for disabled
    if (value === 1) {
        $('#_certreportType').val("1");
        $('#_certreportType-div').html("CSV");
    } else if (value === 0) {
        $('#_certreportType').val("0");
        $('#_certreportType-div').html("PDF");
    }else if (value === 2) {
        $('#_certreportType').val("2");
        $('#_certreportType-div').html("TEXT");
    }
  }