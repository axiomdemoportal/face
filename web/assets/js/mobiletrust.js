function strcmpTrust(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function Alert4TrustSetting(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function (result) {
        if (result == false) {
        } else {
        }
    });
}
function SetBackupSetting(value, _div, disp) {
    if (value == 1) {
        $('#_Backup').val(value);
        $(_div).html(disp);
    } else if (value == 2) {
        $('#_Backup').val(value);
        $(_div).html(disp);
    }
}

function SetHashAlgo(value, _div, disp) {
        $('#_hashalgo').val(value);
        $(_div).html(disp);
   
}



function SetExpiryMin(value, _div, disp) {

//    if (value === 1) {
//        $('#_ExpiryMin').val(value);
//        $(_div).html(disp);
//    }
//    else if (value === 2) {
//        $('#_ExpiryMin').val(value);
//        $(_div).html(disp);
//    } else if (value === 3) {
//        $('#_ExpiryMin').val(value);
//        $(_div).html(disp);
//    }

    $('#_ExpiryMin').val(value);
    $(_div).html(disp);
}
function SetSilentSetting(value, _div, disp) {

    if (value == 1) {
        $('#_SilentCall').val(value);
        $(_div).html(disp);
    } else if (value == 2) {
        $('#_SilentCall').val(value);
        $(_div).html(disp);
    }
}


function SetSelfDestructAlertSetting(value, _div, disp) {

    if (value == 1) {
        $('#_SelfDestructEnable').val(value);
        $(_div).html(disp);
    } else if (value == 2) {
        $('#_SelfDestructEnable').val(value);
        $(_div).html(disp);
    }
}

function SelfDestructAttempts(value, _div, disp) {

//    alert(value);
    if (value === 3) {
        $('#_SelfDestructAttempts').val(value);
        $(_div).html(disp);
    } else if (value === 5) {
        $('#_SelfDestructAttempts').val(value);
        $(_div).html(disp);
    } else if (value === 7) {
        $('#_SelfDestructAttempts').val(value);
        $(_div).html(disp);
    }
}

function SetCountrySetting(div, disp) {
//    alert(disp);
    $('#_CountryName1').val(disp);
    $(div).html(disp);

}


function LoadTrustSettings() {
    var s = './loadmobiletrustsetting';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            $('#_CountryName').val(data._CountryName);
            $('#_ExpiryMin').val(data._ExpiryMin);
            $('#_Backup').val(data._Backup);
            $('#_SilentCall').val(data._SilentCall);
            $('#_SelfDestructEnable').val(data._SelfDestructEnable);
            $('#_SelfDestructAttempts').val(data._SelfDestructAttempts);
            $('#_hashalgo').val(data._hashalgo);
          
                SetHashAlgo(data._hashalgo, '#_hashalogdiv', data._hashalgo);
           
            $('#_selfDestructURl').val(data._selfDestructURl);
            $('#_LicenseKeyforAndroid').val(data._licenseforAndroid);
            $('#_LicenseKeyforIphone').val(data._licenseforIphone);
            $('#_LicenseKeyforIphone').val(data._licenseforIphone);
            $('#_timeStamp').val(data._timeStamp);
            $('#_deviceTracking').val(data._deviceTracking);
            $('#_geoFencing').val(data._geoFencing);
            //for adding alloweddomains
            var allowedDomains1 = "";
            for (var i = 1; i < data._allowedDomainnames.length; i++) {

                allowedDomains1 = allowedDomains1 + ',' + data._allowedDomainnames[i];

            }

            //  $('#_arrallowedDomainNames').val("Pramod0777");
            //end of Domains
            //  var allowedS=allowedDomains1.split(',')[1];
            //   alert(allowedS);
            $('#_arrallowedDomainNames').text(allowedDomains1);
            $("#_arrallowedDomainNames").select2({
                tags: [],
                dropdownCss: {display: 'none'},
                tokenSeparators: [","]
            });
            $('#_LicenseKeyforWeb').val(data._licenseforWEB);
//              alert(data._selfDestructURlM);
            $('#_selfDestructURlM').val(data._selfDestructURlM);
//             $('#_CountryName').val(data._CountryName);
            //alert(data._CountryName);
            if (data._ExpiryMin == 1)
                SetExpiryMin(1, '#_ExpiryMin_div', '1 minute');
            else if (data._ExpiryMin == 2)
                SetExpiryMin(2, '#_ExpiryMin_div', '2 minutes');
            else if (data._ExpiryMin == 3)
                SetExpiryMin(3, '#_ExpiryMin_div', '3 minutes');
            else if (data._ExpiryMin == 5)
                SetExpiryMin(5, '#_ExpiryMin_div', '5 minutes');
            else if (data._ExpiryMin == 7)
                SetExpiryMin(7, '#_ExpiryMin_div', '7 minutes');
            else if (data._ExpiryMin == 10)
                SetExpiryMin(10, '#_ExpiryMin_div', '10 minutes');

            if (data._Backup == true)
                SetBackupSetting(1, '#_backUp_div', 'Yes');
            else if (data._Backup == false)
                SetBackupSetting(2, '#_backUp_div', 'No');

            if (data._SilentCall == true)
                SetSilentSetting(1, '#_silentCall_div', 'Yes');
            else if (data._SilentCall == false)
                SetSilentSetting(2, '#_silentCall_div', 'No');

            if (data._SelfDestructEnable == true)
                SetSelfDestructAlertSetting(1, '#_selfDestructAlert_div', 'Yes');
            else if (data._SelfDestructEnable == false)
                SetSelfDestructAlertSetting(2, '#_selfDestructAlert_div', 'No');



            if (data._SelfDestructAttempts == 3)
                SelfDestructAttempts(3, '#_selfDestructAttempts_div', '3 Attempts');
            else if (data._SelfDestructAttempts == 5)
                SelfDestructAttempts(5, '#_selfDestructAttempts_div', '5 Attempts')
            else if (data._SelfDestructAttempts == 7)
                SelfDestructAttempts(7, '#_selfDestructAttempts_div', '7 Attempts');

            if (data._timeStamp == true)
                Timestamp(1, '#_timeStamp_div', 'Enable');
            else if (data._timeStamp == false)
                Timestamp(2, '#_timeStamp_div', 'Disable');

            if (data._deviceTracking == true)
                DeviceTracking(1, '#_deviceTracking_div', 'Enable');
            else if (data._deviceTracking == false)
                DeviceTracking(2, '#_deviceTracking_div', 'Disable');

            if (data._geoFencing == true)
                GeoFencing(1, '#_geoFencing_div', 'Enable');
            else if (data._geoFencing == false)
                GeoFencing(2, '#_geoFencing_div', 'Disable');

            //if(data._CountryName != null)
            //  SetCountrySetting(div ,data._CountryName);

            if (data._allowAlertM == true)
                allowAlertM(1, '#_allowAlert_div', 'Enabled');
            else if (data._allowAlertM == false)
                allowAlertM(2, '#_allowAlert_div', 'Disabled');

            if (data._allowAlertForM == 1)
                allowAlertForM(1, '#_allowAlertFor_div', 'Operators');
            else if (data._allowAlertFor == 2)
                allowAlertForM(2, '#_allowAlertFor_div', 'Users');
            else if (data._allowAlertFor == 0)
                allowAlertForM(2, '#_allowAlertFor_div', 'Both');
//             alert(data._gatewayType);

            if (data._gatewayTypeM == 1)
                AlertViaM(1, '#_gatewayType_div', 'SMS');
            else if (data._gatewayTypeM == 2)
                AlertViaM(2, '#_gatewayType_div', 'USSD');
            else if (data._gatewayTypeM == 3)
                AlertViaM(3, '#_gatewayType_div', 'VOICE');
            else if (data._gatewayTypeM == 4)
                AlertViaM(4, '#_gatewayType_div', 'EMAIL');
            else if (data._gatewayTypeM == 5)
                AlertViaM(5, '#_gatewayType_div', 'FAX');
            else if (data._gatewayTypeM == 18)
                AlertViaM(18, '#_gatewayType_div', 'ANDROIDPUSH');
            else if (data._gatewayTypeM == 19)
                AlertViaM(19, '#_gatewayType_div', 'IPHONEPUSH');


            if (data._alertAttemptM == 1)
                AlertAttemptsM(1, '#_alertAttempt_div', '1 Attempt');
            else if (data._alertAttemptM == 2)
                AlertAttemptsM(2, '#_alertAttempt_div', '2 Attempts');
            else if (data._alertAttemptM == 3)
                AlertAttemptsM(3, '#_alertAttempt_div', '3 Attempts');

            $("#_blackListedCountries").val(data._isBlackListed);
//            alert()
            if (data._isBlackListed == true)
                SetBlackListedCountriesSetting(1, '#_block_Countries', 'Yes');
            else if (data._isBlackListed == false)
                SetBlackListedCountriesSetting(2, '#_block_Countries', 'No');


        }
    });
}
function editTrustSettingsW() {
    var s = './editmobiletrustsetting?_type=' + 2;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#mobiletrustsettingsformW").serialize(),
        success: function (data) {
            if (strcmpTrust(data._result, "error") == 0) {
                $('#save-global-settings-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4TrustSetting("<span><font color=red>" + data._message + "</font></span>");
            } else if (strcmpTrust(data._result, "success") == 0) {
//                $('#save-global-settings-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4TrustSetting("<span><font color=blue>" + data._message + "</font></span>");
                //$('#mobile-trust-license').html("<span><font color=blue>" + data._license + "</font></span>");
                $('#_LicenseKeyforAndroid').val(data._licenseforAndroid);
                $('#_LicenseKeyforIphone').val(data._licenseforIphone);
                $('#_LicenseKeyforWeb').val(data._licenseWebTRUST);

            }
        }
    });
}
function editTrustSettingsM() {
    var s = './editmobiletrustsetting?_type=' + 1;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#mobiletrustsettingsformM").serialize(),
        success: function (data) {
            if (strcmpTrust(data._result, "error") == 0) {
                $('#save-global-settings-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4TrustSetting("<span><font color=red>" + data._message + "</font></span>");
            } else if (strcmpTrust(data._result, "success") == 0) {
//                $('#save-global-settings-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4TrustSetting("<span><font color=blue>" + data._message + "</font></span>");
                //$('#mobile-trust-license').html("<span><font color=blue>" + data._license + "</font></span>");
                $('#_LicenseKeyforAndroid').val(data._licenseforAndroid);
                $('#_LicenseKeyforIphone').val(data._licenseforIphone);
            }
        }
    });
}
function editTrustSettings() {
    var s = './editmobiletrustsetting?_type=' + 0;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#mobiletrustsettingsform").serialize(),
        success: function (data) {
            if (strcmpTrust(data._result, "error") == 0) {
                $('#save-global-settings-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4TrustSetting("<span><font color=red>" + data._message + "</font></span>");
            } else if (strcmpTrust(data._result, "success") == 0) {
//                $('#save-global-settings-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4TrustSetting("<span><font color=blue>" + data._message + "</font></span>");
                //$('#mobile-trust-license').html("<span><font color=blue>" + data._license + "</font></span>");
                $('#_LicenseKeyforAndroid').val(data._licenseforAndroid);
                $('#_LicenseKeyforIphone').val(data._licenseforIphone);
            }
        }
    });
}

function Timestamp(value, _div, disp) {

    if (value == 1) {
        $('#_timeStamp').val(value);
        $(_div).html(disp);
    } else if (value == 2) {
        $('#_timeStamp').val(value);
        $(_div).html(disp);
    }
}
function DeviceTracking(value, _div, disp) {

    if (value == 1) {
        $('#_deviceTracking').val(value);
        $(_div).html(disp);
    } else if (value == 2) {
        $('#_deviceTracking').val(value);
        $(_div).html(disp);
    }
}
function GeoFencing(value, _div, disp) {

    if (value == 1) {
        $('#_geoFencing').val(value);
        $(_div).html(disp);
    } else if (value == 2) {
        $('#_geoFencing').val(value);
        $(_div).html(disp);
    }
}


function allowAlertM(value, _div, disp) {
    $('#_allowAlertM').val(value);
    $(_div).html(disp);

}
function allowAlertForM(value, _div, disp) {
    $('#_allowAlertForM').val(value);
    $(_div).html(disp);

}
function AlertViaM(value, _div, disp) {

    $('#_gatewayTypeM').val(value);
    $(_div).html(disp);

}
function AlertAttemptsM(value, _div, disp) {

    $('#_alertAttemptM').val(value);
    $(_div).html(disp);

}

function SetBlackListedCountriesSetting(value, _div, disp) {

    if (value === 1) {
        $('#_isBlackListed').val(value);
        $(_div).html(disp);
    } else if (value === 2) {
        $('#_isBlackListed').val(value);
        $(_div).html(disp);
    }

}

function SetBlackListedCountries(value, _div, disp) {

    if (value === 1) {
        $('#_isBlackListed').val(value);
        $(_div).html(disp);
        $('#_CountryName').show();
    } else if (value === 2) {
        $('#_isBlackListed').val(value);
        $(_div).html(disp);
        $('#_CountryName').show();
    } else if (value === 3) {
        $('#_isBlackListed').val(value);
        $(_div).html(disp);
    } else if (value === 4) {
        $('#_isBlackListed').val(value);
        $(_div).html(disp);
    } else if (value === 5) {
        $('#_isBlackListed').val(value);
        $(_div).html(disp);
    }

}