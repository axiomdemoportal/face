function strcmpAuthorization(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function Alert4Authorization(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}

//function RefreshAuthorizationList() {
//    window.location.href = "./PendingListForApproval.jsp";
//}

function RefreshAuthorizationList() {
    window.location.href = "./searchPendingList.jsp";
}

//function removeAuthorization(approvalId, _unitId) {
//    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
//        if (result == false) {
//        } else {
//            var s = './removeAuthorizationRequest?_approvalID=' + encodeURIComponent(approvalId) + "&_unitId=" + _unitId;
//            $.ajax({
//                type: 'GET',
//                url: s,
//                dataType: 'json',
//                success: function(data) {
//                    if (strcmpAuthorization(data._result, "error") == 0) {
//                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
//                    }
//                    else if (strcmpAuthorization(data._result, "success") == 0) {
//                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
//                        window.setTimeout(RefreshAuthorizationList, 2000);
//
//                    }
//                }
//            });
//        }
//    });
//}
//;
function assigntokenHWAuthorization(userid, _category, _subcategory, _serialnumber, _markerID, _approvalId, _unitId) {
//    alert(_unitId);
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './assigntoken?_userid=' + encodeURIComponent(userid) + "&_category=" + _category
                    + "&_subcategory=" + _subcategory + "&_serialnumber=" + _serialnumber + "&_markerID=" + encodeURIComponent(_markerID) + "&_approvalId=" + _approvalId + "&_unitId=" + _unitId;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAuthorization(data._result, "error") == 0) {
                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpAuthorization(data._result, "success") == 0) {
                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshAuthorizationList, 2000);
                        //window.setTimeout(RefreshTokenList, 2000);
//                        var uiToChange = '#' + uidiv;
//                        $(uiToChange).html(data._value);
//                        var uToChange = '#' + udiv;
//                        $(uToChange).html(data._status);
                    }
                }
            });
        }
    });
}
;
function assigntokenOOBandSWAuthorization(userid, _category, _subcategory, _markerID, _approvalId, _unitId) {
//    alert(_markerID);
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './assigntoken?_userid=' + encodeURIComponent(userid) + "&_category=" + _category
                    + "&_subcategory=" + _subcategory + "&_markerID=" + encodeURIComponent(_markerID) + "&_approvalId=" + _approvalId + "&_unitId=" + _unitId;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAuthorization(data._result, "error") == 0) {
                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpAuthorization(data._result, "success") == 0) {
                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshAuthorizationList, 2000);
                    }
                }
            });
        }
    });
}
;
function changeotpstatusAuthorization(status, userid, _category, _subcategory, _markerID, _approvalId, _unitId) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        }
        else {
            var s = './changeotpstatus?_status=' + status + '&_userid=' + encodeURIComponent(userid) + "&_category="
                    + _category + "&_subcategory=" + _subcategory + "&_markerID=" + encodeURIComponent(_markerID) + "&_approvalId=" + _approvalId + "&_unitId=" + _unitId;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAuthorization(data._result, "error") == 0) {
                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpAuthorization(data._result, "success") == 0) {
                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshAuthorizationList, 2000);
                        //window.setTimeout(RefreshTokenList, 2000);
//                        var uiToChange = '#' + uidiv;
//                        $(uiToChange).html(data._value);
                    }
                }
            });
        }
    });
}
function changetokenOOBandSWAuthorization(userid, _category, _subcategory, _markerID, _approvalId, _unitId) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './changetokentype?_userid=' + encodeURIComponent(userid) + "&_category=" + _category
                    + "&_subcategory=" + _subcategory + "&_markerID=" + encodeURIComponent(_markerID) + "&_approvalId=" + _approvalId + "&_unitId=" + _unitId;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAuthorization(data._result, "error") == 0) {
                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpAuthorization(data._result, "success") == 0) {
                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshAuthorizationList, 2000);
                        //window.setTimeout(RefreshTokenList, 2000);
//                        var uiToChange = '#' + uidiv;
//                        $(uiToChange).html(data._value);
                    }
                }
            });
        }
    });
}

///User Management

function addUserAuthorization(_Name, _Email, _Phone, _markerID, _approvalId, _unitId) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './adduser?_Name=' + encodeURIComponent(_Name) + "&_Email=" + _Email
                    + "&_Phone=" + _Phone + "&_markerID=" + encodeURIComponent(_markerID) + "&_approvalId=" + _approvalId + "&_unitId=" + _unitId;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAuthorization(data._result, "error") == 0) {
                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpAuthorization(data._result, "success") == 0) {
                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshAuthorizationList, 2000);

                    }
                }
            });
        }
    });
}
function changeUserStatusAuthorization(_userid, _status, _markerID, _approvalId, _unitId) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './changeuserstatus?_userid=' + encodeURIComponent(_userid) + "&_status=" + _status + "&_markerID=" + encodeURIComponent(_markerID) + "&_approvalId=" + _approvalId + "&_unitId=" + _unitId;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAuthorization(data._result, "error") == 0) {
                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpAuthorization(data._result, "success") == 0) {
                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshAuthorizationList, 2000);

                    }
                }
            });
        }
    });
}

function editUserAuthorization(_userIdE, _NameE, _EmailE, _PhoneE, _markerID, _approvalId, _unitId) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './edituser?_userIdE=' + encodeURIComponent(_userIdE) + "&_NameE=" + _NameE + "&_EmailE=" + _EmailE
                    + "&_PhoneE=" + _PhoneE + "&_markerID=" + encodeURIComponent(_markerID) + "&_approvalId=" + _approvalId + "&_unitId=" + _unitId;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAuthorization(data._result, "error") == 0) {
                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpAuthorization(data._result, "success") == 0) {
                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshAuthorizationList, 2000);

                    }
                }
            });
        }
    });
}
function removeUserAuthorization(_userid, _markerID, _approvalId, _unitId) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './removeuser?_userid=' + encodeURIComponent(_userid) + "&_markerID=" + encodeURIComponent(_markerID) + "&_approvalId=" + _approvalId + "&_unitId=" + _unitId;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAuthorization(data._result, "error") == 0) {
                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpAuthorization(data._result, "success") == 0) {
                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshAuthorizationList, 2000);

                    }
                }
            });
        }
    });
}

function unlockUserPasswordAuthorization(_userid, _status, _markerID, _approvalId, _unitId) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './unlockuserpassword?_userid=' + encodeURIComponent(_userid) + "&_status=" + _status + "&_markerID=" + encodeURIComponent(_markerID) + "&_approvalId=" + _approvalId + "&_unitId=" + _unitId;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAuthorization(data._result, "error") == 0) {
                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpAuthorization(data._result, "success") == 0) {
                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshAuthorizationList, 2000);

                    }
                }
            });
        }
    });
}

function resendUserPasswordAuthorization(_userid, _markerID, _approvalId, _unitId) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './resenduserpassword?_userid=' + encodeURIComponent(_userid) + "&_markerID=" + encodeURIComponent(_markerID) + "&_approvalId=" + _approvalId + "&_unitId=" + _unitId;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAuthorization(data._result, "error") == 0) {
                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpAuthorization(data._result, "success") == 0) {
                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshAuthorizationList, 2000);

                    }
                }
            });
        }
    });
}

function setAndResendUserPasswordAuthorization(_userid, _markerID, _approvalId, _unitId) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './setandresenduserpassword?_userid=' + encodeURIComponent(_userid) + "&_markerID=" + encodeURIComponent(_markerID) + "&_approvalId=" + _approvalId + "&_unitId=" + _unitId;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAuthorization(data._result, "error") == 0) {
                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpAuthorization(data._result, "success") == 0) {
                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshAuthorizationList, 2000);

                    }
                }
            });
        }
    });
}

function resetUserPasswordAuthorization(_userid, _markerID, _approvalId, _unitId) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './resetuserpassword?_userid=' + encodeURIComponent(_userid) + "&_markerID=" + encodeURIComponent(_markerID) + "&_approvalId=" + _approvalId + "&_unitId=" + _unitId;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAuthorization(data._result, "error") == 0) {
                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpAuthorization(data._result, "success") == 0) {
                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshAuthorizationList, 2000);

                    }
                }
            });
        }
    });
}

/// Certificat and PKi tokens

function assignCertAuthorization(_userid, _markerID, _approvalId, _unitId) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './generatecertificate?_userid=' + encodeURIComponent(_userid) + "&_markerID=" + encodeURIComponent(_markerID) + "&_approvalId=" + _approvalId + "&_unitId=" + _unitId;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAuthorization(data._result, "error") == 0) {
                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpAuthorization(data._result, "success") == 0) {
                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshAuthorizationList, 2000);

                    }
                }
            });
        }
    });
}
function sendCertAuthorization(_userSENDCERT, _emailSENDCERT, _markerID, _approvalId, _unitId) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './sendcertfile?_userSENDCERT=' + encodeURIComponent(_userSENDCERT) + "&_emailSENDCERT=" + _emailSENDCERT + "&_markerID=" + encodeURIComponent(_markerID) + "&_approvalId=" + _approvalId
                    + "&_unitId=" + _unitId;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAuthorization(data._result, "error") == 0) {
                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpAuthorization(data._result, "success") == 0) {
                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshAuthorizationList, 2000);

                    }
                }
            });
        }
    });
}
function renewCertAuthorization(_userid, _markerID, _approvalId, _unitId) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './renewcertificate?_userid=' + encodeURIComponent(_userid) + "&_markerID=" + encodeURIComponent(_markerID) + "&_approvalId=" + _approvalId
                    + "&_unitId=" + _unitId;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAuthorization(data._result, "error") == 0) {
                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpAuthorization(data._result, "success") == 0) {
                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshAuthorizationList, 2000);

                    }
                }
            });
        }
    });
}

function revokeCertAuthorization(_userCR, _reason, _markerID, _approvalId, _unitId) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './revokecertificate?_userCR=' + encodeURIComponent(_userCR) + "&_reason=" + _reason + "&_markerID=" + encodeURIComponent(_markerID) + "&_approvalId=" + _approvalId
                    + "&_unitId=" + _unitId;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAuthorization(data._result, "error") == 0) {
                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpAuthorization(data._result, "success") == 0) {
                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshAuthorizationList, 2000);

                    }
                }
            });
        }
    });
}
function assignHWPKITokenAuthorization(_userid, _ser, _category, _markerID, _approvalId, _unitId) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './assignnewtoken?_userid=' + encodeURIComponent(_userid) + "&_ser=" + _ser + "&_category=" + _category + "&_markerID=" + encodeURIComponent(_markerID) + "&_approvalId=" + _approvalId
                    + "&_unitId=" + _unitId;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAuthorization(data._result, "error") == 0) {
                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpAuthorization(data._result, "success") == 0) {
                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshAuthorizationList, 2000);

                    }
                }
            });
        }
    });
}

function changeHWPKITokenAuthorization(_userid, _status, _category, _markerID, _approvalId, _unitId) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './changepkitokenstatus?_userid=' + encodeURIComponent(_userid) + "&_status=" + _status + "&_category=" + _category + "&_markerID=" + encodeURIComponent(_markerID) + "&_approvalId=" + _approvalId
                    + "&_unitId=" + _unitId;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAuthorization(data._result, "error") == 0) {
                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpAuthorization(data._result, "success") == 0) {
                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshAuthorizationList, 2000);

                    }
                }
            });
        }
    });
}
function sendPfxFileAuthorization(_userid, _category, _markerID, _approvalId, _unitId) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './sendpfxfile?_userid=' + encodeURIComponent(_userid) + "&_category=" + _category + "&_markerID=" + encodeURIComponent(_markerID) + "&_approvalId=" + _approvalId
                    + "&_unitId=" + _unitId;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAuthorization(data._result, "error") == 0) {
                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpAuthorization(data._result, "success") == 0) {
                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshAuthorizationList, 2000);

                    }
                }
            });
        }
    });
}
function sendPfxPasswordAuthorization(_userid, _markerID, _approvalId, _unitId) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './sendpfxpassword?_userid=' + encodeURIComponent(_userid) + "&_markerID=" + encodeURIComponent(_markerID) + "&_approvalId=" + _approvalId
                    + "&_unitId=" + _unitId;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAuthorization(data._result, "error") == 0) {
                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpAuthorization(data._result, "success") == 0) {
                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshAuthorizationList, 2000);

                    }
                }
            });
        }
    });
}
function sendPKITOKENREGCODEAuthorization(_userid, _category, _markerID, _approvalId, _unitId) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './sendpkitokenregistration?_userid=' + encodeURIComponent(_userid) + "&_category=" + _category + "&_markerID=" + encodeURIComponent(_markerID) + "&_approvalId=" + _approvalId
                    + "&_unitId=" + _unitId;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAuthorization(data._result, "error") == 0) {
                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpAuthorization(data._result, "success") == 0) {
                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshAuthorizationList, 2000);

                    }
                }
            });
        }
    });
}
function sendOTPTOKENREGCODEAuthorization(_userid, _category, _subcategory, _markerID, _approvalId, _unitId) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './sendregistration?_userid=' + encodeURIComponent(_userid) + "&_category=" + _category + "&_subcategory=" + _subcategory + "&_markerID=" + encodeURIComponent(_markerID) + "&_approvalId=" + _approvalId
                    + "&_unitId=" + _unitId;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAuthorization(data._result, "error") == 0) {
                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpAuthorization(data._result, "success") == 0) {
                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshAuthorizationList, 2000);

                    }
                }
            });
        }
    });
}

//Pramod
function ApproveRequest1(url,jsondata,approvalId) {
    var s = './' + url + '?_fromApprove=yes';
   // alert(document.getElementById("_userid"+approvalId).value);
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data:jsondata,
        success: function(data) {
            if (strcmpAuthorization(data._result, "error") == 0) {
                Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpAuthorization(data._result, "success") == 0) {
                RemveRequest(approvalId);
                Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");

            }
        }
    });
}

//nilesh 22-5-15
function ApproveRequest(formaname, url, approvalId,newvalue,oldvalue) {
    var s = './' + url + '?_fromApprove=yes';
   var jsonData={};
   if(document.getElementById(formaname).elements!==null){
    for(var i=0;i<document.getElementById(formaname).elements.length;i++)
    
    {  var valueOfElement=document.getElementById(formaname).elements[i].value;
        var valueOfID= document.getElementById(formaname).elements[i].id;
       var element= valueOfID.split("+");
       jsonData[element[0]]=valueOfElement;
        
    }}
//    jsonData["id"]=approvalId;
//    alert(JSON.stringify(jsonData));
 // var jsonData={"_userid":document.getElementById('_userid'+approvalId).value,"_category":document.getElementById('_category'+approvalId).value,"_subcategory":document.getElementById('_subcategory'+approvalId).value,"_serialnumber":document.getElementById('_serialnumber'+approvalId).value,"_status":document.getElementById('_status'+approvalId).value};
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: jsonData,
        success: function(data) {
            if (strcmpAuthorization(data._result, "error") == 0) {
                Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpAuthorization(data._result, "success") == 0) {
               
                RemveRequest(approvalId,newvalue,oldvalue);
                Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");

            }
        }
    });
}
function RemveRequest(_approvalId,newvalue,oldvalue) {
    var s = './removeAuthRequest?_approvalID=' + _approvalId + "&newvalue="+newvalue+"&oldvalue="+oldvalue;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            window.setTimeout(RefreshAuthorizationList, 2000);
        }
    });
}

function removeAuthorization(_approvalId, _unitId,_oldvalue,_newvalue){
    $('#_approvalID').val(_approvalId);
    $('#_unitId').val(_unitId);
    $('#_oldvalue').val(_oldvalue);
    $('#_newvalue').val(_newvalue);
    $("#requestReject").modal();
}
function removeAuthorizationReq() {
     
      bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function (result) {
        if (result == false) {
          
        } else {
            var s = './removeAuthorizationRequest';
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                data: $("#requestRejectform").serialize(),
                success: function (data) {
                    if (strcmpAuthorization(data._result, "error") == 0) {
                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
                    } else if (strcmpAuthorization(data._result, "success") == 0) {
                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
                        $("#requestReject").hide();
                        window.setTimeout(RefreshAuthorizationList, 2000);
                    }
                }
            });
        }
    });
}


//from old js
$(document).ready(function() {
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    $('#_searchTypeA').change(function() {
        var selectedValue = $(this).val();
        if (strcmpAuthorization(selectedValue, "0") == 0) {
//            var s = './searchRequestTable.jsp?_startAtTime=' + "val1" + '&_endAtTime=' + "val" + '&_unitIDs=' + "val2" + '&_searchType=' + 0;
            var s = './searchArchiveRequestTableAuth.jsp?_startAtTime=' + "val1" + '&_endAtTime=' + "val" + '&_unitIDs=' + "val2" + '&_searchType=' + 0;
            pleaseWaitDiv.modal();
            $.ajax({
                type: 'post',
                url: s,
                success: function(data) {
                    pleaseWaitDiv.modal('hide');
                    $('#licenses_data_table').html(data);
                }
            });
        } else {
            if (selectedValue == 1) {
                var ele = document.getElementById("_schedulerStatusA");
                ele.style.display = "block";
                document.getElementById("_schedulerStatusB").style.display = 'none';

            } else if (selectedValue == 2) {
                var ele = document.getElementById("_schedulerStatusB");
                ele.style.display = "block";
                document.getElementById("_schedulerStatusA").style.display = 'none';
            }
        }
    });
});
function generateArchiveOperatorsTable() {
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var val1 = encodeURIComponent(document.getElementById('startdateB').value);
    var val2 = encodeURIComponent(document.getElementById('enddateB').value);
    var val3 = encodeURIComponent(document.getElementById('_unitIDB').value);
    var val4 = encodeURIComponent(document.getElementById('_statusB').value);
//    alert(val3);
    var s = './searchArchiveRequestTableAuth.jsp?_startAtTime=' + val1 + '&_endAtTime=' + val2 + '&_unitIDs=' + val3 + '&_searchType=' + 2 + '&_statusA=' + val4;
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'post',
        url: s,
        success: function(data) {
//            alert(data);
            pleaseWaitDiv.modal('hide');
            $('#licenses_data_table').html(data);
        }
    });
}
$(document).ready(function() {
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    $('#_searchType').change(function() {
        var selectedValue = $(this).val();
        if (strcmpAuthorization(selectedValue, "0") == 0) {
            var s = './searchRequestTable.jsp?_startAtTime=' + "val1" + '&_endAtTime=' + "val" + '&_unitIDs=' + "val2" + '&_searchType=' + 0;
            pleaseWaitDiv.modal();
            $.ajax({
                type: 'post',
                url: s,
                success: function(data) {
                    pleaseWaitDiv.modal('hide');
                    $('#licenses_data_table').html(data);
                }
            });
        } else {
            if (selectedValue == 1) {
                var ele = document.getElementById("_schedulerStatus1");
                ele.style.display = "block";
                document.getElementById("_schedulerStatus2").style.display = 'none';

            } else if (selectedValue == 2) {
                var ele = document.getElementById("_schedulerStatus2");
                ele.style.display = "block";
                document.getElementById("_schedulerStatus1").style.display = 'none';
            }
        }
    });
});
$(document).ready(function() {
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    $('#_searchTypeR').change(function() {
        var selectedValue = $(this).val();
        if (strcmpAuthorization(selectedValue, "0") == 0) {
            var s = './searchArchiveRequestTableReq.jsp?_startAtTime=' + "val1" + '&_endAtTime=' + "val" + '&_unitIDs=' + "val2" + '&_searchType=' + 0;
            pleaseWaitDiv.modal();
            $.ajax({
                type: 'post',
                url: s,
                success: function(data) {
//                    alert(data);
                    pleaseWaitDiv.modal('hide');
                    $('#licenses_data_table').html(data);
                }
            });
        } else {
            if (selectedValue == 1) {
                var ele = document.getElementById("_schedulerStatusR");
                ele.style.display = "block";
//            alert(document.getElementById("_schedulerStatusRB"));
                document.getElementById("_schedulerStatusRB").style.display = 'none';

            } else if (selectedValue == 2) {
                var ele = document.getElementById("_schedulerStatusRB");
                ele.style.display = "block";
                document.getElementById("_schedulerStatusR").style.display = 'none';
            }
        }
    });
});

function authoriserReportCSV(val, val3, val4, val1, val2, val5) {
    var s = './downloadArchiveList?_startAtTime=' + val1 + "&_endAtTime=" + val2 + "&_unitIDs=" + encodeURIComponent(val) + "&_searchType=" + val3 + "&_statusA=" + val4 + "&_reporttype=" + 1 + "&_operatorType=" + val5;
    window.location.href = s;
    return false;
}

function authoriserReportPDF(val, val3, val4, val1, val2, val5) {
    var s = './downloadArchiveList?_startAtTime=' + val1 + "&_endAtTime=" + val2 + "&_unitIDs=" + encodeURIComponent(val) + "&_searchType=" + val3 + "&_statusA=" + val4 + "&_reporttype=" + 0 + "&_operatorType=" + val5;
    window.location.href = s;
    return false;
}
function authoriserReportTXT(val, val3, val4, val1, val2, val5) {
    var s = './downloadArchiveList?_startAtTime=' + val1 + "&_endAtTime=" + val2 + "&_unitIDs=" + encodeURIComponent(val) + "&_searchType=" + val3 + "&_statusA=" + val4 + "&_reporttype=" + 2 + "&_operatorType=" + val5;
    window.location.href = s;
    return false;
}

function generateArchiveUnitsTable() {
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var val1 = encodeURIComponent(document.getElementById('startdate').value);
    var val2 = encodeURIComponent(document.getElementById('enddate').value);
    var val3 = encodeURIComponent(document.getElementById('_unitIDA').value);
    var val4 = encodeURIComponent(document.getElementById('_statusA').value);

    var s = './searchArchiveRequestTableAuth.jsp?_startAtTime=' + val1 + '&_endAtTime=' + val2 + '&_unitIDs=' + val3 + '&_searchType=' + 1 + '&_statusA=' + val4;
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'post',
        url: s,
        success: function(data) {
            pleaseWaitDiv.modal('hide');
            $('#licenses_data_table').html(data);
        }
    });

}
function generateUnitsTable() {
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var val = encodeURIComponent(document.getElementById('starTimeUnit').value);
    var val1 = encodeURIComponent(document.getElementById('endTimeUnit').value);
    var val2 = encodeURIComponent(document.getElementById('_unitIDs').value);
    var val3 = encodeURIComponent(document.getElementById('_searchType').value);
    if (val3 === "-1") {
        alert("Please select Search By option?");
        return false;
    }
    var s = './searchRequestTable.jsp?_startAtTime=' + val1 + '&_endAtTime=' + val + '&_unitIDs=' + val2 + '&_searchType=' + val3;
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'post',
        url: s,
        success: function(data) {
            pleaseWaitDiv.modal('hide');
            $('#licenses_data_table').html(data);
        }
    });

}
function generateOperatorsTable() {
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    //var val = encodeURIComponent(document.getElementById('starTime').value);
    //var val1 = encodeURIComponent(document.getElementById('endTime').value);
    var val2 = encodeURIComponent(document.getElementById('_tagIDMC1').value);
    var val3 = encodeURIComponent(document.getElementById('_searchType').value);
//    alert(val3);
    //var s = './searchRequestTable.jsp?_startAtTime=' + val1 + '&_endAtTime=' + val + '&_unitIDs=' + val2 + '&_searchType=' + val3;
    var s = './searchRequestTable.jsp?' + '&_unitIDs=' + val2 + '&_searchType=' + val3;
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'post',
        url: s,
        success: function(data) {
//            alert(data);
            pleaseWaitDiv.modal('hide');
            $('#licenses_data_table').html(data);
        }
    });

}
function generateArchiveUnitsTableReq() {
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var val1 = encodeURIComponent(document.getElementById('startdate').value);
    var val2 = encodeURIComponent(document.getElementById('enddate').value);
    var val3 = encodeURIComponent(document.getElementById('_unitIDR').value);
    var val4 = encodeURIComponent(document.getElementById('_statusR').value);
    var s = './searchArchiveRequestTableReq.jsp?_startAtTime=' + val1 + '&_endAtTime=' + val2 + '&_unitIDs=' + val3 + '&_searchType=' + 1 + '&_statusA=' + val4;
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'post',
        url: s,
        success: function(data) {
            pleaseWaitDiv.modal('hide');
            $('#licenses_data_table').html(data);
        }
    });

}
function generateArchiveOperatorsTableReq() {
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var val1 = encodeURIComponent(document.getElementById('startdateB').value);
    var val2 = encodeURIComponent(document.getElementById('enddateB').value);
    var val3 = encodeURIComponent(document.getElementById('_unitIDRB').value);
    var val4 = encodeURIComponent(document.getElementById('_statusRB').value);
//    alert(val3);
    var s = './searchArchiveRequestTableReq.jsp?_startAtTime=' + val1 + '&_endAtTime=' + val2 + '&_unitIDs=' + val3 + '&_searchType=' + 2 + '&_statusA=' + val4;
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'post',
        url: s,
        success: function(data) {
//            alert(data);
            pleaseWaitDiv.modal('hide');
            $('#licenses_data_table').html(data);
        }
    });
}

function removeReq(val,oldvalue,newvalue) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './removeAuthorizationRequest?_approvalID=' + val + '&_oldvalue='+oldvalue+'&_newvalue='+newvalue;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                data: $("#requestRejectform").serialize(),
                success: function(data) {
                    if (strcmpAuthorization(data._result, "error") == 0) {
                        Alert4Authorization("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpAuthorization(data._result, "success") == 0) {
                        Alert4Authorization("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshAuthorizationList, 2000);
                    }
                }
            });
        }
    });
}
;