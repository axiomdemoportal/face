function Alert4Cert(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function (result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}
function strcmpCert(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function ChangeCertCategory(value) {
    if (value === 1) {
        $('#_changeCertCategory').val("1");
        $('#_change_Certificate_category_Axiom').html("Certificate");
    }
}

function ChangeCertStateType(value) {
    if (value === 1) {
        $('#_changeCertStatus').val("1");
        $('#_change_Certificate_Status_Axiom').html("Active");
    } else if (value === -10) {
        $('#_changeCertStatus').val("-10");
        $('#_change_Certificate_Status_Axiom').html("Expired");
    } else if (value === -5) {
        $('#_changeCertStatus').val("-5");
        $('#_change_Certificate_Status_Axiom').html("Revoked");
    } else if (value === -1) {
        $('#_changeCertStatus').val("-1");
        $('#_change_Certificate_Status_Axiom').html("Expiring soon");
    }
}



function CertificateReport() {
    var val3 = encodeURIComponent(document.getElementById('startdate').value);
    var val4 = encodeURIComponent(document.getElementById('enddate').value);
    var val1 = document.getElementById('_changeCertStatus').value;
    var s = './certreport?_changeCertStatus=' + val1 + "&_reporttype=" + 1 + '&_enddate=' + val4 + '&_startdate=' + val3+'&_reporttype='+1;
    window.location.href = s;
    return false;
}

function CertificateReportpdf() {
    var val3 = encodeURIComponent(document.getElementById('startdate').value);
    var val4 = encodeURIComponent(document.getElementById('enddate').value);
    var val1 = document.getElementById('_changeCertStatus').value;
    var s = './certreport?_changeCertStatus=' + val1 + "&_reporttype=" + 0 + '&_enddate=' + val4 + '&_startdate=' + val3+'&_reporttype='+0;
    window.location.href = s;
    return false;
}
function CertificateReportTXT() {
    var val3 = encodeURIComponent(document.getElementById('startdate').value);
    var val4 = encodeURIComponent(document.getElementById('enddate').value);
    var val1 = document.getElementById('_changeCertStatus').value;
    var s = './certreport?_changeCertStatus=' + val1 + "&_reporttype=" + 2 + '&_enddate=' + val4 + '&_startdate=' + val3+'&_reporttype='+2;
    window.location.href = s;
    return false;
}
function generatecert() {
    var s = './assigntokenandgeneratecert';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#generatecertificate").serialize(),
        success: function (data) {
            if (strcmpCert(data._result, "error") === 0) {
                $('#generate-certificate-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Cert("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpCert(data._result, "success") === 0) {
                $('#generate-certificate-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Cert("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

//function generateCertificateTable() {
//    var val = document.getElementById('_changeCertCategory').value;
//    var val1 = document.getElementById('_changeCertStatus').value;
//    var val3 = encodeURIComponent(document.getElementById('startdate').value);
//    var val4 = encodeURIComponent(document.getElementById('enddate').value);
//
////    if (document.getElementById('startdate').value.length == 0 || document.getElementById('enddate').value.length == 0) {
////         Alert4Cert("<span><font color=red>Date Range is not selected!!!</font></span>");
////        return;
////    }
//
////    if (document.getElementById('startdate').value > document.getElementById('enddate').value) {
////        Alert4Cert("<span><font color=red>Enter Proper Date Range!!!</font></span>");
////        return;
////    }
//    
//    var today = new Date();
//    var dd = today.getDate();
//    var mm = today.getMonth() + 1; //January is 0!
//    var yyyy = today.getFullYear();
//    var todaysDate = mm + '/' + dd + '/' + yyyy;
////    alert(document.getElementById('enddate').value);
////    alert(todaysDate);
//
////    if (todaysDate < document.getElementById('enddate').value) {
////        Alert4Cert("<span><font color=red>Enter Proper Date Range!!!</font></span>");
////        return;
////    }
//   // document.getElementById("REPORT").style.display='block';
//   document.getElementById("licenses_data_table_onLoad").style.display='none';//new change
//    var s = './certificatesreporttable.jsp?_changeCertStatus=' + val1 + '&_startdate=' + val3 + '&_enddate=' + val4;
//    $.ajax({
//        type: 'GET',
//        url: s,
//        success: function (data) {
//            $('#licenses_data_table').html(data);
//
//            var day_data = null;
//            day_data = certDonutChart(val1);
//            Morris.Donut({
//                element: 'certdonutchart',
//                data: day_data
//            });
//            //Bar Chart
//            var day_data1 = null;
//            day_data1 = certBarChart(val1);
//            Morris.Bar({
//                element: 'certbarchart',
//                data: day_data1,
//                xkey: 'label',
//                ykeys: ['value'],
//                labels: ['value'],
//                barColors: function (type) {
//                    if (type === 'bar') {
//
//                        return '#0066CC';
//                    }
//                    else {
//
//                        return '#0066CC';
//                    }
//                }
//            });
//
//        }
////    $('#certdonutchart').empty();
////    $('#certbarchart').empty();
//        //donut
//
//
//    });
//
//}


function generateCertificateTable() {
    var val = document.getElementById('_changeCertCategory').value;
    var val1 = document.getElementById('_changeCertStatus').value;
    var val3 = encodeURIComponent(document.getElementById('startdate').value);
    var val4 = encodeURIComponent(document.getElementById('enddate').value);

    if(val1 != -1){
    if (document.getElementById('startdate').value.length == 0 || document.getElementById('enddate').value.length == 0) {
         Alert4Cert("<span><font color=red>Date Range is not selected!!!</font></span>");
        return;
    }
var startDate = new Date(document.getElementById('startdate').value);
var endDate = new Date(document.getElementById('enddate').value);

    if (startDate > endDate) {
        Alert4Cert("<span><font color=red>Enter Proper Date Range!!!</font></span>");
        return;
    }
}
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    var todaysDate = mm + '/' + dd + '/' + yyyy;  
   
   document.getElementById("licenses_data_table_onLoad").style.display='none';//new change
   $('#licenses_data_table').html("<h3>Loading....</h3>");
//   var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
//   pleaseWaitDiv.modal();
    var s = './certificatesreporttable.jsp?_changeCertStatus=' + val1 + '&_startdate=' + val3 + '&_enddate=' + val4;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
           var myJsonObj1 = jsonParse(data);
            if(strcmpCert(myJsonObj1._result,"error"))
            {
                alert("You dont have access to execute this operation");
                return;
            }
            
            $('#licenses_data_table').html(data);

            var day_data = null;
            day_data = certDonutChart(val3,val4);
            Morris.Donut({
                element: 'certdonutchart',
                data: day_data
            });
            //Bar Chart
            var day_data1 = null;
            day_data1 = certBarChart(val3,val4);
            Morris.Bar({
                element: 'certbarchart',
                data: day_data1,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function (type) {
                    if (type === 'bar') {

                        return '#0066CC';
                    }
                    else {

                        return '#0066CC';
                    }
                }
            });
             //pleaseWaitDiv.modal('hide');
        }
    });

}





function certDonutChart() {
    var s = './certdonutchart';
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}
function Alert4Tokens(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}
function InvalidRequestCertificate(id) {
    var s = './reportResponse?_requestID=' + id;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpCert(data._result, "error") == 0) {
                Alert4Tokens("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpCert(data._result, "success") == 0) {
                Alert4Tokens("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}



function certBarChart(val3,val4) {
    var s = './certbarchart?_startdate=' + val3 + '&_enddate=' + val4;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}

function certDonutChart(val3,val4) {
    var s = './certdonutchart?_startdate=' + val3 + '&_enddate=' + val4;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}

