function strcmpEPIN(a, b)
{
    return (a<b?-1:(a>b?1:0));
}


function Alert4EPINSetting(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
        //end here
        }
    });
}


function ChangeDeliveryTypeEPIN(value,disp) {
    $('#_pinDeliveryType').val(value);
    $('#_pinDeliveryType_div').html(disp);    
}

function ChangeChannelEPIN(type,value,disp) {
    if ( type == 1) {
        $('#_channelType').val(value);
        $('#_channelType_div').html(disp);    
    } else if ( type == 2) {
        $('#_channelType2').val(value);
        $('#_channelType2_div').html(disp);    
    }    
}


function ChangeDayRestrictionEPIN(value,disp) {
    $('#_dayRestriction').val(value);
    $('#_dayRestriction_div').html(disp);    
}


function ChangeTimeRestrictionEPIN(type,value,disp) {
    if ( type == 1) {
        $('#_timeFromRestriction').val(value);
        $('#_timeFromRestriction_div').html(disp);    
    } else if ( type == 2) {
        $('#_timeToRestriction').val(value);
        $('#_timeToRestriction_div').html(disp);    
    }    
}



function ChangeSplitDurationEPIN(value,disp) {
    $('#_splitduration').val(value);
    $('#_splitduration_div').html(disp);    
}


function ChangeExpiryTimeLimitEPIN(value,disp) {
    $('#_expiryTime').val(value);
    $('#_expiryTime_div').html(disp);    
}


function ChangePinRequestCountEPIN(value,disp) {
    $('#_pinRequestCount').val(value);
    $('#_pinRequestCount_div').html(disp);    
}


function ChangePinRequestCountDurationEPIN(value,disp) {
    $('#_pinRequestCountDuration').val(value);
    $('#_pinRequestCountDuration_div').html(disp);    
}


function ChangeOperatorControllerEPIN(value,disp) {
    $('#_operatorController').val(value);
    $('#_operatorController_div').html(disp);    
}

function ChangeAlertOperatorOnFailureEPIN(value,disp) {
    $('#_isAlertOperatorOnFailure').val(value);
    $('#_isAlertOperatorOnFailure_div').html(disp);    
}

function ChangePINFormEPIN(value,disp) {
    if(value == 1){
    $('#_PINSource').val(value);
    $('#_PINSource_div').html(disp);  
    document.getElementById("hidePinSourcetxtbox").style.display = 'none';
   }else if (value == 2){
       $('#_PINSource').val(value);
    $('#_PINSource_div').html(disp);  
    var ele = document.getElementById("hidePinSourcetxtbox");
        ele.style.display = "block";
   }
}


function ChangeChallengeResponseForm(value,disp) {
    if(value == 1){
    $('#_ChallengeResponse').val(value);
    $('#_ChallengeResponse_div').html(disp);  
    document.getElementById("hideChallengeResponsetxtbox").style.display = 'none';
   }else if (value == 2){
       $('#_ChallengeResponse').val(value);
    $('#_ChallengeResponse_div').html(disp);  
    var ele = document.getElementById("hideChallengeResponsetxtbox");
        ele.style.display = "block";
   }
}
function ChangeSMSResponseURLStatus(value,disp) {
    $('#_smsurlStatus').val(value);
    $('#_smsurlStatus_div').html(disp);    
   }
function ChangeVOICEResponseURLStatus(value,disp) {
    $('#_voiceurlStatus').val(value);
    $('#_voiceurlStatus_div').html(disp);    
   }
function ChangeUSSDResponseURLStatus(value,disp) {
    $('#_ussdurlStatus').val(value);
    $('#_ussdurlStatus_div').html(disp);    
   }
   function ChangeWEBResponseURLStatus(value,disp) {
    $('#_weburlStatus').val(value);
    $('#_weburlStatus_div').html(disp);    
   }

function ChangePINLengthEPIN(value,disp) {
    $('#_PINLength').val(value);
    $('#_PINLength_div').html(disp);    
}



function ChangePINValidityEPIN(value,disp) {
    $('#_PINValidity').val(value);
    $('#_PINValidity_div').html(disp);    
}


function LoadEPINSettings(){
    var s = './loadepinsettings';

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {                        
            $('#_pinDeliveryType').val(data._pinDeliveryType);
            $('#_channelType').val(data._channelType);
            $('#_channelType2').val(data._channelType2);
            $('#_dayRestriction').val(data._dayRestriction);
            $('#_timeFromRestriction').val(data._timeFromInHour);
            $('#_timeToRestriction').val(data._timeToInHour);
            $('#_splitduration').val(data._duration);
            $('#_expiryTime').val(data._expiryTime);
            $('#_pinRequestCountDuration').val(data._pinRequestCountDuration);
            $('#_pinRequestCount').val(data._pinRequestCount);
            $('#_operatorController').val(data._operatorController);
            $('#_isAlertOperatorOnFailure').val(data._isAlertOperatorOnFailure);
            $('#_PINForm').val(data._PINForm);
            $('#_PINLength').val(data._PINLength);
            $('#_PINValidity').val(data._PINValidity);
//            $('#_classname').val(data._classname);
            $('#_smstext').val(data._smstext);
            $('#_smsurl').val(data._smsurl);
            $('#_voiceurl').val(data._voiceurl);
            $('#_weburl').val(data._weburl);
            $('#_ussdurl').val(data._ussdurl);
            
             $('#_ChallengeResponse').val(data._ChallengeResponse);
              $('#_smsurlStatus').val(data._smsurlStatus);
               $('#_voiceurlStatus').val(data._voiceurlStatus);
                $('#_ussdurlStatus').val(data._ussdurlStatus);
                 $('#_webeurlStatus').val(data._weburlStatus);
                    $('#_PinSourceClassNameclassname').val(data._PinSourceClassNameclassname);
                     $('#_ChallengeResponseclassname').val(data._ChallengeResponseclassname);
            
            
            if (data._pinDeliveryType == 1)
                ChangeDeliveryTypeEPIN(1,'Single Channel Delivery');
            else if (data._pinDeliveryType == 2)
                ChangeDeliveryTypeEPIN(2,'Dual Channel Delivery');
            else if (data._pinDeliveryType == 3)
                ChangeDeliveryTypeEPIN(3,'Single Channel Split Delivery');
            else if (data._pinDeliveryType == 4)
                ChangeDeliveryTypeEPIN(4,'Dual Channel Split Delivery');
            
            if (data._channelType == 1)            
                ChangeChannelEPIN(1,1,'SMS Channel');
            else if (data._channelType == 2)            
                ChangeChannelEPIN(1,2,'USSD Channel');
            else if (data._channelType == 3)            
                ChangeChannelEPIN(1,3,'Voice Channel');
            else if (data._channelType == 4)            
                ChangeChannelEPIN(1,4,'Email Channel');
            else if (data._channelType == 5)            
                ChangeChannelEPIN(1,5,'PIN Mailer');
            
            
            if (data._channelType2 == 1)            
                ChangeChannelEPIN(2,1,'SMS Channel');
            else if (data._channelType2 == 2)            
                ChangeChannelEPIN(2,2,'USSD Channel');
            else if (data._channelType2 == 3)            
                ChangeChannelEPIN(2,3,'Voice Channel');
            else if (data._channelType2 == 4)            
                ChangeChannelEPIN(2,4,'Email Channel');
            else if (data._channelType2 == 5)            
                ChangeChannelEPIN(2,5,'PIN Mailer');

            
            if (data._dayRestriction == 1)   
                ChangeDayRestrictionEPIN(1,'Week days only');
            else if (data._dayRestriction == 2)               
                ChangeDayRestrictionEPIN(2,'Whole Week (including Weekend)');
            
            if (data._timeFromInHour == 7)   
                ChangeTimeRestrictionEPIN(1,7,'7AM');
            else if (data._timeFromInHour == 8)   
                ChangeTimeRestrictionEPIN(1,8,'8AM');
            else if (data._timeFromInHour == 9)   
                ChangeTimeRestrictionEPIN(1,9,'9AM');
            else if (data._timeFromInHour == 10)   
                ChangeTimeRestrictionEPIN(1,10,'10AM');
            else if (data._timeFromInHour == 11)   
                ChangeTimeRestrictionEPIN(1,11,'11AM');
            else if (data._timeFromInHour == 0)   
                ChangeTimeRestrictionEPIN(1,0,'Any');
            
            if (data._timeToInHour == 12)                   
                ChangeTimeRestrictionEPIN(2,12,'12PM');
            else if (data._timeToInHour == 13)                   
                ChangeTimeRestrictionEPIN(2,13,'1PM');
            else if (data._timeToInHour == 14)                   
                ChangeTimeRestrictionEPIN(2,14,'2PM');
            else if (data._timeToInHour == 15)                   
                ChangeTimeRestrictionEPIN(2,15,'3PM');
            else if (data._timeToInHour == 16)                   
                ChangeTimeRestrictionEPIN(2,16,'4PM');
            else if (data._timeToInHour == 17)                   
                ChangeTimeRestrictionEPIN(2,17,'5PM');
            else if (data._timeToInHour == 18)                   
                ChangeTimeRestrictionEPIN(2,18,'6PM');
            else if (data._timeToInHour == 19)                   
                ChangeTimeRestrictionEPIN(2,19,'7PM');
            else if (data._timeToInHour == 24)                   
                ChangeTimeRestrictionEPIN(2,24,'Any');
            
            if (data._duration == 2)                                           
                ChangeSplitDurationEPIN(2,'2 mins');
            else if (data._duration == 5)                                           
                ChangeSplitDurationEPIN(5,'5 mins');
            else if (data._duration == 10)                                           
                ChangeSplitDurationEPIN(10,'10 mins');
            else if (data._duration == 15)                                           
                ChangeSplitDurationEPIN(15,'15 mins');
            else if (data._duration == 30)                                           
                ChangeSplitDurationEPIN(30,'30 mins');
                                    
            if (data._expiryTime == 1)                                           
                ChangeExpiryTimeLimitEPIN(1,'1 min');
            else if (data._expiryTime == 2)                                           
                ChangeExpiryTimeLimitEPIN(2,'2 mins');
            else if (data._expiryTime == 5)                                           
                ChangeExpiryTimeLimitEPIN(5,'5 mins');
            else if (data._expiryTime == 10)                                           
                ChangeExpiryTimeLimitEPIN(10,'10 mins');
            
            if (data._pinRequestCount == 1)                                           
                ChangePinRequestCountEPIN(1,'1 request');
            else if (data._pinRequestCount == 3)                                           
                ChangePinRequestCountEPIN(3,'3 requests');
            else if (data._pinRequestCount == 5)                                           
                ChangePinRequestCountEPIN(5,'5 requests');
            else if (data._pinRequestCount == 10)                                           
                ChangePinRequestCountEPIN(10,'10 requests');
            else if (data._pinRequestCount == -1)                                           
                ChangePinRequestCountEPIN(-1,'No Limit');
            
            if (data._pinRequestCountDuration == 1)                                           
                ChangePinRequestCountDurationEPIN(1,'Day');
            else if (data._pinRequestCountDuration == 2)                                           
                ChangePinRequestCountDurationEPIN(2,'Week');
            else if (data._pinRequestCountDuration == 3)                                           
                ChangePinRequestCountDurationEPIN(3,'Month');
            
            
            if (data._operatorController == 0)                                           
                ChangeOperatorControllerEPIN(0,'No, Not Required');
            else if (data._operatorController == 1)  
                ChangeOperatorControllerEPIN(1,'Yes, Single Operator Required');
            else if (data._operatorController == 2)  
                ChangeOperatorControllerEPIN(2,'Yes, Dual Operator Required');
            
                                                        
            if (data._isAlertOperatorOnFailure == false)                                           
                ChangeAlertOperatorOnFailureEPIN(false,'No, Do not inform on EPIN Failure.');
            else 
                ChangeAlertOperatorOnFailureEPIN(true,'Yes, Do inform on EPIN Failure');
            
            
            if (data._PINSource == 1)                                           
                ChangePINFormEPIN(1,'Internally by Axiom');
            else if (data._PINSource == 2)                                           
                ChangePINFormEPIN(2,'External By Third party Source');

             if (data._ChallengeResponse == 1)                                           
                ChangeChallengeResponseForm(1,'Internally by Axiom');
            else if (data._ChallengeResponse == 2)                                           
                ChangeChallengeResponseForm(2,'External By Third party Source');
            
            if (data._PINLength == 6)   
                ChangePINLengthEPIN(6,'6');
            else if (data._PINLength == 8)   
                ChangePINLengthEPIN(8,'8');
            else if (data._PINLength == 10)   
                ChangePINLengthEPIN(10,'10');
            else if (data._PINLength == 12)   
                ChangePINLengthEPIN(12,'12');
            else if (data._PINLength == 14)   
                ChangePINLengthEPIN(14,'14');
            else if (data._PINLength == 16)   
                ChangePINLengthEPIN(16,'16');
            
            
            if (data._PINValidity == 10)  
                ChangePINValidityEPIN(10,'10 minutes');
            else if (data._PINValidity == 20)  
                ChangePINValidityEPIN(30,'30 minutes');
            else if (data._PINValidity == 60)  
                ChangePINValidityEPIN(60,'1 hour');
            else if (data._PINValidity == 360)  
                ChangePINValidityEPIN(360,'6 hours');
            else if (data._PINValidity == 1440)  
                ChangePINValidityEPIN(1440,'1 day');
            else if (data._PINValidity == 10080)  
                ChangePINValidityEPIN(10080,'1 week');
            else if (data._PINValidity == -1)  
                ChangePINValidityEPIN(-1,'No Limit');
            
            
               if (data._smsurlStatus == 1)                                           
                ChangeSMSResponseURLStatus(1,'Active');
            else if (data._smsurlStatus == 0)                                           
                ChangeSMSResponseURLStatus(0,'In-Active');
            
             if (data._voiceurlStatus == 1)                                           
                ChangeVOICEResponseURLStatus(1,'Active');
            else if (data._voiceurlStatus == 0)                                           
                ChangeVOICEResponseURLStatus(0,'In-Active');
          
          
            if (data._ussdurlStatus == 1)                                           
                ChangeUSSDResponseURLStatus(1,'Active');
            else if (data._ussdurlStatus == 0)                                           
                ChangeUSSDResponseURLStatus(0,'In-Active');
            
            if (data._weburlStatus == 1)                                           
                ChangeWEBResponseURLStatus(1,'Active');
            else if (data._weburlStatus == 0)                                           
                ChangeWEBResponseURLStatus(0,'Active');
        }
    });
}


function changeepin() {
    $('#buttonChangeEpin').attr("disabled", true);
    var s = './changeepinstatus';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#rejectedForm").serialize(),
        success: function(data) {
            if (strcmpEPIN(data._result, "error") == 0) {
                $('#rejected-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#buttonChangeEpin').attr("disabled", false);
            }
            else if (strcmpEPIN(data._result, "success") == 0) {
                $('#rejected-result').html("<span><font color=blue>" + data._message + "</font></span>");

                window.setTimeout(RefreshEpin, 3000);

            }
        }
    });
}



function loadepinDetails(_trackid, userid, status) {
    $("#_epinid").val(_trackid);
    $("#_userid").val(userid);
    $("#_e_status").val(status);
    $('#rejected-result').html("");
    $("#rejected").modal();
}


function ChangeEpinStatus(epinid, userid, status) {
    var s = './changeepinstatus?_e_status=' + status + '&_userid=' + encodeURIComponent(userid) + '&_epinid=' + encodeURIComponent(epinid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpEPIN(data._result, "error") == 0) {
                Alert4EPINSetting("<span><font color=red>" + data._message + "</font></span>");

            }
            else if (strcmpEPIN(data._result, "success") == 0) {
                Alert4EPINSetting("<span><font color=blue>" + data._message + "</font></span>");
                window.setTimeout(RefreshEpin, 3000);
//                var uiToChange = '#'+uidiv;
//                $(uiToChange).html(data._value);
            }
        }
    });
}

function resendepin(epinid, userid, status) {
    var s = './changeepinstatus?_e_status=' + status + '&_userid=' + encodeURIComponent(userid) + '&_epinid=' + encodeURIComponent(epinid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpEPIN(data._result, "error") == 0) {
                Alert4EPINSetting("<span><font color=red>" + data._message + "</font></span>");

            }
            else if (strcmpEPIN(data._result, "success") == 0) {
                Alert4EPINSetting("<span><font color=blue>" + data._message + "</font></span>");
                window.setTimeout(RefreshFailedEpin, 3000);
//                var uiToChange = '#'+uidiv;
//                $(uiToChange).html(data._value);
            }
        }
    });
}


function RefreshEpin() {
    window.location.href = "./ecopinpending.jsp";
}
function RefreshFailedEpin() {
    window.location.href = "./ecopinfailed.jsp";
}
function RefreshEpinSettting() {
    window.location.href = "./epinsetting.jsp";
}


function editEPINSettings(){
    var s = './editepinsettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#epinsettingsform").serialize(),
        success: function(data) {
            if ( strcmpEPIN(data._result,"error") == 0 ) {
                $('#save-epin-settings-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4EPINSetting("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpEPIN(data._result,"success") == 0 ) {
                $('#save-epin-settings-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4EPINSetting("<span><font color=blue>" + data._message + "</font></span>");
//                 window.setTimeout(RefreshEpinSettting(), 10000);
            }
        }
    });
}