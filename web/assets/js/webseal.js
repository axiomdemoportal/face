function strcmpWebSealMsg(a, b) {
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4WebSealSetting(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function (result) {

    });
}

function imageFileUploadForWebseal(type) {
    var s = './UploadImageForWebSeal?type=' + type;
    $.ajaxFileUpload({
        type: 'POST',
        fileElementId: 'uploadForWebseal' + type,
        url: s,
        dataType: 'json',
        success: function (data, status) {

            if (strcmpWebSealMsg(data.result, "error") == 0) {
                Alert4WebSealSetting("<span><font color=blue>" + data.message + "</font></span>");
            }
            else if (strcmpWebSealMsg(data.result, "success") == 0) {
                bootbox.alert("<h2>" + "<span><font color=blue>" + data.message + "</font></span>" + "</h2>", function (result) {
                });

            }
        },
        error: function (data, status, e) {
            alert(e);
        }
    });
}

function loadWebSealSetting() {
    var s = './LoadWebSealSetting';

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (data._webSealURL !== '') {
                $('#_webSealURL').val(data._webSealURL);
                $('#_hackerResult').val(data._hackerResult);
                var obj = JSON.parse(data._dnsForWeb);
                $('#_dnsForWeb1').val(obj._dnsForWeb1);
                $('#_dnsForWeb2').val(obj._dnsForWeb2);
                $('#_dnsForWeb3').val(obj._dnsForWeb3);
                var img = document.createElement("IMG");
                img.src = "data:image/png;base64," + data._errorImage;
                img.style.height = '40px';
                img.style.width = '40px';
                document.getElementById('_errorImage').appendChild(img);
                var img1 = document.createElement("IMG");
                img1.src = "data:image/png;base64," + data._successImage;
                img1.style.height = '40px';
                img1.style.width = '40px';
                document.getElementById('_successImage').appendChild(img1);

            }
        }
    });
}

function addWebSealSetting() {
    var s = './AddWebSealSetting';
   if (strcmpWebSealMsg(document.getElementById("_webSealURL").value, "") === 0) {
       alert("Please enter websealurl");
       return;
   }
    if (strcmpWebSealMsg(document.getElementById("_hackerResult").value, "") === 0) {
       alert("Please enter Hacker Page url");
       return;
   }
    if (strcmpWebSealMsg(document.getElementById("_dnsForWeb1").value, "") === 0) {
       alert("Please enter dnsserver addrsss");
       return;
   }
    
    
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#_webSealInfoForm").serialize(),
        success: function (data) {
            if (strcmpWebSealMsg(data._result, "error") == 0) {

                Alert4WebSealSetting("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpWebSealMsg(data._result, "success") == 0) {
                bootbox.alert("<h2>" + "<span><font color=blue>" + data._message + "</font></span>" + "</h2>", function (result) {
                    window.location = "./websealSetting.jsp";
                });
            }
        }
    });
}

// Added by abhishek
function generateWebSealTable() {
    
    document.getElementById("graphBar").innerHTML="";
    var val3 = encodeURIComponent(document.getElementById('startdate').value);
    var val4 = encodeURIComponent(document.getElementById('enddate').value);
    var val5 = document.getElementById("_UnitId").value;
    //alert("val5 "+val5);
    var ele = document.getElementById("REPORT");
    ele.style.display = "block";
    
    if (document.getElementById('startdate').value.length == 0 || document.getElementById('enddate').value.length == 0) {
         Alert4Cert("<span><font color=red>Date Range is not selected!!!</font></span>");
        return;
    }
    var day_data = DonutChart(val3, val4, val5);
    Morris.Donut({
        element: 'graphDount',
        data: day_data
    });
    var day_data = BarChartDemo(val3, val4, val5);
    Morris.Bar({
        element: 'graphBar',
        data: day_data,
        xkey: 'label',
        ykeys: ['value'],
        labels: ['value'],
        barColors: function (type) {
            if (type === 'bar') {
                return '#0066CC';
            }
            else {
                return '#0066CC';
            }
        }
    });

    var s = './websealReporttable.jsp?&_startdate=' + val3 + '&_enddate=' + val4 + '&monitorName=' +val5;
    
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#webseal_table_main').html(data);
            
        }
        
    });
}

function DonutChart(val3, val4, val5) {
    var s = './WebSealDountChart?_startDate=' + val3 + "&_endDate=" + val4 + '&_monitername=' +val5;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}

function BarChartDemo(val3, val4, val5) {
    var s = './WebSealBarChart?_startDate=' + val3 + "&_endDate=" + val4 + '&_monitername=' +val5; 
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}
function downloadwebsealReport(type, startdate, enddate) {
    var s = './DownloadWebSealReport?_reporttype=' + type + "&_startDate=" + startdate + "&_endDate=" + enddate;
    window.location.href = s;
    return false;
}