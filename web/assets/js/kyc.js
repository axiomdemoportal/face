/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function strcmpkyc(a, b) {
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function Alert4Kyc(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}

function RefreshKycList() {
    window.location.href = "./kycpendingrequest.jsp";
}
function approve(userid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './approve?_userid=' + encodeURIComponent(userid);
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpkyc(data._result, "error") == 0) {
                        Alert4Kyc("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpkyc(data._result, "success") == 0) {
                        Alert4Kyc("<span><font color=blue>" + data._message + "</font></span>");
                         window.setTimeout(RefreshKycList, 2000);
                        
                    }
                }
            });
        }
    });

}

function reject(_uid) {
     $('#_usersid').val(_uid);
     $("#kycreject").modal();
}

function rejectRequest(){
    
    var s = './rejectkyc';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#kycrejectform").serialize(),
        success: function(data) {
            if ( strcmpkyc(data._result,"error") == 0 ) {
                $('#rejectkyc-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//                Alert4Kyc("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpkyc(data._result,"success") == 0 ) {
                $('#rejectkyc-result').html("<span><font color=blue>" + data._message + "</font></span>");
                   window.setTimeout(RefreshKycList, 2000);
//                Alert4Kyc("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
} 
//
//function downloadkycfiles(userid) {
//    var s = './downloadkyczip?_userid=' + encodeURIComponent(userid);
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
////        data: $("#certificateform").serialize(),
//        success: function(data) {
//            if ( strcmpkyc(data._result,"error") == 0 ) {
//                $('#rejectkyc-result').html("<span><font color=red>" + data._message + "</font></span></small>");
////                Alert4Kyc("<span><font color=red>" + data._message + "</font></span>");
//            }
//            else if ( strcmpkyc(data._result,"success") == 0 ) {
//                $('#rejectkyc-result').html("<span><font color=blue>" + data._message + "</font></span>");
////                Alert4Kyc("<span><font color=blue>" + data._message + "</font></span>");
//            }
//        }
//    });
//
//}
function downloadkycfiles(userid){
     var s = './downloadkyczip?_userid=' + encodeURIComponent(userid);
     window.location.href = s;
     return false;
    
}