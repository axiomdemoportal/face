/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function strcmpResource(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4Resource(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}

function ResourceType(value) {
    if (value === 1) {
        $('#_resourceName').val("1");
        $('#_resourceName-Axiom').html("User & Password");
    } else if (value === 2) {
        $('#_resourceName').val("2");
        $('#_resourceName-Axiom').html("Secure Phase (Image Based)");
    } else if (value === 3) {
        $('#_resourceName').val("3");
        $('#_resourceName-Axiom').html("One Time Password Tokens");
    } else if (value === 4) {
        $('#_resourceName').val("4");
        $('#_resourceName-Axiom').html("Digital Certificates And Tokens");
    } else if (value === 5) {
        $('#_resourceName').val("5");
        $('#_resourceName-Axiom').html("Challenge Response (Q&A)");
    } else if (value === 6) {
        $('#_resourceName').val("6");
        $('#_resourceName-Axiom').html("Trusted Device (via Mobile)");
    } else if (value === 7) {
        $('#_resourceName').val("7");
        $('#_resourceName-Axiom').html("Geo-location (Fence & Track)");
    } else if (value === 8) {
        $('#_resourceName').val("8");
        $('#_resourceName-Axiom').html("TwoWayAuth Management");
    }
}

function searchResource() {
    var val = document.getElementById('_keyword').value;
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    if (val.length < 1) {
        Alert4Resource("<span><font color=red>Search keyword cannot be blank!!!</font></span>");
        return;
    }
    $('d').is;
    var val1 = document.getElementById('_resourceName').value;
    var s;
     s = './pkitokenstable.jsp?_searchtext=' + val;
    if (val1 == 1) {
        var s = './userstable.jsp?_searchtext=' + encodeURIComponent(val);
    } else if (val1 == 2) {
        s = './userSecurePhraseTable.jsp?_searchtext=' + encodeURIComponent(val);
    } else if (val1 == 3) {
        s = './otptables.jsp?_searchtext=' + val;
    } else if (val1 == 4) {
       
    } else if (val1 == 5) {
        s = './ChallengeResponsetable.jsp?_searchtext=' + val;
    } else if (val1 == 6) {
        s = './trusteddevicetablesMultiple.jsp?_searchtext=' + val;
    } else if (val1 == 7) {
        s = './Geotracking.jsp?_searchtext=' + val;
    } else if (val1 == 8) {
        s = './twowayauthtable.jsp?_searchtext=' + val;
    } else if (val1 == 9) {
      
        s = './biometrictable.jsp?_searchtext=' + val;
    } else if (val1 == 10) {
     
        s = './externalSystemReports.jsp?_searchtext=' + val;
    }

    pleaseWaitDiv.modal();
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#users_table_main').html(data);
            pleaseWaitDiv.modal('hide');
        }
    });
}


function searchResourceAppCerts() {
    var val = document.getElementById('_keyword').value;
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    if (val.length < 1) {
        Alert4Resource("<span><font color=red>Search keyword cannot be blank!!!</font></span>");
        return;
    }
    $('d').is
    var val1 = document.getElementById('_resourceName').value;
    var s;
     s = './pkitokenstableForAppCerts.jsp?_searchtext=' + val;
//    if (val1 == 1) {
//        var s = './userstable.jsp?_searchtext=' + encodeURIComponent(val);
//    } else if (val1 == 2) {
//        s = './userSecurePhraseTable.jsp?_searchtext=' + encodeURIComponent(val);
//    } else if (val1 == 3) {
//        s = './otptables.jsp?_searchtext=' + val;
//    } else if (val1 == 4) {
//       
//    } else if (val1 == 5) {
//        s = './ChallengeResponsetable.jsp?_searchtext=' + val;
//    } else if (val1 == 6) {
//        s = './trusteddevicetablesMultiple.jsp?_searchtext=' + val;
//    } else if (val1 == 7) {
//        s = './Geotracking.jsp?_searchtext=' + val;
//    } else if (val1 == 8) {
//        s = './twowayauthtable.jsp?_searchtext=' + val;
//    }

    pleaseWaitDiv.modal();
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#users_table_main').html(data);
            pleaseWaitDiv.modal('hide');
        }
    });
}

$(document).ready(function() {
    $('#_resourceName').change(function() {
        var selectedValue = $(this).val();
        if (strcmpResource(selectedValue, "1") == 0) {
              document.getElementById("otp_hide").style.display = 'none';
              var ele = document.getElementById("user_hide");
                ele.style.display = "block";
        }else if (strcmpResource(selectedValue, "3") == 0) {
              document.getElementById("user_hide").style.display = 'none';
              var ele = document.getElementById("otp_hide");
                ele.style.display = "block";
        }else{
            document.getElementById("user_hide").style.display = 'none';
            document.getElementById("otp_hide").style.display = 'none';
        }
 });
});
    