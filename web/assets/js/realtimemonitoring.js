var updateInterval = 5;
var upinter = 5000;
var v1 = 0;
var settingName;
var type;
var starttime = new Date();
var endtime = new Date();

function loadCounts(settingName, type) {
    var s = './realtime?_settingName=' + settingName + '&_type=' + type;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#realtime").serialize(),
        success: function(data) {
            v1 = data._count;
            updateInterval = 5;
            upinter = updateInterval * 1000;
        }
    });
}
//// area chart
// msg 1
$(function() {
    var data1 = new RealTimeData1(1);
    var chart1 = $('#real-time-area1').epoch({
        type: 'time.line',
        data: data1.history(),
        axes: ['left', 'bottom']
    });
    setInterval(function() {
        chart1.push(data1.next());
    }, upinter);
    chart1.push(data1.next());
});

// bar chart

// msg 1
$(function() {
    var data1 = new RealTimeData1(1);
    var chart1 = $('#real-time-bar1').epoch({
        type: 'time.bar',
        data: data1.history(),
        axes: ['left', 'bottom']
    });
    charts = $('#real-time-bar1').epoch;
    setInterval(function() {
        chart1.push(data1.next());
    }, upinter);
    chart1.push(data1.next());
});

(function() {
    var RealTimeData1 = function(layers) {
        this.layers = layers;
        var t = new Date();
        var mili = t.getMilliseconds();
        t.setMilliseconds(mili - 30000);
//        alert(t.getTime());
        this.timestamp = (t.getTime() / 1000) | 0;
//        alert(this.timestamp);
    };
//     history for initial data

    RealTimeData1.prototype.history = function(entries)
    {
        if (typeof (entries) != 'number' || !entries) {
            entries = 1;
        }
        var history = [];
        for (var k = 0; k < this.layers; k++) {
            history.push({values: []});
        }
        for (var i = 0; i < entries; i++) {
            history[0].values.push({time: this.timestamp, y: (v1)});
//            history[1].values.push({time: this.timestamp, y: v2});
//            history[2].values.push({time: this.timestamp, y: v3});
//            history[3].values.push({time: this.timestamp, y: v4});
            this.timestamp = this.timestamp + 1;
        }
        return history;
    };
    RealTimeData1.prototype.next = function() {     // push data for the graph    
        var entry = [];
        loadCounts(settingName, type);
        entry.push({time: this.timestamp, y: (v1)});
//        entry.push({time: this.timestamp, y: v2});
//        entry.push({time: this.timestamp, y: v3});
//        entry.push({time: this.timestamp, y: v4});
        this.timestamp = this.timestamp + updateInterval;
        return entry;
    };
    window.RealTimeData1 = RealTimeData1;
})();

function loadmsgs(SettingName, Type)
{
    settingName = SettingName;
    type = Type;
    loadCounts(settingName, type);
}
