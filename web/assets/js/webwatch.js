function realtime(settingName, type,start,end)
{
//    $('#tabDetail').empty();
    document.getElementById('tabDetail').style.display = "block";
      document.getElementById('tabDs').style.display = "none";
           document.getElementById('tabchart').style.display = "none";
//           
//                   document.getElementById("tabDs").innerHTML = "";
//            document.getElementById("tabchart").innerHTML = "";
//           
     
    var s = './realtime.jsp?_settingName=' + settingName + '&_type=' + type+ '&_startdate=' + start + "&_enddate=" + end;
    
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#tabDetail').html(data);
        }
    });
}
function chart(settingName, type,start,end)
{
    $('#tabchart').empty();
    document.getElementById('tabDs').style.display = "none";
    document.getElementById('tabDetail').style.display = "none";
     document.getElementById('tabchart').style.display = "block";
             document.getElementById("tabDs").innerHTML = "";
            document.getElementById("tabchart").innerHTML = "";
    var s = './chart.jsp?_settingName=' + settingName + '&_type=' + type+ '&_startdate=' + start + "&_enddate=" + end;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#tabchart').html(data);
        }
    });
}
function recentreport(settingName, type)
{
    $('#tabDetail').empty();
//    document.getElementById('tabDs').style.display = "none";
    var s = './settingExecutionTable.jsp?_settingName=' + settingName + '&_type=' + type;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#tabDetail').html(data);
        }
    });
}
function textualreport(settingName, type,start,end)
{
    $('#tabDetail').empty();
    document.getElementById('tabDs').style.display = "none";
    document.getElementById('tabDetail').style.display = "block";     
     document.getElementById('tabchart').style.display = "none";
//        document.getElementById("tabDetail").innerHTML = "";
//            document.getElementById("tabchart").innerHTML = "";
    var s = './settingExecutionTextReport.jsp?_settingName=' + settingName + '&_type=' + type+ '&_startdate=' + start + "&_enddate=" + end;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#tabDetail').html(data);
        }
    });
}

function searchReport(settingName, type) {
    var val1 = encodeURIComponent(document.getElementById('startdate').value);
    var val2 = encodeURIComponent(document.getElementById('enddate').value);
    if (document.getElementById('startdate').value.length == 0 || document.getElementById('enddate').value.length == 0) {
        Alert4Msg("Date Range is not selected!!!");
        return;
    }
    
//    if (document.getElementById('startdate').value > document.getElementById('enddate').value) {
//        Alert4Msg("Enter Proper Date Range!!!");
//        return;
//    }
      $('#tabD').empty();
    var s = './settingExecutionReport.jsp?_settingName=' + settingName + '&_type=' + type + '&_startdate=' + val1 + "&_enddate=" + val2;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#tabD').html(data);
        }});
}
