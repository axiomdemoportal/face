function strcmpPushMsg(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}


function Alert4PushMsgSetting(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function (result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}
function uploadPushWatch(){
    
}

function ChangeActiveStatusPushMsg(perference, value) {

    if (perference == 1) {
        if (value == 1) {
            $('#_status').val("1");
            $('#_status-primary-PushMsg').html("Active");
        } else {
            $('#_status').val("0");
            $('#_status-primary-PushMsg').html("Suspended");
        }
    } else if (perference == 2) {
        if (value == 1) {
            $('#_statusS').val("1");

            $('#_statusS-secondary-PushMsg').html("Active");
        } else {
            $('#_statusS').val("0");
            $('#_statusS-secondary-PushMsg').html("Suspended");
        }
    } else if (perference == 3) {
        if (value == 1) {
            $('#_statusT').val("1");

            $('#_status-ternory-PushMsg').html("Active");
        } else {
            $('#_statusT').val("0");
            $('#_status-ternory-PushMsg').html("Suspended");
        }
    }
}

function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
    
    var CSV = '';    
    
    CSV += ReportTitle + '\r\n\n';
      if (ShowLabel) {
        var row = "";
        for (var index in arrData[0]) {
            row += index + ',';
        }
   row = row.slice(0, -1);
        CSV += row + '\r\n';
    }
    
    for (var i = 0; i < arrData.length; i++) {
        var row = "";
        
        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            row += '"' + arrData[i][index] + '",';
        }

        row.slice(0, row.length - 1);
        
        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {        
        alert("Invalid data");
        return;
    }   
    
    //Generate a file name
    var fileName = "DeviceTokenDetails";
    //this will remove the blank-spaces from the title and replace it with an underscore
    fileName += ReportTitle.replace(/ /g,"_");   
    
    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
    
    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension    
    
    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");    
    link.href = uri;
    
    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";
    
    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}


function ChangeDelayWhileIdel(perference, value) {
    if (perference == 1) {
        if (value == 1) {
            $('#_delayWhileIdle').val("1");
            $('#_delayWhileIdle-primary-PushMsg').html("Active");
        } else {
            $('#_delayWhileIdle').val("0");
            $('#_delayWhileIdle-primary-PushMsg').html("Suspended");
        }
    } else if (perference == 2) {

        if (value == 1) {
            $('#_delayWhileIdleS').val("1");
            $('#_delayWhileIdleS-secondary-PushMsg').html("Production");
        } else {
            $('#_delayWhileIdleS').val("0");
            $('#_delayWhileIdleS-secondary-PushMsg').html("Sandbox");
        }
    } else if (perference == 3) {
        if (value == 1) {
            $('#_delayWhileIdleT').val("1");
            $('#_delayWhileIdle-ternory-PushMsg').html("Active");
        } else {
            $('#_delayWhileIdleT').val("0");
            $('#_delayWhileIdle-ternory-PushMsg').html("Suspended");
        }

    }
}
function ChangeFailOverPushMsg(value) {
    //1 for enabled
    //0 for disabled
    if (value == 1) {
        $('#_autofailover').val("1");
        $('#_autofailover-primary-PushMsg').html("Enabled");
    } else {
        $('#_autofailover').val("0");
        $('#_autofailover-primary-PushMsg').html("Disabled");
    }
}
function ChangeFailOverPushMsgT(value) {
    //1 for enabled
    //0 for disabled
    if (value == 1) {
        $('#_autofailoverT').val("1");
        $('#_autofailover-primary-PushMsg').html("Enabled");
    } else {
        $('#_autofailoverT').val("0");
        $('#_autofailover-primary-PushMsg').html("Disabled");
    }
}

function ChangeRetryPushMsg(preference, count) {

    if (preference == 1) {
        if (count == 2) {
            $('#_retries').val("2");
            $('#_retries-primary-PushMsg').html("2 retries");
        } else if (count == 3) {
            $('#_retries').val("3");
            $('#_retries-primary-PushMsg').html("3 retries");
        } else if (count == 5) {
            $('#_retries').val("5");
            $('#_retries-primary-PushMsg').html("5 retries");
        }
    } else if (preference == 2) {

        if (count == 2) {
            $('#_retriesS').val("2");
            $('#_retriesS-secondary-PushMsg').html("2 retries");
        } else if (count == 3) {
            $('#_retriesS').val("3");
            $('#_retriesS-secondary-PushMsg').html("3 retries");
        } else if (count == 5) {
            $('#_retriesS').val("5");
            $('#_retriesS-secondary-PushMsg').html("5 retries");
        }
    } else if (preference == 3) {
        if (count == 2) {
            $('#_retriesT').val("2");
            $('#_retries-ternory-PushMsg').html("2 retries");
        } else if (count == 3) {
            $('#_retriesT').val("3");
            $('#_retries-ternory-PushMsg').html("3 retries");
        } else if (count == 5) {
            $('#_retriesT').val("5");
            $('#_retries-ternory-PushMsg').html("5 retries");
        }
    }
}

function ChangeRetryFromGoogle(preference, count) {

    if (preference == 1) {
        if (count == 2) {
            $('#_retriesFromGoogle').val("2");
            $('#_retriesFromGoogle-primary-PushMsg').html("2 retries");
        } else if (count == 3) {
            $('#_retriesFromGoogle').val("3");
            $('#_retriesFromGoogle-primary-PushMsg').html("3 retries");
        } else if (count == 5) {
            $('#_retriesFromGoogle').val("5");
            $('#_retriesFromGoogle-primary-PushMsg').html("5 retries");
        }
    } else if (preference == 2) {

        if (count == 2 || count == 0) {
            $('#_retriesFromGoogleS').val("2");
            $('#_retriesFromGoogleS-secondary-PushMsg').html("2 retries");
        } else if (count == 3) {
            $('#_retriesFromGoogleS').val("3");
            $('#_retriesFromGoogleS-secondary-PushMsg').html("3 retries");
        } else if (count == 5) {
            $('#_retriesFromGoogleS').val("5");
            $('#_retriesFromGoogleS-secondary-PushMsg').html("5 retries");
        }
    } else if (preference == 3) {
        if (count == 2) {
            $('#_retriesFromGoogleT').val("2");
            $('#_retriesFromGoogle-ternory-PushMsg').html("2 retries");
        } else if (count == 3) {
            $('#_retriesFromGoogleT').val("3");
            $('#_retriesFromGoogle-ternory-PushMsg').html("3 retries");
        } else if (count == 5) {
            $('#_retriesFromGoogleT').val("5");
            $('#_retriesFromGoogle-ternory-PushMsg').html("5 retries");
        }
    }
}

function ChangeTimeToLive(preference, count) {

    if (preference == 1) {
        if (count == 10) {
            $('#_timetolive').val("10");
            $('#_timetolive-primary-PushMsg').html("10 Seconds");
        } else if (count == 20) {
            $('#_timetolive').val("20");
            $('#_timetolive-primary-PushMsg').html("20 Seconds");
        } else if (count == 30) {
            $('#_timetolive').val("30");
            $('#_timetolive-primary-PushMsg').html("30 Seconds");
        }
    } else if (preference == 2) {
        if (count == 10) {
            $('#_timetoliveS').val("10");
            $('#_timetoliveS-secondary-PushMsg').html("10 Seconds");
        } else if (count == 20) {
            $('#_timetoliveS').val("20");
            $('#_timetoliveS-secondary-PushMsg').html("20 Seconds");
        } else if (count == 30) {
            $('#_timetoliveS').val("30");
            $('#_timetoliveS-secondary-PushMsg').html("30 Seconds");
        }
    } else if (preference == 3) {
        if (count == 10) {
            $('#_timetoliveT').val("10");
            $('#_timetolive-ternory-PushMsg').html("10 Seconds");
        } else if (count == 20) {
            $('#_timetoliveT').val("20");
            $('#_timetolive-ternory-PushMsg').html("20 Seconds");
        } else if (count == 30) {
            $('#_timetoliveT').val("30");
            $('#_timetolive-ternory-PushMsg').html("30 Seconds");
        }
    }
}
function ChangeRetryDurationPushMsg(perference, duration) {
    //1 for enabled
    //0 for disabled
    if (perference == 1) {
        if (duration == 10) {
            $('#_retryduration').val("10");
            $('#_retryduration-primary-PushMsg').html("10 seconds");
        } else if (duration == 30) {
            $('#_retryduration').val("30");
            $('#_retryduration-primary-PushMsg').html("30 seconds");
        } else if (duration == 60) {
            $('#_retryduration').val("60");
            $('#_retryduration-primary-PushMsg').html("60 seconds");
        }
    } else if (perference == 2) {
        if (duration == 10) {
            $('#_retrydurationS').val("10");
            $('#_retrydurationS-secondary-PushMsg').html("10 seconds");
        } else if (duration == 30) {
            $('#_retrydurationS').val("30");
            $('#_retrydurationS-secondary-PushMsg').html("30 seconds");
        } else if (duration == 60) {
            $('#_retrydurationS').val("60");
            $('#_retrydurationS-secondary-PushMsg').html("60 seconds");
        }
    } else if (perference == 3) {
        if (duration == 10) {
            $('#_retrydurationT').val("10");
            $('#_retryduration-ternory-PushMsg').html("10 seconds");
        } else if (duration == 30) {
            $('#_retrydurationT').val("30");
            $('#_retryduration-ternory-PushMsg').html("30 seconds");
        } else if (duration == 60) {
            $('#_retrydurationT').val("60");
            $('#_retryduration-ternory-PushMsg').html("60 seconds");
        }
    }
}



function PushMsgprimary() {
    var s = './loadpushnotificationsetting?_type=18&_preference=1';

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            $('#_ip').val(data._ip);
            $('#_port').val(data._port);
            if (data._logConfirmation == true) {
                $('#_logConfirmation').attr("checked", true);
            } else {
                $('#_logConfirmation').attr("checked", false);
            }
            $('#_className').val(data._className);
            $('#_reserve1').val(data._reserve1);
            $('#_reserve2').val(data._reserve2);
            $('#_reserve3').val(data._reserve3);
            $('#_autofailover').val(data._autofailover);
            $('#_retries').val(data._retries);
            $('#_retryduration').val(data._retryduration);
            $('#_gcmurl').val(data._gcmurl);
            $('#_apikey').val(data._apikey);
            $('#_googleSenderKey').val(data._googleSenderKey);
            $('#_status').val(data._status);
            $('#_retriesFromGoogle').val(data._retriesFromGoogle);
            $('#_timetolive').val(data._timetolive);
            $('#_delayWhileIdle').val(data._delayWhileIdle);
            if (data._status == 1)
                ChangeActiveStatusPushMsg(1, 1);
            else
                ChangeActiveStatusPushMsg(1, 0);

            if (data._autofailover == 1)
                ChangeFailOverPushMsg(1);
            else
                ChangeFailOverPushMsg(0);

            if (data._retryduration == 10)
                ChangeRetryDurationPushMsg(1, 10);
            else if (data._retryduration == 30)
                ChangeRetryDurationPushMsg(1, 30);
            else if (data._retryduration == 60)
                ChangeRetryDurationPushMsg(1, 60);
            else
                ChangeRetryDurationPushMsg(1, 10);

            if (data._retries == 2)
                ChangeRetryPushMsg(1, 2);
            else if (data._retries == 3)
                ChangeRetryPushMsg(1, 3);
            else if (data._retries == 5)
                ChangeRetryPushMsg(1, 5);
            else
                ChangeRetryPushMsg(1, 2);


            if (data._retriesFromGoogle == 2)
                ChangeRetryFromGoogle(1, 2);
            else if (data._retriesFromGoogle == 3)
                ChangeRetryFromGoogle(1, 3);
            else if (data._retriesFromGoogle == 5)
                ChangeRetryFromGoogle(1, 5);


            if (data._timetolive == 10)
                ChangeTimeToLive(1, 10);
            else if (data._timetolive == 20)
                ChangeTimeToLive(1, 20);
            else if (data._timetolive == 30)
                ChangeTimeToLive(1, 30);

            if (data._delayWhileIdle == 1)
                ChangeDelayWhileIdel(1, 1);
            else if (data._delayWhileIdle == 0)
                ChangeDelayWhileIdel(1, 0);
        }
    });
}

function PushMsgsecondary() {
    var s = './loadiphonepushsettings?_type=19&_preference=1';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            $('#_ipS').val(data._ipS);
            $('#_portS').val(data._portS);
            if (data._logConfirmationS == true) {
                $('#_logConfirmationS').attr("checked", true);
            } else {
                $('#_logConfirmationS').attr("checked", false);
            }
            $('#_classNameS').val(data._classNameS);
            $('#_reserve1S').val(data._reserve1S);
            $('#_reserve2S').val(data._reserve2S);
            $('#_reserve3S').val(data._reserve3S);
            $('#_retriesS').val(data._retriesS);
            $('#_retrydurationS').val(data._retrydurationS);
            $('#_statusS').val(data._statusS);
            $('#_bundleID').val(data._bundleID);
            $('#_certpassowrd').val(data._certpassowrd);

//             alert(data.uploadStatus);
            $('#uploadStatus').html(data.uploadStatus);
//             alert(data._retriesS);

            if (data._retriesS == 2)
                ChangeRetryPushMsg(2, 2);
            else if (data._retriesS == 3)
                ChangeRetryPushMsg(2, 3);
            else if (data._retriesS == 5)
                ChangeRetryPushMsg(2, 5);
            else
                ChangeRetryPushMsg(1, 2);


            if (data._statusS == 1)
                ChangeActiveStatusPushMsg(2, 1);
            else
                ChangeActiveStatusPushMsg(2, 0);

            if (data._retrydurationS == 10)
                ChangeRetryDurationPushMsg(2, 10);
            else if (data._retrydurationS == 30)
                ChangeRetryDurationPushMsg(2, 30);
            else if (data._retrydurationS == 60)
                ChangeRetryDurationPushMsg(2, 60);
            else
                ChangeRetryDurationPushMsg(2, 10);

            if (data._delayWhileIdleS == 1)
                ChangeDelayWhileIdel(2, 1);
            else if (data._delayWhileIdleS == 0)
                ChangeDelayWhileIdel(2, 0);
        }
    }
    );
}

function PushMsgternory() {
    var s = './loadwebpushnotificationsetting?_type=33&_preference=3';

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            $('#_ipT').val(data._ipT);
            $('#_portT').val(data._portT);
            if (data._logConfirmation == true) {
                $('#_logConfirmation').attr("checked", true);
            } else {
                $('#_logConfirmation').attr("checked", false);
            }
            $('#_classNameT').val(data._classNameT);
            $('#_reserve1T').val(data._reserve1T);
            $('#_reserve2T').val(data._reserve2T);
            $('#_reserve3T').val(data._reserve3T);
            $('#_autofailoverT').val(data._autofailoverT);
            $('#_retriesT').val(data._retriesT);
            $('#_retrydurationT').val(data._retrydurationT);
            $('#_gcmurlT').val(data._gcmurlT);
            $('#_apikeyT').val(data._apikeyT);
            $('#_googleSenderKeyT').val(data._googleSenderKeyT);
            $('#_statusT').val(data._statusT);
            $('#_retriesFromGoogleT').val(data._retriesFromGoogleT);
            $('#_timetoliveT').val(data._timetoliveT);
            $('#_delayWhileIdleT').val(data._delayWhileIdleT);
            if (data._statusT == 1)
                ChangeActiveStatusPushMsg(3, 1);
            else
                ChangeActiveStatusPushMsg(3, 0);

            if (data._autofailoverT == 1)
                ChangeFailOverPushMsgT(1);
            else
                ChangeFailOverPushMsgT(0);

            if (data._retrydurationT == 10)
                ChangeRetryDurationPushMsg(3, 10);
            else if (data._retrydurationT == 30)
                ChangeRetryDurationPushMsg(3, 30);
            else if (data._retrydurationT == 60)
                ChangeRetryDurationPushMsg(3, 60);
            else
                ChangeRetryDurationPushMsg(3, 10);

            if (data._retriesT == 2)
                ChangeRetryPushMsg(3, 2);
            else if (data._retriesT == 3)
                ChangeRetryPushMsg(3, 3);
            else if (data._retriesT == 5)
                ChangeRetryPushMsg(3, 5);
            else
                ChangeRetryPushMsg(3, 2);


            if (data._retriesFromGoogleT == 2)
                ChangeRetryFromGoogle(3, 2);
            else if (data._retriesFromGoogleT == 3)
                ChangeRetryFromGoogle(3, 3);
            else if (data._retriesFromGoogleT == 5)
                ChangeRetryFromGoogle(3, 5);


            if (data._timetoliveT == 10)
                ChangeTimeToLive(3, 10);
            else if (data._timetoliveT == 20)
                ChangeTimeToLive(3, 20);
            else if (data._timetoliveT == 30)
                ChangeTimeToLive(3, 30);

            if (data._delayWhileIdleT == 1)
                ChangeDelayWhileIdel(3, 1);
            else if (data._delayWhileIdleT == 0)
                ChangeDelayWhileIdel(3, 0);
        }
    });
}

function LoadPushMsgSetting(type) {
    if (type === 1) {
        PushMsgprimary();
    } else if (type === 2) {
        PushMsgsecondary();
    } else if (type === 3) {
        PushMsgternory();
    }
}


function editPushMsgprimary() {
    var s = './editpushnotificationsetting';

    if (strcmpPushMsg(document.getElementById("_ip").value, "") === 0) {
        alert("Please enter IP");
        return;
    }
    if (strcmpPushMsg(document.getElementById("_port").value, "") === 0) {

        alert("Please enter Port number");
        return;
    }
    if (strcmpPushMsg(document.getElementById("_gcmurl").value, "") === 0) {

        alert("Please enter GCM URL");
        return;
    }
    if (strcmpPushMsg(document.getElementById("_apikey").value, "") === 0) {

        alert("Please enter API Key");
        return;
    }
    if (strcmpPushMsg(document.getElementById("_googleSenderKey").value, "") === 0) {

        alert("Please enter google sender key");
        return;
    }
    if (strcmpPushMsg(document.getElementById("_googleSenderKey").value, "") === 0) {

        alert("Please enter google sender key");
        return;
    }
    if (strcmpPushMsg(document.getElementById("_className").value, "") === 0) {

        alert("Please enter Implementation class name");
        return;
    }
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#PushMsgprimaryform").serialize(),
        success: function (data) {
            if (strcmpPushMsg(data._result, "error") == 0) {
                $('#save-PushMsg-gateway-primary-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4PushMsgSetting("<span><font color=red>" + data._message + "</font></span>");
            } else if (strcmpPushMsg(data._result, "success") == 0) {
                $('#save-PushMsg-gateway-primary-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4PushMsgSetting("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function editPushMsgsecondary() {
    var s = './editiphonepushsettings';

    if (strcmpPushMsg(document.getElementById("_ipS").value, "") === 0) {
        alert("Please enter IP");
        return;
    }
    if (strcmpPushMsg(document.getElementById("_portS").value, "") === 0) {

        alert("Please enter Port number");
        return;
    }
    if (strcmpPushMsg(document.getElementById("_bundleID").value, "") === 0) {

        alert("Please enter GCM URL");
        return;
    }

    if (strcmpPushMsg(document.getElementById("_certpassowrd").value, "") === 0) {

        alert("Please enter pfx password");
        return;
    }

    if (strcmpPushMsg(document.getElementById("_classNameS").value, "") === 0) {

        alert("Please enter Implementation class name");
        return;
    }

    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#PushMsgecondaryform").serialize(),
        success: function (data) {
            if (strcmpPushMsg(data._result, "error") == 0) {
                $('#save-PushMsg-gateway-secondary-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4PushMsgSetting("<span><font color=red>" + data._message + "</font></span>");
            } else if (strcmpPushMsg(data._result, "success") == 0) {
                $('#save-PushMsg-gateway-secondary-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4PushMsgSetting("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}


function editPushMsgTernory() {
    var s = './editwebpushnotificationsetting';

    if (strcmpPushMsg(document.getElementById("_ipT").value, "") === 0) {
        alert("Please enter IP");
        return;
    }
    if (strcmpPushMsg(document.getElementById("_portT").value, "") === 0) {

        alert("Please enter Port number");
        return;
    }
    if (strcmpPushMsg(document.getElementById("_gcmurlT").value, "") === 0) {

        alert("Please enter GCM URL");
        return;
    }
    if (strcmpPushMsg(document.getElementById("_apikeyT").value, "") === 0) {

        alert("Please enter API Key");
        return;
    }
    if (strcmpPushMsg(document.getElementById("_googleSenderKeyT").value, "") === 0) {

        alert("Please enter google sender key");
        return;
    }
    if (strcmpPushMsg(document.getElementById("_googleSenderKeyT").value, "") === 0) {

        alert("Please enter google sender key");
        return;
    }
    if (strcmpPushMsg(document.getElementById("_classNameT").value, "") === 0) {

        alert("Please enter Implementation class name");
        return;
    }
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#WebPushMsgternoryform").serialize(),
        success: function (data) {
            if (strcmpPushMsg(data._result, "error") == 0) {
                $('#save-PushMsg-gateway-ternory-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4PushMsgSetting("<span><font color=red>" + data._message + "</font></span>");
            } else if (strcmpPushMsg(data._result, "success") == 0) {
                $('#save-PushMsg-gateway-ternory-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4PushMsgSetting("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function EditPushMsgSetting(type) {
    if (type == 1) {
        editPushMsgprimary();
    } else if (type == 2) {
        editPushMsgsecondary();
    } else if (type == 3) {
        editPushMsgTernory();
    }
}

function LoadTestPushMsgConnectionUI(type) {
    if (type == 1)
        $("#testPushMsgPrimary").modal();
    else if (type == 2)
        $("#testPushMsgSecondary").modal();
    else if (type == 3)
        $("#testPushMsgTernoy").modal();
}

function testconnectionPushMsgprimary() {

    var s = './testconnection';

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#testPushMsgPrimaryForm").serialize(),
        success: function (data) {
             
         
            if (strcmpPushMsg(data._result, "error") === 0) {
      
                $('#test-PushMsg-primary-configuration-resultGatway').html("<span><font color=red>" + data._message + "</font></span></small>");
            } else if (strcmpPushMsg(data._result, "success") === 0) {
     
                $('#test-PushMsg-primary-configuration-resultGatway').html("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function testconnectionPushMsgsecondary() {

    var s = './testconnection';

    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#testPushMsgSecondaryForm").serialize(),
        success: function (data) {
            if (strcmpPushMsg(data._result, "error") == 0) {
                $('#test-PushMsg-secondary-configuration-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            } else if (strcmpPushMsg(data._result, "success") == 0) {
                $('#test-PushMsg-secondary-configuration-result').html("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}


function sendCustomizePush() {
     
      var val=document.getElementById('_Pushmsg').value;
    
    var val1=document.getElementById('_Pushtype').value;
 
    var val2=document.getElementById('_groupname').value;
      
   var s = './sendCustomisePush?_Pushmsg=' +val + "&_Pushtype=" + val1+ "&_groupname=" + val2;
  //  var s = './sendCustomisePush';

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#sendCustomizePushForm").serialize(),
        success: function (data) {
          
            
            if (strcmpPushMsg(data._result, "error") == 0) {
                $('#test-PushMsg-primary-configuration-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            } else if (strcmpPushMsg(data._result, "success") == 0) {
                $('#test-PushMsg-primary-configuration-result').html("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}








function testconnectionPushMsgternory() {

    var s = './testconnection';

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#testPushMsgTernoyForm").serialize(),
        success: function (data) {

            if (strcmpPushMsg(data._result, "error") == 0) {
                $('#test-PushMsg-ternory-configuration-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            } else if (strcmpPushMsg(data._result, "success") == 0) {
                $('#test-PushMsg-ternory-configuration-result').html("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}


function TestPushMsgConnection(type) {
    if (type == 1) {
        testconnectionPushMsgprimary();
    } else if (type == 2) {
        testconnectionPushMsgsecondary();

    } else if (type == 3) {
        testconnectionPushMsgternory();

    }
}

function MessageLengthPushMsg(preference, count) {
    if (preference == 1) {
        if (count == 1) {
//              alert(count);
            $('#_messgeLength').val("1");
            $('#_messgeLength-primary-PushMsg').html("No");
        } else if (count == 0) {
//            alert(count);
            $('#_messgeLength').val("0");
            $('#_messgeLength-primary-PushMsg').html("Yes");
        }

    } else if (preference == 2) {
        if (count == 1) {
//              alert(count);
            $('#_messgeLengthS').val("1");
            $('#_messgeLengthS-secondary-PushMsg').html("No");
        } else if (count == 0) {
//            alert(count);
            $('#_messgeLengthS').val("0");
            $('#_messgeLengthS-secondary-PushMsg').html("Yes");
        }

    }
}

//function CertificateFileUpload() {
//
//    $('#buttonUploadEAD').attr("disabled", true);
//  var s = './editpushnotificationsetting?_password=' + document.getElementById('_certpassowrd').value;
//    $.ajaxFileUpload({
//        type: 'POST',
//        fileElementId: 'fileXMLToUploadEAD',
//        url: s,
//        dataType: 'json',
//        success: function(data,status) {
//            alert("hi");
//            if (strcmpPushMsg(data._result,"error") == 0) {
//                Alert4PushMsgSetting("<span><font color=blue>" + data._message + "</font></span>");
//            } else if (strcmpPushMsg(data._result, "success") == 0) {
//              Alert4PushMsgSetting("<span><font color=blue>" + data._message + "</font></span>");
//            }
//        },
//        error: function(data, status, e)
//        {
//            alert(e);
//        }
//    });
//}


function CertificateFileUpload() {

    $('#buttonUploadEAD').attr("disabled", true);

    var s = './editiphonepushsettings?_password=' + document.getElementById('_certpassowrd').value;
    $.ajaxFileUpload({
        type: 'POST',
        fileElementId: 'fileXMLToUploadEAD',
        url: s,
        dataType: 'json',
        success: function (data, status) {

            if (strcmpPushMsg(data._result, "error") == 0) {
                Alert4PushMsgSetting("<span><font color=blue>" + data._message + "</font></span>");


            } else if (strcmpPushMsg(data._result, "success") == 0) {
                Alert4PushMsgSetting("<span><font color=blue>" + data._message + "</font></span>");
            }


        },
        error: function (data, status, e)
        {
            //alert(e);
        }
    });
}
//PushwatchSaveSettings
function PushwatchSaveSettings() {
    var val=document.getElementById('_groupId').value;
  
    var val1=document.getElementById('_type').value;
   

    $('#savePushWatchSecondaryBut').attr("disabled", true);
  
    var s = './savePushWatch?_groupId=' +val + "&_type=" + val1;
    $.ajaxFileUpload({
        type: 'POST',
        fileElementId: 'fileXMLToUploadEADpush',
        url: s,
        dataType: 'json',
        success: function (data, status) {

            if (strcmpPushMsg(data._result, "error") == 0) {
                Alert4PushMsgSetting("<span><font color=blue>" + data._message + "</font></span>");


            } else if (strcmpPushMsg(data._result, "success") == 0) {
                Alert4PushMsgSetting("<span><font color=blue>" + data._message + "</font></span>");
            }


        },
        error: function (data, status, e)
        {
            alert(e);
        }
    });
}


function getDeviceCounts()
{
//     var val=document.getElementById('_groupnameExp').value;
//    var val1=document.getElementById('_Pushtypeexp').value;
//    $('#savePushWatchSecondaryBut').attr("disabled", true);
     var s = './getDeviceCounts';
     $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#testPushMsgPrimaryForm").serialize(),
        success: function (data) {
            if (strcmpPushMsg(data._result, "error") === 0) {
             
            } else if (strcmpPushMsg(data._result, "success") === 0) {
//              var table = document.createElement("tableDevToken");
//               var row1 = table.insertRow(0);
//               var cell1 = row1.insertCell(0);
//               var cell2 = row1.insertCell(1);
//               var cell3 = row1.insertCell(2);
//              cell1.innerHTML = "Pramod"+data._androidDeviceCount;
//              cell2.innerHTML ="Chaudhari"+ data._iosdevicecount;
//              cell3.innerHTML = "SNHBNS"+data.webpushDeviceCount;
              var table = document.createElement('table');
               var tr = document.createElement('tr');   
    var td1 = document.createElement('td');
    var td2 = document.createElement('td');
    var text1 = document.createTextNode('Text1');
    var text2 = document.createTextNode('Text2');
    td1.appendChild(text1);
    td2.appendChild(text2);
    tr.appendChild(td1);
    tr.appendChild(td2);
    table.appendChild(tr);
    
     var dvTable = document.getElementById("deviceTokenByType");
    
           //  dvTable.innerHTML = "pramodd";
//             $('#deviceTokenByType').html(table);
//             $('#deviceTokenByType').show();
           dvTable.appendChild(table);
                
            }
        }
    });
    
    
}



function exportDeviceToken() {
    var val=document.getElementById('_groupnameExp').value;
    var val1=document.getElementById('_Pushtypeexp').value;
    $('#savePushWatchSecondaryBut').attr("disabled", true);
    var s = './exportDeviceTokens?_groupId=' +val + "&_type=" + val1;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#testPushMsgPrimaryForm").serialize(),
        success: function (data) {
            if (strcmpPushMsg(data._result, "error") === 0) {
               Alert4PushMsgSetting("<span><font color=red>" + data._message + "</font></span>");
            } else if (strcmpPushMsg(data._result, "success") === 0) {
             JSONToCSVConvertor(data._message,"DEVICE TOKENS",true);
             
            }
        }
    });
   
}



function PushwatchFileUpload() {
    var val=document.getElementById('_groupId').value;
    var val1=document.getElementById('_type').value;
    $('#buttonUploadEADPush').attr("disabled", true);
  
    var s = './loadPushWatch?_groupId=' +val + "&_type=" + val1;
    $.ajaxFileUpload({
        type: 'POST',
        fileElementId: 'fileXMLToUploadEADpush',
        url: s,
        dataType: 'json',
        success: function (data, status) {

            if (strcmpPushMsg(data._result, "error") == 0) {
                Alert4PushMsgSetting("<span><font color=blue>" + data._message + "</font></span>");


            } else if (strcmpPushMsg(data._result, "success") == 0) {
                Alert4PushMsgSetting("<span><font color=blue>" + data._message + "</font></span>");
            }


        },
        error: function (data, status, e)
        {
            alert(e);
        }
    });
}

