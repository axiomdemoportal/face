/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function strcmpEX(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4EasyLogin(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function (result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}
function generateEasyLoginTable() {
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var val3 = encodeURIComponent(document.getElementById('startdate').value);
    var val4 = encodeURIComponent(document.getElementById('enddate').value);
    var val5 = encodeURIComponent(document.getElementById('_searchtext').value);
    var ele = document.getElementById("REPORT");
    document.getElementById("graph1").innerHTML="";
//    var ele = document.getElementById("refreshButton");
//    ele.style.display = "block";
    //var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    //var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
//    if (document.getElementById('_searchtext').value.length == 0) {
//         Alert4EasyLogin("<span><font color=red>Please enter username!!!</font></span>");
//        return;
//    }
    if (document.getElementById('startdate').value.length == 0 || document.getElementById('enddate').value.length == 0) {
         Alert4EasyLogin("<span><font color=red>Date Range is not selected!!!</font></span>");
        return;
    }
    pleaseWaitDiv.modal();
    ele.style.display = "block";
    $('#usreasyLogin a[href="#usreasyLoginChart"]').tab('show');
    var day_data = DonutChart(val3, val4, val5);
    
    Morris.Donut({
        element: 'graph',
        data: day_data
    });
    var day_data = BarChartDemo(val3, val4, val5);
    if(typeof day_data=="undefined")
    {
         pleaseWaitDiv.modal('hide');
         return;
    }
    Morris.Bar({
        element: 'graph1',
        data: day_data,
        xkey: 'label',
        ykeys: ['value'],
        labels: ['value'],
        barColors: function (type) {
            if (type === 'bar') {
                return '#0066CC';
            }
            else {
                return '#0066CC';
            }
        }
    });
    pleaseWaitDiv.modal();
    var s = './easyloginreporttable.jsp?&_startdate=' + val3 + '&_enddate=' + val4+ '&_searchtext=' + val5;
    
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#auth_table_main').html(data);
             pleaseWaitDiv.modal('hide');
            
        }
        
    });
    //pleaseWaitDiv.modal('hide');
}
function DonutChart(val3, val4, val5) {
    var s = './ELDonutChart?_startDate=' + val3 + "&_endDate=" + val4+ '&_searchtext=' + val5;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
   
//    if(strcmpEX(myJsonObj._result),"error")
//    {
//        alert("You dont have access to execute this operation");
//       
//        return;
//    }
    
    return myJsonObj;
}
function BarChartDemo(val3, val4, val5) {
    var s = './ELBarChart?_startDate=' + val3 + "&_endDate=" + val4+ '&_searchtext=' + val5; 
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    
    return myJsonObj;
}

// for system level report
function generateSystemEasyLoginTable() {
    
    var val3 = encodeURIComponent(document.getElementById('sysstartdate').value);
    var val4 = encodeURIComponent(document.getElementById('sysenddate').value);
    var ele = document.getElementById("SYSREPORT");
    document.getElementById("sysgraph1").innerHTML="";
    
    if (document.getElementById('sysstartdate').value.length == 0 || document.getElementById('sysenddate').value.length == 0) {
         Alert4EasyLogin("<span><font color=red>Date Range is not selected!!!</font></span>");
        return;
    }
    ele.style.display = "block";
    $('#syseasylogintab a[href="#syseasyloginchart"]').tab('show');
    var day_data = SysDonutChart(val3, val4);
    Morris.Donut({
        element: 'sysgraph',
        data: day_data
    });
    var day_data = SysBarChart(val3, val4);
    Morris.Bar({
        element: 'sysgraph1',
        data: day_data,
        xkey: 'label',
        ykeys: ['value'],
        labels: ['value'],
        barColors: function (type) {
            if (type === 'bar') {
                return '#0066CC';
            }
            else {
                return '#0066CC';
            }
        }
    });
  
    var s = './easyloginsystemtable.jsp?&_startdate=' + val3 + '&_enddate=' + val4;
    
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#easyLoginSys_table_main').html(data);   
        }      
    });
}

//session level report
function generateSessionEasyLoginTable() {
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var val3 = encodeURIComponent(document.getElementById('sessionstartdate').value);
    var val4 = encodeURIComponent(document.getElementById('sessionenddate').value);
    var val1 = document.getElementById('_statusName').value;
    var ele = document.getElementById("SESSIONREPORT");
    document.getElementById("sessiongraph1").innerHTML="";    
    if (document.getElementById('sessionstartdate').value.length == 0 || document.getElementById('sessionenddate').value.length == 0) {
         Alert4EasyLogin("<span><font color=red>Date Range is not selected!!!</font></span>");
        return;
    }
    pleaseWaitDiv.modal();
    ele.style.display = "block";
    $('#sessioneasylogintab a[href="#sessioneasyloginchart"]').tab('show');
    var day_data = SessionDonutChart(val3, val4, val1);
    if(typeof day_data=="undefined")
    {
         pleaseWaitDiv.modal('hide');
         return;
    }
    Morris.Donut({
        element: 'sessiongraph',
        data: day_data
    });
    var day_data = SessionBarChart(val3, val4, val1);
    Morris.Bar({
        element: 'sessiongraph1',
        data: day_data,
        xkey: 'label',
        ykeys: ['value'],
        labels: ['value'],
        barColors: function (type) {
            if (type === 'bar') {
                return '#0066CC';
            }
            else {
                return '#0066CC';
            }
        }
    });
  
    var s = './easyloginsessiontable.jsp?&_startdate=' + val3 + '&_enddate=' + val4+'&_searchStatus='+ val1;
    
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#easyLoginSession_table_main').html(data);  
            pleaseWaitDiv.modal('hide');
        }      
    });
}

function SysDonutChart(val3, val4) {
    var s = './SysELDonutChart?_startDate=' + val3 + "&_endDate=" + val4;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}
function SysBarChart(val3, val4) {
    var s = './SysELBarChart?_startDate=' + val3 + "&_endDate=" + val4; 
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}

// session chart
function SessionDonutChart(val3, val4, val1) {
    var s = './SessionELDonutChart?_startDate=' + val3 + "&_endDate=" + val4+'&_searchStatus='+ val1;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
//     if(strcmpEX(myJsonObj._result),"error")
//    {
//        alert("You dont have access to execute this operation");
//       
//        return;
//    }
   
    return myJsonObj;
}
function SessionBarChart(val3, val4, val1) {
    var s = './SessionELBarChart?_startDate=' + val3 + "&_endDate=" + val4+'&_searchStatus='+ val1; 
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}
function downloadeasycheckinReport(type, startdate, enddate) {
    var s = './DownloadEasyCheckInReport?_reporttype=' + type + "&_startDate=" + startdate + "&_endDate=" + enddate;
    window.location.href = s;
    return false;
}
function downloadeasysessionReport(type, startdate, enddate) {
    var s = './DownloadEasySessionReport?_reporttype=' + type + "&_startDate=" + startdate + "&_endDate=" + enddate;
    window.location.href = s;
    return false;
}