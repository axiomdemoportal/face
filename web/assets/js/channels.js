function Alert4Channel(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
    //end here
    }
    });
}

function strcmpChannel(a, b)
{
    return (a<b?-1:(a>b?1:0));
}

function RefreshChannels() {
    window.location.href = "./channels.jsp";
}


function addchannel(){
    $('#buttonAddChannel').attr("disabled", true);
    var s = './addchannel';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#addchannelForm").serialize(),
        success: function(data) {
            if ( strcmpChannel(data._result,"error") == 0 ) {
                $('#addchannel-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                //Alert4Channel("<span><font color=red>" + data._message + "</font></span>");
                $('#buttonAddChannel').attr("disabled", false);
            }
            else if ( strcmpChannel(data._result,"success") == 0 ) {
                $('#addchannel-result').html("<span><font color=blue>" + data._message + "</font></span>");
                //Alert4Channel("<span><font color=blue>" + data._message + "</font></span>");
                window.setTimeout(RefreshChannels, 3000);
            }
        }
    }); 
}

function loadEditChannelDetails(_chid){
    var s = './getchannel?_chid='+encodeURIComponent(_chid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if ( strcmpChannel(data._result,"success") == 0 ) {
                $('#idEditChannelName').html(data._name);
                $("#_ch_nameE").val(data._name);
                $("#_ch_virtual_pathE").val(data._vpath);
                $("#_channelidE").val(data._id);
                $("#_ch_statusE").val(data._status);
                $("#_rem_loginidE").val(data._raloginid);
                $("#_rem_passwordE").val(data._rapassword);
                $("#_rem_expiryminE").val(data._rem_expirymin);
                $("#_rem_passwordEC").val(data._rapassword);
                $('#editchannel-result').html("");
                $("#editChannel").modal();
            } else {
                Alert4Channel("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}




function editChannel(){

    $('#buttonEditChannel').attr("disabled", true);
    var s = './editchannel';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editchannelForm").serialize(),
        success: function(data) {
            if ( strcmpChannel(data._result,"error") == 0 ) {
                $('#editchannel-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#buttonEditChannel').attr("disabled", false);
                //Alert4Channel("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpChannel(data._result,"success") == 0 ) {
                $('#editchannel-result').html("<span><font color=blue>" + data._message + "</font></span>");
                //this.delay(1000);
                window.setTimeout(RefreshChannels, 3000);
                //Alert4Channel("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    }); 
}



function getauxillary(){
    var s = './getauxillary';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#").serialize(),
        success: function(data) {
            if ( strcmpChannel(data._result,"error") == 0 ) {
                $('#addchannel-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }
            else if ( strcmpChannel(data._result,"success") == 0 ) {
                $('#addchannel-result').html("<span><font color=blue>" + data._message + "</font></span>");
                
            }
        }
    }); 
}

function ChangeChannelRemoteAccess(cid,flag,uidiv){
    var s = './changeremoteaccess?_channelid='+encodeURIComponent(cid) + "&_ch_rem_status="+flag;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if ( strcmpChannel(data._result,"error") == 0 ) {
                Alert4Channel("<span><font color=red>" + data._message + "</font></span>");
            //$('#addchannel-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }
            else if ( strcmpChannel(data._result,"success") == 0 ) {
                //$('#addchannel-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Channel("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#'+uidiv;
                $(uiToChange).html(data._value);                
            }
        }
    });
}

function ChangeChannelStatus(channelId,status,uidiv){
    var s = './changestatus?_ch_status='+status+ '&_channelid='+encodeURIComponent(channelId);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if ( strcmpChannel(data._result,"error") == 0 ) {
                Alert4Channel("<span><font color=red>" + data._message + "</font></span>");
            //$('#addchannel-result').html("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpChannel(data._result,"success") == 0 ) {
                Alert4Channel("<span><font color=blue>" + data._message + "</font></span>");
                //$('#addchannel-result').html("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#'+uidiv;
                $(uiToChange).html(data._value);                
            }
        }
    });
}

//sachin
function ChangeSource(preference) {
    //1 for enabled
    //0 for disabled
    if (preference == 1) {
        $('#_datasource').val("1");
        $('#_data-source').html("Internal Source");
    } else if (preference == 0) {
        $('#_datasource').val("0");
        $('#_data-source').html("External Source");
    }

}

//function loadchannelsettings() {
//    var s = './loadchannelsettings';
//
//    $.ajax({
//        type: 'GET',
//        url: s,
//        dataType: 'json',
//        success: function(data) {
//
//
//            $('#_ipSource').val(data._ipSource);
//            $('#_portSource').val(data._portSource);
//            $('#_phoneNumber').val(data._phoneNumber);
//            if (data._sslSource === true) {
//                $('#_sslSource').val("true");
//            } else {
//                $('#_sslSource').val("false");
//            }
//            if (data._datasource === true)
//            {
//                ChangeSource(1);
//            }
//            else {
//                ChangeSource(o);
//            }
//
//
//            $('#_databaseNameSource').val(data._databaseNameSource);
//            $('#_tableNameSource').val(data._tableNameSource);
//            $('#_userIdSource').val(data._userIdSource);
//            $('#_passwordSource').val(data._passwordSource);
//
//        }
//    });
//}

function loadchannelsettings() {
    var s = './loadchannelsettings';

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {


            $('#_ipSource').val(data._ipSource);
            $('#_portSource').val(data._portSource);
            $('#_phoneNumber').val(data._phoneNumber);
            if (data._sslSource === true) {
                $('#_sslSource').val("true");
            } else {
                $('#_sslSource').val("false");
            }
            if (data._datasource === true)
            {
                ChangeSource(1);
            }
            else {
                ChangeSource(0);
            }


            $('#_databaseNameSource').val(data._databaseNameSource);
            $('#_tableNameSource').val(data._tableNameSource);
            $('#_userIdSource').val(data._userIdSource);
            $('#_passwordSource').val(data._passwordSource);
            
            $('#_operatorname').val(data._operatorname);
            $('#_mobile').val(data._mobile);
            $('#_mail').val(data._mail);    

        }
    });
}

function testConnection() {
    var s = './sourceConnectiontest';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#datasourceform").serialize(),
        success: function(data) {
            if (strcmpChannel(data._result, "error") == 0) {
                Alert4Channel("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpChannel(data._result, "success") == 0) {
                Alert4Channel("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function editChannelsettings() {
    var s = './editChannelSettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#datasourceform").serialize(),
        success: function(data) {
            if (strcmpChannel(data._result, "error") == 0) {
                Alert4Channel("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpChannel(data._result, "success") == 0) {
                Alert4Channel("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}