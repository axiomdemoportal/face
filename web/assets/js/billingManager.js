function strcmpBillingManager(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function Alert4BillingManager(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
        }
    });
}

function allowBillingManager(value, _div, disp) {
    $('#_allowBillingManager').val(value);
    $(_div).html(disp);
}
//function selectCurrency(value, _div, disp) {
//    $('#_currancyType').val(value);
//    $(_div).html(disp);
//}
//swotp
function allowBillingManagerSWOTP(value, _div, disp) {
    $('#_allowBillingManagerSWOTP').val(value);
    $(_div).html(disp);
}
function selectBillTypeSWOTP(value, _div, disp) {
    $('#_selectBillTypeSWOTP').val(value);
    $(_div).html(disp);
}
function selectBillingDurationSWOTP(value, _div, disp) {
    $('#_selectBillingDurationSWOTP').val(value);
    $(_div).html(disp);
}
function selectAlertBeforeSWOTP(value, _div, disp) {
    $('#_selectAlertBeforeSWOTP').val(value);
    $(_div).html(disp);
}
//hwotp
function allowBillingManagerHWOTP(value, _div, disp) {
    $('#_allowBillingManagerHWOTP').val(value);
    $(_div).html(disp);
}
function selectBillTypeHWOTP(value, _div, disp) {
    $('#_selectBillTypeHWOTP').val(value);
    $(_div).html(disp);
}
function selectBillingDurationHWOTP(value, _div, disp) {
    $('#_selectBillingDurationHWOTP').val(value);
    $(_div).html(disp);
}
function selectAlertBeforeHWOTP(value, _div, disp) {
    $('#_selectAlertBeforeHWOTP').val(value);
    $(_div).html(disp);
}
//SWPKIOTP
function allowBillingManagerSWPKIOTP(value, _div, disp) {
    $('#_allowBillingManagerSWPKIOTP').val(value);
    $(_div).html(disp);
}
function selectBillTypeSWPKIOTP(value, _div, disp) {
    $('#_selectBillTypeSWPKIOTP').val(value);
    $(_div).html(disp);
}
function selectBillingDurationSWPKIOTP(value, _div, disp) {
    $('#_selectBillingDurationSWPKIOTP').val(value);
    $(_div).html(disp);
}
function selectAlertBeforeSWPKIOTP(value, _div, disp) {
    $('#_selectAlertBeforeSWPKIOTP').val(value);
    $(_div).html(disp);
}
//HWPKIOTP
function allowBillingManagerHWPKIOTP(value, _div, disp) {
    $('#_allowBillingManagerHWPKIOTP').val(value);
    $(_div).html(disp);
}
function selectBillTypeHWPKIOTP(value, _div, disp) {
    $('#_selectBillTypeHWPKIOTP').val(value);
    $(_div).html(disp);
}
function selectBillingDurationHWPKIOTP(value, _div, disp) {
    $('#_selectBillingDurationHWPKIOTP').val(value);
    $(_div).html(disp);
}
function selectAlertBeforeHWPKIOTP(value, _div, disp) {
    $('#_selectAlertBeforeHWPKIOTP').val(value);
    $(_div).html(disp);
}
//certificate

function allowBillingManagerCertificate(value, _div, disp) {
    $('#_allowBillingManagerCertificate').val(value);
    $(_div).html(disp);
}
function selectBillTypeCertificate(value, _div, disp) {
    $('#_selectBillTypeCertificate').val(value);
    $(_div).html(disp);
}
function selectBillingDurationCertificate(value, _div, disp) {
    $('#_selectBillingDurationCertificate').val(value);
    $(_div).html(disp);
}
function selectAlertBeforeCertificate(value, _div, disp) {
    $('#_selectAlertBeforeCertificate').val(value);
    $(_div).html(disp);
}
//OOBOTP
function allowBillingManagerOOBOTP(value, _div, disp) {
    $('#_allowBillingManagerOOBOTP').val(value);
    $(_div).html(disp);
}
function selectBillTypeOOBOTP(value, _div, disp) {
    $('#_selectBillTypeOOBOTP').val(value);
    $(_div).html(disp);
}
function selectBillingDurationOOBOTP(value, _div, disp) {
    $('#_selectBillingDurationOOBOTP').val(value);
    $(_div).html(disp);
}
function selectAlertBeforeOOBOTP(value, _div, disp) {
    $('#_selectAlertBeforeOOBOTP').val(value);
    $(_div).html(disp);
}
//function selectTXperDayOOBOTP(value, _div, disp) {
//    $('#_txPerDayOOBOTP').val(value);
//    $(_div).html(disp);
//}

//alert
function allowRepeatationOfCharacterB(value, _div, disp) {
//    alert(value);
    $('#_allowRepetationCharacterB').val(value);
    $(_div).html(disp);
}
function AlertViaB(value, _div, disp) {
    $('#_gatewayTypeB').val(value);
    $(_div).html(disp);
}
function AlertAttemptsB(value, _div, disp) {
    $('#_alertAttemptB').val(value);
    $(_div).html(disp);
}
function forwarTemplateRequestforAlertB() {
    var val = document.getElementById('_templateName').value;
    var s = './getTemplateType.jsp?_tid=' + val;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#AddNewSchedulerForm").serialize(),
        success: function(data) {
            if (strcmpPasswordPolicy(data._result, "1") == 0) {
                var s = './TemplateMobileEdit.jsp?_greetingsType=1&_tid=' + val;
                window.open(s);
            } else if (strcmpPasswordPolicy(data._result, "2") == 0) {
                var s = './TemplateEmailEdit.jsp?_greetingsType=1&_tid=' + val;
                window.open(s);
            }
        }
    });
}

function LoadBillingManagerSettings() {
    var s = './loadbillingmanagersettings';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
             
            $('#_allowBillingManager').val(data._allowBillingManager);
            if (data._allowBillingManager == true)
                allowBillingManager(1, '#_allowBillingManager_div', 'Enabled');
            else if (data._allowBillingManager == false)
                allowBillingManager(0, '#_allowBillingManager_div', 'Disabled');

            $('#_currancyType').val(data._currancyType);
//            if (data._currancyType == 1)
//                selectCurrency(1, '#_currancyType_div', 'Doller');
//            else if (data._currancyType == 2)
//                selectCurrency(2, '#_currancyType_div', 'INR');
//            else if (data._currancyType == 3)
//                selectCurrency(3, '#_currancyType_div', 'YEN');
//            else if (data._currancyType == 4)
//                selectCurrency(4, '#_currancyType_div', 'EURO');

            $('#_allowBillingManagerSWOTP').val(data._allowBillingManagerSWOTP);
            if (data._allowBillingManagerSWOTP == true)
                allowBillingManagerSWOTP(1, '#_allowBillingManagerSWOTP_div', 'Enabled');
            else if (data._allowBillingManagerSWOTP == false)
                allowBillingManagerSWOTP(0, '#_allowBillingManagerSWOTP_div', 'Disabled');

            $('#_selectBillTypeSWOTP').val(data._selectBillTypeSWOTP);
            if (data._selectBillTypeSWOTP == 1)
                selectBillTypeSWOTP(1, '#_selectBillTypeSWOTP_div', 'Bill/Transaction');
            else if (data._selectBillTypeSWOTP == 2)
                selectBillTypeSWOTP(2, '#_selectBillTypeSWOTP_div', 'Subscription');
            else if (data._selectBillTypeSWOTP == 3)
                selectBillTypeSWOTP(3, '#_selectBillTypeSWOTP_div', 'One Time');

            $('#_selectBillingDurationSWOTP').val(data._selectBillingDurationSWOTP);
            if (data._selectBillingDurationSWOTP == 1)
                selectBillingDurationSWOTP(1, '#_selectBillingDurationSWOTP_div', 'Monthly');
            else if (data._selectBillingDurationSWOTP == 2)
                selectBillingDurationSWOTP(2, '#_selectBillingDurationSWOTP_div', 'Yearly');
            else if (data._selectBillingDurationSWOTP == 3)
                selectBillingDurationSWOTP(3, '#_selectBillingDurationSWOTP_div', 'Quarterly');

            $('#_selectAlertBeforeSWOTP').val(data._selectAlertBeforeSWOTP);
            if (data._selectAlertBeforeSWOTP == 1)
                selectAlertBeforeSWOTP(1, '#_selectAlertBeforeSWOTP_div', '1 Week');
            else if (data._selectAlertBeforeSWOTP == 2)
                selectAlertBeforeSWOTP(2, '#_selectAlertBeforeSWOTP_div', '2 Week');
            else if (data._selectAlertBeforeSWOTP == 3)
                selectAlertBeforeSWOTP(3, '#_selectAlertBeforeSWOTP_div', '3 Week');
            
            $('#_costSWOTP').val(data._costSWOTP);


            //HWOTP
            
            $('#_allowBillingManagerHWOTP').val(data._allowBillingManagerHWOTP);
            if (data._allowBillingManagerHWOTP == true)
                allowBillingManagerHWOTP(1, '#_allowBillingManagerHWOTP_div', 'Enabled');
            else if (data._allowBillingManagerHWOTP == false)
                allowBillingManagerHWOTP(0, '#_allowBillingManagerHWOTP_div', 'Disabled');

            $('#_selectBillTypeHWOTP').val(data._selectBillTypeHWOTP);
            if (data._selectBillTypeHWOTP == 1)
                selectBillTypeHWOTP(1, '#_selectBillTypeHWOTP_div', 'Bill/Transaction');
            else if (data._selectBillTypeHWOTP == 2)
                selectBillTypeHWOTP(2, '#_selectBillTypeHWOTP_div', 'Subscription');
            else if (data._selectBillTypeHWOTP == 3)
                selectBillTypeHWOTP(3, '#_selectBillTypeHWOTP_div', 'One Time');

            $('#_selectBillingDurationHWOTP').val(data._selectBillingDurationHWOTP);
            if (data._selectBillingDurationHWOTP == 1)
                selectBillingDurationHWOTP(1, '#_selectBillingDurationHWOTP_div', 'Monthly');
            else if (data._selectBillingDurationHWOTP == 2)
                selectBillingDurationHWOTP(2, '#_selectBillingDurationHWOTP_div', 'Yearly');
            else if (data._selectBillingDurationHWOTP == 3)
                selectBillingDurationHWOTP(3, '#_selectBillingDurationHWOTP_div', 'Quarterly');

            $('#_selectAlertBeforeHWOTP').val(data._selectAlertBeforeHWOTP);
            if (data._selectAlertBeforeHWOTP == 1)
                selectAlertBeforeHWOTP(1, '#_selectAlertBeforeHWOTP_div', '1 Week');
            else if (data._selectAlertBeforeHWOTP == 2)
                selectAlertBeforeHWOTP(2, '#_selectAlertBeforeHWOTP_div', '2 Week');
            else if (data._selectAlertBeforeHWOTP == 3)
                selectAlertBeforeHWOTP(3, '#_selectAlertBeforeHWOTP_div', '3 Week');
            $('#_costHWOTP').val(data._costHWOTP);

            //SWPKIOTP
            $('#_allowBillingManagerSWPKIOTP').val(data._allowBillingManagerSWPKIOTP);
            if (data._allowBillingManagerSWPKIOTP == true)
                allowBillingManagerSWPKIOTP(1, '#_allowBillingManagerSWPKIOTP_div', 'Enabled');
            else if (data._allowBillingManagerSWPKIOTP == false)
                allowBillingManagerSWPKIOTP(0, '#_allowBillingManagerSWPKIOTP_div', 'Disabled');

            $('#_selectBillTypeSWPKIOTP').val(data._selectBillTypeSWPKIOTP);
            if (data._selectBillTypeSWPKIOTP == 1)
                selectBillTypeSWPKIOTP(1, '#_selectBillTypeSWPKIOTP_div', 'Bill/Transaction');
            else if (data._selectBillTypeSWPKIOTP == 2)
                selectBillTypeSWPKIOTP(2, '#_selectBillTypeSWPKIOTP_div', 'Subscription');
            else if (data._selectBillTypeSWPKIOTP == 3)
                selectBillTypeSWPKIOTP(3, '#_selectBillTypeSWPKIOTP_div', 'One Time');

            $('#_selectBillingDurationSWPKIOTP').val(data._selectBillingDurationSWPKIOTP);
            if (data._selectBillingDurationSWPKIOTP == 1)
                selectBillingDurationSWPKIOTP(1, '#_selectBillingDurationSWPKIOTP_div', 'Monthly');
            else if (data._selectBillingDurationSWPKIOTP == 2)
                selectBillingDurationSWPKIOTP(2, '#_selectBillingDurationSWPKIOTP_div', 'Yearly');
            else if (data._selectBillingDurationSWPKIOTP == 3)
                selectBillingDurationSWPKIOTP(3, '#_selectBillingDurationSWPKIOTP_div', 'Quarterly');

            $('#_selectAlertBeforeSWPKIOTP').val(data._selectAlertBeforeSWPKIOTP);
            if (data._selectAlertBeforeSWPKIOTP == 1)
                selectAlertBeforeSWPKIOTP(1, '#_selectAlertBeforeSWPKIOTP_div', '1 Week');
            else if (data._selectAlertBeforeSWPKIOTP == 2)
                selectAlertBeforeSWPKIOTP(2, '#_selectAlertBeforeSWPKIOTP_div', '2 Week');
            else if (data._selectAlertBeforeSWPKIOTP == 3)
                selectAlertBeforeSWPKIOTP(3, '#_selectAlertBeforeSWPKIOTP_div', '3 Week');
            $('#_costSWPKIOTP').val(data._costSWPKIOTP);


            //HWPKIOTP
            $('#_allowBillingManagerHWPKIOTP').val(data._allowBillingManagerHWPKIOTP);
            if (data._allowBillingManagerHWPKIOTP == true)
                allowBillingManagerHWPKIOTP(1, '#_allowBillingManagerHWPKIOTP_div', 'Enabled');
            else if (data._allowBillingManagerHWPKIOTP == false)
                allowBillingManagerHWPKIOTP(0, '#_allowBillingManagerHWPKIOTP_div', 'Disabled');

            $('#_selectBillTypeHWPKIOTP').val(data._selectBillTypeHWPKIOTP);
            if (data._selectBillTypeHWPKIOTP == 1)
                selectBillTypeHWPKIOTP(1, '#_selectBillTypeHWPKIOTP_div', 'Bill/Transaction');
            else if (data._selectBillTypeHWPKIOTP == 2)
                selectBillTypeHWPKIOTP(2, '#_selectBillTypeHWPKIOTP_div', 'Subscription');
            else if (data._selectBillTypeHWPKIOTP == 3)
                selectBillTypeHWPKIOTP(3, '#_selectBillTypeHWPKIOTP_div', 'One Time');

            $('#_selectBillingDurationHWPKIOTP').val(data._selectBillingDurationHWPKIOTP);
            if (data._selectBillingDurationHWPKIOTP == 1)
                selectBillingDurationHWPKIOTP(1, '#_selectBillingDurationHWPKIOTP_div', 'Monthly');
            else if (data._selectBillingDurationHWPKIOTP == 2)
                selectBillingDurationHWPKIOTP(2, '#_selectBillingDurationHWPKIOTP_div', 'Yearly');
            else if (data._selectBillingDurationHWPKIOTP == 3)
                selectBillingDurationHWPKIOTP(3, '#_selectBillingDurationHWPKIOTP_div', 'Quarterly');

            $('#_selectAlertBeforeHWPKIOTP').val(data._selectAlertBeforeHWPKIOTP);
            if (data._selectAlertBeforeHWPKIOTP == 1)
                selectAlertBeforeHWPKIOTP(1, '#_selectAlertBeforeHWPKIOTP_div', '1 Week');
            else if (data._selectAlertBeforeHWPKIOTP == 2)
                selectAlertBeforeHWPKIOTP(2, '#_selectAlertBeforeHWPKIOTP_div', '2 Week');
            else if (data._selectAlertBeforeHWPKIOTP == 3)
                selectAlertBeforeHWPKIOTP(3, '#_selectAlertBeforeHWPKIOTP_div', '3 Week');
            $('#_costHWPKIOTP').val(data._costHWPKIOTP);

            //CERTIFICATE
            $('#_allowBillingManagerCertificate').val(data._allowBillingManagerCertificate);
            if (data._allowBillingManagerCertificate == true)
                allowBillingManagerCertificate(1, '#_allowBillingManagerCertificate_div', 'Enabled');
            else if (data._allowBillingManagerCertificate == false)
                allowBillingManagerCertificate(0, '#_allowBillingManagerCertificate_div', 'Disabled');

            $('#_selectBillTypeCertificate').val(data._selectBillTypeCertificate);
            if (data._selectBillTypeCertificate == 1)
                selectBillTypeCertificate(1, '#_selectBillTypeCertificate_div', 'Bill/Transaction');
            else if (data._selectBillTypeCertificate == 2)
                selectBillTypeCertificate(2, '#_selectBillTypeCertificate_div', 'Subscription');
            else if (data._selectBillTypeCertificate == 3)
                selectBillTypeCertificate(3, '#_selectBillTypeCertificate_div', 'One Time');

            $('#_selectBillingDurationCertificate').val(data._selectBillingDurationCertificate);
            if (data._selectBillingDurationCertificate == 1)
                selectBillingDurationCertificate(1, '#_selectBillingDurationCertificate_div', 'Monthly');
            else if (data._selectBillingDurationCertificate == 2)
                selectBillingDurationCertificate(2, '#_selectBillingDurationCertificate_div', 'Yearly');
            else if (data._selectBillingDurationCertificate == 3)
                selectBillingDurationCertificate(3, '#_selectBillingDurationCertificate_div', 'Quarterly');

            $('#_selectAlertBeforeCertificate').val(data._selectAlertBeforeCertificate);
            if (data._selectAlertBeforeCertificate == 1)
                selectAlertBeforeCertificate(1, '#_selectAlertBeforeCertificate_div', '1 Week');
            else if (data._selectAlertBeforeCertificate == 2)
                selectAlertBeforeCertificate(2, '#_selectAlertBeforeCertificate_div', '2 Week');
            else if (data._selectAlertBeforeCertificate == 3)
                selectAlertBeforeCertificate(3, '#_selectAlertBeforeCertificate_div', '3 Week');
            $('#_costCertificate').val(data._costCertificate);


            //OOBOTP
           
            $('#_allowBillingManagerOOBOTP').val(data._allowBillingManagerOOBOTP);
            if (data._allowBillingManagerOOBOTP == true)
                allowBillingManagerOOBOTP(1, '#_allowBillingManagerOOBOTP_div', 'Enabled');
            else if (data._allowBillingManagerOOBOTP == false)
                allowBillingManagerOOBOTP(0, '#_allowBillingManagerOOBOTP_div', 'Disabled');

            $('#_selectBillTypeOOBOTP').val(data._selectBillTypeOOBOTP);
            if (data._selectBillTypeOOBOTP == 1)
                selectBillTypeOOBOTP(1, '#_selectBillTypeOOBOTP_div', 'Bill/Transaction');
            else if (data._selectBillTypeOOBOTP == 2)
                selectBillTypeOOBOTP(2, '#_selectBillTypeOOBOTP_div', 'Subscription');
            else if (data._selectBillTypeOOBOTP == 3)
                selectBillTypeOOBOTP(3, '#_selectBillTypeOOBOTP_div', 'One Time');

            $('#_selectBillingDurationOOBOTP').val(data._selectBillingDurationOOBOTP);
            if (data._selectBillingDurationOOBOTP == 1)
                selectBillingDurationOOBOTP(1, '#_selectBillingDurationOOBOTP_div', 'Monthly');
            else if (data._selectBillingDurationOOBOTP == 2)
                selectBillingDurationOOBOTP(2, '#_selectBillingDurationOOBOTP_div', 'Yearly');
            else if (data._selectBillingDurationOOBOTP == 3)
                selectBillingDurationOOBOTP(3, '#_selectBillingDurationOOBOTP_div', 'Quarterly');

            $('#_selectAlertBeforeOOBOTP').val(data._selectAlertBeforeOOBOTP);
            if (data._selectAlertBeforeOOBOTP == 1)
                selectAlertBeforeOOBOTP(1, '#_selectAlertBeforeOOBOTP_div', '1 Week');
            else if (data._selectAlertBeforeOOBOTP == 2)
                selectAlertBeforeOOBOTP(2, '#_selectAlertBeforeOOBOTP_div', '2 Week');
            else if (data._selectAlertBeforeOOBOTP == 3)
                selectAlertBeforeOOBOTP(3, '#_selectAlertBeforeOOBOTP_div', '3 Week');
            
             $('#_txPerDayOOBOTP').val(data._txPerDayOOBOTP);
//             alert(data._txPerDayOOBOTP);
//            if (data._txPerDayOOBOTP == 3)
//                selectTXperDayOOBOTP(3, '#_txPerDayOOBOTP_div', '3 Tx/Day');
//            else if (data._txPerDayOOBOTP == 5)
//                selectTXperDayOOBOTP(5, '#_txPerDayOOBOTP_div', '5 Tx/Day');
//            else if (data._txPerDayOOBOTP == 7)
//                selectTXperDayOOBOTP(7, '#_txPerDayOOBOTP_div', '7 Tx/Day');
            
//             alert(data._txPerDayOOBOTP);
            
            $('#_costOOBOTP').val(data._costOOBOTP);
            $('#_costperTxOOBOTP').val(data._costperTxOOBOTP);
//            $('#_thresholdCostOOBOTP').val(data._thresholdCostOOBOTP);
            
               $('#_allowRepetationCharacterB').val(data._allowRepetationCharacterB);
            if (data._allowRepetationCharacterB == true)
                allowRepeatationOfCharacterB(1, '#_allowRepetationCharacterB_div', 'Enabled');
            else if (data._allowRepetationCharacterB == false)
                allowRepeatationOfCharacterB(0, '#_allowRepetationCharacterB_div', 'Disabled');
            
               $('#_gatewayTypeB').val(data._gatewayTypeB);
            if (data._gatewayTypeB == 1)
                AlertViaB(1, '#_gatewayTypeB_div', 'SMS');
            else if (data._gatewayTypeB == 2)
                AlertViaB(2, '#_gatewayTypeB_div', 'USSD');
            else if (data._gatewayTypeB == 3)
                AlertViaB(3, '#_gatewayTypeB_div', 'VOICE');
              else if (data._gatewayTypeB == 4)
                AlertViaB(4, '#_gatewayTypeB_div', 'EMAIL');
              else if (data._gatewayTypeB == 5)
                AlertViaB(5, '#_gatewayTypeB_div', 'FAX');
              else if (data._gatewayTypeB == 18)
                AlertViaB(18, '#_gatewayTypeB_div', 'ANDROIDPUSH');
              else if (data._gatewayTypeB == 19)
                AlertViaB(19, '#_gatewayTypeB_div', 'IPHONEPUSH');
            
               $('#_alertAttemptB').val(data._alertAttemptB);
            if (data._alertAttemptB == 1)
                AlertAttemptsB(1, '#_alertAttemptB_div', '1 Attempt');
            else if (data._alertAttemptB == 2)
                AlertAttemptsB(2, '#_alertAttemptB_div', '2 Attempt');
            else if (data._alertAttemptB == 3)
                AlertAttemptsB(3, '#_alertAttemptB_div', '3 Attempt');
            
            

        }
    });
}

function editBillingManagerSettings() {
    var s = './editbillingmanagersettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#billingManagersettingsform").serialize(),
        success: function(data) {
            if (strcmpBillingManager(data._result, "error") == 0) {
//                $('#save-password-settings-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4BillingManager("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpBillingManager(data._result, "success") == 0) {
//                $('#save-password-settings-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4BillingManager("<span><font color=blue>" + data._message + "</font></span>");

            }
        }
    });
}

//nilesh 13-10


