function strcmp(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function get_time_zone_offset( ) {
    var current_date = new Date();
    return -current_date.getTimezoneOffset() / 60;
}

function Alert4login(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}

//function Login(){
//    var s = './login';
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#loginForm").serialize(),
//        success: function(data) {
//            if ( strcmp(data._result,"error") == 0 ) {
//                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//            }else if ( strcmp(data._result,"Blocked") == 0 ) {
//                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//               
//            }else if(strcmp(data._result,"1sLogin") == 0){
////                Alert4login("<span><font color=blue>" + data._message + "</font></span>");
//                      $('#ChangePassword').modal('show');
//            }else if ( strcmp(data._result,"success") == 0 ) {
//                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                window.location.href = data._url;                
//            }
//        }
//    }); 
//}

//function ForgotPassword(){
//    var s = './forgotpw';
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#forgotPasswdForm").serialize(),
//        success: function(data) {
//            if ( strcmp(data._result,"error") == 0 ) {
//                $('#forgot-password-result').html("<span><font color=red>" + data._message + "</font></span>");
//            }
//            else if ( strcmp(data._result,"success") == 0 ) {
//                $('#forgot-password-result').html("<span><font color=blue>" + data._message + "</font></span>");                
//            }
//        }
//    });
//}


function TellTimezone() {
    var s = './timezone?_gmt=' + get_time_zone_offset();
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            //alert(data._result);
        }
    });
}

function RefreshforgotPassword() {
    window.location.href = "./index.jsp";
}

function forgotpassword() {
    var s = './forgotpassword';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#loginForm").serialize(),
        success: function(data) {
            if (strcmp(data._result, "error") == 0) {
                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }
            else if (strcmp(data._result, "success") == 0) {
                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.setTimeout(RefreshforgotPassword, 5000);
            }
        }
    });
}

function changeoperatorpasswordAfter1stLogin() {
    var s = './changeoprpassword';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#ChangePasswordform").serialize(),
        success: function(data) {
            if (strcmp(data._result, "error") == 0) {
                $('#Password-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#_oldPassword').val("");
                $('#_newPassword').val("");
                $('#_confirmPassword').val("");
                //ClearAddOperatorForm();
            }
            else if (strcmp(data._result, "success") == 0) {
                $('#Password-result').html("<span><font color=blue>" + data._message + "</font></span>");
                //ClearAddOperatorForm();
                $('#passwordChange').attr("disabled", true);
                window.setTimeout(RefreshLogout, 5000);
            }
        }
    });
}



function Login() {
     var s = './login';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#loginForm").serialize(),
        success: function(data) {
            if (strcmp(data._result, "error") == 0) {
                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            } else if (strcmp(data._result, "Blocked") == 0) {
                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
          } else if (strcmp(data._result, "1sLogin") == 0) {
//                Alert4login("<span><font color=blue>" + data._message + "</font></span>");
                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#ChangePassword').modal('show');
            } else if (strcmp(data._result, "passwordexpire") == 0) {
//                      Alert4login("<span><font color=blue>" + data._message + "</font></span>");
                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#ChangePassword').modal('show');
            } else if (strcmp(data._result, "success") == 0) {
               
                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.location.href = data._url;
            }
        }
    });
}


function geoLogin() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, error, geo_options);
    } else {
        alert("Geolocation services are not supported by your web browser.");
    }
    function success(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        var altitude = position.coords.altitude;
        var accuracy = position.coords.accuracy;
     Login(latitude,longitude);
    }
    function error(error) {
        alert("Unable to retrieve your location due to " + error.code + " : " + error.message);
    }
    ;
    var geo_options = {
        enableHighAccuracy: true,
        maximumAge: 30000,
        timeout: 27000
    };
}


function geoFindMeOLD() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, error, geo_options);
    } else {
//        alert("Geolocation services are not supported by your web browser.");
    }
    function success(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        var altitude = position.coords.altitude;
        var accuracy = position.coords.accuracy;
        //do something with above position thing e.g. below
       var val = './addgeotracking?latitude='+latitude+'&longitude='+longitude;
//          window.location.href = val ;
    $.ajax({
        type: 'POST',
        url: val,
//        dataType: 'json',
//        data: $("#loginForm").serialize(),
        success: function(data) {
        }
    });
//        alert('I am here! lat:' + latitude + ' and long : ' + longitude);
    }
    function error(error) {
//        alert("Unable to retrieve your location due to " + error.code + " : " + error.message);
    }
    ;
    var geo_options = {
        enableHighAccuracy: true,
        maximumAge: 30000,
        timeout: 27000
    };
}

function RefreshLogout() {
    window.location.href = "./signout.jsp";
}

function RefreshLogoutDBsetting() {
    window.location.href = "./showConfig.jsp";    
}
function DbsettingLogin() {
    if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
        $('#login-result').html("<span><font color=red>" + 'Internet Explorer is Not Supported' + "</font></span></small>");
        return;
    }

    var s = './dbsettingLogin';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#loginForm").serialize(),
        success: function(data) {
            if (strcmp(data._result, "error") == 0) {
                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            } else if (strcmp(data._result, "Blocked") == 0) {
                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");

            } else if (strcmp(data._result, "1sLogin") == 0) {
//                Alert4login("<span><font color=blue>" + data._message + "</font></span>");
                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#ChangePassword').modal('show');
            } else if (strcmp(data._result, "passwordexpire") == 0) {
//                      Alert4login("<span><font color=blue>" + data._message + "</font></span>");
                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#ChangePassword').modal('show');
            } else if (strcmp(data._result, "success") == 0) {
                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.location.href = data._url;
            }
        }
    });
}
function savedbsetting() {
    var s = './savesetting';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#SaveForm").serialize(),
       success: function(data) {
           alert( data._message);
            if (strcmp(data._result, "error") == 0) {
                $('#dbsetting-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            } else if (strcmp(data._result, "success") == 0) {
                $('#dbsetting-result').html("<span><font color=blue>" + data._message + "</font></span>");
                window.location.href = data._url;
            }
        }
    });
}