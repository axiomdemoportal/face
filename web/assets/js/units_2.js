
function strcmpUnits(a, b)
{   
    return (a<b?-1:(a>b?1:0));  
}

function Alert4Units(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
        //end here
        }
    });
}


function RefreshUnitsList() {
    window.location.href = "./UnitsList.jsp"    
}

function editUnits(){
    var s = './edittemplates';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#messageedittemplateform").serialize(),
        success: function(data) {
            if ( strcmpTemplates(data._result,"error") == 0 ) {
                $('#edittemplateM-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                  Alert4Templates(data._message);
            }
            else if ( strcmpTemplates(data._result,"success") == 0 ) {
                $('#edittemplateM-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Templates(data._message);
                $('#buttonEditMessage').attr("disabled", true);
                
            }
        }
    }); 
}




function removeUnits(_tid){
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './removeUnit?_tid='+_tid;
   
            $.ajax({
                type: 'GET',    
                url: s,
                dataType: 'json',
                success: function(data) {
                    if ( strcmpUnits(data._result,"success") == 0 ) {
                        Alert4Units("<span><font color=blue>" + data._message + "</font></span>");                        
                        window.setTimeout(RefreshUnitsList, 2000);
              
                    } else {
                        Alert4Units("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}










function ChangeType(value) {
    
    if ( value === 1) {
        $('#_types').val("1");
        $('#_type-primary-sms').html("Mobile");
    } 
    else if(value === 2){
        $('#_types').val("2");
        $('#_type-primary-sms').html("Email");
            
    }
    
        
}

function changeUnitsStatus(_tid, _status,uidiv) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {

            var s = './changeUnitsStatus?_status=' + _status + '&_unitId=' + encodeURIComponent(_tid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpUnits(data._result, "success") == 0) {
                        Alert4Units("<span><font color=blue>" + data._message + "</font></span>");
                        var uiToChange = '#' + uidiv;
                        $(uiToChange).html(data._value);

                    } else {
                        Alert4Units("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}



function addNewUnit(){
    $('#addnewUnitSubmitBut').attr("disabled", true);
    var s = './addUnit';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#AddNewUnitForm").serialize(),
        success: function(data) {
            if ( strcmpUnits(data._result,"error") == 0 ) {
                $('#add-new-unit-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#addnewUnitSubmitBut').attr("disabled", false);
                //ClearAddOperatorForm();
                //window.setTimeout(RefreshOperators, 3000);
            }
            else if ( strcmpUnits(data._result,"success") == 0 ) {
                $('#aadd-new-unit-result').html("<span><font color=blue>" + data._message + "</font></span>");
                //ClearAddOperatorForm();
                window.setTimeout(RefreshUnitsList, 3000);
            }
        }
    }); 
}



function loadEditUnitDetails(_unitName,_unitId){
$("#_unitNameE").val(_unitName);
$("#_unitId").val(_unitId);
$("#_oldunitNameE").val(_unitName);
$("#_thresholdLimitO").val(_thresholdLimitE);
$("#_thresholdLimitE").val(_thresholdLimitE);
 $("#editUnit").modal();
}

function restrictLocation(_unitName,_unitId){
$("#_unitNameE").val(_unitName);
$("#_unitId").val(_unitId);
$("#_oldunitNameE").val(_unitName);
$("#editUnit").modal();
}


function loadShiftUnitDetails(_unitName,_unitId){
$("#_oldunitNameS").val(_unitName);
$("#_unitIdS").val(_unitId);
//$("#_oldunitNameE").val(_unitName);
 $("#shiftUnit").modal();
}
function loadAssignTokens(_unitName,_unitId){
$("#_oldunitNameA").val(_unitName);
$("#_unitIdA").val(_unitId);
 $("#AssignTokens").modal();
}

function loadShiftTokensTokens(_unitName,_unitId){
$("#_oldunitNameT").val(_unitName);
$("#_unitIdT").val(_unitId);
 $("#ShiftFreeHWTokens").modal();
}


function editUnit(){
    $('#buttonEditUnitSubmit').attr("disabled", true);
    var s = './editUnit';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editUnitForm").serialize(),
        success: function(data) {
            if ( strcmpOpr(data._result,"error") == 0 ) {
                $('#editunit-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#buttonEditUnitSubmit').attr("disabled", false);
            //ClearEditOperatorForm();
            }
            else if ( strcmpOpr(data._result,"success") == 0 ) {
                $('#editunit-result').html("<span><font color=blue>" + data._message + "</font></span>");
                //ClearEditOperatorForm();
                window.setTimeout(RefreshUnitsList, 3000);
            }
        }
    });
}
function shiftUnit(){
    $('#buttonShiftUnitSubmit').attr("disabled", true);
    var s = './shiftUnitsOperators';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#shiftUnitForm").serialize(),
        success: function(data) {
            if ( strcmpUnits(data._result,"error") == 0 ) {
                $('#shiftunit-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#buttonEditUnitSubmit').attr("disabled", false);
            //ClearEditOperatorForm();
            }
            else if ( strcmpUnits(data._result,"success") == 0 ) {
                $('#shiftunit-result').html("<span><font color=blue>" + data._message + "</font></span>");
                //ClearEditOperatorForm();
                window.setTimeout(RefreshUnitsList, 3000);
            }
        }
    });
}

function UploadFreeTokensFile() {
    $('#buttonUploadFreeTokens').attr("disabled", true);
    var s = './uploadTokensFile?_type='+1;
    $.ajaxFileUpload({
        fileElementId: 'fileFreeTokenToUploadEAD',
        url: s,
        dataType: 'json',
        success: function(data, status) {

            if (strcmpUnits(data.result, "error") == 0) {
                Alert4Units("<span><font color=red>" + data.message + "</font></span>");
            }
            else if (strcmpUnits(data.result, "success") == 0) {
                Alert4Units("<span><font color=blue>" + data.message + "</font></span>");
                $('#buttonUploadFreeTokens').attr("disabled", false);
            }
        },
        error: function(data, status, e)
        {
            alert(e);
        }
    });
}
function UploadSifftTokensFile() {
    $('#buttonUploadFreeTokensS').attr("disabled", true);
    var s = './uploadTokensFile?_type='+2;
    $.ajaxFileUpload({
        fileElementId: 'fileFreeTokenToUploadEADS',
        url: s,
        dataType: 'json',
        success: function(data, status) {

            if (strcmpUnits(data.result, "error") == 0) {
                Alert4Units("<span><font color=red>" + data.message + "</font></span>");
            }
            else if (strcmpUnits(data.result, "success") == 0) {
                Alert4Units("<span><font color=blue>" + data.message + "</font></span>");
                $('#buttonUploadFreeTokensS').attr("disabled", false);
            }
        },
        error: function(data, status, e)
        {
            alert(e);
        }
    });
}

function AssignTokenToUnit(){
    $('#buttonAssignTokenUnitSubmit').attr("disabled", true);
    var s = './assignTokenToUnit';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#AssignTokenForm").serialize(),
        success: function(data) {
            if ( strcmpUnits(data._result,"error") == 0 ) {
                $('#assign-unit-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#buttonAssignTokenUnitSubmit').attr("disabled", false);
            //ClearEditOperatorForm();
            }
            else if ( strcmpUnits(data._result,"success") == 0 ) {
                $('#assign-unit-result').html("<span><font color=blue>" + data._message + "</font></span>");
                //ClearEditOperatorForm();
                window.setTimeout(RefreshUnitsList, 3000);
            }
        }
    });
}
function ShiftTokenToUnit(){
    $('#buttonAssignTokenUnitSubmitS').attr("disabled", true);
    var s = './shiftTokensInUnits';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#ShiftTokenForm").serialize(),
        success: function(data) {
            if ( strcmpUnits(data._result,"error") == 0 ) {
                $('#assign-unit-resultS').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#buttonAssignTokenUnitSubmitS').attr("disabled", false);
            //ClearEditOperatorForm();
            }
            else if ( strcmpUnits(data._result,"success") == 0 ) {
                $('#assign-unit-resultS').html("<span><font color=blue>" + data._message + "</font></span>");
                //ClearEditOperatorForm();
                window.setTimeout(RefreshUnitsList, 3000);
            }
        }
    });
}