function Alert4PDFSign(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}
function strcmpPDFSign(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}


function signPDF() {

    document.getElementById("savemepost").value =document.getElementById("displayarea").innerHTML;

    var s = './pdfsigning';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#PDFSigning").serialize(),
        success: function(data) {
            if (strcmpPDFSign(data._result, "error") === 0) {
                $('#sign-pdf-result').html("<span><font color=red>" + data._message + "</font></span>");
//                $('#sign-pdf-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//                Alert4PDFSign("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpPDFSign(data._result, "success") === 0) {
                $('#sign-pdf-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                Alert4PDFSign("<span><font color=blue>" + data._message + "</font></span>");
//                alert(data._pdffilename);
                $('#download-signed-pdf').html("<a href=./downloadfiles?_pdffilename=" + data._pdffilename + ">Download Signed PDF</a>");

            }
        }
    });
}


function PDFUpload() {
    $('#buttonUploadEAD').attr("disabled", true);
    var s = './uploadpdfsigndoc';
    $.ajaxFileUpload({
        type: 'POST',
        fileElementId: 'filePdfToUpload',
        url: s,
        dataType: 'json',
        success: function(data) {

            if (strcmpPDFSign(data.result, "error") === 0) {
                Alert4PDFSign("<span><font color=red>" + data.message + "</font></span>");
            }
            else if (strcmpPDFSign(data.result, "success") === 0) {
                $('#_filename').val(data.filename);
                Alert4PDFSign("<span><font color=blue>" + data.message + "</font></span>");
                document.getElementById("signeraddition").style.display = 'block';
//                document.getElementById("change").style.display = "none";
                var msg = "<h4><font color=blue>"+ data.filename + " File Uploaded !!!"+"</font></h4>";
                document.getElementById("uploadingfile").innerHTML = msg;
            }
        }
    });
}

