function listSettings()
{
    var s = './tableSettings.jsp';
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#settings_table_main1').html(data);
        }
    });
}

function strcmpChannel(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4Channel(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function (result) {

        if (result == false) {
        } else {
            //end here
        }
    });
}

$(document).ready(function () {

    $('#_selectedType').change(function () {
        var selectedValue = $(this).val();
        if (selectedValue === '0')
        {
            document.getElementById("website").style.display = 'none';
            document.getElementById("portmonitor").style.display = 'none';
            document.getElementById("DNS").style.display = 'none';
            document.getElementById("ping").style.display = 'none';
            document.getElementById("ssl").style.display = 'none';
        }
        else if (selectedValue === '1')
        {
            document.getElementById("portmonitor").style.display = 'none';
            document.getElementById("DNS").style.display = 'none';
            document.getElementById("ping").style.display = 'none';
            document.getElementById("ssl").style.display = 'none';
            var ele = document.getElementById("website");
            ele.style.display = "block";
        }
        else if (selectedValue === '4')
        {
            document.getElementById("website").style.display = 'none';
            document.getElementById("portmonitor").style.display = 'none';
            document.getElementById("ping").style.display = 'none';
            document.getElementById("ssl").style.display = 'none';
            var ele = document.getElementById("DNS");
            ele.style.display = "block";
        }
        else if (selectedValue === '6')
        {
            document.getElementById("website").style.display = 'none';
            document.getElementById("DNS").style.display = 'none';
            document.getElementById("ping").style.display = 'none';
            document.getElementById("ssl").style.display = 'none';
            var ele = document.getElementById("portmonitor");
            ele.style.display = "block";
        }
        else if (selectedValue === '9')
        {
            document.getElementById("website").style.display = 'none';
            document.getElementById("portmonitor").style.display = 'none';
            document.getElementById("DNS").style.display = 'none';
            document.getElementById("ssl").style.display = 'none';
            var ele = document.getElementById("ping");
            ele.style.display = "block";
        }
        else if (selectedValue === '10')
        {
            document.getElementById("website").style.display = 'none';
            document.getElementById("portmonitor").style.display = 'none';
            document.getElementById("DNS").style.display = 'none';
            document.getElementById("ping").style.display = 'none';
            var ele = document.getElementById("ssl");
            ele.style.display = "block";
        }
    });


    $('#_servicesalert').change(function () {
        var selectedValue = $(this).val();
        if (selectedValue === 'yes')
        {
            var ele = document.getElementById("monitors");
            ele.style.display = "block";
        }
        else if (selectedValue === 'no')
        {
            document.getElementById("monitors").style.display = 'none';
        }
    });




});





function showList()
{
    var value1 = $('#_selectedRoles').val();

//    alert(value1);

    var s = './listOperators.jsp?_selectedRole=' + value1;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#operator_table_main1').html(data);
        }
    });

}

function showLists(edit, gname)
{
    var value1 = $('#_selectedRoles').val();
    var s = './listOperators.jsp?_selectedRole=' + value1 + "&gname=" + gname + "&edit=" + edit;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#operator_table_main1').html(data);
        }
    });

}
function Updatemonitor(_cid) {

    var cid = "#_sel" + _cid;
    var isChecked = $(cid).is(':checked');
    var currentList = $('#_operatorsList').val();

    if (isChecked == false) {
        var cidS = "" + _cid + ",";
        currentList = currentList.replace(cidS, "");
    } else if (isChecked === true)
    {
        var cidS = "" + _cid + ",";
        var n = currentList.search(cidS);
//        alert(cidS);
        if (n == -1) {
            currentList = currentList + cidS;
        }
    }

    $('#_operatorsList').val(currentList);



    //var ele = document.getElementById("ChangeContactStatus");
    //ele.style.display = "block"; 

    //document.getElementById('body1').style.display = "block"; 



    //$("#DeleteMultipleContacts").show();



    //alert($('#_deleteContactsList').val());
}

function addSettings()
{
    var value1 = $('#_selectedRoles').val();
//    alert(value1);
    var s = './AddMonitorSettings?_selectedRoles=' + value1;
    $.ajax({
        type: 'GET',
        url: s,
        data: $('#AddNewSchedulerForm,#operatorsForm').serialize(),
        success: function (data) {
            if (strcmpChannel(data._result, "error") == 0) {
                Alert4Channel("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpChannel(data._result, "success") == 0) {
                bootbox.confirm("<span><font color=blue><h2>" + data._message + "</h2></font></span>", function (result) {
                    window.setTimeout(window.location.href = "./listGroup.jsp", 10000);
                })
            }
        }
    });
}

function forwarTemplateRequestformonitors() {

    var val = document.getElementById('_templateName').value;
    alert(val);
    var s = './getTemplateByName.jsp?_tid=' + val;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#AddNewSettingsForm").serialize(),
        success: function (data) {
            if (strcmpChannel(data._result, "1") == 0) {
//                alert("hidknc");
                var s = './TemplateMobileEdit.jsp?_greetingsType=1&_tid=' + val;
                window.open(s);
//                window.location.href = s;
//                return false;
            } else if (strcmpChannel(data._result, "2") == 0) {
                var s = './TemplateEmailEdit.jsp?_greetingsType=1&_tid=' + val;
                window.open(s);
//                window.location.href = s;
//                return false;
            }

        }
    });
}


function importContactList()
{
    var value1 = $('#_deleteContactsList').val();
//    alert(value1)
}


function removeGroup(val)
{
    bootbox.confirm("<span><font color=red><h2> Are you sure ?</h2></font></span>", function (result) {
        if (result === true)
        {
            var s = './RemoveGroupM?_key=' + val;
            $.ajax({
                type: 'GET',
                url: s,
                success: function (data) {
                    if (strcmpChannel(data._result, "error") == 0) {
                        Alert4Channel("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpChannel(data._result, "success") == 0) {
                        bootbox.confirm("<span><font color=blue><h2>" + data._message + "</h2></font></span>", function (result) {
                            window.setTimeout(window.location.href = "./listGroup.jsp", 10000);
                        })
                    }
                }
            });
        }
//                    window.setTimeout(window.location.href = "./listGroup.jsp", 10000);
    })
}

function addMSettings(edit)
{
    var val = $('#_selectedAlert').val();
    var s = './AddMSettings?_selectedAlert=' + val + "&_edit=" + edit;
    $.ajax({
        type: 'POST',
        url: s,
        data: $('#AddNewSettingsForm').serialize(),
        success: function (data) {
            var dData=JSON.parse(data);
            if (strcmpChannel(dData._result, "error") == 0) {
                Alert4Channel("<span><font color=red>" + dData._message + "</font></span>");
            }
            else if (strcmpChannel(dData._result, "success") == 0) {
                bootbox.confirm("<span><font color=blue><h2>" + dData._message + "</h2></font></span>", function (result) {
                    window.setTimeout(window.location.href = "./listSettings.jsp", 10000);
                })
            }
        }
    });
}

function ChangeMonitorStatus(name, newstatus, oldstatus)
{
    var s = './ChangeSettingStatus?name=' + name + "&newstatus=" + newstatus + "&oldstatus=" + oldstatus;
    $.ajax({
        type: 'POST',
        url: s,
        success: function (data) {
            var dData=JSON.parse(data);
            if (strcmpChannel(dData._result, "error") === 0) {
               
                Alert4Channel("<span><font color=red>" + dData._message + "</font></span>");
            }
            else if (strcmpChannel(dData._result, "success") == 0) {
                Alert4Channel("<span><font color=blue>" + dData._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(dData._value);
            }
        }
    });
}
function removeMonitorSetting(moniotorId)
{
    bootbox.confirm("<span><font color=red><h2> Are you sure ?</h2></font></span>", function (result) {
        if (result === true)
        {
            var s = './RemoveMonitorSetting?moniotorId=' + moniotorId;
            $.ajax({
                type: 'GET',
                url: s,
                success: function (data) {
                    if (strcmpChannel(data._result, "error") == 0) {
                        Alert4Channel("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpChannel(data._result, "success") == 0) {
                        bootbox.confirm("<span><font color=blue><h2>" + data._message + "</h2></font></span>", function (result) {
                            window.setTimeout(window.location.href = "./listSettings.jsp", 10000);
                        })
                    }
                }
            });
        }
//                    window.setTimeout(window.location.href = "./listGroup.jsp", 10000);
    });
}

function ReportPDF(name, type, checkfor)
{
    //alert(checkfor);
    var s = './DownloadReport?name=' + name + "&_reporttype=" + 0 + "&monitorType=" + type + "&ChechFor=" + checkfor;

    window.location.href = s;
    return false;
}

function ReportCSV(name, type, checkfor)
{
    var s = './DownloadReport?name=' + name + "&_reporttype=" + 1 + "&monitorType=" + type + "&ChechFor=" + checkfor;
    window.location.href = s;
    return false;

}

function startss(val)
{

    bootbox.confirm("<span><font color=red><h2> Are you sure ?</h2></font></span>", function (result) {
        if (result === true)
        {
            var s = './listSettings.jsp?val=' + val;
            $.ajax({
                type: 'GET',
                url: s,
                success: function (data) {
                    alert(val);
                }
            });
        }
//                    window.setTimeout(window.location.href = "./listGroup.jsp", 10000);
    });
}

function fresh()
{
    window.location.href = "./listSettings.jsp"
}

function scriptforimage(urlGet, value) {
//    alert(urlGet);
    var imagelink = "<image src='" + urlGet + "?AP=" + value + " '/>";
    $('#_imagescript').html(imagelink);
}

function getUniqueId(url) {
    if (url === '') {
        Alert4Channel("<span><font color=red>Please do setting for WebSeal.</font></span>");
        return;

    }
    var _webAddress = document.getElementById("_webAddress").value;
    if (_webAddress !== '') {
        var s = './GetUniqueId?_webAddress=' + _webAddress;
        $.ajax({
            type: 'GET',
            url: s,
            success: function (data) {
                if (strcmpChannel(data._result, "error") == 0) {
                    Alert4Channel("<span><font color=red>" + data._message + "</font></span>");
                }
                else if (strcmpChannel(data._result, "success") == 0) {
                    var imgScript = '<script>\n' +
                            'var img = document.createElement("IMG");\n' +
                            'img.src = "' + url + '?urlUniqueId=' + data._message + '";\n' +
                            'document.getElementById("imageDiv").appendChild(img);\n' +
                            '</script>';

                    document.getElementById("_imagescript").value = imgScript;
                    document.getElementById("_UniqueIdVal").value = data._message;

                }
            }
        });
    }
}
