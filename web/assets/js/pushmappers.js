

function strcmpPushmappers(a, b)
{   
    return (a<b?-1:(a>b?1:0));  
}
function Alert4Pushmappers(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
        //end here
        }
    });
}

function RefreshPushmappersList() {
    window.location.href = "./FXpushmappers.jsp?";    
}


function removePushmappers(_mapperId,_callerId){
    
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './removepushmapper?_mapperId='+_mapperId;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if ( strcmpPushmappers(data._result,"success") == 0 ) {
                        Alert4Pushmappers("<span><font color=blue>" + data._message + "</font></span>");                
                        var funcName = "RefreshPushmappersList2("+_callerId+")";
                        window.setTimeout(funcName, 3000);
                    //window.setTimeout(RefreshPushmappersList, 2000);
                    } else {
                        Alert4Pushmappers("<span><font color=red>" + data._message + "</font></span>");                
                    }
                }
            });
        }
    });
}


function loadEditPushmapperDetails(_mid){
    var s = './getpushmapper?_mapperid='+encodeURIComponent(_mid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpPushmappers(data._result,"success") == 0 ) {                
                $('#idEditPushMaperName').html('Edit ' + data._messagefomat);                                
                $("#_MapperId").val(data._mapperID);
                $("#_MessageformatE").val(data._messagefomat);
                $("#_templateNameE").val(data._templateName);
                $("#_templateTypeE").val(data._templateType);
                $("#_templateClassE").val(data._templateClass);
                $('#editPushMappers-result').html("");
                $("#editPushMappers").modal();
            } else {
                Alert4Pushmappers("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}

function RefreshPushmappersList2(_cid) {    
    window.location.href = "./FXpushmappers.jsp?_cid="+_cid;    
}

function editpushmappers(){
    var s = './editpushmapper';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editISOMessageForm").serialize(),
        success: function(data) {
            if ( strcmpPushmappers(data._result,"error") == 0 ) {
                $('#editPushMappers-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            //Alert4Pushmappers(data._message);
            }
            else if ( strcmpPushmappers(data._result,"success")==0 ) {
                $('#editPushMappers-result').html("<span><font color=blue>" + data._message + "</font></span>");
                $('#buttonEditPushMappers').attr("disabled", true);
                //window.setTimeout(RefreshPushmappersList2,2000);
                var callerid = $('#_CallerId').val();
                var funcName = "RefreshPushmappersList2("+callerid+")";
                window.setTimeout(funcName, 3000);
                
            }
        }
    }); 
}





function search() {
    var jsonData = $.ajax({
        type: 'POST',
        url: 'templatenames',
        dataType: "json",
        //        data: $("#homeForm").serialize(),
        async: false
    }).responseText;
    var dataset = jsonParse(jsonData);

    $(function() {
        $(".basicTypeahead").typeahead({
            source: dataset
            , 
            items: 15
        });
    });
}




//function removePushmapper(_callerid){
//    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
//        if (result == false) {
//        } else {
//            var s = './removepushmessage?_callerid='+_callerid;
//               $.ajax({
//                type: 'GET',    
//                url: s,
//                dataType: 'json',
//                success: function(data) {
//                    if ( strcmpPushmessages(data._result,"success") == 0 ) {
//                        Alert4Pushmessages("<span><font color=red>" + data._message + "</font></span>");                        
//                        window.setTimeout(RefreshPushmappersList, 2000);
//              
//                    } else {
//                        Alert4Pushmessages("<span><font color=red>" + data._message + "</font></span>");
//                    }
//                }
//            });
//        }
//    });
//}


function addpushmapper(){
    var s = './addpushmapper';
    $.ajax({
        type: 'POST',
        url:s,
        dataType: 'json',
        data: $("#addISOMessageForm").serialize(),
        success: function(data) {
            if ( strcmpPushmappers(data._result,"error") == 0 ) {
                $('#addPushMappers-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Pushmappers("<span><font color=red>" + data._message + "</font></span>");
                $('#buttonaddPushMappers').attr("disabled", false);
            }
            else if ( strcmpPushmappers(data._result,"success") == 0 ) {
                $('#addPushMappers-result').html("<span><font color=blue>" + data._message + "</font></span>");
                $('#buttonaddPushMappers').attr("disabled", true);
                var callerid = $('#_CallerId').val();
                var funcName = "RefreshPushmappersList2("+callerid+")";
                window.setTimeout(funcName, 2000);
            }
        }
    }); 
}

