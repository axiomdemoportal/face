function strcmpPushmessages(a, b)
{   
    return (a<b?-1:(a>b?1:0));  
}

function Alert4Pushmessages(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
    //end here
    }
    });
}

function RefreshPushMessageList() {
    window.location.href = "./FXpushmessages.jsp";    
}


//function removePushmessage(_callerid){
//    var s = './removepushmessage?_callerid='+_callerid;
//     $.ajax({
//        type: 'GET',
//        url: s,
//        dataType: 'json',
//        success: function(data) {
//            if ( strcmpPushmessages(data._result,"success") == 0 ) {
//                Alert4Pushmessages("<span><font color=red>" + data._message + "</font></span>");
//                window.setTimeout(RefreshPushMessageList, 2000);
//             } else {
//                Alert4Pushmessages("<span><font color=red>" + data._message + "</font></span>");
//                
//            }
//        }
//    });
//}


function loadEditPushmessageDetails(_callid){
    var s = './getpushmessage?_callid='+encodeURIComponent(_callid);
    
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
                 if ( strcmpPushmessages(data._result,"success") == 0 ) {
                      
                 
                $('#idEditTemplatepushName').html("Edit (" + data._name + ") FX Caller");
                $("#_epush_idE").val(data._callidE);
                $("#_epush_name").val(data._name);
                $("#_epush_ip").val(data._ip);
                $("#_epush_port").val(data._port);
                $("#_epush_status").val(data._status);
                $("#_epush_success").val(data._success);
                $("#_epush_failure").val(data._failure);
                $("#_epush_enforceremoteaccess").val(data._enforceremoteaccess);
                $('#editpushmessage-result').html("");
                $("#editPushMessage").modal();
            } else {
                Alert4Pushmessages("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}

function editpushmessage(){
    var s = './editpushmessages';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editPushmessageForm").serialize(),
        success: function(data) {
            if ( strcmpPushmessages(data._result,"error") == 0 ) {
                $('#editpushmessage-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                  Alert4Pushmessages(data._message);
            }
            else if ( strcmpPushmessages(data._result,"success")==0 ) {
                $('#editpushmessage-result').html("<span><font color=blue>" + data._message + "</font></span>");
                $('#buttonEditPushmessage').attr("disabled", true);
               window.setTimeout(RefreshPushMessageList, 2000);
            }
        }
    }); 
}





function removePushmessage(_callerid){
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './removepushmessage?_callerid='+_callerid;
               $.ajax({
                type: 'GET',    
                url: s,
                dataType: 'json',
                success: function(data) {
                    if ( strcmpPushmessages(data._result,"success") == 0 ) {
                        Alert4Pushmessages("<span><font color=red>" + data._message + "</font></span>");                        
                        window.setTimeout(RefreshPushMessageList, 2000);
              
                    } else {
                        Alert4Pushmessages("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}


function addpushmessage(){
    var s = './addpushmessages';
     $.ajax({
        type: 'POST',
        url:s,
        dataType: 'json',
        data: $("#addPushmessageForm").serialize(),
        success: function(data) {
            if ( strcmpPushmessages(data._result,"error") == 0 ) {
                $('#addpushmessage-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Pushmessages("<span><font color=red>" + data._message + "</font></span>");
                $('#buttonAddPushmessage').attr("disabled", false);
              }
            else if ( strcmpPushmessages(data._result,"success") == 0 ) {
                $('#addpushmessage-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                Alert4Pushmessages(data._message ");
                $('#buttonAddPushmessage').attr("disabled", true);
                 window.setTimeout(RefreshPushMessageList, 2000);
            }
        }
    }); 
}

