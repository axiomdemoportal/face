/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


function strcmpRadius(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4Radius(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}

function RefreshRadiusList() {
    window.location.href = "./radiusserver2.jsp";
}

function LoadRadiusSetting() {

    var s = './loadradiussettings?_type=' + 5 + '&_preference=' + 1;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {

//           alert(data._ldapValidate);

            $('#_ldapServerIp').val(data._ldapServerIp);
            $('#_ldapServerPort').val(data._ldapServerPort);
            $('#_ldapSearchinitials').val(data._ldapSearchInitial);
            $('#_ldapSearchPath').val(data._ldapSearchPath);
            
            $('#_ldapUsername').val(data._ldapUsername);
            $('#_ldapPassword').val(data._ldapPassword);
            $('#_radiusClientIp').val(data._radiusClientIp);
            $('#_radiusClientSecretkey').val(data._radiusClientSecretkey);
            

            if (data._authEnabled === true) {

                $('#_authEnabled').attr("checked", true);
                $('#_authIp').val(data._authIp);
                $('#_authPort').val(data._authPort);


            } else {
                ('#_authEnabled').attr("checked", false);
                $('#_accountIp').val(data._accountIp);
                $('#_accountPort').val(data._accountPort);
            }

            if (data._accountEnabled == false) {
                $('#_accountEnabled').attr("checked", false);
                $('#_authIp').val(data._authIp);
                $('#_authPort').val(data._authPort);
            } else {
                $('#_accountEnabled').attr("checked", true);
                $('#_accountIp').val(data._accountIp);
                $('#_accountPort').val(data._accountPort);

            }


            if (data._ldapValidate == false) {
                $('#_ldapValidate').val("axiom");
            } else if (data._ldapValidate == true) {
                $('#_ldapValidate').val("ldap");
            }
            
           

              }
    });
}


function editradiussettings() {
    if (strcmpRadius(document.getElementById("_authIp").value, "") == 0) {
         
         alert("Ip can not be null");
         return;
     }
     if (strcmpRadius(document.getElementById("_authPort").value, "") == 0) {
         
         alert("Port can not be null");
         return;
     }
     if (strcmpRadius(document.getElementById("_ldapServerIp").value, "") == 0) {
         
         alert("Ldap server ip can not be null");
         return;
     }
    if (strcmpRadius(document.getElementById("_ldapServerPort").value, "") == 0) {
         
         alert("Ldap server port can not be null");
         return;
     }
    
    if (strcmpRadius(document.getElementById("_ldapSearchinitials").value, "") == 0) {
         
         alert("Ldap Search initialis can not be null");
         return;
     }
        if (strcmpRadius(document.getElementById("_ldapSearchPath").value, "") == 0) {
         
         alert("Ldap Search path can not be null");
         return;
     }
    
    
    var s = './editradiussetting?_type=' + 5 + '&_preference=' + 1;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#primaryform").serialize(),
        success: function(data) {

            if (strcmpRadius(data._result, "error") == 0) {
                Alert4Radius("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpRadius(data._result, "success") == 0) {
                Alert4Radius("<span><font color=blue>" + data._message + "</font></span>");
//                window.setTimeout(RefreshRadiusList, 2000);

            }

        }
    }
    );
}




function loadClientDetails(_srno, _eclientdisplayname, authenticationtype, _radiusClientIp, _radiusClientSecretkey,_dayrestriction,_timefrom,_fromampm,timeto,toampm) {
    $('#srno').val(_srno);
    $('#_eclientdisplayname').val(_eclientdisplayname);
    $('#eauthenticationtype').val(authenticationtype);
    $('#_eradiusClientIp').val(_radiusClientIp);
    $('#_eradiusClientSecretkey').val(_radiusClientSecretkey);
    
    $('#edayrestriction').val(_dayrestriction);
    $('#_etimerange').val(_timefrom);
    $('#_etimefromampm').val(_fromampm);
    $('#_etotimerange').val(timeto);
    $('#_etimetoampm').val(toampm);

    $("#editRadiusClient").modal();
}



function ChangeradiusClientStatus(status, srno, uidiv) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './changeClientStatus?_status=' + status + '&srno=' + encodeURIComponent(srno);
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpRadius(data._result, "error") == 0) {
                        Alert4Radius("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpRadius(data._result, "success") == 0) {
                        Alert4Radius("<span><font color=blue>" + data._message + "</font></span>");
                        //                window.setTimeout(RefreshUsersList, 2000);
                        var uiToChange = '#' + uidiv;
                        $(uiToChange).html(data._value);

                    }
                }
            });
        }
    });

}


function removeRadiusClient(userid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './removeRadiusClient?srno=' + encodeURIComponent(userid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpRadius(data._result, "success") == 0) {
                        Alert4Radius("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshRadiusList, 2000);
                    } else {
                        Alert4Radius("<span><font color=red>" + data._message + "</font></span>");
                        window.setTimeout(RefreshRadiusList, 2000);
                    }
                }
            });
        }
    });
}


function addNewRadiusClient() {
    var s = './addRadiusClient?_type=' + 5 + '&_preference=' + 1;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#RadiusClientForm").serialize(),
        success: function(data) {

            if (strcmpRadius(data._result, "error") == 0) {
                Alert4Radius("<span><font color=red>" + data._message + "</font></span>");

                $('#_eclientdisplayname').val("");
                $('#eauthenticationtype').val("");
                $('#_eradiusClientIp').val("");
                $('#_eradiusClientSecretkey').val("");
                window.setTimeout(RefreshRadiusList, 2000);

            }
            else if (strcmpRadius(data._result, "success") == 0) {
                Alert4Radius("<span><font color=blue>" + data._message + "</font></span>");

                $('#_eclientdisplayname').val("");
                $('#eauthenticationtype').val("");
                $('#_eradiusClientIp').val("");
                $('#_eradiusClientSecretkey').val("");
                window.setTimeout(RefreshRadiusList, 2000);


            }

        }
    }
    );
}


function editRadiusClient() {
    var s = './editRadiusClient?_type=' + 5 + '&_preference=' + 1;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editRadiusClientForm").serialize(),
        success: function(data) {

            if (strcmpRadius(data._result, "error") == 0) {
                Alert4Radius("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpRadius(data._result, "success") == 0) {
                Alert4Radius("<span><font color=blue>" + data._message + "</font></span>");
                window.setTimeout(RefreshRadiusList, 2000);

            }

        }
    }
    );
}

function ChangeDayRestriction(value, disp) {
    $('#_dayRestriction').val(value);
    $('#_dayRestriction_div').html(disp);
} 
function ChangeTimeRestriction(type, value, disp) {
    if (type == 1) {
        $('#_timeFromRestriction').val(value);
        $('#_timeFromRestriction_div').html(disp);
    } else if (type == 2) {
        $('#_timeToRestriction').val(value);
        $('#_timeToRestriction_div').html(disp);
    }
}

function eChangeDayRestriction(value, disp) {
    $('#_edayRestriction').val(value);
    $('#_edayRestriction_div').html(disp);
} 
function eChangeTimeRestriction(type, value, disp) {
    if (type == 1) {
        $('#_etimeFromRestriction').val(value);
        $('#_etimeFromRestriction_div').html(disp);
    } else if (type == 2) {
        $('#_etimeToRestriction').val(value);
        $('#_etimeToRestriction_div').html(disp);
    }
}
