function strcmpUsers(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4Users(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}

function viewUserID(id) {
    bootbox.alert("<h3>" + id + "</h3>");
}

//changes by Mayuri for XpayBridge User Reports

function viewBankID(id) {
    bootbox.alert("<h3>" + id + "</h3>");
}
function viewCardNumber(id) {
    bootbox.alert("<h3>" + id + "</h3>");
}
//end
function RefreshResourceList() {
    window.location.href = "./resources.jsp";
}
function RefreshResourcList() {
    window.location.href = "./addresource.jsp";
}
function RefreshUsersList() {
    window.location.href = "./searchResource.jsp";
}


function OperatorAudits(_userid, _duration) {

    var s = './getuseraudits?_oprid=' + encodeURIComponent(_userid) + "&_duration=" + _duration + "&_format=pdf";
    window.location.href = s;
    return false;

}



function adduser() {

    $('#addUserButton').attr("disabled", true);


    var s = './adduser';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#addUserForm").serialize(),
        success: function(data) {
            if (strcmpUsers(data._result, "error") == 0) {
                $('#addUser-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#addUserButton').attr("disabled", false);
                //                $('#_Name').val("");
                //                $('#_Email').val("");
                //                $('#_Phone').val("");
            }
            else if (strcmpUsers(data._result, "success") == 0) {
                $('#addUser-result').html("<span><font color=blue>" + data._message + "</font></span>");
                $('#addUserButton').attr("disabled", false);
                //window.setTimeout(RefreshUsersList, 2000);
                $('#_Name').val("");
                $('#_Email').val("");
                $('#_Phone').val("");
            }
        }
    });
}

function edituser() {
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var s = './edituser';
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editUserForm").serialize(),
        success: function(data) {
            pleaseWaitDiv.modal('hide');
            if (strcmpUsers(data._result, "error") == 0) {
                $('#editUser-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#_NameE').val("");
                $('#_EmailE').val("");
                $('#_PhoneE').val("");
            }
            else if (strcmpUsers(data._result, "success") == 0) {
                $('#editUser-result').html("<span><font color=blue>" + data._message + "</font></span>");
                $('#editUserButton').attr("disabled", true);
                $('#_NameE').val("");
                $('#_EmailE').val("");
                $('#_PhoneE').val("");

            }
        }
    });
}

function changeuserstatus(status, userid, uidiv) {

    var s = './changeuserstatus?_status=' + status + '&_userid=' + encodeURIComponent(userid);
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpUsers(data._result, "error") == 0) {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpUsers(data._result, "success") == 0) {
                Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                //                window.setTimeout(RefreshUsersList, 2000);
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);

            }
        }
    });
}

function changeunlockpassword(status, userid) {

    var s = './unlockuserpassword?_status=' + status + '&_userid=' + encodeURIComponent(userid);
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpUsers(data._result, "error") == 0) {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpUsers(data._result, "success") == 0) {
                Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function removeUser(userid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './removeuser?_userid=' + encodeURIComponent(userid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpUsers(data._result, "success") ===0) {
                        Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshUsersList, 2000);
                    } else {
                        Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}



function resendpassword(userid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './resenduserpassword?_userid=' + encodeURIComponent(userid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpUsers(data._result, "success") == 0) {
                        Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                    } else {
                        Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}


function unlockpassword(userid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './unlockuserpassword?_userid=' + encodeURIComponent(userid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpUsers(data._result, "success") == 0) {
                        Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                    } else {
                        Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}
function resetpassword(userid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './resetuserpassword?_userid=' + encodeURIComponent(userid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpUsers(data._result, "error") == 0) {
                        Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpUsers(data._result, "success") == 0) {
                        Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}
function resetandsendpassword(userid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './setandresenduserpassword?_userid=' + encodeURIComponent(userid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpUsers(data._result, "error") == 0) {
                        Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpUsers(data._result, "success") == 0) {
                        Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}
var grouptype;
function loadUserDetails(_uid) {
    $('#editUserButton').attr("disabled", false);
    var s = './loadUser?_uid=' + encodeURIComponent(_uid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpUsers(data._result, "success") == 0) {
                $('#idEditUser').html(data._Name);
                $('#_userIdE').val(data._uid);
                $("#_NameE").val(data._Name);
                $("#_EmailE").val(data._Email);
                $("#_PhoneE").val(data._Phone);
                grouptype = data._groupName;
//                $("#abc").val(data._groupName);
//                alert(data._groupName);
                $("#_groupidE").val(data._groupName);
                $('#editUser-result').html("");
                $("#editUser").modal();
            } else {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}

function searchUsers() {
    var val = document.getElementById('_keyword').value;
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');

//    if ( val.length <= 3 ){
//        Alert4Users("Keyword should be meaningful and/or more than 3 characters");
//        return;
//    }

    if (val.length < 1) {
        //Alert4Tokens("Keyword should be meaningful and/or more than 3 characters");
        Alert4Users("Search keyword cannot be blank!!!");
        return;
    }

    var s = './userstable.jsp?_searchtext=' + encodeURIComponent(val);
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#users_table_main').html(data);
            pleaseWaitDiv.modal('hide');
        }
    });
}


function UsersAudits(_userid, _duration) {
    var s = './getuseraudits?_userid=' + encodeURIComponent(_userid) + "&_duration=" + _duration + "&_format=pdf";
    window.location.href = s;
    return false;
}

function changeunlockpassword(status, userid, uidiv) {

    var s = './unlockuserpassword?_status=' + status + '&_userid=' + encodeURIComponent(userid);
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpUsers(data._result, "error") == 0) {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpUsers(data._result, "success") == 0) {
                Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}


function passwordAudits(_userid, _itemtype, _duration) {
    var s = './getuseraudits?_userid=' + encodeURIComponent(_userid) + "&_itemType=" + _itemtype + "&_duration=" + _duration + "&_format=pdf";
    window.location.href = s;
    return false;

}

function ChangeStatusType(value) {
    //1 for enabled
    //0 for disabled
    if (value === 1) {
        $('#_changeStatus').val("1");
        $('#_change-status-Axiom').html("Active");
    } else if (value === 0) {
        $('#_changeStatus').val("0");
        $('#_change-status-Axiom').html("Suspended");
    } else if (value === -1) {
        $('#_changeStatus').val("-1");
        $('#_change-status-Axiom').html("Locked");
    } else if (value === -99) {
        $('#_changeStatus').val("-99");
        $('#_change-status-Axiom').html("Removed");
    } else if (value === 99) {
        $('#_changeStatus').val("99");
        $('#_change-status-Axiom').html("All");
    }
}

//function searchUserReport() {
//    var val = document.getElementById('_searchtext').value;
//    var val1 = document.getElementById('_changeStatus').value;
// 
//
//    var s = './usersreportstable.jsp?_searchtext=' + val +"&_status="+val1;
//    $.ajax({
//        type: 'GET',
//        url: s,
//        success: function(data) {
//            $('#search_user_main').html(data);
//        }
//    });
//}


function UserReportDownload(val2) {
    var val = document.getElementById('_searchtext').value;
    var val1 = document.getElementById('_changeStatus').value;
    var s = './userreportdown?_strSearch=' + val + "&_status=" + val1 + "&_reporttype=" + val2;
    window.location.href = s;
    return false;
}
function UserRoleReportDownload(val2) {
    var val = document.getElementById('_searchtext').value;
    var val1 = document.getElementById('_changeStatus').value;
    var s = './userrolereportdown?_strSearch=' + val + "&_status=" + val1 + "&_reporttype=" + val2;
    window.location.href = s;
    return false;
}

///////shailendra
function strcmpUsers(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4Users(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}


function InvalidRequest(id) {
    var s = './reportResponse?_requestID=' + id;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpUsers(data._result, "error") == 0) {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpUsers(data._result, "success") == 0) {
                Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}

function UnitRoleReportDownload(val2) {
    var val = document.getElementById('_searchtext').value;
    var val1 = document.getElementById('_changeStatus').value;
    var s = './unitrolereportdown?_strSearch=' + val + "&_status=" + val1 + "&_reporttype=" + val2;
    window.location.href = s;
    return false;
}

function UserReportDonutChart(val, val1) {
    var s = './userdonut?_searchtext=' + val + "&_status=" + val1;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;

}
function UserReportBarChart(val, val1) {
    var s = './userbarchart?_searchtext=' + val + "&_status=" + val1;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;

}

//function searchUserReport() {
//
//    var valK = document.getElementById('_searchtext').value;
//
//
//    if (valK.length <= 3) {
//        
//        Alert4Users("<h2><font color=red>Keyword should be meaningful and/or more than 3 characters</font></h2>");
//        return;
//    }
//
//    var val = document.getElementById('_searchtext').value;
//    var val1 = document.getElementById('_changeStatus').value;
//
//    var s = './usersreportstable.jsp?_searchtext=' + val + "&_status=" + val1;
//
//    $.ajax({
//        type: 'GET',
//        url: s,
//        success: function(data) {
//            $('#licenses_data_table').html(data);
//            //$("#UserReportgraph").empty();
//            //$("#UserReportgraph1").empty();
//
//
//            var day_data = UserReportDonutChart(val, val1);
//
//
//            Morris.Donut({
//                element: 'UserReportgraph',
//                data: day_data
//
//            });
//
//            var day_data = UserReportBarChart(val, val1);
//            Morris.Bar({
//                element: 'UserReportgraph1',
//                data: day_data,
//                xkey: 'label',
//                ykeys: ['value'],
//                labels: ['value'],
//                barColors: function(type) {
//                    if (type === 'bar') {
//
//                        return '#0066CC';
//                    }
//                    else {
//
//                        return '#0066CC';
//                    }
//                }
//            });
//
//        }
//    });
//}


//function searchUserReport() {
//
//    var valK = document.getElementById('_searchtext').value;
//    
//    var fromDate = document.getElementById('userstartdate').value;
//    var toDate = document.getElementById('userenddate').value;
//
//    if (valK.length <= 3) {        
//        Alert4Users("<h2><font color=red>Keyword should be meaningful and/or more than 3 characters</font></h2>");
//        return;
//    }
//
//    var val = document.getElementById('_searchtext').value;
//    var val1 = document.getElementById('_changeStatus').value;
//   
//    var s = './usersreportstable.jsp?_searchtext=' + val + "&_status=" + val1 +"&_fromDate="+fromDate+"&_toDate="+toDate;
//    
//    
//    $.ajax({
//        type: 'GET',
//        url: s,
//        success: function(data) {
//            $('#licenses_data_table').html(data);
//            $("#UserReportgraph").empty();
//            $("#UserReportgraph1").empty();
//
//            var day_data = UserReportDonutChart(val, val1, fromDate, toDate);
//
//
//            Morris.Donut({
//                element: 'UserReportgraph',
//                data: day_data
//
//            });
//
//            var day_data = UserReportBarChart(val, val1,fromDate,toDate);
//            Morris.Bar({
//                element: 'UserReportgraph1',
//                data: day_data,
//                xkey: 'label',
//                ykeys: ['value'],
//                labels: ['value'],
//                barColors: function(type) {
//                    if (type === 'bar') {
//
//                        return '#0066CC';
//                    }
//                    else {
//
//                        return '#0066CC';
//                    }
//                }
//            });
//
//        }
//    });
//}

function searchRoleReport() {

    var valK = document.getElementById('_searchtext').value;
    
    var fromDate = document.getElementById('userstartdate').value;
    var toDate = document.getElementById('userenddate').value;
    var frdate = new Date(fromDate);
    var edate = new Date(toDate);
//    if (fromDate == "") {        
//        Alert4Users("<h2><font color=red>From date cannot be blank!!!</font></h2>");
//        return;
//    }else if(toDate == ""){
//        Alert4Users("<h2><font color=red>To date cannot be blank!!!</font></h2>");
//        return;
//    }else if(frdate > edate){
//         Alert4Users("<h2><font color=red>From date should be less then To date!!!</font></h2>");
//        return;
//    }
    if(valK === '-1'){
        Alert4Users("<h2><font color=red>Please select role name!!! </font></h2>");
        return;
    }

    var val = document.getElementById('_searchtext').value;
    var val1 = document.getElementById('_changeStatus').value;
//    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
//    pleaseWaitDiv.modal();
    var s = './rolesreport.jsp?_searchtext=' + val + "&_status=" + val1 +"&_fromDate="+fromDate+"&_toDate="+toDate;
    
    $('#licenses_data_table').html("<h3>Loading....</h3>");
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#licenses_data_table').html(data);
            $("#UserReportgraph").empty();
            $("#UserReportgraph1").empty();

            var day_data = UserReportDonutChart(val, val1, fromDate, toDate);


            Morris.Donut({
                element: 'UserReportgraph',
                data: day_data

            });

            var day_data = UserReportBarChart(val, val1,fromDate,toDate);
            Morris.Bar({
                element: 'UserReportgraph1',
                data: day_data,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {

                        return '#0066CC';
                    }
                    else {

                        return '#0066CC';
                    }
                }
            });
            //pleaseWaitDiv.modal('hide');
        }
    });
}



function searchUserReport() {

    var valK = document.getElementById('_searchtext').value;
    
    var fromDate = document.getElementById('userstartdate').value;
    var toDate = document.getElementById('userenddate').value;

    if (valK.length <= 3) {        
        Alert4Users("<h2><font color=red>Keyword should be meaningful and/or more than 3 characters</font></h2>");
        return;
    }

    var val = document.getElementById('_searchtext').value;
    var val1 = document.getElementById('_changeStatus').value;
//    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
//    pleaseWaitDiv.modal();
    var s = './usersreportstable.jsp?_searchtext=' + val + "&_status=" + val1 +"&_fromDate="+fromDate+"&_toDate="+toDate;
    
    $('#licenses_data_table').html("<h3>Loading....</h3>");
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#licenses_data_table').html(data);
            $("#UserReportgraph").empty();
            $("#UserReportgraph1").empty();

            var day_data = UserReportDonutChart(val, val1, fromDate, toDate);


            Morris.Donut({
                element: 'UserReportgraph',
                data: day_data

            });

            var day_data = UserReportBarChart(val, val1,fromDate,toDate);
            Morris.Bar({
                element: 'UserReportgraph1',
                data: day_data,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {

                        return '#0066CC';
                    }
                    else {

                        return '#0066CC';
                    }
                }
            });
            //pleaseWaitDiv.modal('hide');
        }
    });
}





//function userReportCSV() {
//    var val = document.getElementById('_searchtext').value;
//    var val1 = document.getElementById('_changeStatus').value;
//    var s = './userreportdown?_strSearch=' + val + "&_status=" + val1 + "&_reporttype=" + 1;
//    window.location.href = s;
//    return false;
//}
//
//function userReportPDF() {
//    var val = document.getElementById('_searchtext').value;
//    var val1 = document.getElementById('_changeStatus').value;
//    var s = './userreportdown?_strSearch=' + val + "&_status=" + val1 + "&_reporttype=" + 0;
//    window.location.href = s;
//    return false;
//}


function removemobiletrustUser(userid) {
    bootbox.confirm("<h2><font color=red>Mobile Trust includes Mobile OTP,Mobile PKI, Digital Certificate,Device Profile and Geo Tracking information. All will be removed. Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {

            bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
                if (result == false) {
                } else {
                    var s = './eraseMobileTrustCredential?_userid=' + encodeURIComponent(userid);
                    $.ajax({
                        type: 'GET',
                        url: s,
                        dataType: 'json',
                        success: function(data) {
                            if (strcmpUsers(data._result, "success") == 0) {
                                Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                                //window.setTimeout(RefreshResourceList, 2000);
                                //window.setTimeout(RefreshUsersList, 2000);
                            } else {
                                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                            }
                        }
                    });
                }
            });
        }
    });
}

//var myApp;
//myApp = myApp || (function() {
//    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
//    return {
//        showPleaseWait: function() {
//            pleaseWaitDiv.modal();
//        },
//        hidePleaseWait: function() {
//            pleaseWaitDiv.modal('hide');
//        },
//    };
//})(); 
//
//myApp.showPleaseWait();

//function userReportTXT() {
//    var val = document.getElementById('_searchtext').value;
//    var val1 = document.getElementById('_changeStatus').value;
//    var s = './userreportdown?_strSearch=' + val + "&_status=" + val1 + "&_reporttype=" + 2;
//    window.location.href = s;
//    return false;
//}

function loadUserPasswordDetails(_userId, _userName) {
    $("#_auditUserID").val(_userId);
    $("#_auditUserName").val(_userName);
    $("#userauditDownload").modal();
}

function searchUserAudit() {
    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
    var val4 = encodeURIComponent(document.getElementById('_auditUserID').value);
    var val5 = document.getElementById('_auditUserName').value;
//    alert(val5);

    if (document.getElementById('_auditStartDate').value.length == 0 || document.getElementById('_auditEndDate').value.length == 0) {
        Alert4Msg("Date Range is not selected!!!");
        return;
    }
    var s = './userPasswordAudit.jsp?_startdate=' + val1 + "&_enddate=" + val2 + "&_auditUserID=" + val4 + "&_auditUserName=" + val5 + "&_itemType=USERPASSWORD";
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
//            $('#licenses_data_table').html(data);
            $('#auditTable').html(data);
            $("#userauditDownload").modal('hide');

        }
    });
}
function userReportCSV(_itemType , startdate, enddate, userid) {

//    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
//    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
//    var val4 = encodeURIComponent(document.getElementById('_auditUserID').value);
// 
//    if (document.getElementById('_auditStartDate').value.length == 0 || document.getElementById('_auditEndDate').value.length == 0) {
//        val1 = encodeURIComponent(document.getElementById('_auditStartDate1').value);
//        val2 = encodeURIComponent(document.getElementById('_auditEndDate1').value);
//        val4 = encodeURIComponent(document.getElementById('_auditUserID1').value);
//       
//    } else if (document.getElementById('_auditStartDate1').value.length == 0 || document.getElementById('_auditEndDate1').value.length == 0) {
//        val1 = encodeURIComponent(document.getElementById('_auditStartDatePKI').value);
//        val2 = encodeURIComponent(document.getElementById('_auditEndDatePKI').value);
//        val4 = encodeURIComponent(document.getElementById('_auditUserIDPKI').value);
//      
//    } else if (document.getElementById('_auditStartDatePKI').value.length == 0 || document.getElementById('_auditEndDatePKI').value.length == 0) {
//        val1 = encodeURIComponent(document.getElementById('_auditStartDateSecurePhrase').value);
//        val2 = encodeURIComponent(document.getElementById('_auditEndDateSecurePhrase').value);
//        val4 = encodeURIComponent(document.getElementById('_userIDSecureSecurePhrase').value);
//     
//    }
   var val1 = startdate;
   var val2 = enddate;
   var val4 = userid;

//    alert("5" + val1);
    var s = './getuseraudits?_startdate=' + val1 + "&_enddate=" + val2 + "&_auditUserID=" + val4 + "&_itemType=" + _itemType + "&_format=1";
    window.location.href = s;
    return false;
}

function userReportPDF(_itemType, startdate, enddate, userid) {
//    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
//    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
//    var val4 = encodeURIComponent(document.getElementById('_auditUserID').value);
//    if (document.getElementById('_auditStartDate').value.length == 0 || document.getElementById('_auditEndDate').value.length == 0) {
//        val1 = encodeURIComponent(document.getElementById('_auditStartDate1').value);
//        val2 = encodeURIComponent(document.getElementById('_auditEndDate1').value);
//        val4 = encodeURIComponent(document.getElementById('_auditUserID1').value);
//        if (document.getElementById('_auditStartDate1').value.length == 0 || document.getElementById('_auditEndDate1').value.length == 0) {
//            val1 = encodeURIComponent(document.getElementById('_auditStartDatePKI').value);
//            val2 = encodeURIComponent(document.getElementById('_auditEndDatePKI').value);
//            val4 = encodeURIComponent(document.getElementById('_auditUserIDPKI').value);
//        }
//        if (document.getElementById('_auditStartDatePKI').value.length == 0 || document.getElementById('_auditEndDatePKI').value.length == 0) {
//            val1 = encodeURIComponent(document.getElementById('_auditStartDateSecurePhrase').value);
//            val2 = encodeURIComponent(document.getElementById('_auditEndDateSecurePhrase').value);
//            val4 = encodeURIComponent(document.getElementById('_userIDSecureSecurePhrase').value);
//        }
//    }
  var val1 = startdate;
   var val2 = enddate;
   var val4 = userid;   
    var s = './getuseraudits?_startdate=' + val1 + "&_enddate=" + val2 + "&_auditUserID=" + val4 + "&_itemType=" + _itemType + "&_format=0";
    window.location.href = s;
    return false;
}

function userReportTXT(_itemType, startdate, enddate, userid) {
//    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
//    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
//    var val4 = encodeURIComponent(document.getElementById('_auditUserID').value);
//    if (document.getElementById('_auditStartDate').value.length == 0 || document.getElementById('_auditEndDate').value.length == 0) {
//        val1 = encodeURIComponent(document.getElementById('_auditStartDate1').value);
//        val2 = encodeURIComponent(document.getElementById('_auditEndDate1').value);
//        val4 = encodeURIComponent(document.getElementById('_auditUserID1').value);
//        if (document.getElementById('_auditStartDate1').value.length == 0 || document.getElementById('_auditEndDate1').value.length == 0) {
//            val1 = encodeURIComponent(document.getElementById('_auditStartDatePKI').value);
//            val2 = encodeURIComponent(document.getElementById('_auditEndDatePKI').value);
//            val4 = encodeURIComponent(document.getElementById('_auditUserIDPKI').value);
//            if (document.getElementById('_auditStartDate1').value.length == 0 || document.getElementById('_auditEndDate1').value.length == 0) {
//                val1 = encodeURIComponent(document.getElementById('_auditStartDateChallenge').value);
//                val2 = encodeURIComponent(document.getElementById('_auditEndDateChallenge').value);
//                val4 = encodeURIComponent(document.getElementById('_auditUserIDChallenge').value);
//            }
//            if (document.getElementById('_auditStartDatePKI').value.length == 0 || document.getElementById('_auditEndDatePKI').value.length == 0) {
//                val1 = encodeURIComponent(document.getElementById('_auditStartDateSecurePhrase').value);
//                val2 = encodeURIComponent(document.getElementById('_auditEndDateSecurePhrase').value);
//                val4 = encodeURIComponent(document.getElementById('_userIDSecureSecurePhrase').value);
//            }
//        }
//    }
var val1 = startdate;
   var val2 = enddate;
   var val4 = userid;   
    var s = './getuseraudits?_startdate=' + val1 + "&_enddate=" + val2 + "&_auditUserID=" + val4 + "&_itemType=" + _itemType + "&_format=2";
    window.location.href = s;
    return false;
}



function searchTwowayauthUsers() {
    var val = document.getElementById('_authkeyword').value;
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    if (val.length < 1) {
        //Alert4Tokens("Keyword should be meaningful and/or more than 3 characters");
        Alert4Users("<font color=red>Search keyword cannot be blank!!!</font>");
        return;
    }
    var s = './twowayauthtable.jsp?_searchtext=' + encodeURIComponent(val);
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#auth_table_main').html(data);
            pleaseWaitDiv.modal('hide');
        }
    });
}

function ChangeAuthType(type, status, keyword, userid) {

    var s = './EditAuth?type=' + type + '&status=' + status + '&userid=' + userid;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpUsers(data._result, "success") == 0) {
                bootbox.confirm("<h2><font color=blue>" + data._message + "</font></h2>", function(result) {
                    window.location.href = "./Twowayauth.jsp?_authkeyword=" + keyword;
                }
                );
            } else {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}

function sendPushRegcode(userid, keyword) {

    var s = './SendPushRegCode?userid=' + userid;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpUsers(data._result, "success") == 0) {
                bootbox.confirm("<h2><font color=blue>" + data._message + "</font></h2>", function(result) {
                    window.location.href = "./Twowayauth.jsp?_authkeyword=" + keyword;
                }
                );
            } else {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}

function generateTxRepo() {
    var s = './TxReport';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#TransactionSearch").serialize(),
        success: function(data) {
            if (strcmpUsers(data._result, "success") == 0) {
                bootbox.confirm("<h2><font color=blue>" + data._message + "</font></h2>", function(result) {
                    window.location.href = "./transactionReportTable.jsp";
                });
            } else {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}

function txReport(type) {
    var s = './TxReportDownload?_reporttype=' + type;
    window.location.href = s;
    return false;
}

function InvalidRequest(id) {
    var s = './reportResponse?_requestID=' + id;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpUsers(data._result, "error") == 0) {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpUsers(data._result, "success") == 0) {
                Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}

function addnewresource() {
    $('#addResourceButton').attr("disabled", true);
    var s = './addresource';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#addResourceForm").serialize(),
        success: function(data) {
            if (strcmpUsers(data._result, "error") == 0) {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
//                $('#add-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#addResourceButton').attr("disabled", false);
            }
            else if (strcmpUsers(data._result, "success") == 0) {
                Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                $('#addResourceButton').attr("disabled", false);
                window.setTimeout(RefreshResourceList, 2000);
            }
        }
    });
}
function editback() {
    window.location = './resources.jsp';
}
function editresource() {

    $('#editResourceButton').attr("disabled", true);
    var s = './editresource';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editResourceForm").serialize(),
        success: function(data) {
            if (strcmpUsers(data._result, "error") == 0) {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                $('#editResourceButton').attr("disabled", false);
            }
            else if (strcmpUsers(data._result, "success") == 0) {
                Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
//                window.setTimeout(RefreshUsersList, 2000);
                $('#_Name').val("");
                $('#_URL').val("");
                $('#_groupName').val(null);
                $('#fileImageToUpload').val("");
                $('#_Status').val("");
                $('#editResourceButton').attr("disabled", false);
            }
        }
    });
}
function changestat(status, resid, uidiv, keyword) {
    var s = './ChangeStatus?_status=' + status + '&_resourceid=' + resid;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpUsers(data._result, "error") == 0) {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpUsers(data._result, "success") == 0) {
                Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
                window.location.href = "./resources.jsp?_keyword=" + keyword;
            }
        }
    });
}

function removeResource(resid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './removeresource?_resid=' + encodeURIComponent(resid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpUsers(data._result, "success") == 0) {
                        Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshResourceList, 2000);
                    } else {
                        Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}
function UploadImagesFile() {
    $('#buttonUpload').attr("disabled", true);
    var s = './UploadImage';
    $.ajaxFileUpload({
        fileElementId: 'fileImageToUpload',
        url: s,
        dataType: 'json',
        success: function(data, status) {
            if (strcmpUsers(data._message, "error") == 0) {
                Alert4Upload("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpUsers(data._result, "success") == 0) {
                Alert4Upload("<span><font color=blue>" + data._message + "</font></span>");
                $('#buttonUploadImage').attr("disabled", false);
            }
        },
        error: function(data, status, e)
        {
            alert("hi.....");
            alert(e);
        }
    });
}
function addresource() {
//    $('#addResourceButton').attr("disabled", true);
    window.location = './addresource.jsp';
}

function addnewclose() {
//    $('#addResourceButton').attr("disabled", true);
    window.location = './resources.jsp';
}
function loadResourceDetails(_resid) {
    var s = './loadResource?_resid=' + encodeURIComponent(_resid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpUsers(data._result, "success") == 0) {
                window.location = './editresource.jsp';
            } else {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}
function searchResources() {

    var valK = document.getElementById('_keyword').value;
    if (valK.length < 3) {
        Alert4Users("Keyword should be meaningful and/or more than 3 characters");
        return;
    }
//    var val = document.getElementById('_keyword').value;
    var s = './resourcestable.jsp?_keyword=' + valK;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#resource_table_main').html(data);
        }
    });

}
function UploadImageFile() {
    $('#buttonUpload').attr("disabled", true);
    var s = './UploadIcons';
    $.ajaxFileUpload({
        type: 'POST',
        fileElementId: 'fileImageToUpload',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpUsers(data.message, "error") === 0) {
                Alert4Users("<span><font color=red>" + data.message + "</font></span>");
            }
            else if (strcmpUsers(data.result, "success") === 0) {
                Alert4Users("<span><font color=blue>" + data.message + "</font></span>");
                $('#buttonUploadImage').attr("disabled", false);
            }
        },
        error: function(data, status, e)
        {
            alert(e);
        }
    });
}


function changeusergroup(groupid,userid,groupname, uidiv) {
var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var s = './changeusergroup?_groupidE=' + groupid + '&_userIdE=' + encodeURIComponent(userid)+'&_groupname=' + groupname;
   pleaseWaitDiv.modal();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpUsers(data._result, "error") == 0) {
                  pleaseWaitDiv.modal('hide');
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpUsers(data._result, "success") == 0) {
                  pleaseWaitDiv.modal('hide');
                Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                //                window.setTimeout(RefreshUsersList, 2000);
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
                

            }
        }
    });
}

function getUserDetails(_uid) {
    $('#userDetail').attr("disabled", false);
    var s = './getUserDetails?_uid=' + encodeURIComponent(_uid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpUsers(data._result, "success") == 0) {
                $('#iduserDetails').html(data._Name+"'s Details");
                $('#_userIdD').val(data._uid);
                $("#_NameD").val(data._Name);
                $("#_EmailD").val(data._Email);
                $("#_PhoneD").val(data._Phone);
                $("#_CountryD").val(data._Country);
                $("#_LocationD").val(data._Location);
                $("#_StreetD").val(data._Street);
                $("#_DesignationD").val(data._Designation);
                $("#_OrganisationD").val(data._Organisation);
                $("#_OrganisationUnitD").val(data._OrganisationUnit);
                $("#_StatusD").val(data._Status);
                $("#_UserIdentity").val(data._UserIdentity);
                $("#_IdType").val(data._IdType);
                grouptype = data._groupName;
//                $("#abc").val(data._groupName);

                $('#userDetail-result').html("");
                $("#userDetail").modal();
            } else {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}
function edituserdetails() {
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var s = './edituserdetails';
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editUserDetailsForm").serialize(),
        success: function(data) {
            pleaseWaitDiv.modal('hide');
            if (strcmpUsers(data._result, "error") == 0) {
                $('#userDetail-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                
            }
            else if (strcmpUsers(data._result, "success") == 0) {
                $('#userDetail-result').html("<span><font color=blue>" + data._message + "</font></span>");
                
                
            }
        }
    });
}


function downloadTwowayAuthAudit(usrid, name) {
    $("#_twowayUserId").val(usrid);
    $("#_twowayUserName").val(name);
    $("#twowayAuthAuditDowload").modal();
}
function downloadTWAuthAudit() {
    var val1 = encodeURIComponent(document.getElementById('_auditStartDateTwoway').value);
    var val2 = encodeURIComponent(document.getElementById('_auditEndDateTwoWay').value);
    var val4 = encodeURIComponent(document.getElementById('_twowayUserId').value);
    var val5 = document.getElementById('_twowayUserName').value;
//    alert(val1 +" "+ val2);

    if (document.getElementById('_auditStartDateTwoway').value.length == 0 || document.getElementById('_auditEndDateTwoWay').value.length == 0) {
        Alert4Tokens("<font color=red>Date Range is not selected!!!");
        return;
    }
    var s = './twowayauthAudit.jsp?_startdate=' + val1 + "&_enddate=" + val2 + "&_auditUserID=" + val4 + "&_auditUserName=" + val5;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
//            $('#licenses_data_table').html(data);
            $('#auditTable').html(data);
            $("#twowayAuthAuditDowload").modal('hide');

        }
    });
}

function downloadtxReport(type, name, startdate, enddate) {
    var s = './DownloadAuthReport?_reporttype=' + type + "&_startDate=" + startdate + "&_endDate=" + enddate + "&_name=" + name;
    window.location.href = s;
    return false;
}
//Mayuri
function sendQRCodeViaEmail(userid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './sendGoogleAuthToken?_userid=' + encodeURIComponent(userid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpUsers(data._result, "error") == 0) {
                        Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpUsers(data._result, "success") == 0) {
                        Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}