function strcmpaudits(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4audits(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
        //end here
        }
    });
}
function SessionTerminate() {
     var _sessionId = document.getElementById ('_sessionId').value;
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
           var s = './terminatesession?_sessionId='+encodeURIComponent(_sessionId);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpaudits(data._result, "success") == 0) {
                        Alert4audits("<span><font color=red>" + data._message + "</font></span>");
                        window.setTimeout(Refreshaudit, 2000);
                    } else {
                        Alert4audits("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}

function SessionAudits(){
   var _sessionId = document.getElementById ('_sessionId').value;
   var _sessionType = document.getElementById ('_sessionType').value;
     if(strcmpaudits(_sessionType,"0")===0)
     {
         if(strcmpaudits(_sessionId,"")===0)
         {
             alert("Please enter SessionId");
             return;
             
         }
         
     } 
    var s = './sessionaudit?_sessionId='+encodeURIComponent(_sessionId) + "&_format=pdf"+"&_sessionType="+document.getElementById("_sessionType").value;
    window.location.href = s;
    return false;

}
function SessionAuditsCSV(){
   var _sessionId = document.getElementById ('_sessionId').value;
    var s = './sessionaudit?_sessionId='+encodeURIComponent(_sessionId) + "&_format=csv";
    window.location.href = s;
    return false;

}


function ChannelAudits(){
   var _channeliD = document.getElementById ('_channeliD').value;
      var val1 = encodeURIComponent(document.getElementById('_channelDate').value);
        var val2 = encodeURIComponent(document.getElementById('_channelSDate').value);
    var s = './channelaudit?_channeliD='+encodeURIComponent(_channeliD) + "&_format=pdf&_channelAuditDate="+val1+"&_channelSAuditDate="+val2;
    window.location.href = s;
    return false;

}
function ChannelAuditsCSV(){
   var _channeliD = document.getElementById ('_channeliD').value;
   var val1 = encodeURIComponent(document.getElementById('_channelDate').value);
    var val2 = encodeURIComponent(document.getElementById('_channelSDate').value);
    var s = './channelaudit?_channeliD='+encodeURIComponent(_channeliD) + "&_format=csv&_channelAuditDate="+val1+"&_channelSAuditDate="+val2;
    window.location.href = s;
    return false;
}