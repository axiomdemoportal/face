
function strcmperrormessage(a, b)
{   
    return (a<b?-1:(a>b?1:0));  
}

function Alert4ErrorMessage(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
        //end here
        }
    });
}


function RefreshMobileTemplatesList() {
    window.location.href = "./TemplateMobileList.jsp"    
}

function RefreshEmailTemplatesList() {
    window.location.href = "./TemplateEmailList.jsp"    
}


function  editMessagetemplates(){
    var s = './editemessage';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#messageedittemplateform").serialize(),
        success: function(data) {
            if ( strcmperrormessage(data._result,"error") == 0 ) {
//                $('#edittemplateM-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                  Alert4ErrorMessage(data._message);
            }
            else if ( strcmperrormessage(data._result,"success") == 0 ) {
//                $('#edittemplateM-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4ErrorMessage(data._message);
//                $('#buttonEditMessage').attr("disabled", true);
                
            }
        }
    }); 
}


function loadErrorMessageDetails(_tid){
    var s = './loademessage?_tid='+_tid;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if ( strcmperrormessage(data._result,"success") == 0 ) {    
                
                $('#idEditTemplateName').html(data._name);
                $('#idMessageTemplateId').val(data._tid);
                $("#_templateM_name").val(data._name);
                //$("#_templateM_subject").val(data._subject);
                $("#_templateM_body").val(data._body);
                $("#_templateM_variable").val(data._variables);
                $('#edittemplateM-result').html("");
                
            } else {
                Alert4ErrorMessage("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}










function ChangeType(value) {
    
    if ( value === 1) {
        $('#_types').val("1");
        $('#_type-primary-sms').html("Mobile");
    } 
    else if(value === 2){
        $('#_types').val("2");
        $('#_type-primary-sms').html("Email");
            
    }
    
        
}

function changemessagestatus(_tid, _status,uidiv) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {

            var s = './changeemessagestatus?_status=' + _status + '&_templateid=' + encodeURIComponent(_tid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmperrormessage(data._result, "success") == 0) {
                        Alert4ErrorMessage("<span><font color=blue>" + data._message + "</font></span>");
                        var uiToChange = '#' + uidiv;
                        $(uiToChange).html(data._value);

                    } else {
                        Alert4ErrorMessage("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}