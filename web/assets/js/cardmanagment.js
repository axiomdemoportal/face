/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function strcmpcard(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4card(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}





function getCardDetails(_uid) {
    $('#cardDetail').attr("disabled", false);
    var s = './getCardDetails?_uid=' + encodeURIComponent(_uid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpcard(data._result, "success") == 0) {
                
               // $('#idpassDetails').html(data._NamePass+"'s Details");
                $('#_userCard').val(data._uid);
                
            
                $("#_ExpirydateCard").val(data._ExpirydateCard);
                $("#_CreatedOnCard").val(data._CreatedOnCard);
                $("#_UpdatedOnCard").val(data._UpdatedOnCard);
                
                $("#_CardnumberCard").val(data._CardnumberCard);
               
            
                $('#cardDetails-result').html("");
                $("#cardDetail").modal();
            } else {
                Alert4card("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}

function loadCardDetails(_uid) {
    $('#editCardDetailsButton').attr("disabled", false);
    var s = './getCardDetails?_uid=' + encodeURIComponent(_uid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpcard(data._result, "success") == 0) {
                
               
                $("#_ExpirydateCard").val(data._ExpirydateCard);
                $("#_CreatedOnCard").val(data._CreatedOnCard);
                $("#_UpdatedOnCard").val(data._UpdatedOnCard);
                
                $("#_CardnumberCard").val(data._CardnumberCard);
               
            
                $('#cardDetails-result').html("");
                $("#cardDetail").modal();
            } else {
                Alert4card("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}


function editcarddetails() {
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var s = './editcarddetails';
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editCardDetailsForm").serialize(),
        success: function(data) {
            pleaseWaitDiv.modal('hide');
            if (strcmpcard(data._result, "error") == 0) {
                $('#cardDetails-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                
            }
            else if (strcmpcard(data._result, "success") == 0) {
                $('#cardDetails-result').html("<span><font color=blue>" + data._message + "</font></span>");
                
                
            }
        }
    });
}
