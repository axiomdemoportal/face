function Alert4US(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
        //end here
        }
    });
}

function strcmpUS(a, b)
{
    return (a<b?-1:(a>b?1:0));
}

function AddOption(selectitem,name,value)
{
    var x=document.getElementById(selectitem);
    var option=document.createElement("option");
    option.text=name;
    option.value=value;
    try
    {
        // for IE earlier than version 8
        x.add(option,x.options[null]);
    }
    catch (e)
    {
        x.add(option,null);
    }
}


function LoadUserSourceSetting() {

    var s = './loadusersrcsettings';
    
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
      
        success: function(data) 
        { 
            $('#_ipUS').val(data._ip);
            $('#_portUS').val(data._port);
            $('#_statusUS').val(data._status);
            $('#_userIdUS').val(data._userId);
            $('#_passwordUS').val(data._password);
            $('#_sslUS').val(''+data._ssl);
            $('#_classNameUS').val(data._className);
            $('#_reserve1US').val(data._reserve1);
            $('#_reserve2US').val(data._reserve2);
            $('#_reserve3US').val(data._reserve3);
            $('#_databaseTypeUS').val(data._databaseType);
            $('#_databaseNameUS').val(data._databaseName);
            $('#_tableNameUS').val(data._tableName);
            $('#_passwordTypeUS').val(data._passwordType);
            $('#_algoTypeUS').val(data._algoType);
            
            $('#_algoAttrReserve1US').val(data._algoAttrReserve1);
            $('#_algoAttrReserve2US').val(data._algoAttrReserve2);
            $('#_algoAttrReserve3US').val(data._algoAttrReserve3);
            $('#_algoAttrReserve4US').val(data._algoAttrReserve4);
            
            $('#_userValidityDays').val(data._userValidityDays);
            
            if ( strcmpUS(data._databaseType,"") != 0) {
                $('#_databaseTypeUS').each(function(){
                    var $t = $(this);
                    if ( strcmpUS($t.val(),data._databaseType) == 0 ) { 
                        $t.prop('selected', true);
                        return;
                    }                
                });
            }            
            
            $('#_passwordTypeUS').each(function(){
                var $t = $(this);
                if ($t.val() == data._passwordType) {
                    $t.prop('selected', true);
                    return;
                }                
            });
            
            $('#_algoTypeUS').each(function(){
                var $t = $(this);
                if ($t.val() == data._algoType) {
                    $t.prop('selected', true);
                    return;
                }                
            });
            
            $('#_sslUS').each(function(){
                var $t = $(this);
                if ($t.val() == data._ssl) {
                    $t.prop('selected', true);
                    return;
                }                 
            });
            
            
            
        }
    });
    
}


function EditUserSourceSetting() {
    
      if (strcmpUS(document.getElementById("_ipUS").value, "") === 0) {
          alert("Please enter ip/hostname");
          return;
      }
      if (strcmpUS(document.getElementById("_portUS").value, "") === 0) {
           alert("Please enter portno");
          return;
      }
      if (strcmpUS(document.getElementById("_databaseNameUS").value, "") === 0) {
          
          
          alert("Please enter databasename");
          return;
      }
      if (strcmpUS(document.getElementById("_tableNameUS").value, "") === 0) {
          
          
          alert("Please enter tablename");
          return;
      }
      if (strcmpUS(document.getElementById("_userIdUS").value, "") === 0) {
          
          
          alert("Please enter userid");
          return;
      }
      if (strcmpUS(document.getElementById("_passwordUS").value, "") === 0) {
          
          
          alert("Please enter password");
          return;
      }
      if (strcmpUS(document.getElementById("_classNameUS").value, "") === 0) {
          
          
          alert("Please enter classname");
          return;
      }
      
     
    var s = './editusersrcsettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#usersourceform").serialize(),
        success: function(data) {
            if (strcmpUS(data._result, "error") === 0) {
                $('#save-user-source-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4US("<span><font color=red>" + data._message + "</font></span>");    
            }
            else if (strcmpUS(data._result, "success") === 0) {
//                $('#save-user-source-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4US("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}


function testConnectionUS(){
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var s = './testconnection?_type=8&_preference=1';
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if ( strcmpUS(data._result,"error") == 0 ) {
                $('#save-user-source-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }
            else if ( strcmpUS(data._result,"success") == 0 ) {
                $('#save-user-source-result').html("<span><font color=blue>" + data._message + "</font></span>");
            }
            pleaseWaitDiv.modal('hide');
        }
    });
}


