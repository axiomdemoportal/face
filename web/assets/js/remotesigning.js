function Alert4RemoteSigning(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
        //end here
        }
    });
}

function ChangeSearchType(value) {
 
    if ( value == 1) {
        $('#_searchType').val("1");
        $('#_searchType_div').html("User ID");
    } else if ( value == 2){
        $('#_searchType').val("2");
        $('#_searchType_div').html("Reference ID");
    }else {
        $('#_searchType').val("0");
        $('#_searchType_div').html("Archive ID");
    }
    
}

function searchRemotSigningRecords() {
    
    var searchType = document.getElementById('_searchType').value;
    var userName = document.getElementById('_userName').value;
    var startdaters = document.getElementById('startdaters').value;
    var enddaters = document.getElementById('enddaters').value;
    
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    pleaseWaitDiv.modal();
    var s = './RSSReportTable.jsp?_userName='+encodeURIComponent(userName)+'&_searchType='+encodeURIComponent(searchType)+'&_startdaters='+encodeURIComponent(startdaters)+'&_enddaters='+encodeURIComponent(enddaters);
    $.ajax({
        type: 'post',
        url: s,
        success: function(data) {
            pleaseWaitDiv.modal('hide');
            $('#roaming_table_main').html(data);
        }
    });
}


function downloadRSSReport(type, startdate, enddate, searchType) {
    var s = './DownloadRemoteSigningReport?_reporttype=' + type + "&_startDate=" + startdate + "&_endDate=" + enddate+"&_searchType=" +searchType ;
    window.location.href = s;
    return false;
}
function strcmpRemoteSign(a, b){
    return (a < b ? -1 : (a > b ? 1 : 0));
    }

function InvalidRequestRemoteSign(id) {
    var s = './reportResponse?_requestID=' + id;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpRemoteSign(data._result, "error") == 0) {
                Alert4RemoteSigning("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpRemoteSign(data._result, "success") == 0) {
                Alert4RemoteSigning("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}