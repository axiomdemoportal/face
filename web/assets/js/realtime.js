function realtimes(settingName, type, start, end)
{
    $('#tabrealtime').empty();
    document.getElementById('tabrealtime').style.display = "block";
    document.getElementById('tabchart').style.display = "none";
    document.getElementById('tabtextual').style.display = "none";
    document.getElementById("tabchart").innerHTML = "";
    document.getElementById("tabtextual").innerHTML = "";
    var s = './realtime.jsp?_settingName=' + settingName + '&_type=' + type + '&_startdate=' + start + "&_enddate=" + end;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#tabrealtime').html(data);
        }
    });
}
function chart(settingName, type, start, end)
{
    $('#tabchart').empty();
    document.getElementById('tabtextual').style.display = "none";
    document.getElementById('tabrealtime').style.display = "none";
    document.getElementById('tabchart').style.display = "block";
    document.getElementById("tabtextual").innerHTML = "";
    document.getElementById("tabchart").innerHTML = "";
    var s = './chart.jsp?_settingName=' + settingName + '&_type=' + type + '&_startdate=' + start + "&_enddate=" + end;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#tabchart').html(data);
        }
    });
}
function recentreport(settingName, type)
{
    $('#tabHome').empty();
    var s = './settingRecentReport.jsp?_settingName=' + settingName + '&_type=' + type;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#tabHome').html(data);
        }
    });
}

function textualreport(settingName, type, start, end)
{
    $('#tabtextual').empty();
    document.getElementById('tabrealtime').style.display = "none";
    document.getElementById('tabtextual').style.display = "block";
    document.getElementById('tabchart').style.display = "none";
//        document.getElementById("tabDetail").innerHTML = "";
//            document.getElementById("tabchart").innerHTML = "";
    var s = './settingExecutionTextReport.jsp?_settingName=' + settingName + '&_type=' + type + '&_startdate=' + start + "&_enddate=" + end;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#tabtextual').html(data);
        }
    });
}

function searchReport(settingName, type) {
    var val1 = encodeURIComponent(document.getElementById('startdate').value);
    var val2 = encodeURIComponent(document.getElementById('enddate').value);
    if (document.getElementById('startdate').value.length == 0 || document.getElementById('enddate').value.length == 0) {
        Alert4Msg("Date Range is not selected!!!");
        return;
    }
    
//    if (document.getElementById('startdate').value > document.getElementById('enddate').value) {
//        Alert4Msg("Enter Proper Date Range!!!");
//        return;
//    }
    $('#tabHome').empty();
    var s = './settingExecutionSearch.jsp?_settingName=' + settingName + '&_type=' + type + '&_startdate=' + val1 + "&_enddate=" + val2;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#tabHome').html(data);
        }});
}
