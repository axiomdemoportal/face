function strcmpBillReport(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4BillReport(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}

function searchBillTrackByType(val) {
    if (document.getElementById('startdate').value.length == 0 || document.getElementById('enddate').value.length == 0) {
         Alert4BillReport("<span><font color=red>Date Range is not selected!!!</font></span>");
        return;
    }
    var val1 = encodeURIComponent(document.getElementById('startdate').value);
    var val2 = encodeURIComponent(document.getElementById('enddate').value);
    var val3 = document.getElementById('_changeCategory').value;
    var pleaseWaitDiv1 = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    pleaseWaitDiv1.modal();
    
    var s = './billReportTable.jsp?_startdate=' + val1 + "&_enddate=" + val2 + "&_type=" + val3 +"&_reportType=" +val;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            pleaseWaitDiv1.modal('hide');
            $('#licenses_data_table').html(data);
        }});
}

function searchBillTrackByTypeV1(val) {
    if (document.getElementById('startdate').value.length == 0 || document.getElementById('enddate').value.length == 0) {
         Alert4BillReport("<span><font color=red>Date Range is not selected!!!</font></span>");
        return;
    }
    var val1 = encodeURIComponent(document.getElementById('startdate').value);
    var val2 = encodeURIComponent(document.getElementById('enddate').value);
    var val3 = document.getElementById('_changeCategory').value;
//    var pleaseWaitDiv1 = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
//    pleaseWaitDiv1.modal();
    $('#licenses_data_table').html("<h3>Loading....</h3>");
    var s = './billReportTable.jsp?_startdate=' + val1 + "&_enddate=" + val2 + "&_type=" + val3 +"&_reportType=" +val;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
//            pleaseWaitDiv1.modal('hide');
            $('#licenses_data_table').html(data);
        }});
}


function geoBillReportCSV(type,val) {
    var val1 = encodeURIComponent(document.getElementById('startdate').value);
    var val2 = encodeURIComponent(document.getElementById('enddate').value);
    var s = './getBillRepot?_startdate=' + val1 + "&_enddate=" + val2 +"&_type="+ type+"&_reporttype=" + 1+"&_billreportType=" +val;
    window.location.href = s;
    return false;
}

function geoBillReportPDF(type,val) {
 var val1 = encodeURIComponent(document.getElementById('startdate').value);
    var val2 = encodeURIComponent(document.getElementById('enddate').value);
    var s = './getBillRepot?_startdate=' + val1 + "&_enddate=" + val2 +"&_type="+ type +"&_reporttype=" + 0+"&_billreportType=" +val;
    window.location.href = s;
    return false;
}
function geoBillReportTXT(type,val) {
    var val1 = encodeURIComponent(document.getElementById('startdate').value);
    var val2 = encodeURIComponent(document.getElementById('enddate').value);
    var s = './getBillRepot?_startdate=' + val1 + "&_enddate=" + val2 +"&_type="+ type + "&_reporttype=" + 2+"&_billreportType=" +val;
    window.location.href = s;
    return false;
}

function InvalidRequestBillReport(id) {
    var s = './reportResponse?_requestID=' + id;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpBillReport(data._result, "error") == 0) {
                Alert4BillReport("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpBillReport(data._result, "success") == 0) {
                Alert4BillReport("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}