/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function generateTwowayAuthTable() {
    var val3 = encodeURIComponent(document.getElementById('startdate').value);
    var val4 = encodeURIComponent(document.getElementById('enddate').value);

    if (document.getElementById('startdate').value.length == 0 || document.getElementById('enddate').value.length == 0) {
         Alert4Cert("<span><font color=red>Date Range is not selected!!!</font></span>");
        return;
    }
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    pleaseWaitDiv.modal();
    var s = './twowayauthreporttable.jsp?&_startdate=' + val3 + '&_enddate=' + val4;
    
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
             $('#auth_table_main').html(data);
                var day_data1 = null;
               day_data1 = BarChartDemo(val3, val4);
                Morris.Bar({
                    element: 'twoWayAuthgraph1',
                    data: day_data1,
                    xkey: 'label',
                    ykeys: ['value'],
                    labels: ['value'],
                    barColors: function (type) {
                        if (type === 'bar') {
                            return '#0066CC';
                        }
                        else {
                            return '#0066CC';
                        }
                    }
                });
        
                var day_data = null;
                day_data = DonutChart(val3, val4);
                Morris.Donut({
                    element: 'twoWayAuthgraph',
                    data: day_data
                });
                
                pleaseWaitDiv.modal('hide');
        }
        
    });
}


function DonutChart(val3, val4) {
    var s = './Authdonutchart?_startDate=' + val3 + "&_endDate=" + val4;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}
function BarChartDemo(val3, val4) {
    var s = './Authbarchart?_startDate=' + val3 + "&_endDate=" + val4; 
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}
function downloadauthReport(type, startdate, enddate) {
    var s = './DownloadAuthByDate?_reporttype=' + type + "&_startDate=" + startdate + "&_endDate=" + enddate + "&_name=" + name;
    window.location.href = s;
    return false;
}
function RefreshAuthReport() {
    window.location.href = "./twoWayAuthReport.jsp";
}