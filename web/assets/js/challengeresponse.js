function strcmpChallenge(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4Challege(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
        //end here
        }
    });
}

function RefreshUsersList() {
    window.location.href = "./ChallengeResponse.jsp";
}


function changeuserChallengestatus(status, userid, uidiv) {

    var s = './changeChallengestatus?_status=' + status + '&_userid=' + encodeURIComponent(userid);
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpChallenge(data._result, "error") == 0) {
                Alert4Challege("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpChallenge(data._result, "success") == 0) {
                Alert4Challege("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);

            }
        }
    });
}



function removeChallengeUser(userid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './removeChallengeuser?_userid=' + encodeURIComponent(userid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpChallenge(data._result, "success") == 0) {
                        Alert4Challege("<span><font color=red>" + data._message + "</font></span>");
                        window.setTimeout(RefreshUsersList, 2000);
                    } else {
                        Alert4Challege("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}








function loadQuestionsDetails(_uid) {
          
         $("#_userid").val(_uid);
         $("#listquestions").modal();
           
}

function searchChallengeUsers() {
    var val = document.getElementById('_keyword').value;
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    
    if ( val.length <= 3 ){
        Alert4Challege("Keyword should be meaningful and/or more than 3 characters");
        return;
    }
    var s = './ChallengeResponsetable.jsp?_searchtext=' + val;
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#challenge_table_main').html(data);
            pleaseWaitDiv.modal('hide');
        }
    });
}






function challengeAudits(_userid,_itemtype,_duration){
    var s = './getChallengeaudits?_userid='+encodeURIComponent(_userid) +"&_itemType="+_itemtype+"&_duration="+_duration + "&_format=pdf";
    window.location.href = s;
    return false;

}


// changed by abhishek
function loadUserChallengeResponseAuditDetails(_userId, _userName) {
    $("#_auditUserID").val(_userId);
    $("#_auditUserName").val(_userName);
    //alert("userId "+_userId);
    $("#userChallengeResponseauditDownload").modal();
}
function searchUsersChallengeResponseAudit() {
    var val1 = encodeURIComponent(document.getElementById('_auditStartDateChallenge').value);
    var val2 = encodeURIComponent(document.getElementById('_auditEndDateChallenge').value);
    var val4 = encodeURIComponent(document.getElementById('_auditUserID').value); // changed
    var val5 = document.getElementById('_auditUserName').value; // changed
//    alert(val5);

    if (document.getElementById('_auditStartDateChallenge').value.length == 0 || document.getElementById('_auditEndDateChallenge').value.length == 0) {
        Alert4Msg("Date Range is not selected!!!");
        return;
    }
    var s = './userPasswordAudit.jsp?_startdate=' + val1 + "&_enddate=" + val2 + "&_auditUserID=" + val4 +"&_auditUserName="+ val5 +"&_itemType=CHALLENGERESPONSE";
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
//            $('#licenses_data_table').html(data);
            $('#auditTable').html(data);
            $("#userChallengeResponseauditDownload").modal('hide');

        }
    });
}

//function userReportCSV(_itemType) {
//   var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
//    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
//    var val4 = encodeURIComponent(document.getElementById('_auditUserID').value);
//    var s = './getuseraudits?_startdate=' + val1 + "&_enddate=" + val2 + "&_auditUserID=" + val4  +"&_itemType="+_itemType + "&_format=1";
//    window.location.href = s;
//    return false;
//}
//
//function userReportPDF(_itemType) {
//    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
//    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
//    var val4 = encodeURIComponent(document.getElementById('_auditUserID').value);
//    var s = './getuseraudits?_startdate=' + val1 + "&_enddate=" + val2 + "&_auditUserID=" + val4 +"&_itemType="+_itemType + "&_format=0";
//    window.location.href = s;
//    return false;
//}
//
//function userReportTXT(_itemType) {
//    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
//    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
//    var val4 = encodeURIComponent(document.getElementById('_auditUserID').value);
//    var s = './getuseraudits?_startdate=' + val1 + "&_enddate=" + val2 + "&_auditUserID=" + val4 +"&_itemType="+_itemType+ "&_format=2";
//    window.location.href = s;
//    return false;
//}

// Added by abhishek
function generateChallengeResponseTable() {
    //window.location.href = "./twoWayAuthReport.jsp";
    //$('#generatereportButton').attr("disabled", true);
    document.getElementById("graphBar").innerHTML="";
    var val3 = encodeURIComponent(document.getElementById('startdate').value);
    var val4 = encodeURIComponent(document.getElementById('enddate').value);
    var ele = document.getElementById("REPORT");
    ele.style.display = "block";
//    var ele = document.getElementById("refreshButton");
//    ele.style.display = "block";
    //var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    //var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    if (document.getElementById('startdate').value.length == 0 || document.getElementById('enddate').value.length == 0) {
         Alert4Cert("<span><font color=red>Date Range is not selected!!!</font></span>");
        return;
    }
    var day_data = DonutChartCA(val3, val4);
    Morris.Donut({
        element: 'graphDount',
        data: day_data
    });
    var day_data = BarChartCA(val3, val4);
    Morris.Bar({
        element: 'graphBar',
        data: day_data,
        xkey: 'label',
        ykeys: ['value'],
        labels: ['value'],
        barColors: function (type) {
            if (type === 'bar') {
                return '#0066CC';
            }
            else {
                return '#0066CC';
            }
        }
    });
    //pleaseWaitDiv.modal();
    var s = './challengeResponseQnATable.jsp?&_startdate=' + val3 + '&_enddate=' + val4 +"&_itemType=CHALLENGERESPONSE";
    
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#auth_table_main').html(data);
            
        }
        
    });
    //pleaseWaitDiv.modal('hide');
}

function downloadCAReport(type, startdate, enddate) {
    var s = './DownloadCAAudit?_reporttype=' + type + "&_startDate=" + startdate + "&_endDate=" + enddate ;
    window.location.href = s;
    return false;
}

function DonutChartCA(val3, val4) {
    var s = './CAdonutchart?_startDate=' + val3 + "&_endDate=" + val4 +"&_itemType=CHALLENGERESPONSE";
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}
function BarChartCA(val3, val4) {
    var s = './CAbarchart?_startDate=' + val3 + "&_endDate=" + val4 +"&_itemType=CHALLENGERESPONSE"; 
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}