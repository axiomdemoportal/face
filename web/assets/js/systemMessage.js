/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function strcmpmessage(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function Alert4message(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function (result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}


function RefresList() {
    //alert("im here");
    window.location.href = "ListSystemMessage.jsp";
}



function addmessagesetting() {

    var s = './AddSystemMessage';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#AddNewOperatorForm").serialize(),
        success: function (data) {
            if (strcmpmessage(data._result, "error") == 0) {
                Alert4message("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpmessage(data._result, "success") == 0) {
                // $('#addUser-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4message("<span><font color=blue>" + data._message + "</font></span>");
                window.setTimeout(RefresList, 2000);
            }
        }
    });


}

function addmessagetracking(msgid, alertto) {
//    alert(msgid, alertto);
    var s = './AddMessageTracking?_msgid=' + msgid + '&_alertto=' + alertto;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#AddNewOperatorForm").serialize(),
        success: function (data) {
            if (strcmpmessage(data._result, "error") == 0) {
//                Alert4message("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpmessage(data._result, "success") == 0) {
                // $('#addUser-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                Alert4message("<span><font color=blue>" + data._message + "</font></span>");
                window.setTimeout(RefresList, 2000);
            }
        }
    });


}




function deletemessagesettingentry(sid) {
//    alert("im deleting");
//    alert(sid);
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function (result) {
        if (result == false) {
        } else {
            var s = './Deletemessagesettingentry?_sid=' + encodeURIComponent(sid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function (data) {
                    if (strcmpmessage(data._result, "success") == 0) {
                        Alert4message("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefresList, 2000);
                    } else {
                        Alert4message("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}

function ChangemessageSettingStatus(name, status, uidiv) {
    //alert(name);
    var s = './ChangesystemmessageStatus?_status=' + status + '&_messageno=' + name;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpmessage(data._result, "error") == 0) {
                Alert4message("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpmessage(data._result, "success") == 0) {
                Alert4message("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}

function loadEditmessageDetails(sid) {
    //alert(sid);
    var s = './GetSystemMessage?_sid=' + encodeURIComponent(sid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpmessage(data._result, "success") == 0) {
                $('#_messagenoedit').val(data._messagenoedit);
                $('#_messagebodyedit').val(data._messagebodyedit);
                $('#_alerttoedit').val(data._alerttoedit);
                $('#editmessage-result').html("");
                $("#editmessageDetails").modal();
            } else {
                Alert4message("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}

function editsystemmessagedetails() {
    $('#editmessageButton').attr("disabled", true);
    var s = './EditsystemmessageSettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editsytemmessageForm").serialize(),
        success: function (data) {
            if (strcmpmessage(data._result, "error") == 0) {
                $('#editmessage-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#editmessageButton').attr("disabled", false);
                //ClearEditOperatorForm();
            }
            else if (strcmpmessage(data._result, "success") == 0) {
                $('#editmessage-result').html("<span><font color=blue>" + data._message + "</font></span>");
                //ClearEditOperatorForm();
                window.setTimeout(RefresList, 2000);
            }
        }
    });
}

function editsnmpdetails() {
    // alert("i am here");
    //alert(_empid);

    $('#editUserButton').attr("disabled", true);
    var s = './EditsnmpSettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editEmployeeForm").serialize(),
        success: function (data) {
            if (strcmpmessage(data._result, "error") == 0) {
                // $('#addUser-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//                $('#editUserButton').attr("disabled", false);
            }
            else if (strcmpmessage(data._result, "success") == 0) {
                // $('#addUser-result').html("<span><font color=blue>" + data._message + "</font></span>");
                $('#editUserButton').attr("disabled", false);
                //window.setTimeout(RefreshUsersList, 2000);
                //$('#_snmpipedit').val("");
                $('#_snmpportedit').val("");
                $('#_leveledit').val("");
                $('#_aliveedit').val("");
                $('#_erroredit').val("");
                $('#_warningedit').val("");
                $('#_statusedit').val("");
            }
        }
    });
}

function editsnmpserver() {
    //alert(name);
    $('#greetings').attr("disabled", true);

    var s = './EditsnmpSettings?';

    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editOperatorForm").serialize(),
        success: function (data) {
            if (strcmpmessage(data._result, "error") == 0) {
                // $('#addUser-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4message("<span><font color=blue>" + data._message + "</font></span>");
                $('#buttonEditOperatorSubmit').attr("disabled", false);
            }
            else if (strcmpmessage(data._result, "success") == 0) {
                // $('#addUser-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4message("<span><font color=blue>" + data._message + "</font></span>");
                $('#buttonEditOperatorSubmit').attr("disabled", false);
                window.setTimeout(RefresList, 2000);

                $('#_snmpipedit').val("");
                $('#_snmpportedit').val("");
                $('#_leveledit').val("");
                $('#_aliveedit').val("");
                $('#_erroredit').val("");
                $('#_warningedit').val("");
                $('#_statusedit').val("");
            }
        }
    });
}

