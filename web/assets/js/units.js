
function strcmpUnits(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4Units(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function (result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}


function RefreshUnitsList() {
    window.location.href = "./UnitsList.jsp"
}

function editUnits() {
    var s = './edittemplates';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#messageedittemplateform").serialize(),
        success: function (data) {
            if (strcmpTemplates(data._result, "error") == 0) {
                $('#edittemplateM-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4Templates(data._message);
            }
            else if (strcmpTemplates(data._result, "success") == 0) {
                $('#edittemplateM-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4Templates(data._message);
                $('#buttonEditMessage').attr("disabled", true);

            }
        }
    });
}

function Alert4Resource(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}

////shailendra

function strcmpUsers(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4Users(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}


function InvalidRequest(id) {
    var s = './reportResponse?_requestID=' + id;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpUsers(data._result, "error") == 0) {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpUsers(data._result, "success") == 0) {
                Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}

function getunitRepprts()
{
    var val = document.getElementById('_searchtext').value;
    var startDate = document.getElementById("userstartdate").value;
    var enddate = document.getElementById("userenddate").value;
    var parts = startDate.split('/');
    var d1 = Number(parts[2] + parts[1] + parts[0]);
    parts = enddate.split('/');
    var d2 = Number(parts[2] + parts[1] + parts[0]);
   if(enddate == ""){
       Alert4Resource("<span><font color=red>To date cannot be blank!!!</font></span>");
        return;
   }else if(d1 > d2){
       Alert4Resource("<span><font color=red>From date is greater then To date!!!</font></span>");
        return;
   }else if(val === '-1'){
       Alert4Resource("<span><font color=red>Please select unit name !!!</font></span>");
        return ;
    }
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    if (val.length < 1) {
        Alert4Resource("<span><font color=red>Search keyword cannot be blank!!!</font></span>");
        return;
    }
   
      s = './unitReportTable.jsp?_searchtext=' + val+"&startdate="+startDate+"&enddate="+enddate+"&status="+status;
         pleaseWaitDiv.modal();
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#units_table_main').html(data);
            pleaseWaitDiv.modal('hide');
        }
    });
}



function removeUnits(_tid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function (result) {
        if (result == false) {
        } else {
            var s = './removeUnit?_tid=' + _tid;

            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function (data) {
                    if (strcmpUnits(data._result, "success") == 0) {
                        Alert4Units("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshUnitsList, 2000);

                    } else {
                        Alert4Units("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}










function ChangeType(value) {

    if (value === 1) {
        $('#_types').val("1");
        $('#_type-primary-sms').html("Mobile");
    }
    else if (value === 2) {
        $('#_types').val("2");
        $('#_type-primary-sms').html("Email");

    }


}

function changeUnitsStatus(_tid, _status, uidiv) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function (result) {
        if (result == false) {
        } else {

            var s = './changeUnitsStatus?_status=' + _status + '&_unitId=' + encodeURIComponent(_tid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function (data) {
                    if (strcmpUnits(data._result, "success") == 0) {
                        Alert4Units("<span><font color=blue>" + data._message + "</font></span>");
                        var uiToChange = '#' + uidiv;
                        $(uiToChange).html(data._value);

                    } else {
                        Alert4Units("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}
 var validate = function(password){
    valid = true;

    var validation = [/[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/]/]

    $.each(validation, function(i){
        if(this)
            $('.form table tr').eq(i+1).attr('class', 'check');
        else{
            $('.form table tr').eq(i+1).attr('class', '');
            valid = false
        }
    });
    
    return(valid);

}


function addNewUnit() {
    $('#addnewUnitSubmitBut').attr("disabled", true);
    
    var spChar=/k/;
  var password=document.getElementById("_unitName").value;
 if((password.match(/[_~\-!@#\$%\^&\*\(\)]+$/))==null)
 {
     
 }else{
     
     alert("Do not use any special characters in unit name");
     return;
     
 }
  
    var s = './addUnit';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#AddNewUnitForm").serialize(),
        success: function (data) {
            if (strcmpUnits(data._result, "error") == 0) {
                $('#add-new-unit-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#addnewUnitSubmitBut').attr("disabled", false);
                //ClearAddOperatorForm();
                //window.setTimeout(RefreshOperators, 3000);
            }
            else if (strcmpUnits(data._result, "success") == 0) {
                $('#add-new-unit-result').html("<span><font color=blue>" + data._message + "</font></span>");
                //ClearAddOperatorForm();
                window.setTimeout(RefreshUnitsList, 3000);
            }
        }
    });
}

  function doSequence(optionclass){
      
      var selectList = $('#_unitID option');

selectList.sort(function(a,b){
    a = a.value;
    b = b.value;

    return a-b;
});

$('#_unitID').html(selectList);
      
//    var options = $(optionclass);
//    var arr = options.map(function(_, o) {
//        return {
//            t: $(o).text(),
//            v: o.value
//        };
//    }).get();
//    arr.sort(function(o1, o2) {
//        return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0;
//    });
//    options.each(function(i, o) {
//        console.log(i);
//        o.value = arr[i].v;
//        $(o).text(arr[i].t);
//    });
};

function enabelAdd()
{ 
    $('#addnewUnitSubmitBut').attr("disabled", false);
    
}



function loadEditUnitDetails(_unitName, _unitId,_thresholdLimitE) {
//    alert(_thresholdLimitE);
    $("#_unitNameE").val(_unitName);
    $("#_unitId").val(_unitId);
    $("#_oldunitNameE").val(_unitName);
    $("#_thresholdLimitO").val(_thresholdLimitE);
    $("#_thresholdLimitE").val(_thresholdLimitE);
    $("#editUnit").modal();
}

function restrictLocation(_unitName, _unitId) {
    $("#_unitNameE").val(_unitName);
    $("#_unitId").val(_unitId);
    $("#_oldunitNameE").val(_unitName);
    $("#editUnit").modal();
}


function loadShiftUnitDetails(_unitName, _unitId) {
    $("#_oldunitNameS").val(_unitName);
    $("#_unitIdS").val(_unitId);
//$("#_oldunitNameE").val(_unitName);
    $("#shiftUnit").modal();
}
function loadAssignTokens(_unitName, _unitId) {
    $("#_oldunitNameA").val(_unitName);
    $("#_unitIdA").val(_unitId);
    $("#AssignTokens").modal();
}

function loadShiftTokensTokens(_unitName, _unitId) {
    $("#_oldunitNameT").val(_unitName);
    $("#_unitIdT").val(_unitId);
    $("#ShiftFreeHWTokens").modal();
}


function editUnit() {
    $('#buttonEditUnitSubmit').attr("disabled", true);
    var s = './editUnit';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editUnitForm").serialize(),
        success: function (data) {
            if (strcmpOpr(data._result, "error") == 0) {
                $('#editunit-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#buttonEditUnitSubmit').attr("disabled", false);
                //ClearEditOperatorForm();
            }
            else if (strcmpOpr(data._result, "success") == 0) {
                $('#editunit-result').html("<span><font color=blue>" + data._message + "</font></span>");
                //ClearEditOperatorForm();
                window.setTimeout(RefreshUnitsList, 3000);
            }
        }
    });
}
function shiftUnit() {
    $('#buttonShiftUnitSubmit').attr("disabled", true);
    var s = './shiftUnitsOperators';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#shiftUnitForm").serialize(),
        success: function (data) {
            if (strcmpUnits(data._result, "error") == 0) {
                $('#shiftunit-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#buttonEditUnitSubmit').attr("disabled", false);
                //ClearEditOperatorForm();
            }
            else if (strcmpUnits(data._result, "success") == 0) {
                $('#shiftunit-result').html("<span><font color=blue>" + data._message + "</font></span>");
                //ClearEditOperatorForm();
                window.setTimeout(RefreshUnitsList, 3000);
            }
        }
    });
}

function UploadFreeTokensFile() {
    $('#buttonUploadFreeTokens').attr("disabled", true);
    var s = './uploadTokensFile?_type=' + 1;
    $.ajaxFileUpload({
        fileElementId: 'fileFreeTokenToUploadEAD',
        url: s,
        dataType: 'json',
        success: function (data, status) {

            if (strcmpUnits(data.result, "error") == 0) {
                Alert4Units("<span><font color=red>" + data.message + "</font></span>");
            }
            else if (strcmpUnits(data.result, "success") == 0) {
                Alert4Units("<span><font color=blue>" + data.message + "</font></span>");
                $('#buttonUploadFreeTokens').attr("disabled", false);
            }
        },
        error: function (data, status, e)
        {
            alert(e);
        }
    });
}
function UploadSifftTokensFile() {
    $('#buttonUploadFreeTokensS').attr("disabled", true);
    var s = './uploadTokensFile?_type=' + 2;
    $.ajaxFileUpload({
        fileElementId: 'fileFreeTokenToUploadEADS',
        url: s,
        dataType: 'json',
        success: function (data, status) {

            if (strcmpUnits(data.result, "error") == 0) {
                Alert4Units("<span><font color=red>" + data.message + "</font></span>");
            }
            else if (strcmpUnits(data.result, "success") == 0) {
                Alert4Units("<span><font color=blue>" + data.message + "</font></span>");
                $('#buttonUploadFreeTokensS').attr("disabled", false);
            }
        },
        error: function (data, status, e)
        {
            alert(e);
        }
    });
}

function AssignTokenToUnit() {
    $('#buttonAssignTokenUnitSubmit').attr("disabled", true);
    var s = './assignTokenToUnit';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#AssignTokenForm").serialize(),
        success: function (data) {
            if (strcmpUnits(data._result, "error") == 0) {
                $('#assign-unit-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#buttonAssignTokenUnitSubmit').attr("disabled", false);
                //ClearEditOperatorForm();
            }
            else if (strcmpUnits(data._result, "success") == 0) {
                $('#assign-unit-result').html("<span><font color=blue>" + data._message + "</font></span>");
                //ClearEditOperatorForm();
                window.setTimeout(RefreshUnitsList, 3000);
            }
        }
    });
}
function ShiftTokenToUnit() {
    $('#buttonAssignTokenUnitSubmitS').attr("disabled", true);
    var s = './shiftTokensInUnits';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#ShiftTokenForm").serialize(),
        success: function (data) {
            if (strcmpUnits(data._result, "error") == 0) {
                $('#assign-unit-resultS').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#buttonAssignTokenUnitSubmitS').attr("disabled", false);
                //ClearEditOperatorForm();
            }
            else if (strcmpUnits(data._result, "success") == 0) {
                $('#assign-unit-resultS').html("<span><font color=blue>" + data._message + "</font></span>");
                //ClearEditOperatorForm();
                window.setTimeout(RefreshUnitsList, 3000);
            }
        }
    });
}

//ashish
$(document).ready(function () {
    $('#_selectedType').change(function () {
        var selectedValue = $(this).val();
        if (selectedValue === 'No Restriction')
        {
            document.getElementById("country").style.display = 'none';
            document.getElementById("_StateName").style.display = 'none';
            document.getElementById("_City").style.display = 'none';
            document.getElementById("_Pin").style.display = 'none';
        }
        else if (selectedValue === 'Country')
        {
            document.getElementById("country").style.display = 'block';
            document.getElementById("_StateName").style.display = 'none';
            document.getElementById("_City").style.display = 'none';
            document.getElementById("_Pin").style.display = 'none';
        }
        else if (selectedValue === 'State')
        {
            document.getElementById("country").style.display = 'block';
            document.getElementById("_StateName").style.display = 'block';
            document.getElementById("_City").style.display = 'none';
            document.getElementById("_Pin").style.display = 'none';
        }
        else if (selectedValue === 'City')
        {
            document.getElementById("country").style.display = 'block';
            document.getElementById("_StateName").style.display = 'block';
            document.getElementById("_City").style.display = 'block';
            document.getElementById("_Pin").style.display = 'none';
        }
        else if (selectedValue === 'Pincode')
        {
            document.getElementById("country").style.display = 'block';
            document.getElementById("_StateName").style.display = 'block';
            document.getElementById("_Pin").style.display = 'block';
            document.getElementById("_City").style.display = 'none'
        }

    });


});

function addGroup()
{
    var s = './AddGroup';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#AddNewUnitForm").serialize(),
        success: function (data) {
            $("#addNewUnit").modal('hide');
            if (strcmpUnits(data._result, "error") == 0) {
                Alert4Units("<h2><font color=red>" + data._message + "</font><h2>");
            }
            else if (strcmpUnits(data._result, "success") == 0) {
                bootbox.confirm("<h2><font color=blue>" + data._message + "</font></h2>", function (result) {
                    window.location = "./UsersGroup.jsp";
                })
            }
        }
    });
}
function loadEditGroupDetails(_unitName)
{
    var s = './editGroupDetails.jsp?_unitName=' + _unitName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#editGroups').html(data);
            $("#editGroup").modal();
        }
    });

}
function editGroup()
{
    var s = './EditGroup';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editGroupForm").serialize(),
        success: function (data) {
            $("#editGroup").modal('hide');
            if (strcmpUnits(data._result, "error") == 0) {
                Alert4Units("<h2><font color=red>" + data._message + "</font><h2>");
            }
            else if (strcmpUnits(data._result, "success") == 0) {
                bootbox.confirm("<h2><font color=blue>" + data._message + "</font></h2>", function (result) {
                    window.location = "./UsersGroup.jsp";
                });
            }
        }
    });
}

function changeGroupStatus(_tid, _status, uidiv) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function (result) {
        if (result == false) {
        } else {

            var s = './ChangeGroupStatus?_status=' + _status + '&_unitId=' + encodeURIComponent(_tid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function (data) {
                    if (strcmpUnits(data._result, "success") == 0) {
                        Alert4Units("<span><font color=blue>" + data._message + "</font></span>");
                        window.location="./UsersGroup.jsp";

                    } else {
                        Alert4Units("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}

function removeGroup(groupname)
{
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function (result) {
        if (result == false) {
        } else {

            var s = './RemoveGroup?groupname=' + groupname ;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function (data) {
                    if (strcmpUnits(data._result, "success") == 0) {
                        Alert4Units("<span><font color=blue>" + data._message + "</font></span>");
                        window.location="./UsersGroup.jsp";

                    } else {
                        Alert4Units("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}

function loadShiftGroupDetails(_unitName, _unitId) {
    $("#_oldGroupNameS").val(_unitName);
    $("#_unitIdS").val(_unitId);
//$("#_oldunitNameE").val(_unitName);
    $("#shiftGroups").modal();
}

function shiftGroup() {
    var s = './ShiftGroupUsers';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#shiftGroupForm").serialize(),
        success: function (data) {
            $("#shiftGroups").modal('hide');
            if (strcmpUnits(data._result, "error") == 0) {
                Alert4Units('<h3><font color=red>' + data._message + '</font><h3>');
            }
            else if (strcmpUnits(data._result, "success") == 0) {
              bootbox.confirm("<h2><font color=blue>" + data._message + "</font></h2>", function (result) {
                    window.location = "./UsersGroup.jsp";
                });
            }
        }
    });
}


function MakeItDefault(name,id) {
     
   
    
    var s = './makeItDefault?_unitIdS=' + id + '&_oldGroupNameS= '+ name;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
       
        success: function (data) {
            $("#shiftGroups").modal('hide');
            if (strcmpUnits(data._result, "error") == 0) {
                Alert4Units('<h3><font color=red>' + data._message + '</font><h3>');
            }
            else if (strcmpUnits(data._result, "success") == 0) {
              bootbox.confirm("<h2><font color=blue>" + data._message + "</font></h2>", function (result) {
                    window.location = "./UsersGroup.jsp";
                });
            }
        }
    });
}

function checkUnits(_status, _unitId)
{
    var s = './checkUnits.jsp?_status='+_status+'&_unitId='+_unitId;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpUnits(data._result, "error") == 0) {
                Alert4Units("<span><font color=red>" + data._message + "</font></span>");
            }else{
                window.location = './operators.jsp?_status='+_status+'&_unitId='+_unitId;
            }
                
        }
    });
}