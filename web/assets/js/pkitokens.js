function strcmpPkiTokens(a, b) {
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function Alert4PkiTokens(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}

function RefreshTokenList() {
    window.location.href = "./pkitokens.jsp";
}

//function changepkitokenstatus(status, userid,_category) {
//    var s = './changepkitokenstatus?_status=' + status + '&_userid=' + encodeURIComponent(userid) +"&_category="+_category;
//    $.ajax({
//        type: 'GET',
//        url: s,
//        dataType: 'json',
//        success: function(data) {
//            if (strcmpPkiTokens(data._result, "error") == 0) {
//                Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
//            }
//            else if (strcmpPkiTokens(data._result, "success") == 0) {
//                Alert4PkiTokens("<span><font color=blue>" + data._message + "</font></span>");
//               
//            }
//        }
//    });
//}



function sendpfxfile(userid,email) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './sendPfxfileForSSL?&_userSENDCERT=' + encodeURIComponent(userid) + '&_emailSENDCERT=' +encodeURIComponent(email);
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpPkiTokens(data._result, "error") == 0) {
                        Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpPkiTokens(data._result, "success") == 0) {
                        Alert4PkiTokens("<span><font color=blue>" + data._message + "</font></span>");

                    }
                }
            });
        }
    });

}





function sendpfxpassword(userid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './sendpfxpassword?&_userid=' + encodeURIComponent(userid);
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpPkiTokens(data._result, "error") == 0) {
                        Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpPkiTokens(data._result, "success") == 0) {
                        Alert4PkiTokens("<span><font color=blue>" + data._message + "</font></span>");

                    }
                }
            });
        }
    });


}




function renewcertificate(userid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './renewcertificate?&_userid=' + encodeURIComponent(userid);
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpPkiTokens(data._result, "error") == 0) {
                        Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpPkiTokens(data._result, "success") == 0) {
                        Alert4PkiTokens("<span><font color=blue>" + data._message + "</font></span>");

                    }
                }
            });
        }
    });


}

//function revokecertificate(userid){
//    var s = './generatecertificate?&_userid=' + encodeURIComponent(userid);
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        success: function(data) {
//            if (strcmpPkiTokens(data._result, "error") == 0) {
//                Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
//            }
//            else if (strcmpPkiTokens(data._result, "success") == 0) {
//                Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
//
//            }
//        }
//    });
//}


//function generatecertificate(userid) {
//    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
//        if (result == false) {
//        } else {
//            var s = './generatecertificate?&_userid=' + encodeURIComponent(userid);
//            $.ajax({
//                type: 'POST',
//                url: s,
//                dataType: 'json',
//                success: function(data) {
//                    if (strcmpPkiTokens(data._result, "error") == 0) {
//                        Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
//                    }
//                    else if (strcmpPkiTokens(data._result, "success") == 0) {
//                        Alert4PkiTokens("<span><font color=blue>" + data._message + "</font></span>");
//                        searchpkitokenUsers();
//                    }
//                }
//            });
//        }
//    });
//
//}
function generatecertificate(userid) {
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './generatecertificate?&_userid=' + encodeURIComponent(userid);
            pleaseWaitDiv.modal();
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
                success: function(data) {
                    pleaseWaitDiv.modal('hide');
                    if (strcmpPkiTokens(data._result, "error") == 0) {
                        Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpPkiTokens(data._result, "success") == 0) {
                        Alert4PkiTokens("<span><font color=blue>" + data._message + "</font></span>");
                        searchpkitokenUsers();
                    }

                }
            });
        }
    });

}


function loadUsersDetails(_uid) {
    var s = './loadUserDetails?_uid=' + encodeURIComponent(_uid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpPkiTokens(data._result, "success") == 0) {
                $('#_userid').val(data._uid);
                $("#_username").val(data._Name);
                $("#_email").val(data._Email);
                $("#_Phone").val(data._Phone);
                $("#_city").val(data._city);
                $("#_Country").val(data._Country);
                $("#_Organisation").val(data._Organisation);
                $("#_OrganisationUnit").val(data._OrganisationalUnit);
                $("#_State").val(data._State);
                $("#_pincode").val(data._pincode);
            } else {
                Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}


//function loadCeritificateDetails(_uid) {
//    var s = './loadUserDetails?_uid=' + encodeURIComponent(_uid);
//    $.ajax({
//        type: 'GET',
//        url: s,
//        dataType: 'json',
//        success: function(data) {
//            if (strcmpPkiTokens(data._result, "success") == 0) {
//                $('#_userid').val(data._uid);
//                $("#_username").val(data._Name);
//                $("#_email").val(data._Email);
//                $("#_Phone").val(data._Phone);
//                $("#_city").val(data._city);
//                $("#_Country").val(data._Country);
//                $("#_Organisation").val(data._Organisation);
//                $("#_OrganisationUnit").val(data._OrganisationalUnit);
//                $("#_State").val(data._State);
//                $("#_pincode").val(data._pincode);
//            } else {
//                Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
//            }
//        }
//    });
//}


//function loadUsersID(_uid) {
//
//    $.ajax({
//        type: 'GET',
//        dataType: 'json',
//        success: function(data) {
//
//            $('#_userIDHR').val(data._uid);
//        }
//
//    });
//}

function loadUsersID(_uid) {
    $("#_userIDHR").val(_uid);
    $("#HardwarePkiToken").modal();
}


//function assignhardwaretokens(_category) {
//   var userid=document.getElementById("_userIDHR").value;
//    alert(userid);
//   var serialnumber=document.getElementById("_registrationCode").value;
//   alert(userid)
//   alert(serialnumber)
//    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
//        if (result == false) {
//        } else {
//            var s = './assignnewtoken?_userid=' + encodeURIComponent(userid) +"&_ser="+serialnumber+ "&_category="+_category;
//            $.ajax({
//                type: 'GET',
//                url: s,
//                dataType: 'json',
//                success: function(data) {
//                    if (strcmpPkiTokens(data._result, "error") == 0) {
//                        Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
//                    }
//                    else if (strcmpPkiTokens(data._result, "success") == 0) {
//                        Alert4PkiTokens("<span><font color=blue>" + data._message + "</font></span>");
//                        //window.setTimeout(RefreshTokenList, 2000);
//                    }
//                }
//            });
//        }
//    });
//}

//function assignhardwaretokens(_category) {
//
//    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
//        if (result == false) {
//        } else {
//            var s = './assignnewtoken?_userid=' + encodeURIComponent(userid) + "&_ser=" + serialnumber + "&_category=" + _category;
//            $.ajax({
//                type: 'GET',
//                url: s,
//                dataType: 'json',
//                success: function(data) {
//                    if (strcmpPkiTokens(data._result, "error") == 0) {
//                        Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
//                    }
//                    else if (strcmpPkiTokens(data._result, "success") == 0) {
//                        Alert4PkiTokens("<span><font color=blue>" + data._message + "</font></span>");
//                        //window.setTimeout(RefreshTokenList, 2000);
//                    }
//                }
//            });
//        }
//    });
//}


function assignhardwaretokens(_category) {
    var userid = document.getElementById("_userIDHR").value;
    var serialnumber = document.getElementById("_registrationCode").value;

    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './assignnewtoken?_userid=' + encodeURIComponent(userid) + "&_ser=" + serialnumber + "&_category=" + _category;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpPkiTokens(data._result, "error") == 0) {
                        Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
                        $("#_registrationCode").val("");

                    }
                    else if (strcmpPkiTokens(data._result, "success") == 0) {
                        Alert4PkiTokens("<span><font color=blue>" + data._message + "</font></span>");
                        //window.setTimeout(RefreshTokenList, 2000);
                        $("#HardwarePkiToken").modal('hide');
                        searchpkitokenUsers();
                    }
                }
            });
        }
    });
}


function assignewtoken(userid, _category, uidiv) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './assignnewtoken?_userid=' + encodeURIComponent(userid) + "&_category=" + _category;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpPkiTokens(data._result, "error") == 0) {
                        Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpPkiTokens(data._result, "success") == 0) {
                        Alert4PkiTokens("<span><font color=blue>" + data._message + "</font></span>");
                        var uiToChange = '#' + uidiv;
                        $(uiToChange).html(data._value);
                    }
                }
            });
        }
    });
}


//function assignewtoken(userid,_category) {
//    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
//        if (result == false) {
//        } else {
//            var s = './assignnewtoken?_userid=' + encodeURIComponent(userid) + "&_category="+_category;
//            $.ajax({
//                type: 'GET',
//                url: s,
//                dataType: 'json',
//                success: function(data) {
//                    if (strcmpPkiTokens(data._result, "error") == 0) {
//                        Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
//                    }
//                    else if (strcmpPkiTokens(data._result, "success") == 0) {
//                        Alert4PkiTokens("<span><font color=blue>" + data._message + "</font></span>");
//                        //window.setTimeout(RefreshTokenList, 2000);
//                    }
//                }
//            });
//        }
//    });
//}

function PKIAudits(_userid, _itemtype, _duration) {

    var s = './pkitokensaudit?_userid=' + encodeURIComponent(_userid) + "&_itemType=" + _itemtype + "&_duration=" + _duration + "&_format=pdf";
    window.location.href = s;
    return false;

}





function sendregistrationcode(userid, _category) {

    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            //var s = './sendregistration?_userid=' + userid;
            var s = './sendpkitokenregistration?_userid=' + encodeURIComponent(userid) + "&_category=" + _category;

            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpPkiTokens(data._result, "success") == 0) {
                        Alert4PkiTokens("<span><font color=blue>" + data._message + "</font></span>");
                    } else {
                        Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}

function assignsoftwaretoken(token, userid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './assignsoftwaretoken?_token=' + token + '&_userid=' + encodeURIComponent(userid);
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpPkiTokens(data._result, "error") == 0) {
                        Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpPkiTokens(data._result, "success") == 0) {
                        Alert4PkiTokens("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshTokenList, 2000);

                    }
                }
            });
        }
    });
}
function assignhardwaretoken() {
    var s = './assignhardwaretoken';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#HardwareRegistrationform").serialize(),
        success: function(data) {
            if (strcmpPkiTokens(data._result, "error") == 0) {
                Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpPkiTokens(data._result, "success") == 0) {
                Alert4PkiTokens("<span><font color=blue>" + data._message + "</font></span>");
                window.setTimeout(RefreshTokenList, 2000);

            }
        }
    });

}

function changehardstatus(status, userid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './changehardwarestatus?_status=' + status + '&_userid=' + encodeURIComponent(userid);
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpPkiTokens(data._result, "error") == 0) {
                        Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpPkiTokens(data._result, "success") == 0) {
                        Alert4PkiTokens("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshTokenList, 2000);
                    }
                }
            });
        }
    });

}



function searchpkitokenUsers() {
    var val = document.getElementById('_keyword').value;
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');

//    if (val.length <= 3) {
//        Alert4PkiTokens("Keyword should be meaningful and/or more than 3 characters");
//        return;
//    }

    if (val.length < 1) {
        //Alert4Tokens("Keyword should be meaningful and/or more than 3 characters");
        Alert4PkiTokens("Search keyword cannot be blank!!!");
        return;
    }

    var s = './pkitokenstable.jsp?_searchtext=' + val;
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#pkitoken_table_main').html(data);
            pleaseWaitDiv.modal('hide');
        }
    });
}

function generatePkiTable() {
    var val = document.getElementById('_changePkiCategory').value;
    var val1 = document.getElementById('_changePkiStatus').value;
    
    var startDate = document.getElementById('pkistartdate').value;
    var endDate = document.getElementById('pkienddate').value;
//    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var s = './pkitokenreportstable.jsp?_changePkiCategory=' + val + "&_changePkiStatus=" + val1 +"&_startDate="+startDate+"&_endDate="+endDate;
//    pleaseWaitDiv.modal();
$('#licenses_data_table').html("<h3>Loading....</h3>");
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#licenses_data_table').html(data);
            var day_data = null;
            day_data = pkiDonutChart(val,startDate,endDate);
            Morris.Donut({
                element: 'pkidonutchart',
                data: day_data
            });
            //Bar Chart
            var day_data1 = null;
            day_data1 = pkiBarChart(val,startDate,endDate);
            Morris.Bar({
                element: 'pkibarchart',
                data: day_data1,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {

                        return '#0066CC';
                    }
                    else {

                        return '#0066CC';
                    }
                }
            });
            
        }
        
    });

}


function pkiBarChart(val1,val2,val3) {
    var s = './pkibarchart?_changePkiCategory=' + val1+"&_startDate="+val2+"&_endDate="+val3;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}


function pkiDonutChart(val1,val2,val3) {
    var s = './pkidonutchart?_changePkiCategory=' + val1+"&_startDate="+val2+"&_endDate="+val3;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}

function ChangePkiCategory(value) {
    //1 for enabled
    //0 for disabled
    if (value === 1) {
        $('#_changePkiCategory').val("1");
        $('#_change_pki_category_Axiom').html("Mobile Pki Token");
    } else if (value === 2) {
        $('#_changePkiCategory').val("2");
        $('#_change_pki_category_Axiom').html("Hardware Pki Token");
    }
}


//function ChangePkiStateType(value) {
//
//    if (value === 1) {
//        $('#_changePkiStatus').val("1");
//        $('#_change_Pki_Status_Axiom').html("Active State");
//    } else if (value === -1) {
//        $('#_changePkiStatus').val("-1");
//        $('#_change_Pki_Status_Axiom').html("Locked State");
//    } else if (value === 0) {
//        $('#_changePkiStatus').val("0");
//        $('#_change_Pki_Status_Axiom').html("Assigned State");
//    } else if (value === -10) {
//        $('#_changePkiStatus').val("-10");
//        $('#_change_Pki_Status_Axiom').html("Unassigned State");
//    } else if (value === -2) {
//        $('#_changePkiStatus').val("-2");
//        $('#_change_Pki_Status_Axiom').html("Suspended State");
//    } else if (value === 2) {
//        $('#_changePkiStatus').val("2");
//        $('#_change_Pki_Status_Axiom').html("Show All States");
//    }
//}


function ChangePkiStateType(value) {

    if (value === 1) {
        $('#_changePkiStatus').val("1");
        $('#_change_Pki_Status_Axiom').html("Active State");
    } else if (value === -1) {
        $('#_changePkiStatus').val("-1");
        $('#_change_Pki_Status_Axiom').html("Locked State");
    } else if (value === 0) {
        $('#_changePkiStatus').val("0");
        $('#_change_Pki_Status_Axiom').html("Assigned State");
    } else if (value === -5) {
        $('#_changePkiStatus').val("-5");
        $('#_change_Pki_Status_Axiom').html("Lost State");
    } else if (value === -2) {
        $('#_changePkiStatus').val("-2");
        $('#_change_Pki_Status_Axiom').html("Suspended State");
    } else if (value === 2) {
        $('#_changePkiStatus').val("2");
        $('#_change_Pki_Status_Axiom').html("Show All States");
    } else if (value === -10) {
        $('#_changePkiStatus').val("-10");
        $('#_change_Pki_Status_Axiom').html("Free And Available State");
    }
}

//function uploadCustomerDetails(_uid) {
//     $('#_usersid').val(_uid);
//     $("#kycupload").modal();
//}



function loadCertificateDetails(_uid) {
    var s = './loadCertificateDetails?_uid=' + encodeURIComponent(_uid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpPkiTokens(data._result, "success") == 0) {
//                alert("s"+data._srno);
//                $('#_srno').val(data._srno);
                 $('#_srnoCertifiacte').val(data._srno);
                $("#_algoname").val(data._algoname);
                $("#_issuerdn").val(data._issuerdn);
                $("#_notafter").val(data._notafter);
                $('#_notbefore').val(data._notbefore);
                $("#_subjectdn").val(data._subjectdn);
                $('#_version').val(data._version);
                $("#certificateDetails").modal();

            } else {
                Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}

function loadrevoke(userid) {
    $("#_userCR").val(userid);
    $("#RevokeCertificate").modal();
}

function revokecertificate() {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './revokecertificate';
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
                data: $("#RevokeCertificateform").serialize(),
                success: function(data) {
                    if (strcmpPkiTokens(data._result, "error") == 0) {
                        Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
                        $("#_reason").val("");

                    }
                    else if (strcmpPkiTokens(data._result, "success") == 0) {
                        Alert4PkiTokens("<span><font color=blue>" + data._message + "</font></span>");
                        searchpkitokenUsers();
                        $("#RevokeCertificate").modal('hide');


                    }
                }
            });
        }
    });


}

function loadsendcertfile(userid) {
    $("#_userSENDCERT").val(userid);
    $("#SendCertificate").modal();
}

function loadsendcertfile1(userid) {
    $("#_userSENDCERTPFX").val(userid);
    $("#Sendpfxfile").modal();
}


function sendcertificatefile() {


    var _emailidToSendTo = $("#_emailSENDCERT").val();
    var userid = $("#_userSENDCERT").val();

    var s = './sendcertfile?&_userSENDCERT=' + encodeURIComponent(userid) + "&_emailSENDCERT=" + _emailidToSendTo;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#SendCertificateform").serialize(),
        success: function(data) {
            if (strcmpPkiTokens(data._result, "error") == 0) {
                Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpPkiTokens(data._result, "success") == 0) {
                Alert4PkiTokens("<span><font color=blue>" + data._message + "</font></span>");
                searchpkitokenUsers();
                $("#SendCertificate").modal('hide');
            }
        }
    });
}

function sendSSLpfxFile()
{
     var _emailidToSendTo = $("#_emailSENDCERTPFX").val();
    var userid = $("#_userSENDCERTPFX").val();

    var s = './sendPfxfileForSSL?&_userSENDCERT=' + encodeURIComponent(userid) + "&_emailSENDCERT=" + _emailidToSendTo;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#SendCertificateform").serialize(),
        success: function(data) {
            if (strcmpPkiTokens(data._result, "error") == 0) {
                Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpPkiTokens(data._result, "success") == 0) {
                Alert4PkiTokens("<span><font color=blue>" + data._message + "</font></span>");
                searchpkitokenUsers();
                $("#SendCertificate").modal('hide');
            }
        }
    });
    
 }

function changepkitokenstatus(status, userid, _category, uidiv) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './changepkitokenstatus?_status=' + status + '&_userid=' + encodeURIComponent(userid) + "&_category=" + _category;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpPkiTokens(data._result, "error") == 0) {
                        Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpPkiTokens(data._result, "success") == 0) {
                        Alert4PkiTokens("<span><font color=blue>" + data._message + "</font></span>");
                        var uiToChange = '#' + uidiv;
                        $(uiToChange).html(data._value);
                    }
                }
            });
        }
    });


}

function PKIReport() {
    var val = document.getElementById('_changePkiCategory').value;
    var val1 = document.getElementById('_changePkiStatus').value;
    var s = './pkitokensreport?_changePkiCategory=' + val + "&_changePkiStatus=" + val1 + "&_reporttype=" + 1;
    window.location.href = s;
    return false;
}

function PKIReportpdf() {
    var val = document.getElementById('_changePkiCategory').value;
    var val1 = document.getElementById('_changePkiStatus').value;
    var s = './pkitokensreport?_changePkiCategory=' + val + "&_changePkiStatus=" + val1 + "&_reporttype=" + 0;
    window.location.href = s;
    return false;
}



//function UploadkycFile() {
////    alert("here");
//    $('#buttonkycUpload').attr("disabled", true);
//    var s = './kycfileupload';
//    $.ajaxFileUpload({
//        fileElementId: 'filekycupload',
//        url: s,
//        dataType: 'json',
//        data: $("#kycuploadForm").serialize(),
//        success: function(data, status) {
//            if (strcmpPkiTokens(data.result, "error") == 0) {
//                Alert4PkiTokens("<span><font color=red>" + data.message + "</font></span>");
//            }
//            else if (strcmpPkiTokens(data.result, "success") == 0) {
//                Alert4PkiTokens("<span><font color=blue>" + data.message + "</font></span>");
//
//            }
//        },
//        error: function(data, status, e)
//        {
//            alert(e);
//        }
//    });
//}

function uploadCustomerDetails(_uid) {
//    alert(_uid);
     $('#_usersid').val(_uid);
     $("#kycupload").modal();
}  


function UploadkycFile() {
     var val = document.getElementById('_usersid').value;
//    alert(val);
    $('#buttonUpload').attr("disabled", true);
    var s = './kycfileupload?_usersid='+encodeURIComponent(val);
    $.ajaxFileUpload({
        fileElementId: 'filekycupload',
        url: s,
        dataType: 'json',
        success: function(data, status) {

            if (strcmpPkiTokens(data.result, "error") == 0) {
                Alert4PkiTokens("<span><font color=red>" + data.message + "</font></span>");

            }
            else if (strcmpPkiTokens(data.result, "success") == 0) {
                Alert4PkiTokens("<span><font color=blue>" + data.message + "</font></span>");

            }
             $("#kycupload").modal('hide');
//             $("#filekycupload").empty();
             
        },
        error: function(data, status, e)
        {
            alert(e);
        }
    });
}
function PKIReportTXT() {
    var val = document.getElementById('_changePkiCategory').value;
    var val1 = document.getElementById('_changePkiStatus').value;
    var s = './pkitokensreport?_changePkiCategory=' + val + "&_changePkiStatus=" + val1 + "&_reporttype=" + 2;
    window.location.href = s;
    return false;
}

function loadUserPKITokenAuditDetails(_userId, _userName) {
    $("#_auditUserIDPKI").val(_userId);
    $("#_auditUserNamePKI").val(_userName);
    $("#userPkiTokenauditDownload").modal();
}
function searchUsersPKITokenAuditPKI() {
    var val1 = encodeURIComponent(document.getElementById('_auditStartDatePKI').value);
    var val2 = encodeURIComponent(document.getElementById('_auditEndDatePKI').value);
    var val4 = encodeURIComponent(document.getElementById('_auditUserIDPKI').value);
    var val5 = document.getElementById('_auditUserNamePKI').value;
   
    if (document.getElementById('_auditStartDatePKI').value.length == 0 || document.getElementById('_auditEndDatePKI').value.length == 0) {
        Alert4PkiTokens("<span><font color=blue>Date Range is not selected!!!</font></span>");
        return;
    }
    var s = './userPasswordAudit.jsp?_startdate=' + val1 + "&_enddate=" + val2 + "&_auditUserID=" + val4 +"&_auditUserName="+ val5 +"&_itemType=PKITOKEN";
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
//            $('#licenses_data_table').html(data);
            $('#auditTable').html(data);
            $("#userPkiTokenauditDownload").modal('hide');

        }
    });
}
function searchUsersPKITokenAudit() {
    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
    var val4 = encodeURIComponent(document.getElementById('_auditUserID').value);
    var val5 = document.getElementById('_auditUserName').value;
//    alert(val5);

    if (document.getElementById('_auditStartDate').value.length == 0 || document.getElementById('_auditEndDate').value.length == 0) {
        Alert4PkiTokens("<span><font color=blue>Date Range is not selected!!!</font></span>");
        return;
    }
    var s = './userPasswordAudit.jsp?_startdate=' + val1 + "&_enddate=" + val2 + "&_auditUserID=" + val4 +"&_auditUserName="+ val5 +"&_itemType=PKITOKEN";
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
//            $('#licenses_data_table').html(data);
            $('#auditTable').html(data);
            $("#userPkiTokenauditDownload").modal('hide');

        }
    });
}

function searchUsersPKITokenAuditv3() {
    var val1 = encodeURIComponent(document.getElementById('_auditStartDate1').value);
    var val2 = encodeURIComponent(document.getElementById('_auditEndDate1').value);
    var val4 = encodeURIComponent(document.getElementById('_auditUserIDPKI').value);
    var val5 = document.getElementById('_auditUserNamePKI').value;
   if (document.getElementById('_auditStartDate1').value.length == 0 || document.getElementById('_auditEndDate1').value.length == 0) {
        Alert4PkiTokens("<span><font color=blue>Date Range is not selected!!!</font></span>");
        return;
    }
    var s = './userPasswordAudit.jsp?_startdate=' + val1 + "&_enddate=" + val2 + "&_auditUserID=" + val4 +"&_auditUserName="+ val5 +"&_itemType=CERTIFICATEMANAGEMENT";
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
//            $('#licenses_data_table').html(data);
            $('#auditTable').html(data);
            $("#userPkiTokenauditDownload").modal('hide');

        }
    });
}


//function userReportCSV(_itemType) {
//   var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
//    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
//    var val4 = encodeURIComponent(document.getElementById('_auditUserID').value);
//    var s = './getuseraudits?_startdate=' + val1 + "&_enddate=" + val2 + "&_auditUserID=" + val4  +"&_itemType="+_itemType + "&_format=1";
//    window.location.href = s;
//    return false;
//}
//
//function userReportPDF(_itemType) {
//    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
//    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
//    var val4 = encodeURIComponent(document.getElementById('_auditUserID').value);
//    var s = './getuseraudits?_startdate=' + val1 + "&_enddate=" + val2 + "&_auditUserID=" + val4 +"&_itemType="+_itemType + "&_format=0";
//    window.location.href = s;
//    return false;
//}
//
//function userReportTXT(_itemType) {
//    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
//    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
//    var val4 = encodeURIComponent(document.getElementById('_auditUserID').value);
//    var s = './getuseraudits?_startdate=' + val1 + "&_enddate=" + val2 + "&_auditUserID=" + val4 +"&_itemType="+_itemType+ "&_format=2";
//    window.location.href = s;
//    return false;
//}

function InvalidRequestPKIToken(id) {
    var s = './reportResponse?_requestID=' + id;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpPkiTokens(data._result, "error") == 0) {
                Alert4PkiTokens("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpPkiTokens(data._result, "success") == 0) {
                Alert4PkiTokens("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}