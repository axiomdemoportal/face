function strcmpSSO(a, b) {
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4SSO(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
    });
}

function searchSSOUsers() {
    var val = document.getElementById('_ssokeyword').value;
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    if (val.length < 1) {
        //Alert4Tokens("Keyword should be meaningful and/or more than 3 characters");
        Alert4Users("<font color=red>Search keyword cannot be blank!!!</font>");
        return;
    }
    var s = './ssoMappingTable.jsp?_searchtext=' + encodeURIComponent(val);
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#sso_table_main').html(data);
            pleaseWaitDiv.modal('hide');
        }
    });
}

function addSSODetails(userid) {
    window.location = './addSSOMapping.jsp?_searchtext=' + encodeURIComponent(userid);
}
function editSSODetails(userid) {
    window.location = './editSSOMapping.jsp?_searchtext=' + encodeURIComponent(userid);
}
function addSSOMapping(userid, len) {
    var s = './AddSSOMapping?userid=' + userid + "&len=" + len;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#ssoMappingForm").serialize(),
        success: function(data) {
            if (strcmpSSO(data.result, "error") == 0) {
                Alert4SSO("<span><font color=red>" + data.message + "</font></span>");
            } else {
                Alert4SSO("<span><font color=blue>" + data.message + "</font></span>");
            }
        }
    });
}