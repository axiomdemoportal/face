/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function strcmppass(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4pass(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}





function getPassportDetails(_uid) {
    $('#passDetail').attr("disabled", false);
    var s = './getPassportDetails?_uid=' + encodeURIComponent(_uid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmppass(data._result, "success") == 0) {
                
               // $('#idpassDetails').html(data._NamePass+"'s Details");
                $('#_userId').val(data._uid);
                $("#_NamePass").val(data._NamePass);
                 $("#_BirthDate").val(data._BirthDate);
                $("#_Nationality").val(data._Nationality);
                $("#_Expirydate").val(data._Expirydate);
                $("#_CreatedOn").val(data._CreatedOn);
                $("#_UpdatedOn").val(data._UpdatedOn);
                $("#_Passportnumber").val(data._Passportnumber);
                $("#_Sex").val(data._Sex);
                $("#_CurrentAttempts").val(data._CurrentAttempts);
                $("#_Issueplace").val(data._Issueplace);
                $("#_Status").val(data._Status);
                $('#passDetails-result').html("");
                $("#passDetail").modal();
            } else {
                Alert4pass("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}
function loadPassportDetails(_uid) {
    $('#editPassDetailsButton').attr("disabled", false);
    var s = './getPassportDetails?_uid=' + encodeURIComponent(_uid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmppass(data._result, "success") == 0) {
                
                $('#idpassDetails').html(data._NamePass+"'s Details");
                $('#_userId').val(data._uid);
                $("#_NamePass").val(data._NamePass);
                 $("#_BirthDate").val(data._BirthDate);
                $("#_Nationality").val(data._Nationality);
                $("#_Expirydate").val(data._Expirydate);
                $("#_CreatedOn").val(data._CreatedOn);
                $("#_UpdatedOn").val(data._UpdatedOn);
                $("#_Passportnumber").val(data._Passportnumber);
                $("#_Sex").val(data._Sex);
                $("#_CurrentAttempts").val(data._CurrentAttempts);
                $("#_Issueplace").val(data._Issueplace);
                $("#_Status").val(data._Status);
                $('#passDetails-result').html("");
                $("#passDetail").modal();
            } else {
                Alert4pass("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}


function editpassdetails() {
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var s = './editpassportdetails';
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editPassDetailsForm").serialize(),
        success: function(data) {
            pleaseWaitDiv.modal('hide');
            if (strcmppass(data._result, "error") == 0) {
                $('#passDetail-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                
            }
            else if (strcmppass(data._result, "success") == 0) {
                $('#passDetail-result').html("<span><font color=blue>" + data._message + "</font></span>");
                
                
            }
        }
    });
}
