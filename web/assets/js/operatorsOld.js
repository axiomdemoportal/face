function Alert4Operator(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function (result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}

function RefreshOperators() {
    window.location.href = "./operatorsmain.jsp";
}

function strcmpOpr(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function doSequence(optionclass) {
    var options = $(optionclass);
    var arr = options.map(function (_, o) {
        return {
            t: $(o).text().toLowerCase(),
            v: o.value
        };
    }).get();
    arr.sort(function (o1, o2) {
        return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0;
    });
    options.each(function (i, o) {
        console.log(i);
        o.value = arr[i].v.toLowerCase();
        $(o).text(arr[i].t);
    });
}
;


function OperatorAudits(_oprid, _duration) {
    //var durationValue = document.getElementById ('durationValueHW').value;
    //window.location.href = './getopraudits?_oprid='+encodeURIComponent(_oprid) +"&_duration="+_duration + "&_format=pdf";
    //return false;
    var s = './getopraudits?_oprid=' + encodeURIComponent(_oprid) + "&_duration=" + _duration + "&_format=pdf";
    window.location.href = s;
    return false;
//    $.ajax({
//        type: 'GET',
//        url: s,
//        success: function(data) {
//            if ( strcmpOpr(data._result,"error") == 0 ) {
//                $('#result').html("<span><font color=red>" + data._message + "</font></span></small>");
//            }
//            else if ( strcmpOpr(data._result,"success") == 0 ) {
//                $('#result').html("<span><font color=blue>" + data._message + "</font></span>");
//                window.location.href = data._url;
//            }
//        }
//    });
}




function get_time_zone_offset( ) {
    var current_date = new Date();
    return -current_date.getTimezoneOffset() / 60;
}

function ClearAddOperatorForm() {
    $('#add-new-operator-result').html("<span><font color=blue>" + "" + "</font></span>");
    $("#_oprname").val("");
    $("#_opremail").val("");
    $("#_oprphone").val("");
}

function ClearEditOperatorForm() {
    $('#editoperator-result').html("<span><font color=blue>" + "" + "</font></span>");
    $("#_oprnameE").val("");
    $("#_opremailE").val("");
    $("#_opremailE").val("");
}

function addOperator() {
    $('#addnewOperatorSubmitBut').attr("disabled", true);
    var s = './addoperator';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#AddNewOperatorForm").serialize(),
        success: function (data) {
            if (strcmpOpr(data._result, "error") == 0) {
                $('#add-new-operator-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#addnewOperatorSubmitBut').attr("disabled", false);
                //ClearAddOperatorForm();
                //window.setTimeout(RefreshOperators, 3000);
            } else if (strcmpOpr(data._result, "success") == 0) {
                $('#add-new-operator-result').html("<span><font color=blue>" + data._message + "</font></span>");
                //ClearAddOperatorForm();
                window.setTimeout(RefreshOperators, 3000);
            }
        }
    });
}


function addLdapOperator() {
//    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var s = './addldapoperator';
//    pleaseWaitDiv.modal();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#addldapoperatorForm").serialize(),
        success: function (data) {
//            pleaseWaitDiv.modal('hide');
            if (strcmpOpr(data._result, "error") == 0) {
                $('#addldapopertor-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            } else if (strcmpOpr(data._result, "success") == 0) {
                $('#addldapopertor-result').html("<span><font color=blue>" + data._message + "</font></span>");
                $('#addldapopertor').attr("disabled", true);

            }
        }
    });
}


function SearchOperators() {
    var val = document.getElementById('_keyword').value;
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');

    if (val.length < 1) {
        Alert4Users("Search keyword cannot be blank!!!");
        return;
    }

    var s = './operatorsearchtable.jsp?_searchtext=' + encodeURIComponent(val);
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#operatorsearch_table_main').html(data);
            pleaseWaitDiv.modal('hide');
        }
    });
}

function hideModal() {
    $("#_oprname").val("");
    $("#_opremail").val("");
    $("#_oprphone").val("");
    $("#_oprphone").val("");
    $("#_oprphone").val("");
    $('#addOperator').modal('hide');
}

function loadEditOperatorDetails(_orpid) {
    var s = './getoperator?_oprid=' + encodeURIComponent(_orpid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpOpr(data._result, "success") == 0) {
                $('#idEditOperatorName').html(data._name);
                $("#_oprnameE").val(data._name);
                $("#_opremailE").val(data._email);
                $("#_oprphoneE").val(data._phone);
                $("#_oprroleidE").val(data._roleid);
                $("#_oprstatusE").val(data._status);
                $("#_opridE").val(data._id);
                $("#_unitsEDIT").val(data._unitsEDIT);
                $("#_operatorTypeE").val(data._operatorTypeE);
                $('#editoperator-result').html("");
                $("#editOperator").modal();
            } else {
                Alert4Operator("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}
function loadEditOperatorAccess(_orpid) {
    $("#_oprAccessId").val(_orpid);
    $("#operatorAccess").modal();
}


function removeOperator(operatorid)
{
    var s = './removeoperator?operatorid=' + encodeURIComponent(operatorid);
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function (result) {
        if (result == false) {
        } else {
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
                data: $("#editOperatorForm").serialize(),
                success: function (data) {
                    if (strcmpOpr(data._result, "error") == 0) {
                        Alert4Operator("<span><font color=red>" + data._message + "</font></span>");
                        //ClearEditOperatorForm();
                    } else if (strcmpOpr(data._result, "success") == 0) {
                        Alert4Operator("<span><font color=blue>" + data._message + "</font></span>");
                        //ClearEditOperatorForm();
                        window.setTimeout(RefreshOperators, 3000);
                    }
                }
            });

        }
    })
}


function editoperator() {
    $('#buttonEditOperatorSubmit').attr("disabled", true);
    var s = './editoperator';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editOperatorForm").serialize(),
        success: function (data) {
            if (strcmpOpr(data._result, "error") == 0) {
                $('#editoperator-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#buttonEditOperatorSubmit').attr("disabled", false);
                //ClearEditOperatorForm();
            } else if (strcmpOpr(data._result, "success") == 0) {
                $('#editoperator-result').html("<span><font color=blue>" + data._message + "</font></span>");
                //ClearEditOperatorForm();
                window.setTimeout(RefreshOperators, 3000);
            }
        }
    });
}

//function editoperator() {
//    var opertaorId=
//  //  $('#buttonEditOperatorSubmit').attr("disabled", true);
//    var s = './removeoperator?operatorid='+;
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#editOperatorForm").serialize(),
//        success: function (data) {
//            if (strcmpOpr(data._result, "error") == 0) {
//                $('#editoperator-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//                $('#buttonEditOperatorSubmit').attr("disabled", false);
//                //ClearEditOperatorForm();
//            }
//            else if (strcmpOpr(data._result, "success") == 0) {
//                $('#editoperator-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                //ClearEditOperatorForm();
//                window.setTimeout(RefreshOperators, 3000);
//            }
//        }
//    });
//}


//function changerole(){
//    var s = './changeoprrole';
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#changerole").serialize(),
//        success: function(data) {
//            if ( strcmpOpr(data._result,"error") == 0 ) {
//                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//            }
//            else if ( strcmpOpr(data._result,"success") == 0 ) {
//                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                window.location.href = data._url;
//            }
//        }
//    });
//}


function ChangeOperatorStatus(oprid, status, uidiv) {
    var s = './changeoprstatus?_op_status=' + status + '&_oprid=' + encodeURIComponent(oprid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpOpr(data._result, "error") == 0) {
                Alert4Operator("<span><font color=red>" + data._message + "</font></span>");
                window.location.href = "./operatorsmain.jsp";
                //$('#addchannel-result').html("<span><font color=red>" + data._message + "</font></span>");
            } else if (strcmpOpr(data._result, "success") == 0) {
                Alert4Operator("<span><font color=blue>" + data._message + "</font></span>");
                //$('#addchannel-result').html("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
                window.location.href = "./operatorsmain.jsp";
            }
        }
    });
}

function ChangeOperatorType(operatorid,opearatortype)
{  var s = './changeOperatortype?_operatorId=' + encodeURIComponent(operatorid) + '&_opearatortype=' + encodeURIComponent(opearatortype);
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpOpr(data._result, "error") == 0) {
                Alert4Operator("<span><font color=red>" + data._message + "</font></span>");
                window.location.href = "./operatorsmain.jsp";
                //$('#addchannel-result').html("<span><font color=red>" + data._message + "</font></span>");
            } else if (strcmpOpr(data._result, "success") == 0) {
                Alert4Operator("<span><font color=blue>" + data._message + "</font></span>");
                //$('#addchannel-result').html("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
                window.location.href = "./operatorsmain.jsp";
            }
        }
    });
    
    
}




function ChangeOperatorAccess(oprid, status, uidiv) {
    var s = './changeOperatorAccess?_op_type=' + status + '&_oprid=' + encodeURIComponent(oprid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpOpr(data._result, "error") == 0) {
                Alert4Operator("<span><font color=red>" + data._message + "</font></span>");
                //$('#addchannel-result').html("<span><font color=red>" + data._message + "</font></span>");
            } else if (strcmpOpr(data._result, "success") == 0) {
                Alert4Operator("<span><font color=blue>" + data._message + "</font></span>");
                //$('#addchannel-result').html("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}


function ChangeOperatorRole(_roleid, _rolename, _oprid, uidiv) {
    var s = './changeoprrole?_roleId=' + _roleid + '&_oprid=' + encodeURIComponent(_oprid) + "&_roleName=" + encodeURIComponent(_rolename);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpOpr(data._result, "error") == 0) {
                Alert4Operator("<span><font color=red>" + data._message + "</font></span>");
                //$('#addchannel-result').html("<span><font color=red>" + data._message + "</font></span>");
            } else if (strcmpOpr(data._result, "success") == 0) {
                Alert4Operator("<span><font color=blue>" + data._message + "</font></span>");
                //$('#addchannel-result').html("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}



//function changestatus(){
//    var s = './changeoprstatus';
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#licenses_data_table").serialize(),
//        success: function(data) {
//            if ( strcmpOpr(data._result,"error") == 0 ) {
//                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//            }
//            else if ( strcmpOpr(data._result,"success") == 0 ) {
//                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                window.location.href = data._url;
//            }
//        }
//    });
//}


//function getopraudits(){
//    var s = './getopraudits';
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#addOperator").serialize(),
//        success: function(data) {
//            if ( strcmpOpr(data._result,"error") == 0 ) {
//                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//            }
//            else if ( strcmpOpr(data._result,"success") == 0 ) {
//                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                window.location.href = data._url;
//            }
//        }
//    });
//}
//
//function listoperators(){
//    var s = './listoperators';
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#addOperator").serialize(),
//        success: function(data) {
//            if ( strcmpOpr(data._result,"error") == 0 ) {
//                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//            }
//            else if ( strcmpOpr(data._result,"success") == 0 ) {
//                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                window.location.href = data._url;
//            }
//        }
//    });
//}

//function resendpassword(){
//    var s = './resendoprpassword';
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#passwordid").serialize(),
//        success: function(data) {
//            if ( strcmpOpr(data._result,"error") == 0 ) {
//                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//            }
//            else if ( strcmpOpr(data._result,"success") == 0 ) {
//                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                window.location.href = data._url;
//            }
//        }
//    });
//}
//
//function setpassword(){
//    var s = './setoprpassword';
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#passwordid").serialize(),
//        success: function(data) {
//            if ( strcmpOpr(data._result,"error") == 0 ) {
//                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//            }
//            else if ( strcmpOpr(data._result,"success") == 0 ) {
//                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                window.location.href = data._url;
//            }
//        }
//    });
//}


function OperatorUnlockPassword(oprid, uidiv) {
    var s = './unlockoprpassword?_oprid=' + encodeURIComponent(oprid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpOpr(data._result, "error") == 0) {
                Alert4Operator("<span><font color=red>" + data._message + "</font></span>");
                //$('#addchannel-result').html("<span><font color=red>" + data._message + "</font></span>");
            } else if (strcmpOpr(data._result, "success") == 0) {
                Alert4Operator("<span><font color=blue>" + data._message + "</font></span>");
                //$('#addchannel-result').html("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}
function OperatorResendPassword(oprid) {
    var s = './resendoprpassword?_oprid=' + encodeURIComponent(oprid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpOpr(data._result, "error") == 0) {
                Alert4Operator("<span><font color=red>" + data._message + "</font></span>");
            } else if (strcmpOpr(data._result, "success") == 0) {
                Alert4Operator("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });

}
function OperatorSetPassword(oprid) {
    var s = './sendrandompassword?_oprid=' + encodeURIComponent(oprid) + '&_send=false';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpOpr(data._result, "error") == 0) {
                Alert4Operator("<span><font color=red>" + data._message + "</font></span>");
            } else if (strcmpOpr(data._result, "success") == 0) {
                Alert4Operator("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });

}
function OperatorSendRandomPassword(oprid) {
    var s = './sendrandompassword?_oprid=' + encodeURIComponent(oprid) + '&_send=true';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpOpr(data._result, "error") == 0) {
                Alert4Operator("<span><font color=red>" + data._message + "</font></span>");
            } else if (strcmpOpr(data._result, "success") == 0) {
                Alert4Operator("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });

}


function RefreshLogout() {
    window.location.href = "./signout.jsp";
}


function changeoperatorpassword() {
    var s = './changeoprpassword';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#ChangePasswordform").serialize(),
        success: function (data) {
            if (strcmpOpr(data._result, "error") == 0) {
                $('#Password-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#_oldPassword').val("");
                $('#_newPassword').val("");
                $('#_confirmPassword').val("");
                //ClearAddOperatorForm();
            } else if (strcmpOpr(data._result, "success") == 0) {
                $('#Password-result').html("<span><font color=blue>" + data._message + "</font></span>");
                //ClearAddOperatorForm();
                $('#passwordChange').attr("disabled", true);
                window.setTimeout(RefreshLogout, 5000);
            }
        }
    });
}

function DownloadZippedLog() {
    var s = './downloadlogs';
    window.location.href = s;
    return false;

}
function DownloadCleanupLog() {
    var s = './downloadCleanuplogs';
    window.location.href = s;
    return false;

}
function DownloadZippedLogByDay() {
    $('#downloadzippedlog').modal('hide');
    //var val1 = encodeURIComponent(document.getElementById('_logsDate').value);
    // alert(document.getElementById('_logsDate').value);
    var val1 = document.getElementById('_logsDate').value;
    if (val1 === "") {
        Alert4Operator("<span><font color=red>Please select date</font></span>");
        return;
    }
    var s = './downloadlogsCheck?_date=' + val1;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpOpr(data._result, "error") == 0) {
                Alert4Operator("<span><font color=red>" + data._message + "</font></span>");
            } else if (strcmpOpr(data._result, "success") == 0) {
//                Alert4Operator("<span><font color=blue>" + data._message + "</font></span>");
                var s = './downloadlogs';
                window.location.href = s;
                return false;
            }
        }
    });

}
//function DownloadZippedLogByDay() {
//    $('#downloadzippedlog').modal('hide');
//    var val1 = encodeURIComponent(document.getElementById('_logsDate').value);
//    if (val1 == "") {
//        Alert4Operator("<span><font color=red>Please select date</font></span>");
//        return;
//    }
//    var s = './downloadlogs?logsDate=' + val1;
//    window.location.href = s;
//    return false;
//}
function searchAudit() {
    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
    var val3 = encodeURIComponent(document.getElementById('_oprnameR').value);
    var val4 = encodeURIComponent(document.getElementById('_opridR').value);

    if (document.getElementById('_auditStartDate').value.length == 0 || document.getElementById('_auditEndDate').value.length == 0) {
        Alert4Msg("Date Range is not selected!!!");
        return;
    }

    var s = './requesterOperatorAudit.jsp?_startdate=' + val1 + "&_enddate=" + val2 + "&_opridR=" + val4 + "&_oprName=" + val3;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
//            $('#licenses_data_table').html(data);
            $('#auditTable').html(data);
            $("#auditDownload").modal('hide');

        }
    });

}
//function searchAudit() {
//    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
//    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
//    var val3 = encodeURIComponent(document.getElementById('_oprnameR').value);
//    var val4 = encodeURIComponent(document.getElementById('_opridR').value);
//    
//    if (document.getElementById('_auditStartDate').value.length == 0 || document.getElementById('_auditEndDate').value.length == 0) {
//        Alert4Msg("Date Range is not selected!!!");
//        return;
//    }
//      var s = './checkOperatorAudit.jsp';
//    $.ajax({
//        type: 'GET',
//        url: s,
//        dataType: 'json',
//        success: function (data) {
//            if (strcmpOpr(data._result, "error") === 0) {
//                Alert4Operator("<span><font color=red> " + data._message + "</font></span>");
//                $("#auditDownload").modal('hide');
//            }
//            if (strcmpOpr(data._result, "success") === 0)
//            {
//                auditDown();
//            }
//        }
//    });
//}
//function grantAccess() {
//    var val1 = encodeURIComponent(document.getElementById('_accessEndDate').value);
//    var val2 = encodeURIComponent(document.getElementById('_accessStartDate').value);
//   if (document.getElementById('_accessStartDate').value.length == 0 || document.getElementById('_accessEndDate').value.length == 0) {
//        Alert4Operator("Date Range is not selected!!!");
//        return;
//    }
//      var s = './grantAccessOperator?_accessEndDate'+val1+'&_accessStartDate'+val2;
//    $.ajax({
//        type: 'GET',
//        url: s,
//        dataType: 'json',
//        success: function (data) {
//            if (strcmpOpr(data._result, "error") === 0) {
//                Alert4Operator("<span><font color=red> " + data._message + "</font></span>");
//                $("#operatorAccess").modal('hide');
//            }
//            if (strcmpOpr(data._result, "success") === 0)
//            {
//                 Alert4Operator("<span><font color=blue> " + data._message + "</font></span>");
//                $("#operatorAccess").modal('hide');
//            }
//        }
//    });
//}
function grantAccess() {
    var val1 = encodeURIComponent(document.getElementById('_accessEndDate').value);
    var val2 = encodeURIComponent(document.getElementById('_accessStartDate').value);
    if (document.getElementById('_accessStartDate').value.length == 0 || document.getElementById('_accessEndDate').value.length == 0) {
        Alert4Operator("Date Range is not selected!!!");
        return;
    }
   
    var s = './grantAccessOperator?_accessEndDate' + val1 + '&_accessStartDate' + val2;
     alert();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#operatorAcessForm").serialize(),
        success: function (data) {
            if (strcmpOpr(data._result, "error") == 0) {
                Alert4Operator("<span><font color=red> " + data._message + "</font></span>");
                $("#operatorAccess").modal('hide');
            } else if (strcmpOpr(data._result, "success") == 0) {
                Alert4Operator("<span><font color=blue> " + data._message + "</font></span>");
//                $("#operatorAccess").modal('hide');
            }
        }
    });
}
function auditDown()
{
    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
    var val3 = encodeURIComponent(document.getElementById('_oprnameR').value);
    var val4 = encodeURIComponent(document.getElementById('_opridR').value);
    var s1 = './requesterOperatorAudit.jsp?_startdate=' + val1 + "&_enddate=" + val2 + "&_opridR=" + val4 + "&_oprName=" + val3;
    $.ajax({
        type: 'GET',
        url: s1,
        success: function (data) {
            $('#auditTable').html(data);
            $("#auditDownload").modal('hide');

        }
    });
}

function loadEditRequesterOperatorDetails(_orpid) {

    var s = './getoperator?_oprid=' + encodeURIComponent(_orpid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpOpr(data._result, "success") == 0) {
                $('#idEditOperatorName').html(data._name);
                $("#_oprnameR").val(data._name);
                $("#_opridR").val(data._id);
                $("#auditDownload").modal();
            } else {
                Alert4Operator("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}


//function requesterReportCSV() {
//    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
//    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
//    var val3 = encodeURIComponent(document.getElementById('_oprnameR').value);
//    var val4 = encodeURIComponent(document.getElementById('_opridR').value);
//
//    var s = './getRequesterAudits?_startdate=' + val1 + "&_enddate=" + val2 + "&_opridR=" + val4 + "&_oprName=" + val3 + "&_format=csv";
//    window.location.href = s;
//    return false;
//}
//
//function requesterPDF() {
//    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
//    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
//    var val3 = encodeURIComponent(document.getElementById('_oprnameR').value);
//    var val4 = encodeURIComponent(document.getElementById('_opridR').value);
//
//    var s = './getRequesterAudits?_startdate=' + val1 + "&_enddate=" + val2 + "&_opridR=" + val4 + "&_oprName=" + val3 + "&_format=pdf";
//    window.location.href = s;
//    return false;
//}
//
//function requesterTXT() {
//    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
//    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
//    var val3 = encodeURIComponent(document.getElementById('_oprnameR').value);
//    var val4 = encodeURIComponent(document.getElementById('_opridR').value);
//    var s = './getRequesterAudits?_startdate=' + val1 + "&_enddate=" + val2 + "&_opridR=" + val4 + "&_oprName=" + val3;
//    window.location.href = s;
//    return false;
//}amol
//amol
function requesterReportCSV() {
    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
    var val3 = encodeURIComponent(document.getElementById('_oprnameR').value);
    var val4 = encodeURIComponent(document.getElementById('_opridR').value);

    var s = './getRequesterAudits?_startdate=' + val1 + "&_enddate=" + val2 + "&_opridR=" + val4 + "&_oprName=" + val3 + "&_format=1";
    window.location.href = s;
    return false;
}

function requesterPDF() {
    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
    var val3 = encodeURIComponent(document.getElementById('_oprnameR').value);
    var val4 = encodeURIComponent(document.getElementById('_opridR').value);

    var s = './getRequesterAudits?_startdate=' + val1 + "&_enddate=" + val2 + "&_opridR=" + val4 + "&_oprName=" + val3 + "&_format=0";
    window.location.href = s;
    return false;
}

function requesterTXT() {
    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
    var val3 = encodeURIComponent(document.getElementById('_oprnameR').value);
    var val4 = encodeURIComponent(document.getElementById('_opridR').value);
    var s = './getRequesterAudits?_startdate=' + val1 + "&_enddate=" + val2 + "&_opridR=" + val4 + "&_oprName=" + val3 + "&_format=2";
    window.location.href = s;
    return false;
}//amol


//function checkAuditIntigrity(){
//    var s = './AuditIntigrityCheck.jsp';
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#CheckIntegrityform").serialize(),
//        success: function(data) {
//            alert(data._result);
//            if ( strcmpOpr(data._result,"error") == 0 ) {
//                $('#Integrity-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//                $('#_IntegrityID').val("");
//              
//            }
//            else if ( strcmpOpr(data._result,"success") == 0 ) {
//                $('#Integrity-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                $('#IntegrityButton').attr("disabled",true);
////                window.setTimeout(RefreshLogout, 5000);
//            }
//        }
//    }); 
//}
function checkAuditIntigrity() {
//    alert("ds");
    var val1 = encodeURIComponent(document.getElementById('_IntegrityID').value);
    var s = './auditIntigrityTest';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#CheckIntegrityform").serialize(),
        success: function (data) {
            if (strcmpOpr(data._result, "error") == 0) {
//               $('#Integrity-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#Integrity-result').html("<a href=./AuditIntigrityCheck.jsp?_IntegrityID=" + val1 + "&_status=" + 0 + "><font color=red>" + data._message + "</a>");
                $('#_IntegrityID').val("");

            } else if (strcmpOpr(data._result, "success") == 0) {
//               $('#Integrity-result').html("<span><font color=blue>" + data._message + "</font></span>");
                $('#Integrity-result').html("<a href=./AuditIntigrityCheck.jsp?_IntegrityID=" + val1 + "&_status=" + 1 + "><font color=blue>" + data._message + "</a>");
                //ClearAddOperatorForm();
//                $('#IntegrityButton').attr("disabled", true);
                $('#_IntegrityID').val("");
                document.getElementById("txtareaauditRecord").style.display = "block";
                //  var display_txt = data._auditrecord.replace("&#13;&#", <br />);
                //   $('#txtareaauditRecord').html(data._auditrecord.replace(/\r?\n/g, '\\\n'));
                $('#sessionId').val(data.sessionId);
                $('#channelId').val(data.channelId);
                $('#OperatorId').val(data.OperatorId);

                $('#IpAddress').val(data.IpAddress);

                $('#ChannelName').val(data.ChannelName);

                $('#Remoteaccesslogin').val(data.Remoteaccesslogin);

                $('#Time').val(data.Time);

                $('#Action').val(data.Action);
                $('#Result').val(data.Result);
                $('#Resultcode').val(data.Resultcode);
                $('#Category').val(data.Category);
                $('#Newvalue').val(data.Newvalue);
                $('#Oldvalue').val(data.Oldvalue);
                $('#Itemtype').val(data.Itemtype);
                $('#Itemid').val(data.Itemid);

//                window.setTimeout(RefreshLogout, 5000);
            } else if (strcmpOpr(data._result, "InvaidID") == 0) {
                $('#Integrity-result').html("<span><font color=red>" + data._message + "</font></span>");
//                $('#Integrity-result').html("<a href=./AuditIntigrityCheck.jsp?_IntegrityID="+val1+"&_status="+1+"><font color=red>"+ data._message +"</a>");
                //ClearAddOperatorForm();
//                $('#IntegrityButton').attr("disabled", true);
                $('#_IntegrityID').val("");
//                window.setTimeout(RefreshLogout, 5000);
            }
        }
    });
}


function Operatordetails(name, mobile, email) {

    $('#_operatorname').val(name);
    $('#_operatormobile').val(mobile);
    $("#_operatoremail").val(email);
    $('#addldapopertor-result').html("");
    $("addldapopertor").modal();

}



function checkValidMonitor() {
    var s = './MonitorHandling';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {

        }
    });
}

function searchOperators() {
    var val = document.getElementById('_keyword').value;
    var _unitID = document.getElementById('_unitID').value;
    var _opStatuc = document.getElementById('_operatorStatus').value;
    // var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    if (_unitID == -1)
    {
        if (val.length < 1) {

            Alert4Operator("<span><font color=red>Search keyword cannot be blank!!!</font></span>");
            return;
        }
    }
    //    alert(_unitID);
    var s = './operatorstable.jsp?_searchtext=' + encodeURIComponent(val) + '&_unitID=' + _unitID + "&_operatorStatuc=" + _opStatuc;
    //  pleaseWaitDiv.modal();
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#users_table_main').html(data);
            // pleaseWaitDiv.modal('hide');
        }
    });
}
