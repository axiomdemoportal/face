function Alert4AccessMatrix(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });

}
function strcmpUsers(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4Users(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}


function InvalidRequest(id) {
    var s = './reportResponse?_requestID=' + id;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpUsers(data._result, "error") == 0) {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpUsers(data._result, "success") == 0) {
                Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}


function Confirm4AccessMatrix(msg,val1, val4) {
    bootbox.confirm("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
              if (strcmpAccessMatrix(val1, "-1") == 0) {
                window.setTimeout(RefreshOperatorsTable(val1, val4), 10000);
            } else {
                window.setTimeout(RefreshRolesTable(val1, val4), 10000);
            }
        } else {
            if (strcmpAccessMatrix(val1, "-1") == 0) {
                window.setTimeout(RefreshOperatorsTable(val1, val4), 10000);
            } else {
                window.setTimeout(RefreshRolesTable(val1, val4), 10000);
            }
        }
    });
}
function RefreshRoles() {
    window.location.href = "./addAccessRole.jsp";
}
function RefreshRolesTable(val1, val4) {

//    window.location.href = "./roleAccessRightN_1.jsp";
    window.location.href = "./roleAccessRightN_1.jsp?_roleId=" + val1 + "&_type=" + val4;
}
function RefreshOperatorsTable(val1, val4) {

//    window.location.href = "./roleAccessRightN_1.jsp";
    window.location.href = "./roleAccessRightN_1.jsp?_roleId=" + val1 + "&_type=OPERATOR" + "&_operatorID=" + val4;
    ;
}
function strcmpAccessMatrix(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function accessList(_cid) {
  
    var cid = "#_sel" + _cid;
 
    var isChecked = $(cid).is(':checked');
   
    var deleteList = $('#_deleteList').val();
  
    var addList = $('#_addList').val();
//     alert(currentList);
    if (isChecked == false) {
        var cidS = "" + _cid + ",";
        var n = deleteList.search(cidS);
        if (n == -1) {
            deleteList = deleteList + cidS;
        }
    } else {
        var cidS = "" + _cid + ",";
        var n = addList.search(cidS);
        if (n == -1) {
            addList = addList + cidS;
        }
    }
//     alert(addList);
    $('#_addList').val(addList);
    $('#_deleteList').val(deleteList);

}


function addAccessMatrixList(_cid) {
    var cid = "#_sel" + _cid;
    var isChecked = $(cid).is(':checked');

    var currentList = $('#_addAccessMatrixList').val();
    if (isChecked == false) {
        var cidS = "" + _cid + ",";
        currentList = currentList.replace(cidS, "");
    } else {
        var cidS = "" + _cid + ",";

        var n = currentList.search(cidS);
        if (n == -1) {
            currentList = currentList + cidS;
        }
    }

    $('#_addAccessMatrixList').val(currentList);
//    alert(currentList);
}
function editAccessMatrixList(_cid) {
    var cid = "#_sel" + _cid;
    var isChecked = $(cid).is(':checked');

    var currentList = $('#_editAccessMatrixList').val();
    if (isChecked == false) {
        var cidS = "" + _cid + ",";
        currentList = currentList.replace(cidS, "");
    } else {
        var cidS = "" + _cid + ",";

        var n = currentList.search(cidS);
        if (n == -1) {
            currentList = currentList + cidS;
        }
    }

    $('#_editAccessMatrixList').val(currentList);
//    alert(currentList);

}
function deleteAccessMatrixList(_cid) {
    var cid = "#_sel" + _cid;
    var isChecked = $(cid).is(':checked');

    var currentList = $('#_deleteAccessMatrixList').val();
    if (isChecked == false) {
        var cidS = "" + _cid + ",";
        currentList = currentList.replace(cidS, "");
    } else {
        var cidS = "" + _cid + ",";

        var n = currentList.search(cidS);
        if (n == -1) {
            currentList = currentList + cidS;
        }
    }

    $('#_deleteAccessMatrixList').val(currentList);
//    alert(currentList);
}

function addMatrix()
{
//    alert("hi");
    valadd = document.getElementById("_addAccessMatrixList").value;
//    alert("Hi" + valadd);
    valdelete = document.getElementById("_deleteAccessMatrixList").value;
    valedit = document.getElementById("_editAccessMatrixList").value;
//
//    alert(valdelete);
//    alert(valedit);


    var s = './AccessMatrix';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#addAccessMatrixListForm , accessMatrix").serialize(),
        success: function(data) {
            if (strcmpChannel(data._result, "error") == 0) {

                Alert4Channel(data._message);
            }
            else if (strcmpChannel(data._result, "success") == 0) {
                Alert4Channel("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

$(document).ready(function() {
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    $('#_searchByRoles').change(function() {
        var selectedValue = $(this).val();
//           if (strcmpAuthorization(selectedValue, "0") == 0) {
        var s = './accessMatrixTable.jsp?_roleID=' + selectedValue;
        pleaseWaitDiv.modal();
        $.ajax({
            type: 'post',
            url: s,
            success: function(data) {
                pleaseWaitDiv.modal('hide');
                $('#licenses_data_table').html(data);
            }
        });
//        }
    });
});

//function saveMatrix(val1){
////    alert(val1);
//      var val =  document.getElementById("_deleteContactsList").value;
//    var s = './saveAccessMatrix?_channelList='+val+'&_roleID='+val1;
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#ChannelForm").serialize(),
//        success: function (data) {
//            if (strcmpAccessMatrix(data._result, "error") == 0) {
//                Alert4AccessMatrix(data._message);
//            }
//            else if (strcmpAccessMatrix(data._result, "success") == 0) {
//                Alert4AccessMatrix("<span><font color=blue>" + data._message + "</font></span>");
//            }
//        }
//    });
//}

function saveMatrix(val1, val3, val4) {
//   alert( val4);
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var val = document.getElementById("_addList").value;
            var val2 = document.getElementById("_deleteList").value;
            var s = './saveAccessMatrix?_addList=' + val + '&_deleteList=' + val2 + '&_roleID=' + val1 + '&_type=' + val3 + '&_accesstype=' + val4;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAccessMatrix(data._result, "error") == 0) {
                      Confirm4AccessMatrix("<span><font color=red>" + data._message + "</font></span>", val1, val4);
//                        if (strcmpAccessMatrix(val1, "-1") == 0) {
//                            window.setTimeout(RefreshOperatorsTable(val1, val4), 10000);
//                        } else {
//                            window.setTimeout(RefreshRolesTable(val1, val4), 10000);
//                        }
                    }
                    else if (strcmpAccessMatrix(data._result, "success") == 0) {
                        Confirm4AccessMatrix("<span><font color=blue>" + data._message + "</font></span>", val1, val4);
//                        if (strcmpAccessMatrix(val1, "-1") == 0) {
//                            window.setTimeout(RefreshOperatorsTable(val1, val4), 10000);
//                        } else {
//                            window.setTimeout(RefreshRolesTable(val1, val4), 10000);
//                        }
                    }
                }
            });
        }
    });
}

function addRole() {
    $('#addnewRoleSubmitBut').attr("disabled", true);
    var s = './addRole';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#AddNewRoleForm").serialize(),
        success: function(data) {
            if (strcmpAccessMatrix(data._result, "error") == 0) {
                $('#add-new-role-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//                Alert4AccessMatrix("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpAccessMatrix(data._result, "success") == 0) {
                $('#add-new-role-result').html("<span><font color=blue>" + data._message + "</font></span></small>");
//                $('#_rolename').val("");
//                 $("#addNewRole").modal('hide');
                window.setTimeout(RefreshRoles, 3000);
//                 Alert4AccessMatrix("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}


function ChangeRoleStatus(name, status, uidiv) {
    var s = './changeRoleStatus?_role_status=' + status + '&_roleId=' + name;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpAccessMatrix(data._result, "error") == 0) {
                Alert4AccessMatrix("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpAccessMatrix(data._result, "success") == 0) {
                Alert4AccessMatrix("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}
function loadRolesDetails(_rolenameE, _rolestatusE, _roleIDE) {
    $('#_rolenameE').val(_rolenameE);
    $('#_rolestatusE').val(_rolestatusE);
    $('#_roleIDE').val(_roleIDE);
    $("#EditRole").modal();
}

function EditRole() {
    $('#editRoleSubmitBut').attr("disabled", true);
    var s = './EditRoles';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#EditRoleForm").serialize(),
        success: function(data) {

            if (strcmpAccessMatrix(data._result, "error") == 0) {
                $('#edit-new-role-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//                Alert4AccessMatrix("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpAccessMatrix(data._result, "success") == 0) {
                $('#edit-new-role-result').html("<span><font color=blue>" + data._message + "</font></span></small>");
                window.setTimeout(RefreshRoles, 3000);
//                 $("#EditRole").modal('hide');
//                 Alert4AccessMatrix("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function RefresList() {
    //alert("im here");
    window.location.href = "addAccessRole.jsp";
}
function deleteRole(sid) {

    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './removeAccessRole?_roleId=' + encodeURIComponent(sid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAccessMatrix(data._result, "success") == 0) {
                        Alert4AccessMatrix("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefresList, 2000);
                    } else {
                        Alert4AccessMatrix("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}

function downlaoAccessMatrix(val2,roleId){
    var val = roleId;
    var s = './AccessMatrixDownload?_strSearch=' + val + "&_reporttype=" + val2 + "&_roleId="+roleId;
    window.location.href = s;
    return false;
}

function downlaodAllAccessMatrix(val1){
    var s = './AllAccessMatriXDownload?_reporttype='+val1;
    window.location.href = s;
    return false;
}