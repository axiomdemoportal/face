function strcmpContentFilter(a, b)
{
    return (a<b?-1:(a>b?1:0));
}


function Alert4ContentFilterSetting(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
    //end here
    }
    });
}


function ChangeStatusContentFilter(value) {
   
        if ( value == 1) {
            $('#_statusContent').val("1");
            $('#_status-content-filter').html("Active");
        } else {
            $('#_statusContent').val("0");
            $('#_status-content-filter').html("Suspended");
        }
    
}


function ContentFilter(){
    var s = './loadcontentfiltersetting';

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
          ChangeStatusContentFilter($('#_statusContent').val(data._status));
           $('#_content').val(data._content);
        }
    });
}



function SaveContentFilerList(){
    $('#SaveContentFilteringButton').attr("disabled", true);
    
    var s = './editcontentfiltersetting';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#contentfilterform").serialize(),
        success: function(data) {
            if ( strcmpContentFilter(data._result,"error") == 0 ) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4ContentFilterSetting("<span><font color=red>" + data._message + "</font></span>");
                $('#SaveContentFilteringButton').attr("disabled", false);
            }
            else if ( strcmpContentFilter(data._result,"success") == 0 ) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4ContentFilterSetting("<span><font color=blue>" + data._message + "</font></span>");
                
            }
        }
    });
}

