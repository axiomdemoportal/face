function strcmpGlobal(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}


function Alert4GlobalSetting(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
    });
}


function RefreshChannelGLobalSetting() {
    window.location.href = "./globalsettingv2.jsp";
}

function SaveChannelContentFilerList() {
    $('#SaveChannelContentFilteringButton').attr("disabled", true);

    var s = './editglobalsettingv2?_settype=' + 1;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#channelcontentfilterform").serialize(),
        success: function(data) {
            if (strcmpGlobal(data._result, "error") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4GlobalSetting("<span><font color=red>" + data._message + "</font></span>");
                $('#SaveChannelContentFilteringButton').attr("disabled", false);
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);
            }
            else if (strcmpGlobal(data._result, "success") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4GlobalSetting("<span><font color=blue>" + data._message + "</font></span>");
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);

            }
        }
    });
}


function SaveChannelIPFilerList() {
    $('#SaveChannelIPFilteringButton').attr("disabled", true);

    var s = './editglobalsettingv2?_settype=' + 2;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#channelipfilterform").serialize(),
        success: function(data) {
            if (strcmpGlobal(data._result, "error") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4GlobalSetting("<span><font color=red>" + data._message + "</font></span>");
                $('#SaveChannelIPFilteringButton').attr("disabled", false);
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);
            }
            else if (strcmpGlobal(data._result, "success") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4GlobalSetting("<span><font color=blue>" + data._message + "</font></span>");
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);

            }
        }
    });
}

function AlertUser() {
    $('#SaveChannelAlertUserButton').attr("disabled", true);

    var s = './editglobalsettingv2?_settype=' + 10;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#alertuserform").serialize(),
        success: function(data) {
            if (strcmpGlobal(data._result, "error") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4GlobalSetting("<span><font color=red>" + data._message + "</font></span>");
                $('#SaveChannelAlertUserButton').attr("disabled", false);
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);
            }
            else if (strcmpGlobal(data._result, "success") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4GlobalSetting("<span><font color=blue>" + data._message + "</font></span>");
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);

            }
        }
    });
}

function SaveChannelPrefixFilerList() {
    $('#SaveChannelPrefixFilteringButton').attr("disabled", true);

    var s = './editglobalsettingv2?_settype=' + 3;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#channelprefixfilterform").serialize(),
        success: function(data) {
            if (strcmpGlobal(data._result, "error") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4GlobalSetting("<span><font color=red>" + data._message + "</font></span>");
                $('#SaveChannelPrefixFilteringButton').attr("disabled", false);
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);
            }
            else if (strcmpGlobal(data._result, "success") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4GlobalSetting("<span><font color=blue>" + data._message + "</font></span>");
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);

            }
        }
    });
}

function SaveChannelDomainFilerList() {
    $('#SaveChannelDomainFilteringButton').attr("disabled", true);

    var s = './editglobalsettingv2?_settype=' + 11;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#channeldomainfilterform").serialize(),
        success: function(data) {
            if (strcmpGlobal(data._result, "error") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4GlobalSetting("<span><font color=red>" + data._message + "</font></span>");
                $('#SaveChannelDomainFilteringButton').attr("disabled", false);
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);
            }
            else if (strcmpGlobal(data._result, "success") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4GlobalSetting("<span><font color=blue>" + data._message + "</font></span>");
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);

            }
        }
    });
}
function SaveChannelSMSFilerList() {
    $('#SaveChannelPrefixFilteringButton').attr("disabled", true);

    var s = './editglobalsettingv2?_settype=' + 3;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#channelprefixfilterform").serialize(),
        success: function(data) {
            if (strcmpGlobal(data._result, "error") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4GlobalSetting("<span><font color=red>" + data._message + "</font></span>");
                $('#SaveChannelPrefixFilteringButton').attr("disabled", false);
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);
            }
            else if (strcmpGlobal(data._result, "success") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4GlobalSetting("<span><font color=blue>" + data._message + "</font></span>");
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);

            }
        }
    });
}
function SaveChannelSMSSetting() {
    $('#SaveChannelSMSSettingButton').attr("disabled", true);

    var s = './editglobalsettingv2?_settype=' + 4;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#SMSForm").serialize(),
        success: function(data) {
            if (strcmpGlobal(data._result, "error") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4GlobalSetting("<span><font color=red>" + data._message + "</font></span>");
                $('#SaveChannelSMSSettingButton').attr("disabled", false);
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);
            }
            else if (strcmpGlobal(data._result, "success") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4GlobalSetting("<span><font color=blue>" + data._message + "</font></span>");
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);
            }
        }
    });
}
function SaveChannelEMAILSetting() {
    $('#SaveChannelEMAILSettingButton').attr("disabled", true);

    var s = './editglobalsettingv2?_settype=' + 7;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#EMAILForm").serialize(),
        success: function(data) {
            if (strcmpGlobal(data._result, "error") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4GlobalSetting("<span><font color=red>" + data._message + "</font></span>");
                $('#SaveChannelEMAILSettingButton').attr("disabled", false);
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);
            }
            else if (strcmpGlobal(data._result, "success") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4GlobalSetting("<span><font color=blue>" + data._message + "</font></span>");
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);

            }
        }
    });
}

function SaveChannelFAXSetting() {
    $('#SaveChannelFAXSettingButton').attr("disabled", true);

    var s = './editglobalsettingv2?_settype=' + 8;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#FAXForm").serialize(),
        success: function(data) {
            if (strcmpGlobal(data._result, "error") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4GlobalSetting("<span><font color=red>" + data._message + "</font></span>");
                $('#SaveChannelFAXSettingButton').attr("disabled", false);
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);
            }
            else if (strcmpGlobal(data._result, "success") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4GlobalSetting("<span><font color=blue>" + data._message + "</font></span>");
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);

            }
        }
    });
}
function SaveChannelPUSHSetting() {
    $('#SaveChannelPUSHSettingButton').attr("disabled", true);

    var s = './editglobalsettingv2?_settype=' + 9;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#PUSHForm").serialize(),
        success: function(data) {
            if (strcmpGlobal(data._result, "error") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4GlobalSetting("<span><font color=red>" + data._message + "</font></span>");
                $('#SaveChannelPUSHSettingButton').attr("disabled", false);
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);
            }
            else if (strcmpGlobal(data._result, "success") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4GlobalSetting("<span><font color=blue>" + data._message + "</font></span>");
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);

            }
        }
    });
}
function SaveChannelVOICESetting() {
    $('#SaveChannelVOICESettingButton').attr("disabled", true);

    var s = './editglobalsettingv2?_settype=' + 5;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#VOICEForm").serialize(),
        success: function(data) {
            if (strcmpGlobal(data._result, "error") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4GlobalSetting("<span><font color=red>" + data._message + "</font></span>");
                $('#SaveChannelVOICESettingButton').attr("disabled", false);
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);
            }
            else if (strcmpGlobal(data._result, "success") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4GlobalSetting("<span><font color=blue>" + data._message + "</font></span>");
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);

            }
        }
    });
}

function SaveChannelUSSDSetting() {
    $('#SaveChannelUSSDSettingButton').attr("disabled", true);

    var s = './editglobalsettingv2?_settype=' + 6;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#USSDForm").serialize(),
        success: function(data) {
            if (strcmpGlobal(data._result, "error") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4GlobalSetting("<span><font color=red>" + data._message + "</font></span>");
                $('#SaveChannelUSSDSettingButton').attr("disabled", false);
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);
            }
            else if (strcmpGlobal(data._result, "success") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4GlobalSetting("<span><font color=blue>" + data._message + "</font></span>");
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);

            }
        }
    });
}
function SaveUserType() {
    $('#SaveUserTypeButton').attr("disabled", true);

    var s = './editglobalsettingv2?_settype=' + 10;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#channelusersettingform").serialize(),
        success: function(data) {
            if (strcmpGlobal(data._result, "error") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4GlobalSetting("<span><font color=red>" + data._message + "</font></span>");
                $('#SaveUserTypeButton').attr("disabled", false);
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);
            }
            else if (strcmpGlobal(data._result, "success") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4GlobalSetting("<span><font color=blue>" + data._message + "</font></span>");
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);

            }
        }
    });
}

function SaveMSGType() {
    $('#SaveMSGTypeButton').attr("disabled", true);

    var s = './editglobalsettingv2?_settype=' + 12;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#channelmsgsettingform").serialize(),
        success: function(data) {
            if (strcmpGlobal(data._result, "error") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4GlobalSetting("<span><font color=red>" + data._message + "</font></span>");
                $('#SaveMSGTypeButton').attr("disabled", false);
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);
            }
            else if (strcmpGlobal(data._result, "success") === 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4GlobalSetting("<span><font color=blue>" + data._message + "</font></span>");
                //window.setTimeout(RefreshChannelGLobalSetting, 1000);

            }
        }
    });
}
function ChangeUserType(value) {

    if (value === 1) {
        $('#_userType').val("1");
        $('#_userTypeDIV').html("Keep UserName Unique");
    } else {
        $('#_userType').val("0");
        $('#_userTypeDIV').html("Keep Email,Phone Unique");
    }

}
function ChangeMSGType(value) {

    if (value === 1) {
        $('#_msgType').val("1");
        $('#_msgTypeDIV').html("Erase MSG Body After Send");
    } else {
        $('#_msgType').val("0");
        $('#_msgTypeDIV').html("Don't Erase MSG Body After Send");
    }

}


function ChangeLimitStatusSMS(value) {

    if (value === 1) {
        $('#_smsStatus').val("1");
        $('#_smsStatusDIV').html("Active");
    } else {
        $('#_smsStatus').val("0");
        $('#_smsStatusDIV').html("Suspended");
    }

}
function ChangeFooterStatusSMS(value) {

    if (value === 1) {
        $('#_smsFooterStatus').val("1");
        $('#_smsFooterStatusDIV').html("Active");
    } else {
        $('#_smsFooterStatus').val("0");
        $('#_smsFooterStatusDIV').html("Suspended");
    }

}
function ChangeFooterStatusVOICE(value) {

    if (value === 1) {
        $('#_voiceFooterStatus').val("1");
        $('#_voiceFooterStatusDIV').html("Active");
    } else {
        $('#_voiceFooterStatus').val("0");
        $('#_voiceFooterStatusDIV').html("Suspended");
    }

}
function ChangeFooterStatusUSSD(value) {

    if (value === 1) {
        $('#_ussdFooterStatus').val("1");
        $('#_ussdFooterStatusDIV').html("Active");
    } else {
        $('#_ussdFooterStatus').val("0");
        $('#_ussdFooterStatusDIV').html("Suspended");
    }

}
function ChangeFooterStatusEMAIL(value) {

    if (value === 1) {
        $('#_emailFooterStatus').val("1");
        $('#_emailFooterStatusDIV').html("Active");
    } else {
        $('#_emailFooterStatus').val("0");
        $('#_emailFooterStatusDIV').html("Suspended");
    }

}
function ChangeFooterStatusFAX(value) {

    if (value === 1) {
        $('#_faxFooterStatus').val("1");
        $('#_faxFooterStatusDIV').html("Active");
    } else {
        $('#_faxFooterStatus').val("0");
        $('#_faxFooterStatusDIV').html("Suspended");
    }

}

function ChangeAttempts(value) {

    if (value === 3) {
        $('#_attempts').val("1");
        $('#_answersattempts').html("3 attempts");
    } else if (value === 5) {
        $('#_attempts').val("5");
        $('#_answersattempts').html("5 attempts");
    }
    else if (value === 7) {
        $('#_attempts').val("7");
        $('#_answersattempts').html("7 attempts");
    }

}

function ChangePercentage(value) {

    if (value === 75) {
        $('#_faxFooterStatus').val("75");
        $('#_weigtage_div').html("75 percentage");
    } else if (value === 80) {
        $('#_faxFooterStatus').val("80");
        $('#_weigtage_div').html("80 percentage");
    }
    else if (value === 90) {
        $('#_faxFooterStatus').val("90");
        $('#_weigtage_div').html("90 percentage");
    } else {
        $('#_faxFooterStatus').val("100");
        $('#_weigtage_div').html("100 percentage");
    }


}




function ChangeFooterStatusPUSH(value) {

    if (value === 1) {
        $('#_pushFooterStatus').val("1");
        $('#_pushFooterStatusDIV').html("Active");
    } else {
        $('#_pushFooterStatus').val("0");
        $('#_pushFooterStatusDIV').html("Suspended");
    }

}
function ChangeLimitAlertStatusSMS(value) {

    if (value === 1) {
        $('#_smsAlertStatus').val("1");
        $('#_smsAlertStatusDIV').html("Enable");
    } else {
        $('#_smsAlertStatus').val("0");
        $('#_smsAlertStatusDIV').html("Disable");
    }

}

function ChangeRepeatStatusSMS(value) {

    if (value === 1) {
        $('#_smsRepeatStatus').val("1");
        $('#_smsRepeatStatusDIV').html("1 Month");
    } else if (value === 2) {
        $('#_smsRepeatStatus').val("2");
        $('#_smsRepeatStatusDIV').html("3 Months");
    } else if (value === 3) {
        $('#_smsRepeatStatus').val("3");
        $('#_smsRepeatStatusDIV').html("6 Months");
    }

}


function ChangeRepeatDurationSMS(value) {

//        if ( value == 1) {
//            $('#_smsRepeatDuration').val("1");
//            $('#_smsRepeatDurationDIV').html("One Week");
//        } else
    if (value === 1) {
        $('#_smsRepeatDuration').val("1");
        $('#_smsRepeatDurationDIV').html("One Month");
    } else if (value === 2) {
        $('#_smsRepeatDuration').val("2");
        $('#_smsRepeatDurationDIV').html("Three Month");
    } else if (value === 3) {
        $('#_smsRepeatDuration').val("3");
        $('#_smsRepeatDurationDIV').html("Six Month");
    }

}
//push

function ChangeLimitStatusPUSH(value) {

    if (value === 1) {
        $('#_pushStatus').val("1");
        $('#_pushStatusDIV').html("Active");
    } else {
        $('#_pushStatus').val("0");
        $('#_pushStatusDIV').html("Suspended");
    }

}

function ChangeLimitAlertStatusPUSH(value) {

    if (value === 1) {
        $('#_pushAlertStatus').val("1");
        $('#_pushAlertStatusDIV').html("Enable");
    } else {
        $('#_pushAlertStatus').val("0");
        $('#_pushAlertStatusDIV').html("Disable");
    }

}

function ChangeRepeatStatusPUSH(value) {

    if (value === 1) {
        $('#_pushRepeatStatus').val("1");
        $('#_pushRepeatStatusDIV').html("1 Month");
    } else if (value === 2) {
        $('#_pushRepeatStatus').val("2");
        $('#_pushRepeatStatusDIV').html("3 Months");
    } else if (value === 3) {
        $('#_pushRepeatStatus').val("3");
        $('#_pushRepeatStatusDIV').html("6 Months");
    }

}


function ChangeRepeatDurationPUSH(value) {

    if (value === 1) {
        $('#_pushRepeatDuration').val("1");
        $('#_pushRepeatDurationDIV').html("One Week");
    } else if (value === 2) {
        $('#_pushRepeatDuration').val("2");
        $('#_pushRepeatDurationDIV').html("One Month");
    } else if (value === 3) {
        $('#_pushRepeatDuration').val("3");
        $('#_pushRepeatDurationDIV').html("Three Months");
    } else if (value === 4) {
        $('#_pushRepeatDuration').val("4");
        $('#_pushRepeatDurationDIV').html("Six Months");
    }

}


function SetPACEPUSH(category, value, _div, disp) {

    if (category === 1) {
        $('#_slowPacePUSH').val(value);
        $(_div).html(disp);
    }
    else if (category === 2) {
        $('#_normalPacePUSH').val(value);
        $(_div).html(disp);
    } else if (category === 3) {
        $('#_fastPacePUSH').val(value);
        $(_div).html(disp);
    } else if (category === 4) {
        $('#_hyperPacePUSH').val(value);
        $(_div).html(disp);
    }
}

//end push


//email
function ChangeLimitStatusEMAIL(value) {

    if (value === 1) {
        $('#_emailStatus').val("1");
        $('#_emailStatusDIV').html("Active");
    } else {
        $('#_emailStatus').val("0");
        $('#_emailStatusDIV').html("Suspended");
    }

}

function ChangeLimitAlertStatusEMAIL(value) {

    if (value === 1) {
        $('#_emailAlertStatus').val("1");
        $('#_emailAlertStatusDIV').html("Enable");
    } else {
        $('#_emailAlertStatus').val("0");
        $('#_emailAlertStatusDIV').html("Disable");
    }

}

function ChangeRepeatStatusEMAIL(value) {

    if (value === 1) {
        $('#_emailRepeatStatus').val("1");
        $('#_emailRepeatStatusDIV').html("1 Month");
    } else if (value === 2) {
        $('#_emailRepeatStatus').val("2");
        $('#_emailRepeatStatusDIV').html("3 Months");
    } else if (value === 3) {
        $('#_emailRepeatStatus').val("3");
        $('#_emailRepeatStatusDIV').html("6 Months");
    }

}

function SetPACEEMAIL(category, value, _div, disp) {

    if (category === 1) {
        $('#_slowPaceEMAIL').val(value);
        $(_div).html(disp);
    }
    else if (category === 2) {
        $('#_normalPaceEMAIL').val(value);
        $(_div).html(disp);
    } else if (category === 3) {
        $('#_fastPaceEMAIL').val(value);
        $(_div).html(disp);
    } else if (category === 4) {
        $('#_hyperPaceEMAIL').val(value);
        $(_div).html(disp);
    }
}

function ChangeRepeatDurationEMAIL(value) {

    if (value === 1) {
        $('#_emailRepeatDuration').val("1");
        $('#_emailRepeatDurationDIV').html("One Week");
    } else if (value === 2) {
        $('#_emailRepeatDuration').val("2");
        $('#_emailRepeatDurationDIV').html("One Month");
    } else if (value === 3) {
        $('#_emailRepeatDuration').val("3");
        $('#_emailRepeatDurationDIV').html("Three Months");
    } else if (value === 4) {
        $('#_emailRepeatDuration').val("4");
        $('#_emailRepeatDurationDIV').html("Six Months");
    }


    }
//FAX

    function ChangeLimitStatusFAX(value) {

        if (value === 1) {
            $('#_faxStatus').val("1");
            $('#_faxStatusDIV').html("Active");
        } else {
            $('#_faxStatus').val("0");
            $('#_faxStatusDIV').html("Suspended");
        }

    }

    function ChangeLimitAlertStatusFAX(value) {

        if (value === 1) {
            $('#_faxAlertStatus').val("1");
            $('#_faxAlertStatusDIV').html("Enable");
        } else {
            $('#_faxAlertStatus').val("0");
            $('#_faxAlertStatusDIV').html("Disable");
        }

    }

    function ChangeRepeatStatusFAX(value) {

        if (value === 1) {
            $('#_faxRepeatStatus').val("1");
            $('#_faxRepeatStatusDIV').html("1 Month");
        } else if (value === 2) {
            $('#_faxRepeatStatus').val("2");
            $('#_faxRepeatStatusDIV').html("3 Months");
        } else if (value === 3) {
            $('#_faxRepeatStatus').val("3");
            $('#_faxRepeatStatusDIV').html("6 Months");
        }

    }

    function SetPACEFAX(category, value, _div, disp) {

        if (category === 1) {
            $('#_slowPaceFAX').val(value);
            $(_div).html(disp);
        }
        else if (category === 2) {
            $('#_normalPaceFAX').val(value);
            $(_div).html(disp);
        } else if (category === 3) {
            $('#_fastPaceFAX').val(value);
            $(_div).html(disp);
        } else if (category === 4) {
            $('#_hyperPaceFAX').val(value);
            $(_div).html(disp);
        }
    }

    function ChangeRepeatDurationFAX(value) {

        if (value === 1) {
            $('#_faxRepeatDuration').val("1");
            $('#_faxRepeatDurationDIV').html("One Week");
        } else if (value === 2) {
            $('#_faxRepeatDuration').val("2");
            $('#_faxRepeatDurationDIV').html("One Month");
        } else if (value === 3) {
            $('#_faxRepeatDuration').val("3");
            $('#_faxRepeatDurationDIV').html("Three Months");
        } else if (value === 4) {
            $('#_faxRepeatDuration').val("4");
            $('#_faxRepeatDurationDIV').html("Six Months");
        }

    }
//
    function ChangeRepeatDurationVOICE(value) {
//    alert(value)
        if (value === 1) {
            $('#_voiceRepeatDuration').val("1");
            $('#_voiceRepeatdurationDIV').html("One Week");
        } else if (value === 2) {
            $('#_voiceRepeatDuration').val("2");
            $('#_voiceRepeatdurationDIV').html("One Month");
        } else if (value === 3) {
            $('#_voiceRepeatDuration').val("3");
            $('#_voiceRepeatdurationDIV').html("Three Month");
        } else if (value === 4) {
            $('#_voiceRepeatDuration').val("4");
            $('#_voiceRepeatdurationDIV').html("Six Month");
        }

    }

    function SetPACESMS(category, value, _div, disp) {

        if (category === 1) {
            $('#_slowPaceSMS').val(value);
            $(_div).html(disp);
        }
        else if (category === 2) {
            $('#_normalPaceSMS').val(value);
            $(_div).html(disp);
        } else if (category === 3) {
            $('#_fastPaceSMS').val(value);
            $(_div).html(disp);
        } else if (category === 4) {
            $('#_hyperPaceSMS').val(value);
            $(_div).html(disp);
        }
    }


//voice

    function ChangeLimitStatusVOICE(value) {

        if (value === 1) {
            $('#_voiceStatus').val("1");
            $('#_voiceStatusDIV').html("Active");
        } else {
            $('#_voiceStatus').val("0");
            $('#_voiceStatusDIV').html("Suspended");
        }

    }

    function ChangeLimitAlertStatusVOICE(value) {

        if (value === 1) {
            $('#_voiceAlertStatus').val("1");
            $('#_voiceAlertStatusDIV').html("Enable");
        } else {
            $('#_voiceAlertStatus').val("0");
            $('#_voiceAlertStatusDIV').html("Disable");
        }

    }

    function ChangeRepeatStatusVOICE(value) {

        if (value === 1) {
            $('#_voiceRepeatStatus').val("1");
            $('#_voiceRepeatStatusDIV').html("1 Month");
        } else if (value === 2) {
            $('#_voiceRepeatStatus').val("2");
            $('#_voiceRepeatStatusDIV').html("3 Months");
        } else if (value === 3) {
            $('#_voiceRepeatStatus').val("3");
            $('#_voiceRepeatStatusDIV').html("6 Months");
        }

    }



    function SetPACEVOICE(category, value, _div, disp) {

        if (category === 1) {
            $('#_slowPaceVOICE').val(value);
            $(_div).html(disp);
        }
        else if (category === 2) {
            $('#_normalPaceVOICE').val(value);
            $(_div).html(disp);
        } else if (category === 3) {
            $('#_fastPaceVOICE').val(value);
            $(_div).html(disp);
        } else if (category === 4) {
            $('#_hyperPaceVOICE').val(value);
            $(_div).html(disp);
        }
    }

//USSD
    function ChangeLimitStatusUSSD(value) {

        if (value === 1) {
            $('#_ussdStatus').val("1");
            $('#_ussdStatusDIV').html("Active");
        } else {
            $('#_ussdStatus').val("0");
            $('#_ussdStatusDIV').html("Suspended");
        }

    }

    function ChangeLimitAlertStatusUSSD(value) {

        if (value === 1) {
            $('#_ussdAlertStatus').val("1");
            $('#_ussdAlertStatusDIV').html("Enable");
        } else {
            $('#_ussdAlertStatus').val("0");
            $('#_ussdAlertStatusDIV').html("Disable");
        }

    }

    function ChangeRepeatStatusUSSD(value) {

        if (value === 1) {
            $('#_ussdRepeatStatus').val("1");
            $('#_ussdRepeatStatusDIV').html("1 Month");
        } else if (value === 2) {
            $('#_ussdRepeatStatus').val("2");
            $('#_ussdRepeatStatusDIV').html("3 Months");
        } else if (value === 3) {
            $('#_ussdRepeatStatus').val("3");
            $('#_ussdRepeatStatusDIV').html("6 Months");
        }

    }



    function SetPACEUSSD(category, value, _div, disp) {

        if (category === 1) {
            $('#_slowPaceUSSD').val(value);
            $(_div).html(disp);
        }
        else if (category === 2) {
            $('#_normalPaceUSSD').val(value);
            $(_div).html(disp);
        } else if (category === 3) {
            $('#_fastPaceUSSD').val(value);
            $(_div).html(disp);
        } else if (category === 4) {
            $('#_hyperPaceUSSD').val(value);
            $(_div).html(disp);
        }
    }


    function ChangeRepeatDurationUSSD(value) {

        if (value === 1) {
            $('#_ussdRepeatDuration').val("1");
            $('#_ussdRepeatdurationDIV').html("One Week");
        } else if (value === 2) {
            $('#_ussdRepeatDuration').val("2");
            $('#_ussdRepeatdurationDIV').html("One Month");
        } else if (value === 3) {
            $('#_ussdRepeatDuration').val("3");
            $('#_ussdRepeatdurationDIV').html("Three Month");
        } else if (value === 4) {
            $('#_ussdRepeatDuration').val("4");
            $('#_ussdRepeatdurationDIV').html("Six Month");
        }

    }

    function loadglobalsetting(val) {
        var s = './loadglobalsettingv2?_type=' + val;

        $.ajax({
            type: 'GET',
            url: s,
            dataType: 'json',
            success: function(data) {
                if (val === 4) {
                    $('#_smslimit').val(data._smslimit);
                    $('#_smscost').val(data._smscost);
                    $('#_smsStatus').val(data._smsStatus);
                    $('#_smsAlertStatus').val(data._smsAlertStatus);
                    $('#_smsRepeatStatus').val(data._smsRepeatStatus);
//            $('#_smsRepeatDuration').val(data._smsRepeatDuration);
                    $('#_smsThresholdlimit').val(data._smsThresholdlimit);
                    $('#_fastPaceSMS').val(data._fastPaceSMS);
                    $('#_hyperPaceSMS').val(data._hyperPaceSMS);
                    $('#_normalPaceSMS').val(data._normalPaceSMS);
                    $('#_slowPaceSMS').val(data._slowPaceSMS);
                    $('#_smsFooter').val(data._smsFooter);


                    $('#_dayRestrictionSMS').val(data._dayRestrictionSMS);
                    $('#_timeFromRestrictionSMS').val(data._timeFromRestrictionSMS);
                    $('#_timeToRestrictionSMS').val(data._timeToRestrictionSMS);

                    $('#_smsFooterStatus').val(data._smsFooterStatus);
                    if (data._smsFooterStatus === 1)
                        ChangeFooterStatusSMS(1);
                    else if (data._smsFooterStatus === 0)
                        ChangeFooterStatusSMS(0);

                    if (data._smsStatus === 1)
                        ChangeLimitStatusSMS(1);
                    else if (data._smsStatus === 0)
                        ChangeLimitStatusSMS(0);

                    if (data._smsAlertStatus === 1)
                        ChangeLimitAlertStatusSMS(1);
                    else if (data._smsAlertStatus === 0)
                        ChangeLimitAlertStatusSMS(0);

                    if (data._smsRepeatStatus === 1)
                        ChangeRepeatStatusSMS(1);
                    else if (data._smsRepeatStatus === 2)
                        ChangeRepeatStatusSMS(2);
                    else if (data._smsRepeatStatus === 3)
                        ChangeRepeatStatusSMS(3);



                    if (data._slowPaceSMS === 1) {
                        SetPACESMS(1, 1, '#_slowPaceSMS_div', '1 message/second');
                    }
                    else if (data._slowPaceSMS === 3) {

                        SetPACESMS(1, 3, '#_slowPaceSMS_div', '3 messages/second');
                    }

                    else if (data._slowPaceSMS === 5) {

                        SetPACESMS(1, 5, '#_slowPaceSMS_div', '5 messages/second');
                    }

                    if (data._normalPaceSMS === 6)
                        SetPACESMS(2, 6, '#_normalPaceSMS_div', '6 messages/second');
                    else if (data._normalPaceSMS === 8)
                        SetPACESMS(2, 8, '#_normalPaceSMS_div', '8 messages/second');
                    else if (data._normalPaceSMS === 10)
                        SetPACESMS(2, 10, '#_normalPaceSMS_div', '10 messages/second');

                    if (data._fastPaceSMS === 15)
                        SetPACESMS(3, 15, '#_fastPaceSMS_div', '15 messages/second');
                    else if (data._fastPaceSMS === 20)
                        SetPACESMS(3, 20, '#_fastPaceSMS_div', '20 messages/second');
                    else if (data._fastPaceSMS === 25)
                        SetPACESMS(3, 25, '#_fastPaceSMS_div', '25 messages/second');
                    else if (data._fastPaceSMS === 30)
                        SetPACESMS(3, 30, '#_fastPaceSMS_div', '30 messages/second');

                    if (data._hyperPaceSMS === 35)
                        SetPACESMS(4, 35, '#_hyperPaceSMS_div', '35 messages/second');
                    else if (data._hyperPaceSMS === 50)
                        SetPACESMS(4, 50, '#_hyperPaceSMS_div', '50 messages/second');
                    else if (data._hyperPaceSMS === 75)
                        SetPACESMS(4, 75, '#_hyperPaceSMS_div', '75 messages/second');
                    else if (data._hyperPaceSMS === 100)
                        SetPACESMS(4, 100, '#_hyperPaceSMS_div', '100 messages/second');

                    if (data._dayRestrictionSMS === 1)
                        ChangeDayRestrictionSMS(1, 'Week days only');
                    else if (data._dayRestrictionSMS === 2)
                        ChangeDayRestrictionSMS(2, 'Whole Week (including Weekend)');

                    if (data._timeFromRestrictionSMS === 7)
                        ChangeTimeRestrictionSMS(1, 7, '7AM');
                    else if (data._timeFromRestrictionSMS === 8)
                        ChangeTimeRestrictionSMS(1, 8, '8AM');
                    else if (data._timeFromRestrictionSMS === 9)
                        ChangeTimeRestrictionSMS(1, 9, '9AM');
                    else if (data._timeFromRestrictionSMS === 10)
                        ChangeTimeRestrictionSMS(1, 10, '10AM');
                    else if (data._timeFromRestrictionSMS === 11)
                        ChangeTimeRestrictionSMS(1, 11, '11AM');
                    else if (data._timeFromRestrictionSMS === 0)
                        ChangeTimeRestrictionSMS(1, 0, 'Any');

                    if (data._timeToRestrictionSMS === 12)
                        ChangeTimeRestrictionSMS(2, 12, '12PM');
                    else if (data._timeToRestrictionSMS === 13)
                        ChangeTimeRestrictionSMS(2, 13, '1PM');
                    else if (data._timeToRestrictionSMS === 14)
                        ChangeTimeRestrictionSMS(2, 14, '2PM');
                    else if (data._timeToRestrictionSMS === 15)
                        ChangeTimeRestrictionSMS(2, 15, '3PM');
                    else if (data._timeToRestrictionSMS === 16)
                        ChangeTimeRestrictionSMS(2, 16, '4PM');
                    else if (data._timeToRestrictionSMS === 17)
                        ChangeTimeRestrictionSMS(2, 17, '5PM');
                    else if (data._timeToRestrictionSMS === 18)
                        ChangeTimeRestrictionSMS(2, 18, '6PM');
                    else if (data._timeToRestrictionSMS === 19)
                        ChangeTimeRestrictionSMS(2, 19, '7PM');
                    else if (data._timeToRestrictionSMS === 24)
                        ChangeTimeRestrictionSMS(2, 24, 'Any');


                }
                if (val === 5) {
                    $('#_voicelimit').val(data._voicelimit);
                    $('#_voicecost').val(data._voicecost);
                    $('#_voiceStatus').val(data._voiceStatus);
                    $('#_voiceAlertStatus').val(data._voiceAlertStatus);
                    $('#_voiceRepeatStatus').val(data._voiceRepeatStatus);
//            $('#_voiceRepeatDuration').val(data._voiceRepeatDuration);
                    $('#_voiceThresholdlimit').val(data._voiceThresholdlimit);
                    $('#_fastPaceVOICE').val(data._fastPaceVOICE);
                    $('#_hyperPaceVOICE').val(data._hyperPaceVOICE);
                    $('#_normalPaceVOICE').val(data._normalPaceVOICE);
                    $('#_slowPaceVOICE').val(data._slowPaceVOICE);
                    $('#_voiceFooter').val(data._voiceFooter);

                    $('#_dayRestrictionVOICE').val(data._dayRestrictionVOICE);
                    $('#_timeFromRestrictionVOICE').val(data._timeFromRestrictionVOICE);
                    $('#_timeToRestrictionVOICE').val(data._timeToRestrictionVOICE);

                    $('#_voiceFooterStatus').val(data._voiceFooterStatus);
                    if (data._voiceFooterStatus === 1)
                        ChangeFooterStatusVOICE(1);
                    else if (data._voiceFooterStatus === 0)
                        ChangeFooterStatusVOICE(0);

                    if (data._dayRestrictionVOICE === 1)
                        ChangeDayRestrictionVOICE(1, 'Week days only');
                    else if (data._dayRestrictionVOICE === 2)
                        ChangeDayRestrictionVOICE(2, 'Whole Week (including Weekend)');

                    if (data._timeFromRestrictionVOICE === 7)
                        ChangeTimeRestrictionVOICE(1, 7, '7AM');
                    else if (data._timeFromRestrictionVOICE === 8)
                        ChangeTimeRestrictionVOICE(1, 8, '8AM');
                    else if (data._timeFromRestrictionVOICE === 9)
                        ChangeTimeRestrictionVOICE(1, 9, '9AM');
                    else if (data._timeFromRestrictionVOICE === 10)
                        ChangeTimeRestrictionVOICE(1, 10, '10AM');
                    else if (data._timeFromRestrictionVOICE === 11)
                        ChangeTimeRestrictionVOICE(1, 11, '11AM');
                    else if (data._timeFromRestrictionVOICE === 0)
                        ChangeTimeRestrictionVOICE(1, 0, 'Any');

                    if (data._timeToRestrictionVOICE === 12)
                        ChangeTimeRestrictionVOICE(2, 12, '12PM');
                    else if (data._timeToRestrictionVOICE === 13)
                        ChangeTimeRestrictionVOICE(2, 13, '1PM');
                    else if (data._timeToRestrictionVOICE === 14)
                        ChangeTimeRestrictionVOICE(2, 14, '2PM');
                    else if (data._timeToRestrictionVOICE === 15)
                        ChangeTimeRestrictionVOICE(2, 15, '3PM');
                    else if (data._timeToRestrictionVOICE === 16)
                        ChangeTimeRestrictionVOICE(2, 16, '4PM');
                    else if (data._timeToRestrictionVOICE === 17)
                        ChangeTimeRestrictionVOICE(2, 17, '5PM');
                    else if (data._timeToRestrictionVOICE === 18)
                        ChangeTimeRestrictionVOICE(2, 18, '6PM');
                    else if (data._timeToRestrictionVOICE === 19)
                        ChangeTimeRestrictionVOICE(2, 19, '7PM');
                    else if (data._timeToRestrictionVOICE === 24)
                        ChangeTimeRestrictionVOICE(2, 24, 'Any');



                    if (data._voiceStatus === 1)
                        ChangeLimitStatusVOICE(1);
                    else if (data._voiceStatus === 0)
                        ChangeLimitStatusVOICE(0);

                    if (data._voiceAlertStatus === 1)
                        ChangeLimitAlertStatusVOICE(1);
                    else if (data._voiceAlertStatus === 0)
                        ChangeLimitAlertStatusVOICE(0);

                    if (data._voiceRepeatStatus === 1)
                        ChangeRepeatStatusVOICE(1);
                    else if (data._voiceRepeatStatus === 2)
                        ChangeRepeatStatusVOICE(2);
                    else if (data._voiceRepeatStatus === 3)
                        ChangeRepeatStatusVOICE(3);

//                 if(data._voiceRepeatDuration === 1)
//                ChangeRepeatDurationVOICE(1);
//            else if(data._voiceRepeatDuration === 2)
//                ChangeRepeatDurationVOICE(2);
//             else if(data._voiceRepeatDuration === 3)
//                ChangeRepeatDurationVOICE(3);
//            else if(data._voiceRepeatDuration === 4)
//                ChangeRepeatDurationVOICE(4);



                    if (data._slowPaceVOICE === 1) {
                        SetPACEVOICE(1, 1, '#_slowPaceVOICE_div', '1 message/second');
                    }
                    else if (data._slowPaceVOICE === 3) {

                        SetPACEVOICE(1, 3, '#_slowPaceVOICE_div', '3 messages/second');
                    }

                    else if (data._slowPaceVOICE === 5) {

                        SetPACEVOICE(1, 5, '#_slowPaceVOICE_div', '5 messages/second');
                    }

                    if (data._normalPaceVOICE === 6)
                        SetPACEVOICE(2, 6, '#_normalPaceVOICE_div', '6 messages/second');
                    else if (data._normalPaceVOICE === 8)
                        SetPACEVOICE(2, 8, '#_normalPaceVOICE_div', '8 messages/second');
                    else if (data._normalPaceVOICE === 10)
                        SetPACEVOICE(2, 10, '#_normalPaceVOICE_div', '10 messages/second');

                    if (data._fastPaceVOICE === 15)
                        SetPACEVOICE(3, 15, '#_fastPaceVOICE_div', '15 messages/second');
                    else if (data._fastPaceVOICE === 20)
                        SetPACEVOICE(3, 20, '#_fastPaceVOICE_div', '20 messages/second');
                    else if (data._fastPaceVOICE === 25)
                        SetPACEVOICE(3, 25, '#_fastPaceVOICE_div', '25 messages/second');
                    else if (data._fastPaceVOICE === 30)
                        SetPACEVOICE(3, 30, '#_fastPaceVOICE_div', '30 messages/second');

                    if (data._hyperPaceVOICE === 35)
                        SetPACEVOICE(4, 35, '#_hyperPaceVOICE_div', '35 messages/second');
                    else if (data._hyperPaceVOICE === 50)
                        SetPACEVOICE(4, 50, '#_hyperPaceVOICE_div', '50 messages/second');
                    else if (data._hyperPaceVOICE == 75)
                        SetPACEVOICE(4, 75, '#_hyperPaceVOICE_div', '75 messages/second');
                    else if (data._hyperPaceVOICE == 100)
                        SetPACEVOICE(4, 100, '#_hyperPaceVOICE_div', '100 messages/second');

                }

                if (val === 6) {
                    $('#_ussdlimit').val(data._ussdlimit);
                    $('#_ussdcost').val(data._ussdcost);
                    $('#_ussdStatus').val(data._ussdStatus);
                    $('#_ussdAlertStatus').val(data._ussdAlertStatus);
                    $('#_ussdRepeatStatus').val(data._ussdRepeatStatus);
//            $('#_ussdRepeatDuration').val(data._ussdRepeatDuration);
                    $('#_ussdThresholdlimit').val(data._ussdThresholdlimit);
                    $('#_fastPaceUSSD').val(data._fastPaceUSSD);
                    $('#_hyperPaceUSSD').val(data._hyperPaceUSSD);
                    $('#_normalPaceUSSD').val(data._normalPaceUSSD);
                    $('#_slowPaceUSSD').val(data._slowPaceUSSD);
                    $('#_ussdFooter').val(data._ussdFooter);

                    $('#_dayRestrictionUSSD').val(data._dayRestrictionUSSD);
                    $('#_timeFromRestrictionUSSD').val(data._timeFromRestrictionUSSD);
                    $('#_timeToRestrictionUSSD').val(data._timeToRestrictionUSSD);

                    $('#_ussdFooterStatus').val(data._ussdFooterStatus);
                    if (data._ussdFooterStatus === 1)
                        ChangeFooterStatusUSSD(1);
                    else if (data._ussdFooterStatus === 0)
                        ChangeFooterStatusUSSD(0);

                    if (data._dayRestrictionUSSD === 1)
                        ChangeDayRestrictionUSSD(1, 'Week days only');
                    else if (data._dayRestrictionUSSD === 2)
                        ChangeDayRestrictionUSSD(2, 'Whole Week (including Weekend)');

                    if (data._timeFromRestrictionUSSD === 7)
                        ChangeTimeRestrictionUSSD(1, 7, '7AM');
                    else if (data._timeFromRestrictionUSSD === 8)
                        ChangeTimeRestrictionUSSD(1, 8, '8AM');
                    else if (data._timeFromRestrictionUSSD === 9)
                        ChangeTimeRestrictionUSSD(1, 9, '9AM');
                    else if (data._timeFromRestrictionUSSD === 10)
                        ChangeTimeRestrictionUSSD(1, 10, '10AM');
                    else if (data._timeFromRestrictionUSSD === 11)
                        ChangeTimeRestrictionUSSD(1, 11, '11AM');
                    else if (data._timeFromRestrictionUSSD === 0)
                        ChangeTimeRestrictionUSSD(1, 0, 'Any');

                    if (data._timeToRestrictionUSSD === 12)
                        ChangeTimeRestrictionUSSD(2, 12, '12PM');
                    else if (data._timeToRestrictionUSSD === 13)
                        ChangeTimeRestrictionUSSD(2, 13, '1PM');
                    else if (data._timeToRestrictionUSSD === 14)
                        ChangeTimeRestrictionUSSD(2, 14, '2PM');
                    else if (data._timeToRestrictionUSSD === 15)
                        ChangeTimeRestrictionUSSD(2, 15, '3PM');
                    else if (data._timeToRestrictionUSSD === 16)
                        ChangeTimeRestrictionUSSD(2, 16, '4PM');
                    else if (data._timeToRestrictionUSSD === 17)
                        ChangeTimeRestrictionUSSD(2, 17, '5PM');
                    else if (data._timeToRestrictionUSSD === 18)
                        ChangeTimeRestrictionUSSD(2, 18, '6PM');
                    else if (data._timeToRestrictionUSSD === 19)
                        ChangeTimeRestrictionUSSD(2, 19, '7PM');
                    else if (data._timeToRestrictionUSSD === 24)
                        ChangeTimeRestrictionUSSD(2, 24, 'Any');



                    if (data._ussdStatus === 1)
                        ChangeLimitStatusUSSD(1);
                    else if (data._ussdStatus === 0)
                        ChangeLimitStatusUSSD(0);

                    if (data._ussdAlertStatus === 1)
                        ChangeLimitAlertStatusUSSD(1);
                    else if (data._ussdAlertStatus === 0)
                        ChangeLimitAlertStatusUSSD(0);

                    if (data._ussdRepeatStatus === 1)
                        ChangeRepeatStatusUSSD(1);
                    else if (data._ussdRepeatStatus === 2)
                        ChangeRepeatStatusUSSD(2);
                    else if (data._ussdRepeatStatus === 3)
                        ChangeRepeatStatusUSSD(3);

//                 if(data._ussdRepeatDuration === 1)
//                ChangeRepeatDurationUSSD(1);
//            else if(data._ussdRepeatDuration === 2)
//                ChangeRepeatDurationUSSD(2);
//             else if(data._ussdRepeatDuration === 3)
//                ChangeRepeatDurationUSSD(3);
//            else if(data._ussdRepeatDuration === 4)
//                ChangeRepeatDurationUSSD(4);



                    if (data._slowPaceUSSD === 1) {
                        SetPACEUSSD(1, 1, '#_slowPaceUSSD_div', '1 message/second');
                    }
                    else if (data._slowPaceUSSD === 3) {

                        SetPACEUSSD(1, 3, '#_slowPaceUSSD_div', '3 messages/second');
                    }

                    else if (data._slowPaceUSSD === 5) {

                        SetPACEUSSD(1, 5, '#_slowPaceUSSD_div', '5 messages/second');
                    }

                    if (data._normalPaceUSSD === 6)
                        SetPACEUSSD(2, 6, '#_normalPaceUSSD_div', '6 messages/second');
                    else if (data._normalPaceUSSD === 8)
                        SetPACEUSSD(2, 8, '#_normalPaceUSSD_div', '8 messages/second');
                    else if (data._normalPaceUSSD === 10)
                        SetPACEUSSD(2, 10, '#_normalPaceUSSD_div', '10 messages/second');

                    if (data._fastPaceUSSD === 15)
                        SetPACEUSSD(3, 15, '#_fastPaceUSSD_div', '15 messages/second');
                    else if (data._fastPaceUSSD === 20)
                        SetPACEUSSD(3, 20, '#_fastPaceUSSD_div', '20 messages/second');
                    else if (data._fastPaceUSSD === 25)
                        SetPACEUSSD(3, 25, '#_fastPaceUSSD_div', '25 messages/second');
                    else if (data._fastPaceUSSD === 30)
                        SetPACEUSSD(3, 30, '#_fastPaceUSSD_div', '30 messages/second');

                    if (data._hyperPaceUSSD === 35)
                        SetPACEUSSD(4, 35, '#_hyperPaceUSSD_div', '35 messages/second');
                    else if (data._hyperPaceUSSD === 50)
                        SetPACEUSSD(4, 50, '#_hyperPaceUSSD_div', '50 messages/second');
                    else if (data._hyperPaceUSSD === 75)
                        SetPACEUSSD(4, 75, '#_hyperPaceUSSD_div', '75 messages/second');
                    else if (data._hyperPaceUSSD === 100)
                        SetPACEUSSD(4, 100, '#_hyperPaceUSSD_div', '100 messages/second');

                }

                if (val === 7) {
                    $('#_emaillimit').val(data._emaillimit);
                    $('#_emailcost').val(data._emailcost);
                    $('#_emailStatus').val(data._emailStatus);
                    $('#_emailAlertStatus').val(data._emailAlertStatus);
                    $('#_emailRepeatStatus').val(data._emailRepeatStatus);
//            $('#_emailRepeatDuration').val(data._emailRepeatDuration);
                    $('#_emailThresholdlimit').val(data._emailThresholdlimit);
                    $('#_fastPaceEMAIL').val(data._fastPaceEMAIL);
                    $('#_hyperPaceEMAIL').val(data._hyperPaceEMAIL);
                    $('#_normalPaceEMAIL').val(data._normalPaceEMAIL);
                    $('#_slowPaceEMAIL').val(data._slowPaceEMAIL);
//                $('#_emailFooter').val(data._emailFooter);

                    $('#_dayRestrictionEMAIL').val(data._dayRestrictionEMAIL);
                    $('#_timeFromRestrictionEMAIL').val(data._timeFromRestrictionEMAIL);
                    $('#_timeToRestrictionEMAIL').val(data._timeToRestrictionEMAIL);

//                alert(data._emailFooter);
//                    CKEDITOR.instances['_emailFooter'].setData(data._emailFooter);
//            $('#_emailFooter').val(data._emailFooter);


                    $('#_emailFooterStatus').val(data._emailFooterStatus);
                    if (data._emailFooterStatus === 1)
                        ChangeFooterStatusEMAIL(1);
                    else if (data._emailFooterStatus === 0)
                        ChangeFooterStatusEMAIL(0);

                    if (data._dayRestrictionEMAIL === 1)
                        ChangeDayRestrictionEMAIL(1, 'Week days only');
                    else if (data._dayRestrictionEMAIL === 2)
                        ChangeDayRestrictionEMAIL(2, 'Whole Week (including Weekend)');

                    if (data._timeFromRestrictionEMAIL === 7)
                        ChangeTimeRestrictionEMAIL(1, 7, '7AM');
                    else if (data._timeFromRestrictionEMAIL === 8)
                        ChangeTimeRestrictionEMAIL(1, 8, '8AM');
                    else if (data._timeFromRestrictionEMAIL === 9)
                        ChangeTimeRestrictionEMAIL(1, 9, '9AM');
                    else if (data._timeFromRestrictionEMAIL === 10)
                        ChangeTimeRestrictionEMAIL(1, 10, '10AM');
                    else if (data._timeFromRestrictionEMAIL === 11)
                        ChangeTimeRestrictionEMAIL(1, 11, '11AM');
                    else if (data._timeFromRestrictionEMAIL === 0)
                        ChangeTimeRestrictionEMAIL(1, 0, 'Any');

                    if (data._timeToRestrictionEMAIL === 12)
                        ChangeTimeRestrictionEMAIL(2, 12, '12PM');
                    else if (data._timeToRestrictionEMAIL === 13)
                        ChangeTimeRestrictionEMAIL(2, 13, '1PM');
                    else if (data._timeToRestrictionEMAIL === 14)
                        ChangeTimeRestrictionEMAIL(2, 14, '2PM');
                    else if (data._timeToRestrictionEMAIL === 15)
                        ChangeTimeRestrictionEMAIL(2, 15, '3PM');
                    else if (data._timeToRestrictionEMAIL === 16)
                        ChangeTimeRestrictionEMAIL(2, 16, '4PM');
                    else if (data._timeToRestrictionEMAIL === 17)
                        ChangeTimeRestrictionEMAIL(2, 17, '5PM');
                    else if (data._timeToRestrictionEMAIL === 18)
                        ChangeTimeRestrictionEMAIL(2, 18, '6PM');
                    else if (data._timeToRestrictionEMAIL === 19)
                        ChangeTimeRestrictionEMAIL(2, 19, '7PM');
                    else if (data._timeToRestrictionEMAIL === 24)
                        ChangeTimeRestrictionEMAIL(2, 24, 'Any');


                    if (data._emailStatus === 1)
                        ChangeLimitStatusEMAIL(1);
                    else if (data._emailStatus === 0)
                        ChangeLimitStatusEMAIL(0);

                    if (data._emailAlertStatus === 1)
                        ChangeLimitAlertStatusEMAIL(1);
                    else if (data._emailAlertStatus === 0)
                        ChangeLimitAlertStatusEMAIL(0);

                    if (data._emailRepeatStatus === 1)
                        ChangeRepeatStatusEMAIL(1);
                    else if (data._emailRepeatStatus === 2)
                        ChangeRepeatStatusEMAIL(2);
                    else if (data._emailRepeatStatus === 3)
                        ChangeRepeatStatusEMAIL(3);

//                 if(data._emailRepeatDuration === 1)
//                ChangeRepeatDurationEMAIL(1);
//            else if(data._emailRepeatDuration === 2)
//                ChangeRepeatDurationEMAIL(2);
//             else if(data._emailRepeatDuration === 3)
//                ChangeRepeatDurationEMAIL(3);
//            else if(data._emailRepeatDuration === 4)
//                ChangeRepeatDurationEMAIL(4);



                    if (data._slowPaceEMAIL === 1) {
                        SetPACEEMAIL(1, 1, '#_slowPaceEMAIL_div', '1 message/second');
                    }
                    else if (data._slowPaceEMAIL === 3) {

                        SetPACEEMAIL(1, 3, '#_slowPaceEMAIL_div', '3 messages/second');
                    }

                    else if (data._slowPaceEMAIL === 5) {

                        SetPACEEMAIL(1, 5, '#_slowPaceEMAIL_div', '5 messages/second');
                    }

                    if (data._normalPaceEMAIL === 6)
                        SetPACEEMAIL(2, 6, '#_normalPaceEMAIL_div', '6 messages/second');
                    else if (data._normalPaceEMAIL === 8)
                        SetPACEEMAIL(2, 8, '#_normalPaceEMAIL_div', '8 messages/second');
                    else if (data._normalPaceEMAIL === 10)
                        SetPACEEMAIL(2, 10, '#_normalPaceEMAIL_div', '10 messages/second');

                    if (data._fastPaceEMAIL === 15)
                        SetPACEEMAIL(3, 15, '#_fastPaceEMAIL_div', '15 messages/second');
                    else if (data._fastPaceEMAIL === 20)
                        SetPACEEMAIL(3, 20, '#_fastPaceEMAIL_div', '20 messages/second');
                    else if (data._fastPaceEMAIL === 25)
                        SetPACEEMAIL(3, 25, '#_fastPaceEMAIL_div', '25 messages/second');
                    else if (data._fastPaceEMAIL === 30)
                        SetPACEEMAIL(3, 30, '#_fastPaceEMAIL_div', '30 messages/second');

                    if (data._hyperPaceEMAIL === 35)
                        SetPACEEMAIL(4, 35, '#_hyperPaceEMAIL_div', '35 messages/second');
                    else if (data._hyperPaceEMAIL === 50)
                        SetPACEEMAIL(4, 50, '#_hyperPaceEMAIL_div', '50 messages/second');
                    else if (data._hyperPaceEMAIL === 75)
                        SetPACEEMAIL(4, 75, '#_hyperPaceEMAIL_div', '75 messages/second');
                    else if (data._hyperPaceEMAIL === 100)
                        SetPACEEMAIL(4, 100, '#_hyperPaceEMAIL_div', '100 messages/second');

                }
                if (val === 8) {
                    $('#_faxlimit').val(data._faxlimit);
                    $('#_faxcost').val(data._faxcost);
                    $('#_faxStatus').val(data._faxStatus);
                    $('#_faxAlertStatus').val(data._faxAlertStatus);
                    $('#_faxRepeatStatus').val(data._faxRepeatStatus);
//            $('#_faxRepeatDuration').val(data._faxRepeatDuration);
                    $('#_faxThresholdlimit').val(data._faxThresholdlimit);
                    $('#_fastPaceFAX').val(data._fastPaceFAX);
                    $('#_hyperPaceFAX').val(data._hyperPaceFAX);
                    $('#_normalPaceFAX').val(data._normalPaceFAX);
                    $('#_slowPaceFAX').val(data._slowPaceFAX);
                    $('#_faxFooter').val(data._faxFooter);

                    $('#_dayRestrictionFAX').val(data._dayRestrictionFAX);
                    $('#_timeFromRestrictionFAX').val(data._timeFromRestrictionFAX);
                    $('#_timeToRestrictionFAX').val(data._timeToRestrictionFAX);

//                 CKEDITOR.instances['_faxFooter'].setData(data._faxFooter);
//            $('#_faxFooter').val(data._faxFooter);


                    $('#faxFooterStatus').val(data._faxFooterStatus);
                    if (data._faxFooterStatus === 1)
                        ChangeFooterStatusFAX(1);
                    else if (data._faxFooterStatus === 0)
                        ChangeFooterStatusFAX(0);

                    if (data._dayRestrictionFAX === 1)
                        ChangeDayRestrictionFAX(1, 'Week days only');
                    else if (data._dayRestrictionFAX === 2)
                        ChangeDayRestrictionFAX(2, 'Whole Week (including Weekend)');

                    if (data._timeFromRestrictionFAX === 7)
                        ChangeTimeRestrictionFAX(1, 7, '7AM');
                    else if (data._timeFromRestrictionFAX === 8)
                        ChangeTimeRestrictionFAX(1, 8, '8AM');
                    else if (data._timeFromRestrictionFAX === 9)
                        ChangeTimeRestrictionFAX(1, 9, '9AM');
                    else if (data._timeFromRestrictionFAX === 10)
                        ChangeTimeRestrictionFAX(1, 10, '10AM');
                    else if (data._timeFromRestrictionFAX === 11)
                        ChangeTimeRestrictionFAX(1, 11, '11AM');
                    else if (data._timeFromRestrictionFAX === 0)
                        ChangeTimeRestrictionFAX(1, 0, 'Any');

                    if (data._timeToRestrictionFAX === 12)
                        ChangeTimeRestrictionFAX(2, 12, '12PM');
                    else if (data._timeToRestrictionFAX === 13)
                        ChangeTimeRestrictionFAX(2, 13, '1PM');
                    else if (data._timeToRestrictionFAX === 14)
                        ChangeTimeRestrictionFAX(2, 14, '2PM');
                    else if (data._timeToRestrictionFAX === 15)
                        ChangeTimeRestrictionFAX(2, 15, '3PM');
                    else if (data._timeToRestrictionFAX === 16)
                        ChangeTimeRestrictionFAX(2, 16, '4PM');
                    else if (data._timeToRestrictionFAX === 17)
                        ChangeTimeRestrictionFAX(2, 17, '5PM');
                    else if (data._timeToRestrictionFAX === 18)
                        ChangeTimeRestrictionFAX(2, 18, '6PM');
                    else if (data._timeToRestrictionFAX === 19)
                        ChangeTimeRestrictionFAX(2, 19, '7PM');
                    else if (data._timeToRestrictionFAX === 24)
                        ChangeTimeRestrictionFAX(2, 24, 'Any');



                    if (data._faxStatus === 1)
                        ChangeLimitStatusFAX(1);
                    else if (data._faxStatus === 0)
                        ChangeLimitStatusFAX(0);

                    if (data._faxAlertStatus === 1)
                        ChangeLimitAlertStatusFAX(1);
                    else if (data._faxAlertStatus === 0)
                        ChangeLimitAlertStatusFAX(0);

                    if (data._faxRepeatStatus === 1)
                        ChangeRepeatStatusFAX(1);
                    else if (data._faxRepeatStatus === 2)
                        ChangeRepeatStatusFAX(2);
                    else if (data._faxRepeatStatus === 3)
                        ChangeRepeatStatusFAX(3);

//                 if(data._faxRepeatDuration === 1)
//                ChangeRepeatDurationFAX(1);
//            else if(data._faxRepeatDuration === 2)
//                ChangeRepeatDurationFAX(2);
//             else if(data._faxRepeatDuration === 3)
//                ChangeRepeatDurationFAX(3);
//            else if(data._faxRepeatDuration === 4)
//                ChangeRepeatDurationFAX(4);



                    if (data._slowPaceFAX === 1) {
                        SetPACEFAX(1, 1, '#_slowPaceFAX_div', '1 message/second');
                    }
                    else if (data._slowPaceFAX === 3) {

                        SetPACEFAX(1, 3, '#_slowPaceFAX_div', '3 messages/second');
                    }

                    else if (data._slowPaceFAX === 5) {

                        SetPACEFAX(1, 5, '#_slowPaceFAX_div', '5 messages/second');
                    }

                    if (data._normalPaceFAX === 6)
                        SetPACEFAX(2, 6, '#_normalPaceFAX_div', '6 messages/second');
                    else if (data._normalPaceFAX === 8)
                        SetPACEFAX(2, 8, '#_normalPaceFAX_div', '8 messages/second');
                    else if (data._normalPaceFAX === 10)
                        SetPACEFAX(2, 10, '#_normalPaceFAX_div', '10 messages/second');

                    if (data._fastPaceFAX === 15)
                        SetPACEFAX(3, 15, '#_fastPaceFAX_div', '15 messages/second');
                    else if (data._fastPaceFAX === 20)
                        SetPACEFAX(3, 20, '#_fastPaceFAX_div', '20 messages/second');
                    else if (data._fastPaceFAX === 25)
                        SetPACEFAX(3, 25, '#_fastPaceFAX_div', '25 messages/second');
                    else if (data._fastPaceFAX === 30)
                        SetPACEFAX(3, 30, '#_fastPaceFAX_div', '30 messages/second');

                    if (data._hyperPaceFAX === 35)
                        SetPACEFAX(4, 35, '#_hyperPaceFAX_div', '35 messages/second');
                    else if (data._hyperPaceFAX === 50)
                        SetPACEFAX(4, 50, '#_hyperPaceFAX_div', '50 messages/second');
                    else if (data._hyperPaceFAX === 75)
                        SetPACEFAX(4, 75, '#_hyperPaceFAX_div', '75 messages/second');
                    else if (data._hyperPaceFAX === 100)
                        SetPACEFAX(4, 100, '#_hyperPacFAX_div', '100 messages/second');

                }

                if (val === 9) {
                    $('#_pushlimit').val(data._pushlimit);
                    $('#_pushcost').val(data._pushcost);
                    $('#_pushStatus').val(data._pushStatus);
                    $('#_pushAlertStatus').val(data._pushAlertStatus);
                    $('#_pushRepeatStatus').val(data._pushRepeatStatus);
//            $('#_pushRepeatDuration').val(data._pushRepeatDuration);
                    $('#_pushThresholdlimit').val(data._pushThresholdlimit);
                    $('#_fastPacePUSH').val(data._fastPacePUSH);
                    $('#_hyperPacePUSH').val(data._hyperPacePUSH);
                    $('#_normalPacePUSH').val(data._normalPacePUSH);
                    $('#_slowPacePUSH').val(data._slowPacePUSH);
                    $('#_pushFooter').val(data._pushFooter);

                    $('#_dayRestrictionPUSH').val(data._dayRestrictionPUSH);
                    $('#_timeFromRestrictionPUSH').val(data._timeFromRestrictionPUSH);
                    $('#_timeToRestrictionPUSH').val(data._timeToRestrictionPUSH);

                    $('#pushFooterStatus').val(data._pushFooterStatus);
                    if (data._pushFooterStatus === 1)
                        ChangeFooterStatusPUSH(1);
                    else if (data._pushFooterStatus === 0)
                        ChangeFooterStatusPUSH(0);

                    if (data._dayRestrictionPUSH === 1)
                        ChangeDayRestrictionPUSH(1, 'Week days only');
                    else if (data._dayRestrictionPUSH === 2)
                        ChangeDayRestrictionPUSH(2, 'Whole Week (including Weekend)');

                    if (data._timeFromRestrictionPUSH === 7)
                        ChangeTimeRestrictionPUSH(1, 7, '7AM');
                    else if (data._timeFromRestrictionPUSH === 8)
                        ChangeTimeRestrictionPUSH(1, 8, '8AM');
                    else if (data._timeFromRestrictionFAX === 9)
                        ChangeTimeRestrictionPUSH(1, 9, '9AM');
                    else if (data._timeFromRestrictionPUSH === 10)
                        ChangeTimeRestrictionPUSH(1, 10, '10AM');
                    else if (data._timeFromRestrictionPUSH === 11)
                        ChangeTimeRestrictionPUSH(1, 11, '11AM');
                    else if (data._timeFromRestrictionPUSH === 0)
                        ChangeTimeRestrictionPUSH(1, 0, 'Any');

                    if (data._timeToRestrictionPUSH === 12)
                        ChangeTimeRestrictionPUSH(2, 12, '12PM');
                    else if (data._timeToRestrictionPUSH === 13)
                        ChangeTimeRestrictionPUSH(2, 13, '1PM');
                    else if (data._timeToRestrictionPUSH === 14)
                        ChangeTimeRestrictionPUSH(2, 14, '2PM');
                    else if (data._timeToRestrictionPUSH === 15)
                        ChangeTimeRestrictionPUSH(2, 15, '3PM');
                    else if (data._timeToRestrictionPUSH === 16)
                        ChangeTimeRestrictionPUSH(2, 16, '4PM');
                    else if (data._timeToRestrictionPUSH === 17)
                        ChangeTimeRestrictionPUSH(2, 17, '5PM');
                    else if (data._timeToRestrictionPUSH === 18)
                        ChangeTimeRestrictionPUSH(2, 18, '6PM');
                    else if (data._timeToRestrictionPUSH === 19)
                        ChangeTimeRestrictionPUSH(2, 19, '7PM');
                    else if (data._timeToRestrictionPUSH === 24)
                        ChangeTimeRestrictionPUSH(2, 24, 'Any');



                    if (data._pushStatus === 1)
                        ChangeLimitStatusPUSH(1);
                    else if (data._pushStatus === 0)
                        ChangeLimitStatusPUSH(0);

                    if (data._pushAlertStatus === 1)
                        ChangeLimitAlertStatusPUSH(1);
                    else if (data._pushAlertStatus === 0)
                        ChangeLimitAlertStatusPUSH(0);

                    if (data._pushRepeatStatus === 1)
                        ChangeRepeatStatusPUSH(1);
                    else if (data._pushRepeatStatus === 2)
                        ChangeRepeatStatusPUSH(2);
                    else if (data._pushRepeatStatus === 3)
                        ChangeRepeatStatusPUSH(3);

//                 if(data._pushRepeatDuration === 1)
//                ChangeRepeatDurationPUSH(1);
//            else if(data._pushRepeatDuration === 2)
//                ChangeRepeatDurationPUSH(2);
//             else if(data._pushRepeatDuration === 3)
//                ChangeRepeatDurationPUSH(3);
//            else if(data._pushRepeatDuration === 4)
//                ChangeRepeatDurationPUSH(4);



                    if (data._slowPacePUSH === 1) {
                        SetPACEPUSH(1, 1, '#_slowPacePUSH_div', '1 message/second');
                    }
                    else if (data._slowPacePUSH === 3) {

                        SetPACEPUSH(1, 3, '#_slowPacePUSH_div', '3 messages/second');
                    }

                    else if (data._slowPacePUSH === 5) {

                        SetPACEPUSH(1, 5, '#_slowPacePUSH_div', '5 messages/second');
                    }

                    if (data._normalPacePUSH === 6)
                        SetPACEPUSH(2, 6, '#_normalPacePUSH_div', '6 messages/second');
                    else if (data._normalPacePUSH === 8)
                        SetPACEPUSH(2, 8, '#_normalPacePUSH_div', '8 messages/second');
                    else if (data._normalPacePUSH === 10)
                        SetPACEPUSH(2, 10, '#_normalPacePUSH_div', '10 messages/second');

                    if (data._fastPacePUSH === 15)
                        SetPACEPUSH(3, 15, '#_fastPacePUSH_div', '15 messages/second');
                    else if (data._fastPacePUSH === 20)
                        SetPACEPUSH(3, 20, '#_fastPacePUSH_div', '20 messages/second');
                    else if (data._fastPacePUSH === 25)
                        SetPACEPUSH(3, 25, '#_fastPacePUSH_div', '25 messages/second');
                    else if (data._fastPacePUSH === 30)
                        SetPACEPUSH(3, 30, '#_fastPacePUSH_div', '30 messages/second');

                    if (data._hyperPacePUSH === 35)
                        SetPACEPUSH(4, 35, '#_hyperPacePUSH_div', '35 messages/second');
                    else if (data._hyperPacePUSH === 50)
                        SetPACEPUSH(4, 50, '#_hyperPacePUSH_div', '50 messages/second');
                    else if (data._hyperPacePUSH === 75)
                        SetPACEPUSH(4, 75, '#_hyperPacePUSH_div', '75 messages/second');
                    else if (data._hyperPacePUSH === 100)
                        SetPACEPUSH(4, 100, '#_hyperPacPUSH_div', '100 messages/second');


                }

                if (val === 13) {
                    $('#_weigtage').val(data._weigtage);
                    $('#_answersattempts').val(data._answersattempts);

                    if (data._weigtage === 75)
                        SetPercentage(75, '75 Percent');
                    else if (data._weigtage === 80)
                        SetPercentage(80, '80 Percent');
                    else if (data._weigtage === 90)
                        SetPercentage(90, '90 Percent');
                    else if (data._weigtage === 100)
                        SetPercentage(100, '100 Percent');

                    if (data._answersattempts === 3)
                        setAttempts(3, '3 attempts')
                    else if (data._answersattempts === 5)
                        setAttempts(5, '5 attempts')
                    else if (data._answersattempts === 7)
                        setAttempts(7, '7 attempts')
                }
//        if(val === 10){
//         if(data._userType === 0)
//                    ChangeUserType(0)
//                else if(data._userType === 1)
//                    ChangeUserType(1)
//            }
//             if(val === 12){
//         if(data._msgType === 0)
//                    ChangeMSGType(0)
//                else if(data._msgType === 1)
//                    ChangeMSGType(1)
//            }
            }});
    }

//day restriction SMS
    function ChangeDayRestrictionSMS(value, disp) {
        $('#_dayRestrictionSMS').val(value);
        $('#_dayRestrictionSMS_div').html(disp);
    }

    function ChangeTimeRestrictionSMS(type, value, disp) {
        if (type === 1) {
            $('#_timeFromRestrictionSMS').val(value);
            $('#_timeFromRestrictionSMS_div').html(disp);
        } else if (type === 2) {
            $('#_timeToRestrictionSMS').val(value);
            $('#_timeToRestrictionSMS_div').html(disp);
        }
    }

//day restriction VOICE
    function ChangeDayRestrictionVOICE(value, disp) {
        $('#_dayRestrictionVOICE').val(value);
        $('#_dayRestrictionVOICE_div').html(disp);
    }

    function ChangeTimeRestrictionVOICE(type, value, disp) {
        if (type === 1) {
            $('#_timeFromRestrictionVOICE').val(value);
            $('#_timeFromRestrictionVOICE_div').html(disp);
        } else if (type === 2) {
            $('#_timeToRestrictionVOICE').val(value);
            $('#_timeToRestrictionVOICE_div').html(disp);
        }
    }

//day restriction USSD
    function ChangeDayRestrictionUSSD(value, disp) {
        $('#_dayRestrictionUSSD').val(value);
        $('#_dayRestrictionUSSD_div').html(disp);
    }

    function ChangeTimeRestrictionUSSD(type, value, disp) {
        if (type === 1) {
            $('#_timeFromRestrictionUSSD').val(value);
            $('#_timeFromRestrictionUSSD_div').html(disp);
        } else if (type === 2) {
            $('#_timeToRestrictionUSSD').val(value);
            $('#_timeToRestrictionUSSD_div').html(disp);
        }
    }

//day restriction EMAIL
    function ChangeDayRestrictionEMAIL(value, disp) {
        $('#_dayRestrictionEMAIL').val(value);
        $('#_dayRestrictionEMAIL_div').html(disp);
    }

    function ChangeTimeRestrictionEMAIL(type, value, disp) {
        if (type === 1) {
            $('#_timeFromRestrictionEMAIL').val(value);
            $('#_timeFromRestrictionEMAIL_div').html(disp);
        } else if (type === 2) {
            $('#_timeToRestrictionEMAIL').val(value);
            $('#_timeToRestrictionEMAIL_div').html(disp);
        }
    }

//day restriction FAX
    function ChangeDayRestrictionFAX(value, disp) {
        $('#_dayRestrictionFAX').val(value);
        $('#_dayRestrictionFAX_div').html(disp);
    }

    function ChangeTimeRestrictionFAX(type, value, disp) {
        if (type === 1) {
            $('#_timeFromRestrictionFAX').val(value);
            $('#_timeFromRestrictionFAX_div').html(disp);
        } else if (type === 2) {
            $('#_timeToRestrictionFAX').val(value);
            $('#_timeToRestrictionFAX_div').html(disp);
        }
    }

//day restriction PUSH
    function ChangeDayRestrictionPUSH(value, disp) {
        $('#_dayRestrictionPUSH').val(value);
        $('#_dayRestrictionPUSH_div').html(disp);
    }

    function ChangeTimeRestrictionPUSH(type, value, disp) {
        if (type === 1) {
            $('#_timeFromRestrictionPUSH').val(value);
            $('#_timeFromRestrictionPUSH_div').html(disp);
        } else if (type === 2) {
            $('#_timeToRestrictionPUSH').val(value);
            $('#_timeToRestrictionPUSH_div').html(disp);
        }


    }

    function SetPercentage(count, disp) {

        if (count === 75) {
            $('#_weigtage').val(count);
            $('#_weigtage_div').html(disp);
        } else if (count === 80) {
            $('#_weigtage').val(count);
            $('#_weigtage_div').html(disp);
        }
        else if (count === 90) {
            $('#_weigtage').val(count);
            $('#_weigtage_div').html(disp);
        } else if (count === 100) {
            $('#_weigtage').val(count);
            $('#_weigtage_div').html(disp);
        }

    }

    function setAttempts(count, disp) {

        if (count === 3) {
            $('#_answersattempts').val(count);
            $('#_answersattempts_div').html(disp);
        } else if (count === 5) {
            $('#_answersattempts').val(count);
            $('#_answersattempts_div').html(disp);
        }
        else if (count === 7) {
            $('#_answersattempts').val(count);
            $('#_answersattempts_div').html(disp);
        }

    }

    function SetPercentagesInGlobalSettings() {
        $('#setPercentage').attr("disabled", true);

        var s = './editglobalsettingv2?_settype=' + 13;
        $.ajax({
            type: 'POST',
            url: s,
            dataType: 'json',
            data: $("#channelcrsettingsform").serialize(),
            success: function(data) {
                if (strcmpGlobal(data._result, "error") === 0) {
                    //$('#save-ContentFilter-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                    Alert4GlobalSetting("<span><font color=red>" + data._message + "</font></span>");
                    $('#setPercentage').attr("disabled", false);
                    //window.setTimeout(RefreshChannelGLobalSetting, 1000);
                }
                else if (strcmpGlobal(data._result, "success") === 0) {
                    //$('#save-ContentFilter-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                    Alert4GlobalSetting("<span><font color=blue>" + data._message + "</font></span>");
                    //window.setTimeout(RefreshChannelGLobalSetting, 1000);

                }
            }
        });
}