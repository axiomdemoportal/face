/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function strcmpCatg(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function Alert4epdf(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(_result) {
        if (_result == false) {

        } else {
            //end here
        }
    });
}
function RefreshCategoryList() {
    window.location.href = './signcategory.jsp';
}

function addCategory() {
    $('#addCategoryButton').attr("disabled", true);
    var s = './addCategory';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#addCategoryForm").serialize(),
        success: function(data) {
            if (strcmpCatg(data._result, "error") == 0) {
                $('#add-category-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#addCategoryButton').attr("disabled", false);
                window.setTimeout(RefreshCategoryList, 3000);
            }
            else if (strcmpCatg(data._result, "success") == 0) {
                $('#add-category-result').html("<span><font color=blue>" + data._message + "</font></span>");
                $('#addCategoryButton').attr("disabled", false);
                window.setTimeout(RefreshCategoryList, 3000);
            }
        }
    });
}

function removeCategory(_cid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './removeCategory?_cid=' + encodeURIComponent(_cid);
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
//        data: $("#uploadCertFileForm").serialize(),
                success: function(data) {
                    if (strcmpCatg(data._result, "error") == 0) {
                        bootbox.alert("<h2><font color=red>" + data._message + "</font></h2>", function(_result) {
                        });
                    }
                    else if (strcmpCatg(data._result, "success") == 0) {
                        bootbox.alert("<h2><font color=blue>" + data._message + "</font></h2>", function(_result) {
                            window.location = "./signcategory.jsp";
                        });
                    }
                }
            });
        }
    });
}

function changeCategoryStatus(status, categoryid, uidiv) {
    var s = './ChangeCategorystatus?_status=' + status + '&_categoryid=' + categoryid;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpCatg(data._result, "error") == 0) {
                Alert4epdf("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpCatg(data._result, "success") == 0) {
                bootbox.confirm("<h2><font color=blue>" + data._message + "</font></h2>", function(_result) {
                    window.location = "./signcategory.jsp";
                });
            }
        }
    });
}

function editCategorymodal(_categoryid, _categoryname, _note, _status) {
    var status = "suspended";
    if (_status === 1) {
        status = "active";
    }
    document.getElementById("mycategoryeditmodel").innerHTML = "Edit Category of " + _categoryname;
    document.getElementById("_ecategoryId").value = _categoryid;
    document.getElementById("_ecategoryName").value = _categoryname;
    document.getElementById("_eStatus").value = _status;
    document.getElementById("_eNote").value = _note;
    $("#_eStatus").find('option[value="' + status + '"]').prop("selected", true);
    $('#editCategory').modal();
//    }
}
function editCategory() {

    $('#editCategoryButton').attr("disabled", true);
    $('#editCategory-result').html("");
    var s = './editCategory';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editCategoryForm").serialize(),
        success: function(data) {
            if (strcmpCatg(data._result, "error") == 0) {
                $('#edit-category-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }
            else if (strcmpCatg(data._result, "success") == 0) {
                $('#_categoryName').val("");
                $('#_Status').val("");
                $('#_note').val("");
                $('#edit-category-result').html("<span><font color=blue>" + data._message + "</font></span>");
                $('#editCategoryButton').attr("disabled", false);
                window.setTimeout(RefreshCategoryList, 3000);
            }
        }
    });
}


