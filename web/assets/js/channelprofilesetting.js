/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function strcmpchannelprofile(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}


function Alert4ChannelprofileSettings(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}


function ChangeCheckUser(value) {
    if (value == 1) {
        $('#_checkuser').val("1");
        $('#_check-user').html("Name");
    } else {
        $('#_checkuser').val("2");
        $('#_check-user').html("Email/Phone");
    }

}
function ChangeCheckUser(value) {
    if (value == 1) {
        $('#_checkuser').val("1");
        $('#_check-user').html("Name");
    } else {
        $('#_checkuser').val("2");
        $('#_check-user').html("Email/Phone");
    }

}
function ChangeAuthorizerStatus(value) {
    if (value == 1) {
        $('#_authorizer').val("1");
        $('#_Authorizer-user').html("Enable");
    } else {
        $('#_authorizer').val("0");
        $('#_Authorizer-user').html("Disable");
    }
}
function ChangePendingDuration(value) {
//    if (value == 1) {
//        $('#_Authorizer_PeningDuration').val("1");
//        $('#_Authorizer_PeningDuration-user').html("1 Hour");
//    } else if (value == 3) {
//        $('#_Authorizer_PeningDuration').val("3");
//        $('#_Authorizer_PeningDuration-user').html("3 Hour");
//    } else if (value == 6) {
//        $('#_Authorizer_PeningDuration').val("6");
//        $('#_Authorizer_PeningDuration-user').html("6 Hour");
//    } else if (value == 24) {
//        $('#_Authorizer_PeningDuration').val("24");
//        $('#_Authorizer_PeningDuration-user').html("1 Day");
//    }
     if (value == 1) {
        $('#_Authorizer_PeningDuration').val("1");
        $('#_Authorizer_PeningDuration-user').html("1 Day");
    } else if (value == 7) {
        $('#_Authorizer_PeningDuration').val("7");
        $('#_Authorizer_PeningDuration-user').html("1 Week");
    } else if (value == 14) {
        $('#_Authorizer_PeningDuration').val("14");
        $('#_Authorizer_PeningDuration-user').html("2 Weeks");
    }else if (value == 30) {
        $('#_Authorizer_PeningDuration').val("30");
        $('#_Authorizer_PeningDuration-user').html("1 Month");
    }else if (value == 90) {
        $('#_Authorizer_PeningDuration').val("90");
        $('#_Authorizer_PeningDuration-user').html("3 Months");
    }else if (value == 180) {
        $('#_Authorizer_PeningDuration').val("180");
        $('#_Authorizer_PeningDuration-user').html("6 Months");
    }else if (value == 365) {
        $('#_Authorizer_PeningDuration').val("365");
        $('#_Authorizer_PeningDuration-user').html("1 year");
    }
    
}
function ChangeUnit(value) {
    if (value == 1) {
        $('#_Authorizer_Units').val("1");
        $('#_Authorizer_Units-user').html("By Same Unit");
    } else if (value == 2) {
        $('#_Authorizer_Units').val("2");
        $('#_Authorizer_Units-user').html("By Different Units");
    }
}
function ChangeUnitAutherization(value) {
    if (value == 1) {
        $('#UnitAutherization').val("1");
        $('#_UnitAutherization').html("Enable");
    } else if (value == 2) {
        $('#UnitAutherization').val("2");
        $('#_UnitAutherization').html("Disable");
    }
}


function ChangeAlertChanges(value) {
    if (value == 1) {
        $('#_alertuser').val("1");
        $('#_alert-User').html("Active");
    } else {
        $('#_alertuser').val("0");
        $('#_alert-User').html("Suspended");
    }
}
function ChangeEditUser(value) {
    if (value == 1) {
        $('#_edituser').val("1");
        $('#_edit-User').html("YES");
    } else {
        $('#_edituser').val("0");
        $('#_edit-User').html("NO");
    }
}
function ChangeDeleteUser(value) {
    if (value == 1) {
        $('#_deleteuser').val("1");
        $('#_delete-User').html("YES");
    } else {
        $('#_deleteuser').val("0");
        $('#_delete-User').html("NO");
    }
}

function ChangeResetUser(value) {
    if (value == 1) {
        $('#_resetuser').val("1");
        $('#_reset-User').html("YES");
    } else if (value == 0) {
        $('#_resetuser').val("0");
        $('#_reset-User').html("NO");
    }

}

function ChangeTokenSettingLoad(value) {
    if (value == 1) {
        $('#_tokenload').val("1");
        $('#_token-load').html("Every Time");
    } else {
        $('#_tokenload').val("0");
        $('#_token-load').html("Once at Restart");
    }
}

function ChangeConncetorStatus(value) {
    if (value == 300) {
        $('#_connectorstatus').val("300");
        $('#_connector-status').html("5 minutes");
    } else if (value == 600) {
        $('#_connectorstatus').val("600");
        $('#_connector-status').html("10 minutes");
    }
    else if (value == 1800) {
        $('#_connectorstatus').val("1800");
        $('#_connector-status').html("30 minutes");
    }
    else if (value == 3600) {
        $('#_connectorstatus').val("3600");
        $('#_connector-status').html("60 minutes");
    } else if (value == 10800) {
        $('#_connectorstatus').val("10800");
        $('#_connector-status').html("180 minutes");
    } else if (value == 21600) {
        $('#_connectorstatus').val("21600");
        $('#_connector-status').html("360 minutes");
    }

}

function ChangeCleanupAuditDays(value) {
    if (value == 90) {
        $('#_cleanupdays').val("90");
        $('#_cleanup-days').html("3 Months");
    } else if (value == 180) {
        $('#_cleanupdays').val("180");
        $('#_cleanup-days').html("6 Months");
    }
    else if (value == 270) {
        $('#_cleanupdays').val("270");
        $('#_cleanup-days').html("9 Months");
    }
    else if (value == 364) {
        $('#_cleanupdays').val("364");
        $('#_cleanup-days').html("1 Year");
    } else if (value == 728) {
        $('#_cleanupdays').val("728");
        $('#_cleanup-days').html("2 Year");
    }

}






function ChangeCheckSession(value) {
    if (value == 1) {
        $('#_checksession').val("1");
        $('#_check-session').html("Yes");
    } else {
        $('#_checksession').val("0");
        $('#_check-session').html("No");
    }

}
function ChangeAlertMedia(value) {
    if (value == 1) {
        $('#_alertmedia').val("1");
        $('#_alert-media').html("SMS");
    } else if (value == 2) {
        $('#_alertmedia').val("2");
        $('#_alert-media').html("EMAIL");
    }
    else if (value == 3) {
        $('#_alertmedia').val("3");
        $('#_alert-media').html("VOICE");
    }
    else if (value == 4) {
        $('#_alertmedia').val("4");
        $('#_alert-media').html("USSD");
    }
}
function ChangeSoftwareOTPType(value) {
    if (value == 1) {
        $('#_softwaretype').val("1");
        $('#_software-type').html("Google/Apple Store");
    } else {
        $('#_softwaretype').val("0");
        $('#_software-type').html("Mobile Trust SDK");
    }
}

function LoadChannelprofileSettings() {
    var s = './loadChannelprofileSettings';

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            $('#_cleanuppath').val(data._cleanuppath);
            $('#_rssarchive').val(data._rssarchive);
            $('#_uploads').val(data._uploads);
            $('#_otpspecification').val(data._otpspecification);
            $('#_certspecification').val(data._certspecification);
            $('#_ocraspecification').val(data._ocraspecification);
            $('#_locationclassName').val(data._locationclassName);
            $('#_authorizer').val(data._authorizer);

            $('#_dayrestriction').val(data._dayrestriction);
            $('#_timerange').val(data._timerange);
            $('#_timefromampm').val(data._timefromampm);
            $('#_totimerange').val(data._totimerange);
            $('#_timetoampm').val(data._timetoampm);
            $('#_multipleSession').val(data._multipleSession);
            $('#_serverRestriction').val(data._serverRestriction);
            $('#_sweetSpotDeviation').val(data._sweetSpotDeviation);
            $('#_xDeviation').val(data._xDeviation);
            $('#_yDeviation').val(data._yDeviation);
            
            $('#_userValidityDays').val(data._userValidityDays);
            
            ChangeUnitAutherization(data._UnitAutherizationForToken);
             
            if (data._checkuser == 1) {
                ChangeCheckUser(1);
            }
            else {
                ChangeCheckUser(2);
            }
            //alert(data._status);
            if (data._alertuser == 1) {
                ChangeAlertChanges(1);
            }
            else {
                ChangeAlertChanges(0);
            }

            if (data._edituser == 1) {
                ChangeEditUser(1);
            }
            else {
                ChangeEditUser(0);
            }

            if (data._deleteuser == 1) {
                ChangeDeleteUser(1);
            }
            else {
                ChangeDeleteUser(0);
            }
            if (data._tokenload == "yes") {
                ChangeTokenSettingLoad(1);
            }
            else {
                ChangeTokenSettingLoad(0);
            }
            if (data._alertmedia == "sms") {
                ChangeAlertMedia(1);
            } else if (data._alertmedia == "email") {
                ChangeAlertMedia(2);
            } else if (data._alertmedia == "voice") {
                ChangeAlertMedia(3);
            } else {
                ChangeAlertMedia(4);
            }

            if (data._softwaretype == "simple") {
                ChangeSoftwareOTPType(1);
            }
            else {
                ChangeSoftwareOTPType(0);
            }

            if (data._resetuser == 0) {
                ChangeResetUser(0);
            } else {
                ChangeResetUser(1);
            }

            if (data._connectorstatus == 300) {
                ChangeConncetorStatus(300);
            }
            else if (data._connectorstatus == 600) {
                ChangeConncetorStatus(600);
            } else if (data._connectorstatus == 1800) {
                ChangeConncetorStatus(1800);
            } else if (data._connectorstatus == 3600) {
                ChangeConncetorStatus(3600);
            } else if (data._connectorstatus == 10800) {
                ChangeConncetorStatus(10800);
            } else if (data._connectorstatus == 21600) {
                ChangeConncetorStatus(21600);
            }


            if (data._cleanupdays == 90) {
                ChangeCleanupAuditDays(90);
            }
            else if (data._cleanupdays == 180) {
                ChangeCleanupAuditDays(180);
            } else if (data._cleanupdays == 270) {
                ChangeCleanupAuditDays(270);
            } else if (data._cleanupdays == 364) {
                ChangeCleanupAuditDays(364);
            } else if (data._cleanupdays == 728) {
                ChangeCleanupAuditDays(728);
            }

//            alert(data._authorizer);
            if (data._authorizer == 1) {
                ChangeAuthorizerStatus(1);
            } else if (data._authorizer == 0) {
                ChangeAuthorizerStatus(0);
            }
            ChangePendingDuration(data._Authorizer_PeningDuration);
            ChangeUnit(data._Authorizer_Units);

//            if (data._authorizer == 1) {
//                 ChangePendingDuration(1);
//             }else if (data._authorizer == 0) {
//                 ChangeAuthorizerStatus(0);
//             }


        }
    });
}


function editChannelprofileSettings() {
    var s = './editChannelprofilesettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#channelprofileform").serialize(),
        success: function(data) {
            if (strcmpchannelprofile(data._result, "error") == 0) {
                $('#channel-profile-settings-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4ChannelprofileSettings("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpchannelprofile(data._result, "success") == 0) {
//                $('#channel-profile-settings-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4ChannelprofileSettings("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });

}


