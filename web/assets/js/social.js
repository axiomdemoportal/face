function strcmpSocial(a, b)
{
    return (a<b?-1:(a>b?1:0));
}


function Alert4SocialSetting(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
    //end here
    }
    });
}



function ChangeRetrySocial(preference, count) {
    //1 for enabled
    //0 for disabled
    if (preference == 1) {
        if (count == 2) {
            $('#_retries').val("2");
            $('#_retries-primary-social').html("2 retries");
        } else if (count == 3) {
            $('#_retries').val("3");
            $('#_retries-primary-social').html("3 retries");
        }
        else if (count == 5) {
            $('#_retries').val("5");
            $('#_retries-primary-social').html("5 retries");
        }
    }
}


function ChangeActiveStatusSocial(perference, value) {
    if (perference == 1) {
        if (value == 1) {
            $('#_status').val("1");
            $('#_status-primary-social').html("Active");
        } else {
            $('#_status').val("0");
            $('#_status-primary-social').html("Suspended");
        }
    } else if (perference == 2) {
        if (value == 1) {
            $('#_statusS').val("1");
            $('#_status-secondary-sms').html("Active");
        } else {
            $('#_statusS').val("0");
            $('#_status-secondary-sms').html("Suspended");
        }
    }
}


function ChangeRetryDurationSocial(perference, duration) {
    //1 for enabled
    //0 for disabled
    if (perference == 1) {
        if (duration == 10) {
            $('#_retryduration').val("10");
            $('#_retryduration-primary-social').html("10 seconds");
        } else if (duration == 30) {
            $('#_retryduration').val("30");
            $('#_retryduration-primary-social').html("30 seconds");
        }
        else if (duration == 60) {
            $('#_retryduration').val("60");
            $('#_retryduration-primary-social').html("60 seconds");
        }
    }
}

function EditSocialSetting(type) {
    if (type == 1) {
        editsocialprimary();
    } else if (type == 2) {
        editSMSsecondary();
    }
}

function editsocialprimary() {
    var s = './editsocialsettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#socialform").serialize(),
        success: function(data) {
            if (strcmpSocial(data._result, "error") == 0) {
                $('#save-social-gateway-primary-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4SocialSetting("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpSocial(data._result, "success") == 0) {
                $('#save-social-gateway-primary-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4SocialSetting("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}



function LoadTestSMSConnectionUI(type) {
    if (type == 1)
        $("#testSMSPrimary").modal();
    else
        $("#testSMSSecondary").modal();
}

function LoadSocialSetting(type) {
    if (type == 1) {
        socialprimary();
    } else if (type == 2) {
        smssecondary();
    }
}


function socialprimary() {
    var s = './loadsocialsettings?_type=17&_preference=1';

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            //$('#_status').val(data._status);
            $('#_ip').val(data._ip);
            $('#_port').val(data._port);
            $('#_facebookappid').val(data._facebookappid);
            $('#_facebookappsecret').val(data._facebookappsecret);
            $('#_linkedinkey').val(data._linkedinkey);
            $('#_linkedinsecret').val(data._linkedinsecret);
            $('#_linkedintoken').val(data._linkedintoken);
            $('#_linkedinaccesssecret').val(data._linkedinaccesssecret);
            $('#_twitterkey').val(data._twitterkey);
            $('#_twittersecret').val(data._twittersecret);
//            $('#_googlekey').val(data._googlekey);

            $('#_facebookURL').val(data._facebookURL);
            $('#_linkedinURL').val(data._linkedinURL);
            $('#_linkedinAccessURL').val(data._linkedinAccessURL);
            $('#_twitterURL').val(data._twitterURL);
            $('#_twitterCallbackURL').val(data._twitterCallbackURL);
        
            $('#_reserve1').val(data._reserve1);
            $('#_reserve2').val(data._reserve2);
            $('#_reserve3').val(data._reserve3);
            $('#_retries').val(data._retries);
            $('#_retryduration').val(data._retryduration);

            //alert(data._status);

            if (data._status == 1)
                ChangeActiveStatusSocial(1, 1);
            else
                ChangeActiveStatusSocial(1, 0);

            if (data._retryduration == 10)
                ChangeRetryDurationSocial(1, 10);
            else if (data._retryduration == 30)
                ChangeRetryDurationSocial(1, 30);
            else if (data._retryduration == 60)
                ChangeRetryDurationSocial(1, 60);
            else
                ChangeRetryDurationSocial(1, 10);

            if (data._retries == 2)
                ChangeRetrySocial(1, 2);
            else if (data._retries == 3)
                ChangeRetrySocial(1, 3);
            else if (data._retries == 5)
                ChangeRetrySocial(1, 5);
            else
                ChangeRetrySocial(1, 2);

//            alert(data._messgeLength);

        }
    });
}

