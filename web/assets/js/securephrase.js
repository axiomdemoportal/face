/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function strcmpPhrase(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4Phrase(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}

function searchUsersPhrases() {
    var val = document.getElementById('_keyword').value;
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    if (val.length < 1) {
        Alert4Phrase("Search keyword cannot be blank!!!");
        return;
    }

    var s = './userSecurePhraseTable.jsp?_searchtext=' + encodeURIComponent(val);
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            pleaseWaitDiv.modal('hide');
            $('#users_secure_phrase_table_main').html(data);

        }
    });
}



function loadSecurePhraseModalDetails(userId, userName) {
//    alert(userId);
    $("#_userIDSecureSecurePhrase").val(userId);
    $("#_userNameSecureSecurePhrase").val(userName);
    $("#securePhraseAuditDowload").modal();
}
function searchSecurePhraseAudit1() {
    var val1 = encodeURIComponent(document.getElementById('_auditStartDateSecurePhrase').value);
    var val2 = encodeURIComponent(document.getElementById('_auditEndDateSecurePhrase').value);
    var val4 = encodeURIComponent(document.getElementById('_userIDSecureSecurePhrase').value);
    var val5 = document.getElementById('_userNameSecureSecurePhrase').value;
    if (document.getElementById('_auditStartDateSecurePhrase').value.length == 0 || document.getElementById('_auditEndDateSecurePhrase').value.length == 0) {
        Alert4Msg("Date Range is not selected!!!");
        return;
    }
    var s = './userPasswordAudit.jsp?_startdate=' + val1 + "&_enddate=" + val2 + "&_auditUserID=" + val4 +"&_auditUserName="+ val5 +"&_itemType=SECURITYPHRASE";
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#auditTable').html(data);
            $("#securePhraseAuditDowload").modal('hide');
        }
    });
}
function searchSecurePhraseAudit() {
    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
    var val3 = encodeURIComponent(document.getElementById('_userNameSecure').value);
    var val4 = encodeURIComponent(document.getElementById('_userIDSecure').value);
    if (document.getElementById('_auditStartDate').value.length == 0 || document.getElementById('_auditEndDate').value.length == 0) {
        Alert4Msg("<span><font color=blue>Date Range is not selected!!!</font></span>");
        return;
    }
    var s = './userSecurePhraseAudit.jsp?_startdate=' + val1 + "&_enddate=" + val2 + "&_opridR=" + encodeURIComponent(val4) + "&_oprName=" + val3 + "&_itemType=SECURITYPHRASE";
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#secureAuditTable').html(data);
               $("#auditDownload").modal('hide');
        }
    });
}
//function searchSecurePhraseAudit() {
//    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
//    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
//    var val3 = encodeURIComponent(document.getElementById('_userNameSecure').value);
//    var val4 = encodeURIComponent(document.getElementById('_userIDSecure').value);
////    alert(val4);
//    if (document.getElementById('_auditStartDate').value.length == 0 || document.getElementById('_auditEndDate').value.length == 0) {
//        Alert4Msg("Date Range is not selected!!!");
//        return;
//    }
//    var s = './userSecurePhraseAudit.jsp?_startdate=' + val1 +  "&_enddate=" + val2 + "&_opridR=" + encodeURIComponent(val4)+ "&_oprName=" + val3+"&_itemType=SECURITYPHRASE";
//    $.ajax({
//        type: 'GET',
//        url: s,
//        success: function(data) {
////            $('#licenses_data_table').html(data);
//              $('#secureAuditTable').html(data);
////              $("#securePhraseAuditDowload").modal('hide');
//           
//        }
//    });
//}

function secureReportCSV(val1, val2, val4, val3) {
//    alert('hi');
//     var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
//    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
//    var val3 = encodeURIComponent(document.getElementById('_userNameSecure').value);
//    var val4 = encodeURIComponent(document.getElementById('_userIDSecure').value);

    var s = './getSecurePhrase?_startdate=' + val1 + "&_enddate=" + val2 + "&_opridR=" + val4 + "&_oprName=" + val3 + "&_format=csv" + "&_itemType=SECURITYPHRASE";
    window.location.href = s;
    return false;
}

function secureReportPDF(val1, val2, val4, val3) {
//         var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
//    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
//    var val3 = encodeURIComponent(document.getElementById('_userNameSecure').value);
//    var val4 = encodeURIComponent(document.getElementById('_userIDSecure').value);

    var s = './getSecurePhrase?_startdate=' + val1 + "&_enddate=" + val2 + "&_opridR=" + val4 + "&_oprName=" + val3 + "&_format=pdf" + "&_itemType=SECURITYPHRASE";
    window.location.href = s;
    return false;
}
function RefreshSecureList() {
    window.location.href = "./searchResource.jsp"    
}
function removePhraseDetails(_tid){
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './removeSecurePhrase?_userid='+_tid;
            $.ajax({
                type: 'GET',    
                url: s,
                dataType: 'json',
                success: function(data) {
                    if ( strcmpPhrase(data._result,"success") == 0 ) {
                        Alert4Phrase("<span><font color=blue>" + data._message + "</font></span>");                        
                        window.setTimeout(RefreshSecureList, 2000);
              
                    } else {
                        Alert4Phrase("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}
function unlockPhraseDetails(_tid){
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './unlockImage?_userid='+_tid;
            $.ajax({
                type: 'GET',    
                url: s,
                dataType: 'json',
                success: function(data) {
                    if ( strcmpPhrase(data._result,"success") == 0 ) {
                        Alert4Phrase("<span><font color=blue>" + data._message + "</font></span>");                        
                        window.setTimeout(RefreshSecureList, 2000);
              
                    } else {
                        Alert4Phrase("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}