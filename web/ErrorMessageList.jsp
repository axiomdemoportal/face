<%@page import="com.mollatech.axiom.nucleus.db.Errormessages"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.ErrorMessageManagement"%>
<%@page import="com.mollatech.axiom.common.utils.UtilityFunctions"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.Templates"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.operation.TemplateVariables"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/errormessage.js"></script>
<%
    String _sessionID = (String) session.getAttribute("_apSessionID");

    SessionManagement smObj = new SessionManagement();
    int iStatus = smObj.GetSessionStatus(_sessionID);
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");

    Errormessages[] templatesObj = null;
    if (iStatus == 1) { //active
        smObj.UpdateSession(_sessionID);
        ErrorMessageManagement cmObj = new ErrorMessageManagement();
        templatesObj = cmObj.ListErrorMessages(_sessionID, channel.getChannelid());
        cmObj = null;
    }
%>
<div class="container-fluid">
    <h1 class="text-success">Error Messages</h1>
    <div class="row-fluid">
        <div id="licenses_data_table">
            <table class="display responsive wrap" id="MessageListTable">
                <thead>
                    <tr>

                        <td><b>Code</td>
                        <td><b>Name</td>                                 
                        <td><b>Manage</td>                    
                        <td><b>Status</td>                    
                        <td><b>Created On</td>
                        <td><b>Last Updated On</td>
                        <td><b>Message</td>       

                    </tr>

                </thead>

                <%
                    out.flush();
                    ErrorMessageManagement tmObj = new ErrorMessageManagement();

                    for (int i = 0; i < templatesObj.length; i++) {
                        Errormessages axcObj = templatesObj[i];

                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                        String TypeoF = null;
                        String strStatus = "Active";
                        if (axcObj.getStatus() == 1) {
                            strStatus = "Active";
                        } else {
                            strStatus = "Suspended";
                        }
                        String userStatus = "user-status-value-" + i;
                        //ByteArrayInputStream bais = new ByteArrayInputStream(axcObj.getErrorMessage());
                        //String errormessage = (String) UtilityFunctions.deserializeFromObject(bais);
                        String errormessage = axcObj.getUsermessage();
                        String errorname = axcObj.getErrorname();
                %>
                <tr>

                    <td><%=axcObj.getErrorcode()%></td>
                    <td><%=errorname%></td>

                    <td>
                        <div class="btn-group">
                            <button class="btn btn-mini">Edit</button>
                            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="./ErrorMessageEdit.jsp?_tid=<%=axcObj.getErrorid()%>" data-toggle="modal">Edit Error Message</a></li>
                            </ul>
                        </div>
                    </td>           

                    <td>
                        <%=strStatus%>                        
                    </td>                    

                    <td><%=sdf.format(axcObj.getCreatedOn())%>
                    <td><%=sdf.format(axcObj.getLastupDatedOn())%></td>
                    <td><%=errormessage%></td>

                </tr>
                <%}%>
            </table>
        </div>
    </div>
    <br>

    <script language="javascript">

    </script>

    <script>
        $(document).ready(function () {
            $('#MessageListTable').DataTable({
                responsive: true
            });
        });
    </script>

</div>


<%@include file="footer.jsp" %>