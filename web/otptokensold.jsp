<%@include file="header.jsp" %>

<%
    SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm ");
    java.util.Date dLA = new java.util.Date();

%>
<div class="container-fluid">
    <h2>OTP Token Management</h2>
    <p>List of users and their OTP token management.</p>
    <p>
        <br>
    <div class="input-append">
        <input type="text" id="" name="" placeholder="enter userid, emailid, phone, token serial number" class="span4"><span class="add-on"><i class="icon-search"></i></span>
        <a href="#" class="btn btn-success">Search&raquo;</a>        
    </div>
</p>

<br>
<div class="row-fluid">
    <div id="licenses_data_table">
        <table class="table" width="100%">

            <tr>
                <td>No.</td>
                <td>User ID</td>
                <td>Mobile</td>
                <td>Email</td>
                <td>Attempts</td>
                <td>Out Of Band Token</td>
                <td>Software Token</td>
                <td>Hardware Token</td>
                <td>Audit</td>
                <td>Last Access On</td>
            </tr>


            <%
                for (int i = 0; i < 5; i++) {
            %>
            <tr>
                <td><%=i%></td>
                <td>userid + <%=i%></td>
                <td>34830983<%=i%></td>
                <td>userid<%=i%>@email.com</td>
                <td>2</td>               
                <td>
                    <div class="btn-group">
                        <span class="label label-success">Active</span>
                        <button class="btn btn-mini">SMS</button>                        
                        <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Assign Token</a></li>
                            <li><a href="#">Change Token Type</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Mark as Active</a></li>
                            <li><a href="#">Mark as Suspended</a></li>
                            <li><a href="#"><font color="red">Un-Assign Token</font></a></li>
                        </ul>

                    </div>
                </td>
                <td>
                    <div class="btn-group">
                        <span class="label label-info">Active</span>
                        <button class="btn btn-mini">Not Assigned</button>
                        <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">

                            <li><a href="#">Assign New Token</a></li>
                            <li><a href="#">(Re)send Registration Code</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Mark as Active</a></li>
                            <li><a href="#">Mark as Suspended</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Resync Token</a></li>
                            <li class="divider"></li>
                            <li><a href="#"><font color="red">Un-Assign Token</font></a></li>

                        </ul>
                    </div>
                </td>
                <td>
                    <div class="btn-group">
                        <span class="label label-warning">Suspended</span>
                        <button class="btn btn-mini">382938723</button>
                        <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Assign Token</a></li>
                            <li><a href="#">Activate Token</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Mark as Active</a></li>
                            <li><a href="#">Mark as Suspended</a></li>                            
                            <li class="divider"></li>
                            <li><a href="#"><font color="">Resync Token</font></a></li>
                            <li class="divider"></li>
                            <li><a href="#"><font color="red">Mark as Lost</font></a></li>
                            <li><a href="#"><font color="red">Un-Assign Token</font></a></li>                                                        
                        </ul>
                    </div>
                </td>

                <td>
                    <div class="btn-group">
                        <button class="btn btn-mini">Audit for</button>
                        <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="OperatorAudits('1','1day')">Today</a></li>
                            <li><a href="#" onclick="OperatorAudits('1','7days')">Last 7 days</a></li>
                            <li class="divider"></li>
                            <li><a href="#" onclick="OperatorAudits('2','1month')">Last 30 days</a></li>
                            <li><a href="#" onclick="OperatorAudits('3','2months')">Last 2 months</a></li>
                            <li><a href="#" onclick="OperatorAudits('4','3months')">Last 3 months</a></li>
                            <li><a href="#" onclick="OperatorAudits('5','6months')">Last 6 months</a></li>
                        </ul>
                    </div>
                </td>
                <td><%=sdf.format(dLA)%></td>

            </tr>
            <%}%>
        </table>


    </div>
</div>

<script language="javascript">
    //listChannels();
</script>
</div>

<%@include file="footer.jsp" %>