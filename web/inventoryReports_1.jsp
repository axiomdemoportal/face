<%@include file="header.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
          <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>-->
    </head>
    <body>

        <div class="container" >
            <h2>Image Gallery</h2>
            <p>The .thumbnail class can be used to display an image gallery. Click on the images to see it in full size:</p>            
            <div class="row">
                
                <div class="col-md-4">
                    <table>
                        <tr>
                            <td>
                                 <a href="pulpitrock.jpg" class="thumbnail" style="width:  300px" >

                        <img src="./assets/img/user.png" alt="Pulpit Rock" style="width:150px;height:150px">
                        <!--<p>Pulpit Rock: A famous tourist attraction in Forsand, Ryfylke, Norway.</p>-->  
                        <div class="caption">
                            <h3>
                                How to Add New Client?
                            </h3>
                            <p>
                                This video clip will show you how to add new client profile.
                            </p>												
                        </div>
                    </a>
                            </td>
                            <td>
                                    <a href="pulpitrock.jpg" class="thumbnail" style="width:  300px" >

                        <img src="./assets/img/user.png" alt="Pulpit Rock" style="width:150px;height:150px">
                        <!--<p>Pulpit Rock: A famous tourist attraction in Forsand, Ryfylke, Norway.</p>-->  
                        <div class="caption">
                            <h3>
                                How to Add New Client?
                            </h3>
                            <p>
                                This video clip will show you how to add new client profile.
                            </p>												
                        </div>
                    </a>
                            </td>
                        </tr>
                    </table>
                   
                 
                </div>

            </div>
        </div>

    </body>
</html