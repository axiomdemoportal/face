<%@page import="java.io.InputStream"%>
<%@page import="org.bouncycastle.util.encoders.Base64"%>
<!DOCTYPE html>
<html lang="en">
    <%@page import="java.awt.image.BufferedImage"%>
    <%@page import="java.io.ByteArrayOutputStream"%>
    <%@page import="java.io.File"%>"
    <%@page import="javax.imageio.ImageIO"%> 
    <%@page import="java.io.FileReader"%>
    <%@page import="java.io.BufferedReader"%>
    <%@page import="java.nio.channels.FileChannel"%>
    <%@page import="java.io.FileInputStream"%>
    <%@page import="org.apache.commons.io.FileUtils"%>
    <%@page import="java.io.File"%>
    <%@page import="org.json.JSONObject"%>
    <%@page import="java.io.ByteArrayInputStream"%>
    <%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
    <%@page import="com.mollatech.axiom.nucleus.db.DocumentTemplate"%>
    <%@page import="com.mollatech.axiom.nucleus.db.connector.management.DocsTemplatesManagement"%>

    <link href="assets/dococr/CSSJS/bootstrap.css" rel="stylesheet" type="text/css"/>
    <script src="assets/dococr/CSSJS/Js/bootstrap.js" type="text/javascript"></script>
    <script src="assets/dococr/CSSJS/Js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="assets/dococr/CSSJS/Js/mdb.js" type="text/javascript"></script>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <script src="assets/dococr/js/jquery.min.js" type="text/javascript"></script>
    <script src="assets/dococr/js/jquery.Jcrop.js" type="text/javascript"></script>
    <script src="assets/dococr/js/documentsTemplate.js" type="text/javascript"></script>

    <link href="assets/dococr/css/demo_files/main.css" rel="stylesheet" type="text/css"/>
    <link href="assets/dococr/css/demo_files/demos.css" rel="stylesheet" type="text/css"/>
    <link href="assets/dococr/css/jquery.Jcrop.css" rel="stylesheet" type="text/css"/>

    <div class="card text-center">
        <script type="text/javascript">

            jQuery(function ($) {

                var jcrop_api;

                $('#target').Jcrop({
                    onChange: showCoords,
                    onSelect: showCoords,
                    onRelease: clearCoords
                }, function () {
                    jcrop_api = this;
                });

                $('#coords').on('change', 'input', function (e) {
                    var x1 = $('#x1').val(),
                            x2 = $('#x2').val(),
                            y1 = $('#y1').val(),
                            y2 = $('#y2').val();
                    jcrop_api.setSelect([x1, y1, x2, y2]);
                });

            });

            // Simple event handler, called from onChange and onSelect
            // event handlers, as per the Jcrop invocation above
            function showCoords(c)
            {
                $('#x1').val(c.x);
                $('#y1').val(c.y);
                $('#x2').val(c.x2);
                $('#y2').val(c.y2);
                $('#w').val(c.w);
                $('#h').val(c.h);
            }
            ;

            function clearCoords()
            {
                $('#coords input').val('');
            }
            ;
        </script>
        <%
            String templatename = request.getParameter("fileName");
            String encodedImage = (String) request.getSession().getAttribute("preprocessImage");
            ////////PreprocessImage///////////////////////  
            byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(encodedImage);
            BufferedImage img = ImageIO.read(new ByteArrayInputStream(imageBytes));
            request.getSession().setAttribute("_finalImage", imageBytes);
            System.out.println("encodedImage == " + encodedImage);
            String width = Integer.toString(img.getWidth());
            String height = Integer.toString(img.getHeight());
            System.out.println("Width: " + width);
            System.out.println("Height: " + height);
        %>
        <div class="card-header">
            <!--Tabs-->
            <form id="coords"
                  class="coords"
                  onsubmit="return false;"
                  action="http://example.com/post.php">
                <div class="inline-labels">
                    <!--<label>Enter Template name :</label>-->
                    <input size="15" id="_templatename" name="_templatename" style="width:200px;height: 30px !important" type="text" placeholder="Enter Template Name"/> 
                    <!--<label>Select Template type :</label>-->
                    <select id="templateType" name="templateType"  style="width:200px;height: 30px !important">
                       <option value="-1">--Select Template type</option>
                        <option value="1">Zonal Ocr</option>
                        <option value="2">Scanner Ocr</option>
                        <option value="3">RegEx Ocr</option>
                    </select>
                    <input type="hidden" id="docsName" name="docsName" value="<%=templatename%>">
                    <button type="button" onclick="savetemplate()" class="btn btn-success" style="vertical-align: super !important">Save</button>
                    <a href="./home.jsp"><button type="button" class="btn btn-danger" style="vertical-align: super !important">Cancel</button></a>
                </div>
            </form>
        </div>
        <div class="card-body">
            <div div class="jc-demo-box col-md-12" style="overflow-y: auto;height:450px;border: black;">
                <div class="span12" >
                    <img style="width: '<%=width%>px';height: '<%=height%>px'" src="data:image/png;base64,<%=encodedImage%>" id="target" alt="[Jcrop Example]" />
                </div>
            </div>
        </div>
    </div>