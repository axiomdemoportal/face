<%@include file="header.jsp" %>
<script src="./ckeditor/ckeditor.js"></script>
<script src="./ckeditor/config.js"></script>
<script src="./ckeditor/styles.js"></script>
<script src="./assets/js/template.js"></script>
<%
    String strTID = request.getParameter("_tid");
%>
<div class="container-fluid">
    <h2 id="idEditEmailTemplateName" class="text-success">Edit Template </h2>
    <hr>
     <div class="tabbable">
        <div class="tab-content">
            <div class="tab-pane active" id="primary">
                <div class="row-fluid">
                    <form class="form-horizontal" id="emailtemplateForm" name="emailtemplateForm">
                        <input type="hidden" id="_templateE_name" name="_templateE_name" class="span4">
                        <input type="hidden" id="emailContents" name="emailContents" >
                        <input type="hidden" id="idEmailTemplateId" name="idEmailTemplateId" >
                        <input type="hidden" id="_templateE_variable" name="_templateE_variable" placeholder="Name,Date,Bankname" class="input-xlarge">
                        <fieldset>                                                        
                            <!--<div class="control-group">
                                <label class="control-label"  for="username">Unique Name</label>
                                <div class="controls">
                                    <input type="text" id="_templateE_name" name="_templateE_name" class="span4">
                                </div>
                            </div>-->
                            <div class="control-group">
                                <label class="control-label"  for="username">Subject</label>
                                <div class="controls">
                                    <input type="text" id="_templateE_subject" name="_templateE_subject" class="span6">
                                </div>
                            </div>
                            <!--<div class="control-group">
                                <label class="control-label"  for="username">Variables </label>
                                <div class="controls">
                                    <input type="text" id="_templateE_variable" name="_templateE_variable" placeholder="Name,Date,Bankname" class="input-xlarge">
                                </div>
                            </div>-->
                            <div class="control-group">
                                <label class="control-label"  for="username">Body</label>
                                <div class="controls">
                                    <textarea class="ckeditor" id="_templateE_body" name="_templateE_body" ></textarea>
                                </div>
                            </div>
                            <hr>
                            <div id="trackingDiv" ></div>
                            <script type="text/javascript">
                                        CKEDITOR.replace('_templateE_body');
                                        timer = setInterval('updateDiv()', 100);
                                        function updateDiv() {
                                            var editorText = CKEDITOR.instances._templateE_body.getData();
                                            $('#emailContents').val(editorText);    
                                        }
                            </script>
                            <!-- Submit -->
                            <div class="control-group">
                                <div class="controls">
                                    <a href="./TemplateEmailList.jsp" class="btn" type="button">Go To List</a>
                                    <%//if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >=3 ) {%>                                
                                    <button class="btn btn-primary" onclick="editEmailtemplates()" type="button" id="buttonEmailEditMessage" >Save Email Template</button>
                                    <%//}%>
                                    <div id="edittemplateE-result"></div>
                                </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
     <script>
                 loadEmailTemplateDetails('<%=strTID%>');
    </script>
<%@include file="footer.jsp" %>