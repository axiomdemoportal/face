<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/addSettings.js"></script>
<%
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");

    Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
    String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
    String operatorId = operator.getOperatorid();
    SettingsManagement sMngmt = new SettingsManagement();
    Map keymap = null;
    String key = "";
    Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.MONITORGROUPS, SettingsManagement.PREFERENCE_ONE);
    if (settingsObj != null) {
        keymap = (Map) settingsObj;
        session.setAttribute("keymap", keymap);
        Iterator i = keymap.entrySet().iterator();
        while (i.hasNext()) {
            Map.Entry e = (Map.Entry) i.next();
            key = key + e.getKey().toString() + ",";
        }
    }
    String[] keys = {};
    if (!key.equals("")) {
        key = key.substring(0, key.length() - 1);
        keys = key.split(",");
    }
%>
<div class="container-fluid">
    <h1 class="text-success">Group Management</h1>
    <p>You can manage all your groups </p>
    <br>
    <div class="row-fluid">
        <div id="licenses_data_table">
            <table class="table table-striped">
                <tr>
                    <td>No.</td>
                    <td>Name</td>
                    <td>Manage</td>
                    <td>Created By</td>
                    <td>Created</td>
                    <td>Last Updated</td>
                </tr>
                <%
                    if (keys.length != 0) {
                        for (int i = 0; i < keys.length; i++) {
                            Map valuemap = (Map) keymap.get(keys[i]);
                %>
                <tr>
                    <td><%= i + 1%></td>
                    <td><%= keys[i]%></td>
                    <td>
                        <div class="btn-group">
                            <button class="btn btn-mini" id="Manage">Manage</button>
                            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">

                                <li><a href="./createGroup.jsp?_groupName=<%=keys[i]%>&edit=1" onclick="">Edit Details</a></li>

                                <li><a href="#" onclick="removeGroup('<%=keys[i]%>')" data-toggle="modal"><font color="red">Remove?</font></a></li>
                            </ul>
                        </div>
                    </td>
                    <td><%= valuemap.get("Created By")%></td>
                    <td><%= valuemap.get("Created On")%></td>
                    <td><%= valuemap.get("Updated On")%></td>
                </tr>   
                <% }
                    }
                    else
                    {%>
                    <tr><td><%=1%></td>
                        <td>No Records Found</td>
                        <td>No Records Found</td>

                        <td>No Records Found</td> 

                        <td>No Records Found</td>
                        <td>No Records Found</td>
                        </tr>
                   <% }
                %>
            </table>
            <div class="controls">
                
                <a href="./addSetting.jsp" class="btn" type="button"> << Back </a>
                <a href="./createGroup.jsp" class="btn btn-primary" type="button"> Create New Group >> </a>
            </div>
        </div>
    </div>
</div>
            
                <%@include file="footer.jsp" %>