<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Properties"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.AxiomProtect"%>
<%@include file="header.jsp" %>
<%    String _sessionID = (String) session.getAttribute("_apSessionID");

    SessionManagement smObj = new SessionManagement();
    int iStatus = smObj.GetSessionStatus(_sessionID);
    AxiomChannel[] channelsObj = null;
    if (iStatus == 1) { //active
        smObj.UpdateSession(_sessionID);
        ChannelManagement cmObj = new ChannelManagement();
        channelsObj = cmObj.ListChannelsInternal();
        cmObj = null;
    }
%>
<%
    Properties p = AxiomProtect.LicenseDetails();
    Enumeration enamObj = p.propertyNames();
%>
<div class="container-fluid">
    <h1 class="text-success">License Details</h1>
    <table class="table table-striped">
        <% while (enamObj.hasMoreElements()) {
                String key = (String) enamObj.nextElement();
                String value = (String) p.getProperty(key);
                //out.println(key+"="+value+"<br>");
%>
        <tr>
            <td>
                <b><%=key%></b>
            </td>
            <td>
                <%=value%>
            </td>
        </tr>
        <%}%>        
    </table>

</div>


<%@include file="footer.jsp" %>