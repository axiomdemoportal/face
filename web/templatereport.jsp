<%@include file="header.jsp" %>
<script src="./assets/js/jquery.js"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="./assets/css/datepicker.css">
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">
<script src="assets/TemplateJs/deleteTemplate.js" type="text/javascript"></script>
<div class="container-fluid">
    <h1 class="text-success">Template Details</h1>
    <br>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="control-group form-inline">
                            <form id="templateReport" name="templateReport">
                            <div class="input-prepend">

                                <!--<div class="well">-->
                                <span class="add-on">From:</span>   
                                <div id="datetimepicker1" class="input-append date">
                                    <input id="startdate1" name="startdate1" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                                <!--</div>-->
                            </div>
                            <div class="input-prepend">
                                <!--<div class="well">-->
                                <span class="add-on">till:</span>   
                                <div id="datetimepicker2" class="input-append date">
                                    <input id="enddate1" name="enddate1" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="input-prepend">
                                <input id="_templateName" name="_templateName" type="text" placeholder="Enter Template Name"></input>
                            </div>
                            <button class="btn btn-success" id="btnsearch2" onclick="searchTemplate()" type="button">Show Template</button>
                            <br><br>
                            <div id="tempDetails"></div>
                            </form>
                        </div>
                    </div>
                </div>
        </div>
 <script type="text/javascript">
        $(function() {
            $('#datetimepicker1').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            });
        });
        $(function() {
            $('#datetimepicker2').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            });
        });
//        ChangeMediaType(0);
    </script>
    <script>
        function searchTemplate() {
    var s = './templatereportTable.jsp';
    $.ajax({
        type: 'GET',
        url: s,
        data: $("#templateReport").serialize(),
        success: function(data) {
            document.getElementById("tempDetails").innerHTML = data;
             },
            error:function(data) {
                alert("Error"+JSON.stringify(data));
            }
    });
        }
    </script>

<%@include file="footer.jsp" %>