<%@page import="com.mollatech.axiom.nucleus.db.connector.management.MonitorSettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SchedulerManagement"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.AxiomProtect"%>
<%@page import="com.mollatech.axiom.nucleus.settings.NucleusMonitorSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.Monitorsettings"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.axiom.nucleus.db.Monitortracking"%>
<script src="./assets/js/addSettings.js"></script>

<div class="tab-content">
    <h3>Recent Report..</h3>
    <div class="tab-pane active" id="primary">
        <%!
            public String ChechFor = "";
        %> 
        <%  Channels _apSChannelDetails = (Channels) session.getAttribute("_apSChannelDetails");
            String _settingName = request.getParameter("_settingName");
            int type = Integer.parseInt(request.getParameter("_type"));
            Map settingsMap = (Map) session.getAttribute("setting");
            Monitorsettings monitorsettings = null;
            monitorsettings = (Monitorsettings) settingsMap.get(_settingName);
            int types;
            NucleusMonitorSettings nms = null;
            if (monitorsettings != null) {
                byte[] obj = monitorsettings.getMonitorSettingEntry();
                byte[] f = AxiomProtect.AccessDataBytes(obj);
                ByteArrayInputStream bais = new ByteArrayInputStream(f);
                Object object = SchedulerManagement.deserializeFromObject(bais);
                //NucleusMonitorSettings nms = null;
                if (object instanceof NucleusMonitorSettings) {
                    nms = (NucleusMonitorSettings) object;
                }
            }
            MonitorSettingsManagement management = new MonitorSettingsManagement();
            Monitortracking[] monitortrackings = management.getMonitorTrackingByName(_apSChannelDetails.getChannelid(), _settingName);
        %>
        <div class="container-fluid">
            <div class="row-fluid">
                <div id="licenses_data_table">
                    <table class="table table-striped" id="table_main">
                        <tr>
                            <td>No.</td>
                            <td>Monitor Name</td>
                            <td>Monitor Type</td>
                            <td>Check For</td>
                            <td>Performance</td>
                            <td>Executed On</td>
                        </tr>
                        <%
                            if (monitortrackings != null) {
                                for (int i = 0; i < monitortrackings.length; i++) {
                        %>
                        <tr>
                            <td><%=i + 1%></td>
                            <td><%= _settingName%></td>
                            <td><%= getStringType(type, nms)%></td>
                            <td><%= ChechFor%></td>
                            <td><%= monitortrackings[i].getPerformance()%></td>
                            <td><%= monitortrackings[i].getExecutionStartOn()%></td>
                        </tr>
                        <% }
                } else {%>
                        <tr>
                            <td>1</td>
                            <td>No Record Found</td>
                            <td>No Record Found</td>
                            <td>No Record Found</td>
                            <td>No Record Found</td>
                            <td>No Record Found</td>
                        </tr>
                        <% }
                        %>
                    </table>
                </div>
            </div>
        </div>
        <%! public String getStringType(int type, NucleusMonitorSettings nms) {

                String types = "";

                if (type == 1) {
                    types = "Web Site";
                    ChechFor = nms.getWebadd();
                } else if (type == 2) {
                    types = "FTP Server Monitor";
                    ChechFor = nms.getFtpHost() + ":" + nms.getFtpPort();
                } else if (type == 3) {
                    types = "FTP RTT monitor";
                    ChechFor = nms.getRttHost();
                } else if (type == 4) {
                    types = "DNS Monitor";
                    ChechFor = nms.getDnshost();
                } else if (type == 5) {
                    types = "Mail Server RTT";
                    ChechFor = nms.getEmailTo();
                } else if (type == 6) {
                    types = "Port Monitor";
                    ChechFor = nms.getPorthost() + ":" + nms.getPortport();
                } else if (type == 7) {
                    types = "POP Service Monitor";
                    ChechFor = nms.getPOPhost() + ":" + nms.getPOPport();
                } else if (type == 8) {
                    types = "SMTP Service Monitor";
                    ChechFor = nms.getSmtphost() + ":" + nms.getSmtpport();
                } else if (type == 9) {
                    types = "Ping Monitor";
                    ChechFor = nms.getPinghost();
                } else if (type == 10) {
                    types = "SSL Certificate Monitor";
                    ChechFor = nms.getSelectedprotocol() + ":" + nms.getSslHost();
                }
                return types;
            }
        %>
    </div>
</div>