
<%@page import="com.mollatech.axiom.nucleus.db.Roles"%>
<%@page import="com.mollatech.axiom.nucleus.db.Operators"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();
    String _searchtext = request.getParameter("_searchtext");

    OperatorsManagement operObj = new OperatorsManagement();

    Operators opers = operObj.SearchOperators(sessionId, _channelId, _searchtext);
    if (opers != null) {

%>
<h3>Results for <i>"<%=_searchtext%>"</i></h3>

<table class="table table-striped" id="table_main">

    <tr>
        <td>No.</td>
        <td>Name</td>
        <td>Mobile</td>
        <td>Email</td>
        <td>Status</td>
        <td>Click to Add</td>

    </tr>

    <%
        //if (Users != null) {

        SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm");

        String strStatus = "Inactive";

        if (opers.getStatus() == 1) {

            strStatus = "Active (Already added)";

        } else if (opers.getStatus() == 0) {

            strStatus = "Suspended (Not Added Yet)";

        }


    %>
    <tr id="user_search_<%=opers.getName()%>">
        <td><%=(1)%></td>
        <td><%=opers.getName()%></td>
        <td><%=opers.getPhone()%></td>
        <td><%=opers.getEmailid()%></td>
        <td><%=strStatus%></td> 
        <td> <a href="#addldapopertor" class="btn btn-success" onclick="Operatordetails('<%=opers.getName()%>','<%=opers.getPhone()%>','<%=opers.getEmailid()%>')"  data-toggle="modal">Add LdAP Operator&raquo;</a></td>
        
<!--        <td><div class="btn-group"> 
                <button class="btn btn-mini" id="roleid">Roles</button>
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="ChangeOperatorStatus()">Sysadmin</a></li>
                    <li><a href="#" onclick="ChangeOperatorStatus()">Admin</a></li>
                    <li><a href="#" onclick="ChangeOperatorStatus()">Reporter</a></li>
                    <li><a href="#" onclick="ChangeOperatorStatus()">Helpdesk</a></li>
                </ul>
            </div></td>-->

            <!--        <td> 
                        <div class="btn-group">
                             <button class="btn btn-mini" id="roleid">Unit</button>
                            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeOperatorStatus()">Pune</a></li>
                                <li><a href="#" onclick="ChangeOperatorStatus()">Mumbai</a></li>
                                <li><a href="#" onclick="ChangeOperatorStatus()">Delhi</a></li>
                                <li><a href="#" onclick="ChangeOperatorStatus()">culcutta</a></li>
                            </ul>
                        </div>
                    </td>   -->
           
    </tr>

</table>
<%  } else if (opers == null) { %>
<h3>No Operators found...</h3>
<%}%>

<br><br>

