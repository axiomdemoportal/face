
<%@page import="java.net.UnknownHostException"%>
<%@page import="java.net.InetAddress"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CertDiscoveryManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.ApCertDiscovery"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.certDiscovery.utils.IPAddress"%>
<%@page import="com.mollatech.certDiscovery.utils.InstallCert"%>
<%@page import="com.mollatech.certDiscovery.utils.CertDetails"%>
<%@page import="java.util.*"%>

<!--pagination-->
<script src="./assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/dataTables.responsive.min.js"></script>
<link  rel="stylesheet" href="assets/css/jquery.dataTables.min.css">
<link  rel="stylesheet" href="assets/css/responsive.dataTables.min.css">


<div id="auditTable">
    <div class="row-fluid">
        <div id="licenses_data_table">
            <%

                String cat = request.getParameter("cat");
                String type = request.getParameter("Type");

                //    Beast', 'Weak Cipher Suites', 'SSLv2.0 Protocol Enable','SSLv3.0 Protocol Enable','Heartbleed','Freak Attack
                String Type = null;
                String Condition = null;
                ApCertDiscovery[] arrCobj = null;
                //JSONObject PortIp=null;

                CertDiscoveryManagement cMgnt = new CertDiscoveryManagement();
                ApCertDiscovery[] apCertDiscovery = cMgnt.getCertDiscoveryDetails();
                List PortIpDetailslist = new ArrayList();
                List ArchiveIdlist = new ArrayList();

                // for SSLEndpoints
                if (cat.equals("SSL Endpoint Notices")) {
                    if (type.equals("Beast")) {
                        Type = "_BeastStatus";
                        Condition = "vulnerable";
                    } else if (type.equals("Weak Cipher Suites")) {
                        Type = "_csStrength";
                        Condition = "weak encryption (40-bit)";

                    } else if (type.equals("SSLv2.0 Protocol Enable")) {
                        Type = "_Protocalversion";
                        Condition = "SSLv2.0";
                    } else if (type.equals("SSLv3.0 Protocol Enable")) {
                        Type = "_Protocalversion";
                        Condition = "SSLv3.0";
                    } else if (type.equals("Heartbleed")) {
                        Type = "_Heartbleed";
                        Condition = "vulnerable";
                    } else if (type.equals("Freak Attack")) {
                        Type = "_freak";
                        Condition = "vulnerable";
                    } else if (type.equals("All")) {
                        for (int j = 0; j < apCertDiscovery.length; j++) {

                            String SSLVulnerabulityDetails = apCertDiscovery[j].getSslvulnerability();

                            String IssuerDetails = apCertDiscovery[j].getSubject();
                            JSONObject CAtdetail = new JSONObject(IssuerDetails);
                            JSONObject SvDetails = new JSONObject(SSLVulnerabulityDetails);

                            String Best = (String) SvDetails.get("_BeastStatus");
                            if (Best != null) {
                                if (Best.equals("vulnerable")) {
                                    String PortIpDetails1 = apCertDiscovery[j].getPortIpDetails();
                                    PortIpDetailslist.add(PortIpDetails1);
                                }

                            }

                            String Freak = (String) SvDetails.get("_freak");

                            if (Freak.equals("vulnerable")) {
                                String PortIpDetails1 = apCertDiscovery[j].getPortIpDetails();
                                PortIpDetailslist.add(PortIpDetails1);

                            }
                            String Crime = (String) SvDetails.get("_crime");

                            if (Crime.equals("vulnerable")) {
                                String PortIpDetails1 = apCertDiscovery[j].getPortIpDetails();
                                PortIpDetailslist.add(PortIpDetails1);

                            }
                            String HeartBleed = (String) SvDetails.get("_Heartbleed");

                            if (HeartBleed.equals("vulnerable")) {
                                String PortIpDetails1 = apCertDiscovery[j].getPortIpDetails();
                                PortIpDetailslist.add(PortIpDetails1);

                            }
                            String protocalVersion = (String) SvDetails.get("_Protocalversion");
                            if (protocalVersion != null) {
                                if (protocalVersion.equals("SSLv3.0")) {
                                    String PortIpDetails1 = apCertDiscovery[j].getPortIpDetails();
                                    PortIpDetailslist.add(PortIpDetails1);

                                }

                                if (protocalVersion.equals("SSLv2.0")) {
                                    String PortIpDetails1 = apCertDiscovery[j].getPortIpDetails();
                                    PortIpDetailslist.add(PortIpDetails1);

                                }
                            }

                            String csStrength = (String) SvDetails.get("_csStrength");
                            if (csStrength.equals("weak encryption (40-bit)")) {
                                String PortIpDetails1 = apCertDiscovery[j].getPortIpDetails();
                                PortIpDetailslist.add(PortIpDetails1);
                            }

                            //            String Issuer = (String) CAtdetail.get("_Issuer");
                            //            String[] list = null;
                            //            if (Issuer != null && !Issuer.isEmpty()) {
                            //                list = Issuer.split(",");
                            //            }
                            //            HashMap map = new HashMap();
                            //            if (list != null) {
                            //                for (String str : list) {
                            //                    String[] values = str.split("=");
                            //                    map.put(values[0], values[1]);
                            //
                            //                }
                            //
                            //                String CN = (String) map.get("CN");
                            //                if (mapCN.get(CN) != null) {
                            //                    mapCN.put(CN, mapCN.get(CN) + 1);
                            //                } else {
                            //                    mapCN.put(CN, 1);
                            //                }
                            //            }
                        }
                    }

                    if (!type.equals("All")) {

                        for (int j = 0; j < apCertDiscovery.length; j++) {

                            String SSLVulnerabulityDetails = apCertDiscovery[j].getSslvulnerability();

                            JSONObject SvDetails = new JSONObject(SSLVulnerabulityDetails);

                            String status = (String) SvDetails.get(Type);
                            if (status != null) {
                                if (status.equals(Condition)) {
                                    String PortIpDetails1 = apCertDiscovery[j].getPortIpDetails();
                                    PortIpDetailslist.add(PortIpDetails1);
                                }
                            }
                        }
                    }
                }//for Certificate Notices
                else if (cat.equals("Certificate Notices")) {

                    for (int j = 0; j < apCertDiscovery.length; j++) {

                        String SSLVulnerabulityDetails = apCertDiscovery[j].getSslvulnerability();
                        String IssuerDetails = apCertDiscovery[j].getSubject();
                        JSONObject SvDetails = new JSONObject(SSLVulnerabulityDetails);
                        JSONObject CAtdetail = new JSONObject(IssuerDetails);
                        String signAlgo = null;

                        //   String keySize =(String) CAtdetail.get("_PublicKeyLenght");
                        if (type.equals("Weak Keys<2048")) {
                            int k = CAtdetail.getInt("_PublicKeyLenght");

                            if (k < 2048) {
                                String PortIpDetails1 = apCertDiscovery[j].getPortIpDetails();
                                //              JSONObject PortIp = new JSONObject(PortIpDetails1);

                                PortIpDetailslist.add(PortIpDetails1);

                            }
                        } else if (type.equals("ShA1 Violation")) {
                            signAlgo = (String) CAtdetail.get("_SignAlgo");
                            if (signAlgo != null) {
                                if (signAlgo.contains("SHA1")) {
                                    String PortIpDetails1 = apCertDiscovery[j].getPortIpDetails();
                                    //              JSONObject PortIp = new JSONObject(PortIpDetails1);

                                    PortIpDetailslist.add(PortIpDetails1);

                                }
                            }
                        } else if (type.equals("Weak Hashing Algorithm")) {
                            signAlgo = (String) CAtdetail.get("_SignAlgo");
                            if (signAlgo != null) {
                                if (!signAlgo.contains("SHA2")) {

                                    String PortIpDetails1 = apCertDiscovery[j].getPortIpDetails();
                                    //              JSONObject PortIp = new JSONObject(PortIpDetails1);

                                    PortIpDetailslist.add(PortIpDetails1);
                                }
                            }
                        } else if (type.equals("Certificate Name Mismatch")) {

                            String Iptdetail = apCertDiscovery[j].getPortIpDetails();
                            JSONObject Iptdetail1 = new JSONObject(Iptdetail);
                            String ip = (String) Iptdetail1.get("ip");

                            try {
                                InetAddress host = InetAddress.getByName(ip);
                                String Host = host.getHostName();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {

                                InetAddress addr1 = InetAddress.getByName(ip);
                                String h1 = addr1.getHostName();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            String Issuer = (String) CAtdetail.get("_Subject");
                            String[] list = null;
                            if (Issuer != null && !Issuer.isEmpty()) {
                                list = Issuer.split(",");
                            }
                            HashMap map = new HashMap();
                            String CN = null;
                            if (list != null) {
                                for (String str : list) {
                                    if (str.contains("=")) {
                                        String[] values = str.split("=");
                                        try {
                                            map.put(values[0], values[1]);
                                        } catch (Exception ex) {
                                            System.out.println("Catching the ArrayIndexOutOfBoundsException   " + ex);
                                        } finally {
                                            // ... cleanup that will execute whether or not an error occurred ...
                                        }
                                    } else {

                                        continue;
                                    }
                                }

                                CN = (String) map.get("CN");
                                try {
                                    InetAddress host2 = InetAddress.getByName(ip);

                                    String Host2 = host2.getHostName();

                                    if (!Host2.equals(CN)) {
                                        String PortIpDetails1 = apCertDiscovery[j].getPortIpDetails();
                                        //              JSONObject PortIp = new JSONObject(PortIpDetails1);

                                        PortIpDetailslist.add(PortIpDetails1);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                        } else if (type.equals("All")) {
                            int k = CAtdetail.getInt("_PublicKeyLenght");

                            if (k < 2048) {
                                String PortIpDetails1 = apCertDiscovery[j].getPortIpDetails();
                                PortIpDetailslist.add(PortIpDetails1);
                            }

                            signAlgo = (String) CAtdetail.get("_SignAlgo");
                            if (signAlgo != null) {
                                if (signAlgo.contains("SHA1")) {
                                    String PortIpDetails1 = apCertDiscovery[j].getPortIpDetails();
                                    PortIpDetailslist.add(PortIpDetails1);

                                }

                                if (!signAlgo.contains("SHA2")) {
                                    String PortIpDetails1 = apCertDiscovery[j].getPortIpDetails();
                                    PortIpDetailslist.add(PortIpDetails1);

                                }
                            }
                            String Iptdetail = apCertDiscovery[j].getPortIpDetails();
                            JSONObject Iptdetail1 = new JSONObject(Iptdetail);
                            String ip = (String) Iptdetail1.get("ip");

                            try {
                                InetAddress host = InetAddress.getByName(ip);
                                String Host = host.getHostName();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {

                                InetAddress addr1 = InetAddress.getByName(ip);
                                String h1 = addr1.getHostName();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            String Issuer = (String) CAtdetail.get("_Subject");
                            String[] list = null;
                            if (Issuer != null && !Issuer.isEmpty()) {
                                list = Issuer.split(",");
                            }
                            HashMap map = new HashMap();
                            String CN = null;
                            if (list != null) {
                                for (String str : list) {
                                    if (str.contains("=")) {
                                        String[] values = str.split("=");
                                        try {
                                            map.put(values[0], values[1]);
                                        } catch (Exception ex) {
                                            System.out.println("Catching the ArrayIndexOutOfBoundsException   " + ex);
                                        } finally {
                                            // ... cleanup that will execute whether or not an error occurred ...
                                        }
                                    } else {

                                        continue;
                                    }
                                }

                                CN = (String) map.get("CN");
                                try {
                                    InetAddress host2 = InetAddress.getByName(ip);

                                    String Host2 = host2.getHostName();

                                    if (!Host2.equals(CN)) {
                                        String PortIpDetails1 = apCertDiscovery[j].getPortIpDetails();
                                        //              JSONObject PortIp = new JSONObject(PortIpDetails1);

                                        PortIpDetailslist.add(PortIpDetails1);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                        }

                    }
                } //end
                //for Expiring Cert
                else if (cat.equals("Expiring Certificates")) {
                    if (type.equals("All")) {
                        arrCobj = cMgnt.getExpireCertificateIn0Days();
                        if (arrCobj != null) {
                            for (int j = 0; j < arrCobj.length; j++) {

                                String PortIpDetails1 = arrCobj[j].getPortIpDetails();
                                PortIpDetailslist.add(PortIpDetails1);
                            }
                        }
                        arrCobj = cMgnt.getExpireCertificateIn7Days();
                        if (arrCobj != null) {
                            for (int j = 0; j < arrCobj.length; j++) {
                                String PortIpDetails1 = arrCobj[j].getPortIpDetails();
                                PortIpDetailslist.add(PortIpDetails1);
                            }
                        }
                        arrCobj = cMgnt.getExpireSoonCertificate();
                        if (arrCobj != null) {
                            for (int j = 0; j < arrCobj.length; j++) {

                                String PortIpDetails1 = arrCobj[j].getPortIpDetails();
                                PortIpDetailslist.add(PortIpDetails1);
                            }
                        }

                        arrCobj = cMgnt.getExpireCertificateIn45Days();
                        if (arrCobj != null) {
                            for (int j = 0; j < arrCobj.length; j++) {

                                String PortIpDetails1 = arrCobj[j].getPortIpDetails();
                                PortIpDetailslist.add(PortIpDetails1);
                            }
                        }

                        arrCobj = cMgnt.getExpireCertificateIn60Days();
                        if (arrCobj != null) {
                            for (int j = 0; j < arrCobj.length; j++) {

                                String PortIpDetails1 = arrCobj[j].getPortIpDetails();
                                PortIpDetailslist.add(PortIpDetails1);
                            }
                        }

                        arrCobj = cMgnt.getExpireCertificateIn90Days();
                        if (arrCobj != null) {
                            for (int j = 0; j < arrCobj.length; j++) {

                                String PortIpDetails1 = arrCobj[j].getPortIpDetails();
                                PortIpDetailslist.add(PortIpDetails1);
                            }
                        }
                    } else if (type.equals("0 Days")) {
                        arrCobj = cMgnt.getExpireCertificateIn0Days();
                        if (arrCobj != null) {
                            for (int j = 0; j < arrCobj.length; j++) {

                                String PortIpDetails1 = arrCobj[j].getPortIpDetails();
                                PortIpDetailslist.add(PortIpDetails1);
                            }
                        }

                    } else if (type.equals("7 Days")) {
                        arrCobj = cMgnt.getExpireCertificateIn7Days();
                        if (arrCobj != null) {
                            for (int j = 0; j < arrCobj.length; j++) {
                                String PortIpDetails1 = arrCobj[j].getPortIpDetails();
                                PortIpDetailslist.add(PortIpDetails1);
                            }
                        }
                    }

                    if (type.equals("30 Days")) {
                        arrCobj = cMgnt.getExpireSoonCertificate();
                        if (arrCobj != null) {
                            for (int j = 0; j < arrCobj.length; j++) {

                                String PortIpDetails1 = arrCobj[j].getPortIpDetails();
                                PortIpDetailslist.add(PortIpDetails1);
                            }
                        }

                    }
                    if (type.equals("45 Days")) {
                        arrCobj = cMgnt.getExpireCertificateIn45Days();
                        if (arrCobj != null) {
                            for (int j = 0; j < arrCobj.length; j++) {

                                String PortIpDetails1 = arrCobj[j].getPortIpDetails();
                                PortIpDetailslist.add(PortIpDetails1);
                            }
                        }
                    }

                    if (type.equals("60 Days")) {
                        arrCobj = cMgnt.getExpireCertificateIn60Days();
                        if (arrCobj != null) {
                            for (int j = 0; j < arrCobj.length; j++) {

                                String PortIpDetails1 = arrCobj[j].getPortIpDetails();
                                PortIpDetailslist.add(PortIpDetails1);
                            }
                        }

                    }
                    if (type.equals("90 Days")) {
                        arrCobj = cMgnt.getExpireCertificateIn90Days();
                        if (arrCobj != null) {
                            for (int j = 0; j < arrCobj.length; j++) {

                                String PortIpDetails1 = arrCobj[j].getPortIpDetails();
                                PortIpDetailslist.add(PortIpDetails1);
                            }
                        }

                    }
                } //    for CA
                // ApCarddetails cardDetails = new ApCarddetails();
                else if (cat.equals("Certificate Authorities")) {

                    for (int k = 0; k < apCertDiscovery.length; k++) {
                        String IssuerDetails = apCertDiscovery[k].getSubject();
                        JSONObject CAtdetail = new JSONObject(IssuerDetails);

                        String Issuer = (String) CAtdetail.get("_Issuer");
                        String[] list = null;
                        if (Issuer != null && !Issuer.isEmpty()) {
                            list = Issuer.split(",");
                        }
                        HashMap map = new HashMap();
                        if (list != null) {
                            for (String str : list) {
                                String[] values = str.split("=");
                                try {
                                    map.put(values[0], values[1]);
                                } catch (Exception ex) {
                                    System.out.println("Catching the ArrayIndexOutOfBoundsException   " + ex);
                                } finally {
                                    // ... cleanup that will execute whether or not an error occurred ...
                                }

                            }

                            String CN = (String) map.get("CN");
                            String[] result = CN.split(" ", 2);
                            String first = result[0];
                            if(!type.equals("All")){
                            if (first.equals(type)) {
                                String PortIpDetails1 = apCertDiscovery[k].getPortIpDetails();
                                PortIpDetailslist.add(PortIpDetails1);
                            } 
                            }else{
                                        String PortIpDetails1 = apCertDiscovery[k].getPortIpDetails();
                                PortIpDetailslist.add(PortIpDetails1);
                            }
                        }
                    }
                }

            %>


            <table class="table table-striped table-bordered table-hover" id="certDisctable">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Hostname/IP Address</th>
                        <th>Port</th>
                        <th>Subject</th>
                        <th>Issuer</th>
                        <th>Issued Date</th>
                        <th>Expiry Date</th>
                        <th>Details</th>
                    </tr>
                </thead>

                <%            String strerr = "No Record Found";

                    int i = 0;
                    if (!PortIpDetailslist.isEmpty()) {
                        for (int idx = 0; idx < PortIpDetailslist.size(); idx++) {
                            Object PortIpobj = PortIpDetailslist.get(idx);

                            ApCertDiscovery[] apCertDiscoverys = cMgnt.getCertDiscoveryDetailsByPortIp(PortIpobj.toString());

                            for (int z = 0; z < apCertDiscoverys.length; z++) {

                                if (apCertDiscoverys != null) {
                                    String PortIpDetails = apCertDiscoverys[z].getPortIpDetails();
                                    Date Expirydatetime = apCertDiscoverys[z].getExpirydatetime();
                                    String Details = apCertDiscoverys[z].getSubject();
                                    JSONObject PortIp = new JSONObject(PortIpDetails);

                                    String Port = (String) PortIp.get("_Port");
                                    String Ip = (String) PortIp.get("ip");
                                    JSONObject moduledetails = new JSONObject(Details);
                                    String _Subject = (String) moduledetails.get("_Subject");
                                    String _Issuer = (String) moduledetails.get("_Issuer");
                                    String _SHA1 = (String) moduledetails.get("_SHA1");
                                    String _MD5 = (String) moduledetails.get("_MD5");
                                    String _IssuedDate = (String) moduledetails.get("_IssuedDate");
                                    String _expDate = (String) moduledetails.get("_ExpiryDate");

                                    String _Type = (String) moduledetails.get("_Type");
                                    i = i + 1;

                %>

                <tr id="<%=Ip%>">
                    <td><%=i%></td>
                    <td><%= Ip%></td>
                    <td id="Port<%=i%>"><%= Port%></td>
                    <td id="Subject<%=i%>"><%= _Subject%></td>
                    <td id="Issuer<%=i%>"><%= _Issuer%></td>
                    <td id="Issuer<%=i%>"><%= _IssuedDate%></td>
                    <td id="expdaTE"><%= _expDate%></td>
                    <td id="MODEL">
                        <div class="btn-group">
                            <a href="#certDetail<%=i%>" class="btn btn-mini" data-toggle="modal" >Details</a>
                        </div>
                        <div id="certDetail<%=i%>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>

                                <h3 class="text text-success" id="myModalLabel"><div id="idcertDetails"></div></h3>
                            </div>
                            <div class="modal-body">
                                <div class="row-fluid">
                                    <form class="form-horizontal" id="addCertForm">

                                        <fieldset>
                                            <table class="table table-striped   table-bordered table-hover" id="table_main1">
                                                <tr>
                                                    <td>Subject : </td>
                                                    <td id="_Subject<%=i%>"><%= _Subject%></td>
                                                </tr>
                                                <tr>
                                                    <td>Issuer : </td>
                                                    <td id="_Issuer<%=i%>"><%= _Issuer%></td>
                                                </tr>
                                                <tr>
                                                    <td>SHA1 : </td>
                                                    <td id="_SHA1<%=i%>"><%= _SHA1%></td>
                                                </tr>
                                                <tr>
                                                    <td>MD5 : </td>
                                                    <td id="_MD5<%=i%>"><%= _MD5%></td>
                                                </tr>
                                                <tr>
                                                    <td>Expiry Date : </td>
                                                    <td id="_ExpiryDate<%=i%>"><%= _expDate%></td>
                                                </tr>
                                                <tr>
                                                    <td>Issued Date : </td>
                                                    <td id="_IssuedDate<%=i%>"><%=_IssuedDate%></td>
                                                </tr>
                                                <tr>
                                                    <td>Type : </td>
                                                    <td id="_Type<%=i%>"><%= _Type%></td>
                                                </tr>

                                            </table>


                                        </fieldset>

                                    </form>     


                                </div>
                            </div>
                            <div class="modal-footer">

                                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>

                            </div>
                        </div>
                    </td>
                </tr>
                <% } else {%>
                <tr>
                    <td><%=strerr%></td>
                    <td><%= strerr%></td>
                    <td><%= strerr%></td>
                    <td><%= strerr%></td>
                    <td><%=strerr%></td>
                    <td><%= strerr%></td>
                    <td><%= strerr%></td>
                    <td><%= strerr%></td>


                </tr>
                <%}
                        }
                    }%>
                <%
                } else {%>  <tr>
                    <td><%=strerr%></td>
                    <td><%= strerr%></td>
                    <td><%= strerr%></td>
                    <td><%= strerr%></td>
                    <td><%=strerr%></td>
                    <td><%= strerr%></td>
                    <td><%= strerr%></td>
                    <td><%= strerr%></td>
                </tr>  <%}
                %>
            </table>

        </div>
    </div>

    <script>
        $(document).ready(function () {

            $('#certDisctable').DataTable({
                responsive: true
            });
        });
    </script>
</div>