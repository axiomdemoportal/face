<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>
<%@page import="com.mollatech.axiom.nucleus.settings.GlobalChannelSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/jquery.js"></script>
<script src="./assets/js/dictum/bulkmsg.js"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/bootstrap-datepicker.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/css/datepicker.css">
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">


<div class="container-fluid">
    <h1 class="text-success">Message Report</h1>
    <!--<h3>Make Your Own Report</h3>-->
    <br>
    <div class="tabbable">
        <div class="tab-content">
            <div class="tab-pane active" id="traditional">
                <div class="container-fluid">

                    <h3 class="text-success">Message Reports</h3>
                    <h5>Make Your Own Report</h5>

                    <input type="hidden" id="_changeType" name="_changeType" value="0">
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="control-group form-inline">

                                <div class="input-prepend">

                                    <!--<div class="well">-->
                                    <span class="add-on">From:</span>   
                                    <div id="datetimepicker1" class="input-append date">
                                        <input id="startdate" name="startdate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                    <!--</div>-->
                                </div>
                                <div class="input-prepend">
                                    <!--<div class="well">-->
                                    <span class="add-on">till:</span>   
                                    <div id="datetimepicker2" class="input-append date">
                                        <input id="enddate" name="enddate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                    <!--</div>-->
                                </div>

                                <div class="input-prepend">
                                    <span class="add-on" >To</span><input id="_searchtext" name="_searchtext" class="" type="text" data-bind="value: vm.ActualDoorSizeDepth" />
                                </div>

                                <div class="btn-group">

                                    <button class="btn"><div id="_change-Type-Axiom">Select Type</div></button>
                                    <button class="btn dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ChangeMediaType(1)">SMS</a></li>
                                        <li><a href="#" onclick="ChangeMediaType(2)">VOICE</a></li>
                                        <li><a href="#" onclick="ChangeMediaType(3)">USSD</a></li>
                                        <li><a href="#" onclick="ChangeMediaType(4)">EMAIL</a></li>
                                        <li><a href="#" onclick="ChangeMediaType(5)">FAX</a></li>
                                        <li><a href="#" onclick="ChangeMediaType(18)">Android Push</a></li>
                                        <li><a href="#" onclick="ChangeMediaType(19)">APNS Push</a></li>
                                        <li><a href="#" onclick="ChangeMediaType(33)">Web Push</a></li>

                                    </ul>
                                </div>
                                <%if (oprObj.getRoleid() != 1) {//1 sysadmin
                                        if (accessObj != null && accessObj.viewMessageReport == true) {%>
                                <button class="btn btn-success" onclick="searchMSGReport(1)" id="addUserButton">Generate Report Now</button>
                                <%} else {%>
                                <button class="btn btn-success " onclick="InvalidRequestMSG('msgreport')" type="button">Generate Report Now</button>
                                <%}
                                } else {%>
                                <button class="btn btn-success" onclick="searchMSGReport(1)" id="addUserButton">Generate Report Now</button>
                                <%}%>
                            </div>
                            <div id="users_table_main_MSG">
                            </div>
                        </div>
                    </div>


                    <div id="SelectColumns" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                            <h3 id="myModalLabel">Select Columns</h3>
                        </div>
                        <div class="modal-body">
                            <div class="row-fluid">
                                <form class="form-horizontal" id="selectColmnForm">
                                    <fieldset>
                                        <!-- Name -->


                                        <div id="legend">

                                            <input type="checkbox" id="_msgID" name ="_msgID" value="1"> Message ID
                                        </div>
                                        <div id="legend">
                                            <input type="checkbox" id="_typeS" name ="_typeS" value="2"> Type
                                        </div>
                                        <div id="legend">
                                            <input type="checkbox" id="_deliverTo" name ="_deliverTo" value="3"> Delivered To 
                                        </div>
                                        <div id="legend">
                                            <input type="checkbox" id="_status" name ="_status" value="4"> Status
                                        </div>
                                        <div id="legend">
                                            <input type="checkbox" id="_cost" name ="_cost" value="5"> Cost
                                        </div>
                                        <div id="legend">
                                            <input type="checkbox" id="_sentOn" name ="_sentOn" value="6"> Sent On
                                        </div>
                                        <div id="legend">
                                            <input type="checkbox" id="_updateOn" name ="_updateOn" value="7"> Update On
                                        </div>

                                    </fieldset>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <!--<div id="addUser-result"></div>-->
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                            <!--<button class="btn btn-primary" onclick="searchRecords()" id="addUserButton">Generate Report Now</button>-->
                            <%if (oprObj.getRoleid() != 1) {//1 sysadmin
                                    if (accessObj != null && accessObj.viewMessageReport == true) {%>
                            <button class="btn btn-success" onclick="searchRecords()" id="addUserButton">Generate Report Now</button>
                            <%} else {%>
                            <button class="btn btn-success " onclick="InvalidRequestMSG('msgreport')" type="button">Generate Report Now</button>
                            <%}
                            } else {%>
                            <button class="btn btn-success" onclick="searchRecords()" id="addUserButton">Generate Report Now</button>
                            <%}%>
                        </div>
                    </div>


                    <%
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                        BulkMSGManagement bMngt = new BulkMSGManagement();
                        SettingsManagement sMngt = new SettingsManagement();
                        int limit = 20;
                        int TRADITIONAL = 1;
                        Channellogs[] clogobj = bMngt.getALLMsgObj(channel.getChannelid(), limit, 1);

                        String strmsg = "No Record Found";

                        final int SMS = 1;
                        final int USSD = 2;
                        final int VOICE = 3;
                        final int EMAIL = 4;
                        final int FAX = 5;
                        final int PUSH = 6;
                    %>


                    <div class="row-fluid">
                        <div id="licenses_data_table">
                            <h3>Recent Messages</h3>
                            <table class="table table-striped" >

                                <tr>
                                    <td>No.</td>
                                    <td>Message ID</td>
                                    <td>Type</td>
                                    <td>Delivered to</td>
                                    <td>Status</td>
                                    <td>Cost</td>
                                    <td>Sent On</td>
                                    <td>Update On</td>
                                </tr>
                                <%
                                    if (clogobj != null) {
                                        for (int i = 0; i < clogobj.length; i++) {
                                %>
                                <tr>
                                    <%
                                        String strType = "";
                                        if (clogobj[i].getType() == 1) {
                                            strType = "SMS";
                                        } else if (clogobj[i].getType() == 2) {
                                            strType = "VOICE";
                                        } else if (clogobj[i].getType() == 3) {
                                            strType = "USSD";
                                        } else if (clogobj[i].getType() == 4) {
                                            strType = "EMAIL";
                                        } else if (clogobj[i].getType() == 16) {
                                            strType = "FAX";
                                        }

                                    %>

                                    <%                        String strSource = "";
                                        if (clogobj[i].getSourceid() == 1) {
                                            strSource = "BULK MSG";
                                        } else if (clogobj[i].getSourceid() == 2) {
                                            strSource = "BULK MSG";
                                        } else if (clogobj[i].getSourceid() == 3) {
                                            strSource = "BULK MSG";
                                        } else if (clogobj[i].getSourceid() == 4) {
                                            strSource = "BULK MSG";
                                        }

                                    %>

                                    <%                        final int PENDING = 2;
                                        final int SENT = 0;
                                        final int BLOCKED = -5;

                                        String strStatus = "";
                                        if (clogobj[i].getStatus() == PENDING) {
                                            strStatus = "PENDING";
                                        } else if (clogobj[i].getStatus() == SENT) {
                                            strStatus = "SENT";
                                        } else if (clogobj[i].getStatus() == BLOCKED) {
                                            strStatus = "BLOCKED";
                                        } else {
                                            strStatus = "FAILED";
                                        }

                                        /*                    
                                         final int PENDING = 2;
                                         final int SENT = 0;
                                         final int FAILD = -1;
                                         String strStatus = "";
                                         if (clogobj[i].getStatus() == PENDING) {
                                         strStatus = "PENDING";
                                         } else if (clogobj[i].getStatus() == SENT) {
                                         strStatus = "SENT";
                                         } else if (clogobj[i].getStatus() == FAILD) {
                                         strStatus = "FAILED";
                                         }
                                         */
                                        String strMSGID = clogobj[i].getMsgid();
                                        if (strMSGID == null || strMSGID.isEmpty() == true) {
                                            strMSGID = "<span class='label label-Default'>" + "Not Available" + "</span>";
                                        }

                                        String phone = clogobj[i].getPhone();
                                        String email = clogobj[i].getEmail();
                                        String device = "";
                                        if (phone != null && phone.isEmpty() == false) {
                                            device = phone;
                                        }

                                        if (email != null && email.isEmpty() == false) {
                                            device = email;
                                        }

                                    %>
                                    <td><%=(i + 1)%></td>
                                    <td><%=strMSGID%></td>
                                    <td><span class="label label-info"><%=strType%></span></td>
                                    <td><%=device%></td>
                                    <td><%=strStatus%></td> 
                                    <%if (clogobj[i].getCost() != null) {%>
                                    <td><%=clogobj[i].getCost()%></td> 
                                    <%} else {%>
                                    <td><%=0.0%></td> 
                                    <%}%>
                                    <td><%=sdf.format(clogobj[i].getSentUtctime())%></td>
                                    <td><%=sdf.format(clogobj[i].getLastupdateUtctime())%></td>  


                                </tr>
                                <%}
                                } else {%>
                                <tr>
                                    <td><%=strmsg%></td>
                                    <td><%=strmsg%></td>
                                    <td><%=strmsg%></td>
                                    <td><%=strmsg%></td>
                                    <td><%=strmsg%></td>
                                    <td><%=strmsg%></td>
                                    <td><%=strmsg%></td>
                                    <td><%=strmsg%></td>
                                </tr>
                                <%}%>
                            </table>
                        </div>
                    </div>

                    <script type="text/javascript">
                        $(function () {
                            $('#datetimepicker1').datepicker({
                                format: 'dd/mm/yyyy',
                                language: 'pt-BR'
                            });
                        });
                        $(function () {
                            $('#datetimepicker2').datepicker({
                                format: 'dd/mm/yyyy',
                                language: 'pt-BR'

                            });
                        });
                        ChangeMediaType(0);
                    </script>
                </div>

            </div>
            <div class="tab-pane" id="social">

                <div class="container-fluid">
                    <h3 class="text-success">Social Media Reports</h3>
                    <h5>Make Your Own Report</h5>
                    <input type="hidden" id="_socialchangeType" name="_socialchangeType" value="0">
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="control-group form-inline">

                                <div class="input-prepend">

                                    <!--<div class="well">-->
                                    <span class="add-on">From:</span>   
                                    <div id="Socialdatetimepicker1" class="input-append date">
                                        <input id="socialstartdate" name="socialstartdate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                    <!--</div>-->
                                </div>
                                <div class="input-prepend">
                                    <!--<div class="well">-->
                                    <span class="add-on">till:</span>   
                                    <div id="Socialdatetimepicker2" class="input-append date">
                                        <input id="socialenddate" name="socialenddate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                    <!--</div>-->
                                </div>

                                <div class="input-prepend">
                                    <span class="add-on" >To</span><input id="_socialsearchtext" name="_socialsearchtext" class="" type="text" data-bind="value: vm.ActualDoorSizeDepth" />
                                </div>

                                <div class="btn-group">

                                    <button class="btn"><div id="_change-TypeSocial-Axiom">Select Type</div></button>
                                    <button class="btn dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ChangeMediaType(6)">FACEBOOK</a></li>
                                        <li><a href="#" onclick="ChangeMediaType(7)">LINKEDIN</a></li>
                                        <li><a href="#" onclick="ChangeMediaType(8)">TWITTER</a></li>
                                    </ul>
                                </div>
                                <!--<button class="btn btn-success" onclick="searchMSGReport(2)" type="button">Generate Report</button>-->
                                <%if (oprObj.getRoleid() != 1) {//1 sysadmin
                                        if (accessObj != null && accessObj.viewMessageReport == true) {%>
                                <button class="btn btn-success" onclick="searchMSGReport(2)" id="addUserButton">Generate Report</button>
                                <%} else {%>
                                <button class="btn btn-success " onclick="InvalidRequestMSG('msgreport')" type="button">Generate Report</button>
                                <%}
                                } else {%>
                                <button class="btn btn-success" onclick="searchMSGReport(2)" id="addUserButton">Generate Report</button>
                                <%}%>

                            </div>
                            <div id="users_table_main_SOCIAL">
                            </div>
                        </div>
                    </div>


                    <%
                        //        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                        //        BulkMSGManagement bMngt = new BulkMSGManagement();
                        //
                        //        int limit = 20;
                        Channellogs[] csociallogobj = bMngt.getALLMsgObj(channel.getChannelid(), limit, 2);
                    %>


                    <div class="row-fluid">
                        <div id="licenses_data_table_Social">
                            <h3>Recent Messages</h3>
                            <table class="table table-striped" >
                                <tr>
                                    <td>No.</td>
                                    <td>Message ID</td>
                                    <td>Type</td>
                                    <td>Delivered to</td>
                                    <td>Status</td>
                                    <!--<td>Cost</td>-->
                                    <td>Sent On</td>
                                    <td>Update On</td>
                                </tr>
                                <%if (csociallogobj != null) {
                                        for (int i = 0; i < csociallogobj.length; i++) {
                                %>
                                <tr>
                                    <%
                                        String strType = "";
                                        if (csociallogobj[i].getType() == 6) {
                                            strType = "FACEBOOK";
                                        } else if (csociallogobj[i].getType() == 7) {
                                            strType = "LINKEDIN";
                                        } else if (csociallogobj[i].getType() == 8) {
                                            strType = "TWITTER";
                                        }

                                    %>

                                    <%                        String strSource = "";
                                        if (csociallogobj[i].getSourceid() == 5) {
                                            strSource = "BULK MSG";
                                        } else if (csociallogobj[i].getSourceid() == 6) {
                                            strSource = "BULK MSG";
                                        } else if (csociallogobj[i].getSourceid() == 7) {
                                            strSource = "BULK MSG";
                                        }

                                    %>

                                    <%                        final int PENDING = 2;
                                        final int SENT = 0;
                                        final int BLOCKED = -5;

                                        String strStatus = "";
                                        if (csociallogobj[i].getStatus() == PENDING) {
                                            strStatus = "PENDING";
                                        } else if (csociallogobj[i].getStatus() == SENT) {
                                            strStatus = "SENT";
                                        } else if (csociallogobj[i].getStatus() == BLOCKED) {
                                            strStatus = "BLOCKED";
                                        } else {
                                            strStatus = "FAILED";
                                        }

                                        String strMSGID = csociallogobj[i].getMsgid();
                                        if (strMSGID == null || strMSGID.isEmpty() == true) {
                                            strMSGID = "<span class='label label-Default'>" + "Not Available from Sender" + "</span>";
                                        }

                                        String SocialId = csociallogobj[i].getSocial();
                                        String device = "";
                                        if (SocialId != null && SocialId.isEmpty() == false) {
                                            device = SocialId;
                                        }


                                    %>


                                    <td><%=(i + 1)%></td>
                                    <td><%=strMSGID%></td>
                                    <td><span class="label label-info"><%=strType%></span></td>
                                    <td><%=csociallogobj[i].getEmail()%></td>
                                    <td><%=strStatus%></td> 

                                    <td><%=sdf.format(csociallogobj[i].getSentUtctime())%></td>
                                    <td><%=sdf.format(csociallogobj[i].getLastupdateUtctime())%></td>  
                                </tr>
                                <%}
                                } else {%>
                                <td>1</td>
                                <td><%=strmsg%></td>
                                <td><%=strmsg%></td>
                                <td><%=strmsg%></td>
                                <!--<td><%=strmsg%></td>-->
                                <td><%=strmsg%></td>
                                <td><%=strmsg%></td>
                                <td><%=strmsg%></td>
                                <%}%>
                            </table>
                        </div>
                    </div>

                    <script type="text/javascript">
                        $(function () {
                            $('#Socialdatetimepicker1').datepicker({
                                format: 'dd/mm/yyyy',
                                language: 'pt-BR'
                            });
                        });
                        $(function () {
                            $('#Socialdatetimepicker2').datepicker({
                                format: 'dd/mm/yyyy',
                                language: 'pt-BR'
                            });
                        });
                        ChangeMediaType(0);
                    </script>
                </div>

            </div>
            <div class="tab-pane" id="Push">
                <div class="container-fluid">
                    <h3 class="text-success">Push Notification Reports</h3>
                    <h5>Make Your Own Report</h5>
                    <input type="hidden" id="_pushchangeType" name="_pushchangeType" value="0">
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="control-group form-inline">

                                <div class="input-prepend">

                                    <!--<div class="well">-->
                                    <span class="add-on">From:</span>   
                                    <div id="Pushdatetimepicker1" class="input-append date">
                                        <input id="pushstartdate" name="pushstartdate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                    <!--</div>-->
                                </div>
                                <div class="input-prepend">
                                    <!--<div class="well">-->
                                    <span class="add-on">till:</span>   
                                    <div id="Pushdatetimepicker2" class="input-append date">
                                        <input id="pushenddate" name="pushenddate" type="text" data-format="dd--yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                    <!--</div>-->
                                </div>

                                <div class="input-prepend">
                                    <span class="add-on" >To</span><input id="_pushsearchtext" name="_pushsearchtext" class="" type="text" data-bind="value: vm.ActualDoorSizeDepth" />
                                </div>

                                <div class="btn-group">

                                    <button class="btn"><div id="_change-TypePush-Axiom">Select Type</div></button>
                                    <button class="btn dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ChangeMediaType(18)">Android Push</a></li>
                                        <li><a href="#" onclick="ChangeMediaType(19)">I-Phone Push</a></li>
                                    </ul>
                                </div>
                                <!--<button class="btn btn-success" onclick="searchMSGReport(3)" type="button">Generate Report</button>-->
                                <%if (oprObj.getRoleid() != 1) {//1 sysadmin
                                        if (accessObj != null && accessObj.viewMessageReport == true) {%>
                                <button class="btn btn-success" onclick="searchMSGReport(3)" id="addUserButton">Generate Report Now</button>
                                <%} else {%>
                                <button class="btn btn-success " onclick="InvalidRequestMSG('msgreport')" type="button">Generate Report Now</button>
                                <%}
                                } else {%>
                                <button class="btn btn-success" onclick="searchMSGReport(3)" id="addUserButton">Generate Report Now</button>
                                <%}%>
                            </div>
                            <div id="users_table_main_PUSH">
                            </div>
                        </div>
                    </div>


                    <%
                        Channellogs[] cpushlogobj = bMngt.getALLMsgObj(channel.getChannelid(), limit, 3);
                    %>


                    <div class="row-fluid">
                        <div id="licenses_data_table_push">
                            <h3>Recent Messages</h3>
                            <table class="table table-striped" >
                                <tr>
                                    <td>No.</td>
                                    <td>Message ID</td>
                                    <td>Type</td>
                                    <td>Delivered to</td>
                                    <td>Status</td>
                                    <td>Cost</td>
                                    <td>Sent On</td>
                                    <td>Update On</td>
                                </tr>
                                <%
                                    if (cpushlogobj != null) {
                                        for (int i = 0; i < cpushlogobj.length; i++) {
                                %>
                                <tr>
                                    <%
                                        String strType = "";
                                        if (cpushlogobj[i].getType() == 18) {
                                            strType = "Android Push";
                                        } else if (cpushlogobj[i].getType() == 19) {
                                            strType = "I-Phone Push";
                                        }

                                    %>

                                    <%                        String strSource = "";
                                        if (cpushlogobj[i].getSourceid() == 5) {
                                            strSource = "BULK MSG";
                                        } else if (cpushlogobj[i].getSourceid() == 6) {
                                            strSource = "BULK MSG";
                                        } else if (cpushlogobj[i].getSourceid() == 7) {
                                            strSource = "BULK MSG";
                                        }

                                    %>

                                    <%                        final int PENDING = 2;
                                        final int SENT = 0;
                                        final int BLOCKED = -5;

                                        String strStatus = "";
                                        if (cpushlogobj[i].getStatus() == PENDING) {
                                            strStatus = "PENDING";
                                        } else if (cpushlogobj[i].getStatus() == SENT) {
                                            strStatus = "SENT";
                                        } else if (cpushlogobj[i].getStatus() == BLOCKED) {
                                            strStatus = "BLOCKED";
                                        } else {
                                            strStatus = "FAILED";
                                        }

                                        String strMSGID = cpushlogobj[i].getMsgid();
                                        if (strMSGID == null || strMSGID.isEmpty() == true) {
                                            strMSGID = "<span class='label label-Default'>" + "Not Available from Sender" + "</span>";
                                        }

                                        String SocialId = cpushlogobj[i].getSocial();
                                        String device = "";
                                        if (SocialId != null && SocialId.isEmpty() == false) {
                                            device = SocialId;
                                        }

//                                                Object globalobj = sMngt.getSetting(sessionid, channel.getChannelid(), SettingsManagement.GlobalSettings, 1);
//                                        float fcost = 0.0f;
//                                        if (globalobj != null) {
//                                            GlobalChannelSettings gObj = (GlobalChannelSettings) globalobj;
//
//                                            if (cpushlogobj[i].getType() == PUSH) {
//                                                fcost = gObj.pushsettingobj.pushcost;
//                                            }
//                                        }

                                    %>


                                    <td><%=(i + 1)%></td>
                                    <td><%=strMSGID%></td>

                                    <td><span class="label label-info"><%=strType%></span></td>

                                    <td><%=cpushlogobj[i].getEmail()%></td>
                                    <td><%=strStatus%></td> 
                                    <%if (cpushlogobj[i].getCost() != null) {%>
                                    <td><%=cpushlogobj[i].getCost()%></td> 
                                    <%} else {%>
                                    <td><%=0.0%></td> 
                                    <%}%>
                                    <td><%=sdf.format(cpushlogobj[i].getSentUtctime())%></td>
                                    <td><%=sdf.format(cpushlogobj[i].getLastupdateUtctime())%></td>  
                                </tr>
                                <%}
                                } else {%>
                                <td>1</td>
                                <td><%=strmsg%></td>
                                <td><%=strmsg%></td>
                                <td><%=strmsg%></td>
                                <td><%=strmsg%></td>
                                <td><%=strmsg%></td>
                                <td><%=strmsg%></td>
                                <td><%=strmsg%></td>
                                <%}%>
                            </table>
                        </div>
                    </div>

                    <script type="text/javascript">
                        $(function () {
                            $('#Pushdatetimepicker1').datepicker({
                                format: 'dd/mm/yyyy',
                                language: 'pt-BR'
                            });
                        });
                        $(function () {
                            $('#Pushdatetimepicker2').datepicker({
                                format: 'dd/mm/yyyy',
                                language: 'pt-BR'
                            });
                        });
                        ChangeMediaType(0);
                    </script>
                </div>

            </div>
        </div>
    </div>


</div>

<%@include file="footer.jsp" %>