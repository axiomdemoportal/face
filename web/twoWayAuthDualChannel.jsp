<%@include file="header.jsp" %>
<!--<script src="./assets/js/certificate.js"></script>-->
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">
<link rel="stylesheet" href="./assets/css/datepicker.css">
<script src="./assets/js/bootstrap-datepicker.js"></script>
<script src="./assets/js/twowayauth.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<div class="container-fluid">
    <h2 class="text-success">Two Way (Dual Channel) Authentication Report</h2>   
    <div class="row-fluid">
        <div class="span12">
            <div class="control-group form-inline">
                <div class="input-append">
                    <span class="add-on">From:</span>   
                    <div id="datetimepicker1" class="input-append date">
                        <input id="startdate" name="startdate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth">
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                </div>
                <div class="input-append">
                    <span class="add-on">to:</span>   
                    <div id="datetimepicker2" class="input-append date">
                        <input id="enddate" name="enddate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth">
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                    <div class="input-append">                
                        <span><button class="btn btn-success" onclick="generateTwowayAuthTable()" type="button" id="generatereportButton">Search</button></span>
                    </div>   
                </div>
            </div>

            <div id="auth_table_main">
            </div>
        </div>

    </div>

    <!-- New added -->
    <!--    <div class="tabbable" id="REPORT">
            <ul class="nav nav-tabs" id="twoWayReportTab">
                <li class="active"><a href="#twowaycharts" data-toggle="tab">Charts</a></li>
                <li><a href="#twowayreport" data-toggle="tab">Tabular List</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="twowaycharts">
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="control-group">
                                <div class="span4">
                                    <div id="twoWayAuthgraph" ></div>
                                </div>
                                <div  class="span8">
                                    <div id="twoWayAuthgraph1"></div>
    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    
                <div class="tab-pane" id="twowayreport">  
                    <div id="auth_table_main">
                    </div>
                </div>
            </div>
        </div>-->

</div>
<!--    <div id="auth_table_main" style="margin-top: 50px"></div>-->
<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datepicker({
            format: 'dd/mm/yyyy',
            language: 'pt-BR'
        });
    });
    $(function () {
        $('#datetimepicker2').datepicker({
            format: 'dd/mm/yyyy',
            language: 'pt-BR'

        });
    });

</script>
<script>
    //document.getElementById("REPORT").style.display = 'none';
    //document.getElementById("refreshButton").style.display = 'none';
</script>
<%@include file="footer.jsp" %>