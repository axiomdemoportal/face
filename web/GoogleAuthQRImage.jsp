<%-- 
    Document   : GoogleAuthQRImage
    Created on : 20 Dec, 2016, 5:59:39 PM
    Author     : bluebricks
--%>




<%@page import="com.mollatech.axiom.nucleus.crypto.HWSignature"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="qrcodegenerator.QrcodeGenerator"%>
<%@page import="com.google.common.io.BaseEncoding"%>
<%@page import="com.mollatech.axiom.mobiletrust.crypto.CryptoManager"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Otptokens"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
   
     SessionManagement sManagement = new SessionManagement();
            String sessionid = sManagement.OpenSessionForWS("DxJUGvzOF1+zS7BaeclbdudlSIs=", "dc57912c214", "dc57912c214", " ");
          String channelid = "DxJUGvzOF1+zS7BaeclbdudlSIs=";
            ChannelManagement cManagement = new ChannelManagement();
           Channels channel = cManagement.getChannelByID(channelid);
            String useridencode = request.getParameter("userid");
     OTPTokenManagement otp = new OTPTokenManagement(channelid);
    String password = "E1BB465D57CAE7ACDBBE8091F9CE83DF";
    
         CryptoManager cmObj = new CryptoManager();
            byte[] aeskey = cmObj.generateKeyAES(password.getBytes(), 128);
            byte [] encDataBytes = HWSignature.hex2Byte(useridencode);
            byte[] byteEncryptedData = cmObj.decryptAES(aeskey, encDataBytes);
             String userid= new String(byteEncryptedData);
             int sotpstatus = otp.getStatus(sessionid, channelid, userid, 1, 1);
            String image=null;
            if (sotpstatus == 1) {
                Otptokens details = otp.getOtpObjByUserId(sessionid, channelid, userid, 1);
                String secret = details.getSecret();
                System.out.println("Secret>> " + secret);
                byte[] secret1 = CryptoManager.hexStringToByteArray(secret);
               
                String secrestforQR = BaseEncoding.base32().encode(secret1);
                String url = "otpauth://totp/" + channel.getName() + ":" + userid + "?secret=" + secrestforQR + "&issuer=" + channel.getName();
                System.out.println("url is :" + url);
                QrcodeGenerator Qrimage = new QrcodeGenerator();
                image = Qrimage.CreateQRCode(url);
            }else{
                String error="User otp status is not active..!!";
            }
    %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Google Authentication QR Image</title>
    </head>
    <body>
        <br>
      <h1 class="text-success"> &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;Google Authentication QR Image</h1>
      <hr>  &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<img src="data:image/png;base64,<%=image%>">
        <br>
</html>
<%--<%@include file="footer.jsp" %>--%>