<%@page import="com.mollatech.axiom.nucleus.db.Operators"%>
<%@page import="com.mollatech.axiom.connector.access.controller.AccessMatrixSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Epintracker"%>
<%@page import="com.mollatech.ecopin.management.EPINManagement"%>
<%@page import="com.mollatech.axiom.nucleus.settings.SendNotification"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="java.text.SimpleDateFormat"%>


<%
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    String _userID = request.getParameter("_userID");
    String _startdate = request.getParameter("_startDate");
    String _enddate = request.getParameter("_endDate");
    String _reporttype = request.getParameter("_reporttype");
    DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    Date startDate = null;
    if (_startdate != null && !_startdate.isEmpty()) {
        startDate = (Date) formatter.parse(_startdate);
    }
    Date endDate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        endDate = (Date) formatter.parse(_enddate);
    }
    int ireporttype = 000;
    if (_reporttype != null) {
        ireporttype = Integer.parseInt(_reporttype);
    }
    EPINManagement eMngt = new EPINManagement();
    UserManagement uMngt = new UserManagement();
    Epintracker[] epinobj = null;
    if (ireporttype == 2) {//ecopin system report = 2
        epinobj = eMngt.searchSystemEPINObjByUserID(channel.getChannelid(), startDate, endDate);
    } else {
        epinobj = eMngt.searchEPINObjByUserID(channel.getChannelid(), _userID, startDate, endDate);
    }
    //   AuthUser[] arruser =  uMngt.SearchUsers(sessionId, channel.getChannelid(), value);
    String start = null;
    if (startDate != null) {
        start = formatter.format(startDate);
    }
    String end = null;
    if (endDate != null) {
        end = formatter.format(endDate);
    }


%>
<div class="row-fluid">
    <div class="span6">
        <div class="control-group">                        
            <div class="span1">
                <div class="control-group form-inline">
                    <%   Operators oprObjI = (Operators) session.getAttribute("_apOprDetail");
                      AccessMatrixSettings accessObjN = (AccessMatrixSettings) session.getAttribute("_apAccessEntry");
                     if(oprObjI.getRoleid() != 1 ){//1 sysadmin
                        if (ireporttype == 2) {%>
                    <%if (accessObjN != null && accessObjN.downloadEpinSystemReport == true) {%>
              
                      <button class="btn btn-info " onclick="ecopinSystemReportDownloadCSV('<%=_userID%>', '<%=_startdate%>', '<%=_enddate%>')" type="button"><i class="icon-white icon-chevron-down"></i> CSV</button>
                        <%} else {%>
                        <button class="btn btn-info " onclick="InvalidRequestEpin('epinreportdownload')" type="button"><i class="icon-white icon-chevron-down"></i> CSV</button>
                        <%}%>
                        <!--<i class="icon-white icon-chevron-down"></i>Download CSV</a>-->
                        <%} else {%>
                        <% if (accessObjN != null && accessObjN.downloadEpinUserSpecificReport == true) {%>
                        <button class="btn btn-info " onclick="ecopinReportDownloadCSV('<%=_userID%>', '<%=_startdate%>', '<%=_enddate%>')" type="button"><i class="icon-white icon-chevron-down"></i> CSV</button>
                        <%} else {%>
                        <button class="btn btn-info " onclick="InvalidRequestEpin('epinreportdownload')" type="button"><i class="icon-white icon-chevron-down"></i> CSV</button>
                        <%}%>
                        <%}}else{  if (ireporttype == 2) {%>
                           <button class="btn btn-info " onclick="ecopinReportDownloadCSV('<%=_userID%>', '<%=_startdate%>', '<%=_enddate%>')" type="button"><i class="icon-white icon-chevron-down"></i> CSV</button>
                          <%}else{%>
                           <button class="btn btn-info " onclick="ecopinSystemReportDownloadCSV('<%=_userID%>', '<%=_startdate%>', '<%=_enddate%>')" type="button"><i class="icon-white icon-chevron-down"></i> CSV</button>
                          <%}%> 
                        <%}%>
                </div>
            </div>
            <div class="span1">
                <div class="control-group form-inline">
                    <%   if(oprObjI.getRoleid() != 1 ){//1 sysadmin
                      if (ireporttype == 2) {%>
                    <%if (accessObjN != null && accessObjN.downloadEpinSystemReport == true) {%>
                       <button class="btn btn-info" onclick="ecopinSystemReportDownloadPDF('<%=_userID%>', '<%=_startdate%>', '<%=_enddate%>')" type="button"><i class="icon-white icon-chevron-down"></i> PDF</button>
                        <%} else {%>
                        <button class="btn btn-info" onclick="InvalidRequestEpin('epinreportdownload')" type="button"><i class="icon-white icon-chevron-down"></i> PDF</button>
                        <%}%>
                        <!--<i class="icon-white icon-chevron-down"></i>Download PDF</a>-->
                        <%} else {%>
                        <% if (accessObjN != null && accessObjN.downloadEpinUserSpecificReport == true) {%>
                        <button class="btn btn-info " onclick="ecopinReportDownloadPDF('<%=_userID%>', '<%=_startdate%>', '<%=_enddate%>')" type="button"><i class="icon-white icon-chevron-down"></i> PDF</button>
                        <%} else {%>
                        <button class="btn btn-info " onclick="InvalidRequestEpin('epinreportdownload')" type="button"><i class="icon-white icon-chevron-down"></i> PDF</button>
                        <%}%>
                        <!--<i class="icon-white icon-chevron-down"></i>Download PDF</a>-->
                        <%}}else{if (ireporttype == 2) {%>
                        <button class="btn btn-info" onclick="ecopinSystemReportDownloadPDF('<%=_userID%>', '<%=_startdate%>', '<%=_enddate%>')" type="button"><i class="icon-white icon-chevron-down"></i> PDF</button>
                       <%}else{%>
                        <button class="btn btn-info " onclick="ecopinReportDownloadPDF('<%=_userID%>', '<%=_startdate%>', '<%=_enddate%>')" type="button"><i class="icon-white icon-chevron-down"></i> PDF</button>
                       <%}}%>
                </div>
            </div>
            <div class="span1">
                <div class="control-group form-inline">
                    <%   if(oprObjI.getRoleid() != 1 ){//1 sysadmin
                    if (ireporttype == 2) {%>
                    <%if (accessObjN != null && accessObjN.downloadEpinSystemReport == true) {%>
                    
                          <button class="btn btn-info" onclick="ecopinSystemReportDownloadTXT('<%=_userID%>', '<%=_startdate%>', '<%=_enddate%>')" type="button"><i class="icon-white icon-chevron-down"></i> TXT</button>
                        <%} else {%>
                        <button class="btn btn-info" onclick="InvalidRequestEpin('epinreportdownload')" type="button"><i class="icon-white icon-chevron-down"></i> TXT</button>
                        <%}%>
                        <!--<i class="icon-white icon-chevron-down"></i>Download TXT</a>-->
                        <%} else {%>
                        <% if (accessObjN != null && accessObjN.downloadEpinUserSpecificReport == true) {%>
                        <button class="btn btn-info " onclick="ecopinReportDownloadTXT('<%=_userID%>', '<%=_startdate%>', '<%=_enddate%>')" type="button"><i class="icon-white icon-chevron-down"></i> TXT</button>
                        <%} else {%>
                        <button class="btn btn-info " onclick="InvalidRequestEpin('epinreportdownload')" type="button"><i class="icon-white icon-chevron-down"></i> TXT</button>
                        <%}%>
                        <!--<i class="icon-white icon-chevron-down"></i>Download TXT</a>-->
                        <%}}else{if (ireporttype == 2) {%>
                          <button class="btn btn-info" onclick="ecopinSystemReportDownloadTXT('<%=_userID%>', '<%=_startdate%>', '<%=_enddate%>')" type="button"><i class="icon-white icon-chevron-down"></i> TXT</button>
                         <%}else{%>
                          <button class="btn btn-info " onclick="ecopinReportDownloadTXT('<%=_userID%>', '<%=_startdate%>', '<%=_enddate%>')" type="button"><i class="icon-white icon-chevron-down"></i> TXT</button>
                         <%}}%>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div id="licenses_data_table">
            <table class="table table-striped" >
                <tr>
                    <td>No.</td>
                    <td>User Name</td>
                    <td>Phone</td>
                    <td>Email</td>
                    <td>Status</td>
                    <td>Type</td>
                    <td>Expiry In Minutes</td>
                    <td>Requested On</td>
                    <td>Sent On</td>
                </tr>
                <% if (epinobj != null) {
                        for (int i = 0; i < epinobj.length; i++) {
                            String uID = epinobj[i].getUserid();
                            System.out.print(uID);
                            AuthUser user = uMngt.getUser(sessionId, channel.getChannelid(), epinobj[i].getUserid());
                            String strstatus = "-";
                            if (user != null) {

                                if (epinobj[i].getStatus() == EPINManagement.APPROVED) {
                                    strstatus = "Delivered";
                                } else if (epinobj[i].getStatus() == EPINManagement.FAILEDTOVALIDATE) {
                                    strstatus = "Invalid";
                                } else if (epinobj[i].getStatus() == EPINManagement.EXPIRED) {
                                    strstatus = "Expired";
                                } else if (epinobj[i].getStatus() == EPINManagement.ALLOWED) {
                                    strstatus = "Allowed";
                                } else if (epinobj[i].getStatus() == EPINManagement.NOT_ALLOWED) {
                                    strstatus = "Dropped";
                                } else if (epinobj[i].getStatus() == EPINManagement.FAILED) {
                                    strstatus = "Not delivered";
                                } else if (epinobj[i].getStatus() == EPINManagement.REJECTED) {
                                    strstatus = "Rejected";
                                }

                                String strtype = "-";
                                if (epinobj[i].getType() == EPINManagement.OPERATOR_CONTROLLED) {
                                    strtype = "Operator Controlled";
                                } else if (epinobj[i].getType() == EPINManagement.DELIVERY) {
                                    strtype = "Delivery";
                                } else if (epinobj[i].getType() == EPINManagement.POLICY_CHECK) {
                                    strtype = "Policy Check";
                                } else if (epinobj[i].getType() == EPINManagement.VALIDATION) {
                                    strtype = "Validation";
                                }

                                Calendar current = Calendar.getInstance();
                                current.setTime(epinobj[i].getRequestdatetime());
                                current.add(Calendar.MINUTE, epinobj[i].getExpiryInMins());
                                Date expirytime = current.getTime();
                                String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";
                                SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
                                String extime = sdf.format(expirytime);

                                String senttime = "-";
                                if (epinobj[i].getSentdatetime() != null) {
                                    senttime = sdf.format(epinobj[i].getSentdatetime());
                                }

                %>
                <tr>
                    <td><%=(i + 1)%></td>
                    <td><%=user.getUserName()%></td> 
                    <td><%=epinobj[i].getPhone()%></td> 
                    <td><%=epinobj[i].getEmailid()%></td> 
                    <td><%=strstatus%></td>
                    <td><%=strtype%></td>  
                    <td><%=extime%></td>  
                    <td><%=epinobj[i].getRequestdatetime()%></td>  
                    <td><%= senttime%></td>  
                </tr>
                <%}
                    }
                } else {%>
                <tr>
                    <td><%=1%></td>
                    <td>No Records Found</td>
                    <td>No Records Found</td>
                    <td>No Records Found</td> 
                    <td>No Records Found</td>
                    <td>No Records Found</td>
                    <td>No Records Found</td>
                    <td>No Records Found</td>
                    <td>No Records Found</td>
                    <%}%>
                </tr>
            </table>
        </div>
    </div>
                <div id ="d">
    <div class="span6">
        <div class="control-group">                        
            <div class="span1">
                <div class="control-group form-inline">
                    <% //   Operators oprObjI = (Operators) session.getAttribute("_apOprDetail");
//                      AccessMatrixSettings accessObjN = (AccessMatrixSettings) session.getAttribute("_apAccessEntry");
                     if(oprObjI.getRoleid() != 1 ){//1 sysadmin
                        if (ireporttype == 2) {%>
                    <%if (accessObjN != null && accessObjN.downloadEpinSystemReport == true) {%>
              
                      <button class="btn btn-info " onclick="ecopinSystemReportDownloadCSV('<%=_userID%>', '<%=_startdate%>', '<%=_enddate%>')" type="button"><i class="icon-white icon-chevron-down"></i> CSV</button>
                        <%} else {%>
                        <button class="btn btn-info " onclick="InvalidRequestEpin('epinreportdownload')" type="button"><i class="icon-white icon-chevron-down"></i> CSV</button>
                        <%}%>
                        <!--<i class="icon-white icon-chevron-down"></i>Download CSV</a>-->
                        <%} else {%>
                        <% if (accessObjN != null && accessObjN.downloadEpinUserSpecificReport == true) {%>
                        <button class="btn btn-info " onclick="ecopinReportDownloadCSV('<%=_userID%>', '<%=_startdate%>', '<%=_enddate%>')" type="button"><i class="icon-white icon-chevron-down"></i> CSV</button>
                        <%} else {%>
                        <button class="btn btn-info " onclick="InvalidRequestEpin('epinreportdownload')" type="button"><i class="icon-white icon-chevron-down"></i> CSV</button>
                        <%}%>
                        <%}}else{  if (ireporttype == 2) {%>
                           <button class="btn btn-info " onclick="ecopinReportDownloadCSV('<%=_userID%>', '<%=_startdate%>', '<%=_enddate%>')" type="button"><i class="icon-white icon-chevron-down"></i> CSV</button>
                          <%}else{%>
                           <button class="btn btn-info " onclick="ecopinSystemReportDownloadCSV('<%=_userID%>', '<%=_startdate%>', '<%=_enddate%>')" type="button"><i class="icon-white icon-chevron-down"></i> CSV</button>
                          <%}%> 
                        <%}%>
                </div>
            </div>
            <div class="span1">
                <div class="control-group form-inline">
                    <%   if(oprObjI.getRoleid() != 1 ){//1 sysadmin
                      if (ireporttype == 2) {%>
                    <%if (accessObjN != null && accessObjN.downloadEpinSystemReport == true) {%>
                       <button class="btn btn-info" onclick="ecopinSystemReportDownloadPDF('<%=_userID%>', '<%=_startdate%>', '<%=_enddate%>')" type="button"><i class="icon-white icon-chevron-down"></i> PDF</button>
                        <%} else {%>
                        <button class="btn btn-info" onclick="InvalidRequestEpin('epinreportdownload')" type="button"><i class="icon-white icon-chevron-down"></i> PDF</button>
                        <%}%>
                        <!--<i class="icon-white icon-chevron-down"></i>Download PDF</a>-->
                        <%} else {%>
                        <% if (accessObjN != null && accessObjN.downloadEpinUserSpecificReport == true) {%>
                        <button class="btn btn-info " onclick="ecopinReportDownloadPDF('<%=_userID%>', '<%=_startdate%>', '<%=_enddate%>')" type="button"><i class="icon-white icon-chevron-down"></i> PDF</button>
                        <%} else {%>
                        <button class="btn btn-info " onclick="InvalidRequestEpin('epinreportdownload')" type="button"><i class="icon-white icon-chevron-down"></i> PDF</button>
                        <%}%>
                        <!--<i class="icon-white icon-chevron-down"></i>Download PDF</a>-->
                        <%}}else{if (ireporttype == 2) {%>
                        <button class="btn btn-info" onclick="ecopinSystemReportDownloadPDF('<%=_userID%>', '<%=_startdate%>', '<%=_enddate%>')" type="button"><i class="icon-white icon-chevron-down"></i> PDF</button>
                       <%}else{%>
                        <button class="btn btn-info " onclick="ecopinReportDownloadPDF('<%=_userID%>', '<%=_startdate%>', '<%=_enddate%>')" type="button"><i class="icon-white icon-chevron-down"></i> PDF</button>
                       <%}}%>
                </div>
            </div>
            <div class="span1">
                <div class="control-group form-inline">
                    <%   if(oprObjI.getRoleid() != 1 ){//1 sysadmin
                    if (ireporttype == 2) {%>
                    <%if (accessObjN != null && accessObjN.downloadEpinSystemReport == true) {%>
                    
                          <button class="btn btn-info" onclick="ecopinSystemReportDownloadTXT('<%=_userID%>', '<%=_startdate%>', '<%=_enddate%>')" type="button"><i class="icon-white icon-chevron-down"></i> TXT</button>
                        <%} else {%>
                        <button class="btn btn-info" onclick="InvalidRequestEpin('epinreportdownload')" type="button"><i class="icon-white icon-chevron-down"></i> TXT</button>
                        <%}%>
                        <!--<i class="icon-white icon-chevron-down"></i>Download TXT</a>-->
                        <%} else {%>
                        <% if (accessObjN != null && accessObjN.downloadEpinUserSpecificReport == true) {%>
                        <button class="btn btn-info " onclick="ecopinReportDownloadTXT('<%=_userID%>', '<%=_startdate%>', '<%=_enddate%>')" type="button"><i class="icon-white icon-chevron-down"></i> TXT</button>
                        <%} else {%>
                        <button class="btn btn-info " onclick="InvalidRequestEpin('epinreportdownload')" type="button"><i class="icon-white icon-chevron-down"></i> TXT</button>
                        <%}%>
                        <!--<i class="icon-white icon-chevron-down"></i>Download TXT</a>-->
                        <%}}else{if (ireporttype == 2) {%>
                          <button class="btn btn-info" onclick="ecopinSystemReportDownloadTXT('<%=_userID%>', '<%=_startdate%>', '<%=_enddate%>')" type="button"><i class="icon-white icon-chevron-down"></i> TXT</button>
                         <%}else{%>
                          <button class="btn btn-info " onclick="ecopinReportDownloadTXT('<%=_userID%>', '<%=_startdate%>', '<%=_enddate%>')" type="button"><i class="icon-white icon-chevron-down"></i> TXT</button>
                         <%}}%>
                </div>
            </div>
        </div>
    </div>
                        </div>
</div>             
