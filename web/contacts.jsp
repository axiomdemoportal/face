<%@page import="com.mollatech.axiom.nucleus.settings.ContactTagsSetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Contacts"%>
<%@page import="com.mollatech.dictum.management.ContactManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/ajaxfileupload.js"></script>
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<script src="./assets/js/contacts.js"></script>
<%
    Channels channelOnj = (Channels) session.getAttribute("_apSChannelDetails");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");


//        String _activeElement = request.getParameter("_activeElement");
//        int iactiveElement = 0;
//        if(_activeElement != null){
//            iactiveElement = Integer.parseInt(_activeElement);
//        }

    SettingsManagement smng = new SettingsManagement();
    ContactTagsSetting tagsObj = (ContactTagsSetting) smng.getSetting(sessionId, channel.getChannelid(), SettingsManagement.CONTACT_TAGS, SettingsManagement.PREFERENCE_ONE);

    //TagsManagement tagmngObj = new TagsManagement();
    //Tags[] tagsObj = tagmngObj.getAllTags(channelOnj.getChannelid());
%>

<div id="editContact0" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >    
</div>



<div class="container-fluid">
    <h2 class="text-success">Contact and Tag Management</h2>
    <p>Complete list of contacts to whom individual, bulk messages and interactions (surveys, feedback etc) can be sent out.</p>

    <div class="tabbable">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#contacts" data-toggle="tab">Contacts</a></li>
            <li><a href="#tags" data-toggle="tab">Tags</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="contacts">

                <div class="row-fluid">
                    <h3>Add Contacts</h3>
                    <div>
                        <p>Add bulk list of contacts through. <a href="#uploadContactsFile" class="btn" data-toggle="modal">File Upload </a>
                            Or add one contact at a time using  <a href="#uploadContact" class="btn" data-toggle="modal">Add Contact Form</a> </p>
                    </div>

                    <h3>Search Contacts through name,phone or email</h3>
                    <div class="input-append">
                        <form id="searchContactsForm" name="searchContactsForm">
                            <input type="text" id="_keyword" name="_keyword" placeholder="search contact using name, phone or email" class="span9"><span class="add-on"><i class="icon-search"></i></span>
                            <a href="#" class="btn btn-success" onclick="searchContacts(1)">Search Contact(s)</a>
                        </form> 
                    </div>
                    <div id="contants_table_main">

                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tags">
                <div class="row-fluid">
                    <h3>Manage Tags</h3>
                    <div>
                        <p>
                        <div class="row-fluid">
                            <form class="form-horizontal" id="tagsform" name="tagsform">
                                <div class="control-group">
                                    <label class="control-label"  for="username">Keywords</label>
                                    <div class="controls">
                                        <textarea name="_tagsList" id="_tagsList" style="width:100%">
                                            <%
                                                String[] strTags;
                                                if (tagsObj == null) {
                                                    strTags = new String[3];
                                                    strTags[0] = "facebook";
                                                    strTags[1] = "twitter";
                                                    strTags[2] = "linkedin";                                                    
                                                } else {
                                                    strTags = tagsObj.getTags();

                                                }
                                                if (strTags != null)
                                                    for (int j = 0; j < strTags.length; j++) {
                                            %>        
                                            <%=strTags[j]%>,
                                            <%
                                                }
                                            %>
                                        </textarea>
                                        <br>Attention: Please enter comma (",") for separating tags.
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="controls">
                                        <div id="save-epin-settings-result"></div>
                                        <button class="btn btn-primary" onclick="SaveTags()" type="button" id="SaveTagsButton">Save Changes Now >> </button>
                                    </div>
                                </div>

                            </form>
                        </div>
                        </p>
                    </div>
                    <h3>Search Contacts using Tags</h3>
                    <div class="control-group">
                                    
                                    <div class="controls">
                                        <form id="searchContactsTAGForm" name="searchContactsTAGForm">
                            <select name="_tags2Search" id="_tags2Search">
                                    <% for (int j = 0; j < strTags.length; j++) {
                                    %>
                                    <option value="<%=strTags[j]%>"><%=strTags[j]%></option>                                             
                                    <%
                                    }%>
                                </select>
                            <a href="#" class="btn btn-success" onclick="searchContacts(2)">Search Contact(s)</a>
                        </form> 
                                    </div>
                    </div>
                    
                    <div id="contants_table_main1">
                    </div>
                </div>


            </div>
        </div>
    </div>

</div>


<div id="uploadContactsFile" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Upload Contacts</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="uploadContactsFileForm" name="uploadContactsFileForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Record Format</label>
                        <div class="controls">
                            <select class="span9" name="_Format" id="_Format" multiple>
                                <option value="1">Phone</option>
                                <option value="2">Email</option>
                                <option value="7">Fax No.</option>
                                <option value="3">Name followed by Phone</option>
                                <option value="4">Name followed by Email</option>                                                                
                                <option value="8">Name followed by Fax No</option>
                                <option value="9">Name followed by Phone & Email & Fax No</option>
                                <option value="5">Phone followed by Email</option>
                                <option value="6">Name followed by Phone & Email</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Delimiter</label>
                        <div class="controls">
                            <select class="span6" name="_delimiter" id="_delimiter" multiple>
                                <option value=",">,</option>
                                <option value=";">;</option>
                                <option value=":">:</option>
                                <option value="#">#</option>
                                <option value="~">~</option>
                                <option value="^">^</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Select File</label>
                        <div class="controls">
                            <input type="hidden" id="ads_imageEAD" name="ads_imageEAD">
                            <a id="displayTextEAD" href="#" onclick="toogleEAD()">Click to upload</a>
                            <div id="uploadImageEAD" style="display: none">
                                <form id="uploadFormEAD" id="uploadFormEAD">
                                    <input id="fileToUploadEAD" type="file" name="fileToUploadEAD"/>
                                    <button class="btn btn-mini btn-primary" id="buttonUploadEAD" onclick="return UploadContactFile()">Upload File Now>></button>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label"  for="username">Tag(s)</label>
                        <div class="controls">
                            <select class="span9" name="_tagIDCF" id="_tagIDCF" multiple>
                                <% for (int j = 0; j < strTags.length; j++) {
                                %>        
                                <option data-content="<span class='label label-success'><%=strTags[j]%></span>"value="<%=strTags[j]%>"><%=strTags[j]%></option>                                             
                                <%
                                    }%>
                            </select>

                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>
    <div id="add-new-contact-result"></div>
    <div id="download-contact"></div>
    <div class="modal-footer">
        
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="addContactFile()" id="addnewContactSubmitBut">Add Contacts Now >> </button>
    </div>
</div>

<div id="uploadContact" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Add New Contact</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="addContactsForm" name="addContactsForm">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label"  for="username">Name</label>
                        <div class="controls">
                            <input type="text" id="_nameMC" name="_nameMC" placeholder="set unique name for login" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Email Id</label>
                        <div class="controls">
                            <input type="text" id="_emailMC" name="_emailMC" placeholder="set emailid for notification" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Phone</label>
                        <div class="controls">
                            <input type="text" id="_phoneMC" name="_phoneMC" placeholder="set phone for notification" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Fax No</label>
                        <div class="controls">
                            <input type="text" id="_faxMC" name="_faxMC" placeholder="set fax number for notification" class="input-xlarge">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label"  for="username">Tag(s)</label>
                        <div class="controls">
                            <select  name="_tagIDMC" id="_tagIDMC"  class="span9" multiple>
                                <% for (int j = 0; j < strTags.length; j++) {
                                %>
                                <option data-content="<span class='label label-success'><%=strTags[j]%></span>"value="<%=strTags[j]%>"><%=strTags[j]%></option>                                             
                                <%
                                    }%>
                            </select>
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>
    <div class="span3" id="add-create-contact-result"></div>

    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="addContact()" id="addnewSingleContactSubmitBut">Add Contacts Now >> </button>
    </div>
</div>    

<script>    
    $(document).ready(function() { $("#_tagIDCF").select2() }); 
    $(document).ready(function() { $("#_tagIDMC").select2() });        
    $(document).ready(function() { $("#_tagID").select2() });
    $("#_Format").select2({ maximumSelectionSize: 1 });
    $("#_delimiter").select2({ maximumSelectionSize: 1 });
    
    $(document).ready(function() { 
        $("#_tagsList").select2({
            tags:[],
            dropdownCss: {display: 'none'},
            tokenSeparators: [","]
        });                   
    }); 
    

    
    
</script>




<%@include file="footer.jsp" %>
