<%@page import="com.mollatech.axiom.nucleus.db.Srtracking"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SignReqTrackingManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SignRequestManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Signingrequest"%>
<%@page import="com.mollatech.axiom.nucleus.db.Doccategory"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CategoryManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Users"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Usergroups"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserGroupsManagement"%>
<%@include file="header.jsp" %>
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/epdfsigning.js"></script>
<script src="./assets/js/select2/select2.js"></script>
<script src="./assets/js/ajaxfileupload.js"></script>
<script src="./assets/js/bootstrap-fileupload.js"></script> 
<link href="./assets/css/bootstrap-fileupload.css" rel="stylesheet">
<div class="container-fluid">
    <br>
    <div id="legend">
        <h1 class="text-success">Edit E-Signing document Request</h1>
        <p>Update the E-Signing document request</p>
        <legend class=""></legend>
    </div>
    <%        String _docId = request.getParameter("_docId");
        int docId = -1;
        if (_docId != null) {
            docId = Integer.parseInt(_docId);
        }
        Channels channels = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        int type = 1, no = 1;
        String channel = channels.getChannelid();
        UserGroupsManagement ugm = new UserGroupsManagement();
        Usergroups[] ugr = ugm.getAllGroup(sessionId, channel);
        UserManagement um = new UserManagement();
        CategoryManagement cm = new CategoryManagement();
        Doccategory[] categorydetails = cm.getAllCategoryDetails(sessionId, channel);
        SignRequestManagement srm = new SignRequestManagement();
        Signingrequest sr = srm.getEpdfbyId(sessionId, channel, docId);
        Doccategory catdetails = cm.getCategoryDetail(sessionId, channel, sr.getDoccategory());
    %>        
    <form class="form-horizontal" id="editDocumentform" name="editDocumentform">
        <div class="container-fluid">
            <div class="row-fluid">
                &nbsp;&nbsp;
                <div class="input-prepend">
                    <span class="add-on">&nbsp;&nbsp;Document Name :&nbsp;&nbsp;</span>               
                    <input type="text" id="_edocumentname" name="_edocumentname" placeholder="Name of the document" class="span7" value="<%= sr.getDocname()%>">                 
                </div>
                &nbsp;&nbsp;&nbsp;
                <div class="input-prepend">
                    <span class="add-on">&nbsp;&nbsp;Document Category:&nbsp;&nbsp;</span>               
                    <select class="span6" name="_edocCategory" id="_edocCategory">
                        <option value="-1"> Select Category </option> 
                        <% if (categorydetails != null) {
                                for (int i = 0; i < categorydetails.length; i++) {

                                    if (categorydetails[i].getStatus() != 0) {
                        %>
                        <option value=<%= categorydetails[i].getId()%> <% if (catdetails.getId() == categorydetails[i].getId()) {%> selected <%}%>> <%= categorydetails[i].getCategoryname()%> 
                        </option>
                        <%}
                                }
                            }%>
                    </select>
                </div>   
                <br>
                <br>                        
                <!--upload document-->
                <div class="row-fluid">
<!--                    <form class="form-horizontal" id="uploadXMLFormEAD" name="uploadXMLFormEAD">
                        <fieldset>
                            <div id="uploadingfile">
                                <label class="control-label"  for="username"><strong>Select Document File:</strong></label>                                    
                                <div class="controls fileupload fileupload-new" data-provides="fileupload">
                                                                    <div class="input-append">
                                                                        <div class="uneditable-input span3"><i class="icon-file fileupload-exists"></i> 
                                                                            <span class="fileupload-preview"></span>
                                                                        </div>
                                                                        <span class="btn btn-file" >
                                                                            <span class="fileupload-new">Select file</span>
                                                                            <span class="fileupload-exists">Change</span>
                                                                            <input type="file" id="filePdfToUpload" name="filePdfToUpload"/>
                                                                        </span>
                                                                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                                    </div>
                                    <input type="file" id="filePdfToUpload" name="filePdfToUpload" accept="application/pdf"/>
                                    <button class="btn btn-success" id="buttonUploadImage"  onclick="PDFUpload()" type="button">Upload Now</button>
                                    <p><font style="font-style: oblique">(  Note: Document is already uploaded, upload document again to update)</font></p>
                                </div>
                            </div>
                        </fieldset>
                    </form>                    -->
<form class="form-horizontal" id="uploadXMLFormEAD" name="uploadXMLFormEAD">
                    <fieldset>
                        <div id="uploadingeditfile">
                            <label class="control-label"  for="username">Select Document File:</label>                                    
                            <div class="controls fileupload fileupload-new" data-provides="fileupload">
                                <input type="file" id="filePdfToUpload" name="filePdfToUpload" accept="application/pdf"/>
<!--                                <div class="input-append">
                                    <div class="uneditable-input span3"><i class="icon-file fileupload-exists"></i> 
                                        <span class="fileupload-preview"></span>
                                    </div>
                                    <span class="btn btn-file" >
                                        <span class="fileupload-new">Select file</span>
                                        <span class="fileupload-exists">Change</span>                                       
                                        <input type="file" id="filePdfToUpload" name="filePdfToUpload"/>
                                    </span>                                  
                                    <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                </div>-->
                                &nbsp;&nbsp; &nbsp;
                                <button class="btn btn-success" id="buttonUploadImage"  onclick="PDFUploadForEdit()" type="button">Upload Now</button>
                            </div>                        
                        </div>
                    </fieldset>
                </form>   
                </div>            
                <br>
                <div class="row-fluid">
                    &nbsp;
                    <strong>&nbsp;Document Note:&nbsp;&nbsp;  </strong>
                    <textarea name="_edocNote" placeholder="provide proper document note" id="_edocNote"><%= sr.getNote()%></textarea>                   
                    &nbsp;&nbsp;Expiry (in Days):&nbsp;&nbsp;&nbsp;               
                    <select class="span2" name="_edocExp" id="_edocExp" >                     
                        <option value="1">1 day</option>
                        <option value="2">2 days</option>
                        <option value="3">3 days</option>
                        <option value="4">4 days</option>
                        <option value="5">5 days</option>
                        <option value="6">6 days</option>
                        <option value="7">7 days</option>
                        <option value="8">8 days</option>
                        <option value="9">9 days</option>
                        <option value="10">10 days</option>
                    </select> 
                </div>
                <br>
                <legend class=""></legend>
                <h3 class="text-success">Signers</h3>
                <div class="row-fluid">                   
                    <a     id="addnewsigner" name="addnewsigner"  class="btn btn-primary" onclick="addsg()"> Add New Signer</a>
                    <a     id="removesigner" name="removesigner"  class="btn btn-primary" onclick="removefield(no - 1)"> Remove</a>                  
                </div>
                <br>   
                <%
                    SignReqTrackingManagement srtm = new SignReqTrackingManagement();
                    Srtracking[] srtracking = srtm.getAllSRTrackingDetailsbydocId(sessionId, channel, docId);
                    if (srtracking != null) {
                        for (int i = 0; i < srtracking.length; i++) {
                            AuthUser user = um.getUser(sessionId, channel, srtracking[i].getUserid());
                            String Username = user.getUserName();

                %>
                <script>
                    var no = <%=(i + 1)%>;
                </script>

                <epdf<%=(i + 1)%>>
                    <div class="row-fluid">&nbsp;&nbsp;
                        <span class="add-on"><strong> Signer <%=(i + 1)%> :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></span>
                        <div class="input-prepend">
                            <span class="add-on">&nbsp;&nbsp;Group : &nbsp;</span>
                            <select id="_groupAdd<%=(i + 1)%>" name="_groupAdd<%=(i + 1)%>" class="span8"  onchange="changeusers(this.value, 1)"> 
                                <%
                                    if (ugr != null) {
                                        for (int g = 0; g < ugr.length; g++) {
                                            Usergroups usergroup = ugm.getGroupByGroupId(sessionId, channel, ugr[g].getGroupid());
                                            String group = ugr[g].getGroupname();
                                            System.out.println(i + " " + srtracking[i].getGroupid());
                                            System.out.println(g + " " + ugr[g].getGroupid());
                                %>      

                                <option value="<%= ugr[g].getGroupid()%>" <%if (srtracking[i].getGroupid() == ugr[g].getGroupid()) {%> selected <%}%> ><%=group%></option>
                                <%}
                                    }%>                                
                                <div id="_GroupSelect" name="_GroupSelect">
                                </div>                                                             
                            </select> 
                        </div>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <div class="input-prepend">
                            <span class="add-on">&nbsp;&nbsp;User :&nbsp;</span>
                            <select id="_userAdd<%=(i + 1)%>" name="_userAdd<%=(i + 1)%>" class="span18"  >
                                <%
                                    AuthUser[] allusers = um.getAllUsers(sessionId, channel);
                                    if (allusers != null) {
                                        for (int u = 0; u < allusers.length; u++) {

                                            String UserName = allusers[u].getUserName();
                                %>  
                                <option value="<%=allusers[u].getUserId()%>" <%if (srtracking[i].getUserid().equals(allusers[u].getUserId())) {%> selected <%}%> ><%=UserName%></option>
                                <%}
                                    }%>
                                <div id="_UsersSelect" name="_UsersSelect"></div>
                            </select> 
                        </div>
                    </div> 
                </epdf<%=(i + 1)%>>         
                <br>
                <%}
                    }%>
                <addsigner></addsigner>
                <br>

                <div class="row-fluid">                
                    <div class="control-group">
                        <button class="btn btn-success" id="editepdfsign" onclick="editpdfsign(no - 1,<%=docId%>)" type="button">Update Now >> </button>
                        <button class="btn btn-primary" onclick="cancel()" type="button">Cancel </button>            
                    </div>    
                </div>
            </div> 
        </div>
    </form>
</div>
<script>
    $('#_edocExp option[value="<%=sr.getExpiryday()%>"]').attr('selected', true);
    no =<%=srtracking.length%> + 1;
    function removefield(values) {
        if (no > 2) {
            $('epdf' + values).remove();
            no = no - 1;
        }
        if (no == 2) {
            $('#removesigner').attr("disabled", true);
        }
    }
    function changeusers(val, val1) {

        var s1 = './ChangeGroup.jsp?_groupId=' + val;
        $.ajax({
            type: 'GET',
            url: s1,
            success: function(data) {
                $('#_userAdd' + val1).html(data);
            }
        });
    }
    function loadgroups(val) {
        var s = './loadGroups.jsp?';
        $.ajax({
            type: 'GET',
            url: s,
            success: function(data) {
                $('#_groupAdd' + val).html(data);
            }
        });
    }

    function addsg() {
        $("addsigner").append('<epdf' + no + '><div class="row-fluid">&nbsp;&nbsp;&nbsp;<span class="add-on"><strong>Signer&nbsp;' + no + '&nbsp;:' +
                '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></span>' +
                '<div class="input-prepend"><span class="add-on"> &nbsp;&nbsp;Group' +
                '&nbsp;:&nbsp;&nbsp;</span><select id="_groupAdd' + no + '"name="_groupAdd' + no + '"class="span8"' +
                'onchange="changeusers(this.value,' + no + ')"><//%for(int i=0; i<ugr.length; i++)' +
                '{%><option value ="<//%=ugr[i].getGroupid()%>"><//%=ugr[i].getGroupname()%> ' +
                '</option><//%}%></select></div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
                ' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class="input-prepend">' +
                '<span class="add-on">&nbsp;&nbsp;User :&nbsp;&nbsp;</span>' +
                '<select id="_userAdd' + no + '"name="_userAdd' + no + '"class="span18">' +
                '<div id="_UsersSelect" name="_UsersSelect"></div></select></div>&nbsp;' +
                '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
                '&nbsp;&nbsp;&nbsp;&nbsp;</div><br></epdf' + no + '>');
        loadgroups(no);
        $('#removesigner').attr("disabled", false);
        no = no + 1;
    }

</script>
<%@include file="footer.jsp" %>
