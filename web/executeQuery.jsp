<%-- 
    Document   : executeQuery
    Created on : May 9, 2017, 4:46:41 PM
    Author     : pramodchaudhari
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="org.hibernate.Transaction"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.AES"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.LoadSettings"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.cfg.AnnotationConfiguration"%>
<%@page import="org.hibernate.SessionFactory"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        try{
        String query = (String) request.getParameter("Query");
        AES aesObj = new AES();
        String dbtype = LoadSettings.g_sSettings.getProperty("db.type");
        System.out.print("dbtype"+dbtype);
        dbtype = aesObj.PINDecrypt(dbtype, AES.getSignature());
          System.out.print("dbtype"+dbtype);
        AnnotationConfiguration config1 = new AnnotationConfiguration();
        config1.setProperty("hibernate.connection.datasource", "java:/comp/env/jdbc/mainDB");
        config1.setProperty("hibernate.show_sql", "true");
        if (dbtype.equalsIgnoreCase("Mysql")) {
            config1.setProperty("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
            config1.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
        } else if (dbtype.equalsIgnoreCase("Oracle")) {
            config1.setProperty("hibernate.connection.driver_class", "oracle.jdbc.OracleDriver");
            config1.setProperty("hibernate.dialect", "org.hibernate.dialect.OracleDialect");
        }
        config1.setProperty("hibernate.transaction.factory_class", "org.hibernate.transaction.JDBCTransactionFactory");
        config1.setProperty("hibernate.current_session_context_class", "thread");
        config1.setProperty("hibernate.show_sql", "false");
        config1.setProperty("log4j.logger.org.hibernate", "server");
        
        config1.addResource("com/mollatech/axiom/nucleus/db/Accesses.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Channels.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Operators.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Otptokens.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Remoteaccess.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Resources.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Roles.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Sessions.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Settings.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Channellogs.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Audit.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Twowaysession.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Pushmessagemappers.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Isologs.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Epintracker.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Epinsessions.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Bulkmsg.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Templates.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Contacts.hbm.xml");
//            config1.addResource("com/mollatech/axiom/nucleus/db/Tags.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Pushmessages.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Interactions.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Interactionresponse.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Interactionsexecutions.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Certificates.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Pkitokens.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Trusteddevice.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Geofence.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Geotrack.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Timestamp.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Clientdestroy.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Remotesignature.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Questionsandanswers.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Questions.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Otptrail.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Registerdevicepush.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Passwordtrail.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Connectorstatusaudit.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Registrationcodetrail.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Schedulersettings.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Schedulertracking.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Approvalsettings.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Units.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Errormessages.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Kyctable.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Snmpreceiversettings.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Snmpreceivertracking.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Securephrase.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Pushbasedauthentication.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Usergroups.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Countrylist.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Statelist.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Securetrap.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Systemmessagesettings.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Systemmessagereceivertracking.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Txdetails.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Twowayauth.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Billmanager.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Monitorsettings.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Monitortracking.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Webresource.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Signingrequest.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Srtracking.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Doccategory.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/Ssodetails.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/ApWebseal.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/ApEasylogin.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/ApEasyloginsession.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/ApOtpacknowledgement.hbm.xml");
            config1.addResource("com/mollatech/axiom/nucleus/db/ApCertDiscovery.hbm.xml");
             config1.addResource("com/mollatech/axiom/nucleus/db/ApPushWatch.hbm.xml");
        
         System.out.print("TTT");
        SessionFactory factoy = config1.buildSessionFactory();
        Session ss = factoy.openSession();
        out.write("SSS"+ss.connection().getCatalog());
        out.write("SSS"+query);
        Query que = ss.createQuery(query);
        out.print(que);
        if (query.contains("delete") || query.contains("update") || query.contains("create")) {
            Transaction tx = ss.beginTransaction();
            int a = que.executeUpdate();
            out.print(" Query Result : " + a);
            tx.commit();
            ss.close();
        } else {
            List ll = que.list();
          
            if (ll != null && !ll.isEmpty()) {
                 out.println(" Query Result Count : " + ll.size());
                 out.print("\n");
                for (int i = 0; i < ll.size(); i++) {
                    out.println(" Query Result : " + ll.get(i));
                }
            }
            ss.close();
        }
        }catch(Exception ex)
        {
        ex.printStackTrace();
        }
        

    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!--<title>JSP Page</title>-->
    </head>
</html>
