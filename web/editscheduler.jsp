<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Hashtable"%>
<%@page import="com.mollatech.axiom.nucleus.db.Roles"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.settings.SchedulerSettingEntry"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.AxiomProtect"%>
<%@page import="com.mollatech.axiom.nucleus.db.Schedulersettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SchedulerManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%
    String _schedulerName = request.getParameter("_schedulerName");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");

    SchedulerManagement sObj = new SchedulerManagement();
    Schedulersettings schObj = sObj.getSetting(sessionId, channel.getChannelid(), _schedulerName);
    ByteArrayInputStream bais = new ByteArrayInputStream(AxiomProtect.AccessDataBytes(schObj.getSchedulerSettingEntry()));
    Object object = SchedulerManagement.deserializeFromObject(bais);
    SchedulerSettingEntry schedulerSetting = null;
    if (object instanceof SchedulerSettingEntry) {
        schedulerSetting = (SchedulerSettingEntry) object;
        
    }


%> 

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
    <h3><div id="idEditSchedulerName">Edit "<%=schObj.getSchedulerName()%>"</div></h3>
</div>
<div class="modal-body">
    <div class="row-fluid">
        <form class="form-horizontal" id="editSchedulerForm" name="editSchedulerForm">
            <fieldset>
                <input type="hidden" id="_gatewayType" name="_gatewayType">
                <input type="hidden" id="_schedulerStatus" name="_schedulerStatus">
                <input type="hidden" id="_gatewayPreference" name="_gatewayPreference">
                <input type="hidden" id="_gatewayStatus" name="_gatewayStatus">
                <input type="hidden" id="_operatorRoles" name="_operatorRoles">
                <input type="hidden" id="_schedulerDuration" name="_schedulerDuration">
                <input type="hidden" id="_reportType" name="_reportType">
                <div class="control-group">
                    <label class="control-label"  for="username">Scheduler Name</label>
                    <div class="controls">
                        <input type="text" id="_schedulername" readonly name="_schedulername" value="<%=schObj.getSchedulerName()%>" placeholder="set unique name for scheduler" class="input-xlarge">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"  for="username">For Type</label>
                    <div class="controls">
                        <div>
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_gatewayType-scheduler"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">                                    
                                    <li><a href="#" onclick="GatewayType(1)">SMS</a></li>
                                    <li><a href="#" onclick="GatewayType(2)">USSD</a></li>
                                    <li><a href="#" onclick="GatewayType(3)">VOICE</a></li>
                                    <li><a href="#" onclick="GatewayType(4)">EMAIL</a></li>
                                    <li><a href="#" onclick="GatewayType(5)">FAX</a></li>
<!--                                    <li><a href="#" onclick="GatewayType(6)">FACEBOOK</a></li>
                                    <li><a href="#" onclick="GatewayType(7)">LINKEDIN</a></li>
                                    <li><a href="#" onclick="GatewayType(8)">TWITTER</a></li>-->
                                    <li><a href="#" onclick="GatewayType(18)">ANDROIDPUSH</a></li>
                                    <li><a href="#" onclick="GatewayType(19)">IPHONEPUSH</a></li>
                                </ul>
                            </div>

                            &nbsp;&nbsp;with Status
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_gatewayStatus-scheduler"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="GatewayStatus(99)">ALL</a></li>
                                    <li><a href="#" onclick="GatewayStatus(0)">SENT</a></li>
                                    <li><a href="#" onclick="GatewayStatus(2)">PENDING</a></li>
                                    <li><a href="#" onclick="GatewayStatus(-1)">FAILED</a></li>
                                    <li><a href="#" onclick="GatewayStatus(-5)">BLOCKED</a></li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"  for="username">For Preference</label>
                    <div class="controls">
                        <div>
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_gatewayPreference-scheduler"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="GatewayPreference(0)">Both</a></li>
                                    <li><a href="#" onclick="GatewayPreference(1)">Primary Gateway</a></li>
                                    <li><a href="#" onclick="GatewayPreference(2)">Secondary Gateway</a></li>
                                </ul>
                            </div>
                            Duration
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_schedulerDuration-scheduler"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="SchedulerDuration(0)">Hourly</a></li>
                                   <li><a href="#" onclick="SchedulerDuration(1)">Daily</a></li>
                                        <li><a href="#" onclick="SchedulerDuration(7)">Weekly</a></li>
                                        <li><a href="#" onclick="SchedulerDuration(30)">Monthly</a></li>
                                        <li><a href="#" onclick="SchedulerDuration(91)">Quarterly</a></li>
                                        <li><a href="#" onclick="SchedulerDuration(365)">Yearly</a></li>
                                </ul>
                            </div>


                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"  for="username">Send Report to</label>
                    <div class="controls">
                        <div>
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_operatorRoles-scheduler"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <%
                                        Roles currentrole=null;
                                        OperatorsManagement oMngt = new OperatorsManagement();
                                        Roles[] roles1 = oMngt.getAllRoles(channel.getChannelid());
                                        if (roles1 != null) {
                                            for (int i = 0; i < roles1.length; i++) {
                                                    if (schedulerSetting.getRolename() == ("" + roles1[i].getRoleid()) ) {
                                                        currentrole = roles1[i];
                                                    }
                                            %>

                                    <li><a href="#" onclick="OperatorRoles(<%=roles1[i].getRoleid()%>,'<%=roles1[i].getName()%>')"><%=roles1[i].getName()%></a></li>
                                        <%}
                                            }%>
                                </ul>
                            </div>
                                
                             &nbsp;report type    
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_reportType-scheduler"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ReportTypeScheduler(99)">PDF&CSV</a></li>
                                        <li><a href="#" onclick="ReportTypeScheduler(0)">PDF</a></li>
                                        <li><a href="#" onclick="ReportTypeScheduler(1)">CSV</a></li>
                                    </ul>
                                </div>

                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"  for="username">Tag(s)</label>
                    <div class="controls">
                        <!--                        <select  name="_EditSchedulerContact" id="_EditSchedulerContact"  class="span9" multiple>
                        
                        <%

                            Hashtable tagsAvailable = new Hashtable();
                            String[] tags = schedulerSetting.getSendTo();
                            for (int i = 0; i < tags.length; i++) {
                                tagsAvailable.put(tags[i], tags[i]);
                            }
                            for (int i = 0; i < tags.length; i++) {
                                tagsAvailable.remove(tags[i]);
                            }
                            for (int i = 0; i < tags.length; i++) {
                        %>
                        <option value="<%=tags[i]%>" selected><%=tags[i]%></option>                                             
                        <%
                            }
                            Enumeration tagids = tagsAvailable.keys();
                            while (tagids.hasMoreElements()) {
                                String tagid = (String) tagids.nextElement();
                                String tagname = (String) tagsAvailable.get(tagid);
                        %>
                            <option value="<%=tagname%>"><%=tagname%></option>                                             
                        <%
                            }
                        %>  
              </select>-->
                        <textarea name="_EditSchedulerContact0" id="_EditSchedulerContact0" style="width:100%">
                            <%
                                String[] strTags;
                                if (schedulerSetting == null) {
                                    strTags = new String[3];
                                    strTags[0] = "facebook";
                                    strTags[1] = "twitter";
                                    strTags[2] = "linkedin";
                                } else {
                                    strTags = schedulerSetting.getSendTo();

                                }
                                if (strTags != null)
                                    for (int j = 0; j < strTags.length; j++) {
                            %>        
                            <%=strTags[j]%>,
                            <%
                                }
                            %>
                        </textarea>
                        <br>Attention: Please enter comma (",") for separating emailids.

                    </div>
                </div>

            </fieldset>
            <script>
                SchedulerStatus(<%=schObj.getSchedulerStatus()%>);
                GatewayType(<%=schedulerSetting.getType()%>);
                GatewayStatus(<%=schedulerSetting.getGatewayStatus()%>);
                GatewayPreference(<%=schedulerSetting.getPrefrence()%>);
                ReportTypeScheduler(<%=schedulerSetting.getReportType()%>)
                SchedulerDuration(<%=schedulerSetting.getDuration()%>);
                <% if ( currentrole != null) {  
                %> 
                OperatorRoles(<%=currentrole.getRoleid()%>,'<%=currentrole.getName()%>%');
                <% } %> 
                
            </script>

        </form>
    </div>
</div>
<div class="modal-footer">
    <div id="editscheduler-result"></div>
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button class="btn btn-primary" onclick="editScheduler()" id="addEditSchedulerSubmitBut">Save Changes>></button>
</div>
<script>
    $(document).ready(function() {
        $("#_EditSchedulerContact0").select2({
            tags: [],
            dropdownCss: {display: 'none'},
            tokenSeparators: [","]
        });
    });
</script>
</div>