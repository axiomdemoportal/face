<%@page import="org.bouncycastle.util.encoders.Base64"%>
<%@page import="com.mollatech.axiom.common.utils.UtilityFunctions"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Webresource"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="java.util.Map"%>
<%@page import="org.json.JSONObject"%>
<%@include file="header.jsp" %>
<%    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();
    Webresource[] ws = new ResourceManagement().getAllResources(sessionId, _channelId);
    String _searchtext = request.getParameter("_searchtext");
    AuthUser user = new UserManagement().getUser(sessionId, _channelId, _searchtext);
    JSONObject jsono = null;
%>
<script src="./assets/js/ssomapping.js"></script>
<div class="container-fluid">
    <h1 class="text-success">SSO Application Management</h1>
    <!--<p>List of users in the system. Their two way authentication management can be managed from this interface.</p>-->
    <h3>Search Users</h3>  
    <form class="form-horizontal" id="ssoMappingForm" name="ssoMappingForm">
        <table class="table table-striped" id="table_main">
            <tr>
                <th>Sr. No</th>
                <th>App Name</th>
                <th>Field 1</th>
                <th>Field 2</th>
                <th>Field 3</th>
                <th>Field 4</th>
                <th>Field 5</th>
                <th>Field 6</th>
                <th>Field 7</th>
                <th>Field 8</th>
                <th>Field 9</th>
                <th>Field 10</th>

            </tr>
            <%if (ws != null) {
                    for (int j = 0; j < ws.length; j++) {
                        JSONObject json = new JSONObject(ws[j].getSsofields());
                        Map map = (Map) UtilityFunctions.deserialize(user.sso);
                        Object obj = map.get(new String(Base64.encode((ws[j].getResourceid() + ":" + ws[j].getResourcename()).getBytes())));
                        String[] arr = new String[0];
                        if (obj != null) {
                            jsono = new JSONObject(obj.toString());
                        }
            %>
            <tr>
                <td>
                    <%=j + 1%>
                    <input type="hidden" id="_hiddenApId<%=j%>" name="_hiddenApId<%=j%>" value="<%=ws[j].getResourceid() + ":" + ws[j].getResourcename()%>" class="span2">
                </td>
                <td><%=ws[j].getResourcename()%></td>
                <td><input type="text" id="_field1<%=j%>" name="_field1<%=j%>" <%if (json != null) {
                        if (json.get("_field1") != null) {
                            if (!json.get("_field1").toString().isEmpty()) {%>placeholder="<%=json.get("_field1")%>"<%} else {%>placeholder="value"<%}
                            } else {%>placeholder="value"<%}
                            } else {%>placeholder="value"<%}%> class="span1" <% if (jsono != null) {%>value="<%=jsono.get("_field1")%>"<%}%>></td>
                <td><input type="text" id="_field2<%=j%>" name="_field2<%=j%>" <%if (json != null) {
                        if (json.get("_field2") != null) {
                            if (!json.get("_field2").toString().isEmpty()) {%>placeholder="<%=json.get("_field2")%>"<%} else {%>placeholder="value"<%}
                            } else {%>placeholder="value"<%}
                            } else {%>placeholder="value"<%}%> class="span1" <% if (jsono != null) {%>value="<%=jsono.get("_field2")%>"<%}%>></td>
                <td><input type="text" id="_field3<%=j%>" name="_field3<%=j%>" <%if (json != null) {
                        if (json.get("_field3") != null) {
                            if (!json.get("_field3").toString().isEmpty()) {%>placeholder="<%=json.get("_field3")%>"<%} else {%>placeholder="value"<%}
                            } else {%>placeholder="value"<%}
                            } else {%>placeholder="value"<%}%> class="span1" <% if (jsono != null) {%>value="<%=jsono.get("_field3")%>"<%}%>></td>
                <td><input type="text" id="_field4<%=j%>" name="_field4<%=j%>" <%if (json != null) {
                        if (json.get("_field4") != null) {
                            if (!json.get("_field4").toString().isEmpty()) {%>placeholder="<%=json.get("_field4")%>"<%} else {%>placeholder="value"<%}
                            } else {%>placeholder="value"<%}
                            } else {%>placeholder="value"<%}%> class="span1" <% if (jsono != null) {%>value="<%=jsono.get("_field4")%>"<%}%>></td>
                <td><input type="text" id="_field5<%=j%>" name="_field5<%=j%>" <%if (json != null) {
                        if (json.get("_field5") != null) {
                            if (!json.get("_field5").toString().isEmpty()) {%>placeholder="<%=json.get("_field5")%>"<%} else {%>placeholder="value"<%}
                            } else {%>placeholder="value"<%}
                            } else {%>placeholder="value"<%}%> class="span1" <% if (jsono != null) {%>value="<%=jsono.get("_field5")%>"<%}%>></td>
                <td><input type="text" id="_field6<%=j%>" name="_field6<%=j%>" <%if (json != null) {
                        if (json.get("_field6") != null) {
                            if (!json.get("_field6").toString().isEmpty()) {%>placeholder="<%=json.get("_field6")%>"<%} else {%>placeholder="value"<%}
                            } else {%>placeholder="value"<%}
                            } else {%>placeholder="value"<%}%> class="span1" <% if (jsono != null) {%>value="<%=jsono.get("_field6")%>"<%}%>></td>
                <td><input type="text" id="_field7<%=j%>" name="_field7<%=j%>" <%if (json != null) {
                        if (json.get("_field7") != null) {
                            if (!json.get("_field7").toString().isEmpty()) {%>placeholder="<%=json.get("_field7")%>"<%} else {%>placeholder="value"<%}
                            } else {%>placeholder="value"<%}
                            } else {%>placeholder="value"<%}%> class="span1" <% if (jsono != null) {%>value="<%=jsono.get("_field7")%>"<%}%>></td>
                <td><input type="text" id="_field8<%=j%>" name="_field8<%=j%>" <%if (json != null) {
                        if (json.get("_field8") != null) {
                            if (!json.get("_field8").toString().isEmpty()) {%>placeholder="<%=json.get("_field8")%>"<%} else {%>placeholder="value"<%}
                            } else {%>placeholder="value"<%}
                            } else {%>placeholder="value"<%}%> class="span1" <% if (jsono != null) {%>value="<%=jsono.get("_field8")%>"<%}%>></td>
                <td><input type="text" id="_field9<%=j%>" name="_field9<%=j%>" <%if (json != null) {
                        if (json.get("_field9") != null) {
                            if (!json.get("_field9").toString().isEmpty()) {%>placeholder="<%=json.get("_field9")%>"<%} else {%>placeholder="value"<%}
                            } else {%>placeholder="value"<%}
                            } else {%>placeholder="value"<%}%> class="span1" <% if (jsono != null) {%>value="<%=jsono.get("_field9")%>"<%}%>></td>
                <td><input type="text" id="_field10<%=j%>" name="_field10<%=j%>" <%if (json != null) {
                        if (json.get("_field10") != null) {
                            if (!json.get("_field10").toString().isEmpty()) {%>placeholder="<%=json.get("_field10")%>"<%} else {%>placeholder="value"<%}
                            } else {%>placeholder="value"<%}
                            } else {%>placeholder="value"<%}%> class="span1" <% if (jsono != null) {%>value="<%=jsono.get("_field10")%>"<%}%>></td>
            </tr>
            <%}%>

            <%} else {%>
            <tr>
                <td>NA</td>
                <td>NA</td>
                <td>NA</td>
                <td>NA</td>
                <td>NA</td>
                <td>NA</td>
                <td>NA</td>
                <td>NA</td>
                <td>NA</td>
                <td>NA</td>
                <td>NA</td>
                <td>NA</td>
            </tr>
            <%}%>
        </table>
    </form>
    <%if (ws != null) {%>
    <button class="btn btn-primary" onclick="addSSOMapping('<%=_searchtext%>',<%=ws.length%>)" type="button">Edit Mapping </button>
    <%}%>
</div>