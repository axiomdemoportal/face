<%@include file="header.jsp" %>
<script src="./assets/js/jquery.js"></script>
<script src="./assets/js/dictum/bulkmsg.js"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/bootstrap-datepicker.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/css/datepicker.css">
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">

<div class="container-fluid">
    <h1 class="text-success">Summary Report</h1>
    <!--<h3>Make Your Own Report</h3>-->
    <br>
    <div class="tabbable">
        <ul class="nav nav-pills">
            <!--
            <li class="active"><a href="#traditional" data-toggle="tab">Traditional Reports</a></li>
            -->    
            <!--<li><a href="#social"   data-toggle="tab">Social Reports</a></li>
            <li><a href="#Push"   data-toggle="tab">Push Notification Reports</a></li>
            -->
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="traditional">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="control-group form-inline">

                            <div class="input-prepend">

                                <!--<div class="well">-->
                                <span class="add-on">From:</span>   
                                <div id="datetimepicker1" class="input-append date">
                                    <input id="startdate" name="startdate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                                <!--</div>-->
                            </div>
                            <div class="input-prepend">
                                <!--<div class="well">-->
                                <span class="add-on">till:</span>   
                                <div id="datetimepicker2" class="input-append date">
                                    <input id="enddate" name="enddate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                                <!--</div>-->
                            </div>
                            <!--<button class="btn btn-success" onclick="searchCostReport(1)" type="button">Generate Report</button>-->
                            <%if (oprObj.getRoleid() != 1) {//1 sysadmin
                                    if (accessObj != null && accessObj.viewMessageReport == true) {%>
                            <button class="btn btn-success" onclick="searchCostReport(1)" id="addUserButton">Generate Report</button>
                            <%} else {%>
                            <button class="btn btn-success " onclick="InvalidRequestMSG('msgreport')" type="button">Generate Report Now</button>
                            <%}
                            } else {%>
                            <button class="btn btn-success" onclick="searchCostReport(1)" id="addUserButton">Generate Report</button>
                            <%}%>
                        </div>

                        <div id="costreport" ><p><a href="#" class="btn btn-info" onclick="CostReportpdf(1)">Download Cost Report</a></p></div>
                        <div class="tab-pane active" id="msgcharts">
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">
                                        <div class="span4">
                                            <div id="MsgCostReportgraph" ></div>
                                            <!--Donut Chart-->
                                        </div>
                                        <div class="span7">
                                            <div id="MsgCostReportgraph1"></div>
                                            <!--Bar Chart-->   
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
            <div class="tab-pane" id="social">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="control-group form-inline">

                            <div class="input-prepend">

                                <!--<div class="well">-->
                                <span class="add-on">From:</span>   
                                <div id="datetimepicker21" class="input-append date">
                                    <input id="startdate1" name="startdate1" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                                <!--</div>-->
                            </div>
                            <div class="input-prepend">
                                <!--<div class="well">-->
                                <span class="add-on">till:</span>   
                                <div id="datetimepicker22" class="input-append date">
                                    <input id="enddate1" name="enddate1" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                                <!--</div>-->
                            </div>
                            <!--<button class="btn btn-success" onclick="searchCostReport(2)" type="button">Generate Report</button>-->
                            <%if (oprObj.getRoleid() != 1) {//1 sysadmin
                                    if (accessObj != null && accessObj.viewMessageReport == true) {%>
                            <button class="btn btn-success" onclick="searchCostReport(2)" id="addUserButton">Generate Report</button>
                            <%} else {%>
                            <button class="btn btn-success " onclick="InvalidRequestMSG('msgreport')" type="button">Generate Report Now</button>
                            <%}
                            } else {%>
                            <button class="btn btn-success" onclick="searchCostReport(2)" id="addUserButton">Generate Report</button>
                            <%}%>
                            <!--<a href="#SelectColumns" class="btn btn-success" data-toggle="modal">Generate Report</a>-->
                        </div>
                        <div id="costreportsocial" ><p><a href="#" class="btn btn-info" onclick="CostReportpdf(2)">Download Cost Report</a></p></div>
                        <div class="tab-pane active" id="msgcharts">
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">
                                        <div class="span4">
                                            <div id="MsgCostReportgraph2" ></div>
                                            <!--Donut Chart-->
                                        </div>
                                        <div class="span7">
                                            <div id="MsgCostReportgraph21"></div>
                                            <!--Bar Chart-->   
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="Push">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="control-group form-inline">

                            <div class="input-prepend">

                                <!--<div class="well">-->
                                <span class="add-on">From:</span>   
                                <div id="datetimepicker31" class="input-append date">
                                    <input id="startdate2" name="startdate2" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                                <!--</div>-->
                            </div>
                            <div class="input-prepend">
                                <!--<div class="well">-->
                                <span class="add-on">till:</span>   
                                <div id="datetimepicker32" class="input-append date">
                                    <input id="enddate2" name="enddate2" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                                <!--</div>-->
                            </div>
                            <button class="btn btn-success" onclick="searchCostReport(3)" type="button">Generate Report</button>
                            <!--<a href="#SelectColumns" class="btn btn-success" data-toggle="modal">Generate Report</a>-->
                        </div>
                        <div id="costreportpush" ><p><a href="#" class="btn btn-info" onclick="CostReportpdf(3)">Download Cost Report</a></p></div>
                        <div class="tab-pane active" id="msgcharts">
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">
                                        <div class="span4">
                                            <div id="MsgCostReportgraph3" ></div>
                                            <!--Donut Chart-->
                                        </div>
                                        <div class="span7">
                                            <div id="MsgCostReportgraph31"></div>
                                            <!--Bar Chart-->   
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            });
        });
        $(function () {
            $('#datetimepicker2').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            });
        });
        //        ChangeMediaType(0);
    </script>

    <script type="text/javascript">
        $(function () {
            $('#datetimepicker21').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            });
        });
        $(function () {
            $('#datetimepicker22').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            });
        });
//        ChangeMediaType(0);
    </script>

    <script type="text/javascript">
        $(function () {
            $('#datetimepicker31').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            });
        });
        $(function () {
            $('#datetimepicker32').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            });
        });
//        ChangeMediaType(0);
    </script>

    <script>
        document.getElementById("costreport").style.display = 'none';
        document.getElementById("costreportsocial").style.display = 'none';
        document.getElementById("costreportpush").style.display = 'none';

    </script>

</div>

<%@include file="footer.jsp" %>