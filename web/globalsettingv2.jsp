<%@page import="com.mollatech.axiom.nucleus.settings.pushsetting"%>
<%@page import="com.mollatech.axiom.nucleus.settings.faxlimitsetting"%>
<%@page import="com.mollatech.axiom.nucleus.settings.emailsetting"%>
<%@page import="com.mollatech.axiom.nucleus.settings.ussdsetting"%>
<%@page import="com.mollatech.axiom.nucleus.settings.smssetting"%>
<%@page import="com.mollatech.axiom.nucleus.settings.voicesetting"%>
<%@page import="java.util.Calendar"%>
<%@include file="header.jsp" %>
<%@page import="com.mollatech.axiom.nucleus.settings.GlobalChannelSettings"%>
<%@page import="com.mollatech.axiom.nucleus.settings.IPConfigFilter"%>
<%@page import="com.mollatech.axiom.nucleus.settings.ContentFilterSetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>

<%    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");

    SettingsManagement smng = new SettingsManagement();
    GlobalChannelSettings globalObj = (GlobalChannelSettings) smng.getSetting(sessionId, channel.getChannelid(), SettingsManagement.GlobalSettings, SettingsManagement.PREFERENCE_ONE);

    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
    Date channeldate = channel.getCreatedOn();
    Calendar c = Calendar.getInstance();
    c.setTime(channeldate);
    c.add(Calendar.DATE, 30);

    Date d = c.getTime();

    String strchannedate = sdf.format(d);


%>

<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<script src="./assets/js/globalsettingv2.js"></script>
<script src="./ckeditor/ckeditor.js"></script>
<script src="./ckeditor/config.js"></script>
<script src="./ckeditor/styles.js"></script>
<div class="container-fluid">
    <h1 class="text-success">Global Configuration</h1>
    <p>To facilitate deeper control on channel usability in form of messages sent out, other systems to call, outgoing messages to domain,IP and prefix allowed. You can also allocate quotas for various messaging media.</p>

    <div class="tabbable">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#primary" data-toggle="tab">Filter Settings</a></li>
            <li><a href="#secondary" data-toggle="tab">Limit and Cost Settings</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="primary">
                <div class="container-fluid">
                    <div class="row-fluid">

                        <!--                        <hr>-->
                        <form class="form-horizontal" id="channelcontentfilterform" name="channelcontentfilterform">
                            <h3 class="text-success">Content Filter</h3>
                            <div class="control-group">
                                <label class="control-label"  for="username">Blocked Keywords</label>
                                <div class="controls">
                                    <textarea name="_keywords" id="_keywords" style="width:100%">
                                        <%                            String[] strKeywords;

                                            int iStatus = 1;
                                            if (globalObj == null) {
                                                strKeywords = new String[3];
                                                strKeywords[0] = "dog";
                                                strKeywords[1] = "cat";
                                                strKeywords[2] = "eggs";
                                            } else {
                                                strKeywords = globalObj.content;

                                            }
                                            if (strKeywords != null)
                                                for (int j = 0; j < strKeywords.length; j++) {
                                        %>        
                                        <%=strKeywords[j]%>,
                                        <%
                                            }
                                        %>
                                    </textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <!--<label class="control-label"  for="username">Enable Filtering?</label>-->
                                <div class="controls">
                                    <div> 

                                        <select class="span2" name="_statusContent" id="_statusContent">
                                            <% if (globalObj != null) {
                                                    if (globalObj.contentstatus == 0) {%> 
                                            <option value="0" selected>Enable Filtering</option>
                                            <option value="1">Disable Filtering</option>
                                            <% } else {
                                            %>
                                            <option value="0" >Enable Filtering</option>
                                            <option value="1" selected>Disable Filtering</option>
                                            <%}
                                            } else {
                                            %>
                                            <option value="0" >Enable Filtering</option>
                                            <option value="1" selected>Disable Filtering</option>
                                            <%}%>

                                        </select>
                                        and alert if content is blocked as  
                                        <select class="span3" name="_alertOperator" id="_alertOperator">
                                            <% if (globalObj != null) {
                                                    if (globalObj.contentalertstatus == 0) { %> 
                                            <option value="0" selected>Yes, Alert Administrator</option>
                                            <option value="1">No, Do Not Alert Administrator</option>
                                            <% } else {
                                            %>
                                            <option value="0" >Yes, Alert Administrator</option>
                                            <option value="1" selected>No, Do Not Alert Administrator</option>
                                            <%
                                                }
                                            } else {
                                            %>
                                            <option value="0" >Yes, Alert Administrator</option>
                                            <option value="1" selected>No, Do Not Alert Administrator</option>
                                            <%}%>
                                        </select>
                                        <%//if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
                                        <button class="btn btn-primary" onclick="SaveChannelContentFilerList()" type="button" id="SaveChannelContentFilteringButton">Save Changes Now >> </button>
                                        <%//}%>
                                    </div>

                                </div>
                            </div>
                            <!--                            <div class="control-group">
                                                            <div class="controls">
                                                                <div id="save-global-content-filter-settings-result"></div>
                                                                <button class="btn btn-primary" onclick="SaveChannelContentFilerList()" type="button" id="SaveChannelContentFilteringButton">Save Changes Now >> </button>
                                                            </div>
                                                        </div>-->
                        </form>
                        <hr>
                        <form class="form-horizontal" id="channelipfilterform" name="channelipfilterform">
                            <h3 class="text-success">IP Filter</h3>
                            <div class="control-group">
                                <label class="control-label"  for="username">Allowed List:</label>
                                <div class="controls">
                                    <textarea name="_ip" id="_ip" style="width:100%">
                                        <%
                                            String[] strip;

                                            if (globalObj == null) {
                                                strip = new String[2];
                                                strip[0] = "127.0.0.1";
                                                strip[1] = "localhost";

                                            } else {
                                                strip = globalObj.IP;

                                            }
                                            if (strip != null)
                                                for (int j = 0; j < strip.length; j++) {
                                        %>        
                                        <%=strip[j]%>,
                                        <%
                                            }
                                        %>
                                    </textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <!--<label class="control-label"  for="username">Enabled IP Restriction?</label>-->
                                <div class="controls">
                                    <div> 

                                        <select class="span2" name="_statusIP" id="_statusIP">
                                            <%
                                                if (globalObj != null) {

                                                    if (globalObj.ipstatus == 0) {%> 
                                            <option value="0" selected >Enforce Restriction.</option>
                                            <option value="1">Allow All?</option>
                                            <% } else {
                                            %>
                                            <option value="0"  >Enforce Restriction.</option>
                                            <option value="1" selected>Allow All?</option>
                                            <%                            }
                                            } else {
                                            %><option value="0"  >Enforce Restriction.</option>
                                            <option value="1" selected>Allow All?</option>


                                            <%                                        }%>

                                        </select>
                                        and alert if ip is blocked as  
                                        <!--IP Blocked Action:--> 
                                        <select class="span3" name="_alertipOperator" id="_alertipOperator">
                                            <%if (globalObj != null) {
                                                    if (globalObj.ipalertstatus == 0) {%> 
                                            <option value="0" selected>Yes, Alert Administrator</option>
                                            <option value="1">No, Do Not Alert Administrator</option>
                                            <% } else {
                                            %>
                                            <option value="0" >Yes, Alert Administrator</option>
                                            <option value="1" selected>No, Do Not Alert Administrator</option>
                                            <%                            }
                                            } else {
                                            %>
                                            <option value="0" >Yes, Alert Administrator</option>
                                            <option value="1" selected>No, Do Not Alert Administrator</option>
                                            <%}%>

                                        </select>
                                        <%//if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
                                        <button class="btn btn-primary" onclick="SaveChannelIPFilerList()" type="button" id="SaveChannelIPFilteringButton">Save Changes Now >> </button>
                                        <%//}%>
                                    </div>
                                </div>
                            </div>
                            <!--                            <div class="control-group">
                                                            <div class="controls">
                                                                <div id="save-global-content-filter-settings-result"></div>
                                                                <button class="btn btn-primary" onclick="SaveChannelIPFilerList()" type="button" id="SaveChannelIPFilteringButton">Save Changes Now >> </button>
                                                            </div>
                                                        </div>-->
                        </form>
                        <hr>
                        <form class="form-horizontal" id="channelprefixfilterform" name="channelprefixfilterform">
                            <h3 class="text-success">Prefix Filter</h3>
                            <div class="control-group">
                                <label class="control-label"  for="username">Allowed List:</label>
                                <div class="controls">
                                    <textarea name="_prefix" id="_prefix" style="width:100%">
                                        <%
                                            String[] strprefix;
                                            if (globalObj == null) {
                                                strprefix = new String[1];
                                                strprefix[0] = "91";

                                            } else {
                                                strprefix = globalObj.prefix;

                                            }
                                            if (strprefix != null)
                                                for (int j = 0; j < strprefix.length; j++) {
                                        %>        
                                        <%=strprefix[j]%>,
                                        <%
                                            }
                                        %>
                                    </textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <!--<label class="control-label"  for="username">Enabled Prefix Restriction?</label>-->
                                <div class="controls">
                                    <div> 

                                        <select class="span2" name="_statusPrefix" id="_statusPrefix">
                                            <%
                                                if (globalObj != null) {
                                                    if (globalObj.prefixstatus == 0) {%> 
                                            <option value="0" selected >Enforce Restriction.</option>
                                            <option value="1">Allow All?</option>
                                            <% } else {
                                            %>
                                            <option value="0"  >Enforce Restriction.</option>
                                            <option value="1" selected>Allow All?</option>
                                            <%                            }
                                            } else {
                                            %><option value="0"  >Enforce Restriction.</option>
                                            <option value="1" selected>Allow All?</option>


                                            <%                                        }%>

                                        </select>
                                        and alert if prefix is blocked as  
                                        <select class="span3" name="_alertPrefixOperator" id="_alertPrefixOperator">
                                            <% if (globalObj != null) {
                                                    if (globalObj.prefixalertstatus == 0) {%> 
                                            <option value="0" selected>Yes, Alert Administrator</option>
                                            <option value="1">No, Do Not Alert Administrator</option>
                                            <% } else {
                                            %>
                                            <option value="0" >Yes, Alert Administrator</option>
                                            <option value="1" selected>No, Do Not Alert Administrator</option>
                                            <%                            }
                                            } else {
                                            %>
                                            <option value="0" >Yes, Alert Administrator</option>
                                            <option value="1" selected>No, Do Not Alert Administrator</option>
                                            <%}%>

                                        </select>
                                        <%//if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
                                        <button class="btn btn-primary" onclick="SaveChannelPrefixFilerList()" type="button" id="SaveChannelPrefixFilteringButton">Save Changes Now >> </button>
                                        <%//}%>
                                    </div>
                                </div>
                            </div>
                            <!--                            <div class="control-group">
                                                            <div class="controls">
                                                                <div id="save-global-content-filter-settings-result"></div>
                                                                <button class="btn btn-primary" onclick="SaveChannelPrefixFilerList()" type="button" id="SaveChannelPrefixFilteringButton">Save Changes Now >> </button>
                                                            </div>
                                                        </div>-->
                        </form>  
                        <hr>
                        <form class="form-horizontal" id="channeldomainfilterform" name="channeldomainfilterform">
                            <h3 class="text-success">Domain Filter</h3>
                            <div class="control-group">
                                <label class="control-label"  for="username">Allowed List:</label>
                                <div class="controls">
                                    <textarea name="_domain" id="_domain" style="width:100%">
                                        <%
                                            String[] strdomain;
                                            if (globalObj == null) {
                                                strdomain = new String[1];
                                                strdomain[0] = ".rediff";

                                            } else {
                                                strdomain = globalObj.domain;

                                            }
                                            if (strdomain != null)
                                                for (int j = 0; j < strdomain.length; j++) {
                                        %>        
                                        <%=strdomain[j]%>,
                                        <%
                                            }
                                        %>
                                    </textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"  for="username">Enabled Domain Restriction?</label>
                                <div class="controls">
                                    <div> 

                                        <select class="span2" name="_statusDomain" id="_statusDomain">
                                            <%
                                                if (globalObj != null) {
                                                    if (globalObj.domainstatus == 0) {%> 
                                            <option value="0" selected >Enforce Restriction.</option>
                                            <option value="1">Allow All?</option>
                                            <% } else {
                                            %>
                                            <option value="0"  >Enforce Restriction.</option>
                                            <option value="1" selected>Allow All?</option>
                                            <%                            }
                                            } else {
                                            %><option value="0"  >Enforce Restriction.</option>
                                            <option value="1" selected>Allow All?</option>


                                            <%                                        }%>

                                        </select>
                                        and alert if domain is blocked as  
                                        <select class="span3" name="_alertDomainOperator" id="_alertDomainOperator">
                                            <% if (globalObj != null) {
                                                    if (globalObj.domainalertstatus == 0) {%> 
                                            <option value="0" selected>Yes, Alert Administrator</option>
                                            <option value="1">No, Do Not Alert Administrator</option>
                                            <% } else {
                                            %>
                                            <option value="0" >Yes, Alert Administrator</option>
                                            <option value="1" selected>No, Do Not Alert Administrator</option>
                                            <%                            }
                                            } else {
                                            %>
                                            <option value="0" >Yes, Alert Administrator</option>
                                            <option value="1" selected>No, Do Not Alert Administrator</option>
                                            <%}%>

                                        </select>
                                        <%//if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
                                        <button class="btn btn-primary" onclick="SaveChannelDomainFilerList()" type="button" id="SaveChannelDomainFilteringButton">Save Changes Now >> </button>
                                        <%//}%>
                                    </div>
                                </div>
                            </div>
                            <!--                            <div class="control-group">
                                                            <div class="controls">
                                                                <div id="save-global-content-filter-settings-result"></div>
                                                                <button class="btn btn-primary" onclick="SaveChannelDomainFilerList()" type="button" id="SaveChannelDomainFilteringButton">Save Changes Now >> </button>
                                                            </div>
                                                        </div>-->
                        </form>  
                        <hr>
                        <form class="form-horizontal" id="channelcrsettingsform" name="channelcrsettingsform">
                            <input type="hidden" id="_weigtage" name="_weigtage">
                            <input type="hidden" id="_answersattempts" name="_answersattempts">
                            <h3 class="text-success">Challenge Response</h3>
                            <div class="control-group">
                                
                                <div class="controls">
                                    <div> 
                                        Minimum passing weightage as 
                                        <div class="btn-group">
                                            <button class="btn btn-small"><div id="_weigtage_div"></div></button>
                                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#" onclick="SetPercentagev2(75, '75 Percent')">75 percent</a></li>
                                                <li><a href="#" onclick="SetPercentagev2(80, '80 Percent')">80 percent</a></li>
                                                <li><a href="#" onclick="SetPercentagev2(90, '90 Percent')">90 percent</a></li>
                                                <li><a href="#" onclick="SetPercentagev2(100, '100 Percent')">100 percent</a></li>                                
                                            </ul>
                                        </div>
                                        with 
                                        <div class="btn-group">
                                            <button class="btn btn-small"><div id="_answersattempts_div"></div></button>
                                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#" onclick="setAttemptsv2(3, '3 attempts')">3 attempts</a></li>
                                                <li><a href="#" onclick="setAttemptsv2(5, '5 attempts')">5 attempts</a></li>
                                                <li><a href="#" onclick="setAttemptsv2(7, '7 attempts')">7 attempts</a></li>

                                            </ul>
                                        </div>
                                        wrong Attempts 
                                        <%//if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
                                        <button class="btn btn-primary" onclick="SetPercentagesInGlobalSettings()" type="button" id="setPercentage">Save Changes Now >> </button>
                                        <%//}%>
                                    </div>
                                </div>
                            </div>
                            <!--                            <div class="control-group">
                                                            <div class="controls">
                                                                <div id="save-global-content-filter-settings-result"></div>
                                                                <button class="btn btn-primary" onclick="SetPercentagesInGlobalSettings()" type="button" id="setPercentage">Save Changes Now >> </button>
                                                            </div>
                                                        </div>-->
                            <script language="javascript" type="text/javascript">
                                loadglobalsetting(13);
                            </script>
                        </form>   

                    </div>
                </div>
            </div>
            <div class="tab-pane" id="secondary">
                <!--<h3 class="text-success">Gateway Configuration</h3>-->
                <div class="row-fluid">
                    <div class="tabbable">
                        <ul class="nav nav-pills">
                            <li class="active"><a href="#SMS" data-toggle="tab">SMS</a></li>
                            <li><a href="#VOICE" data-toggle="tab">VOICE</a></li>
                            <li><a href="#USSD" data-toggle="tab">USSD</a></li>
                            <li><a href="#EMAIL" data-toggle="tab">EMAIL</a></li>
                            <li><a href="#FAX" data-toggle="tab">FAX</a></li>
                            <li><a href="#PUSH"  hidden="true" data-toggle="tab">PUSH</a></li>
                        </ul>
                    </div>   

                    <div class="tab-content">
                        <div class="tab-pane active" id="SMS">
                            <div class="row-fluid">
                                <%
                                    Date smsendDate = null;
                                    Date smsstartDate = null;
                                    if (globalObj != null) {
                                        if (globalObj.smssettingobj != null) {
                                            //    globalObj.smssettingobj = new smssetting();
                                            smsendDate = globalObj.smssettingobj.smsenddate;
                                            if (globalObj.smssettingobj.repeat == globalObj.MONTH) {
                                                c.setTime(smsendDate);
                                                c.add(Calendar.MONTH, -1);
                                                smsstartDate = c.getTime();
                                            } else if (globalObj.smssettingobj.repeat == globalObj.THREE_MONTH) {
                                                c.setTime(smsendDate);
                                                c.add(Calendar.MONTH, -3);
                                                smsstartDate = c.getTime();
                                            } else if (globalObj.smssettingobj.repeat == globalObj.SIX_MONTH) {
                                                c.setTime(smsendDate);
                                                c.add(Calendar.MONTH, -6);
                                                smsstartDate = c.getTime();
                                            }
                                        }
                                    }

                                %>

                                <!--                                <div id="legend">
                                                                    <legend class="">Started on <%=smsstartDate%> and ends on <%=smsendDate%></legend>
                                                                </div>-->

                                <form class="form-horizontal" id="SMSForm" name="SMSForm">
                                    <!--sms-->
                                    <input type="hidden" id="_smsStatus" name="_smsStatus">
                                    <input type="hidden" id="_smsAlertStatus" name="_smsAlertStatus">
                                    <input type="hidden" id="_smsRepeatStatus" name="_smsRepeatStatus">
                                    <input type="hidden" id="_smsRepeatDuration" name="_smsRepeatDuration">
                                    <!--smsspeed-->
                                    <input type="hidden" id="_slowPaceSMS" name="_slowPaceSMS">
                                    <input type="hidden" id="_normalPaceSMS" name="_normalPaceSMS">
                                    <input type="hidden" id="_fastPaceSMS" name="_fastPaceSMS">
                                    <input type="hidden" id="_hyperPaceSMS" name="_hyperPaceSMS">
                                    <!--restriction-->
                                    <input type="hidden" id="_dayRestrictionSMS" name="_dayRestrictionSMS">
                                    <input type="hidden" id="_timeFromRestrictionSMS" name="_timeFromRestrictionSMS">

                                    <input type="hidden" id="_timeToRestrictionSMS" name="_timeToRestrictionSMS">
                                    <input type="hidden" id="_smsFooterStatus" name="_smsFooterStatus">
                                    <fieldset>

                                        <div class="control-group">
                                            <label class="control-label"  for="username">Disable Quota</label>
                                            <div class="controls">
                                                <div>
                                                    <div class="btn-group">
                                                        <button class="btn btn-small"><div id="_smsStatusDIV"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeLimitStatusSMS(1)">Mark as Active?</a></li>
                                                            <li><a href="#" onclick="ChangeLimitStatusSMS(0)">Mark as Suspended?</a></li>
                                                        </ul>
                                                    </div>
                                                    , if enabled use Day Restriction as 
                                                    <div class="btn-group">
                                                        <button class="btn btn-small"><div id="_dayRestrictionSMS_div"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeDayRestrictionSMS(1, 'Week days only')">Week days only</a></li>
                                                            <li><a href="#" onclick="ChangeDayRestrictionSMS(2, 'Whole Week (including Weekend)')">Whole Week (including Weekend)</a></li>
                                                        </ul>
                                                    </div>
                                                    with time range as
                                                    <div class="btn-group">
                                                        <button class="btn btn-small"><div id="_timeFromRestrictionSMS_div"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeTimeRestrictionSMS(1, 7, '7AM')">7am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionSMS(1, 8, '8AM')">8am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionSMS(1, 9, '9AM')">9am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionSMS(1, 10, '10AM')">10am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionSMS(1, 11, '11AM')">11am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionSMS(1, 0, 'Any')">Any</a></li>
                                                        </ul>                        
                                                    </div>
                                                    to 
                                                    <div class="btn-group">
                                                        <button class="btn btn-small"><div id="_timeToRestrictionSMS_div"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeTimeRestrictionSMS(2, 12, '12PM')">12pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionSMS(2, 13, '1PM')">1pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionSMS(2, 14, '2PM')">2pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionSMS(2, 15, '3PM')">3pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionSMS(2, 16, '4PM')">4pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionSMS(2, 17, '5PM')">5pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionSMS(2, 18, '6PM')">6pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionSMS(2, 19, '7PM')">7pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionSMS(2, 24, 'Any')">Any</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!--<hr>-->
                                        <div class="control-group">
                                            <label class="control-label"  for="username">Maximum Quota</label>
                                            <div class="controls">
                                                <input type="text" id="_smslimit" name="_smslimit" placeholder="SMS Limit" class="span1">
                                                ,with threshold limit 

                                                <input type="text" id="_smsThresholdlimit" name="_smsThresholdlimit" placeholder="SMS Cost" class="span1">
                                                (alert operator after threshold limit)
                                                <div class="btn-group">

                                                    <button class="btn btn-small"><div id="_smsAlertStatusDIV"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="ChangeLimitAlertStatusSMS(1)">Enable</a></li>
                                                        <li><a href="#" onclick="ChangeLimitAlertStatusSMS(0)">Disable</a></li>
                                                    </ul>
                                                </div>
                                                ,and cost
                                                <input type="text" id="_smscost" name="_smscost" placeholder="SMS Cost" class="span1"> for one month Duration
<!--                                                per message for duration of 
                                                <div class="btn-group">

                                                    <button class="btn btn-small"><div id="_smsRepeatStatusDIV"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="ChangeRepeatStatusSMS(1)">1 Month</a></li>
                                                        <li><a href="#" onclick="ChangeRepeatStatusSMS(2)">3 Months</a></li>
                                                        <li><a href="#" onclick="ChangeRepeatStatusSMS(3)">6 Months</a></li>
                                                    </ul>
                                                </div>-->
                                            </div>

                                        </div>

                                        <hr>

                                        <div class="control-group">
                                            <label class="control-label"  for="username">Speed Defination</label>

                                            <div class="controls">
                                                Slow
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_slowPaceSMS_div"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="SetPACESMS(1, 1, '#_slowPaceSMS_div', '1 message/second')">1 message/second</a></li>
                                                        <li><a href="#" onclick="SetPACESMS(1, 3, '#_slowPaceSMS_div', '3 messages/second')">3 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACESMS(1, 5, '#_slowPaceSMS_div', '5 messages/second')">5 messages/second</a></li>
                                                    </ul>
                                                </div>

                                                , Normal
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_normalPaceSMS_div"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="SetPACESMS(2, 6, '#_normalPaceSMS_div', '6 messages/second')">6 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACESMS(2, 8, '#_normalPaceSMS_div', '8 messages/second')">8 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACESMS(2, 10, '#_normalPaceSMS_div', '10 messages/second')">10 messages/second</a></li>
                                                    </ul>
                                                </div>
                                                , Fast
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_fastPaceSMS_div"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="SetPACESMS(3, 15, '#_fastPaceSMS_div', '15 messages/second')">15 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACESMS(3, 20, '#_fastPaceSMS_div', '20 messages/second')">20 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACESMS(3, 25, '#_fastPaceSMS_div', '25 messages/second')">25 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACESMS(3, 30, '#_fastPaceSMS_div', '30 messages/second')">30 messages/second</a></li>                                
                                                    </ul>
                                                </div>

                                                ,and Hyper: 
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_hyperPaceSMS_div"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="SetPACESMS(4, 35, '#_hyperPaceSMS_div', '35 messages/second')">35 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACESMS(4, 50, '#_hyperPaceSMS_div', '50 messages/second')">50 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACESMS(4, 75, '#_hyperPaceSMS_div', '75 messages/second')">75 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACESMS(4, 100, '#_hyperPaceSMS_div', '100 messages/second')">100 messages/second</a></li>                                
                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                        <hr>

                                        <div class="control-group">
                                            <label class="control-label"  for="username">Message Footer</label>
                                            <div class="controls">
                                                <input type="text" id="_smsFooter" name="_smsFooter" placeholder="Footer" class="input-xlarge">
                                                with footer status is
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_smsFooterStatusDIV"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="ChangeFooterStatusSMS(1)">Mark as Active?</a></li>
                                                        <li><a href="#" onclick="ChangeFooterStatusSMS(0)">Mark as Suspended?</a></li>
                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                        <!--                                        <div class="control-group">
                                                                                    <label class="control-label"  for="username">Message Footer </label>
                                                                                    <div class="controls">
                                                                                        <input type="text" id="_smsFooter" name="_smsFooter" placeholder="Footer" class="input-xlarge">
                                        
                                                                                    </div>
                                                                                </div>-->
                                        <hr>
                                    </fieldset>
                                    <%//if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
                                    <div class="control-group">
                                        <div class="controls">
                                            <div id="save-global-content-filter-settings-result"></div>
                                            <button class="btn btn-primary" onclick="SaveChannelSMSSetting()" type="button" id="SaveChannelSMSSettingButton">Save Changes Now >> </button>
                                        </div>
                                    </div>
                                    <%//}%>

                                    <script language="javascript" type="text/javascript">
                                        loadglobalsetting(4);

                                    </script>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane" id="VOICE">
                            <div class="row-fluid">
                                <%
                                    Date voiceendDate = null;
                                    Date voicestartDate = null;
                                    if (globalObj != null) {
                                        if (globalObj.voicesettingobj != null) {
                                            //    globalObj.smssettingobj = new smssetting();
                                            voiceendDate = globalObj.voicesettingobj.voiceenddate;
                                            if (globalObj.voicesettingobj.repeat == globalObj.MONTH) {
                                                c.setTime(voiceendDate);
                                                c.add(Calendar.MONTH, -1);
                                                voicestartDate = c.getTime();
                                            } else if (globalObj.voicesettingobj.repeat == globalObj.THREE_MONTH) {
                                                c.setTime(voiceendDate);
                                                c.add(Calendar.MONTH, -3);
                                                voicestartDate = c.getTime();
                                            } else if (globalObj.voicesettingobj.repeat == globalObj.SIX_MONTH) {
                                                c.setTime(voiceendDate);
                                                c.add(Calendar.MONTH, -6);
                                                voicestartDate = c.getTime();
                                            }
                                        }
                                    }

                                %>

                                <!--                                <div id="legend">
                                                                    <legend class=""> Started on <%=voicestartDate%> and ends on <%=voiceendDate%></legend>
                                                                </div>-->
                                <!--<hr>-->
                                <form class="form-horizontal" id="VOICEForm" name="VOICEForm">
                                    <!--voice-->
                                    <input type="hidden" id="_voiceStatus" name="_voiceStatus">
                                    <input type="hidden" id="_voiceAlertStatus" name="_voiceAlertStatus">
                                    <input type="hidden" id="_voiceRepeatStatus" name="_voiceRepeatStatus">
                                    <input type="hidden" id="_voiceRepeatDuration" name="_voiceRepeatDuration">
                                    <!--voicespeed-->
                                    <input type="hidden" id="_slowPaceVOICE" name="_slowPaceVOICE">
                                    <input type="hidden" id="_normalPaceVOICE" name="_normalPaceVOICE">
                                    <input type="hidden" id="_fastPaceVOICE" name="_fastPaceVOICE">
                                    <input type="hidden" id="_hyperPaceVOICE" name="_hyperPaceVOICE">
                                    <!--restriction-->
                                    <input type="hidden" id="_dayRestrictionVOICE" name="_dayRestrictionVOICE">
                                    <input type="hidden" id="_timeFromRestrictionVOICE" name="_timeFromRestrictionVOICE">
                                    <input type="hidden" id="_timeToRestrictionVOICE" name="_timeToRestrictionVOICE">
                                    <input type="hidden" id="_voiceFooterStatus" name="_voiceFooterStatus">

                                    <fieldset>
                                        <div class="control-group">
                                            <label class="control-label"  for="username">Disable Quota</label>
                                            <div class="controls">
                                                <div>

                                                    <div class="btn-group">

                                                        <button class="btn btn-small"><div id="_voiceStatusDIV"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeLimitStatusVOICE(1)">Mark as Active?</a></li>
                                                            <li><a href="#" onclick="ChangeLimitStatusVOICE(0)">Mark as Suspended?</a></li>
                                                        </ul>
                                                    </div>
                                                    , if enabled use Day Restriction as
                                                    <div class="btn-group">
                                                        <button class="btn btn-small"><div id="_dayRestrictionVOICE_div"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeDayRestrictionVOICE(1, 'Week days only')">Week days only</a></li>
                                                            <li><a href="#" onclick="ChangeDayRestrictionVOICE(2, 'Whole Week (including Weekend)')">Whole Week (including Weekend)</a></li>
                                                        </ul>
                                                    </div>
                                                    with time range as  
                                                    <div class="btn-group">
                                                        <button class="btn btn-small"><div id="_timeFromRestrictionVOICE_div"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeTimeRestrictionVOICE(1, 7, '7AM')">7am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionVOICE(1, 8, '8AM')">8am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionVOICE(1, 9, '9AM')">9am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionVOICE(1, 10, '10AM')">10am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionVOICE(1, 11, '11AM')">11am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionVOICE(1, 0, 'Any')">Any</a></li>
                                                        </ul>                        
                                                    </div>
                                                    to 
                                                    <div class="btn-group">
                                                        <button class="btn btn-small"><div id="_timeToRestrictionVOICE_div"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeTimeRestrictionVOICE(2, 12, '12PM')">12pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionVOICE(2, 13, '1PM')">1pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionVOICE(2, 14, '2PM')">2pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionVOICE(2, 15, '3PM')">3pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionVOICE(2, 16, '4PM')">4pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionVOICE(2, 17, '5PM')">5pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionVOICE(2, 18, '6PM')">6pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionVOICE(2, 19, '7PM')">7pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionVOICE(2, 24, 'Any')">Any</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label"  for="username">Maximum Quota</label>
                                            <div class="controls">
                                                <input type="text" id="_voicelimit" name="_voicelimit" placeholder="Voice Limit" class="span1">
                                                ,with threshold limit
                                                <input type="text" id="_voiceThresholdlimit" name="_voiceThresholdlimit" placeholder="SMS Cost" class="span1">
                                                (alert operator after threshold limit)
                                                <div class="btn-group">

                                                    <button class="btn btn-small"><div id="_voiceAlertStatusDIV"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="ChangeLimitAlertStatusVOICE(1)">Enable</a></li>
                                                        <li><a href="#" onclick="ChangeLimitAlertStatusVOICE(0)">Disable</a></li>
                                                    </ul>
                                                </div>
                                                ,and cost
                                                <input type="text" id="_voicecost" name="_voicecost" placeholder="VOICE Cost" class="span1">
                                                for one month Duration
<!--                                                per message for duration of
                                                <div class="btn-group">

                                                    <button class="btn btn-small"><div id="_voiceRepeatStatusDIV"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="ChangeRepeatStatusVOICE(1)">1 Month</a></li>
                                                        <li><a href="#" onclick="ChangeRepeatStatusVOICE(2)">3 Months</a></li>
                                                        <li><a href="#" onclick="ChangeRepeatStatusVOICE(3)">6 Months</a></li>
                                                    </ul>
                                                </div>-->


                                            </div>
                                        </div>
                                        <hr>


                                        <div class="control-group">
                                            <label class="control-label"  for="username">Speed Defination</label>
                                            <div class="controls">
                                                Slow
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_slowPaceVOICE_div"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="SetPACEVOICE(1, 1, '#_slowPaceVOICE_div', '1 message/second')">1 message/second</a></li>
                                                        <li><a href="#" onclick="SetPACEVOICE(1, 3, '#_slowPaceVOICE_div', '3 messages/second')">3 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEVOICE(1, 5, '#_slowPaceVOICE_div', '5 messages/second')">5 messages/second</a></li>
                                                    </ul>
                                                </div>

                                                , Normal
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_normalPaceVOICE_div"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="SetPACEVOICE(2, 6, '#_normalPaceVOICE_div', '6 messages/second')">6 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEVOICE(2, 8, '#_normalPaceVOICE_div', '8 messages/second')">8 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEVOICE(2, 10, '#_normalPaceVOICE_div', '10 messages/second')">10 messages/second</a></li>
                                                    </ul>
                                                </div>
                                                , Fast
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_fastPaceVOICE_div"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="SetPACEVOICE(3, 15, '#_fastPaceVOICE_div', '15 messages/second')">15 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEVOICE(3, 20, '#_fastPaceVOICE_div', '20 messages/second')">20 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEVOICE(3, 25, '#_fastPaceVOICE_div', '25 messages/second')">25 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEVOICE(3, 30, '#_fastPaceVOICE_div', '30 messages/second')">30 messages/second</a></li>                                
                                                    </ul>
                                                </div>
                                                ,and Hyper
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_hyperPaceVOICE_div"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="SetPACEVOICE(4, 35, '#_hyperPaceVOICE_div', '35 messages/second')">35 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEVOICE(4, 50, '#_hyperPaceVOICE_div', '50 messages/second')">50 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEVOICE(4, 75, '#_hyperPaceVOICE_div', '75 messages/second')">75 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEVOICE(4, 100, '#_hyperPaceVOICE_div', '100 messages/second')">100 messages/second</a></li>                                
                                                    </ul>
                                                </div>

                                            </div>
                                        </div>

                                        <hr>
                                        <div class="control-group">
                                            <label class="control-label"  for="username">Message Footer</label>


                                            <div class="controls">
                                                <input type="text" id="_voiceFooter" name="_voiceFooter" placeholder="Footer" class="input-xlarge">
                                                with footer status is
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_voiceFooterStatusDIV"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="ChangeFooterStatusVOICE(1)">Mark as Active?</a></li>
                                                        <li><a href="#" onclick="ChangeFooterStatusVOICE(0)">Mark as Suspended?</a></li>
                                                    </ul>
                                                </div>

                                            </div>
                                        </div> 

                                        <!--                                        <div class="control-group">
                                                                                    <label class="control-label"  for="username">Message Footer </label>
                                                                                    <div class="controls">
                                                                                        <input type="text" id="_voiceFooter" name="_voiceFooter" placeholder="Footer" class="input-xlarge">
                                        
                                                                                    </div>
                                        
                                        
                                                                                </div>-->
                                        <hr>
                                    </fieldset>
                                    <%//if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
                                    <div class="control-group">
                                        <div class="controls">
                                            <div id="save-global-content-filter-settings-result"></div>
                                            <button class="btn btn-primary" onclick="SaveChannelVOICESetting()" type="button" id="SaveChannelVOICESettingButton">Save Changes Now >> </button>
                                        </div>
                                    </div>
                                    <%//}%>

                                    <script language="javascript" type="text/javascript">
                                        loadglobalsetting(5);
                                    </script>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane" id="USSD">
                            <div class="row-fluid">
                                <%
                                    Date ussdendDate = null;
                                    Date ussdstartDate = null;
                                    if (globalObj != null) {
                                        if (globalObj.ussdsettingobj != null) {
                                            //    globalObj.smssettingobj = new smssetting();
                                            ussdendDate = globalObj.ussdsettingobj.ussdenddate;
                                            if (globalObj.ussdsettingobj.repeat == globalObj.MONTH) {
                                                c.setTime(smsendDate);
                                                c.add(Calendar.MONTH, -1);
                                                ussdstartDate = c.getTime();
                                            } else if (globalObj.ussdsettingobj.repeat == globalObj.THREE_MONTH) {
                                                c.setTime(smsendDate);
                                                c.add(Calendar.MONTH, -3);
                                                ussdstartDate = c.getTime();
                                            } else if (globalObj.ussdsettingobj.repeat == globalObj.SIX_MONTH) {
                                                c.setTime(smsendDate);
                                                c.add(Calendar.MONTH, -6);
                                                ussdstartDate = c.getTime();
                                            }
                                        }
                                    }

                                %>

                                <!--                                <div id="legend">
                                                                    <legend class=""> Started on <%=ussdstartDate%> and ends on <%=ussdendDate%></legend>
                                                                </div>-->
                                <!--                             <hr>-->
                                <form class="form-horizontal" id="USSDForm" name="USSDForm">

                                    <fieldset>
                                        <input type="hidden" id="_ussdStatus" name="_ussdStatus">
                                        <input type="hidden" id="_ussdAlertStatus" name="_ussdAlertStatus">
                                        <input type="hidden" id="_ussdRepeatStatus" name="_ussdRepeatStatus">
                                        <input type="hidden" id="_ussdRepeatDuration" name="_ussdRepeatDuration">
                                        <!--voicespeed-->
                                        <input type="hidden" id="_slowPaceUSSD" name="_slowPaceUSSD">
                                        <input type="hidden" id="_normalPaceUSSD" name="_normalPaceUSSD">
                                        <input type="hidden" id="_fastPaceUSSD" name="_fastPaceUSSD">
                                        <input type="hidden" id="_hyperPaceUSSD" name="_hyperPaceUSSD">
                                        <!--restriction-->
                                        <input type="hidden" id="_dayRestrictionUSSD" name="_dayRestrictionUSSD">
                                        <input type="hidden" id="_timeFromRestrictionUSSD" name="_timeFromRestrictionUSSD">
                                        <input type="hidden" id="_timeToRestrictionUSSD" name="_timeToRestrictionUSSD">
                                        <input type="hidden" id="_ussdFooterStatus" name="_ussdFooterStatus">

                                        <div class="control-group">
                                            <label class="control-label"  for="username">Disable Quota</label>
                                            <div class="controls">
                                                <div>
                                                    <div class="btn-group">

                                                        <button class="btn btn-small"><div id="_ussdStatusDIV"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeLimitStatusUSSD(1)">Mark as Active?</a></li>
                                                            <li><a href="#" onclick="ChangeLimitStatusUSSD(0)">Mark as Suspended?</a></li>
                                                        </ul>
                                                    </div>
                                                    , if enabled use Day Restriction as 
                                                    <div class="btn-group">
                                                        <button class="btn btn-small"><div id="_dayRestrictionUSSD_div"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeDayRestrictionUSSD(1, 'Week days only')">Week days only</a></li>
                                                            <li><a href="#" onclick="ChangeDayRestrictionUSSD(2, 'Whole Week (including Weekend)')">Whole Week (including Weekend)</a></li>
                                                        </ul>
                                                    </div>
                                                    with time range as
                                                    <div class="btn-group">
                                                        <button class="btn btn-small"><div id="_timeFromRestrictionUSSD_div"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeTimeRestrictionUSSD(1, 7, '7AM')">7am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionUSSD(1, 8, '8AM')">8am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionUSSD(1, 9, '9AM')">9am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionUSSD(1, 10, '10AM')">10am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionUSSD(1, 11, '11AM')">11am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionUSSD(1, 0, 'Any')">Any</a></li>
                                                        </ul>                        
                                                    </div>
                                                    to 
                                                    <div class="btn-group">
                                                        <button class="btn btn-small"><div id="_timeToRestrictionUSSD_div"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeTimeRestrictionUSSD(2, 12, '12PM')">12pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionUSSD(2, 13, '1PM')">1pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionUSSD(2, 14, '2PM')">2pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionUSSD(2, 15, '3PM')">3pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionUSSD(2, 16, '4PM')">4pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionUSSD(2, 17, '5PM')">5pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionUSSD(2, 18, '6PM')">6pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionUSSD(2, 19, '7PM')">7pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionUSSD(2, 24, 'Any')">Any</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label"  for="username">Maximum Quota</label>
                                            <div class="controls">
                                                <input type="text" id="_ussdlimit" name="_ussdlimit" placeholder="USSD Limit" class="span1">
                                                ,with threshold limit 
                                                <input type="text" id="_ussdThresholdlimit" name="_ussdThresholdlimit" placeholder="SMS Cost" class="span1">
                                                (alert operator after threshold limit)
                                                <div class="btn-group">

                                                    <button class="btn btn-small"><div id="_ussdAlertStatusDIV"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="ChangeLimitAlertStatusUSSD(1)">Enable</a></li>
                                                        <li><a href="#" onclick="ChangeLimitAlertStatusUSSD(0)">Disable</a></li>
                                                    </ul>
                                                </div>
                                                ,and cost
                                                <input type="text" id="_ussdcost" name="_ussdcost" placeholder="USSD Cost" class="span1">
                                                for one month Duration
<!--                                                per message for duration of-->
<!--                                                <div class="btn-group">

                                                    <button class="btn btn-small"><div id="_ussdRepeatStatusDIV"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="ChangeRepeatStatusUSSD(1)">1 Month</a></li>
                                                        <li><a href="#" onclick="ChangeRepeatStatusUSSD(2)">3 Months</a></li>
                                                        <li><a href="#" onclick="ChangeRepeatStatusUSSD(3)">6 Months</a></li>
                                                    </ul>
                                                </div>-->

                                            </div>
                                        </div>

                                        <hr>
                                        <!--</div>-->

                                        <div class="control-group">
                                            <label class="control-label"  for="username">Speed Defination</label>
                                            <div class="controls">
                                                Slow 
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_slowPaceUSSD_div"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="SetPACEUSSD(1, 1, '#_slowPaceUSSD_div', '1 message/second')">1 message/second</a></li>
                                                        <li><a href="#" onclick="SetPACEUSSD(1, 3, '#_slowPaceUSSD_div', '3 messages/second')">3 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEUSSD(1, 5, '#_slowPaceUSSD_div', '5 messages/second')">5 messages/second</a></li>
                                                    </ul>
                                                </div>

                                                , Normal:
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_normalPaceUSSD_div"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="SetPACEUSSD(2, 6, '#_normalPaceUSSD_div', '6 messages/second')">6 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEUSSD(2, 8, '#_normalPaceUSSD_div', '8 messages/second')">8 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEUSSD(2, 10, '#_normalPaceUSSD_div', '10 messages/second')">10 messages/second</a></li>
                                                    </ul>
                                                </div>
                                                Fast
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_fastPaceUSSD_div"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="SetPACEUSSD(3, 15, '#_fastPaceUSSD_div', '15 messages/second')">15 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEUSSD(3, 20, '#_fastPaceUSSD_div', '20 messages/second')">20 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEUSSD(3, 25, '#_fastPaceUSSD_div', '25 messages/second')">25 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEUSSD(3, 30, '#_fastPaceUSSD_div', '30 messages/second')">30 messages/second</a></li>                                
                                                    </ul>
                                                </div>
                                                ,and Hyper 
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_hyperPaceUSSD_div"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="SetPACEUSSD(4, 35, '#_hyperPaceUSSD_div', '35 messages/second')">35 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEUSSD(4, 50, '#_hyperPaceUSSD_div', '50 messages/second')">50 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEUSSD(4, 75, '#_hyperPaceUSSD_div', '75 messages/second')">75 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEUSSD(4, 100, '#_hyperPaceUSSD_div', '100 messages/second')">100 messages/second</a></li>                                
                                                    </ul>
                                                </div>

                                            </div>
                                        </div>

                                        <hr>
                                        <div class="control-group">
                                            <label class="control-label"  for="username">Message Footer</label>
                                            <div class="controls">
                                                <input type="text" id="_ussdFooter" name="_ussdFooter" placeholder="Footer" class="input-xlarge">
                                                with footer status is
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_ussdFooterStatusDIV"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="ChangeFooterStatusUSSD(1)">Mark as Active?</a></li>
                                                        <li><a href="#" onclick="ChangeFooterStatusUSSD(0)">Mark as Suspended?</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div> 




                                    </fieldset>
                                    <%//if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
                                    <div class="control-group">
                                        <div class="controls">
                                            <div id="save-global-content-filter-settings-result"></div>
                                            <button class="btn btn-primary" onclick="SaveChannelUSSDSetting()" type="button" id="SaveChannelUSSDSettingButton">Save Changes Now >> </button>
                                        </div>
                                    </div>
                                    <%//}%>
                                    <script language="javascript" type="text/javascript">
                                        loadglobalsetting(6);

                                    </script>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane" id="EMAIL">
                            <div class="row-fluid">
                                <%
                                    Date emailendDate = null;
                                    Date emailstartDate = null;
                                    if (globalObj != null) {
                                        if (globalObj.emailsettingobj != null) {
                                            //    globalObj.smssettingobj = new smssetting();
                                            emailendDate = globalObj.emailsettingobj.emailenddate;
                                            if (globalObj.emailsettingobj.repeat == globalObj.MONTH) {
                                                c.setTime(emailendDate);
                                                c.add(Calendar.MONTH, -1);
                                                emailstartDate = c.getTime();
                                            } else if (globalObj.emailsettingobj.repeat == globalObj.THREE_MONTH) {
                                                c.setTime(emailendDate);
                                                c.add(Calendar.MONTH, -3);
                                                emailstartDate = c.getTime();
                                            } else if (globalObj.emailsettingobj.repeat == globalObj.SIX_MONTH) {
                                                c.setTime(emailendDate);
                                                c.add(Calendar.MONTH, -6);
                                                emailstartDate = c.getTime();
                                            }
                                        }
                                    }

                                %>

                                <!--                                <div id="legend">
                                                                    <legend class=""> Started on <%=emailstartDate%> and ends on <%=emailendDate%></legend>
                                                                </div>-->
                                <!--<hr>-->
                                <form class="form-horizontal" id="EMAILForm" name="EMAILForm">
                                    <fieldset>
                                        <input type="hidden" id="_emailStatus" name="_emailStatus">
                                        <input type="hidden" id="_emailAlertStatus" name="_emailAlertStatus">
                                        <input type="hidden" id="_emailRepeatStatus" name="_emailRepeatStatus">
                                        <input type="hidden" id="_emailRepeatDuration" name="_emailRepeatDuration">
                                        <!--voicespeed-->
                                        <input type="hidden" id="_slowPaceEMAIL" name="_slowPaceEMAIL">
                                        <input type="hidden" id="_normalPaceEMAIL" name="_normalPaceEMAIL">
                                        <input type="hidden" id="_fastPaceEMAIL" name="_fastPaceEMAIL">
                                        <input type="hidden" id="_hyperPaceEMAIL" name="_hyperPaceEMAIL">
                                        <!--restriction-->
                                        <input type="hidden" id="_dayRestrictionEMAIL" name="_dayRestrictionEMAIL">
                                        <input type="hidden" id="_timeFromRestrictionEMAIL" name="_timeFromRestrictionEMAIL">
                                        <input type="hidden" id="_timeToRestrictionEMAIL" name="_timeToRestrictionEMAIL">
                                        <input type="hidden" id="_emailFooterStatus" name="_emailFooterStatus">

                                        <input type="hidden" id="_emailFooter" name="_emailFooter">

                                        <div class="control-group">
                                            <label class="control-label"  for="username">Disable Quota</label>
                                            <div class="controls">
                                                <div>
                                                    <div class="btn-group">

                                                        <button class="btn btn-small"><div id="_emailStatusDIV"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeLimitStatusEMAIL(1)">Mark as Active?</a></li>
                                                            <li><a href="#" onclick="ChangeLimitStatusEMAIL(0)">Mark as Suspended?</a></li>
                                                        </ul>
                                                    </div>
                                                    , if enabled use Day Restriction as 
                                                    <div class="btn-group">
                                                        <button class="btn btn-small"><div id="_dayRestrictionEMAIL_div"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeDayRestrictionEMAIL(1, 'Week days only')">Week days only</a></li>
                                                            <li><a href="#" onclick="ChangeDayRestrictionEMAIL(2, 'Whole Week (including Weekend)')">Whole Week (including Weekend)</a></li>
                                                        </ul>
                                                    </div>
                                                    with time range as
                                                    <div class="btn-group">
                                                        <button class="btn btn-small"><div id="_timeFromRestrictionEMAIL_div"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeTimeRestrictionEMAIL(1, 7, '7AM')">7am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionEMAIL(1, 8, '8AM')">8am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionEMAIL(1, 9, '9AM')">9am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionEMAIL(1, 10, '10AM')">10am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionEMAIL(1, 11, '11AM')">11am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionEMAIL(1, 0, 'Any')">Any</a></li>
                                                        </ul>                        
                                                    </div>
                                                    to 
                                                    <div class="btn-group">
                                                        <button class="btn btn-small"><div id="_timeToRestrictionEMAIL_div"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeTimeRestrictionEMAIL(2, 12, '12PM')">12pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionEMAIL(2, 13, '1PM')">1pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionEMAIL(2, 14, '2PM')">2pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionEMAIL(2, 15, '3PM')">3pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionEMAIL(2, 16, '4PM')">4pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionEMAIL(2, 17, '5PM')">5pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionEMAIL(2, 18, '6PM')">6pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionEMAIL(2, 19, '7PM')">7pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionEMAIL(2, 24, 'Any')">Any</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label"  for="username">Maximum Quota</label>
                                            <div class="controls">
                                                <input type="text" id="_emaillimit" name="_emaillimit" placeholder="EMAIL Limit" class="span1">
                                                ,with threshold limit
                                                <input type="text" id="_emailThresholdlimit" name="_emailThresholdlimit" placeholder="SMS Cost" class="span1">
                                                (alert operator after threshold limit)
                                                <div class="btn-group">

                                                    <button class="btn btn-small"><div id="_emailAlertStatusDIV"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="ChangeLimitAlertStatusEMAIL(1)">Enable</a></li>
                                                        <li><a href="#" onclick="ChangeLimitAlertStatusEMAIL(0)">Disable</a></li>
                                                    </ul>
                                                </div>
                                                ,and cost
                                                <input type="text" id="_emailcost" name="_emailcost" placeholder="EMAIL Cost" class="span1">
                                                for one month Duration
<!--                                                per message for duration of
                                                <div class="btn-group">

                                                    <button class="btn btn-small"><div id="_emailRepeatStatusDIV"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="ChangeRepeatStatusEMAIL(1)">1 Month</a></li>
                                                        <li><a href="#" onclick="ChangeRepeatStatusEMAIL(2)">3 Month</a></li>
                                                        <li><a href="#" onclick="ChangeRepeatStatusEMAIL(3)">6 Month</a></li>
                                                    </ul>
                                                </div> -->

                                            </div>
                                        </div>

                                        <hr>
                                        <div class="control-group">
                                            <label class="control-label"  for="username">Speed Defination</label>
                                            <div class="controls">
                                                Slow
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_slowPaceEMAIL_div"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="SetPACEEMAIL(1, 1, '#_slowPaceEMAIL_div', '1 message/second')">1 message/second</a></li>
                                                        <li><a href="#" onclick="SetPACEEMAIL(1, 3, '#_slowPaceEMAIL_div', '3 messages/second')">3 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEEMAIL(1, 5, '#_slowPaceEMAIL_div', '5 messages/second')">5 messages/second</a></li>
                                                    </ul>
                                                </div>

                                                , Normal
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_normalPaceEMAIL_div"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="SetPACEEMAIL(2, 6, '#_normalPaceEMAIL_div', '6 messages/second')">6 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEEMAIL(2, 8, '#_normalPaceEMAIL_div', '8 messages/second')">8 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEEMAIL(2, 10, '#_normalPaceEMAIL_div', '10 messages/second')">10 messages/second</a></li>
                                                    </ul>
                                                </div>
                                                Fast
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_fastPaceEMAIL_div"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="SetPACEEMAIL(3, 15, '#_fastPaceEMAIL_div', '15 messages/second')">15 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEEMAIL(3, 20, '#_fastPaceEMAIL_div', '20 messages/second')">20 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEEMAIL(3, 25, '#_fastPaceEMAIL_div', '25 messages/second')">25 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEEMAIL(3, 30, '#_fastPaceEMAIL_div', '30 messages/second')">30 messages/second</a></li>                                
                                                    </ul>
                                                </div>
                                                ,and Hyper
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_hyperPaceEMAIL_div"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="SetPACEEMAIL(4, 35, '#_hyperPaceEMAIL_div', '35 messages/second')">35 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEEMAIL(4, 50, '#_hyperPaceEMAIL_div', '50 messages/second')">50 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEEMAIL(4, 75, '#_hyperPaceEMAIL_div', '75 messages/second')">75 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEEMAIL(4, 100, '#_hyperPaceEMAIL_div', '100 messages/second')">100 messages/second</a></li>                                
                                                    </ul>
                                                </div>

                                            </div>
                                        </div>

                                        <hr>
                                        <div class="control-group">
                                            <label class="control-label"  for="username">EMAIL Footer Status </label>
                                            <div class="controls">
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_emailFooterStatusDIV"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="ChangeFooterStatusEMAIL(1)">Mark as Active?</a></li>
                                                        <li><a href="#" onclick="ChangeFooterStatusEMAIL(0)">Mark as Suspended?</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div> 

                                        <div class="control-group">
                                            <label class="control-label"  for="username">Body</label>
                                            <div class="controls">
                                                <textarea class="ckeditor" id="_templatebodyS" name="_templatebodyS" ></textarea>                                    
                                            </div>                                
                                        </div>
                                        <hr>
                                        <div id="trackingDiv" ></div>
                                        <script type="text/javascript">
                                            CKEDITOR.replace('_templatebodyS');
                                            timer = setInterval('updateDiv()', 100);
                                            function updateDiv() {
                                                var editorText = CKEDITOR.instances._templatebodyS.getData();
                                                $('#_emailFooter').val(editorText);
                                            }
                                        </script>


                                    </fieldset>
                                    <%//if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
                                    <div class="control-group">
                                        <div class="controls">
                                            <div id="save-global-content-filter-settings-result"></div>
                                            <button class="btn btn-primary" onclick="SaveChannelEMAILSetting()" type="button" id="SaveChannelEMAILSettingButton">Save Changes Now >> </button>
                                        </div>
                                    </div>
                                    <%//}%>
                                    <script language="javascript" type="text/javascript">
                                        loadglobalsetting(7);

                                    </script>
                                </form> 
                            </div>
                        </div>
                        <div class="tab-pane" id="FAX">
                            <div class="row-fluid">
                                <%
                                    Date faxendDate = null;
                                    Date faxstartDate = null;
                                    if (globalObj != null) {
                                        if (globalObj.faxlimitsettingobj != null) {
                                            //    globalObj.smssettingobj = new smssetting();
                                            faxendDate = globalObj.faxlimitsettingobj.faxenddate;
                                            if (globalObj.faxlimitsettingobj.repeat == globalObj.MONTH) {
                                                c.setTime(faxendDate);
                                                c.add(Calendar.MONTH, -1);
                                                emailstartDate = c.getTime();
                                            } else if (globalObj.faxlimitsettingobj.repeat == globalObj.THREE_MONTH) {
                                                c.setTime(faxendDate);
                                                c.add(Calendar.MONTH, -3);
                                                emailstartDate = c.getTime();
                                            } else if (globalObj.emailsettingobj.repeat == globalObj.SIX_MONTH) {
                                                c.setTime(faxendDate);
                                                c.add(Calendar.MONTH, -6);
                                                emailstartDate = c.getTime();
                                            }
                                        }
                                    }

                                %>

                                <!--                                <div id="legend">
                                                                    <legend class=""> Started on <%=faxstartDate%> and ends on <%=faxendDate%></legend>
                                                                </div>-->
                                <!--                                <hr>-->
                                <form class="form-horizontal" id="FAXForm" name="FAXForm">
                                    <fieldset>

                                        <!--fax-->
                                        <input type="hidden" id="_faxStatus" name="_faxStatus">
                                        <input type="hidden" id="_faxAlertStatus" name="_faxAlertStatus">
                                        <input type="hidden" id="_faxRepeatStatus" name="_faxRepeatStatus">
                                        <input type="hidden" id="_faxRepeatDuration" name="_faxRepeatDuration">
                                        <!--faxspeed-->
                                        <input type="hidden" id="_slowPaceFAX" name="_slowPaceFAX">
                                        <input type="hidden" id="_normalPaceFAX" name="_normalPaceFAX">
                                        <input type="hidden" id="_fastPaceFAX" name="_fastPaceFAX">
                                        <input type="hidden" id="_hyperPaceFAX" name="_hyperPaceFAX">
                                        <!--restriction-->
                                        <input type="hidden" id="_dayRestrictionFAX" name="_dayRestrictionFAX">
                                        <input type="hidden" id="_timeFromRestrictionFAX" name="_timeFromRestrictionFAX">
                                        <input type="hidden" id="_timeToRestrictionFAX" name="_timeToRestrictionFAX">
                                        <input type="hidden" id="_faxFooterStatus" name="_faxFooterStatus">

                                        <div class="control-group">
                                            <label class="control-label"  for="username">Disable Quota</label>
                                            <div class="controls">
                                                <div>
                                                    <div class="btn-group">

                                                        <button class="btn btn-small"><div id="_faxStatusDIV"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeLimitStatusFAX(1)">Mark as Active?</a></li>
                                                            <li><a href="#" onclick="ChangeLimitStatusFAX(0)">Mark as Suspended?</a></li>
                                                        </ul>
                                                    </div>
                                                    , if enabled use Day Restriction as 
                                                    <div class="btn-group">
                                                        <button class="btn btn-small"><div id="_dayRestrictionFAX_div"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeDayRestrictionFAX(1, 'Week days only')">Week days only</a></li>
                                                            <li><a href="#" onclick="ChangeDayRestrictionFAX(2, 'Whole Week (including Weekend)')">Whole Week (including Weekend)</a></li>
                                                        </ul>
                                                    </div>
                                                    with time range as
                                                    <div class="btn-group">
                                                        <button class="btn btn-small"><div id="_timeFromRestrictionFAX_div"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeTimeRestrictionFAX(1, 7, '7AM')">7am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionFAX(1, 8, '8AM')">8am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionFAX(1, 9, '9AM')">9am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionFAX(1, 10, '10AM')">10am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionFAX(1, 11, '11AM')">11am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionFAX(1, 0, 'Any')">Any</a></li>
                                                        </ul>                        
                                                    </div>
                                                    to 
                                                    <div class="btn-group">
                                                        <button class="btn btn-small"><div id="_timeToRestrictionFAX_div"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeTimeRestrictionFAX(2, 12, '12PM')">12pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionFAX(2, 13, '1PM')">1pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionFAX(2, 14, '2PM')">2pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionFAX(2, 15, '3PM')">3pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionFAX(2, 16, '4PM')">4pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionFAX(2, 17, '5PM')">5pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionFAX(2, 18, '6PM')">6pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionFAX(2, 19, '7PM')">7pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionFAX(2, 24, 'Any')">Any</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label"  for="username">Maximum Quota</label>
                                            <div class="controls">
                                                <input type="text" id="_faxlimit" name="_faxlimit" placeholder="FAX Limit" class="span1">
                                                ,with threshold limit 
                                                <input type="text" id="_faxThresholdlimit" name="_faxThresholdlimit" placeholder="SMS Cost" class="span1">
                                                (alert operator after threshold limit)
                                                <div class="btn-group">

                                                    <button class="btn btn-small"><div id="_faxAlertStatusDIV"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="ChangeLimitAlertStatusFAX(1)">Enable</a></li>
                                                        <li><a href="#" onclick="ChangeLimitAlertStatusFAX(0)">Disable</a></li>
                                                    </ul>
                                                </div>
                                                ,and cost 
                                                <input type="text" id="_faxcost" name="_faxcost" placeholder="EMAIL Cost" class="span1">
                                                for one month Duration
<!--                                                per message for duration of
                                                <div class="btn-group">

                                                    <button class="btn btn-small"><div id="_faxRepeatStatusDIV"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="ChangeRepeatStatusFAX(1)">1 Month</a></li>
                                                        <li><a href="#" onclick="ChangeRepeatStatusFAX(2)">3 Month</a></li>
                                                        <li><a href="#" onclick="ChangeRepeatStatusFAX(3)">6 Month</a></li>
                                                    </ul>
                                                </div>-->

                                            </div>
                                        </div>

                                        <hr>

                                        <div class="control-group">
                                            <label class="control-label"  for="username">Speed Defination</label>
                                            <div class="controls">
                                                Slow
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_slowPaceFAX_div"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="SetPACEFAX(1, 1, '#_slowPaceFAX_div', '1 message/second')">1 message/second</a></li>
                                                        <li><a href="#" onclick="SetPACEFAX(1, 3, '#_slowPaceFAX_div', '3 messages/second')">3 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEFAX(1, 5, '#_slowPaceFAX_div', '5 messages/second')">5 messages/second</a></li>
                                                    </ul>
                                                </div>
                                                , Normal 
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_normalPaceFAX_div"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="SetPACEFAX(2, 6, '#_normalPaceFAX_div', '6 messages/second')">6 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEFAX(2, 8, '#_normalPaceFAX_div', '8 messages/second')">8 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEFAX(2, 10, '#_normalPaceFAX_div', '10 messages/second')">10 messages/second</a></li>
                                                    </ul>
                                                </div>
                                                Fast
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_fastPaceFAX_div"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="SetPACEFAX(3, 15, '#_fastPaceFAX_div', '15 messages/second')">15 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEFAX(3, 20, '#_fastPaceFAX_div', '20 messages/second')">20 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEFAX(3, 25, '#_fastPaceFAX_div', '25 messages/second')">25 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEFAX(3, 30, '#_fastPaceFAX_div', '30 messages/second')">30 messages/second</a></li>                                
                                                    </ul>
                                                </div>
                                                ,and Hyper 
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_hyperPaceFAX_div"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="SetPACEFAX(4, 35, '#_hyperPaceFAX_div', '35 messages/second')">35 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEFAX(4, 50, '#_hyperPaceFAX_div', '50 messages/second')">50 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEFAX(4, 75, '#_hyperPaceFAX_div', '75 messages/second')">75 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEFAX(4, 100, '#_hyperPaceFAX_div', '100 messages/second')">100 messages/second</a></li>                                
                                                    </ul>
                                                </div>

                                            </div>
                                        </div>

                                        <hr>
                                        <div class="control-group">
                                            <label class="control-label"  for="username">Footer Status </label>
                                            <div class="controls">

                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_faxFooterStatusDIV"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="ChangeFooterStatusFAX(1)">Mark as Active?</a></li>
                                                        <li><a href="#" onclick="ChangeFooterStatusFAX(0)">Mark as Suspended?</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div> 

                                        <!--                                        <div class="control-group" id="_faxFooterS">
                                                                                    <label class="control-label"  for="username"></label>
                                                                                    <div class="controls">
                                                                                        <textarea id="_faxFooter" name="_faxFooter"  class="span9" rows="3" cols="20"></textarea>   
                                                                                    </div>
                                                                                </div>-->

                                        <!--                                        <div class="control-group">
                                                                                    <label class="control-label"  for="username">FAX Footer :</label>
                                                                                    <div class="controls">
                                                                                        <textarea class="ckeditor" id="_faxFooter" name="_faxFooter" ></textarea>                                    
                                        
                                                                                    </div>                                
                                                                                </div>-->
                                        <div class="control-group" id="_faxFooterS">
                                            <label class="control-label"  for="username">Fax Footer</label>
                                            <div class="controls">
                                                <textarea id="_faxFooter" name="_faxFooter"  class="span9" rows="3" cols="20"></textarea>   
                                            </div>
                                        </div>
                                        <!--                                        <script type="text/javascript">
                                                                                    CKEDITOR.replace('_faxFooter');
                                                                                    timer = setInterval('updateDiv()', 100);
                                                                                    function updateDiv() {
                                                                                        var editorText = CKEDITOR.instances._faxFooter.getData();
                                                                                        $('#faxCotents').val(editorText);
                                                                                    }
                                                                                </script>  -->

                                    </fieldset>
                                    <%//if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
                                    <div class="control-group">
                                        <div class="controls">
                                            <div id="save-global-content-filter-settings-result"></div>
                                            <button class="btn btn-primary" onclick="SaveChannelFAXSetting()" type="button" id="SaveChannelFAXSettingButton">Save Changes Now >> </button>
                                        </div>
                                    </div>
                                    <%//}%>
                                    <script language="javascript" type="text/javascript">
                                        loadglobalsetting(8);

                                    </script>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane" id="PUSH">
                            <div class="row-fluid">
                                <hr>
                                <form class="form-horizontal" id="PUSHForm" name="PUSHForm">
                                    <fieldset>
                                        <!--PUSH-->
                                        <input type="hidden" id="_pushStatus" name="_pushStatus">
                                        <input type="hidden" id="_pushAlertStatus" name="_pushAlertStatus">
                                        <input type="hidden" id="_pushRepeatStatus" name="_pushRepeatStatus">
                                        <input type="hidden" id="_pushRepeatDuration" name="_pushRepeatDuration">
                                        <!--pushspeed-->
                                        <input type="hidden" id="_slowPacePUSH" name="_slowPacePUSH">
                                        <input type="hidden" id="_normalPacePUSH" name="_normalPacePUSH">
                                        <input type="hidden" id="_fastPacePUSH" name="_fastPacePUSH">
                                        <input type="hidden" id="_hyperPacePUSH" name="_hyperPacePUSH">
                                        <!--restriction-->
                                        <input type="hidden" id="_dayRestrictionPUSH" name="_dayRestrictionPUSH">
                                        <input type="hidden" id="_timeFromRestrictionPUSH" name="_timeFromRestrictionPUSH">
                                        <input type="hidden" id="_timeToRestrictionPUSH" name="_timeToRestrictionPUSH">
                                        <input type="hidden" id="_pushFooterStatus" name="_pushFooterStatus">

                                        <div class="control-group">
                                            <label class="control-label"  for="username">Disable Quota</label>
                                            <div class="controls">
                                                <div>
                                                    <div class="btn-group">

                                                        <button class="btn btn-small"><div id="_pushStatusDIV"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeLimitStatusPUSH(1)">Mark as Active?</a></li>
                                                            <li><a href="#" onclick="ChangeLimitStatusPUSH(0)">Mark as Suspended?</a></li>
                                                        </ul>
                                                    </div>
                                                    , if enabled use Day Restriction as
                                                    <div class="btn-group">
                                                        <button class="btn btn-small"><div id="_dayRestrictionPUSH_div"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeDayRestrictionPUSH(1, 'Week days only')">Week days only</a></li>
                                                            <li><a href="#" onclick="ChangeDayRestrictionPUSH(2, 'Whole Week (including Weekend)')">Whole Week (including Weekend)</a></li>
                                                        </ul>
                                                    </div>
                                                    with time range as
                                                    <div class="btn-group">
                                                        <button class="btn btn-small"><div id="_timeFromRestrictionPUSH_div"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(1, 7, '7AM')">7am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(1, 8, '8AM')">8am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(1, 9, '9AM')">9am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(1, 10, '10AM')">10am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(1, 11, '11AM')">11am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(1, 0, 'Any')">Any</a></li>
                                                        </ul>                        
                                                    </div>
                                                    to 
                                                    <div class="btn-group">
                                                        <button class="btn btn-small"><div id="_timeToRestrictionPUSH_div"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(2, 12, '12PM')">12pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(2, 13, '1PM')">1pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(2, 14, '2PM')">2pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(2, 15, '3PM')">3pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(2, 16, '4PM')">4pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(2, 17, '5PM')">5pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(2, 18, '6PM')">6pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(2, 19, '7PM')">7pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(2, 24, 'Any')">Any</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label"  for="username">Maximum Quota</label>
                                            <div class="controls">
                                                <input type="text" id="_pushlimit" name="_pushlimit" placeholder="PUSH MSG Limit" class="span1">
                                                ,with threshold limit
                                                <input type="text" id="_pushThresholdlimit" name="_pushThresholdlimit" placeholder="SMS Cost" class="span1">
                                                (alert operator after threshold limit) 
                                                <div class="btn-group">

                                                    <button class="btn btn-small"><div id="_pushAlertStatusDIV"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="ChangeLimitAlertStatusPUSH(1)">Enable</a></li>
                                                        <li><a href="#" onclick="ChangeLimitAlertStatusPUSH(0)">Disable</a></li>
                                                    </ul>
                                                </div>
                                                ,and cost
                                                <input type="text" id="_pushcost" name="_pushcost" placeholder="PUSH MSG Cost" class="span1">
                                                for one month Duration
<!--                                                per message for duration of -->
<!--                                                <div class="btn-group">

                                                    <button class="btn btn-small"><div id="_pushRepeatStatusDIV"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="ChangeRepeatStatusPUSH(1)">1 Month</a></li>
                                                        <li><a href="#" onclick="ChangeRepeatStatusPUSH(2)">3 Month</a></li>
                                                        <li><a href="#" onclick="ChangeRepeatStatusPUSH(3)">6 Month</a></li>
                                                    </ul>
                                                </div>-->
                                            </div>
                                        </div>

<!--                                        <hr>
                                        <div class="control-group">
                                            <label class="control-label"  for="username">Status :</label>
                                            <div class="controls">
                                                                                                <div class="btn-group">
                                                
                                                                                                    <button class="btn btn-small"><div id="_pushStatusDIV"></div></button>
                                                                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                                                                    <ul class="dropdown-menu">
                                                                                                        <li><a href="#" onclick="ChangeLimitStatusPUSH(1)">Mark as Active?</a></li>
                                                                                                        <li><a href="#" onclick="ChangeLimitStatusPUSH(0)">Mark as Suspended?</a></li>
                                                                                                    </ul>
                                                                                                </div>

                                                , Alert Operator :
                                                <div class="btn-group">

                                                    <button class="btn btn-small"><div id="_pushAlertStatusDIV"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="ChangeLimitAlertStatusPUSH(1)">Enable</a></li>
                                                        <li><a href="#" onclick="ChangeLimitAlertStatusPUSH(0)">Disable</a></li>
                                                    </ul>
                                                </div>
                                                , Repeat After :
                                                <div class="btn-group">

                                                    <button class="btn btn-small"><div id="_pushRepeatStatusDIV"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="ChangeRepeatStatusPUSH(1)">1 Month</a></li>
                                                        <li><a href="#" onclick="ChangeRepeatStatusPUSH(2)">3 Month</a></li>
                                                        <li><a href="#" onclick="ChangeRepeatStatusPUSH(3)">6 Month</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>-->
                                        <div class="control-group">
<!--                                            <label class="control-label"  for="username">Day Restriction</label>
                                            <div class="controls">
                                                <div>
                                                    <div class="btn-group">
                                                        <button class="btn btn-small"><div id="_dayRestrictionPUSH_div"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeDayRestrictionPUSH(1, 'Week days only')">Week days only</a></li>
                                                            <li><a href="#" onclick="ChangeDayRestrictionPUSH(2, 'Whole Week (including Weekend)')">Whole Week (including Weekend)</a></li>
                                                        </ul>
                                                    </div>
                                                    time between 
                                                    <div class="btn-group">
                                                        <button class="btn btn-small"><div id="_timeFromRestrictionPUSH_div"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(1, 7, '7AM')">7am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(1, 8, '8AM')">8am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(1, 9, '9AM')">9am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(1, 10, '10AM')">10am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(1, 11, '11AM')">11am</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(1, 0, 'Any')">Any</a></li>
                                                        </ul>                        
                                                    </div>
                                                    to 
                                                    <div class="btn-group">
                                                        <button class="btn btn-small"><div id="_timeToRestrictionPUSH_div"></div></button>
                                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(2, 12, '12PM')">12pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(2, 13, '1PM')">1pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(2, 14, '2PM')">2pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(2, 15, '3PM')">3pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(2, 16, '4PM')">4pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(2, 17, '5PM')">5pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(2, 18, '6PM')">6pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(2, 19, '7PM')">7pm</a></li>
                                                            <li><a href="#" onclick="ChangeTimeRestrictionPUSH(2, 24, 'Any')">Any</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>-->
                                        <hr>

                                        <div class="control-group">
                                            <label class="control-label"  for="username">PUSH MSG Speed</label>
                                            <div class="controls">
                                                Slow Space
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_slowPacePUSH_div"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="SetPACEPUSH(1, 1, '#_slowPacePUSH_div', '1 message/second')">1 message/second</a></li>
                                                        <li><a href="#" onclick="SetPACEPUSH(1, 3, '#_slowPacePUSH_div', '3 messages/second')">3 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEPUSH(1, 5, '#_slowPacePUSH_div', '5 messages/second')">5 messages/second</a></li>
                                                    </ul>
                                                </div>
                                                , Normal Space 
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_normalPacePUSH_div"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="SetPACEPUSH(2, 6, '#_normalPacePUSH_div', '6 messages/second')">6 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEPUSH(2, 8, '#_normalPacePUSH_div', '8 messages/second')">8 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEPUSH(2, 10, '#_normalPacePUSH_div', '10 messages/second')">10 messages/second</a></li>
                                                    </ul>
                                                </div>
                                                Fast Pace:
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_fastPacePUSH_div"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="SetPACEPUSH(3, 15, '#_fastPacePUSH_div', '15 messages/second')">15 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEPUSH(3, 20, '#_fastPacePUSH_div', '20 messages/second')">20 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEPUSH(3, 25, '#_fastPacePUSH_div', '25 messages/second')">25 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEPUSH(3, 30, '#_fastPacePUSH_div', '30 messages/second')">30 messages/second</a></li>                                
                                                    </ul>
                                                </div>
                                                ,and Hyper Pace: 
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_hyperPacePUSH_div"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="SetPACEPUSH(4, 35, '#_hyperPacePUSH_div', '35 messages/second')">35 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEPUSH(4, 50, '#_hyperPacePUSH_div', '50 messages/second')">50 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEPUSH(4, 75, '#_hyperPacePUSH_div', '75 messages/second')">75 messages/second</a></li>
                                                        <li><a href="#" onclick="SetPACEPUSH(4, 100, '#_hyperPacePUSH_div', '100 messages/second')">100 messages/second</a></li>                                
                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                        <hr>
                                        <div class="control-group">

                                            <label class="control-label"  for="username">PUSH MSG Footer Status </label>
                                            <div class="controls">
                                                <div class="btn-group">
                                                    <button class="btn btn-small"><div id="_pushFooterStatusDIV"></div></button>
                                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="ChangeFooterStatusPUSH(1)">Mark as Active?</a></li>
                                                        <li><a href="#" onclick="ChangeFooterStatusPUSH(0)">Mark as Suspended?</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="control-group" id="_pushFooterS">
                                            <label class="control-label"  for="username">PUSH Footer :</label>
                                            <div class="controls">
                                                <textarea id="_pushFooter" name="_pushFooter"  class="span9" rows="3" cols="20"></textarea>   
                                            </div>
                                        </div>

                                    </fieldset>
                                    <%//if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
                                    <div class="control-group">
                                        <div class="controls">
                                            <div id="save-global-content-filter-settings-result"></div>
                                            <button class="btn btn-primary" onclick="SaveChannelPUSHSetting()" type="button" id="SaveChannelPUSHSettingButton">Save Changes Now >> </button>
                                        </div>
                                    </div>
                                    <%//}%>
                                    <script language="javascript" type="text/javascript">
                                        loadglobalsetting(9);

                                    </script>
                                </form>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    

    $(document).ready(function() {
        $("#_keywords").select2({
            tags: [],
            dropdownCss: {display: 'none'},
            tokenSeparators: [","]
        });
    });

    $(document).ready(function() {
        $("#_ip").select2({
            tags: [],
            dropdownCss: {display: 'none'},
            tokenSeparators: [","]
        });
    });

    $(document).ready(function() {
        $("#_prefix").select2({
            tags: [],
            dropdownCss: {display: 'none'},
            tokenSeparators: [","]
        });
    });

    $(document).ready(function() {
        $("#_domain").select2({
            tags: [],
            dropdownCss: {display: 'none'},
            tokenSeparators: [","]
        });
    });
</script>




<%@include file="footer.jsp" %>