<%@page import="com.mollatech.axiom.nucleus.db.Users"%>
<%@page import="com.mollatech.axiom.nucleus.db.Doccategory"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CategoryManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserGroupsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Usergroups"%>
<%@include file="header.jsp" %>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<!--<script src="./assets/js/usermanagement.js"></script>-->
<script src="./assets/js/category.js"></script>

<div class="container-fluid" >
    <div id="auditTable">
        <h1 class="text-success">Document Category Management</h1>
        <p>List of Document Category  in the system. Their document category management can be managed from this interface.</p>
        <h3>List of Document Categories</h3>   
        <!--        <div class="input-append">
                    <form id="searchUserForm" name="searchUserForm">
                        <input type="text" id="_keyword" name="_keyword" placeholder="Search using document category name..." class="span4"><span class="add-on"><i class="icon-search"></i></span>
                        <a href="#" class="btn btn-success" onclick="searchUsers()">Search Now</a>
                    </form>
                </div>-->
        <table class="display responsive wrap" id="table_main">
            <thead>
            <tr><td><b>Sr.No.</td>
                <td><b>Category Name</td>
                <td><b>Manage</td>
                <td><b>Category Note</td>
                <td><b>Created On</td>
                <td><b>Last Updated On</td>
            </tr>
            </thead>
            <%  int no = 1;
                int count = 0;
                Channels channel = (Channels) session.getAttribute("_apSChannelDetails");
                OperatorsManagement oManagement = new OperatorsManagement();
                String _sessionID = (String) session.getAttribute("_apSessionID");
                Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
                CategoryManagement cmt = new CategoryManagement();
                Doccategory[] categorydetails = cmt.getAllCategoryDetails(_sessionID, channel.getChannelid());
                if (categorydetails != null) {
                    for (int i = 0; i < categorydetails.length; i++) {
                        String categoryStatus = "user-status-value-" + i;
                        if (categorydetails[i].getStatus() != -99) {
                            count = count + 1;
            %>

            <tr><td><%=no%></td><td><%= categorydetails[i].getCategoryname()%></td>
                <td>
                    <div class="btn-group">
                        <button class="btn btn-mini" id="<%=categorydetails[i].getId()%>"><% if (categorydetails[i].getStatus() == 1) {%> ACTIVE <%} else {%> SUSPENDED <%}%></button>
                        <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#"  onclick="changeCategoryStatus(1, <%= categorydetails[i].getId()%>, '<%= categoryStatus%>')" >Mark as Active?</a></li>
                            <li><a href="#" onclick="changeCategoryStatus(0, <%= categorydetails[i].getId()%>, '<%=categoryStatus%>')" >Mark as Suspended?</a></li>
                            <li class="divider"></li>
                            <li><a href="#" onclick="editCategorymodal('<%=categorydetails[i].getId()%>', '<%=categorydetails[i].getCategoryname()%>', '<%=categorydetails[i].getNote()%>', <%=categorydetails[i].getStatus()%>)" data-toggle="modal">Edit Details</a></li>
                            <!--<li><a href="#" onclick="removeCategory('<%=categorydetails[i].getId()%>')" data-toggle="modal"><font color="red">Remove?</font></a></li>-->
                        </ul>
                    </div>
                </td>
                <td><%= categorydetails[i].getNote()%></td>
                <td><%=categorydetails[i].getCreatedon()%></td>
                <td><%=categorydetails[i].getLastupdate()%></td>
            </tr>
            <%
                            no++;
                        }
                    }
                }else{
                if (count != 0) {%>
            <td>No Record Found</td><td>No Record Found</td><td>No Record Found</td><td>No Record Found</td><td>No Record Found</td>
            <% }}%>
        </table>
        <div id="users_table_main">
        </div>
        <p><a href="#addNewUser" class="btn btn-primary" data-toggle="modal">Add New Category&raquo;</a></p>
    </div>
    <div id="addNewUser" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3 id="myModalLabel">Add New Category</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="addCategoryForm">
                    <fieldset>
                        <!-- Name -->
                        <div class="control-group">
                            <label class="control-label"  for="username">Category Name</label>
                            <div class="controls">
                                <input type="text" id="_Name" name="_Name" placeholder="Category name" class="input-xlarge">
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label"  for="username">Status</label>
                            <div class="controls">
                                <select id="_Status" name="_Status" >
                                    <option value="1">Active</option>
                                    <option value="0">Suspended</option>
                                </select>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label"  for="username">Category Note</label>
                            <div class="controls">
                                <textarea id="_Note" name="_Note"></textarea>
                            </div>
                        </div>                  

                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <div id="add-category-result"></div>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button class="btn btn-primary" onclick="addCategory()" id="addCategoryButton">Create Category</button>
        </div>
    </div>
    <div id="editCategory" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3><div id="idEditCategory"></div></h3>
            <h3 id="mycategoryeditmodel">Edit Category</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="editCategoryForm">
                    <fieldset>
                        <!-- Name -->
                        <div class="control-group">
                            <label class="control-label"  for="username">Name</label>
                            <div class="controls">
                                <input type="hidden" readonly id="_ecategoryId" name="_ecategoryId" class="input-mini">
                                <input type="text" id="_ecategoryName" name="_ecategoryName"  class="input-xlarge">
                            </div>
                        </div>   
                        <div class="control-group">
                            <label class="control-label"  for="username">Status</label>
                            <div class="controls">
                                <select class="span7" name="_eStatus" id="_eStatus">
                                    <option value="1">Active</option>
                                    <option value="0">Suspended</option>                               
                                </select>
                            </div>
                        </div>   
                        <div class="control-group">
                            <label class="control-label"  for="username">Note</label>
                            <div class="controls">
                                <textarea id="_eNote" name="_eNote">
                                    
                                </textarea>
                            </div>
                        </div>  
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <div id="edit-category-result"></div>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>        
            <button class="btn btn-primary" onclick="editCategory()" id="editCategoryButton">Update Category</button>
        </div>
    </div>

    <script>
            $(function() {
                $('#Pushdatetimepicker1').datepicker({
                     format: 'dd/MM/yyyy',
            language: 'pt-BR'
        });
        });
            $(function() {
                $('#Pushdatetimepicker2').datepicker({
                     format: 'dd/MM/yyyy',
            language: 'pt-BR'
        });
        });
    </script>
    
    

    <%@include file="footer.jsp" %>
    <script> 
    $(document).ready(function () { 
        $('#table_main').DataTable({ 
            responsive: true 
        }); 
    }); 
</script>