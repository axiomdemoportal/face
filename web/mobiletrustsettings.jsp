<%@page import="com.mollatech.axiom.nucleus.db.connector.SettingsUtil"%>
<%@page import="com.mollatech.axiom.nucleus.db.Statelist"%>
<%@page import="com.mollatech.axiom.nucleus.db.Countrylist"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.LocationManagement"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Hashtable"%>
<%@page import="com.mollatech.axiom.nucleus.db.Templates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%@page import="com.mollatech.axiom.nucleus.settings.RootCertificateSettings"%>
<%@page import="com.mollatech.axiom.nucleus.settings.MobileTrustSettings"%>

<%@page import="com.mollatech.axiom.face.common.CountryCodes"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/mobiletrust.js"></script>
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<script>

    $(document).ready(function() {
      //  var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
        $('#_CountryName').change(function() {
            var selectedValue = $(this).val();
          // alert(selectedValue);
           
     var s = './stateList.jsp?_countryId=' + selectedValue;
    
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
           
                $('#_StateName').html(data);
            
        }
    });
        });
    });

</script>
<%    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    //String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    SettingsManagement smng = new SettingsManagement();
    //MobileTrustSettings cfObj = (MobileTrustSettings) smng.getSetting(sessionId, channel.getChannelid(), SettingsManagement.MOBILETRUST_SETTING, SettingsManagement.PREFERENCE_ONE);

    Object obj = smng.getSettingInner(channel.getChannelid(), smng.RootConfiguration, smng.PREFERENCE_ONE);
%>
<div class="container-fluid">
    <h1 class="text-success">Trust Settings</h1>
    <p>To facilitate multi layered security using various authentication mechanism, this configuration helps you with setting you needed configuration to enforce mobile/web trust.</p>
    <div class="tabbable">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#General" data-toggle="tab">General Trust Settings</a></li>
            <li><a href="#Mobile" data-toggle="tab">Mobile Trust Settings</a></li>
            <li><a href="#Web" data-toggle="tab">Web Trust Settings</a></li>
        </ul>
        <!--    <h1 class="text-success">Mobile Trust Settings</h1>-->
        <!--<h4><span class="marker">note: <em><strong><span class="marker">Please Fill Certificat</span>e Connector Setting First</strong></em></span></h4>-->
        <div class="tab-content">
            <div class="tab-pane active" id="General">
                <!--    <p>To facilitate multi layered security using various authentication mechanism, this configuration helps you with setting you needed configuration to enforce mobile trust.</p>-->
                <div class="row-fluid">
                    <form class="form-horizontal" id="mobiletrustsettingsform" name="mobiletrustsettingsform">

                        <input type="hidden" id="_ExpiryMin" name="_ExpiryMin">
                        <input type="hidden" id="_Backup" name="_Backup">
                         <input type="hidden" id="_hashalgo" name="_hashalgo">
                        <input type="hidden" id="_SilentCall" name="_SilentCall">
                        <input type="hidden" id="_SelfDestructEnable" name="_SelfDestructEnable">
                        <input type="hidden" id="_SelfDestructAttempts" name="_SelfDestructAttempts">

                        <input type="hidden" id="_timeStamp" name="_timeStamp">
                        <input type="hidden" id="_deviceTracking" name="_deviceTracking">
                        <input type="hidden" id="_geoFencing" name="_geoFencing">
                        <!--                        <input type="hidden" id="_alertTo" name="_alertTo">
                                                <input type="hidden" id="_gatewayType" name="_gatewayType">
                                                <input type="hidden" id="_alertAttempt" name="_alertAttempt">-->
                        <input type="hidden" id="_allowAlertM" name="_allowAlertM">
                        <input type="hidden" id="_gatewayTypeM" name="_gatewayTypeM">
                        <input type="hidden" id="_alertAttemptM" name="_alertAttemptM">
                        <input type="hidden" id="_allowAlertForM" name="_allowAlertForM">
                        <input type="hidden" id="_isBlackListed" name="_isBlackListed">

                        <label  for="username">Trust Settings:</label>
                        <div class="control-group">
                            <label class="control-label"  for="username">Home Country</label>
                            <div class="controls">
                                <select class="span4" name="_CountryName" id="_CountryName">
                                    <%
                                        //getAll conotries here 
                                        LocationManagement lManagement = new LocationManagement();
                                        int countryCode = -1;
                                        Countrylist[] countryList = lManagement.getAllCountries();
                                        if (countryList != null) {
                                            for (int j = 0; j < countryList.length; j++) {
                                                String strcode = countryList[j].getCountyName();
                                                countryCode = countryList[j].getCountryid();
                                    %>
                                    <option value="<%=countryCode%>"><%=strcode%></option>

                                    <%}
                                        }%>
                                </select>
                            </div>
                        </div>
                                <div name="_StateName" id="_StateName">                           
                        </div>


                        <div class="control-group">
                            <label class="control-label"  for="username">Block Countries:</label>
                            <div class="controls">

                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_block_Countries"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="SetBlackListedCountriesSetting(1, '#_block_Countries', 'Yes')">Yes</a></li>
                                        <li><a href="#" onclick="SetBlackListedCountriesSetting(2, '#_block_Countries', 'No')">No</a></li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Select Black Listed Countries:</label> 
                            <div class="controls">

                                <!--                                    <select  name="_blackListedCountries" id="_blackListedCountries"  class="span3" multiple>
                                <% for (int j = 0; j < countryList.length; j++) {
                                %>
                                <option data-content="<span class='label label-success'><%=countryList[j]%></span>"value="<%=countryList[j]%>"><%=countryList[j]%></option>                                             
                                <%}%>
                            </select>-->
                                <select  name="_blackListedCountries" id="_blackListedCountries"  class="input-xxlarge" multiple>
                                    <%
                                        SettingsManagement sMngt = new SettingsManagement();
//                                        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                                        MobileTrustSettings tagsObj = (MobileTrustSettings) sMngt.getSettingInner(channel.getChannelid(), SettingsManagement.MOBILETRUST_SETTING, SettingsManagement.PREFERENCE_ONE);
                                        Countrylist[] oldColumns1 = countryList;

                                        String[] strCOlumnSequence1 = new String[3];
                                        strCOlumnSequence1[0] = "India";
                                        strCOlumnSequence1[1] = "Malasiya";
                                        strCOlumnSequence1[2] = "Combodia";
                                        if (tagsObj != null) {
                                            strCOlumnSequence1 = tagsObj.listOfBlackListedCountries.split(",");
                                        }
                                        Hashtable columnsAvailable11 = new Hashtable();
                                        Countrylist[] cols1 = oldColumns1;
                                        for (int i = 0; i < cols1.length; i++) {
                                            columnsAvailable11.put(cols1[i].getCountyName(), cols1[i].getCountyName());
                                        }

                                        if (strCOlumnSequence1 != null) {
                                            for (int i = 0; i < strCOlumnSequence1.length; i++) {
                                                columnsAvailable11.remove(strCOlumnSequence1[i]);
                                            }

                                            for (int i = 0; i < strCOlumnSequence1.length; i++) {
                                    %>
                                    <option value="<%=strCOlumnSequence1[i]%>" selected><%=strCOlumnSequence1[i]%></option>                                             
                                    <%
                                            }
                                        }
                                        Enumeration colids = columnsAvailable11.keys();

                                        while (colids.hasMoreElements()) {
                                            String tagid = (String) colids.nextElement();
                                            String tagname = (String) columnsAvailable11.get(tagid);
                                    %>
                                    <option value="<%=tagname%>"><%=tagname%></option>                                             
                                    <%
                                        }
                                    %>  
                                </select>




                            </div>

                        </div>  
                        <!--</div>-->
                        <!--                            <div class="control-group">
                                                    <label class="control-label"  for="username">Enforce Key Backup:</label>
                                                    <div class="controls">
                        
                                                         <div class="btn-group">
                                                            <button class="btn btn-small"><div id="_block_Countries"></div></button>
                                                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#" onclick="SetBlackListedCountriesSetting(1, '#_block_Countries', 'Yes')">Yes</a></li>
                                                                <li><a href="#" onclick="SetBlackListedCountriesSetting(2, '#_block_Countries', 'No')">No</a></li>
                        
                                                            </ul>
                                                        </div>
                                                      Geolocation Source :
                                                        <select  name="_blackListedCountries" id="_blackListedCountries"  class="span3" multiple>
                        <% for (int j = 0; j < countryList.length; j++) {
                        %>
                        <option data-content="<span class='label label-success'><%=countryList[j]%></span>"value="<%=countryList[j]%>"><%=countryList[j]%></option>                                             
                        <%
                                            }%>
                    </select>

            </div>
              

        </div> -->
                        <div class="control-group">
                            <label class="control-label"  for="username">Enforce Key Backup:</label>
                            <div class="controls">

                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_backUp_div"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="SetBackupSetting(1, '#_backUp_div', 'Yes')">Yes</a></li>
                                        <li><a href="#" onclick="SetBackupSetting(2, '#_backUp_div', 'No')">No</a></li>

                                    </ul>
                                </div>
                                Geolocation Source :
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_backUp_div">IP to Geo</div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="SetBackupSetting(1, '#_backUp_div', 'Yes')">Google API's</a></li>
                                        <li><a href="#" onclick="SetBackupSetting(2, '#_backUp_div', 'No')">IP to Geo</a></li>

                                    </ul>
                                </div>
                                 Hashing Algorithm :
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_hashalogdiv">Hashing Algo</div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="SetHashAlgo('SHA1', '#_hashalogdiv', 'SHA1')">SHA1</a></li>
                                        <li><a href="#" onclick="SetHashAlgo('SHA256', '#_hashalogdiv', 'SHA256')">SHA256</a></li>
                                        <li><a href="#" onclick="SetHashAlgo('SHA512', '#_hashalogdiv', 'SHA512')">SHA512</a></li>
                                    </ul>
                                </div>

                            </div>

                        </div>     
                        <div class="control-group">
                            <label class="control-label"  for="username">Expiry Time:</label>
                            <div class="controls">

                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_ExpiryMin_div"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="SetExpiryMin(1, '#_ExpiryMin_div', '1 minute')">1 minute</a></li>
                                        <li><a href="#" onclick="SetExpiryMin(2, '#_ExpiryMin_div', '2 minutes')">2 minutes</a></li>
                                        <li><a href="#" onclick="SetExpiryMin(3, '#_ExpiryMin_div', '3 minutes')">3 minutes</a></li>
                                        <li><a href="#" onclick="SetExpiryMin(5, '#_ExpiryMin_div', '5 minutes')">5 minutes</a></li>
                                        <li><a href="#" onclick="SetExpiryMin(7, '#_ExpiryMin_div', '7 minutes')">7 minutes</a></li>
                                        <li><a href="#" onclick="SetExpiryMin(10, '#_ExpiryMin_div', '10 minutes')">10 minutes</a></li>

                                    </ul>
                                </div>
                                Suppress Registration Code (via OOB):
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_silentCall_div"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="SetSilentSetting(1, '#_silentCall_div', 'Yes')">Yes</a></li>
                                        <li><a href="#" onclick="SetSilentSetting(2, '#_silentCall_div', 'No')">No</a></li>

                                    </ul>
                                </div>

                            </div>

                        </div>     
                        <div class="control-group">
                            <label class="control-label"  for="username">Self Destruct Alert:</label>
                            <div class="controls">
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_selfDestructAlert_div"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="SetSelfDestructAlertSetting(1, '#_selfDestructAlert_div', 'Yes')">Yes</a></li>
                                        <li><a href="#" onclick="SetSelfDestructAlertSetting(2, '#_selfDestructAlert_div', 'No')">No</a></li>

                                    </ul>
                                </div>
                                Self Destruct Attempts:
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_selfDestructAttempts_div"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="SelfDestructAttempts(3, '#_selfDestructAttempts_div', '3 Attempts')">3 Attempts</a></li>
                                        <li><a href="#" onclick="SelfDestructAttempts(5, '#_selfDestructAttempts_div', '5 Attempts')">5 Attempts</a></li>
                                        <li><a href="#" onclick="SelfDestructAttempts(7, '#_selfDestructAttempts_div', '7 Attempts')">7 Attempts</a></li>

                                    </ul>
                                </div>

                            </div>

                        </div>    

                        <div class="control-group">
                            <label class="control-label"  for="username">Timestamp:</label>
                            <div class="controls">
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_timeStamp_div"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="Timestamp(1, '#_timeStamp_div', 'Enable')">Enable</a></li>
                                        <li><a href="#" onclick="Timestamp(2, '#_timeStamp_div', 'Disable')">Disable</a></li>

                                    </ul>
                                </div>
                                Device Tracking:
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_deviceTracking_div"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="DeviceTracking(1, '#_deviceTracking_div', 'Enable')">Enable</a></li>
                                        <li><a href="#" onclick="DeviceTracking(2, '#_deviceTracking_div', 'Disable')">Disable</a></li>
                                    </ul>
                                </div>
                                Geofencing:
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_geoFencing_div"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="GeoFencing(1, '#_geoFencing_div', 'Enable')">Enable</a></li>
                                        <li><a href="#" onclick="GeoFencing(2, '#_geoFencing_div', 'Disable')">Disable</a></li>
                                    </ul>
                                </div>

                            </div>

                        </div>
                        <hr>
                        
                        <div class="control-group" style="">
                            <label class="control-label"  for="username">Notification Settings:</label>
                            <div class="controls">

                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_allowAlert_div"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="allowAlertM(1, '#_allowAlert_div', 'Enable')">Enable</a></li>
                                        <li><a href="#" onclick="allowAlertM(2, '#_allowAlert_div', 'Disable')">Disable</a></li>
                                    </ul>
                                </div>
                                for
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_allowAlertFor_div"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="allowAlertForM(1, '#_allowAlertFor_div', 'Operators')">Operators</a></li>
                                        <li><a href="#" onclick="allowAlertForM(2, '#_allowAlertFor_div', 'Users')">Users</a></li>
                                        <li><a href="#" onclick="allowAlertForM(0, '#_allowAlertFor_div', 'Both')">Both</a></li>
                                    </ul>
                                </div>
                                Via
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_gatewayType_div"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="AlertViaM(1, '#_gatewayType_div', 'SMS')">SMS</a></li>
                                        <li><a href="#" onclick="AlertViaM(2, '#_gatewayType_div', 'USSD')">USSD</a></li>
                                        <li><a href="#" onclick="AlertViaM(3, '#_gatewayType_div', 'VOICE')">VOICE</a></li>
                                        <li><a href="#" onclick="AlertViaM(4, '#_gatewayType_div', 'EMAIL')">EMAIL</a></li>
                                        <li><a href="#" onclick="AlertViaM(5, '#_gatewayType_div', 'FAX')">FAX</a></li>
                                        <li><a href="#" onclick="AlertViaM(18, '#_gatewayType_div', 'ANDROIDPUSH')">Android PUSH</a></li>
                                        <li><a href="#" onclick="AlertViaM(19, '#_gatewayType_div', 'IPHONEPUSH')">iOS PUSH</a></li>


                                    </ul>
                                </div>
                                With Attempts:
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_alertAttempt_div"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="AlertAttemptsM(1, '#_alertAttempt_div', '1 Attempts')">1 Attempts</a></li>
                                        <li><a href="#" onclick="AlertAttemptsM(2, '#_alertAttempt_div', '2 Attempts')">2 Attempts</a></li>
                                        <li><a href="#" onclick="AlertAttemptsM(3, '#_alertAttempt_div', '3 Attempts')">3 Attempts</a></li>
                                    </ul>
                                </div>
                                Using Template : <b>email.mobile.trust.alert</b>
                                <input type="hidden" name="_templateName" id="_templateName" value="email.mobile.trust.alert">
                            </div>

                        </div>
                        <hr>
                        <div class="control-group">
                            <div class="controls">
                                <div id="save-global-settings-result"></div>
                                <%if (obj != null) {%>
                                <button class="btn btn-primary" onclick="editTrustSettings()" type="button">Save Setting Now >> </button>
                                <%} else {%>
                                <button class="btn btn-primary" onclick="editTrustSettings()" disabled="true" type="button">Save Setting Now >> </button>
                                <%}%>
                            </div>
                        </div>
                    </form>
                </div>


                <script>


                    $(document).ready(function() {
                        $("#_blackListedCountries").select2()
                    });
                </script>



                <script language="javascript" type="text/javascript">
                    LoadTrustSettings();
                </script>
            </div>
            <div class="tab-pane" id="Mobile">
                <div class="row-fluid">
                    <form class="form-horizontal" id="mobiletrustsettingsformM" name="mobiletrustsettingsformM">
                        <div class="control-group">
                            <label class="control-label"  for="username">Self Destruct Alert URL:</label>
                            <div class="controls">
                                <input type="text" id="_selfDestructURl" name="_selfDestructURl" placeholder="Enter Url like www.abc.com" class="span3">                                    
                            </div>
                        </div>

                        <div class="control-group" id="mobile-trust-license">
                            <label class="control-label"  for="username">License Key For Android:</label>
                            <div class="controls">
                                <textarea id="_LicenseKeyforAndroid" name="_LicenseKeyforAndroid"  class="span9" rows="3" cols="20"></textarea>   
                            </div>
                        </div>
                        <hr>
                        <div class="control-group" id="mobile-trust-license">
                            <label class="control-label"  for="username">License Key For iOS:</label>
                            <div class="controls">
                                <textarea id="_LicenseKeyforIphone" name="_LicenseKeyforIphone"  class="span9" rows="3" cols="20"></textarea>   
                            </div>
                        </div>
                        <%//if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
                        <div class="control-group">
                            <div class="controls">
                                <div id="save-mobile-settings-result"></div>
                                <%if (obj != null) {%>
                                <button class="btn btn-primary" onclick="editTrustSettingsM()" type="button">Save Setting Now >> </button>
                                <%} else {%>
                                <button class="btn btn-primary" onclick="editTrustSettingsM()" disabled="true" type="button">Save Setting Now >> </button>
                                <%}%>
                            </div>
                        </div>

                        <%//}%>

                    </form>
                </div>
            </div>
            <div class="tab-pane" id="Web">
                <div class="row-fluid">
                    <form class="form-horizontal" id="mobiletrustsettingsformW" name="mobiletrustsettingsformW">
                        <div class="control-group">
                            <label class="control-label"  for="username">Self Destruct Alert URL:</label>
                            <div class="controls">
                                <input type="text" id="_selfDestructURlM" name="_selfDestructURlM" placeholder="Enter Url like www.abc.com" class="span3">                                    
                            </div>
                        </div>

                        <div class="control-group" id="mobile-trust-license">
                            <label class="control-label"  for="username">License Key For Web:</label>
                            <div class="controls">
                                <textarea id="_LicenseKeyforWeb" name="_LicenseKeyforWeb"  class="span9" rows="3" cols="20"></textarea>   
                            </div>
                        </div>
                          <div class="control-group" id="mobile-trust-domain">
                          <label class="control-label"  for="username">Allowed Domain Names</label>
                                <div class="controls">
                       <textarea name="_arrallowedDomainNames" id="_arrallowedDomainNames" style="width:55%">
                     
                        </textarea>
                          </div>
                          </div>
                        
                        <hr>
                        <!--                        <div class="control-group" id="mobile-trust-license">
                                                    <label class="control-label"  for="username">License Key For iOS:</label>
                                                    <div class="controls">
                                                        <textarea id="_LicenseKeyforIphone" name="_LicenseKeyforIphone"  class="span9" rows="3" cols="20"></textarea>   
                                                    </div>
                                                </div>-->
                        <%//if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
                        <div class="control-group">
                            <div class="controls">
                                <div id="save-mobile-settings-result"></div>
                                <%if (obj != null) {%>
                                <button class="btn btn-primary" onclick="editTrustSettingsW()" type="button">Save Setting Now >> </button>
                                <%} else {%>
                                <button class="btn btn-primary" onclick="editTrustSettingsW()" disabled="true" type="button">Save Setting Now >> </button>
                                <%}%>
                            </div>
                        </div>

                        <%//}%>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
                        <script>
//                             $(document).ready(function() {
//        $("#_arrallowedDomainNames").select2({
//            tags: [],
//            dropdownCss: {display: 'none'},
//            tokenSeparators: [","]
//        });
//    });
                            
                        </script>


<%@include file="footer.jsp" %>