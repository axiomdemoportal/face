
<%@page import="com.mollatech.axiom.nucleus.db.Pushmessagemappers"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.PushMessageManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Pushmessages"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.PushMessageUtils"%>
<%@page import="com.mollatech.axiom.nucleus.db.Isologs"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.IsoUtils"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>
<%@page import="com.mollatech.axiom.nucleus.settings.GlobalChannelSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/jquery.js"></script>
<script src="./assets/js/dictum/fxlistener.js"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/bootstrap-datepicker.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/css/datepicker.css">
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">
<%    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    PushMessageManagement push = new PushMessageManagement();
    String _sessionID = (String) session.getAttribute("_apSessionID");
    Pushmessages[] pushmsgs = push.ListPushmessages(_sessionID, channel.getChannelid());


%>

<div class="container-fluid">
    <h1 class="text-success">Listener Report</h1>
    <br>
    <div class="tabbable">
        <div class="tab-content">
            <div class="tab-pane active" id="fxmessage">


                <h3>Make Your Own Report</h3>

                <input type="hidden" id="_changeType" name="_changeType" value="0">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="control-group form-inline">

                            <div class="input-prepend" class="span6">
                                <span class="add-on">From:</span>   
                                <div id="datetimepicker1" class="input-append date">
                                    <input id="startdate"  name="startdate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth"/>
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="input-prepend">
                                <!--<div class="well">-->
                                <span class="add-on">till:</span>   
                                <div id="datetimepicker2" class="input-append date">
                                    <input id="enddate" name="enddate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <%                                if (pushmsgs != null) {
                            %>

                            <div class="input-prepend">
                                <span class="add-on">Caller:</span> 
                                <select name="_pushmessageSearch" id="_pushmessageSearch">
                                    <% for (int j = -1; j < pushmsgs.length; j++) {

                                            if (j == -1) {

                                    %>
                                    <option value="option">Select Call System?</option>                                             
                                    <%} else {%>
                                    <option value="<%=pushmsgs[j].getCallerName()%>"><%=pushmsgs[j].getCallerName()%></option>                                             
                                    <%}
                                        }%>
                                </select>
                                <div class="input-prepend">
                                    <span class="add-on">Message:</span> 
                                    <select name="_pushmappersSearch" id="_pushmappersSearch">
                                        <option>Select Message?</option>
                                    </select>
                                </div>
                            </div>

                            <% } else if (pushmsgs == null) {%>
                            <div class="input-prepend">
                                <span class="add-on">Caller::</span> 
                                <select name="_pushmessageSearch" id="_pushmessageSearch">

                                    <option value="option"> Select Call System?</option>                                             

                                </select>
                                <div class="input-prepend">
                                    <span class="add-on">Message:</span>     
                                    <select name="_pushmappersSearch" id="_pushmappersSearch">
                                        <option>Select Message?</option>
                                    </select>
                                </div>
                            </div>    

                            <%  }%>



                        </div>

                        <button class="btn btn-success" onclick="searchFXReport(1)" id="addUserButton">Generate Report >></button>
                        <div id="users_table_main_MSG">
                        </div>
                    </div>
                </div>


                <!--                <div id="SelectColumns" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                        <h3 id="myModalLabel">Select Columns</h3>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row-fluid">
                                            <form class="form-horizontal" id="selectColmnForm">
                                                <fieldset>
                                                     Name 
                                                    <div id="legend">
                                                        <input type="checkbox" id="_msgID" name ="_msgID" value="1">ID
                                                    </div>
                                                    <div id="legend">
                                                        <input type="checkbox" id="_typeS" name ="_typeS" value="2">Indicator
                                                    </div>
                                                    <div id="legend">
                                                        <input type="checkbox" id="_deliverTo" name ="_deliverTo" value="3">Caller
                                                    </div>
                                                    <div id="legend">
                                                        <input type="checkbox" id="_status" name ="_status" value="4"> Message Received
                                                    </div>
                                                    <div id="legend">
                                                        <input type="checkbox" id="_cost" name ="_cost" value="5"> Message Parsed
                                                    </div>
                                                    <div id="legend">
                                                        <input type="checkbox" id="_sentOn" name ="_sentOn" value="6"> Sent On
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div id="addUser-result"></div>
                                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                        <button class="btn btn-primary" onclick="searchFXRecords()" id="addUserButton">Generate Report Now</button>
                                    </div>
                                </div>-->


                <%;

//                        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                    int limit = 20;
                    int TRADITIONAL = 1;

                    SessionFactoryUtil supSession = new SessionFactoryUtil(SessionFactoryUtil.isologs);
                    Session spSession = supSession.openSession();
                    IsoUtils iUtils = new IsoUtils(supSession, spSession);

                    Isologs[] clogobj = iUtils.getALLMsgObj(channel.getChannelid(), limit, TRADITIONAL);

                    String strmsg = "No Record Found";

                    final int success = 1;
                    final int failure = 2;

                %>


                <div class="row-fluid">
                    <div id="licenses_data_table">                                                
                    </div>
                </div>

                <script type="text/javascript">
                    $(function () {
                        $('#datetimepicker1').datepicker({
                            format: 'dd/mm/yyyy',
                            language: 'pt-BR'
                        });
                    });
                    $(function () {
                        $('#datetimepicker2').datepicker({
                            format: 'dd/mm/yyyy',
                            language: 'pt-BR'

                        });
                    });
                    ChangeResponseType(1);
                </script>

            </div>


        </div>
    </div>


</div>

<%@include file="footer.jsp" %>