<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Interactionresponse"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.Interactions"%>
<%@page import="com.mollatech.dictum.management.SurveyManagement"%>
<div class="container-fluid">

    <%
        SurveyManagement survey = new SurveyManagement();

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String interactionId = request.getParameter("_interactionID");
        String _interactionExecutionID = request.getParameter("_interactionExecutionID");

 //       String createdOn = request.getParameter("_createdOn");
        //     String expireOn = request.getParameter("_expireOn");
        int iInteractionId = -9999;
        if (interactionId != null) {
            iInteractionId = Integer.parseInt(interactionId);
        }

        int iinteractionExecutionID = -9999;
        if (_interactionExecutionID != null) {
            iinteractionExecutionID = Integer.parseInt(_interactionExecutionID);
        }

        Interactionresponse iresponseObj = survey.getInteractionResponseObj(channel.getChannelid(), iInteractionId);

    %>
    <h1 class="text-success">Interaction Reports</h1>
    <h3>Make Your Own Report</h3> 

    <div class="tabbable">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#greetings" data-toggle="tab">Greetings</a></li>
            <li ><a href="#question1" data-toggle="tab">Question 1</a></li>
            <li ><a href="#question2" data-toggle="tab">Question 2</a></li>
            <li><a href="#question3" data-toggle="tab">Question 3</a></li>
            <li><a href="#question4" data-toggle="tab">Question 4</a></li>
            <li><a href="#question5" data-toggle="tab">Question 5</a></li>
            <li><a href="#dropout" data-toggle="tab">Summary</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="greetings">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="tab-pane active" id="msgcharts">
                            <%if (iresponseObj != null) {%>
                            <p><h3><em><strong><%=iresponseObj.getGreetings()%></strong></em></h3></p>
                            <%}%>
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">
                                        <div class="span4">
                                            <div id="greetingsDonutChart" ></div>
                                            <!--Donut Chart-->
                                        </div>
                                        <div class="span7">
                                            <div id="greetingsBarChart"></div>
                                            <!--Bar Chart-->   
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div id="SearchResponseButton">
                    <button class="btn btn-success" onclick="InteractionReportTable('<%=interactionId%>', '<%=iinteractionExecutionID%>', '<%=0%>')" type="button">Show Table >> </button>
                </div>
                <div id="HideSearchResponseButton">
                    <button class="btn btn-success" onclick="HidInteractionReportTable(0)" type="button">Hide Table << </button>
                </div>
                <div id="greetings_data_table">
                </div>
                <script>
//                        document.getElementById("greetings_data_table").style.display = 'none';
                </script>
            </div>
            <div class="tab-pane active" id="question1">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="tab-pane active" id="msgcharts">
                            <%if (iresponseObj != null) {%>
                            <p><h3><em><strong><%=iresponseObj.getQuestion1()%></strong></em></h3></p>
                            <%}%>
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">
                                        <div class="span4">
                                            <div id="question1DonutChart" ></div>
                                            <!--Donut Chart-->
                                        </div>
                                        <div class="span7">
                                            <div id="question1BarChart"></div>
                                            <!--Bar Chart-->   
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div id="Searchquestion1ResponseButton">
                    <button class="btn btn-success" onclick="InteractionReportTable('<%=interactionId%>', '<%=iinteractionExecutionID%>', '<%=1%>')" type="button">Show Table >> </button>
                </div>
                <div id="Hidequestion1ResponseButton">
                    <button class="btn btn-success" onclick="HidInteractionReportTable(1)" type="button">Hide Table << </button>
                </div>
                <div id="question1_data_table">
                </div>
            </div>

            <div class="tab-pane active" id="question2">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="tab-pane active" id="msgcharts">
                            <%if (iresponseObj != null) {%>
                            <p><h3><em><strong><%=iresponseObj.getQuestion2()%></strong></em></h3></p>
                            <%}%>
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">
                                        <div class="span4">
                                            <div id="question2DonutChart" ></div>
                                            <!--Donut Chart-->
                                        </div>
                                        <div class="span7">
                                            <div id="question2BarChart"></div>
                                            <!--Bar Chart-->   
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div id="Searchquestion2ResponseButton">
                    <button class="btn btn-success" onclick="InteractionReportTable('<%=interactionId%>', '<%=iinteractionExecutionID%>', '<%=2%>')" type="button">Show Table >> </button>
                </div>
                <div id="Hidequestion2ResponseButton">
                    <button class="btn btn-success" onclick="HidInteractionReportTable(2)" type="button">Hide Table << </button>
                </div>
                <div id="question2_data_table">
                </div>
            </div>

            <div class="tab-pane active" id="question3">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="tab-pane active" id="msgcharts">
                            <%if (iresponseObj != null) {%>
                            <p><h3><em><strong><%=iresponseObj.getQuestion3()%></strong></em></h3></p>
                            <%}%>
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">
                                        <div class="span4">
                                            <div id="question3DonutChart" ></div>
                                            <!--Donut Chart-->
                                        </div>
                                        <div class="span7">
                                            <div id="question3BarChart"></div>
                                            <!--Bar Chart-->   
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div id="Searchquestion3ResponseButton">
                    <button class="btn btn-success" onclick="InteractionReportTable('<%=interactionId%>', '<%=iinteractionExecutionID%>', '<%=3%>')" type="button">Show Table >> </button>
                </div>
                <div id="Hidequestion3ResponseButton">
                    <button class="btn btn-success" onclick="HidInteractionReportTable(3)" type="button">Hide Table << </button>
                </div>
                <div id="question3_data_table">
                </div>
            </div>


            <div class="tab-pane active" id="question4">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="tab-pane active" id="msgcharts">
                            <%if (iresponseObj != null) {%>
                            <p><h3><em><strong><%=iresponseObj.getQuestion4()%></strong></em></h3></p>
                            <%}%>
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">
                                        <div class="span4">
                                            <div id="question4DonutChart" ></div>
                                            <!--Donut Chart-->
                                        </div>
                                        <div class="span7">
                                            <div id="question4BarChart"></div>
                                            <!--Bar Chart-->   
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div id="Searchquestion4ResponseButton">
                    <button class="btn btn-success" onclick="InteractionReportTable('<%=interactionId%>', '<%=iinteractionExecutionID%>', '<%=4%>')" type="button">Show Table >> </button>
                </div>
                <div id="Hidequestion4ResponseButton">
                    <button class="btn btn-success" onclick="HidInteractionReportTable(4)" type="button">Hide Table << </button>
                </div>
                <div id="question4_data_table">
                </div>
            </div>

            <div class="tab-pane active" id="question5">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="tab-pane active" id="msgcharts">
                            <%if (iresponseObj != null) {%>
                            <p><h3><em><strong><%=iresponseObj.getQuestion5()%></strong></em></h3></p>
                            <%}%>
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">
                                        <div class="span4">
                                            <div id="question5DonutChart" ></div>
                                            <!--Donut Chart-->
                                        </div>
                                        <div class="span7">
                                            <div id="question5BarChart"></div>
                                            <!--Bar Chart-->   
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div id="Searchquestion5ResponseButton">
                    <button class="btn btn-success" onclick="InteractionReportTable('<%=interactionId%>', '<%=iinteractionExecutionID%>', '<%=5%>')" type="button">Show Table >> </button>
                </div>
                <div id="Hidequestion5ResponseButton">
                    <button class="btn btn-success" onclick="HidInteractionReportTable(5)" type="button">Hide Table << </button>
                </div>
                <div id="question5_data_table">
                </div>
            </div>

            <div class="tab-pane active" id="dropout">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="tab-pane active" id="msgcharts">
                            <%if (iresponseObj != null) {%>
                            <p><h3><em><strong><%="Summary"%></strong></em></h3></p>
                            <%}%>
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">
                                        <div class="span4">
                                            <div id="dropoutDonutChart" ></div>
                                            Donut Chart
                                        </div>
                                        <div class="span7">
                                            <div id="dropoutBarChart"></div>
                                            Bar Chart   
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
              
            </div>

        </div>
    </div>



</div>
<script>
//      document.getElementById("question1").style.display = 'none';
//    document.getElementById("question2").style.display = 'none';
//    document.getElementById("question3").style.display = 'none';
//    document.getElementById("question4").style.display = 'none';
//    document.getElementById("question5").style.display = 'none';
    document.getElementById("InteractionLable").style.display = 'none';
    document.getElementById("interactionList").style.display = 'none';
    document.getElementById("HideSearchResponseButton").style.display = 'none';
    document.getElementById("Hidequestion1ResponseButton").style.display = 'none';
    document.getElementById("Hidequestion2ResponseButton").style.display = 'none';
    document.getElementById("Hidequestion3ResponseButton").style.display = 'none';
    document.getElementById("Hidequestion4ResponseButton").style.display = 'none';
    document.getElementById("Hidequestion5ResponseButton").style.display = 'none';

</script>
<%--<%@include file="footer.jsp" %>--%>