<%@page import="com.mollatech.axiom.nucleus.settings.RootCertificateSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/caconnector.js"></script>
<script src="./assets/js/ajaxfileupload.js"></script>
<script src="./assets/js/bootstrap-fileupload.js"></script> 
<link href="./assets/css/bootstrap.css" rel="stylesheet">
<link href="./assets/css/bootstrap-fileupload.css" rel="stylesheet">
<script src="./assets/js/otptokens.js"></script>
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<script src="./assets/js/globalsetting.js"></script>
<div class="container-fluid">

    <%    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        String[] strTags = null;
        SettingsManagement smng = new SettingsManagement();
        RootCertificateSettings rCertSettings = (RootCertificateSettings) smng.getSetting(sessionid, channel.getChannelid(), SettingsManagement.RootConfiguration, 1);
        if (rCertSettings != null) {
            if (rCertSettings.CAEmailides != null) {
                strTags = rCertSettings.CAEmailides.split(",");
            }
        }
    %>
       <h1 class="text-success">Certificate Authority (CA) Connector Configuration</h2>
        <p>To facilitate certificate issuance from internal/external Certificate Authority, following setting need to be configured.  </p>
        <br>
       <div class="tabbable">
        <ul class="nav nav-tabs">
       <li class="active"><a href="#primary" data-toggle="tab">SelfSigned Certs</a></li>
            <li><a href="#secondary" data-toggle="tab">ThirdParty CA</a></li>
        </ul>
               <div class="tab-content">
            <div class="tab-pane active" id="primary">
              <div class="row-fluid">
            <form class="form-horizontal" id ="certificateform" >
                <fieldset>
                    <div class="control-group">
                        <label class="control-label"  for="certstatus">Status </label>
                        <input type="hidden" id="_statusCACERT" name="_statusCACERT">
                            <div class="controls">
                        <div class="btn-group">
                                    <button class="btn btn-small"><div id="_status-certificate"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ChangeActiveStatusCACert(1)">Mark as Active?</a></li>
                                        <li><a href="#" onclick="ChangeActiveStatusCACert(0)">Mark as Suspended?</a></li>
                                        
                                    </ul>
                                </div>
                            </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Host:Port</label>
                        <div class="controls">
                            <input type="text" id="_ip" name="_ip" placeholder="example localhost/127.0.0.1" class="input-xlarge">
                            : <input type="text" id="_port" name="_port" placeholder="443" class="span2">
                        </div>
                    </div>
                 <div class="control-group">
                        <label class="control-label" for="password">Authenticate Using </label>
                        <div class="controls">
                            <input type="text" id="_userId" name="_userId" placeholder=" userid" class="input-xlarge">
                            : <input type="password" id="_password" name="_password" placeholder="password" class="input-xlarge">
                        </div>
                    </div>

                    <div id="legend">
                        <legend class="">Configuration Attributes</legend>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="password">Upload PFX File </label>
                        <div class="controls">
                      <button href="#uploadpfx" class="btn btn-success" data-toggle="modal" id="savecrtsettings">Upload Now >></button>
                       <button class="btn btn-success" data-toggle="modal" id="btnViewCertSelfSigned" name="btnViewCertSelfSigned" onclick="ViewCert(1)" >View Certificate</button>
                       </div>
                    </div> 
                    <div class="control-group">
                        <label class="control-label"  for="username">CRL Link</label>
                        <div class="controls">
                            <input type="text" id="_crllink" name="_crllink" placeholder="name1=value1" class="span3">
                            Pulled Time
                            <select class="span2" name="_pulledtime" id="_pulledtime">
                                <option value="6">6 hours</option>
                                <option value="24">24 hours</option>
                                <option value="24*7">1 week</option>
                            </select>
                        </div> 
                        <!--<label class="control-label" for="password">Pulled Time</label>-->
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="password">Authenticate CRL path </label>
                        <div class="controls">
                            <input type="text" id="_crlusername" name="_crlusername" placeholder=" userid" class="input-xlarge">
                            : <input type="password" id="_crlpassword" name="_crlpassword" placeholder="password" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                       <label class="control-label"  for="username">OCSP</label>
<!--                        <div class="controls">-->
                      <input type="text" id="_ocspusername" name="_ocspusername" placeholder="Ocsp Username" class="input-xlarge">
                            : <input type="password" id="_ocsppassword" name="_ocsppassword" placeholder="Ocsp Password" class="input-xlarge">
                     
                       <input type="text" id="ocspurl" name="ocspurl" placeholder="name1=value1" class="span3">
                      
                      <button class="btn btn-success" data-toggle="modal" id="savecrtsettings" onclick="checkOcspUrl()">Check Status</button>
<!--                        </div> -->
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Email Address</label>
                        <div class="controls">
                            <textarea name="_tagsList" id="_tagsList" style="width:100%">
                                <%
                                    if (strTags != null)
                                        for (int j = 0; j < strTags.length; j++) {
                                %>        
                                <%=strTags[j]%>,
                                <%
                                    }
                                %>
                            </textarea>
                            <br>Attention: Please enter comma (",") for separating tags.
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="password">Validity Duration</label>
                        <div class="controls">
                            <select class="span2" name="_validityDays" id="_validityDays">
                                <option value="30">30 days</option>
                                <option value="365">1 year</option>
                                <option value="730">2 years</option>
                                <option value="1095">3 years</option>
                                 <option value="1460">4 years</option>
                                  <option value="1825">5 years</option>
                                   <option value="2190">6 years</option>
                                    <option value="2555">7 years</option>
                                     <option value="2920">8 years</option>
                                      <option value="3285">9 years</option>
                                      <option value="3650">10 years</option>
                            </select> 
                            Key Length
                            <select class="span2" name="_keyLength" id="_keyLength">
                                <option value="1024">1024</option>
                                <option value="2048">2048</option>
                                <option value="4096">4096</option>
                            </select>
                        </div> 
                    </div>
                    <div class="control-group" style="display:none">
                        <label class="control-label" for="password">KYC</label>
                        <div class="controls">
                            <select class="span2" name="_statuskyc" id="_statuskyc">
                                <option value="0">Enable</option>
                                <option value="0">Disable</option>
                            </select> 
                            Alert Using
                            <select class="span2" name="_alertsource" id="_alertsource">
                                <option value="1">SMS</option>
                                <option value="2">Email</option>
                                <option value="3">SMS&Email</option>
                            </select>
                        </div> 
                    </div>


                    <div class="control-group">
                        <label class="control-label"  for="username">Additional Attributes</label>
                        <div class="controls">
                            <input type="text" id="_reserve1" name="_reserve1" placeholder="name1=value1" class="span3">
                            : <input type="text" id="_reserve2" name="_reserve2" placeholder="name2=value2" class="span3">
                            : <input type="text" id="_reserve3" name="_reserve3" placeholder="name3=value3" class="span3">
                        </div>
                    </div>
                    <hr>
                    <div class="control-group">
                        <label class="control-label"  for="username">Implementation </label>
                        <div class="controls">
                            <input type="text" id="_className" name="_className" placeholder="complete class name including package" class="input-xlarge">
                        </div>
                    </div>

                    <div class="control-group">
                        <div class="controls">
                            <div id="savecrtsettings-result"></div>
                            <% //if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) { %>                                
          <button class="btn btn-primary" onclick="editcasettings()" type="button" id="savecrtsettings">Save Settings Now >></button>                        
                            <%//}%>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
            </div>
             <div class="tab-pane" id="secondary">  
                  <div class="row-fluid">
            <form class="form-horizontal" id ="certificateform" >
                <fieldset>
                    <div class="control-group">
                        <label class="control-label"  for="certstatusTP">Status </label>
                        <input type="hidden" id="_statusCACERTTP" name="_statusCACERTTP">
                            <div class="controls">
                            <div class="btn-group">
                                    <button class="btn btn-small"><div id="_status-certificateTP"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ChangeActiveStatusCACert1(1)">Mark as Active?</a></li>
                                        <li><a href="#" onclick="ChangeActiveStatusCACert1(0)">Mark as Suspended?</a></li>
                                        
                                    </ul>
                                </div>
                            </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Host:Port</label>
                        <div class="controls">
                            <input type="text" id="_ipTP" name="_ipTP" placeholder="example localhost/127.0.0.1" class="input-xlarge">
                            : <input type="text" id="_portTP" name="_portTP" placeholder="443" class="span2">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="password">Authenticate Using </label>
                        <div class="controls">
                            <input type="text" id="_userIdTP" name="_userId" placeholder=" useridTP" class="input-xlarge">
                            : <input type="password" id="_passwordTP" name="_passwordTP" placeholder="password" class="input-xlarge">
                        </div>
                    </div>
                      <div id="legend">
                        <legend class="">Configuration Attributes</legend>
                    </div>
                
                    <div class="control-group">
                      <label class="control-label" for="password">OR&nbsp;&nbsp; UPload Pfx File</label>
                      
                        <div class="controls">
                     <button href="#uploadpfx" class="btn btn-success" data-toggle="modal" id="savecrtsettings">Upload Now >></button>   
                      <button class="btn btn-success" data-toggle="modal" id="btnViewCertSelfSigned" name="btnViewCertSelfSigned" onclick="ViewCert(2)" >View Certificate</button>
                        </div>
                    </div> 
                  

<!--                    <div class="control-group">
                        <label class="control-label" for="password">Upload PFX File </label>
                        <div class="controls">
                            <button href="#uploadpfx" class="btn btn-success" data-toggle="modal" id="savecrtsettings">Upload Now >></button>
                        </div>
                    </div> -->
                    <div class="control-group">
                        <label class="control-label"  for="username">CRL Link</label>
                        <div class="controls">
                            <input type="text" id="_crllinkTP" name="_crllinkTP" placeholder="name1=value1" class="span3">
                            Pulled Time
                            <select class="span2" name="_pulledtimeTP" id="_pulledtimeTP">
                                <option value="6">6 hours</option>
                                <option value="24">24 hours</option>
                                <option value="24*7">1 week</option>
                            </select>
                        </div> 
                        <!--<label class="control-label" for="password">Pulled Time</label>-->
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="password">Authenticate CRL path </label>
                        <div class="controls">
                            <input type="text" id="_crlusernameTP" name="_crlusernameTP" placeholder=" userid" class="input-xlarge">
                        : <input type="password" id="_crlpasswordTP" name="_crlpasswordTP" placeholder="password" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">OCSP</label>
                        <div class="controls">
                            <input type="text" id="ocspurlTP" name="ocspurlTP" placeholder="name1=value1" class="span3">
                            <button href="#addNewUser" class="btn btn-success" data-toggle="modal" id="savecrtsettings" onclick="checkOcspUrl()">Check Status</button>
                        </div> 
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Email Address</label>
                        <div class="controls">
                            <textarea name="_tagsListTP" id="_tagsListTP" style="width:100%">
                                <%
                                    if (strTags != null)
                                        for (int j = 0; j < strTags.length; j++) {
                                %>        
                                <%=strTags[j]%>,
                                <%
                                    }
                                %>
                            </textarea>
                            <br>Attention: Please enter comma (",") for separating tags.
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="password">Validity Duration</label>
                        <div class="controls">
                            <select class="span2" name="_validityDaysTP" id="_validityDaysTP">
                                <option value="30">30 days</option>
                                <option value="365">1 year</option>
                                <option value="730">2 years</option>
                                <option value="1095">3 years</option>
                                 <option value="1460">4 years</option>
                                  <option value="1825">5 years</option>
                                   <option value="2190">6 years</option>
                                    <option value="2555">7 years</option>
                                     <option value="2920">8 years</option>
                                      <option value="3285">9 years</option>
                                      <option value="3650">10 years</option>
                            </select> 
                            Key Length
                            <select class="span2" name="_keyLengthTP" id="_keyLengthTP">
                                <option value="1024">1024</option>
                                <option value="2048">2048</option>
                                  <option value="2048">4096</option>
                            </select>
                        </div> 
                    </div>
                            <div class="control-group" style="display:none">
                        <label class="control-label" for="password">KYC</label>
                        <div class="controls">
                            <select class="span2" name="_statuskycTP" id="_statuskycTP">
                                <option value="1">Enable</option>
                                <option value="0">Disable</option>
                            </select> 
                            Alert Using
                            <select class="span2" name="_alertsourceTP" id="_alertsourceTP">
                                <option value="1">SMS</option>
                                <option value="2">Email</option>
                                <option value="3">SMS&Email</option>
                            </select>
                        </div> 
                    </div>


                  <div class="control-group" style="display:none">
                        <label class="control-label"  for="username">Additional Attributes</label>
                        <div class="controls">
                            <input type="text" id="_reserve1TP" name="_reserve1TP" placeholder="name1=value1" class="span3">
                            : <input type="text" id="_reserve2TP" name="_reserve2TP" placeholder="name2=value2" class="span3">
                            : <input type="text" id="_reserve3TP" name="_reserve3TP" placeholder="name3=value3" class="span3">
                        </div>
                    </div>
                    <hr>
                    <div class="control-group">
                        <label class="control-label"  for="username">Implementation </label>
                        <div class="controls">
                            <input type="text" id="_classNameTP" name="_classNameTP" placeholder="complete class name including package" class="input-xlarge">
                        </div>
                    </div>

                    <div class="control-group">
                        <div class="controls">
                            <div id="savecrtsettings-result"></div>
                            <% //if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) { %>                                
                            <button class="btn btn-primary" onclick="editcasettings1()" type="button" id="savecrtsettings">Save Settings Now >></button>                        
                            <%//}%>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
             </div></div>
</div>
<div id="uploadpfx" class="modal hide fade" tabindex="-1" role="dialog" style="width: 800px;"   aria-labelledby="myModalLabel"  aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">PFX File upload details</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="fileppfxuploadform" name="fileppfxuploadform">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">PFX Password</label>
                        <div class="controls">
                            <input type="password" id="_pfxpassword" name="_pfxpassword" placeholder="xxxxxxx" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">PFX Alias</label>
                        <div class="controls">
                            <input type="text" id="_pfxalias" name="_pfxalias" placeholder="xxxxxxx" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Select File:</label>                                    
                        <div class="controls fileupload fileupload-new" data-provides="fileupload">
                            <div class="input-append">
                                <div class="uneditable-input span4"><i class="icon-file fileupload-exists"></i> 
                                    <span class="fileupload-preview"></span>
                                </div>
                                <span class="btn btn-file" >
                                    <span class="fileupload-new">Select file</span>
                                    <span class="fileupload-exists">Change</span>
                                    <input type="file" id="fileppfxupload" name="fileppfxupload"/>
                                </span>
                                  <div id="addResource-result"></div>
                                  
                                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                           <button class="btn btn-success" id="buttonUpload"  data-toggle="modal" onclick="UploadFile()" >Upload Now>></button>
                                
                            </div>
                        </div>

                    </div>

                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="addUser-result"></div>
          
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="uploadpfxdetails()" id="addUserButton">Save Details Now>></button>
    </div>
</div>
                        
         <div id="certificateDetails" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3 id="myModalLabel">Certificate Details</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="certform">
                    <fieldset>
                        <div class="control-group" style="display: none">
                            <label class="control-label"  for="username">Serial Number</label>
                            <div class="controls">
                                <input type="text" id="_srnoCertifiacte" name="_srnoCertifiacte" class="span4">   
                                & Algorithm: <input type="text" id="_algoname" name="_algoname" class="span4">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">User Details</label>
                            <div class="controls">
                                <textarea id="_subjectdn" name="_subjectdn" style="width:95%"></textarea>
                                <!--<input type="text" id="_subjectdn" name="_subjectdn"  class="input-xlarge">-->
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Issuer Details</label>
                            <div class="controls">
                                <textarea id="_issuerdn" name="_issuerdn" style="width:95%"></textarea>
                                <!--<input type="text" id="_issuerdn" name="_issuerdn"  class="input-xlarge">-->
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Issued On</label>
                            <div class="controls">
                                <input type="text" id="_notbefore" name="_notbefore"  class="input-xlarge">

                            </div>
                        </div>                    
                        <div class="control-group">
                            <label class="control-label"  for="username">Valid till</label>
                            <div class="controls">
                                <input type="text" id="_notafter" name="_notafter"  class="input-xlarge">
                            </div>
                        </div>                    


                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
    </div>
                        

<script>
//$(document).ready(function() { $("#_keywords").select2() }); 

    $(document).ready(function() {
        $("#_tagsList").select2({
            tags: [],
            dropdownCss: {display: 'none'},
            tokenSeparators: [","]
        });
    });
    
     $(document).ready(function() {
        $("#_tagsListTP").select2({
            tags: [],
            dropdownCss: {display: 'none'},
            tokenSeparators: [","]
        });
    });
    
</script>

<script type="text/javascript">
      certificatesetting();
      certificatesetting1();
</script>


<%@include file="footer.jsp" %>