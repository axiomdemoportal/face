<%@page import="com.mollatech.axiom.nucleus.db.Units"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet"%>
<%@include file="header.jsp" %>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="./assets/css/datepicker.css">
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">
<div class="container-fluid">
    <div id="auditTable">
        <h1 class="text-success">Operator Management</h1>
        <p>You can manage all your operators, their roles and password management. The "admin" is the most powerful operator followed with "helpdesk" to manage daily request and the "reporter" can read-only operator role for pulling reports of the system. </p>
        <br>
        <div class="row-fluid">
            <div id="licenses_data_table">
                <table class="table table-striped">
                    <tr>
                        <td>No.</td>
                        <td>Name</td>
                        <td>Mobile</td>
                        <td>Email</td>
                        <td>Unit</td>
                        <td>Status</td><!--
                        <td>Audit</td>-->
                        <td>Created</td>
                        <td>Last Access</td>
                    </tr>

                    <%
                        UnitsManagemet uMngt = new UnitsManagemet();

                        Channels _apSChannelDetails = (Channels) session.getAttribute("_apSChannelDetails");
                        OperatorsManagement oManagement = new OperatorsManagement();
                        String _sessionID = (String) session.getAttribute("_apSessionID");
                        Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
                        SessionManagement smObj = new SessionManagement();
                        int iStatus = smObj.GetSessionStatus(_sessionID);

                        String _status = request.getParameter("_status");
                        String _unitId = request.getParameter("_unitId");
                        AxiomOperator[] axiomoperatorObj = null;
                        //                    Roles roleObj = oManagement.getRoleByRoleId(_apSChannelDetails.getChannelid(), operator.getRoleid());
                        //                    if (_status == null && _unitId == null) {
                        //                        if (iStatus == 1) { //active
                        //                            axiomoperatorObj = oManagement.ListOperatorsInternal(_apSChannelDetails.getChannelid());
                        //                        }
                        //                    } else {
                        //                        int iUnitId = Integer.parseInt(_unitId);
                        //                        int istatus = Integer.parseInt(_status);
                        //                        axiomoperatorObj = oManagement.getOperatorByUnitStatus(sessionid, _apSChannelDetails.getChannelid(), iUnitId, istatus);
                        //                    }
                        //                    axiomoperatorObj = oManagement.GetOperatorByRoleId(_apSChannelDetails.getChannelid(), OperatorsManagement.Requester);
                        axiomoperatorObj = oManagement.GetOperatorByTypeV2(_apSChannelDetails.getChannelid(), OperatorsManagement.REQUESTER, OperatorsManagement.AUTHORIZER);
                        if (axiomoperatorObj != null) {
                            for (int i = 0; i < axiomoperatorObj.length; i++) {
                                AxiomOperator axoprObj = axiomoperatorObj[i];
                                java.util.Date dLA = new java.util.Date(axoprObj.getLastUpdateOn());
                                java.util.Date dCR = new java.util.Date(axoprObj.getUtcCreatedOn());

                                int iOprStatus = axoprObj.getiStatus();
                                String strStatus;
                                if (iOprStatus == 1) {
                                    strStatus = "Active";
                                } else {
                                    strStatus = "Suspended";
                                }
                                if (axoprObj.getiStatus() == 1) {
                                    status = "Active";
                                }
                                if (axoprObj.getiStatus() == OperatorsManagement.LOCKED_STATUS) {
                                    status = "Locked";
                                }
                                if (axoprObj.getiStatus() == OperatorsManagement.SUSPEND_STATUS) {
                                    status = "Suspended";
                                }

                                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm ");

                                String uidiv4OprStatus = "operator-status-value-" + i;

                                String uidiv4OprRole = "operator-role-value-" + i;

                                String uidiv4OprAttempts = "operator-attempts-value-" + i;

                                String unitName = "Not Found";

                                Units uN = uMngt.getUnitByUnitId(sessionid, _apSChannelDetails.getChannelid(), axoprObj.getiUnit());
                                if (uN != null) {
                                    unitName = uN.getUnitname();
                                }


                    %>
                    <tr>
                        <td><%=i + 1%></td>
                        <td><%=axoprObj.getStrName()%></td>
                        <td><%=axoprObj.getStrPhone()%></td>
                        <td><%=axoprObj.getStrEmail()%></td>
                        <td><%=unitName%></td>
                        <td><%=status%>
                            <!--                        <div class="btn-group">
                                                        <button class="btn btn-mini" id="<%=uidiv4OprStatus%>"><%=strStatus%></button>
                                                        <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="ChangeOperatorStatus('<%=axoprObj.getStrOperatorid()%>', 1, '<%=uidiv4OprStatus%>')">Mark as Active?</a></li>
                                                            <li><a href="#" onclick="ChangeOperatorStatus('<%=axoprObj.getStrOperatorid()%>', 0, '<%=uidiv4OprStatus%>')">Mark as Suspended?</a></li>
                            
                                                        </ul>
                                                    </div>-->
                        </td>


                        <!--                    <td>
                                                <a href="#" class="btn btn-mini" onclick="loadEditRequesterOperatorDetails('<%=axoprObj.getStrOperatorid()%>')">Audit Download</a>
                                                 <button class="btn" id="requester-<%=i%>" onclick="loadEditRequesterOperatorDetails(<%=axoprObj.getStrOperatorid()%>)" type="button">Download Audit</button>
                                                                        <div class="btn-group">
                                                                            <a href="#" class="btn btn-mini" id="emailTemplateSubject-<%=i%>" onclick="loadEditRequesterOperatorDetails(<%=axoprObj.getStrOperatorid()%>)" rel="popover" data-html="true">Audit</a>
                                                                        </div>
                                            </td>-->
                        <td><%=sdf.format(dCR)%></td>
                        <td><%=sdf.format(dLA)%></td>
                    </tr>
                    <%}
                    } else {%>
                    <td>1</td>
                    <td>No Record</td>
                    <td>No Record</td>
                    <td>No Record</td>
                    <td>No Record</td>
                    <td>No Record</td>
                    <td>No Record</td>
                    <td>No Record</td>
                    <td>No Record</td>
                    <%}%>
                </table>


            </div>
        </div>
    </div>
    <br>
    <!--    <p><a href="#addOperator" class="btn btn-primary" data-toggle="modal">Add New Operator&raquo;</a></p>-->
    <script language="javascript">
        //listChannels();
    </script>
</div>

<div id="auditDownload" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idauditDownload"></div></h3>
        <h3 id="myModalLabel">Download Audit</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="testSMSPrimaryForm" name="testSMSPrimaryForm">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_oprnameR" name="_oprnameR"/>
                    <input type="hidden" id="_opridR" name="_opridR"/>
                    <div class="control-group">
                        <label class="control-label"  for="username">Start Date</label>
                        <div class="controls" align="left" >

                            <!--<span class="add-on">From:</span>-->   
                            <div id="Pushdatetimepicker1" class="input-append date">
                                <input id="_auditStartDate" name="_auditStartDate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">End Date</label>
                        <div class="controls" align="left">
                            <!--<span class="add-on">Till:</span>-->   
                            <div id="Pushdatetimepicker2" class="input-append date">
                                <input id="_auditEndDate" name="_auditEndDate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>                    
                </fieldset>
            </form>
        </div>
    </div>

    <div class="modal-footer">
        <div id="editoperator-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="searchAudit()" id="buttonEditOperatorSubmit">Show Audit</button>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('#Pushdatetimepicker1').datepicker({
            format: 'dd/MM/yyyy',
            language: 'pt-BR'
        });
    });
    $(function () {
        $('#Pushdatetimepicker2').datepicker({
            format: 'dd/MM/yyyy',
            language: 'pt-BR'
        });
    });
//                            ChangeMediaType(0);
</script>
<%@include file="footer.jsp" %>
