
<%@page import="com.mollatech.axiom.nucleus.db.Operators"%>
<%@page import="com.mollatech.axiom.connector.access.controller.AccessMatrixSettings"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<script src="assets/js/usermanagement.js" type="text/javascript"></script>
<%
    final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;
    final int LOCKED_STATUS = -1;
    final int REMOVE_STATUS = -99;
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _searchtext = request.getParameter("_searchtext");
    String _changeStatus = request.getParameter("_status");
    String _fromDate = request.getParameter("_fromDate");
    String _toDate = request.getParameter("_toDate");
    Date dstartdate = null;
    Date denddate = null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    if (_fromDate != null && _toDate != null && !_fromDate.equals("") && !_toDate.equals("")) {
        dstartdate = sdf.parse(_fromDate);
        denddate = sdf.parse(_toDate);
    }
    int istatus = Integer.parseInt(_changeStatus);
    UserManagement usermngObj = new UserManagement();
    AuthUser user[] = null;
    //user = usermngObj.SearchUserByStatus(sessionId, channel.getChannelid(), _searchtext, istatus);
    user = usermngObj.SearchUserByStatusBetDate(sessionId, channel.getChannelid(), _searchtext, istatus, dstartdate, denddate);
    session.setAttribute("inventoryUserReport", user);
    String strerr = "No Record Found";
%>
<h3>Searched Results for <i>"<%=_searchtext%>"</i></h3>

<div class="tabbable">
    <ul class="nav nav-tabs" id="userReportTab">
        <li class="active"><a href="#usercharts" data-toggle="tab">Charts</a></li>
        <li><a href="#userreport" data-toggle="tab">Tabular List</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="usercharts">
            <div class="row-fluid">
                <div class="span12">
                    <div class="control-group">
                        <div class="span4">
                            <div id="UserReportgraph" ></div>

                        </div>
                        <div  class="span8">
                            <div id="UserReportgraph1"></div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="tab-pane" id="userreport">            
            <div class="row-fluid">
                <div class="span6">
                    <div class="control-group">                        
                        <div class="span1">
                            <div class="control-group form-inline">
                                <%Operators oprObjI = (Operators) session.getAttribute("_apOprDetail");
                                    AccessMatrixSettings accessObjN = (AccessMatrixSettings) session.getAttribute("_apAccessEntry");
                                    if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                        if (accessObjN != null && accessObjN.downloadUserReport == true) {%>
                                <a href="#" class="btn btn-info" onclick="UserReportDownload(1)" >
                                    <%} else {%>
                                    <a href="#" class="btn btn-info" onclick="InvalidRequest('userreportDownload')" >
                                        <%}
                                        } else {%>
                                        <a href="#" class="btn btn-info" onclick="UserReportDownload(1)" >
                                            <%}%>
                                            <i class="icon-white icon-chevron-down"></i> CSV</a>
                                        </div>
                                        </div>
                                        <div class="span1">
                                            <div class="control-group form-inline">
                                                <%    if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                        if (accessObjN != null && accessObjN.downloadUserReport == true) {%>
                                                <a href="#" class="btn btn-info" onclick="UserReportDownload(0)" >
                                                    <%} else {%>
                                                    <a href="#" class="btn btn-info" onclick="InvalidRequest('userreportDownload')" >
                                                        <%}
                                                        } else {%>
                                                        <a href="#" class="btn btn-info" onclick="UserReportDownload(0)" >
                                                            <%}%>
                                                            <i class="icon-white icon-chevron-down"></i> PDF</a>
                                                        </div>
                                                        </div>
                                                        <div class="span1">
                                                            <div class="control-group form-inline">
                                                                <!--                                    <a href="#" class="btn btn-info" onclick="userReportTXT()" >
                                                                                                        <i class="icon-white icon-chevron-down"></i>Download TXT</a>-->
                                                                <%    if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                        if (accessObjN != null && accessObjN.downloadUserReport == true) {%>
                                                                <a href="#" class="btn btn-info" onclick="UserReportDownload(2)" >
                                                                    <%} else {%>
                                                                    <a href="#" class="btn btn-info" onclick="InvalidRequest('userreportDownload')" >
                                                                        <%}
                                                                        } else {%>
                                                                        <a href="#" class="btn btn-info" onclick="UserReportDownload(2)" >
                                                                            <%}%>
                                                                            <i class="icon-white icon-chevron-down"></i> TXT</a>
                                                                        </div>
                                                                        </div>

                                                                        </div>
                                                                        </div>
                                                                        </div>
                                                                        <table class="table table-striped" id="table_main">
                                                                            <tr>
                                                                                <td>No.</td>
                                                                                <td>User ID</td>
                                                                                <td>Name</td>
                                                                                <td>Mobile</td>
                                                                                <td>Email</td>
                                                                                <td>Status</td>
                                                                                <td>Created On</td>
                                                                                <td>Last Access On</td>
                                                                            </tr>

                                                                            <%
                                                                                if (user != null) {

                                                                                    for (int i = 0; i < user.length; i++) {

                                                                                        String strStatus = null;
                                                                                        if (user[i].getStatePassword() == ACTIVE_STATUS) {
                                                                                            strStatus = "ACTIVE";
                                                                                        } else if (user[i].getStatePassword() == LOCKED_STATUS) {
                                                                                            strStatus = "LOCKED";
                                                                                        } else if (user[i].getStatePassword() == SUSPEND_STATUS) {
                                                                                            strStatus = "SUSPEND";
                                                                                        } else if (user[i].getStatePassword() == REMOVE_STATUS) {
                                                                                            strStatus = "REMOVED";
                                                                                        }
                                                                                        Date date = new Date(user[i].lCreatedOn);
                                                                                        SimpleDateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                                                                                        String strCreatedOn = df2.format(date);

                                                                                        Date date1 = new Date(user[i].lLastAccessOn);
                                                                                        String strLastAccessOn = df2.format(date1);
                                                                            %>

                                                                            <tr>
                                                                                <td><%=(i + 1)%></td>
                                                                                <td><%= user[i].getUserId()%></td>
                                                                                <td><%= user[i].getUserName()%></td>
                                                                                <td><%=user[i].getPhoneNo()%></td>
                                                                                <td><%=user[i].getEmail()%></td>
                                                                                <td><%=strStatus%></td>
                                                                                <td><%=strCreatedOn%></td>
                                                                                <td><%=strLastAccessOn%></td>

                                                                            </tr>
                                                                            <%}
                                                                            } else {%>
                                                                            <tr>
                                                                                <td><%=1%></td>
                                                                                <td><%= strerr%></td>
                                                                                <td><%= strerr%></td>
                                                                                <td><%= strerr%></td>
                                                                                <td><%= strerr%></td>
                                                                                <td><%= strerr%></td>
                                                                                <td><%= strerr%></td>
                                                                                <td><%= strerr%></td>
                                                                            </tr>
                                                                            <%}%>
                                                                        </table>

                                                                        <div class="row-fluid">
                                                                            <div class="span6">
                                                                                <!--                    <div class="control-group">
                                                                                
                                                                                                        <div class="span3">
                                                                                                            <div class="control-group form-inline">
                                                                                                                <a href="#" class="btn btn-info" onclick="userReportCSV()" >
                                                                                                                    <i class="icon-white icon-chevron-down"></i>Download CSV</a>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="span3">
                                                                                                            <div class="control-group form-inline">
                                                                                                                <a href="#" class="btn btn-info" onclick="userReportPDF()" >
                                                                                                                    <i class="icon-white icon-chevron-down"></i>Download PDF</a>
                                                                                                            </div>
                                                                                                        </div>
                                                                                
                                                                                                        <div class="span3">
                                                                                                            <div class="control-group form-inline">
                                                                                                                <a href="#" class="btn btn-info" onclick="userReportTXT()" >
                                                                                                                    <i class="icon-white icon-chevron-down"></i>Download TXT</a>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>-->
                                                                                <div class="control-group">                        
                                                                                    <div class="span1">
                                                                                        <div class="control-group form-inline">
                                                                                            <%  if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                    if (accessObjN != null && accessObjN.downloadUserReport == true) {%>
                                                                                            <a href="#" class="btn btn-info" onclick="UserReportDownload(1)" >
                                                                                                <%} else {%>
                                                                                                <a href="#" class="btn btn-info" onclick="InvalidRequest('userreportDownload')" >
                                                                                                    <%}
                                                                                                    } else {%>
                                                                                                    <a href="#" class="btn btn-info" onclick="UserReportDownload(1)" >
                                                                                                        <%}%>
                                                                                                        <i class="icon-white icon-chevron-down"></i> CSV</a>
                                                                                                    </div>
                                                                                                    </div>
                                                                                                    <div class="span1">
                                                                                                        <div class="control-group form-inline">
                                                                                                            <%    if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                                    if (accessObjN != null && accessObjN.downloadUserReport == true) {%>
                                                                                                            <a href="#" class="btn btn-info" onclick="UserReportDownload(0)" >
                                                                                                                <%} else {%>
                                                                                                                <a href="#" class="btn btn-info" onclick="InvalidRequest('userreportDownload')" >
                                                                                                                    <%}
                                                                                                                    } else {%>
                                                                                                                    <a href="#" class="btn btn-info" onclick="UserReportDownload(0)" >
                                                                                                                        <%}%>
                                                                                                                        <i class="icon-white icon-chevron-down"></i> PDF</a>
                                                                                                                    </div>
                                                                                                                    </div>
                                                                                                                    <div class="span1">
                                                                                                                        <div class="control-group form-inline">
                                                                                                                            <!--                                    <a href="#" class="btn btn-info" onclick="userReportTXT()" >
                                                                                                                                                                    <i class="icon-white icon-chevron-down"></i>Download TXT</a>-->
                                                                                                                            <%    if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                                                    if (accessObjN != null && accessObjN.downloadUserReport == true) {%>
                                                                                                                            <a href="#" class="btn btn-info" onclick="UserReportDownload(2)" >
                                                                                                                                <%} else {%>
                                                                                                                                <a href="#" class="btn btn-info" onclick="InvalidRequest('userreportDownload')" >
                                                                                                                                    <%}
                                                                                                                                    } else {%>
                                                                                                                                    <a href="#" class="btn btn-info" onclick="UserReportDownload(2)" >
                                                                                                                                        <%}%>
                                                                                                                                        <i class="icon-white icon-chevron-down"></i> TXT</a>
                                                                                                                                    </div>
                                                                                                                                    </div>

                                                                                                                                    </div>
                                                                                                                                    </div>
                                                                                                                                    </div>

                                                                                                                                    </div>    
                                                                                                                                    </div>

                                                                                                                                    </div>

                                                                                                                                    <br>
                                                                                                                                    <br>
