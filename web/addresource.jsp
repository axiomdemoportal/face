<%@page import="com.mollatech.axiom.nucleus.db.Usergroups"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserGroupsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Webresource"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.ResourceUtils"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.ResourceManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/pdfsigning.js"></script>
<script src="./assets/js/json_sans_eval.js"></script>
<script src="./assets/js/bootbox.min.js"></script>
<script src="./assets/js/ajaxfileupload.js"></script>
<script src="./assets/js/bootstrap-fileupload.js"></script> 
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<script src="./assets/js/usermanagement.js"></script>
<link href="./assets/css/bootstrap.css" rel="stylesheet">
<link href="./assets/css/bootstrap-fileupload.css" rel="stylesheet">
<style type="text/css">
    <%    String _sessionID = (String) session.getAttribute("_apSessionID");
        SessionManagement smObj = new SessionManagement();
        int iStatus = smObj.GetSessionStatus(_sessionID);
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        OperatorsManagement oMngt = new OperatorsManagement();
    %>
    body {
        padding-top: 60px;
        padding-bottom: 40px;
    }

    #signatureparent {
        color:darkblue;

    }
    #signature {
        alignment-adjust: central;
        width:25%;
        hight:25%;
        border: 2px dotted black;
        background-color:lightgrey;
    }
    /*
    */    
    html.touch #content {
        alignment-adjust: central;
        float:center;
        width:25%;
        hight:20%;
    }

</style> 
<div class="container-fluid">
    <h2 class="text-success">Add New Resource</h2>
    <div class="row-fluid">
        <form class="form-horizontal" id="addResourceForm">
            <fieldset>
                <!-- Name -->
                <div class="control-group">
                    <label class="control-label"  for="resourcename">Name</label>
                    <div class="controls">
                        <input type="text" id="_Name" name="_Name" placeholder="resource name" class="input-xlarge">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"  for="weburl">URL</label>
                    <div class="controls">
                        <input type="text" id="_URL" name="_URL" placeholder="Website URL" class="input-xlarge">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"  for="status">Status</label>
                    <div class="controls"  class="input-large">                               
                        <select name="_Status" id="_Status">
                            <option value="active">Active</option>
                            <option value="Suspended">Suspended</option>
                        </select>
                    </div>
                </div>

                <div class="controls-row" id="licenses">
                    <div class="row-fluid">
                        <form class="form-horizontal" id="uploadXMLFormEAD" name="uploadXMLFormEAD">
                            <fieldset>

                                <div class="control-group">
                                    <label class="control-label"  for="username">Select File:</label>                                    
                                    <div class="controls fileupload fileupload-new" data-provides="fileupload">
                                        <div class="input-append">
                                            <div class="uneditable-input span4"><i class="icon-file fileupload-exists"></i> 
                                                <span class="fileupload-preview"></span>
                                            </div>
                                            <span class="btn btn-file" >
                                                <span class="fileupload-new">Select file</span>
                                                <span class="fileupload-exists">Change</span>
                                                <input type="file" id="fileImageToUpload" name="fileImageToUpload"/>
                                            </span>
                                            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        </div>
                                        <button class="btn btn-success" id="buttonUploadImage"  onclick="UploadImageFile()" type="button">Upload Now</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>

                    <div class="control-group">
                        <label class="control-label"  for="group">Group</label>
                        <div class="controls"  class="input-large">                               
                            <select name="_groupName" id="_groupName" multiple class="span3">
                                <%UserGroupsManagement ugmngt = new UserGroupsManagement();
                                    Usergroups[] ugroup = null;
                                    ugroup = ugmngt.ListGroups(sessionid, channel.getChannelid());
                                    for (int i = 0; i < ugroup.length; i++) {
                                %>
                                <option value="<%=ugroup[i].getGroupname()%>"><%=ugroup[i].getGroupname()%></option>
                                <%}%>
                            </select>
                        </div>
                    </div>

                </div>         
                <h4 class="text-success">Set Key Mapper:</h4>
                <table class="table table-striped" id="table_main">
                    <tr>
                        <th>Field 1</th>
                        <th>Field 2</th>
                        <th>Field 3</th>
                        <th>Field 4</th>
                        <th>Field 5</th>
                        <th>Field 6</th>
                        <th>Field 7</th>
                        <th>Field 8</th>
                        <th>Field 9</th>
                        <th>Field 10</th>
                    </tr>
                    <tr>
                        <td><input type="text" id="_field1" name="_field1" placeholder="key" class="span10"></td>
                        <td><input type="text" id="_field2" name="_field2" placeholder="key" class="span10"></td>
                        <td><input type="text" id="_field3" name="_field3" placeholder="key" class="span10"></td>
                        <td><input type="text" id="_field4" name="_field4" placeholder="key" class="span10"></td>
                        <td><input type="text" id="_field5" name="_field5" placeholder="key" class="span10"></td>
                        <td><input type="text" id="_field6" name="_field6" placeholder="key" class="span10"></td>
                        <td><input type="text" id="_field7" name="_field7" placeholder="key" class="span10"></td>
                        <td><input type="text" id="_field8" name="_field8" placeholder="key" class="span10"></td>
                        <td><input type="text" id="_field9" name="_field9" placeholder="key" class="span10"></td>
                        <td><input type="text" id="_field10" name="_field10" placeholder="key" class="span10"></td>
                    </tr>
                </table>
            </fieldset>
        </form>

        <button class="btn btn-primary" onclick="addnewresource()" id="addResourceButton">Add Resource</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true" onclick="addnewclose()">Back</button>           
    </div>
    <div id="addResource-result"></div>
</div>
</div>
<script>
    $(document).ready(function() {
        $("#_groupName").select2();
    });
</script>
<%@include file="footer.jsp" %>
