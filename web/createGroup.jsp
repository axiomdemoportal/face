<%@page import="java.util.Map"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.Roles"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/addSettings.js"></script>
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<%
    Channels _apSChannelDetails = (Channels) session.getAttribute("_apSChannelDetails");
  
    String channelID=_apSChannelDetails.getChannelid();
    OperatorsManagement oManagement = new OperatorsManagement();
    Roles role[]=oManagement.getAllRoles(channelID);
    String edit = request.getParameter("edit");
    Map keymap = null;
    keymap = (Map) request.getSession().getAttribute("keymap");
    //System.out.println(keymap + " " + edit);
    String _groupName = request.getParameter("_groupName");
    if (_groupName == null) {
        _groupName = "";
    }
%>
<div class="container-fluid">
    <h1 class="text-success">Group Setting.</h1>
    <p>add new group setting for sending alerts</p>
    <hr>
    <form class="form-horizontal" id="AddNewSchedulerForm" name="AddNewSchedulerForm">
        <fieldset>

            <div class="control-group">
                <label class="control-label"  for="username">Group Name </label>
                <div class="controls">
                    <% if (_groupName.equals("")) {%>
                    <input type="text" id="_gname" name="_gname" value='<%= _groupName%>'>
                    <% } else {%>
                    <input type="text" id="_gname" name="_gname" value='<%= _groupName%>' readonly>
                    <%}%>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label"  for="username">Operator Roles</label>
                <div class="controls">

                    <form id="searchContactsTAGForm" name="searchContactsTAGForm">
                        <select  name="_selectedRoles" id="_selectedRoles"  class="span4" multiple>
                            <option value="ALL" id="ALL">ALL</option>
                            <%
                                if(role.length!=0)
                                    for(int i=0;i<role.length;i++)
                                    {
                            %>
                            <option value="<%= role[i].getName() %>" id="<%= role[i].getName() %>"><%= role[i].getName() %></option>
                            <%}
                                Map valuemap = null;
                                String roles1 = "";
                                String[] rolearray = {};
                                if (keymap != null && edit != null) {
                                    valuemap = (Map) keymap.get(_groupName);
                                    roles1 = (String) valuemap.get("selectedRoles");
                                    rolearray = roles1.split(",");
                                    for (int i = 0; i < rolearray.length; i++) {
                            %>
                            <script>
                                var e = document.getElementById('_selectedRoles');
                                var val = e.options['<%= rolearray[i]%>'].selected = 'true';
//                               alert(val);
                            </script>
<!--                            <option value="<%= rolearray[i]%>" selected><%= rolearray[i]%></option>-->
                            <%
                                }
                            %>
                            <script>
                                showLists('<%= edit%>', '<%= _groupName%>');
                            </script>
                            <%   }

                            %>
                        </select>
                        <% if (edit != null && _groupName != null) {
                        %>
                        <a href="#" class="btn btn-success" id="SearchContactsUsngTagsDIV" onclick="showLists('<%= edit%>', '<%= _groupName%>')">Search Operator(s)</a>
                        <%} else {%>
                        <a href="#" class="btn btn-success" id="SearchContactsUsngTagsDIV" onclick="showList()">Search Operator(s)</a>
                        <%}%>
                    </form>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"  for="username">Vendors </label>
                <div class="controls">
                    <textarea name="_vendors" id="_vendors" class="input-xxlarge"  >
                        <%                            String vendor = "";
                            String[] vendorsarray = {};
                            if (valuemap != null) {
                                vendor = (String) valuemap.get("vendors");
                                vendorsarray = vendor.split(",");
                                for (int i = 0; i < vendorsarray.length; i++) {
                        %>
                        <%= vendorsarray[i]%>,
                        <% }
                            }
                        %>                                                   
                    </textarea>

                </div>
            </div>
            <div id="operator_table_main1">
            </div>
            <div class="control-group">

                <div class="controls">
                    <a href="./listGroup.jsp" class="btn" type="button"> << Back </a>
                    <button class="btn btn-primary" onclick="addSettings()" id="pull_settings" type="button">Save Group Now>></button>
                    
                </div>
            </div>
        </fieldset>
    </form>


</div>

<script>
    $(document).ready(function() {
        $("#_vendors").select2({
            tags: [],
            dropdownCss: {display: 'none'},
            tokenSeparators: [","]
        });
    });
    $(document).ready(function() {
        $("#_selectedRoles").select2()
    });
</script>


<%@include  file="footer.jsp" %>