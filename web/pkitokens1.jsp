<%@include file="header.jsp" %>
<script src="./assets/js/pkitokens.js"></script>

<%    SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm ");
    java.util.Date dLA = new java.util.Date();

%>
<div class="container-fluid">
    <div id="auditTable">
        <h1 class="text-success">Certificate & PKI Token Management</h1>
        <p>List of users, their certificates, pki tokens along with their management.</p>
        <h3>Search Users</h3>   
        <div class="input-append">
            <form id="searchPkiTokenForm" name="searchPkiTokenForm">
                <input type="text" id="_keyword" name="_keyword" placeholder="Search using name,phone,email..." class="span4"><span class="add-on"><i class="icon-search"></i></span>
                <a href="#" class="btn btn-success" onclick="searchpkitokenUsers()">Search Now</a>
            </form>
        </div>
        <div id="pkitoken_table_main">
        </div>
    </div>
</p>
<div id="Details" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">User Details</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="Detailsform">
                <fieldset>
                    <!--                    Name 
                                        <div class="control-group">
                                            <label class="control-label"  for="username">User ID</label>
                                            <div class="controls">
                                                <input type="text" id="_userid" name="_userid" class="input-xlarge">
                                            </div>
                                        </div>-->
                    <div class="control-group">
                        <label class="control-label"  for="username">User Name</label>
                        <div class="controls">
                            <input type="text" id="_username" name="_username" class="input-xlarge">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label"  for="username">Email</label>
                        <div class="controls">
                            <input type="text" id="_email" name="_email"  class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Phone</label>
                        <div class="controls">
                            <input type="text" id="_Phone" name="_Phone"  class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Organization</label>
                        <div class="controls">
                            <input type="text" id="_Organisation" name="_Organisation"  class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Organizational Unit</label>
                        <div class="controls">
                            <input type="text" id="_OrganisationUnit" name="_OrganisationUnit"  class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">City</label>
                        <div class="controls">
                            <input type="text" id="_city" name="_city"  class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">State</label>
                        <div class="controls">
                            <input type="text" id="_State" name="_State"  class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Pincode</label>
                        <div class="controls">
                            <input type="text" id="_pincode" name="_pincode"  class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Country</label>
                        <div class="controls">
                            <input type="text" id="_Country" name="_Country"  class="input-xlarge">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
</div>


<div id="certificateDetails" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Certificate Details</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="certform">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label"  for="username">Serial Number</label>
                        <div class="controls">
                            <input type="text" id="_srno" name="_srno" class="span4">   
                            & Algorithm: <input type="text" id="_algoname" name="_algoname" class="span4">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">User Details</label>
                        <div class="controls">
                            <textarea id="_subjectdn" name="_subjectdn" style="width:95%"></textarea>
                            <!--<input type="text" id="_subjectdn" name="_subjectdn"  class="input-xlarge">-->
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Issuer Details</label>
                        <div class="controls">
                            <textarea id="_issuerdn" name="_issuerdn" style="width:95%"></textarea>
                            <!--<input type="text" id="_issuerdn" name="_issuerdn"  class="input-xlarge">-->
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Issued On</label>
                        <div class="controls">
                            <input type="text" id="_notbefore" name="_notbefore"  class="input-xlarge">

                        </div>
                    </div>                    
                    <div class="control-group">
                        <label class="control-label"  for="username">Valid till</label>
                        <div class="controls">
                            <input type="text" id="_notafter" name="_notafter"  class="input-xlarge">
                        </div>
                    </div>                    


                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
</div>


<div id="HardwarePkiToken" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idEditPushMaperName"></div></h3>
        <h3 id="myModalLabel">Assign Hardware Token</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="HardwarePkiTokenform">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_userIDHR" name="_userIDHR" >

                    <div class="control-group">
                        <label class="control-label"  for="username">Serial Number</label>
                        <div class="controls">
                            <input type="text" id="_registrationCode" name="_registrationCode" placeholder="2210D03840000A" class="input-xlarge">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="HardwarePkiToken-result"></div>
        <button class="btn btn-primary" onclick="assignhardwaretokens(2)" id="buttonHardwareRegistration" type="button">Submit</button>
    </div>
</div>   
<div id="RevokeCertificate" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idEditPushMaperName"></div></h3>
        <h3 id="myModalLabel">Reason To Revoke</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="RevokeCertificateform">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_userCR" name="_userCR" >

                    <div class="control-group">
                        <label class="control-label"  for="username">Reason</label>
                        <div class="controls">
                            <input type="text" id="_reason" name="_reason" placeholder="Any reason?" class="input-xlarge">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="HardwarePkiToken-result"></div>
        <button class="btn btn-primary" onclick="revokecertificate()" id="buttonHardwareRegistration" type="button">Submit</button>
    </div>
</div>


<div id="SendCertificate" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idSendCertificate"></div></h3>
        <h3 id="myModalLabel">Send Certificate To</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="SendCertificateform">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_userSENDCERT" name="_userSENDCERT" >

                    <div class="control-group">
                        <label class="control-label"  for="username">Recipient's Emailid</label>
                        <div class="controls">
                            <input type="text" id="_emailSENDCERT" name="_emailSENDCERT" placeholder="email id " class="input-xlarge">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="SendCertificate-result"></div>
        <button class="btn btn-primary" onclick="sendcertificatefile()" id="buttonSendCertificate" type="button">Submit</button>
    </div>
</div>

<div id="kycupload" class="modal hide fade" tabindex="-1" role="dialog" style="width: 650px;"   aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Upload Documents</h3>
        <p>Please Compress all documents in zip and upload here.</p>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="kycuploadForm">
                <fieldset>
                    <input type="hidden" id="_usersid" name="_usersid" >

                    <div class="control-group">
                        <label class="control-label"  for="username">Select File:</label>                                    
                        <div class="controls fileupload fileupload-new" data-provides="fileupload">
                            <div class="input-append">
                                <div class="uneditable-input span3"><i class="icon-file fileupload-exists"></i> 
                                    <span class="fileupload-preview"></span>
                                </div>
                                <span class="btn btn-file" >
                                    <span class="fileupload-new"><i class='icon-file'></i></span>
                                    <span class="fileupload-exists"><i class='icon-file'></i></span>
                                    <input type="file" id="filekycupload" name="filekycupload"/>
                                </span>
                                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><i class='icon-trash'></i></a>
                                <button class="btn btn-success" id="buttonkycUpload"  onclick="UploadkycFile()">Upload</button>
                            </div>
                        </div>

                    </div>
                    <br>
                </fieldset>
            </form>
        </div>
    </div>
    <!--    <div class="modal-footer">
            <div id="addUser-result"></div>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button class="btn btn-primary" onclick="adduser()" id="addUserButton">Save Details Now>></button>
        </div>-->
</div> 

<div id="userPkiTokenauditDownload" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idauditDownload"></div></h3>
        <h3 id="myModalLabel">Download Audit</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="testSMSPrimaryForm" name="testSMSPrimaryForm">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_auditUserID" name="_auditUserID"/>
                    <input type="hidden" id="_auditUserName" name="_auditUserName"/>
                    <div class="control-group">
                        <label class="control-label"  for="username">Start Date</label>
                        <div class="controls" align="left" >

                            <!--<span class="add-on">From:</span>-->   
                            <div id="Pushdatetimepicker1" class="input-append date">
                                <input id="_auditStartDate" name="_auditStartDate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">End Date</label>
                        <div class="controls" align="left">
                            <!--<span class="add-on">Till:</span>-->   
                            <div id="Pushdatetimepicker2" class="input-append date">
                                <input id="_auditEndDate" name="_auditEndDate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>                    
                </fieldset>
            </form>
        </div>
    </div>

    <div class="modal-footer">
        <div id="editoperator-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="searchUsersPKITokenAudit()" id="buttonEditOperatorSubmit">Show Audit</button>
    </div>
</div>
<script>
    $(function () {
        $('#Pushdatetimepicker1').datepicker({
            format: 'dd/MM/yyyy',
            language: 'pt-BR'
        });
    });
    $(function () {
        $('#Pushdatetimepicker2').datepicker({
            format: 'dd/MM/yyyy',
            language: 'pt-BR'
        });
    });
</script>
<script language="javascript">
    //listChannels();
</script>

<%@include file="footer.jsp" %>