<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Audit"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.AuditManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Geotrack"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.MobileTrustManagment"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.GeoLocationManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>
<%@page import="com.mollatech.axiom.nucleus.settings.GlobalChannelSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>
<%@include file="header.jsp" %>
<!--<script src="./assets/js/jquery.js"></script>-->
<script src="./assets/js/otptokens.js"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/bootstrap-datepicker.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/css/datepicker.css">
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">

<div class="container-fluid">
    <h1 class="text-success">Usage Report</h1>
    <br>
    <input type="hidden" id="_changeType" name="_changeType" value="0">
    <div class="row-fluid">
        <div class="span12">
            <div class="control-group form-inline">
                <div class="input-prepend">
                    <span class="add-on">From:</span>   
                    <div id="datetimepicker1" class="input-append date">
                        <input id="startdate" name="startdate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                </div>
                <div class="input-prepend">
                    <!--<div class="well">-->
                    <span class="add-on">till:</span>   
                    <div id="datetimepicker2" class="input-append date">
                        <input id="enddate" name="enddate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                    <!--</div>-->
                </div>
                <select class="selectpicker" name="_changeCategory" id="_changeCategory"  class="span3">
                    <option  value="VERIFY OTP">OTP Token</option>       
                    <option  value="VERIFY SIGNATURE OTP">Signature OTP Token</option>       
                    <option  value="Verify Password">Verify Password</option> 
                    <option  value="ChallengeResponse Management">Challenge Response</option> 
                    <option  value="PKI Sign Data">PKI Sign</option> 

                </select>
                <%if (oprObj.getRoleid() != 1) {//1 sysadmin
                         if (accessObj.viewOtpFailureReport == true) { %>
                <button class="btn btn-success" onclick="searchTokenReport()" id="addUserButton">Generate Report Now</button>
                <%} else {%>
                <button class="btn btn-success" onclick="InvalidRequestOTPToken('otpfailurereport')" id="addUserButton">Generate Report Now</button>
                <%}
              } else {%>
                <button class="btn btn-success" onclick="searchTokenReport()" id="addUserButton">Generate Report Now</button>
                <%}%>
            </div>
            <div id="users_table_main_MSG">
            </div>
        </div>
    </div>


    <%  Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        int limit = 20;
        int TRADITIONAL = 1;
        String itemType = "VERIFY OTP";
        String itemType1 = "Verify SOTP";
        AuditManagement audit = new AuditManagement();
        UserManagement user = new UserManagement();
        Audit[] arraudit = audit.searchAuditObj(channel.getChannelid(), itemType, itemType1, limit);
        String strerr = "No Records Found";
    %>

    <div class="row-fluid">
        <div id="licenses_data_table">
            <h3>Recent Location</h3>
            <table class="table table-striped" id="table_main">
                <tr>
                    <td>No.</td>
                    <td>Name</td>
                    <td>Session ID</td>
                    <td>Channel ID</td>
                    <td>Result Code</td>
                    <td>Result</td>
                    <td>Dated</td>
                </tr>
                <%
                    if (arraudit != null) {
                        for (int i = 0; i < arraudit.length; i++) {
                            SimpleDateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            String start = df2.format(arraudit[i].getAuditedon());

//                            String strtype = "";
//                            if (arraudit[i].getResultcode() == -9) {
//                                strtype = "Expired";
//                            } else if (arraudit[i].getResultcode() == -8) {
//                                strtype = "Consumed";
//                            } else if (arraudit[i].getResultcode() ==-17) {
//                                strtype = "not found";
//                            } else if (arraudit[i].getResultcode() == -22) {
//                                strtype = "Locked";
//                            } else if (arraudit[i].getResultcode() == -1) {
//                                strtype = "Error";
//                            } 
//                            
                            AuthUser aUser = user.getUser(channel.getChannelid(), arraudit[i].getItemid());
                            String username = "user Not Found";
                            if (aUser != null) {
                                username = aUser.getUserName();
                            }

                %>
                <tr>
                    <td><%=(i + 1)%></td>
                    <td><%=username%></td>
                    <td><%=arraudit[i].getSessionId()%></td>
                    <td><%=arraudit[i].getChannelid()%></td>
                    <td><%=arraudit[i].getResultcode()%></td>
                    <td><%=arraudit[i].getResult()%></td>
                    <td><%=start%></td>
                </tr>
                <%
                    }
                } else {%>
                <tr>
                    <td><%=1%></td>
                    <td><%= strerr%></td>
                    <td><%= strerr%></td>
                    <td><%= strerr%></td>
                    <td><%= strerr%></td>
                    <td><%= strerr%></td>
                    <td><%= strerr%></td>
                    <%}%>
                </tr>
            </table>
        </div>
    </div>

    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datepicker({
                format: 'dd/MM/yyyy',
                language: 'pt-BR'
            });
        });
        $(function () {
            $('#datetimepicker2').datepicker({
                format: 'dd/MM/yyyy',
                language: 'pt-BR'

            });
        });
//        ChangeMediaType(0);

    </script>
    <script language="javascript">

    </script>



</div>

<%@include file="footer.jsp" %>