<%@page import="com.mollatech.axiom.nucleus.db.Schedulertracking"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.AxiomProtect"%>
<%@page import="com.mollatech.axiom.nucleus.settings.SchedulerSettingEntry"%>
<%@page import="com.mollatech.axiom.nucleus.db.Schedulersettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SchedulerManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/scheduler.js"></script>
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<div class="container-fluid">
    <h1 class="text-success">Report Scheduler Management (for SMS,Email Messages etc)</h1>
    <br>
    <div class="row-fluid">
        <div id="licenses_data_table">
            <table class="table table-striped">
                <tr>
                    <td>No.</td>
                    <td>Name</td>
                    <td>Manage</td>
                    <td>Duration</td>
                    <td>Executions</td>
                    <td>View Report</td>
                    <td>Created On</td>
                    <td>Updated On</td>
                </tr>

                <%   
                   Channels _apSChannelDetails = (Channels) session.getAttribute("_apSChannelDetails");
                    SchedulerManagement sManagement = new SchedulerManagement();
                    String _sessionID = (String) session.getAttribute("_apSessionID");
                    Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
                    String channelId = _apSChannelDetails.getChannelid();
                    Schedulersettings[] schSetting = sManagement.getSchedulerSetting(_sessionID, channelId);

                    
                    if (schSetting != null) {

                        for (int i = 0; i < schSetting.length; i++) {
                            byte[] obj = schSetting[i].getSchedulerSettingEntry();
//                           Object obj = deserializeFromObject(schSetting[i].getSchedulerSettingEntry());
                              ByteArrayInputStream bais = new ByteArrayInputStream(obj);
                                Object object = SchedulerManagement.deserializeFromObject(bais);
                             String strStatus = null;
                                if (schSetting[i].getSchedulerStatus() == 1) {
                                    strStatus = "Active";
                                } else {
                                    strStatus = "Suspended";
                                }
                          String uidiv4SchedulerStatus = "operator-status-value-" + i;
                            SchedulerSettingEntry schedulerSetting = null;
                            if (object instanceof SchedulerSettingEntry) {
                                schedulerSetting = (SchedulerSettingEntry) object;
                            }
                            
                          Schedulertracking[] tracking = sManagement.getarrSchedulerTracking(channelId, schSetting[i].getSchedulerName());
                          
                          if (schedulerSetting != null) {
                                String strduration = null;
                              if(schedulerSetting.getDuration() == 0){
                                  strduration = "Hourly";
                              }else if(schedulerSetting.getDuration() == 1){
                                  strduration = "Daily";
                              }else if(schedulerSetting.getDuration() == 7){
                                  strduration = "Weekly";
                              }else if(schedulerSetting.getDuration() == 30){
                                  strduration = "Monthly";
                              }else if(schedulerSetting.getDuration() == 91){
                                  strduration = "Quarterly";
                              }else if(schedulerSetting.getDuration() == 365){
                                  strduration = "Yearly";
                              }
                                
                %>
                <tr>
                    <td><%=(i + 1)%></td>
                    <td><%=schSetting[i].getSchedulerName()%></td>
                     <td>
                        <div class="btn-group">
                            <button class="btn btn-mini" id="<%=uidiv4SchedulerStatus%>"><%=strStatus%></button>
                            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeSchedulerStatus('<%=schSetting[i].getSchedulerName()%>',1,'<%=uidiv4SchedulerStatus%>')">Mark as Active?</a></li>
                                <li><a href="#" onclick="ChangeSchedulerStatus('<%=schSetting[i].getSchedulerName()%>',0,'<%=uidiv4SchedulerStatus%>')">Mark as Suspended?</a></li>
                                <li class="divider"></li>
                                <li><a href="#" onclick="loadEditSchedulerDetails('<%=schSetting[i].getSchedulerName()%>')">Edit Details</a></li>
                                 <li><a href="#" onclick="removeScheduler('<%=schSetting[i].getSchedulerName()%>')" data-toggle="modal"><font color="red">Remove?</font></a></li>
                            </ul>
                        </div>
                    </td>
                    
                    <td><%=strduration%></td>
                    <%if(tracking != null){%>
                    <td><%=tracking.length%></td>
                    <%}else{%>
                     <td><%=0%></td>
                    <%}%>
                    <td><a href="./schedulerexecutionlist.jsp?_schdulerName=<%=schSetting[i].getSchedulerName()%>" class="btn btn-mini" data-toggle="modal">Show Report</a></td>
                    <td><%=schSetting[i].getCreatedOn()%></td>
                    <td><%=schSetting[i].getUpdatedOn()%></td>
                </tr>

                <%}
                    }
                } else {%>
                <!--                 <tr>
                                    <td>No Entries Found</td>
                                 </tr>-->
                <%}%>
            </table>


        </div>
    </div>
    <br>
    <p><a href="#addSchedularSetting" class="btn btn-primary" data-toggle="modal">Add New Scheduler&raquo;</a></p>
    <script language="javascript">
        //listChannels();
    </script>
</div>

<div id="editScheduler0" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   
</div>

<div id="addSchedularSetting" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Add New Scheduler</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="AddNewSchedulerForm" name="AddNewSchedulerForm">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_gatewayType" name="_gatewayType">
                    <input type="hidden" id="_schedulerStatus" name="_schedulerStatus">
                    <input type="hidden" id="_gatewayPreference" name="_gatewayPreference">
                    <input type="hidden" id="_gatewayStatus" name="_gatewayStatus">
                    <input type="hidden" id="_operatorRoles" name="_operatorRoles">
                    <input type="hidden" id="_schedulerDuration" name="_schedulerDuration">
                     <input type="hidden" id="_reportType" name="_reportType">
                    
                    <div class="control-group">
                        <label class="control-label"  for="username">Scheduler Name</label>
                        <div class="controls">
                            <input type="text" id="_schedulername" name="_schedulername" placeholder="set unique name for scheduler" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Scheduler Status</label>
                        <div class="controls">
                            <div>
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_schedulerStatus-scheduler"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="SchedulerStatus(1)">Active</a></li>
                                        <li><a href="#" onclick="SchedulerStatus(0)">Suspended</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">For Type</label>
                        <div class="controls">
                            <div>
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_gatewayType-scheduler"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">                                        
                                        <li><a href="#" onclick="GatewayType(1)">SMS</a></li>
                                        <li><a href="#" onclick="GatewayType(2)">USSD</a></li>
                                        <li><a href="#" onclick="GatewayType(3)">VOICE</a></li>
                                        <li><a href="#" onclick="GatewayType(4)">EMAIL</a></li>
                                        <li><a href="#" onclick="GatewayType(5)">FAX</a></li>
<!--                                        <li><a href="#" onclick="GatewayType(6)">FACEBOOK</a></li>
                                        <li><a href="#" onclick="GatewayType(7)">LINKEDIN</a></li>
                                        <li><a href="#" onclick="GatewayType(8)">TWITTER</a></li>-->
                                        <li><a href="#" onclick="GatewayType(18)">ANDROIDPUSH</a></li>
                                        <li><a href="#" onclick="GatewayType(19)">IPHONEPUSH</a></li>
                                    </ul>
                                </div>

                                &nbsp;&nbsp;&nbsp; with Status
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_gatewayStatus-scheduler"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="GatewayStatus(99)">ALL</a></li>
                                        <li><a href="#" onclick="GatewayStatus(0)">SENT</a></li>
                                        <li><a href="#" onclick="GatewayStatus(2)">PENDING</a></li>
                                        <li><a href="#" onclick="GatewayStatus(-1)">FAILED</a></li>
                                        <li><a href="#" onclick="GatewayStatus(-5)">BLOCKED</a></li>
                                       
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">For Preference</label>
                        <div class="controls">
                            <div>
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_gatewayPreference-scheduler"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="GatewayPreference(0)">Both</a></li>
                                        <li><a href="#" onclick="GatewayPreference(1)">Primary Gateway</a></li>
                                        <li><a href="#" onclick="GatewayPreference(2)">Secondary Gateway</a></li>
                                    </ul>
                                </div>
                               &nbsp;and duration
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_schedulerDuration-scheduler"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="SchedulerDuration(0)">Hourly</a></li>
                                        <li><a href="#" onclick="SchedulerDuration(1)">Daily</a></li>
                                        <li><a href="#" onclick="SchedulerDuration(7)">Weekly</a></li>
                                        <li><a href="#" onclick="SchedulerDuration(30)">Monthly</a></li>
                                        <li><a href="#" onclick="SchedulerDuration(91)">Quarterly</a></li>
                                        <li><a href="#" onclick="SchedulerDuration(365)">Yearly</a></li>
                                    </ul>
                                </div>
                                

                            </div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label"  for="username">Send Report to</label>
                        <div class="controls">
                            <div>
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_operatorRoles-scheduler"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <%
                                            OperatorsManagement oMngt = new OperatorsManagement();
                                            Roles[] roles1 = oMngt.getAllRoles(channelId);
                                            if (roles1 != null) {
                                                for (int i = 0; i < roles1.length; i++) {%>

                                        <li><a href="#" onclick="OperatorRoles(<%=roles1[i].getRoleid()%>,'<%=roles1[i].getName()%>')"><%=roles1[i].getName()%></a></li>
                                            <%}
                                                }%>
                                    </ul>
                                </div>
                                    
                                    &nbsp;with report type    
                                  <div class="btn-group">
                                    <button class="btn btn-small"><div id="_reportType-scheduler"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ReportTypeScheduler(99)">PDF&CSV</a></li>
                                        <li><a href="#" onclick="ReportTypeScheduler(0)">PDF</a></li>
                                        <li><a href="#" onclick="ReportTypeScheduler(1)">CSV</a></li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Add Emailid's</label>
                        <div class="controls">
                            <textarea name="_tagsList" id="_tagsList" style="width:100%">
                                <%
                                %>                                                   
                            </textarea>
                            <br>Attention: Please enter comma (",") for separating emailids.
                        </div>
                    </div>

                </fieldset>
                <script>
                    SchedulerStatus(1);
                    GatewayType(1);
                    GatewayStatus(99);
                    GatewayPreference(0);
                    //OperatorRoles(1,'sysadmin');
                    SchedulerDuration(1);
                    ReportTypeScheduler(0);
                </script>
            </form>
        </div>
    </div>



    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <div class="span3" id="add-new-scheduler-result"></div>
        <button class="btn btn-primary" onclick="addScheduler()" id="addnewSchedulerSubmitBut">Add New Scheduler</button>
    </div>
</div>


<script>
           
    $(document).ready(function() {
        $("#_tagsList").select2({
            tags: [],
            dropdownCss: {display: 'none'},
            tokenSeparators: [","]
        });
    });
</script>
<%@include file="footer.jsp" %>
