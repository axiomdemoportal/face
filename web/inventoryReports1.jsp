<%@include file="header.jsp" %>
<script src="./assets/js/emailgateway.js"></script>
<%if (oprObj.getRoleid() != 1) {
//not sysadmin%>
<div class="container-fluid">
    <%   
        String _type = request.getParameter("_type");
        if (_type.equals("1")) {
    %>
    <div class="row-fluid" >
        <h1 class="text-success">Inventory Reports</h1>
        <p>list of reports</p>
        <ul class="thumbnails">
            <%if (accessObj.listUserReport) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="usersreport.jsp" >
                        <img src="./assets/img/user.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Users Reports</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listOtpReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="otptokensreport.jsp" >
                        <img src="./assets/img/otp6.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>OTP Tokens Reports</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listCertificateReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="certificatereport.jsp" >
                        <img src="./assets/img/certificate3.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Certificate Reports</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listPkiReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="pkitokenreport.jsp" >
                        <img src="./assets/img/pki1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>PKI Tokens Reports</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listEpinSystemReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="EcponSystemReportMain.jsp" >
                        <img src="./assets/img/epin1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>E-PIN Reports</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listtranscationReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="transactionReport.jsp" >
                        <img src="./assets/img/transaction5.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Transaction Details</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
                 <%if (accessObj.listRemoteSigningReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="RSSDetailsMain.jsp" >
                        <img src="./assets/img/transaction5.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Remote Signing Report</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>

        </ul>
    </div>


    <%} else if (_type.equals("2")) {%>
    <div class="row-fluid" >
        <h1 class="text-success">Usage Reports</h1>
        
        <ul class="thumbnails">
            <%if (accessObj.listOtpFailureReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="tokenFailureReport.jsp" >
                        <img src="./assets/img/user.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Usage Report</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listHoneyTrapReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="honeyTrapmain.jsp" >
                        <img src="./assets/img/honeytrap1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Honey Trap   Report</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
            <%
            //accessObj.listTwoWayAuthManagement = true;
            if (accessObj.listTwoWayAuthManagement == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="twoWayAuthDualChannel.jsp" >
                        <img src="./assets/img/TwoWayAuthReport.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Two Way (Dual Channel) Authentication Report</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
            <%
            //accessObj.listTwoWayAuthManagement = true;
            if (accessObj.listWebSealManagement == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="websealreport.jsp" >
                        <img src="./assets/img/secure-site.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>WebSeal Usage Report</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
        </ul>
    </div>

    <%} else if (_type.equals("3")) {%>
    <div class="row-fluid" >
        <h1 class="text-success">Billing Reports</h1>
        
        <ul class="thumbnails">
            <%if (accessObj.listsubscriptionBaseBillingReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="billReportmain.jsp" >
                        <img src="./assets/img/bill7.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Billing Report (Subscription)</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listtranscationBaseBillingReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="billReportmainPerTx.jsp" >
                        <img src="./assets/img/trans1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Billing Report (/Tx) </h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
        </ul>
    </div>

    <%} else if (_type.equals("4")) {%>
    <div class="row-fluid" >
        <h1 class="text-success">Message Reports</h1>
        
        <ul class="thumbnails">
            <%if (accessObj.listMessageReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="msgreportmain.jsp" >
                        <img src="./assets/img/sms2.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Message Report</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listMessageReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="msgcostreport.jsp" >
                        <img src="./assets/img/trans1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Summary Cost Report </h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
        </ul>
    </div>

    <%} else if (_type.equals("6")) {%>
    <div class="row-fluid" >
        <h1 class="text-success">Audit Reports</h1>
        
        <ul class="thumbnails">
            <%if (accessObj.downloadAuditTrail == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="channelaudit.jsp" >
                        <img src="./assets/img/ca1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Complete Audit Trail</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.downloadSessionAuditTrail == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="sessionaudit.jsp" >
                        <img src="./assets/img/ca1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Session ID Audit </h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.auditIntegrity == true) { %>
<!--            <li class="span2">
                <div class="thumbnail">
                    <a href="#CheckAuditIntegrity" >
                        <img src="./assets/img/ca1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Audit Integrity </h5>
                        <h3>Check audit integrity</h3>
                    </div>
                </div>
            </li>-->
            <%}%>
        </ul>
    </div>

    <%} else if (_type.equals("7")) {%>
    <div class="row-fluid" >
        <h1 class="text-success">Gateway Configuration</h1>       
        <ul class="thumbnails">
            <%if (accessObj.listSmsGateway == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="smsgateway.jsp" >
                        <img src="./assets/img/sms2.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>SMS Gateway Configuration</h5>                        
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listUSSDGateway == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="ussdgateway.jsp" >
                        <img src="./assets/img/ussd8.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>USSD Gateway Configuration</h5>                        
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listVOICEGateway == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="voicegateway.jsp" >
                        <img src="./assets/img/voice11.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>VOICE Gateway Configuration</h5>
                        
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listEMAILGateway == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="emailgateway.jsp" >
                        <img src="./assets/img/email14.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Email Gateway Configuration</h5>
                        
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listFAXGateway == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="faxgateway.jsp" >
                        <img src="./assets/img/fax4.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>FAX Gateway Configuration</h5>
                        
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listPushGateway == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="pushgateway.jsp" >
                        <img src="./assets/img/push5.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Push Notification Gateway Configuration</h5>
                        
                    </div>
                </div>
            </li>
            <%}%>

        </ul>
    </div>


    <%} else if (_type.equals("8")) {%>
    <div class="row-fluid" >
        <h1 class="text-success">System Configuration</h1>
        
        <ul class="thumbnails">
            <%if (accessObj.listPasswordPolicySettings == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="PasswordPolicySetting.jsp" >
                        <img src="./assets/img/pp4.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Password Policy Settings</h5>
                        
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listChannelProfileSettings == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="channelProfile.jsp" >
                        <img src="./assets/img/cp2.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Channel Profile Settings</h5>
                        
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listCertificateSettings == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="caconnector.jsp" >
                        <img src="./assets/img/certificate3.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Certificate Authority (CA) Connector Configuration</h5>
                        
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listMobileTrustSettings == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="mobiletrustsettings.jsp" >
                        <img src="./assets/img/mobiletrust4.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Trust Settings</h5>
                        
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listRadiusConfigSettings == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="radiusserver2.jsp" >
                        <img src="./assets/img/image1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Radius General and Client Setting Management</h5>
                        
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listUserSourceSettings == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="usersrcsetting.jsp" >
                        <img src="./assets/img/user.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>User Source Configuration</h5>
                        
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listGlobalSettings == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="globalsettingv2.jsp" >
                        <img src="./assets/img/global5.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Global Configuration</h5>
                        
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listEpinConfigSettings == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="epinsetting.jsp" >
                        <img src="./assets/img/epin1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>E-PIN Settings</h5>
                        
                    </div>
                </div>
            </li>            
            <%}%>
            <%if (accessObj.listImageSettings == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="imagesettings.jsp" >
                        <img src="./assets/img/image1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Secure Phrase Configuration</h5>
                        <p>Secure Phrase Configuration.</p>
                    </div>
                </div>
            </li>
            <%}%>
            <%if (accessObj.listBillingManager == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="BillingManager.jsp" >
                        <img src="./assets/img/bill7.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Billing Manager</h5>
                        
                    </div>
                </div>
            </li>
            <%}if (accessObj.listOtpTokensSettings == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="tokensettings.jsp" >
                        <img src="./assets/img/otp6.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>OTP Token Configuration</h5>
                        
                    </div>
                </div>
            </li>
            <%}%>

        </ul>
    </div>


    <%}%>


</div>
<%} else 
{ // sysadmin
%>
<div class="container-fluid">
    <%   
        String _type = request.getParameter("_type");
//        _type = "6";
        if (_type.equals("1")) {
    %>
    <div class="row-fluid" >
        <h1 class="text-success">Inventory Reports</h1>
        <p>list of reports</p>
        <ul class="thumbnails">

            <li class="span2">
                <div class="thumbnail">
                    <a href="usersreport.jsp" >
                        <img src="./assets/img/user.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Users Reports</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <li class="span2">
                <div class="thumbnail">
                    <a href="otptokensreport.jsp" >
                        <img src="./assets/img/otp6.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>OTP Tokens Reports</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <li class="span2">
                <div class="thumbnail">
                    <a href="certificatereport.jsp" >
                        <img src="./assets/img/certificate3.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Certificate Reports</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <li class="span2">
                <div class="thumbnail">
                    <a href="pkitokenreport.jsp" >
                        <img src="./assets/img/pki1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>PKI Tokens Reports</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <%if (accessObj.listEpinSystemReport == true) 
                    if ( 1 == 0) 
                { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="EcponSystemReportMain.jsp" >
                        <img src="./assets/img/epin1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>E-PIN Reports</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <% } %> 
            <%if (accessObj.editTwoWayAuthManagement == true)  { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="transactionReport.jsp" >
                        <img src="./assets/img/transaction5.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Transaction Details</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <li class="span2">
                <div class="thumbnail">
                    <a href="transactionReport.jsp" >
                        <img src="./assets/img/transaction5.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Transaction Details</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <% } %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="RSSDetailsMain.jsp" >
                        <img src="./assets/img/transaction5.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Remote Signing Report</h5>
                        
                    </div>
                </div>
            </li>

        </ul>
    </div>


    <%} else if (_type.equals("2")) {%>
    <div class="row-fluid" >
        <h1 class="text-success">Usage Reports</h1>
        <p>list of reports</p>
        <ul class="thumbnails">
            <li class="span2">
                <div class="thumbnail">
                    <a href="tokenFailureReport.jsp" >
                        <img src="./assets/img/user.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Usage Report</h5>
                        
                    </div>
                </div>
            </li>
            <%if (accessObj.listHoneyTrapReport == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="honeyTrapmain.jsp" >
                        <img src="./assets/img/honeytrap1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Honey Trap Report</h5>
                        
                    </div>
                </div>
            </li>
            <%  } %>
             <%if (accessObj.editWebWatch == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="websealSetting.jsp" >
                        <img src="./assets/img/secure-site.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Web Seal Configuration</h5>
                        
                    </div>
                </div>
            </li>
            <%  } %>
        </ul>
    </div>

    <%} else if (_type.equals("3")) {%>
    <div class="row-fluid" >
        <h1 class="text-success">Billing Reports</h1>
        <p>list of reports</p>
        <ul class="thumbnails">
            <li class="span2">
                <div class="thumbnail">
                    <a href="billReportmain.jsp" >
                        <img src="./assets/img/bill7.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Billing Report (Subscription)</h5>
                        
                    </div>
                </div>
            </li>
            <li class="span2">
                <div class="thumbnail">
                    <a href="billReportmainPerTx.jsp" >
                        <img src="./assets/img/trans1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Billing Report (/Tx) </h5>
                        
                    </div>
                </div>
            </li>
        </ul>
    </div>

    <%} else if (_type.equals("4")) {%>
    <div class="row-fluid" >
        <h1 class="text-success">Message Reports</h1>
        <p>list of reports</p>
        <ul class="thumbnails">
            <li class="span2">
                <div class="thumbnail">
                    <a href="msgreportmain.jsp" >
                        <img src="./assets/img/sms2.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Message Report</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <li class="span2">
                <div class="thumbnail">
                    <a href="msgcostreport.jsp" >
                        <img src="./assets/img/trans1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Summary Cost Report </h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
        </ul>
    </div>

    <%} else if (_type.equals("6")) {%>
    <div class="row-fluid" >
        <h1 class="text-success">Audit Reports</h1>
        <p>list of reports</p>
        <ul class="thumbnails">
            <li class="span2">
                <div class="thumbnail">
                    <a href="channelaudit.jsp" >
                        <img src="./assets/img/ca1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Complete Audit Trail</h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
            <li class="span2">
                <div class="thumbnail">
                    <a href="sessionaudit.jsp" >
                        <img src="./assets/img/ca1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Session ID Audit </h5>
                        <!--<h3>Make Your Own Report</h3>-->
                    </div>
                </div>
            </li>
<!--            <li class="span2">
                <div class="thumbnail">
                    <a href="#CheckAuditIntegrity" >
                        <img src="./assets/img/ca1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Audit Integrity </h5>
                        <h3>Check audit integrity</h3>
                    </div>
                </div>
            </li>-->
        </ul>
    </div>

    <%} else if (_type.equals("7")) {%>
    <div class="row-fluid" >
        <h1 class="text-success">Gateway Configuration</h1>
        <p>list of gateways:</p>
        <ul class="thumbnails">
            <li class="span2">
                <div class="thumbnail">
                    <a href="smsgateway.jsp" >
                        <img src="./assets/img/sms2.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>SMS Gateway Configuration</h5>                        
                    </div>
                </div>
            </li>
            <li class="span2">
                <div class="thumbnail">
                    <a href="ussdgateway.jsp" >
                        <img src="./assets/img/ussd8.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>USSD Gateway Configuration</h5>
                        
                    </div>
                </div>
            </li>
            <li class="span2">
                <div class="thumbnail">
                    <a href="voicegateway.jsp" >
                        <img src="./assets/img/voice11.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>VOICE Gateway Configuration</h5>
                        
                    </div>
                </div>
            </li>
            <li class="span2">
                <div class="thumbnail">
                    <a href="emailgateway.jsp" >
                        <img src="./assets/img/email14.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>EMAIL Gateway Configuration</h5>
                        
                    </div>
                </div>
            </li>
            <li class="span2">
                <div class="thumbnail">
                    <a href="faxgateway.jsp" >
                        <img src="./assets/img/fax4.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>FAX Gateway Configuration</h5>
                        
                    </div>
                </div>
            </li>
            <li class="span2">
                <div class="thumbnail">
                    <a href="pushgateway.jsp" >
                        <img src="./assets/img/push5.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Push Notification Gateway Configuration</h5>
                        
                    </div>
                </div>
            </li>

        </ul>
    </div>


    <%} else if (_type.equals("8")) {%>
    <div class="row-fluid" >
        <h1 class="text-success">System Configuration</h1>
        
        <ul class="thumbnails">

            <li class="span2">
                <div class="thumbnail">
                    <a href="PasswordPolicySetting.jsp" >
                        <img src="./assets/img/pp4.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Password Policy Settings</h5>
                        
                    </div>
                </div>
            </li>
            <li class="span2">
                <div class="thumbnail">
                    <a href="channelProfile.jsp" >
                        <img src="./assets/img/cp2.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Channel Profile Settings</h5>
                        
                    </div>
                </div>
            </li>
            <li class="span2">
                <div class="thumbnail">
                    <a href="caconnector.jsp" >
                        <img src="./assets/img/certificate3.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Certificate Authority (CA) Connector Configuration</h5>
                        
                    </div>
                </div>
            </li>
            <li class="span2">
                <div class="thumbnail">
                    <a href="mobiletrustsettings.jsp" >
                        <img src="./assets/img/mobiletrust4.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Trust Settings</h5>
                        
                    </div>
                </div>
            </li>
            <li class="span2">
                <div class="thumbnail">
                    <a href="radiusserver2.jsp" >
                        <img src="./assets/img/image1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Radius General and Client Setting Management</h5>
                        
                    </div>
                </div>
            </li>
            <li class="span2">
                <div class="thumbnail">
                    <a href="usersrcsetting.jsp" >
                        <img src="./assets/img/user.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>User Source Configuration</h5>
                        
                    </div>
                </div>
            </li>

            <li class="span2">
                <div class="thumbnail">
                    <a href="globalsettingv2.jsp" >
                        <img src="./assets/img/global5.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Global Configuration</h5>
                        
                    </div>
                </div>
            </li>
            <%if (accessObj.listEpinSystemReport == true)
                if ( 1 == 0 ) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="epinsetting.jsp" >
                        <img src="./assets/img/epin1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>E-PIN Settings</h5>
                        
                    </div>
                </div>
            </li>
            <%  } %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="imagesettings.jsp" >
                        <img src="./assets/img/image1.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Secure Phrase Configuration</h5>
                        
                    </div>
                </div>
            </li>
            <%if (accessObj.editOtpTokensSettings == true) { %>
            <li class="span2">
                <div class="thumbnail">
                    <a href="BillingManager.jsp" >
                        <img src="./assets/img/bill7.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>Billing Manager</h5>
                        
                    </div>
                </div>
            </li>
            <%  } %>
            <%if (accessObj.editOtpTokensSettings == true) { %>
             <li class="span2">
                <div class="thumbnail">
                    <a href="tokensettings.jsp" >
                        <img src="./assets/img/otp6.png" alt="Pulpit Rock" height="150"  style="width:150px;height:150px">
                    </a>
                    <div class="caption">
                        <h5>OTP Token Configuration</h5>
                        
                    </div>
                </div>
            </li>
            <%  } %>
           
            
            
        </ul>
    </div>


    <%}%>


</div>
<%}%>

<%@include file="footer.jsp" %>

