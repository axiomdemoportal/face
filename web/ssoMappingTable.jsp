
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TwowayauthManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Twowayauth"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="java.text.SimpleDateFormat"%>
<%
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();
    String _searchtext = request.getParameter("_searchtext");

    UserManagement usermngObj = new UserManagement();
    AuthUser Users[] = null;
    Users = usermngObj.SearchUsers(sessionId, _channelId, _searchtext);
    SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm");
%>
<script src="./assets/js/ssomapping.js"></script>
<table class="table table-striped" id="table_main">

    <tr>
        <td>No.</td>
        <td>Name</td>
        <td>Email</td>
        <td>Add Mapping</td>
        <td>Edit Mapping</td>
        <td>Created On</td>
        <td>Last Updated On</td>
    </tr>
    <%if (Users != null) {
            for (int j = 0; j < Users.length; j++) {
    %>
    <tr>
        <td><%= j + 1%></td>
        <td><%= Users[j].getUserName()%></td>
        <td><%= Users[j].getEmail()%></td>
        <td>
            <a href="#" class="btn btn-mini" onclick="addSSODetails('<%=Users[j].getUserId()%>')">Add Mapping</a>
        </td>
        <td>
            <a href="#" class="btn btn-mini" <%if(Users[j].sso!=null){%> onclick="editSSODetails('<%=Users[j].getUserId()%>')" <%}else{%>disabled<%}%>>Edit Mapping</a>
        </td>
        <td><%= sdf.format(new java.util.Date(Users[j].lCreatedOn))%></td>
        <td><%= sdf.format(new java.util.Date(Users[j].lLastAccessOn))%></td>
    </tr>
    <%}
    } else {%>
    <tr>
        <td>No Record Found</td>
        <td>No Record Found</td>
        <td>No Record Found</td>
        <td>No Record Found</td>
        <td>No Record Found</td>
        <td>No Record Found</td>
        <td>No Record Found</td>
        <td>No Record Found</td>
    </tr>
    <%}%>
</table>