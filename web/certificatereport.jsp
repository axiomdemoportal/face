<%@page import="com.mollatech.axiom.nucleus.db.Certificates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/jquery.js"></script>
<script src="./assets/js/certificate.js"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">
<link rel="stylesheet" href="./assets/css/datepicker.css">
<script src="./assets/js/bootstrap-datepicker.js"></script>

<div class="container-fluid">
    <h1 class="text-success">Certificate Reports</h1>    

    <!--<h3 class="text-info"><i>License does not allow this package</i></h3>-->

    <% if (0 == 0) {%>    
    <h3>Make Your Own Report</h3>


    <input type="hidden" id="_changeCertCategory" name="_changeCertCategory" value="0">
    <input type="hidden" id="_changeCertStatus" name="_changeCertStatus" value="0">

    <div class="row-fluid">
        <div class="span12">
            <div class="control-group form-inline">
                (For Table List only, select Status:)
                <div class="btn-group">
                    <button class="btn"><div id="_change_Certificate_Status_Axiom"></div></button>
                    <button class="btn" data-toggle="dropdown"><span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="#" onclick="ChangeCertStateType(1)">Active</a></li>
                        <li><a href="#" onclick="ChangeCertStateType(-5)">Revoked</a></li>
                        <li><a href="#" onclick="ChangeCertStateType(-10)">Expired</a></li>
                        <li><a href="#" onclick="ChangeCertStateType(-1)">Expire Soon</a></li>
                    </ul>
                </div>
                <div class="input-prepend">

                    <!--<div class="well">-->
                    <span class="add-on">From:</span>   
                    <div id="datetimepicker1" class="input-append date">
                        <input id="startdate" name="startdate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                    <!--</div>-->
                </div>
                <div class="input-prepend">
                    <!--<div class="well">-->
                    <span class="add-on">till:</span>   
                    <div id="datetimepicker2" class="input-append date">
                        <input id="enddate" name="enddate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                    <!--</div>-->
                </div>
                <div class="input-prepend">
                    <%if (oprObj.getRoleid() != 1) {
                            if (accessObj != null && accessObj.viewCertificateReport == true) {%>
                    <button class="btn btn-success" onclick="generateCertificateTable()" type="button">Filter</button>
                    <%} else {%>
                    <button class="btn btn-success " onclick="InvalidRequestCertificate('certreport')" type="button">Generate Report Now</button>
                    <%}
                    } else {%>
                    <button class="btn btn-success" onclick="generateCertificateTable()" type="button">Filter</button>
                    <%}%>

                </div>
            </div>
        </div>
    </div>

    <div class="tabbable" id="REPORT" style="display: none">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#otpcharts" data-toggle="tab">Charts</a></li>
            <li><a href="#otpreport" data-toggle="tab">Tabular List</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="otpcharts">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="control-group">
                            <div class="span4">
                                <div id="graph" ></div>

                            </div>
                            <div  class="span8">
                                <div id="graph1"></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--            <div class="tab-pane" id="otpreport">  
                            <div id="licenses_data_table">
                            </div>
                        </div>-->
        </div>
    </div>
    <!--    <div class="tab-pane" id="otpreport">  -->
    <div id="licenses_data_table">
    </div>
    <!--    </div>-->
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            });
        });
        $(function () {
            $('#datetimepicker2').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'

            });
        });

    </script>
    <%
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String _channelId = channel.getChannelid();
        int limit = 50;
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        CertificateManagement cObj = new CertificateManagement();
        Certificates[] arrCobj = cObj.loadCertObj(_channelId, limit);

//        Certificates[] arrCobj = null;
        String strerr = "No Record Found";

        if (arrCobj != null) {
    %>

    <div id="licenses_data_table_onLoad">
        <table class="table table-striped" id="table_main">
            <tr>
                <td>No.</td>
                <td>User Id</td>
                <td>User Name</td>
                <td>Certificate ID</td>  
                <td>Status</td>  
                <td>Issued On</td>
                <td>Expires On</td>
            </tr>
            <%
                int CERT_STATUS_ACTIVE = 1;
                int CERT_STATUS_EXPIRED = -10;
                int CERT_STATUS_REVOKED = -5;
                int GOING_TO_EXPIRED = -1;
                int SHOW_ALL = 2;

                if (arrCobj != null) {
                    for (int i = 0; i < arrCobj.length; i++) {
                        UserManagement uObj = new UserManagement();
                        AuthUser userObj = uObj.getUser(sessionId, _channelId, arrCobj[i].getUserid());
                        String strCertStatus = null;
                        if (arrCobj[i].getStatus() == CERT_STATUS_ACTIVE) {
                            strCertStatus = "Active Certificate";
                        } else if (arrCobj[i].getStatus() == CERT_STATUS_REVOKED) {
                            strCertStatus = "Revoked Certificate";
                        } else if (arrCobj[i].getStatus() == CERT_STATUS_EXPIRED) {
                            strCertStatus = "Expired Certificate";
                        } else if (arrCobj[i].getStatus() == GOING_TO_EXPIRED) {
                            strCertStatus = "Expiring Soon...";
                        }
            %>

            <tr>
                <td><%=(i + 1)%></td>
                <%if (userObj != null) {%>
                <td><%=userObj.getUserId()%></td>
                <td><%=userObj.getUserName()%></td>
                <%} else {%>
                <td>-</td>
                <td>-</td>
                <%}%>
                <td><%=arrCobj[i].getCertid()%></td>
                <td><%=strCertStatus%></td>
                <td><%=sdf.format(arrCobj[i].getCreationdatetime())%></td>
                <td><%=sdf.format(arrCobj[i].getExpirydatetime())%></td>

            </tr>


            <%}
            } else {%>
            <tr>
                <td><%=1%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>

            </tr>
            <%}%>


        </table>
        <% }%>

    </div>
    <script type="text/javascript">
        ChangeCertStateType(1);
    </script>
</div>
<% }%>

<%@include file="footer.jsp" %>