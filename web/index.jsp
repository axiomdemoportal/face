<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.AxiomProtect"%>
<%@page import="java.io.File"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.LoadSettings"%>
<%
    String strContextPath = this.getServletContext().getContextPath();
    

%>
<%
//    String sep = System.getProperty("file.separator");
//    String usrhome = System.getProperty("user.dir");
//    usrhome += sep + "axiomv2-settings";
//            // g_strPath = usrhome + sep;
//    // String filepath = usrhome + sep + "license.enc";
//    String filepath = LoadSettings.g_strPath + "license.enc";
//    File f = new File(filepath);
//    
//
//    if (f.exists()) {
//        int iResult = AxiomProtect.ValidateLicense();
//        if (iResult != 0) {
//            response.sendRedirect("register.jsp");
//            return;
//            //register.jsp
//        } else {
//            boolean result = AxiomProtect.IsSecurityInitialized();
//            if (result != true) {
//                response.sendRedirect("initialize.jsp");
//                return;
//                //register.jsp
//            }
//        }
//    } else {
//        //register.jsp
//        response.sendRedirect("register.jsp");
//        return;
//    }
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><%=strContextPath%> Management Portal For You</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="./assets/css/bootstrap.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 40px;
                padding-bottom: 40px;
                background-color: #f5f5f5;
            }

            .form-signin {
                max-width: 300px;
                padding: 19px 29px 29px;
                margin: 0 auto 20px;
                background-color: #fff;
                border: 1px solid #e5e5e5;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
            }
            .form-signin .form-signin-heading,
            .form-signin .checkbox {
                margin-bottom: 10px;
            }
            .form-signin input[type="text"],
            .form-signin input[type="password"] {
                font-size: 16px;
                height: auto;
                margin-bottom: 15px;
                padding: 7px 9px;
            }

        </style>
        <link href="./assets/css/bootstrap-responsive.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="./assets/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="./assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="./assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="./assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="./assets/ico/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="./assets/shield_256.png">
    </head>

    <body>

        <div class="container">

            <div class="hero-unit" align="center">
                <h1>"<%=strContextPath%>" </h1>
                <h2>management portal</h2>
                <br>
                <form class="form-signin" method="POST" action="#" id="loginForm" name="loginForm">
                    <p>
                    <div class="text" align="center" id="login-result" ></div>
                    <input type="text" id="_name"  name="_name"  class="input-block-level" placeholder="Enter name">
                    <input type="password" id="_passwd" autocomplete="off" name="_passwd" class="input-block-level" placeholder="Enter password">
                    <button class="btn btn-large btn-primary" onclick="Login()" type="button" >Sign in</button>
                    </p>
                </form>
            </div>
        </div> <!-- /container -->
        
             <div id="ChangePassword" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>-->
        <h3 id="myModalLabel">Change Password before login</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="ChangePasswordform">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_userIDS" name="_userIDS" >
                    <div class="control-group">
                        <label class="control-label"  for="username">Current Password</label>
                        <div class="controls">
                            <input type="password" id="_oldPassword" name="_oldPassword" placeholder="Current Password" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">New Password</label>
                        <div class="controls">
                            <input type="password" id="_newPassword" name="_newPassword" placeholder="New Password" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Confirm Password</label>
                        <div class="controls">
                            <input type="password" id="_confirmPassword" name="_confirmPassword" placeholder="Confirm Password" class="input-xlarge">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="Password-result"></div>
        <button class="btn btn-primary" onclick="changeoperatorpasswordAfter1stLogin()" id="passwordChange" type="button">Change</button>
    </div>
</div>

        <%
            java.util.Date dFooter = new java.util.Date();
            SimpleDateFormat sdfFooter = new SimpleDateFormat("yyyy");
            String strYYYY = sdfFooter.format(dFooter);
            SimpleDateFormat tz = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
             String completeTimewithLocalTZ =  tz.format(dFooter);
             long LongTime = dFooter.getTime()/1000;

        %>

        <footer>
            <div align="center">
                <p>&copy; Molla Technologies 2009-<%=strYYYY%> (www.mollatech.com)</p>
                <p>Local Date and Time::<%=completeTimewithLocalTZ%> (<%=LongTime%>)</p>                
            </div>
        </footer>

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="./assets/js/jquery.js"></script>
        <script src="./assets/js/bootstrap-transition.js"></script>
        <script src="./assets/js/bootstrap-alert.js"></script>
        <script src="./assets/js/bootstrap-modal.js"></script>
        <script src="./assets/js/bootstrap-dropdown.js"></script>
        <script src="./assets/js/bootstrap-scrollspy.js"></script>
        <script src="./assets/js/bootstrap-tab.js"></script>
        <script src="./assets/js/bootstrap-tooltip.js"></script>
        <script src="./assets/js/bootstrap-popover.js"></script>
        <script src="./assets/js/bootstrap-button.js"></script>
        <script src="./assets/js/bootstrap-collapse.js"></script>
        <script src="./assets/js/bootstrap-carousel.js"></script>
        <script src="./assets/js/bootstrap-typeahead.js"></script>
        <script src="./assets/js/login.js"></script>
        
        <link rel="stylesheet" href="./assets/css/datepicker.css">
        <link rel="stylesheet" href="./assets/css/jquery.sidr.dark.css">

        <script src="./assets/js/json_sans_eval.js"></script>
        <script src="./assets/js/bootbox.min.js"></script>
        
        
        
        

        <script language="javascript">
                        TellTimezone();
//                       alert(navigator.userAgent); 
        </script>
    </body>
</html>
