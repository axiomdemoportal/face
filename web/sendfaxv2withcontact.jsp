<%@include file="header.jsp" %>
<script src="./assets/js/ajaxfileupload.js"></script>
<script src="./ckeditor/ckeditor.js"></script>
<script src="./assets/js/dictum/fax.js"></script>
<%@page import="com.mollatech.axiom.nucleus.db.Contacts"%>
<%@page import="com.mollatech.dictum.management.ContactManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Templates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%@page import="com.mollatech.axiom.nucleus.settings.ContactTagsSetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<h1 class="text-success">Send Fax To Group</h1>
<div class="tabbable">
    <ul class="nav nav-tabs">
        <li class="active" ><a href="#primary" data-toggle="tab">Message as FAX</a></li>
        <li ><a href="#secondary" data-toggle="tab">File as FAX</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="primary">
            <form class="form-horizontal" id="fax" name="fax">
                <fieldset>
                    <%
                        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
                        Channels channelOnj = (Channels) session.getAttribute("_apSChannelDetails");                        
                        SettingsManagement smng = new SettingsManagement();
                        ContactTagsSetting tagssettingObj = (ContactTagsSetting) smng.getSetting(sessionId, channelOnj.getChannelid(), SettingsManagement.CONTACT_TAGS, SettingsManagement.PREFERENCE_ONE);
                        String[] tags = tagssettingObj.getTags();
                        TemplateManagement tempObj = new TemplateManagement();
                        Templates[] templates = tempObj.Listtemplates(sessionId, channelOnj.getChannelid());

                        ContactManagement cmObj = new ContactManagement();

                    %>

                    <div class="control-group">
                        <label class="control-label"  for="toNumber">Select TAG</label>
                        <div class="controls">
                            <select class="selectpicker" name="_tagID" id="_tagID">

                                <% for (int j = 0; j < tags.length; j++) {
                                        int iCount = cmObj.getContactCountBytag(sessionId, channelOnj.getChannelid(), tags[j], ContactManagement.FAX);
                                        if (iCount == 0) {
                                %>                                 
                                <option value="<%=tags[j]%>" disabled><%=tags[j]%> (<%=iCount%> contacts)</option>                                             
                                <% } else {
                                %>
                                <option value="<%=tags[j]%>"><%=tags[j]%> (<%=iCount%> contacts)</option>                                             
                                <%
                                        }
                                    }
                                %>
                            </select>

                        </div>
                    </div>  

                    <div class="control-group">
                        <label class="control-label"  for="toNumber">Choose Template</label>
                        <div class="controls">
                            <select class="selectpicker" name="_templateID" id="_templateID" onchange="LoadFaxBody()">
                                <option  value="...">....</option>
                                <% for (int j = 0; j < templates.length; j++) {

                                        Templates t = templates[j];
                                        int iType = t.getType();
                                        // if(iType == 2){
                               %><%if (iType == 2) {%>
                                <option  value="<%=t.getTemplateid()%>"><%=t.getTemplatename()%></option>                                             
                                <% }

                                    }%>
                            </select>
                            : with speed 
                            <select name="_speed" id="_speed">
                                <option value="1" >Slow</option>
                                <option value="3" >Normal</option>
                                <option value="10" >Fast</option>                            
                                <option value="30" >Hyper Fast</option>                            
                            </select>
                        </div>
                    </div>     



                    <!--                    <input type="hidden" id="_type" name="_type" value="5">
                                        <input type="hidden" id="_typeS" name="_typeS" value="6">-->
                     <input type="hidden" id="emailCotents" name="emailCotents">
                    <div class="control-group">
                        <label class="control-label"  for="username">Body</label>
                        <div class="controls">
                            <textarea class="ckeditor" id="_messageemailbody" name="_messageemailbody" ></textarea>                                    
                            <br>Attention: #name#,#date#,#datetime#,#email# and #phone# are going to get replaced with real values.
                        </div>                                
                    </div>
                    <hr>
                    <div id="trackingDiv" ></div>
                    <script type="text/javascript">
                                CKEDITOR.replace('_messageemailbody');
                                timer = setInterval('updateDiv()', 100);
                                function updateDiv() {
                                    var editorText = CKEDITOR.instances._messageemailbody.getData();
                                    $('#emailCotents').val(editorText);
                                }
                    </script>       
                    <!-- Submit -->
                    <div class="control-group">
                        <div class="controls">
                            <button class="btn btn-primary btn-large" onclick="sendfaxv3(5)" type="button">Send Messages Now >></button>
                            <div id="fax-gateway-result"></div>
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
        <div class="tab-pane" id="secondary">
            <form class="form-horizontal" id="fax1" name="fax1">
                <fieldset>

                    <div class="control-group">
                        <label class="control-label"  for="toNumber">Select TAG</label>
                        <div class="controls">
                            <select class="selectpicker" name="_tagID" id="_tagID">

                                <% for (int j = 0; j < tags.length; j++) {
                                        int iCount = cmObj.getContactCountBytag(sessionId, channelOnj.getChannelid(), tags[j], ContactManagement.FAX);
                                        if (iCount == 0) {
                                %>                                 
                                <option value="<%=tags[j]%>" disabled><%=tags[j]%> (<%=iCount%> contacts)</option>                                             
                                <% } else {
                                %>
                                <option value="<%=tags[j]%>"><%=tags[j]%> (<%=iCount%> contacts)</option>                                             
                                <%
                                        }
                                    }
                                %>
                            </select>

                            : with speed 
                            <select name="_speedS" id="_speedS">
                                <option value="1" >Slow</option>
                                <option value="3" >Normal</option>
                                <option value="10" >Fast</option>                            
                                <option value="30" >Hyper Fast</option>                            
                            </select>


                        </div>
                    </div>  


<!--                    <div class="control-group">
                                    <label class="control-label"  for="username">Select License File</label>                                    
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="input-append">
                                                <div class="uneditable-input span4"><i class="icon-file fileupload-exists"></i> 
                                                    <span class="fileupload-preview"></span>
                                                </div>
                                                <span class="btn btn-file">
                                                    <span class="fileupload-new">Select file</span>
                                                    <span class="fileupload-exists">Change</span>
                                                    <input type="file" id="licensefile" name="licensefile"/>
                                                </span>
                                                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                        </div>
                                     <button class="btn btn-success" id="buttonRegisterLicense" onclick="UploadFAXFilev2()">Register Now>></button>
                      </div>-->

                    <div class="control-group">
                        <label class="control-label"  for="username">Select File</label>
                        <div class="controls">
                            <input type="hidden" id="ads_imageEAD" name="ads_imageEAD">
                            <a id="displayTextEAD" href="#" onclick="toogleFAXEAD()">Click to upload</a>
                            <div id="uploadImageEAD" style="display: none">
                                <form id="uploadFormEAD" id="uploadFormEAD">
                                    <input id="fileToUploadEAD" type="file" name="fileToUploadEAD"/>
                                    <button class="btn btn-mini btn-primary" id="buttonUploadEAD" onclick="return UploadFAXFile()">Upload File Now>></button>
                                </form>
                            </div>
                        </div>
                    </div>


                    <!-- Submit -->
                    <div class="control-group">
                        <div class="controls">
                            <button class="btn btn-primary btn-large" onclick="sendfaxv3Attachment(6)" type="button">Send Messages Now >></button>
                            <div id="fax-gateway-attachment-result"></div>
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>
</div>
