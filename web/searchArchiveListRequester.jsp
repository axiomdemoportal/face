<%@page import="com.mollatech.axiom.nucleus.db.Units"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet"%>
<%@page import="com.mollatech.axiom.nucleus.settings.ContactTagsSetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.connector.user.TokenStatusDetails"%>
<%@page import="com.mollatech.axiom.nucleus.db.Certificates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Pkitokens"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/authorization.js"></script>
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>

<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/bootstrap-datepicker.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/css/datepicker.css">
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">


<div class="container-fluid">
    <h1 class="text-success">Archived Requests</h2>
        <h3>Make Your Own Search</h3>
        <form class="form-horizontal" id="AddNewSchedulerForm" name="AddNewSchedulerForm">
            <div class="row-fluid">
                <div class="span12">
                    <div class="control-group form-inline">

                        <div class="control-group">
                            <label class="control-label"  for="username"> </label>
                            <div class="controls"> Search By :
                                <div class="input-prepend">
                                    <select span="1"  name="_searchTypeR" id="_searchTypeR" >
                                        <option value="-1">Select</option>
                                        <option value="0">All</option>
                                        <option value="1">Units</option>
                                        <option value="2">Operators</option>     
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="_schedulerStatusR">
                            Units :
                            <select  name="_unitIDR" id="_unitIDR" class="input-large" >
                                <%
                                    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                                    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
                                    UnitsManagemet unitMngt = new UnitsManagemet();
                                    Units[] arrUnits = unitMngt.ListUnitss(sessionId, channel.getChannelid());
                                    if (arrUnits != null) {
                                        for (int j = 0; j < arrUnits.length; j++) {
                                %>
                                <option data-content="<span class='label label-success'><%=arrUnits[j].getUnitid()%></span>" value="<%=arrUnits[j].getUnitid()%>"><%=arrUnits[j].getUnitname()%></option>                                             
                                <%}
                                    }%>
                            </select>
                            Status :
                            <select span="1"  name="_statusR" id="_statusR" class="input-medium" >
                                <!--<option value="-1">--</option>-->
                                <option value="0">All</option>
                                <option value="1">Approved</option>
                                <option value="-5">Rejected</option>     
                            </select>
                            <!--Start Time :-->
                            <div class="input-prepend">

                                <!--<div class="well">-->
                                <span class="add-on">From:</span>   
                                <div id="datetimepicker1" class="input-append date">
                                    <input id="startdate" name="startdate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                                <!--</div>-->
                            </div>
                            <!--End Time :-->
                            <div class="input-prepend">
                                <!--<div class="well">-->
                                <span class="add-on">till:</span>   
                                <div id="datetimepicker2" class="input-append date">
                                    <input id="enddate" name="enddate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                                <!--</div>-->
                            </div>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $("#_unitIDR").select2()
                                });
                                $(function () {
                                    $('#datetimepicker1').datepicker({
                                        language: 'pt-BR',
                                        format: 'dd/mm/yyyy'
                                    });
                                });
                                $(function () {
                                    $('#datetimepicker2').datepicker({
                                        language: 'pt-BR',
                                        format: 'dd/mm/yyyy'

                                    });
                                });

                                document.getElementById("_schedulerStatusR").style.display = 'none';

                            </script>
                            <div class="input-prepend">
                                <button class="btn btn-success" onclick="generateArchiveUnitsTableReq()" type="button">Filter</button>
                            </div>
                        </div>

                        <div id="_schedulerStatusRB">
                            Operators Name :
                            <select  name="_unitIDRB" id="_unitIDRB" class="input-large"  >
                                <%
                                    Channels channel1 = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                                    OperatorsManagement smng = new OperatorsManagement();
                                    AxiomOperator[] arrOpR = smng.GetOperatorByTypeV2(channel1.getChannelid(), OperatorsManagement.REQUESTER, OperatorsManagement.AUTHORIZER);

                                    if (arrOpR != null) {
                                        for (int j = 0; j < arrOpR.length; j++) {
                                %>
                                <option data-content="<span class='label label-success'><%=arrOpR[j].strOperatorid%></span>"value="<%=arrOpR[j].strOperatorid%>"><%=arrOpR[j].strName%></option>                                             
                                <%}
                                    }%>
                            </select>

                            Status :
                            <select span="1"  name="_statusRB" id="_statusRB" class="input-medium" >
                                <!--<option value="-1">--</option>-->
                                <option value="0">All</option>
                                <option value="1">Approved</option>
                                <!--                                <option value="-2">Rejected</option>     -->
                                <option value="-5">Rejected</option> 
                            </select>
                            <!--Start Time :-->
                            <div class="input-prepend">

                                <!--<div class="well">-->
                                <span class="add-on">From:</span>   
                                <div id="datetimepicker1B" class="input-append date">
                                    <input id="startdateB" name="startdate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                                <!--</div>-->
                            </div>
                            <!--End Time :-->
                            <div class="input-prepend">
                                <!--<div class="well">-->
                                <span class="add-on">till:</span>   
                                <div id="datetimepicker2B" class="input-append date">
                                    <input id="enddateB" name="enddate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                                <!--</div>-->
                            </div>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $("#_unitIDRB").select2()
                                });
                                //                          document.getElementById("_schedulerStatus1").style.display = 'none'; 
                                $(function () {
                                    $('#datetimepicker1B').datepicker({
                                        language: 'pt-BR',
                                        format: 'dd/mm/yyyy'
                                    });
                                });
                                $(function () {
                                    $('#datetimepicker2B').datepicker({
                                        language: 'pt-BR',
                                        format: 'dd/mm/yyyy'


                                    });
                                });
                            </script>
                            <div class="input-prepend">
                                <button class="btn btn-success" onclick="generateArchiveOperatorsTableReq()" type="button">Filter</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row-fluid">
                <div id="licenses_data_table">

                </div>
            </div>
        </form>


        <script type="text/javascript">

            document.getElementById("_schedulerStatusRB").style.display = 'none';
//             document.getElementById("_schedulerStatusR").style.display = 'none';

        </script>
</div>

<%@include file="footer.jsp" %>