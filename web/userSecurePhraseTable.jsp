<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.mollatech.axiom.nucleus.settings.ImageSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Securephrase"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SecurePhraseManagment"%>
<%@page import="com.mollatech.axiom.connector.user.TokenStatusDetails"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.Otptokens"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%

    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();
    String _searchtext = request.getParameter("_searchtext");
    UserManagement usermngObj = new UserManagement();
    SecurePhraseManagment secureMngt = new SecurePhraseManagment();
    AuthUser Users[] = null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    if (!StringUtils.isWhitespace(_searchtext)) {
        Users = usermngObj.SearchUsers(sessionId, _channelId, _searchtext);
    }
    if (Users == null) {
        Users = usermngObj.SearchUsersByID(sessionId, _channelId, _searchtext);
    }
    if (Users == null) {
        Otptokens otptokenObj = null;
        otptokenObj = new OTPTokenManagement(channel.getChannelid()).getOtpObjBySerialNo(sessionId, _searchtext);
        if (otptokenObj != null) {
            Users = usermngObj.SearchUsersByID(sessionId, _channelId, otptokenObj.getUserid());
        }
    }

    if (Users != null) {
%>
<h3>Results for <i>"<%=_searchtext%>"</i> in Secure Phrase (Image Authentication)</h3>
<div id="secureAudit">
    <table class="display responsive wrap" id="table_main">
        <thead>
            <tr>
                <th>No.</th>
                <th>UserId</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Remove</th>
                <th>Unlock?</th>
                <th>Audit</th>
                <th>Created On</th>
                <th>Last Updated On</th>
            </tr>
        </thead>

        <%
            SettingsManagement sMnt = new SettingsManagement();
            Object obj1 = null;
            ImageSettings iSettings = null;
            if (Users != null) {
                obj1 = sMnt.getSetting(sessionId, _channelId, SettingsManagement.IMAGE_SETTINGS, SettingsManagement.PREFERENCE_ONE);
            }
            if (obj1 != null) {
                if (obj1 instanceof ImageSettings) {
                    iSettings = (ImageSettings) obj1;
                }
            }
            for (int i = 0; i < Users.length; i++) {

                Securephrase sObj = secureMngt.GetSecurePhrase(sessionId, _channelId, Users[i].getUserId());

                if (sObj != null) {
                    String userStatus = "Active";
                    if (sObj.getWrongattempts() >= iSettings._lockedAttempt) {
                        userStatus = "locked";
                    }


        %>
        <tr id="user_search_<%=Users[i].getUserId()%>">
            <td><%=i + 1%></td>
            <td><a href="#" class="btn btn-mini" onclick="viewUserID('<%=Users[i].getUserId()%>')" >View ID</a></td>
            <td><%=Users[i].getUserName()%></td>
            <td><%=Users[i].getPhoneNo()%></td>
            <td><%=Users[i].getEmail()%></td>

            <td>
                <a href="#" class="btn btn-mini" onclick="removePhraseDetails('<%=Users[i].getUserId()%>')">Remove?</a>
            </td>
            <% if (sObj.getWrongattempts() >= iSettings._lockedAttempt) {%>
            <td>
                <a href="#" class="btn btn-danger btn-mini"  onclick="unlockPhraseDetails('<%=Users[i].getUserId()%>')"><%=userStatus%></a>
            </td>
            <%} else {%>
            <td>Active</td>        
            <%}%>        
            <td>
                <a href="#" class="btn btn-mini" onclick="loadSecurePhraseModalDetails('<%=Users[i].getUserId()%>', '<%=Users[i].getUserName()%>')">Audit Download</a>
            </td>
            <td><%=sdf.format(sObj.getCreatedOn())%></td>
            <td><%=sdf.format(sObj.getLastUpdateOn())%></td>
        </tr>
        <%
                }
            }
        %>
    </table>
    <%  } else { %>
    <h3>No users found...</h3>
    <%}%>
</div>
<br><br>
<script>
    $(document).ready(function () {
        $('#table_main').DataTable({
            responsive: true
        });
    });
</script>