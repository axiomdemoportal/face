<%@page import="com.mollatech.axiom.nucleus.settings.RootCertificateSettings"%>
<%@page import="com.mollatech.axiom.nucleus.settings.MobileTrustSettings"%>

<%@page import="com.mollatech.axiom.face.common.CountryCodes"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/mobiletrust.js"></script>


<%    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    //String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    SettingsManagement smng = new SettingsManagement();
    //MobileTrustSettings cfObj = (MobileTrustSettings) smng.getSetting(sessionId, channel.getChannelid(), SettingsManagement.MOBILETRUST_SETTING, SettingsManagement.PREFERENCE_ONE);

   Object obj =  smng.getSettingInner(channel.getChannelid(), smng.RootConfiguration, smng.PREFERENCE_ONE );
%>
<div class="container-fluid">
    <h1 class="text-success">Mobile Trust Settings</h1>
   <!--<h4><span class="marker">note: <em><strong><span class="marker">Please Fill Certificat</span>e Connector Setting First</strong></em></span></h4>-->

    <p>To facilitate multi layered security using various authentication mechanism, this configuration helps you with setting you needed configuration to enforce mobile trust.</p>
    <div class="row-fluid">
        <form class="form-horizontal" id="mobiletrustsettingsform" name="mobiletrustsettingsform">

            <input type="hidden" id="_ExpiryMin" name="_ExpiryMin">
            <input type="hidden" id="_Backup" name="_Backup">
            <input type="hidden" id="_SilentCall" name="_SilentCall">
            <input type="hidden" id="_SelfDestructEnable" name="_SelfDestructEnable">
            <input type="hidden" id="_SelfDestructAttempts" name="_SelfDestructAttempts">
            
            <input type="hidden" id="_timeStamp" name="_timeStamp">
            <input type="hidden" id="_deviceTracking" name="_deviceTracking">
            <input type="hidden" id="_geoFencing" name="_geoFencing">

            <label  for="username">Mobile Trust Settings:</label>
            <div class="control-group">
                <label class="control-label"  for="username">Home Country</label>
                <div class="controls">
                    <select class="span4" name="_CountryName" id="_CountryName">
                        <%CountryCodes code = new CountryCodes();
                            String[] arrCode = code.getCountryName();
                            for (int j = 0; j < arrCode.length; j++) {
                                String strcode = arrCode[j];
                        %>
                        <option value="<%=strcode%>"><%=strcode%></option>
                        <%}%>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"  for="username">Enforce Key Backup:</label>
                <div class="controls">

                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_backUp_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="SetBackupSetting(1, '#_backUp_div', 'Yes')">Yes</a></li>
                            <li><a href="#" onclick="SetBackupSetting(2, '#_backUp_div', 'No')">No</a></li>

                        </ul>
                    </div>

                </div>

            </div>     
            <div class="control-group">
                <label class="control-label"  for="username">Expiry Time:</label>
                <div class="controls">

                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_ExpiryMin_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="SetExpiryMin(1, '#_ExpiryMin_div', '1 minute')">1 minute</a></li>
                            <li><a href="#" onclick="SetExpiryMin(2, '#_ExpiryMin_div', '2 minutes')">2 minutes</a></li>
                            <li><a href="#" onclick="SetExpiryMin(3, '#_ExpiryMin_div', '3 minutes')">3 minutes</a></li>
                            <li><a href="#" onclick="SetExpiryMin(5, '#_ExpiryMin_div', '5 minutes')">5 minutes</a></li>
                            <li><a href="#" onclick="SetExpiryMin(7, '#_ExpiryMin_div', '7 minutes')">7 minutes</a></li>
                            <li><a href="#" onclick="SetExpiryMin(10, '#_ExpiryMin_div', '10 minutes')">10 minutes</a></li>

                        </ul>
                    </div>
                    Suppress Registration Code (via OOB):
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_silentCall_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="SetSilentSetting(1, '#_silentCall_div', 'Yes')">Yes</a></li>
                            <li><a href="#" onclick="SetSilentSetting(2, '#_silentCall_div', 'No')">No</a></li>

                        </ul>
                    </div>

                </div>

            </div>     
            <div class="control-group">
                <label class="control-label"  for="username">Self Destruct Alert:</label>
                <div class="controls">
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_selfDestructAlert_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="SetSelfDestructAlertSetting(1, '#_selfDestructAlert_div', 'Yes')">Yes</a></li>
                            <li><a href="#" onclick="SetSelfDestructAlertSetting(2, '#_selfDestructAlert_div', 'No')">No</a></li>

                        </ul>
                    </div>
                    Self Destruct Attempts:
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_selfDestructAttempts_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="SelfDestructAttempts(3, '#_selfDestructAttempts_div', '3 Attempts')">3 Attempts</a></li>
                            <li><a href="#" onclick="SelfDestructAttempts(5, '#_selfDestructAttempts_div', '5 Attempts')">5 Attempts</a></li>
                            <li><a href="#" onclick="SelfDestructAttempts(7, '#_selfDestructAttempts_div', '7 Attempts')">7 Attempts</a></li>

                        </ul>
                    </div>

                </div>

            </div>    
                    
                        <div class="control-group">
                <label class="control-label"  for="username">Timestamp:</label>
                <div class="controls">
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_timeStamp_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="Timestamp(1, '#_timeStamp_div', 'Enable')">Enable</a></li>
                            <li><a href="#" onclick="Timestamp(2, '#_timeStamp_div', 'Disable')">Disable</a></li>

                        </ul>
                    </div>
                    Device Tracking:
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_deviceTracking_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="DeviceTracking(1, '#_deviceTracking_div', 'Enable')">Enable</a></li>
                            <li><a href="#" onclick="DeviceTracking(2, '#_deviceTracking_div', 'Disable')">Disable</a></li>
                        </ul>
                    </div>
                     Geofencing:
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_geoFencing_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="GeoFencing(1, '#_geoFencing_div', 'Enable')">Enable</a></li>
                            <li><a href="#" onclick="GeoFencing(2, '#_geoFencing_div', 'Disable')">Disable</a></li>
                        </ul>
                    </div>

                </div>

            </div>

            <div class="control-group">
                <label class="control-label"  for="username">Self Destruct Alert URL:</label>
                <div class="controls">
                    <input type="text" id="_selfDestructURl" name="_selfDestructURl" placeholder="Enter Url like www.abc.com" class="span3">                                    
                </div>
            </div>

            <div class="control-group" id="mobile-trust-license">
                <label class="control-label"  for="username">License Key For Android:</label>
                <div class="controls">
                    <textarea id="_LicenseKeyforAndroid" name="_LicenseKeyforAndroid"  class="span9" rows="3" cols="20"></textarea>   
                </div>
            </div>
             <hr>
            <div class="control-group" id="mobile-trust-license">
                <label class="control-label"  for="username">License Key For iOS:</label>
                <div class="controls">
                    <textarea id="_LicenseKeyforIphone" name="_LicenseKeyforIphone"  class="span9" rows="3" cols="20"></textarea>   
                </div>
            </div>


            <hr>
            <!-- Submit -->
<!--            <div class="control-group">
                <div class="controls">
                    <div id="save-global-settings-result"></div>
                    <%if(obj != null){%>
                    <button class="btn btn-primary" onclick="editTrustSettings()" type="button">Save Setting Now >> </button>
                    <%}else{%>
                    <button class="btn btn-primary" onclick="editTrustSettings()" disabled="true" type="button">Save Setting Now >> </button>
                    <%}%>
                </div>
            </div>-->
                    <%//if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
            <div class="control-group">
                <div class="controls">
                    <div id="save-global-settings-result"></div>
                    <%if (obj != null) {%>
                    <button class="btn btn-primary" onclick="editTrustSettings()" type="button">Save Setting Now >> </button>
                    <%} else {%>
                    <button class="btn btn-primary" onclick="editTrustSettings()" disabled="true" type="button">Save Setting Now >> </button>
                    <%}%>
                </div>
            </div>

            <%//}%>



        </form>
    </div>


    <script>



    </script>



    <script language="javascript" type="text/javascript">
        LoadTrustSettings();
    </script>
</div>



<%@include file="footer.jsp" %>