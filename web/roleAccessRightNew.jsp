<%@include file="header.jsp" %>
<script src="./assets/js/accessMatrix.js"></script>
<%@page import="com.mollatech.axiom.common.utils.UtilityFunctions"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.AxiomProtect"%>
<%@page import="java.io.ByteArrayInputStream"%>

<%@page import="com.mollatech.axiom.nucleus.db.Roles"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    OperatorsManagement opMngt = new OperatorsManagement();
    String roleId = request.getParameter("_roleId");
    int iRoleID = Integer.parseInt(roleId);
    Roles roleObj = opMngt.getRoleByRoleId(channel.getChannelid(), iRoleID);

%>
<div class="container-fluid">
    <%if (roleObj != null) {%>
    <h1 class="text-success">Role Access for <%=roleObj.getName()%></h1>
    <%} else {%>
    <h1 class="text-success">Role Access Management %></h1>
    <%}%>
    <div class="tabbable">
        <ul class="nav nav-tabs">
            <div class="tab-content">
             <li class="active"><a href="#SystemManagement" data-toggle="tab">Channel Managment</a></li>
            <li><a href="#Templates" data-toggle="tab">Templates</a></li>
            <li><a href="#Settings" data-toggle="tab">Settings</a></li>
            </div>
        </ul>
    </div>
    
    <div class="tab-content">
        <div class="tab-pane active" id="SystemManagement">
            
        </div>
    </div>
    
    
    <table>
        <tr>
            <td>
                <p class="text-info">System Management</p>
                <table class="table table-bordered table-hover table-striped" style="width: 600px;">

                    <%                                byte[] accessByte = roleObj.getAccessentry();
                        AccessMatrixSettings accessSettings = null;
                        if (accessByte != null) {
                            ByteArrayInputStream bais = new ByteArrayInputStream(AxiomProtect.AccessDataBytes(accessByte));
                            Object object = UtilityFunctions.deserializeFromObject(bais);

                            if (object != null) {
                                accessSettings = (AccessMatrixSettings) object;
                            }
                        }

                        if (accessSettings == null) {
                            accessSettings = new AccessMatrixSettings();
                        }
                    %>
                    <tr>
                        <td>Sr. No. </td>
                        <td>Type</td>
                        <td>List</td>
                        <td>Add</td>
                        <td>Edit</td>
                        <td>Delete</td>
                    </tr>
                    <tr>
                        <td>1 </td>
                        <td>Channels Management</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td>Not Available</td>

                    </tr>
                    <tr>
                        <td>2 </td>
                        <td>Operators Management</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td>Not Available</td>

                    </tr>
                    <tr>
                        <td>3 </td>
                        <td>Units Management</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td>4 </td>
                        <td>Templates Management</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td>5 </td>
                        <td>User Management</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>

                    <tr>
                        <td> 6</td>
                        <td>Challenge Response</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td><input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td> 7</td>
                        <td>Trusted Device Management</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td><input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td> 8</td>
                        <td>Geo-location Management</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td><input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td> 9</td>
                        <td>System Message Management</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td><input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td> </td>
                        <td><p><button class="btn btn-primary" onclick="saveMatrix('<%=roleId%>',<%=OperatorsManagement.MatrixChannel%>)">Save&raquo;</button></p></td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                    </tr>
                </table>
                <p class="text-info">System Configuration</p>
                <table class="table table-bordered table-hover table-striped" style="width: 600px;">

                    <tr>
                        <td>Sr. No. </td>
                        <td>Type</td>
                        <td>List</td>
                        <td>Add</td>
                        <td>Edit</td>
                        <td>Delete</td>
                    </tr>
                    <tr>
                        <td>1 </td>
                        <td>SMS Configuration</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td>2 </td>
                        <td>USSD Configuration</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td>3 </td>
                        <td>VOICE Configuration</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td>4 </td>
                        <td>EMAIL Configuration</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td>5 </td>
                        <td>Billing Manager</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td>6 </td>
                        <td>Push Configuration</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td>7 </td>
                        <td>User Source Configuration</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td>8 </td>
                        <td>Password Policy Configuration</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td>9 </td>
                        <td>Channel Profile Configuration</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td>10 </td>
                        <td>OTP Token</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td>11 </td>
                        <td>Certificate Connector</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td>12 </td>
                        <td>Mobile Trust</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td>13 </td>
                        <td>Radius</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td>14 </td>
                        <td>Global Settings</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td>15 </td>
                        <td>EPIN Settings</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td> </td>
                        <td><p><button class="btn btn-primary" onclick="saveMatrix('<%=roleId%>',<%=OperatorsManagement.MatrixChannel%>)">Save&raquo;</button></p></td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                    </tr>
                </table>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
                <p class="text-info">Security Management</p>
                <table class="table table-bordered  table-striped" style="width: 600px;">

                    <tr>
                        <td>Sr. No. </td>
                        <td>Type</td>
                        <td>List</td>
                        <td>Add</td>
                        <td>Edit</td>
                        <td>Delete</td>
                    </tr>
                    <tr>
                        <td>1 </td>
                        <td>OOB OTP Tokens</td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>SMS Token</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td><input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td> </td>
                        <td>VOICE Token</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td><input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td> </td>
                        <td>USSD Token</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td><input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td> </td>
                        <td>EMAIL Token</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td><input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td>2 </td>
                        <td>Software OTP Tokens</td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>Mobile Token</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td><input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>

                    <tr>
                        <td> </td>
                        <td>WEB Token</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td><input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
<!--                    <tr>
                        <td> </td>
                        <td>PC Token</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td><input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>-->
                    <tr>
                        <td>3 </td>
                        <td>Hardware OTP Tokens</td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>Hardware Token</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td><input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td>4 </td>
                        <td>PKI Tokens</td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>Certificate</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td><input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>

                    <tr>
                        <td> </td>
                        <td>Hardware PKI</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td><input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>
                    <tr>
                        <td> </td>
                        <td>Software PKI</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td><input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>


                    <tr>
                        <td> </td>
                        <td><p><button class="btn btn-primary" onclick="saveMatrix('<%=roleId%>',<%=OperatorsManagement.MatrixChannel%>)">Save&raquo;</button></p></td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                    </tr>
                </table>
                <p class="text-info">System Configuration</p>
                <table class="table table-bordered table-hover table-striped" style="width: 600px;">

                    <tr>
                        <td>Sr. No. </td>
                        <td>Type</td>
                        <td>List</td>
                        <td>Add</td>
                        <td>Edit</td>
                        <td>Delete</td>
                    </tr>
                    <tr>
                        <td>1 </td>
                        <td>SMS Configuration</td>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  readonly></td>
                            <%if (accessSettings.addChannel == true) { %>
                        <td> <input type="checkbox" checked id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox" id="_selAddChannel" name="_selAddChannel"  onchange="accessList('AddChannel')"></td>
                            <%}%>
                            <%if (accessSettings.editChannel == true) { %>
                        <td> <input type="checkbox"  checked id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%} else {%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>
                            <%}%>
                        <td> <input type="checkbox"   id="_selEditChannel" name="_selEditChannel"  onchange="accessList('EditChannel')"></td>

                    </tr>

                    <tr>
                        <td> </td>
                        <td><p><button class="btn btn-primary" onclick="saveMatrix('<%=roleId%>',<%=OperatorsManagement.MatrixChannel%>)">Save&raquo;</button></p></td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</div>