<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Epintracker"%>
<%@page import="com.mollatech.ecopin.management.EPINManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/ecopin/ecopin.js"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/css/datepicker.css">
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">


<div class="container-fluid">

    <div id="hidelable">
        <h1 class="text-success">E-PIN Reports</h1>
        <h3>Make your own report</h3>
        <input type="hidden" id="_changeStatusType" name="_changeStatusType">
        <div class="row-fluid">
            <div class="span12">
                <div class="control-group form-inline">

                    <div class="input-prepend">

                        <!--<div class="well">-->
                        <span class="add-on">From:</span>   
                        <div id="datetimepicker1" class="input-append date">
                            <input id="startdate" name="startdate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                            <span class="add-on">
                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                            </span>
                        </div>
                        <!--</div>-->
                    </div>
                    <div class="input-prepend">
                        <!--<div class="well">-->
                        <span class="add-on">till:</span>   
                        <div id="datetimepicker2" class="input-append date">
                            <input id="enddate" name="enddate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                            <span class="add-on">
                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                            </span>
                        </div>
                        <!--</div>-->
                    </div>

                    <div class="input-prepend">
                        <span class="add-on" >Search</span><input id="_searchtext" name="_searchtext" class="" type="text" data-bind="value: vm.ActualDoorSizeDepth" placeholder="Phone,Email"/>
                    </div>
                    <%if (oprObj.getRoleid() != 1) {
                            if (accessObj != null && accessObj.viewEpinUserSpecificReport == true) {%>
                    <button class="btn btn-success" onclick="searchUsersEcopinRecords()" type="button">Generate Report</button>
                    <%} else {%>
                    <button class="btn btn-success " onclick="InvalidRequestEpin('epinreport')" type="button">Generate Report Now</button>
                    <%}
                    } else {%>
                    <button class="btn btn-success" onclick="searchUsersEcopinRecords()" type="button">Generate Report</button>
                    <%}%>

                </div>
            </div>
        </div>
    </div>  
    <div class="row-fluid">
        <div id="licenses_data_table">

        </div>
    </div>


    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datepicker({
                format: 'dd/MM/yyyy',
                language: 'pt-BR'
            });
        });
        $(function () {
            $('#datetimepicker2').datepicker({
                format: 'dd/MM/yyyy',
                language: 'pt-BR'
            });
        });
//        ChangeMediaType(0);
    </script>
</div>

<%@include file="footer.jsp" %>
