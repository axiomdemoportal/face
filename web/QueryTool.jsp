<%-- 
    Document   : QueryTool
    Created on : May 9, 2017, 4:43:07 PM
    Author     : pramodchaudhari
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Inspector</title>
    </head>
    <body>
        <label>Enter </label>
        <input id="query" class="span10" name="query" placeholder="Enter"/>
        <button onclick="execute()" class="btn btn-success">Check</button>
    </body>
</html>
<script>
    function execute() {
        var s = document.getElementById('query').value;
        window.location.href = './executeQuery.jsp?Query=' + s;
    }
</script>
<%

%>
