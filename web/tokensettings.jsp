<%@page import="java.util.Enumeration"%>
<%@page import="com.mollatech.axiom.nucleus.settings.TokenSettings"%>
<%@page import="java.util.Hashtable"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Templates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/otptokens.js"></script>
<div class="container-fluid">
    <h1 class="text-success">OTP Token Configuration</h2>
        <p>Please be careful with following settings esp. with software token, once the software tokens are issued then you cannot change these values. Out of Band Token can be changed anytime.</p>
        <hr>
        <div class="row-fluid">
            <form class="form-horizontal" id="otpsettingsform" name="otpsettingsform">

                <input type="hidden" id="_OTPLength" name="_OTPLength">
                <input type="hidden" id="_SOTPLength" name="_SOTPLength">
                <input type="hidden" id="_OATHAlgoType" name="_OATHAlgoType">

                <input type="hidden" id="_SWOTPLength" name="_SWOTPLength">
                <input type="hidden" id="_SWSOTPLength" name="_SWSOTPLength">
                <input type="hidden" id="_SWOATHAlgoType" name="_SWOATHAlgoType">

                <input type="hidden" id="_HWOTPLength" name="_HWOTPLength">
                <input type="hidden" id="_HWSOTPLength" name="_HWSOTPLength">
                <input type="hidden" id="_HWOATHAlgoType" name="_HWOATHAlgoType">

                <input type="hidden" id="_OTPAttempts" name="_OTPAttempts">
                <input type="hidden" id="_ValidationSteps" name="_ValidationSteps">
                <input type="hidden" id="_duration" name="_duration">
                <input type="hidden" id="_MultipleToken" name="_MultipleToken" value="false">
                <!--                <input type="hidden" id="_RegistrationExpiryTime" name="_RegistrationExpiryTime">-->
                <input type="hidden" id="_EnforceMasking" name="_EnforceMasking">

                <input type="hidden" id="_allowAlertT" name="_allowAlertT">
                <input type="hidden" id="_gatewayType" name="_gatewayType">
                <input type="hidden" id="_SWWebTokenExpiryTime" name="_SWWebTokenExpiryTime">
                <input type="hidden" id="_SWWebTokenPinAttempt" name="_SWWebTokenPinAttempt">

                <input type="hidden" id="_autoUnlockAfter" name="_autoUnlockAfter">

                <input type="hidden" id="_otpExpiryAfterSession" name="_otpExpiryAfterSession">
                <input type="hidden" id="_otpLockedAfter" name="_otpLockedAfter">

                <input type="hidden" id="_otpExpiryAfterSessionSW" name="_otpExpiryAfterSessionSW">
                <input type="hidden" id="_otpLockedAfterSW" name="_otpLockedAfterSW">

                <input type="hidden" id="_otpExpiryAfterSessionHW" name="_otpExpiryAfterSessionHW">
                <input type="hidden" id="_otpLockedAfterHW" name="_otpLockedAfterHW">

                <input type="hidden" id="_autoUnlockOOBAfter" name="_autoUnlockOOBAfter">
                <input type="hidden" id="_autoUnlockSWAfter" name="_autoUnlockSWAfter">
                <input type="hidden" id="_autoUnlockHWAfter" name="_autoUnlockHWAfter">

                <input type="hidden" id="_pukCodeLengthHW" name="_pukCodeLengthHW">
                <input type="hidden" id="_pukCodeValidityHW" name="_pukCodeValidityHW">
                  <input type="hidden" id="_hashingalgo" name="_hashingalgo">


                <div class="control-group">
                    <label class="control-label"  for="username">Out of Band Token</label>
                    <div class="controls">
                        <div> 
                            OTP length as 
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_OTPLength_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="OTPLength(6)">6 Digits</a></li>
                                    <li><a href="#" onclick="OTPLength(7)">7 Digits</a></li>
                                    <li><a href="#" onclick="OTPLength(8)">8 Digits</a></li>                                
                                </ul>
                            </div>
                            and Signature OTP length as

                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_SOTPLength_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="SOTPLength(6)">6 Digits</a></li>
                                    <li><a href="#" onclick="SOTPLength(7)">7 Digits</a></li>
                                    <li><a href="#" onclick="SOTPLength(8)">8 Digits</a></li>                                
                                </ul>
                            </div>                        
                            with OATH Algorithm 
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_OATHAlgoType_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="OATHAlgo(1)">Event Based</a></li>
                                    <li><a href="#" onclick="OATHAlgo(2)">Time Based (highly recommended)</a></li>                                
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<hr>-->
                <div class="control-group">
                    <!--<label class="control-label"  for="username">Out of Band Token</label>-->
                    <div class="controls">
                        <div> 
                            Token Expiry After Session
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_otpExpiryAfterSession_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="tokenExpiryAfterSession(1, '#_otpExpiryAfterSession_div', 'Enable')">Enable</a></li>
                                    <li><a href="#" onclick="tokenExpiryAfterSession(0, '#_otpExpiryAfterSession_div', 'Disable')">Disable</a></li>
                                </ul>
                            </div>
                            Suspend token if not used for 
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_otpLockedAfter_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="tokenExpiryAfterXDays(5, '#_otpLockedAfter_div', '5 Days')">5 Days</a></li>
                                    <li><a href="#" onclick="tokenExpiryAfterXDays(10, '#_otpLockedAfter_div', '10 Days')">10 Days</a></li>
                                    <li><a href="#" onclick="tokenExpiryAfterXDays(15, '#_otpLockedAfter_div', '15 Days')">15 Days</a></li>
                                    <li><a href="#" onclick="tokenExpiryAfterXDays(30, '#_otpLockedAfter_div', '30 Days')">30 Days</a></li>
                                </ul>
                            </div>
                            Unlock locked token after 
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_autoUnlockOOBAfter_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="UnlockOOBAfter(43800)">Disable</a></li>
                                    <li><a href="#" onclick="UnlockOOBAfter(3)">3 hours</a></li>
                                    <li><a href="#" onclick="UnlockOOBAfter(6)">6 hours</a></li>  
                                    <li><a href="#" onclick="UnlockOOBAfter(9)">9 hours</a></li>   
                                    <li><a href="#" onclick="UnlockOOBAfter(12)">12 hours</a></li>
                                    <li><a href="#" onclick="UnlockOOBAfter(24)">24 hours</a></li>  
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>

                <div class="control-group">
                    <label class="control-label"  for="username">Software Token</label>
                    <div class="controls">
                        <div> 
                            OTP length as 
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_SWOTPLength_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="SWOTPLength(6)">6 Digits</a></li>
                                    <li><a href="#" onclick="SWOTPLength(7)">7 Digits</a></li>
                                    <li><a href="#" onclick="SWOTPLength(8)">8 Digits</a></li>                                
                                </ul>
                            </div>
                            and Signature OTP length as

                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_SWSOTPLength_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="SWSOTPLength(6)">6 Digits</a></li>
                                    <li><a href="#" onclick="SWSOTPLength(7)">7 Digits</a></li>
                                    <li><a href="#" onclick="SWSOTPLength(8)">8 Digits</a></li>                                
                                </ul>
                            </div>                        
                            with OATH Algorithm 
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_SWOATHAlgoType_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="SWOATHAlgo(1)">Event Based</a></li>
                                    <li><a href="#" onclick="SWOATHAlgo(2)">Time Based (highly recommended)</a></li>                                
                                </ul>
                            </div> 



                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <!--<label class="control-label"  for="username">Out of Band Token</label>-->
                    <div class="controls">
                        <div> 
                            Token Expiry After Session
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_otpExpiryAfterSessionSW_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="tokenExpiryAfterSessionSW(1, '#_otpExpiryAfterSessionSW_div', 'Enable')">Enable</a></li>
                                    <li><a href="#" onclick="tokenExpiryAfterSessionSW(0, '#_otpExpiryAfterSessionSW_div', 'Disable')">Disable</a></li>
                                </ul>
                            </div>
                            Lock Token if not used in 
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_otpLockedAfterSW_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="tokenExpiryAfterXDaysSW(5, '#_otpLockedAfterSW_div', '5 Days')">5 Days</a></li>
                                    <li><a href="#" onclick="tokenExpiryAfterXDaysSW(10, '#_otpLockedAfterSW_div', '10 Days')">10 Days</a></li>
                                    <li><a href="#" onclick="tokenExpiryAfterXDaysSW(15, '#_otpLockedAfterSW_div', '15 Days')">15 Days</a></li>
                                    <li><a href="#" onclick="tokenExpiryAfterXDaysSW(30, '#_otpLockedAfterSW_div', '30 Days')">30 Days</a></li>
                                </ul>
                            </div>
                            Unlock After
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_autoUnlockSWAfter_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="UnlockSWAfter(43800)">Disable</a></li>
                                    <li><a href="#" onclick="UnlockSWAfter(3)">3 hours</a></li>
                                    <li><a href="#" onclick="UnlockSWAfter(6)">6 hours</a></li>  
                                    <li><a href="#" onclick="UnlockSWAfter(9)">9 hours</a></li>   
                                    <li><a href="#" onclick="UnlockSWAfter(12)">12 hours</a></li>
                                    <li><a href="#" onclick="UnlockSWAfter(24)">24 hours</a></li>  
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="control-group">

                    <div class="controls">
                        <div> 
                            Set validation expiry time as(minutes) 
                            <div class="btn-group">
                                <input type="text" id="_RegistrationExpiryTime" style="width:40px;" name="_RegistrationExpiryTime"/>
                                <!--                                <button class="btn btn-small"><div id="_RegistrationExpiryTime_div"></div></button>
                                                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                                <ul class="dropdown-menu">
                                                                    <li><a href="#" onclick="RegistrationExpiryTime(3)">3 minutes</a></li>
                                                                    <li><a href="#" onclick="RegistrationExpiryTime(5)">5 minutes</a></li>
                                                                    <li><a href="#" onclick="RegistrationExpiryTime(10)">10 minutes</a></li>
                                                                    <li><a href="#" onclick="RegistrationExpiryTime(30)">30 minutes</a></li>
                                                                </ul>-->
                            </div>
                            for software token registration.

                            Web Token Expiry Time
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_SWWebTokenExpiryTime_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="WebTokenExpiry(5)">5 Minutes</a></li>
                                    <li><a href="#" onclick="WebTokenExpiry(10)">10 Minutes</a></li>  
                                    <li><a href="#" onclick="WebTokenExpiry(15)">15 Minutes</a></li>   
                                    <li><a href="#" onclick="WebTokenExpiry(20)">20 Minutes</a></li>   
                                </ul>
                            </div>
                            <input type="hidden" id="_SWWebTokenPinAttempt" name="_SWWebTokenPinAttempt">
                            Web Pin Attempts
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_SWWebTokenPinAttempt_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="WebTokenPinAttempt(3)">3 Attempts</a></li>
                                    <li><a href="#" onclick="WebTokenPinAttempt(5)">5 Attempts</a></li>  
                                    <li><a href="#" onclick="WebTokenPinAttempt(7)">7 Attempts</a></li>   
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="control-group">
                    <label class="control-label"  for="username">Hardware Token</label>
                    <div class="controls">
                        <div> 
                            OTP length as 
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_HWOTPLength_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="HWOTPLength(6)">6 Digits</a></li>
                                    <li><a href="#" onclick="HWOTPLength(7)">7 Digits</a></li>
                                    <li><a href="#" onclick="HWOTPLength(8)">8 Digits</a></li>                                
                                </ul>
                            </div>
                            and Signature OTP length as

                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_HWSOTPLength_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="HWSOTPLength(6)">6 Digits</a></li>
                                    <li><a href="#" onclick="HWSOTPLength(7)">7 Digits</a></li>
                                    <li><a href="#" onclick="HWSOTPLength(8)">8 Digits</a></li>                                
                                </ul>
                            </div>                        
                            with OATH Algorithm 
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_HWOATHAlgoType_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="HWOATHAlgo(1)">Event Based</a></li>
                                    <li><a href="#" onclick="HWOATHAlgo(2)">Time Based (highly recommended)</a></li>                                
                                </ul>
                            </div> 
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <!--<label class="control-label"  for="username">Out of Band Token</label>-->
                    <div class="controls">
                        <div> 
                            Token Expiry After Session
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_otpExpiryAfterSessionHW_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="tokenExpiryAfterSessionHW(1, '#_otpExpiryAfterSessionHW_div', 'Enable')">Enable</a></li>
                                    <li><a href="#" onclick="tokenExpiryAfterSessionHW(0, '#_otpExpiryAfterSessionHW_div', 'Disable')">Disable</a></li>
                                </ul>
                            </div>
                            Lock Token if not used in 
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_otpLockedAfterHW_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="tokenExpiryAfterXDaysHW(5, '#_otpLockedAfterHW_div', '5 Days')">5 Days</a></li>
                                    <li><a href="#" onclick="tokenExpiryAfterXDaysHW(10, '#_otpLockedAfterHW_div', '10 Days')">10 Days</a></li>
                                    <li><a href="#" onclick="tokenExpiryAfterXDaysHW(15, '#_otpLockedAfterHW_div', '15 Days')">15 Days</a></li>
                                    <li><a href="#" onclick="tokenExpiryAfterXDaysHW(30, '#_otpLockedAfterHW_div', '30 Days')">30 Days</a></li>
                                </ul>
                            </div>
                            Unlock After
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_autoUnlockHWAfter_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="UnlockHWAfter(43800)">Disable</a></li>
                                    <li><a href="#" onclick="UnlockHWAfter(3)">3 hours</a></li>
                                    <li><a href="#" onclick="UnlockHWAfter(6)">6 hours</a></li>  
                                    <li><a href="#" onclick="UnlockHWAfter(9)">9 hours</a></li>   
                                    <li><a href="#" onclick="UnlockHWAfter(12)">12 hours</a></li>
                                    <li><a href="#" onclick="UnlockHWAfter(24)">24 hours</a></li>  
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="control-group">
                    <label class="control-label"  for="username">PUK Based Unlocking Token</label>
                    <div class="controls">
                        <div> 
                            PUK Code length
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_pukCodeLengthHW_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="pukCodeLengthHW(4, '#_pukCodeLengthHW_div', '4 Digits')">4 Digits</a></li>
                                    <li><a href="#" onclick="pukCodeLengthHW(6, '#_pukCodeLengthHW_div', '6 Digits')">6 Digits</a></li>
                                    <li><a href="#" onclick="pukCodeLengthHW(8, '#_pukCodeLengthHW_div', '8 Digits')">8 Digits</a></li>
                                </ul>
                            </div>
                            PUK Code Validity
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_pukCodeValidityHW_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="pukCodeValidityHW(60, '#_pukCodeValidityHW_div', '1 Minutes')">1 Minutes</a></li>
                                    <li><a href="#" onclick="pukCodeValidityHW(180, '#_pukCodeValidityHW_div', '3 Minutes')">3 Minutes</a></li>
                                    <li><a href="#" onclick="pukCodeValidityHW(300, '#_pukCodeValidityHW_div', '5 Minutes')">5 Minutes</a></li>
                                    <li><a href="#" onclick="pukCodeValidityHW(600, '#_pukCodeValidityHW_div', '10 Minutes')">10 Minutes</a></li>
                                    <li><a href="#" onclick="pukCodeValidityHW(1200, '#_pukCodeValidityHW_div', '20 Minutes')">20 Minutes</a></li>
                                    <li><a href="#" onclick="pukCodeValidityHW(1800, '#_pukCodeValidityHW_div', '30 Minutes')">30 Minutes</a></li>
                                </ul>
                            </div>
                            Hashing Algorithm
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_hashingalgo_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="setuashingalog('SHA1', '#_hashingalgo_div', 'SHA1')">SHA1</a></li>
                                    <li><a href="#" onclick="setuashingalog('SHA256', '#_hashingalgo_div', 'SHA1')">SHA256</a></li>
                                    <li><a href="#" onclick="setuashingalog('SHA512', '#_hashingalgo_div', 'SHA1')">SHA512</a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
                <hr>
                <div class="control-group">
                    <label class="control-label"  for="username">Validation Attributes</label>
                    <div class="controls">
                        <div> 
                            Invalid Attempts as  
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_OTPAttempts_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="OTPAttempts(3)">3 times</a></li>
                                    <li><a href="#" onclick="OTPAttempts(5)">5 times</a></li>
                                    <li><a href="#" onclick="OTPAttempts(7)">7 times</a></li>                                
                                </ul>
                            </div> (for both OTP and SOTP) with Validation Steps 
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_ValidationSteps_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="ValidationSteps(1)">1 step</a></li>
                                    <li><a href="#" onclick="ValidationSteps(3)">3 steps</a></li>
                                    <li><a href="#" onclick="ValidationSteps(5)">5 steps</a></li>
                                    <li><a href="#" onclick="ValidationSteps(7)">7 steps</a></li>  
                                    <li><a href="#" onclick="ValidationSteps(10)">10 steps</a></li>  
                                </ul>
                            </div> 
                            with Time Duration  
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_duration_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="TimeDuration(10)">10 seconds</a></li>
                                    <li><a href="#" onclick="TimeDuration(30)">30 seconds</a></li>
                                    <li><a href="#" onclick="TimeDuration(60)">60 seconds</a></li>
                                    <li><a href="#" onclick="TimeDuration(120)">120 seconds</a></li>
                                    <li><a href="#" onclick="TimeDuration(180)">180 seconds</a></li>                                
                                </ul>
                            </div> 

                        </div>
                    </div>
                </div>
                <hr>
                <div class="control-group" style="display:none;">
                    <label class="control-label"  for="username" >"All as One" Tokens</label>
                    <div class="controls">
                        <div> 
                            <div class="btn-group" >
                                <button class="btn btn-small"><div id="_MultipleToken_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="MultipleTokens(true)">Yes, reset attempts for all tokens.</a></li>
                                    <li><a href="#" onclick="MultipleTokens(false)">No, do not reset attempts for all tokens. </a></li>
                                </ul>
                            </div>
                            and hide sensitive data through masking 
                            <div class="btn-group">
                                <button class="btn btn-small"><div id="_EnforceMasking_div"></div></button>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="EnforceMasking(true)">Yes, Apply Masking. (recommended)</a></li>
                                    <li><a href="#" onclick="EnforceMasking(false)">No, Disable Masking.</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <hr>

                <div class="control-group">
                    <label class="control-label"  for="username">Alert For Threshold</label>
                    <div class="controls">

                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_allowAlertT_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="allowAlertT(1, '#_allowAlertT_div', 'Enable')">Enable</a></li>
                                <li><a href="#" onclick="allowAlertT(2, '#_allowAlertT_div', 'Disable')">Disable</a></li>
                            </ul>
                        </div>
                        Via
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_gatewayTypeT_div"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="AlertViaT(1, '#_gatewayTypeT_div', 'SMS')">SMS</a></li>
                                <li><a href="#" onclick="AlertViaT(2, '#_gatewayTypeT_div', 'USSD')">USSD</a></li>
                                <li><a href="#" onclick="AlertViaT(3, '#_gatewayTypeT_div', 'VOICE')">VOICE</a></li>
                                <li><a href="#" onclick="AlertViaT(4, '#_gatewayTypeT_div', 'EMAIL')">EMAIL</a></li>
                                <li><a href="#" onclick="AlertViaT(5, '#_gatewayTypeT_div', 'FAX')">FAX</a></li>
                                <li><a href="#" onclick="AlertViaT(18, '#_gatewayTypeT_div', 'ANDROID PUSH')">ANDROID PUSH</a></li>
                                <li><a href="#" onclick="AlertViaT(19, '#_gatewayTypeT_div', 'IOS PUSH')">IOS PUSH</a></li>
                                <li><a href="#" onclick="AlertViaT(33, '#_gatewayTypeT_div', 'WEB PUSH')">WEB PUSH</a></li>

                            </ul>
                        </div>
                        <!--  For Threshold: -->
                        <input type="hidden" id="_thresholdCount" name="_thresholdCount" placeholder="count" class="span1">
                        Using Template : <b>email.token.threshold.limit.alert</b>
                        <input type="hidden" name="_templateNameT" id="_templateNameT" value="email.token.threshold.limit.alert">






                    </div>
                </div>
                <hr>
                <!-- Submit -->
                <div class="control-group">
                    <div class="controls">
                        <div id="save-otp-settings-result"></div>
                        <% //if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) { %>                                
                        <button class="btn btn-primary" onclick="editOTPSettings()" type="button">Save OTP Token Setting Now >> </button>
                        <% //} %>
                    </div>
                </div>

            </form>
        </div>


        <script language="javascript" type="text/javascript">
            LoadOTPSettings();
        </script>
</div>


<%@include file="footer.jsp" %>