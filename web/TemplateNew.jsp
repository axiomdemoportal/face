<%@include file="header.jsp" %>
<script src="./ckeditor/ckeditor.js"></script>
<script src="./assets/js/template.js"></script>
<div class="container-fluid">
    <h2 class="text-success">New Message Template</h2>
    <p>To facilitate out of band authentication and sms based notification, SMS Gateway connection needs to be configured. You can configure primary and secondary entries to have fail over option. </p>
    <br>
    <div class="tabbable">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#primary" data-toggle="tab">Mobile Message</a></li>
            <li><a href="#secondary" data-toggle="tab">Email Message</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="primary">
                <div class="row-fluid">
                    <form class="form-horizontal" id="templateform">
                        <fieldset>
                            <div id="legend">
                                <legend class="">Mobile Message Template (i.e SMS, Voice and USSD)</legend>
                            </div>

                            <input type="hidden" id="_types" name="_types" value="1">

                            <div class="control-group">
                                <label class="control-label"  for="username">Unique Name</label>
                                <div class="controls">
                                    <input type="text" id="_templatename" name="_templatename" placeholder="Unique Name for your reference" class="input-xlarge">

                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"  for="username">Message</label>
                                <div class="controls">
                                    <textarea id="_templatebody" name="_templatebody" placeholder="Dear #user#, Amount #amount# withdraw from your account. Thanks regards #date# #time#." class="span6" cols="60" rows="2"></textarea>
                                    <br><i>Please keep it below 160 characters.</i>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"  for="username">Custom variables</label>
                                <div class="controls">
                                    <input type="text" id="_templatevariables" name="_templatevariables" placeholder="e.g. name,date,bankname" class="span6">
                                    <br><i>Please enter your own variables that will be replaced at runtime with real data for each user.</i>
                                </div>
                            </div>
                            <hr>
                            <!-- Submit -->
                            <div class="control-group">
                                <div class="controls">
                                    <a href="./TemplateMobileList.jsp" class="btn" type="button">Go to List </a>
                                    <button class="btn btn-primary" onclick="addtemplate()" type="button" id="SaveMobileMsgTemplate">Save Mobile Message Template >> </button>
                                    <div id="save-template-result"></div>
                                </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            <div class="tab-pane" id="secondary">
                <div class="row-fluid">
                    <form class="form-horizontal" id="emailtemplateForm" name="emailtemplateForm">
                        <fieldset>
                            <div id="legend">
                                <legend class="">Email Template (for both text or rich text)</legend>
                            </div>

                            <input type="hidden" id="_types" name="_types" value="2">
                            <input type="hidden" id="emailCotents" name="emailCotents" >

                            <div class="control-group">
                                <label class="control-label"  for="username">Unique Name</label>
                                <div class="controls">
                                    <input type="text" id="_templatenameS" name="_templatenameS" placeholder="" class="input-xlarge">

                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"  for="username">Subject </label>
                                <div class="controls">
                                    <input type="text" id="_templatesubjectS" name="_templatesubjectS" placeholder="Meaningful Subject" class="input-xlarge">
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label"  for="username"> Variables </label>
                                <div class="controls">
                                    <input type="text" id="_templatevariablesS" name="_templatevariablesS" placeholder="Name,Date,Bankname" class="input-xlarge">
                                    <br><i>Please enter your own variables that will be replaced at runtime with real data for each user.</i>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"  for="username">Body</label>
                                <div class="controls">
                                    <textarea class="ckeditor" id="_templatebodyS" name="_templatebodyS" ></textarea>                                    
                                </div>                                
                            </div>
                            <hr>
                            <div id="trackingDiv" ></div>
                            <script type="text/javascript">
                                        CKEDITOR.replace('_templatebodyS');
                                        timer = setInterval('updateDiv()', 100);
                                        function updateDiv() {
                                            var editorText = CKEDITOR.instances._templatebodyS.getData();
                                            $('#emailCotents').val(editorText);    
                                        }
                            </script>
                            <!-- Submit -->
                            <div class="control-group">
                                <div class="controls">
                                    <a href="./TemplateEmailList.jsp" class="btn" type="button">Go To List</a>
                                    <button class="btn btn-primary" onclick="addemailtemplate()" type="button" id="emailTemplate-button">Save Template Setting </button>
                                    <div id="save-secondrytemplate-result"></div>
                                </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<%@include file="footer.jsp" %>
