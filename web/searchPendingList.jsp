<%@page import="com.mollatech.axiom.nucleus.db.Units"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet"%>
<%@page import="com.mollatech.axiom.nucleus.settings.ContactTagsSetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.connector.user.TokenStatusDetails"%>
<%@page import="com.mollatech.axiom.nucleus.db.Certificates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Pkitokens"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement"%>
<%@include file="header.jsp" %> 
<script src="./assets/js/authorization.js"></script>
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<script src="./assets/js/bootstrap.timepicker.min.js"></script>
<link href="./assets/css/bootstrap-timepicker.min.css" rel="stylesheet"/>
<link type="text/css" href="css/bootstrap.min.css" />
<link href="./assets/css/bootstrap-responsive.min.css" rel="stylesheet"/>
<link href="./assets/css/bootstrap-timepicker.css" rel="stylesheet"/>
<script type="text/javascript" src="./assets/js/bootbox.min.js"></script>
<script src="./assets/js/bootstrap-timepicker.js"></script>


<div class="container-fluid">
    <h1 class="text-success">Pending List For Approval</h2>
        <h3>Make Your Own Search</h3>
        <form class="form-horizontal" id="AddNewSchedulerForm" name="AddNewSchedulerForm">
            <div class="row-fluid">
                <div class="span12">
                    <div class="control-group form-inline">

                        <div class="control-group">
                            <label class="control-label"  for="username"> </label>
                            <div class="controls"> Search By :
                                <div class="input-prepend">
                                    <select span="1"  name="_searchType" id="_searchType" >
                                        <option value="-1">Select</option>
                                        <option value="0">All</option>
                                        <!--<option value="1">Units</option>-->
                                        <option value="2">Operators</option>     
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="_schedulerStatus1">
                            Select Units Name :
                            <select  name="_unitIDs" id="_unitIDs" class="input-xlarge" >
                                <%                                

                                Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                                    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
                                    Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
                                    UnitsManagemet unitMngt = new UnitsManagemet();
                                    Units[] arrUnits = unitMngt.ListUnitss(sessionId, channel.getChannelid());
                                    if (arrUnits != null) {
                                        for (int j = 0; j < arrUnits.length; j++) {
                                %>
                                <option data-content="<span class='label label-success'><%=arrUnits[j].getUnitid()%></span>" value="<%=arrUnits[j].getUnitid()%>"><%=arrUnits[j].getUnitname()%></option>                                             
                                <%
                                        }
                                    }%>
                            </select>
                            Start Time :
                            <div class="input-prepend">  
                                <div class="bootstrap-timepicker">
                                    <input id="starTimeUnit" type="text" class="input-small">
                                </div>
                            </div>
                            End Time :
                            <div class="input-prepend">  
                                <div class="bootstrap-timepicker">
                                    <input id="endTimeUnit" type="text" class="input-small">
                                </div>
                            </div>
                            <script type="text/javascript">
                                $(document).ready(function() {
                                    $("#_unitIDs").select2()
                                });

                                document.getElementById("_schedulerStatus1").style.display = 'none';
                                $('#starTimeUnit').timepicker({
                                    minuteStep: 5,
                                    showInputs: false,
                                    disableFocus: true,
                                });
                                $('#endTimeUnit').timepicker({
                                    minuteStep: 5,
                                    showInputs: false,
                                    disableFocus: true
                                });
                            </script>
                            <div class="input-prepend">
                                <button class="btn btn-success" onclick="generateUnitsTable()" type="button">Filter</button>
                            </div>
                        </div>

                        <div id="_schedulerStatus2">
                            Select Operators Name :
                            <select  name="_tagIDMC1" id="_tagIDMC1" class="input-xlarge" >
                                <%
                                    Channels channel1 = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                                    OperatorsManagement smng = new OperatorsManagement();
                                    AxiomOperator[] arrOpR = smng.GetOperatorByTypeV2(channel1.getChannelid(), OperatorsManagement.REQUESTER, OperatorsManagement.AUTHORIZER);

                                    if (arrOpR != null) {

                                        for (int j = 0; j < arrOpR.length; j++) {
                                            
                                %>
                                <option data-content="<span class='label label-success'><%=arrOpR[j].strOperatorid%></span>" value="<%=arrOpR[j].strOperatorid%>"><%=arrOpR[j].strName%></option>                                             
                                <%
                                        }
                                    }%>
                            </select> 
                            <!--

                            Start Time :
                            <div class="input-prepend">  
                                <div class="bootstrap-timepicker">
                                    <input id="starTime" type="text" class="input-small">
                                </div>
                            </div>
                            End Time :
                            <div class="input-prepend">  
                                <div class="bootstrap-timepicker">
                                    <input id="endTime" type="text" class="input-small">
                                </div>
                            </div>
                            <script type="text/javascript">
                                $(document).ready(function() {
                                    $("#_tagIDMC1").select2()
                                });
                                //                          document.getElementById("_schedulerStatus1").style.display = 'none'; 
                                $('#starTime').timepicker({
                                    minuteStep: 5,
                                    showInputs: false,
                                    disableFocus: true
                                });
                                $('#endTime').timepicker({
                                    minuteStep: 5,
                                    showInputs: false,
                                    disableFocus: true
                                });
                            </script>
                            -->
                            <div class="input-prepend">
                                <button class="btn btn-success" onclick="generateOperatorsTable()" type="button">Filter</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row-fluid">
                <div id="licenses_data_table">

                </div>
            </div>
        </form>


        <script type="text/javascript">
            document.getElementById("_schedulerStatus2").style.display = 'none';

        </script>
</div>

<div id="rejectRequset" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3>Reject Request</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="editOperatorForm" name="editOperatorForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Reason</label>
                        <div class="controls">
                            <!--<input type="text" id="_reasonForRejection" name="_reasonForRejection" placeholder="Enter Rejection Reason" class="input-xlarge">-->
                            <textarea id="_reasonForRejection" name="_reasonForRejection" placeholder="Enter Rejection Reason" class="span9" cols="60" rows="3"></textarea>
                            <input type="hidden" id="_approvalid" name="_approvalid"/>
                            <input type="hidden" id="_unitId" name="_unitId"/>
                            <!--<input type="hidden" id="_opridE" name="_opridE"/>-->
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="editoperator-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="removeAuthorization()" id="buttonEditOperatorSubmit">Reject Request></button>
    </div>
</div>
<%@include file="footer.jsp" %>