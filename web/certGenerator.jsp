<%@include file="header.jsp" %>
<script src="./assets/js/ussdgateway.js"></script>
<script src="./assets/js/certGenerator.js" type="text/javascript"></script>
<div class="container-fluid">
    <h1 class="text-success">CSR AND Certificate Generator</h1>
    <p>TO generate certificates for appication and ssl purpose with available options as follows</p>
   <br/>
   <%
   String selfSignedCAName = LoadSettings.g_sSettings.getProperty("selfsigned.ca.name");
   String thirdpartCa = LoadSettings.g_sSettings.getProperty("thirparty.ca.name");
   %>
                <div class="row-fluid">
               <form class="form-horizontal" id="ussdprimaryform" name="ussdprimaryform">
                        <div class="control-group">
                      <label class="control-label"  for="type">Certificate Type:</label>
                      <div class="controls" >
                        <select span="1"  name="_certtype" id="_resourceName" >
                        <option value="1" onblur="displayCA(1)">Application Certificate(v3)</option>
                      <option value="2" onblur="displayCA(2)">SSL Certificate</option>
                             </select>
                    </div>
                          <label class="control-label"  for="type">Select CA(Certificate Issuer)</label>
                          <div class="controls">
                             <select span="1"  name="_certIssuer" id="" >
                             <option value="1" ><%=selfSignedCAName%></option>
                             <option value="2" ><%=thirdpartCa%></option>
                             </select>
                           </div>
                            </div>
                            <div class="controls" id="divCASSLCERTS" style="display:none">
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_status-primary-ussd"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ChangeActiveStatusUSSD(1,1)">Mark as Active?</a></li>
                                        <li><a href="#" onclick="ChangeActiveStatusUSSD(1,0)">Mark as Suspended?</a></li>
                                    </ul>
                                </div>
                                with Auto Fail-Over to Secondary Gateway 
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_autofailover-primary-ussd"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ChangeFailOverUSSD(1)">Mark as Enabled?</a></li>
                                        <li><a href="#" onclick="ChangeFailOverUSSD(0)">Mark as Disabled?</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" id="_statusUSSD" name="_statusUSSD">
                        <input type="hidden" id="_perferenceUSSD" name="_perferenceUSSD" value="1">
                        <input type="hidden" id="_typeUSSD" name="_typeUSSD" value="2">
                        <input type="hidden" id="_autofailoverUSSD" name="_autofailoverUSSD">
                        <input type="hidden" id="_retriesUSSD" name="_retriesUSSD">
                        <input type="hidden" id="_retrydurationUSSD" name="_retrydurationUSSD">

                        

                        <div class="control-group">
                            <label class="control-label"  for="username">Retry Count</label>
                            <div class="controls">
                                <div class="btn-group">
                                    <button class="btn btn-small"><div id="_retries-primary-ussd"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ChangeRetryUSSD(1,2)">2 Retries</a></li>
                                        <li><a href="#" onclick="ChangeRetryUSSD(1,3)">3 Retries</a></li>
                                        <li><a href="#" onclick="ChangeRetryUSSD(1,5)">5 Retries</a></li>
                                    </ul>
                                </div>
                                 with wait time between retries as  
                                 <div class="btn-group">
                                    <button class="btn btn-small"><div id="_retryduration-primary-ussd"></div></button>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ChangeRetryDurationUSSD(1,10)">10 seconds</a></li>
                                        <li><a href="#" onclick="ChangeRetryDurationUSSD(1,30)">30 seconds</a></li>
                                        <li><a href="#" onclick="ChangeRetryDurationUSSD(1,60)">60 seconds</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <hr>
                        
                        <div class="control-group">
                            <label class="control-label"  for="username">Host:Port</label>
                            <div class="controls">
                                <input type="text" id="_ipUSSD" name="_ipUSSD" placeholder="example localhost/127.0.0.1" class="span3">
                                : <input type="text" id="_portUSSD" name="_portUSSD" placeholder="443" class="span1">

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Authenticate using </label>
                            <div class="controls">
                                <input type="text" id="_userIdUSSD" name="_userIdUSSD" placeholder="enter user id for authenticaiton " class="span3">
                                : <input type="password" id="_passwordUSSD" name="_passwordUSSD" placeholder="leave blank is no authentication" class="span3">
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label"  for="username">Registered Phone</label>
                            <div class="controls">
                                <input type="text" id="_phoneNumberUSSD" name="_phoneNumberUSSD" placeholder="display number while sending" class="span3">                                
                            </div>
                        </div>
                        <hr>
                        <div class="control-group">
                            <label class="control-label"  for="username">Additional Attributes</label>
                            <div class="controls">
                                <input type="text" id="_reserve1USSD" name="_reserve1USSD" placeholder="additional details >> name1=value1" class="span3">
                                : <input type="text" id="_reserve2USSD" name="_reserve2USSD" placeholder="additional details >> name1=value1" class="span3">
                                : <input type="text" id="_reserve3USSD" name="_reserve3USSD" placeholder="additional details >> name1=value1" class="span3">
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label"  for="username">Implementation Class</label>
                            <div class="controls">
                                <input type="text" id="_classNameUSSD" name="_classNameUSSD" placeholder="complete class name including package" class="input-xlarge">
                            </div>
                        </div>
                         <!-- Submit -->
                        <div class="control-group">
                            <div class="controls">
                                <div id="save-ussd-gateway-primary-result"></div>
                                <%// if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) { %>
                                <button class="btn btn-primary" onclick="EditUSSDSetting(1)" type="button">Save Setting Now >> </button>
                                <%//}%>
                                <button class="btn" onclick="LoadTestUSSDConnectionUI(1)" type="button">Test Connection >> </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
          
        </div>
   


    <script language="javascript" type="text/javascript">
        LoadUSSDSetting(1);
        LoadUSSDSetting(2);
    </script>
</div>




<div id="testUSSDPrimary" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Test Primary Gateway</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="testUSSDPrimaryForm" name="testUSSDPrimaryForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Test Message</label>
                        <div class="controls">
                            <input type="text" id="_testmsg" name="_testmsg" placeholder="this is the sample message to be sent out..." class="input-xlarge">
                            <input type="hidden" id="_type" name="_type" value="2">
                            <input type="hidden" id="_preference" name="_preference" value="1">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Phone</label>
                        <div class="controls">
                            <input type="text" id="_testphone" name="_testphone" placeholder="set phone for notification" class="input-xlarge">
                        </div>
                    </div>                    
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div class="span3" id="test-ussd-primary-configuration-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="testconnectionUSSDprimary()" id="testUSSDPrimaryBut">Send Message Now</button>
    </div>
</div>

<div id="testUSSDSecondary" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Test Secondary Gateway</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="testUSSDSecondaryForm" name="testUSSDSecondaryForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Test Message</label>
                        <div class="controls">
                            <input type="text" id="_testmsgS" name="_testmsgS" placeholder="this is the sample message to be sent out..." class="input-xlarge">
                            <input type="hidden" id="_type" name="_type" value="2">
                            <input type="hidden" id="_preference" name="_preference" value="2">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Phone</label>
                        <div class="controls">
                            <input type="text" id="_testphoneS" name="_testphoneS" placeholder="set phone for notification" class="input-xlarge">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div class="span3" id="test-ussd-secondary-configuration-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="testconnectionUSSDsecondary()" id="testUSSDSecondaryBut">Send Message Now</button>
    </div>
</div>

<%@include file="footer.jsp" %>