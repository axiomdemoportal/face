<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserGroupsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Usergroups"%>
<%@include file="header.jsp" %>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<script src="./assets/js/usermanagement.js"></script>

<div class="container-fluid" >
    <div id="auditTable">
        <h1 class="text-success">User & Password Management</h1>
        <p>List of users in the system. Their password management can be managed from this interface.</p>
        <h3>Search Users</h3>   
        <div class="input-append">
            <form id="searchUserForm" name="searchUserForm">
                <input type="text" id="_keyword" name="_keyword" placeholder="Search using name, phone, email..." class="span4"><span class="add-on"><i class="icon-search"></i></span>
                <a href="#" class="btn btn-success" onclick="searchUsers()">Search Now</a>
            </form>
        </div>

        <%        Channels _apSChannelDetails = (Channels) session.getAttribute("_apSChannelDetails");
            OperatorsManagement oManagement = new OperatorsManagement();
            String _sessionID = (String) session.getAttribute("_apSessionID");
            Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        %>

        <div id="users_table_main">
        </div>

        <!--</div>-->
        </p>
        <br>
        <p><a href="#addNewUser" class="btn btn-primary" data-toggle="modal">Add New User&raquo;</a></p>
    </div>
    <div id="addNewUser" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3 id="myModalLabel">Add New User</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="addUserForm">
                    <fieldset>
                        <!-- Name -->
                        <div class="control-group">
                            <label class="control-label"  for="username">Name</label>
                            <div class="controls">
                                <input type="text" id="_Name" name="_Name" placeholder="full name" class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Email</label>
                            <div class="controls">
                                <input type="text" id="_Email" name="_Email" placeholder="valid email id" class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Phone</label>
                            <div class="controls">
                                <input type="text" id="_Phone" name="_Phone" placeholder="phone with country code" class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Group</label>
                            <div class="controls">

                                <select class="span4" name="_groupid" id="_groupid">
                                    <%
                                        UserGroupsManagement uMngt = new UserGroupsManagement();
                                        Usergroups[] uList = uMngt.ListGroups(sessionid, _apSChannelDetails.getChannelid());
                                        if (uList != null) {
                                            for (int i = 0; i < uList.length; i++) {

                                    %>
                                    <option value="<%=uList[i].getGroupid()%>"><%=uList[i].getGroupname()%></option>
                                    <%
                                            }
                                        }
                                    %>
                                </select>
                            </div>
                        </div>

                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <div id="addUser-result"></div>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button class="btn btn-primary" onclick="adduser()" id="addUserButton">Create User</button>
        </div>
    </div>
    <div id="editUser" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3><div id="idEditUser"></div></h3>
            <h3 id="myModalLabel">Edit User</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="editUserForm">
                    <fieldset>
                        <!-- Name -->
                        <input type="hidden" id="_userIdE" name="_userIdE" >
                        <div class="control-group">
                            <label class="control-label"  for="username">Name</label>
                            <div class="controls">
                                <input type="text" id="_NameE" name="_NameE" placeholder="localhost/127.0.10.1" class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Email</label>
                            <div class="controls">
                                <input type="text" id="_EmailE" name="_EmailE" placeholder="xyz@gmail.com" class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Phone</label>
                            <div class="controls">
                                <input type="text" id="_PhoneE" name="_PhoneE" placeholder="1234567890" class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Edit Group</label>
                            <div class="controls">

                                <select class="span4" name="_groupid" id="_groupid">
                                    <%
                                        if (uList != null) {
                                            for (int i = 0; i < uList.length; i++) {

                                    %>
                                    <option value="<%=uList[i].getGroupid()%>"><%=uList[i].getGroupname()%></option>
                                    <%
                                            }
                                        }
                                    %>
                                </select>
                            </div>
                        </div>

                        <hr>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <div id="editUser-result"></div>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <%String edituser = LoadSettings.g_sSettings.getProperty("user.edit");
                if (edituser.equals("true")) {
            %>
            <button class="btn btn-primary" onclick="edituser()" id="editUserButton">Edit User</button>
            <%}%>
        </div>
    </div>

    <div id="userauditDownload" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3><div id="idauditDownload"></div></h3>
            <h3 id="myModalLabel">Download Audit</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="testSMSPrimaryForm" name="testSMSPrimaryForm">
                    <fieldset>
                        <!-- Name -->
                        <input type="hidden" id="_auditUserID" name="_auditUserID"/>
                        <input type="hidden" id="_auditUserName" name="_auditUserName"/>
                        <div class="control-group">
                            <label class="control-label"  for="username">Start Date</label>
                            <div class="controls" align="left" >

                                <!--<span class="add-on">From:</span>-->   
                                <div id="Pushdatetimepicker1" class="input-append date">
                                    <input id="_auditStartDate" name="_auditStartDate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">End Date</label>
                            <div class="controls" align="left">
                                <!--<span class="add-on">Till:</span>-->   
                                <div id="Pushdatetimepicker2" class="input-append date">
                                    <input id="_auditEndDate" name="_auditEndDate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>                    
                    </fieldset>
                </form>
            </div>
        </div>

        <div class="modal-footer">
            <div id="editoperator-result"></div>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button class="btn btn-primary" onclick="searchUserAudit()" id="buttonEditOperatorSubmit">Show Audit</button>
        </div>
    </div>
    <script>
        $(function () {
            $('#Pushdatetimepicker1').datepicker({
                format: 'dd/MM/yyyy',
                language: 'pt-BR'
            });
        });
        $(function () {
            $('#Pushdatetimepicker2').datepicker({
                format: 'dd/MM/yyyy',
                language: 'pt-BR'
            });
        });
    </script>

    <%@include file="footer.jsp" %>