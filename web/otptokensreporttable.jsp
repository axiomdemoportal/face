cert<%@page import="com.mollatech.axiom.nucleus.db.Operators"%>
<%@page import="com.mollatech.axiom.connector.access.controller.AccessMatrixSettings"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.axiom.connector.user.TokenStatusDetails"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Otptokens"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%
    int SOFTWARE_TOKEN = 1;
    int HARDWARE_TOKEN = 2;
    int OOB_TOKEN = 3;
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();
    String _category = request.getParameter("_changeCategory");
    String _changeStatus = request.getParameter("_changeOTPStatus");
    String _UnitId = request.getParameter("_UnitId");
    String _otpStartDate = request.getParameter("_otpstartdate");
    String _otpEndDate = request.getParameter("_otpenddate");
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
    Date sDate = null;
    Date eDate = null;
    if (_otpStartDate != null && !_otpStartDate.isEmpty()) {
        sDate = formatter.parse(_otpStartDate);
    }
    if (_otpEndDate != null && !_otpEndDate.isEmpty()) {
        eDate = formatter.parse(_otpEndDate);
    }
    int iunitId = 0;
    if (_UnitId != null) {
        iunitId = Integer.parseInt(_UnitId);
    }
    int icategory = Integer.parseInt(_category);
    int istatus = Integer.parseInt(_changeStatus);
    OTPTokenManagement oObj = new OTPTokenManagement(_channelId);
    Otptokens[] arrObj = null;
//             oObj.getUserOTPTokenCountByCategory(channelId, category, status, unitId);

    if (iunitId != -1) //arrObj = oObj.searchOtpObjByStatusUnitiD(_channelId, icategory, istatus, iunitId);
    {
        arrObj = oObj.searchOtpObjByStatusUnitiDV2(_channelId, icategory, istatus, iunitId, sDate, eDate);
    } else //arrObj = oObj.searchOtpObjByStatusUnitiD(_channelId, icategory, istatus, 0);
    {
        arrObj = oObj.searchOtpObjByStatusUnitiDV2(_channelId, icategory, istatus, 0, sDate, eDate);
    }
    session.setAttribute("otpReportRecord", arrObj);
    String strerr = "No Record Found";

    String strtoken = null;
    if (icategory == SOFTWARE_TOKEN) {
        strtoken = "Software Token";
    } else if (icategory == HARDWARE_TOKEN) {
        strtoken = "Hardware Token";
    } else if (icategory == OOB_TOKEN) {
        strtoken = "OOB Token";
    }

    int ALL = OTPTokenManagement.TOKEN_STATUS_ALL;
    int SUSPEND = OTPTokenManagement.TOKEN_STATUS_SUSPENDED;
    int ASSIGN = OTPTokenManagement.TOKEN_STATUS_ASSIGNED;
    int LOCKED = OTPTokenManagement.TOKEN_STATUS_LOCKEd;
    int ACTIVE = OTPTokenManagement.TOKEN_STATUS_ACTIVE;
    int FREE = OTPTokenManagement.TOKEN_STATUS_FREE;
    int LOST = OTPTokenManagement.TOKEN_STATUS_LOST;

    String status = null;

    if (istatus == ALL) {
        status = "All States";
    } else if (istatus == SUSPEND) {
        status = "Suspended";
    } else if (istatus == ASSIGN) {
        status = "Assigned";
    } else if (istatus == LOCKED) {
        status = "Locked";
    } else if (istatus == ACTIVE) {
        status = "Active";
    } else if (istatus == FREE) {
        status = "Free";
    } else if (istatus == LOST) {
        status = "Lost";
    }


%>

<h3>Searched Results for Type is <i>"<%=strtoken%>" and State is "<%=status%>"</i></h3>

<div class="tabbable">

    <!-- newly added -->    
    <ul class="nav nav-tabs" id="otpTokenReport">
        <li class="active"><a href="#otpcharts" data-toggle="tab">Charts</a></li>
        <li><a href="#otpreport" data-toggle="tab">Tabular List</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="otpcharts">
            <div class="row-fluid">
                <div class="span12">
                    <div class="control-group">
                        <div class="span4">
                            <div id="otpReportgraph" ></div>

                        </div>
                        <div  class="span8">
                            <div id="otpReportgraph1"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-pane" id="otpreport">            
            <div class="row-fluid">
                <div class="span6">
                    <div class="control-group">                        
                        <div class="span1">
                            <div class="control-group form-inline">
                                <%Operators oprObjI = (Operators) session.getAttribute("_apOprDetail");
                                    AccessMatrixSettings accessObjN = (AccessMatrixSettings) session.getAttribute("_apAccessEntry");
                                    if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                        if (accessObjN != null && accessObjN.downloadOtpReport == true) {%>
                                <a href="#" class="btn btn-info" onclick="OTPReport()" >
                                    <%} else {%>
                                    <a href="#" class="btn btn-info" onclick="InvalidRequestOTPToken('otpreportdownload')" >
                                        <%}
                                        } else {%>
                                        <a href="#" class="btn btn-info" onclick="OTPReport()" >
                                            <%}%>
                                            <i class="icon-white icon-chevron-down"></i> CSV</a>
                                        </div>
                                        </div>
                                        <div class="span1">
                                            <div class="control-group form-inline">
                                                <!--<a href="#" class="btn btn-info" onclick="OTPReportpdf()" >-->
                                                <%if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                        if (accessObjN != null && accessObjN.downloadOtpReport == true) {%>
                                                <a href="#" class="btn btn-info" onclick="OTPReportpdf()" >
                                                    <%} else {%>
                                                    <a href="#" class="btn btn-info" onclick="InvalidRequestOTPToken('otpreportdownload')" >
                                                        <%}
                                                        } else {%>
                                                        <a href="#" class="btn btn-info" onclick="OTPReportpdf()" >
                                                            <%}%>
                                                            <i class="icon-white icon-chevron-down"></i> PDF</a>
                                                        </div>
                                                        </div>
                                                        <div class="span1">
                                                            <div class="control-group form-inline">
                                                                <!--                                    <a href="#" class="btn btn-info" onclick="userReportTXT()" >
                                                                                                        <i class="icon-white icon-chevron-down"></i>Download TXT</a>-->
                                                                <%    if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                        if (accessObjN != null && accessObjN.downloadOtpReport == true) {%>
                                                                <a href="#" class="btn btn-info" onclick="OTPReportTXT()" >
                                                                    <%} else {%>
                                                                    <a href="#" class="btn btn-info" onclick="InvalidRequestOTPToken('otpreportdownload')" >
                                                                        <%}
                                                                        } else {%>
                                                                        <a href="#" class="btn btn-info" onclick="OTPReportTXT()" >
                                                                            <%}%>
                                                                            <i class="icon-white icon-chevron-down"></i> TXT</a>
                                                                        </div>
                                                                        </div>
                                                                        </div>
                                                                        </div>
                                                                        </div>
                                                                        <table class="table table-striped" id="table_main">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th style="width: 10px">No.</th>
                                                                                    <th>User Name</th>
                                                                                    <th>Mobile</th>
                                                                                    <th>Email</th>
                                                                                    <th>Serial Number</th>
                                                                                    <th>Attempts</th>
                                                                                        <%if (icategory == OOB_TOKEN) {%>
                                                                                    <th>Out Of Band Token</th>
                                                                                        <%} else if (icategory == SOFTWARE_TOKEN) {%>
                                                                                    <th>Software Token</th>
                                                                                        <%} else if (icategory == HARDWARE_TOKEN) {%>
                                                                                    <th>Hardware Token</th>
                                                                                        <%}%>
                                                                                    <th>Created On</th>
                                                                                    <th>Last Access On</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <%
                                                                                if (arrObj != null) {
                                                                                    int iShowRecords = 0;
                                                                                    if (arrObj.length >= 1000) {
                                                                                        iShowRecords = 1000;
                                                                                    } else {
                                                                                        iShowRecords = arrObj.length;
                                                                                    }

                                                                                    for (int i = 0; i < arrObj.length; i++) {
                                                                                        int iStatus = arrObj[i].getStatus();
                                                                                        if (icategory != HARDWARE_TOKEN) {
                                                                                            if (iStatus != OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                                                                                                String uID = arrObj[i].getUserid();
                                                                                                UserManagement uObj = new UserManagement();

                                                                                                AuthUser[] Users = uObj.SearchUsersByID(sessionId, _channelId, uID);

                                                                                                String softstatus = "Unassigned";
                                                                                                //String hardstatus = "Unassigned";
                                                                                                //String OOBstatus = "Unassigned";

                                                                                                if (iStatus == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                                                                                                    //strLabelSW += " label-success";   //active
                                                                                                    softstatus = "Active";
                                                                                                } else if (iStatus == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
                                                                                                    //strLabelSW += " label-warning";   //locked
                                                                                                    softstatus = "Locked";
                                                                                                } else if (iStatus == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                                                                                                    //strLabelSW += " label-info";   //assigned                
                                                                                                    softstatus = "Assigned";

                                                                                                } else if (iStatus == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                                                                                                    softstatus = "Unassigned";

                                                                                                } else if (iStatus == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
                                                                                                    //strLabelSW += " label-important";   //suspended
                                                                                                    softstatus = "Suspended";

                                                                                                }

                                                                                                //                                String OOBtype = "---";
                                                                                                //                                String strLabelHW = "label";    //unassigned
                                                                                                //                                String strLabelOOB = "label";    //unassigned
                                                                                                //                                String strLabelSW = "label";    //unassigned
                                                                                                //                                TokenStatusDetails tokenDetailsOfSoft = null;
                                                                                                //                                TokenStatusDetails tokendetails[] = null;
                                                                                                //                                TokenStatusDetails tokenDetailsOfOOB = null;
                                                                                                //                                TokenStatusDetails tokenDetailsOfHard = null;
                                                                                                //                                tokendetails = oObj.getTokenList(sessionId, _channelId, uID);
                                                                                                //                                if (tokendetails != null) {
                                                                                                //                                    for (int j = 0; j < tokendetails.length; j++) {
                                                                                                //                                        if (tokendetails[j].Catrgory == OTPTokenManagement.SOFTWARE_TOKEN) {
                                                                                                //                                            tokenDetailsOfSoft = tokendetails[j];
                                                                                                //                                        } else if (tokendetails[j].Catrgory == OTPTokenManagement.HARDWARE_TOKEN) {
                                                                                                //                                            tokenDetailsOfHard = tokendetails[j];
                                                                                                //                                        } else if (tokendetails[j].Catrgory == OTPTokenManagement.OOB_TOKEN) {
                                                                                                //                                            tokenDetailsOfOOB = tokendetails[j];
                                                                                                //                                        }
                                                                                                //                                    }
                                                                                                //                                    if (tokenDetailsOfSoft != null) {
                                                                                                //                                        //
                                                                                                //                                        if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                                                                                                //                                            strLabelSW += " label-success";   //active
                                                                                                //                                            softstatus = "Active";
                                                                                                //                                        } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
                                                                                                //                                            strLabelSW += " label-warning";   //locked
                                                                                                //                                            softstatus = "Locked";
                                                                                                //                                        } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                                                                                                //                                            strLabelSW += " label-info";   //assigned                
                                                                                                //                                            softstatus = "Assigned";
                                                                                                //
                                                                                                //                                        } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                                                                                                //                                            softstatus = "Unassigned";
                                                                                                //
                                                                                                //                                        } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
                                                                                                //                                            strLabelSW += " label-important";   //suspended
                                                                                                //                                            softstatus = "suspended";
                                                                                                //
                                                                                                //                                        }
                                                                                                //                                    }
                                                                                                //
                                                                                                //                                    if (tokenDetailsOfHard != null) {
                                                                                                //
                                                                                                //                                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                                                                                                //                                            hardstatus = "Active";
                                                                                                //                                            strLabelHW += " label-success";   //active
                                                                                                //                                        }
                                                                                                //
                                                                                                //                                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_LOST) {
                                                                                                //                                            hardstatus = "Lost";
                                                                                                //                                            strLabelHW += " label-important";   //suspended
                                                                                                //
                                                                                                //                                        }
                                                                                                //
                                                                                                //                                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
                                                                                                //                                            hardstatus = "Locked";
                                                                                                //                                            strLabelHW += " label-warning";   //locked
                                                                                                //
                                                                                                //                                        }
                                                                                                //                                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                                                                                                //                                            hardstatus = "Assigned";
                                                                                                //                                            strLabelHW += " label-info";   //assigned                
                                                                                                //
                                                                                                //                                        }
                                                                                                //                                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                                                                                                //                                            hardstatus = "Unassigned";
                                                                                                //
                                                                                                //                                        }
                                                                                                //                                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
                                                                                                //                                            hardstatus = "suspended";
                                                                                                //                                            strLabelHW += " label-important";   //suspended
                                                                                                //                                        }
                                                                                                //                                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_FREE) {
                                                                                                //                                            hardstatus = "Free";
                                                                                                //                                            strLabelHW += " label-important";   //suspended
                                                                                                //
                                                                                                //                                        }
                                                                                                //                                    }
                                                                                                //
                                                                                                //                                    if (tokenDetailsOfOOB != null) {
                                                                                                //
                                                                                                //                                        if (tokenDetailsOfOOB.Status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                                                                                                //                                            strLabelOOB += " label-success";   //active
                                                                                                //
                                                                                                //                                            OOBstatus = "Active";
                                                                                                //                                        }
                                                                                                //                                        if (tokenDetailsOfOOB.Status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
                                                                                                //
                                                                                                //                                            strLabelOOB += " label-warning";   //locked
                                                                                                //
                                                                                                //                                            OOBstatus = "Locked";
                                                                                                //
                                                                                                //                                        }
                                                                                                //                                        if (tokenDetailsOfOOB.Status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                                                                                                //                                            strLabelOOB += " label-info";   //assigned                
                                                                                                //
                                                                                                //                                            OOBstatus = "Assigned";
                                                                                                //
                                                                                                //                                        }
                                                                                                //                                        if (tokenDetailsOfOOB.Status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                                                                                                //
                                                                                                //                                            OOBstatus = "Unassigned";
                                                                                                //
                                                                                                //                                        }
                                                                                                //                                        if (tokenDetailsOfOOB.Status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
                                                                                                //                                            strLabelOOB += " label-important";   //suspended
                                                                                                //                                            OOBstatus = "suspended";
                                                                                                //                                        }
                                                                                                //
                                                                                                //                                        if (tokenDetailsOfOOB.SubCategory == OTPTokenManagement.OOB__SMS_TOKEN) {
                                                                                                //                                            OOBtype = "SMS";
                                                                                                //
                                                                                                //                                        }
                                                                                                //                                        if (tokenDetailsOfOOB.SubCategory == OTPTokenManagement.OOB__VOICE_TOKEN) {
                                                                                                //                                            OOBtype = "VOICE";
                                                                                                //
                                                                                                //                                        }
                                                                                                //                                        if (tokenDetailsOfOOB.SubCategory == OTPTokenManagement.OOB__USSD_TOKEN) {
                                                                                                //
                                                                                                //                                            OOBtype = "USSD";
                                                                                                //
                                                                                                //                                        }
                                                                                                //                                        if (tokenDetailsOfOOB.SubCategory == OTPTokenManagement.OOB__EMAIL_TOKEN) {
                                                                                                //                                            OOBtype = "EMAIL";
                                                                                                //
                                                                                                //                                        }
                                                                                                //                                    }
                                                                                                //
                                                                                                //
                                                                                                //                                }
%>


                                                                            <tr>
                                                                                <td><%=(i + 1)%></td> 
                                                                                <%if (Users != null) {%>
                                                                                <td><%=Users[0].getUserName()%></td>
                                                                                <td><%=Users[0].getPhoneNo()%></td>
                                                                                <td><%=Users[0].getEmail()%></td>
                                                                                <%} else {%>
                                                                                <td>NA</td>
                                                                                <td>NA</td>
                                                                                <td>NA</td>
                                                                                <%}%>

                                                                                <td><%=arrObj[i].getSrno()%></td>
                                                                                <td><%=arrObj[i].getAttempts()%>                    
                                                                                <td><%= softstatus%></td>
                                                                                <td><%= sdf.format(arrObj[i].getCreationdatetime())%></td>
                                                                                <td><%= sdf.format(arrObj[i].getLastaccessdatetime())%></td>
                                                                            </tr>


                                                                            <%}
                                                                            } else if (icategory == HARDWARE_TOKEN) {
                                                                                //                     if (iStatus != OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                                                                                AuthUser[] Users = null;
                                                                                String uID = arrObj[i].getUserid();
                                                                                if (arrObj[i].getStatus() != OTPTokenManagement.TOKEN_STATUS_FREE) {
                                                                                    //                        System.out.println("in not free status");
                                                                                    UserManagement uObj = new UserManagement();

                                                                                    Users = uObj.SearchUsersByID(sessionId, _channelId, uID);
                                                                                }
                                                                                //String softstatus = "Unassigned";
                                                                                String hardstatus = "Unassigned";
                                                                                //String OOBstatus = "Unassigned";

                                                                                if (iStatus == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                                                                                    //strLabelSW += " label-success";   //active
                                                                                    hardstatus = "Active";
                                                                                } else if (iStatus == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
                                                                                    //strLabelSW += " label-warning";   //locked
                                                                                    hardstatus = "Locked";
                                                                                } else if (iStatus == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                                                                                    //strLabelSW += " label-info";   //assigned                
                                                                                    hardstatus = "Assigned";

                                                                                } else if (iStatus == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                                                                                    hardstatus = "Unassigned";

                                                                                } else if (iStatus == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
                                                                                    //strLabelSW += " label-important";   //suspended
                                                                                    hardstatus = "Suspended";

                                                                                } else if (iStatus == OTPTokenManagement.TOKEN_STATUS_LOST) {
                                                                                    //strLabelSW += " label-important";   //suspended
                                                                                    hardstatus = "Lost";

                                                                                }

                                                                            %>


                                                                            <tr>
                                                                                <td><%=(i + 1)%></td> 
                                                                                <%if (Users != null) {%>
                                                                                <td><%=Users[0].getUserName()%></td>
                                                                                <td><%=Users[0].getPhoneNo()%></td>
                                                                                <td><%=Users[0].getEmail()%></td>
                                                                                <%} else {%>
                                                                                <td><span class='label label-Default'>"+"Not Available"+"</span></td>
                                                                                <td><span class='label label-Default'>"+"Not Available"+"</span></td>
                                                                                <td><span class='label label-Default'>"+"Not Available"+"</span></td>
                                                                                <%}%>

                                                                                <td><%=arrObj[i].getSrno()%></td>
                                                                                <td><%=arrObj[i].getAttempts()%>                    
                                                                                <td><%= hardstatus%></td>
                                                                                <td><%= sdf.format(arrObj[i].getCreationdatetime())%></td>
                                                                                <td><%= sdf.format(arrObj[i].getLastaccessdatetime())%></td>
                                                                            </tr>
                                                                            <%out.flush();%>


                                                                            <%}
                                                                                    //                                                                                System.out.println("Array object end " + i + new Date());
                                                                                }

                                                                            } else {%>
                                                                            <td><%=1%></td>
                                                                            <td><%= strerr%></td>
                                                                            <td><%= strerr%></td>
                                                                            <td><%= strerr%></td>
                                                                            <td><%= strerr%></td>
                                                                            <td><%= strerr%></td>
                                                                            <%if (icategory == OOB_TOKEN) {%>
                                                                            <td><%= strerr%></td>
                                                                            <%} else if (icategory == SOFTWARE_TOKEN) {%>
                                                                            <td><%= strerr%></td>
                                                                            <td><%= strerr%></td>
                                                                            <%} else if (icategory == HARDWARE_TOKEN) {%>
                                                                            <td><%= strerr%></td>
                                                                            <%}%>
                                                                            <td><%= strerr%></td>
                                                                            <td><%= strerr%></td>
                                                                            <%}%>

                                                                        </table>

                                                                        <div class="row-fluid">
                                                                            <div class="span6">

                                                                                <div class="control-group">                        
                                                                                    <div class="span1">
                                                                                        <div class="control-group form-inline">
                                                                                            <%        if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                    if (accessObjN != null && accessObjN.downloadOtpReport == true) {%>
                                                                                            <a href="#" class="btn btn-info" onclick="OTPReport()" >
                                                                                                <%} else {%>
                                                                                                <a href="#" class="btn btn-info" onclick="InvalidRequestOTPToken('otpreportdownload')" >
                                                                                                    <%}
                                                                                                    } else {%>
                                                                                                    <a href="#" class="btn btn-info" onclick="OTPReport()" >
                                                                                                        <%}%>
                                                                                                        <i class="icon-white icon-chevron-down"></i> CSV</a>
                                                                                                    </div>
                                                                                                    </div>
                                                                                                    <div class="span1">
                                                                                                        <div class="control-group form-inline">
                                                                                                            <!--<a href="#" class="btn btn-info" onclick="OTPReportpdf()" >-->
                                                                                                            <%if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                                    if (accessObjN != null && accessObjN.downloadOtpReport == true) {%>
                                                                                                            <a href="#" class="btn btn-info" onclick="OTPReportpdf()" >
                                                                                                                <%} else {%>
                                                                                                                <a href="#" class="btn btn-info" onclick="InvalidRequestOTPToken('otpreportdownload')" >
                                                                                                                    <%}
                                                                                                                    } else {%>
                                                                                                                    <a href="#" class="btn btn-info" onclick="OTPReportpdf()" >
                                                                                                                        <%}%>
                                                                                                                        <i class="icon-white icon-chevron-down"></i> PDF</a>
                                                                                                                    </div>
                                                                                                                    </div>
                                                                                                                    <div class="span1">
                                                                                                                        <div class="control-group form-inline">
                                                                                                                            <!--                                    <a href="#" class="btn btn-info" onclick="userReportTXT()" >
                                                                                                                                                                    <i class="icon-white icon-chevron-down"></i>Download TXT</a>-->
                                                                                                                            <%    if (oprObjI.getRoleid() != 1) {//1 sysadmin
                                                                                                                                    if (accessObjN != null && accessObjN.downloadOtpReport == true) {%>
                                                                                                                            <a href="#" class="btn btn-info" onclick="OTPReportTXT()" >
                                                                                                                                <%} else {%>
                                                                                                                                <a href="#" class="btn btn-info" onclick="InvalidRequestOTPToken('otpreportdownload')" >
                                                                                                                                    <%}
                                                                                                                                    } else {%>
                                                                                                                                    <a href="#" class="btn btn-info" onclick="OTPReportTXT()" >
                                                                                                                                        <%}%>
                                                                                                                                        <i class="icon-white icon-chevron-down"></i> TXT</a>
                                                                                                                                    </div>
                                                                                                                                    </div>
                                                                                                                                    </div>
                                                                                                                                    </div>
                                                                                                                                    </div>
                                                                                                                                    </div>
                                                                                                                                    </div>
                                                                                                                                    <br><br>

                                                                                                                                    </div>                                                                                        
                                                                                                                                    <script>
                                                                                                                                        $(document).ready(function () {
                                                                                                                                            $('#table_main').DataTable({
                                                                                                                                                responsive: true
                                                                                                                                            });
                                                                                                                                        });
                                                                                                                                    </script>
