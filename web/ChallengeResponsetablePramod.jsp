<%@page import="com.mollatech.axiom.nucleus.db.Questionsandanswers"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.ChallengeResponsemanagement"%>
<%@page import="org.bouncycastle.asn1.cmp.Challenge"%>
<%@page import="com.mollatech.axiom.connector.user.QuestionAndAnswer"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<script src="./assets/js/challengeresponse.js"></script>

<%
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();
    String _searchtext = request.getParameter("_searchtext");

    UserManagement usermngObj = new UserManagement();
    AuthUser Users[] = null;
    Users = usermngObj.SearchUsers(sessionId, _channelId, _searchtext);
    if(Users==null)
    {
        Users = usermngObj.SearchUsersByID(sessionId, _channelId, _searchtext);
    }
    String strerr = "No Records Found";
//    if (Users != null) {

%>
<h3>Results for <i>"<%=_searchtext%>"</i> in Challenge Response (Q&A)</h3>

<table class="table table-striped" id="table_main">

    <tr>
        <td>No.</td>
        <td>UserId</td>
        <td>Name</td>
        <td>Mobile</td>
        <td>Email</td>
        <td>Status</td>
        <td>Challenge Response</td>
        <td>Attempts</td>
        <td>Audit</td>
        <td>Created On</td>
        <td>Last Access On</td>
    </tr>

    <%
    if(Users != null){
         for (int i = 0; i < Users.length; i++) {
              SimpleDateFormat sdf = new SimpleDateFormat("d MMM,yyyy HH:mm");

            String strStatus = "Inactive";

            String userStatus = "user-status-value-" + i;
            ChallengeResponsemanagement mgmt = new ChallengeResponsemanagement();
            Questionsandanswers QandA = mgmt.getRegisterUser(sessionId, _channelId, Users[i].getUserId());

            if (QandA != null) {

                if (QandA.getStatus() == 1) {

                    strStatus = "Active";

                } else if (QandA.getStatus() == 0) {

                    strStatus = "Suspended";

                } else {
                    strStatus = "Locked";
                }
            

             
    %>
    <!--<tr id="user_search_<%=Users[i].getUserId()%>">-->
    <tr> 
    <td><%=(i + 1)%></td>
     <td><a href="#" class="btn btn-mini" onclick="viewUserID('<%=Users[i].getUserId()%>')" >View ID</a></td>
        <td><%=Users[i].getUserName()%></td>
        <td><%=Users[i].getPhoneNo()%></td>
        <td><%=Users[i].getEmail()%></td>
        <td>
            <div class="btn-group">

                <button class="btn btn-mini" id="<%=userStatus%>"><%=strStatus%></button>
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#"  onclick="changeuserChallengestatus(1, '<%=Users[i].getUserId()%>', '<%=userStatus%>')" >Mark as Active?</a></li>
                    <li><a href="#" onclick="changeuserChallengestatus(0, '<%=Users[i].getUserId()%>', '<%=userStatus%>')" >Mark as Suspended?</a></li>
                    <li class="divider"></li>
                    <li><a href="#" onclick="removeChallengeUser('<%=Users[i].getUserId()%>')" data-toggle="modal"><font color="red">Remove?</font></a></li>
                </ul>
            </div>
        </td>
        <td>
            <div class="btn-group">
                <button class="btn btn-mini" >Manage</button>
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#"  onclick="changeuserChallengestatus(1, '<%=Users[i].getUserId()%>', '<%=userStatus%>')" >Unlock?</a></li>
                    <li><a href="./UserQuestionList.jsp?_userID=<%=Users[i].getUserId()%>" data-toggle="modal">List Questions?</a></li>
                    <!--<li><a href="#" onclick="loadQuestionsDetails('<%=Users[i].getUserId()%>')" data-toggle="modal">List questions?</a></li>-->
                </ul>
            </div>
        </td> 

        <td><%=QandA.getAttempts()%></td>
        <td>
            <a href="#" class="btn btn-mini" onclick="loadUserChallengeResponseAuditDetails('<%=Users[i].getUserId()%>','<%=Users[i].getUserName() %>')">Audit Download</a>
        </td>

        <td><%=QandA.getCreatedOn()%></td>
        <td><%=QandA.getLastUpdatedOn()%></td>
    </tr>
   
    <%} }}else{%>  
         <td><%=1%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                 <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
               
    <%}%>
 </table>
<br><br>
