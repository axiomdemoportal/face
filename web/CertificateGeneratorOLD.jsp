<%@page import="com.mollatech.axiom.nucleus.db.Countrylist"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.LocationManagement"%>
<%@page import="com.mollatech.axiom.nucleus.settings.RootCertificateSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@include file="header.jsp" %>
<script src="assets/js/certGenerator.js" type="text/javascript"></script>
<%    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    String[] strTags = null;
    SettingsManagement smng = new SettingsManagement();
    RootCertificateSettings rCertSettings = (RootCertificateSettings) smng.getSetting(sessionid, channel.getChannelid(), SettingsManagement.RootConfiguration, 1);
    if (rCertSettings != null) {
        if (rCertSettings.CAEmailides != null) {
            strTags = rCertSettings.CAEmailides.split(",");
        }
    }
    String selfSignedCAName = LoadSettings.g_sSettings.getProperty("selfsigned.ca.name");
    String thirdpartCa = LoadSettings.g_sSettings.getProperty("thirparty.ca.name");
%>

<script>
   

    $(document).ready(function () {
        //  var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
        $('#_CountryName').change(function () {
            var selectedValue = $(this).val();
            // alert(selectedValue);

            var s = './stateList.jsp?_countryId=' + selectedValue;

            $.ajax({
                type: 'GET',
                url: s,
                success: function (data) {

                    $('#_StateName').html(data);

                }
            });
        });
    });

</script>
<h1 class="text-success">Generate Certificate for Domain and Application Server</h2>
<div class="tabbable">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#primary" data-toggle="tab">SelfSigned Certs</a></li>
        <li><a href="#secondary" data-toggle="tab">ThirdParty CA</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="primary">
            <div id="certificaterGen">
                <div class="row-fluid">
                    <form class="form-horizontal" id="frmCertificateGeneration" name="frmCertificateGeneration">
                        <fieldset>
                            <input type="hidden" id="_auditUserIDPKI" name="_auditUserIDPKI"/>
                            <input type="hidden" id="_auditUserNamePKI" name="_auditUserNamePKI"/>
                            <!--                    Certificate Type:-->
                            <select span="1"  name="_certType" id="_certType" >
                                <option value="1">Application Certificates </option>
                                <option value="2">SSL Certificates</option>
                            </select>
                            <!--                    &nbsp;&nbsp;&nbsp;-->
                            <!--                    Issuer:-->
                            <select span="1" onblur="getDivforSSL();"  name="_caType" id="_caType" style="display:none" >
                                <option value="1" onclick="getDivforSSL();"><%=selfSignedCAName%> </option>
                                <option value="2"  onclick="getDivforSSL();"><%=selfSignedCAName%></option>
                            </select>

                            <!--                    Product Type:-->
                            <select span="1"  name="_product_type" id="_product_type"  style="display:none">
                                <option value="2">AlphaSSL:</option>
                                <option value="2">DomainSSL:</option>
                                <option value="2">OrganizationSSL:</option>
                                <option value="2">Extended SSL:</option>
                            </select>       
                            <h5>   Please fill Details below for csr generation:</h5>
                            <br/>
                            <div id="divSelfSigned">
                                <table>
                                    <tr>
                                        <td style="text-allign:right">
                                            Common Name:  
                                        </td>
                                        <td style="text-allign:left">
                                            <input id="common_name"  onblur="checkTextField(this);" placeholder="ip,domain name" style="width:170px" name="common_name" type="text" ></input>
                                        </td>
                                        <td>&nbsp;</td>
                                        <td style="text-allign:right">
                                            OrganizationName:
                                        </td>
                                        <td style="text-allign:left">
                                            <input  onblur="checkTextField(this);" id="_txtorganizationName" placeholder="ex.telecom malaysia" style="width:170px" name="_txtorganizationName" type="text" ></input>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td> &nbsp; &nbsp;</td> <td> &nbsp;&nbsp;</td> <td> &nbsp;&nbsp; </td> <td>&nbsp;&nbsp;  </td>
                                    </tr>
                                    <tr>
                                        <td style="text-allign:right">
                                            OrganizationUnit:
                                        </td>
                                        <td style="text-allign:left">
                                            <input id="_txtOrganizationUnit"   onblur="checkTextField(this);" style="width:170px" placeholder="ex.business management" name="_txtOrganizationUnit" type="text" ></input>
                                        </td>   <td>&nbsp;</td>
                                        <td style="text-allign:right">
                                            LocalityName:
                                        </td>
                                        <td style="text-allign:left">
                                            <input id="_txtLocalityName"  onblur="checkTextField(this);" style="width:170px" placeholder="ex.cyberjaya" name="_txtLocalityName" type="text" ></input>
                                        </td>
                                    </tr><tr>
                                        <td> &nbsp; &nbsp;</td> <td> &nbsp;&nbsp;</td> <td> &nbsp;&nbsp; </td> <td>&nbsp;&nbsp;  </td> 
                                    </tr>
                                    <tr>
                                        <td style="text-allign:right">
                                            State:
                                        </td>
                                        <td style="text-allign:left">
                                            <input id="_txtState" style="width:170px;"   onblur="checkTextField(this);"  placeholder="ex.Kaula Lampur" name="_txtState" type="text" ></input>
                                        </td>   <td>&nbsp;</td>
                                        <td style="text-allign:right">
                                            Country:
                                        </td>
                                        <td style="text-allign:left">

                                            <input id="_txtCountry" style="width:80px"   onblur="checkTextField(this);" name="_txtCountry" placeholder="ex.MY" type="text" ></input>
                                        </td>
                                    </tr><tr>
                                        <td> &nbsp; &nbsp;</td> <td> &nbsp;&nbsp;</td> <td> &nbsp;&nbsp; </td> <td>&nbsp;&nbsp;  </td>
                                    </tr>
                                    <tr>
                                        <td style="text-allign:right">
                                            Email
                                        </td>
                                        <td style="text-allign:left">
                                            <input id="_txtEmail" style="width:170px" style="display:none"   onblur="checkTextField(this);" placeholder="ex.pramod@mollatech.com" name="_txtEmail" type="text" ></input>
                                        </td>   <td>&nbsp;</td>
                                        <td style="text-allign:right">
                                            Validity         
                                        </td>
                                        <td style="text-allign:left">
                                            <input id="_txtval" style="width:50px"    onblur="checkTextField(this);" placeholder="In Years" maxlength="2" name="_txtval" type="text" ></input>
                                            &nbsp; Key Size:
                                            <select span="1" style="width:70px"  name="_keysize" id="_keysize" >
                                                <option value="1">1024 </option>
                                                <option value="2">2048</option>
                                                <option value="2">4096</option>
                                            </select>
                                            <!--                                       <input type="button" class="btn-primary success" onclick="generateCSR()" value="Generate CSR">-->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-allign:right">

                                        </td>
                                        <td style="text-allign:left">

                                        </td>  <td>&nbsp;</td>
                                        <td style="text-allign:right">
                                            &nbsp;
                                        </td>
                                        <td style="text-allign:left">
                                            &nbsp;  &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-allign:right">
                                            <!--                                  CSR-->
                                        </td>
                                        <td style="text-allign:left">
                                            <!--                                    <textarea rows="3" cols="150">   
                                            
                                                                                </textarea>-->
                                        </td>  <td>&nbsp;</td>
                                        <td style="text-allign:right">
                                            &nbsp;
                                        </td>
                                        <td>  &nbsp;  &nbsp;</td>


                                    </tr>

                                    <tr>
                                        <td style="text-allign:right">

                                        </td>
                                        <td style="text-allign:left">

                                        </td>  <td>&nbsp;</td>
                                        <td style="text-allign:right">
                                            &nbsp;
                                        </td>
                                        <td style="text-allign:left">
                                            &nbsp;  &nbsp;
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="text-allign:right">

                                        </td>
                                        <td style="text-allign:left">
                                            <input type="button" id="btnCertGen" onclick="generateCertificate()" class="btn btn-primary" value="Generate Certificate">
                                            &nbsp;&nbsp;
                                            <input type="button" id="btnSearchCert" onclick="gotoCertificateList()" class="btn btn-primary" value="Search Certificates">
                                        </td>   <td>&nbsp;</td>

                                        <td style="text-allign:right">
                                            &nbsp;
                                        </td>
                                        <td style="text-allign:left">
                                            &nbsp;  &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </div>


                        </fieldset>
                    </form>
                </div>


            </div>
        </div>
        <div class="tab-pane" id="secondary">  
            <div class="row-fluid">
                <form class="form-horizontal" id ="certificateform" >
                    <div id="divThirdParty">

                        <div class="control-group">
                            <input type="hidden" id="_certType" name="_certType" value="2">
                            
                              &emsp;  Product Type:  
                                <select span="1" style="width:120px"  name="_product_type" id="_product_type" >
                                    <option value="1">Domain SSL </option>
                                    <option value="2">Extended SSL</option>
                                </select>
                               &emsp; Common Name:  
                                <input id="common_nameTP"  onblur="checkTextField(this);" placeholder="ip,domain name" style="width:170px" name="common_nameTP" type="text" ></input>

                              &emsp;  Oraganization:
                                <input id="_txtOrganizationTP"   onblur="checkTextField(this);" style="width:170px" placeholder="ex.Telekom Maysia" name="_txtOrganizationTP" type="text" ></input>


                              &emsp;  OrganizationUnit:

                                <input id="_txtOrganizationUnitTP"   onblur="checkTextField(this);" style="width:170px" placeholder="ex.business management" name="_txtOrganizationUnitTP" type="text" ></input>
                        </div>
                        <br/>
                        <div class="control-group">
                              &emsp;  LocalityName:
                                
           <input id="_txtLocalityNameTP"  onblur="checkTextField(this);" style="width:170px" placeholder="ex.cyberjaya" name="_txtLocalityNameTP" type="text" ></input>
                             &emsp;   Select Country:
                                <select class="span4" name="_CountryNameTP" style="width:100px" id="_CountryName">
                                    <%
                                        //getAll conotries here 
                                        LocationManagement lManagement = new LocationManagement();
                                        int countryCode = -1;
                                        Countrylist[] countryList = lManagement.getAllCountries();
                                        if (countryList != null) {
                                            for (int j = 0; j < countryList.length; j++) {
                                                String strcode = countryList[j].getCountyName();
                                                countryCode = countryList[j].getCountryid();
                                    %>
                                    <option value="<%=strcode%>"><%=strcode%></option>

                                    <%}
                                        }%>
                                </select>
                              &emsp; Enter State: 
                             <input id="_txttpState"  onblur="checkTextField(this);" style="width:170px" placeholder="ex.Kaula Lampur" name="_txttpStateTP" type="text" ></input>   
                             &emsp;   Enter City:
                             <input id="_txttpCity"  onblur="checkTextField(this);" style="width:170px" placeholder="ex.Kaula Lampur" name="_txttpCityTP" type="text" ></input>
 
                        
                        </div>   <br/> 
                                <div class="control-group">
                                   &emsp; Validity Period(Months)

                      &emsp;  <select span="1"  style="width:50px;" name="_txtvalidityperiod" id="_txtvalidityperiod" >
                            <%for (int i = 1; i <= 24; i++) {%>
                            <option><%=i%></option>
                            <%}%>
                        </select>
                                &emsp;  &emsp;   Key Length:<select span="1" style="width:70px"  name="_keysizeTP" id="_keysizeTP" >
                                                <option value="1024">1024 </option>
                                                <option value="2048">2048</option>
                                                <option value="4096">4096</option>
                                            </select>
                            &emsp;  &emsp;  Email
                    <input id="_txtEmailTP" style="width:170px"   onblur="checkTextField(this);" placeholder="ex.pramod@mollatech.com" name="_txtEmailTP" type="text" ></input>
                                </div>
                       

                      <div class='control-group'>
                       
                &emsp;   &emsp;       <input type="button" id="btngenCSR" onclick="generateCSR()" class="btn-primary success" value="GenerateCSR"> 
                         &emsp; &emsp;   CSR(For Reference)
                    <textarea cols="50" rows="4" id="txtcsrtp" name="txtcsrtp">
                                        
                    </textarea>
                      </div>
                              <br/>
                  &emsp;  &emsp;    &emsp;  &emsp;  <h5>Organization Address:</h5>
                      <div class="control-group">
                   &emsp;    Address Line 1:
                  &emsp;    <input id="_txttporgaddrline1" style="width:170px"   onblur="checkTextField(this);"  name="_txttporgaddrline1TP" type="text" ></input>
              &emsp;           Address Line 2:
                  &emsp;    <input id="_txttporgaddrline2" style="width:170px"   onblur="checkTextField(this);"  name="_txttporgaddrline2TP" type="text" ></input>
                &emsp;        Address Line 3:
                    <input id="_txttporgaddrline3" style="width:170px"   onblur="checkTextField(this);"  name="_txttporgaddrline3TP" type="text" ></input>
               &emsp;         City:
                   &emsp;   <input id="_txttpcity" style="width:170px"   onblur="checkTextField(this);"  name="_txttpcityTP" type="text" ></input>
                                            
                      </div>
                      
                      <div class='control-group'>
                  &emsp; &emsp; &emsp;          Region:
                    <input id="_txttpRegion:" style="width:170px"   onblur="checkTextField(this);"  name="_txttpRegionTP:" type="text" ></input>
                 &emsp; &emsp; &emsp;     Postal Code:
                    <input id="_txttppostCode:" style="width:170px"   onblur="checkTextField(this);"  name="_txttppostCode:" type="text" ></input>
                &emsp;  &emsp; &emsp;     Select Country:
                                <select class="span4" name="_CountryNameTP" style="width:100px" id="_CountryName">
                                    <%
                                        //getAll conotries here 
                                      
                                        if (countryList != null) {
                                            for (int j = 0; j < countryList.length; j++) {
                                                String strcode = countryList[j].getCountyName();
                                                countryCode = countryList[j].getCountryid();
                                    %>
                                    <option value="<%=countryCode%>"><%=strcode%></option>

                                    <%}
                                        }%>
                                </select>
                            &emsp;  Phone No:   
                             <input id="_txttpPhono:" style="width:170px"   onblur="checkTextField(this);"  name="_txttp_txtPhono" type="text" ></input>
                     
                          
                      </div>
                                <br/>
                                <h5>Fill Organization Details Below</h5>
                                <div class='control-group'>
                                    &emsp; &emsp; BusinessAssumedName:
                            &emsp; &emsp; <input type="text" id="txtbusinessaname" name="txtbusinessanameTP">  
                             &emsp; &emsp;CreditAgency:
                             <select span="1"  name="_caType" style="width:70px" id="_caType" >
                                <option value="1">DUM</option>
                                <option value="2">POOP</option>

                            </select>
                            &emsp; Org Code:<input type="text" id="txtorgCode" name="txtorgCodeTP" style="width:50px">


                            &emsp;  OrganizationNameNative:
                           &emsp;  <input type="text" name="_txtorgnativenameTP" id="_txtorgnativename">
                         
 
                                </div>
                                <br/>
                                <div class="control-group">
                                      &emsp;  BusinessCategoryCode
                          &emsp;  <input type="text" name="_businesscategorycodeTP" id="_businesscategorycode" style="width:50px"> 
                            OrganizationAddress:
                           &emsp; &emsp;  <textarea id="txtorgAddr" name="txtorgAddrTP" cols="50" rows="3"> </textarea> 
                                    
                                </div>
                                    <br/>
                      &emsp; &emsp;  <h5> Requestor Inormation</h5> 
                                    <div class="control-group">
                                 &emsp; &emsp;        FirstName;
                       &emsp; &emsp; <input type="text" id="txtfirstnameTP" name="txtfirstnameTP">

                          &emsp; &emsp;Last Name: 

                          &emsp; &emsp;<input type="text" id="txtlastnameTP" name="txtlastnameTP">


                            OrganizationUnit:
                            &emsp; &emsp;  <input type="text" id="txtreqorunitTP" name="txtreqorunitTP">
                                        
                                    </div>
                      <div class="control-group">
                          
                           &emsp; &emsp; OrganizationName:  

                           &emsp;  <input type="text" id="txtreqorgnameTP" name="txtreqorgnameTP">


                             &emsp; &emsp;Phone :
                            &emsp; &emsp; <input type="text" id="txtreqphoneTP" name="txtreqphoneTP">

                      </div>
                      <br/>
                     &emsp; &emsp;   <h5>ApproverInfo</h5>
                      <div class="control-group">
                           &emsp; &emsp;   First Name:
                           &emsp; &emsp; <input type="text" id="txtreqphoneTP" name="txtauthFirstNameTP">
                            &emsp; &emsp;   LastName Name:
                           &emsp; &emsp; <input type="text" id="txtreqphoneTP" name="txtautLasstNameTP">
                            &emsp; &emsp;   Function
                            &emsp; &emsp; <input type="text" style="width:70px" id="txtreqphoneTP" name="txtautfunctiinTP">
                           
                      </div>
                     <br/>
                     <div class="control-group">
                          &emsp;  Organization:
                           &emsp; &emsp; <input type="text" id="txtreqphoneTP" name="txtautorgNameTP">
                           &emsp; &emsp;OrganizationUnit   
                           &emsp; &emsp; <input type="text" id="txtreqphoneTP" name="txtauthorgunitTP">
                            &emsp; &emsp; Phon no:
                           &emsp; &emsp; <input type="text" id="txtreqphoneTP" name="txtauthphonnoTP">
                         
                        
                      </div>
                     <br/>
                     <div>
                         
                             &emsp; &emsp; &emsp; &emsp;   Email:
                           &emsp; &emsp; <input type="text" id="txtreqphoneTP" name="txtautEmail;TP">
                     </div>
                      
                      
                        <br/>
                        <h5> Contact Information</h5>
                        <div class="control-group">
                           &emsp; &emsp;  Fist Name:
                           &emsp; &emsp;  <input id="_txtContactFirstNameTP" style="width:170px"   onblur="checkTextField(this);" placeholder="First Name" name="_txtContactFirstNameTP" type="text" ></input>    
                  &emsp; &emsp;  Last Name:
                           &emsp; &emsp;  <input id="_txtContactFirstLastNameTP" style="width:170px"   onblur="checkTextField(this);" placeholder="Last Name" name="_txtContactFirstLastNameTP" type="text" ></input>     
                           &emsp; &emsp;  Phone No:       
                           &emsp; &emsp;  <input id="_txtContactfirstphontp" style="width:170px"   onblur="checkTextField(this);" placeholder="Phone no" name="_txtContactfirstphontp" type="text" ></input>     
                        </div>
                        <div class="control-group">
                             &emsp; &emsp;  Email Id:
                            &emsp; &emsp; &emsp;  <input id="_txtContatcEmailTp" style="width:170px"   onblur="checkTextField(this);" placeholder="Email ID" name="_txtContatcEmailTp" type="text" ></input>     
                        </div>
                      <br/>
                      <div class="control-group">
                 <input type="button" id="btnorderCert" onclick="generateTPCert()" class="btn-primary success" value="Order Certificate"> 
                      </div>






                    

                   

                  


                  
                          


                        




                          

                           




                            </div>
                            </form>
                            </div>
                            </div></div>
                            </div>
                            <%@include file="footer.jsp" %>