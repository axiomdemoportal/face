
<%@page import="com.mollatech.axiom.nucleus.settings.SendNotification"%>
<%@page import="com.mollatech.axiom.nucleus.settings.OOBMobileChannelSettings"%>
<%@page import="com.twilio.sdk.client.TwilioCapability"%>
<%@page import="com.mollatech.axiom.common.utils.UtilityFunctions"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.settings.ContactTagsSetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Templates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%--<%@page import="com.mollatech.axiom.nucleus.db.Tags"%>
<%@page import="com.mollatech.dictum.management.TagsManagement"%>--%>
<%@include file="header.jsp" %>
<%    Channels _apSChannelDetails = (Channels) session.getAttribute("_apSChannelDetails");
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");

    SettingsManagement sManagement = new SettingsManagement();
    OOBMobileChannelSettings settings = null;
    String accountSid = null;
    String authToken = null;
    String appSid = null;
    Object[] obj = sManagement.getSettings(sessionId, _apSChannelDetails.getChannelid());
    if (obj != null) {
        mainLable:
        for (int i = 0; i < obj.length; i++) {

            if (obj[i] instanceof OOBMobileChannelSettings) {

                settings = (OOBMobileChannelSettings) obj[i];
                if (settings.getStatus() == 1 && settings.getType() == 3) {
                    accountSid = settings.getUserid();
                    authToken = settings.getPassword();
                    appSid = (String) settings.getReserve2();
                    break mainLable;
                } else if (settings.getAutofailover() == 1) {
                    continue;
                }
            }
        }
    }

    TwilioCapability capability = new TwilioCapability(accountSid, authToken);
    capability.allowClientOutgoing(appSid);

%>
<script src="./assets/js/dictum/bulkmsg.js"></script>
<!-- @start snippet -->
<script type="text/javascript" src="//static.twilio.com/libs/twiliojs/1.1/twilio.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<script type="text/javascript">
    var connection = null;
    $(document).ready(function() {
        Twilio.Device.setup("<%=capability.generateToken()%>", {"debug": true});
        $("#call").click(function() {
            // alert("in call");
            Twilio.Device.connect();
        });
        $("#hangup").click(function() {
            // alert("in hangup");
            connection.sendDigits("#");
        });

        Twilio.Device.ready(function(device) {
            $('#status').text('Ready to start recording');
        });

        Twilio.Device.offline(function(device) {
            $('#status').text('Offline');
        });

        Twilio.Device.error(function(error) {
            $('#status').text(error);
        });

        Twilio.Device.connect(function(conn) {
            connection = conn;
            $('#status').text("On Air");
            $('#status').css('color', 'red');
            toggleCallStatus();
        });

//        Twilio.Device.disconnect(function(conn) {
//            $('#status').html('Recording ended<br/><a href="showrecordings.jsp">view recording list</a>');
//            $('#status').css('color', 'black');
//            toggleCallStatus();
//        });

        function toggleCallStatus() {
            $('#call').toggle();
            $('#hangup').toggle();
        }
    });
</script>
<!-- @end snippet -->
<div class="container-fluid">
    <h2>Record and Play Voice Message </h2>
    <div class="row-fluid">
        <form class="form-horizontal" id="bulkmsg" name="bulkmsg">
            <fieldset>
                <div class="control-group">
                    <label class="control-label"  for="messagebody">Record Message</label>
                    <div class="controls">

                        <button class="btn btn-primary btn-large" onclick="" type="button" id="call" value="Record Now >>">Record Now</button>                                               
                        <button class="btn btn-primary btn-large" onclick="" type="button" id="hangup" value="Stop Recording" style="display:none;">Stop Recording</button>                                               

                    </div>
                    <div id="status">
                        Offline
                    </div>
                </div>
                <%
                    String url = (String) session.getAttribute("_url");
                    if(url == null){
                        url = (String)settings.getReserve1();
                    }
                %>
                <div class="control-group">
                    <label class="control-label"  for="messagebody">Listen Message</label>
                    <div class="controls">
                        <button class="btn btn-success btn-large" onclick="Play(<%=url%>);" type="button">Listen Now >></button>                                                
                    </div>
                </div>

                <!-- Submit -->
                <div class="control-group">
                    <div class="controls">
                        <button class="btn btn-primary btn-large" onclick="gotobroadcast(<%=url%>)" type="button">Broadcast now >></button>
                        <div id="bulk-sms-gateway-result"></div>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>

                        <script src="./assets/js/dictum/voicebroadcast/voicerecord.js"></script>
    <script language="javascript" type="text/javascript">
                            //ChangeSpeed(1);
                            //  ChangeFormat(1);
    </script>

    <%@include file="footer.jsp" %>