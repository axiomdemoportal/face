<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.Pushmessages"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.PushMessageManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.operation.TemplateVariables"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/pushmessage.js"></script>
<%
    String _sessionID = (String) session.getAttribute("_apSessionID");

    SessionManagement smObj = new SessionManagement();
    int iStatus = smObj.GetSessionStatus(_sessionID);
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");

    Pushmessages[] PushmessagesObj = null;
    if (iStatus == 1) { //active
        smObj.UpdateSession(_sessionID);
        PushMessageManagement cmObj = new PushMessageManagement();
        PushmessagesObj = cmObj.ListPushmessages(_sessionID, channel.getChannelid());
        cmObj = null;
    }
%>
<div class="container-fluid">
     <h1 class="text-success">Caller System Management</h1>    
    <p>List of caller systems like FX (ISO8583/SWIFT/SOX) with their messages format, templates, requesting server details, response format etc can be stated here.</p>
    <p>
        <a href="#addPushMessage" class="btn btn-primary" data-toggle="modal">Add Caller&raquo;</a>        
    </p>
    
    
    <div class="row-fluid">
        
            <table class="table table-striped">
                <tr>
                    <td>No.</td>
                    <td>Name</td>
                    <td>Host</td>                    
                    <td>Status</td>
                    <td>Manage</td>                    
                    <td>Messages</td>                    
                    <td>Success Code</td>
                    <td>Failure Code</td>
                    <td>Created On</td>
                    <td>Modified On</td>
                </tr>
               <%
                   out.flush();
                    PushMessageManagement tmObj = new PushMessageManagement();
                    if(PushmessagesObj!=null)
                    for (int i = 0; i < PushmessagesObj.length; i++) {
                        Pushmessages axcObj = PushmessagesObj[i];
                        SimpleDateFormat sdf = new SimpleDateFormat("d MMM,yyyy HH:mm ");
                        String Status = null;
                        String EnforceRemote = null;
                        if (axcObj.getStatus() == 1) {
                            Status = "Active";
                        } else if (axcObj.getStatus() == 0) {
                            Status = "Suspended";
                        }
                       byte b =  axcObj.getEnforceRemoteAccess();
                        if (b!=0 == true) {
                            EnforceRemote = "Yes";
                        } else if (b!=0 == false) {
                            EnforceRemote = "No";
                        }
                %>
                <tr>
                    <td><%=(i + 1)%></td>
                    <td><%=axcObj.getCallerName()%></td>
                    <td><%=axcObj.getCallerIp()%></td>
                    <!--<td><%=axcObj.getCallerPort()%></td>-->
                    <td><%=Status%></td>
                    <td>
                        <div class="btn-group">
                            <button class="btn btn-mini">Manage</button>
                            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#editPushMessage" onclick="loadEditPushmessageDetails('<%=axcObj.getCallerid()%>')" data-toggle="modal">Edit Pushmessage</a></li>
                                <li><a href="#" onclick="removePushmessage(<%=axcObj.getCallerid()%>)"><font color="red">Remove Pushmessage?</font></a></li>
                            </ul>
                        </div>
                    </td>
                    
                    <td>
                        <a class="btn btn-mini" href="./FXpushmappers.jsp?_cid=<%=axcObj.getCallerid()%>" data-toggle="modal" id="buttonISOdetails">View List</a></button>
                     </td>
<!--                    <td align=center>
                        
                    </td>-->
                    <td><%=axcObj.getResponsesuccess()%></td> 
                    <td><%=axcObj.getResponcefailure()%></td>
                    <td><%=axcObj.getCreatedOn()%></td> 
                    <td><%=axcObj.getModifiedOn()%></td>
                </tr>
                <%
                    }
                    %>
            </table>
        
    </div>
    
    
    
    
    
    
    
    
    
    
    
    
    
</div>
            
            
<!-- Modal -->
<div id="addPushMessage" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Add New Caller</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="addPushmessageForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Name</label>
                        <div class="controls">
                            <input type="text" id="_callerName" name="_callerName" placeholder="" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">IP/Host </label>
                        <div class="controls">
                            <input type="text" id="_push_ip" name="_push_ip" placeholder="Caller Ip Address" class="input-xlarge">
                        </div>
                    </div>
<!--                    <div class="control-group">
                        <label class="control-label"  for="username">Port</label>
                        <div class="controls">
                            <input type="text" id="_push_port" name="_push_port" placeholder="Caller Ip Address" class="span2">
                        </div>
                    </div>-->
                    <div class="control-group">
                        <label class="control-label"  for="username">Status</label>
                        <div class="controls">
                            <select class="span4" name="_push_status" id="_push_status">
                                <option value="1">Active</option>
                                <option value="0">Suspended</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <div class="control-group">
                        <label class="control-label"  for="username">Success Code</label>
                        <div class="controls">
                            <input type="text" id="_push_success" name="_push_success" placeholder="Thank you Message Sent Successfully" class="input-xlarge">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label"  for="username">Failure Code</label>
                        <div class="controls">
                            <input type="text" id="_push_failure" name="_push_failure" placeholder="Sorry Message Not Sent " class="input-xlarge">
                        </div>
                    </div>
                    <!--                    <div class="control-group">
                                            <label class="control-label"  for="username">EnforceSSL</label>
                                            <div class="controls">
                                                <select class="span4" name="_push_enforce" id="_push_enforce">
                                                    <option value="1">Active</option>
                                                    <option value="0">Suspended</option>
                                                </select>
                                            </div>
                                        </div>-->
<!--                    <div class="control-group">
                        <label class="control-label"  for="username">Enforce Authenticaiton?</label>
                        <div class="controls">
                            <select class="span4" name="_push_enforceremoteaccess" id="_push_enforceremoteaccess">
                                <option value="1">Yes, Enforce Remote Access</option>
                                <option value="0">No, Do not enforce Remote Access</option>
                            </select>
                        </div>
                    </div>        -->
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="addpushmessage-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="addpushmessage()" id="buttonAddPushmessage"  >Create FX Caller >> </button>
    </div>
</div>
<div id="editPushMessage" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idEditTemplatepushName"></div></h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="editPushmessageForm">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_epush_idE" name="_epush_idE"  >
                    <div class="control-group">
                        <label class="control-label"  for="username">Name</label>
                        <div class="controls">
                            <input type="text" id="_epush_name" name="_epush_name" placeholder="Bank ATM" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">IP/Host</label>
                        <div class="controls">
                            <input type="text" id="_epush_ip" name="_epush_ip" placeholder="127.1.1.12" class="input-xlarge">
                        </div>
                    </div>                    
                    <div class="control-group">
                        <label class="control-label"  for="username">Status</label>
                        <div class="controls">
                            <select class="span4" name="_epush_status" id="_epush_status">
                                <option value="1">Active</option>
                                <option value="0">Suspended</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <div class="control-group">
                        <label class="control-label"  for="username">Success Code</label>
                        <div class="controls">
                            <input type="text" id="_epush_success" name="_epush_success" placeholder="Thank you Message Sent Successfully" class="input-xlarge">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label"  for="username">Failure Code</label>
                        <div class="controls">
                            <input type="text" id="_epush_failure" name="_epush_failure" placeholder="Sorry Message Not Sent " class="input-xlarge">
                        </div>
                    </div>
<!--                    <div class="control-group">
                        <label class="control-label"  for="username">EnforceSSL</label>
                        <div class="controls">
                            <select class="span4" name="_push_enforce" id="_push_enforce">
                                <option value="1">Active</option>
                                <option value="0">Suspended</option>
                            </select>
                        </div>
                    </div>-->
<!--                    <div class="control-group">
                        <label class="control-label"  for="username">Enforce Authentication?</label>
                        <div class="controls">
                            <select class="span4" name="_epush_enforceremoteaccess" id="_epush_enforceremoteaccess">
                                <option value="1">Active</option>
                                <option value="0">Suspended</option>
                            </select>
                        </div>
                    </div>        -->
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="editpushmessage-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="editpushmessage()" id="buttonEditPushmessage" type="button">Save FX Caller</button>
    </div>
</div>

<%@include file="footer.jsp" %>