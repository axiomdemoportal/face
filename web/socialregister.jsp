
<%@page import="com.mollatech.axiom.nucleus.settings.OOBSocialChannelSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.ChannelsUtils"%>
<%@page import="org.hibernate.Session"%>
<%@page language="java" import="java.util.*"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%--<%@page import="com.mollatech.dictum.social.LinkedinAccessTokenRequestServlet"%>--%>
<%@page import="com.mollatech.dictum.social.LinkedinOauthRequestServlet"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.RemoteAccessUtils"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.AxiomProtect"%>
<%@page import="java.io.File"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.LoadSettings"%>
<html>

    <head>
        <meta name="google-translate-customization" content="26e4d928eae0659b-61f8bcc48ec8af9a-g74ee54aac640948a-11"></meta>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="./assets/css/bootstrap.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 60px;
                padding-bottom: 40px;
            }
        </style>
        <link href="./assets/css/bootstrap-responsive.css" rel="stylesheet">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="./assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="./assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="./assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="./assets/ico/apple-touch-icon-57-precomposed.png">
        <link rel="stylesheet" type="text/css" href="./assets/css/zocial.css" />
        <link href='http://fonts.googleapis.com/css?family=Pompiere' rel='stylesheet' type='text/css'>
        <link rel="shortcut icon" href="./assets/ico/favicon.png">
        <script src="./assets/js/jquery.js"></script>



        <script src="./assets/js/bootstrap-transition.js"></script>
        <script src="./assets/js/bootstrap-alert.js"></script>
        <script src="./assets/js/bootstrap-modal.js"></script>
        <script src="./assets/js/bootstrap-dropdown.js"></script>
        <script src="./assets/js/bootstrap-scrollspy.js"></script>
        <script src="./assets/js/bootstrap-tab.js"></script>
        <script src="./assets/js/bootstrap-tooltip.js"></script>
        <script src="./assets/js/bootstrap-popover.js"></script>
        <script src="./assets/js/bootstrap-button.js"></script>
        <script src="./assets/js/bootstrap-collapse.js"></script>
        <script src="./assets/js/bootstrap-carousel.js"></script>
        <script src="./assets/js/bootstrap-typeahead.js"></script>

        <script src="./assets/js/json_sans_eval.js"></script>
        <script src="./assets/js/bootbox.min.js"></script>
        <script src="./assets/js/channels.js"></script>
        <script src="./assets/js/operators.js"></script>
        <script src="http://code.jquery.com/jquery-2.0.3.js" type="text/javascript"></script>
        <script src="https://connect.facebook.net/en_US/all.js" type="text/javascript"></script>

        <%

            int port = request.getServerPort();
            String scheme = request.getScheme();
            boolean bSecure = request.isSecure();
            String server = request.getServerName();
            String channelname = request.getContextPath();
            String _channelName = this.getServletContext().getContextPath();
            _channelName = _channelName.replaceAll("/", "");
            SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
            Session sChannel = suChannel.openSession();
            ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
            Channels channel = cUtil.getChannel(_channelName);
            SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
            Session sRemoteAcess = suRemoteAcess.openSession();
            RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
            String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
            request.getSession().setAttribute("_apSessionRemoteAccessLoginID", credentialInfo[0]);
            SessionManagement sManagement = new SessionManagement();
            String sessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());
            SettingsManagement sMngmt = new SettingsManagement();
            OOBSocialChannelSettings socialObj = (OOBSocialChannelSettings) sMngmt.getSetting(sessionId, channel.getChannelid(), 17, 1);
            // String facebook_register=scheme.concat("://").
            String facebook_register = scheme.concat("://").concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("/").concat("facebookRegister?callback=?");
            //      String LINKEDINOAUTHREQUESTMAKER = scheme.concat(":").concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("/").concat("linkedinOauthRequest?callback=?");
            String LINKEDINOAUTHREQUESTMAKER = scheme.concat(":").concat(channelname).concat("/").concat("linkedinOauthRequest?callback=?");
//           String LINKEDINOAUTHREQUESTMAKER = "http://localhost:8080/face/linkedinOauthRequest?callback=?";
//            String TWITTEROAUTHREQUESTMAKER = scheme.concat(":").concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("twitterOauthRequest?callback=?");
            String TWITTEROAUTHREQUESTMAKER = "https://axiomprotect.fwd.wf/face/twitterOauthRequest?callback=?";

        %>



        <script>

            //  var FACEBOOK_REGISTER =facebook_register;

//            var LINKEDIN_OAUTH_REQUEST_MAKER = LINKEDINOAUTHREQUESTMAKER;
//
//
//            var TWITTER_OAUTH_REQUEST_MAKER = TWITTER_OAUTH_REQUEST_MAKER;

            var TWITTER_ACCESS_TOKEN_REQUEST = "https://api.twitter.com/oauth/authorize";




//            var FACEBOOK_REGISTER = "http://localhost:8080/face/facebookRegister?callback=?";
//            var FACEBOOK_POST = "http://localhost:8080/face/facebookPost?callback=?";
//            var LINKEDIN_OAUTH_REQUEST_MAKER = "http://localhost:8080/face/linkedinOauthRequest?callback=?";
//            var LINKEDIN_POST = "http://localhost:8080/face/linkedinPost?callback=?";
//
//            var TWITTER_OAUTH_REQUEST_MAKER = "http://www.ideasventure.com:8080/TwitterNotification/twitterOauthRequest?callback=?";
//            var TWITTER_POST = "http://www.ideasventure.com:8080/TwitterNotification/twitterPost?callback=?";
//            var TWITTER_ACCESS_TOKEN_REQUEST = "https://api.twitter.com/oauth/authorize";





            var RESPONSE_NO_RESULT = "NO_RESULT";
            var RESPONSE_OK = "OK";

            function addLinkedin(LINKEDINOAUTHREQUESTMAKER) {
                var LINKEDIN_OAUTH_REQUEST_MAKER = LINKEDINOAUTHREQUESTMAKER;
                var clientId = document.getElementById("clientId").value;
                if (isValidInteger(clientId)) {
                    $.getJSON(LINKEDIN_OAUTH_REQUEST_MAKER, {
                        clientId: clientId,
                        ts: new Date().toString()
                    }, function(data) {
//                        log("addLinkedin 03 LINKEDIN_OAUTH_REQUEST_MAKER [has oauth_url:"
//                                + data.hasOwnProperty('oauth_url') + "]");
//                        Alert4Social("<span><font color=red>" + "You are successfully registered with dictum using your Linkedin Account" + "</font></span>");
            
                        if (data.hasOwnProperty('oauth_url')) {
//                            log("addLinkedin 05 " + data.oauth_url);
//                            Alert4Social("<span><font color=red>" + "Registration Failed with dictum using your Facebook account" + "</font></span>");
                            window.location = data.oauth_url;
                        }
                    });
                }
            }


            function addFacebook(facebook_register, appid) {
                var clientId = document.getElementById("clientId").value;
                if (isValidInteger(clientId)) {
                    FB.init({
                        appId: appid,
                        cookie: true,
                        xfbml: true,
                        status: true
                    });

                    FB.login(function(response) {
                        facebookLoginCallback(facebook_register, response, clientId)
                    }, {scope: 'email,read_stream,publish_actions,publish_stream'});

                }
            }

            function addTwitter(TWITTEROAUTHREQUESTMAKER) {
                var TWITTER_OAUTH_REQUEST_MAKER = TWITTEROAUTHREQUESTMAKER;
                var clientId = document.getElementById("clientId").value;
                if (isValidInteger(clientId)) {
                    $.getJSON(TWITTER_OAUTH_REQUEST_MAKER, {
                        clientId: clientId,
                        ts: new Date().toString()
                    }, function(data) {
                        log("addTwitter 03 TWITTER_OAUTH_REQUEST_MAKER [has oauth_token:"
                                + data.hasOwnProperty('oauth_token') + "]");
                        if (data.hasOwnProperty('oauth_token')) {
                            log("addTwitter 04 TWITTER_OAUTH_REQUEST_MAKER [oauth_token:"
                                    + data.oauth_token + "]");
                            var href = TWITTER_ACCESS_TOKEN_REQUEST + "?oauth_token="
                                    + encodeURI(data.oauth_token);
                            log("addTwitter 05 " + href);
                            window.location = href;
                        }
                    });
                }
            }





            function facebookLoginCallback(facebookregister, response, clientId) {
                var FACEBOOK_REGISTER = facebookregister;
//                log("facebookLoginCallback(), USELESS [response.status:" + response.status + "] [response.authResponse:" + response.authResponse + "]");
                if (response.status === 'connected') {
                    var uid = response.authResponse.userID;
                    var accessToken = response.authResponse.accessToken;
                    log("facebookLoginCallback() [uid:" + uid + "], [accessToken:" + accessToken + "]");
                    FB.api('/me', function(response) {
                        log("facebookLoginCallback() [me:" + response.name + "]");
                        log("facebookLoginCallback() [email:" + response.email + "]");
                        log(facebookregister);
                        $.getJSON(FACEBOOK_REGISTER, {
                            clientId: clientId,
                            uid: uid,
                            accessToken: accessToken,
                            name: response.name,
                            email: response.email,
//                            phone:response.phone,
                            ts: new Date().toString()
                        }, function(data) {
//                            log("facebookLoginCallback() 01");
                            if (RESPONSE_OK.localeCompare(data.result) == 0) {
//                                log("facebook account is registered. Reload the page");
                                //location.reload();
                                Alert4Social("<span><font color=blue>" + "You are successfully registered with dictum using your Facebook account" + "</font></span>");
                                window.setTimeout(RefreshSocialpage, 2000);
                            } else {
//                                log("facebook account registration is failed.");
                                Alert4Social("<span><font color=red>" + "Registration Failed with dictum using your Facebook account" + "</font></span>");
                            }
                        });
                    });
                } else if (response.status === 'not_authorized') {
                } else {
                }
            }
            function facebookAuthLoginCallback(response) {
//                Alert4Social("<span><font color=red>" + "You are successfully registered with dictum using your Facebook account" + "</font></span>");
//                log("facebookAuthLoginCallback(), [response.status:" + response.status + "]");
            }





            function postMessageSuccess() {
                log("Notification is sent");
            }
            function postMessageFail() {
                log("Notification is failed");
            }
            // **************** utils **************
            function log(logText) {
                var content = document.getElementById("log").innerHTML;
                document.getElementById("log").innerHTML = content + "<br/>" + logText;
            }
            function logClean() {
                document.getElementById("log").innerHTML = "";
            }
            function isValidInteger(value) {
                if (value.length == 0) {
                    return false;
                }
                var intValue = parseInt(value);
                if (intValue == Number.NaN) {
                    return false;
                }
                return true;
            }
            function toHHMMSS(sec1970) {
                sec1970 = sec1970 / 1000;
                // var sec_num = parseInt(this, 10); // don't forget the second parm
                var daySec = sec1970 % (24 * 3600);
                var hours = Math.floor(daySec / 3600);
                var minutes = Math.floor((daySec - (hours * 3600)) / 60);
                var seconds = Math.floor(daySec - (hours * 3600) - (minutes * 60));

                if (hours < 10) {
                    hours = "0" + hours;
                }
                if (minutes < 10) {
                    minutes = "0" + minutes;
                }
                if (seconds < 10) {
                    seconds = "0" + seconds;
                }
                var time = hours + ':' + minutes + ':' + seconds;
                return time;
            }


            function Alert4Social(msg) {
                bootbox.alert("<h2>" + msg + "</h2>", function(result) {
                    if (result == false) {
                    } else {
                        //end here
                    }
                });
            }

            function RefreshSocialpage() {
                window.location.href = "./socialregister.jsp";
            }

        </script>
    </head>
    <body>
        <div class="container">

            <div class="hero-unit" align="center">
                <h2>Social Register Page</h2>
                <br>
                <input type="hidden" id="clientId" name="clientId" value="1">

<!--                <button class="btn btn-large btn-primary" onclick="addFacebook('<%=facebook_register%>', '<%=socialObj.getFacebookappid()%>')" type="button" >Facebook Register</button>

                <button class="btn btn-large btn-primary" onclick="addLinkedin('<%=LINKEDINOAUTHREQUESTMAKER%>')" type="button" >LinkedIn Register</button>
                <br><hr>-->


                <a href="#" class="zocial facebook" onclick="addFacebook('<%=facebook_register%>', '<%=socialObj.getFacebookappid()%>')">Register with Facebook</a>
                <a href="#" class="zocial linkedin" onclick="addLinkedin('<%=LINKEDINOAUTHREQUESTMAKER%>')">Register with LinkedIn</a>



<!--                <button class="btn btn-large btn-primary" onclick="addTwitter('<%=TWITTEROAUTHREQUESTMAKER%>')" type="button" >Twitter  Register</button>
<br><hr>-->

            </div>
        </div> <!-- /container -->
        <br/>
        <div id="log" align="left" />
    </body>
</html>
