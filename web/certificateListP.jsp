<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserGroupsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Usergroups"%>
<%@include file="header.jsp" %>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<script src="./assets/js/resourcemanagment.js"></script>
<script src="./assets/js/otptokens.js"></script>
<script src="./assets/js/pkitokens.js"></script>
<script src="./assets/js/challengeresponse.js"></script>
<script src="./assets/js/trustedDevice.js"></script>
<script src="./assets/js/geotracking.js"></script>
<script src="./assets/js/usermanagement.js"></script>
<script src="./assets/js/securephrase.js"></script>
<script src="./assets/js/certGenerator.js"></script>
<script>

    function getDivforSSL() {

        if (document.getElementById("_caType").value === '1')
        {
            document.getElementById("divSelfSigned").style.display = "block";
            document.getElementById("divThirdParty").style.display = "none";
        } else {
            document.getElementById("divSelfSigned").style.display = "none";
            document.getElementById("divThirdParty").style.display = "block";

        }
    }

    function checkTextField(field) {
        if (field.value === '') {
            alert("Field can not be empty");
        }
    }
</script>
<style>
    .modal1 {
        position: fixed;
        top: 10%;
        left: 50%;
        z-index: 1050;
        width: 750px;
        margin-left: -280px;
        background-color: #ffffff;
        border: 1px solid #999;
        border: 1px solid rgba(0, 0, 0, 0.3);
        *border: 1px solid #999;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        border-radius: 6px;
        outline: none;
        -webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
        -moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
        box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
        -webkit-background-clip: padding-box;
        -moz-background-clip: padding-box;
        background-clip: padding-box;
    }
</style>
<div class="container-fluid" >
    <div id="auditTable">
        <h1 class="text-success">Certificate Management</h1>
        <p>List of certificates in the system generated from Application or SSL purpose with respect to specific domain/server/application</p>
        <h3>Search Resource</h3>   
        <div class="input-append">
            <form id="searchUserForm" name="searchUserForm">
                <input type="text" id="_keyword" name="_keyword" placeholder="Search using ip,domain name" class="span4">
                <select span="1"  name="_resourceName" id="_resourceName" >
                    <option value="1">Application Certificates </option>
                    <option value="2">SSL Certificates</option>
                </select>
                <a href="#" class="btn btn-success" onclick="searchResourceAppCerts(1)">Search Now</a>
            </form>
        </div>

        <%
            Channels _apSChannelDetails = (Channels) session.getAttribute("_apSChannelDetails");
            OperatorsManagement oManagement = new OperatorsManagement();
            String _sessionID = (String) session.getAttribute("_apSessionID");
            Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
            String selfSignedCAName = LoadSettings.g_sSettings.getProperty("selfsigned.ca.name");
            String thirdpartCa = LoadSettings.g_sSettings.getProperty("thirparty.ca.name");
        %>

        <div id="users_table_main">
        </div>

        <!--</div>-->
        </p>
        <br>
        <p><a href="#" class="btn btn-primary" onclick="CertGen()" id="user_hide" style="width: 110px" data-toggle="modal">New Certificate&raquo;</a></p>
        <p id="otp_hide">To Check Hardware Token Secret Validity <a href="#CheckSecret"  class="btn" style="width: 90px" data-toggle="modal">Click Here >> </a></p>
        <script>
            document.getElementById("otp_hide").style.display = 'none';
        </script>

    </div>


</div>


<div id="userauditDownload" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idauditDownload"></div></h3>
        <h3 id="myModalLabel">Download Audit</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="testSMSPrimaryForm" name="testSMSPrimaryForm">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_auditUserID" name="_auditUserID"/>
                    <input type="hidden" id="_auditUserName" name="_auditUserName"/>
                    <div class="control-group">
                        <label class="control-label"  for="username">Start Date</label>
                        <div class="controls" align="left" >
                            <!--<span class="add-on">From:</span>-->   
                            <div id="Pushdatetimepicker11" class="input-append date">
                                <input id="_auditStartDate" name="_auditStartDate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">End Date</label>
                        <div class="controls" align="left">
                            <!--<span class="add-on">Till:</span>-->   
                            <div id="Pushdatetimepicker21" class="input-append date">
                                <input id="_auditEndDate" name="_auditEndDate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>                    
                </fieldset>
            </form>
        </div>
    </div>

    <div class="modal-footer">
        <div id="editoperator-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="searchUserAudit()" id="buttonEditOperatorSubmit">Show Audit</button>
    </div>
</div>
<script>
    $(function () {
        $('#Pushdatetimepicker11').datepicker({
            format: 'dd/MM/yyyy',
            language: 'pt-BR'
        });
    });
    $(function () {
        $('#Pushdatetimepicker21').datepicker({
            format: 'dd/MM/yyyy',
            language: 'pt-BR'
        });
    });
</script>

<!--otp tokens-->









<script language="javascript">
    //listChannels();
</script>

<!--end otp tokens-->
<!--pki--> 

<div id="userPkiTokenauditDownload" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idauditDownload"></div></h3>
        <h3 id="myModalLabel">Download Audit</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="testSMSPrimaryForm" name="testSMSPrimaryForm">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_auditUserID" name="_auditUserID"/>
                    <input type="hidden" id="_auditUserName" name="_auditUserName"/>
                    <div class="control-group">
                        <label class="control-label"  for="username">Start Date</label>
                        <div class="controls" align="left" >

                            <!--<span class="add-on">From:</span>-->   
                            <div id="Pushdatetimepicker1" class="input-append date">
                                <input id="_auditStartDate1" name="_auditStartDate1" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">End Date</label>
                        <div class="controls" align="left">
                            <!--<span class="add-on">Till:</span>-->   
                            <div id="Pushdatetimepicker2" class="input-append date">
                                <input id="_auditEndDate1" name="_auditEndDate1" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>                    
                </fieldset>
            </form>
        </div>
    </div>

    <div class="modal-footer">
        <div id="editoperator-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="searchUsersPKITokenAuditv3()" id="buttonEditOperatorSubmit">Show Audit</button>
    </div>
</div>

<div id="certificateDetails" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Certificate Details</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="certform">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label"  for="username">Serial Number</label>
                        <div class="controls">
                            <input type="text" id="_srnoCertifiacte" name="_srnoCertifiacte" class="span4">   
                            & Algorithm: <input type="text" id="_algoname" name="_algoname" class="span4">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">User Details</label>
                        <div class="controls">
                            <textarea id="_subjectdn" name="_subjectdn" style="width:95%"></textarea>
                            <!--<input type="text" id="_subjectdn" name="_subjectdn"  class="input-xlarge">-->
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Issuer Details</label>
                        <div class="controls">
                            <textarea id="_issuerdn" name="_issuerdn" style="width:95%"></textarea>
                            <!--<input type="text" id="_issuerdn" name="_issuerdn"  class="input-xlarge">-->
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Issued On</label>
                        <div class="controls">
                            <input type="text" id="_notbefore" name="_notbefore"  class="input-xlarge">

                        </div>
                    </div>                    
                    <div class="control-group">
                        <label class="control-label"  for="username">Valid till</label>
                        <div class="controls">
                            <input type="text" id="_notafter" name="_notafter"  class="input-xlarge">
                        </div>
                    </div>                    


                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
</div>



<div id="RevokeCertificate" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idEditPushMaperName"></div></h3>
        <h3 id="myModalLabel">Reason To Revoke</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="RevokeCertificateform">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_userCR" name="_userCR" >

                    <div class="control-group">
                        <label class="control-label"  for="username">Reason</label>
                        <div class="controls">
                            <input type="text" id="_reason" name="_reason" placeholder="Any reason?" class="input-xlarge">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="HardwarePkiToken-result"></div>
        <button class="btn btn-primary" onclick="revokecertificate()" id="buttonHardwareRegistration" type="button">Submit</button>
    </div>
</div>


<div id="SendCertificate" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idSendCertificate"></div></h3>
        <h3 id="myModalLabel">Send Certificate To</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="SendCertificateform">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_userSENDCERT" name="_userSENDCERT" >

                    <div class="control-group">
                        <label class="control-label"  for="username">Recipient's Emailid</label>
                        <div class="controls">
                            <input type="text" id="_emailSENDCERT" name="_emailSENDCERT" placeholder="email id " class="input-xlarge">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="SendCertificate-result"></div>
        <button class="btn btn-primary" onclick="sendcertificatefile()" id="buttonSendCertificate" type="button">Submit</button>
    </div>
</div>


<div id="Sendpfxfile" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idSendpfxfile"></div></h3>
        <h3 id="myModalLabel">Send PFX To</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="Sendpfxfile">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_userSENDCERTPFX" name="_userSENDCERTPFX" >

                    <div class="control-group">
                        <label class="control-label"  for="username">Recipient's Emailid</label>
                        <div class="controls">
                            <input type="text" id="_emailSENDCERTPFX" name="_emailSENDCERTPFX" placeholder="email id " class="input-xlarge">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="SendCertificate-result"></div>
        <button class="btn btn-primary" onclick="sendSSLpfxFile()" id="buttonSendCertificate" type="button">Submit</button>
    </div>
</div>





<div id="certificaterGen" class="modal1 hide fade"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idauditDownload"></div></h3>
        <h3 id="myModalLabel">Generate CSR And Certificate(SSL and Application certs)</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="frmCertificateGeneration" name="frmCertificateGeneration">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_auditUserIDPKI" name="_auditUserIDPKI"/>
                    <input type="hidden" id="_auditUserNamePKI" name="_auditUserNamePKI"/>

                    Certificate Type:
                    <select span="1"  name="_certType" id="_certType" >
                        <option value="1">Application Certificates </option>
                        <option value="2">SSL Certificates</option>
                    </select>
                    &nbsp;&nbsp;&nbsp;
                    Issuer:
                    <select span="1" onblur="getDivforSSL();"  name="_caType" id="_caType" >
                        <option value="1" onclick="getDivforSSL();"><%=selfSignedCAName%> </option>
                        <option value="2"  onclick="getDivforSSL();"><%=thirdpartCa%></option>
                    </select>
                    <hr/>
                    Product Type:
                    <select span="1"  name="_product_type" id="_product_type" >
                        <option value="1">AlphaSSL:</option>
                        <option value="2">DomainSSL:</option>
                        <option value="3">OrganizationSSL:</option>
                        <option value="4">Extended SSL:</option>
                    </select>       
                    <h5>   Please fill Details below for csr generation:</h5>
                    <br/>
                    <div id="divSelfSigned">
                        <table>
                            <tr>
                                <td style="text-allign:right">
                                    Common Name:  
                                </td>
                                <td style="text-allign:left">
                                    <input id="common_name"  onblur="checkTextField(this);" placeholder="ip,domain name" style="width:170px" name="common_name" type="text" ></input>
                                </td>
                                <td>&nbsp;</td>
                                <td style="text-allign:right">
                                    OrganizationName:
                                </td>
                                <td style="text-allign:left">
                                    <input  onblur="checkTextField(this);" id="_txtorganizationName" placeholder="ex.telecom malaysia" style="width:170px" name="_txtorganizationName" type="text" ></input>
                                </td>
                            </tr>
                            <tr>
                                <td> &nbsp; &nbsp;</td> <td> &nbsp;&nbsp;</td> <td> &nbsp;&nbsp; </td> <td>&nbsp;&nbsp;  </td>
                            </tr>
                            <tr>
                                <td style="text-allign:right">
                                    OrganizationUnit:
                                </td>
                                <td style="text-allign:left">
                                    <input id="_txtOrganizationUnit"   onblur="checkTextField(this);" style="width:170px" placeholder="ex.business management" name="_txtOrganizationUnit" type="text" ></input>
                                </td>   <td>&nbsp;</td>
                                <td style="text-allign:right">
                                    LocalityName:
                                </td>
                                <td style="text-allign:left">
                                    <input id="_txtLocalityName"  onblur="checkTextField(this);" style="width:170px" placeholder="ex.cyberjaya" name="_txtLocalityName" type="text" ></input>
                                </td>
                            </tr><tr>
                                <td> &nbsp; &nbsp;</td> <td> &nbsp;&nbsp;</td> <td> &nbsp;&nbsp; </td> <td>&nbsp;&nbsp;  </td> 
                            </tr>
                            <tr>
                                <td style="text-allign:right">
                                    State:
                                </td>
                                <td style="text-allign:left">
                                    <input id="_txtState" style="width:170px;"   onblur="checkTextField(this);"  placeholder="ex.Kaula Lampur" name="_txtState" type="text" ></input>
                                </td>   <td>&nbsp;</td>
                                <td style="text-allign:right">
                                    Country:
                                </td>
                                <td style="text-allign:left">
                                    <input id="_txtCountry" style="width:80px"   onblur="checkTextField(this);" name="_txtCountry" placeholder="ex.MY" type="text" ></input>
                                </td>
                            </tr><tr>
                                <td> &nbsp; &nbsp;</td> <td> &nbsp;&nbsp;</td> <td> &nbsp;&nbsp; </td> <td>&nbsp;&nbsp;  </td>
                            </tr>
                            <tr>
                                <td style="text-allign:right">
                                    Email
                                </td>
                                <td style="text-allign:left">
                                    <input id="_txtEmail" style="width:170px"   onblur="checkTextField(this);" placeholder="ex.pramod@mollatech.com" name="_txtEmail" type="text" ></input>
                                </td>   <td>&nbsp;</td>
                                <!--                                 <td style="text-allign:right">
                                                                 Country:
                                                                </td>
                                                                 <td style="text-allign:left">
                                                                 <input id="Country" style="width:80px" name="_txtCountry" type="text" ></input>
                                                                </td>-->
                            </tr>
                        </table>
                    </div>
                    <div id="divThirdParty" style="display:none;">
                        <table>
                            <tr>
                                <td style="text-allign:right">
                                    Common Name:  
                                </td>
                                <td style="text-allign:left">
                                    <input id="common_nameTP"  onblur="checkTextField(this);" placeholder="ip,domain name" style="width:170px" name="common_nameTP" type="text" ></input>
                                </td>
                                <td>&nbsp;</td>
                                <td style="text-allign:right">
                                    OrganizationName:
                                </td>
                                <td style="text-allign:left">
                                    <input  onblur="checkTextField(this);" id="_txtorganizationNameTP" placeholder="ex.telecom malaysia" style="width:170px" name="_txtorganizationNameTP" type="text" ></input>
                                </td>
                            </tr>
                            <tr>
                                <td> &nbsp; &nbsp;</td> <td> &nbsp;&nbsp;</td> <td> &nbsp;&nbsp; </td> <td>&nbsp;&nbsp;  </td>
                            </tr>
                            <tr>
                                <td style="text-allign:right">
                                    OrganizationUnit:
                                </td>
                                <td style="text-allign:left">
                                    <input id="_txtOrganizationUnitTP"   onblur="checkTextField(this);" style="width:170px" placeholder="ex.business management" name="_txtOrganizationUnitTP" type="text" ></input>
                                </td>   <td>&nbsp;</td>
                                <td style="text-allign:right">
                                    LocalityName:
                                </td>
                                <td style="text-allign:left">
                                    <input id="_txtLocalityNameTP"  onblur="checkTextField(this);" style="width:170px" placeholder="ex.cyberjaya" name="_txtLocalityNameTP" type="text" ></input>
                                </td>
                            </tr><tr>
                                <td> &nbsp; &nbsp;</td> <td> &nbsp;&nbsp;</td> <td> &nbsp;&nbsp; </td> <td>&nbsp;&nbsp;  </td> 
                            </tr>
                            <tr>
                                <td style="text-allign:right">
                                    State:
                                </td>
                                <td style="text-allign:left">
                                    <input id="_txtStateTP" style="width:170px;"   onblur="checkTextField(this);"  placeholder="ex.Kaula Lampur" name="_txtStateTP" type="text" ></input>
                                </td>   <td>&nbsp;</td>
                                <td style="text-allign:right">
                                    Country:
                                </td>
                                <td style="text-allign:left">
                                    <input id="_txtCountryTP" style="width:80px"   onblur="checkTextField(this);" name="_txtCountryTP" placeholder="ex.MY" type="text" ></input>
                                </td>

                                <td style="text-allign:right">
                                    City:
                                </td>
                                <td style="text-allign:left">
                                    <input id="_txtcityTP" style="width:80px"   onblur="checkTextField(this);" name="_txtcityTP" placeholder="ex.MY" type="text" ></input>
                                </td>


                            </tr><tr>
                                <td> &nbsp;&nbsp;</td> <td> &nbsp;&nbsp;</td> <td> &nbsp;&nbsp; </td> <td>&nbsp;&nbsp;  </td>
                            </tr>
                            <tr>
                                <td style="text-allign:right">
                                    Email
                                </td>
                                <td style="text-allign:left">
                                    <input id="_txtEmailTP" style="width:170px"   onblur="checkTextField(this);" placeholder="ex.pramod@mollatech.com" name="_txtEmailTP" type="text" ></input>
                                </td>   <td>&nbsp;</td>
                                <td style="text-allign:right">

                                    <a onclick="generateCSR()" id="btngenCSR">Generate CSR</a>
                                </td>

                                <td style="text-allign:left">
                                    <select id="selApproverEmailTP" name="selApproverEmailTP">

                                    </select>

                                </td>

                            </tr>

                            <tr>
                                <td><h5>Contact </h5> </td> <td> <h5> Information</h5> </td> <td> &nbsp;&nbsp; </td> <td>&nbsp;&nbsp; </td>
                            </tr>

                            <tr>
                                <td style="text-allign:right">
                                    Coupon Code:
                                </td>
                                <td style="text-allign:left">
                                    <input id="_txtCouponCode" style="width:170px"   onblur="checkTextField(this);" placeholder="code" name="_txtCouponCode" type="text" ></input>
                                </td>   <td>&nbsp;</td>
                                <td style="text-allign:right">
                                    Fist Name:
                                </td>
                                <td style="text-allign:left">
                                    <input id="_txtFirstName" style="width:170px"   onblur="checkTextField(this);" placeholder="First Name" name="_txtFirstName" type="text" ></input>    
                                </td>
                            </tr>



                            <tr>
                                <td> &nbsp; &nbsp;</td> <td> &nbsp;&nbsp;</td> <td> &nbsp;&nbsp; </td> <td>&nbsp;&nbsp;  </td>
                            </tr>
                            <tr>
                                <td style="text-allign:right">
                                    Last Name:
                                </td>
                                <td style="text-allign:left">
                                    <input id="_txtLastName" style="width:170px"   onblur="checkTextField(this);" placeholder="Last Name" name="_txtLastName" type="text" ></input>     
                                </td> <td>&nbsp;</td> 
                                <td>
                                    Phone No:       
                                </td>
                                <td style="text-allign:right">
                                    <input id="_txtphon" style="width:170px"   onblur="checkTextField(this);" placeholder="Phone no" name="_txtphon" type="text" ></input>     
                                </td>


                            </tr>
                            <tr>
                                <td> &nbsp; &nbsp;</td> <td> &nbsp;&nbsp;</td> <td> &nbsp;&nbsp; </td> <td>&nbsp;&nbsp;  </td>
                            </tr>
                            <tr>
                                <td style="text-allign:right">
                                    Email Id:
                                </td>
                                <td style="text-allign:left">
                                    <input id="_txtEmail" style="width:170px"   onblur="checkTextField(this);" placeholder="Email ID" name="_txtEmail" type="text" ></input>     
                                </td> <td>&nbsp;</td> 
                                <td>

                                </td>
                                <td style="text-allign:right">

                                </td>


                            </tr>
                        </table>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>
    <script>
        $(function () {
            $('#PushdatetimepickerPKI').datepicker({
                language: 'pt-BR'
            });
        });
        $(function () {
            $('#PushdatetimepickerPKI1').datepicker({
                language: 'pt-BR'
            });
        });
    </script>
    <script>
        $(function () {
            $('#Pushdatetimepicker1').datepicker({
                language: 'pt-BR'
            });
        });
        $(function () {
            $('#Pushdatetimepicker2').datepicker({
                language: 'pt-BR'
            });
        });
    </script>
    <div class="modal-footer">
        <div id="editoperator-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="generateCertificate()" id="buttonEditOperatorSubmit">Generate Certificate</button>
    </div>
</div>

<!--end pki-->

<!--challenge response-->

<!--end challenge response-->
<!--secure phrase-->

<script type="text/javascript">
    $(function () {
        $('#PushdatetimepickerSecurePhrase').datepicker({
            language: 'pt-BR'
        });
    });
    $(function () {
        $('#PushdatetimepickerSecurePhrase1').datepicker({
            language: 'pt-BR'
        });
    });
    //                            ChangeMediaType(0);
</script>    



<!--secure phrase-->

<!-- Added by abhishek-->





<%@include file="footer.jsp" %>