<%@page import="com.mollatech.axiom.nucleus.settings.ContactTagsSetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Templates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/pdfsettings.js"></script>
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<script src="./assets/js/bootstrap.timepicker.min.js"></script>
<link href="./assets/css/bootstrap-timepicker.min.css" rel="stylesheet"/>
<link type="text/css" href="css/bootstrap.min.css" />

<link href="./assets/css/bootstrap-responsive.min.css" rel="stylesheet"/>
<link href="./assets/css/bootstrap-timepicker.css" rel="stylesheet"/>
<script type="text/javascript" src="./assets/js/bootbox.min.js"></script>
<script src="./assets/js/bootstrap-timepicker.js"></script>

<script src="./assets/js/ajaxfileupload.js"></script>
<script src="./assets/js/bootstrap-fileupload.js"></script> 
<link href="./assets/css/bootstrap.css" rel="stylesheet">
<link href="./assets/css/bootstrap-fileupload.css" rel="stylesheet">
<html>
    <style>
        .topcorner{
            position:absolute;
            top:70px;
            right:500px;
        }

        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }
    </style>
</style>
<div class="topcorner" >
    <div class="container" >
        <h1 class="text-success" style="margin-left: 200px">Test Template</h1>
        <hr>
        <form class="form-horizontal" id="TestResult" name="TestResult">
            <div id="testdocument">
                <div class="controls fileupload fileupload-new" data-provides="fileupload">
                    <div class="input-append">
                        <div class="uneditable-input span4"><i class="icon-file fileupload-exists"></i> 
                            <span class="fileupload-preview"></span>
                        </div>
                        <span class="btn btn-file" >
                            <span class="fileupload-new">Select file</span>
                            <span class="fileupload-exists">Change</span>
                            <input type="file" id="filetemplatetoupload" name="filetemplatetoupload"/>
                        </span>
                        <a href="./TestResult.jsp" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                        <button class="btn btn-success" style="margin-left: 5px" id="buttonUploadpdf"  onclick="TestFileUpload()" >Upload Now>></button>

                        <input type="text" style="width: 170px;margin-left: 5px" size="4" id="docsname" name="docsname" placeholder="Enter Document Name"/>
                        <input type="text"  size="4" style="width: 170px; margin-left: 5px" id="templateName1" name="templateName1" placeholder="Enter template name"/>
                        <button type="button" style="margin-left: 5px" onclick="generateRes()" class="btn btn-success">Generate Result</button>
                        <button type="button" id="correctword" style="margin-left: 5px" onclick="getRawdata()" class="btn btn-success" disabled>Correct word</button>
                    </div>
                </div>
                <div class="controls" data-provides="fileupload">
                    <table  id="customers"  style=" font-family: arial, sans-serif; width: 300%;">
                    </table>
                </div>
            </div>
            <div id="showrawdata">

            </div>
        </form>
    </div>  
</div>
</html>
<script>
    function strcmpPull(a, b)
    {
        return (a < b ? -1 : (a > b ? 1 : 0));
    }
    function Alert4Pull(msg) {
        bootbox.alert("<h2>" + msg + "</h2>", function (result) {
            if (result == false) {
            } else {
                //end here
            }
        });
    }
    function TestFileUpload() {
        $('#buttonUploadpdf').attr("disabled", true);
        var s = './TestFIleUpload';
        $.ajaxFileUpload({
            fileElementId: 'filetemplatetoupload',
            url: s,
            dataType: 'json',
            success: function (data) {
                if (strcmpPull(data.result, "error") == 0) {
                    Alert4Pull("<span><font color=red>" + data.message + "</font></span>");
                    $('#buttonUploadtemplate').attr("disabled", false);
                } else if (strcmpPull(data.result, "success") == 0) {
                    Alert4Pull("<span><font color=blue>" + "File uploaded successfully" + "</font></span>");
                    $('#buttonUploadtemplate').attr("disabled", false);
                }
            },
            error: function (data) {
                alert(JSON.stringify(data));
            }

        });
    }
    function generateRes() {
        var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
        pleaseWaitDiv.modal();
        var s = './SendTestdata';
        $.ajax({
            type: 'GET',
            url: s,
            dataType: 'json',
            data: $('#TestResult').serialize(),
            success: function (data) {
                pleaseWaitDiv.modal('hide');
                var table = document.getElementById("customers");
                var jsondata = JSON.parse(data.result);
                var Key = "Key";
                var Value = "Value";
                jsondata[Key] = Value;
                document.getElementById('correctword').disabled = false;
                for (var i in jsondata) {
                    var row = table.insertRow(0);
                    var cell1 = row.insertCell(0);
                    var cell2 = row.insertCell(1);
                    cell1.innerHTML = i;
                    if (i === 'Mrz') {
                        var a = jsondata[i].replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '&lt');
                        cell2.innerHTML = a;
                    } else {
                        cell2.innerHTML = jsondata[i];
                    }
                }
            },
            error: function (data) {
                JSON.stringify(data);
            }
        });
    }

    function getRawdata(){
    $("#testdocument").empty;
    $("#testdocument").hide();
    
    pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    pleaseWaitDiv.modal();
    var s = './correctionRawData.jsp';
    $.ajax({
    type: 'GET',
            url: s,
            dataType: 'json',
            success: function (data) {
            pleaseWaitDiv.modal('hide');
            $('#showrawdata').html(data);
            },
            error: function (data) {
                alert(error);
            JSON.stringify(data);
            pleaseWaitDiv.modal('hide');
            }
    });
    }
</script>