<%@include file="header.jsp" %>
<script src="./assets/js/interactions.js"></script>
<div class="container-fluid">
    <h2>Create Interaction</h2>
    <p>You can create new interaction (survey, feedback, polling) where you can gather information real time using mobile channels like SMS, USSD and Voice from your customers. </p>
    <div class="row-fluid">
        <form class="form-horizontal" id="createInteractionForm" name="createInteractionForm">

            <hr>
            <div class="tabbable">
                <ul class="nav  nav-tabs">
                    <li class="active"><a href="#greetings" data-toggle="tab">Greetings</a></li>
                    <li ><a href="#question1" data-toggle="tab">Question 1</a></li>
                    <li><a href="#question2" data-toggle="tab">Question 2</a></li>
                    <li><a href="#question3" data-toggle="tab">Question 3</a></li>
                    <li><a href="#question4" data-toggle="tab">Question 4</a></li>
                    <li><a href="#question5" data-toggle="tab">Question 5</a></li>
                    <li><a href="#offer" data-toggle="tab">Completion Offer</a></li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="greetings">
                        <div class="control-group">
                            <label class="control-label"  for="username">Interaction Name</label>
                            <div class="controls">
                                <input type="text" id="_nameIR" name="_nameIR" placeholder="example... loan-plan" class="span4">                    
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Greeting Message</label>
                            <div class="controls">
                                <input type="text" id="_nameG" name="_nameG" placeholder="Dear #name#, we are conducting loan repayment survey. Thanks for your support. Dated: #today#, #now#" class="span9">

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Option "Accept"</label>
                            <div class="controls">                                
                                <input type="text" id="_nameYES" name="_nameYES" placeholder="e.g. Press 1 to accept and proceed ahead. " class="span6">                    
                                with User Response: <input type="text" id="_respYES" name="_respYES" placeholder="1" class="span1">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Option "Reject"</label>
                            <div class="controls">                                
                                <input type="text" id="_nameNO" name="_nameNO" placeholder="e.g Press 2 to opt out." class="span6">                    
                                with User Response: <input type="text" id="_respNO" name="_respNO" placeholder="2" class="span1">
                            </div>
                        </div>                        
                    </div>                    
                    <div class="tab-pane" id="question1">
                        <div class="control-group">
                            <label class="control-label"  for="username">Question 1</label>
                            <div class="controls">
                                <input type="text" id="_Q1" name="_Q1" placeholder="You can ask question 1 now here...." class="span9">                    
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Option 1</label>
                            <div class="controls">                                
                                <input type="text" id="_Q1O1M" name="_Q1O1M" placeholder="e.g. Press 1 to option 1?" class="span6">                    
                                with User Response: <input type="text" id="_Q1O1R" name="_Q1O1R" placeholder="1" class="span1">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Option 2</label>
                            <div class="controls">                                
                                <input type="text" id="_Q1O2M" name="_Q1O2M" placeholder="e.g. Press 2 to option 1?" class="span6">                    
                                with User Response: <input type="text" id="_Q1O2R" name="_Q1O2R" placeholder="2" class="span1">
                            </div>
                        </div> 
                        <div class="control-group">
                            <label class="control-label"  for="username">Option 3</label>
                            <div class="controls">                                
                                <input type="text" id="_Q1O3M" name="_Q1O3M" placeholder="e.g. Press 3 to option 1?" class="span6">                    
                                with User Response: <input type="text" id="_Q1O3R" name="_Q1O3R" placeholder="3" class="span1">
                            </div>
                        </div> 
                    </div>
                    <div class="tab-pane" id="question2">
                        <div class="control-group">
                            <label class="control-label"  for="username">Question 2</label>
                            <div class="controls">
                                <input type="text" id="_Q2" name="_Q2" placeholder="You can ask question 1 now here...." class="span9">                    
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Option 1</label>
                            <div class="controls">                                
                                <input type="text" id="_Q2O1M" name="_Q2O1M" placeholder="e.g. Press 1 to option 1?" class="span6">                    
                                with User Response: <input type="text" id="_Q2O1R" name="_Q2O1R" placeholder="1" class="span1">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Option 2</label>
                            <div class="controls">                                
                                <input type="text" id="_Q2O2M" name="_Q2O2M" placeholder="e.g. Press 2 to option 1?" class="span6">                    
                                with User Response: <input type="text" id="_Q2O2R" name="_Q2O2R" placeholder="2" class="span1">
                            </div>
                        </div> 
                        <div class="control-group">
                            <label class="control-label"  for="username">Option 3</label>
                            <div class="controls">                                
                                <input type="text" id="_Q2O3M" name="_Q2O3M" placeholder="e.g. Press 3 to option 1?" class="span6">                    
                                with User Response: <input type="text" id="_Q2O3R" name="_Q2O3R" placeholder="3" class="span1">
                            </div>
                        </div> 
                    </div>
                    <div class="tab-pane" id="question3">
                        <div class="control-group">
                            <label class="control-label"  for="username">Question 3</label>
                            <div class="controls">
                                <input type="text" id="_Q3" name="_Q3" placeholder="You can ask question 1 now here...." class="span9">                    
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Option 1</label>
                            <div class="controls">                                
                                <input type="text" id="_Q3O1M" name="_Q3O1M" placeholder="e.g. Press 1 to option 1?" class="span6">                    
                                with User Response: <input type="text" id="_Q3O1R" name="_Q3O1R" placeholder="1" class="span1">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Option 2</label>
                            <div class="controls">                                
                                <input type="text" id="_Q3O2M" name="_Q3O2M" placeholder="e.g. Press 2 to option 1?" class="span6">                    
                                with User Response: <input type="text" id="_Q3O2R" name="_Q3O2R" placeholder="2" class="span1">
                            </div>
                        </div> 
                        <div class="control-group">
                            <label class="control-label"  for="username">Option 3</label>
                            <div class="controls">                                
                                <input type="text" id="_Q3O3M" name="_Q3O3M" placeholder="e.g. Press 3 to option 1?" class="span6">                    
                                with User Response: <input type="text" id="_Q3O3R" name="_Q3O3R" placeholder="3" class="span1">
                            </div>
                        </div> 
                    </div>
                    <div class="tab-pane" id="question4">
                        <div class="control-group">
                            <label class="control-label"  for="username">Question 4</label>
                            <div class="controls">
                                <input type="text" id="_Q4" name="_Q4" placeholder="You can ask question 1 now here...." class="span9">                    
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Option 1</label>
                            <div class="controls">                                
                                <input type="text" id="_Q4O1M" name="_Q4O1M" placeholder="e.g. Press 1 to option 1?" class="span6">                    
                                with User Response: <input type="text" id="_Q4O1R" name="_Q4O1R" placeholder="1" class="span1">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Option 2</label>
                            <div class="controls">                                
                                <input type="text" id="_Q4O2M" name="_Q4O2M" placeholder="e.g. Press 2 to option 1?" class="span6">                    
                                with User Response: <input type="text" id="_Q4O2R" name="_Q4O2R" placeholder="2" class="span1">
                            </div>
                        </div> 
                        <div class="control-group">
                            <label class="control-label"  for="username">Option 3</label>
                            <div class="controls">                                
                                <input type="text" id="_Q4O3M" name="_Q4O3M" placeholder="e.g. Press 3 to option 1?" class="span6">                    
                                with User Response: <input type="text" id="_Q4O3R" name="_Q4O3R" placeholder="3" class="span1">
                            </div>
                        </div> 
                    </div>
                    <div class="tab-pane" id="question5">
                        <div class="control-group">
                            <label class="control-label"  for="username">Question 5</label>
                            <div class="controls">
                                <input type="text" id="_Q5" name="_Q5" placeholder="You can ask question 1 now here...." class="span9">                    
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Option 1</label>
                            <div class="controls">                                
                                <input type="text" id="_Q5O1M" name="_Q5O1M" placeholder="e.g. Press 1 to option 1?" class="span6">                    
                                with User Response: <input type="text" id="_Q5O1R" name="_Q5O1R" placeholder="1" class="span1">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Option 2</label>
                            <div class="controls">                                
                                <input type="text" id="_Q5O2M" name="_Q5O2M" placeholder="e.g. Press 2 to option 1?" class="span6">                    
                                with User Response: <input type="text" id="_Q5O2R" name="_Q5O2R" placeholder="2" class="span1">
                            </div>
                        </div> 
                        <div class="control-group">
                            <label class="control-label"  for="username">Option 3</label>
                            <div class="controls">                                
                                <input type="text" id="_Q5O3M" name="_Q5O3M" placeholder="e.g. Press 3 to option 1?" class="span6">                    
                                with User Response: <input type="text" id="_Q5O3R" name="_Q5O3R" placeholder="3" class="span1">
                            </div>
                        </div> 
                    </div>
                    <div class="tab-pane" id="offer">
                        <div class="control-group">
                            <label class="control-label"  for="username">Completion Message</label>
                            <div class="controls">
                                <input type="text" id="_nameTHANKS" name="_nameTHANKS" placeholder="Thanks #name#, please find your coupon on your next purchase. Have a good day. Dated: #today#, #now#" class="span9">
                            </div>
                        </div>
                         
                    </div>
                </div>
            </div>
            <hr>
            <p class="text-info">Attention: #name#,#now#,#today#,#phone# will be replaced with real-time data for each request. Please use these tags in messages.</p>
    
            
            <!-- Submit -->
                <p class="text-left">
                    <div id="save-interactions-settings-result"></div>
                    <button class="btn btn-primary" onclick="saveNewInteraction()" type="button">Create New Interaction Now >> </button>                
                </p>
        </form>
    </div>
</div>


<%@include file="footer.jsp" %>