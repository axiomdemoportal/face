<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.connector.access.controller.AccessMatrixSettings"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Roles"%>
<%@page import="com.mollatech.axiom.nucleus.db.operation.AxiomOperator"%>
<%@page import="com.mollatech.axiom.nucleus.db.Operators"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet"%>
<%@page import="com.mollatech.axiom.nucleus.db.Units"%>
<%--<%@include file="header.jsp" %>--%>
<%
    UnitsManagemet uMngt = new UnitsManagemet();
    Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
    AccessMatrixSettings accessObj = (AccessMatrixSettings) session.getAttribute("_apAccessEntry");
%>
<!--<div class="container-fluid">-->
<div id="auditTable">
    <!--        <h1 class="text-success">Operator Management</h1>
            <p>You can manage all your operators, their roles and password management. The "admin" is the most powerful operator followed with "helpdesk" to manage daily request and the "reporter" can read-only operator role for pulling reports of the system. </p>-->
    <br>
    <div class="row-fluid">
        <div id="licenses_data_table">
            <table class="display responsive wrap" id="OperatorTable">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone no</th>
                        <th>Unit</th>
                        <!--                    <td>Type</td>-->
                        <th>Manage</th>
                        <th>Role</th>
                        <th>Password</th>
                        <!--                    <td>Access As</td>-->
                        <th>Attempts</th>
                        <th>Audit</th>
                        <th>Access From</th>
                        <th>Expires</th>
                        <th>Created On </th>
                        <th>Last Access</th>
                    </tr>
                </thead>
                <%
                    String _status = request.getParameter("status");
                    String startDate = request.getParameter("fromDate");
                    String endDate = request.getParameter("toDate");
                    String keyword = request.getParameter("_searchtext");
                    String roleId = request.getParameter("_roleid");
                    int _roleID = Integer.parseInt(roleId);
                    int statuUs = Integer.parseInt(_status);
                    String _operatorStatuc = request.getParameter("_operatorStatuc");

                    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                    Date _startDate = (Date) formatter.parse(startDate);
                    Date _endDate = (Date) formatter.parse(endDate);
                    int opStatus = 0;
                    if (_status != null) {
                        opStatus = Integer.parseInt(_status);
                    }

                    Channels _apSChannelDetails = (Channels) session.getAttribute("_apSChannelDetails");
                    OperatorsManagement oManagement = new OperatorsManagement();
                    String _sessionID = (String) session.getAttribute("_apSessionID");

                    SessionManagement smObj = new SessionManagement();
                    int iStatus = smObj.GetSessionStatus(_sessionID);
                    AxiomOperator[] axiomoperatorObj = null;
                    Roles roleObj = oManagement.getRoleByRoleId(_apSChannelDetails.getChannelid(), Integer.parseInt(roleId));
                    axiomoperatorObj = oManagement.GetOperatorsUsingDates(_sessionID, _apSChannelDetails.getChannelid(), _startDate, _endDate, _roleID, statuUs);
                    if (axiomoperatorObj != null) {
                        for (int i = 0; i < axiomoperatorObj.length; i++) {
                            if (!axiomoperatorObj[i].getStrName().equals(operator.getName()) || axiomoperatorObj[i].getStrName().equals("sysadmin")) {
                                AxiomOperator axoprObj = axiomoperatorObj[i];
                                java.util.Date dCR = new java.util.Date(axoprObj.getUtcCreatedOn());
                                java.util.Date dLR = new java.util.Date(axoprObj.getLastUpdateOn());

                                int iOprStatus = axoprObj.getiStatus();
                                int iOprAccessType = axoprObj.iAccessType;
                                String strStatus;
                                if (iOprStatus == 1) {
                                    strStatus = "Active";
                                } else if (iOprStatus == -1) {
                                    strStatus = "Locked";
                                } else {
                                    strStatus = "Suspended";
                                }

                                String strOprAccessType = "Not Set";
                                if (iOprAccessType == 1) {
                                    strOprAccessType = "Operator";
                                } else {
                                    strOprAccessType = "Role";
                                }
                                String s1 = "Not Set";
                                String s2 = "Not Set";
                                if (axiomoperatorObj[i].accessStartFrom != null && axiomoperatorObj[i].accessTill != null) {
                                    SimpleDateFormat tzTimeHeader1 = new SimpleDateFormat("dd/MMM/yyyy");
                                    String completeTimeHeader1 = tzTimeHeader1.format(axiomoperatorObj[i].accessStartFrom);
                                    String completeTimeHeader2 = tzTimeHeader1.format(axiomoperatorObj[i].accessTill);
//                                    s = s + " Access(" + completeTimeHeader1 + "-" + completeTimeHeader2 + ")";
                                    s1 = completeTimeHeader1;
                                    s2 = completeTimeHeader2;
                                }

                                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy hh:mm");

                                String uidiv4OprStatus = "operator-status-value-" + i;

                                String uidiv4Opraccess = "operator-access-value-" + i;

                                String uidiv4OprRole = "operator-role-value-" + i;

                                String uidiv4OprAttempts = "operator-attempts-value-" + i;

                                UnitsManagemet unMngt = new UnitsManagemet();
                                Units unObj = unMngt.getUnitByUnitId(_sessionID, _apSChannelDetails.getChannelid(), axoprObj.getiUnit());
                                String unitName = "Not-Set";
                                if (unObj != null && unObj.equals("") == false) {
                                    unitName = unObj.getUnitname();
                                }

                                int iOperatorTypeMC = axoprObj.getiOperatorType();

                                String sOperatorTypeMC = "";
                                if (iOperatorTypeMC == 2) {
                                    sOperatorTypeMC = "Authorizer";
                                } else {
                                    sOperatorTypeMC = "Requester";
                                }

                %>
                <tr>
                    <%if (!operator.getName().equals("sysadmin") && axoprObj.getStrName().equals("sysadmin")) {%>

                    <%} else {%>
                    <td><%=i + 1%></td>
                    <td><%=axoprObj.getStrName()%></td>
                    <td><%=axoprObj.getStrEmail()%></td>
                    <td><%= axoprObj.getStrPhone()%></td>
                    <td><%=unitName%></td>
<!--                    <td><%=sOperatorTypeMC%></td>-->
                    <%if (axiomoperatorObj[i].getiStatus() != -99) {%>
                    <td>
                        <div class="btn-group">
                            <button class="btn btn-mini" id="<%=uidiv4OprStatus%>"><%=strStatus%></button>
                            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <%if (!axoprObj.getStrName().equals("sysadmin")) {%>
                                <li><a href="#" onclick="ChangeOperatorStatus('<%=axoprObj.getStrOperatorid()%>', 1, '<%=uidiv4OprStatus%>')">Mark as Active?</a></li>
                                <li><a href="#" onclick="ChangeOperatorStatus('<%=axoprObj.getStrOperatorid()%>', 0, '<%=uidiv4OprStatus%>')">Mark as Suspended?</a></li>
                                <li class="divider"></li>
                                    <%}%>

                                <li><a href="#" onclick="loadEditOperatorDetails('<%=axoprObj.getStrOperatorid()%>')">Edit Details</a></li>
                                    <%if (operator.getRoleid() == 1) {%>
<!--                                <li><a href="./roleAccessRightN_1.jsp?_operatorID=<%=axoprObj.getStrName()%>&_type=OPERATOR" data-toggle="modal">Access Rights</a></li>-->
                                <li><a href="#" onclick="loadEditOperatorAccess('<%=axoprObj.getStrOperatorid()%>')">Date Restriction</a></li> 
                                    <%}%>

                                <%if (axoprObj.getStrRoleName().equals("sysadmin")) {%>
                                <li><a href="#" onclick="removeOperator('<%=axoprObj.getStrOperatorid()%>')">Remove</a></li>
                                    <%}%>
                            </ul>
                        </div>

                    </td>
                    <%} else {%>
                    <td>NA</td>
                    <%}%>
                    <%if (axiomoperatorObj[i].getiStatus() != -99) {%>
                    <td>
                        <div class="btn-group">
                            <button class="btn btn-mini" id="<%=uidiv4OprRole%>"><%=axoprObj.getStrRoleName()%></button>
                            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <%
                                    Roles[] roles1 = oManagement.getAllRoles(axoprObj.strChannelid);
                                    if (!operator.getName().equals("sysadmin")) {
                                        for (int j = 0; j < roles1.length; j++) {
                                            if (!roles1[j].getName().equals("sysadmin")) {
                                %>
                                <li><a href="#" onclick="ChangeOperatorRole(<%=roles1[j].getRoleid()%>, '<%=roles1[j].getName()%>', '<%=axoprObj.getStrOperatorid()%>', '<%=uidiv4OprRole%>')"><%=roles1[j].getName()%></a></li>

                                <%} else {%>
<!--                                <li><a href="#" onclick="ChangeOperatorRole(1, 'sysadmin', '<%=axoprObj.getStrOperatorid()%>', '<%=uidiv4OprRole%>')">sysadmin</a></li>-->
                                <%;
                                        }
                                    }
                                } else {
                                    for (int j = 0; j < roles1.length; j++) {
                                %>
                                <li><a href="#" onclick="ChangeOperatorRole(<%=roles1[j].getRoleid()%>, '<%=roles1[j].getName()%>', '<%=axoprObj.getStrOperatorid()%>', '<%=uidiv4OprRole%>')"><%=roles1[j].getName()%></a></li>


<!--                                <li><a href="#" onclick="ChangeOperatorRole(1, 'sysadmin', '<%=axoprObj.getStrOperatorid()%>', '<%=uidiv4OprRole%>')">sysadmin</a></li>-->
                                <%
                                        }
                                    }
                                %>

                            </ul>
                        </div>
                    </td>
                    <%} else {%><td>NA</td><%}%>

                    <%if (axiomoperatorObj[i].getiStatus() != -99) {%> 
                    <td>
                        <div class="btn-group">
                            <button class="btn btn-mini">*****</button>
                            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="OperatorUnlockPassword('<%=axoprObj.getStrOperatorid()%>', '<%=uidiv4OprAttempts%>')" >Unlock Password</a></li>
                                <li class="divider"></li>
                                <li><a href="#" onclick="OperatorResendPassword('<%=axoprObj.getStrOperatorid()%>')">Resend Current Password (via email)</a></li>
                                <li class="divider"></li>
                                <li><a href="#" onclick="OperatorSetPassword('<%=axoprObj.getStrOperatorid()%>')">Set Random Password</a></li>
                                <li><a href="#" onclick="OperatorSendRandomPassword('<%=axoprObj.getStrOperatorid()%>')">Set & Send Random Password (via email)</a></li>
                            </ul>
                        </div>
                    </td>
                    <%} else {%>
                    <td>NA</td>
                    <%}%>
                    <!--                    <td>
                                            <div class="btn-group">
                                                <button class="btn btn-mini" id="<%=uidiv4Opraccess%>"><%=strOprAccessType%></button>
                                                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" onclick="ChangeOperatorAccess('<%=axoprObj.getStrOperatorid()%>', <%=OperatorsManagement.OPERATOR_ACCESS%>, '<%=uidiv4Opraccess%>')">As Operator</a></li>
                                                    <li><a href="#" onclick="ChangeOperatorAccess('<%=axoprObj.getStrOperatorid()%>',<%=OperatorsManagement.ROLE_ACCESS%>, '<%=uidiv4Opraccess%>')">As Role</a></li>
                                                </ul>
                                            </div>
                    
                                        </td>-->
                    <td><div id="<%=uidiv4OprAttempts%>"><%=axoprObj.getiWrongAttempts()%></div></td>

                    <td>
                        <a href="#" class="btn btn-mini" onclick="loadEditRequesterOperatorDetails('<%=axoprObj.getStrOperatorid()%>')">Download</a>
                    </td>
                    <%if (axoprObj.getAccessStartFrom() != null) {%>
                    <td><%=sdf.format(axoprObj.getAccessStartFrom())%></td>
                    <%} else {%>
                    <td>Not Set</td>
                    <%}%>
                    <%if (axoprObj.getAccessTill() != null) {%>
                    <td><%=sdf.format(axoprObj.getAccessTill())%></td>
                    <%} else {%>
                    <td>Not Set</td>
                    <%}%>

                    <td><%=sdf.format(dCR)%></td>
                    <td><%=sdf.format(dLR)%></td>
                </tr>
                <%}%>
                <%}
                    }
                } //for 
                //if 
                else {%>
                <h3>No users found...</h3>
                <%}%>
            </table>
        </div>
    </div>
    <br>
    <script>
        $(document).ready(function () {
            $('#OperatorTable').DataTable({
                responsive: true
            });
        });
    </script>
</div>
