<%@include file="header.jsp" %>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<script src="./assets/js/audit.js"></script>

<div class="container-fluid">
    <h1 class="text-success">Session ID Audit</h1>
    <p>
        <br>
        <!--<h3>Search Contacts through name,phone or email</h3>-->   
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="testUSSDSecondaryForm" name="testUSSDSecondaryForm">
                <fieldset>
                     <div class="control-group">
                        <label class="control-label"  for="username">Select Option:</label>
                        <div class="controls">
                            <br></br>
                            <div>
                           <select class="span4" name="_sessionType" id="_sessionType">
                                <option value="1">Using Active Session</option>
                                <option value="0">Using Session Id</option>
                            </select>
                            </div>

                        </div>
                    </div>
                    
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Enter Session ID:</label>
                        <div class="controls">
                            <input type="text" id="_sessionId" name="_sessionId" placeholder="e.g. uan59nBXV6PHjCnwWOFEWrqbeac=" class="span6">
                            <!--<input type="text" id="_testmsgS" name="_testmsgS" placeholder="this is the sample message to be sent out..." class="input-xlarge">-->
                            <br></br>
                            <div>
                                <a href="#" class="btn btn-large btn-info" onclick="SessionAudits()">Download PDF</a>
                                <a href="#" class="btn btn-large btn-info" onclick="SessionAuditsCSV()">Download CSV</a>
                                <a href="#" class="btn btn-large btn-danger" onclick="SessionTerminate()">Kill Session</a>
                            </div>

                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>
    <br>
    <%@include file="footer.jsp" %>