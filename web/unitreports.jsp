<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>
<%@page import="com.mollatech.axiom.connector.communication.AXIOMStatus"%>
<%@page import="com.mollatech.axiom.nucleus.settings.SendNotification"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.TemplateUtils"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.db.Templates"%>
<%@page import="com.mollatech.axiom.nucleus.db.operation.TemplateNames"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%@page import="com.mollatech.axiom.nucleus.settings.TokenSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet"%>
<%@page import="com.mollatech.axiom.nucleus.db.Units"%>
<%@include file="header.jsp" %>
<script src="assets/js/units.js" type="text/javascript"></script>
<div class="container-fluid" >
    <h1 class="text-success">Unit Reports</h1>
    <h3>Make Your Own Report</h3>
    <input type="hidden" id="_changeStatus" name="_changeStatus">

    <div class="row-fluid"  id="searchUReport">
        <div class="span12">
            <form id="unitReportform" name="unitReportform">
                <div class="control-group form-inline">                
                    <div class="input-append">

                        <span class="add-on">From:</span>   
                        <div id="datetimepicker1" class="input-append date">
                            <input id="userstartdate" name="userstartdate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth">
                            <span class="add-on">
                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                            </span>
                        </div>

                    </div>
                    <div class="input-append">
                        <span class="add-on">To:</span>   
                        <div id="datetimepicker2" class="input-append date">
                            <input id="userenddate" name="userend  date" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth">
                            <span class="add-on">
                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                            </span>
                        </div>
                    </div>
                    <%
                        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
                        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                        UnitsManagemet unitList = new UnitsManagemet();
                        Units[] unitdetails;
                        unitdetails = unitList.ListUnitss(sessionId, channel.getChannelid());
                    %>
                    <div class="input-prepend">
                        <select name="_searchtext" id="_searchtext" >
                            <option value="-1" disabled selected style="display: none;">--Select unit name</option>
                        <option value="All">All Role</option>
                            <%
                                if (unitdetails != null) {
                                    for (int i = 0; i < unitdetails.length; i++) {
                            %>
                            <option value="<%=unitdetails[i].getUnitname()%>"><%=unitdetails[i].getUnitname()%></option>
                            <%}
                                }%>
                        </select>
                        <!--<span class="add-on " >Enter </span><input id="_searchtext" name="_searchtext" class="" type="text" placeholder="Unit Name" data-bind="value: vm.ActualDoorSizeDepth" />-->
                    </div>
                    <!--<i>For List only, select Status: </i>--> 
                    <!--                <select name="_status" id="_status" >
                                        <option value="2">All</option>
                                        <option value="1">Active Only</option>
                                        <option value="0">Suspended Only</option>
                                        <option value="-1">Locked Only</option>
                                        <option value="-99">Permanantly delete</option>
                                    </select>-->
                    <%if (oprObj.getRoleid() != 1) {%>
                    <%if (accessObj != null && accessObj.viewUnitRports == true) {%>
                    <a href="#" class="btn btn-success" onclick="getunitRepprts()">Generate Report</a>
                    <%} else {%>
                    <a href="#" class="btn btn-success" onclick="InvalidRequest('unitreport')">Generate Report</a>
                    <%}
                    } else {%>
                    <a href="#" class="btn btn-success" onclick="getunitRepprts()">Generate Report</a>
                    <%}%>
                </div>
            </form>
        </div>
    </div>


    <div id="auditDownload" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3><div id="idauditDownload"></div></h3>
            <h3 id="myModalLabel">Download Audit</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="testSMSPrimaryForm" name="testSMSPrimaryForm">
                    <fieldset>
                        <!-- Name -->
                        <input type="hidden" id="_oprnameR" value="<%=oprObj.getName()%>" name="_oprnameR"/>
                        <input type="hidden" id="_opridR" value="<%=oprObj.getOperatorid()%>" name="_opridR"/>
                        <div class="control-group">
                            <label class="control-label"  for="username">Start Date</label>
                            <div class="controls" align="left" >

                                <!--<span class="add-on">From:</span>-->   
                                <div id="Pushdatetimepicker1" class="input-append date">
                                    <input id="_auditStartDate" name="_auditStartDate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">End Date</label>
                            <div class="controls" align="left">
                                <!--<span class="add-on">Till:</span>-->   
                                <div id="Pushdatetimepicker2" class="input-append date">
                                    <input id="_auditEndDate" name="_auditEndDate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>                    
                    </fieldset>
                </form>
            </div>
        </div>

        <div class="modal-footer">
            <div id="editoperator-result"></div>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button class="btn btn-primary" onclick="searchForUnit()" id="buttonEditOperatorSubmit">Show Audit</button>
        </div>
    </div>


    <div id="units_table_main"></div>
    <div id="auditTable"></div>

    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            });
        });
        $(function () {
            $('#datetimepicker2').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'

            });

        });
        $(function () {
            $('#Pushdatetimepicker1').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            });
        });
        $(function () {
            $('#Pushdatetimepicker2').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            });
        });

    </script>
</div>

<%@include file="footer.jsp" %>
