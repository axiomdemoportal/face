<%@page import="com.mollatech.axiom.nucleus.db.Roles"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Operators"%>
<%

    Operators operatorS1 = (Operators) request.getSession().getAttribute("_apOprDetail");
    Operators oprObj1 = (Operators) session.getAttribute("_apOprDetail");
    //String name1 = oprObj1.getName();

    OperatorsManagement oprmngObj1 = new OperatorsManagement();
    Roles[] roles1 = oprmngObj1.getAllRoles(oprObj1.getChannelid());
    String strRole = "";
    for (int j = 0; j < roles1.length; j++) {
        if (operatorS1.getRoleid() == roles1[j].getRoleid()) {
            strRole = roles1[j].getName();
            session.setAttribute("_apOprRoleName", strRole);
            break;
        }
    }

    int iOperatorRole1 = 0;

    if (strRole.compareTo("sysadmin") == 0) {
        iOperatorRole1 = 4;
    } else if (strRole.compareTo("admin") == 0) {
        iOperatorRole1 = 3;
    } else if (strRole.compareTo("helpdesk") == 0) {
        iOperatorRole1 = 2;
    } else if (strRole.compareTo("reporter") == 0) {
        iOperatorRole1 = 1;
    } else if (strRole.compareTo("requester") == 0) {
        iOperatorRole1 = 5;
    } else if (strRole.compareTo("authorizer") == 0) {
        iOperatorRole1 = 6;
    }else if (strRole.compareTo("ca") == 0) {
        iOperatorRole1 = 1;
    }
    session.setAttribute("_apOprRole", "" + iOperatorRole1);

    if (strRole.compareTo("sysadmin") == 0) {
%>

<jsp:include page="channels.jsp" /> 
<%--<%@include file="channels.jsp"
     
%>--%>
<%
    return;
} else if (strRole.compareTo("admin") == 0) {%>
<%--<%@include file="mychannel.jsp" %>--%>
<jsp:include page="mychannel.jsp" /> 
<% 
    return;
} else if (strRole.compareTo("helpdesk") == 0) {%>
<%--<%@include file="users.jsp" %>--%>
<jsp:include page="users.jsp" /> 
<%  
    return;
} else if (strRole.compareTo("reporter") == 0) {%>
<%--<%@include file="msgreportmain.jsp" %>--%>
<jsp:include page="msgreportmain.jsp" /> 
<%  
    return;
} else if (strRole.compareTo("requester") == 0) {%>
<%--<%@include file="otptokens.jsp" %>--%>
<jsp:include page="otptokens.jsp" /> 
<%
    
    return;
} else if (strRole.compareTo("authorizer") == 0) {%>
<%--<%@include file="requesterOperators.jsp" %>--%>
<jsp:include page="requesterOperators.jsp" /> 
<%
        return;
    }else if (strRole.compareTo("ca") == 0) {%>
<%--<%@include file="kycpendingrequest.jsp" %>--%>
<jsp:include page="kycpendingrequest.jsp" /> 
<%

        return;
    }%>
    
  
    