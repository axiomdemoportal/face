<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/jquery.js"></script>
<script src="./assets/js/usermanagement.js"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">
<link rel="stylesheet" href="./assets/css/datepicker.css">
<script src="./assets/js/bootstrap-datepicker.js"></script>
<div class="container-fluid">
    <h1 class="text-success">Role Reports</h1>
    <h3>Make Your Own Report</h3>
    <input type="hidden" id="_changeStatus" name="_changeStatus">

    <div class="row-fluid">
        <div class="span12">
            <div class="control-group form-inline">                
                <div class="input-append">
                    <span class="add-on">From:</span>   
                    <div id="datetimepicker1" class="input-append date">
                        <input id="userstartdate" name="userstartdate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth">
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>

                </div>
                <div class="input-append">
                    <span class="add-on">To:</span>   
                    <div id="datetimepicker2" class="input-append date">
                        <input id="userenddate" name="userenddate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth">
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                </div>    
                <div class="input-prepend">
                    <span class="add-on " >Enter </span><input id="_searchtext" name="_searchtext" class="" type="text" placeholder="Role name" data-bind="value: vm.ActualDoorSizeDepth" />
                </div>
                <div class="input-append">
                    <%if (oprObj.getRoleid() != 1) {%>
                    <%if (accessObj != null && accessObj.viewRoleRports == true) {%>
                    <button class="btn btn-success " onclick="searchRoleReport()" type="button">Search Now</button>
                    <%} else {%>
                    <button class="btn btn-success " onclick="InvalidRequest('rolereports')" type="button">Search Now</button>
                    <%}
                    } else {%>
                    <button class="btn btn-success " onclick="searchRoleReport()" type="button">Search Now</button>
                    <%}%>
                </div>

            </div>
        </div>
    </div>



    <div id="licenses_data_table">

    </div>
    <script type="text/javascript">
        ChangeStatusType(99);
    </script>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            });
        });
        $(function () {
            $('#datetimepicker2').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'

            });
        });

    </script>
</div>

<%@include file="footer.jsp" %>
