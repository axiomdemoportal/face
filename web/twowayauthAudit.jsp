<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TxManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Txdetails"%>
<%@page import="com.mollatech.axiom.nucleus.db.Audit"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.AuditManagement"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="java.text.SimpleDateFormat"%>


<%
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _startdate = request.getParameter("_startdate");
    String _enddate = request.getParameter("_enddate");
    String _auditUserID = request.getParameter("_auditUserID");
    String _auditUserName = request.getParameter("_auditUserName");
    DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    Date startDate = null;
    if (_startdate != null && !_startdate.isEmpty()) {
        startDate = (Date) formatter.parse(_startdate);
        startDate.setHours(0);
        startDate.setMinutes(0);
        startDate.setSeconds(0);
    }
    Date endDate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        endDate = (Date) formatter.parse(_enddate);
        endDate.setHours(23);
        endDate.setMinutes(59);
        endDate.setSeconds(59);
    }
    Txdetails[] txdetailses = new TxManagement().getAllTxdetailsByUserIdAndDate(channel.getChannelid(), _auditUserID, startDate, endDate);
    session.setAttribute("txdetailses", txdetailses);
    String strmsg = "No Record Found";

    SimpleDateFormat tz1 = new SimpleDateFormat("EEE dd MMM");
%>
<h3> Results from <%=tz1.format(startDate)%> to <%=tz1.format(endDate)%> for <%=_auditUserName%> </i></h3>
<div class="row-fluid">
    <div class="span6">
        <div class="control-group">                        
            <div class="span1">
                <div class="control-group form-inline">
                    <a href="#" class="btn btn-info" onclick="downloadtxReport(1,'<%=_auditUserName%>','<%=_startdate %>','<%=_enddate %>')" >
                        <i class="icon-white icon-chevron-down"></i> CSV</a>
                </div>
            </div>
            <div class="span1">
                <div class="control-group form-inline">
                    <a href="#" class="btn btn-info" onclick="downloadtxReport(0,'<%=_auditUserName%>','<%=_startdate %>','<%=_enddate %>')" >
                        <i class="icon-white icon-chevron-down"></i> PDF</a>
                </div>
            </div>
            <div class="span1">
                <div class="control-group form-inline">
                    <a href="#" class="btn btn-info" onclick="downloadtxReport(2,'<%=_auditUserName%>','<%=_startdate %>','<%=_enddate %>')" >
                        <i class="icon-white icon-chevron-down"></i> TEXT</a>
                </div>
            </div>
        </div>
    </div>
    <table class="table table-striped" >
        <tr>
            <td>No.</td>
            <td>Tx. Id</td>
            <td>Type</td>
            <td>Devide Id</td>
            <td>Message</td>
            <td>Status</td>
            <td>Question Asked</td>
            <td>QnA Status</td>
            <td>Created On</td>
            <td>Updated On</td>
            <td>Expired On</td>
        </tr>
        <%    if (txdetailses != null) {

                for (int i = 0; i < txdetailses.length; i++) {
                    String type = "PUSH";
                    String deviceId = "NA";
                    String strStatus = "APPROVED";
                    String questionAsked = "NA";
                    String updatedOn = "NA";
                    String QnAStatus = "APPROVED";
                    if (txdetailses[i].getType() == TxManagement.SMS) {
                        type = "SMS";
                    } else if (txdetailses[i].getType() == TxManagement.VOICE) {
                        type = "VOICE";
                    } else if (txdetailses[i].getType() == TxManagement.MISSEDCALL) {
                        type = "MISSEDCALL";
                    }
                    if (txdetailses[i].getDeviceid() != null) {
                        deviceId = txdetailses[i].getDeviceid();
                    }
                    if (txdetailses[i].getStatus() == TxManagement.CANCELED) {
                        strStatus = "CANCELED";
                    } else if (txdetailses[i].getStatus() == TxManagement.DENIED) {
                        strStatus = "DENIED";
                    } else if (txdetailses[i].getStatus() == TxManagement.EXPIRED) {
                        strStatus = "EXPIRED";
                    } else if (txdetailses[i].getStatus() == TxManagement.PENDING) {
                        strStatus = "PENDING";
                    } else if (txdetailses[i].getStatus() == TxManagement.RESPONDED) {
                        strStatus = "RESPONDED";
                    }
                    if (txdetailses[i].getQuestion() != null) {
                        questionAsked = "YES";
                    }
                    if (txdetailses[i].getQnAStatus() == TxManagement.CANCELED) {
                        QnAStatus = "CANCELED";
                    } else if (txdetailses[i].getQnAStatus() == TxManagement.DENIED) {
                        QnAStatus = "DENIED";
                    } else if (txdetailses[i].getQnAStatus() == TxManagement.EXPIRED) {
                        QnAStatus = "EXPIRED";
                    } else if (txdetailses[i].getQnAStatus() == TxManagement.PENDING) {
                        QnAStatus = "PENDING";
                    } else if (txdetailses[i].getQnAStatus() == TxManagement.RESPONDED) {
                        QnAStatus = "RESPONDED";
                    }
                    if (txdetailses[i].getUpdatedOn() != null) {
                        updatedOn = txdetailses[i].getUpdatedOn().toString();
                    }
                    if (questionAsked.equals("NA")) {
                        QnAStatus = "NA";
                    }
        %>
        <tr>
            <td><%=i + 1%></td>
            <td><%=txdetailses[i].getTransactionId()%></td>
            <td><%=type%></td>
            <td><%=deviceId%></td>
            <td>
                <a href="#/" class="btn btn-mini" id="twoWayMsg-<%=i%>"  rel="popover" data-html="true">Click to View</a>
                <script>
                    $(function ()
                    {
                        $("#twoWayMsg-<%=i%>").popover({title: 'Message', content: "<%=txdetailses[i].getTxmsg()%>"});
                    });
                </script>
            </td>
            <td><%=strStatus%></td>
            <td><%=questionAsked%></td>
            <td><%=QnAStatus%></td>
            <td><%=txdetailses[i].getCreatedOn()%></td>
            <td><%= updatedOn%></td>
            <td><%=txdetailses[i].getExpiredOn()%></td>
        </tr>
        <%}
        } else {%>
        <tr>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>

        </tr>
        <%}%>
    </table>
    <div class="row-fluid">
        <div class="span6">

            <div class="control-group">                        
                <div class="span1">
                    <div class="control-group form-inline">
                        <a href="#" class="btn btn-info" onclick="downloadtxReport(1,'<%=_auditUserName%>','<%=_startdate %>','<%=_enddate %>')" >
                            <i class="icon-white icon-chevron-down"></i> CSV</a>
                    </div>
                </div>
                <div class="span1">
                    <div class="control-group form-inline">
                        <a href="#" class="btn btn-info" onclick="downloadtxReport(0,'<%=_auditUserName%>','<%=_startdate %>','<%=_enddate %>')" >
                            <i class="icon-white icon-chevron-down"></i> PDF</a>
                    </div>
                </div>
                <div class="span1">
                    <div class="control-group form-inline">
                        <a href="#" class="btn btn-info" onclick="downloadtxReport(2,'<%=_auditUserName%>','<%=_startdate %>','<%=_enddate %>')" >
                            <i class="icon-white icon-chevron-down"></i> TEXT</a>
                    </div>
                </div>


            </div>
        </div>
    </div> 

</div>
