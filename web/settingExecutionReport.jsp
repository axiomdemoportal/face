<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SchedulerManagement"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.AxiomProtect"%>
<%@page import="com.mollatech.axiom.nucleus.settings.NucleusMonitorSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.Monitorsettings"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.axiom.nucleus.db.Monitortracking"%>
<script src="./assets/js/addSettings.js"></script>
<%! public String ChechFor = "";%> 
<%  Channels _apSChannelDetails = (Channels) session.getAttribute("_apSChannelDetails");
    String _settingName = request.getParameter("_settingName");
    int type = Integer.parseInt(request.getParameter("_type"));
    String _start=request.getParameter("_startdate");
    String _end=request.getParameter("_enddate");    
%>
<div class="tabbable" style="margin-left: 15px; alignment-adjust: central">      
    <ul class="nav nav-tabs">
        <li class="active"><a onclick="chart('<%=_settingName%>', '<%=type%>','<%=_start%>','<%=_end%>')" data-toggle="tab">Historical report</a></li>
        <li><a onclick="textualreport('<%=_settingName%>', '<%=type%>','<%=_start%>','<%=_end%>')" data-toggle="tab">Tabular Report</a></li> 
        <li><a onclick="realtime('<%=_settingName%>', '<%=type%>','<%=_start%>','<%=_end%>')" data-toggle="tab">Real Time Report</a></li> 
    </ul>                    
</div>
      <div id="tabDs" ></div>
      <div id="tabchart" style="display: none"></div>
    <div id="tabDetails" style="display: none"></div>
  
    <script>      
        chart('<%=_settingName%>', '<%=type%>','<%=_start%>','<%=_end%>');
    </script>