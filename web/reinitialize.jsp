<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Properties"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.AxiomProtect"%>
<%@include file="header.jsp" %>
<%
    String _sessionID = (String) session.getAttribute("_apSessionID");

    SessionManagement smObj = new SessionManagement();
    int iStatus = smObj.GetSessionStatus(_sessionID);
    AxiomChannel[] channelsObj = null;
    if (iStatus == 1) { //active
        smObj.UpdateSession(_sessionID);
        ChannelManagement cmObj = new ChannelManagement();
        channelsObj = cmObj.ListChannelsInternal();
        cmObj = null;
    }
%>
<%
    Properties p = AxiomProtect.LicenseDetails();
    
%>
<div class="container-fluid">
    <h1 class="text-success">Change Security Credentials</h1>
            <hr>
            <div class="row-fluid">
                <div class="span4">
                    <form class="form-signin" id="formOffice1" name="formOffice1">
                        <h2 class="form-signin-heading">Officer 1</h2>
                        <input type="hidden" id="type" name="type" value="1">
                        <input type="password" id="officer1passwordO" name="officer1passwordO" class="input-block-level" placeholder="Current Password">
                        <input type="password" id="officer1password" name="officer1password" class="input-block-level" placeholder="New Password">
                        <input type="password" id="officer1passwordC" name="officer1passwordC" class="input-block-level" placeholder="Confirm Password">
                        <button id="officer1button" class="btn btn-primary" type="button" onclick="ChangeOfficerCredential(1)">Save Now</button>
                    </form>
                </div>
                <div class="span4">
                    <form class="form-signin" id="formOffice2" name="formOffice2">
                        <h2 class="form-signin-heading">Officer 2</h2>
                        <input type="hidden" id="type" name="type" value="2">
                        <input type="password" id="officer2passwordO" name="officer2passwordO" class="input-block-level" placeholder="Current Password">
                        <input type="password" id="officer2password" name="officer2password" class="input-block-level" placeholder="New Password">
                        <input type="password" id="officer2passwordC" name="officer2passwordC" class="input-block-level" placeholder="Confirm Password">
                        <button id="officer2button" class="btn  btn-primary" type="button" onclick="ChangeOfficerCredential(2)">Save Now</button>
                    </form>
                </div>
                <div class="span4">
                    <form class="form-signin" id="formOffice3" name="formOffice3">
                        <h2 class="form-signin-heading">Officer 3</h2>
                        <input type="hidden" id="type" name="type" value="3">
                        <input type="password" id="officer3passwordO" name="officer3passwordO" class="input-block-level" placeholder="Current Password">
                        <input type="password" id="officer3password" name="officer3password" class="input-block-level" placeholder="New Password">
                        <input type="password" id="officer3passwordC" name="officer3passwordC" class="input-block-level" placeholder="Confirm Password">
                        <button id="officer3button" class="btn  btn-primary" type="button" onclick="ChangeOfficerCredential(3)">Save Now</button>
                    </form>
                </div>
            </div>
            <p class="lead">Click below button to re-generate security credential from above and bind it to this device.
                <br>   
            <form class="form-horizontal" id="formConfirm" name="formConfirm">
                <input type="hidden" id="type" name="type" value="0">
                <button id="GenerateSecurityCredentialsButton" class="btn btn-large btn-success" type="button" onclick="ChangeOfficerCredential(0)">Change Security Credentials</button>                
            </form>
        </div>
        <script src="./assets/js/reinitialize.js"></script>


<%@include file="footer.jsp" %>