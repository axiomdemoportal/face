<%@page import="com.mollatech.axiom.nucleus.db.Usergroups"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserGroupsManagement"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();
    String _searchtext = request.getParameter("_searchtext");

    UserManagement usermngObj = new UserManagement();
    AuthUser Users[] = null;
    Users = usermngObj.SearchUsers(sessionId, _channelId, _searchtext);
    if(Users==null)
    {
         Users = usermngObj.SearchUsersByID(sessionId, _channelId, _searchtext);
    }
    if (Users != null) {
      
%>
<h3>Results for <i>"<%=_searchtext%>"</i> in User & Password</h3>

<table class="table table-striped" id="table_main">

    <tr>
        <td>No.</td>
        <td>User ID</td>
        <td>Name</td>
        <td>Mobile</td>
        <td>Email</td>
        <td>Status</td>
        <td>User Group</td>
        <td>Details</td>
        <td>Password</td>
        <td>Attempts</td>
        <td>Mobile Trust</td>
        <td>Audit</td>
        <td>Created On</td>
        <td>Last Access On</td>
    </tr>

    <%
        //if (Users != null) {
        for (int i = 0; i < Users.length; i++) {

            java.util.Date dCreatedOn = new java.util.Date(Users[i].lCreatedOn);
            java.util.Date dLastUpdated = new java.util.Date(Users[i].lLastAccessOn);

            SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm");

            String strStatus = "Inactive";

            if (Users[i].getStatePassword() == 1) {

                strStatus = "Active";

            } else if (Users[i].getStatePassword() == 0) {

                strStatus = "Suspended";

            }
            String groupName = "Not-Available";
            UserGroupsManagement uManagement = new UserGroupsManagement();
            if (Users[i].groupid != 0) {

                Usergroups uGroups = uManagement.getGroupByGroupId(sessionId, channel.getChannelid(), Users[i].groupid);
                if (uGroups != null) {
                    groupName = uGroups.getGroupname();
                }

            }

            /*String strStatus=null;
                
             if(Users[i].getStatePassword()==1)
             {
                  
             strStatus="Active";
                
             }
             else if(Users[i].getStatePassword()==-1)
             {
                  
             strStatus="Suspended";
                
             }*/
            String userStatus = "user-status-value-" + i;
            String uidiv4OprRole = "operator-role-value-" + i;

    %>
    <tr id="user_search_<%=Users[i].getUserId()%>">
        <td><%=(i + 1)%></td>
         <td><a href="#" class="btn btn-mini" onclick="viewUserID('<%=Users[i].getUserId()%>')" >View ID</a></td>

        <td><%=Users[i].getUserName()%></td>
        <td><%=Users[i].getPhoneNo()%></td>
        <td><%=Users[i].getEmail()%></td>
        <td>
            <div class="btn-group">

                <button class="btn btn-mini" id="<%=userStatus%>"><%=strStatus%></button>
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#"  onclick="changeuserstatus(1, '<%=Users[i].getUserId()%>', '<%=userStatus%>')" >Mark as Active?</a></li>
                    <li><a href="#" onclick="changeuserstatus(0, '<%=Users[i].getUserId()%>', '<%=userStatus%>')" >Mark as Suspended?</a></li>
                    <li class="divider"></li>
                    <li><a href="#editUser" onclick="loadUserDetails('<%=Users[i].getUserId()%>')" data-toggle="modal">Edit Details</a></li>
                    <li><a href="#" onclick="removeUser('<%=Users[i].getUserId()%>')" data-toggle="modal"><font color="red">Remove?</font></a></li>
                </ul>
            </div>
        </td>
        <!--<td><%=groupName%></td>-->
        <td>
            <div class="btn-group">
                <button class="btn btn-mini" id="<%=uidiv4OprRole%>"><%=groupName%></button>
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <%
                        Usergroups[] roles1 = uManagement.ListGroups(sessionId, channel.getChannelid());
                        for (int j = 0; j < roles1.length; j++) {

                    %>

                    <li><a href="#" onclick="changeusergroup('<%=roles1[j].getGroupid()%>' ,'<%=Users[i].getUserId()%>','<%=roles1[j].getGroupname()%>','<%=uidiv4OprRole%>')"><%=roles1[j].getGroupname()%></a></li>


                    <%  }
                    %>
                </ul>
            </div>
        </td>
        <td>
            <div class="btn-group">
                <a href="#userDetails" class="btn btn-mini" onclick="getUserDetails('<%=Users[i].getUserId()%>')" data-toggle="modal" >Details</a>
            </div>
        </td>

        <td>
            <div class="btn-group">
                <button class="btn btn-mini">*****</button>
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="changeunlockpassword(1, '<%=Users[i].getUserId()%>', '<%=userStatus%>')">Unlock Password</a></li>
                    <li><a href="#" onclick="resendpassword('<%=Users[i].getUserId()%>')" >Re(send) Password (via Mobile)</a></li>
                    <li class="divider"></li>
                    <li><a href="#" onclick="resetpassword('<%=Users[i].getUserId()%>')" >Set Random Password</a></li>
                    <li><a href="#" onclick="resetandsendpassword('<%=Users[i].getUserId()%>')" >Set & Send Random Password (via Mobile)</a></li>

                </ul>
            </div>
        </td>      
        <td><%=Users[i].iAttempts%></td>
        <td><button class="btn btn-sm btn-danger" onclick="removemobiletrustUser('<%=Users[i].getUserId()%>')" data-toggle="modal">Erase</button></td>

        <td>
            <a href="#" class="btn btn-mini" onclick="loadUserPasswordDetails('<%=Users[i].getUserId()%>', '<%=Users[i].getUserName()%>')">Download</a>
        </td>
        <!--        <td>
                    <div class="btn-group">
                        <button class="btn btn-mini">Audit for</button>
                        <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="passwordAudits('<%=Users[i].getUserId()%>', 'USERPASSWORD', '1day')" >Today</a></li>
                            <li><a href="#" onclick="passwordAudits('<%=Users[i].getUserId()%>', 'USERPASSWORD', '7days')" >Last 7 days</a></li>
                            <li class="divider"></li>
                            <li><a href="#" onclick="passwordAudits('<%=Users[i].getUserId()%>', 'USERPASSWORD', '1month')">Last 30 days</a></li>
                            <li><a href="#" onclick="passwordAudits('<%=Users[i].getUserId()%>', 'USERPASSWORD', '2months')">Last 2 months</a></li>
                            <li><a href="#" onclick="passwordAudits('<%=Users[i].getUserId()%>', 'USERPASSWORD', '3months')" >Last 3 months</a></li>
                            <li><a href="#" onclick="passwordAudits('<%=Users[i].getUserId()%>', 'USERPASSWORD', '6months')">Last 6 months</a></li>
                        </ul>
                    </div>
                </td>-->
        <td><%=sdf.format(dCreatedOn)%></td>
        <td><%=sdf.format(dLastUpdated)%></td>
    </tr>
    <% } %>        
</table>
<%  } else if (Users == null) { %>
<h3>No users found...</h3>
<%}%>

<br><br>
