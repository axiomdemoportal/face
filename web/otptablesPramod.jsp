<%@page import="com.mollatech.axiom.connector.access.controller.AccessMatrixSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.Operators"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.mollatech.axiom.nucleus.settings.TokenSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement"%>
<%@page import="com.mollatech.axiom.connector.user.TokenStatusDetails"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.Otptokens"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%

    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();
    String _searchtext = request.getParameter("_searchtext");
    UserManagement usermngObj = new UserManagement();
    AuthUser Users[] = null;
    Users = usermngObj.SearchUsers(sessionId, _channelId, _searchtext);
    request.getSession().setAttribute("_apOTPSearchText", _searchtext);

    String strOperatorRoleN = (String) session.getAttribute("_apOprRole");
    int iOperatorRoleN = Integer.valueOf(strOperatorRoleN).intValue();

    SettingsManagement setObj = new SettingsManagement();
    Object settingsObj = setObj.getSetting(sessionId, _channelId, SettingsManagement.Token, SettingsManagement.PREFERENCE_ONE);

    TokenSettings tokenObj = null;
    boolean bAddSetting = false;
    Operators oprObj = (Operators) session.getAttribute("_apOprDetail");
    if (settingsObj != null) {
        tokenObj = (TokenSettings) settingsObj;
    }
        int subtype = -1;
        int subtypeSwtoken = -1;
    AccessMatrixSettings accessObj = (AccessMatrixSettings) session.getAttribute("_apAccessEntry");
    if (Users != null) {

%>
<h3>Results for <i>"<%=_searchtext%>"</i></h3>
<%if (oprObj.getRoleid() != 1 && accessObj != null) {//not sysadmin%>
<table class="table table-striped" id="table_main">

    <tr>
        <td>No.</td>
        <td>Name</td>
        <td>Mobile</td>
        <td>Email</td>
        <td>Attempts</td>
        <%if (accessObj.listOOBEMAILToken == true || accessObj.listOOBSMSToken == true || accessObj.listOOBUSSDToken == true || accessObj.listOOBVOICEToken == true) {%>
        <td>Out Of Band Token</td>
        <%}%>
        <% if (accessObj.listSWMOBILEToken == true || accessObj.listSWPCToken == true || accessObj.listSWWEBToken == true) {%>
        <td>Software Token</td>
        <%}%>
        <% if (accessObj.listHWOTPToken == true) {%>
        <td>Hardware Token</td>
        <%}%>
        <td>Audit</td>
        <td>Last Access On</td>
    </tr>

    <%
        if (Users != null)
         
            for (int i = 0; i < Users.length; i++) {

                String strLabelOOB = "label";    //unassigned
                String strLabelSW = "label";    //unassigned
                String strLabelHW = "label";    //unassigned

                java.util.Date dLastUpdated = new java.util.Date(Users[i].lLastAccessOn);
                java.util.Date dsoftUpdated = new java.util.Date(Users[i].lLastAccessOn);
                java.util.Date dhardUpdated = new java.util.Date(Users[i].lLastAccessOn);
                java.util.Date dOOBUpdated = new java.util.Date(Users[i].lLastAccessOn);

//                java.util.Date dCreatedOn = new java.util.Date(Users[i].lCreatedOn);
//                java.util.Date dLastUpdated = new java.util.Date(Users[i].lLastAccessOn);
                SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm ");
                String strStatus = null;
                TokenStatusDetails tokendetails[] = null;

                String softstatus = "Unassigned";
                String hardstatus = "Unassigned";
                String OOBstatus = "Unassigned";
                String OOBtype = "---";
                String SOBtype = "---";
                String serialNumbersoft = "------";
                String serialNumberhard = "------";
                String serialNumberoob = "";
                int attempt = 0;
                int attemptS = 0;
                int attemptH = 0;
                int attemptO = 0;

                OTPTokenManagement OtpObj = new OTPTokenManagement(_channelId);

                tokendetails = OtpObj.getTokenList(sessionId, _channelId, Users[i].getUserId());

                if (tokendetails != null) {
                    TokenStatusDetails tokenDetailsOfOOB = null;
                    TokenStatusDetails tokenDetailsOfSoft = null;
                    TokenStatusDetails tokenDetailsOfHard = null;

                    for (int j = 0; j < tokendetails.length; j++) {

                        if (tokendetails[j].Catrgory == OTPTokenManagement.SOFTWARE_TOKEN) {

                            tokenDetailsOfSoft = tokendetails[j];
                            if (tokenDetailsOfSoft.serialnumber != null) {
                                serialNumbersoft = tokenDetailsOfSoft.serialnumber;
                            }

                            dsoftUpdated = new java.util.Date(tokenDetailsOfSoft.lastaccessOn);
                        } else if (tokendetails[j].Catrgory == OTPTokenManagement.HARDWARE_TOKEN) {
                            tokenDetailsOfHard = tokendetails[j];
//                           
                            if (tokenDetailsOfHard.serialnumber != null) {
                                serialNumberhard = tokenDetailsOfHard.serialnumber;
                            }
                            dhardUpdated = new java.util.Date(tokenDetailsOfHard.lastaccessOn);
                        } else if (tokendetails[j].Catrgory == OTPTokenManagement.OOB_TOKEN) {
                            tokenDetailsOfOOB = tokendetails[j];
//                            
                            if (tokenDetailsOfOOB.serialnumber != null) {
                                serialNumberoob = tokenDetailsOfOOB.serialnumber;
                            }
                            dOOBUpdated = new java.util.Date(tokenDetailsOfOOB.lastaccessOn);
                        }

                        if (dsoftUpdated.after(dhardUpdated) && dsoftUpdated.after(dOOBUpdated)) {

                            dLastUpdated = dsoftUpdated;

                        } else if (dhardUpdated.after(dsoftUpdated) && dhardUpdated.after(dOOBUpdated)) {

                            dLastUpdated = dhardUpdated;

                        } else if (dOOBUpdated.after(dsoftUpdated) && dsoftUpdated.after(dhardUpdated)) {

                            dLastUpdated = dOOBUpdated;

                        }
                        if (tokenDetailsOfOOB != null) {
                            attemptO = tokenDetailsOfOOB.Attempts;
                        }
                        if (tokenDetailsOfSoft != null) {
                            attemptS = tokenDetailsOfSoft.Attempts;
                        }
                        if (tokenDetailsOfHard != null) {
                            attemptH = tokenDetailsOfHard.Attempts;
                        }

                        attempt = attemptO + attemptS + attemptH;

                    }
                   
                    if (tokenDetailsOfSoft != null) {

                        if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                            strLabelSW += " label-success";   //active
                            softstatus = "Active";
                        } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
                            strLabelSW += " label-warning";   //locked
                            softstatus = "Locked";
                        } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                            strLabelSW += " label-info";   //assigned                
                            softstatus = "Assigned";

                        } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                            softstatus = "Unassigned";

                        } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
                            strLabelSW += " label-important";   //suspended
                            softstatus = "suspended";
                        }

                        if (tokenDetailsOfSoft.SubCategory == OTPTokenManagement.SW_WEB_TOKEN) {
                            SOBtype = "WEB(" + serialNumbersoft + ")";
                            subtypeSwtoken = 1;
                        }
                        if (tokenDetailsOfSoft.SubCategory == OTPTokenManagement.SW_MOBILE_TOKEN) {
                            SOBtype = "MOBILE(" + serialNumbersoft + ")";
                            subtypeSwtoken = 2;
                        }
                        if (tokenDetailsOfSoft.SubCategory == OTPTokenManagement.SW_PC_TOKEN) {
                            subtypeSwtoken = 3;
                            SOBtype = "PC(" + serialNumbersoft + ")";

                        }
                    }
                    if (tokenDetailsOfHard != null) {

                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                            hardstatus = "Active";
                            strLabelHW += " label-success";   //active

                        }
                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
                            hardstatus = "Locked";
                            strLabelHW += " label-warning";   //locked

                        }
                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                            hardstatus = "Assigned";
                            strLabelHW += " label-info";   //assigned                

                        }
                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                            hardstatus = "Unassigned";

                        }
                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
                            hardstatus = "suspended";
                            strLabelHW += " label-important";   //suspended

                        }
                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_LOST) {
                            hardstatus = "Lost";
                            strLabelHW += " label-important";   //suspended

                        }

                    }

                    if (tokenDetailsOfOOB != null) {

                        if (tokenDetailsOfOOB.Status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                            strLabelOOB += " label-success";   //active

                            OOBstatus = "Active";
                        }
                        if (tokenDetailsOfOOB.Status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {

                            strLabelOOB += " label-warning";   //locked

                            OOBstatus = "Locked";

                        }
                        if (tokenDetailsOfOOB.Status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                            strLabelOOB += " label-info";   //assigned                

                            OOBstatus = "Assigned";

                        }
                        if (tokenDetailsOfOOB.Status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {

                            OOBstatus = "Unassigned";

                        }
                        if (tokenDetailsOfOOB.Status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
                            strLabelOOB += " label-important";   //suspended
                            OOBstatus = "suspended";
                        }

                        if (tokenDetailsOfOOB.SubCategory == OTPTokenManagement.OOB__SMS_TOKEN) {
                            OOBtype = "SMS";
                            subtype = 1;
                        }
                        if (tokenDetailsOfOOB.SubCategory == OTPTokenManagement.OOB__VOICE_TOKEN) {
                            OOBtype = "VOICE";
                            subtype = 2; 
                        }
                        if (tokenDetailsOfOOB.SubCategory == OTPTokenManagement.OOB__USSD_TOKEN) {
                            subtype = 3; 
                            OOBtype = "USSD";

                        }
                        if (tokenDetailsOfOOB.SubCategory == OTPTokenManagement.OOB__EMAIL_TOKEN) {
                            OOBtype = "EMAIL";
                            subtype = 4; 
                        }
                    }

                }
                String userStatus = "user-status-value-" + i;

                String otpType = "otp-type-value-" + i;

                String otpStatus = "otp-status-value-" + i;

                String sotpStatus = "sotp-status-value-" + i;

                String hotpStatus = "hotp-status-value-" + i;

                String sotpType = "sotp-type-value-" + i;

                String hotpType = "hotp-type-value-" + i;


    %>
    <tr id="user_search_<%=Users[i].getUserId()%>">
        <td><%=(i + 1)%></td>
        <td><%=Users[i].getUserName()%></td>
        <td><%=Users[i].getPhoneNo()%></td>
        <td><%=Users[i].getEmail()%></td>
        <td><%=attempt%></td>
        <%if (accessObj != null) {
                if (accessObj.listOOBEMAILToken == true || accessObj.listOOBSMSToken == true || accessObj.listOOBUSSDToken == true || accessObj.listOOBVOICEToken == true) {%>
        <td>
            <div class="btn-group">
                <span id="<%=otpStatus%>" class="<%=strLabelOOB%>"><%=OOBstatus%></span>
                <!--<button class="btn btn-mini">Not Assigned</button>-->
                <button class="btn btn-mini" id="<%=otpType%>" ><%=OOBtype%></button>                        
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li class="dropdown-submenu">
                        <a tabindex="-1" href="#">Assign Token</a>
                        <ul class="dropdown-menu">
                            <%if (accessObj.addOOBSMSToken == true) {%>
                            <li><a tabindex="-1" href="#" onclick="assigntokenOOBandSW('<%=Users[i].getUserId()%>', 3, 1, '<%=otpType%>', '<%=otpStatus%>')">SMS Token</a></li>
                                <%}
                                    if (accessObj.addOOBVOICEToken == true) {%>
                            <li><a tabindex="-1" href="#" onclick="assigntokenOOBandSW('<%=Users[i].getUserId()%>', 3, 2, '<%=otpType%>', '<%=otpStatus%>')">Voice Token</a></li>
                                <%}
                                    if (accessObj.addOOBUSSDToken == true) {%>
                            <li><a tabindex="-1" href="#" onclick="assigntokenOOBandSW('<%=Users[i].getUserId()%>', 3, 3, '<%=otpType%>', '<%=otpStatus%>')">USSD Token</a></li>
                                <%}
                                    if (accessObj.addOOBEMAILToken == true) {%>
                            <li><a tabindex="-1" href="#" onclick="assigntokenOOBandSW('<%=Users[i].getUserId()%>', 3, 4, '<%=otpType%>', '<%=otpStatus%>')">Email Token</a></li>
                                <%}%>
                        </ul>
                    </li>
                    <li class="dropdown-submenu">
                        <a tabindex="-1" href="#">Change Token Type</a>
                        <ul class="dropdown-menu">
                            <%if (accessObj.editOOBSMSToken == true) {%>
                            <li><a tabindex="-1" href="#" onclick="changetokenOOBandSW('<%=Users[i].getUserId()%>', 3, 1, '<%=otpType%>')">SMS Token</a></li>
                                <%}
                                    if (accessObj.editOOBVOICEToken == true) {%>
                            <li><a tabindex="-1" href="#" onclick="changetokenOOBandSW('<%=Users[i].getUserId()%>', 3, 2, '<%=otpType%>')">Voice Token</a></li>
                                <%}
                                    if (accessObj.editOOBUSSDToken == true) {%>
                            <li><a tabindex="-1" href="#" onclick="changetokenOOBandSW('<%=Users[i].getUserId()%>', 3, 3, '<%=otpType%>')">USSD Token</a></li>
                                <%}
                                    if (accessObj.editOOBEMAILToken == true) {%>
                            <li><a tabindex="-1" href="#" onclick="changetokenOOBandSW('<%=Users[i].getUserId()%>', 3, 4, '<%=otpType%>')">Email Token</a></li>
                                <%}%>
                        </ul>
                    </li>
                    <li class="divider"></li>
                        <%if (accessObj.editOOBSMSToken == true || accessObj.editOOBVOICEToken == true || accessObj.editOOBUSSDToken == true || accessObj.editOOBEMAILToken == true) {%>
                    <li><a href="#" onclick="changeotpstatus(1, '<%=Users[i].getUserId()%>', 3, <%=subtype%>, '<%=otpStatus%>')">Mark as Active</a></li>
                    <li><a href="#" onclick="changeotpstatus(-2, '<%=Users[i].getUserId()%>', 3, <%=subtype%>, '<%=otpStatus%>')">Mark as Suspended</a></li>
                        <%} %>
                    <li><a href="#"  onclick="LoadTestOTPUI('<%=Users[i].getUserId()%>', 3)">Verify One Time Password</a></li>
                    <li class="divider"></li>
                        <%if (accessObj.removeOOBSMSToken == true || accessObj.removeOOBVOICEToken == true || accessObj.removeOOBUSSDToken == true || accessObj.removeOOBEMAILToken == true) {%>
                    <li><a href="#" onclick="changeotpstatus(-10, '<%=Users[i].getUserId()%>', 3, 1, '<%=otpStatus%>')"><font color="red">Un-Assign Token</font></a></li>
                       <%}%>
                </ul>
            </div>
        </td>
        <%}
            }%>
        <%if (accessObj != null) {
                if (accessObj.listSWMOBILEToken == true || accessObj.listSWPCToken == true || accessObj.listSWWEBToken == true) {%>
        <td>
            <div class="btn-group">
                <span class="<%=strLabelSW%>" id="<%=sotpStatus%>"><%=softstatus%></span>
                <button class="btn btn-mini" id="<%=sotpType%>"><%=SOBtype%></button>
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li class="dropdown-submenu">
                        <a tabindex="-1" href="#">Assign Token</a>
                        <ul class="dropdown-menu">   
                            <% if (accessObj.addSWWEBToken == true) {%>
                            <li><a tabindex="-1" href="#" onclick="assigntokenOOBandSW('<%=Users[i].getUserId()%>', 1, 1, '<%=sotpType%>', '<%=sotpStatus%>')">Web Token</a></li>
                                <%}
                                    if (accessObj.addSWMOBILEToken == true) {%>
                            <li><a tabindex="-1" href="#" onclick="assigntokenOOBandSW('<%=Users[i].getUserId()%>', 1, 2, '<%=sotpType%>', '<%=sotpStatus%>')">Mobile Token</a></li>
                                <%}
                                    if (accessObj.addSWPCToken == true) {%>
                            <li><a tabindex="-1" href="#" onclick="assigntokenOOBandSW('<%=Users[i].getUserId()%>', 1, 3, '<%=sotpType%>', '<%=sotpStatus%>')">PC Token</a></li>                            
                                <%}%>
                        </ul>
                    </li>
                    <li><a href="#" onclick="sendregistration('<%=Users[i].getUserId()%>', 1, 1)">(Re)send Registration Code</a></li>
                    <li class="divider"></li>
                        <% if (accessObj.editSWWEBToken == true || accessObj.editSWMOBILEToken == true || accessObj.editSWPCToken == true) {%>
                    <li><a href="#" onclick="changeotpstatus(1, '<%=Users[i].getUserId()%>', 1, <%=subtypeSwtoken%>, '<%=sotpStatus%>')">Mark as Active</a></li>
                    <li><a href="#" onclick="changeotpstatus(-2, '<%=Users[i].getUserId()%>', 1,<%=subtypeSwtoken%>, '<%=sotpStatus%>')">Mark as Suspended</a></li>
                        <%}%>
                    <li class="divider"></li>
                    <li><a href="#"  onclick="LoadTestOTPUI('<%=Users[i].getUserId()%>', 1)">Verify One Time Password</a></li>
                    <!--<li><a href="#" onclick="loadresyncSW('<%=Users[i].getUserId()%>', '<%=serialNumbersoft%>', 1)">Resync Token</a></li>-->
                    <li class="divider"></li>
                        <% // if (accessObj.removeSWWEBToken == true || accessObj.removeSWMOBILEToken == true || accessObj.removeSWPCToken == true) {%>
                    <li><a href="#" onclick="changeotpstatus(-10, '<%=Users[i].getUserId()%>', 1, <%=subtypeSwtoken%>, '<%=sotpStatus%>')"><font color="red">Un-Assign Token</font></a></li>
                            <% //} %>
                </ul>
            </div>
        </td>
        <%}
            }%>
        <%if (accessObj != null) {
                if (accessObj.listHWOTPToken == true) {%>
        <td>
            <div class="btn-group">
                <span class="<%=strLabelHW%>" id="<%=hotpStatus%>"><%=hardstatus%></span>
                <button class="btn btn-mini" id="<%=hotpType%>"><%=serialNumberhard%></button>
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <% if (accessObj.addHWOTPToken == true) {%>
                    <li><a href="#" onclick="loadregisterHW('<%=Users[i].getUserId()%>', 2, 1)">Assign Token</a></li>
                        <%}%>
                    <!--<li><a href="#HardwareRegistration"  data-toggle="modal">Assign Token</a></li>-->
                    <% if (accessObj.editHWOTPToken == true) {%>
                    <li class="divider"></li>
                    <li><a href="#" onclick="changeotpstatus(1, '<%=Users[i].getUserId()%>', 2, 1, '<%=hotpStatus%>')">Mark as Active</a></li>
                    <li><a href="#" onclick="changeotpstatus(-2, '<%=Users[i].getUserId()%>', 2, 1, '<%=hotpStatus%>')">Mark as Suspended</a></li>                            
                        <% }%>
                    <li class="divider"></li>
                    <li><a href="#"  onclick="LoadTestOTPUI('<%=Users[i].getUserId()%>', 2)">Verify One Time Password</a></li>
                    <!--<li><a href="#" onclick="loadresyncHW('<%=Users[i].getUserId()%>', '<%=serialNumberhard%>', 2)">Resync Token</a></li>-->
                    <% if (accessObj.editHWOTPToken == true) {%>
                    <li><a href="#ResyncHardware" onclick="loadresyncHW('<%=Users[i].getUserId()%>', '<%=serialNumberhard%>', 2)"><font color="">Resync Token</font></a></li>
                    <li><a href="#" data-toggle="modal" onclick="LoadTestOTPForReplace('<%=Users[i].getUserId()%>', 2, 1)"><font color="">Replace Token</font></a></li>

                    <li><a href="#" onclick="setAlert()">PUK Code</a></li>

                    <li class="divider"></li>
                    <li><a href="#" onclick="changeotpstatus(-5, '<%=Users[i].getUserId()%>', 2, 1, '<%=hotpStatus%>')"><font color="red">Mark as Lost</font></a></li>
                            <%}%>
                            <% // if (accessObj.removeHWOTPToken == true) {%>
                    <li><a href="#" onclick="changeotpstatus(-10, '<%=Users[i].getUserId()%>', 2, 1, '<%=hotpStatus%>')"><font color="red">Un-Assign Token</font></a></li>                                                        
                            <% //}%>
                </ul>
            </div>
        </td>
        <%}
            }%>
        <%if (0 == 1) {%>
        <td><span class="label label-inverse">Not Available</span></td>
        <td><span class="label label-inverse">Not Available</span></td>
        <%}%>        

        <td>
            <a href="#" class="btn btn-mini" onclick="loadUserTokenAuditDetails('<%=Users[i].getUserId()%>', '<%=Users[i].getUserName()%>')">Audit Download</a>
        </td>
        <td><%=sdf.format(dLastUpdated)%></td>
    </tr>

    <%} %>
</table>
<%} else {%>   
<table class="table table-striped" id="table_main">

    <tr>
        <td>No.</td>
        <td>Name</td>
        <td>Mobile</td>
        <td>Email</td>
        <td>Attempts</td>
        <td>Out Of Band Token</td>
        <td>Software Token</td>
        <td>Hardware Token</td>
        <td>Audit</td>
        <td>Last Access On</td>
    </tr>

    <%
        if (Users != null)
            for (int i = 0; i < Users.length; i++) {

                String strLabelOOB = "label";    //unassigned
                String strLabelSW = "label";    //unassigned
                String strLabelHW = "label";    //unassigned

                java.util.Date dLastUpdated = new java.util.Date(Users[i].lLastAccessOn);
                java.util.Date dsoftUpdated = new java.util.Date(Users[i].lLastAccessOn);
                java.util.Date dhardUpdated = new java.util.Date(Users[i].lLastAccessOn);
                java.util.Date dOOBUpdated = new java.util.Date(Users[i].lLastAccessOn);

//                java.util.Date dCreatedOn = new java.util.Date(Users[i].lCreatedOn);
//                java.util.Date dLastUpdated = new java.util.Date(Users[i].lLastAccessOn);
                SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm ");
                String strStatus = null;
                TokenStatusDetails tokendetails[] = null;

                String softstatus = "Unassigned";
                String hardstatus = "Unassigned";
                String OOBstatus = "Unassigned";
                String OOBtype = "---";
                String SOBtype = "---";
                String serialNumbersoft = "------";
                String serialNumberhard = "------";
                String serialNumberoob = "";
                int attempt = 0;
                int attemptS = 0;
                int attemptH = 0;
                int attemptO = 0;

                OTPTokenManagement OtpObj = new OTPTokenManagement(_channelId);

                tokendetails = OtpObj.getTokenList(sessionId, _channelId, Users[i].getUserId());

                if (tokendetails != null) {
                    TokenStatusDetails tokenDetailsOfOOB = null;
                    TokenStatusDetails tokenDetailsOfSoft = null;
                    TokenStatusDetails tokenDetailsOfHard = null;

                    for (int j = 0; j < tokendetails.length; j++) {

                        if (tokendetails[j].Catrgory == OTPTokenManagement.SOFTWARE_TOKEN) {

                            tokenDetailsOfSoft = tokendetails[j];
                            if (tokenDetailsOfSoft.serialnumber != null) {
                                serialNumbersoft = tokenDetailsOfSoft.serialnumber;
                            }

                            dsoftUpdated = new java.util.Date(tokenDetailsOfSoft.lastaccessOn);
                        } else if (tokendetails[j].Catrgory == OTPTokenManagement.HARDWARE_TOKEN) {
                            tokenDetailsOfHard = tokendetails[j];
//                           
                            if (tokenDetailsOfHard.serialnumber != null) {
                                serialNumberhard = tokenDetailsOfHard.serialnumber;
                            }
                            dhardUpdated = new java.util.Date(tokenDetailsOfHard.lastaccessOn);
                        } else if (tokendetails[j].Catrgory == OTPTokenManagement.OOB_TOKEN) {
                            tokenDetailsOfOOB = tokendetails[j];
//                            
                            if (tokenDetailsOfOOB.serialnumber != null) {
                                serialNumberoob = tokenDetailsOfOOB.serialnumber;
                            }
                            dOOBUpdated = new java.util.Date(tokenDetailsOfOOB.lastaccessOn);
                        }

                        if (dsoftUpdated.after(dhardUpdated) && dsoftUpdated.after(dOOBUpdated)) {

                            dLastUpdated = dsoftUpdated;

                        } else if (dhardUpdated.after(dsoftUpdated) && dhardUpdated.after(dOOBUpdated)) {

                            dLastUpdated = dhardUpdated;

                        } else if (dOOBUpdated.after(dsoftUpdated) && dsoftUpdated.after(dhardUpdated)) {

                            dLastUpdated = dOOBUpdated;

                        }
                        if (tokenDetailsOfOOB != null) {
                            attemptO = tokenDetailsOfOOB.Attempts;
                        }
                        if (tokenDetailsOfSoft != null) {
                            attemptS = tokenDetailsOfSoft.Attempts;
                        }
                        if (tokenDetailsOfHard != null) {
                            attemptH = tokenDetailsOfHard.Attempts;
                        }

                        attempt = attemptO + attemptS + attemptH;

                    }
                    if (tokenDetailsOfSoft != null) {

                        if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                            strLabelSW += " label-success";   //active
                            softstatus = "Active";
                        } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
                            strLabelSW += " label-warning";   //locked
                            softstatus = "Locked";
                        } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                            strLabelSW += " label-info";   //assigned                
                            softstatus = "Assigned";

                        } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                            softstatus = "Unassigned";

                        } else if (tokenDetailsOfSoft.Status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
                            strLabelSW += " label-important";   //suspended
                            softstatus = "suspended";
                        }

                        if (tokenDetailsOfSoft.SubCategory == OTPTokenManagement.SW_WEB_TOKEN) {
                            SOBtype = "WEB(" + serialNumbersoft + ")";

                        }
                        if (tokenDetailsOfSoft.SubCategory == OTPTokenManagement.SW_MOBILE_TOKEN) {
                            SOBtype = "MOBILE(" + serialNumbersoft + ")";

                        }
                        if (tokenDetailsOfSoft.SubCategory == OTPTokenManagement.SW_PC_TOKEN) {

                            SOBtype = "PC(" + serialNumbersoft + ")";

                        }
                    }
                    if (tokenDetailsOfHard != null) {

                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                            hardstatus = "Active";
                            strLabelHW += " label-success";   //active

                        }
                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
                            hardstatus = "Locked";
                            strLabelHW += " label-warning";   //locked

                        }
                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                            hardstatus = "Assigned";
                            strLabelHW += " label-info";   //assigned                

                        }
                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                            hardstatus = "Unassigned";

                        }
                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
                            hardstatus = "suspended";
                            strLabelHW += " label-important";   //suspended

                        }
                        if (tokenDetailsOfHard.Status == OTPTokenManagement.TOKEN_STATUS_LOST) {
                            hardstatus = "Lost";
                            strLabelHW += " label-important";   //suspended

                        }

                    }

                    if (tokenDetailsOfOOB != null) {

                        if (tokenDetailsOfOOB.Status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
                            strLabelOOB += " label-success";   //active

                            OOBstatus = "Active";
                        }
                        if (tokenDetailsOfOOB.Status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {

                            strLabelOOB += " label-warning";   //locked

                            OOBstatus = "Locked";

                        }
                        if (tokenDetailsOfOOB.Status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                            strLabelOOB += " label-info";   //assigned                

                            OOBstatus = "Assigned";

                        }
                        if (tokenDetailsOfOOB.Status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {

                            OOBstatus = "Unassigned";

                        }
                        if (tokenDetailsOfOOB.Status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
                            strLabelOOB += " label-important";   //suspended
                            OOBstatus = "suspended";
                        }

                        if (tokenDetailsOfOOB.SubCategory == OTPTokenManagement.OOB__SMS_TOKEN) {
                            OOBtype = "SMS";

                        }
                        if (tokenDetailsOfOOB.SubCategory == OTPTokenManagement.OOB__VOICE_TOKEN) {
                            OOBtype = "VOICE";

                        }
                        if (tokenDetailsOfOOB.SubCategory == OTPTokenManagement.OOB__USSD_TOKEN) {

                            OOBtype = "USSD";

                        }
                        if (tokenDetailsOfOOB.SubCategory == OTPTokenManagement.OOB__EMAIL_TOKEN) {
                            OOBtype = "EMAIL";

                        }
                    }

                }
                String userStatus = "user-status-value-" + i;

                String otpType = "otp-type-value-" + i;

                String otpStatus = "otp-status-value-" + i;

                String sotpStatus = "sotp-status-value-" + i;

                String hotpStatus = "hotp-status-value-" + i;

                String sotpType = "sotp-type-value-" + i;

                String hotpType = "hotp-type-value-" + i;


    %>
    <tr id="user_search_<%=Users[i].getUserId()%>">
        <td><%=(i + 1)%></td>
        <td><%=Users[i].getUserName()%></td>
        <td><%=Users[i].getPhoneNo()%></td>
        <td><%=Users[i].getEmail()%></td>
        <td><%=attempt%></td>
        <td>
            <div class="btn-group">
                <span id="<%=otpStatus%>" class="<%=strLabelOOB%>"><%=OOBstatus%></span>
                <!--<button class="btn btn-mini">Not Assigned</button>-->
                <button class="btn btn-mini" id="<%=otpType%>" ><%=OOBtype%></button>                        
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li class="dropdown-submenu">
                        <a tabindex="-1" href="#">Assign Token</a>
                        <ul class="dropdown-menu">
                            <li><a tabindex="-1" href="#" onclick="assigntokenOOBandSW('<%=Users[i].getUserId()%>', 3, 1, '<%=otpType%>', '<%=otpStatus%>')">SMS Token</a></li>
                            <li><a tabindex="-1" href="#" onclick="assigntokenOOBandSW('<%=Users[i].getUserId()%>', 3, 2, '<%=otpType%>', '<%=otpStatus%>')">Voice Token</a></li>
                            <li><a tabindex="-1" href="#" onclick="assigntokenOOBandSW('<%=Users[i].getUserId()%>', 3, 3, '<%=otpType%>', '<%=otpStatus%>')">USSD Token</a></li>
                            <li><a tabindex="-1" href="#" onclick="assigntokenOOBandSW('<%=Users[i].getUserId()%>', 3, 4, '<%=otpType%>', '<%=otpStatus%>')">Email Token</a></li>
                        </ul>
                    </li>
                    <li class="dropdown-submenu">
                        <a tabindex="-1" href="#">Change Token Type</a>
                        <ul class="dropdown-menu">
                            <li><a tabindex="-1" href="#" onclick="changetokenOOBandSW('<%=Users[i].getUserId()%>', 3, 1, '<%=otpType%>')">SMS Token</a></li>
                            <li><a tabindex="-1" href="#" onclick="changetokenOOBandSW('<%=Users[i].getUserId()%>', 3, 2, '<%=otpType%>')">Voice Token</a></li>
                            <li><a tabindex="-1" href="#" onclick="changetokenOOBandSW('<%=Users[i].getUserId()%>', 3, 3, '<%=otpType%>')">USSD Token</a></li>
                            <li><a tabindex="-1" href="#" onclick="changetokenOOBandSW('<%=Users[i].getUserId()%>', 3, 4, '<%=otpType%>')">Email Token</a></li>
                        </ul>
                    </li>
                    <li class="divider"></li>
                    <li><a href="#" onclick="changeotpstatus(1, '<%=Users[i].getUserId()%>', 3, 1, '<%=otpStatus%>')">Mark as Active</a></li>
                    <li><a href="#" onclick="changeotpstatus(-2, '<%=Users[i].getUserId()%>', 3, 1, '<%=otpStatus%>')">Mark as Suspended</a></li>
                    <li><a href="#"  onclick="LoadTestOTPUI('<%=Users[i].getUserId()%>', 3)">Verify One Time Password</a></li>
                    <li class="divider"></li>
                    <li><a href="#" onclick="changeotpstatus(-10, '<%=Users[i].getUserId()%>', 3, 1, '<%=otpStatus%>')"><font color="red">Un-Assign Token</font></a></li>
                </ul>
            </div>
        </td>
        <%if (0 == 0) {%>
        <td>
            <div class="btn-group">
                <span class="<%=strLabelSW%>" id="<%=sotpStatus%>"><%=softstatus%></span>
                <button class="btn btn-mini" id="<%=sotpType%>"><%=SOBtype%></button>
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li class="dropdown-submenu">
                        <a tabindex="-1" href="#">Assign Token</a>
                        <ul class="dropdown-menu">                                                                                                          
                            <li><a tabindex="-1" href="#" onclick="assigntokenOOBandSW('<%=Users[i].getUserId()%>', 1, 1, '<%=sotpType%>', '<%=sotpStatus%>')">Web Token</a></li>
                            <li><a tabindex="-1" href="#" onclick="assigntokenOOBandSW('<%=Users[i].getUserId()%>', 1, 2, '<%=sotpType%>', '<%=sotpStatus%>')">Mobile Token</a></li>
                            <li><a tabindex="-1" href="#" onclick="assigntokenOOBandSW('<%=Users[i].getUserId()%>', 1, 3, '<%=sotpType%>', '<%=sotpStatus%>')">PC Token</a></li>                            
                        </ul>
                    </li>
                    <li><a href="#" onclick="sendregistration('<%=Users[i].getUserId()%>', 1, 1)">(Re)send Registration Code</a></li>
                    <li class="divider"></li>
                    <li><a href="#" onclick="changeotpstatus(1, '<%=Users[i].getUserId()%>', 1, 1, '<%=sotpStatus%>')">Mark as Active</a></li>
                    <li><a href="#" onclick="changeotpstatus(-2, '<%=Users[i].getUserId()%>', 1, 1, '<%=sotpStatus%>')">Mark as Suspended</a></li>
                    <li class="divider"></li>
                    <li><a href="#"  onclick="LoadTestOTPUI('<%=Users[i].getUserId()%>', 1)">Verify One Time Password</a></li>
                    <!--<li><a href="#" onclick="loadresyncSW('<%=Users[i].getUserId()%>', '<%=serialNumbersoft%>', 1)">Resync Token</a></li>-->
                    <li class="divider"></li>
                    <li><a href="#" onclick="changeotpstatus(-10, '<%=Users[i].getUserId()%>', 1, 1, '<%=sotpStatus%>')"><font color="red">Un-Assign Token</font></a></li>
                </ul>
            </div>
        </td>

        <td>
            <div class="btn-group">
                <span class="<%=strLabelHW%>" id="<%=hotpStatus%>"><%=hardstatus%></span>
                <button class="btn btn-mini" id="<%=hotpType%>"><%=serialNumberhard%></button>
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="loadregisterHW('<%=Users[i].getUserId()%>', 2, 1)">Assign Token</a></li>
                    <!--<li><a href="#HardwareRegistration"  data-toggle="modal">Assign Token</a></li>-->
                    <li class="divider"></li>
                    <li><a href="#" onclick="changeotpstatus(1, '<%=Users[i].getUserId()%>', 2, 1, '<%=hotpStatus%>')">Mark as Active</a></li>
                    <li><a href="#" onclick="changeotpstatus(-2, '<%=Users[i].getUserId()%>', 2, 1, '<%=hotpStatus%>')">Mark as Suspended</a></li>                            
                    <li class="divider"></li>
                    <li><a href="#"  onclick="LoadTestOTPUI('<%=Users[i].getUserId()%>', 2)">Verify One Time Password</a></li>
                    <!--<li><a href="#" onclick="loadresyncHW('<%=Users[i].getUserId()%>', '<%=serialNumberhard%>', 2)">Resync Token</a></li>-->
                    <li><a href="#ResyncHardware" onclick="loadresyncHW('<%=Users[i].getUserId()%>', '<%=serialNumberhard%>', 2)"><font color="">Resync Token</font></a></li>
                    <li><a href="#" data-toggle="modal" onclick="LoadTestOTPForReplace('<%=Users[i].getUserId()%>', 2, 1)"><font color="">Replace Token</font></a></li>
                    <li><a href="#" onclick="setAlert()">PUK Code</a></li>
                    <li class="divider"></li>
                    <li><a href="#" onclick="changeotpstatus(-5, '<%=Users[i].getUserId()%>', 2, 1, '<%=hotpStatus%>')"><font color="red">Mark as Lost</font></a></li>
                    <li><a href="#" onclick="changeotpstatus(-10, '<%=Users[i].getUserId()%>', 2, 1, '<%=hotpStatus%>')"><font color="red">Un-Assign Token</font></a></li>                                                        
                </ul>
            </div>
        </td>
        <%}%>        
        <%if (0 == 1) {%>
        <td><span class="label label-inverse">Not Available</span></td>
        <td><span class="label label-inverse">Not Available</span></td>
        <%}%>        

        <td>
            <a href="#" class="btn btn-mini" onclick="loadUserTokenAuditDetails('<%=Users[i].getUserId()%>', '<%=Users[i].getUserName()%>')">Audit Download</a>
        </td>
        <td><%=sdf.format(dLastUpdated)%></td>
    </tr>

    <%} %>
</table>
<%  }
} else if (Users
        == null) { %>
<h3>No users found...</h3>
<%}%>
<br><br>

