<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.axiom.nucleus.db.Audit"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.AuditManagement"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="java.text.SimpleDateFormat"%>

<%
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _startdate = request.getParameter("_startdate");
    String _enddate = request.getParameter("_enddate");
    //String _auditUserID = request.getParameter("_auditUserID");
    String _itemType = request.getParameter("_itemType");
    String _auditUserName = request.getParameter("_auditUserName");
    DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    Date startDate = null;
    if (_startdate != null && !_startdate.isEmpty()) {
        startDate = (Date) formatter.parse(_startdate);
    }
    Date endDate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        endDate = (Date) formatter.parse(_enddate);
    }

    AuditManagement audit = new AuditManagement();
    String strmsg = "No Record Found";
    Audit[] arrAudit = audit.searchChallengeResponseByDate(channel.getChannelid(), _itemType, startDate, endDate);
    session.setAttribute("arrAudit", arrAudit);
%>

<h3>Search Results</h3>

<div class="tabbable" id="REPORT">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#otpcharts1" data-toggle="tab">Charts</a></li>
        <li><a href="#otpreport2" data-toggle="tab">Tabular List</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="otpcharts1">
            <div class="row-fluid">
                <div class="span12">
                    <div class="control-group">
                        <div class="span4">
                            <div id="graph" ></div>

                        </div>
                        <div  class="span8">
                            <div id="graph1"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="otpreport2">  
            <div id="licenses_data_table">                           
                <div class="row-fluid">
                    <table class="table table-striped" >
                        <tr>
                            <td>No.</td>
                            <td>User name</td>            
                            <td>IP</td>                
                            <td>Action</td>
                            <td>Result</td>
                            <!--<td>Old</td>-->
                            <td>Time</td>
                            <td>New</td>

                        </tr>
                        <%    if (arrAudit != null) {
                                Map map = new HashMap();
                                for (int i = 0; i < arrAudit.length; i++) {
                                    AuthUser user = null;
                                    String userName = "NA";
                                    if (map.isEmpty()) {
                                        user = new UserManagement().getUser(channel.getChannelid(), arrAudit[i].getItemid());
                                        map.put(arrAudit[i].getItemid(), user);
                                    } else {
                                        if (map.get(arrAudit[i].getItemid()) == null) {
                                            user = new UserManagement().getUser(channel.getChannelid(), arrAudit[i].getItemid());
                                            map.put(arrAudit[i].getItemid(), user);
                                        } else {
                                            user = (AuthUser) map.get(arrAudit[i].getItemid());
                                        }
                                    }
                                    if (user != null) {
                                        userName = user.userName;
                                    }

                        %>
                        <tr>
                            <td><%=i + 1%></td>
                            <td><%=userName%></td>
                <!--            <td><%=arrAudit[i].getRemoteaccesslogin()%></td>-->
                            <td><%=arrAudit[i].getIpaddress()%></td>
                <!--            <td><%=arrAudit[i].getOperatorname()%></td>
                            <td><%=arrAudit[i].getCategory()%></td>
                            <td><%=arrAudit[i].getItemtype()%></td>-->
                            <td><%=arrAudit[i].getAction()%></td>
                            <td><%=arrAudit[i].getResult()%></td>
                            <td><%=sdf.format(arrAudit[i].getAuditedon())%></td>
                            <td><%=arrAudit[i].getNewvalue()%></td>


                        </tr>
                        <%}
                        } else {%>
                        <tr>
                            <td>1</td>
                            <td><%=strmsg%></td>
                            <td><%=strmsg%></td>
                            <td><%=strmsg%></td>
                            <td><%=strmsg%></td>
                            <td><%=strmsg%></td>
                            <td><%=strmsg%></td>
                <!--            <td><%=strmsg%></td>
                            <td><%=strmsg%></td>
                            <td><%=strmsg%></td>-->
                            <!--<td><%=strmsg%></td>-->
                        </tr>
                        <%}%>
                    </table>
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="control-group">                        
                                <div class="span1">
                                    <div class="control-group form-inline">
                                        <a href="#" class="btn btn-info" onclick="downloadCAReport(1, '<%=_startdate%>', '<%=_enddate%>')" >
                                            CSV</a>
                                    </div>
                                </div>
                                <div class="span1">
                                    <div class="control-group form-inline">
                                        <a href="#" class="btn btn-info" onclick="downloadCAReport(0, '<%=_startdate%>', '<%=_enddate%>')" >
                                            PDF</a>
                                    </div>
                                </div>
                                <div class="span1">
                                    <div class="control-group form-inline">
                                        <a href="#" class="btn btn-info" onclick="downloadCAReport(2, '<%=_startdate%>', '<%=_enddate%>')" >
                                            TEXT</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>

