<%@page import="java.util.Calendar"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.ChannelProfile"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.AxiomProtect"%>
<%@page import="com.mollatech.axiom.connector.access.controller.ApprovalSetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SchedulerManagement"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.db.Approvalsettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.AuthorizationManagement"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.connector.user.TokenStatusDetails"%>
<%@page import="com.mollatech.axiom.nucleus.db.Certificates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Pkitokens"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/authorization.js"></script>


<div class="container-fluid">
    <h1 class="text-success">Archived Requests</h2>
        <!--<h3>Make Your Own Report</h3>-->
        <%    
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
            String _channelId = channel.getChannelid();
            AuthorizationManagement aObj = new AuthorizationManagement();
    //        int limit = 50;
            
//         SettingsManagement sMngmt = new SettingsManagement();
//        Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
//        ChannelProfile chSettingObj = null;
//        if (settingsObj != null) {
//            chSettingObj = (ChannelProfile) settingsObj;
//        }
//        if(chSettingObj != null){
//            Date d = new Date();
//        Calendar current = Calendar.getInstance();
//        current.setTime(d);
////        current.set(Calendar.AM_PM, Calendar.AM);
//////        current.set(Calendar.HOUR, 0);
////        current.set(Calendar.MINUTE, 0);
////        current.set(Calendar.SECOND, 0);
////        current.set(Calendar.MILLISECOND, 0);
////        //  current.add(Calendar.DATE, 1);//add a date
////        Date endDate = current.getTime();
//
////        current.setTime(d);
////        current.set(Calendar.AM_PM, Calendar.AM);
//        current.add(Calendar.HOUR, -chSettingObj.authorizationDuration);
//        current.set(Calendar.MINUTE, 0);
//        current.set(Calendar.SECOND, 0);
//        current.set(Calendar.MILLISECOND, 0);
//        //current.add(Calendar.DATE, -1);//minus a date
//        Date startDate = current.getTime();
//            Approvalsettings[] arrexpiredRequest =  aObj.getExpiredRequest(sessionId, _channelId,startDate);
//           if(arrexpiredRequest != null){
//           for(int i=0;i<arrexpiredRequest.length;i++){
//               aObj.removeAuthorizationRequest(sessionId, _channelId, arrexpiredRequest[i].getApprovalId());
//           }
//           }
//        }
           
            Approvalsettings[] arrRequest = aObj.getALLArchiveRequest(sessionId,operatorS.getOperatorid(),_channelId);
           
            String strerr = "No Record Found";
      %>

        <div class="row-fluid">
            <div id="licenses_data_table">
<!--                <h3>Recent Additions</h3>-->
                <table class="table table-striped" id="table_main">

                    <tr>
                        <td>No.</td>
                        <td>Action</td>
                        <!--<td>Status</td>-->
                        <td>Marked By</td>
                        <td>User Name</td>
                        <td>Action</td>
                        <td>Created On</td>
                        <!--<td>Expired On</td>-->
                        <td>Updated On</td>
                    </tr>
                    <%                    if (arrRequest != null) {
                            OperatorsManagement opMngt = new OperatorsManagement();
                            UserManagement uMngt = new UserManagement();
                            for (int i = 0; i < arrRequest.length; i++) {

                                byte[] obj = arrRequest[i].getApprovalSettingEntry();
                                byte[] f = AxiomProtect.AccessDataBytes(obj);
                                ByteArrayInputStream bais = new ByteArrayInputStream(f);
                                Object object = AuthorizationManagement.deserializeFromObject(bais);

                                ApprovalSetting approvalSetting = null;
                                if (object instanceof ApprovalSetting) {
                                    approvalSetting = (ApprovalSetting) object;
                                }
                                String strStatus = "-";
                                if (arrRequest[i].getStatus() == 1) {
                                    strStatus = "Active";
                                } else if (arrRequest[i].getStatus() == 2) {
                                    strStatus = "Pending";
                                }
                                
                    %>

                    <tr>
                        <%if (approvalSetting != null) {
                                if(operatorS.getOperatorid().equals(approvalSetting.makerid)){
                             

                               
                                AuthUser u = uMngt.getUser(sessionId, _channelId, approvalSetting.userId);
                                String userName = "-";
                                if(u != null){
                                    userName = u.getUserName();
                                }
                                String strAuthStatus ="-";
                              if(arrRequest[i].getStatus() == AuthorizationManagement.AUTORIZATION_APPROVE_STATUS ){
                                  strAuthStatus = "Approve";
                              }else if(arrRequest[i].getStatus() == AuthorizationManagement.AUTORIZATION_REJECT_STATUS ){
                                  strAuthStatus = "Reject";
                              }else if(arrRequest[i].getStatus() == AuthorizationManagement.AUTORIZATION_EXPIRE_STATUS ){
                                  strAuthStatus = "Expired";
                              }
                                 Operators oprbj = opMngt.getOperatorById(_channelId, arrRequest[i].getApproverOperatorid());
                                 
                        %>
                        <td><%=(i + 1)%></td>
                        <td><%=approvalSetting.itemid%></td>
                        <td><%= oprbj.getName()%></td>
                        <td><%=userName%></td>
                        <td><%=strAuthStatus%></td>
                        <td><%=arrRequest[i].getCreatedOn()%></td>
                        <!--<td><%=arrRequest[i].getExpireOn()%></td>-->
                        <td><%=arrRequest[i].getUpdatedOn()%></td>
                        <%}%>
                    </tr>


                    <%}}
                } else {%>
                    <tr>
                        <td><%=1%></td>
                        <td><%= strerr%></td>
                        <td><%= strerr%></td>
                        <td><%= strerr%></td>
                        <td><%= strerr%></td>
                        <td><%= strerr%></td>
                        <td><%= strerr%></td>
                     
                    </tr>
                    <%}%>


                </table>


            </div>
        </div>
        <script type="text/javascript">
            //          ChangePkiCategory(1);
            //            ChangePkiStateType(0);
            //        generateOtpTable()
        </script>
</div>
<%@include file="footer.jsp" %>