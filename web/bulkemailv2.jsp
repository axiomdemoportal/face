<%@page import="com.mollatech.dictum.management.ContactManagement"%>
<%@page import="com.mollatech.axiom.nucleus.settings.ContactTagsSetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Templates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%--<%@page import="com.mollatech.axiom.nucleus.db.Tags"%>
<%@page import="com.mollatech.dictum.management.TagsManagement"%>--%>
<%@include file="header.jsp" %>
<!--<script src="./assets/js/ajaxfileupload.js"></script>-->
<!--<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>-->
<script src="./ckeditor/ckeditor.js"></script>
<script src="./assets/js/dictum/bulkemail.js"></script>
<!--<script src="./assets/js/contacts.js"></script>-->
<!--new js-->
   <script src="./assets/js/json_sans_eval.js"></script>
        <script src="./assets/js/bootbox.min.js"></script>
        <script src="./assets/js/ajaxfileupload.js"></script>
        <script src="./assets/js/bootstrap-fileupload.js"></script>   
       
<link href="./assets/css/bootstrap.css" rel="stylesheet">
        <link href="./assets/css/bootstrap-fileupload.css" rel="stylesheet">
<div class="container-fluid">
    <h2>Send Bulk Email Message</h2>
    <br>
    <div class="row-fluid">
        <form class="form-horizontal" id="bulkemail" name="bulkemail">
            <fieldset>
                <%                    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
                    Channels channelOnj = (Channels) session.getAttribute("_apSChannelDetails");
//                    TagsManagement tagmngObj = new TagsManagement();
//                    Tags[] tagsObj = tagmngObj.getAllTags(channelOnj.getChannelid());

                    SettingsManagement smng = new SettingsManagement();
                    ContactTagsSetting tagssettingObj = (ContactTagsSetting) smng.getSetting(sessionId, channelOnj.getChannelid(), SettingsManagement.CONTACT_TAGS, SettingsManagement.PREFERENCE_ONE);

                    String[] tags = tagssettingObj.getTags();

                    TemplateManagement tempObj = new TemplateManagement();
                    Templates[] templates = tempObj.Listtemplates(sessionId, channelOnj.getChannelid());

                    ContactManagement cmObj = new ContactManagement();

                %>

                <div class="control-group">
                    <label class="control-label"  for="toNumber">Select TAG</label>
                    <div class="controls">
                        <select class="selectpicker" name="_tagID" id="_tagID">

                            <% for (int j = 0; j < tags.length; j++) {
                                    int iCount = cmObj.getContactCountBytag(sessionId, channelOnj.getChannelid(), tags[j]);
                                    if (iCount == 0) {
                            %>                                 
                            <option value="<%=tags[j]%>" disabled><%=tags[j]%> (<%=iCount%> contacts)</option>                                             
                            <% } else {
                            %>
                            <option value="<%=tags[j]%>"><%=tags[j]%> (<%=iCount%> contacts)</option>                                             
                            <%
                                    }
                                }
                            %>
                        </select>

                    </div>
                </div>  

                <div class="control-group">
                    <label class="control-label"  for="toNumber">Choose Template</label>
                    <div class="controls">
                        <select class="selectpicker" name="_templateID" id="_templateID" onchange="LoadTemplateBody()">
                            <option  value="...">....</option>
                            <% for (int j = 0; j < templates.length; j++) {

                                    Templates t = templates[j];
                                    int iType = t.getType();
                                    // if(iType == 2){
                            %><%if (iType == 2) {%>
                            <option  value="<%=t.getTemplateid()%>"><%=t.getTemplatename()%></option>                                             
                            <% }

                                }%>
                        </select>
                        : with speed 
                        <select name="_speed" id="_speed">
                            <option value="1" >Slow</option>
                            <option value="3" >Normal</option>
                            <option value="10" >Fast</option>                            
                            <option value="30" >Hyper Fast</option>                            
                        </select>



                    </div>
                </div>     


                <input type="hidden" id="_type" name="_type" value="4">
                <div class="control-group">
                    <label class="control-label"  for="subject">subject</label>
                    <div class="controls">
                        <input type="text" id="_subject" name="_subject" placeholder="Subject" class="input-xlarge">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"  for="Cc">Cc</label>
                    <div class="controls">
                        <input type="text" id="_cc" name="_cc" placeholder="Cc Id" class="input-xlarge"/>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"  for="Bcc">Bcc</label>
                    <div class="controls">
                        <input type="text" id="_bcc" name="_bcc" placeholder="Bcc Id" class="input-xlarge"/>
                    </div>
                </div>
     <input type="hidden" id="emailCotents" name="emailCotents">



                <div class="control-group">
                    <label class="control-label"  for="username">Body</label>
                    <div class="controls">
                        <textarea class="ckeditor" id="_messageemailbody" name="_messageemailbody" ></textarea>                                    
                        <br>Attention: #name#,#date#,#datetime#,#email# and #phone# are going to get replaced with real values.
                    </div>                                
                </div>
                <hr>
                <div id="trackingDiv" ></div>
                <script type="text/javascript">
                    CKEDITOR.replace('_messageemailbody');
                    timer = setInterval('updateDiv()', 100);
                    function updateDiv() {
                        var editorText = CKEDITOR.instances._messageemailbody.getData();
                        $('#emailCotents').val(editorText);
                    }
                </script>       



<!--                <div id="file_container">
                    <label class="control-label"  for="fileName">Attachment </label>

                    <input name="images[]" type="file"  />
                    <br />
                </div>
                <a href="javascript:void(0);" onClick="add_file_field();">Add Another File</a><br />
                <a href="javascript:void(0);" onClick="remove_field();">Remove File</a><br />-->


<!--                <div class="control-group">
                    <label class="control-label"  for="username">Attach File 1: </label>
                    <div class="controls">
                        <input type="hidden" id="ads_imageEAD" name="ads_imageEAD">
                        <a id="displayTextEAD" href="#" onclick="toogleEAD()">Click to upload</a>
                        <div id="uploadImageEAD" style="display: none">
                            <form id="uploadFormEAD" id="uploadFormEAD">
                                <input id="fileToUploadEAD" type="file" name="fileToUploadEAD"/>
                                <button class="btn btn-mini btn-primary" id="buttonUploadEAD" onclick="return UploadContactFile()">Upload File Now>></button>
                            </form>
                        </div>
                    </div>
                </div>


                <div class="control-group">
                    <label class="control-label"  for="username">Attach File 2: </label>
                    <div class="controls">
                        <input type="hidden" id="ads_imageEAD" name="ads_imageEAD">
                        <a id="displayTextEAD" href="#" onclick="toogleEAD()">Click to upload</a>
                        <div id="uploadImageEAD" style="display: none">
                            <form id="uploadFormEAD" id="uploadFormEAD">
                                <input id="fileToUploadEAD" type="file" name="fileToUploadEAD"/>
                                <button class="btn btn-mini btn-primary" id="buttonUploadEAD" onclick="return UploadContactFile()">Upload File Now>></button>
                            </form>
                        </div>
                    </div>
                </div>-->


     <input type="hidden" id="_filename" name="_filename">
      <input type="hidden" id="_filenameS" name="_filenameS">
                
                   <div class="controls-row" id="licenses">
                    <div class="row-fluid">
                        <form class="form-horizontal" id="uploadXMLFormEAD" name="uploadXMLFormEAD">
                            <fieldset>
                                
                                <div class="control-group">
                                    <label class="control-label"  for="username">Select File:</label>                                    
                                    <div class="controls fileupload fileupload-new" data-provides="fileupload">
                                        <div class="input-append">
                                            <div class="uneditable-input span4"><i class="icon-file fileupload-exists"></i> 
                                                <span class="fileupload-preview"></span>
                                            </div>
                                            <span class="btn btn-file" >
                                                <span class="fileupload-new">Select file</span>
                                                <span class="fileupload-exists">Change</span>
                                                <input type="file" id="fileXMLToUploadEAD" name="fileXMLToUploadEAD"/>
                                            </span>
                                            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        </div>
                                       <button class="btn btn-success" id="buttonUploadEAD"  onclick="AttachmentUpload()" >Upload Now>></button>
                                    </div>
  
                                </div>
                                <!--<button class="btn btn-success" id="buttonUploadEAD"  onclick="PDFUpload()" >Upload Now>></button>-->
                            </fieldset>
                        </form>
                       
                    </div>
                    <!-- Submit -->
                </div>

                               <div class="controls-row" id="licensesS">
                    <div class="row-fluid">
                        <form class="form-horizontal" id="uploadXMLFormEAD" name="uploadXMLFormEAD">
                            <fieldset>
                                
                                <div class="control-group">
                                    <label class="control-label"  for="username">Select File:</label>                                    
                                    <div class="controls fileupload fileupload-new" data-provides="fileupload">
                                        <div class="input-append">
                                            <div class="uneditable-input span4"><i class="icon-file fileupload-exists"></i> 
                                                <span class="fileupload-preview"></span>
                                            </div>
                                            <span class="btn btn-file" >
                                                <span class="fileupload-new">Select file</span>
                                                <span class="fileupload-exists">Change</span>
                                                <input type="file" id="fileXMLToUploadEADS" name="fileXMLToUploadEADS"/>
                                            </span>
                                            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        </div>
                                       <button class="btn btn-success" id="buttonUploadEADS"  onclick="AttachmentUpload1()" >Upload Now>></button>
                                    </div>
  
                                </div>
                                <!--<button class="btn btn-success" id="buttonUploadEAD"  onclick="PDFUpload()" >Upload Now>></button>-->
                            </fieldset>
                        </form>
                       
                    </div>
                    <!-- Submit -->
                </div>


                <!-- Submit -->
                <div class="control-group">
                    <div class="controls">
                        <button class="btn btn-primary btn-large" onclick="emailblast()" type="button">Send Messages Now >></button>
                        <div id="bulk-email-gateway-result"></div>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>


    <script language="javascript" type="text/javascript">
        ChangeEmailSpeed(1);
        ChangeEmailFormat(1);
    </script>

    <%@include file="footer.jsp" %>