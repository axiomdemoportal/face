<%-- 
    Document   : SNMPaddsettings
    Created on : 1 Nov, 2014, 9:49:48 AM
    Author     : bluebricks6
--%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SystemMessageManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Systemmessagereceivertracking"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SchedulerManagement"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.AxiomProtect"%>
<%--<%@page import="com.mollatech.axiom.nucleus.settings.RecieverpdfSettingEntry"%>--%>
<%@page import="com.mollatech.axiom.nucleus.db.Systemmessagesettings"%>
<%@page import="java.util.List"%>
<%@include file="header.jsp" %>

<script src="./assets/js/systemMessage.js"></script>
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>

<div class="container-fluid">
    <h1 class="text-success">System Alert's( for Operators)</h1>
    
    <br>
    <div class="row-fluid">
        <div id="licenses_data_table">
            <table class="table table-striped">
                <tr>
                    <TD ><FONT size=3 ><B>No.</B></FONT></TD>
                    <!--                    <TD ><FONT size=3 ><B>Message No</B></FONT></TD>-->
                    <TD ><FONT size=3 ><B>Alert To</B></FONT></TD>
                    <TD ><FONT size=3 ><B>Brief</B></FONT></TD>
                    <TD ><FONT size=3 ><B>Message</B></FONT></TD>
                    <TD ><FONT size=3 ><B>Manage</B></FONT></TD>                    
                    <TD ><FONT size=3 ><B>Created On</B></FONT></TD>
                    <TD ><FONT size=3 ><B>Updated On</B></FONT></TD>
                    <TD ><FONT size=3 ><B>Expired On</B></FONT></TD>
                </tr>
                
                    <%
                        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                        SystemMessageManagement m = new SystemMessageManagement();
                        Systemmessagesettings[] list1 = m.listmessagesettingentry();
                        OperatorsManagement omang=new OperatorsManagement();
                        String strerr = "No Record Found";
                        if (list1 != null) {
                            for (int i = 0; i < list1.length; i++) {
                                String strStatus = "Active";
                                if (list1[i].getStatus() == 1) {
                                    strStatus = "Active";
                                } else {
                                    strStatus = "Suspended";
                                }
                                 String alertTo= null;
                              Roles r=omang.getRoleByRoleId(channel.getChannelid(), Integer.parseInt(list1[i].getAlertTo()));
                              if(r != null){
                              alertTo=r.getName();
                              }
                              String userStatus = "user-status-value-" + i;


                    %>
               
                    <td><%=i + 1%></td>
                    <td><%=alertTo%></td>
                    <td><%=list1[i].getMessage()%></td>
                    <td>
                        <a href="#" class="btn btn-mini" id="mobileTemplateBody-<%=i%>"  rel="popover" data-html="true">Click to View</a>
                        <script>
                            $(function ()
                            { $("#mobileTemplateBody-<%=i%>").popover({title: 'Message', content: "<%=list1[i].getMessage()%>"});
                            });
                        </script>

                    </td>
                    <td>
                        <div class="btn-group">
                            <button class="btn btn-mini" id="<%=userStatus%>"><%=strStatus%></button>
                            <!--<button class="btn btn-mini">Manage</button>-->
                            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#"  onclick="ChangemessageSettingStatus('<%=list1[i].getMessageId()%>', 1, '<%=userStatus%>')" >Mark as Active?</a></li>
                                <li><a href="#" onclick="ChangemessageSettingStatus('<%=list1[i].getMessageId()%>', 0, '<%=userStatus%>')" >Mark as Suspended?</a></li>
                                <li class="divider"></li>
                                <li><a href="#" onclick="loadEditmessageDetails('<%=list1[i].getMessageId()%>')">Edit</a></li>
                                <li><a href="#" onclick="deletemessagesettingentry('<%=list1[i].getMessageId()%>')"><font color="red">Remove?</font></a></li>

                            </ul>
                        </div>

                        <%

                        %>
                    </td>
                                        <TD ><%=list1[i].getCreatedOn()%></TD>
                    <td ><%=list1[i].getUpdatedOn()%></td>
                    <td ><%=list1[i].getExpiredOn()%></td>
                </tr>
                <%
                    }
                } else {

                %>

                <td><%=strerr%></td>
                <td><%=strerr%></td>
                <td><%=strerr%></td>
                <td><%=strerr%></td>
                <td><%=strerr%></td>
                <td><%=strerr%></td>
                <td><%=strerr%></td>
                <!--<td><%=strerr%></td>-->

                <%}%>
            </table>


        </div>
    </div>
    <br>
    <p><a href="#addOperator" class="btn btn-primary" data-toggle="modal">Add System Message&raquo;</a></p>
    <script language="javascript">
        //listChannels();
    </script>
</div>



<div id="addOperator" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Add System Message</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="AddNewOperatorForm" name="AddNewOperatorForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">System Messsage</label>
                        <div class="controls">
                            <!--                            <input type="text" id="_snmpip" name="_snmpip" placeholder="set ip" class="input-xlarge">-->
                            <textarea id="_messagebody" name="_messagebody"  class="span9" cols="60" rows="3"></textarea>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label"  for="username">Alert To</label>
                        <div class="controls">
                            <select class="span4" name="_alertto" id="_alertto" class="input-large">
                                <%
                                    OperatorsManagement oManagement = new OperatorsManagement();
                                   
                                   Roles[] roles1 = oManagement.getAllRoles(channel.getChannelid());
//                                    Roles roleObj = oManagement.getRoleByRoleId(_apSChannelDetails.getChannelid, operator.getRoleid());
                                    for (int i = 0; i < roles1.length; i++) {
//                                       if(roleObj.getName().equals(OperatorsManagement.admin) && !roles1[i].getName().equals(OperatorsManagement.sysadmin)){
                                %>
                                <!--                                <option value="<%=roles1[i].getRoleid()%>"><%=roles1[i].getName()%></option>-->
                                <%

//                                       }else if(roleObj.getName().equals(OperatorsManagement.sysadmin)){
                                %>
                                <option value="<%=roles1[i].getRoleid()%>"><%=roles1[i].getName()%></option>
                                <%
//                                       }}
                                    }
                            %>
                            </select>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label"  for="username">Status</label>
                        <div class="controls">

                            <select name="_status" id="_status" class="input-large">
                                <option value="1">Active</option>
                                <option value="2">Suspended</option>
                            </select>   
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>

    </div>


    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <div class="span3" id="add-new-operator-result"></div>
        <button class="btn btn-primary" onclick="addmessagesetting()" id="addnewOperatorSubmitBut">Create</button>
    </div>
</div>

<div id="editmessageDetails" class=" modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel1">Edit Message</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="editsytemmessageForm" name="editsytemmessageForm">
                <!--                <input type="" readonly id="_snmpipedit" name="_snmpipedit"  >-->
                <fieldset>
                    <input type="hidden" id="_messagenoedit" name="_messagenoedit" class="input-xlarge">
                    <div class="control-group">
                        <label class="control-label"  for="username">System Messsage</label>
                        <div class="controls">
                            <!--                            <input type="text" id="_snmpip" name="_snmpip" placeholder="set ip" class="input-xlarge">-->
                            <textarea id="_messagebodyedit" name="_messagebodyedit"  class="span9" cols="60" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Alert To</label>
                        <div class="controls">
                            <select class="span4" name="_alerttoedit" id="_alerttoedit" class="input-large">
                                <%
                                    oManagement = new OperatorsManagement();
//                                    Roles roleObj = oManagement.getRoleByRoleId(_apSChannelDetails.getChannelid, operator.getRoleid());
                                    for (int i = 0; i < roles1.length; i++) {
//                                       if(roleObj.getName().equals(OperatorsManagement.admin) && !roles1[i].getName().equals(OperatorsManagement.sysadmin)){
                                %>
                                <!--                                <option value="<%=roles1[i].getRoleid()%>"><%=roles1[i].getName()%></option>-->
                                <%

//                                       }else if(roleObj.getName().equals(OperatorsManagement.sysadmin)){
                                %>
                                <option value="<%=roles1[i].getRoleid()%>"><%=roles1[i].getName()%></option>
                                <%
//                                       }}
                                    }
                            %>
                            </select>
                        </div>
                    </div>
                </fieldset>
            </form>

        </div>
    </div>  
    <div class="modal-footer">
        <div id="editmessage-result"></div>
        <button class="btn btn-primary" onclick="editsystemmessagedetails()" id="editmessageButton">Save</button>
        <button class="btn" data-dismiss="modal" >Cancel</button>
    </div>
</div>
<%@include file="footer.jsp" %>

