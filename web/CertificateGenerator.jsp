<%@page import="com.mollatech.axiom.nucleus.settings.RootCertificateSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@include file="header.jsp" %>
<script src="assets/js/certGenerator.js" type="text/javascript"></script>

<%    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    String[] strTags = null;
    SettingsManagement smng = new SettingsManagement();
    RootCertificateSettings rCertSettings = (RootCertificateSettings) smng.getSetting(sessionid, channel.getChannelid(), SettingsManagement.RootConfiguration, 1);
    if (rCertSettings != null) {
        if (rCertSettings.CAEmailides != null) {
            strTags = rCertSettings.CAEmailides.split(",");
        }
    }
    String selfSignedCAName = LoadSettings.g_sSettings.getProperty("selfsigned.ca.name");
    String thirdpartCa = LoadSettings.g_sSettings.getProperty("thirparty.ca.name");
%>
<h1 class="text-success">Generate Certificate for Domain and Application Server</h2>
<div class="tabbable">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#primary" data-toggle="tab">SelfSigned Certs</a></li>
        <li><a href="#secondary" data-toggle="tab">ThirdParty CA</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="primary">
            <div id="certificaterGen">
                <div class="row-fluid">
                    <form class="form-horizontal" id="frmCertificateGeneration" style="margin-left:45px;" name="frmCertificateGeneration">
                        <fieldset>
                            <input type="hidden" id="_auditUserIDPKI" name="_auditUserIDPKI"/>
                            <input type="hidden" id="_auditUserNamePKI" name="_auditUserNamePKI"/>
                            <!--                    Certificate Type:-->
                            <br>
                            Select Cert Type:
                            <select span="1"  name="_certType" id="_certType" >
                                <option value="1">Application Certificates </option>
                                <option value="2">SSL Certificates</option>
                            </select>
                            <!--                    &nbsp;&nbsp;&nbsp;-->
                            <!--                    Issuer:-->
                            <select span="1" onblur="getDivforSSL();"  name="_caType" id="_caType" style="display:none" >
                                <option value="1" onclick="getDivforSSL();"><%=selfSignedCAName%> </option>
                                <option value="2"  onclick="getDivforSSL();"><%=selfSignedCAName%></option>
                            </select>

                            <!--                    Product Type:-->
                            <select span="1"  name="_product_type" id="_product_type"  style="display:none">
                                <option value="2">AlphaSSL:</option>
                                <option value="2">DomainSSL:</option>
                                <option value="2">OrganizationSSL:</option>
                                <option value="2">Extended SSL:</option>
                            </select>       
                            <h5>   Please fill Details below for csr generation:</h5>
                            <br/>
                            <div id="divSelfSigned">
                                <table>
                                    <tr>
                                        <td style="text-allign:right">
                                            Common Name:  
                                        </td>
                                        <td style="text-allign:left">
                                            <input id="common_name"  onblur="checkTextField(this);" placeholder="ip,domain name" style="width:170px" name="common_name" type="text" ></input>
                                        </td>
                                        <td>&nbsp;</td>
                                        <td style="text-allign:right">
                                            OrganizationName:
                                        </td>
                                        <td style="text-allign:left">
                                            <input  onblur="checkTextField(this);" id="_txtorganizationName" placeholder="eg.telecom malaysia" style="width:170px" name="_txtorganizationName" type="text" ></input>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td> &nbsp; &nbsp;</td> <td> &nbsp;&nbsp;</td> <td> &nbsp;&nbsp; </td> <td>&nbsp;&nbsp;  </td>
                                    </tr>
                                    <tr>
                                        <td style="text-allign:right">
                                            OrganizationUnit:
                                        </td>
                                        <td style="text-allign:left">
                                            <input id="_txtOrganizationUnit"   onblur="checkTextField(this);" style="width:170px" placeholder="eg.business management" name="_txtOrganizationUnit" type="text" ></input>
                                        </td>   <td>&nbsp;</td>
                                        <td style="text-allign:right">
                                            LocalityName:
                                        </td>
                                        <td style="text-allign:left">
                                            <input id="_txtLocalityName"  onblur="checkTextField(this);" style="width:170px" placeholder="eg.cyberjaya" name="_txtLocalityName" type="text" ></input>
                                        </td>
                                    </tr><tr>
                                        <td> &nbsp; &nbsp;</td> <td> &nbsp;&nbsp;</td> <td> &nbsp;&nbsp; </td> <td>&nbsp;&nbsp;  </td> 
                                    </tr>
                                    <tr>
                                        <td style="text-allign:right">
                                            State:
                                        </td>
                                        <td style="text-allign:left">
                                            <input id="_txtState" style="width:170px;"   onblur="checkTextField(this);"  placeholder="eg.Kaula Lampur" name="_txtState" type="text" ></input>
                                        </td>   <td>&nbsp;</td>
                                        <td style="text-allign:right">
                                            Country:
                                        </td>
                                        <td style="text-allign:left">

                                            <input id="_txtCountry" style="width:80px"   onblur="checkTextField(this);" name="_txtCountry" placeholder="eg.MY" type="text" ></input>
                                        </td>
                                    </tr><tr>
                                        <td> &nbsp; &nbsp;</td> <td> &nbsp;&nbsp;</td> <td> &nbsp;&nbsp; </td> <td>&nbsp;&nbsp;  </td>
                                    </tr>
                                    <tr>
                                        <td style="text-allign:right">
                                            Email
                                        </td>
                                        <td style="text-allign:left">
                                            <input id="_txtEmail" style="width:170px" style="display:none"   onblur="checkTextField(this);" placeholder="eg.pramod@mollatech.com" name="_txtEmail" type="text" ></input>
                                        </td>   <td>&nbsp;</td>
                                        <td style="text-allign:right">
                                            Validity         
                                        </td>
                                        <td style="text-allign:left">
                                            <input id="_txtval" style="width:50px"    onblur="checkTextField(this);" placeholder="In Years" maxlength="2" name="_txtval" type="text" ></input>
                                            &nbsp; Key Size:
                                            <select span="1" style="width:70px"  name="_keysize" id="_keysize" >
                                                <option value="1024">1024 </option>
                                                <option value="2048">2048</option>
                                                <option value="4096">4096</option>
                                            </select>
                                            <!--                                       <input type="button" class="btn-primary success" onclick="generateCSR()" value="Generate CSR">-->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-allign:right">

                                        </td>
                                        <td style="text-allign:left">

                                        </td>  <td>&nbsp;</td>
                                        <td style="text-allign:right">
                                            &nbsp;
                                        </td>
                                        <td style="text-allign:left">
                                            &nbsp;  &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-allign:right">
                                            <!--                                  CSR-->
                                        </td>
                                        <td style="text-allign:left">
                                            <!--                                    <textarea rows="3" cols="150">   
                                            
                                                                                </textarea>-->
                                        </td>  <td>&nbsp;</td>
                                        <td style="text-allign:right">
                                            &nbsp;
                                        </td>
                                        <td>  &nbsp;  &nbsp;</td>


                                    </tr>

                                    <tr>
                                        <td style="text-allign:right">

                                        </td>
                                        <td style="text-allign:left">

                                        </td>  <td>&nbsp;</td>
                                        <td style="text-allign:right">
                                            &nbsp;
                                        </td>
                                        <td style="text-allign:left">
                                            &nbsp;  &nbsp;
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="text-allign:right">

                                        </td>
                                        <td style="text-allign:left">
                                            <input type="button" id="btnCertGen" onclick="generateCertificate()" class="btn btn-primary" value="Generate Certificate">
                                            &nbsp;&nbsp;
                                            <input type="button" id="btnSearchCert" onclick="gotoCertificateList()" class="btn btn-primary" value="Search Certificates">
                                        </td>   <td>&nbsp;</td>

                                        <td style="text-allign:right">
                                            &nbsp;
                                        </td>
                                        <td style="text-allign:left">
                                            &nbsp;  &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </div>


                        </fieldset>
                    </form>
                </div>


            </div>
        </div>
        <div class="tab-pane" id="secondary">  
            <div class="row-fluid">
                <form class="form-horizontal" id ="certificateform" >
                    <div id="divThirdParty">
                        <table style="margin-left:15px;">
                            <tr>
                                <td style="text-allign:right">
                                </td>
                                <td style="text-allign:left">

                                </td>
                                <td>&nbsp;</td>
                                <td style="text-allign:right">

                                </td>
                                <td style="text-allign:left">
                                    &nbsp;
                                </td>
                            </tr>

                            <td style="text-allign:right">
                                Product Type:  
                            </td>
                            <td style="text-allign:left">
                                <select span="1" style="width:120px"  name="_keysize" id="_keysize" >
                                    <option value="1">Domain SSL </option>
                                    <option value="2">Extended SSL</option>
                                </select>
                            </td>
                            <td style="width: 1%"></td><td></td>
                            <td style="text-allign:right">Licenses</td>
                            <td style="text-allign:left">
                                <input type="text" name="txtLicense"  placeholder="0 to 99" style="width:50px">
                            </td>

                            </tr>
                            <tr>
                                <td style="text-allign:right">
                                </td>
                                <td style="text-allign:left">

                                </td>
                                <td>&nbsp;</td>
                                <td style="text-allign:right">

                                </td>
                                <td style="text-allign:left">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="text-allign:right">
                                    Common Name:  
                                </td>
                                <td style="text-allign:left">
                                    <input id="common_nameTP"  onblur="checkTextField(this);" placeholder="ip,domain name" style="width:170px" name="common_nameTP" type="text" ></input>
                                </td>
                                <td>&nbsp;</td><td></td>
                                <td style="text-allign:right">
                                    OrganizationName:
                                </td>
                                <td style="text-allign:left">
                                    <input  onblur="checkTextField(this);" id="_txtorganizationNameTP" placeholder="eg.telecom malaysia" style="width:170px" name="_txtorganizationNameTP" type="text" ></input>
                                </td>
                            </tr>
                            <tr>
                                <td> &nbsp; &nbsp;</td> <td> &nbsp;&nbsp;</td> <td> &nbsp;&nbsp; </td> <td>&nbsp;&nbsp;  </td>
                            </tr>
                            <tr>
                                <td style="text-allign:right">
                                    OrganizationUnit:
                                </td>
                                <td style="text-allign:left">
                                    <input id="_txtOrganizationUnitTP"   onblur="checkTextField(this);" style="width:170px" placeholder="eg.business management" name="_txtOrganizationUnitTP" type="text" ></input>
                                </td>   <td>&nbsp;</td> <td></td>
                                <td style="text-allign:right">
                                    LocalityName:
                                </td>
                                <td style="text-allign:left">
                                    <input id="_txtLocalityNameTP"  onblur="checkTextField(this);" style="width:170px" placeholder="eg.cyberjaya" name="_txtLocalityNameTP" type="text" ></input>
                                </td>
                            </tr><tr>
                                <td> &nbsp; &nbsp;</td> <td> &nbsp;&nbsp;</td> <td> &nbsp;&nbsp; </td> <td>&nbsp;&nbsp;  </td> 
                            </tr>
                            <tr>
                                <td style="text-allign:right">
                                    State:
                                </td>
                                <td style="text-allign:left">
                                    <input id="_txtStateTP" style="width:170px;"   onblur="checkTextField(this);"  placeholder="eg.Kaula Lampur" name="_txtStateTP" type="text" ></input>
                                </td>   <td>&nbsp;</td><td></td>
                                <td style="text-allign:right">
                                    Select Country:
                                </td>
                                <td style="text-allign:left">
                                    <select span="1"  name="_txtcountrylist" id="_txtcountrylist" >

                                    </select>
                                    <!--                                    <input id="_txtCountryTP" style="width:80px"   onblur="checkTextField(this);" name="_txtCountryTP" placeholder="ex.MY" type="text" ></input>-->
                                </td>





                            </tr><tr>
                                <td> &nbsp;&nbsp;</td> <td> &nbsp;&nbsp;</td> <td> &nbsp;&nbsp; </td> <td>&nbsp;&nbsp;  </td>
                            </tr>
                            <tr>
                                <td style="text-allign:right">
                                    Email
                                </td>

                                <td style="text-allign:left">
                                    <input id="_txtEmailTP" style="width:170px"   onblur="checkTextField(this);" placeholder="eg.pramod@mollatech.com" name="_txtEmailTP" type="text" ></input>
                                </td>  <td>&nbsp;</td><td></td>
                                <td style="text-allign:right">
                                    Select City: 
                                </td>                                <td style="text-allign:left">
                                    <select span="1"  name="_txtcityList" id="_txtcityList" >

                                    </select>

                                </td><td>&nbsp;</td>





                            </tr>
                            <tr>
                                <td style="text-allign:right">
                                    &nbsp;
                                </td>
                                <td style="text-allign:left">
                                    &nbsp;
                                </td>   <td>&nbsp;</td>
                                <td style="text-allign:right">
                                    &nbsp;
                                </td>

                                <td style="" >
                                    &nbsp;

                                </td>

                            </tr>
                            <tr>
                                <td style="text-allign:right">
                                    <input type="button" id="btngenCSR" onclick="generateCSR()" class="btn-primary success" value="GenerateCSR">
                                </td>

                                <td style="text-allign:right">
                                    &nbsp;
                                </td>
                                <td></td><td></td>
                                <td style="text-allign:left">
                                    CSR(For Reference)
                                </td>
                                <td style="text-allign:right">
                                    <textarea cols="45" rows="4" id="txtcsr" name="txtcsr">
                                        
                                    </textarea>
                                </td></tr>


                            <tr>
                                <td style="text-allign:right">
                                    &nbsp;
                                </td>
                                <td style="text-allign:left">
                                    &nbsp;
                                </td>   <td>&nbsp;</td>
                                <td style="text-allign:right">
                                    &nbsp;
                                </td>

                                <td style="" >
                                    &nbsp;

                                </td>

                            </tr>
                            <td style="text-allign:right">
                                SubjectID(AgreeMent-ID)</td> <td><input  onblur="checkTextField(this);" style="width:70px" id="_txtsubid" placeholder="eg.telecom malaysia" style="width:170px" name="_txtsubid" type="text" ></input>
                            </td>


                            <td></td><td> &nbsp;</td>
                            <td style="text-allign:left;" >
                                Select ApproverEmail:
                            <td>
                                <select id="selApproverEmailTP"  name="selApproverEmailTP">

                                </select>
                            </td>

                            </tr>

                            <td style="" >
                                &nbsp;

                            </td>

                            </tr>
                            <tr>
                                <td style="text-allign:right">
                                    &nbsp;
                                </td>
                                <td style="text-allign:left">
                                    &nbsp;
                                </td>   <td>&nbsp;</td>
                                <td style="text-allign:right">
                                    &nbsp;
                                </td>

                                <td style="" >
                                    &nbsp;

                                </td>

                            </tr>
                        </table>
                        <div class="panel-group">
                            <div class = "pane panel-info">
                                <div class = "panel-heading">
                                    <h5 class = "panel-title">Fill Organization Details Below</h5>
                                </div>

                                <div class = "panel-body"><table>

                                        <tr>
                                            <td style="text-allign:right">
                                                &nbsp;
                                            </td>
                                            <td style="text-allign:left">
                                                &nbsp;
                                            </td>   <td>&nbsp;</td>
                                            <td style="text-allign:right">
                                                &nbsp;
                                            </td>

                                            <td style="" >
                                                &nbsp;

                                            </td>

                                        </tr>
                                        <tr>
                                            <td style="text-allign:right">
                                                BusinessAssumedName:
                                            </td>
                                            <td style="text-allign:left">
                                                <input type="text" id="txtbusinessaname" name="txtbusinessaname">   &nbsp;   &nbsp;
                                            </td>   <td>&nbsp;</td> <td></td>
                                            <td style="text-allign:right">
                                                CreditAgency:
                                            </td>
                                            <td style="" >
                                                <select span="1"  name="_caType" style="width:70px" id="_caType" >
                                                    <option value="1">DUM</option>
                                                    <option value="2">POOP</option>

                                                </select>
                                                Org Code:<input type="text" id="txtorgCode" name="txtorgCode" style="width:50px">
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>BusinessCategoryCode</td>
                                            <td style="text-allign:right">
                                                <input type="text" name="_businesscategorycode" id="_businesscategorycode" style="width:50px"> 
                                            </td>
                                            <td style="text-allign:left">
                                                &nbsp;
                                            </td>   <td>&nbsp;</td>

                                        </tr>
                                        <tr>
                                            <td style="text-allign:right">
                                                OrganizationNameNative:
                                            </td>
                                            <td style="text-allign:left">
                                                <input type="text" name="_txtorgnativename" id="_txtorgnativename">
                                            </td>  
                                            <td>&nbsp;</td>
                                            <td></td>
                                            <td>
                                                OrganizationAddress:
                                            </td><td>
                                                <textarea id="txtorgAddr" name="txtorgAddr" cols="50" rows="3"> </textarea> 
                                            </td>


                                        </tr>
                                    </table>
                                </div>
                            </div>



                            <div class = "pane panel-info">
                                <div class = "panel-heading">
                                    <h5 class = "panel-title">Requestor Information</h5>
                                </div>

                                <div class = "panel-body">
                                    <table>



                                        <td style="text-allign:right">
                                            FirstName;
                                        </td>
                                        <td style="text-allign:left">
                                            <input type="text" id="txtfirstname" name="txtfirstname">

                                        </td> 
                                        <td>&nbsp;</td>
                                        <td style="width: 10%"></td>
                                        <td style="text-allign:right">
                                            Last Name:
                                        </td>

                                        <td style="text-align: left" >
                                            <input type="text" id="txtlastname" name="txtlastname">


                                        </td>


                                        <tr>
                                            <td style="text-allign:right">
                                                &nbsp;
                                            </td>
                                            <td style="text-allign:left">
                                                &nbsp;
                                            </td>   <td>&nbsp;</td>
                                            <td style="text-allign:right">
                                                &nbsp;
                                            </td>

                                            <td style="" >
                                                &nbsp;

                                            </td>

                                        </tr>

                                        <tr>
                                            <td style="text-allign:right">
                                                OrganizationUnit:
                                            </td>
                                            <td style="text-allign:left">
                                                <input type="text" id="txtreqorunit" name="txtreqorunit">
                                            </td>   <td>&nbsp;</td><td></td>
                                            <td style="text-allign:right">
                                                OrganizationName:  
                                            </td>

                                            <td style="text-allign:left " >
                                                <input type="text" id="txtreqorgname" name="txtreqorgname">

                                            </td>

                                        </tr>
                                        <tr>
                                            <td style="text-allign:right">
                                                &nbsp;
                                            </td>
                                            <td style="text-allign:left">
                                                &nbsp;
                                            </td>   <td>&nbsp;</td>
                                            <td style="text-allign:right">
                                                &nbsp;
                                            </td>

                                            <td style="" >
                                                &nbsp;

                                            </td>

                                        </tr>

                                        <tr>
                                            <td style="text-allign:right">
                                                Phone :
                                            </td>
                                            <td style="text-allign:left">
                                                <input type="text" id="txtreqphone" name="txtreqphone">
                                            </td>   <td>&nbsp;</td>
                                            <td style="text-allign:right">

                                            </td>

                                            <td style="" >

                                            </td>

                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div>
                            </div>

                            <br>
                            <div class = "pane panel-info">
                                <div class = "panel-heading">
                                    <h5 class = "panel-title">Approver Information</h5>
                                </div>

                                <div class = "panel-body"><table>

                                        <tr>
                                            <td style="text-allign:right">
                                                Coupon Code:
                                            </td>
                                            <td style="text-allign:left">
                                                <input id="_txtCouponCode" style="width:170px"   onblur="checkTextField(this);" placeholder="code" name="_txtCouponCode" type="text" ></input>
                                            </td>   <td>&nbsp;</td><td></td>
                                            <td style="text-allign:right">
                                                First Name:
                                            </td>
                                            <td style="text-allign:left">
                                                <input id="_txtFirstName" style="width:170px"   onblur="checkTextField(this);" placeholder="First Name" name="_txtFirstName" type="text" ></input>    
                                            </td>
                                        </tr>



                                        <tr>
                                            <td> &nbsp; &nbsp;</td> <td> &nbsp;&nbsp;</td> <td> &nbsp;&nbsp; </td> <td>&nbsp;&nbsp;  </td>
                                        </tr>
                                        <tr>
                                            <td style="text-allign:right">
                                                Last Name:
                                            </td>
                                            <td style="text-allign:left">
                                                <input id="_txtLastName" style="width:170px"   onblur="checkTextField(this);" placeholder="Last Name" name="_txtLastName" type="text" ></input>     
                                            </td> <td>&nbsp;</td> <td></td>
                                            <td>
                                                Phone No:       
                                            </td>
                                            <td style="text-allign:right">
                                                <input id="_txtphon" style="width:170px"   onblur="checkTextField(this);" placeholder="Phone no" name="_txtphon" type="text" ></input>     
                                            </td>


                                        </tr>
                                        <tr>
                                            <td> &nbsp; &nbsp;</td> <td> &nbsp;&nbsp;</td> <td> &nbsp;&nbsp; </td> <td>&nbsp;&nbsp;  </td>
                                        </tr>
                                        <tr>
                                            <td style="text-allign:right">
                                                Email Id:
                                            </td>
                                            <td style="text-allign:left">
                                                <input id="_txtEmail" style="width:170px"   onblur="checkTextField(this);" placeholder="Email ID" name="_txtEmail" type="text" ></input>     
                                            </td> <td>&nbsp;</td> 
                                            <td>

                                            </td>
                                            <td style="text-allign:right">

                                            </td>


                                        </tr>
                                        <tr>
                                            <td style="text-allign:right">
                                                &nbsp;
                                            </td>
                                            <td style="text-allign:left">
                                                &nbsp;
                                            </td>   <td>&nbsp;</td>
                                            <td style="text-allign:right">
                                                &nbsp;
                                            </td>

                                            <td style="" >
                                                &nbsp;

                                            </td>

                                        </tr>

                                        <td></td><td>&nbsp; </td>
                                        <td style="text-allign:left">
                                            <input type="button" id="orderCertificate" onclick="generateCSR()" class="btn-primary success" value="Order Certificate">
                                        </td>

                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>




<%@include file="footer.jsp" %>