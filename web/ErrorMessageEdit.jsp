<%@page import="com.mollatech.axiom.nucleus.db.Templates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.operation.TemplateVariables"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%@include file="header.jsp" %>
<%
    String strTID = request.getParameter("_tid");
%>
<script src="./assets/js/errormessage.js"></script>
<div class="container-fluid" >
    <h2 id="idEditTemplateName">Edit Message </h2>
    <hr>
    <div class="tabbable">
        <div class="tab-content">
            <div class="tab-pane active" id="primary">
                <div class="row-fluid">
                    <form class="form-horizontal" id="messageedittemplateform" name="messageedittemplateform">
                        <input type="hidden" id="idMessageTemplateId" name="idMessageTemplateId"  >
                        <input type="hidden" id="_templateM_name" name="_templateM_name"  class="input-xlarge">
                        <input type="hidden" id="_templateM_variable" name="_templateM_variable" class="input-xlarge">
                        <fieldset>
                            <!--<div class="control-group">
                                <label class="control-label"  for="username">Unique Name</label>
                                <div class="controls">
                                    <input type="text" id="_templateM_name" name="_templateM_name"  class="input-xlarge">
                                </div>
                            </div>-->

                            <div class="control-group">
                                <label class="control-label"  for="username">Message</label>
                                <div class="controls">
                                    <textarea id="_templateM_body" name="_templateM_body"  class="span9" cols="60" rows="3"></textarea>
                                </div>
                            </div>

                            <!--<div class="control-group">
                                <label class="control-label"  for="username">Variables </label>
                                <div class="controls">
                                    <input type="text" id="_templateM_variable" name="_templateM_variable" class="input-xlarge">
                                </div>
                            </div>-->

                            <!-- Submit -->
                            
                            <div class="control-group">
                                <div class="controls">
                                    <a href="./ErrorMessageList.jsp" class="btn" type="button">Back To List</a>
                                    <% //if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >=3 ) {%>
                                    <button class="btn btn-primary" onclick="editMessagetemplates()" id="buttonEditMessage" type="button">Save Error Message</button>
                                    <% //}%>
                                    <div id="edittemplateM-result"></div>
                                </div>
                        </fieldset>
                    </form>
                </div>
            </div>


        </div>
    </div>
    <script>
        loadErrorMessageDetails('<%=strTID%>');
    </script>

</div>
<%@include file="footer.jsp" %>