<%@include file="header.jsp" %>
<script src="./assets/js/channelprofilesetting.js"></script>
<div class="container-fluid">
    <h1 class="text-success">Channel Profile Settings </h1>
    <p>These following settings are global to this channel to offer more flexibility in terms of user, tokens, certificates and geolocation settings.</p>
    <hr>
    <br>
    <div class="row-fluid">
        <form class="form-horizontal" id="channelprofileform" name="channelprofileform">
            <div class="control-group">
                <label class="control-label"  for="username">User Management</label>
                <div class="controls">
                    <div>
                       Unique by
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_check-user"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeCheckUser(1)">Name?</a></li>
                                <li><a href="#" onclick="ChangeCheckUser(2)">Email/Phone?</a></li>
                            </ul>
                        </div>
                        and
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_alert-User"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeAlertChanges(1)">Yes?</a></li>
                                <li><a href="#" onclick="ChangeAlertChanges(0)">No?</a></li>
                            </ul>
                        </div>
                        to alert user for any change(edit or delete) using
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_alert-media"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeAlertMedia(1)">SMS?</a></li>
                                <li><a href="#" onclick="ChangeAlertMedia(2)">EMAIL?</a></li>
                                <li><a href="#" onclick="ChangeAlertMedia(3)">VOICE?</a></li>
                                <li><a href="#" onclick="ChangeAlertMedia(4)">USSD?</a></li>
                            </ul>

                        </div>
                        .
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"  for="username"></label>
                <div class="controls">
                    <div>
                        Enable
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_edit-User"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeEditUser(1)">Yes?</a></li>
                                <li><a href="#" onclick="ChangeEditUser(0)">No?</a></li>
                            </ul>
                        </div>
                        for edit user, enable
                        <!--, Delete User--> 
                        <div class="btn-group">

                            <button class="btn btn-small"><div id="_delete-User"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeDeleteUser(1)">Yes?</a></li>
                                <li><a href="#" onclick="ChangeDeleteUser(0)">No?</a></li>
                            </ul>
                        </div>
                        for delete user and
                        enable
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_reset-User"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeResetUser(1)">yes?</a></li>
                                <li><a href="#" onclick="ChangeResetUser(0)">No?</a></li>
                            </ul>
                        </div>
                        for Reset User 
                    </div>
                </div>
            </div>
            <hr>
            <div class="control-group">
                <label class="control-label"  for="username">Others</label>
                <div class="controls">

                    <div>
                        Load Token Settings
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_token-load"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeTokenSettingLoad(1)">Every Time</a></li>
                                <li><a href="#" onclick="ChangeTokenSettingLoad(0)">Once at Restart</a></li>
                            </ul>
                        </div>   
                        , Mobile Token Source As 
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_software-type"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeSoftwareOTPType(1)">Google/Apple Store</a></li>
                                <li><a href="#" onclick="ChangeSoftwareOTPType(0)">Mobile Trust SDK</a></li>
                            </ul>
                        </div>
                        , Check Gateway connectivity status after every 
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_connector-status"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeConncetorStatus(300)">5 minutes</a></li>
                                <li><a href="#" onclick="ChangeConncetorStatus(600)">10 minutes</a></li>
                                <li><a href="#" onclick="ChangeConncetorStatus(1800)">30 minutes</a></li>
                                <li><a href="#" onclick="ChangeConncetorStatus(3600)">60 minutes</a></li>
                                <li><a href="#" onclick="ChangeConncetorStatus(10800)">180 minutes</a></li>
                                <li><a href="#" onclick="ChangeConncetorStatus(21600)">360 minutes</a></li>
                            </ul>
                        </div>  
                    </div>
                </div>
            </div>

            <div class="control-group" style="display:none">
                <!--<label class="control-label"  for="username">Audit Cleanup Days</label>-->
                <div class="controls">
                    <div>
                        Cleanup Audit Trails and Message logs older than 
                        <div class="btn-group">
                            <button class="btn btn-small"><div id="_cleanup-days"></div></button>
                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="ChangeCleanupAuditDays(90)">3 Months</a></li>
                                <li><a href="#" onclick="ChangeCleanupAuditDays(180)">6 Months</a></li>
                                <li><a href="#" onclick="ChangeCleanupAuditDays(270)">9 Months</a></li>
                                <li><a href="#" onclick="ChangeCleanupAuditDays(364)">12 Months</a></li>
                                <li><a href="#" onclick="ChangeCleanupAuditDays(728)">24 Months</a></li>
                            </ul>
                        </div>   
                        with logging path as 
                        <input type="text" id="_cleanuppath" name="_cleanuppath" placeholder="/var/axiomprotect2/interface/axiomv2-settings/cleanup.logs" class="span3">
                        .
                    </div>
                </div>
            </div>

            <input type="hidden" id="_checkuser" name="_checkuser">
            <input type="hidden" id="_alertuser" name="_alertuser" >
            <input type="hidden" id="_edituser" name="_edituser">
            <input type="hidden" id="_deleteuser" name="_deleteuser">
            <input type="hidden" id="_tokenload" name="_tokenload">            
            <input type="hidden" id="_alertmedia" name="_alertmedia">
            <input type="hidden" id="_softwaretype" name="_softwaretype">
            <input type="hidden" id="_resetuser" name="_resetuser">
            <input type="hidden" id="_connectorstatus" name="_connectorstatus">
            <input type="hidden" id="_cleanupdays" name="_cleanupdays">
            <input type="hidden" id="_authorizer" name="_authorizer" >
            <input type="hidden" id="_Authorizer_PeningDuration" name="_Authorizer_PeningDuration" >
            <input type="hidden" id="_Authorizer_Units" name="_Authorizer_Units" value="1">
            <input type="hidden" id="UnitAutherization" name="UnitAutherization" value="1">
           

            <div class="control-group">
                <!--<label class="control-label"  for="username">Required Paths</label>-->
                <div class="controls">
                    Remote server signing path as 
                    <!--<input type="text" id="_cleanuppath" name="_cleanuppath" placeholder="/var/axiomprotect2/interface/axiomv2-settings/cleanup.logs" class="span3">-->
                    <input type="text" id="_rssarchive" name="_rssarchive" placeholder="/var/axiomprotect2/interface/axiomv2-settings/rss-archive/" class="span3">
                    and temporary file upload path as <input type="text" id="_uploads" name="_uploads" placeholder="/var/axiomprotect2/interface/axiomv2-settings/uploads/" class="span3">
                </div>
            </div>
            <hr>
            <div class="control-group">
                <label class="control-label"  for="username"> Authorizer Enforcement (Maker-Checker)</label>
                <div class="controls">
                    <!--<div>-->
                    <!--Search by-->
                    Status
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_Authorizer-user"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="ChangeAuthorizerStatus(1)">Enable</a></li>
                            <li><a href="#" onclick="ChangeAuthorizerStatus(0)">Disable</a></li>
                        </ul>
                    </div>
                    Keep Pending for duration of
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_Authorizer_PeningDuration-user"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
<!--                            <li><a href="#" onclick="ChangePendingDuration(1)">1 Hour</a></li>
                            <li><a href="#" onclick="ChangePendingDuration(3)">3 Hour</a></li>
                            <li><a href="#" onclick="ChangePendingDuration(6)">6 Hour</a></li>-->
                            <li><a href="#" onclick="ChangePendingDuration(1)">1 Day</a></li>
                            <li><a href="#" onclick="ChangePendingDuration(7)">1 Week</a></li>
                            <li><a href="#" onclick="ChangePendingDuration(14)">2 Weeks</a></li>
                            <li><a href="#" onclick="ChangePendingDuration(30)">1 Month</a></li>
                            <li><a href="#" onclick="ChangePendingDuration(90)">3 Months</a></li>
                            <li><a href="#" onclick="ChangePendingDuration(180)">6 Months</a></li>
                            <li><a href="#" onclick="ChangePendingDuration(365)">1 Year</a></li>
                        </ul>
                    </div>
<!--                    Authorized By-->
                   <div class="btn-group">
                        <button class="btn btn-small"><div id="_Authorizer_Units-user"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="ChangeUnit(1)">Same Units</a></li>
                            <li><a href="#" onclick="ChangeUnit(2)">Different Units</a></li>

                        </ul>
                    </div>
                 Authorization Per Unit(HW tokens only)
                   <div class="btn-group">
                        <button class="btn btn-small"><div id="_UnitAutherization" name='_UnitAutherization'></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="ChangeUnitAutherization(1)">Enable</a></li>
                            <li><a href="#" onclick="ChangeUnitAutherization(2)">Disable</a></li>

                        </ul>
                    </div>
                

                    <!--</div>-->
                </div>
            </div>
           
            <div class="control-group">
                <label class="control-label"  for="username">OCRA Specification</label>
                <div class="controls">
                    Plain OTP
                    <input type="text" id="_otpspecification" name="_otpspecification" placeholder="OCRA-1\:HOTP-SHA1-6\:QN08" class="span2">
                    , Challenge Response OTP
                    <input type="text" id="_certspecification" name="_certspecification" placeholder="OCRA-1\:HOTP-SHA1-8\:QA08" class="span2">
                    and Signature OTP
                    <input type="text" id="_ocraspecification" name="_ocraspecification" placeholder="OCRA-1\:HOTP-SHA1-8\:QA08" class="span2">
                </div>
            </div>
            <hr>
            <div class="control-group">
                <label class="control-label"  for="username">Geo-Location</label>
                <div class="controls">
                    <input type="text" id="_locationclassName" name="_locationclassName" placeholder="complete class name including package"  class="span4">
                    as implementation class for finding location using geo-coordinates. 
                </div>

            </div>
            <hr>
            <div class="control-group" style="display:none">
                <!--<label class="control-label"  for="username">System Configuration</label>-->
                <label class="control-label"  for="username">Server Restriction</label>
                <div class="controls">
                    <select class="span5" name="_serverRestriction" id="_serverRestriction" style="width:20%">
                        <option value="1" >Enable</option>
                        <option value="0" >Disable</option>
                    </select>
                    Day Restriction
                    <select class="span5" name="_dayrestriction" id="_dayrestriction" style="width:20%">
                        <option value="1" >Week Days Only</option>
                        <option value="2" selected>Whole Week (including weekend)</option>
                    </select>
                    For Time Range
                    <input type="text" id="_timerange" name="_timerange" placeholder="1" value="3" class="span1" >
                    <select class="span3" name="_timefromampm" id="_timefromampm" style="width:5%" class="span1">
                        <option value="1" selected>AM</option>
                        <option value="2" >PM</option>
                    </select>
                    TO
                    <input type="text" id="_totimerange" name="_totimerange" placeholder="12" value="12" class="span1">
                    <select class="span3" name="_timetoampm" id="_timetoampm" style="width:5%">
                        <option value="1" >AM</option>
                        <option value="2" selected>PM</option>
                    </select> 
                </div>
            </div>

            <div class="control-group" style="display:none">
                <!--<label class="control-label"  for="username">System Configuration</label>-->
                <label class="control-label"  for="username">Multiple Session</label>
                <div class="controls">
                    <select class="span5" name="_multipleSession" id="_multipleSession" style="width:20%">
                        <option value="1" >Enable</option>
                        <option value="0" >Disable</option>
                    </select>
               
                <!--<label class="control-label"  for="username">User Validity Days</label>-->
              
<!--                <div class="controls">
                    
                </div>-->
            </div>
            </div>


            <%//if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>
            <div class="control-group">
                <div class="controls">
                    <div id="channel-profile-settings-result"></div>
                    <button class="btn btn-primary" onclick="editChannelprofileSettings()" type="button">Save Channel Settings Now >> </button>
                </div>
            </div>

            <%//}%>

        </form>
    </div>


    <script language="javascript" type="text/javascript">
        LoadChannelprofileSettings();
    </script>
</div>


<%@include file="footer.jsp" %>