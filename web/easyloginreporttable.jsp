<%@page import="java.util.concurrent.TimeUnit"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.axiom.nucleus.db.ApEasylogin"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.EasyLoginManagement"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>

<script src="./assets/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="./assets/css/jquery.dataTables.min.css">
<script src="assets/DataTable/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>


<%
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();

    String _startdate = request.getParameter("_startdate");
    String _enddate = request.getParameter("_enddate");
    String _searchtext = request.getParameter("_searchtext");
    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    DateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
    Date startdate = null;
    ApEasylogin[] txdetailses = null;
    if (_startdate != null && !_startdate.isEmpty()) {
        startdate = (Date) formatter.parse(_startdate);
    }
    Date enddate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        enddate = (Date) formatter.parse(_enddate);
    }
    if (startdate != null && startdate.equals(enddate)) {
        enddate = new Date(startdate.getTime() + TimeUnit.DAYS.toMillis(1));
    }
    if (startdate == null || enddate == null) {
        txdetailses = new EasyLoginManagement().getAllEasyLoginByDate(sessionId, _channelId, startdate, enddate);
    } else {
        txdetailses = new EasyLoginManagement().getAllEasyLoginByUser(sessionId, _channelId, startdate, enddate, _searchtext);
    }
    session.setAttribute("easylogindetailses", txdetailses);
    String strmsg = "No Record Found";
%>
<div class="container-fluid">

    <table class="display responsive wrap" id="LoginTable" >
        <thead style="text-align: center">
            <tr>
                <th><b>No.</th>
                <th><b>User Name</th>
                <th><b>IP</th>
                <th><b>Latitude</th>
                <th><b>Longitude</th>
                <th><b><b>Device Details</th>
                            <th><b>Session Details</th>
                            <th><b>Status</th>
                            <th><b>Message</th>
                            <th><b>Created On</th>
                            <th><b>Expire On</th>
                            </tr>
                            </thead>
                            <%    if (txdetailses != null && txdetailses.length != 0) {
                                    Map map = new HashMap();
                                    for (int i = 0; i < txdetailses.length; i++) {
                                        String errmessage = "NA";
                                        String deviceId = "NA";
                                        String strStatus = "NA";
                                        String createdOn = "NA";
                                        String expiresOn = "NA";
                                        String deviceName = "NA";
                                        String deviceOS = "NA";
                                        String deviceVersion = "NA";
                                        String[] deviceDetail = null;
                                        if (txdetailses[i].getStatus() == EasyLoginManagement.EXPIRED_STATUS) {
                                            strStatus = "Expired";
                                        } else if (txdetailses[i].getStatus() == EasyLoginManagement.ACTIVE_STATUS) {
                                            strStatus = "Active";
                                        } else if (txdetailses[i].getStatus() == EasyLoginManagement.COMPLETED_STATUS) {
                                            strStatus = "Compeleted";
                                        } else if (txdetailses[i].getStatus() == EasyLoginManagement.PENDING_STATUS) {
                                            strStatus = "Pending";
                                        }
                                        if (txdetailses[i].getErrormsg() != null && !txdetailses[i].getErrormsg().isEmpty()) {
                                            errmessage = txdetailses[i].getErrormsg();
                                        }
                                        if (txdetailses[i].getCreatedon() != null) {
                                            createdOn = format.format(txdetailses[i].getCreatedon());
                                        }
                                        if (txdetailses[i].getExpireson() != null) {
                                            expiresOn = format.format(txdetailses[i].getExpireson());
                                            if (txdetailses[i].getExpireson().before(new Date())) {
                                                txdetailses[i].setStatus(2);
                                                strStatus = "Expired";
                                            }
                                        }

                                        AuthUser user = null;
                                        if (map.isEmpty()) {
                                            user = new UserManagement().getUser(_channelId, txdetailses[i].getUserid());
                                            map.put(txdetailses[i].getUserid(), user);
                                        } else {
                                            if (map.get(txdetailses[i].getUserid()) == null) {
                                                user = new UserManagement().getUser(_channelId, txdetailses[i].getUserid());
                                                map.put(txdetailses[i].getUserid(), user);
                                            } else {
                                                user = (AuthUser) map.get(txdetailses[i].getUserid());
                                            }
                                        }
                                        if (txdetailses[i].getDeviceinfo() != null) {
                                            deviceDetail = txdetailses[i].getDeviceinfo().split(":");
                                        }
                                        if (txdetailses[i].getDeviceid() != null) {
                                            deviceId = txdetailses[i].getDeviceid();
                                        }
                                        if (deviceDetail != null) {
                                            if (deviceDetail.length >= 1) {
                                                deviceOS = deviceDetail[0];
                                            }
                                            if (deviceDetail.length >= 2) {
                                                deviceName = deviceDetail[1];
                                            }
                                            if (deviceDetail.length >= 3) {
                                                deviceVersion = deviceDetail[2];
                                            }
                                        }
                            %>
                            <tr>
                                <td><%=i + 1%></td>
                                <td><%=user.userName%></td>
                                <td><%=txdetailses[i].getIp()%></td>
                                <td><%=txdetailses[i].getLattitude()%></td>
                                <td><%=txdetailses[i].getLongitude()%></td>
                                <td>
                                    <a href="#/" class="btn btn-mini" id="deviceInfoMsgs-<%=i%>"  rel="popover" data-html="true">Device Info</a>
                                    <script>
                                        $(function ()
                                        {
                                            $("#deviceInfoMsgs-<%=i%>").popover({title: 'Device Info', content: "Device Name : <%=deviceName%> <br/> Device OS : <%=deviceOS%> <br/> Device Version : <%=deviceVersion%> <br/> Device Id : <%=deviceId%>"});
                                        });
                                    </script>



                                </td>
                                <td>
                                    <a href="./easyloginSessionDetails.jsp?_easyloginId=<%=txdetailses[i].getEasyloginId()%>&startdate=<%=_startdate%>&enddate=<%=_enddate%>&_user=<%=user.userName%>" target="_blank" class="btn btn-mini" data-toggle="tooltip" title="Details open in new tab">
                                        Session Details
                                    </a>
                                </td>
                                <td>
                                    <%if (txdetailses[i].getStatus() == 0) {%>
                                    <span class=" label label-info"><%=strStatus%></span>
                                    <%}
                                        if (txdetailses[i].getStatus() == 1) {%>
                                    <span class=" label label-success"><%=strStatus%></span>
                                    <%}
                                        if (txdetailses[i].getStatus() == 2) {%>
                                    <span class=" label label-orange"><%=strStatus%></span>
                                    <%}
                                        if (txdetailses[i].getStatus() == -1) {%>
                                    <span class=" label label-warning"><%=strStatus%></span>
                                    <%}%>
                                </td>
                                <td><%=errmessage%></td>
                                <td><%=createdOn%></td>
                                <td><%=expiresOn%></td>

                            </tr>
                            <%
                                }
                            } else {%>
                            <tr>
                                <td><%=strmsg%></td>
                                <td><%=strmsg%></td>
                                <td><%=strmsg%></td>
                                <td><%=strmsg%></td>
                                <td><%=strmsg%></td>
                                <td><%=strmsg%></td>
                                <td><%=strmsg%></td>
                                <td><%=strmsg%></td>
                                <td><%=strmsg%></td>
                                <td><%=strmsg%></td>
                                <td><%=strmsg%></td>
                            </tr>
                            <%}%>
                            </table>

                            <script>
                                $(document).ready(function () {
                                    $('#LoginTable').DataTable({
                                        responsive: true
                                    });
                                });
                            </script> 

                            <div class="row-fluid">   
                                <div class="span6">

                                    <div class="control-group">                        
                                        <div class="span1">
                                            <div class="control-group form-inline">
                                                <a href="#" class="btn btn-info" onclick="downloadeasycheckinReport(1, '<%=_startdate%>', '<%=_enddate%>')" >
                                                    CSV</a>
                                            </div>
                                        </div>
                                        <div class="span1">
                                            <div class="control-group form-inline">
                                                <a href="#" class="btn btn-info" onclick="downloadeasycheckinReport(0, '<%=_startdate%>', '<%=_enddate%>')" >
                                                    PDF</a>
                                            </div>
                                        </div>
                                        <div class="span1">
                                            <div class="control-group form-inline">
                                                <a href="#" class="btn btn-info" onclick="downloadeasycheckinReport(2, '<%=_startdate%>', '<%=_enddate%>')" >
                                                    TEXT</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
