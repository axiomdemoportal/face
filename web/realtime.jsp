<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="java.util.Map"%>
<%
    Channels _apSChannelDetails = (Channels) session.getAttribute("_apSChannelDetails");
    String _settingName = request.getParameter("_settingName");
    int type = Integer.parseInt(request.getParameter("_type"));
%>
<script src="./assets/js/jquery.js"></script>
<script src="./assets/js/d3.js"></script>
<script src="./assets/js/epoach.js"></script>
<link rel="stylesheet" type="text/css" href="./assets/css/epoach.css">
<script src="./assets/js/ZeroClipboard.min.js"></script>
<script src="./assets/js/realtimemonitoring.js"></script>

<div id="real-time-area1" class="epoch" style="height: 300px; width: 95%"></div>
<script> loadmsgs('<%=_settingName%>', '<%=type%>');
</script>

