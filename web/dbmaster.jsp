<%@page import="com.mollatech.axiom.common.utils.UtilityFunctions"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.settings.ContactTagsSetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Templates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/replication.js"></script>
<div class="container-fluid">
    <h2>Master DB Management</h2>
    <p>This instance is configured to run as master. Please use the below information for configuring slave(s).</p>
    <br>
    <div class="row-fluid">
        <form id="ConfigMasterForm" name="ConfigMasterForm">
            <fieldset>                
                <div class="control-group">
                    <legend>Step 1: Enter Slave IP below</legend>
                    <div class="controls">
                        <input type="text" id="_slaveip" name="_slaveip" placeholder="Provide slave server ip" class="span4">
                    </div>
                </div>                
                <div class="control-group">                    
                    <div class="controls">
                        <button class="btn btn-primary" onclick="ConfigMaster()" id="idConfigMasterButton" type="button">Add Slave</button>
                        <div id="db-replica-master-result"></div>
                    </div>
                </div>
            </fieldset>
            <input type="hidden" id="_dnid" name="_dnid">
        </form>      
        <hr>
        <fieldset>       
            <legend>Step 2: Master Details for Slave</legend>
            <div class="control-group">
                <label class="control-label" for="username">Data File ID:</label>
                <div class="controls">
                    <div id="master-file-id"></div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"  for="username">Data File Index:</label>
                <div class="controls">
                    <div id="master-file-index"></div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"  for="username">Data Image File:</label>
                <div class="controls">

                    <button class="btn btn-file" onclick="DownloadMasterDBCopy()" type="button" id="idDownloadMasterDBCopyButton">Download File >></button>                        
                </div>
            </div>
        </fieldset> 

    </div>

    <script>
        StartStateDBMaster()
    </script>

    <%@include file="footer.jsp" %>