<%@ page import="java.io.*" %>
<%@ page import="org.apache.commons.fileupload.*"%>
<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@ page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@ page import="org.apache.commons.io.FilenameUtils"%>
<%@ page import="org.apache.commons.fileupload.*"%>
<%@ page import="java.util.*,java.io.File,java.lang.Exception,java.io.*,java.net.*" %>

<%
    String strError = "";
    String saveFile = "";
    String savepath = "";

    savepath = System.getProperty("catalina.home");
    if(savepath == null) { savepath = System.getenv("catalina.home"); }
    savepath += System.getProperty("file.separator");
    savepath += "axiomv2-settings";
    savepath += System.getProperty("file.separator");
    savepath += "uploads";
    savepath += System.getProperty("file.separator");
    

    String strUniqueID = new String(request.getSession().getId());
    strUniqueID = strUniqueID.substring(0, 8);

    String optionalFileName = "";
    FileItem fileItem = null;
    String[] files = new String[1];	 // file names
    String dirName = savepath;

    int i = 0;

    if (ServletFileUpload.isMultipartContent(request)) {
        ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
        List fileItemsList = servletFileUpload.parseRequest(request);
        Iterator it = fileItemsList.iterator();

        while (it.hasNext()) {
            FileItem fileItemTemp = (FileItem) it.next();
            if (fileItemTemp.isFormField()) {
                if (fileItemTemp.getFieldName().equals("filename")) {
                    optionalFileName = fileItemTemp.getString();
                } else {
                    System.out.println("Additional fields in file upload form is  " + fileItemTemp.getFieldName());
                }
            } else {
                fileItem = fileItemTemp;
            }
            if (fileItem != null) {
                String fileName = fileItem.getName();
                if(fileItem.getSize() == 0){
                           strError = "Please Select File To Upload...!!!";
                    break;
                        }
                if (fileItem.getSize() > 0 && fileItem.getSize() < 1024000) { // size cannot be more than 65Kb. We want it light.
                    if (optionalFileName.trim().equals("")) {
                        fileName = FilenameUtils.getName(fileName);
                    } else {
                        fileName = optionalFileName;
                    }
                    files[i++] = dirName + fileName;
                    File saveTo = new File(dirName + fileName);

                    saveFile = fileName;

                    try {
                        fileItem.write(saveTo);
                         session.setAttribute("_filepath", saveTo.getAbsolutePath());
                    } catch (Exception e) {
                      e.printStackTrace();
                    }
                } else {
                    strError = "Error: " + fileName + " size is more than 1MB. Please upload lighter files.";
                    break;
                }
            }
        }
    }

    try {
        //boolean success = (new File(files[0])).delete();
        //System.out.println(files[0] + " deleted successfully");
        
        //session.setAttribute("_filepath", files[0]);

    } catch (Exception e) {
        e.getMessage();
        strError = e.getMessage();
    }
    out.clear();
    //response.setContentType("application/json");
%>
{error:'<%=strError%>',msg: '<%=saveFile%>',filename: '<%=(strUniqueID + "_" + saveFile)%>'}
<% out.flush();%>