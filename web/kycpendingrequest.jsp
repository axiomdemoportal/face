<%@page import="com.mollatech.axiom.nucleus.db.Kyctable"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.KYCManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Certificates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Pkitokens"%>
<%@page import="com.mollatech.axiom.connector.user.TokenStatusDetails"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.Otptokens"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/kyc.js"></script>
<script src="./assets/js/caconnector.js"></script>
<script src="./assets/js/ajaxfileupload.js"></script>
<script src="./assets/js/bootstrap-fileupload.js"></script> 
<link href="./assets/css/bootstrap.css" rel="stylesheet">
<link href="./assets/css/bootstrap-fileupload.css" rel="stylesheet">
<script src="./assets/js/otptokens.js"></script>
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<script src="./assets/js/globalsetting.js"></script>
<%    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();

    KYCManagement kManagement = new KYCManagement();
    Kyctable[] kObjList = kManagement.getKyctableByStatus(sessionid, channel.getChannelid(), CertificateManagement.KYC_PENDING);


%>
<div class="container-fluid">
    <div id="auditTable">

        <h1 class="text-success">KYC Management</h1>
        <p>List of KYC Pending request and their Management.</p>
        <%            if (kObjList != null) {
        %>
        <div class="row-fluid"> 
            <div id="licenses_data_table"> 
                <table class="table table-striped">

                    <tr>
                        <td>No.</td>
                        <td>Name</td>
                        <td>Mobile</td>
                        <td>Email</td>
                        <td>Status</td>
                        <td>Approve</td>
                        <td>Reject</td>
                        <td>KYC Documents </td>
                        <td>Uploaded On</td>
                        <td>Updated On</td>
                        <td></td>
                    </tr>

                    <%
                        if (kObjList != null)
                            for (int i = 0; i < kObjList.length; i++) {
                                //System.out.println("userid   :: " + kObjList[i].getUserid());
                                java.util.Date dUploadDate = null;
                                java.util.Date dLastUpdated = null;

                                SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm ");

                                UserManagement uManagement = new UserManagement();
                                AuthUser aUser = uManagement.getUser(sessionId, channel.getChannelid(), kObjList[i].getUserid());

                                String strStatus = "Not-Avialable";
                                if (kObjList[i] != null) {
                                    if (kObjList[i].getCheckValid() == CertificateManagement.KYC_APPROVED) {
                                        strStatus = "APPROVED";
                                    } else if (kObjList[i].getCheckValid() == CertificateManagement.KYC_NEEDMOREDOCS) {
                                        strStatus = "NEEDMOREDOCS";
                                    } else if (kObjList[i].getCheckValid() == CertificateManagement.KYC_PENDING) {
                                        strStatus = "PENDING";
                                    } else if (kObjList[i].getCheckValid() == CertificateManagement.KYC_REJECTED) {
                                        strStatus = "KYC_REJECTED";
                                    }

                                    dUploadDate = kObjList[i].getUploadeddate();
                                    dLastUpdated = kObjList[i].getUpdateddate();

                                }

                                String issuedOn = "NA";
                                if (dUploadDate != null) {
                                    issuedOn = sdf.format(dUploadDate);
                                }

                                String ExpireOn = "NA";
                                if (dLastUpdated != null) {
                                    ExpireOn = sdf.format(dLastUpdated);
                                }


                    %>
                    <tr id="user_search_<%=kObjList[i].getUserid()%>">
                        <td><%=(i + 1)%></td>
                        <td><%=aUser.getUserName()%></td>  
                        <td><%=aUser.getPhoneNo()%></td>
                        <td><%=aUser.getEmail()%></td>
                        <td><%=strStatus%></td>
                        <%if (0 == 0) {%>
                        <td>
                            <button href="#" onclick="approve('<%=aUser.getUserId()%>')" class="btn btn-success">Approve</button>
                        </td> 
                        <td>
                            <button href="#" onclick="reject('<%=aUser.getUserId()%>')" class="btn btn-danger" >Decline</button>
                        </td>


                        <td>
                            <button href="#" onclick="downloadkycfiles('<%=aUser.getUserId()%>')" class="btn btn-info" >Download</button>
                        </td>

                        <td><%=kObjList[i].getUploadeddate()%></td>
                        <td><%=kObjList[i].getUpdateddate()%></td>
                        <% } else { %>
                        <td><span class="label label-inverse">Not Available</span></td>
                        <td><span class="label label-inverse">Not Available</span></td>
                        <td><span class="label label-inverse">Not Available</span></td>
                        <td><span class="label label-inverse">Not Available</span></td>
                        <td>--</td>
                        <td>--</td>
                        <td>--</td>

                        <% } %>

                    </tr>
                    <%
                        }%>
                </table>
            </div></div>

        <%  } else if (kObjList == null) { %>
        <h3>No Record found...</h3>
        <%}%>

    </div>
</div>
<div id="kycreject" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">KYC Reject Form</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="kycrejectform" name="kycrejectform">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_usersid" name="_usersid" >
                    <div class="control-group">
                        <label class="control-label"  for="username">Reason</label>
                        <div class="controls">
                            <select class="span7" name="authenticationtype" id="authenticationtype">
                                <option value="Invaliddoc" >Fake Documents</option>
                                <option value="Needmoredoc" >Need More Documents</option>
                                <option value="other" >Other</option>
                                <!--<option value="both" >Password+OTP </option>-->
                            </select>
                        </div>
                    </div>  
                    <div class="control-group">
                        <label class="control-label"  for="username">Description</label>
                        <div class="controls">
                            <textarea class="form-control" rows="3" id="_description" name="_description"></textarea>

                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="rejectkyc-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="rejectRequest()" id="addUserButton">Apply Now>></button>
    </div>
</div>
<br><br>
<%@include file="footer.jsp" %>