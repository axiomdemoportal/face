<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Epintracker"%>
<%@page import="com.mollatech.ecopin.management.EPINManagement"%>
<%@page import="com.mollatech.axiom.nucleus.settings.SendNotification"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="java.text.SimpleDateFormat"%>


<%
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
     String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    String _searchtext = request.getParameter("_searchtext");
    String _startdate = request.getParameter("_startdate");
    String _enddate = request.getParameter("_enddate");
    String _type = request.getParameter("_type");
    
    if ( _type == null || _type.isEmpty() == true)  {
        _type = "" + SendNotification.SMS;
    }    

    DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    Date startDate = null;
    if (_startdate != null && !_startdate.isEmpty()) {
        startDate = (Date) formatter.parse(_startdate);
    }
    Date endDate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        endDate = (Date) formatter.parse(_enddate);
    }
    EPINManagement eMngt = new EPINManagement();
    UserManagement uMngt = new UserManagement();
 
    int type = Integer.parseInt(_type);
    Epintracker[] epinobj = eMngt.searchEPINObj(channel.getChannelid(), type, _searchtext, endDate, startDate);
  //   AuthUser[] arruser =  uMngt.SearchUsers(sessionId, channel.getChannelid(), value);
    String start = null;
    if(startDate != null){
      start = formatter.format(startDate);
    }
    String end = null;
    if(endDate != null){
      end = formatter.format(endDate);
    }

   
%>


<div class="tabbable">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#msgcharts" data-toggle="tab">Charts</a></li>
        <li><a href="#msgreport" data-toggle="tab">Tabular List</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="msgcharts">
            <div class="row-fluid">
                <div class="span12">
                    <div class="control-group">
                        <div class="span4">
                            <div id="MsgReportgraph" ></div>
                            Donut Chart
                        </div>
                        <div class="span7">
                            <div id="MsgReportgraph1"></div>
                            Bar Chart   
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="tab-pane" id="msgreport">   
            <div class="row-fluid">
                <div class="span6">
                    <div class="control-group">                        
                        <div class="span3">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="ecopinReportCSV()" >
                                    <i class="icon-white icon-chevron-down"></i>Download CSV</a>
                            </div>
                        </div>
                        <div class="span3">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="ecopinReportPDF()" >
                                    <i class="icon-white icon-chevron-down"></i>Download PDF</a>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped" id="table_main">

                            <tr>
                                <td>No.</td>
                                <td>User Name</td>
                                <td>Phone</td>
                                <td>Email</td>
                                <td>Status</td>
                                 <td>Type</td>
                                 <td>Expiry In Minutes</td>
                                <td>Requested On</td>
                                <td>Sent On</td>
                            </tr>
                            <% if (epinobj != null) {
                                    for (int i = 0; i < epinobj.length;i++) {
                                        AuthUser user = uMngt.getUser(sessionId, channel.getChannelid(), epinobj[i].getUserid());

                                          String strstatus = "-";
                                        if(epinobj[i].getStatus() == EPINManagement.APPROVED ){
                                            strstatus = " Delivered";
                                        }else if(epinobj[i].getStatus() == EPINManagement.FAILEDTOVALIDATE){
                                            strstatus = "Invalid";
                                        }else if(epinobj[i].getStatus() == EPINManagement.EXPIRED){
                                            strstatus = "Expired";
                                        }else if(epinobj[i].getStatus() == EPINManagement.ALLOWED){
                                            strstatus = "Allowed";
                                        }else if(epinobj[i].getStatus() == EPINManagement.NOT_ALLOWED){
                                            strstatus = "Dropped";
                                        }else if(epinobj[i].getStatus() == EPINManagement.FAILED){
                                            strstatus = "Not  delivered";
                                        }else if(epinobj[i].getStatus() == EPINManagement.REJECTED){
                                            strstatus = "Rejected";
                                        }
                                        
                                        String strtype = "-";
                                        if(epinobj[i].getType() == EPINManagement.OPERATOR_CONTROLLED){
                                            strtype = "Operator Controlled";
                                        }else if(epinobj[i].getType() == EPINManagement.DELIVERY){
                                            strtype = "Delivery";
                                        }else if(epinobj[i].getType() == EPINManagement.POLICY_CHECK){
                                            strtype = "Policy Check";
                                        }else if(epinobj[i].getType() == EPINManagement.VALIDATION){
                                            strtype = "Validation";
                                        }
                                        
                                       
                                        Calendar current = Calendar.getInstance();
                                        
                                          current.setTime( epinobj[i].getRequestdatetime());
                                          current.add(Calendar.MINUTE, epinobj[i].getExpiryInMins());
                                          Date expirytime = current.getTime();
                                         String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";
                                          SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
                                          String extime = sdf.format(expirytime);
                                          
                                            String senttime = "-";
                                          if(epinobj[i].getSentdatetime() != null){
                                              senttime = sdf.format(epinobj[i].getSentdatetime());
                                          }
                            %>
                            <tr>
                                <td><%=(i + 1)%></td>
                                <td><%=user.getUserName()%></td> 
                                <td><%=epinobj[i].getPhone()%></td> 
                                <td><%=epinobj[i].getEmailid()%></td> 
                                <td><%=strstatus%></td>
                                <td><%=strtype%></td>  
                                <td><%=extime%></td>  
                                <td><%=epinobj[i].getRequestdatetime()%></td>  
                                <td><%=senttime %></td>  
                            </tr>
                            <%}
                            } else {%>
                            <tr>
                            <td><%=1%></td>
                            <td>No Records Found</td>
                            <td>No Records Found</td>
                            <td>No Records Found</td> 
                            <td>No Records Found</td>
                            <td>No Records Found</td>
                            <td>No Records Found</td>
                            <td>No Records Found</td>
                            <td>No Records Found</td>
                             <%}%>
                             </tr>
                        </table>
                            
                 <div class="span6">
                    <div class="control-group">                        
                        <div class="span3">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="ecopinReportCSV()" >
                                    <i class="icon-white icon-chevron-down"></i>Download CSV</a>
                            </div>
                        </div>
                        <div class="span3">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="ecopinReportPDF()" >
                                    <i class="icon-white icon-chevron-down"></i>Download PDF</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br><br>