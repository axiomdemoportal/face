<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SchedulerManagement"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.AxiomProtect"%>
<%@page import="com.mollatech.axiom.nucleus.settings.NucleusMonitorSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.Monitorsettings"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Monitortracking"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.MonitorSettingsManagement"%>
<%@include file="header.jsp" %>
<!--<script src="./assets/js/jquery.js"></script>-->
<script src="./assets/js/addSettings.js"></script>
<script src="./assets/js/bootstrap-datepicker.js"></script>
<script src="./assets/js/realtime.js"></script>
<link rel="stylesheet" href="./assets/css/datepicker.css">
<%!
    public String ChechFor = "";
%> 
<%
    Channels _apSChannelDetails = (Channels) session.getAttribute("_apSChannelDetails");
    String _settingName = request.getParameter("_settingName");
    int type = Integer.parseInt(request.getParameter("type"));
    Map settingsMap = (Map) session.getAttribute("setting");
    Monitorsettings monitorsettings = null;
    monitorsettings = (Monitorsettings) settingsMap.get(_settingName);
    NucleusMonitorSettings nms = null;
    if (monitorsettings != null) {
        byte[] obj = monitorsettings.getMonitorSettingEntry();
        byte[] f = AxiomProtect.AccessDataBytes(obj);
        ByteArrayInputStream bais = new ByteArrayInputStream(f);
        Object object = SchedulerManagement.deserializeFromObject(bais);
        //NucleusMonitorSettings nms = null;
        if (object instanceof NucleusMonitorSettings) {
            nms = (NucleusMonitorSettings) object;
        }
    }
    MonitorSettingsManagement management = new MonitorSettingsManagement();
    Monitortracking[] monitortrackings = management.getMonitorTrackingByName(_apSChannelDetails.getChannelid(), _settingName);
%>
<div class="container-fluid">
    <div class="row-fluid">
        <h2 class="text-success">Reports for <%=_settingName%></h2>
        <div class="row-fluid">
            <div class="span12">
                <div class="control-group form-inline">
                    <div class="input-prepend">
                        <span class="add-on">From:</span>   
                        <div id="datetimepicker1" class="input-append date">
                            <input id="startdate" name="startdate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                            <span class="add-on">
                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                            </span>
                        </div>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on">till:</span>   
                        <div id="datetimepicker2" class="input-append date">
                            <input id="enddate" name="enddate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                            <span class="add-on">
                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                            </span>
                        </div>
                    </div>
                    <button class="btn btn-success" onclick="searchReport('<%=_settingName%>', '<%=type%>')" id="addUserButton">Generate Report</button>
                </div>                           
            </div>
        </div>
        <script>
            recentreport('<%=_settingName%>', '<%=type%>');
        </script>
        <div id="tabHome"></div>                
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datepicker({
                    format: 'dd/MM/yyyy',
                    language: 'pt-BR'
                });
            });
            $(function () {
                $('#datetimepicker2').datepicker({
                    format: 'dd/MM/yyyy',
                    language: 'pt-BR'
                });
            });
        </script>
    </div>
</div>
<%@include  file="footer.jsp" %>