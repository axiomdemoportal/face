<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/jquery.js"></script>
<script src="./assets/js/usermanagement.js"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">
<link rel="stylesheet" href="./assets/css/datepicker.css">
<script src="./assets/js/bootstrap-datepicker.js"></script>
<div class="container-fluid">
    <h1 class="text-success">Users Reports</h1>
    <h3>Make Your Own Report</h3>
    <input type="hidden" id="_changeStatus" name="_changeStatus">

    <div class="row-fluid">
        <div class="span12">
            <div class="control-group form-inline">                
                <div class="input-append">
                    <span class="add-on">From:</span>   
                    <div id="datetimepicker1" class="input-append date">
                        <input id="userstartdate" name="userstartdate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth">
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>

                </div>
                <div class="input-append">
                    <span class="add-on">To:</span>   
                    <div id="datetimepicker2" class="input-append date">
                        <input id="userenddate" name="userenddate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth">
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                </div>    
                <div class="input-prepend">
                    <span class="add-on " >Enter </span><input id="_searchtext" name="_searchtext" class="" type="text" placeholder="name/phone/email" data-bind="value: vm.ActualDoorSizeDepth" />
                </div>
                <div class="input-append">
                    <% //Operators oprObj = (Operators) session.getAttribute("_apOprDetail");
                        if (oprObj.getRoleid() != 1) {//1 sysadmin
                            if (accessObj != null && accessObj.viewUserReport == true) {%>
                    <button class="btn btn-success " onclick="searchUserReport()" type="button">Search Now</button>
                    <%} else {%>
                    <button class="btn btn-success " onclick="InvalidRequest('userreport')" type="button">Search Now</button>
                    <%}
                    } else {%>
                    <button class="btn btn-success " onclick="searchUserReport()" type="button">Search Now</button>
                    <%}%>
                </div>
                <i>For List only, select Status: </i> 
                <div class="btn-group">
                    <button class="btn"><div id="_change-status-Axiom"></div></button>
                    <button class="btn" data-toggle="dropdown"><span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="#" onclick="ChangeStatusType(99)">Show All</a></li>
                        <li><a href="#" onclick="ChangeStatusType(1)">Active Only</a></li>
                        <li><a href="#" onclick="ChangeStatusType(0)">Suspended Only</a></li>
                        <li><a href="#" onclick="ChangeStatusType(-1)">Locked Only</a></li>
                        <li><a href="#" onclick="ChangeStatusType(-99)">Removed Only</a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>



    <div id="licenses_data_table">
        <h3>Recent Users</h3>

        <table class="table table-striped" id="table_main">

            <tr>
                <td>No.</td>
                <td>User ID</td>
                <td>Name</td>
                <td>Mobile</td>
                <td>Email</td>
                <td>Status</td>
                <td>Created On</td>
                <td>Last Access On</td>
            </tr>

            <%
                final int ACTIVE_STATUS = 1;
                final int SUSPEND_STATUS = 0;
                final int LOCKED_STATUS = -1;
                final int REMOVE_STATUS = -99;
                Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                String sessionId = (String) request.getSession().getAttribute("_apSessionID");
                UserManagement uMngt = new UserManagement();
                int istatus = 1;
                int limit = 100;
                AuthUser[] user = uMngt.getUserObjByStatus(sessionId, channel.getChannelid(), limit);
                String strerr = "No Record Found";

                if (user != null) {

                    for (int i = 0; i < user.length; i++) {

                        String strStatus = null;
                        String strStatusColor = "success";
                        if (user[i].getStatePassword() == ACTIVE_STATUS) {
                            strStatus = "ACTIVE";
                        } else if (user[i].getStatePassword() == LOCKED_STATUS) {
                            strStatus = "LOCKED";
                            strStatusColor = "inverse";
                        } else if (user[i].getStatePassword() == SUSPEND_STATUS) {
                            strStatus = "SUSPENDED";
                            strStatusColor = "warning";
                        } else if (user[i].getStatePassword() == REMOVE_STATUS) {
                            strStatus = "REMOVED";
                            strStatusColor = "important";
                        }

                        Date date = new Date(user[i].lCreatedOn);
                        SimpleDateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                        String strCreatedOn = df2.format(date);

                        Date date1 = new Date(user[i].lLastAccessOn);

                        String strLastAccessOn = df2.format(date1);
            %>
            <tr>
                <td><%=(i + 1)%></td>
                <td><%= user[i].getUserId()%></td>
                <td><%= user[i].getUserName()%></td>
                <td><%=user[i].getPhoneNo()%></td>
                <td><%=user[i].getEmail()%></td>                
                <td><span class="label label-<%=strStatusColor%>"><%=strStatus%></span></td>
                <td><%=strCreatedOn%></td>
                <td><%=strLastAccessOn%></td>

            </tr>
            <%}
            } else {%>
            <tr>
                <td><%=1%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
                <td><%= strerr%></td>
            </tr>
            <%}%>
        </table>
    </div>
    <script type="text/javascript">
        ChangeStatusType(99);
    </script>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            });
        });
        $(function () {
            $('#datetimepicker2').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'

            });
        });

    </script>
</div>

<%@include file="footer.jsp" %>
