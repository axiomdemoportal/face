<%@page import="com.mollatech.axiom.nucleus.settings.ContentFilterSetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@include file="header.jsp" %>
<%
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");

    SettingsManagement smng = new SettingsManagement();
    ContentFilterSetting cfObj = (ContentFilterSetting) smng.getSetting(sessionId, channel.getChannelid(), SettingsManagement.CONTENT_FILTER, SettingsManagement.PREFERENCE_ONE);
    
    int alertstatus = 1;
    
    
%>

<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<script src="./assets/js/contentfilter.js"></script>
<div class="container-fluid">
    <h1 class="text-success">Content Filter</h1>
    <p>You can enforce content filter on all messages (via mobile or email) going out of the system. The content filter is applied at real time messages to ensure proper business conduct however do not make the list of keywords too long as it will slow down the system.</p>

    <div class="row-fluid">
        <form class="form-horizontal" id="contentfilterform" name="contentfilterform">

            <div class="control-group">
                <label class="control-label"  for="username">Keywords</label>
                <div class="controls">
                    <textarea name="_keywords" id="_keywords" style="width:100%">
                        <%
                            String[] strKeywords;
                            int iStatus = 1;
                            if (cfObj == null) {
                                strKeywords = new String[3];
                                strKeywords[0] = "dog";
                                strKeywords[1] = "cat";
                                strKeywords[2] = "eggs";
                            } else {
                                strKeywords = cfObj.getContent();
                                iStatus = cfObj.getStatus();
                                alertstatus = cfObj.getAlertstatus();
                            }
                            if (strKeywords != null)
                                for (int j = 0; j < strKeywords.length; j++) {
                        %>        
                        <%=strKeywords[j]%>,
                        <%
                            }
                        %>
                    </textarea>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"  for="username">Enable Filtering?</label>
                <div class="controls">
                    <select class="span4" name="_statusContent" id="_statusContent">
                        <% if (iStatus == 0) {%> 
                        <option value="0" selected>Enable Filtering</option>
                        <option value="1">Disable Filtering</option>
                        <% } else {
                        %>
                        <option value="0" >Enable Filtering</option>
                        <option value="1" selected>Disable Filtering</option>
                        <%                            }
                        %>

                    </select>
                </div>
            </div>
                        
                        <div class="control-group">
                <label class="control-label"  for="username">Content Blocked Action</label>
                <div class="controls">
                    <select class="span4" name="_alertOperator" id="_alertOperator">
                        <% if ( alertstatus == 0 ) { %> 
                        <option value="0" selected>Yes, Alert Administrator</option>
                        <option value="1">No, Do Not Alert Administrator</option>
                        <% } else { 
                        %>
                        <option value="0" >Yes, Alert Administrator</option>
                        <option value="1" selected>No, Do Not Alert Administrator</option>
                        <%
                         }
                        %>
                        
                    </select>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <div id="save-epin-settings-result"></div>
                    <button class="btn btn-primary" onclick="SaveContentFilerList()" type="button" id="SaveContentFilteringButton">Save Changes Now >> </button>
                </div>
            </div>

        </form>
    </div>


    <script language="javascript" type="text/javascript">
        //LoadGlobalSettings();        
    </script>
</div>


<script>    
    //$(document).ready(function() { $("#_keywords").select2() }); 
    
    $(document).ready(function() { 
        $("#_keywords").select2({
            tags:[],
            dropdownCss: {display: 'none'},
            tokenSeparators: [","]
        });                   
    }); 
</script>

<%@include file="footer.jsp" %>