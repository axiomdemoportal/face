<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Hashtable"%>
<%@page import="com.mollatech.axiom.nucleus.settings.PasswordPolicySetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Templates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/passwordPolicy.js"></script>
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>

<div class="container-fluid">
    <h1 class="text-success">Password Policy Settings</h1>
    <p>To facilitate password policy enforcement for both users and operators, you can use these settings. The features like assign, reset, change etc will follow these settings.</p>
    <div class="row-fluid">
        <form class="form-horizontal" id="passwordPolicysettingsform" name="passwordPolicysettingsform">

            <input type="hidden" id="_passwordLength" name="_passwordLength">
            <input type="hidden" id="_passwordExpiryTime" name="_passwordExpiryTime">
            <input type="hidden" id="_passwordIssueLimit" name="_passwordIssueLimit">
            <input type="hidden" id="_enforceDormancyTime" name="_enforceDormancyTime">
            <input type="hidden" id="_passwordInvalidAttempts" name="_passwordInvalidAttempts">
            <input type="hidden" id="_changePasswordAfterLogin" name="_changePasswordAfterLogin">
            <input type="hidden" id="_epinResetReissue" name="_epinResetReissue">
            <input type="hidden" id="_crResetReissue" name="_crResetReissue">
            <input type="hidden" id="_oldPasswordReuseTime" name="_oldPasswordReuseTime">
            <input type="hidden" id="_passwordType" name="_passwordType">

            <input type="hidden" id="_allowRepetationCharacter" name="_allowRepetationCharacter">
            <input type="hidden" id="_genearationPassword" name="_genearationPassword">
            <input type="hidden" id="_alertUserPassword" name="_alertUserPassword">
            <input type="hidden" id="_blockCommonWords" name="_blockCommonWords">
            <input type="hidden" id="_allowAlert" name="_allowAlert">
            <input type="hidden" id="_gatewayType" name="_gatewayType">
            <input type="hidden" id="_alertAttempt" name="_alertAttempt">
            <input type="hidden" id="_allowAlertFor" name="_allowAlertFor">

            <hr>
            <div class="control-group">
                <label class="control-label"  for="username">Generate Password</label>
                <div class="controls">
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_passwordType_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="GeneratePassword(1, '#_passwordType_div', 'Numeric')">Numeric</a></li>
                            <li><a href="#" onclick="GeneratePassword(2, '#_passwordType_div', 'AlphaNumeric')">AlphaNumeric</a></li>
                            <li><a href="#" onclick="GeneratePassword(3, '#_passwordType_div', 'AlphaNumeric With Special Characters')">AlphaNumeric With Special Characters</a></li>

                        </ul>
                    </div>
                    , Password length as
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_passwordLength_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="SetPasswordLength(8, '#_passwordLength_div', '8 Characters')">8 Characters</a></li>
                            <li><a href="#" onclick="SetPasswordLength(10, '#_passwordLength_div', '10 Characters')">10 Characters</a></li>
                            <li><a href="#" onclick="SetPasswordLength(12, '#_passwordLength_div', '12 Characters')">12 Characters</a></li>
                            <li><a href="#" onclick="SetPasswordLength(14, '#_passwordLength_div', '14 Characters')">14 Characters</a></li>
                            <li><a href="#" onclick="SetPasswordLength(16, '#_passwordLength_div', '16 Characters')">16 Characters</a></li>
                        </ul>
                    </div>
                    , Password Expiry Time 
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_passwordExpiryTime_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="SetPasswordExpiryTime(30, '#_passwordExpiryTime_div', '30 Days')">30 Days</a></li>
                            <li><a href="#" onclick="SetPasswordExpiryTime(60, '#_passwordExpiryTime_div', '60 Days')">60 Days</a></li>
                            <li><a href="#" onclick="SetPasswordExpiryTime(90, '#_passwordExpiryTime_div', '90 Days')">90 Days</a></li>
                            <li><a href="#" onclick="SetPasswordExpiryTime(120, '#_passwordExpiryTime_div', '120 Days')">120 Days</a></li>
                            <li><a href="#" onclick="SetPasswordExpiryTime(180, '#_passwordExpiryTime_div', '180 Days')">180 Days</a></li>
                            <li><a href="#" onclick="SetPasswordExpiryTime(99, '#_passwordExpiryTime_div', 'Never')">Never</a></li>
                        </ul>
                    </div>
                    with Invalid Attempts
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_passwordInvalidAttempts_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="SetPasswordAttempts(3, '#_passwordInvalidAttempts_div', '3 Attempts')">3 Attempts</a></li>
                            <li><a href="#" onclick="SetPasswordAttempts(5, '#_passwordInvalidAttempts_div', '5 Attempts')">5 Attempts</a></li>
                            <li><a href="#" onclick="SetPasswordAttempts(7, '#_passwordInvalidAttempts_div', '7 Attempts')">7 Attempts</a></li>
                            <li><a href="#" onclick="SetPasswordAttempts(99, '#_passwordInvalidAttempts_div', 'Disabled')">Disabled</a></li>
                        </ul>
                    </div>

                </div>
            </div>  

            <!--            <hr>-->
            <div class="control-group">

                <div class="controls">
                    Repetation of character
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_allowRepetationCharacter_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="allowRepeatationOfCharacter(1, '#_allowRepetationCharacter_div', 'Enable')">Enable</a></li>
                            <li><a href="#" onclick="allowRepeatationOfCharacter(0, '#_allowRepetationCharacter_div', 'Disable')">Disable</a></li>

                        </ul>
                    </div>
                    Old Password reuse after 
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_genearationPassword_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="generationPassword(6, '#_genearationPassword_div', '6 generations')">6 generations</a></li>
                            <li><a href="#" onclick="generationPassword(8, '#_genearationPassword_div', '8 generations')">8 generations</a></li>
                            <li><a href="#" onclick="generationPassword(10, '#_genearationPassword_div', '10 generations')">10 generations</a></li>
                            <li><a href="#" onclick="generationPassword(12, '#_genearationPassword_div', '12 generations')">12 generations</a></li>
                        </ul>
                    </div>

                    <!--After first Login-->
                </div>
            </div>
            <hr>
            <div class="control-group">
                <label class="control-label"  for="username">  Common Passwords</label>
                <div class="controls">

                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_blockCommonWords_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="blockCommonWords(1, '#_blockCommonWords_div', 'Enable')">Enable</a></li>
                            <li><a href="#" onclick="blockCommonWords(0, '#_blockCommonWords_div', 'Disable')">Disable</a></li>

                        </ul>
                    </div>
                    Common Passwords
                    <textarea name="_arrCommonPassword" id="_arrCommonPassword" style="width:55%">
                        <%                                Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                            SettingsManagement setMngt = new SettingsManagement();
                            Object obj = setMngt.getSettingInner(channel.getChannelid(), SettingsManagement.PASSWORD_POLICY_SETTING, SettingsManagement.PREFERENCE_ONE);
                            PasswordPolicySetting password = null;
                            if (obj != null) {
                                if (obj instanceof PasswordPolicySetting) {
                                    password = (PasswordPolicySetting) obj;
                                }
                            }
                            String[] strTags;
                            if (password == null) {
                                strTags = new String[5];
                                strTags[0] = "abc";
                                strTags[1] = "pqrs";
                                strTags[2] = "hijk";
                                strTags[3] = "123456";
                                strTags[4] = "ilovegod";
                            } else {
                                strTags = password.arrCommonPassword;

                            }
                            if (strTags != null) {
                                for (int j = 0; j < strTags.length; j++) {
                        %>        
                        <%=strTags[j]%>,
                        <%
                                }
                            }
                        %>
                    </textarea>
                    <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;Attention: Please enter comma (",") for separating passwords.

                </div>
            </div>

            <hr>
            <div class="control-group">
                <label class="control-label"  for="username"> Change Password </label>
                <div class="controls">
                    <div class="btn-group">
                        <button class="btn btn-small"><div id="_changePasswordAfterLogin_div"></div></button>
                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="SetChangePasswordAfterLogin(1, '#_changePasswordAfterLogin_div', 'Enable')">Enable</a></li>
                            <li><a href="#" onclick="SetChangePasswordAfterLogin(2, '#_changePasswordAfterLogin_div', 'Disable')">Disable</a></li>

                        </ul>
                    </div>
                    After first Login

                   
                </div>
            </div>

   
    <hr>
    <div class="control-group">
        <label class="control-label"  for="username">Issuance Limit</label>
        <div class="controls">
            <div class="btn-group">
                <button class="btn btn-small"><div id="_passwordIssueLimit_div"></div></button>
                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="SetPasswordIssueLimit(3, '#_passwordIssueLimit_div', '3 Passwords/month')">3 Passwords/month</a></li>
                    <li><a href="#" onclick="SetPasswordIssueLimit(5, '#_passwordIssueLimit_div', '5 Passwords/month')">5 Passwords/month</a></li>
                    <li><a href="#" onclick="SetPasswordIssueLimit(7, '#_passwordIssueLimit_div', '7 Passwords/month')">7 Passwords/month</a></li>
                    <li><a href="#" onclick="SetPasswordIssueLimit(10, '#_passwordIssueLimit_div', '10 Passwords/month')">10 Passwords/month</a></li>
                    <li><a href="#" onclick="SetPasswordIssueLimit(99, '#_passwordIssueLimit_div', 'Disabled')">Disabled</a></li>
                </ul>
            </div>
            and Enforce Dormancy Duration Limit Of 
            <div class="btn-group">
                <button class="btn btn-small"><div id="_enforceDormancyTime_div"></div></button>
                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="SetEnforceDormancyTime(1, '#_enforceDormancyTime_div', '1 Month')">1 Month</a></li>
                    <li><a href="#" onclick="SetEnforceDormancyTime(2, '#_enforceDormancyTime_div', '2 Months')">2 Months</a></li>
                    <li><a href="#" onclick="SetEnforceDormancyTime(3, '#_enforceDormancyTime_div', '3 Months')">3 Months</a></li>

                </ul>
            </div>
            and Old Password cannot Reuse In 
            <div class="btn-group">
                <button class="btn btn-small"><div id="_oldPasswordReuseTime_div"></div></button>
                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="SetOldPasswordReuseTime(3, '#_oldPasswordReuseTime_div', '3 Months')">3 Months</a></li>
                    <li><a href="#" onclick="SetOldPasswordReuseTime(6, '#_oldPasswordReuseTime_div', '6 Months')">6 Months</a></li>
                    <li><a href="#" onclick="SetOldPasswordReuseTime(12, '#_oldPasswordReuseTime_div', '12 Months')">12 Months</a></li>
                </ul>
            </div>
        </div>
    </div>  

    <hr>
    <div class="control-group">
        <label class="control-label"  for="username">Password Reset</label>
        <div class="controls">

            <div class="btn-group">
                <button class="btn btn-small"><div id="_epinResetReissue_div"></div></button>
                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="SetEpinResetAndReissue(1, '#_epinResetReissue_div', 'Enable')">Enable</a></li>
                    <li><a href="#" onclick="SetEpinResetAndReissue(2, '#_epinResetReissue_div', 'Disable')">Disable</a></li>
                </ul>

            </div>
            for E-PIN Request
            and  
            <div class="btn-group">
                <button class="btn btn-small"><div id="_crResetReissue_div"></div></button>
                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="SetCRResetReissue(1, '#_crResetReissue_div', 'Enable')">Enable</a></li>
                    <li><a href="#" onclick="SetCRResetReissue(2, '#_crResetReissue_div', 'Disable')">Disable</a></li>
                </ul>
            </div>
            for Challenge Response Request

        </div>

    </div>      

    <hr>
    <div class="control-group">
        <label class="control-label"  for="username">Alert</label>
        <div class="controls">

            <div class="btn-group">
                <button class="btn btn-small"><div id="_allowAlert_div"></div></button>
                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="allowAlert(1, '#_allowAlert_div', 'Enable')">Enable</a></li>
                    <li><a href="#" onclick="allowAlert(2, '#_allowAlert_div', 'Disable')">Disable</a></li>
                </ul>
            </div>
            for
            <div class="btn-group">
                <button class="btn btn-small"><div id="_allowAlertFor_div"></div></button>
                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="allowAlertFor(1, '#_allowAlertFor_div', 'Operators')">Operator</a></li>
<!--                    <li><a href="#" onclick="allowAlertFor(2, '#_allowAlertFor_div', 'Users')">Users</a></li>
                    <li><a href="#" onclick="allowAlertFor(0, '#_allowAlertFor_div', 'Both')">Both</a></li>-->
                </ul>
            </div>
            Via
            <div class="btn-group">
                <button class="btn btn-small"><div id="_gatewayType_div"></div></button>
                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="AlertVia(1, '#_gatewayType_div', 'SMS')">SMS</a></li>
                    <li><a href="#" onclick="AlertVia(2, '#_gatewayType_div', 'USSD')">USSD</a></li>
                    <li><a href="#" onclick="AlertVia(3, '#_gatewayType_div', 'VOICE')">VOICE</a></li>
                    <li><a href="#" onclick="AlertVia(4, '#_gatewayType_div', 'EMAIL')">EMAIL</a></li>
                    <!--<li><a href="#" onclick="AlertVia(5, '#_gatewayType_div', 'FAX')">FAX</a></li>-->
                    <li><a href="#" onclick="AlertVia(18, '#_gatewayType_div', 'ANDROIDPUSH')">ANDROIDPUSH</a></li>
                    <li><a href="#" onclick="AlertVia(19, '#_gatewayType_div', 'IPHONEPUSH')">IPHONEPUSH</a></li>
   <li><a href="#" onclick="AlertVia(33, '#_gatewayType_div', 'WEB PUSH')">WEB PUSH</a></li>

                </ul>
            </div>
            With Attempts:
            <div class="btn-group">
                <button class="btn btn-small"><div id="_alertAttempt_div"></div></button>
                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="AlertAttempts(1, '#_alertAttempt_div', '1 Attempts')">1 Attempts</a></li>
                    <li><a href="#" onclick="AlertAttempts(2, '#_alertAttempt_div', '2 Attempts')">2 Attempts</a></li>
                    <li><a href="#" onclick="AlertAttempts(3, '#_alertAttempt_div', '3 Attempts')">3 Attempts</a></li>
                </ul>
            </div>            
            Using Template : <b>email.mobile.trust.alert</b>
            <input type="hidden" name="_templateName" id="_templateName" value="email.ecopin.user.pin.split.first">
        </div>
    </div>
    <hr>
    <!-- Submit -->
    <!--            <div class="control-group">
                    <div class="controls">
                        <div id="save-password-settings-result"></div>
                        <button class="btn btn-primary" onclick="editPasswordPolicySettings()" type="button">Save Setting Now >> </button>
                    </div>
                </div>-->

    <% //if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) {%>

    <div class="control-group">
        <div class="controls">
            <div id="save-password-settings-result"></div>
            <button class="btn btn-primary" onclick="editPasswordPolicySettings()" type="button">Save Setting Now >> </button>
        </div>
    </div>
    <%//}%>



</form>
</div>
</div>

<script language="javascript" type="text/javascript">
    LoadPasswordPolicySettings();
    $(document).ready(function() {
        $("#_arrCommonPassword").select2({
            tags: [],
            dropdownCss: {display: 'none'},
            tokenSeparators: [","]
        });
    });

</script>
</div>



<%@include file="footer.jsp" %>