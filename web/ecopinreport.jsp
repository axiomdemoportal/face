<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Epintracker"%>
<%@page import="com.mollatech.ecopin.management.EPINManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/ecopin/ecopin.js"></script>
<script src="./assets/js/ecopin/json-minified.js"></script>
<script src="./assets/js/ecopin/raphael-min.js"></script>
<script src="./assets/js/ecopin/morris.js"></script>
<link rel="stylesheet" href="./assets/css/datepicker.css">
<link rel="stylesheet" href="./assets/js/ecopin/morris.css">

<div class="container-fluid">
    <h1 class="text-success">ECOPIN Reports</h1>

    <h3>Make your own report</h3>
    <input type="hidden" id="_changeStatusType" name="_changeStatusType">
    <div class="row-fluid">
        <div class="span12">
            <div class="control-group form-inline">

                <div class="input-prepend">

                    <!--<div class="well">-->
                    <span class="add-on">From:</span>   
                    <div id="datetimepicker1" class="input-append date">
                        <input id="startdate" name="startdate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                    <!--</div>-->
                </div>
                <div class="input-prepend">
                    <!--<div class="well">-->
                    <span class="add-on">till:</span>   
                    <div id="datetimepicker2" class="input-append date">
                        <input id="enddate" name="enddate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                    <!--</div>-->
                </div>

                <div class="input-prepend">
                    <span class="add-on" >Search</span><input id="_searchtext" name="_searchtext" class="" type="text" data-bind="value: vm.ActualDoorSizeDepth" placeholder="Phone,Email"/>
                </div>

                <div class="btn-group">

                    <button class="btn"><div id="_change-Status-Type-Axiom">Select Type</div></button>
                    <button class="btn dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="#" onclick="ChangeEcopinType(0)">All</a></li>
                        <li><a href="#" onclick="ChangeEcopinType(3)">Policy Check</a></li>
                        <li><a href="#" onclick="ChangeEcopinType(4)">QA Validation</a></li>
                        <li><a href="#" onclick="ChangeEcopinType(2)">Delivery</a></li>
                        <li><a href="#" onclick="ChangeEcopinType(1)">Operator Controlled</a></li>

                        <!--<li><a href="#" onclick="ChangeEcopinType(3)">User Controlled</a></li>-->



                    </ul>
                </div>




                <button class="btn btn-success" onclick="searchEcopinRecords()" type="button">Generate Report</button>

            </div>
        </div>
    </div>


    <%
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        EPINManagement eMngt = new EPINManagement();
        UserManagement uMngt = new UserManagement();

        int limit = 25;

        Epintracker[] epinobj = eMngt.getALLEPINObj(channel.getChannelid(), limit);

        String strerr = "No Records Found";
    %>


    <div class="row-fluid">
        <div id="licenses_data_table">
            <h3>Latest Request</h3>
            <table class="table table-striped" >

                <tr>
                    <td>No.</td>
                    <td>User Name</td>
                    <td>Phone</td>
                    <td>Email</td>
                    <td>Status</td>
                    <td>Type</td>
                    <td>Expiry In Minutes</td>
                    <td>Requested On</td>
                    <td>Sent On</td>
                </tr>
                <% if (epinobj != null) {
                        for (int i = 0; i < epinobj.length; i++) {
                            AuthUser user = uMngt.getUser(sessionId, channel.getChannelid(), epinobj[i].getUserid());

                            String strstatus = "-";
                            if (epinobj[i].getStatus() == EPINManagement.APPROVED) {
                                strstatus = "Delivered";
                            } else if (epinobj[i].getStatus() == EPINManagement.FAILEDTOVALIDATE) {
                                strstatus = "Invalid";
                            } else if (epinobj[i].getStatus() == EPINManagement.EXPIRED) {
                                strstatus = "Expired";
                            } else if (epinobj[i].getStatus() == EPINManagement.ALLOWED) {
                                strstatus = "Allowed";
                            } else if (epinobj[i].getStatus() == EPINManagement.NOT_ALLOWED) {
                                strstatus = "Dropped";
                            } else if (epinobj[i].getStatus() == EPINManagement.FAILED) {
                                strstatus = "Not delivered";
                            } else if (epinobj[i].getStatus() == EPINManagement.REJECTED) {
                                strstatus = "Rejected";
                            }

                            String strtype = "-";
                            if (epinobj[i].getType() == EPINManagement.OPERATOR_CONTROLLED) {
                                strtype = "Operator Controlled";
                            } else if (epinobj[i].getType() == EPINManagement.DELIVERY) {
                                strtype = "Delivery";
                            } else if (epinobj[i].getType() == EPINManagement.POLICY_CHECK) {
                                strtype = "Policy Check";
                            } else if (epinobj[i].getType() == EPINManagement.VALIDATION) {
                                strtype = "Validation";
                            }

                            Calendar current = Calendar.getInstance();
                            current.setTime(epinobj[i].getRequestdatetime());
                            current.add(Calendar.MINUTE, epinobj[i].getExpiryInMins());
                            Date expirytime = current.getTime();
                            String DATE_FORMAT_NOW = "dd-MM-yyyy HH:mm:ss";
                            SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
                            String extime = sdf.format(expirytime);

                            String senttime = "-";
                            if (epinobj[i].getSentdatetime() != null) {
                                senttime = sdf.format(epinobj[i].getSentdatetime());
                            }

                %>
                <tr>
                    <td><%=(i + 1)%></td>
                    <td><%=user.getUserName()%></td> 
                    <td><%=epinobj[i].getPhone()%></td> 
                    <td><%=epinobj[i].getEmailid()%></td> 
                    <td><%=strstatus%></td>
                    <td><%=strtype%></td>  
                    <td><%=extime%></td>  
                    <td><%=epinobj[i].getRequestdatetime()%></td>  
                    <td><%= senttime%></td>  
                </tr>
                <%}
                } else {%>
                <tr>
                    <td><%=1%></td>
                    <td>No Records Found</td>
                    <td>No Records Found</td>
                    <td>No Records Found</td> 
                    <td>No Records Found</td>
                    <td>No Records Found</td>
                    <td>No Records Found</td>
                    <td>No Records Found</td>
                    <td>No Records Found</td>
                    <%}%>
                </tr>
            </table>
        </div>
    </div>

    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datepicker({
                format: 'dd/MM/yyyy',
                language: 'pt-BR'
            });
        });
        $(function () {
            $('#datetimepicker2').datepicker({
                format: 'dd/MM/yyyy',
                language: 'pt-BR'
            });
        });
        ChangeMediaType(0);
    </script>
</div>

<%@include file="footer.jsp" %>
