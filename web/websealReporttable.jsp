<%@page import="java.util.Iterator"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.LocationManagement"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.axiom.nucleus.db.Countrylist"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.WebsealManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.ApWebseal"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();

    String _startdate = request.getParameter("_startdate");
    String _enddate = request.getParameter("_enddate");
    String monitorName = request.getParameter("monitorName");
    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    Date startDate = null;
    if (_startdate != null && !_startdate.isEmpty()) {
        startDate = (Date) formatter.parse(_startdate);
    }
    Date endDate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        endDate = (Date) formatter.parse(_enddate);
    }
    ApWebseal[] apdetailses = new WebsealManagement().getDetails(_channelId, Integer.parseInt(monitorName), startDate, endDate);
    session.setAttribute("apdetailses", apdetailses);
    String strmsg = "No Record Found";
%>
<div class="container-fluid">

    <table class="table table-striped" >
        <tr>
            <td>No.</td>
            <td>IP</td>
            <td>Country</td>
            <td>State</td>
            <td>City</td>
            <td>Zip Code</td>
            <td>DNS</td>
            <td>Certificate Expired</td>
            <td>Certificate Issuer</td>
            <td>Certificate Wrong Issuer</td>
            <td>Browser name</td>
            <td>Browser version</td>
            <td>OS name</td>
            <td>OS version</td>
            <td>Status</td>
            <td>Request on</td>            
        </tr>
        <%    if (apdetailses != null) {
                Map map = new HashMap();
                for (int i = 0; i < apdetailses.length; i++) {

                    String certValid = "NA";
                    String strStatus = "Success";
                    String certDiff = "NA";
                    String dns = "";
                    String certIssuer = "NA";
                    String countryName = "NA";
                    if (apdetailses[i].getStatus() == WebsealManagement.DNSNOTFOUND) {
                        strStatus = "DNS not match";
                    } else if (apdetailses[i].getStatus() == WebsealManagement.HTTPPROTOCOLFAILURE) {
                        strStatus = "Protocol not match";
                    } else if (apdetailses[i].getStatus() == WebsealManagement.URLNOTMATCH) {
                        strStatus = "URL not match";
                    } else if (apdetailses[i].getStatus() == WebsealManagement.SSLCERTEXPIRED) {
                        strStatus = "Certificate Expired";
                    } else if (apdetailses[i].getStatus() == WebsealManagement.WEBSEALCERTNOTVALID) {
                        strStatus = "URL unique id not match";
                    } else if (apdetailses[i].getStatus() == WebsealManagement.SSLCERTDIFFERENT) {
                        strStatus = "Certificate not match";
                    }

                    if (apdetailses[i].getCertificateValid() != null && apdetailses[i].getCertificateValid() == WebsealManagement.CERTIINVALID) {
                        certValid = "Certificate expired";
                    } else if (apdetailses[i].getCertificateValid() != null && apdetailses[i].getCertificateValid() == WebsealManagement.CERTIVALID) {
                        certValid = "Certificate valid";
                    }
                    if (apdetailses[i].getCertificateDiff() != null && apdetailses[i].getCertificateDiff() == WebsealManagement.CERTIDIFFN) {
                        certDiff = "Yes";
                    } else if (apdetailses[i].getCertificateDiff() != null && apdetailses[i].getCertificateDiff() == WebsealManagement.CERTIDIFFY) {
                        certDiff = "No";
                    }
                    if (apdetailses[i].getDns() != null) {

                        JSONObject json = new JSONObject(apdetailses[i].getDns());
                        Iterator json_keys = json.keys();
                        while (json_keys.hasNext()) {
                            String keyJSON = (String) json_keys.next();
                            dns += (String) json.getString(keyJSON);
                        }
                    }
                    if (apdetailses[i].getCertificateIssuer() != null) {
                        certIssuer = apdetailses[i].getCertificateIssuer();
                    }
                    Countrylist country = null;
                    if (apdetailses[i].getCountry() != null) {
                        if (map.isEmpty()) {
                            country = new LocationManagement().getCountriesByShortCode(apdetailses[i].getCountry());
                            map.put(apdetailses[i].getCountry(), country);
                        } else {
                            if (map.get(apdetailses[i].getCountry()) == null) {
                                country = new LocationManagement().getCountriesByShortCode(apdetailses[i].getCountry());
                                map.put(apdetailses[i].getCountry(), country);
                            } else {
                                country = (Countrylist) map.get(apdetailses[i].getCountry());
                                countryName = country.getCountyName();
                            }
                        }
                    }
        %>
        <tr>
            <td><%=i + 1%></td>

            <td><%=apdetailses[i].getIp()%></td>
            <td><%=countryName%></td>
            <td><%=apdetailses[i].getStateL()%></td>
            <td><%=apdetailses[i].getCity()%></td>
            <td><%=apdetailses[i].getZipcode()%></td>
            <td><%=dns%></td>
            <td><%=certValid%></td>
            <td><%=certIssuer%></td>
            <td><%=certDiff%></td>
            <td><%=apdetailses[i].getBrowsername()%></td>
            <td><%=apdetailses[i].getBrowserversion()%></td>
            <td><%=apdetailses[i].getOsname()%></td>
            <td><%=apdetailses[i].getOsversion()%></td>
            <td><%=strStatus%></td>
            <td><%=sdf.format(apdetailses[i].getRequestedon())%></td>

        </tr>
        <%
            }
        } else {%>
        <tr>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
            <td><%=strmsg%></td>
        </tr>
        <%}%>
    </table>

    <div class="row-fluid">   
        <div class="span6">

            <div class="control-group">                        
                <div class="span1">
                    <div class="control-group form-inline">
                        <a href="#" class="btn btn-info" onclick="downloadwebsealReport(1, '<%=_startdate%>', '<%=_enddate%>')" >
                            CSV</a>
                    </div>
                </div>
                <div class="span1">
                    <div class="control-group form-inline">
                        <a href="#" class="btn btn-info" onclick="downloadwebsealReport(0, '<%=_startdate%>', '<%=_enddate%>')" >
                            PDF</a>
                    </div>
                </div>
                <div class="span1">
                    <div class="control-group form-inline">
                        <a href="#" class="btn btn-info" onclick="downloadwebsealReport(2, '<%=_startdate%>', '<%=_enddate%>')" >
                            TEXT</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>