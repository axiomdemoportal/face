<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet"%>
<%@page import="com.mollatech.axiom.nucleus.db.Units"%>
<%@include file="header.jsp" %>
<%    UnitsManagemet uMngt = new UnitsManagemet();
    Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
%>
<div class="container-fluid">
    <div id="auditTable">
        <h1 class="text-success">Operators Management</h1>
        <p>You can manage all your operators, their roles and password management. 
            The "admin" is the most powerful operator followed with "helpdesk" to manage daily request and the "reporter" can read-only operator role for pulling reports of the system.
        </p>
        <br>
        <div class="row-fluid">
            <div id="licenses_data_table">
                <table class="table table-striped">
                    <tr>
                        <td>No.</td>
                        <td>Name</td>
                        <td>Email</td>
                        <td>Unit</td>
                        <td>Manage</td>
                        <td>Role</td>
                        <td>Password</td>
                        <td>Access Type</td>
                        <td>Attempts</td>
                        <td>Audit</td>
                        <td>Access From</td>
                        <td>Access Till</td>
                        <td>Created</td>
                        <td>Last Access</td>
                    </tr>

                    <%
                        String _status = request.getParameter("_status");
                        String _unitId = request.getParameter("_unitId");
                        Channels _apSChannelDetails = (Channels) session.getAttribute("_apSChannelDetails");
                        OperatorsManagement oManagement = new OperatorsManagement();
                        String _sessionID = (String) session.getAttribute("_apSessionID");

                        SessionManagement smObj = new SessionManagement();
                        int iStatus = smObj.GetSessionStatus(_sessionID);
                        AxiomOperator[] axiomoperatorObj = null;
                        Roles roleObj = oManagement.getRoleByRoleId(_apSChannelDetails.getChannelid(), operator.getRoleid());

                        if (_status == null && _unitId == null) {
                            if (iStatus == 1) { //active
                   axiomoperatorObj = oManagement.ListOperatorsInternal(_apSChannelDetails.getChannelid());
                            }
                        } else {
                            int iUnitId = Integer.parseInt(_unitId);
                            int istatus = Integer.parseInt(_status);
                            axiomoperatorObj = oManagement.getOperatorByUnitStatus(sessionid, _apSChannelDetails.getChannelid(), iUnitId, istatus);
                        }
                       if (axiomoperatorObj != null) {
                            for (int i = 0; i < axiomoperatorObj.length; i++) {
                                if(!axiomoperatorObj[i].getStrName().equals(operator.getName()) ){
                                AxiomOperator axoprObj = axiomoperatorObj[i];
                                java.util.Date dCR = new java.util.Date(axoprObj.getUtcCreatedOn());
                                java.util.Date dLR = new java.util.Date(axoprObj.getLastUpdateOn());

                                int iOprStatus = axoprObj.getiStatus();
                                int iOprAccessType = axoprObj.iAccessType;
                                String strStatus;
                                if (iOprStatus == 1) {
                                    strStatus = "Active";
                                } else {
                                    strStatus = "Suspended";
                                }

                                String strOprAccessType = "Not Set";
                                if (iOprAccessType == 1) {
                                    strOprAccessType = "Operator";
                                } else {
                                    strOprAccessType = "Role";
                                }
                                String s1 = "Not Set";
                                String s2 = "Not Set";
                                if (axiomoperatorObj[i].accessStartFrom != null && axiomoperatorObj[i].accessTill != null) {
                                    SimpleDateFormat tzTimeHeader1 = new SimpleDateFormat("dd/MMM/yy");
                                    String completeTimeHeader1 = tzTimeHeader1.format(axiomoperatorObj[i].accessStartFrom);
                                    String completeTimeHeader2 = tzTimeHeader1.format(axiomoperatorObj[i].accessTill);
//                                    s = s + " Access(" + completeTimeHeader1 + "-" + completeTimeHeader2 + ")";
                                    s1 = completeTimeHeader1;
                                    s2 = completeTimeHeader2;
                                }

                                SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy");

                                String uidiv4OprStatus = "operator-status-value-" + i;

                                String uidiv4Opraccess = "operator-access-value-" + i;

                                String uidiv4OprRole = "operator-role-value-" + i;

                                String uidiv4OprAttempts = "operator-attempts-value-" + i;

                                UnitsManagemet unMngt = new UnitsManagemet();
                                Units unObj = unMngt.getUnitByUnitId(_sessionID, _apSChannelDetails.getChannelid(), axoprObj.getiUnit());
                                String unitName = "Not-Set";
                                if (unObj != null && unObj.equals("") == false) {
                                    unitName = unObj.getUnitname();
                                }

                    %>
                    <tr>
                        <td><%=i + 1%></td>
                        <td><%=axoprObj.getStrName()%></td>
<!--                        <td><%=axoprObj.getStrPhone()%></td>
                        <td><%=axoprObj.getStrEmail()%></td>-->
                        <td><%=axoprObj.getStrEmail()%></td>
                        <td><%=unitName%></td>
                        <td>
                            <div class="btn-group">
                                <button class="btn btn-mini" id="<%=uidiv4OprStatus%>"><%=strStatus%></button>
                                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <%if(!axoprObj.getStrName().equals("sysadmin")){%>
                                    <li><a href="#" onclick="ChangeOperatorStatus('<%=axoprObj.getStrOperatorid()%>', 1, '<%=uidiv4OprStatus%>')">Mark as Active?</a></li>
                                    <li><a href="#" onclick="ChangeOperatorStatus('<%=axoprObj.getStrOperatorid()%>', 0, '<%=uidiv4OprStatus%>')">Mark as Suspended?</a></li>
                                    <li class="divider"></li>
                                    <%}%>
                                    <li><a href="#" onclick="loadEditOperatorDetails('<%=axoprObj.getStrOperatorid()%>')">Edit Details</a></li>
                                        <%if (operator.getRoleid() == 1) {%>
                                    <li><a href="./roleAccessRightN_1.jsp?_operatorID=<%=axoprObj.getStrName()%>&_type=OPERATOR" data-toggle="modal">Access Rights</a></li>
                                    <li><a href="#" onclick="loadEditOperatorAccess('<%=axoprObj.getStrOperatorid()%>')">Access Till</a></li> 
                                        <%}%>
                                </ul>
                            </div>

                        </td>
                       
                        <td>
                            <div class="btn-group">
                                <button class="btn btn-mini" id="<%=uidiv4OprRole%>"><%=axoprObj.getStrRoleName()%></button>
                                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <% for (int j = 0; j < roles.length; j++) {
                                    if(!axoprObj.getStrName().equals("sysadmin")){    
                                    %>
                                     
                                    <li><a href="#" onclick="ChangeOperatorRole(<%=roles[j].getRoleid()%>, '<%=roles[j].getName()%>', '<%=axoprObj.getStrOperatorid()%>', '<%=uidiv4OprRole%>')"><%=roles[j].getName()%></a></li>

                                    <%}else{%>
                                    <li><a href="#" onclick="ChangeOperatorRole(1, 'sysadmin', '<%=axoprObj.getStrOperatorid()%>', '<%=uidiv4OprRole%>')">sysadmin</a></li>
                                   <% break;}}%>
                                </ul>
                            </div>
                        </td>
                    
                        <td>
                            <div class="btn-group">
                                <button class="btn btn-mini">*****</button>
                                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="OperatorUnlockPassword('<%=axoprObj.getStrOperatorid()%>', '<%=uidiv4OprAttempts%>')" >Unlock Password</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#" onclick="OperatorResendPassword('<%=axoprObj.getStrOperatorid()%>')">Resend Current Password (via email)</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#" onclick="OperatorSetPassword('<%=axoprObj.getStrOperatorid()%>')">Set Random Password</a></li>
                                    <li><a href="#" onclick="OperatorSendRandomPassword('<%=axoprObj.getStrOperatorid()%>')">Set & Send Random Password (via email)</a></li>
                                </ul>
                            </div>
                        </td>
                        <td>
                            <div class="btn-group">
                                <button class="btn btn-mini" id="<%=uidiv4Opraccess%>"><%=strOprAccessType%></button>
                                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="ChangeOperatorAccess('<%=axoprObj.getStrOperatorid()%>', <%=OperatorsManagement.OPERATOR_ACCESS%>, '<%=uidiv4Opraccess%>')">Operator</a></li>
                                    <li><a href="#" onclick="ChangeOperatorAccess('<%=axoprObj.getStrOperatorid()%>',<%=OperatorsManagement.ROLE_ACCESS%>, '<%=uidiv4Opraccess%>')">Role</a></li>
                                </ul>
                            </div>

                        </td>
                        <td><div id="<%=uidiv4OprAttempts%>"><%=axoprObj.getiWrongAttempts()%></div></td>

                        <td>
                            <a href="#" class="btn btn-mini" onclick="loadEditRequesterOperatorDetails('<%=axoprObj.getStrOperatorid()%>')">Audit Download</a>
                        </td>
                        <%if (axoprObj.getAccessStartFrom() != null) {%>
                        <td><%=sdf.format(axoprObj.getAccessStartFrom())%></td>
                        <%} else {%>
                        <td>Not Set</td>
                        <%}%>
                        <%if (axoprObj.getAccessTill() != null) {%>
                        <td><%=sdf.format(axoprObj.getAccessTill())%></td>
                        <%} else {%>
                        <td>Not Set</td>
                        <%}%>

                        <td><%=sdf.format(dCR)%></td>
                        <td><%=sdf.format(dLR)%></td>
                    </tr>
                    <%}} //for 
                    } //if 
                    else {%>
                    <tr><td> No Record</td></tr>
                    <%}%>
                </table>


            </div>
        </div>
        <br>
        <p><a href="#addOperator" class="btn btn-primary" data-toggle="modal">Add New Operator&raquo;</a></p>
        <p><a href="./searchoperator.jsp" class="btn btn-success" >Add Ldap Operator&raquo;</a></p>
        <script language="javascript">
            //listChannels();
        </script>
    </div>
</div>



<div id="addOperator" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Add New Operator</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="AddNewOperatorForm" name="AddNewOperatorForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Name</label>
                        <div class="controls">
                            <input type="text" id="_oprname" name="_oprname" placeholder="set unique name for login" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Email Id</label>
                        <div class="controls">
                            <input type="text" id="_opremail" name="_opremail" placeholder="set emailid for notification" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Phone</label>
                        <div class="controls">
                            <input type="text" id="_oprphone" name="_oprphone" placeholder="set phone for notification" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Assign Role</label>
                        <div class="controls">
                            <select class="span4" name="_oprrole" id="_oprrole">
                                <%   for (int i = 0; i < roles.length; i++) {
//                                        if (roleObj.getName().equals(OperatorsManagement.admin) && !roles[i].getName().equals(OperatorsManagement.sysadmin)) {
%>
                                <!--<option value="<%=roles[i].getRoleid()%>"><%=roles[i].getName()%></option>-->
                                <%
//                                } else if (roleObj.getName().equals(OperatorsManagement.sysadmin)) {
%>
                                <option value="<%=roles[i].getRoleid()%>"><%=roles[i].getName()%></option>
                                <%
                                    }
//                                    }
                                %>
                            </select>
                            Type
                            <select class="span4" name="_operatorType" id="_operatorType">
                                <option  value="1">Requester</option>       
                                <option  value="2">Authorizer</option>  
                            </select>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Unit</label>
                        <div class="controls">
                            <select class="span4" name="_units" id="_units">
                                <%
                                    Units[] uList = uMngt.ListUnitss(sessionid, _apSChannelDetails.getChannelid());
                                    if (uList != null) {
                                        for (int i = 0; i < uList.length; i++) {
                                            if(uList[i].getStatus() == 1){//only active
                                %>
                                <option value="<%=uList[i].getUnitid()%>"><%=uList[i].getUnitname()%></option>
                                <%
                                            }}
                                    }
                                %>
                            </select>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>

    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <div class="span3" id="add-new-operator-result"></div>
        <button class="btn btn-primary" onclick="addOperator()" id="addnewOperatorSubmitBut">Add New Operator</button>

    </div>
</div>

<div id="editOperator" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3>Edit Operator<div id="idEditOperatorName"></div></h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="editOperatorForm" name="editOperatorForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Name</label>
                        <div class="controls">
                            <input type="text" id="_oprnameE" name="_oprnameE" placeholder="Operator Name" class="input-xlarge">
                            <input type="hidden" id="_oprroleidE" name="_oprroleidE"/>
                            <input type="hidden" id="_oprstatusE" name="_oprstatusE"/>
                            <input type="hidden" id="_opridE" name="_opridE"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Email</label>
                        <div class="controls">
                            <input type="text" id="_opremailE" name="_opremailE" placeholder="Operator Emailid" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Phone</label>
                        <div class="controls">
                            <input type="text" id="_oprphoneE" name="_oprphoneE" placeholder="Operator phone" class="input-xlarge">
                        </div>
                    </div>

                    <!--                    <div class="control-group">
                                            <label class="control-label"  for="username"> Type</label>
                                            <div class="controls">
                                                <select class="span4" name="_operatorTypeE" id="_operatorTypeE">
                                                    <option  value="1">Requester</option>       
                                                    <option  value="2">Authorizer</option>  
                                                </select>
                                            </div>
                                        </div>-->
                    <div class="control-group">
                        <label class="control-label"  for="username"> Type</label>
                        <div class="controls">
                            <select class="span4" name="_operatorTypeE" id="_operatorTypeE">
                                <option  value="1">Requester</option>       
                                <option  value="2">Authorizer</option>  
                            </select>
                            Edit Unit
                            <select class="span4" name="_unitsEDIT" id="_unitsEDIT">
                                <%
//                                UnitsManagemet uMngt = new UnitsManagemet();
//                                   Units[] uList = uMngt.ListUnitss(sessionid,_apSChannelDetails.getChannelid());
                                    if (uList != null) {
                                        for (int i = 0; i < uList.length; i++) {
                                                if(uList[i].getStatus() == 1){//only active
                                %>
                                <option value="<%=uList[i].getUnitid()%>"><%=uList[i].getUnitname()%></option>
                                <%
                                                }
                                        }
                                    }
                                %>
                            </select>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="editoperator-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <%String edituser = LoadSettings.g_sSettings.getProperty("user.edit");
            if (edituser.equals("true")) {
        %>
        <button class="btn btn-primary" onclick="editoperator()" id="buttonEditOperatorSubmit">Save Changes</button>
        <%}%>
    </div>
</div>

<div id="auditDownload" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idauditDownload"></div></h3>
        <h3 id="myModalLabel">Download Audit</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="testSMSPrimaryForm" name="testSMSPrimaryForm">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_oprnameR" name="_oprnameR"/>
                    <input type="hidden" id="_opridR" name="_opridR"/>
                    <div class="control-group">
                        <label class="control-label"  for="username">Start Date</label>
                        <div class="controls" align="left" >

                            <!--<span class="add-on">From:</span>-->   
                            <div id="Pushdatetimepicker1" class="input-append date">
                                <input id="_auditStartDate" name="_auditStartDate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">End Date</label>
                        <div class="controls" align="left">
                            <!--<span class="add-on">Till:</span>-->   
                            <div id="Pushdatetimepicker2" class="input-append date">
                                <input id="_auditEndDate" name="_auditEndDate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>                    
                </fieldset>
            </form>
        </div>
    </div>

    <div class="modal-footer">
        <div id="editoperator-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="searchAudit()" id="buttonEditOperatorSubmit">Show Audit</button>
    </div>
</div>

<div id="operatorAccess" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idauditDownload"></div></h3>
        <h3 id="myModalLabel">Grant Access</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="operatorAcessForm" name="operatorAcessForm">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_oprAccessId" name="_oprAccessId"/>
                    <div class="control-group">
                        <label class="control-label"  for="username">Start Date</label>
                        <div class="controls" align="left">
                            <!--<span class="add-on">Till:</span>-->   
                            <div id="Pushdatetimepicker4" class="input-append date">
                                <input id="_accessStartDate" name="_accessStartDate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div> 
                    <div class="control-group">
                        <label class="control-label"  for="username">End Date</label>
                        <div class="controls" align="left">
                            <!--<span class="add-on">Till:</span>-->   
                            <div id="Pushdatetimepicker3" class="input-append date">
                                <input id="_accessEndDate" name="_accessEndDate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>                    
                </fieldset>
            </form>
        </div>
    </div>

    <div class="modal-footer">
        <div id="editoperator-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="grantAccess()" id="buttonEditOperatorSubmit">Grant Access</button>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $('#Pushdatetimepicker1').datepicker({
            language: 'pt-BR'
        });
    });
    $(function() {
        $('#Pushdatetimepicker2').datepicker({
            language: 'pt-BR'
        });
    });
    $(function() {
        $('#Pushdatetimepicker3').datepicker({
            language: 'pt-BR'
        });
    });
    $(function() {
        $('#Pushdatetimepicker4').datepicker({
            language: 'pt-BR'
        });
    });
//                            ChangeMediaType(0);
</script>


<%@include file="footer.jsp" %>
