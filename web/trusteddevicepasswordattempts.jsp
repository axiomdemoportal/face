<%@include file="header.jsp" %>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Clientdestroy"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TrustDeviceManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="./assets/js/json_sans_eval.js"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script src="./assets/js/trustedDevice.js"></script>

<%
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();
    String _duration = request.getParameter("_duration");
      String _userid = request.getParameter("_userid");
      
    
    TrustDeviceManagement tdm = new TrustDeviceManagement();
    UserManagement uManagement = new UserManagement();
    
    AuthUser user =  uManagement.getUser(sessionId, _channelId, _userid);
    
    Clientdestroy[] arrDestroyclint =  tdm.getDestroyedClient(sessionId, _channelId, _userid, _duration);
    
    
    
    
     String strmsg = "No Records Found";
%>


<div class="container-fluid">
    <h1 class="text-success">User <i>"<%=user.getUserName()%>"</i> self-destroy alert list</h1>
    
    <div id="map_transaction" style="width:100%; height: 300px"></div>
</div>

    <hr>
 <div class="container-fluid">   
  
 <div class="row-fluid">
                <div class="span6">
                    <div class="control-group">                        
                        <div class="span3">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="ClientPasswordReport('<%=_userid %>','<%=_duration%>')" >
                                    <i class="icon-white icon-chevron-down"></i>Download CSV</a>
                            </div>
                        </div>
                        <div class="span3">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="ClientPasswordReportPDF('<%=_userid %>','<%=_duration%>')" >
                                    <i class="icon-white icon-chevron-down"></i>Download PDF</a>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
 </div>
 <div class="container-fluid">                                   
<div class="tabbable">
      <div class="tab-pane" id="otpreport">            
            <table class="table table-striped" id="table_main">

                <tr>
                    <td>No.</td>
                    <td>Ip Address</td>
                    <td>Lattitude</td>
                    <td>Longitude</td>
                    <td>Location</td>
                    <td>Type</td>
                    <td>Destroy Time</td>

                </tr>
                <%
                    int wrong_attempts = 1;
                    int forced_destroy = 2;
                    int intentionally_destroy = 3;

                    if (user != null) {

                        if(arrDestroyclint != null){
                        
                        for(int i=0;i<arrDestroyclint.length;i++){
                
                         String  Strtype = "";
                        if(arrDestroyclint[i].getType() == intentionally_destroy){
                            Strtype = "Intentionally Destroy"; 
                        }else if(arrDestroyclint[i].getType() == forced_destroy){
                            Strtype = "Forced Destroy";
                        }else if(arrDestroyclint[i].getType() == wrong_attempts){
                            Strtype = "Wrong Attempts";
                        }
                        
                %>
                 <tr>
                    <td><%=(i+1)%></td>
                    <td><%=arrDestroyclint[i].getIpaddress() %></td>
                    <td><%=arrDestroyclint[i].getLatitude() %></td>
                    <td><%=arrDestroyclint[i].getLongitude() %></td>
                    <td><%=arrDestroyclint[i].getLocation() %></td>
                     <td><%=Strtype %></td>
                    <td><%=arrDestroyclint[i].getDestroyTime()%></td>
                   
                </tr>
                <%}}} else {%>
                <td><%=1%></td>
                <td><%=strmsg%></td>
                <td><%=strmsg%></td>
                <td><%=strmsg%></td>
                <td><%=strmsg%></td>
                <td><%=strmsg%></td>
                <td><%=strmsg%></td>
                 <td><%=strmsg%></td>
              
               <%}%>
                
            </table>

        </div>
 
</div>
 </div>
    <div class="container-fluid">   
    <div class="row-fluid">
                <div class="span6">
                    <div class="control-group">                        
                        <div class="span3">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="deletedclients('<%=_userid %>','<%=_duration%>')" >
                                    <i class="icon-white icon-chevron-down"></i>Download CSV</a>
                            </div>
                        </div>
                        <div class="span3">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="delededclientsReportpdf('<%=_userid %>','<%=_duration%>')" >
                                    <i class="icon-white icon-chevron-down"></i>Download PDF</a>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
    </div>
<script language="javascript">
    PasswordAttemptsMap('<%=_userid%>', '<%=_duration%>');
</script>

    <br><br>