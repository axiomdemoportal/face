
<!DOCTYPE html>
<html lang="en">
<head>
  <!--<meta charset="utf-8">-->
  <title>Switchnow Video User Guide</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

	<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
	<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
	<!--script src="js/less-1.3.3.min.js"></script-->
	<!--append ?#!watch? to the browser URL, then refresh the page. -->
	
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<!--<link href="assets/css/style.css" rel="stylesheet">-->
	<link href="assets/css/custom.css" rel="stylesheet">
	
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
	<![endif]-->
  
        <script type="text/javascript" src="assets/js/jquery.js"></script>
        <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
        <!--<script type="text/javascript" src="assets/js/scripts.js"></script>-->
</head>

<body>
	<div class="header">
		<div class="container">
			<div class="row">
				<div class="span12">
					<div class="pull-left">
						<div class="logo">
							<img src="img/logo.png">
						</div>
					</div>
				</div>
			</div>                   
		</div>
	</div>

	<div class="navbar navbar-inverse">
		<div class="navbar-inner noborder">
			<div class="container margin80left">
				<div class="row">
					<div class="span12">
						<ul class="nav nav-pills">
							<li>
								<a href="<?php echo base_url(); ?>index.php">Home</a>
							</li>
							<li>
								<a href="http://switchnow.com.my/tickets/">Submit Ticket</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<div class="jumbotron">
				<h1>
					Switchnow User Guide
				</h1>
				<p>
					Welcome to Switchnow user guide section. If you have any questions about the system, please find the related video clip in this section.
				</p>
			</div>
			<div class="row clearfix">
				<div class="col-md-12 column">
					<div class="page-header">
						<h1>
							Switchnow <small>Video User Guide</small>
						</h1>
					</div>
					<div class="tabbable" id="tabs-988746">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#panel-187378" data-toggle="tab">Clients</a>
							</li>
							<li>
								<a href="#panel-187379" data-toggle="tab">Vendors</a>
							</li>
							<li>
								<a href="#panel-187380" data-toggle="tab">Inventory</a>
							</li>
							<li>
								<a href="#panel-187381" data-toggle="tab">Quote</a>
							</li>
							<li>
								<a href="#panel-187382" data-toggle="tab">Invoice</a>
							</li>
							<li>
								<a href="#panel-187383" data-toggle="tab">Payment</a>
							</li>
							<li>
								<a href="#panel-187384" data-toggle="tab">Report</a>
							</li>
							<li>
								<a href="#panel-187385" data-toggle="tab">Settings</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="panel-187378">
								<p></p>
								<div class="row">
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="http://www.youtube.com/embed/PnB7fw-n--c" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Add New Client?
												</h3>
												<p>
													This video clip will show you how to add new client profile.
												</p>												
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="http://www.youtube.com/embed/LQEBpGVKG6Y" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to View Client Profile?
												</h3>
												<p>
													This video clip will show you how to view client profile, related quote and invoice and filter via status or search box.
												</p>												
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="panel-187379">
								<p></p>
								<div class="row">
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="http://www.youtube.com/embed/vAJn1PkL3zQ" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Add New Vendor?
												</h3>
												<p>
													This video clip will show you how to add new vendor profile.
												</p>												
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="http://www.youtube.com/embed/r5yehEDKsCY" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to View Vendor Profile?
												</h3>
												<p>
													This video clip will show you how to view vendor profile and filter via status or search box.
												</p>												
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="panel-187380">
								<p></p>
								<div class="row">
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="http://www.youtube.com/embed/8G9pm2cz0tQ" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to View Inventory?
												</h3>
												<p>
													This video clip will show you how to view inventory list and item detail.
												</p>												
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="http://www.youtube.com/embed/XWSP7zOIqcc" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Add New Inventory?
												</h3>
												<p>
													This video clip will show you how to add new inventory.
												</p>												
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/Ttse9zEcbqE" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Add Incoming Stock?
												</h3>
												<p>
													This video clip will show you how to add new incoming stock. Set the status to "Received" after receive stock from vendor.
												</p>												
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="panel-187381">
								<p></p>
								<div class="row">
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/NN6MOgQcgyc" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Add New Quote?
												</h3>
												<p>
													This video clip will show you how to add new quote.
												</p>												
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/-WRnOje_xgg" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Add Item from Inventory?
												</h3>
												<p>
													This video clip will show you how to add item from your inventory list instead of manual typing.
												</p>												
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/zhSOHEqX4_4" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Add Discount?
												</h3>
												<p>
													This video clip will show you how to add discount per item in your quote.
												</p>												
											</div>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/ZpSJ_sUD-sg" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Add GST?
												</h3>
												<p>
													This video clip will show you how to add GST in your quote.
												</p>												
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/QZtdisEOuTY" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to View Quote?
												</h3>
												<p>
													This video clip will show you how to view created quote and filter via status or search box.
												</p>												
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/fYXDtellO64" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Download Quote in PDF?
												</h3>
												<p>
													This video clip will show you how to download PDF quote.
												</p>												
											</div>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/FuHV82tjb3U" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Direct Email Quote?
												</h3>
												<p>
													This video clip will show you how to direct send quote via email.
												</p>												
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/AzzNYF-6Yvc" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Generate Guest URL?
												</h3>
												<p>
													This video clip will show you how to general guest URL for guest to view the quote online and accept or reject the quote.
												</p>												
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/QsKPxxL1DIA" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Convert Quote to Invoice?
												</h3>
												<p>
													This video clip will show you how to convert quote into invoice.
												</p>												
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="panel-187382">
								<p></p>
								<div class="row">
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/AwWvfvPBNPI" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Add New Invoice?
												</h3>
												<p>
													This video clip will show you how to add new invoice.
												</p>												
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/GMIV77_ma88" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Add Item from Inventory?
												</h3>
												<p>
													This video clip will show you how to add item from your inventory list.
												</p>												
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/WebDWxf8TGY" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Add Discount?
												</h3>
												<p>
													This video clip will show you how to add discount per item in your invoice.
												</p>												
											</div>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/ZsYa8qjDqto" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Add GST?
												</h3>
												<p>
													This video clip will show you how to add GST in your invoice.
												</p>												
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/fJfFe7dbx_o" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to View Invoice?
												</h3>
												<p>
													This video clip will show you how to view created invoice and filter via status or search box.
												</p>												
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/9sbeymY8V2E" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Download Invoice in PDF?
												</h3>
												<p>
													This video clip will show you how to download PDF invoice.
												</p>												
											</div>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/QAd_rIL_jPU" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Direct Email Invoice?
												</h3>
												<p>
													This video clip will show you how to direct send invoice via email.
												</p>												
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/fkwkKaQOdbs" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Download Delivery Order in PDF?
												</h3>
												<p>
													This video clip will show you how to download PDF delivery order.
												</p>												
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/MSmSe6UWcaA" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Generate Guest URL?
												</h3>
												<p>
													This video clip will show you how to general guest URL for guest to view the invoice online and make payment (If payment gateway is setup).
												</p>												
											</div>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/MZEiDyccUtw" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Convert Invoice to Recurring Invoice?
												</h3>
												<p>
													This video clip will show you how to convert normal invoice to recurring invoice.
												</p>												
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="panel-187383">
								<p></p>
								<div class="row">
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/wn9V4hYJIzg" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Enter New Payment?
												</h3>
												<p>
													This video clip will show you how to enter new payment record.
												</p>												
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/vsER4Wp19ZM" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to View Payment History?
												</h3>
												<p>
													This video clip will show you how to view payment records.
												</p>												
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="panel-187384">
								<p></p>
								<div class="row">
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/ccakhJ-2e0c" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Generate Invoice Aging?
												</h3>
												<p>
													This video clip will show you how to generate invoice aging and download in PDF.
												</p>												
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/YaZOBRU-fWc" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Generate Payment History?
												</h3>
												<p>
													This video clip will show you how to generate payment history and download in PDF.
												</p>												
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/dV6bxLr6nsw" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Generate Sales Report by Client?
												</h3>
												<p>
													This video clip will show you how to generate sales report by client and download in PDF.
												</p>												
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="panel-187385">
								<p></p>
								<div class="row">
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/9W8eJs3amkA" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Add Custom Fields?
												</h3>
												<p>
													This video clip will show you how to add custom fields for client, vendor, quote and invoice.
												</p>												
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/0xp8o6pcQWM" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Create Email Templates?
												</h3>
												<p>
													This video clip will show you how to create email templates for your quote, invoices and reminder.
												</p>												
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/3Jhy9p0dNeA" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Manage Quote & Invoice Group?
												</h3>
												<p>
													This video clip will show you how to manage your quote and invoice group, including running number and format of your quote and invoice.
												</p>												
											</div>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/QVmeZ_OZx_g" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Add Payment Methods?
												</h3>
												<p>
													This video clip will show you how to add additional payment methods.
												</p>												
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/rfHvW6JEXjU" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Adjust Tax & Discount Rate?
												</h3>
												<p>
													This video clip will show you how to adjust the tax and discount rate.
												</p>												
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="thumbnail">
											<iframe width="350" height="200" src="//www.youtube.com/embed/eYWYXnQXT-o" frameborder="0" allowfullscreen></iframe>
											<div class="caption">
												<h3>
													How to Add New User?
												</h3>
												<p>
													This video clip will show you how to manage your system user account, including add/edit/remove admin or guest accounts. By adding guest account, you may assign client to the guest account so that they only can access to the assigned client information without touching others.
												</p>												
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>