<%@page import="com.mollatech.axiom.nucleus.db.Units"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet"%>
<%@include file="header.jsp" %>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<script src="./assets/js/operators.js"></script>
<%  
       UnitsManagemet uMngt = new UnitsManagemet();
    Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
   
    String operatorsource = LoadSettings.g_sSettings.getProperty("axiom.operator.source");
    if(operatorsource == null){
        operatorsource="MYSQL";
    }
%>
<div class="container-fluid">
    <h1 class="text-success">Search Your Ldap Operator..</h1>
    <h3>Search Operator</h3>   
    <div class="input-append">
        <form id="searchUserForm" name="searchUserForm">
            <input type="text" id="_keyword" name="_keyword" placeholder="Search name.." class="span4"><span class="add-on"><i class="icon-search"></i></span>
            <a href="#" class="btn btn-success" onclick="SearchOperators()">Search Now</a>
        </form>
    </div>

    <!--    <div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false">
            <div class="modal-header">
                <h1>Processing...</h1>
            </div>
            <div class="modal-body">
                <div class="progress progress-striped active">
                    <div class="bar" style="width: 100%;"></div>
                </div>
            </div>
        </div>-->

    <div id="operatorsearch_table_main">
    </div> 
    <br>
    <div id="addldapopertor" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3 id="myModalLabel">Add Ldap Operator</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="addldapoperatorForm">
                    <fieldset>
                        <!-- Name -->
                        <input type="hidden" id="_operatorname" name="_operatorname" >
                        <input type="hidden" id="_operatormobile" name="_operatormobile" >
                        <input type="hidden" id="_operatoremail" name="_operatoremail" >
                        <div class="control-group">
                            <label class="control-label"  for="username">Unit Name</label>
                            <div class="controls">
<!--                                <input type="text" id="_NameE" name="_NameE" placeholder="Mumbai/Pune" class="input-xlarge">-->
                   <select class="span4" name="_units" id="_units">
                                <%
                                    Units[] uList = uMngt.ListUnitss(sessionid, operator.getChannelid());
                                    if (uList != null) {
                                        for (int i = 0; i < uList.length; i++) {
                                            if(uList[i].getStatus() == 1){//only active
                                %>
                                <option value="<%=uList[i].getUnitid()%>"><%=uList[i].getUnitname()%></option>
                                <%
                                            }}
                                    }
                                %>
                            </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Select Role</label>
                            <div class="controls">
                                
                                <select class="span5" name="_role" id="_role">
                                <%for(int i=0;i<roles.length;i++){%>
                                  <%if(!roles[i].getName().equals("sysadmin")){%>
                                  <option value='<%=roles[i].getRoleid()+","+roles[i].getName()%>'>'<%=roles[i].getName()%>'</option>
                                    <%}}%>
<!--                                    <option value="2">Admin</option>
                                    <option value="3">Helpdesk</option>
                                    <option value="4">Reporter</option>-->
                                </select>
                            </div>
                        </div>

                        <hr>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <div id="addldapopertor-result"></div>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button class="btn btn-primary" onclick="addLdapOperator()" id="addldapopertor">Add Operator</button>
           </div>
    </div>



    <%@include file="footer.jsp" %>