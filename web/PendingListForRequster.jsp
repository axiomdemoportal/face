<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Otptokens"%>
<%@page import="com.mollatech.axiom.nucleus.db.Usergroups"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserGroupsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.AccessControllerVariables"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.ChannelProfile"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.AxiomProtect"%>
<%@page import="com.mollatech.axiom.connector.access.controller.ApprovalSetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SchedulerManagement"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.db.Approvalsettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.AuthorizationManagement"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.connector.user.TokenStatusDetails"%>
<%@page import="com.mollatech.axiom.nucleus.db.Certificates"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Pkitokens"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/authorization.js"></script>


<div class="container-fluid">
    <h1 class="text-success">Pending List For Approval</h2>
        <!--<h3>Make Your Own Report</h3>-->
        <%            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
            String _channelId = channel.getChannelid();
            AuthorizationManagement aObj = new AuthorizationManagement();
            Approvalsettings[] arrRequest = aObj.getALLRequsterPendingRequest(sessionId, _channelId, operatorS.getOperatorid(), AuthorizationManagement.AUTORIZATION_PENDING_STATUS);
            String strerr = "No Record Found";
            DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        %>

        <div class="row-fluid">
            <div id="licenses_data_table">
                <h3>Recent Additions</h3>
                <table class="table table-striped" id="table_main">

                    <tr>
                        <td>No.</td>
                        <td>Requester's Action</td>
                        <td>Old value</td>
                        <td>New value</td>
                        <td>Reject</td>
                        <td>Marked By</td>
                        <td>User Name</td>
                        <td>Action</td>
                        <td>Created On</td>
                        <td>Expired On</td>

                    </tr>
                    <%
                        if (arrRequest != null) {
                            OperatorsManagement opMngt = new OperatorsManagement();
                            UserManagement uMngt = new UserManagement();
                            for (int i = 0; i < arrRequest.length; i++) {

                                byte[] obj = arrRequest[i].getApprovalSettingEntry();
                                byte[] f = AxiomProtect.AccessDataBytes(obj);
                                ByteArrayInputStream bais = new ByteArrayInputStream(f);
                                Object object = AuthorizationManagement.deserializeFromObject(bais);

                                ApprovalSetting approvalSetting = null;
                                if (object instanceof ApprovalSetting) {
                                    approvalSetting = (ApprovalSetting) object;
                                }
                                String strStatus = "-";
                                if (arrRequest[i].getStatus() == 1) {
                                    strStatus = "Active";
                                } else if (arrRequest[i].getStatus() == 2) {
                                    strStatus = "Pending";
                                }

                    %>

                    <tr>
                        <%if (approvalSetting != null) {
                                if (operatorS.getOperatorid().equals(approvalSetting.makerid)) {

                                    Operators oprbj = opMngt.getOperatorById(_channelId, approvalSetting.makerid);
                                    AuthUser u = uMngt.getUser(sessionId, _channelId, approvalSetting.userId);
                                    String userName = "-";
                                    if (u != null) {
                                        userName = u.getUserName();
                                    }
                                    String oldValue = "NA";
                                    String newValue = "NA";
                                    if (u != null) {
                                        OTPTokenManagement otpMgmt = new OTPTokenManagement(channel.getChannelid());

                                        if (approvalSetting.tokenSubCategory == 0) {

                                            if (approvalSetting.action == AccessControllerVariables.ASSIGNTOKEN_SENDREGCODE) {
                                                oldValue = "NA";
                                                if (approvalSetting.allowAlert.equalsIgnoreCase("email")) {
                                                    newValue = "Send Regcode To Email " + u.getEmail() + "For Token Category:" + approvalSetting.tokenCategory + " TokenSubcategory" + approvalSetting.tokenSubCategory;
                                                } else {
                                                    newValue = "Send Regcode To Phoneno " + u.getPhoneNo() + "For Token Category:" + approvalSetting.tokenCategory + " TokenSubcategory" + approvalSetting.tokenSubCategory;
                                                }
                                            } else if (approvalSetting.action == AccessControllerVariables.CHANGE_USERGROUP) {
                                                UserGroupsManagement usergrp = new UserGroupsManagement();
                                                Usergroups userGroup = usergrp.getGroupByGroupId(sessionId, channel.getChannelid(), u.getGroupid());
                                                oldValue = "Group Name " + userGroup.getGroupname();
                                                newValue = "Change User group" + approvalSetting.user_groupname;
                                            } else if (approvalSetting.action == AccessControllerVariables.USER_CHANGEDETAILS) {
                                                oldValue = "UserName " + u.getUserName() + " Country:" + u.getCountry() + " Designation:" + u.getDesignation() + " Location" + u.getLocation() + " Organization:" + u.getOrganisation();
                                                newValue = "Change user Details" + " Username:" + approvalSetting.userName + " Country" + approvalSetting.user_country + " Designation" + approvalSetting.user_designation + " Loation" + approvalSetting.user_location + " Organization:" + approvalSetting.user_organization;

                                            } else if (approvalSetting.action == AccessControllerVariables.USER_STATUS) {
                                                oldValue = "userid:" + u.getUserId() + " Status:" + u.getStatus();
                                                newValue = "userid:" + approvalSetting.userId + " Status" + approvalSetting.userStatus;
                                            } else if (approvalSetting.action == AccessControllerVariables.RESET_USER_PASSWORD || approvalSetting.action == AccessControllerVariables.USER_RESEND_PASSWORD || approvalSetting.action == AccessControllerVariables.USER_SET_SEND_PASSWORD || approvalSetting.action == AccessControllerVariables.USER_RESET_PASSWORD) {
                                                oldValue = "userid:" + u.getUserId() + " Status:" + u.getStatus();
                                                newValue = "userid:" + approvalSetting.userId + " Status" + approvalSetting.userStatus + "Reseted Password Sent on Email";
                                            } else if (approvalSetting.action == AccessControllerVariables.USER_EDIT) {
                                                oldValue = "UserName" + u.getUserName() + "PhonNo" + u.getPhoneNo() + "EMaildId" + u.getEmail();
                                                newValue = "UserName" + approvalSetting.userName + "PhNo" + approvalSetting.userPhone + "EmailId" + approvalSetting.userEmail;

                                            } else if (approvalSetting.action == AccessControllerVariables.REPLACE_TOKEN) {
                                                TokenStatusDetails tokenDetailsOfHard = null;
                                                String serialNumberhard = "------";
                                                TokenStatusDetails tokendetails[] = null;
                                                OTPTokenManagement otp = new OTPTokenManagement(channel.getChannelid());
                                                tokendetails = otp.getTokenList(sessionId, _channelId, u.getUserId());
                                                for (int j = 0; j < tokendetails.length; j++) {
                                                    if (tokendetails[j].Catrgory == OTPTokenManagement.HARDWARE_TOKEN) {
                                                        tokenDetailsOfHard = tokendetails[j];
//                           
                                                        if (tokenDetailsOfHard.serialnumber != null) {
                                                            serialNumberhard = tokenDetailsOfHard.serialnumber;
                                                        }
                                                    }
                                                }
                                                oldValue = "UserName" + u.getUserName() + "PhonNo" + u.getPhoneNo() + "EMaildId" + u.getEmail() + "Serial no." + serialNumberhard;
                                                u = uMngt.getUser(sessionId, _channelId, approvalSetting.userId);
                                                newValue = "UserName" + u.userName + "PhoneNo" + u.phoneNo + "EmailId" + u.email + "New Serial no" + approvalSetting.newtokenSerialno;
                                            }
                                        } else {
                                            Otptokens[] otpTopken = otpMgmt.getTokenListv2(sessionId, channel.getChannelid(), u.userId);
                                            if (otpTopken != null) {
                                                if (approvalSetting.tokenCategory != 0) {
                                                    for (int tk = 0; tk < otpTopken.length; tk++) {
                                                        if (approvalSetting.tokenCategory == otpTopken[tk].getCategory() && approvalSetting.tokenSubCategory == otpTopken[tk].getSubcategory()) {

                                                            oldValue = "Token Category" + otpMgmt.getTokenType(otpTopken[tk].getCategory(), otpTopken[tk].getSubcategory());
                                                            oldValue = oldValue + "Status" + otpMgmt.getTokenStatusFromValue(otpTopken[tk].getStatus());

                                                        } else if (approvalSetting.tokenCategory != 0) {
                                                            for (int k = 0; k < otpTopken.length; k++) {
                                                                if (approvalSetting.tokenCategory == otpTopken[k].getCategory()) {

                                                                    oldValue = "Token Category" + otpMgmt.getTokenType(otpTopken[k].getCategory(), otpTopken[k].getSubcategory());
                                                                    oldValue = oldValue + "Status" + otpMgmt.getTokenStatusFromValue(otpTopken[k].getStatus());

                                                                }

                                                            }
                                                        }

                                                    }
                                                }
                                            } else {
                                                oldValue = "Not Assigned";
                                            }
                                            if (approvalSetting.tokenCategory != 0) {
                                                newValue = "Token Category" + otpMgmt.getTokenType(approvalSetting.tokenCategory, approvalSetting.tokenSubCategory);
                                                if (approvalSetting.tokenStatus != null) {
                                                    newValue = newValue + "Status" + otpMgmt.getTokenStatusFromValue(Integer.parseInt(approvalSetting.tokenStatus));
                                                }
                                            }
                                                
                                        }
                                        
                                    }
                                     if(approvalSetting.action == AccessControllerVariables.USER_ADD){
                                              newValue="User name "+approvalSetting.userName+
                                                        "Email id "+approvalSetting.userEmail+
                                                         "Mobile "+approvalSetting.userPhone;  
                                            }
                                    String strAuthStatus = "-";
                                    if (arrRequest[i].getStatus() == AuthorizationManagement.AUTORIZATION_PENDING_STATUS) {
                                        strAuthStatus = "Pending";
                                    } else if (arrRequest[i].getStatus() == AuthorizationManagement.AUTORIZATION_APPROVE_STATUS) {
                                        strAuthStatus = "Approve";
                                    } else if (arrRequest[i].getStatus() == AuthorizationManagement.AUTORIZATION_REJECT_STATUS) {
                                        strAuthStatus = "Reject";
                                    }


                        %>
                        <td><%=(i + 1)%></td>
                        <td><%= approvalSetting.itemid%></td>
                        <td><%=oldValue%></td>
                        <td><%=newValue%></td>
                        <td><button onclick="removeReq('<%= arrRequest[i].getApprovalId()%>','<%=oldValue%>','<%=newValue%>')" type="button button-danger" class="btn btn-mini">Reject</button></td>
                        <td><%= oprbj.getName()%></td>
                        <td><%=userName%></td>
                        <td><%=strAuthStatus%></td>
                        <td><%=sdf.format(arrRequest[i].getCreatedOn())%></td>
                        <td><%=sdf.format(arrRequest[i].getExpireOn())%></td>

                        <%}%>
                    </tr>


                    <%}
                        }
                    } else {%>
                    <tr>
                        <td><%=1%></td>
                        <td><%= strerr%></td>
                        <td><%= strerr%></td>
                        <td><%= strerr%></td>
                        <td><%= strerr%></td>
                        <td><%= strerr%></td>
                        <td><%= strerr%></td>
                        <td><%= strerr%></td>

                    </tr>
                    <%}%>


                </table>


            </div>
        </div>
        <script type="text/javascript">

        </script>
</div>
<div id="requestReject" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Reject Request</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="requestRejectform" name="requestRejectform">
                <fieldset>
                    <input type="hidden" id="_approvalID" name="_approvalID" >
                    <input type="hidden" id="_unitId" name="_unitId" >
                    <input type="hidden" id="_RejectionType" name="_RejectionType" value="requester" >
                    <div class="control-group">
                        <label class="control-label"  for="username">Description</label>
                        <div class="controls">
                            <textarea class="form-control" rows="3" id="_description" name="_description"></textarea>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="rejectkyc-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="removeAuthorizationReq()" id="addUserButton">Apply Now>></button>
    </div>
</div>        
<%@include file="footer.jsp" %>