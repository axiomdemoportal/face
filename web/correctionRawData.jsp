<%@page import="java.util.List"%>
<%@page import="java.util.Arrays"%>
<%@page import="com.mollatech.axiom.nucleus.db.ApOcrresult"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OcrResultManagement"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html lang="en">
    <%@page import="java.awt.image.BufferedImage"%>
    <%@page import="java.io.ByteArrayOutputStream"%>
    <%@page import="java.io.File"%>"
    <%@page import="javax.imageio.ImageIO"%> 
    <%@page import="java.io.FileReader"%>
    <%@page import="java.io.BufferedReader"%>
    <%@page import="java.nio.channels.FileChannel"%>
    <%@page import="java.io.FileInputStream"%>
    <%@page import="org.apache.commons.io.FileUtils"%>
    <%@page import="java.io.File"%>
    <%@page import="org.json.JSONObject"%>
    <%@page import="java.io.ByteArrayInputStream"%>
    <%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
    <%@page import="com.mollatech.axiom.nucleus.db.DocumentTemplate"%>
    <%@page import="com.mollatech.axiom.nucleus.db.connector.management.DocsTemplatesManagement"%>
    <%@page import="java.io.IOException"%>
    <%@page import="java.io.FileOutputStream"%>
    <%@page import="java.io.OutputStreamWriter"%>
    <%@page import="java.io.BufferedWriter"%>
    <%@page import="java.io.Writer"%>
    <%@page import="org.apache.commons.io.IOUtils"%>
    <%@page import="java.io.InputStream"%>
    <%@page import="com.mollatech.axiom.ocr.ResizeImage"%>
    <%@page import="com.mollatech.axiom.document.PreprocessImage"%>
    <%@page import="org.bouncycastle.util.encoders.Base64"%>
    <%@include file="header.jsp" %>
    <!--<script src="assets/dococr/js/documentsTemplate.js" type="text/javascript"></script>-->
    <%
        String docid = (String) request.getSession().getAttribute("docid");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        OcrResultManagement ocrResultManagement = new OcrResultManagement();
        ApOcrresult ocrresultObj = ocrResultManagement.getDocumentDetailsByDocId(docid, channel.getChannelid());
        PreprocessImage preObj = new PreprocessImage();
        byte[] image = ocrresultObj.getTestimageofpdf();
        String base64image = Base64.toBase64String(image);
        JSONObject resultObj;
        String result = null;
        System.out.println("In correction jsp");
        result =  preObj.callserviceForRawdata(docid, base64image, "pdf");
        resultObj = new JSONObject(result);
        List<String> list = new ArrayList<String>();
        JSONObject resultString = (JSONObject) resultObj.get("task");
        String resultdata = resultString.get("data").toString();
        list = Arrays.asList(resultString.get("data").toString());
        String resultcode = resultString.get("Result").toString();
        String errormessage = resultString.get("errorMessage").toString();
        System.out.println("=========="+list.get(0));
        System.out.println("data------> "+resultdata.toString());
        String[] listArray = resultdata.split("\\s*,\\s*");
        String templatename = request.getParameter("fileName");
//        String encodedImage = (String) request.getSession().getAttribute("preprocessImage");
        ////////PreprocessImage///////////////////////  
//        byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(encodedImage);
//        BufferedImage img = ImageIO.read(new ByteArrayInputStream(imageBytes));
//        request.getSession().setAttribute("_finalImage", imageBytes);
//        System.out.println("encodedImage == " + encodedImage);
//        String width = Integer.toString(img.getWidth());
//        String height = Integer.toString(img.getHeight());
    %>
    <style>
        form div {
            padding: x; /*default div padding in the form e.g. 5px 0 5px 0*/
            margin: y; /*default div padding in the form e.g. 5px 0 5px 0*/
        }
        .divForText { /*For Text line only*/
            padding: a;
            margin: b;
        }
        .divForLabelInput{ /*For Text and Input line */
            padding: c;
            margin: d;
        }
        .divForInput{ /*For Input line only*/
            padding: e;
            margin: f;
        }
    </style>
    <form id="jsondata" name="jsondata">
        <div id="container" style="width:100%;">                                   
            <div id="left" class="panel panel-default" style="overflow-y: auto; height:600px;float:left; width:50%;">
                <img src="data:image/png;base64,<%=base64image%>"/>
            </div>                     
            <div class="text-center" class="panel panel-default" id="right" style="overflow-y: auto; height:600px;float:left; width:50%;"> 
                <h1>Templete Correction</h1>     
                <br>
                <table style="width:100%">         
                    <tr>
                        <th style="text-align: center">OCR Data</th>
                        <th style="text-align: center">Corrected Data</th> 
                    </tr>
                </table>
                <%
                    for (int i = 0; i < listArray.length; i++) {
                %>
                <div>
                    <center>
                        <table>
                            <th>
                                <div>
                                    <td>
                                        <input type="text" id="rawvalue<%=i%>" name="rawvalue<%=i%>" value="<%=listArray[i]%>"  readonly=""/>
                                    </td>
                                </div>
                            </th>
                            <th>
                                <div>
                                    <td>
                                        <input type="text" id="correctvalue<%=i%>" name="correctvalue<%=i%>"/>
                                        <button type="button" onclick=" " id="<%=i%>" name="correctbutton<%=i%>" >Correct</button>
                                    </td>
                                </div>
                            </th>
                        </table>
                    </center>
                </div>
                <%
                    }
                %>
            </div>                   
        </div> 
    </form>
    <script>


        function alert1() {
            alert('hi');
        }
        function allAlert(msg) {
            bootbox.alert("<h2>" + msg + "</h2>", function (result) {
                if (result === false) {
                } else {
                    //end here
                }
            });
        }
        function strcmpUsers(a, b)
        {
            return (a < b ? -1 : (a > b ? 1 : 0));
        }
    </script>
</html>