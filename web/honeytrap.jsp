<%@page import="com.mollatech.axiom.connector.access.controller.AccessMatrixSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.Operators"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.MobileTrustManagment"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Geotrack"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.GeoLocationManagement"%>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Random" %>
<%--<%@include file="header.jsp" %>--%>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="./assets/js/json_sans_eval.js"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script src="./assets/js/geotracking.js"></script>
<%    int REGISTER = 1;
    int LOGIN = 2;
    int TRANSACTION = 3;
    int CHANGEINPROFILE = 4;

    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    String _startdate = request.getParameter("_startdate");
    String _enddate = request.getParameter("_enddate");
    String _type = request.getParameter("_type");
    DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    Date startDate = null;
    if (_startdate != null && !_startdate.isEmpty()) {
        startDate = (Date) formatter.parse(_startdate);
    }
    Date endDate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        endDate = (Date) formatter.parse(_enddate);
    }
    GeoLocationManagement geoObj = new GeoLocationManagement();
    Geotrack[] Tracks = geoObj.getGeoTracksByTxTypeDuration(channel.getChannelid(), MobileTrustManagment.INVALIDIMAGE, endDate, startDate);

    String strerr = "No Records Found";


%>


<div class="row-fluid">
    <div class="span6">
        <div class="control-group">                        
            <div class="span1">
                <div class="control-group form-inline">
                    
                     <%Operators oprObjI = (Operators) session.getAttribute("_apOprDetail");
                     AccessMatrixSettings accessObjN = (AccessMatrixSettings) session.getAttribute("_apAccessEntry");
                      if(oprObjI.getRoleid() != 1 ){
                          if(accessObjN.downloadHoneyTrapReport == true){%>//1 sysadmin
                             <a href="#" class="btn btn-info" onclick="geoHoneyTrackReportCSV()" >             
                          <%}else{%>
                              <a href="#" class="btn btn-info" onclick="InvalidRequestHoneyTrap('honeyTrapDownload')" >
                          <%}}else{%>
                          <a href="#" class="btn btn-info" onclick="geoHoneyTrackReportCSV()" >  
                         <%}%>
                          <i class="icon-white icon-chevron-down"></i> CSV</a>
                </div>
            </div>
            <div class="span1">
                <div class="control-group form-inline">
                     <%if(oprObjI.getRoleid() != 1 ){
                          if(accessObjN.downloadHoneyTrapReport == true){%>//1 sysadmin
                    <a href="#" class="btn btn-info" onclick="geoHoneyTrackReportPDF()" >
                        <%}else{%>
                         <a href="#" class="btn btn-info" onclick="InvalidRequestHoneyTrap('honeyTrapDownload')" >
                        <%}}else{%>
                        <a href="#" class="btn btn-info" onclick="geoHoneyTrackReportPDF()" >
                        <%}%>
                        <i class="icon-white icon-chevron-down"></i> PDF</a>
                </div>
            </div>
            <div class="span1">
                <div class="control-group form-inline">
                    <!--<a href="#" class="btn btn-info" onclick="geoHoneyTrackReportTXT()" >-->
                    <%if(oprObjI.getRoleid() != 1 ){
                          if(accessObjN.downloadHoneyTrapReport == true){%>//1 sysadmin
                   <a href="#" class="btn btn-info" onclick="geoHoneyTrackReportTXT()" >
                        <%}else{%>
                         <a href="#" class="btn btn-info" onclick="InvalidRequestHoneyTrap('honeyTrapDownload')" >
                        <%}}else{%>
                        <a href="#" class="btn btn-info" onclick="geoHoneyTrackReportTXT()" >
                        <%}%>
                        <i class="icon-white icon-chevron-down"></i> TXT</a>
                </div>
            </div>


        </div>
    </div>
</div>
<div id="licenses_data_table">

    <input type="hidden" id="_tracking" name="_tracking"  >
    <table class="table table-striped" id="table_main">
        <tr>
            <td>No.</td>
            <td>IP Address</td>
            <td>Lattitude</td>
            <td>Longitude</td>
            <td>City</td>
            <td>State</td>
            <td>Country</td>
            <td>Zip code</td>
            <td>Type</td>
            <td>Dated</td>
        </tr>
        <%            if (Tracks != null) {
                for (int i = 0; i < Tracks.length; i++) {
                    SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String start = df2.format(Tracks[i].getExecutedOn());

                    String strtype = "";
                    if (Tracks[i].getTxtype() == REGISTER) {
                        strtype = "Register";
                    } else if (Tracks[i].getTxtype() == LOGIN) {
                        strtype = "Login";
                    } else if (Tracks[i].getTxtype() == TRANSACTION) {
                        strtype = "Transaction";
                    } else if (Tracks[i].getTxtype() == CHANGEINPROFILE) {
                        strtype = "Change Profile";
                    } else if (Tracks[i].getTxtype() == MobileTrustManagment.INVALIDIMAGE) {
                        strtype = "Honey Trap";
                    } else if (Tracks[i].getTxtype() == MobileTrustManagment.VALIDIMAGE) {
                        strtype = "Image";
                    }
        %>
        <tr>
            <td><%=(i + 1)%></td>
            <td><%=Tracks[i].getIp()%></td>
            <td><%=Tracks[i].getLattitude()%></td>
            <td><%=Tracks[i].getLongitude()%></td>
            <td><%=Tracks[i].getCity()%></td>
            <td><%=Tracks[i].getState()%></td>
            <td><%=Tracks[i].getCountry()%></td>
            <td><%=Tracks[i].getZipcode()%></td>
            <td><%=strtype%></td>
            <td><%=start%></td>
        </tr>
        <%
            }
        } else {%>
        <tr>
            <td><%=1%></td>
            <td><%= strerr%></td>
            <td><%= strerr%></td>
            <td><%= strerr%></td>
            <td><%= strerr%></td>
            <td><%= strerr%></td>
            <td><%= strerr%></td>
            <td><%= strerr%></td>
            <td><%= strerr%></td>
            <td><%= strerr%></td>
            <%}%>
        </tr>
    </table>
</div>
<div class="row-fluid">
    <div class="span6">
    <div class="control-group">                        
            <div class="span1">
                <div class="control-group form-inline">
                    
                     <%if(oprObjI.getRoleid() != 1 ){
                          if(accessObjN.downloadHoneyTrapReport == true){%>//1 sysadmin
                             <a href="#" class="btn btn-info" onclick="geoHoneyTrackReportCSV()" >             
                          <%}else{%>
                              <a href="#" class="btn btn-info" onclick="InvalidRequestHoneyTrap('honeyTrapDownload')" >
                          <%}}else{%>
                          <a href="#" class="btn btn-info" onclick="geoHoneyTrackReportCSV()" >  
                         <%}%>
                          <i class="icon-white icon-chevron-down"></i> CSV</a>
                </div>
            </div>
            <div class="span1">
                <div class="control-group form-inline">
                     <%if(oprObjI.getRoleid() != 1 ){
                          if(accessObjN.downloadHoneyTrapReport == true){%>//1 sysadmin
                    <a href="#" class="btn btn-info" onclick="geoHoneyTrackReportPDF()" >
                        <%}else{%>
                         <a href="#" class="btn btn-info" onclick="InvalidRequestHoneyTrap('honeyTrapDownload')" >
                        <%}}else{%>
                        <a href="#" class="btn btn-info" onclick="geoHoneyTrackReportPDF()" >
                        <%}%>
                        <i class="icon-white icon-chevron-down"></i> PDF</a>
                </div>
            </div>
            <div class="span1">
                <div class="control-group form-inline">
                    <!--<a href="#" class="btn btn-info" onclick="geoHoneyTrackReportTXT()" >-->
                    <%if(oprObjI.getRoleid() != 1 ){
                          if(accessObjN.downloadHoneyTrapReport == true){%>//1 sysadmin
                   <a href="#" class="btn btn-info" onclick="geoHoneyTrackReportTXT()" >
                        <%}else{%>
                         <a href="#" class="btn btn-info" onclick="InvalidRequestHoneyTrap('honeyTrapDownload')" >
                        <%}}else{%>
                        <a href="#" class="btn btn-info" onclick="geoHoneyTrackReportTXT()" >
                        <%}%>
                        <i class="icon-white icon-chevron-down"></i> TXT</a>
                </div>
            </div>


        </div>
    </div>
</div> 

<script language="javascript">
    TransactionMapHoneyTrap(<%=1%>);
</script>
<%@include file="footer.jsp" %>