<%@page import="com.mollatech.axiom.nucleus.db.connector.management.QuestionsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Questions"%>
<%@include file="header.jsp" %>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<script src="./assets/js/challengeresponse.js"></script>

<div class="container-fluid">
    <h1 class="text-success">Challenge & Response Management</h1>
    <p>List of users in the system. Their Questions and Answers management can be managed from this interface.</p>
    <h3>Search Users</h3>   
    <div class="input-append">
        <form id="searchUserForm" name="searchUserForm">
            <input type="text" id="_keyword" name="_keyword" placeholder="Search using name, phone, email..." class="span4"><span class="add-on"><i class="icon-search"></i></span>
            <a href="#" class="btn btn-success" onclick="searchChallengeUsers()">Search Now</a>
        </form>
    </div>
    <div id="challenge_table_main">
    </div>
</p><br>

<div id="userChallengeResponseauditDownload" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="idauditDownload"></div></h3>
        <h3 id="myModalLabel">Download Audit</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="testSMSPrimaryForm" name="testSMSPrimaryForm">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_auditUserID" name="_auditUserID"/>
                    <input type="hidden" id="_auditUserName" name="_auditUserName"/>
                    <div class="control-group">
                        <label class="control-label"  for="username">Start Date</label>
                        <div class="controls" align="left" >

                            <!--<span class="add-on">From:</span>-->   
                            <div id="Pushdatetimepicker1" class="input-append date">
                                <input id="_auditStartDate" name="_auditStartDate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">End Date</label>
                        <div class="controls" align="left">
                            <!--<span class="add-on">Till:</span>-->   
                            <div id="Pushdatetimepicker2" class="input-append date">
                                <input id="_auditEndDate" name="_auditEndDate" type="text" data-format="dd-mm-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>                    
                </fieldset>
            </form>
        </div>
    </div>

    <div class="modal-footer">
        <div id="editoperator-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="searchUsersChallengeResponseAudit()" id="buttonEditOperatorSubmit">Show Audit</button>
    </div>
</div>
<script>
    $(function () {
        $('#Pushdatetimepicker1').datepicker({
            format: 'dd/mm/yyyy',
            language: 'pt-BR'
        });
    });
    $(function () {
        $('#Pushdatetimepicker2').datepicker({
            format: 'dd/mm/yyyy',
            language: 'pt-BR'
        });
    });
</script>
<%@include file="footer.jsp" %>