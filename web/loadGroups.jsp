<%@page import="com.mollatech.axiom.nucleus.db.Usergroups"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserGroupsManagement"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Users"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<html>
    <div class="control-group">
        <label class="control-label"  for="username"></label>
        <div class="controls">
            <%
                Channels channels = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                String sessionId = (String) request.getSession().getAttribute("_apSessionID");
                String channelId = channels.getChannelid();
            %>
            <select id="_GroupSelect" name="_GroupSelect">
                <option value="-1" disabled selected>Select Group</option>
                <%
                    UserGroupsManagement ugm = new UserGroupsManagement();
                    Usergroups[] usr = ugm.getAllGroup(sessionId, channelId);
                    for (int i = 0; i < usr.length; i++) {
                %>
                <option value="<%=usr[i].getGroupid()%>">  <%=usr[i].getGroupname()%> </option>
                <%}%>
            </select> 
        </div>
    </div>


