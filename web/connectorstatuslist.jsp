<%@page import="com.mollatech.axiom.nucleus.settings.ConnectorStatusInternal"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Connectorstatusaudit"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.ConnectorStatusAuditManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>
<%@include file="header.jsp" %>
<!--<script src="./assets/js/jquery.js"></script>-->
<script src="./assets/js/connectorstatus.js"></script>
<div class="container-fluid">
    <h1 class="text-success">Connector Status Reports</h1>

    <%    
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        ConnectorStatusAuditManagement cManagement = new ConnectorStatusAuditManagement();
        SettingsManagement sManagement = new SettingsManagement();
        Object obj = sManagement.getSetting(channel.getChannelid(), SettingsManagement.CONNECTORS_STATUS, SettingsManagement.PREFERENCE_ONE);
        ConnectorStatusInternal cStatus = null;
        Connectorstatusaudit[] cStatusList = null;
        if (obj != null) {
            cStatus = (ConnectorStatusInternal) obj;
            //SMS
            cStatusList = new Connectorstatusaudit[12];
            cStatusList[0] = new Connectorstatusaudit();
            cStatusList[0].setAuditid(""+0);
            cStatusList[0].setChannelid(channel.getChannelid());
            cStatusList[0].setType(SettingsManagement.SMS);
            cStatusList[0].setPerference(SettingsManagement.PREFERENCE_ONE);
            cStatusList[0].setStatus(cStatus.SMSPrimary);
            cStatusList[0].setCreatedOn(cStatus.statusDateTime);

            cStatusList[1] = new Connectorstatusaudit();
            cStatusList[1].setAuditid(""+1);
            cStatusList[1].setChannelid(channel.getChannelid());
            cStatusList[1].setType(SettingsManagement.SMS);
            cStatusList[1].setPerference(SettingsManagement.PREFERENCE_TWO);
            cStatusList[1].setStatus(cStatus.SMSSecondary);
            cStatusList[1].setCreatedOn(cStatus.statusDateTime);
            //USSD
            cStatusList[2] = new Connectorstatusaudit();
            cStatusList[2].setAuditid(""+2);
            cStatusList[2].setChannelid(channel.getChannelid());
            cStatusList[2].setType(SettingsManagement.USSD);
            cStatusList[2].setPerference(SettingsManagement.PREFERENCE_ONE);
            cStatusList[2].setStatus(cStatus.USSDPrimary);
            cStatusList[2].setCreatedOn(cStatus.statusDateTime);

            cStatusList[3] = new Connectorstatusaudit();
            cStatusList[3].setAuditid(""+3);
            cStatusList[3].setChannelid(channel.getChannelid());
            cStatusList[3].setType(SettingsManagement.USSD);
            cStatusList[3].setPerference(SettingsManagement.PREFERENCE_TWO);
            cStatusList[3].setStatus(cStatus.USSDSecondary);
            cStatusList[3].setCreatedOn(cStatus.statusDateTime);
            //VOICE
            cStatusList[4] = new Connectorstatusaudit();
            cStatusList[4].setAuditid(""+4);
            cStatusList[4].setChannelid(channel.getChannelid());
            cStatusList[4].setType(SettingsManagement.VOICE);
            cStatusList[4].setPerference(SettingsManagement.PREFERENCE_ONE);
            cStatusList[4].setStatus(cStatus.VOICEPrimary);
            cStatusList[4].setCreatedOn(cStatus.statusDateTime);

            cStatusList[5] = new Connectorstatusaudit();
            cStatusList[5].setAuditid(""+5);
            cStatusList[5].setChannelid(channel.getChannelid());
            cStatusList[5].setType(SettingsManagement.VOICE);
            cStatusList[5].setPerference(SettingsManagement.PREFERENCE_TWO);
            cStatusList[5].setStatus(cStatus.VOICESecondary);
            cStatusList[5].setCreatedOn(cStatus.statusDateTime);

            //EMAIL
            cStatusList[6] = new Connectorstatusaudit();
            cStatusList[6].setAuditid(""+6);
            cStatusList[6].setChannelid(channel.getChannelid());
            cStatusList[6].setType(SettingsManagement.EMAIL);
            cStatusList[6].setPerference(SettingsManagement.PREFERENCE_ONE);
            cStatusList[6].setStatus(cStatus.EMAILPrimary);
            cStatusList[6].setCreatedOn(cStatus.statusDateTime);

            cStatusList[7] = new Connectorstatusaudit();
            cStatusList[7].setAuditid(""+7);
            cStatusList[7].setChannelid(channel.getChannelid());
            cStatusList[7].setType(SettingsManagement.EMAIL);
            cStatusList[7].setPerference(SettingsManagement.PREFERENCE_TWO);
            cStatusList[7].setStatus(cStatus.EMAILSecondary);
            cStatusList[7].setCreatedOn(cStatus.statusDateTime);
            
            
            cStatusList[8] = new Connectorstatusaudit();
            cStatusList[8].setAuditid(""+8);
            cStatusList[8].setChannelid(channel.getChannelid());
            cStatusList[8].setType(SettingsManagement.Radius);
            cStatusList[8].setPerference(SettingsManagement.PREFERENCE_ONE);
            cStatusList[8].setStatus(cStatus.AxiomRadius);
            cStatusList[8].setCreatedOn(cStatus.statusDateTime);
            
             cStatusList[9] = new Connectorstatusaudit();
            cStatusList[9].setAuditid(""+9);
            cStatusList[9].setChannelid(channel.getChannelid());
            cStatusList[9].setType(SettingsManagement.ANDROID_PUSH_NOTIFICATION_SETTING);
            cStatusList[9].setPerference(SettingsManagement.PREFERENCE_ONE);
            cStatusList[9].setStatus(cStatus.AndroidPUSH);
            cStatusList[9].setCreatedOn(cStatus.statusDateTime);
            
            cStatusList[10] = new Connectorstatusaudit();
            cStatusList[10].setAuditid(""+10);
            cStatusList[10].setChannelid(channel.getChannelid());
            cStatusList[10].setType(SettingsManagement.IOS_PUSH_NOTIFICATION_SETTING);
            cStatusList[10].setPerference(SettingsManagement.PREFERENCE_ONE);
            cStatusList[10].setStatus(cStatus.IphonePUSH);
            cStatusList[10].setCreatedOn(cStatus.statusDateTime);

            //crtificate
            cStatusList[11] = new Connectorstatusaudit();
            cStatusList[11].setAuditid(""+11);
            cStatusList[11].setChannelid(channel.getChannelid());
            cStatusList[11].setType(SettingsManagement.RootConfiguration);
            cStatusList[11].setPerference(SettingsManagement.PREFERENCE_ONE);
            cStatusList[11].setStatus(cStatus.CertificateConnector);
            cStatusList[11].setCreatedOn(cStatus.statusDateTime);
        }

        //      perfernece(primary or secondary), statutes (UP or DOWN), Captured Time (show date and time), Audit (same as user-operator today-7days-30days-3months- etc), Show Details
    %>


    <div class="row-fluid">
        <div id="licenses_data_table">
            <h3>Recent Connector Status</h3>
            <table class="table table-striped" >

                <tr>
                    <td>No.</td>
                    <td>Name</td>
                    <td>Preference</td>
                    <td>Status</td>
                    <td>Audit For</td>
                    <td>Captured Time</td>
                    <td>Show Details</td>
                </tr>
                <%                    for (int i = 0; i < cStatusList.length; i++) {
                %>
                <tr>
                    <%
                        String strType = "";
                        if (cStatusList[i].getType() == SettingsManagement.SMS) {
                            strType = "SMS";
                        } else if (cStatusList[i].getType() == SettingsManagement.VOICE) {
                            strType = "VOICE";
                        } else if (cStatusList[i].getType() == SettingsManagement.USSD) {
                            strType = "USSD";
                        } else if (cStatusList[i].getType() == SettingsManagement.EMAIL) {
                            strType = "EMAIL";
                        } else if (cStatusList[i].getType() == SettingsManagement.Radius) {
                            strType = "RADIUS";
                        } else if (cStatusList[i].getType() == SettingsManagement.RootConfiguration) {
                            strType = "CERTIFICATE CONNECTOR";
                        } else if (cStatusList[i].getType() == SettingsManagement.FAX_SETTING) {
                            strType = "FAX";
                        } else if (cStatusList[i].getType() == SettingsManagement.ANDROID_PUSH_NOTIFICATION_SETTING) {
                            strType = "ANDROID PUSH NOTIFICATION";
                        } else if (cStatusList[i].getType() == SettingsManagement.IOS_PUSH_NOTIFICATION_SETTING) {
                            strType = "IPHONE PUSH NOTIFICATION";
                        }

                    %>


                    <%                        final int NOT_CONFIGURED = -2;
                        final int RUNNING = 0;

                        final int SUSPENDED = -1;
                        final int PRIMARY = 1;
                        final int SECONDARY = 2;

                        String strStatus = "";
                        if (cStatusList[i].getStatus() == NOT_CONFIGURED) {
                            strStatus = "NOT_CONFIGURED";
                        } else if (cStatusList[i].getStatus() == RUNNING) {
                            strStatus = "RUNNING";
                        } else if (cStatusList[i].getStatus() == SUSPENDED) {
                            strStatus = "SUSPENDED";
                        } else {
                            strStatus = "DOWN";
                        }

                        String strPreference = "";
                        if (cStatusList[i].getPerference() == PRIMARY) {
                            strPreference = "PRIMARY";
                        } else if (cStatusList[i].getPerference() == SECONDARY) {
                            strPreference = "SECONDARY";
                        }

                        /*                    
                         final int PENDING = 2;
                         final int SENT = 0;
                         final int FAILD = -1;
                         String strStatus = "";
                         if (clogobj[i].getStatus() == PENDING) {
                         strStatus = "PENDING";
                         } else if (clogobj[i].getStatus() == SENT) {
                         strStatus = "SENT";
                         } else if (clogobj[i].getStatus() == FAILD) {
                         strStatus = "FAILED";
                         }
                         */

                    %>


                    <td><%=(i + 1)%></td>
                    <td><%=strType%></td>
                    <td><%=strPreference%></td> 
                    <% if (cStatusList[i].getStatus() == NOT_CONFIGURED) {%>
                    <td><span class="label label-Default"><%=strStatus%></span></td>
                        <%} else if (cStatusList[i].getStatus() == RUNNING) {%>
                    <td><span class="label label-success "><%=strStatus%></span></td>
                        <%} else if (cStatusList[i].getStatus() == SUSPENDED) {%>
                    <td><span class="label label-info"><%=strStatus%></span></td>
                        <%} else {%>
                    <td><span class="label label-important"><%=strStatus%></span></td>
                        <%}%>


                    <td>
                        <div class="btn-group">
                            <button class="btn btn-mini">Audit for</button>
                            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" onclick="connectorAudits('<%=cStatusList[i].getType()%>', '<%=cStatusList[i].getPerference()%>', '1day')">Today</a></li>
                                <li><a href="#" onclick="connectorAudits('<%=cStatusList[i].getType()%>', '<%=cStatusList[i].getPerference()%>', '7days')">Last 7 days</a></li>
                                <li class="divider"></li>
                                <li><a href="#" onclick="connectorAudits('<%=cStatusList[i].getType()%>', '<%=cStatusList[i].getPerference()%>', '1month')">Last 30 days</a></li>
                                <li><a href="#" onclick="connectorAudits('<%=cStatusList[i].getType()%>', '<%=cStatusList[i].getPerference()%>', '2months')">Last 2 months</a></li>
                                <li><a href="#" onclick="connectorAudits('<%=cStatusList[i].getType()%>', '<%=cStatusList[i].getPerference()%>', '3months')">Last 3 months</a></li>
                                <li><a href="#" onclick="connectorAudits('<%=cStatusList[i].getType()%>', '<%=cStatusList[i].getPerference()%>', '6months')">Last 6 months</a></li>
                            </ul>
                        </div>
                    </td>
                    <td><%=cStatusList[i].getCreatedOn()%></td>
                    <%
                        if (cStatusList[i].getType() == SettingsManagement.SMS) {%>
                    <td>
                        <a href="smsgateway.jsp" class="btn btn-mini" id="moveconfigpage" name="moveconfigpage" rel="popover" data-html="true">Show Details</a>
                    </td>
                    <% } else if (cStatusList[i].getType() == SettingsManagement.VOICE) {%>
                    <td>
                        <a href="voicegateway.jsp" class="btn btn-mini" id="moveconfigpage" name="moveconfigpage" rel="popover" data-html="true">Show Details</a>
                    </td>
                    <%  } else if (cStatusList[i].getType() == SettingsManagement.USSD) {%>
                    <td>
                        <a href="ussdgateway.jsp" class="btn btn-mini" id="moveconfigpage" name="moveconfigpage" rel="popover" data-html="true">Show Details</a>
                    </td>
                    <% } else if (cStatusList[i].getType() == SettingsManagement.EMAIL) {%>
                    <td>
                        <a href="emailgateway.jsp" class="btn btn-mini" id="moveconfigpage" name="moveconfigpage" rel="popover" data-html="true">Show Details</a>
                    </td>
                    <% } else if (cStatusList[i].getType() == SettingsManagement.Radius) {%>
                    <td>
                        <a href="radiusserver2.jsp" class="btn btn-mini" id="moveconfigpage" name="moveconfigpage" rel="popover" data-html="true">Show Details</a>
                    </td>
                    <% } else if (cStatusList[i].getType() == SettingsManagement.RootConfiguration) {%>
                    <td>
                        <a href="caconnector.jsp" class="btn btn-mini" id="moveconfigpage" name="moveconfigpage" rel="popover" data-html="true">Show Details</a>
                    </td>
                    <% } else if (cStatusList[i].getType() == SettingsManagement.FAX_SETTING) {%>
                    <td>
                        <a href="faxgateway.jsp" class="btn btn-mini" id="moveconfigpage" name="moveconfigpage" rel="popover" data-html="true">Show Details</a>
                    </td>
                    <% } else if (cStatusList[i].getType() == SettingsManagement.ANDROID_PUSH_NOTIFICATION_SETTING) {%>
                    <td>
                        <a href="#" class="btn btn-mini" id="moveconfigpage" name="moveconfigpage" rel="popover" data-html="true">Show Details</a>
                    </td>
                    <% } else if (cStatusList[i].getType() == SettingsManagement.IOS_PUSH_NOTIFICATION_SETTING) {%>
                    <td>
                        <a href="#" class="btn btn-mini" id="moveconfigpage" name="moveconfigpage" rel="popover" data-html="true">Show Details</a>
                    </td>
                    <%  }
                    %>


                </tr>
                <%}%>
            </table>
        </div>
    </div>

</div>

<%@include file="footer.jsp" %>
