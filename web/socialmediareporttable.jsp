<%@page import="com.mollatech.axiom.nucleus.settings.SendNotification"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channellogs"%>
<%@page import="com.mollatech.dictum.management.BulkMSGManagement"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="java.text.SimpleDateFormat"%>


<%
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _searchtext = request.getParameter("_searchtext");
    String _startdate = request.getParameter("_startdate");
    String _enddate = request.getParameter("_enddate");
    String _type = request.getParameter("_type");

    DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    Date startDate = null;
    if (_startdate != null && !_startdate.isEmpty()) {
        startDate = (Date) formatter.parse(_startdate);
    }
    Date endDate = null;
    if (_enddate != null && !_enddate.isEmpty()) {
        endDate = (Date) formatter.parse(_enddate);
    }
    BulkMSGManagement bMngt = new BulkMSGManagement();



    int type = Integer.parseInt(_type);
    //System.out.println("The message type is " + type);
    Channellogs[] clogobj = bMngt.searchMsgObj(channel.getChannelid(), type, _searchtext, endDate, startDate);

    String start = formatter.format(startDate);
    String end = formatter.format(endDate);

    String typeSC = "None";
    if (type == SendNotification.FACEBOOK) {
        typeSC = "FACEBOOK";
    } else if (type == SendNotification.LINKEDIN) {
        typeSC = "LINKEDIN";
    } else if (type == SendNotification.TWITTER) {
        typeSC = "TWITTER";
    }

%>
<h3> Results from <%=start%> to <%=end%> for <%=typeSC%> type sent to "<%=_searchtext%>"</i></h3>

<div class="tabbable">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#msgcharts" data-toggle="tab">Charts</a></li>
        <li><a href="#msgreport" data-toggle="tab">Tabular List</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="msgcharts">
            <div class="row-fluid">
                <div class="span12">
                    <div class="control-group">
                        <div class="span4">
                            <div id="MsgReportgraphDSocial" ></div>

                        </div>
                        <div class="span7">
                            <div id="MsgReportgraphBSocial"></div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="tab-pane" id="msgreport">   
            <div class="row-fluid">
                <div class="span6">
                    <div class="control-group">                        
                        <div class="span3">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="socialReportCSV()" >
                                    <i class="icon-white icon-chevron-down"></i>Download CSV</a>
                            </div>
                        </div>
                        <div class="span3">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="socialReportPDF()" >
                                    <i class="icon-white icon-chevron-down"></i>Download PDF</a>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped" id="table_main">

                    <tr>
                        <td>No.</td>
                        <td>Message ID</td>
                        <td>Type</td>
                        <td>Delivered To</td>                                
                        <td>Status</td>
                        <td>Sent On</td>
                        <td>Update On</td>
                    </tr>
                    <% if (clogobj != null) {
                            for (int i = 0; i < clogobj.length; i++) {
//                                String strType = "";
                                String strSource = "";
                                final int PENDING = 2;
                                final int SENT = 0;
                                final int BLOCKED = -5;

                                //final int FAILD = -1;
                                String strStatus = "";

                                String strType = "";
                        if (clogobj[i].getType() == 6) {
                            strType = "FACEBOOK";
                        } else if (clogobj[i].getType() == 7) {
                            strType = "LINKEDIN";
                        } else if (clogobj[i].getType() == 8) {
                            strType = "TWITTER";
                        } 
                                
                                
                                

                                if (clogobj[i].getSourceid() == 1) {
                                    strSource = "BULK MSG";
                                } else if (clogobj[i].getSourceid() == 2) {
                                    strSource = "BULK MSG";
                                } else if (clogobj[i].getSourceid() == 3) {
                                    strSource = "BULK MSG";
                                } else if (clogobj[i].getSourceid() == 4) {
                                    strSource = "BULK MSG";
                                }


                                if (clogobj[i].getStatus() == PENDING) {
                                    strStatus = "PENDING";
                                } else if (clogobj[i].getStatus() == SENT) {
                                    strStatus = "SENT";
                                } else if (clogobj[i].getStatus() == BLOCKED) {
                                    strStatus = "BLOCKED";
                                } else {
                                    strStatus = "FAILED";
                                }


                                String strMSGID = clogobj[i].getMsgid();
                                if (strMSGID == null || strMSGID.isEmpty() == true) {
                                    strMSGID = "<span class='label label-Default'>" + "Not Available from Sender" + "</span>";
                                }


                                String social=clogobj[i].getSocial();
                                
                                String device = "";
                                if (social != null && social.isEmpty() == false) {
                                    device = social;
                                }

                                

                    %>
                    <tr>
                        <td><%=(i + 1)%></td>
                        <td><%=strMSGID%></td>
                        <td><span class="label label-info"><%=strType%></span></td>
                        <td><%=device%></td> 
                        <td><%=strStatus%></td> 
                        <td><%=clogobj[i].getSentUtctime()%></td>
                        <td><%=clogobj[i].getLastupdateUtctime()%></td>  
                    </tr>
                    <%}
                            } else {%>
                    <td><%=1%></td>
                    <td>No Records Found</td>
                    <td>No Records Found</td>
                    <td>No Records Found</td>
                    <td>No Records Found</td> 
                    <td>No Records Found</td>
                    <td>No Records Found</td>
                    <td>No Records Found</td>
                    <%}%>

                </table>

                <div class="span6">
                    <div class="control-group">                        
                        <div class="span3">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="socialReportCSV()" >
                                    <i class="icon-white icon-chevron-down"></i>Download CSV</a>
                            </div>
                        </div>
                        <div class="span3">
                            <div class="control-group form-inline">
                                <a href="#" class="btn btn-info" onclick="socialReportPDF()" >
                                    <i class="icon-white icon-chevron-down"></i>Download PDF</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br><br>