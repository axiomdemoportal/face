<%@page import="com.mollatech.dictum.management.ContactManagement"%>
<%@page import="com.mollatech.axiom.nucleus.settings.ContactTagsSetting"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.dictum.management.SurveyManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Interactions"%>
<%@include file="header.jsp" %>
<%
    Channels _apSChannelDetails = (Channels) session.getAttribute("_apSChannelDetails");
    String _irid = (String) request.getParameter("_irid");
    SurveyManagement surmngObj = new SurveyManagement();
    Interactions ir = surmngObj.getInteraction(Integer.valueOf(_irid).intValue(), _apSChannelDetails.getChannelid());
    
    
    

%>
<script src="./assets/js/interactions.js"></script>
<div class="container-fluid">
    <h2>Execute Interaction "<%=ir.getInteractionName()%>" Now </h2>
    <p>Please select below parameters to execute </p>
    <hr>
    <div class="row-fluid">
        <form class="form-horizontal" id="RunInteractionForm" name="RunInteractionForm">
            <input type="hidden" value="<%=_irid%>" id="_irid" name="_irid">
            <fieldset>
                <%
                    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
                    Channels channelOnj = (Channels) session.getAttribute("_apSChannelDetails");
                    SettingsManagement smng = new SettingsManagement();
                    ContactTagsSetting tagssettingObj = (ContactTagsSetting) smng.getSetting(sessionId, channelOnj.getChannelid(), SettingsManagement.CONTACT_TAGS, SettingsManagement.PREFERENCE_ONE);
                    String[] tags = tagssettingObj.getTags();
                    
                    ContactManagement cmObj = new ContactManagement();
                    
                %>

                <div class="control-group">
                    <label class="control-label"  for="toNumber">Select Tag</label>
                    <div class="controls">
                        <select class="selectpicker" name="_tagID" id="_tagID">
                            <option value=".." >..</option>                                             

                            <% for (int j = 0; j < tags.length; j++) {
                            int iCount = cmObj.getContactCountBytag(sessionId,channelOnj.getChannelid(),tags[j]);    
                            
                            if (iCount == 0 ) {
                              %>
                              <option value="<%=tags[j]%>" disabled><%=tags[j]%> (<%=iCount%> contacts)</option>                                             
                              <%                                
                            } 
                            else { 
                                %>
                              <option value="<%=tags[j]%>"><%=tags[j]%> (<%=iCount%> contacts)</option>                                             
                              <%
                            
                            }

                            
                            }
                            %>
                        </select>
                         <i> Tagged contacts will be selected and sent interaction request. </i>

                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"  for="speed">Send As </label>
                    <div class="controls">
                        <select name="_type" id="_type">
                            <option value=".." >..</option>                                             
                            <option value="1" >SMS Message</option>
                            <option value="2" >USSD Message</option>
                            <option value="3" >Voice Message</option>  
                            <option value="4" >Web Form</option>  
                        </select>
                        with speed       
                        <select name="_speed" id="_speed">
                            <option value=".." >..</option>                                             
                            <option value="1" >Slow</option>
                            <option value="3" >Normal</option>
                            <option value="10" >Fast</option>                            
                            <option value="30" >Hyper Fast</option>                            
                        </select>
                    </div>
                </div>
                        
                <div class="control-group">
                    <label class="control-label"  for="toNumber">Run for </label>
                    <div class="controls">
                        <select class="selectpicker" name="_expiry" id="_expiry">
                            <option value=".." >..</option>
                            <option value="1" >1 hour</option>                                             
                            <option value="6" >6 hours</option>                                                                         
                            <option value="12" >12 hours</option>                                             
                            <option value="24" >1 day</option>                                             
                            <option value="72" >3 days</option>                                             
                            <option value="168" >1 week</option>                                                                                                     
                        </select>

                    </div>
                </div>                               
                
                <!-- Submit -->
                <div class="control-group">
                    <div class="controls">
                        <button class="btn btn-primary btn-large" onclick="runInteraction()" type="button" id="runInteractionButton">Execute Now >></button>
                        <div id="runInteractionButton-result"></div>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>


    <script language="javascript" type="text/javascript">
        //ChangeSpeed(1);
        //  ChangeFormat(1);
    </script>

    <%@include file="footer.jsp" %>


