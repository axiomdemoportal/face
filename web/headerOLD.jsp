<%@page import="com.mollatech.axiom.nucleus.crypto.PropsFileUtil"%>
<%@page import="java.util.Properties"%>
<%@page import="com.mollatech.ecopin.management.EPINManagement"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.LoadSettings"%>
<%@ page import="com.mollatech.axiom.nucleus.db.Operators" %>
<%@ page import="com.mollatech.axiom.nucleus.db.Channels" %>
<%@ page import="com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement" %>
<%@ page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement" %>
<%@ page import="com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement" %>
<%@ page import="com.mollatech.axiom.nucleus.db.connector.management.RemoteAccessManagement" %>
<%@ page import="com.mollatech.axiom.nucleus.db.operation.AxiomChannel" %>
<%@ page import="com.mollatech.axiom.nucleus.db.operation.AxiomOperator" %>
<%@ page import="com.mollatech.axiom.nucleus.db.Accesses" %>
<%@ page import="com.mollatech.axiom.nucleus.db.Roles" %>
<%@ page import="org.hibernate.Session" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date"%>
<%
    String strContextPath = this.getServletContext().getContextPath();
    String status = (String) session.getAttribute("_apOprAuth");
    if (status == null || status.compareTo("yes") != 0) {
        String nextJSP = "./index.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
        dispatcher.forward(request, response);
        return;
    }

    Operators oprObj = (Operators) session.getAttribute("_apOprDetail");
    String name = oprObj.getName();

    OperatorsManagement oprmngObj = new OperatorsManagement();
    Roles[] roles = oprmngObj.getAllRoles(oprObj.getChannelid());

SimpleDateFormat tzTimeHeader = new SimpleDateFormat("EEE dd/MMM/yy-HH:mm");
    String completeTimeHeader = tzTimeHeader.format(new Date());

    String strProductType = LoadSettings.g_sSettings.getProperty("product.type");

    int iPRODUCT = (new Integer(strProductType)).intValue();
    int DICTUM = 1;
    int ECOPIN = 2;
    int AXIOM = 3;
    
    //System.out.println("THE PRODUCT TYPE IS::" + iPRODUCT);
    
    //iPRODUCT = ECOPIN = AXIOM;

    //iPRODUCT = DICTUM;

    //int iPRODUCT = AXIOM;//dictum

    //iPRODUCT = ECOPIN;
    
    //iPRODUCT = AXIOM;

    String strOperatorRole = (String) session.getAttribute("_apOprRole");
    int iOperatorRole = Integer.valueOf(strOperatorRole).intValue();

    //sysadmin
    if (iOperatorRole >= 4) {
    }
    //admin
    if (iOperatorRole >= 3) {
    }
    //sysadmin
    if (iOperatorRole >= 2) {
    }
    //sysadmin
    if (iOperatorRole >= 1) {
    }

    
    
    EPINManagement epinmngObj = new EPINManagement();
    String sessionid = (String)session.getAttribute("_apSessionID");
    int iFailedRequests = epinmngObj.GetCountOfEpintracker(sessionid, oprObj.getChannelid(), -3); //failed
    int iPendingRequests = epinmngObj.GetCountOfEpintracker(sessionid, oprObj.getChannelid(), 2); //pending
    
    String FailedRequests="";
    String PendingRequests="";
    
    if (iFailedRequests != 0){
        FailedRequests = "<span class=\"label label-warning\">" + iFailedRequests +"</span>";
    }
    
    if (iPendingRequests != 0){
        PendingRequests = "<span class=\"label label-warning\">" + iPendingRequests +"</span>";        
    }
    
    
    //nilesh  option enable disable 25-3-15
    String sep = System.getProperty("file.separator");
    String usrhome = System.getProperty("catalina.home");
    if (usrhome == null) {
        usrhome = System.getenv("catalina.home");
    }
    Properties navigationSettings = null;
    if (usrhome != null) {
        usrhome += sep + "axiomv2-settings" + sep + "EnableOptions.conf";
        PropsFileUtil p1 = new PropsFileUtil();
        if (p1.LoadFile(usrhome) == true) {
            navigationSettings = p1.properties;
        }
    }
    //nilesh
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="google-translate-customization" content="26e4d928eae0659b-61f8bcc48ec8af9a-g74ee54aac640948a-11"></meta>
        <title><%=strContextPath%> Management Portal For <%=name%></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="./assets/css/bootstrap.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 60px;
                padding-bottom: 40px;
            }
        </style>
        <link href="./assets/css/bootstrap-responsive.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="../assets/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="./assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="./assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="./assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="./assets/ico/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="./assets/ico/favicon.png">
        <link rel="stylesheet" href="./assets/css/datepicker.css">
        <link rel="stylesheet" href="./assets/css/jquery.sidr.dark.css">

        <script src="./assets/js/jquery.js"></script>
        <script src="./assets/js/bootstrap-transition.js"></script>
        <script src="./assets/js/bootstrap-alert.js"></script>
        <script src="./assets/js/bootstrap-modal.js"></script>
        <script src="./assets/js/bootstrap-dropdown.js"></script>
        <script src="./assets/js/bootstrap-scrollspy.js"></script>
        <script src="./assets/js/bootstrap-tab.js"></script>
        <script src="./assets/js/bootstrap-tooltip.js"></script>
        <script src="./assets/js/bootstrap-popover.js"></script>
        <script src="./assets/js/bootstrap-button.js"></script>
        <script src="./assets/js/bootstrap-collapse.js"></script>
        <script src="./assets/js/bootstrap-carousel.js"></script>
        <script src="./assets/js/bootstrap-typeahead.js"></script>
        <script src="./assets/js/bootstrap-datepicker.js"></script>


        <script src="./assets/js/json_sans_eval.js"></script>
        <script src="./assets/js/bootbox.min.js"></script>
        <script src="./assets/js/channels.js"></script>
        <script src="./assets/js/operators.js"></script>

        <script src="./assets/js/jquery.sidr.min.js"></script>


    </head>

    <body>

        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="brand" href="home.jsp"><%=strContextPath%></a>
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <% if (iOperatorRole >= 4) {%> 
                            <li><a href="channels.jsp">Channels</a></li>
                            <% }%>
                            <% if (iOperatorRole >= 3) {%> 
                            <li><a href="operators.jsp">Operators</a></li>
                            <% }%>

                            <% if (iOperatorRole >= 2) {%> 

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Templates<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <% if (iPRODUCT != AXIOM) {%> 
                                    <li><a href="TemplateNew.jsp"><i class="icon-file"></i>Add New Template</a></li>                                    
                                    <li class="divider"></li>
                                    <% }%>
                                    <li><a href="TemplateMobileList.jsp"><i class="icon-file"></i>Mobile Messages</a></li>                                    
                                    <li><a href="TemplateEmailList.jsp"><i class="icon-file"></i>Email Messages</a></li>
                                    <% if (iOperatorRole >= 3) {%> 
                                    <!--<li class="divider"></li>-->
                                    <!--<li><a href="contentfilter.jsp"><i class="icon-user"></i>Content Filters</a></li>-->                                    
                                    <% }%>
                                </ul>
                            </li>
                            <% }%>

                            <% if (iOperatorRole >= 2) {%> 
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Configuration<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="smsgateway.jsp"><i class="icon-cog"></i>SMS Gateways</a></li>
                                    <li><a href="ussdgateway.jsp"><i class="icon-cog"></i>USSD Gateways</a></li>
                                    <li><a href="voicegateway.jsp"><i class="icon-cog"></i>Voice Gateways</a></li>
                                    <li><a href="emailgateway.jsp"><i class="icon-cog"></i>Email Servers</a></li>
                                     
                                    <% if (iPRODUCT == DICTUM) { %> 
                                    <li><a href="faxgateway.jsp"><i class="icon-cog"></i>E-Fax Gateways</a></li>
                                    <li><a href="pushgateway.jsp"><i class="icon-cog"></i>Push Notification Gateways</a></li>                                    
                                    <%}%>
                                    <%if (iPRODUCT == AXIOM) {%>
                                    <li class="divider"></li>
                                    <li><a href="usersrcsetting.jsp"><i class="icon-user"></i>User Source Repository</a></li>
                                     <li><a href="PasswordPolicySetting.jsp"><i class="icon-user"></i>Password Policy Settings</a></li>    
                                    <li><a href="channelProfile.jsp"><i class="icon-user"></i>Channel Profile Settings</a></li>                                    
                                    <li class="divider"></li>
                                    <li><a href="tokensettings.jsp"><i class="icon-user"></i>OTP Token Settings</a></li>                                    
                                    <li><a href="caconnector.jsp"><i class="icon-lock"></i>Certificate Connector</a></li>
                                    <li><a href="mobiletrustsettings.jsp"><i class="icon-user"></i>Mobile Trust Settings</a></li>
                                    <!--<li><a href="pdfsettings.jsp"><i class="icon-user"></i>PDF Signing Settings</a></li>-->                                                                                                            
                                    <!--<li><a href="radiusserver.jsp"><i class="icon-hdd"></i>RADIUS Connector</a></li>-->
                                    <li class="divider"></li>
                                    <li><a href="globalsettingv2.jsp"><i class="icon-user"></i>Global Settings</a></li>                                                                        
                                    <li class="divider"></li>
                                    <li><a href="epinsetting.jsp"><i class="icon-hdd"></i>E-PIN Settings</a></li>                                    
                                    <% }%>                                    
                                    <%if (iPRODUCT == DICTUM) {%>
                                    <li class="divider"></li>
                                    <li><a href="globalsettingv2.jsp"><i class="icon-user"></i>Global Settings</a></li>                                    
                                    <li><a href="socialgatewaysettings.jsp"><i class="icon-user"></i>Go Social Settings</a></li>                                    
                                    <% }%>
                                    <% if (iOperatorRole >= 3) {%> 
                                    <!--<li class="divider"></li>-->
                                    <!--<li><a href="ipfilter.jsp"><i class="icon-lock"></i>Caller IP list</a></li>-->
                                    <!--<li><a href="pluginsetting.jsp"><i class="icon-file"></i>Connector Plug-ins</a></li>-->
                                    <% }%>

                                </ul>
                            </li>
                            <% }%>

                            <%if (iPRODUCT == AXIOM) {%>
                            
                            <% if (iOperatorRole >= 2) {%> 
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Security & User Management<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="users.jsp"><i class="icon-user"></i>User & Password</a></li>
                                    <li><a href="otptokens.jsp"><i class="icon-user"></i>One Time Password Tokens</a></li>
                                    <li><a href="pkitokens.jsp"><i class="icon-user"></i>Digital Certificates And Tokens</a></li> 
                                    <li><a href="ChallengeResponse.jsp"><i class="icon-user"></i>Challenge Response (Q&A)</a></li> 
                                    <li><a href="trusteddevice.jsp"><i class="icon-user"></i>Trusted Device (via Mobile)</a></li>
                                    <li><a href="geotrackingmain.jsp"><i class="icon-user"></i>Geo-location (Fence & Track)</a></li>
                                </ul>
                            </li>
                            <% }%>
                            

                            <% if (iOperatorRole >= 2) {%> 
                            <!--<li><a href="users.jsp">Users</a></li>
                            <li><a href="otptokens.jsp">OTP Tokens</a></li>
                            <li><a href="pkitokens.jsp">Certificates</a></li>                            
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Profile<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="trusteddevice.jsp">Trust Device</a></li>
                                    <li class="divider"></li>
                                    <li><a href="geotrackingmain.jsp">Geo (Fence & Track)</a></li>
                                </ul>
                            </li>
                            -->
                            <% }%>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="usersreport.jsp"><i class="icon-file"></i>User Report</a></li>
                                    <!--<li><a href="msgreportmain.jsp"><i class="icon-file"></i>Message Report</a></li>-->                                    
                                    <li class="dropdown-submenu">
                                        <a tabindex="-1" href="#"><i class="icon-file"></i>Message Reports</a>
                                        <ul class="dropdown-menu">
                                            <li><a tabindex="-1" href="msgreportmain.jsp" >Message Report</a></li>
                                            <li><a tabindex="-1" href="msgcostreport.jsp" >Summary Report</a></li>
                                        </ul>
                                    </li>
                                    <li class="divider"></li>
                                    <li><a href="otptokensreport.jsp"><i class="icon-file"></i>OTP Token Report</a></li>   
                                    <li><a href="certificatereport.jsp"><i class="icon-file"></i>Certificate Report</a></li>
                                    <li><a href="pkitokenreport.jsp"><i class="icon-file"></i>PKI Token Report</a></li>                                                                                                                                                
                                    <li class="dropdown-submenu">
                                        <a tabindex="-1" href="#"><i class="icon-file"></i>E-PIN Reports</a>
                                        <ul class="dropdown-menu">
                                            <li><a tabindex="-1" href="EcponSystemReportMain.jsp" >System Wide</a></li>
                                            <li><a tabindex="-1" href="ecopinuserreport.jsp" >User Specific</a></li>
                                        </ul>
                                    </li>
                                    <li class="divider"></li>
                                    <li><a href="RSSDetailsMain.jsp"><i class="icon-file"></i>Remote Signing Report</a></li>
                                    
                                </ul>
                            </li>
                            <% }%>

                            <%if (iPRODUCT == ECOPIN) {%>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Requests<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="ecopinpending.jsp">Pending Approvals <%=PendingRequests%></a></li>                                    
                                    <li><a href="ecopinfailed.jsp">Failed Deliveries <%=FailedRequests%></a></li>
                                </ul>
                            </li>
                            
                            <% }%>

                            <%if (iPRODUCT == DICTUM) {%>
                            <li><a href="contacts.jsp">Contacts</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Send On<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="bulkmsgv2.jsp">Mobile (SMS/USSD/Voice)</a></li>
                                    <li><a href="bulkemailv2.jsp">Email (Plain/Rich Text)</a></li>
                                    <li class="dropdown-submenu">
                                        <a tabindex="-1" href="#">E-FAX (Single/Group)</a>
                                        <ul class="dropdown-menu">
                                            <li><a tabindex="-1" href="sendfaxv2.jsp" >Single FAX</a></li>
                                            <li><a tabindex="-1" href="sendfaxv2withcontact.jsp" >Group FAX</a></li>
                                        </ul>
                                    </li>                                    
                                    <li class="dropdown-submenu">
                                        <a tabindex="-1" href="#">Social Media</a>
                                        <ul class="dropdown-menu">
                                            <li><a tabindex="-1" href="bulkfacebook.jsp" >Facebook Post</a></li>
                                            <li><a tabindex="-1" href="bulktwitter.jsp" >Twitter Post</a></li>
                                            <li><a tabindex="-1" href="bulklinkedin.jsp" >Linkedin Post</a></li>
                                        </ul>
                                    </li>   
                                    <li><a href="blukpushnotification.jsp">Push Notification</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Interactions<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="InteractionsNew.jsp">Create New Interaction</a></li>                                    
                                    <li class="divider"></li>
                                    <li><a href="InteractionsList.jsp">List Interactions</a></li>
                                    <li><a href="InteractionsReports.jsp">Archived Reports</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">FX Push<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="pushmessages.jsp">List Callers</a></li>
                                    <li class="divider"></li>
                                    <li><a href="InteractionReports.jsp">Archived Reports</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="msgreportmain.jsp"><i class="icon-file"></i>Message Report</a></li>
                                    <li class="divider"></li>                                    
                                    <li><a href="msgcostreport.jsp"><i class="icon-file"></i>Summary Report</a></li>
                                </ul>
                            </li>                            
                            <% }%>
                        </ul>
         <%if (navigationSettings == null) {%>
                        <div class="pull-right">
                            <ul class="nav pull-right">
                                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><%=completeTimeHeader%> Welcome, <%=name%><b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <% if (iOperatorRole >= 4) {%> 
                                        
                                        <li><a href="showlicenseI.jsp"><i class="icon-user"></i>License Details</a></li>
                                       <li><a href="accessMatrix.jsp"><i class="icon-cog"></i>Access Matrix</a></li>
                                      
                                        <%if (iPRODUCT == AXIOM) {%>
                                        <li><a href="hwotpupload.jsp"><i class="icon-user"></i>Hardware Token Uploader</a></li>                                        
                                        <li><a href="QuestionsList.jsp"><i class="icon-file"></i>Challenge Response Questions List</a></li>
                                        <li><a href="signpdf.jsp"><i class="icon-user"></i>Sign PDF Document</a></li>
                                        <%}%>
                                        <li><a href="reinitialize.jsp"><i class="icon-user"></i>Change Security Credentials</a></li>
                                        <% }%>
                                        <% if (iOperatorRole >= 3) {%>                                                                                
                                        <li><a href="channelaudit.jsp"><i class="icon-user"></i>Download Audit Trail</a></li>
                                        <li><a href="sessionaudit.jsp"><i class="icon-file"></i>Session Audit Trail</a></li>                                        
                                        <li><a href="#" onclick="DownloadZippedLog()"><i class="icon-file"></i>Download Logs</a></li>
                                        <li><a href="#" onclick="DownloadCleanupLog()"><i class="icon-file"></i>Download Clean-up Logs</a></li>
                                        <% }%>                                        
                                        <li><a href="connectorstatuslist.jsp" ><i class="icon-cog"></i>Connectors Status</a></li>
                                        <li><a href="#ChangePassword" data-toggle="modal" ><i class="icon-cog"></i>Change Password</a></li>
 					<li><a href="initdbform.jsp" ><i class="icon-cog"></i>Change Database Password</a></li>
                                        <li class="divider"></li>
                                        <li><a href="signout.jsp"><i class="icon-off"></i>Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                       <%}else{
             
             
                     String strrole= null;
                        if(oprObj.getRoleid() == 1){//sysadmin
                            strrole = "sysadmin";
                        }else if(oprObj.getRoleid() == 2){//admin
                            strrole = "admin";
                        }else if(oprObj.getRoleid() == 3){//helpdesk
                            strrole = "helpdesk";
                        }else if(oprObj.getRoleid() == 4){//reporter
                            strrole = "reporter";
                        }%>
       
                       
                        <div class="pull-right">
                            <ul class="nav pull-right">
                                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><%=completeTimeHeader%> Welcome, <%=name%><b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <% if (iOperatorRole >= 4) {%> 
                                         <%if (navigationSettings.getProperty(strrole+".axiom.license.details").equals("true")) {%>
                                        <li><a href="showlicenseI.jsp"><i class="icon-user"></i>License Details</a></li>
                                        <li><a href="accessMatrix.jsp"><i class="icon-cog"></i>Access Matrix</a></li>
                                         <li><a href="addAccessRole.jsp"><i class="icon-cog"></i>Access Role</a></li>
                                        <li><a href="ListSystemMessage.jsp"><i class="icon-cog"></i>System Message</a></li>
                                        <%}%>
                                        <%if (iPRODUCT == AXIOM) {%>
                                            <%if (navigationSettings.getProperty(strrole+".axiom.hwToken.upload").equals("true")) {%>
                                        <li><a href="hwotpupload.jsp"><i class="icon-user"></i>Hardware Token Uploader</a></li>   
                                        <%}%>
                                        <%if (navigationSettings.getProperty(strrole+".axiom.challengeResponseQuestionList").equals("true")) {%>
                                        <li><a href="QuestionsList.jsp"><i class="icon-file"></i>Challenge Response Questions List</a></li>
                                        <%}%>
                                        <%if (navigationSettings.getProperty(strrole+".axiom.signPDF").equals("true")) {%>
                                        <li><a href="signpdf.jsp"><i class="icon-user"></i>Sign PDF Document</a></li>
                                          <%}%>
                                        <%}%>
                                        <%if (navigationSettings.getProperty(strrole+".axiom.changeSecurityCredentials").equals("true")) {%>
                                        <li><a href="reinitialize.jsp"><i class="icon-user"></i>Change Security Credentials</a></li>
                                        <%}%>
                                        <% }%>
                                        <% if (iOperatorRole >= 3) {%>     
                                         <%if (navigationSettings.getProperty(strrole+".axiom.auditTrail").equals("true")) {%>
                                        <li><a href="channelaudit.jsp"><i class="icon-user"></i>Download Audit Trail</a></li>
                                        <%}%>
                                          <%if (navigationSettings.getProperty(strrole+".axiom.sessionAuditTrail").equals("true")) {%>
                                        <li><a href="sessionaudit.jsp"><i class="icon-file"></i>Session Audit Trail</a></li>     
                                         <%}%>
                                          <%if (navigationSettings.getProperty(strrole+".axiom.downloadLogs").equals("true")) {%>
                                        <li><a href="#" onclick="DownloadZippedLog()"><i class="icon-file"></i>Download Logs</a></li>
                                        <%}%>
                                        <%if (navigationSettings.getProperty(strrole+".axiom.downloadCleanUpLogs").equals("true")) {%>
                                        <li><a href="#" onclick="DownloadCleanupLog()"><i class="icon-file"></i>Download Clean-up Logs</a></li>
                                         <% }%>   
                                        <% }%>     
                                         <%if (navigationSettings.getProperty(strrole+".axiom.connectorStatus").equals("true")) {%>
                                        <li><a href="connectorstatuslist.jsp" ><i class="icon-cog"></i>Connectors Status</a></li>
                                        <%}%>
                                          <%if (navigationSettings.getProperty(strrole+".axiom.changePassword").equals("true")) {%>
                                        <li><a href="#ChangePassword" data-toggle="modal" ><i class="icon-cog"></i>Change Password</a></li>
 					<%}%>
                                         <%if (navigationSettings.getProperty(strrole+".axiom.changeDBPassword").equals("true")) {%>
                                        <li><a href="initdbform.jsp" ><i class="icon-cog"></i>Change Database Password</a></li>
                                        <%}%>
                                        <li class="divider"></li>
                                        <li><a href="signout.jsp"><i class="icon-off"></i>Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                       <%}%>
                                        
                    </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>


        <script>
            $(document).ready(function() {
                $('#left-menu').sidr({
                    name: 'sidr-left',
                    side: 'left' // By default
                });

            });
            $(document).ready(function() {
                $('#simple-menu').sidr();
            });
        </script>

