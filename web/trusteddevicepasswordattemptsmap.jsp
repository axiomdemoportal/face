
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.util.Map" %>
<%@ page import="org.json.simple.JSONValue" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<%@page import="com.mollatech.axiom.nucleus.db.Clientdestroy"%>
<%@page import="com.mollatech.axiom.nucleus.db.Geotrack"%>
<%@page import="com.mollatech.axiom.nucleus.db.Geofence"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.GeoLocationManagement"%>
<%@page import="com.mollatech.axiom.connector.user.TokenStatusDetails"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Trusteddevice"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.Otptokens"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.TrustDeviceManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<script src="./assets/js/trustedDevice.js"></script>
<%
    String sessionId = (String) request.getSession().getAttribute("_apSessionID");
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String _channelId = channel.getChannelid();
    String userId = request.getParameter("_userid");
    String _duration = request.getParameter("_duration");
    TrustDeviceManagement tdm = new TrustDeviceManagement();
    UserManagement uManagement = new UserManagement();
    SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm ");

    Clientdestroy[] arrDestroyclint = tdm.getDestroyedClient(sessionId, _channelId, userId, _duration);
    int wrong_attempts = 1;
    int forced_destroy = 2;
    int intentionally_destroy = 3;
    if (arrDestroyclint != null) {

%>

<%        AuthUser uObj = uManagement.getUser(sessionId, _channelId, userId);

        List listHowTos = new LinkedList();
        Map m2 = new HashMap();

        for (int i = 0; i < arrDestroyclint.length; i++) {
//        m2.put("address", arrDestroyclint[i].getLocation());

            String Strtype = "";
            if (arrDestroyclint[i].getType() == intentionally_destroy) {
                Strtype = "Intentionally Destroy";
            } else if (arrDestroyclint[i].getType() == forced_destroy) {
                Strtype = "Forced Destroy";
            } else if (arrDestroyclint[i].getType() == wrong_attempts) {
                Strtype = "Wrong Attempts";
            }

            m2.put("_lat", arrDestroyclint[i].getLatitude());
            m2.put("_lng", arrDestroyclint[i].getLongitude());
            m2.put("_uname", uObj.getUserName());
            m2.put("address", arrDestroyclint[i].getLocation());
            m2.put("_reason", Strtype);
            m2.put("_dtime", sdf.format(arrDestroyclint[i].getDestroyTime()));
            listHowTos.add(m2);
            m2 = new HashMap();

        }
        out.clear();

        response.setContentType("application/json");
        String jsonString = JSONValue.toJSONString(listHowTos);
        out.print(jsonString);
        out.flush();

    }
%>

