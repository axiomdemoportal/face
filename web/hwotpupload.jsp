<%@page import="com.mollatech.axiom.nucleus.db.Units"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet"%>
<%@include file="header.jsp" %>
<script src="./assets/js/ajaxfileupload.js"></script>
<script src="./assets/js/bootstrap-fileupload.js"></script> 
<link href="./assets/css/bootstrap.css" rel="stylesheet">
<link href="./assets/css/bootstrap-fileupload.css" rel="stylesheet">
<script src="./assets/js/otptokens.js"></script>
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<script src="./assets/js/globalsetting.js"></script>
<%
    Channels channel = (Channels) session.getAttribute("_apSChannelDetails");
    String sessionId = (String) session.getAttribute("_apSessionID");
    UnitsManagemet unitObj = new UnitsManagemet();
    Units[] un = unitObj.ListUnitss(sessionId, channel.getChannelid());
%>
<div class="container-fluid">
    <h1 class="text-success">Hardware Token File Uploader</h1>

    <div class="tabbable">
        <ul class="nav nav-tabs">

            <%if (oprObj.getRoleid() != 1 && accessObj != null) {//not sysadmin
                    if (accessObj != null) {%>
            <% if (accessObj.listhwOTPTokenUpload == true) {%>
            <li class="active"><a href="#primary" data-toggle="tab">One Time Password Token</a></li>
                <%}
                    if (accessObj.listhwPKITokenUpload == true) {%>
            <li ><a href="#secondary" data-toggle="tab">PKI Token</a></li>
                <%}
                } else {%>
            <li class="active"><a href="#primary" data-toggle="tab">One Time Password Token</a></li>
            <li><a href="#secondary" data-toggle="tab">PKI Token</a></li>
                <%}
                } else {%>
            <li class="active"><a href="#primary" data-toggle="tab">One Time Password Token</a></li>
            <li><a href="#secondary" data-toggle="tab">PKI Token</a></li>
                <%}%>

        </ul>
        <div class="tab-content">

            <% if (accessObj.listhwOTPTokenUpload == true || oprObj.getRoleid() == 1) {%>
            <div class="tab-pane active" id="primary">

                <div class="container-fluid">

                    <h3 class="text-success">Hardware OTP Token Uploader</h3>
                    <!--<h1 class="text-success">Hardware OTP Token Uploader</h1>-->
                    <h4>1: Please select PSKC file (XML file) having hardware tokens.</h4>    
                    <h4>2: Please select Password file to access the token secrets.</h4>                            
                    <hr>

                    <div class="controls-row" id="licenses">
                        <div class="row-fluid">
                            <form class="form-horizontal" id="uploadXMLFormEAD" name="uploadXMLFormEAD">
                                <fieldset>

                                    <div class="control-group">
                                        <label class="control-label"  for="username"> Unit Name </label>
                                        <div class="controls">  
                                            <select class="selectpicker" name="_UnitId" id="_UnitId"  class="span3">

                                                <%
                                                    if (un != null) {
                                                        for (int j = 0; j < un.length; j++) {

                                                %>
                                                <option  value="<%=un[j].getUnitid()%>"><%=un[j].getUnitname()%></option>       

                                                <%}
                                                } else {%>
                                                <option  value="0">No Units Found</option>   
                                                <%}%>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="control-group">
                                        <label class="control-label"  for="username">Select PSKC XML File:</label>                                    
                                        <div class="controls fileupload fileupload-new" data-provides="fileupload">
                                            <div class="input-append">
                                                <div class="uneditable-input span4"><i class="icon-file fileupload-exists"></i> 
                                                    <span class="fileupload-preview"></span>
                                                </div>
                                                <span class="btn btn-file" >
                                                    <span class="fileupload-new">Select</span>
                                                    <span class="fileupload-exists">Change</span>
                                                    <input type="file" id="fileXMLToUploadEAD" name="fileXMLToUploadEAD"/>
                                                </span>
                                                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                <button class="btn btn-success" id="buttonUploadXMLEAD"  onclick="UploadXMLFile()" >Step 1: Click to upload</button>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="control-group">
                                        <label class="control-label"  for="username">Select Password File:</label>                                    
                                        <div class="controls fileupload fileupload-new" data-provides="fileupload">
                                            <div class="input-append">
                                                <div class="uneditable-input span4"><i class="icon-file fileupload-exists"></i> 
                                                    <span class="fileupload-preview"></span>
                                                </div>
                                                <span class="btn btn-file" >
                                                    <span class="fileupload-new">Select</span>
                                                    <span class="fileupload-exists">Change</span>
                                                    <input type="file" id="fileXMLToUploadEADPASSWORD" name="fileXMLToUploadEADPASSWORD"/>
                                                </span>
                                                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                <button class="btn btn-success" id="buttonUploadPASSEAD"  onclick="UploadPasswordFile()" >Step 2: Click to upload</button>
                                            </div>
                                            OR
                                            <input type="text" id="_password" name="_password" placeholder="Enter password">
                                    
                                        </div>
                                            
                                    </div>


                                    <div class="control-group">
                                        <div class="controls">


                                            <button id="save-otp-tokens" class="btn btn-success" onclick="HardwareOTPTokenUpload()" type="button">Step 3: Import Token Secrets </button>
                                            <!--                              <button class="btn btn-mini" id="download-SuccessEntries" type="button" style="visibility:hidden" onclick="SuccessEntriesDownload()">Download Success Entries>></button>
                                                                        <button class="btn btn-mini" id="download-FailEntries" type="button" style="visibility: hidden" onclick="FailedEntriesDownload()" >Download Failed Entries>></button>-->
                                        </div>
                                    </div>


                                    <div class="control-group">
                                        <div class="controls">
                                            <div id="result-div" style="visibility:hidden">
                                                <h4>  <p> <font color="blue">Download Result</font>
                                                        <button class="btn btn-success" id="download-SuccessEntriesS" type="button"  onclick="SuccessEntriesDownload()">For Success Entries</button>
                                                        <button class="btn btn-danger" id="download-FailEntriesS" type="button"  onclick="FailedEntriesDownload()" >For Failed Entries</button>
                                                    </p>
                                                </h4>
                                            </div> 
                                        </div>
                                    </div>

                                </fieldset>
                            </form>
                            <!--                            <button class="btn" id="download-SuccessEntries" type="button" style="visibility:hidden" onclick="SuccessEntriesDownload()">Download Success Entries>></button>
                                                        <button class="btn" id="download-FailEntries" type="button" style="visibility: hidden" onclick="FailedEntriesDownload()" >Download Failed Entries>></button>-->
                        </div>
                        <!-- Submit -->
                    </div>

                </div>

            </div>

            <%
                }
                if (accessObj.listhwPKITokenUpload == true || oprObj.getRoleid() == 1) {
            %>

            <div class="tab-pane" id="secondary">
                <div class="container-fluid">
                    <h3 class="text-success">Hardware PKI Token Uploader</h3>
                    <!--<h1 class="text-success">Hardware PKI Token Uploader</h1>-->
                    <h4>1: Please select file  with list of hardware PKI token serial numbers.</h4>    
                    <hr>
                    <div class="controls-row" id="licenses">
                        <div class="row-fluid">
                            <form class="form-horizontal" id="uploadPKITOKENFormEAD" name="uploadPKITOKENFormEAD">
                                <fieldset>
                                    <% if (1 == 2) { %>
                                    <div class="control-group">
                                        <label class="control-label"  for="username"> Unit Name </label>
                                        <div class="controls">  
                                            <select class="selectpicker" name="_UnitIdPKI" id="_UnitIdPKI"  class="span3">

                                                <% //   Channels channel = (Channels) session.getAttribute("_apSChannelDetails");
//                                                    String sessionId = (String) session.getAttribute("_apSessionID");
//                                                    UnitsManagemet unitObj = new UnitsManagemet();
//                                                    Units[] un = unitObj.ListUnitss(sessionId, channel.getChannelid());
                                                    if (un != null) {
                                                        for (int j = 0; j < un.length; j++) {

                                                %>
                                                <option  value="<%=un[j].getUnitid()%>"><%=un[j].getUnitname()%></option>       

                                                <%}
                                                } else {%>
                                                <option  value="0">No Units Found</option>   
                                                <%}%>
                                            </select>
                                        </div>
                                    </div>
                                    <% } %>
                                    
                                           <div class="control-group">
                                        <label class="control-label"  for="username"> Unit Name </label>
                                        <div class="controls">  
                                            <select class="selectpicker" name="_UnitIdPKI" id="_UnitIdPKI"  class="span3">

                                                <% //   Channels channel = (Channels) session.getAttribute("_apSChannelDetails");
//                                                    String sessionId = (String) session.getAttribute("_apSessionID");
//                                                    UnitsManagemet unitObj = new UnitsManagemet();
//                                                    Units[] un = unitObj.ListUnitss(sessionId, channel.getChannelid());
                                                    if (un != null) {
                                                        for (int j = 0; j < un.length; j++) {

                                                %>
                                                <option  value="<%=un[j].getUnitid()%>"><%=un[j].getUnitname()%></option>       

                                                <%}
                                                } else {%>
                                                <option  value="0">No Units Found</option>   
                                                <%}%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        
                                        <label class="control-label"  for="username">Select Token File:</label>                                    
                                        <div class="controls fileupload fileupload-new" data-provides="fileupload">
                                            <div class="input-append">
                                                <div class="uneditable-input span4"><i class="icon-file fileupload-exists"></i> 
                                                    <span class="fileupload-preview"></span>
                                                </div>
                                                <span class="btn btn-file" >
                                                    <span class="fileupload-new">Step 1: Select</span>
                                                    <span class="fileupload-exists">Change</span>
                                                    <input type="file" id="PKITOKENfileUploadEAD" name="PKITOKENfileUploadEAD"/>
                                                </span>
                                                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="control-group">
                                        <div class="controls">

                                            <button class="btn btn-success" id="buttonUploadPKITOKENEAD"  onclick="HardwarePKITokenUpload()" >Step 2: Upload And Import</button>
                                        </div>
                                    </div>


                                </fieldset>
                            </form>

                        </div>
                        <!-- Submit -->
                    </div>

                </div>

            </div>


            <% }%>



        </div>
    </div>



</div>


<script>
    InitialState();
</script>

<%@include file="footer.jsp" %>