<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Users"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<html>
    <div class="control-group">
        <label class="control-label"  for="username"></label>
        <div class="controls">
            <%
                Channels channels = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                String sessionId = (String) request.getSession().getAttribute("_apSessionID");
                String groupId = request.getParameter("_groupId");
                int gId = -1;
                if (groupId != null) {
                    gId = Integer.parseInt(groupId);
                }
            %>
            <select id="_UsersSelect" name="_UsersSelect">
                <option value="-1" selected disabled> Select User</option>
                <%
                    UserManagement um = new UserManagement();
                    AuthUser[] usr = um.getAllUserByGroup(sessionId, channels.getChannelid(), gId);
                    for (int i = 0; i < usr.length; i++) {
                %>
                <option value="<%=usr[i].getUserId()%>"> <%= usr[i].getUserName()%> </option>
                <%}%>
            </select> 
        </div>
    </div>


