<%@include file="header.jsp" %>
<script src="./assets/js/ajaxfileupload.js"></script>
<script src="./assets/js/bootstrap-fileupload.js"></script> 
<link href="./assets/css/bootstrap.css" rel="stylesheet">
<link href="./assets/css/bootstrap-fileupload.css" rel="stylesheet">
<script src="./assets/js/webseal.js"></script>
<div class="container-fluid">
    <h1 class="text-success">Web Seal Settings</h1>
    <div class="row-fluid">
        <form class="form-horizontal" id="_webSealInfoForm" name="_webSealInfoForm">
            <div class="control-group">
                <label class="control-label"  for="username">External Web Seal URL</label>
                <div class="controls">
                    <input type="text" id="_webSealURL" name="_webSealURL" placeholder="Enter Url like https://localhost:443/face/GetWebSeal" class="input-xxlarge">                                    
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"  for="username">Success Image</label>
                <div class="controls fileupload fileupload-new" data-provides="fileupload">
                    <div class="input-append">
                        <div class="uneditable-input span4"><i class="icon-file fileupload-exists"></i> 
                            <span class="fileupload-preview"></span>
                        </div>
                        <span class="btn btn-file" >
                            <span class="fileupload-new">Select file</span>
                            <span class="fileupload-exists">Change</span>
                            <input type="file" id="uploadForWebseal1" name="uploadForWebseal1"/>
                        </span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>

                    </div>
                    <a href="#" class="btn btn-success" id="buttonUploadEAD"  onclick="imageFileUploadForWebseal(1)" >Upload Now>></a>
                    <font id="_successImage"></font>
                </div>

            </div>
            <div class="control-group">
                <label class="control-label"  for="username">Error Image</label>
                <div class="controls fileupload fileupload-new" data-provides="fileupload">
                    <div class="input-append">
                        <div class="uneditable-input span4"><i class="icon-file fileupload-exists"></i> 
                            <span class="fileupload-preview"></span>
                        </div>
                        <span class="btn btn-file" >
                            <span class="fileupload-new">Select file</span>
                            <span class="fileupload-exists">Change</span>
                            <input type="file" id="uploadForWebseal2" name="uloadForWebseal2"/>
                        </span>
                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                    </div>
                    <a href="#" class="btn btn-success" id="buttonUploadEAD"  onclick="imageFileUploadForWebseal(2)" >Upload Now>></a>
                    <font id="_errorImage"></font>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"  for="username">Show Hacker</label>
                <div class="controls">
                    <select id="_hackerResult" name="_hackerResult">
                        <option selected value="Error Image">Error  Image</option>
                        <option value="Success Image">Success Image</option>
                    </select>
                </div>
            </div>
            <hr>
            <h3>Use External Domain Name Server (DNS)</h3>
            <div class="control-group">
                <label class="control-label"  for="username">Name Server 1</label>
                <div class="controls">
                    <input type="text" id="_dnsForWeb1" name="_dnsForWeb1" placeholder="Enter DNS Name" class="input-large">                                    
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"  for="username">Name Server 2</label>
                <div class="controls">
                    <input type="text" id="_dnsForWeb2" name="_dnsForWeb2" placeholder="Enter DNS Name" class="input-large">                                    
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"  for="username">Name Server 3</label>
                <div class="controls">
                    <input type="text" id="_dnsForWeb3" name="_dnsForWeb3" placeholder="Enter DNS Name" class="input-large">                                    
                </div>
            </div>
            <div class="control-group">
                <div id="save-mobile-settings-result"></div>
                <button class="btn btn-primary" onclick="addWebSealSetting()" type="button">Save Setting Now >> </button>
            </div>
        </form>
    </div>
</div>
<script>
    loadWebSealSetting();
</script>
<%@include file="footer.jsp" %>
