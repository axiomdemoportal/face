<%@include file="header.jsp" %>
<div class="container-fluid">
    <h2>Certificate Authority(CA) Connector Configuration</h2>
    <p>To facilitate certificate issuance from external Certificate Authority, following   </p>
    <br>
    <div class="row-fluid">
        <form class="form-horizontal">
            <fieldset>

                <div id="legend">
                    <legend class="">Certificate Request Attributes </legend>
                </div>

                <div class="control-group">
                    <label class="control-label"  for="username">Ownership Details</label>
                    <div class="controls">
                        <input type="text" id="_p1host" name="_p1host" placeholder="Organization Unit" class="span2">
                        : <input type="text" id="_p1host" name="_p1host" placeholder="Organization" class="span2">
                        : <input type="text" id="_p1host" name="_p1host" placeholder="City" class="span2">
                        : <input type="text" id="_p1host" name="_p1host" placeholder="State" class="span2">
                        : <input type="text" id="_p1host" name="_p1host" placeholder="Country (US, MY)" class="span1">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="password">Validity Durations</label>
                    <div class="controls">
                        <select class="span2" name="expiry_month" id="expiry_month">
                            <option value="DB2(com.ibm.db2.jcc.DB2Driver)">30 days</option>
                            <option value="DB2(com.ibm.db2.jcc.DB2Driver)">1 year</option>
                            <option value="DB2(com.ibm.db2.jcc.DB2Driver)">2 years</option>
                        </select>
                    </div>
                </div>


                <div id="legend">
                    <legend class=""><input type="checkbox" id="save_card" value="option1"> External CA Connector Details</legend>
                </div>
                <!-- Name -->
                <div class="control-group">
                    <label class="control-label"  for="username">Host/IP : Port </label>
                    <div class="controls">
                        <input type="text" id="_p1host" name="_p1host" placeholder="example localhost/127.0.0.1" class="input-xlarge">
                        : <input type="text" id="_p1host" name="_p1host" placeholder="443" class="span2">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="password">Authenticate Using </label>
                    <div class="controls">
                        <input type="text" id="_p1host" name="_p1host" placeholder=" userid" class="input-xlarge">
                        : <input type="password" id="_p1host" name="_p1host" placeholder="password" class="input-xlarge">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="password">Secure Connection</label>
                    <div class="controls">
                        <input type="checkbox" id="save_card" value="option1">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"  for="username">Implementation </label>
                    <div class="controls">
                        <input type="text" id="_p1host" name="_p1host" placeholder="complete class name including package" class="input-xlarge">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"  for="username">CRL URL </label>
                    <div class="controls">
                        <input type="text" id="_p1host" name="_p1host" placeholder="e.g. /path/pullcrl" class="span3">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"  for="username">OCSP URL</label>
                    <div class="controls">
                        <input type="text" id="_p1host" name="_p1host" placeholder="e.g. /path/ocspcheck" class="span3">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"  for="username">Time Stamp URL</label>
                    <div class="controls">
                        <input type="text" id="_p1host" name="_p1host" placeholder="e.g. /path/gettimestamp" class="span3">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"  for="username">Additional Attributes</label>
                    <div class="controls">
                        <input type="text" id="_p1host" name="_p1host" placeholder="name1=value1" class="span3">
                        : <input type="text" id="_p1host" name="_p1host" placeholder="name2=value2" class="span3">
                        : <input type="text" id="_p1host" name="_p1host" placeholder="name3=value3" class="span3">
                    </div>
                </div>

                <div id="legend">
                    <legend class=""><input type="checkbox" id="save_card" value="option1">  Notification Settings</legend>
                </div>
                <!-- Name -->
                <div class="control-group">
                    <label class="control-label" for="password">Medium</label>
                    <div class="controls">
                        <select class="span2" name="expiry_month" id="expiry_month">
                            <option value="plain">SMS</option>
                            <option value="encrypted">Email</option>
                            <option value="hashed">USSD</option>
                            <option value="hashed">Voice</option>                            
                        </select>
                    </div>
                </div>               

                <div class="control-group">
                    <div class="controls">
                        <button class="btn btn-primary">Save Settings Now >></button>                        
                    </div>
                </div>
            </fieldset>
        </form>
    </div>

</div>

<%@include file="footer.jsp" %>