
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.certDiscovery.utils.IPAddress"%>
<%@page import="com.mollatech.certDiscovery.utils.InstallCert"%>
<%@page import="com.mollatech.certDiscovery.utils.CertDetails"%>
<%@page import="java.util.*"%>

<!--pagination-->
<script src="./assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/dataTables.responsive.min.js"></script>
<link  rel="stylesheet" href="assets/css/jquery.dataTables.min.css">
<link  rel="stylesheet" href="assets/css/responsive.dataTables.min.css">
<%

    String ips = request.getParameter("IP");
    String ports = request.getParameter("port");
    List ip = new ArrayList();

    if (ips.contains("/")) {

        String[] str = ips.split("/");
        List<String> list = IPAddress.getIPAddresses(str[0]);
        ip.addAll(list);
    } else {
        ip.add(ips);
    }

    //
    List port = new ArrayList();
    if (ports.contains(",")) {

        String[] str = ports.split("\\s*,\\s*");

        List<String> items = Arrays.asList(str);
        port.addAll(items);
    } else {
        port.add(ports);
    }

    HashMap<String, CertDetails> map = InstallCert.getCertDetails(ip, port);
    if (map.size() != 0) {

        session.setAttribute("certDetailsObject", map);

%>


<table class="table table-striped   table-bordered table-hover" id="table_main1">
    <thead>
        <tr>
            <th>No</th>
            <th>Hostname/IP Address</th>
            <th>Port</th>
            <th>Subject </th>
            <th>Issuer  </th>
            <th>Issued Date</th>
            <th>Expiry Date</th>
            <th>Expiry Date</th>
            <th>Details</th>
        </tr>
    </thead>

    <%      String strerr = "No Record Found";
        int i = 0;
        Set<String> keys = map.keySet();
        for (String key : keys) {
            i++;
            CertDetails certDetail = map.get(key);
            SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm ");
            if (certDetail != null) {
    %>
    <tbody>
        <tr id="user_search">
            <td><%=i%></td>
            <td id="Ip"><%= certDetail.getIP()%></td>
            <td id="Port<%=i%>"><%= certDetail.getPort()%></td>
            <td><%= certDetail.getSubject()%></td>
            <td><%= certDetail.getIssuer()%></td>
            <td><%= certDetail.getIssueDate()%></td>
            <td><%= certDetail.getExpiry()%></td>
            <td><%= sdf.format(certDetail.getExpiry())%></td>
            <td>
                <div class="btn-group">
                    <a href="#certDetail<%=i%>" class="btn btn-mini" data-toggle="modal" >Details</a>
                </div>
                <div id="certDetail<%=i%>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>

                        <h3 class="text text-success" id="myModalLabel"><div id="idcertDetails"></div></h3>
                    </div>
                    <div class="modal-body">
                        <div class="row-fluid">
                            <form class="form-horizontal" id="addCertForm">

                                <fieldset>
                                    <table class="table table-striped   table-bordered table-hover" id="table_main">
                                        <tr>
                                            <td>Subject : </td>
                                            <td id="_Subject<%=i%>"><%= certDetail.getSubject()%></td>
                                        </tr>
                                        <tr>
                                            <td>Issuer : </td>
                                            <td id="_Issuer<%=i%>"><%= certDetail.getIssuer()%></td>
                                        </tr>
                                        <tr>
                                            <td>SHA1 : </td>
                                            <td id="_SHA1<%=i%>"><%= certDetail.getSha1()%></td>
                                        </tr>
                                        <tr>
                                            <td>MD5 : </td>
                                            <td id="_MD5<%=i%>"><%= certDetail.getMd5()%></td>
                                        </tr>
                                        <tr>
                                            <td>Expiry Date : </td>
                                            <td id="_ExpiryDate<%=i%>"><%= sdf.format(certDetail.getExpiry())%></td>
                                        </tr>
                                        <tr>
                                            <td>Issued Date : </td>
                                            <td id="_IssuedDate<%=i%>"><%= sdf.format(certDetail.getIssueDate())%></td>
                                        </tr>
                                        <tr>
                                            <td>Type : </td>
                                            <td id="_Type<%=i%>"><%= certDetail.getType()%></td>
                                        </tr>

                                    </table>
                                    <!--                                     <script>
                                        $(document).ready(function () {
                                            $('#table_main').DataTable({
                                                responsive: true
                                            });
                                        });
                                    </script>-->
                                </fieldset>
                                <input type="hidden" name="Ips" id="Ips1" value="<%=ips%>">  </input>
                                <input type="hidden" name="Ports" id="Ports1" value="<%=ports%>">  </input>
                            </form>     
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>
            </td>
        </tr>

        <% } else {%>
        <tr>
            <td><%=1%></td>
            <td><%= strerr%></td>
            <td><%= strerr%></td>
            <td><%= strerr%></td>
            <td><%= strerr%></td>
            <td><%= strerr%></td>
            <td><%= strerr%></td>
            <td><%= strerr%></td>
            <td><%= strerr%></td>

        </tr>
        <%}%>
   
    <%}%>
     </tbody>
</table>

<script>
    $(document).ready(function () {
        $('#table_main1').DataTable({
            responsive: true
        });
    });
</script>
<div>
    <div id="addCert-result"></div>
    <button  class="btn btn-primary" onclick="addcert()" id="addCertButton">Add Certificate</button>
</div>
<% } else {  %>  No certificates found  <%}%>



