<%-- 
    Document   : EsignSearhResult
    Created on : 2 Dec, 2015, 12:06:38 AM
    Author     : AMOL
--%>

<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.CategoryManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Doccategory"%>
<%@page import="com.mollatech.axiom.nucleus.db.Signingrequest"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SignRequestManagement"%>
<%
    String sessionId = (String) session.getAttribute("_apSessionID");
    SessionManagement smObj = new SessionManagement();
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
    String channelId = channel.getChannelid();
    String Keys = (String) request.getParameter("_keys");
    String _KeyType = (String) request.getParameter("_keysType");
    int KeyType = 0;
    if (_KeyType != null) {
        KeyType = Integer.parseInt(_KeyType);
    }
    String _startDate = (String) request.getParameter("_startdate");
    String _endDate = (String) request.getParameter("_enddate");
    SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
    Date startDate = sdf.parse(_startDate);
    Date endDate = sdf.parse(_endDate);
    SignRequestManagement srmngt = new SignRequestManagement();
    Signingrequest[] sr = srmngt.getEpdRecords(sessionId, channelId, KeyType, Keys, startDate, endDate);
    int count = 0;%>
<table class="display responsive wrap" id="ResultTable">
    <thead>
    <tr>
        <th><b>Sr No</th>
        <th><b>Name</th>
        <th><b>Category</th>
        <th><b>Status</th>
        <th><b>Details</th>            
        <th><b>Note</th>
        <th><b>Manage</th>
        <th><b>Download</th>
        <th><b>Uploaded On</th>
        <th><b>Published On</th>
        <th><b>Last Updated On</th>                
    </tr>
    </thead>
    <%
        if (sr != null) {
            if (sr.length != 0)
                for (int i = 0; i < sr.length; i++) {
                    String strStatus = "Active";
                    if (sr[i].getStatus() == 1) {
                        strStatus = "Active";
                    } else {
                        strStatus = "Suspended";
                    }
                    String docStatus = "user-status-value-" + i;
                    int catid = sr[i].getDoccategory();
                    CategoryManagement cm = new CategoryManagement();
                    Doccategory dc = cm.getCategoryDetail(sessionId, channelId, sr[i].getDoccategory());
    %>
    <tr>
        <td><%=i + 1%></td><td><%=sr[i].getDocname()%></td><td><%=dc.getCategoryname()%></td>
        <td>
            <%if (sr[i].getStatus() == 1) {%>
            <div class="label label-orange">Active</div>
            <%} else if (sr[i].getStatus() == 0) {%>
            <button class="btn btn-mini" id="starttracking"  href="#" onclick="starttracking(<%= sr[i].getSrid()%>)" > &nbsp; &nbsp; Start  &nbsp; &nbsp;</button>
            <!--<div class="label label-Default">Suspended</div>-->
            <%} else if (sr[i].getStatus() == -1) {%>
            <div class="label label-important">Expired</div>
            <%} else if (sr[i].getStatus() == 2) {%>
            <div class="label label-success">Completed</div>
            <%} else {%>
            <div class="label label-important">Rejected</div>
            <%}%>
        </td>
        <td>
            <a href="./epdfsignTracking.jsp?_docId=<%=sr[i].getSrid()%>" class="btn btn-mini">
                View Details
            </a>
        </td>

        <td>
            <a href="#" class="btn btn-mini" id="documentNote-<%=i%>" data-toggle="popover" data-trigger="focus"  rel="popover" data-html="true">
                Click to View
            </a>
            <script>
                $(function()
                {
                    $("#documentNote-<%=i%>").popover({title: 'Token Details', content: "<%=sr[i].getNote()%>"});
                });
                $("a[rel=popover]")
                        .click(function(e) {
                            e.preventDefault();
                        });

            </script>
        </td>
        <td>
            <div class="btn-group"> 
                <button class="btn btn-mini" id="" <%if (sr[i].getStatus() == 0) {%> <%} else {%> disabled <%}%> >Manage</button>
                <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown" <%if (sr[i].getStatus() == 0) {%> <%} else {%> disabled <%}%> ><span class="caret"></span></button>
                <ul class="dropdown-menu">     
<!--                        <li><a href="#" onclick="changepdfdocStatus(<%= sr[i].getSrid()%>, 1, '<%=docStatus%>')">Mark as Active?</a></li>
                    <li><a href="#" onclick="changepdfdocStatus(<%= sr[i].getSrid()%>, 0, '<%=docStatus%>')">Mark as Suspended?</a></li>
                    <li class="divider"></li>-->
                    <li><a href="./editpdfsignpage.jsp?_docId=<%= sr[i].getSrid()%>" >Edit Details</a></li>
                    <li><a onclick="removeEpdfdoc(<%=sr[i].getSrid()%>)"><font color="red">Remove?</font></a></li>
                </ul> 
            </div>
        </td>
        <td> 
            <a href="./DownloadEpdf?_docId=<%=sr[i].getSrid()%>&_type=1" class="btn btn-mini" target='_blank'>Latest PDF </a>
        </td>
        <td><%= sdf.format(sr[i].getUploadedon())%></td>
        <td><%= sdf.format(sr[i].getPublishon())%></td>
        <td><%= sdf.format(sr[i].getLastupdate())%></td>
        <!--            <td> 
        <% if ((sr[i].getStatus()) == 2 || (sr[i].getStatus()) == 1) {%>
         <button class="btn btn-mini" id="starttracking"  href="#"  disabled="true" > &nbsp; &nbsp; Start  &nbsp; &nbsp;</button>
        <% } else {%>
        <button class="btn btn-mini" id="starttracking"  href="#" onclick="starttracking('<%= sr[i].getSrid()%>')" > &nbsp; &nbsp; Start  &nbsp; &nbsp;</button>
        <%}%>
    </td>-->
    </tr> 
    <%}
    } else {%>
    <tr><td>No record found</td><td>No record found</td><td>No record found</td><td>No record found</td><td>No record found</td><td>No record found</td><td>No record found</td><td>No record found</td><td>No record found</td><td>No record found</td><td>No record found</td></tr>
    <%}%>
</table>

 <script>
    $(document).ready(function () {
        $('#ResultTable').DataTable({
            responsive: true
        });
    });
    </script>
