<%@page import="com.mollatech.axiom.nucleus.db.connector.management.QuestionsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Questions"%>
<%@page import="com.mollatech.axiom.common.utils.UtilityFunctions"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<%@include file="header.jsp" %>
<script src="./assets/js/questions.js"></script>
<%
    String _sessionID = (String) session.getAttribute("_apSessionID");
    SessionManagement smObj = new SessionManagement();
    int iStatus = smObj.GetSessionStatus(_sessionID);
    Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");

    Questions[] questionsObj = null;
    if (iStatus == 1) { //active
        smObj.UpdateSession(_sessionID);
        QuestionsManagement cmObj = new QuestionsManagement();
        questionsObj = cmObj.Listquestions(_sessionID, channel.getChannelid());
        cmObj = null;
    }
    int stat = -1;

    if (questionsObj != null) {
%>
<div class="container-fluid">
    <h1 class="text-success">Challenge Response Management (Q&A Authentication)</h1>
    <div class="row-fluid">
        <div id="licenses_data_table">
            <table class="table table-striped">
                <tr>
                    <td>No</td>
                    <td>Question Type</td>
                    <td>Question</td>
                    <td>Manage</td>
                    <td>Weightage</td>
                    <td>Status</td>
                    <td>Created On</td>
                    <td>Last Updated On</td>
                </tr>

                <%
                    out.flush();
                    QuestionsManagement qObj = new QuestionsManagement();
                    String activeorNot = null;
                    for (int i = 0; i < questionsObj.length; i++) {
                        Questions axcObj = questionsObj[i];
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                        String questionbody = axcObj.getQuestion();

                        stat = axcObj.getStatus();
                        String questionType = null;
                        if (axcObj.getQuestion().length() > 40) {
                            questionType = axcObj.getQuestion().substring(0, 40);
                        } else if (axcObj.getQuestion().length() < 40) {
                            questionType = axcObj.getQuestion();
                        }

                        if (stat == 1) {
                            activeorNot = "Active";

                        } else if (stat == -1) {
                            activeorNot = "Locked";
                        } else {

                            activeorNot = "Suspended";

                        }

                %>
                <tr>
                    <td><%=(i + 1)%></td>
                    <td><%=questionType + "....."%></td>
                    <td>
                        <a href="#" class="btn btn-mini" id="QuestionBody-<%=i%>"  rel="popover" data-html="true">Click to View</a>
                        <script>
                            $(function ()
                            {
                                $("#QuestionBody-<%=i%>").popover({title: 'Question', content: "<%=questionbody%>"});
                            });
                        </script>

                    </td>

                    <td>
                        <div class="btn-group">
                            <button class="btn btn-mini">Manage</button>
                            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">

                                <li><a href="#editQuestion" onclick="loadQuestionDetails('<%=axcObj.getQuestionid()%>')" data-toggle="modal">Edit Details</a></li>


                            </ul>
                        </div>
                    </td>
                    <td><%=axcObj.getWeightage() + "/10"%>
                    <td><%=activeorNot%>
                    <td><%=sdf.format(axcObj.getCreatedOn())%>
                    <td><%=sdf.format(axcObj.getLastUpdatedOn())%></td>

                </tr>
                <%}%>
            </table>
            <%  } else if (questionsObj == null) { %>
            <h3>No Questions found...</h3>
            <%}%>
        </div>
    </div>
    <br>

    <script language="javascript">
        //listChannels();
    </script>
</div>
<p><a href="#addNewQuestion" class="btn btn-primary" data-toggle="modal">Add Question User&raquo;</a></p>            
<!-- Modal -->

<div id="addNewQuestion" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Add New Question</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="questionForm">
                <fieldset>
                    <!-- Name -->
                    <div class="control-group">
                        <label class="control-label"  for="username">Question</label>
                        <div class="controls">
                            <!--<textarea id="_question" name="_question" placeholder="Ex:-  Place of Birth" rows="4" cols="50"></textarea>-->
                            <!--                            <input type="text"  class="input-xlarge">-->
                            <textarea id="_question" name="_question"  placeholder="Ex:-  Place of Birth" class="span9" cols="60" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Weightage</label>
                        <div class="controls">
                            <select class="span4" name="_ch_weightage" id="_ch_weightage">
                                <option value="1">1/10</option>
                                <option value="2">2/10</option>
                                <option value="3">3/10</option>
                                <option value="4">4/10</option>
                                <option value="5">5/10</option>
                                <option value="6">6/10</option>
                                <option value="7">7/10</option>
                                <option value="8">8/10</option>
                                <option value="9">9/10</option>
                                <option value="10">10/10</option>

                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Status</label>
                        <div class="controls">
                            <select class="span4" name="_ch_status" id="_ch_status">
                                <option value="1">Active</option>
                                <option value="0">Suspended</option>
                            </select>
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="addQuestion-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="addQuestion()" id="addQuestionButton">Create Question</button>
    </div>
</div>
<div id="editQuestion" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3><div id="ideditQuestion"></div></h3>
        <h3 id="myModalLabel">Edit Question</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <form class="form-horizontal" id="editQuestionForm">
                <fieldset>
                    <!-- Name -->
                    <input type="hidden" id="_eqid" name="_eqid" >
                    <div class="control-group">
                        <label class="control-label"  for="username">Question</label>
                        <div class="controls">
                            <!--<input type="text" id="_equestion" name="_equestion" placeholder="Ex:-  Place of Birth" class="input-xlarge">-->
                            <textarea id="_equestion" name="_equestion"  placeholder="Ex:-  Place of Birth" class="span9" cols="60" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Weightage</label>
                        <div class="controls">
                            <select class="span4" name="_e_weightage" id="_e_weightage">
                                <option value="1">1/10</option>
                                <option value="2">2/10</option>
                                <option value="3">3/10</option>
                                <option value="4">4/10</option>
                                <option value="5">5/10</option>
                                <option value="6">6/10</option>
                                <option value="7">7/10</option>
                                <option value="8">8/10</option>
                                <option value="9">9/10</option>
                                <option value="10">10/10</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="username">Status</label>
                        <div class="controls">
                            <select class="span4" name="_e_status" id="_e_status">
                                <option value="1">Active</option>
                                <option value="0">Suspended</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <div id="editQuestion-result"></div>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" onclick="editQuestion()" id="editQuestionButton">Edit User</button>
    </div>
</div>

<%@include file="footer.jsp" %>