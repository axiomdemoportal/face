<%@page import="com.mollatech.dictum.management.SurveyManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Interactions"%>
<%@include file="header.jsp" %>
<script src="./assets/js/interactions.js"></script>
<div class="container-fluid">
    <h2>Interaction Management</h2>
    <p>List of interactions, status, action their roles and management. You can manage operator along with its PIN.</p>
    <br>
    <div class="row-fluid">
        <div id="licenses_data_table">
            <table class="table table-striped">
                <tr>
                    <td>No.</td>
                    <td>Name</td>
                    <td>Status</td>
                    <td>Stop?</td>
                    <td>Executions</td>
                    <td>List Executions</td>
                    <td>Manage</td>
                    <td>Execute?</td>
                    <td>Created On</td>
                    <td>Last Updated On</td>
                </tr>

                <%
                            Channels _apSChannelDetails = (Channels) session.getAttribute("_apSChannelDetails");
                            //OperatorsManagement oManagement = new OperatorsManagement();
                            String _sessionID = (String) session.getAttribute("_apSessionID");
                            SessionManagement smObj = new SessionManagement();
                            int iStatus = smObj.GetSessionStatus(_sessionID);
                            
                            SurveyManagement surmngObj = new SurveyManagement();
                            Interactions [] interactionsArrayObj = surmngObj.listInteractions(_apSChannelDetails.getChannelid());
                            
                            if ( interactionsArrayObj != null )
                                    for (int i = 0; i < interactionsArrayObj.length; i++) {
                                Interactions irObj = interactionsArrayObj[i];
                                
                                
                                java.util.Date dLA = irObj.getLastUpdateOn();
                                java.util.Date dCR = irObj.getCreatedOn();

                                int iOprStatus = irObj.getStatus();
                                String strStatus;
                                if (iOprStatus == 1) {
                                    strStatus = "Running...";
                                } else {
                                    strStatus = "Stopped.";
                                }
                                SimpleDateFormat sdf = new SimpleDateFormat("d MMM,yyyy HH:mm ");

                                String uidiv4OprStatus = "operator-status-value-" + i;

                                String uidiv4OprRole = "operator-role-value-" + i;

                                String uidiv4OprAttempts = "operator-attempts-value-" + i;
                                
                                int iExecutions = surmngObj.getExecutionCount(_apSChannelDetails.getChannelid(),irObj.getInteractionid().intValue());
                                

                %>
                <tr>
                    <td><%=i + 1%></td>
                    <td><%=irObj.getInteractionName()%></td>                    
                    
                    <td><span class="label label-success"><%=strStatus%></span></td> 
                     <% if( iOprStatus == 1) {%>
                    <td><a href="#" onclick="stopSurveyJS(<%=irObj.getInteractionid()%>)" class="btn btn-mini btn-danger">Stop Now</a></td><% } 
                    else  {%> 
                    <td><a href="#" onclick="stopSurveyJS(<%=irObj.getInteractionid()%>)" class="btn btn-mini btn-danger" disabled>Stop Now</a></td>
                    <%}%>
                    <td><%=iExecutions%></td>                    
                    <td><a href="InteractionExecutionsList.jsp?_irid=<%=irObj.getInteractionid()%>" class="btn btn-mini">View List</a></td>                    
                    <td>
                        <div class="btn-group">
                            <button class="btn btn-mini" id="<%=uidiv4OprStatus%>">Manage</button>
                            <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="InteractionsEdit.jsp?_irid=<%=irObj.getInteractionid()%>">Edit Details</a></li>
                                <li class="divider"></li>
                                <li><a href="#" onclick="deleteSurveyJS(<%=irObj.getInteractionid()%>)">Remove Interaction?</a></li>
                            </ul>
                        </div>
                    </td>
                    <% if( iOprStatus == 1) {%>
                    <td><a href="InteractionExecuteNow.jsp?_irid=<%=irObj.getInteractionid()%>"  class="btn btn-mini btn-success" disabled>Run Now >></a></td> <% } 
                    else  {%>
                    <td><a href="InteractionExecuteNow.jsp?_irid=<%=irObj.getInteractionid()%>"  class="btn btn-mini btn-success">Run Now >></a></td>
                     <%}%>
                    <td><%=sdf.format(dCR)%></td>
                    <td><%=sdf.format(dLA)%></td>
                </tr>
                <%}%>
            </table>


        </div>
    </div>
    <br>
    <p><a href="InteractionsNew.jsp" class="btn btn-primary">Add New Interaction&raquo;</a></p>
    <script language="javascript">
        //listChannels();
    </script>
</div>





<%@include file="footer.jsp" %>
