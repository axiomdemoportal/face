<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SchedulerManagement"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="com.mollatech.axiom.nucleus.crypto.AxiomProtect"%>
<%@page import="com.mollatech.axiom.nucleus.settings.NucleusMonitorSettings"%>
<%@page import="com.mollatech.axiom.nucleus.db.Monitorsettings"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.axiom.nucleus.db.Monitortracking"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.MonitorSettingsManagement"%>
<%
    int types = 1;
    Channels _apSChannelDetails = (Channels) session.getAttribute("_apSChannelDetails");
    String _settingName = request.getParameter("_settingName");
//    int type = Integer.parseInt(request.getParameter("_type"));
    String start = request.getParameter("_startdate");
    String end = request.getParameter("_enddate");
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    Date startdate = sdf.parse(start);
    Date enddate = sdf.parse(end);
    Map settingsMap = (Map) session.getAttribute("setting");
    Monitorsettings monitorsettings = null;
    monitorsettings = (Monitorsettings) settingsMap.get(_settingName);
    NucleusMonitorSettings nms = null;
    if (monitorsettings != null) {
        byte[] obj = monitorsettings.getMonitorSettingEntry();
        byte[] f = AxiomProtect.AccessDataBytes(obj);
        ByteArrayInputStream bais = new ByteArrayInputStream(f);
        Object object = SchedulerManagement.deserializeFromObject(bais);
        //NucleusMonitorSettings nms = null;
        if (object instanceof NucleusMonitorSettings) {
            nms = (NucleusMonitorSettings) object;
        }
    }
    MonitorSettingsManagement management = new MonitorSettingsManagement();
    Monitortracking[] monitortrackings = management.getMonitorTrackByNameDuration(_apSChannelDetails.getChannelid(), _settingName, startdate, enddate, types);
%>
<script src="./assets/js/raphael-min.js"></script>
<script src="./assets/js/morris.js"></script>
<script src="./assets/js/morris.min.js"></script>
<link rel="stylesheet" href="./assets/css/morris.css">
<script>
    Morris.Line({
    element: 'lineCharts',
            data: [
    <%     if (monitortrackings != null) {
            for (int i = 0; i < monitortrackings.length; i++) {
                Monitortracking mt = (Monitortracking) monitortrackings[i];
                String per = mt.getPerformance();
                String c = per.replaceAll("ms", "");
                int a = Integer.parseInt(c);
                Date d = mt.getExecutionEndOn();
                DateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                String datess = formatter1.format(d);
    %>
            {y: '<%=datess%>', a: <%=a%>} <%if (i != monitortrackings.length) {%>,<%}%>
    <%
        }
    } else {
        Date d = new Date();
        DateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String date = formatter1.format(d);
    %>{y: '<%=date%>', a: 0}<%}%>
            ],
            xkey: 'y',
            ykeys: ['a'],
            xLabels: 'y',
            labels: ['value'],
            lineWidth: 2,
            pointSize: 3,
//            smooth:false,
            postUnits: 'ms'
    });

</script>
<div id="lineCharts"></div>        
