<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserGroupsManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.Usergroups"%>
<%@include file="header.jsp" %>
<%@page import="com.mollatech.axiom.connector.user.AuthUser"%>
<%@page import="com.mollatech.axiom.nucleus.db.Channels"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.UserManagement"%>
<%@page import="com.mollatech.axiom.nucleus.db.connector.management.SessionManagement"%>
<script src="./assets/js/resourcemanagment.js"></script>
<script src="./assets/js/otptokens.js"></script>
<script src="./assets/js/pkitokens.js"></script>
<script src="./assets/js/challengeresponse.js"></script>
<script src="./assets/js/trustedDevice.js"></script>
<script src="./assets/js/geotracking.js"></script>
<script src="./assets/js/usermanagement.js"></script>
<script src="./assets/js/securephrase.js"></script>
<div class="container-fluid" >
    <div id="auditTable">
        <h1 class="text-success">Resource Management</h1>
        <p>List of users in the system. Their password management can be managed from this interface.</p>
        <h3>Search Resource</h3>   
        <div class="input-append">
            <form id="searchUserForm" name="searchUserForm">
                <input type="text" id="_keyword" name="_keyword" placeholder="Search using name, phone, email..." class="span4">
                <select span="1"  name="_resourceName" id="_resourceName" >
                    <%if (oprObj.getRoleid() != 1) {//not sysadmin
                            if (accessObj.ListUsers == true) { %>
                    <option value="1">User & Password</option>
                    <%}
                        if (accessObj.listImageManagement == true) { %>
                    <option value="2">Secure Phase (Image Based)</option>
                    <%}
                        if (accessObj.listOOBSMSToken == true || accessObj.listOOBEMAILToken == true
                                || accessObj.listOOBVOICEToken == true || accessObj.listOOBUSSDToken == true
                                || accessObj.listSWMOBILEToken == true || accessObj.listSWWEBToken == true
                                || accessObj.listSWPCToken == true || accessObj.listHWOTPToken == true) {%>
                    <option value="3">One Time Password Tokens</option>
                    <%}
                        if (accessObj.listCerticate == true || accessObj.listHWPKIToken == true || accessObj.listSWPKIToken == true) {%>
                    <option value="4">Digital Certificates And Tokens</option>
                    <%}
                        if (accessObj.listChallengeResponse == true) {%>
                    <option value="5">Challenge Response (Q&A)</option>
                    <%}
                        if (accessObj.listTrustedDevice == true) {%>
                    <option value="6">Trusted Device (via Mobile)</option>
                    <%}
                        if (accessObj.listGeoLocation == true) {%>
                    <option value="7">Geo-location (Fence & Track)</option>
                    <%}
                        if (accessObj.listTwoWayAuthManagement == true) {%>
                    <option value="8">Two Way Auth Management</option>
                    <%}
                    } else { // sysadmin%>
                    <option value="1">User & Password</option>
                    <option value="2">Secure Phrase (Image Based)</option>
                    <option value="3">One Time Password Tokens</option>
                    <option value="4">Digital Certificates And Tokens</option>
                    <option value="5">Challenge Response (Q&A)</option>
                    <option value="6">Trusted Device</option>
                    <option value="7">Geo-location (Fence & Track)</option>
                    <option value="8">Two Way (Dual Channel)</option>
                    <%}%>
                </select>
                <a href="#" class="btn btn-success" onclick="searchResource(1)">Search Now</a>
            </form>
        </div>

        <%        Channels _apSChannelDetails = (Channels) session.getAttribute("_apSChannelDetails");
            OperatorsManagement oManagement = new OperatorsManagement();
            String _sessionID = (String) session.getAttribute("_apSessionID");
            Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        %>

        <div id="users_table_main">
        </div>

        <!--</div>-->
        </p>
        <br>
        <p><a href="#addNewUser" class="btn btn-primary" id="user_hide" style="width: 110px" data-toggle="modal">Add New User&raquo;</a></p>
        <p id="otp_hide">To Check Hardware Token Secret Validity <a href="#CheckSecret"  class="btn" style="width: 90px" data-toggle="modal">Click Here >> </a></p>
        <script>
            document.getElementById("otp_hide").style.display = 'none';
        </script>

    </div>
    <div id="addNewUser" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3 id="myModalLabel">Add New User</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="addUserForm">
                    <fieldset>
                        <!-- Name -->
                        <div class="control-group">
                            <label class="control-label"  for="username">Name</label>
                            <div class="controls">
                                <input type="text" id="_Name" name="_Name" placeholder="full name" class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Email</label>
                            <div class="controls">
                                <input type="text" id="_Email" name="_Email" placeholder="valid email id" class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Phone</label>
                            <div class="controls">
                                <input type="text" id="_Phone" name="_Phone" placeholder="phone with country code" class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Group</label>
                            <div class="controls">

                                <select class="span4" name="_groupid" id="_groupid">
                                    <%
                                        UserGroupsManagement uMngt = new UserGroupsManagement();
                                        Usergroups[] uList = uMngt.ListGroups(sessionid, _apSChannelDetails.getChannelid());
                                        if (uList != null) {
                                            for (int i = 0; i < uList.length; i++) {
                                                if (uList[i].getStatus() != 0) {
                                                    if (uList[i].getDefaultFlag() == 1) {
                                    %>
                                    <option  selected value="<%=uList[i].getGroupid()%>"><%=uList[i].getGroupname()%></option>
                                    <%} else {%>
                                    <option value="<%=uList[i].getGroupid()%>"><%=uList[i].getGroupname()%></option>
                                    <%
                                                    }
                                                }
                                            }
                                        }
                                    %>
                                </select>
                            </div>
                        </div>

                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <div id="addUser-result"></div>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button class="btn btn-primary" onclick="adduser()" id="addUserButton">Create User</button>
        </div>
    </div>
    <div id="editUser" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>

            <h3 id="myModalLabel">Edit User</h3>
            <h3><div id="idEditUser"></div></h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="editUserForm">
                    <fieldset>
                        <!-- Name -->
                        <input type="hidden" id="_userIdE" name="_userIdE" >
                        <div class="control-group">
                            <label class="control-label"  for="username">Name</label>
                            <div class="controls">
                                <input type="text" id="_NameE" name="_NameE" placeholder="localhost/127.0.10.1" class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Email</label>
                            <div class="controls">
                                <input type="text" id="_EmailE" name="_EmailE" placeholder="xyz@gmail.com" class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Phone</label>
                            <div class="controls">
                                <input type="text" id="_PhoneE" name="_PhoneE" placeholder="1234567890" class="input-xlarge">
                            </div>
                        </div>
                        <!--                        <div class="control-group">
                                                    <label class="control-label"  for="username">Edit Group</label>
                                                    <div class="controls">
                                                        <select class="span4" name="_groupidE" id="_groupidE">
                        <%if (uList != null) {

                                for (int i = 0; i < uList.length; i++) {
                                    if (uList[i].getStatus() != 0) {//not suspend
%>
                                                            <option value="<%=uList[i].getGroupid()%>"><%=uList[i].getGroupname()%></option>
                        <%
                                    }
                                }
                            }
                        %>
                    </select>
                </div>
            </div>-->

                        <!--<hr>-->
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <div id="editUser-result"></div>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <%String edituser = LoadSettings.g_sSettings.getProperty("user.edit");
                if (edituser.equals("true")) {
            %>
            <button class="btn btn-primary" onclick="edituser()" id="editUserButton">Edit User</button>
            <%}%>
        </div>
    </div>

    <div id="userauditDownload" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3><div id="idauditDownload"></div></h3>
            <h3 id="myModalLabel">Download Audit</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="testSMSPrimaryForm" name="testSMSPrimaryForm">
                    <fieldset>
                        <!-- Name -->
                        <input type="hidden" id="_auditUserID" name="_auditUserID"/>
                        <input type="hidden" id="_auditUserName" name="_auditUserName"/>
                        <div class="control-group">
                            <label class="control-label"  for="username">Start Date</label>
                            <div class="controls" align="left" >
                                <!--<span class="add-on">From:</span>-->   
                                <div id="Pushdatetimepicker11" class="input-append date">
                                    <input id="_auditStartDate" name="_auditStartDate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">End Date</label>
                            <div class="controls" align="left">
                                <!--<span class="add-on">Till:</span>-->   
                                <div id="Pushdatetimepicker21" class="input-append date">
                                    <input id="_auditEndDate" name="_auditEndDate" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>                    
                    </fieldset>
                </form>
            </div>
        </div>

        <div class="modal-footer">
            <div id="editoperator-result"></div>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button class="btn btn-primary" onclick="searchUserAudit()" id="buttonEditOperatorSubmit">Show Audit</button>
        </div>
    </div>
    <script>
        $(function () {
            $('#Pushdatetimepicker11').datepicker({
                 format: 'dd/MM/yyyy',
                language: 'pt-BR'
            });
        });
        $(function () {
            $('#Pushdatetimepicker21').datepicker({
                language: 'pt-BR'
            });
        });
    </script>

    <!--otp tokens-->
    <div id="CheckSecret" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3 id="myModalLabel">Hardware Token Secret Validity</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="HWTOKENSECRETVALIDITY">
                    <fieldset>
                        <!-- Name -->
                        <div class="control-group">
                            <label class="control-label"  for="username">Serial Number</label>
                            <div class="controls">
                                <input type="text" id="_serialno" name="_serialno" placeholder="2345xxxxx" class="input-xlarge">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <div id="check-secret-result"></div>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button class="btn btn-primary" onclick="CheckSecretHWToken()" id="checkSecretButton">Check Now >></button>
        </div>
    </div>

    <div id="ResyncSoftware" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3 id="myModalLabel">Resync Software Token</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="ResyncSoftwareform">
                    <fieldset>
                        <!-- Name -->
                        <input type="hidden" id="_userIDS" name="_userIDS" >
                        <input type="hidden" id="_categoryS" name="_categoryS" >
                        <input type="hidden" id="_srno" name="_srno" >
                        <div class="control-group">
                            <label class="control-label"  for="username">First OTP</label>
                            <div class="controls">
                                <input type="text" id="_FirstOtp" name="_FirstOtp" placeholder="2210D03840000A" class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Second OTP</label>
                            <div class="controls">
                                <input type="text" id="_SecondOtp" name="_SecondOtp" placeholder="2210D03840000A" class="input-xlarge">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <div id="ResyncSoftware-result"></div>
            <button class="btn btn-primary" onclick="resyncsoftware()" id="buttonResyncSoftware" type="button">Submit</button>
        </div>
    </div>


    <div id="ResyncHardware" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3 id="myModalLabel">Resync Hardware Token</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="ResyncHardwareform">
                    <fieldset>
                        <!-- Name -->
                        <input type="hidden" id="_userIDHW" name="_userIDHW" >
                        <input type="hidden" id="_categoryHW" name="_categoryHW" value="2">
                        <input type="hidden" id="_srnoHW" name="_srnoHW" >
                        <div class="control-group">
                            <label class="control-label"  for="username">First OTP</label>
                            <div class="controls">
                                <input type="text" id="_FirstOtp" name="_FirstOtp" placeholder="2210D03840000A" class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Second OTP</label>
                            <div class="controls">
                                <input type="text" id="_SecondOtp" name="_SecondOtp" placeholder="2210D03840000A" class="input-xlarge">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <div id="ResyncHardware-result"></div>
            <button class="btn btn-primary" onclick="resynchardware()" id="buttonResyncHardware" type="button">Submit</button>
        </div>
    </div>
    <div id="PUKCODE" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3><div id="idEditPushMaperName"></div></h3>
            <h3 id="myModalLabel">PUK Code</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="pukform">
                    <fieldset>
                        <!-- Name -->
                        <!--<input type="hidden" id="_userIDHR" name="_userIDHR" >-->
                        <input type="hidden" id="_useridPUK" name="_useridPUK" >
                        <input type="hidden" id="_pukExpiry" name="_pukExpiry" >
                        <!--                        <input type="hidden" id="_subcategoryPUK" name="_subcategoryPUK" >
                                                <input type="hidden" id="_pukcode" name="_pukcode" >-->
                        <div class="control-group">
                            <label class="control-label"  for="username">PUK Code</label>
                            <div class="controls">
                                <input type="text" id="_pukcode" name="_pukcode" placeholder="e.g 123231212"  readonly class="input-xlarge">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <div id="puk-result"></div>
            <button class="btn btn-primary" onclick="sendPUKCode('EMAIL')" id="buttonPUK" type="button">Send via Email</button>
            <button class="btn btn-primary" onclick="sendPUKCode('SMS')" id="buttonPUK" type="button">Send via SMS</button>
        </div>
    </div>       
    <div id="HardRegistration" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3><div id="idEditPushMaperName"></div></h3>
            <h3 id="myModalLabel">Assign Hardware Token</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="HardwareRegistrationform">
                    <fieldset>
                        <!-- Name -->
                        <!--<input type="hidden" id="_userIDHR" name="_userIDHR" >-->
                        <input type="hidden" id="_userid" name="_userid" >
                        <input type="hidden" id="_category" name="_category" >
                        <input type="hidden" id="_subcategory" name="_subcategory" >
                        <div class="control-group">
                            <label class="control-label"  for="username">Serial Number</label>
                            <div class="controls">
                                <input type="text" id="_serialnumber" name="_serialnumber" placeholder="e.g 123231212" class="input-xlarge">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <div id="HardwareRegistration-result"></div>
            <button class="btn btn-primary" onclick="assignhardwaretoken1()" id="buttonHardwareRegistration" type="button">Submit</button>
        </div>
    </div>        

    <script language="javascript">
        //listChannels();
    </script>

    <div id="verifyOTP" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3 id="myModalLabel">Verify One Time Password</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="verifyOTPForm" name="verifyOTPForm">
                    <fieldset>
                        <input type="hidden" id="_userIDOV" name="_userIDOV" >
                        <input type="hidden" id="_usertypeIDS" name="_usertypeIDS">
                        <div class="control-group">
                            <label class="control-label"  for="username">Enter One Time Password to Verify:</label>
                            <div class="controls">
                                <input type="text" id="_oobotp" name="_oobotp" placeholder="Enter OTP" class="input-medium">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <div class="span3" id="verify-otp-result"></div>
            <button class="btn btn-primary" onclick="verifyOTP()" id="addnewOperatorSubmitBut">Verify OTP</button>
        </div>
    </div>

    <div id="ReplaceHardware" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3 id="myModalLabel">Replace Hardware Token</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="ReplaceHardwareform">
                    <fieldset>
                        <!-- Name -->
                        <input type="hidden" id="_userIDReplaceHW" name="_userIDReplaceHW" >
                        <input type="hidden" id="_categoryReplaceHW" name="_categoryReplaceHW" >
                        <input type="hidden" id="_subcategoryReplaceHW" name="_subcategoryReplaceHW" >
                        <!--<input type="hidden" id="_srnoHW" name="_srnoHW" >-->
                        <div class="control-group">
                            <label class="control-label"  for="username">Old Token Serial No</label>
                            <div class="controls">
                                <input type="text" id="_oldTokenSerialNo" name="_oldTokenSerialNo" placeholder="2210D03840000A" class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Old Token Marked As</label>
                            <div class="controls">
                                <select name="_oldTokenStatus" id="_oldTokenStatus" class="span3">
                                    <option value="-5">Lost</option>                                             
                                    <option value="-10">Free</option>
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">New Token Serial No</label>
                            <div class="controls">
                                <input type="text" id="_newTokenSerialNo" name="_newTokenSerialNo" placeholder="2210D03840000A" class="input-xlarge">
                            </div>
                        </div>

                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <div id="ReplaceHardware-result"></div>
            <button class="btn btn-primary" onclick="replacehardware()" id="buttonResyncHardware" type="button">Submit</button>
        </div>
    </div>

    <div id="userTokenauditDownload" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3><div id="idauditDownload"></div></h3>
            <h3 id="myModalLabel">Download Audit</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="testSMSPrimaryForm" name="testSMSPrimaryForm">
                    <fieldset>
                        <!-- Name -->
                        <input type="hidden" id="_auditUserID1" name="_auditUserID1"/>
                        <input type="hidden" id="_auditUserName1" name="_auditUserName1"/>
                        <div class="control-group">
                            <label class="control-label"  for="username">Start Date</label>
                            <div class="controls" align="left" >

                                <!--<span class="add-on">From:</span>-->   
                                <div id="Pushdatetimepicker1" class="input-append date">
                                    <input id="_auditStartDate1" name="_auditStartDate1" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">End Date</label>
                            <div class="controls" align="left">
                                <!--<span class="add-on">Till:</span>-->   
                                <div id="Pushdatetimepicker2" class="input-append date">
                                    <input id="_auditEndDate1" name="_auditEndDate1" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>                    
                    </fieldset>
                </form>
            </div>
        </div>

        <div class="modal-footer">
            <div id="editoperator-result"></div>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button class="btn btn-primary" onclick="searchUserTokenAudit()" id="buttonEditOperatorSubmit">Show Audit</button>
        </div>
    </div>
    <script>
        $(function () {
            $('#Pushdatetimepicker1').datepicker({
                 format: 'dd/MM/yyyy',
                language: 'pt-BR'
            });
        });
        $(function () {
            $('#Pushdatetimepicker2').datepicker({
                 format: 'dd/MM/yyyy',
                language: 'pt-BR'
            });
        });
    </script>
    <!--end otp tokens-->
    <!--pki--> 
    <div id="Details" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3 id="myModalLabel">User Details</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="Detailsform">
                    <fieldset>
                        <!--                    Name 
                                            <div class="control-group">
                                                <label class="control-label"  for="username">User ID</label>
                                                <div class="controls">
                                                    <input type="text" id="_userid" name="_userid" class="input-xlarge">
                                                </div>
                                            </div>-->
                        <div class="control-group">
                            <label class="control-label"  for="username">User Name</label>
                            <div class="controls">
                                <input type="text" id="_username" name="_username" class="input-xlarge">
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label"  for="username">Email</label>
                            <div class="controls">
                                <input type="text" id="_email" name="_email"  class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Phone</label>
                            <div class="controls">
                                <input type="text" id="_Phone" name="_Phone"  class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Organization</label>
                            <div class="controls">
                                <input type="text" id="_Organisation" name="_Organisation"  class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Organizational Unit</label>
                            <div class="controls">
                                <input type="text" id="_OrganisationUnit" name="_OrganisationUnit"  class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">City</label>
                            <div class="controls">
                                <input type="text" id="_city" name="_city"  class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">State</label>
                            <div class="controls">
                                <input type="text" id="_State" name="_State"  class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Pincode</label>
                            <div class="controls">
                                <input type="text" id="_pincode" name="_pincode"  class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Country</label>
                            <div class="controls">
                                <input type="text" id="_Country" name="_Country"  class="input-xlarge">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
    </div>


    <div id="certificateDetails" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3 id="myModalLabel">Certificate Details</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="certform">
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label"  for="username">Serial Number</label>
                            <div class="controls">
                                <input type="text" id="_srnoCertifiacte" name="_srnoCertifiacte" class="span4">   
                                & Algorithm: <input type="text" id="_algoname" name="_algoname" class="span4">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">User Details</label>
                            <div class="controls">
                                <textarea id="_subjectdn" name="_subjectdn" style="width:95%"></textarea>
                                <!--<input type="text" id="_subjectdn" name="_subjectdn"  class="input-xlarge">-->
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Issuer Details</label>
                            <div class="controls">
                                <textarea id="_issuerdn" name="_issuerdn" style="width:95%"></textarea>
                                <!--<input type="text" id="_issuerdn" name="_issuerdn"  class="input-xlarge">-->
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Issued On</label>
                            <div class="controls">
                                <input type="text" id="_notbefore" name="_notbefore"  class="input-xlarge">

                            </div>
                        </div>                    
                        <div class="control-group">
                            <label class="control-label"  for="username">Valid till</label>
                            <div class="controls">
                                <input type="text" id="_notafter" name="_notafter"  class="input-xlarge">
                            </div>
                        </div>                    


                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
    </div>


    <div id="HardwarePkiToken" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3><div id="idEditPushMaperName"></div></h3>
            <h3 id="myModalLabel">Assign Hardware Token</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="HardwarePkiTokenform">
                    <fieldset>
                        <!-- Name -->
                        <input type="hidden" id="_userIDHR" name="_userIDHR" >

                        <div class="control-group">
                            <label class="control-label"  for="username">Serial Number</label>
                            <div class="controls">
                                <input type="text" id="_registrationCode" name="_registrationCode" placeholder="2210D03840000A" class="input-xlarge">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <div id="HardwarePkiToken-result"></div>
            <button class="btn btn-primary" onclick="assignhardwaretokens(2)" id="buttonHardwareRegistration" type="button">Submit</button>
        </div>
    </div>   
    <div id="RevokeCertificate" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3><div id="idEditPushMaperName"></div></h3>
            <h3 id="myModalLabel">Reason To Revoke</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="RevokeCertificateform">
                    <fieldset>
                        <!-- Name -->
                        <input type="hidden" id="_userCR" name="_userCR" >

                        <div class="control-group">
                            <label class="control-label"  for="username">Reason</label>
                            <div class="controls">
                                <input type="text" id="_reason" name="_reason" placeholder="Any reason?" class="input-xlarge">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <div id="HardwarePkiToken-result"></div>
            <button class="btn btn-primary" onclick="revokecertificate()" id="buttonHardwareRegistration" type="button">Submit</button>
        </div>
    </div>


    <div id="SendCertificate" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3><div id="idSendCertificate"></div></h3>
            <h3 id="myModalLabel">Send Certificate To</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="SendCertificateform">
                    <fieldset>
                        <!-- Name -->
                        <input type="hidden" id="_userSENDCERT" name="_userSENDCERT" >

                        <div class="control-group">
                            <label class="control-label"  for="username">Recipient's Emailid</label>
                            <div class="controls">
                                <input type="text" id="_emailSENDCERT" name="_emailSENDCERT" placeholder="email id " class="input-xlarge">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <div id="SendCertificate-result"></div>
            <button class="btn btn-primary" onclick="sendcertificatefile()" id="buttonSendCertificate" type="button">Submit</button>
        </div>
    </div>

    <div id="kycupload" class="modal hide fade" tabindex="-1" role="dialog" style="width: 650px;"   aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3 id="myModalLabel">Upload Documents</h3>
            <p>Please Compress all documents in zip and upload here.</p>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="kycuploadForm">
                    <fieldset>
                        <input type="hidden" id="_usersid" name="_usersid" >

                        <div class="control-group">
                            <label class="control-label"  for="username">Select File:</label>                                    
                            <div class="controls fileupload fileupload-new" data-provides="fileupload">
                                <div class="input-append">
                                    <div class="uneditable-input span3"><i class="icon-file fileupload-exists"></i> 
                                        <span class="fileupload-preview"></span>
                                    </div>
                                    <span class="btn btn-file" >
                                        <span class="fileupload-new"><i class='icon-file'></i></span>
                                        <span class="fileupload-exists"><i class='icon-file'></i></span>
                                        <input type="file" id="filekycupload" name="filekycupload"/>
                                    </span>
                                    <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><i class='icon-trash'></i></a>
                                    <button class="btn btn-success" id="buttonkycUpload"  onclick="UploadkycFile()">Upload</button>
                                </div>
                            </div>

                        </div>
                        <br>
                    </fieldset>
                </form>
            </div>
        </div>
        <!--    <div class="modal-footer">
                <div id="addUser-result"></div>
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <button class="btn btn-primary" onclick="adduser()" id="addUserButton">Save Details Now>></button>
            </div>-->
    </div> 

    <div id="userPkiTokenauditDownload" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3><div id="idauditDownload"></div></h3>
            <h3 id="myModalLabel">Download Audit</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="testSMSPrimaryForm" name="testSMSPrimaryForm">
                    <fieldset>
                        <!-- Name -->
                        <input type="hidden" id="_auditUserIDPKI" name="_auditUserIDPKI"/>
                        <input type="hidden" id="_auditUserNamePKI" name="_auditUserNamePKI"/>
                        <div class="control-group">
                            <label class="control-label"  for="username">Start Date</label>
                            <div class="controls" align="left" >

                                <!--<span class="add-on">From:</span>-->   
                                <div id="PushdatetimepickerPKI" class="input-append date">
                                    <input id="_auditStartDatePKI" name="_auditStartDatePKI" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">End Date</label>
                            <div class="controls" align="left">
                                <!--<span class="add-on">Till:</span>-->   
                                <div id="PushdatetimepickerPKI1" class="input-append date">
                                    <input id="_auditEndDatePKI" name="_auditEndDatePKI" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>                    
                    </fieldset>
                </form>
            </div>
        </div>
        <script>
            $(function () {
                $('#PushdatetimepickerPKI').datepicker({
                     format: 'dd/MM/yyyy',
                    language: 'pt-BR'
                });
            });
            $(function () {
                $('#PushdatetimepickerPKI1').datepicker({
                     format: 'dd/MM/yyyy',
                    language: 'pt-BR'
                });
            });
        </script>
        <div class="modal-footer">
            <div id="editoperator-result"></div>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button class="btn btn-primary" onclick="searchUsersPKITokenAuditPKI()" id="buttonEditOperatorSubmit">Show Audit</button>
        </div>
    </div>

    <!--end pki-->

    <!--challenge response-->
    <div id="userChallengeResponseauditDownload" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3><div id="idauditDownload"></div></h3>
            <h3 id="myModalLabel">Download Audit</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="testSMSPrimaryForm" name="testSMSPrimaryForm">
                    <fieldset>
                        <!-- Name -->
                        <input type="hidden" id="_auditUserIDChallenge" name="_auditUserIDChallenge"/>
                        <input type="hidden" id="_auditUserNameChallenge" name="_auditUserNameChallenge"/>
                        <div class="control-group">
                            <label class="control-label"  for="username">Start Date</label>
                            <div class="controls" align="left" >

                                <!--<span class="add-on">From:</span>-->   
                                <div id="PushdatetimepickerChallenge" class="input-append date">
                                    <input id="_auditStartDateChallenge" name="_auditStartDateChallenge" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">End Date</label>
                            <div class="controls" align="left">
                                <!--<span class="add-on">Till:</span>-->   
                                <div id="PushdatetimepickerChallenge2" class="input-append date">
                                    <input id="_auditEndDateChallenge" name="_auditEndDateChallenge" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>                    
                    </fieldset>
                </form>
            </div>
        </div>

        <script>
            $(function () {
                $('#PushdatetimepickerChallenge').datepicker({
                     format: 'dd/MM/yyyy',
                    language: 'pt-BR'
                });
            });
            $(function () {
                $('#PushdatetimepickerChallenge2').datepicker({
                     format: 'dd/MM/yyyy',
                    language: 'pt-BR'
                });
            });
        </script>
        <div class="modal-footer">
            <div id="editoperator-result"></div>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button class="btn btn-primary" onclick="searchUsersChallengeResponseAudit()" id="buttonEditOperatorSubmit">Show Audit</button>
        </div>
    </div>
    <!--end challenge response-->
    <!--secure phrase-->
    <div id="securePhraseAuditDowload" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3><div id="idauditDownload"></div></h3>
            <h3 id="myModalLabel">Download Audit</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="securePhraseForm" name="securePhraseForm">
                    <input type="hidden" id="_userIDSecureSecurePhrase" name="_userIDSecureSecurePhrase"/>
                    <input type="hidden" id="_userNameSecureSecurePhrase" name="_userNameSecureSecurePhrase"/>
                    <fieldset>
                        <!-- Name -->


                        <div class="control-group">
                            <label class="control-label"  for="username">Start Date</label>
                            <div class="controls" align="left" >

                                <!--<span class="add-on">From:</span>-->   
                                <div id="PushdatetimepickerSecurePhrase" class="input-append date">
                                    <input id="_auditStartDateSecurePhrase" name="_auditStartDateSecurePhrase" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">End Date</label>
                            <div class="controls" align="left">
                                <!--<span class="add-on">Till:</span>-->   
                                <div id="PushdatetimepickerSecurePhrase1" class="input-append date">
                                    <input id="_auditEndDateSecurePhrase" name="_auditEndDateSecurePhrase" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>                    
                    </fieldset>
                </form>
            </div>
        </div>

        <div class="modal-footer">
            <div id="editoperator-result"></div>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button class="btn btn-primary" onclick="searchSecurePhraseAudit1()" id="buttonEditOperatorSubmit">Show Audit</button>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('#PushdatetimepickerSecurePhrase').datepicker({
                 format: 'dd/MM/yyyy',
                language: 'pt-BR'
            });
        });
        $(function () {
            $('#PushdatetimepickerSecurePhrase1').datepicker({
                 format: 'dd/MM/yyyy',
                language: 'pt-BR'
            });
        });
        //                            ChangeMediaType(0);
    </script>    


    <div id="suspendToken" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
<!--            <h3 id="myModalLabel">Reject Request</h3>-->
<h3 id="myModalLabel">Are You Sure?</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="suspendTokenform" name="suspendTokenform">
                    <fieldset>
                        <input type="hidden" id="_userstatus" name="_userstatus" >
                        <input type="hidden" id="_userID" name="_userID" >
                        <input type="hidden" id="_usercategory" name="_usercategory" >
                        <input type="hidden" id="_usersubcategory" name="_usersubcategory" >
                        <input type="hidden" id="uidiv" name="uidiv" >
                       <!--change asked by acleda to hide description windows -->
                        <div class="control-group" style="display:none">
                            <label class="control-label"  for="username">Description</label>
                            <div class="controls">
                                <textarea class="form-control" value="No reason" rows="3" id="_suspendreason" name="_suspendreason"></textarea>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <div id="rejectkyc-result"></div>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button class="btn btn-primary" onclick="suspendToken()" id="addUserButton">Suspend Now>></button>
        </div>
    </div>                   
    <!--secure phrase-->

    <!-- Added by abhishek-->
    <div id="userDetail" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>

            <h3 class="text text-success" id="myModalLabel"><div id="iduserDetails"></div></h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="editUserDetailsForm">
                    <fieldset>
                        <!-- Name -->
                        <input type="hidden" id="_userIdD" name="_userIdD" >
                        <div class="control-group">
                            <label class="control-label"  for="username">Name</label>
                            <div class="controls">
                                <input type="text" id="_NameD" name="_NameD" placeholder="localhost/127.0.10.1" class="input-xlarge disabled">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Country</label>
                            <div class="controls">
                                <input type="text" id="_CountryD" name="_CountryD" placeholder="1234567890" class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Location</label>
                            <div class="controls">
                                <input type="text" id="_LocationD" name="_LocationD" placeholder="1234567890" class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Street</label>
                            <div class="controls">
                                <input type="text" id="_StreetD" name="_StreetD" placeholder="1234567890" class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Designation</label>
                            <div class="controls">
                                <input type="text" id="_DesignationD" name="_DesignationD" placeholder="1234567890" class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Organisation</label>
                            <div class="controls">
                                <input type="text" id="_OrganisationD" name="_OrganisationD" placeholder="1234567890" class="input-xlarge">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Organisation Unit</label>
                            <div class="controls">
                                <input type="text" id="_OrganisationUnitD" name="_OrganisationUnitD" placeholder="1234567890" class="input-xlarge">
                            </div>
                        </div>

                        <hr>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <div id="userDetail-result"></div>
            <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
            <%String edituserDetails = LoadSettings.g_sSettings.getProperty("user.edit");
                if (edituserDetails.equals("true")) {
            %>
            <button class="btn btn-primary" onclick="edituserdetails()" id="editUserDetailsButton">Edit User</button>
            <%}%>
        </div>
    </div>


    <div id="twowayAuthAuditDowload" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h3><div id="idauditDownload"></div></h3>
            <h3 id="myModalLabel">Download Audit</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <form class="form-horizontal" id="_twowayAuditForm" name="_twowayAuditForm">
                    <input type="hidden" id="_twowayUserId" name="_twowayUserId"/>
                    <input type="hidden" id="_twowayUserName" name="_twowayUserName"/>
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label"  for="username">Start Date</label>
                            <div class="controls" align="left" >

                                <div id="PushdatetimepickerSecurePhrase" class="input-append date">
                                    <input id="_auditStartDateTwoway" name="_auditStartDateTwoway" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">End Date</label>
                            <div class="controls" align="left">
                                <!--<span class="add-on">Till:</span>-->   
                                <div id="PushdatetimepickerSecurePhrase1" class="input-append date">
                                    <input id="_auditEndDateTwoWay" name="_auditEndDateTwoWay" type="text" data-format="dd-MM-yyyy" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>                    
                    </fieldset>
                </form>
            </div>
        </div>

        <div class="modal-footer">
            <div id="editoperator-result"></div>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button class="btn btn-primary" onclick="downloadTWAuthAudit()" id="buttonEditOperatorSubmit">Show Audit</button>
        </div>
        <script>
            $(function () {
                $('#_auditStartDateTwoway').datepicker({
                     format: 'dd/MM/yyyy',
                    language: 'pt-BR'
                });
            });
            $(function () {
                $('#_auditEndDateTwoWay').datepicker({
                     format: 'dd/MM/yyyy',
                    language: 'pt-BR'
                });
            });
        </script>
    </div>

    <%@include file="footer.jsp" %>