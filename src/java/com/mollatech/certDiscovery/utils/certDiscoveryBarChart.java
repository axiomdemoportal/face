/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.certDiscovery.utils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.face.common.bar;
import com.mollatech.axiom.nucleus.db.ApCertDiscovery;
import com.mollatech.axiom.nucleus.db.Channels;

import com.mollatech.axiom.nucleus.db.connector.management.CertDiscoveryManagement;
import com.mollatech.axiom.v2.face.handler.certificates.certbarchart;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author bluebricks
 */
public class certDiscoveryBarChart extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(certDiscoveryBarChart.class.getName());
    final int iActive = 1;
    final int iRevoked = -5;
    final int iExpired = -10;
    final int iGoing_To_Expire = -1;

   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
         response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String channelID = channel.getChannelid();
//        String _startdate = request.getParameter("_startdate");
//        String _enddate   = request.getParameter("_enddate");
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        Date startDate = null;
        Date endDate = null;
        ApCertDiscovery[] arrCobj = null;
        try {
//            if (_startdate != null && !_startdate.isEmpty()) {
//                startDate = (Date) formatter.parse(_startdate);
//            }
//            
//            if (_enddate != null && !_enddate.isEmpty()) {
//                endDate = (Date) formatter.parse(_enddate);
//            }

            
            CertDiscoveryManagement cObj = new CertDiscoveryManagement();
            int certcount=cObj.getCertDiscoveryCount();
            
           // int Going_To_Expire = cObj.getCertCountByStatus(channelID, iGoing_To_Expire);
            // new change
            arrCobj = cObj.getExpireSoonCertificate();
            int Going_To_Expire = 0;
            if(arrCobj != null){
                Going_To_Expire = arrCobj.length;
            }

            
             arrCobj = cObj.getExpireCertificateIn60Days();
            int Going_To_ExpireIn60Days = 0;
            if(arrCobj != null){
               
                Going_To_ExpireIn60Days = arrCobj.length;
            }
            
            
            arrCobj = cObj.getExpireCertificateIn45Days();
            int Going_To_ExpireIn45Days = 0;
            if(arrCobj != null){
                Going_To_ExpireIn45Days = arrCobj.length;
            }
            
             arrCobj = cObj.getExpireCertificateIn7Days();
            int Going_To_ExpireIn7Days = 0;
            if(arrCobj != null){
                Going_To_ExpireIn7Days = arrCobj.length;
            }
            
             arrCobj = cObj.getExpireCertificateIn90Days();
            int Going_To_ExpireIn90Days = 0;
            if(arrCobj != null){
                Going_To_ExpireIn90Days = arrCobj.length;
            }
            
            arrCobj = cObj.getExpireCertificateIn0Days();
            int Going_To_ExpireIn0Days = 0;
            if(arrCobj != null){
                Going_To_ExpireIn0Days = arrCobj.length;
            }
            

            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            ArrayList<bar> sample = new ArrayList<bar>();
            sample.add(new bar(Going_To_ExpireIn0Days, "0 Days"));
            sample.add(new bar(Going_To_ExpireIn7Days, "7 Days"));
            sample.add(new bar(Going_To_Expire, "30 Days"));
            sample.add(new bar(Going_To_ExpireIn45Days,"45 Days"));
              sample.add(new bar(Going_To_ExpireIn60Days,"60 Days"));
                sample.add(new bar(Going_To_ExpireIn90Days,"90 Days"));


//            for (int i = 0; i < sample.size(); i++) {
//                System.out.println(sample.get(i));
//            }
            Gson gson = new Gson();

            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<bar>>() {
            }.getType());

            JsonArray jsonArray = element.getAsJsonArray();

            out.print(jsonArray);

        }catch(Exception e){
           log.error("Exception caught :: ",e);
        }finally {
            out.close();
            log.info("Servlet ended");
        }
    }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
