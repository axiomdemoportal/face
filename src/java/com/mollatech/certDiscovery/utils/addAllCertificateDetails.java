/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.certDiscovery.utils;


import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.db.ApCertDiscovery;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.CertDiscoveryManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import static com.mollatech.certDiscovery.utils.addcertDiscoveryDetails.log;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class addAllCertificateDetails extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
          Map<String, CertDetails> map = (Map<String, CertDetails>)request.getSession().getAttribute("certDetailsObject");
      AuditManagement audit = new AuditManagement();
        response.setContentType("text/html;charset=UTF-8");
        response.setContentType("application/json");
        log.info("Servlet started");
        String ips = request.getParameter("Ips1");
        log.debug("ips :: "+ips);
        String ports = request.getParameter("Ports1");
        log.debug("ports :: "+ports);
       Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
          Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());
         String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        String remoteAddress = request.getRemoteAddr();
       
      
        String userid = request.getParameter("_userid");
        log.debug("userid :: "+userid);
          OperatorsManagement oManagement = new OperatorsManagement();
            Operators operartorObj = null;
        log.debug("sessionId :: "+sessionId);
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String result = null;
        String message = null;
//    
//       List<String> ip = new ArrayList();
//        if (ips.contains("/")) {
//
//            String[] str = ips.split("/");
//            List<String> list = IPAddress.getIPAddresses(str[0]);
//            ip.addAll(list);
//        } else {
//            ip.add(ips);
//        }
//        //
//        List<String> port = new ArrayList();
//        if (ports.contains(",")) {
//
//            String[] str = ports.split("\\s*,\\s*");
//
//            List<String> items = Arrays.asList(str);
//            port.addAll(items);
//        } else {
//            port.add(ports);
//        }

   //
            List<String> port1 = new ArrayList();
           List<String> ip1 = new ArrayList();

    int i = 0;
     String regcode=null;
     String port=null;
     String ip=null;
     
            Set<String> keys = map.keySet();
            for (String key : keys) {
                i++;
                CertDetails certDetail = map.get(key);
                SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm ");
                if(certDetail!= null){
                   ip= certDetail.getIP();
                   ip1.add(ip);
                    port=certDetail.getPort();
                    port1.add(port);              
                    
                    
                    
            JSONObject detail1 = new JSONObject();
            try {
                detail1.put("_Port", port);
                detail1.put("ip", ip);

            } catch (Exception e) {
                e.printStackTrace();
            }
            String PortIpDetails = detail1.toString();
                   
                    boolean isConnect = NetUtils.isConnectable(ip, port);
                  
                    if (isConnect == true) {
                        regcode = GenerateAcno.generateAckno();
                        
                                   CertDiscoveryManagement cMgnt = new CertDiscoveryManagement();
            ApCertDiscovery[] apCertDiscovery = cMgnt.getCertDiscoveryDetails();
             int l=0;
           boolean alreadyPresentIpFound = false;
             if(apCertDiscovery!=null)
            {   l=apCertDiscovery.length;
             String PresentCID = null;
            for (int j = 0; j < l; j++) {

                if (apCertDiscovery[j].getPortIpDetails().equals(PortIpDetails)) {
                    PresentCID= apCertDiscovery[j].getCid();
                    alreadyPresentIpFound = true;
                  
                    break;
                }
            }         result = "success";
            
                        if(alreadyPresentIpFound == true){
                            message ="Certificate Detalis Updated Successfully..!!";
                        }else {
                            message ="Certificate Detalis Added Successfully..!!";
                        }
                        
            }else
             {
              message ="Certificate Detalis Added Successfully..!!";
             }
                        
                    } else {
                        System.out.println("Unable To connect");  
                         result = "error";
                        message ="Failed to add Certificate Detalis..!!";                        
                        
                    }

            }
            }
            
              Runnable r1 = new certDiscoverythread(ip1, port1, regcode,sessionId,channel,operatorS,remoteAddress,remoteaccesslogin,userid);
            new Thread(r1).start();
        
        
        
        try {
            json.put("_result", result);
            json.put("_message", message);
            
            

        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
//            audit.AddAuditTrail(sessionId, channel.getChannelid(), operartorObj.getOperatorid(),
//                                request.getRemoteAddr(), channel.getName(), credentialInfo[0], operartorObj.getName(),
//                                new Date(), "Login", "FAILED", FAILED,
//                                "Operator Management", "", message, "OPERATOR", operartorObj.getOperatorid());
//            log.info("Servlet ended");
            return;
        }
        
        
    }   
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
