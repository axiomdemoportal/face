package com.mollatech.certDiscovery.utils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class IPAddress {

    public final static int TIME_OUT = 100;
    
    public static List<String> getIPAddresses(String subNet) throws IOException {
        
        List<String> ipAddress = new ArrayList<>();
        
        for (int i = 1; i < 255; i++) {
            String host = subNet + "." + i;
            try{
            
          if (InetAddress.getByName(host).isReachable(TIME_OUT)) {
      
                ipAddress.add(host);
                //System.out.println(host + " is reachable");
           }
           }  catch(UnknownHostException e){
                   System.out.println("Unknown Host" +e);
                    }
        }
        return ipAddress;
    }
     public static List<String> getIPAddressesOffline(String subNet) throws IOException {
        
        List<String> ipAddress = new ArrayList<>();
        
        for (int i = 1; i < 255; i++) {
            String host = subNet + "." + i;
       
      
                ipAddress.add(host);
                //System.out.println(host + " is reachable");
           
        }
        return ipAddress;
    }

    public static void main(String[] arguments) throws IOException {
        List<String> list = getIPAddresses("192.168.0");
        for(String ip : list){
            System.out.println("IP Address : "+ip);
        }
    }
}
