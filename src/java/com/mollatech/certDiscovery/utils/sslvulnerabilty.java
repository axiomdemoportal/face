/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.certDiscovery.utils;

/**
 *
 * @author bluebricks
 */
public class sslvulnerabilty {
   
    private String best;
    private String crime;
    private String protocalversion;
     private String heartbleed;
     private String freak;
     private String csStrength;
     public sslvulnerabilty(){}

    public sslvulnerabilty(String best, String crime, String protocalversion,String heartbleed,String freak,String csStrength) {
        this.best = best;
        this.crime = crime;
        this.protocalversion = protocalversion;
        this.heartbleed = heartbleed;
        this.freak = freak;
        this.csStrength = csStrength;
    }

    public String getBest() {
        return best;
    }

    public void setBest(String best) {
        this.best = best;
    }

    public String getCrime() {
        return crime;
    }

    public void setCrime(String crime) {
        this.crime = crime;
    }

    public String getProtocalversion() {
        return protocalversion;
    }

    public void setProtocalversion(String protocalversion) {
        this.protocalversion = protocalversion;
    }
      public String getHeartbleed() {
        return heartbleed;
    }

    public void setheartbleed(String heartbleed) {
        this.heartbleed = heartbleed;
    }
       public String getFreak() {
        return freak;
    }

    public void setfreak(String freak) {
        this.freak = freak;
    }
       public String getCsStrength() {
        return csStrength;
    }

    public void setCsStrength(String csStrength) {
        this.csStrength = csStrength;
    }
    
    
}
