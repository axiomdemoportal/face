/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.certDiscovery.utils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.face.common.bar;
import com.mollatech.axiom.face.common.float_donut;
import com.mollatech.axiom.nucleus.db.ApCertDiscovery;
import com.mollatech.axiom.nucleus.db.connector.management.CertDiscoveryManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class certDiscoveryChartforCA extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(certDiscoveryChartforCA.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException {
        response.setContentType("application/json");
        log.info("Servlet started");
        PrintWriter out = response.getWriter();

        ApCertDiscovery certdetails = new ApCertDiscovery();
        CertDiscoveryManagement cMgnt = new CertDiscoveryManagement();
        // ApCarddetails cardDetails = new ApCarddetails();
        HashMap<String, Integer> mapCN = new HashMap();

        ApCertDiscovery[] apCertDiscovery = cMgnt.getCertDiscoveryDetails();
        for (int j = 0; j < apCertDiscovery.length; j++) {
            String IssuerDetails = apCertDiscovery[j].getSubject();
            JSONObject CAtdetail = new JSONObject(IssuerDetails);

            String Issuer = (String) CAtdetail.get("_Issuer");
            String[] list = null;
            if (Issuer != null) {
                if(Issuer.isEmpty() == false){
                list = Issuer.split(",");
            }
            }
            HashMap map = new HashMap();
            if (list != null) {
                for (String str : list) {
                    String[] values = str.split("=");
                    try{
                    map.put(values[0], values[1]);
                    } catch(Exception ex) {
         System.out.println("Catching the ArrayIndexOutOfBoundsException   "+ex);
      }

                }

                String CN = (String) map.get("CN");
                 String[] result = CN.split(" ", 2);
                   String first = result[0];
                if (mapCN.get(first) != null) {
                    mapCN.put(first, mapCN.get(first) + 1);
                } else {
                    mapCN.put(first, 1);
                }
            }
        }
        
        
        //added for random color
         Random rand = new Random();
 
    
            ArrayList<float_donut> sample = new ArrayList<float_donut>();
           String[] color = new String[] {"#005CDE", "#00A36A","#7D0096","#992B00","#DE000F","#ED7B00"};
           // int i = 0;
          
            for (String key : mapCN.keySet()) {
                
                
                 
                  
                int redValue = rand.nextInt(255);
    int greenValue = rand.nextInt(255);
    int blueValue = rand.nextInt(255);
     String hex = String.format("#%02x%02x%02x",  redValue, greenValue, blueValue);
                sample.add(new float_donut(key, mapCN.get(key),hex));
              //  i++;
            }
            
        
            JSONObject mapCNJson = new JSONObject(mapCN);
            Gson gson = new Gson();

            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<bar>>() {
            }.getType());

            JsonArray jsonArray = element.getAsJsonArray();

            out.print(jsonArray);

        
            log.info("Servlet ended");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            log.error("Exception caught :: ",ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            log.error("Exception caught :: ",ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
