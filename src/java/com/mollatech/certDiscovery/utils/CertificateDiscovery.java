/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.certDiscovery.utils;

import com.mollatech.axiom.nucleus.db.ApCertDiscovery;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.CertDiscoveryManagement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class CertificateDiscovery {

    final String itemType = "CERTIFICATE DISCOVERY";
    final String itemTypeCert = "CERTIFICATE DISCOVERY";

    public int getcertDetails(List<String> ips, List<String> ports, String archiveId, String sessionId, Channels channel, Operators Operators, String remoteAddress, String remoteAddressLogin, String userid) throws Exception {

        HashMap<String, CertDetails> map = InstallCert.getCertDetails(ips, ports);

        AuditManagement audit = new AuditManagement();
        int i = 0;
        int retValue = -1;
        Set<String> keys = map.keySet();
        for (String key : keys) {
            i++;
            CertDetails certDetail = map.get(key);
            SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm ");
            String _Ip = certDetail.getIP();
            String _Port = certDetail.getPort();
            String _Subject = certDetail.getSubject().getName();
            String _Issuer = certDetail.getIssuer().getName();
            String _SHA1 = certDetail.getSha1();
            String _MD5 = certDetail.getMd5();
            String _ExpiryDate = sdf.format(certDetail.getExpiry());
            String _IssuedDate = sdf.format(certDetail.getIssueDate());
            int _PublicKeyLenght = certDetail.getPublicKeyLength();
            String _Type = certDetail.getType();
            String _Serialnumber = certDetail.getSerialnumber().toString();
            String _Version = certDetail.getVersion().toString();
            String _SignAlgo = certDetail.getSignAlgo();
            sslvulnerabilty map1 = TestSSLServer.getSSLDetails(_Ip, _Port);
            String _BeastStatus = map1.getBest();
            String _crime = map1.getCrime();
            String _freak = map1.getFreak();
            String _csStrength = map1.getCsStrength();
            String _Protocalversion = map1.getProtocalversion();
            sslvulnerabilty map2 = HeartBleed.heartbleed(_Ip, _Port);
            String _Heartbleed = map2.getHeartbleed();

            JSONObject sslvulnerabiltdetails = new JSONObject();

            if (_BeastStatus != null) {
                if( _BeastStatus.isEmpty() == false){
                sslvulnerabiltdetails.put("_BeastStatus", _BeastStatus);
                } else {
                sslvulnerabiltdetails.put("_BeastStatus", "NA");
            }
            } else {
                sslvulnerabiltdetails.put("_BeastStatus", "NA");
            }
            if (_crime != null) {
                if(_crime.isEmpty()==false){
                sslvulnerabiltdetails.put("_crime", _crime);
                }
                else {
                sslvulnerabiltdetails.put("_crime", "NA");
            }
            } else {
                sslvulnerabiltdetails.put("_crime", "NA");
            }
            if (_Protocalversion != null) {
                if( _Protocalversion.isEmpty() == false){
                sslvulnerabiltdetails.put("_Protocalversion", _Protocalversion);
                } else {
                sslvulnerabiltdetails.put("_Protocalversion", "NA");
            }
            } else {
                sslvulnerabiltdetails.put("_Protocalversion", "NA");
            }
            if (_Heartbleed != null) {
                if(_Heartbleed.isEmpty() == false){
                sslvulnerabiltdetails.put("_Heartbleed", _Heartbleed);
                }else {
                sslvulnerabiltdetails.put("_Heartbleed", "NA");
            }
            } else {
                sslvulnerabiltdetails.put("_Heartbleed", "NA");
            }
            if (_freak != null) {
                if(_freak.isEmpty() == false){
                sslvulnerabiltdetails.put("_freak", _freak);
            } else {
                sslvulnerabiltdetails.put("_freak", "NA");
            }
                } else {
                sslvulnerabiltdetails.put("_freak", "NA");
            }
            if ( _csStrength != null) {
                if(_csStrength.isEmpty() == false){
                sslvulnerabiltdetails.put("_csStrength", _csStrength);
                } else {
                sslvulnerabiltdetails.put("_csStrength", "NA");
            }
            } else {
                sslvulnerabiltdetails.put("_csStrength", "NA");
            }
            String vulnerabiltdetails = sslvulnerabiltdetails.toString();

            JSONObject detail = new JSONObject();
            try {
                detail.put("ip", _Ip);
                detail.put("_Port", _Port);
            } catch (Exception e) {
                e.printStackTrace();
            }
            String IPPortDetails = detail.toString();

            JSONObject detail1 = new JSONObject();
            try {
                detail1.put("_Port", _Port);
                detail1.put("ip", _Ip);

            } catch (Exception e) {
                e.printStackTrace();
            }
            String PortIpDetails = detail1.toString();

            JSONObject certdetail = new JSONObject();
           
                try {
//                    certdetail.put("_Subject", _Subject);
//                    certdetail.put("_SHA1", _SHA1);
//                    certdetail.put("_Issuer", _Issuer);
//                    certdetail.put("_IssuedDate", _IssuedDate);
//                    certdetail.put("_MD5", _MD5);
//                    certdetail.put("_Type", _Type);
//                    certdetail.put("_ExpiryDate", _ExpiryDate);
//                    certdetail.put("_Version", _Version);
//                    certdetail.put("_SignAlgo", _SignAlgo);
//                    certdetail.put("_PublicKeyLenght", _PublicKeyLenght);
      if(_Subject!=null){
                certdetail.put("_Subject", _Subject);
                }else{
                    certdetail.put("_Subject", "NA");
                }
                if(_SHA1!=null){
                certdetail.put("_SHA1", _SHA1);
                }else{
                     certdetail.put("_SHA1", "NA");
                }
                if(_Issuer!=null){
                certdetail.put("_Issuer", _Issuer);
                }else{
                    certdetail.put("_Issuer", "NA");
                } if(_IssuedDate!=null){
                certdetail.put("_IssuedDate", _IssuedDate);
                }else{
                      certdetail.put("_IssuedDate", "NA");
                } if(_MD5!=null){
                certdetail.put("_MD5", _MD5);
                }else{
                     certdetail.put("_MD5", "NA");
                } if(_Type!=null){
                certdetail.put("_Type", _Type);
                }else{
                                    certdetail.put("_Type", "NA");

                } if(_ExpiryDate!=null){
                certdetail.put("_ExpiryDate", _ExpiryDate);
                }else{
                                    certdetail.put("_ExpiryDate", "NA");

                }
                if(_Version!=null){
                certdetail.put("_Version", _Version);
                }else{
                                    certdetail.put("_Version", "NA");

                }
                if(_SignAlgo!=null ){
                certdetail.put("_SignAlgo", _SignAlgo);
                }else{
                                    certdetail.put("_SignAlgo", "NA");

                }
                if(_PublicKeyLenght!= 0){
                certdetail.put("_PublicKeyLenght", _PublicKeyLenght);
                }else{
                                    certdetail.put("_PublicKeyLenght", 0);

                }

                } catch (Exception e) {
                    e.printStackTrace();
                }
           
            String CertResult = certdetail.toString();

            Date expirydatetime = new Date(_ExpiryDate);
            String CID = archiveId;

            JSONObject certDiscovery = new JSONObject();
            try {
                certDiscovery.put("detail", detail);
                certDiscovery.put("certdetail", certdetail);
            } catch (Exception e) {
                e.printStackTrace();
            }
            CertDiscoveryManagement cMgnt = new CertDiscoveryManagement();
            ApCertDiscovery[] apCertDiscovery = cMgnt.getCertDiscoveryDetails();
            int l = apCertDiscovery.length;
            boolean alreadyPresentIpFound = false;
            String PresentCID = null;
            for (int j = 0; j < l; j++) {

                if (apCertDiscovery[j].getPortIpDetails().equals(PortIpDetails) || apCertDiscovery[j].getPortIpDetails().equals(IPPortDetails)) {
                    PresentCID = apCertDiscovery[j].getCid();
                    alreadyPresentIpFound = true;
                    break;
                }
            }
            if (alreadyPresentIpFound) {
                retValue = cMgnt.updateCertscoveryDetails(CertResult, IPPortDetails, PortIpDetails, expirydatetime, vulnerabiltdetails, PresentCID);
            } else {
                retValue = cMgnt.addCertificatesDiscovery(CertResult, IPPortDetails, expirydatetime, vulnerabiltdetails, CID);
            }
        }

        if (retValue == 0) {
            String resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), Operators.getOperatorid(),
                    remoteAddress, channel.getName(),
                    remoteAddressLogin, Operators.getName(), new Date(),
                    "Add CertificateDetails",
                    resultString, retValue,
                    itemType, "", "CertificateDetails Added Successfully..!!",
                    "Certificate Discovery",
                    remoteAddressLogin);

        } else if (retValue == 3) {
            String resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), Operators.getOperatorid(),
                    remoteAddress, channel.getName(),
                    remoteAddressLogin, Operators.getName(), new Date(),
                    "Update CertificateDetails",
                    resultString, retValue,
                    itemType, "", "CertificateDetails Updated Successfully..!!",
                    "Certificate Discovery",
                    remoteAddressLogin);

        } else {
            String resultString = "FAILED";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), Operators.getOperatorid(),
                    remoteAddress, channel.getName(),
                    remoteAddressLogin, Operators.getName(), new Date(),
                    "Add CertificateDetails",
                    resultString, retValue,
                    itemType, "", "CertificateDetails Add/Update Failed..!!",
                    "Certificate Discovery",
                    remoteAddressLogin);
        }

        return retValue;
    }
}
