/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.certDiscovery.utils;

import java.math.BigInteger;
import java.security.Principal;
import java.util.Date;

/**
 *
 * @author bluebricks
 */
public class CertDetails {
    private String IP;
    private String Port;
    private Principal Subject;
    private Principal Issuer;
    private String sha1;
    private String md5;
    private Date Expiry;
    private Date IssueDate;
    private String Type;
    private BigInteger Serialnumber;
    private Integer Version;
    private String SignAlgo;
    private Integer PublicKeyLength;
  
    
    public CertDetails(){}

    public CertDetails(Integer Version,BigInteger Serialnumber,String IP, String Port, Principal Subject, Principal Issuer, String sha1, String md5, Date Expiry, Date IssueDate, String Type,String SignAlgo,Integer PublicKeyLength) {
        this.IP = IP;
        this.Port = Port;
        this.Subject = Subject;
        this.Issuer = Issuer;
        this.sha1 = sha1;
        this.md5 = md5;
        this.Expiry = Expiry;
        this.IssueDate = IssueDate;
        this.Type = Type;
        this.Serialnumber = Serialnumber;
        this.Version = Version;
        this.SignAlgo = SignAlgo;
        this.PublicKeyLength = PublicKeyLength;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public String getPort() {
        return Port;
    }

    public void setPort(String Port) {
        this.Port = Port;
    }

    public Principal getSubject() {
        return Subject;
    }

    public void setSubject(Principal Subject) {
        this.Subject = Subject;
    }

    public Principal getIssuer() {
        return Issuer;
    }

    public void setIssuer(Principal Issuer) {
        this.Issuer = Issuer;
    }

    public String getSha1() {
        return sha1;
    }

    public void setSha1(String sha1) {
        this.sha1 = sha1;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public Date getExpiry() {
        return Expiry;
    }

    public void setExpiry(Date Expiry) {
        this.Expiry = Expiry;
    }

    public Date getIssueDate() {
        return IssueDate;
    }

    public void setIssueDate(Date IssueDate) {
        this.IssueDate = IssueDate;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public BigInteger getSerialnumber() {
        return Serialnumber;
    }

    public void setSerialnumber(BigInteger Serialnumber) {
        this.Serialnumber = Serialnumber;
    }

    public Integer getVersion() {
        return Version;
    }

    public void setVersion(Integer Version) {
        this.Version = Version;
    }
    
    public String getSignAlgo(){
        return SignAlgo;
    }
    public void setSignAlgo(String SignAlgo){
        this.SignAlgo = SignAlgo;
    }
     public Integer getPublicKeyLength(){
        return PublicKeyLength;
    }
    public void setPublicKeyLength(Integer PublicKeyLength){
        this.PublicKeyLength = PublicKeyLength;
    }
    
    
}
