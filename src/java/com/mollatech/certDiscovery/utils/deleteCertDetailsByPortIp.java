/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.certDiscovery.utils;

import com.mollatech.axiom.nucleus.db.ApCertDiscovery;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.CertDiscoveryManagement;
import static com.mollatech.certDiscovery.utils.addcertDiscoveryDetails.log;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class deleteCertDetailsByPortIp extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
     final String itemType = "CERTIFICATE DISCOVERY";
    final String itemTypeCert = "CERTIFICATE DISCOVERY";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                  response.setContentType("application/json");
        log.info("Servlet started");
        
        String ip = request.getParameter("PortIpDetails");
        log.debug("PortIpDetails :: "+ip);
        String port = request.getParameter("IPPortDetails");
        log.debug("IPPortDetails :: "+port);
          Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
           Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());
         String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        String remoteAddress = request.getRemoteAddr();
         AuditManagement audit = new AuditManagement();
             PrintWriter out = response.getWriter();
             String PortIpDetails=null;
        JSONObject json = new JSONObject();
                JSONObject detail1 = new JSONObject();
            try {
                detail1.put("_Port", port);
                detail1.put("ip", ip);
                
            } catch (Exception e) {
              
            }
             PortIpDetails = detail1.toString();
        String result = null;
        String message = null;

            CertDiscoveryManagement cMgnt = new CertDiscoveryManagement();
            //AuditManagement audit = new AuditManagement();
            // ApCarddetails oldDetails = cMgnt.getCardDetailsByUserId(sessionId, channel.getChannelid(), _userCard);

            ApCertDiscovery certdetails = new ApCertDiscovery();

            // ApCarddetails cardDetails = new ApCarddetails();
            ApCertDiscovery[] apCertDiscovery = cMgnt.getCertDiscoveryDetails();
            int l=apCertDiscovery.length;
      
             int retValue = -1;
            
           
          
                retValue = cMgnt.deleteCertDiscoveryDetailsByPortIP(PortIpDetails, PortIpDetails);
                log.debug("deleteCertDetailsByPortIp :: "+retValue);
            
       
        

        //        retValue = pMgnt.EditPassportDetails(sessionId,channel.getChannelid(),_userId,passport_NamePass,passport_Passportnumber, passport_Sex,passport_Nationality,passport_BirthDate, passport_Issueplace, passport_Expirydate);
        //AuthUser olduser = uMngt.getUser(sessionId, channel.getChannelid(), _userid);
        //retValue = uMngt.EditUserDetails(sessionId,channel.getChannelid(),_userid, _user_name, _user__OrganisationD,_user__OrganisationUnitD, _user_country, _user_location,_user_street, _user_designation);
        String resultString = "Failure";
        if (retValue == 0) {
            resultString = "Success";
            message = "Certificate  Deleted Successfully!!";
           
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    remoteAddress, channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(),
                    "Delete CertificateDetails",
                    resultString, retValue,
                    itemType,"",message,
                    "Certificate Discovery",
                    remoteaccesslogin);
        
        
 
        } else {
            result = "error";
            message = "failed to delete Certificate!!";
             audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    remoteAddress, channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(),
                    "Delete CertificateDetails",
                    resultString, retValue,
                    itemType,"",message,
                    "Certificate Discovery",
                    remoteaccesslogin);

        }

        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
            return;
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
