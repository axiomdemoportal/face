/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.certDiscovery.utils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.face.common.HBar_chart;
import com.mollatech.axiom.face.common.bar;
import com.mollatech.axiom.face.common.float_donut;
import com.mollatech.axiom.nucleus.db.ApCertDiscovery;
import com.mollatech.axiom.nucleus.db.connector.management.CertDiscoveryManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class SSLEndpointNotice extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SSLEndpointNotice.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException {
        log.info("Servlet started");
          response.setContentType("application/json");
        PrintWriter out = response.getWriter();
int cntBest=0;
int cntCrime=0;
int cntHeartbleed=0;
int cntSSLEnable=0;
int cntSSLEnable2=0;
int cntFreak=0;
int cntcsStrength=0;
        ApCertDiscovery certdetails = new ApCertDiscovery();
        CertDiscoveryManagement cMgnt = new CertDiscoveryManagement();
        // ApCarddetails cardDetails = new ApCarddetails();
        HashMap<String, Integer> mapCN = new HashMap();

        ApCertDiscovery[] apCertDiscovery = cMgnt.getCertDiscoveryDetails();
        for (int j = 0; j < apCertDiscovery.length; j++) {
           
            String SSLVulnerabulityDetails=apCertDiscovery[j].getSslvulnerability();
            
            String IssuerDetails = apCertDiscovery[j].getSubject();
            JSONObject CAtdetail = new JSONObject(IssuerDetails);
            JSONObject SvDetails = new JSONObject(SSLVulnerabulityDetails);
            
            
            String Best = (String) SvDetails.get("_BeastStatus");
            if(Best!=null){
            if(Best.equals("vulnerable")){
                cntBest++;
            }
             
            }
            
             String Freak = (String) SvDetails.get("_freak");
              if(Freak!=null){
            if(Freak.equals("vulnerable")){
                cntFreak++;
            }
             
            }
            String Crime = (String) SvDetails.get("_crime");
            if(Crime!=null){
            if(Crime.equals("vulnerable")){
                cntCrime++;
               
            }
            }
                        String HeartBleed = (String) SvDetails.get("_Heartbleed");
            if(HeartBleed!=null){
            if(HeartBleed.equals("vulnerable")){
                cntHeartbleed++;
               
                
            }
            }
                       String protocalVersion = (String) SvDetails.get("_Protocalversion");
                       
            if(protocalVersion!=null){
            if(protocalVersion.equals("SSLv3.0")){
                cntSSLEnable++;
               
                
            }
            
            if(protocalVersion.equals("SSLv2.0")){
                cntSSLEnable2++;
                
            }
            }
            
            
            
            String csStrength = (String) SvDetails.get("_csStrength");
            if(csStrength!=null){
            if(csStrength.equals("weak encryption (40-bit)")){
                cntcsStrength++;
            }}
            
            

//            String Issuer = (String) CAtdetail.get("_Issuer");
//            String[] list = null;
//            if (Issuer != null && !Issuer.isEmpty()) {
//                list = Issuer.split(",");
//            }
//            HashMap map = new HashMap();
//            if (list != null) {
//                for (String str : list) {
//                    String[] values = str.split("=");
//                    map.put(values[0], values[1]);
//
//                }
//
//                String CN = (String) map.get("CN");
//                if (mapCN.get(CN) != null) {
//                    mapCN.put(CN, mapCN.get(CN) + 1);
//                } else {
//                    mapCN.put(CN, 1);
//                }
//            }
        }
       
        
       ArrayList<HBar_chart> sample = new ArrayList<HBar_chart>();
        sample.add(new HBar_chart(cntCrime,Integer.toString(cntCrime),"FREAK Attack"));
        sample.add(new HBar_chart(cntHeartbleed,Integer.toString(cntHeartbleed),"Heartbleed"));
       
           // sample.add(new HBar_chart(cntCrime,cntCrime,"Crime Status"));
            sample.add(new HBar_chart(cntSSLEnable, Integer.toString(cntSSLEnable), "SSLv3.0 Protocal Enable"));
            sample.add(new HBar_chart(cntSSLEnable2, Integer.toString(cntSSLEnable2), "SSLv2.0 Protocal Enable"));
            sample.add(new HBar_chart(cntcsStrength, Integer.toString(cntcsStrength),"Weak Cipher Suites"));
            sample.add(new HBar_chart(cntBest,Integer.toString(cntBest),"BEAST"));

 
    
//            ArrayList<float_donut> sample = new ArrayList<float_donut>();
//           String[] color = new String[] {"#005CDE", "#00A36A","#7D0096","#992B00","#DE000F","#ED7B00"};
           // int i = 0;
          
//            for (String key : mapCN.keySet()) {
//                int redValue = rand.nextInt(255);
//    int greenValue = rand.nextInt(255);
//    int blueValue = rand.nextInt(255);
//     String hex = String.format("#%02x%02x%02x",  redValue, greenValue, blueValue);
//                sample.add(new float_donut(key, mapCN.get(key),hex));
//              //  i++;
//            }
            
//        
//            JSONObject mapCNJson = new JSONObject(mapCN);
            Gson gson = new Gson();

            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<HBar_chart>>() {
            }.getType());

            JsonArray jsonArray = element.getAsJsonArray();

            out.print(jsonArray);

        log.info("Servlet ended");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            log.error("Exception caught :: ",ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            log.error("Exception caught :: ",ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
