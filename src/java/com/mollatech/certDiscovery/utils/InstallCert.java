package com.mollatech.certDiscovery.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;

import java.io.InputStream;
import java.io.InputStreamReader;

import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.interfaces.DSAPublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.interfaces.ECPublicKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.net.ssl.SSLContext;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
//import sun.security.ec.ECPublicKeyImpl;

public class InstallCert {

    public static void main(final String[] args) throws Exception {

        List ips = IPAddress.getIPAddresses("192.168.0");//new ArrayList();//
//        ips.add("imap.gmail.com");
        List port = new ArrayList();
        port.add("8443");
        HashMap<String, CertDetails> map = getCertDetails(ips, port);

        Set<String> keys = map.keySet();
        for (String key : keys) {
            CertDetails certDetail = map.get(key);
            System.out.println(certDetail.getSubject());
            System.out.println(certDetail.getExpiry());
            System.out.println(certDetail.getMd5());
            System.out.println(certDetail.getSha1());

        }
    }

    public static HashMap<String, CertDetails> getCertDetails(List<String> ipAddress, List<String> Port) throws Exception {
        HashMap<String, CertDetails> certDetail = new HashMap<>();
        for (String ip : ipAddress) {
            for (String port : Port) {
                CertDetails details = getCertDetails(ip, port);
                if (details != null) {
                    certDetail.put(ip + ":" + port, getCertDetails(ip, port));
                }

            }
        }

        return certDetail;
    }

    public static CertDetails getCertDetails(String ipAddress, String portNumber) throws Exception {

        CertDetails CD = null;
        String host;
        int port = 0;
        char[] passphrase;

        host = ipAddress;
        try{
        port = Integer.parseInt(portNumber);
        
         }
    catch (NumberFormatException e) {
        System.out.println("Invalid port" +e);
    }
        final String p = "changeit";
        passphrase = p.toCharArray();

        File file = new File("jssecacerts");
        if (file.isFile() == false) {
            final char SEP = File.separatorChar;
            final File dir = new File(System.getProperty("java.home")
                    + SEP + "lib" + SEP + "security");
            file = new File(dir, "jssecacerts");
            if (file.isFile() == false) {
                file = new File(dir, "cacerts");
            }
        }

        System.out.println("Loading KeyStore " + file + "...");
        final InputStream in = new FileInputStream(file);
        final KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
        ks.load(in, passphrase);
        in.close();

        final SSLContext context = SSLContext.getInstance("TLS");
        final TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(ks);
        final X509TrustManager defaultTrustManager = (X509TrustManager) tmf.getTrustManagers()[0];
        final SavingTrustManager tm = new SavingTrustManager(defaultTrustManager);
        context.init(null, new TrustManager[]{tm}, null);
        final SSLSocketFactory factory = context.getSocketFactory();
       
        try {
            System.out.println("Opening connection to " + host + ":" + port + "...");
            try (SSLSocket socket = (SSLSocket) factory.createSocket(host, port)) {
                socket.setSoTimeout(10000);
                
                System.out.println("Starting SSL handshake...");
                socket.startHandshake();
              //   socket.close();
            }catch(Exception e){
                System.out.println("Failed to create socket::" +e);
            }
            System.out.println();
            System.out.println("No errors, certificate is already trusted");
            
        } catch (Exception e) {
//            System.out.println();
//            e.printStackTrace(System.out);
//            return null;
        }

        final X509Certificate[] chain = tm.chain;
        if (chain == null) {
            System.out.println("Could not obtain server certificate chain");
            return null;
        }
        final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println();
        System.out.println("Server sent " + chain.length + " certificate(s):");
        System.out.println();
        final MessageDigest sha1 = MessageDigest.getInstance("SHA1");
        final MessageDigest md5 = MessageDigest.getInstance("MD5");
        int length = -1;
        for (int i = 0; i < 1; i++) {
            final X509Certificate cert = chain[0];
            if (cert != null) {
                CD = new CertDetails();
                CD.setIP(ipAddress);
                CD.setPort(portNumber);
                CD.setSubject(cert.getSubjectDN());
                CD.setIssuer(cert.getIssuerDN());
                sha1.update(cert.getEncoded());
                CD.setSha1(toHexString(sha1.digest()));
                md5.update(cert.getEncoded());
                CD.setMd5(toHexString(md5.digest()));
                CD.setVersion(cert.getVersion());
                CD.setSerialnumber(cert.getSerialNumber());
                CD.setIssueDate(cert.getNotBefore());
                CD.setExpiry(cert.getNotAfter());
                CD.setType(cert.getType());
                CD.setSignAlgo(cert.getSigAlgName());
                if (cert.getPublicKey() instanceof RSAPublicKey) {
                    RSAPublicKey r = (RSAPublicKey) cert.getPublicKey();

                    CD.setPublicKeyLength(r.getModulus().bitLength());
                } else if (cert.getPublicKey() instanceof ECPublicKey) {
                   // ECPublicKeyImpl e = (ECPublicKeyImpl) cert.getPublicKey();

                    //length = e.getParams().getOrder().bitLength();
                    //CD.setPublicKeyLength(length);

                } else if (cert.getPublicKey() instanceof DSAPublicKey) {
                    length = ((DSAPublicKey) cert.getPublicKey()).getParams().getP().bitLength();
                    CD.setPublicKeyLength(length);
                }

            }
        }

        return CD;
    }

    private static final char[] HEXDIGITS = "0123456789abcdef".toCharArray();

    private static String toHexString(final byte[] bytes) {
        final StringBuilder sb = new StringBuilder(bytes.length * 3);
        for (int b : bytes) {
            b &= 0xff;
            sb.append(HEXDIGITS[b >> 4]);
            sb.append(HEXDIGITS[b & 15]);
            sb.append(' ');
        }
        return sb.toString();
    }

    private static class SavingTrustManager implements X509TrustManager {

        private final X509TrustManager tm;
        private X509Certificate[] chain;

        SavingTrustManager(final X509TrustManager tm) {
            this.tm = tm;
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
            // throw new UnsupportedOperationException();
        }

        @Override
        public void checkClientTrusted(final X509Certificate[] chain,
                final String authType)
                throws CertificateException {
            throw new UnsupportedOperationException();
        }

        @Override
        public void checkServerTrusted(final X509Certificate[] chain,
                final String authType)
                throws CertificateException {
            this.chain = chain;
            this.tm.checkServerTrusted(chain, authType);
        }
    }
}
