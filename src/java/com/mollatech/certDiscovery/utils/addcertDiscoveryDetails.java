/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.certDiscovery.utils;

import com.mollatech.axiom.nucleus.db.ApCertDiscovery;

import com.mollatech.axiom.nucleus.db.connector.management.CertDiscoveryManagement;
import static com.mollatech.certDiscovery.utils.GenerateAcno.generateAckno;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class addcertDiscoveryDetails extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(addcertDiscoveryDetails.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("application/json");
        log.info("Servlet started");
        String ips = request.getParameter("Ips1");
        log.debug("ips :: "+ips);
        String ports = request.getParameter("Ports1");
        log.debug("ports :: "+ports);
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String result = null;
        String message = null;
        List ip = new ArrayList();
        if (ips.contains("/")) {

            String[] str = ips.split("/");
            List<String> list = IPAddress.getIPAddresses(str[0]);
            ip.addAll(list);
        } else {
            ip.add(ips);
        }

        //
        List port = new ArrayList();
        if (ports.contains(",")) {

            String[] str = ports.split("\\s*,\\s*");

            List<String> items = Arrays.asList(str);
            port.addAll(items);
        } else {
            port.add(ports);
        }
        HashMap<String, CertDetails> map = InstallCert.getCertDetails(ip, port);

        int i = 0;
        int retValue = -1;
        Set<String> keys = map.keySet();
        for (String key : keys) {
            i++;
            CertDetails certDetail = map.get(key);
            SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm ");

            String _Ip = certDetail.getIP();
            String _Port = certDetail.getPort();
           
            String _Subject = certDetail.getSubject().getName();
            String _Issuer = certDetail.getIssuer().getName();
            String _SHA1 = certDetail.getSha1();
            String _MD5 = certDetail.getMd5();
            String _ExpiryDate = sdf.format(certDetail.getExpiry());
            String _IssuedDate = sdf.format(certDetail.getIssueDate());
            int _PublicKeyLenght = certDetail.getPublicKeyLength();
                    
            
            String _Type = certDetail.getType();
            String _Serialnumber = certDetail.getSerialnumber().toString();
            String _Version = certDetail.getVersion().toString();
            String _SignAlgo = certDetail.getSignAlgo();
            sslvulnerabilty map1=TestSSLServer.getSSLDetails(_Ip, _Port);
            String _BeastStatus= map1.getBest();
            String _crime=map1.getCrime();
            String _freak = map1.getFreak();
            String _csStrength = map1.getCsStrength();
            
            
            
            String _Protocalversion=map1.getProtocalversion();
            sslvulnerabilty map2=HeartBleed.heartbleed(_Ip, _Port);
            String _Heartbleed = map2.getHeartbleed();
            
            JSONObject sslvulnerabiltdetails = new JSONObject();
            try {
                sslvulnerabiltdetails.put("_BeastStatus", _BeastStatus);
                sslvulnerabiltdetails.put("_crime", _crime);
                sslvulnerabiltdetails.put("_Protocalversion", _Protocalversion);
                sslvulnerabiltdetails.put("_Heartbleed", _Heartbleed);
                sslvulnerabiltdetails.put("_freak", _freak);
                sslvulnerabiltdetails.put("_csStrength", _csStrength);
            } catch (Exception e) {
               log.error("Exception caught :: ",e);
            }
            String vulnerabiltdetails = sslvulnerabiltdetails.toString();
            
            
                    
            JSONObject detail = new JSONObject();
            try {
                detail.put("ip", _Ip);
                detail.put("_Port", _Port);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            String IPPortDetails = detail.toString();
            
            
            JSONObject detail1 = new JSONObject();
            try {
                detail1.put("_Port", _Port);
                detail1.put("ip", _Ip);
                
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            String PortIpDetails = detail1.toString();

            JSONObject certdetail = new JSONObject();
            try {
                certdetail.put("_Subject", _Subject);
                certdetail.put("_SHA1", _SHA1);
                certdetail.put("_Issuer", _Issuer);
                certdetail.put("_IssuedDate", _IssuedDate);
                certdetail.put("_MD5", _MD5);
                certdetail.put("_Type", _Type);
                certdetail.put("_ExpiryDate", _ExpiryDate);
                certdetail.put("_Version", _Version);
                certdetail.put("_SignAlgo",_SignAlgo);
                certdetail.put("_PublicKeyLenght", _PublicKeyLenght);
                

            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            String CertResult = certdetail.toString();
          
            Date expirydatetime = new Date(_ExpiryDate);
            
            //added for auto generated number
           String CID=  GenerateAcno.generateAckno();

            JSONObject certDiscovery = new JSONObject();
            try {
                certDiscovery.put("detail", detail);
                certDiscovery.put("certdetail", certdetail);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }

            // String CertResult = certDiscovery.toString();
            //json.put(_Type, value)
            if (_Ip == null || _Ip.isEmpty() == true || _Ip == null || _Ip.isEmpty() == true) {
                result = "error";
                message = "Fill all Details!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();
                return;
            }

            CertDiscoveryManagement cMgnt = new CertDiscoveryManagement();
            //AuditManagement audit = new AuditManagement();
            // ApCarddetails oldDetails = cMgnt.getCardDetailsByUserId(sessionId, channel.getChannelid(), _userCard);

            ApCertDiscovery certdetails = new ApCertDiscovery();

            // ApCarddetails cardDetails = new ApCarddetails();
            ApCertDiscovery[] apCertDiscovery = cMgnt.getCertDiscoveryDetails();
            int l=apCertDiscovery.length;
            boolean alreadyPresentIpFound = false;     
            
            for(int j=0;j<l;j++){
               
            if ( apCertDiscovery[j].getPortIpDetails().equals(PortIpDetails) ||apCertDiscovery[j].getPortIpDetails().equals(IPPortDetails) ){                      
//            retValue = cMgnt.updateCertscoveryDetails(CertResult, IPPortDetails, PortIpDetails,expirydatetime);
//            break;
                alreadyPresentIpFound = true;
                break;
            }
//            else{            
//            retValue = cMgnt.addCertificatesDiscovery(CertResult, IPPortDetails,expirydatetime);
//          }
            }
            if(alreadyPresentIpFound){
                retValue = cMgnt.updateCertscoveryDetails(CertResult, IPPortDetails, PortIpDetails,expirydatetime,vulnerabiltdetails,CID);
                log.debug("updateCertscoveryDetails :: "+retValue);
            }else{
                 retValue = cMgnt.addCertificatesDiscovery(CertResult, IPPortDetails,expirydatetime,vulnerabiltdetails,CID);
                 log.debug("addCertificatesDiscovery :: "+retValue);
            }    
       
        

        }
//        retValue = pMgnt.EditPassportDetails(sessionId,channel.getChannelid(),_userId,passport_NamePass,passport_Passportnumber, passport_Sex,passport_Nationality,passport_BirthDate, passport_Issueplace, passport_Expirydate);
        //AuthUser olduser = uMngt.getUser(sessionId, channel.getChannelid(), _userid);
        //retValue = uMngt.EditUserDetails(sessionId,channel.getChannelid(),_userid, _user_name, _user__OrganisationD,_user__OrganisationUnitD, _user_country, _user_location,_user_street, _user_designation);
        String resultString = "Failure";
        if (retValue == 0 || retValue == 3) {
            resultString = "Success";
        }
         if (retValue == 3) {
            result = "success";

            message = "Certificate Details Updated Successfully!!";
 
        } else 
        if (retValue == 0) {
            result = "success";

            message = "Certificate Details added Successfully!!";
 
        } else {
            result = "error";
            message = "Certificate Details added failed!!";

        }

        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
            return;
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(addcertDiscoveryDetails.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(addcertDiscoveryDetails.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
