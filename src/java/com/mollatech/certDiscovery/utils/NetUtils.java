package com.mollatech.certDiscovery.utils;
import org.apache.commons.net.telnet.TelnetClient; 
import org.slf4j.Logger; 
import org.slf4j.LoggerFactory; 
 

public class NetUtils 
{ 
// private static Logger log = LoggerFactory.getLogger(NetUtils.class); 
 
 /**
  *  
  */ 
 public NetUtils() 
 { 
 } 
 
 /**
  *  
  * @param ip 
  * @param port 
  * @return 
  */ 
 public static boolean isConnectable(String ip, String port) 
 { 
  try 
  { 
   TelnetClient client = new TelnetClient(); 
   client.connect(ip, Integer.parseInt(port)); 
   return true; 
 
  } catch (Exception e) 
  { 
   return false; 
  } 
 
 } 
 
 /**
  * @param args 
  */ 
 public static void main(String[] args) 
 { 
  try 
  { 
   long start1 = System.currentTimeMillis(); 
   boolean isConnect = isConnectable("www.gmail.com", "25"); 
 
   long end1 = System.currentTimeMillis(); 
   System.out.println(isConnect + ":" + (end1 - start1) + ""); 
 
  } catch (Exception e) 
  { 
   e.printStackTrace(); 
      System.err.println("**********");
  } 
 } 
}

