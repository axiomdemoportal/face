/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.certDiscovery.utils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.face.common.HBar_chart;
import java.net.InetAddress;


import com.mollatech.axiom.nucleus.db.ApCertDiscovery;
import com.mollatech.axiom.nucleus.db.connector.management.CertDiscoveryManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class certificateNotice extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(certificateNotice.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException {
        log.info("Servlet started");
       response.setContentType("application/json");
        PrintWriter out = response.getWriter();

int cntSha1Violation=0;
int cntweakAlgo=0;
int cntCommanName=0;
int cntKeysize=0;

        ApCertDiscovery certdetails = new ApCertDiscovery();
        CertDiscoveryManagement cMgnt = new CertDiscoveryManagement();
        // ApCarddetails cardDetails = new ApCarddetails();
        HashMap<String, Integer> mapCN = new HashMap();

        ApCertDiscovery[] apCertDiscovery = cMgnt.getCertDiscoveryDetails();
        for (int j = 0; j < apCertDiscovery.length; j++) {
           
            String SSLVulnerabulityDetails=apCertDiscovery[j].getSslvulnerability();
            
            String IssuerDetails = apCertDiscovery[j].getSubject();
            JSONObject CAtdetail = new JSONObject(IssuerDetails);
            JSONObject SvDetails = new JSONObject(SSLVulnerabulityDetails);
            
         //   String keySize =(String) CAtdetail.get("_PublicKeyLenght");
            
            int k=CAtdetail.getInt("_PublicKeyLenght");
                    
         
            
            if(k<2048){
                cntKeysize++;
            }
            
            String signAlgo = (String) CAtdetail.get("_SignAlgo");
            if(signAlgo!=null){
            if(signAlgo.contains("SHA1")){
                cntSha1Violation++;
               
             
            }
           
            
            
            
            if(!signAlgo.contains("SHA2")){
                cntweakAlgo++;
               
             
            }}
            String Iptdetail = apCertDiscovery[j].getPortIpDetails();
            JSONObject Iptdetail1 = new JSONObject(Iptdetail);
            String ip=(String) Iptdetail1.get("ip");
            
            try{
            InetAddress host = InetAddress.getByName(ip);
            String Host = host.getHostName();
            }  catch(UnknownHostException e){
                   System.out.println("Unknown Host" +e);
                    }
          try{
            InetAddress addr1 = InetAddress.getByName(ip);
                  String h1=addr1.getHostName();
     }  catch(UnknownHostException e){
                   System.out.println("Unknown Host" +e);
                    }
            String Issuer = (String) CAtdetail.get("_Subject");
            String[] list = null;
            if (Issuer != null) {
                if(Issuer.isEmpty()==false){
                list = Issuer.split(",");
                }
            }
            HashMap map = new HashMap();
            String CN = null;
            if (list != null) {
                for (String str : list) {
                    if(str.contains("="))  {
                    String[] values = str.split("=");
                    map.put(values[0], values[1]);
                    }else{
                        
                        continue;
                    }
                }

                CN = (String) map.get("CN");
                try{
                 InetAddress host2 = InetAddress.getByName(ip);
                
            String Host2 = host2.getHostName();
            if(Host2!= null){
            if (!Host2.equals(CN)) {
                cntCommanName++;
            }
            }
            }  catch(UnknownHostException e){
                    System.out.println("Unknown Host" +e);
                    }
                if (mapCN.get(CN) != null) {
                    mapCN.put(CN, mapCN.get(CN) + 1);
                } else {
                    mapCN.put(CN, 1);
                }
            
            
            
           
            }
            

        }
        
       ArrayList<HBar_chart> sample1 = new ArrayList<HBar_chart>();
        

        
            sample1.add(new HBar_chart(cntSha1Violation,Integer.toString(cntSha1Violation), "SHA1 Violation"));
            sample1.add(new HBar_chart(cntweakAlgo, Integer.toString(cntweakAlgo), "Weak Hashing Algorithm"));
            sample1.add(new HBar_chart(cntCommanName, Integer.toString(cntCommanName), "Certificate Name Mismatch"));
            sample1.add(new HBar_chart(cntKeysize, Integer.toString(cntKeysize), "Weak Keys<2048"));
    
//            ArrayList<float_donut> sample = new ArrayList<float_donut>();
//           String[] color = new String[] {"#005CDE", "#00A36A","#7D0096","#992B00","#DE000F","#ED7B00"};
           // int i = 0;
          
//            for (String key : mapCN.keySet()) {
//                int redValue = rand.nextInt(255);
//    int greenValue = rand.nextInt(255);
//    int blueValue = rand.nextInt(255);
//     String hex = String.format("#%02x%02x%02x",  redValue, greenValue, blueValue);
//                sample.add(new float_donut(key, mapCN.get(key),hex));
//              //  i++;
//            }
            
//        
//            JSONObject mapCNJson = new JSONObject(mapCN);
            Gson gson = new Gson();

            JsonElement element = gson.toJsonTree(sample1, new TypeToken<List<HBar_chart>>() {
            }.getType());

            JsonArray jsonArray1 = element.getAsJsonArray();

            out.print(jsonArray1);

        

    log.info("Servlet ended");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            log.error("Exception caught :: ",ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(certificateNotice.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
