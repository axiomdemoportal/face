package com.mollatech.dictum.pushmessages;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Pushmessages;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PushMessageManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class editpushmessages extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editpushmessages.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final String itemtype = "PUSHMESSAGE";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+ channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+ sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+ remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+ operatorS.getName());
        String operatorId = operatorS.getOperatorid();
        log.debug("operatorId :: "+ operatorId);
        String _callerid = request.getParameter("_epush_idE");
        log.debug("_callerid :: "+ _callerid);
        String _callerIP = request.getParameter("_epush_ip");
        log.debug("_callerIP :: "+ _callerIP);
//        String _callerPort = request.getParameter("_epush_port");
        String _callerName = request.getParameter("_epush_name");
        log.debug("_callerName :: "+ _callerName);
//        String _EnforceSSL = request.getParameter("_push_enforce");
        String _EnforceRemoteAccess = "1";  //request.getParameter("_epush_enforceremoteaccess");
        String _Status = request.getParameter("_epush_status");
        log.debug("_Status :: "+ _Status);
        String _callerMessageSuccess = request.getParameter("_epush_success");
        log.debug("_callerMessageSuccess :: "+ _callerMessageSuccess);
        String _callerMessageFailure = request.getParameter("_epush_failure");
        log.debug("_callerMessageFailure :: "+ _callerMessageFailure);
        boolean _enforceSSL = false;
        boolean _enforceRemoteAccess = false;
        String result = "success";
        String message = "Caller Updated Successfully!!!";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
//        if (_EnforceSSL.equalsIgnoreCase("1")) {
//            _enforceSSL = true;// Boolean.parseBoolean(_EnforceSSL);
//        } else if (_EnforceSSL.equalsIgnoreCase("0")) {
//            _enforceSSL = false;
//
//        }
        if (_EnforceRemoteAccess.equalsIgnoreCase("1")) {
            _enforceRemoteAccess = true;
        } else if (_EnforceRemoteAccess.equalsIgnoreCase("0")) {
            _enforceRemoteAccess = false;
        }
        if (_callerIP == null || _callerIP.length() == 0 || _callerName == null || _callerName.length() == 0 || _Status == null || _callerMessageSuccess == null || _callerMessageSuccess.length() == 0 || _callerMessageFailure == null || _callerMessageFailure.length() == 0) {
            result = "error";
            message = "Invalid Caller Details!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

//        int _callerPortno = Integer.valueOf(_callerPort);
        int _callerID = Integer.valueOf(_callerid);
        log.debug("_callerID :: "+_callerID);
        int _status = Integer.valueOf(_Status);
         log.debug("_status :: "+_status);
        int retValue = -1;
        PushMessageManagement pObj = new PushMessageManagement();
        AuditManagement audit = new AuditManagement();

        Pushmessages checkip = null;
        Pushmessages checkport = null;

        checkip = pObj.CheckIP(sessionId, channel.getChannelid(), _callerIP);
//        checkport = pObj.CheckPort(sessionId, channel.getChannelid(), _callerPortno);
//        if (checkip != null && checkip.getCallerid() != _callerID || checkport != null && checkport.getCallerid() != _callerID) {
        if (checkip != null && checkip.getCallerid() != _callerID) {
            result = "error";
            message = "Duplicate IP/Port!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        Pushmessages checkname = null;
        checkname = pObj.CheckCaller(sessionId, channel.getChannelid(), _callerName);
        if (checkip != null && checkname.getCallerid() != _callerID) {
            result = "error";
            message = "Duplicate Name, please enter unique name!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught ::",e);
            }
            out.print(json);
            out.flush();
            return;

        }

        Pushmessages oldpObj = pObj.getPushmessage(sessionId, channel.getChannelid(), _callerID);
        retValue = pObj.EditPushmessage(sessionId, channel.getChannelid(), _callerID, _callerName, _callerIP, 3333, _enforceSSL, _enforceRemoteAccess, _status, _callerMessageSuccess, _callerMessageFailure);
        log.debug("retValue:: "+retValue);
        int istatus = oldpObj.getStatus();
        String strStatus = "Removed";
        if (istatus == 1) {
            strStatus = "Active";
        } else if (istatus == 0) {
            strStatus = "Suspended";
        } else if (istatus == -1) {
            strStatus = "Locked";
        }

        String strStatus1 = "Removed";
        if (_status == 1) {
            strStatus1 = "Active";
        } else if (_status == 0) {
            strStatus1 = "Suspended";
        } else if (_status == -1) {
            strStatus1 = "Locked";
        }

        String resultString = "ERROR";
        if (retValue == 0) {
            resultString = "SUCCESS";
        }

        if (retValue == 0) {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
                    operatorS.getName(), new Date(), "Edit Push Message", resultString, retValue, "Push Message Management",
                    "Caller IP =" + oldpObj.getCallerIp() + "Caller Name =" + oldpObj.getCallerName() + "Response Failure" + oldpObj.getResponcefailure()
                    + "Response Success =" + oldpObj.getResponsesuccess() + "Caller Port =" + oldpObj.getCallerPort()
                    + "Caller Id =" + oldpObj.getCallerid() + "Status =" + strStatus + "Enforce Remote Access ="
                    + (oldpObj.getEnforceRemoteAccess())
                    + "Enforce SSL ="
                    + (oldpObj.getEnforceSsl()),
                    "Caller IP =" + _callerIP + "Caller Name =" + _callerName + "Response Failure" + _callerMessageFailure
                    + "Response Success =" + _callerMessageSuccess + "Caller Port =" + 3333
                    + "Caller Id =" + _callerid + "Status =" + strStatus1 + "Enforce Remote Access =" + _enforceRemoteAccess
                    + "Enforce SSL =" + _enforceSSL, 
                    itemtype, 
                    ""+_callerID);
        } else if (retValue != 0) {

            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
                    operatorS.getName(), new Date(), "Edit Push Message", resultString, retValue, "Push Message Management",
                    "Caller IP =" + oldpObj.getCallerIp() + "Caller Name =" + oldpObj.getCallerName() + "Response Failure" + oldpObj.getResponcefailure()
                    + "Response Success =" + oldpObj.getResponsesuccess() + "Caller Port =" + oldpObj.getCallerPort()
                    + "Caller Id =" + oldpObj.getCallerid() + "Status =" + strStatus + "Enforce Remote Access ="
                    + (oldpObj.getEnforceRemoteAccess())
                    + "Enforce SSL ="
                    + (oldpObj.getEnforceSsl()),
                    "Failed To Edit Push Messages ...!!!", itemtype, ""+_callerID);

        }

        if (retValue == 0) {
            result = "success";
        } else {
            result = "error";
            message = "Caller Edit Failed!!!";
        }

        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
