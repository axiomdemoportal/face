package com.mollatech.dictum.pushmessages;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Pushmessagemappers;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PushMessageManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class addpushmapper extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(addpushmapper.class.getName());
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */final String itemtype = "PUSHMESSAGE";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String operatorId = operatorS.getOperatorid();
        log.debug("operatorId :: "+operatorId);
        String _Messageformat = request.getParameter("_Messageformat");
        log.debug("_Messageformat :: "+_Messageformat);
        String _templateName = request.getParameter("_templateName");
        log.debug("_templateName :: "+_templateName);
        String _templateType = request.getParameter("_templateType");
        log.debug("_templateType :: "+_templateType);
        String _callerID = request.getParameter("_CallerId");
        log.debug("_callerID :: "+_callerID);
        String _className = request.getParameter("_templateClass");
        log.debug("_className :: "+_className);
        String result = "success";
        String message = "Messages added successfully....";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        if (_Messageformat == null || _Messageformat.length() == 0 || _templateName == null || _templateName.length() == 0 || _templateType == null || _templateType.length() == 0) {
            result = "error";
            message = "Invalid Message Details!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        int _status = Integer.valueOf(_templateType);
        int _Cid = Integer.valueOf(_callerID);
        int retValue = -1;
        Templates tmp = new Templates();
        TemplateManagement tObj = new TemplateManagement();

        tmp = tObj.LoadbyName(sessionId, channel.getChannelid(), _templateName);
        

        if (tmp.getType() == 2 && (_status == 1 || _status == 2 || _status == 3) || tmp.getType() == 1 && _status == 4||tmp==null) {
            result = "error";
            message = "Template Type Don't Match.. !! Or Check For Template Name";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        PushMessageManagement pObj = new PushMessageManagement();
        AuditManagement audit = new AuditManagement();
                
        Pushmessagemappers checkbitmap=pObj.CheckBITMAP(sessionId, channel.getChannelid(),_Cid, _Messageformat);
        if(checkbitmap!=null)
        {
            result = "error";
            message = "ISOBITMAP Already Present..!!!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
            log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
       
         if(tmp.getStatus() == tObj.ACTIVE_STATUS){
                retValue = pObj.AddpushMappers(sessionId, channel.getChannelid(), _Cid, _Messageformat, tmp.getTemplateid(),_status, _className);
         }
        
             String resultString = "ERROR";
                   
            if(retValue == 0){
                resultString = "SUCCESS";
                  audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(),channel.getName(), remoteaccesslogin,
                          operatorS.getName(), new Date(), "Add Push Mapper", resultString, retValue, "Push Message",
                          "","Message Format = "+_Messageformat+"Caller Id = "+_callerID+"Status = "+_status
                          +"Class Name"+_className+"Template Id ="+tmp.getTemplateid(),
                           itemtype, ""+_Cid);
            }else if(retValue != 0){
                
                   audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(),channel.getName(), remoteaccesslogin,
                          operatorS.getName(), new Date(), "Add Push Mapper", resultString, retValue, "Push Message",
                          "","Failed To add Push Mapper",
                          itemtype, ""+_Cid);
               
            }         
         
        if (retValue == 0) {
            result = "success";
        } else {
            result = "error";
        }

        try {
            json.put("_result", result);
            json.put("_message", message);
        }catch(Exception e){
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
