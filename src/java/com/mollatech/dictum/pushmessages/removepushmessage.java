package com.mollatech.dictum.pushmessages;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Pushmessages;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PushMessageManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class removepushmessage extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(removepushmessage.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final String itemtype = "PUSHMESSAGE";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Deleted successfully!!!";
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getName());
        String operatorId = operatorS.getOperatorid();
        log.debug("operatorId :: "+operatorId);
        JSONObject json = new JSONObject();
        int retValue = -1;
        String _tid = request.getParameter("_callerid");
        log.debug("_tid :: "+_tid);
        int _callerid = Integer.valueOf(_tid);
        log.debug("_callerid :: "+_callerid);
        if (_tid == null) {
            result = "error";
            message = "Invalid PushDetails Details!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        PushMessageManagement pObj = new PushMessageManagement();
        AuditManagement audit = new AuditManagement();
        Pushmessages pOldObj = pObj.getPushmessage(sessionId, channel.getChannelid(), _callerid);

        retValue = pObj.deletepushmessage(sessionId, channel.getChannelid(), _callerid);
        log.debug("deletepushmessage :: "+retValue);
        String resultString = "ERROR";
        if (retValue == 0) {
            resultString = "SUCCESS";
        }

        int istatus = pOldObj.getStatus();
        String strStatus = "Removed";
        if (istatus == 1) {
            strStatus = "Active";
        } else if (istatus == 0) {
            strStatus = "Suspended";
        } else if (istatus == -1) {
            strStatus = "Locked";
        }

        if (retValue == 0) {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
                    operatorS.getName(), new Date(), 
                    "Remove Push Notification Message", resultString, retValue, "Push Message Management",
                    "Caller Ip =" + pOldObj.getCallerIp() 
                            + "Caller Name =" + pOldObj.getCallerName() + "Response Failure =" + pOldObj.getResponcefailure()
                    + "Response Success =" + pOldObj.getResponsesuccess() + "Caller Port =" + pOldObj.getCallerPort() + "Caller Id =" + pOldObj.getCallerid()
                    + "Status =" + strStatus + "Enforce Remote Access ="
                    + (pOldObj.getEnforceRemoteAccess() )
                    + "Enforce SSL ="
                    + (pOldObj.getEnforceSsl()),
                    "Push Message Removed", 
                    itemtype, 
                    "" + _callerid);

        } else if (retValue != 0) {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
                    operatorS.getName(), new Date(), 
                    "Remove Push Notification Message", resultString, retValue, 
                    "Push Message Management",
                    "Caller Ip =" + pOldObj.getCallerIp() + "Caller Name =" + pOldObj.getCallerName() + "Response Failure =" + pOldObj.getResponcefailure()
                    + "Response Success =" + pOldObj.getResponsesuccess() + "Caller Port =" + pOldObj.getCallerPort() + "Caller Id =" + pOldObj.getCallerid()
                    + "Status =" + strStatus + "Enforce Remote Access ="
                    + (pOldObj.getEnforceRemoteAccess())
                    + "Enforce SSL ="
                    + (pOldObj.getEnforceSsl()),
                    "Failed To Remove Push Message...!!", itemtype, 
                    "" + _callerid);

        }

        if (retValue == 0) {
            result = "success";
        } else {
            result = "error";
        }

        try {
            json.put("_result", result);
            json.put("_message", message);
        }catch(Exception e){
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
