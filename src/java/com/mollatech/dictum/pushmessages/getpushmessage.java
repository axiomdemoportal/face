package com.mollatech.dictum.pushmessages;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Pushmessages;
import com.mollatech.axiom.nucleus.db.connector.PushMessageUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class getpushmessage extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(getpushmessage.class.getName());
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        String _callid = request.getParameter("_callid");
        log.debug("_callid :: "+_callid);
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        int _call = Integer.valueOf(_callid);
        log.debug("_call :: "+_call);
        try {
//            PushMessageManagement pManagement = new PushMessageManagement();
//            Pushmessages pObj = pManagement.getPushmessage(sessionId, channel.getChannelid(), _call);
            
            SessionFactoryUtil supush = new SessionFactoryUtil(SessionFactoryUtil.pushmessages);
            Session pTem = supush.openSession();
            PushMessageUtils pU= new PushMessageUtils(supush, pTem);
            Pushmessages pObj = pU.getPushmessages(channel.getChannelid(), _call);
            supush.close();
            pTem.close();
            try {
            json.put("_callidE", pObj.getCallerid());
            json.put("_name", pObj.getCallerName());
            json.put("_ip", pObj.getCallerIp());
            json.put("_port", pObj.getCallerPort());
            json.put("_status", pObj.getStatus());
            json.put("_success", pObj.getResponsesuccess());
            json.put("_failure", pObj.getResponcefailure());
            json.put("_result", "success");
            }catch(Exception e){
                log.error("Exception caught :: ",e);
            }
            
            
            byte b =  pObj.getEnforceRemoteAccess();
          byte b1 = pObj.getEnforceRemoteAccess();
//            boolean b  = pObj.isEnforceRemoteAccess();
//           boolean n = b1!=0;
            if(b!=0 ==true)
            {
               json.put("_enforceremoteaccess",1);
            }
           
            else if(b!=0 ==false)
            {
                json.put("_enforceremoteaccess",0);
            
            }
            
            
//            if((pObj.isEnforceRemoteAccess()) ==true)
//            {
//               json.put("_enforceremoteaccess",1);
//            }
//            else if( (pObj.isEnforceRemoteAccess()) ==false)
//            {
//                json.put("_enforceremoteaccess",0);
//            
//            }
  

        } catch (Exception ex) {
            // TODO handle custom exceptions here
            log.error("Exception caught :: ",ex);
            try { json.put("_result", "error");
            json.put("_message", ex.getMessage());
            }catch(Exception e){
                log.error("Exception caught :: ",e);
            }
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
