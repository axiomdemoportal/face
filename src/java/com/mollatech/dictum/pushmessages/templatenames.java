package com.mollatech.dictum.pushmessages;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mollatech2
 */
public class templatenames extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(templatenames.class.getName());
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String searchText = request.getParameter("templatname");
        //System.out.println("in Text" + searchText);
        log.debug("searchText :: "+searchText);
        String[] strText = null;
        String result = "success";
        String message = "Documents Details";
        ArrayList<String> documentList = new ArrayList<String>();
        JsonArray jsonArray = new JsonArray();
        PrintWriter out = response.getWriter();
        int retValue = -1;
        try {
            Templates templates[] = null;
            TemplateManagement tObj = new TemplateManagement();
            templates = tObj.Listtemplates(sessionId, channel.getChannelid());
            // docs =  uuObj1.searchDocumentsByString(searchText);
//            docs =  uuObj1.GetAllDocuments();
            strText = new String[templates.length];
            for (int i = 0; i < templates.length; i++) {
                //System.out.println(templates[i]);
                strText[i] = templates[i].getTemplatename();
                //System.out.println(strText[i]);

            }
            Collections.addAll(documentList, strText);
            Gson gson = new Gson();
            JsonElement element = gson.toJsonTree(documentList, new TypeToken<List<String>>() {
            }.getType());
            jsonArray = element.getAsJsonArray();
            if (documentList != null) {
                retValue = 0;
            }

        } catch (Exception ex) {
            // TODO handle custom exceptions here
            log.error("Exception caught :: ",ex);
        }
        try {
        } finally {
            out.print(jsonArray);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
