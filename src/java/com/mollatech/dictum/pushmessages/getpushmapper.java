package com.mollatech.dictum.pushmessages;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Pushmessagemappers;
import com.mollatech.axiom.nucleus.db.Pushmessages;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.PushMessageUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.management.PushMessageManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class getpushmapper extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(getpushmapper.class.getName());
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        String _mapperid = request.getParameter("_mapperid");
        log.debug("_mapperid :: "+_mapperid);
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel);
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        int _mapper = Integer.valueOf(_mapperid);
        log.debug("_mapper :: "+_mapper);
        try {

            PushMessageManagement pObj = new PushMessageManagement();
            Pushmessagemappers Mapper = pObj.getPushmapper(sessionId, channel.getChannelid(), _mapper);
            
            Pushmessages pmcObj = pObj.getPushmessage(sessionId, channel.getChannelid(), Mapper.getCallerid());
            
            Templates tmp = new Templates();
            TemplateManagement tObj = new TemplateManagement();
            tmp = tObj.LoadTemplate(sessionId, channel.getChannelid(), Mapper.getTemplateid());

            json.put("_mapperID", Mapper.getMapperid());
            json.put("_messagefomat", Mapper.getMessageformat());
            json.put("_templateName", tmp.getTemplatename());
            json.put("_templateType", Mapper.getSendvia());
            json.put("_templateClass", Mapper.getClassname());
            json.put("_callerName", pmcObj.getCallerName());
            
            json.put("_result", "success");


        } catch (Exception ex) {
            // TODO handle custom exceptions here
            log.debug("Exception caught :: ",ex);
            try { json.put("_result", "error");
            json.put("_message", ex.getMessage());
            }catch(Exception e){log.error("Exception caught :: ",e);}
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
