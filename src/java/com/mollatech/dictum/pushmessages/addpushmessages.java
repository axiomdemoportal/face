package com.mollatech.dictum.pushmessages;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Pushmessages;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PushMessageManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class addpushmessages extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(addpushmessages.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final String itemtype = "PUSHMESSAGE";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.THIRD_PARTY_CALLERS) != 0) {
            String result = "error";
            String message = "This feature is not available in this license!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        String operatorId = operatorS.getOperatorid();
        log.debug("operatorId :: "+operatorId);
        String _callerIP = request.getParameter("_push_ip");
        log.debug("_callerIP :: "+_callerIP);
//        String _callerPort = request.getParameter("_push_port");
        String _callerName = request.getParameter("_callerName");
        log.debug("_callerName :: "+_callerName);
        String _EnforceRemoteAccess = "1;"; //request.getParameter("_push_enforceremoteaccess");
        String _Status = request.getParameter("_push_status");
        log.debug("_Status :: "+_Status);
        String _callerMessageSuccess = request.getParameter("_push_success");
        log.debug("_callerMessageSuccess :: "+_callerMessageSuccess);
        String _callerMessageFailure = request.getParameter("_push_failure");
        log.debug("_callerMessageFailure :: "+_callerMessageFailure);
        boolean _enforceSSL = false;
        boolean _enforceRemoteAccess = false;
        String result = "success";
        String message = "Caller added successfully....";

        if (_EnforceRemoteAccess.equalsIgnoreCase("1")) {
            _enforceRemoteAccess = true;
        } else if (_EnforceRemoteAccess.equalsIgnoreCase("0")) {
            _enforceRemoteAccess = false;
        }
        if (_callerIP == null || _callerIP.length() == 0 || _callerName == null || _callerName.length() == 0 || _Status == null || _callerMessageSuccess == null || _callerMessageSuccess.length() == 0 || _callerMessageFailure == null || _callerMessageFailure.length() == 0) {
            result = "error";
            message = "Invalid Message Details!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        int _status = Integer.valueOf(_Status);
        log.debug("_status :: "+_status);
        int retValue = -1;
        PushMessageManagement pObj = new PushMessageManagement();
        AuditManagement audit = new AuditManagement();

        Pushmessages checkip = null;
        Pushmessages checkport = null;

        checkip = pObj.CheckIP(sessionId, channel.getChannelid(), _callerIP);
        if (checkip != null) {
            result = "error";
            message = "Put unique port or Caller IP address....";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        Pushmessages checkname = null;
        checkname = pObj.CheckCaller(sessionId, channel.getChannelid(), _callerName);
        if (checkname != null) {
            result = "error";
            message = "Please specific unique name!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;

        }
        
        int iCurrentCountOfCaller=pObj.PushmessagesCount(channel.getChannelid());
        log.debug("PushmessagesCount :: "+iCurrentCountOfCaller);
        int iCountOFCallers = AxiomProtect.GetCallerCountAllowed();
        log.debug("GetCallerCountAllowed :: "+iCountOFCallers);
        
        if (iCountOFCallers < iCurrentCountOfCaller ) {
             result = "error";
             message = "No More Caller System can be added. Limit reached!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        
        

        retValue = pObj.AddPushMessages(sessionId, channel.getChannelid(), _callerIP, _callerName, 12245, false, _enforceRemoteAccess, _status, _callerMessageSuccess, _callerMessageFailure);

        String resultString = "ERROR";

        if (retValue == 0) {
            resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Add Push Message", resultString, retValue,
                    "Push Message Management", "",
                    "Caller Ip =" + _callerIP + "Caller Name =" + _callerName
                    + "Caller Port =" + 12345 + "Enforce SSL =" + false + "Enforce Remote Access =" + _enforceRemoteAccess
                    + "Status =" + _status + "Caller Message Success =" + _callerMessageSuccess + "Caller Message Failure =" + _callerMessageFailure,
                    itemtype, _callerName);
        } else if (retValue != 0) {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Add Push Message", resultString, retValue,
                    "Push Message Management", "", "Failed To Add Push Message...!!!", itemtype, _callerName);
        }

        if (retValue == 0) {
            result = "success";
        } else {
            result = "error";
        }

        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
