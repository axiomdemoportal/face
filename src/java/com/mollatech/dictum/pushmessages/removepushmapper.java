package com.mollatech.dictum.pushmessages;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Pushmessagemappers;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PushMessageManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class removepushmapper extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(removepushmapper.class.getName());
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */final String itemtype = "PUSHMESSAGE";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Deleted successfully!!!";
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
          String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
          log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getName());
        String operatorId = operatorS.getOperatorid();
        log.debug("operatorId :: "+operatorId);
        JSONObject json = new JSONObject();
        int retValue = -1;
        String _tid = request.getParameter("_mapperId");
        log.debug("_tid :: "+_tid);
        int _mapperid = Integer.valueOf(_tid);
        log.debug("_mapperid :: "+_mapperid);
        if (_tid == null) {
            result = "error";
            message = "Invalid Identifier!!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
                log.debug("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        PushMessageManagement pObj = new PushMessageManagement();
         AuditManagement audit = new AuditManagement();
         
           Pushmessagemappers oldPObj = pObj.getPushmapper(sessionId, channel.getChannelid(), _mapperid);
        retValue = pObj.deletepushmapper(sessionId, channel.getChannelid(), _mapperid);
        
        
          String resultString = "ERROR";
              
        if(retValue == 0){
            resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(),channel.getName(),
                    remoteaccesslogin, operatorS.getName(),new Date(), "Remove Push Mapper", resultString, retValue, 
                    "Push Message Management","Class Name ="+oldPObj.getClassname()+"Message Format"+oldPObj.getMessageformat()
                    +"Caller ID ="+oldPObj.getCallerid()+"Mapper Id ="+oldPObj.getMapperid()+"Send Via ="+oldPObj.getSendvia()
                    +"Template ID ="+oldPObj.getTemplateid(), "Push Mapper Removed Successfully", itemtype, operatorId);
        }
        else if(retValue != 0){
              audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(),channel.getName(),
                    remoteaccesslogin, operatorS.getName(),new Date(), "Remove Push Mapper", resultString, retValue, 
                    "Push Message Management","Class Name ="+oldPObj.getClassname()+"Message Format"+oldPObj.getMessageformat()
                    +"Caller ID ="+oldPObj.getCallerid()+"Mapper Id ="+oldPObj.getMapperid()+"Send Via ="+oldPObj.getSendvia()
                    +"Template ID ="+oldPObj.getTemplateid(), 
                    "Failed To remove Push Mapper ...!!!",itemtype, operatorId);
        }
        
        
        
        if (retValue == 0) {
            result = "success";
        } else {
            result = "error";
            message = "FX Message Removal Failed!!!";
        }

        try {
            json.put("_result", result);
            json.put("_message", message);
        }catch(Exception e){
            log.debug("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
