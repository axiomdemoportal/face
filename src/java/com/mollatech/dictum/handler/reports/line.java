/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.handler.reports;

import java.util.Date;

/**
 *
 * @author mollatech1
 */
public class line {
    
    String date;
    int value;
    int value1;
    int value2;

    public line(String d, int value, int value1, int value2) {
        this.date = d;
        this.value = value;
        this.value1 = value1;
        this.value2 = value2;
    }

    @Override
    public String toString() {
        return "line{" + "d=" + date + ", value=" + value + ", value1=" + value1 + ", value2=" + value2 + '}';
    }
   
    
}
