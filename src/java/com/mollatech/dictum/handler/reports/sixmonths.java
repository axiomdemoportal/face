/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.handler.reports;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.dictum.management.BulkMSGManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mollatech1
 */
public class sixmonths extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(sixmonths.class.getName());
      final int PENDING = 0;
    final int SENT = 1;
    final int FAILD = -1;
    final int SMS = 1;
    final int USSD = 2;
    final int VOICE = 3;
    final int EMAIL = 4;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        log.info("Servlet started");
        PrintWriter out = response.getWriter();
        
//               String _type = request.getParameter("_type");
         String _type = "1";
//        String _status = request.getParameter("_status");
       
         Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
         log.debug("channel Name :: "+channel.getName());
         Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
          log.debug("operator Name :: "+operator.getName());
          int type = 0;
          int status = 0;
          if(_type == "1"){
              type= SMS;
          }else if(_type == "2"){
              type= USSD;
          }else if(_type == "3"){
              type= VOICE;
          }else if(_type == "4"){
              type= EMAIL;
          }
     
        
        try {
                     Calendar cal = Calendar.getInstance();
            BulkMSGManagement bulk = new BulkMSGManagement();
            ArrayList<line> sample = new ArrayList<line>();
            Date date =new Date();

            for (int i = 0; i < 6; i++) {

                cal.setTime(date);
                cal.add(Calendar.DATE, -30);
             
                date = cal.getTime();

                   int PENDING = bulk.GetBulkMSGStatusByMonth(channel.getChannelid(),operator.getOperatorid(), type, 0, date);
                   log.debug("GetBulkMSGStatusByMonth :: "+PENDING);
                   int SENT = bulk.GetBulkMSGStatusByMonth(channel.getChannelid(),operator.getOperatorid(), type, 1, date);
                   log.debug("GetBulkMSGStatusByMonth :: "+SENT);
                   int FAILD = bulk.GetBulkMSGStatusByMonth(channel.getChannelid(),operator.getOperatorid(), type, -1, date);
                   log.debug("GetBulkMSGStatusByMonth :: "+FAILD);
//                int PENDING = bulk.GetBulkMSGStatusByMonth("b6BK5gdwIWqSue/8VSJOuuIiFNc=", type, 0, date);
//                int SENT = bulk.GetBulkMSGStatusByMonth("b6BK5gdwIWqSue/8VSJOuuIiFNc=", type, 1, date);
//                int FAILD = bulk.GetBulkMSGStatusByMonth("b6BK5gdwIWqSue/8VSJOuuIiFNc=", type, -1, date);
//                
  
                     Calendar current = Calendar.getInstance();
            current.setTime(date);

            current.add(Calendar.DATE, 30);//add a date
             current.add(Calendar.DAY_OF_MONTH, 0);
            Date endDate = current.getTime();

                SimpleDateFormat sd = new SimpleDateFormat("yyyy");
                int month = i+1;
                String startdate = sd.format(endDate)+"-"+month;

               // date = startDate;
                sample.add(new line(startdate, PENDING, SENT, FAILD));
                cal.setTime(date);

            }

//            for (int i = 0; i < sample.size(); i++) {
//                System.out.println(sample.get(i));
//            }
            Gson gson = new Gson();

            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<line>>() {
            }.getType());

            JsonArray jsonArray = element.getAsJsonArray();
            response.setContentType("application/json");
            out.print(jsonArray);

            
            
        } finally {            
            out.close();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
