/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.handler.reports;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.dictum.management.BulkMSGManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mollatech1
 */
public class threemonths extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(threemonths.class.getName());
   final int PENDING = 0;
    final int SENT = 1;
    final int FAILD = -1;
    final int SMS = 1;
    final int USSD = 2;
    final int VOICE = 3;
    final int EMAIL = 4;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        log.info("Servlet started");
        PrintWriter out = response.getWriter();
//        String _type = request.getParameter("_type");
               String _type ="1";
        
         Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
         log.debug("channel name :: "+channel.getName());
          Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
          log.debug("operator Name :: "+operator);
          int type = 0;
          int status = 0;
          if(_type == "1"){
              type= SMS;
          }else if(_type == "2"){
              type= USSD;
          }else if(_type == "3"){
              type= VOICE;
          }else if(_type == "4"){
              type= EMAIL;
          }
       
        
        
        try {
             Calendar cal = Calendar.getInstance();
            SimpleDateFormat sd = new SimpleDateFormat("yyyy");
            BulkMSGManagement bulk = new BulkMSGManagement();
            ArrayList<line> sample = new ArrayList<line>();
            Date date = new Date();
            String strYear = sd.format(date);
            int year = Integer.parseInt(strYear);
            Date startDate = null;
           // cal.set(year, Calendar.JANUARY, 01); 
            cal.add(Calendar.WEEK_OF_MONTH, -15);
            //January 1st 1753
            date = cal.getTime();
            for (int i = 0; i < 15; i++) {

                cal.setTime(date);
                cal.set(Calendar.AM_PM, Calendar.AM);

                cal.add(Calendar.DATE, 7);
                date = cal.getTime();

                  int PENDING = bulk.GetBulkMSGStatusByWeek(channel.getChannelid(),operator.getOperatorid(),type, 0, date);
                  log.debug("GetBulkMSGStatusByWeek :: "+PENDING);
                  int SENT = bulk.GetBulkMSGStatusByWeek(channel.getChannelid(),operator.getOperatorid(), type, 1, date);
                  log.debug("GetBulkMSGStatusByWeek :: "+SENT);
                  int FAILD = bulk.GetBulkMSGStatusByWeek(channel.getChannelid(),operator.getOperatorid(), type, -1, date);
                  log.debug("GetBulkMSGStatusByWeek :: "+FAILD);
//               int PENDING = bulk.GetBulkMSGStatusByWeek("b6BK5gdwIWqSue/8VSJOuuIiFNc=",type, 0, date);
//                int SENT = bulk.GetBulkMSGStatusByWeek("b6BK5gdwIWqSue/8VSJOuuIiFNc=", type, 1, date);
//                int FAILD = bulk.GetBulkMSGStatusByWeek("b6BK5gdwIWqSue/8VSJOuuIiFNc=", type, -1, date);



                // SimpleDateFormat sd = new SimpleDateFormat("dd-MM-YYYY");

                Calendar current = Calendar.getInstance();
                current.setTime(date);

                current.add(Calendar.DATE, -7);//add a date
                Date endDate = current.getTime();

                int count = i + 1;
                String startdate = sd.format(endDate) + " W" + count;

                // date = startDate;
                sample.add(new line(startdate, PENDING, SENT, FAILD));
                cal.setTime(date);

            }

//            for (int i = 0; i < sample.size(); i++) {
//                System.out.println(sample.get(i));
//            }
            Gson gson = new Gson();

            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<line>>() {
            }.getType());

            JsonArray jsonArray = element.getAsJsonArray();
            response.setContentType("application/json");
            out.print(jsonArray);

        } finally {            
            out.close();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
