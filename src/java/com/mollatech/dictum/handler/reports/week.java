/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.handler.reports;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.dictum.management.BulkMSGManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mollatech1
 */
public class week extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(week.class.getName());
     final int PENDING = 0;
    final int SENT = 1;
    final int FAILD = -1;
    final int SMS = 1;
    final int USSD = 2;
    final int VOICE = 3;
    final int EMAIL = 4;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        log.info("Servlet started");
        PrintWriter out = response.getWriter();
        
          String _type = request.getParameter("_type");
          log.debug("_type :: "+_type);
       
        
         Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
         log.debug("channel Name :: "+channel.getName());
          Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
          log.debug("operator Name :: "+operator.getName());
          int type = 0;
          int status = 0;
          if(   "1".equals(_type)){
              type= SMS;
          }else if("2".equals(_type)){
              type= USSD;
          }else if("3".equals(_type)){
              type= VOICE;
          }else if("4".equals(_type)){
              type= EMAIL;
          }
        
         
        
        try {
             Calendar cal = Calendar.getInstance();
            BulkMSGManagement bulk = new BulkMSGManagement();
            ArrayList<line> sample = new ArrayList<line>();
            Date date =new Date();
       //     Date startDate = null;
            for (int i = 0; i < 7; i++) {

                cal.setTime(date);
                cal.set(Calendar.AM_PM, Calendar.AM);
                cal.set(Calendar.HOUR, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
                //   cal.add(Calendar.DATE, 1);//add a date
                cal.add(Calendar.DATE, -1);
                date = cal.getTime();
//
                int pending = bulk.GetBulkMSGStatusByDay(channel.getChannelid(),operator.getOperatorid(),  type, 0, date);
                log.debug("GetBulkMSGStatusByDay :: "+pending);
                int sent = bulk.GetBulkMSGStatusByDay(channel.getChannelid(),operator.getOperatorid(),type, 1, date);
                log.debug("GetBulkMSGStatusByDay :: "+sent);
                int failed = bulk.GetBulkMSGStatusByDay(channel.getChannelid(),operator.getOperatorid(),  type, -1, date);
                log.debug("GetBulkMSGStatusByDay :: "+failed);
              
//                  int pending = bulk.GetBulkMSGStatusByDay("b6BK5gdwIWqSue/8VSJOuuIiFNc=","t7v3iQZBhLNiuY8Ab++eSxJpAmk=",1, 0, date);
//                int sent = bulk.GetBulkMSGStatusByDay("b6BK5gdwIWqSue/8VSJOuuIiFNc=","t7v3iQZBhLNiuY8Ab++eSxJpAmk=",1, 1, date);
//                int failed = bulk.GetBulkMSGStatusByDay("b6BK5gdwIWqSue/8VSJOuuIiFNc=","t7v3iQZBhLNiuY8Ab++eSxJpAmk=",1, -1, date);

                
                SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
                // SimpleDateFormat sd = new SimpleDateFormat("dd-MM-YYYY");

                String startdate = sd.format(date);

               // date = startDate;
                sample.add(new line(startdate, pending, sent, failed));
                cal.setTime(date);

            }

//            for (int i = 0; i < sample.size(); i++) {
//                System.out.println(sample.get(i));
//            }
            Gson gson = new Gson();

            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<line>>() {
            }.getType());

            JsonArray jsonArray = element.getAsJsonArray();
            response.setContentType("application/json");
            out.print(jsonArray);

        }catch(Exception ex){
            log.error("Exception caught :: ",ex);
        }
        finally {            
            out.close();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
