/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.handler;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Contacts;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.operation.AxiomMessage;
import com.mollatech.axiom.nucleus.db.operation.AxiomOperator;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.ContentFilterSetting;
import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import static com.mollatech.dictum.handler.bulkfbnotification.FACEBOOK;
import com.mollatech.dictum.management.BulkMSGManagement;
import com.mollatech.dictum.management.ContactManagement;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class bulklinkedinnotification extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(bulklinkedinnotification.class.getName());
    public static int FACEBOOK = 5;
    public static int LINKEDIN = 6;
    final int PENDING = 0;
    final int SENT = 1;
    final int FAIELD = -1;
    final int FAILED = -1;
    final int BLOCKED = -121;
    final String itemType = "BULK_LINKDN";

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        PrintWriter out = response.getWriter();
        try {

            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId :: "+sessionId);
            Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operator :: "+operator.getName());
            String operatorId = operator.getOperatorid();
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("remoteaccesslogin :: "+remoteaccesslogin);
            String _TagList = "linkedin";   //request.getParameter("_tagID");

            String _channelId = channel.getChannelid();

            String _msgBody = request.getParameter("_messagebody");
            log.debug("_msgBody :: "+_msgBody);
            String _speed1 = request.getParameter("_speed");
            log.debug("_speed1 :: "+_speed1);
            int _speed = Integer.parseInt(_speed1);
            String _Option = request.getParameter("_sendingOption");
            log.debug("_Option :: "+_Option);
            int _sendingOption=Integer.parseInt(_Option);

            Contacts[] cont = null;
            int retValue = -1;
            int result = -1;
            BulkMSGManagement bulksms = new BulkMSGManagement();
            AuditManagement audit = new AuditManagement();
            ContactManagement contact = new ContactManagement();
            SettingsManagement sMngmt = new SettingsManagement();
            SendNotification send = new SendNotification();
            TemplateManagement tObj = new TemplateManagement();
            OperatorsManagement oObj = new OperatorsManagement();
            AxiomOperator[] aOperator = oObj.ListOperatorsInternal(_channelId);
            AxiomOperator channelOp = null;
            for (int i = 0; i < aOperator.length; i++) {
                if (aOperator[i].getStrRoleName().equals("admin")) {
                    channelOp = aOperator[i];
                }
            }

            Object obj = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.GlobalSettings, 1);

            if (obj != null) {
                GlobalChannelSettings cObj = (GlobalChannelSettings) obj;
                String strword = sMngmt.checkcontent( channel.getChannelid(), _msgBody,cObj);
                if (strword != null && cObj.contentstatus == 0) {
                    if (cObj.contentalertstatus == 0) {
                        Templates templatesObj = tObj.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.EMAIL_CONTENT_BLOCKED_ALERT);
                      if(templatesObj.getStatus() == tObj.ACTIVE_STATUS){
                        ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                        String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                        String strsubject = templatesObj.getSubject();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                        if (strmessageBody != null) {
                            // Date date = new Date();
                            strmessageBody = strmessageBody.replaceAll("#name#", operator.getName());
                            strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                            strmessageBody = strmessageBody.replaceAll("#email#", operator.getEmailid());
                            strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                            strmessageBody = strmessageBody.replaceAll("#filterword#", strword);
                            strmessageBody = strmessageBody.replaceAll("#alertmsgbody#", _msgBody);

                        }
                        AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), channelOp.getStrEmail(), strsubject, strmessageBody,
                                null, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                        result = BLOCKED;
                    }
                      result = BLOCKED;
                    }
                }
//                ContentFilterSetting cObj = (ContentFilterSetting) obj;
//                int strword = sMngmt.checkcontent(sessionId, channel.getChannelid(), _msgBody);
//                if (strword != 0 && cObj.getStatus() == 0) {
//                    result = BLOCKED;
//                }
//                SettingsManagement setting = new SettingsManagement();
//
//                Date d = new Date();
//                TemplateManagement tManagement = new TemplateManagement();
//                OperatorsManagement opr = new OperatorsManagement();
//                SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                cont = contact.SearchContactsByTag(sessionId, _channelId, _TagList);
            } else {
                cont = contact.SearchContactsByTag(sessionId, _channelId, _TagList);
            }

            AxiomMessage[] axiommessage = new AxiomMessage[cont.length];
            if (cont != null) {
                for (int i = 0; i < cont.length; i++) {
                    String msgTemp = _msgBody;
                    if (cont[i].getLinkedinaccesstoken()!= null) {
                        Date d = new Date();
                        msgTemp = msgTemp.replaceAll("#name#", cont[i].getName());
                        msgTemp = msgTemp.replaceAll("#date#", d.toString());
                        msgTemp = msgTemp.replaceAll("#datetime#", d.toString());
                        msgTemp = msgTemp.replaceAll("#email#", cont[i].getEmailid());

                    }
                    axiommessage[i] = new AxiomMessage();
                    //System.out.println(axiommessage);
                    if (cont[i].getName() != null) {

                        axiommessage[i].name = cont[i].getName();
                    }
                    if (cont[i].getLinkedinaccesstoken() != null) {
                        axiommessage[i].linkedintokenid = cont[i].getLinkedinaccesstoken();
                    }
                    if (msgTemp != null) {
                        axiommessage[i].message = msgTemp;
                    }

                }
            }

              String uniqueid = request.getSession().getId() + channel.getChannelid()+new Date().getTime() + sessionId;
                  String uniqueidforbulk = new String(Base64.encode(UtilityFunctions.SHA1(uniqueid)));
            for (int i = 0; i < axiommessage.length; i++) {
                retValue = bulksms.ADDAxiomMessage(sessionId, channel.getChannelid(), operator.getOperatorid(), axiommessage[i], PENDING, new Date(), new Date(), LINKEDIN, _speed,uniqueidforbulk);
                log.debug("ADDAxiomMessage :: "+retValue);
            }
            result = bulksms.SendLinkedinMSG(sessionId, channel.getChannelid(), LINKEDIN,_sendingOption, _speed,uniqueidforbulk);
            log.debug("SendLinkedinMSG :: "+result);

            String resultstr = "ERROR";
            if (result == 0) {
                resultstr = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operator.getName(), new Date(), "Add and Send Bulk LINKDIN...!", resultstr, result,
                        "Bulk LINKDIN Management", "", "Bulk LINKDIN Added Successfully", itemType, operatorId);
            }else if (result == BLOCKED) {
                //resultstr = "Blocked";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operator.getName(), new Date(), "Add and Send Bulk LINKDIN...!", resultstr, result,
                        "Bulk LINKDIN Management", "", "LINKDIN Blocked by Content Filter Setting ...!", itemType, operatorId);
            } else if (result != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operator.getName(), new Date(), "Add and Send Bulk LINKDIN...!", resultstr, result,
                        "Bulk LINKDIN Management", "", "Failed To Add Bulk LINKDIN", itemType, operatorId);
            }

            if (result == 0) {

                String strresult = "success";
                String message = "Messages are queued and getting sent in background!!!";
                JSONObject json = new JSONObject();
                try { json.put("_result", strresult);
                json.put("_message", message);
                }catch(Exception e){
                   log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();

            } else {

                String message = "error";
                JSONObject json = new JSONObject();
                try { json.put("_result", "error");
                json.put("_message", message);
                }catch(Exception e){
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();
            }


        } finally {
            out.close();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
