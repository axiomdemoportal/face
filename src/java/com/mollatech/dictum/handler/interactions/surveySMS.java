/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.handler.interactions;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Interactionresponse;
import com.mollatech.axiom.nucleus.db.Interactionsexecutions;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.dictum.management.SurveyManagement;
import com.mollatech.axiom.nucleus.db.operation.Survey;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;

/**
 *
 * @author mollatech2
 */
public class surveySMS extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(surveySMS.class.getName());
    final int SMS = 1;
    final int USSD = 2;
    final int VOICE = 3;
    final int EMAIL = 4;
    final int SUCCESS = 0;
    final int FAILED = -1;
    static final int RUNNING = 1;
    static final int STOPED = 2;
    static final int CLOSED = 3;
    final int NO = -1;
    final int YES = 1;
    final int OPTION1 = 1;
    final int OPTION2 = 2;
    final int OPTION3 = 3;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        String smsid = request.getParameter("SmsSid");
        log.debug("smsid :: "+smsid);
        String accountSid = request.getParameter("AccountSid");
        log.debug("accountSid :: "+accountSid);
        String from = request.getParameter("From");
        log.debug("from :: "+from);
        String to = request.getParameter("To");
        log.debug("to :: "+to);
        String body = request.getParameter("Body");
        log.debug("body :: "+body);
        String fromState = request.getParameter("FromState");
        log.debug("fromState :: "+fromState);
        String fromZip = request.getParameter("FromZip");
        log.debug("fromZip :: "+fromZip);
        String FromCountry = request.getParameter("FromCountry");
        log.debug("FromCountry :: "+FromCountry);
        String toCity = request.getParameter("ToCity");
        log.debug("toCity :: "+toCity);
        String toState = request.getParameter("ToState");
        log.debug("toState :: "+toState);
        String toZip = request.getParameter("ToZip");
        log.debug("toZip :: "+toZip);
        String toCountry = request.getParameter("ToCountry");
        log.debug("toCountry :: "+toCountry);
        // String strInteractionid = request.getParameter("interactionId");
        int interactionId = 0;
        AXIOMStatus status = null;
        Interactionsexecutions interaction = null;
        Interactionsexecutions[] interactionsList = null;
        Interactionresponse interactionresponse = null;
        String message = null;
        SendNotification send = new SendNotification();
        Survey survey = null;
//        if (strInteractionid != null) {
//            interactionId = Integer.parseInt(strInteractionid);
//        }

        String _channelName = this.getServletContext().getContextPath();
        _channelName = _channelName.replaceAll("/", "");

        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);

        Channels channel = cUtil.getChannel(_channelName);
        if (channel == null) {
            sChannel.close();
            suChannel.close();
            return;
        }

        body = body.trim();

        sChannel.close();
        suChannel.close();
        //end of addition

        String channelId = channel.getChannelid();
        SurveyManagement sManagement = new SurveyManagement();
        interactionsList = sManagement.getInteractionsExecutionList(channelId);
        for (int i = 0; i < interactionsList.length; i++) {
            if (interactionsList[i].getStatus() == RUNNING) {
                interaction = interactionsList[i];
                break;
            }
        }
        Date currentDate = new Date();

        if (currentDate.getTime() - interaction.getExpirydatetime().getTime() <= 0) {
            sManagement.changeStatus(interaction.getInteractionid(), channelId, CLOSED);
        }

        interactionresponse = sManagement.getInteractionsResponse(channelId, from, interaction.getInteractionexecutionid());

        survey = sManagement.getSurveyQuestionsByIRExecution(interaction);

        if (interactionresponse != null && survey != null) {
            if (body.equalsIgnoreCase(survey.greetingsTags[1])) {
                sManagement.changeInteractionResponse(channelId,interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), NO, interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 0,SurveyManagement.CLOSED);
            } else if (body.equalsIgnoreCase(survey.greetingsTags[0])) {
                // sManagement.changeInteractionResponse(channelId, from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), YES, interactionresponse.getOffer(), NO, interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 0);

                if (interactionresponse.getQuestion1() == null) {
                    //user not found
                } else {

                    message = "Question 1\n" + interactionresponse.getQuestion1()
                            + "\n" + survey.questionAndOptions[0].optionTags[0] + " "
                            + survey.questionAndOptions[0].options[0]
                            + "\n" + survey.questionAndOptions[0].optionTags[1] + " "
                            + survey.questionAndOptions[0].options[1]
                            + "\n" + survey.questionAndOptions[0].optionTags[2] + " "
                            + survey.questionAndOptions[0].options[2];
                    status = send.SendOnMobile(channelId, from, message, SMS, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                    if (status.iStatus == 0 && status.strStatus.equals("completed")) {
                        //    survey.options[0].iCurrent = 1;
                        sManagement.changeInteractionResponse(channelId,interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), YES, interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 1,SurveyManagement.RUNNING);
                    }
                }
            }
            int iCurrent = interactionresponse.getIcurrent();
            if (body.equalsIgnoreCase(survey.questionAndOptions[iCurrent].optionTags[0])) {

                if (interactionresponse.getIcurrent() == 1) {
                    // sManagement.changeInteractionResponse(channelId, from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), YES, interactionresponse.getOffer(), interactionresponse.getGreetingsresponse(), interactionresponse.getQuestion1(), A, interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 0);

                    if (interactionresponse.getQuestion2() == null) {
                        //user not found
                    } else {

                        message = "Question 2\n" + interactionresponse.getQuestion2()
                                + "\n" + survey.questionAndOptions[1].optionTags[0] + " "
                                + survey.questionAndOptions[1].options[0]
                                + "\n" + survey.questionAndOptions[1].optionTags[1] + " "
                                + survey.questionAndOptions[1].options[1]
                                + "\n" + survey.questionAndOptions[1].optionTags[2] + " "
                                + survey.questionAndOptions[1].options[2];
                        status = send.SendOnMobile(channelId, from, message, SMS, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                        if (status.iStatus == 0 && status.strStatus.equals("completed")) {
                            //    survey.options[0].iCurrent = 1;
                            sManagement.changeInteractionResponse(channelId,interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), OPTION1, interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 2,SurveyManagement.RUNNING);
                        }
                    }
                } else if (interactionresponse.getIcurrent() == 2) {
                    // sManagement.changeInteractionResponse(channelId, from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), NO, interactionresponse.getOffer(), interactionresponse.getGreetingsresponse(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), A, interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 0);

                    if (interactionresponse.getQuestion3() == null) {
                        //user not found
                    } else {

                        message = "Question 3\n" + interactionresponse.getQuestion3()
                                + "\n" + survey.questionAndOptions[2].optionTags[0] + " "
                                + survey.questionAndOptions[2].options[0]
                                + "\n" + survey.questionAndOptions[2].optionTags[1] + " "
                                + "\n" + survey.questionAndOptions[2].options[1]
                                + "\n" + survey.questionAndOptions[2].optionTags[2] + " "
                                + "\n" + survey.questionAndOptions[2].options[2];
                        status = send.SendOnMobile(channelId, from, message, SMS, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                        if (status.iStatus == 0 && status.strStatus.equals("completed")) {
                            //    survey.options[0].iCurrent = 1;
                            sManagement.changeInteractionResponse(channelId,interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), OPTION1, interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 3,SurveyManagement.RUNNING);
                        }
                    }
                } else if (interactionresponse.getIcurrent() == 3) {

                    if (interactionresponse.getQuestion4() == null) {
                        //user not found
                    } else {

                        message = "Question 4\n" + interactionresponse.getQuestion4()
                                + "\n" + survey.questionAndOptions[3].optionTags[0] + " "
                                + survey.questionAndOptions[3].options[0]
                                + "\n" + survey.questionAndOptions[3].optionTags[1] + " "
                                + survey.questionAndOptions[3].options[1]
                                + "\n" + survey.questionAndOptions[3].optionTags[2] + " "
                                + survey.questionAndOptions[3].options[2];
                        status = send.SendOnMobile(channelId, from, message, SMS, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                        if (status.iStatus == 0 && status.strStatus.equals("completed")) {
                            //    survey.options[0].iCurrent = 1;
                            sManagement.changeInteractionResponse(channelId,interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), OPTION1, interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 4,SurveyManagement.RUNNING);
                        }
                    }
                } else if (interactionresponse.getIcurrent() == 4) {

                    if (interactionresponse.getQuestion5() == null) {
                        //user not found
                    } else {

                        message = "Question 5\n" + interactionresponse.getQuestion5()
                                + "\n" + survey.questionAndOptions[4].optionTags[0] + " "
                                + survey.questionAndOptions[4].options[0]
                                + "\n" + survey.questionAndOptions[4].optionTags[1] + " "
                                + survey.questionAndOptions[4].options[1]
                                + "\n" + survey.questionAndOptions[4].optionTags[2] + " "
                                + survey.questionAndOptions[4].options[2];
                        status = send.SendOnMobile(channelId, from, message, SMS, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                        if (status.iStatus == 0 && status.strStatus.equals("completed")) {
                            //    survey.options[0].iCurrent = 1;
                            sManagement.changeInteractionResponse(channelId,interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), OPTION1, interactionresponse.getQuestion5(), interactionresponse.getAns5(), 5,SurveyManagement.RUNNING);
                        }
                    }
                } else if (interactionresponse.getIcurrent() == 5) {
                    sManagement.changeInteractionResponse(channelId,interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), OPTION1, interactionresponse.getIcurrent(),SurveyManagement.CLOSED);
                    status = send.SendOnMobile(channelId, from, survey.offer, SMS, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                }
            } else if (body.equalsIgnoreCase(survey.questionAndOptions[iCurrent].optionTags[1])) {

                if (interactionresponse.getIcurrent() == 1) {
                    // sManagement.changeInteractionResponse(channelId, from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), YES, interactionresponse.getOffer(), interactionresponse.getGreetingsresponse(), interactionresponse.getQuestion1(), A, interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 0);

                    if (interactionresponse.getQuestion1() == null) {
                        //user not found
                    } else {

                        message = "Question 2\n" + interactionresponse.getQuestion2()
                                + "\n" + survey.questionAndOptions[1].optionTags[0] + " "
                                + survey.questionAndOptions[1].options[0]
                                + "\n" + survey.questionAndOptions[1].optionTags[1] + " "
                                + survey.questionAndOptions[1].options[1]
                                + "\n" + survey.questionAndOptions[1].optionTags[2] + " "
                                + survey.questionAndOptions[1].options[2];
                        status = send.SendOnMobile(channelId, from, message, SMS, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                        if (status.iStatus == 0 && status.strStatus.equals("completed")) {
                            //    survey.options[0].iCurrent = 1;
                            sManagement.changeInteractionResponse(channelId, interactionresponse.getContactid(),from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), OPTION2, interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 2,SurveyManagement.RUNNING);
                        }
                    }
                } else if (interactionresponse.getIcurrent() == 2) {
                    // sManagement.changeInteractionResponse(channelId, from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), NO, interactionresponse.getOffer(), interactionresponse.getGreetingsresponse(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), A, interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 0);

                    if (interactionresponse.getQuestion1() == null) {
                        //user not found
                    } else {

                        message = "Question 3\n" + interactionresponse.getQuestion3()
                                + "\n" + survey.questionAndOptions[2].optionTags[0] + " "
                                + survey.questionAndOptions[2].options[0]
                                + "\n" + survey.questionAndOptions[2].optionTags[1] + " "
                                + "\n" + survey.questionAndOptions[2].options[1]
                                + "\n" + survey.questionAndOptions[2].optionTags[2] + " "
                                + "\n" + survey.questionAndOptions[2].options[2];
                        status = send.SendOnMobile(channelId, from, message, SMS, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                        if (status.iStatus == 0 && status.strStatus.equals("completed")) {
                            //    survey.options[0].iCurrent = 1;
                            sManagement.changeInteractionResponse(channelId,interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), OPTION2, interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 3,SurveyManagement.RUNNING);
                        }
                    }
                } else if (interactionresponse.getIcurrent() == 3) {

                    if (interactionresponse.getQuestion1() == null) {
                        //user not found
                    } else {

                        message = "Question 4\n" + interactionresponse.getQuestion4()
                                + "\n" + survey.questionAndOptions[3].optionTags[0] + " "
                                + survey.questionAndOptions[3].options[0]
                                + "\n" + survey.questionAndOptions[3].optionTags[1] + " "
                                + survey.questionAndOptions[3].options[1]
                                + "\n" + survey.questionAndOptions[3].optionTags[2] + " "
                                + survey.questionAndOptions[3].options[2];
                        status = send.SendOnMobile(channelId, from, message, SMS, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                        if (status.iStatus == 0 && status.strStatus.equals("completed")) {
                            //    survey.options[0].iCurrent = 1;
                            sManagement.changeInteractionResponse(channelId,interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), OPTION2, interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 4,SurveyManagement.RUNNING);
                        }
                    }
                } else if (interactionresponse.getIcurrent() == 4) {

                    if (interactionresponse.getQuestion1() == null) {
                        //user not found
                    } else {

                        message = "Question 5\n" + interactionresponse.getQuestion5()
                                + "\n" + survey.questionAndOptions[4].optionTags[0] + " "
                                + survey.questionAndOptions[4].options[0]
                                + "\n" + survey.questionAndOptions[4].optionTags[1] + " "
                                + survey.questionAndOptions[4].options[1]
                                + "\n" + survey.questionAndOptions[4].optionTags[2] + " "
                                + survey.questionAndOptions[4].options[2];
                        status = send.SendOnMobile(channelId, from, message, SMS, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                        if (status.iStatus == 0 && status.strStatus.equals("completed")) {
                            //    survey.options[0].iCurrent = 1;
                            sManagement.changeInteractionResponse(channelId,interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), OPTION2, interactionresponse.getQuestion5(), interactionresponse.getAns5(), 5,SurveyManagement.RUNNING);
                        }
                    }
                } else if (interactionresponse.getIcurrent() == 5) {
                    sManagement.changeInteractionResponse(channelId,interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), OPTION2, interactionresponse.getIcurrent(),SurveyManagement.CLOSED);
                    status = send.SendOnMobile(channelId, from, survey.offer, SMS, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                }
            } else if (body.equalsIgnoreCase(survey.questionAndOptions[iCurrent].optionTags[2])) {

                if (interactionresponse.getIcurrent() == 1) {
                    // sManagement.changeInteractionResponse(channelId, from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), YES, interactionresponse.getOffer(), interactionresponse.getGreetingsresponse(), interactionresponse.getQuestion1(), A, interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 0);

                    if (interactionresponse.getQuestion1() == null) {
                        //user not found
                    } else {

                        message = "Question 2\n" + interactionresponse.getQuestion2()
                                + "\n" + survey.questionAndOptions[1].optionTags[0] + " "
                                + survey.questionAndOptions[1].options[0]
                                + "\n" + survey.questionAndOptions[1].optionTags[1] + " "
                                + survey.questionAndOptions[1].options[1]
                                + "\n" + survey.questionAndOptions[1].optionTags[2] + " "
                                + survey.questionAndOptions[1].options[2];
                        status = send.SendOnMobile(channelId, from, message, SMS, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                        if (status.iStatus == 0 && status.strStatus.equals("completed")) {
                            //    survey.options[0].iCurrent = 1;
                            sManagement.changeInteractionResponse(channelId,interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), OPTION3, interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 2,SurveyManagement.RUNNING);
                        }
                    }
                } else if (interactionresponse.getIcurrent() == 2) {
                    // sManagement.changeInteractionResponse(channelId, from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), NO, interactionresponse.getOffer(), interactionresponse.getGreetingsresponse(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), A, interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 0);

                    if (interactionresponse.getQuestion1() == null) {
                        //user not found
                    } else {

                        message = "Question 3\n" + interactionresponse.getQuestion3()
                                + "\n" + survey.questionAndOptions[2].optionTags[0] + " "
                                + survey.questionAndOptions[2].options[0]
                                + "\n" + survey.questionAndOptions[2].optionTags[1] + " "
                                + "\n" + survey.questionAndOptions[2].options[1]
                                + "\n" + survey.questionAndOptions[2].optionTags[2] + " "
                                + "\n" + survey.questionAndOptions[2].options[2];
                        status = send.SendOnMobile(channelId, from, message, SMS, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                        if (status.iStatus == 0 && status.strStatus.equals("completed")) {
                            //    survey.options[0].iCurrent = 1;
                            sManagement.changeInteractionResponse(channelId,interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), OPTION3, interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 3,SurveyManagement.RUNNING);
                        }
                    }
                } else if (interactionresponse.getIcurrent() == 3) {

                    if (interactionresponse.getQuestion1() == null) {
                        //user not found
                    } else {

                        message = "Question 4\n" + interactionresponse.getQuestion4()
                                + "\n" + survey.questionAndOptions[3].optionTags[0] + " "
                                + survey.questionAndOptions[3].options[0]
                                + "\n" + survey.questionAndOptions[3].optionTags[1] + " "
                                + survey.questionAndOptions[3].options[1]
                                + "\n" + survey.questionAndOptions[3].optionTags[2] + " "
                                + survey.questionAndOptions[3].options[2];
                        status = send.SendOnMobile(channelId, from, message, SMS, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                        if (status.iStatus == 0 && status.strStatus.equals("completed")) {
                            //    survey.options[0].iCurrent = 1;
                            sManagement.changeInteractionResponse(channelId,interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), OPTION3, interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 4,SurveyManagement.RUNNING);
                        }
                    }
                } else if (interactionresponse.getIcurrent() == 4) {

                    if (interactionresponse.getQuestion1() == null) {
                        //user not found
                    } else {

                        message = "Question 5\n" + interactionresponse.getQuestion5()
                                + "\n" + survey.questionAndOptions[4].optionTags[0] + " "
                                + survey.questionAndOptions[4].options[0]
                                + "\n" + survey.questionAndOptions[4].optionTags[1] + " "
                                + survey.questionAndOptions[4].options[1]
                                + "\n" + survey.questionAndOptions[4].optionTags[2] + " "
                                + survey.questionAndOptions[4].options[2];
                        status = send.SendOnMobile(channelId, from, message, SMS, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                        if (status.iStatus == 0 && status.strStatus.equals("completed")) {
                            //    survey.options[0].iCurrent = 1;
                            sManagement.changeInteractionResponse(channelId,interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(),
                                    interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), OPTION3, interactionresponse.getQuestion5(), interactionresponse.getAns5(), 5,SurveyManagement.RUNNING);
                        }
                    }
                } else if (interactionresponse.getIcurrent() == 5) {
                    sManagement.changeInteractionResponse(channelId,interactionresponse.getContactid(), from, interaction.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), interactionresponse.getGreetingsresponse(), interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), OPTION3, interactionresponse.getIcurrent(),SurveyManagement.CLOSED);
                    status = send.SendOnMobile(channelId, from, survey.offer, SMS, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                }
            }
//           
        }
        //need to handle IRes nullpointer exception
            log.info("Servlet ended");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
