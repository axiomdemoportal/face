/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.handler.interactions;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Interactionresponse;
import com.mollatech.axiom.v2.face.handler.operator.getopraudits;
import com.mollatech.dictum.management.SurveyManagement;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mollatech1
 */
public class InteractionReportDownload extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(InteractionReportDownload.class.getName());
    private static int PDF_TYPE = 0;
    private static int CSV_TYPE = 1;
    private static final int BUFSIZE = 4096;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            try {
                Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                log.debug("channel  :: "+channel.getName());
//                String sessionId = (String) request.getSession().getAttribute("_apSessionID");
                String _reportformat = request.getParameter("_reportformat");
                log.debug("_reportformat  :: "+_reportformat);
                String interactionId = request.getParameter("_interactionID");
                log.debug("interactionId  :: "+interactionId);
                String _QNo = request.getParameter("_QNo");
                log.debug("_QNo  :: "+_QNo);
                String _interactionExecutionID = request.getParameter("_interactionExecutionID");
                log.debug("_interactionExecutionID  :: "+_interactionExecutionID);
                int iInteractionId = -9999;
                if (interactionId != null) {
                    iInteractionId = Integer.parseInt(interactionId);
                    log.debug("iInteractionId  :: "+iInteractionId);
                }
                int iinteractionExecutionID = -9999;
                if (_interactionExecutionID != null) {
                    iinteractionExecutionID = Integer.parseInt(_interactionExecutionID);
                    log.debug("iinteractionExecutionID  :: "+iinteractionExecutionID);
                }
                int iQNO = -9999;
                if (_QNo != null) {
                    iQNO = Integer.parseInt(_QNo);
                }
                int iFormat = -9999;
                if (_reportformat != null && !_reportformat.isEmpty()) {
                    iFormat = Integer.parseInt(_reportformat);
                }

                if (PDF_TYPE == iFormat) {
                    iFormat = PDF_TYPE;
                } else {
                    iFormat = CSV_TYPE;
                }

                String filepath = null;

                try {
                    try {
                        SurveyManagement survey = new SurveyManagement();
//                        Interactionsexecutions InteractionsexecutionsObj = survey.getInteractionsExecution(channel.getChannelid(), iinteractionExecutionID);
//                        DateFormat formatter = new SimpleDateFormat("YYYY-MM-DD HH:mm:ss");
//                       Date startDate = InteractionsexecutionsObj.getCreatedOn();
//                       Date endDate = InteractionsexecutionsObj.getExpirydatetime();

//                        Date sDate = formatter.parse(startDate.toString());
//                        Date eDate = formatter.parse(endDate.toString());
                       
//                              Calendar current = Calendar.getInstance();
//            current.setTime(startDate);
//            current.set(Calendar.AM_PM, Calendar.AM);
//             Date sDate = current.getTime();
//             
////               Calendar current = Calendar.getInstance();
//            current.setTime(endDate);
//            current.set(Calendar.AM_PM, Calendar.AM);
//             Date eDate = current.getTime();
//                        
                        Interactionresponse[] irObj = survey.getIRObjByDate(channel.getChannelid(), iinteractionExecutionID, iQNO);
                        filepath = survey.generateReport(iFormat, channel.getChannelid(), irObj, iQNO, iInteractionId);
                        irObj = null;
                    } catch (Exception e) {
                       log.error("Exception caught :: ",e);
                    }

                    //  
                    File file = new File(filepath);
                    int length = 0;
                    ServletOutputStream outStream = response.getOutputStream();
                    ServletContext context = getServletConfig().getServletContext();
                    String mimetype = context.getMimeType(filepath);

                    // sets response content type
                    if (mimetype == null) {
                        mimetype = "application/octet-stream";
                    }
                    response.setContentType(mimetype);
                    response.setContentLength((int) file.length());
                    String fileName = (new File(filepath)).getName();

                    // sets HTTP header
                    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

                    byte[] byteBuffer = new byte[BUFSIZE];
                    DataInputStream in = new DataInputStream(new FileInputStream(file));

                    // reads the file's bytes and writes them to the response stream
                    while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                        outStream.write(byteBuffer, 0, length);
                    }

                    in.close();
                    outStream.close();
                    file.delete();

                } catch (Exception ex) {
                    // TODO handle custom exceptions here
                    log.error("Exception caught :: ",ex);
                }

            } catch (Exception ex) {
               log.error("Exception caught :: ",ex);
            }

        } finally {
            //  out.close();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
