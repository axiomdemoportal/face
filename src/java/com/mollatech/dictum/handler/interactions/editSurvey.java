/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.handler.interactions;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.operation.Survey;
import com.mollatech.axiom.nucleus.db.operation.SurveyQueAndOptions;
import com.mollatech.dictum.management.SurveyManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class editSurvey extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editSurvey.class.getName());
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("application/json");
        log.info("Servlet started");
        PrintWriter out = response.getWriter();
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        //String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        //  String _TagList = request.getParameter("_tagID");

        //String _interactionName = request.getParameter("_nameIR");
        
        JSONObject json = new JSONObject();
        
        
        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.INTERACTIONS) != 0) {
            String result = "error";
            String message = "This feature is not available in this license!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        Date _creationTime = new Date();
        String _channelId = channel.getChannelid();


        String interactionId = request.getParameter("_idIR");
         log.debug("interactionId :: "+interactionId);
        int _interactionId = 0;

        if (interactionId != null) {
            _interactionId = Integer.parseInt(interactionId);
            log.debug("_interactionId :: "+_interactionId);
        }

        String status = request.getParameter("_status");
        log.debug("status :: "+status);
        int _status = -1;

        if (status != null) {
            _status = Integer.parseInt(status);
        }


        String greetings = request.getParameter("_nameG");
        log.debug("greetings :: "+greetings);
        String respYES = request.getParameter("_respYES");
        log.debug("respYES :: "+respYES);
        String respNO = request.getParameter("_respNO");
        log.debug("respNO :: "+respNO);
        String _nameYES = request.getParameter("_nameYES");
        log.debug("_nameYES :: "+_nameYES);
        String _nameNO = request.getParameter("_nameNO");
        log.debug("_nameNO :: "+_nameNO);
        String offer = request.getParameter("_nameTHANKS");
        log.debug("offer :: "+offer);           

//        String _speed1 = request.getParameter("_speed");
//        int _speed = Integer.parseInt(_speed1);
//        String _type1 = request.getParameter("_type");
//        int _type = Integer.parseInt(_type1);
        Survey survey = new Survey();
        survey.questionAndOptions = new SurveyQueAndOptions[5];
        survey.greetings = greetings;
        survey.greetingsTags[0] = respYES;
        survey.greetingsTags[1] = respNO;
        survey.offer = offer;
        survey.greetingOptions[0] = _nameYES;
        survey.greetingOptions[1] = _nameNO;

        // survey.offerTags[0] = _nameYES;
        //survey.offerTags[1] = _nameNO;



        for (int i = 1; i <= survey.questionAndOptions.length; i++) {
            survey.questionAndOptions[i - 1] = new SurveyQueAndOptions();
            survey.questionAndOptions[i - 1].question = request.getParameter("_Q" + i);
            //  survey.questionAndOptions[i-1].optionTags = new String[3];
            //survey.questionAndOptions[i-1].options = new String[3];
            survey.questionAndOptions[i - 1].iCurrent = i;

            for (int j = 1; j < 4; j++) {
                survey.questionAndOptions[i - 1].options[j - 1] = request.getParameter("_Q" + i + "O" + j + "M");
                survey.questionAndOptions[i - 1].optionTags[j - 1] = request.getParameter("_Q" + i + "O" + j + "R");
            }
        }
        String result = "success";
        String message = "Survey added successfully....";

        

        if (greetings == null || offer == null
                || survey.questionAndOptions == null 
                || survey.greetingsTags == null
                || survey.questionAndOptions.length <= 0
                || survey.greetingsTags.length <= 0
                || offer.length() <= 0 ) {
            result = "error";
            message = "Fill All Details. Greeting, Completion Offer are must. Atleast 1 Question is required!!";

            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
                log.error("Exception caught :: ",e);           
            }
            out.print(json);
            out.flush();
            return;
        }

        SurveyManagement sManagement = new SurveyManagement();
        int retValue = -1;


        retValue = sManagement.changeInteractions(_interactionId,  _channelId, survey, _status);
        log.debug("retValue :: "+retValue);
        if (retValue != 0) {
            result = "error";
            message = "Failed to add Survey!!";

        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        }catch(Exception e){
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
