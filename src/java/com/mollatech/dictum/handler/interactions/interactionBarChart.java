/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.handler.interactions;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.face.common.bar;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Interactionsexecutions;
import com.mollatech.axiom.nucleus.db.operation.Survey;
import com.mollatech.dictum.management.SurveyManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mollatech1
 */
public class interactionBarChart extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(interactionBarChart.class.getName());
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        try {
            response.setContentType("application/json");
            PrintWriter out = response.getWriter();
            ArrayList<bar> sample = new ArrayList<bar>();
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel);
            String interactionId = request.getParameter("_interactionID");
            log.debug("interactionId :: "+interactionId);
            String _questionNo = request.getParameter("_questionNo");
            log.debug("_questionNo :: "+_questionNo);
            String _interactionExecutionID = request.getParameter("_interactionExecutionID");
            log.debug("_interactionExecutionID :: "+_interactionExecutionID);
            int iInteractionId = -9999;
            if (interactionId != null) {
                iInteractionId = Integer.parseInt(interactionId);
                log.debug("iInteractionId :: "+iInteractionId);
            }
            int _iquestionNo = -9999;
            if (_questionNo != null) {
                _iquestionNo = Integer.parseInt(_questionNo);
                log.debug("_iquestionNo :: "+_iquestionNo);
            }
            int _iinteractionExecutionID = -9999;
            if (_interactionExecutionID != null) {
                _iinteractionExecutionID = Integer.parseInt(_interactionExecutionID);
                log.debug("_iinteractionExecutionID :: "+_iinteractionExecutionID);
            }
            SurveyManagement survey = new SurveyManagement();
            int optionCount1 = 0;
            int optionCount2 = 0;
            int optionCount3 = 0;
            int igrettingOptcountyes = 0;
            int igrettingOptcountNo = 0;
            int iNotAnswerd = 0;
            int igreetingNotAnswerd = 0;

            int greetingCount = 0;
            int qustion1 = 0;
            int qustion2 = 0;
            int qustion3 = 0;
            int qustion4 = 0;
            int qustion5 = 0;

            Interactionsexecutions iresponseObj = survey.getInteractionExecutions(iInteractionId);
            Survey surveyObj = survey.getSurveyQuestionsByIRExecution(iresponseObj);
            int dropout = 999;

            if (surveyObj != null) {
                String[] options = null;

                if (_iquestionNo == 0) {
                    String[] grettingOpt = surveyObj.greetingOptions;

                    igrettingOptcountyes = survey.getOptionCount(channel.getChannelid(), _iinteractionExecutionID, _iquestionNo, 1);
                    igrettingOptcountNo = survey.getOptionCount(channel.getChannelid(), _iinteractionExecutionID, _iquestionNo, 2);
                    igreetingNotAnswerd = survey.getOptionCount(channel.getChannelid(), _iinteractionExecutionID, _iquestionNo, 0);

                    sample.add(new bar(igrettingOptcountyes, grettingOpt[0]));
                    sample.add(new bar(igrettingOptcountNo, grettingOpt[1]));
                    sample.add(new bar(igreetingNotAnswerd, "Not Answered"));
                } else if (_iquestionNo == dropout) {//999 for dropout

                    greetingCount = survey.getDropCount(channel.getChannelid(), _iinteractionExecutionID, 0);
                    qustion1 = survey.getDropCount(channel.getChannelid(), _iinteractionExecutionID, 1);
                    qustion2 = survey.getDropCount(channel.getChannelid(), _iinteractionExecutionID, 2);
                    qustion3 = survey.getDropCount(channel.getChannelid(), _iinteractionExecutionID, 3);
                    qustion4 = survey.getDropCount(channel.getChannelid(), _iinteractionExecutionID, 4);
                    qustion5 = survey.getDropCount(channel.getChannelid(), _iinteractionExecutionID, 5);

                    sample.add(new bar(greetingCount, "Gretings"));
                    sample.add(new bar(qustion1, "Que1"));
                    sample.add(new bar(qustion2, "Que2"));
                    sample.add(new bar(qustion3, "Que3"));
                    sample.add(new bar(qustion4, "Que4"));
                    sample.add(new bar(qustion5, "Que5"));

                } else {
//                   options = surveyObj.questionAndOptions[_iquestionNo].options;
                    if (_iquestionNo == 1) {
                        options = surveyObj.questionAndOptions[0].options;//1 st Questoions Options
                    } else if (_iquestionNo == 2) {
                        options = surveyObj.questionAndOptions[1].options;//2 st Questoions Options
                    } else if (_iquestionNo == 3) {
                        options = surveyObj.questionAndOptions[2].options;//3 st Questoions Options
                    } else if (_iquestionNo == 4) {
                        options = surveyObj.questionAndOptions[3].options;//4 st Questoions Options
                    } else if (_iquestionNo == 5) {
                        options = surveyObj.questionAndOptions[4].options;//5 st Questoions Options
                    }

                    optionCount1 = survey.getOptionCount(channel.getChannelid(), _iinteractionExecutionID, _iquestionNo, 1);
                    optionCount2 = survey.getOptionCount(channel.getChannelid(), _iinteractionExecutionID, _iquestionNo, 2);
                    optionCount3 = survey.getOptionCount(channel.getChannelid(), _iinteractionExecutionID, _iquestionNo, 3);
                    iNotAnswerd = survey.getOptionCount(channel.getChannelid(), _iinteractionExecutionID, _iquestionNo, 0);

                    sample.add(new bar(optionCount1, options[0]));
                    sample.add(new bar(optionCount2, options[1]));
                    sample.add(new bar(optionCount3, options[2]));
                    sample.add(new bar(iNotAnswerd, "Not Answered"));
                }

                Gson gson = new Gson();

                JsonElement element = gson.toJsonTree(sample, new TypeToken<List<bar>>() {
                }.getType());

                JsonArray jsonArray = element.getAsJsonArray();
                out.print(jsonArray);

            }
        } catch (Exception ex) {
            log.debug("Exception caught :: ",ex);
                    
        }
        log.info("Servlet ended");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
