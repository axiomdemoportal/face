/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.handler.interactions;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Interactionresponse;
import com.mollatech.axiom.nucleus.db.Interactionsexecutions;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.dictum.management.SurveyManagement;
import com.mollatech.axiom.nucleus.db.operation.Survey;

import com.twilio.sdk.verbs.Gather;
import com.twilio.sdk.verbs.Say;
import com.twilio.sdk.verbs.TwiMLResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;

/**
 *
 * @author mollatech2
 */
public class surveyIVRgreetings extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(surveyIVRgreetings.class.getName());
    final int SMS = 1;
    final int USSD = 2;
    final int VOICE = 3;
    final int EMAIL = 4;
    final int SUCCESS = 0;
    final int FAILED = -1;
    static final int RUNNING = 1;
    static final int STOPED = 2;
    static final int CLOSED = 3;
    final int NO = -1;
    final int YES = 1;
    final int OPTION1 = 1;
    final int OPTION2 = 2;
    final int OPTION3 = 3;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        PrintWriter out = response.getWriter();
        String smsid = request.getParameter("SmsSid");
        log.debug("smsid :: "+smsid);
        String accountSid = request.getParameter("AccountSid");
        log.debug("accountSid :: "+accountSid);
        String from = request.getParameter("From");
        log.debug("from :: "+from);
        String to = request.getParameter("To");
        log.debug("to :: "+to);
        String body = request.getParameter("Body");
        log.debug("body :: "+body);
        String fromCity = request.getParameter("FromCity");
        log.debug("fromCity :: "+fromCity);
        String fromState = request.getParameter("FromState");
        log.debug("fromState :: "+fromState);
        String fromZip = request.getParameter("FromZip");
        log.debug("fromZip :: "+fromZip);
        String FromCountry = request.getParameter("FromCountry");
        log.debug("FromCountry :: "+FromCountry);
        String toCity = request.getParameter("ToCity");
        log.debug("toCity :: "+toCity);
        String toState = request.getParameter("ToState");
        log.debug("toState :: "+toState);
        String toZip = request.getParameter("ToZip");
        log.debug("toZip :: "+toZip);
        String toCountry = request.getParameter("ToCountry");
        log.debug("toCountry :: "+toCountry);
        String digits = request.getParameter("Digits");
        log.debug("digits :: "+digits);
        int interactionId = 0;
        AXIOMStatus status = null;
        Interactionsexecutions interaction = null;
        Interactionsexecutions[] interactionsList = null;
        Interactionresponse interactionresponse = null;
        String message = null;

        Survey survey = null;
        String _channelName = this.getServletContext().getContextPath();
        _channelName = _channelName.replaceAll("/", "");

        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);




        Channels channel = cUtil.getChannel(_channelName);
        if (channel == null) {
            sChannel.close();
            suChannel.close();
            return;
        }
        sChannel.close();
        suChannel.close();
        //end of addition

        String channelId = channel.getChannelid();
        SurveyManagement sManagement = new SurveyManagement();
        interactionsList = sManagement.getInteractionsExecutionList(channelId);

        for (int i = 0; i < interactionsList.length; i++) {
            if (interactionsList[i].getStatus() == RUNNING) {
                interaction = interactionsList[i];
                break;
            }
        }
        
         Date currentDate = new Date();
        
        if(currentDate.getTime() - interaction.getExpirydatetime().getTime() <= 0){
            sManagement.changeStatus(interaction.getInteractionid(), channelId, CLOSED);
        }
        TwiMLResponse twiml = new TwiMLResponse();
        interactionresponse = sManagement.getInteractionsResponse(channelId, from, interaction.getInteractionexecutionid());
        // survey = sManagement.getSurveyQuestionsByIRExecution(interaction);
        Say say = new Say(interactionresponse.getGreetings());
        Gather gather = new Gather();
        gather.setAction("/surveyIVRQue?interactionId="+interaction.getInteractionexecutionid()+"&channelId="+channelId);
        gather.setNumDigits(1);
        gather.setMethod("POST");
        try {

            gather.append(say);

            twiml.append(gather);

        } catch (Exception e) {
            log.error("Exception :: ",e);
        }
        response.setContentType("application/xml");
        response.getWriter().print(twiml.toXML());
        log.info("Servlet ended");
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
