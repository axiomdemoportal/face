/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.handler.interactions;

import com.mollatech.axiom.nucleus.db.Interactionresponse;
import com.mollatech.axiom.nucleus.db.Interactionsexecutions;
import com.mollatech.dictum.management.SurveyManagement;
import com.mollatech.axiom.nucleus.db.operation.Survey;
import com.twilio.sdk.verbs.Gather;
import com.twilio.sdk.verbs.Say;
import com.twilio.sdk.verbs.TwiMLResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mollatech2
 */
public class surveyIVRQue extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(surveyIVRQue.class.getName());

    final int SMS = 1;
    final int USSD = 2;
    final int VOICE = 3;
    final int EMAIL = 4;
    final int SUCCESS = 0;
    final int FAILED = -1;
    static final int RUNNING = 1;
    static final int STOPED = 2;
    static final int CLOSED = 3;
    final int NO = -1;
    final int YES = 1;
    final int OPTION1 = 1;
    final int OPTION2 = 2;
    final int OPTION3 = 3;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        String digits = request.getParameter("Digits");
        log.debug("digits :: "+digits);
        String from = request.getParameter("From");
        log.debug("from :: "+from);
        TwiMLResponse twiml = new TwiMLResponse();

        Say say = null;
        int _interactionId = -1;
        String interactionId = request.getParameter("interactionId");
         log.debug("interactionId :: "+interactionId);
        String channelId = request.getParameter("channelId");
        log.debug("channelId :: "+channelId);
        if (interactionId != null) {
            _interactionId = Integer.parseInt(interactionId);
        }

        SurveyManagement sManagement = new SurveyManagement();
        Interactionsexecutions iREObj = new Interactionsexecutions();
        Interactionresponse interactionresponse = null;
        iREObj = sManagement.getInteractionExecutions(_interactionId);
        Survey survey = sManagement.getSurveyQuestionsByIRExecution(iREObj);
        
//         Date currentDate = new Date();
//        
//        if(currentDate.getTime() - iREObj.getExpirydatetime().getTime() <= 0){
//            sManagement.changeStatus(iREObj.getInteractionid(), channelId, CLOSED);
//        }
        interactionresponse = sManagement.getInteractionsResponse(channelId, from, iREObj.getInteractionexecutionid());
        Gather gather = new Gather();
        if (digits != null && digits.equals(survey.greetingsTags[0])) {
            if (interactionresponse.getQuestion1() == null) {
                //user not found
            } else {
                say = new Say(interactionresponse.getQuestion1() + survey.questionAndOptions[0].options[0] + survey.questionAndOptions[0].options[1]);
                gather = new Gather();
                gather.setAction("/surveyIVRQueRun?interactionId=" + iREObj.getInteractionexecutionid() + "&channelId=" + channelId);
                gather.setNumDigits(1);
                gather.setMethod("POST");
                 sManagement.changeInteractionResponse(channelId,interactionresponse.getContactid(), from, iREObj.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), YES, interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 1,SurveyManagement.RUNNING);
                try {
                    gather.append(say);
                    twiml.append(gather);

                } catch (Exception e) {
                    log.error("Exception caught :: ",e);
                }

            }
        } else if (digits != null && digits.equals(survey.greetingsTags[1])) {
            sManagement.changeInteractionResponse(channelId,interactionresponse.getContactid(), from, iREObj.getInteractionexecutionid(), SMS, interactionresponse.getGreetings(), NO, interactionresponse.getOffer(), interactionresponse.getQuestion1(), interactionresponse.getAns1(), interactionresponse.getQuestion2(), interactionresponse.getAns2(), interactionresponse.getQuestion3(), interactionresponse.getAns3(), interactionresponse.getQuestion4(), interactionresponse.getAns4(), interactionresponse.getQuestion5(), interactionresponse.getAns5(), 0,SurveyManagement.CLOSED);

        }
        
          response.setContentType("application/xml");
        response.getWriter().print(twiml.toXML());  
        log.info("Servlet ended");
    }
        
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
