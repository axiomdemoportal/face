/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.handler.interactions;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Contacts;
import com.mollatech.axiom.nucleus.db.Interactions;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.InteractionResponseUtils;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.operation.AxiomMessage;
import com.mollatech.dictum.management.SurveyManagement;
import com.mollatech.axiom.nucleus.db.operation.Survey;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.dictum.management.BulkMSGManagement;
import com.mollatech.dictum.management.ContactManagement;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class executeSurvey extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(executeSurvey.class.getName());
    static final int RUNNING = 1;
    static final int STOPED = 2;
    static final int CLOSED = 3;
    public static int SMS = 1;
    public static int USSD = 2;
    public static int VOICE = 3;
    public static int EMAIL = 4;
    final int PENDING = 0;
    final int SENT = 1;
    final int FAIELD = -1;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");
        log.info("Servlet started");
        PrintWriter out = response.getWriter();

        JSONObject json = new JSONObject();

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.INTERACTIONS) != 0) {
            String result = "error";
            String message = "This feature is not available in this license!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
         log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
         log.debug("sessionId :: "+sessionId);
        Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operator :: "+operator.getName());
        String operatorId = operator.getOperatorid();
        log.debug("operatorId :: "+operatorId);
        String _TagList = request.getParameter("_tagID");
        log.debug("_TagList :: "+_TagList);
        String interactionId = request.getParameter("_irid");
        log.debug("interactionId :: "+interactionId);
        int _interactionId = 0;
        int _speed = 0;
        int _type = 0;
        int retValue = 0;
        int expiryTime = 0;
        if (interactionId != null) {
            _interactionId = Integer.parseInt(interactionId);
            log.debug("_interactionId :: "+_interactionId);
        }

        String _speed1 = request.getParameter("_speed");
        log.debug("_speed1 :: "+_speed1);
        if (_speed1 != null) {
            _speed = Integer.parseInt(_speed1);
            log.debug("_speed :: "+_speed);
        }

        String _expiryTime = request.getParameter("_expiry");
        log.debug("_expiryTime :: "+_expiryTime);
        if (_expiryTime != null) {
            expiryTime = Integer.parseInt(_expiryTime);
            log.debug("expiryTime :: "+expiryTime);
        }

        String _type1 = request.getParameter("_type");
        log.debug("_type1 :: "+_type1);
        if (_type1 != null) {
            _type = Integer.parseInt(_type1);
            log.debug("_type :: "+_type);
        }
        Date _creationTime = new Date();
        String _channelId = channel.getChannelid();
        log.debug("_channelId :: "+_channelId);
        String result = "success";
        String message = "Survey Started....";



        if (_type1 == null || _speed1 == null || _TagList == null
                || interactionId == null) {
            result = "error";
            message = "Fill all Details!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        SurveyManagement sManagement = new SurveyManagement();
        Interactions[] interactionList = null;
        Interactions interactions = sManagement.getInteraction(_interactionId, _channelId);
        Survey survey = sManagement.getSurveyQuestions(interactions);

        if (interactions.getStatus() != RUNNING) {
            interactionList = sManagement.listInteractions(_channelId);
            for (int i = 0; i < interactionList.length; i++) {
                if (interactionList[i].getStatus() == RUNNING) {
                    sManagement.changeStatus(interactionList[i].getInteractionid(), _channelId, STOPED);

                }
                sManagement.changeStatus(_interactionId, _channelId, RUNNING);
            }
        }
        Calendar cal = Calendar.getInstance();

        cal.add(Calendar.HOUR, expiryTime);

        Date expiryDate = cal.getTime();
        int interactionExecutionId = sManagement.addInteractionExecution(_channelId, interactions, expiryDate);
        log.debug("addInteractionExecution :: "+interactionExecutionId);
        InteractionResponseUtils iRUtil = new InteractionResponseUtils();
        ContactManagement contact = new ContactManagement();
        Contacts[] cont = contact.SearchContactsByTag(sessionId, _channelId, _TagList);
        int port = request.getLocalPort();
        String server = request.getServerName();
        String channelname = request.getContextPath();

        String megTemp = null;
        String strSameAsFileNPM = "";
        if (_type == SMS || _type == USSD) {
            megTemp = survey.greetings + " "
                    + survey.greetingsTags[0] + " "
                    + "\n" + survey.greetingOptions[0] + " "
                    + survey.greetingsTags[0] + " "
                    + survey.greetingOptions[1];
        } else if (_type == VOICE) {
            megTemp = survey.greetings + " "
                    + "\n" + survey.greetingOptions[0] + " "
                    + survey.greetingOptions[1];
        }

        BulkMSGManagement bulk = new BulkMSGManagement();
        AxiomMessage[] axiommsgs = new AxiomMessage[cont.length];
        TemplateManagement tManagement = new TemplateManagement();
        Templates templates = new Templates();
        ByteArrayInputStream bais = null;

        for (int i = 0; i < cont.length; i++) {
            if (_type == EMAIL) {
                if (cont[i].getEmailid() != null) {
                    templates = tManagement.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.EMAIL_WEB_SURVEY);
                    bais = new ByteArrayInputStream(templates.getTemplatebody());
                    megTemp = (String) TemplateUtils.deserializeFromObject(bais);
                    String subject = templates.getSubject();
                    if (cont[i].getName() != null) {
                        //int _Format = 1;

                        String url = "http://" + server + ":" + port + channelname + "/interactions/startwebsurvey.jsp?interactionid=" + interactionExecutionId + "&contactDetails=" + URLEncoder.encode(cont[i].getEmailid(), "UTF-8") + "&name=" + URLEncoder.encode(cont[i].getName(), "UTF-8");
                        if (request.isSecure() == true) {
                            url = "https://" + server + ":" + port + channelname + "/interactions/startwebsurvey.jsp?interactionid=" + interactionExecutionId + "&contactDetails=" + URLEncoder.encode(cont[i].getEmailid(), "UTF-8") + "&name=" + URLEncoder.encode(cont[i].getName(), "UTF-8");
                        }

                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                        Date d = new Date();
                        megTemp = megTemp.replaceAll("#name#", cont[i].getName());
                        megTemp = megTemp.replaceAll("#today#", d.toString());
                        megTemp = megTemp.replaceAll("#now#", d.toString());
                        megTemp = megTemp.replaceAll("#phone#", cont[i].getPhone());
                        megTemp = megTemp.replaceAll("#email#", cont[i].getEmailid());
                        megTemp = megTemp.replaceAll("#url#", url);
                        megTemp = megTemp.replaceAll("#channel#", channel.getName());
                        megTemp = megTemp.replaceAll("#datetime#", sdf.format(d));
                        megTemp = megTemp.replaceAll("#greetings#", survey.greetings);
                        megTemp = megTemp.replaceAll("#surveyname#", interactions.getInteractionName());
                        subject = subject.replaceAll("#name#", cont[i].getName());
                        subject = subject.replaceAll("#channel#", channel.getName());
                        subject = subject.replaceAll("#datetime#", sdf.format(d));
                        subject = subject.replaceAll("#surveyname#", interactions.getInteractionName());

                        axiommsgs[i] = new AxiomMessage();

                        axiommsgs[i].phone = cont[i].getPhone();
                        axiommsgs[i].emailid = cont[i].getEmailid();
                        axiommsgs[i].subject = subject;
                        axiommsgs[i].message = megTemp;
                        axiommsgs[i].name = cont[i].getName();

                    }
                } else {
                    if (cont[i].getPhone() != null || cont[i].getName() != null) {

                        templates = tManagement.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.MOBILE_WEB_SURVEY);
                        bais = new ByteArrayInputStream(templates.getTemplatebody());
                        megTemp = (String) TemplateUtils.deserializeFromObject(bais);
                        //int _Format = 1;
                        String url = "http://" + server + ":" + port + channelname + "/interactions/startwebsurvey.jsp?interactionid=" + interactionExecutionId + "&contactDetails=" + URLEncoder.encode(cont[i].getPhone(), "UTF-8") + "&name=" + URLEncoder.encode(cont[i].getName(), "UTF-8");
                        if (request.isSecure() == true) {
                            url = "https://" + server + ":" + port + channelname + "/interactions/startwebsurvey.jsp?interactionid=" + interactionExecutionId + "&contactDetails=" + URLEncoder.encode(cont[i].getPhone(), "UTF-8") + "&name=" + URLEncoder.encode(cont[i].getName(), "UTF-8");
                        }

                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                        Date d = new Date();
                        megTemp = megTemp.replaceAll("#name#", cont[i].getName());
                        megTemp = megTemp.replaceAll("#today#", d.toString());
                        megTemp = megTemp.replaceAll("#now#", d.toString());
                        megTemp = megTemp.replaceAll("#phone#", cont[i].getPhone());
                        megTemp = megTemp.replaceAll("#email#", cont[i].getEmailid());
                        megTemp = megTemp.replaceAll("#greetings#", survey.greetings);
                        megTemp = megTemp.replaceAll("#url#", url);
                        megTemp = megTemp.replaceAll("#channel#", channel.getName());
                        megTemp = megTemp.replaceAll("#datetime#", sdf.format(d));
                        megTemp = megTemp.replaceAll("#surveyname#", interactions.getInteractionName());

                        axiommsgs[i] = new AxiomMessage();

                        axiommsgs[i].phone = cont[i].getPhone();
                        axiommsgs[i].emailid = cont[i].getEmailid();
                        axiommsgs[i].message = megTemp;
                        axiommsgs[i].name = cont[i].getName();
                    }

                    //  strSameAsFileNPM += cont[i].getName() + "," + cont[i].getPhone()  + "," + megTemp + "\n";
                }

            }

//            retValue = bulk.ADDAxiomMessage(sessionId, channel.getChannelid(), operator.getOperatorid(), axiommsgs[i], PENDING, new Date(), new Date(), _type, _speed);
//        for (int i = 0; i < cont.length; i++) {
            if (_type == SMS || _type == USSD || _type == VOICE) {
                if (cont[i].getPhone() != null || cont[i].getName() != null) {
                    //int _Format = 1;
//                    String url = "http//:" + request.getRemoteAddr() + ":" + request.getRemotePort() + "/face/startwebsurvey.jsp?interactionid=" + interactionId + "&contactDetails=" + cont[i].getPhone();
//                    if (request.isSecure() == true) {
//                        url = "https//:" + request.getRemoteAddr() + ":" + request.getRemotePort() + "/face/startwebsurvey.jsp?interactionid=" + interactionId + "&contactDetails=" + cont[i].getPhone();
//                    }
                    String subject = "";
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                    Date d = new Date();
                    megTemp = megTemp.replaceAll("#name#", cont[i].getName());
                    megTemp = megTemp.replaceAll("#today#", d.toString());
                    megTemp = megTemp.replaceAll("#now#", d.toString());
                    megTemp = megTemp.replaceAll("#phone#", cont[i].getPhone());
                    megTemp = megTemp.replaceAll("#email#", cont[i].getEmailid());

                    megTemp = megTemp.replaceAll("#channel#", channel.getName());
                    megTemp = megTemp.replaceAll("#datetime#", sdf.format(d));

                    axiommsgs[i] = new AxiomMessage();

                    axiommsgs[i].phone = cont[i].getPhone();
                    axiommsgs[i].emailid = cont[i].getEmailid();
                    axiommsgs[i].subject = subject;
                    axiommsgs[i].message = megTemp;
                    axiommsgs[i].name = cont[i].getName();

                }
            }

            if (cont.length > 0) {
                if (_type == SurveyManagement.WEB) {
                    if (cont[i].getEmailid() == null) {
                        if (templates.getStatus() == tManagement.ACTIVE_STATUS) {
                            retValue = sManagement.ADDMSGv2(sessionId, _channelId, axiommsgs[i].name, interactionExecutionId,
                                    axiommsgs[i].phone,
                                    megTemp,
                                    survey.offer,
                                    survey.questionAndOptions[0].question,
                                    survey.questionAndOptions[1].question,
                                    survey.questionAndOptions[2].question,
                                    survey.questionAndOptions[3].question,
                                    survey.questionAndOptions[4].question,
                                    PENDING,
                                    _creationTime,
                                    new Date(),
                                    _type,
                                    _speed);
                        }
                    } else {
                        if (templates.getStatus() == tManagement.ACTIVE_STATUS) {
                            retValue = sManagement.ADDMSGv2(sessionId, _channelId, axiommsgs[i].name, interactionExecutionId,
                                    axiommsgs[i].emailid,
                                    megTemp,
                                    survey.offer,
                                    survey.questionAndOptions[0].question,
                                    survey.questionAndOptions[1].question,
                                    survey.questionAndOptions[2].question,
                                    survey.questionAndOptions[3].question,
                                    survey.questionAndOptions[4].question,
                                    PENDING,
                                    _creationTime,
                                    new Date(),
                                    _type,
                                    _speed);
                        }
                    }
                }
            } else {
                if (templates.getStatus() == tManagement.ACTIVE_STATUS) {
                    retValue = sManagement.ADDMSGv2(sessionId, _channelId, axiommsgs[i].name, interactionExecutionId,
                            axiommsgs[i].phone,
                            megTemp,
                            survey.offer,
                            survey.questionAndOptions[0].question,
                            survey.questionAndOptions[1].question,
                            survey.questionAndOptions[2].question,
                            survey.questionAndOptions[3].question,
                            survey.questionAndOptions[4].question,
                            PENDING,
                            _creationTime,
                            new Date(),
                            _type,
                            _speed);
                }
            }

        }
//        if (_type == SMS || _type == USSD || _type == VOICE) {
        if (templates.getStatus() == tManagement.ACTIVE_STATUS) {
            retValue = sManagement.SendMSG(sessionId, channel.getChannelid(), _type, _speed);
            log.debug("SendMSG :: "+retValue);
        }
//        } else if (_type == EMAIL) {
//            retValue = bulk.SendMSG(sessionId, channel.getChannelid(), EMAIL, _speed);
//        }
        /*if (strSameAsFileNPM != null && strSameAsFileNPM.isEmpty() == false) {
         retValue = sManagement.AddMSG(sessionId, _channelId, strSameAsFileNPM, megTemp, survey.offer, survey.questionAndOptions[0].question, survey.questionAndOptions[1].question, survey.questionAndOptions[2].question, survey.questionAndOptions[3].question, survey.questionAndOptions[4].question, PENDING, _creationTime, new Date(), _type, _speed);
         }*/
        if (retValue
                != 0) {
            result = "error";
            message = "Failed to Start Survey!!";

        }

        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }

    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
