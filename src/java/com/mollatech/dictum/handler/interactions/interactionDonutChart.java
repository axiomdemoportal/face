/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.handler.interactions;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.face.common.donut;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.dictum.management.SurveyManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mollatech1
 */
public class interactionDonutChart extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(interactionDonutChart.class.getName());
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      
        try {
             response.setContentType("application/json");
            PrintWriter out = response.getWriter();
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel.getName());
            String _questionNo = request.getParameter("_questionNo");
            log.debug("_questionNo :: "+_questionNo);
              String _interactionExecutionID = request.getParameter("_interactionExecutionID");
               log.debug("_interactionExecutionID :: "+_interactionExecutionID);
            int _iquestionNo = -9999;
            if (_questionNo != null) {
                _iquestionNo = Integer.parseInt(_questionNo);
            }
               int _iinteractionExecutionID = -9999;
            if (_interactionExecutionID != null) {
                _iinteractionExecutionID = Integer.parseInt(_interactionExecutionID);
            }
            SurveyManagement survey = new SurveyManagement();
             ArrayList<donut> sample = new ArrayList<donut>();
              int successCount = 0;
              int failedCount = 0;
              int dropout = 999;
              int greetingCount = 0;
            int qustion1 = 0;
            int qustion2 = 0;
            int qustion3 = 0;
            int qustion4 = 0;
            int qustion5 = 0;
              if(_iquestionNo == 999){
                     greetingCount = survey.getDropCount(channel.getChannelid(), _iinteractionExecutionID, 0);
                    qustion1 = survey.getDropCount(channel.getChannelid(), _iinteractionExecutionID, 1);
                    qustion2 = survey.getDropCount(channel.getChannelid(), _iinteractionExecutionID, 2);
                    qustion3 = survey.getDropCount(channel.getChannelid(), _iinteractionExecutionID, 3);
                    qustion4 = survey.getDropCount(channel.getChannelid(), _iinteractionExecutionID, 4);
                    qustion5 = survey.getDropCount(channel.getChannelid(), _iinteractionExecutionID, 5);

                    sample.add(new donut(greetingCount, "Gretings"));
                    sample.add(new donut(qustion1, "Que1"));
                    sample.add(new donut(qustion2, "Que2"));
                    sample.add(new donut(qustion3, "Que3"));
                    sample.add(new donut(qustion4, "Que4"));
                    sample.add(new donut(qustion5, "Que5"));

              }else{
                successCount = survey.getCountOfAnswersGivenToQueston(channel.getChannelid(), _iinteractionExecutionID, _iquestionNo);
                failedCount = survey.getCountOfAnswersNotGivenToQueston(channel.getChannelid(), _iinteractionExecutionID, _iquestionNo);
              }
             sample.add(new donut(successCount, "Answered"));
             sample.add(new donut(failedCount, "Not Answered"));
              Gson gson = new Gson();

                JsonElement element = gson.toJsonTree(sample, new TypeToken<List<donut>>() {
                }.getType());

                JsonArray jsonArray = element.getAsJsonArray();
                out.print(jsonArray);

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        }
        log.info("Servlet ended");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
