package com.mollatech.dictum.handler.interactions;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.dictum.management.SurveyManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class changestatusinteractionexe extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(changestatusinteractionexe.class.getName());
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String interactionId = request.getParameter("_irid");
        log.debug("interactionId :: "+interactionId);
         int _interactionId = 0;
        
        if(interactionId != null){
         _interactionId = Integer.parseInt(interactionId);
         log.debug("_interactionId :: "+_interactionId);
        }
        
        String status = request.getParameter("_status");
        log.debug("status :: "+status);
         int _status = -1;
        
        if(status != null){
         _status = Integer.parseInt(status);
        }
        String result = "success";
        String message = "Survey status change successfully....";

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        if (interactionId == null) {
            result = "error";
            message = "Fill all Details!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
                log.error("Exception caught");
            }

            out.print(json);
            out.flush();
            return;
        }

        int retValue = -1;
        SurveyManagement sManagement = new SurveyManagement();
        
        try {
           retValue = sManagement.changeStatus(_interactionId, channel.getChannelid(),_status);
           log.debug("changeStatus :: "+retValue);
        } catch (Exception ex) {
            // TODO handle custom exceptions here
            log.error("Exception caught :: ",ex);
        }



        if (retValue != 0) {
            result = "error";
            message = "Failed to change status!!";
            
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
            
        }catch(Exception e){
            log.error("Exception caught",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
