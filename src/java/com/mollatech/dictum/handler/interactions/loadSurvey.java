/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.handler.interactions;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Interactions;
import com.mollatech.dictum.management.SurveyManagement;
import com.mollatech.axiom.nucleus.db.operation.Survey;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class loadSurvey extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadSurvey.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");
        log.info("Servlet started");
        PrintWriter out = response.getWriter();
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String interactionId = request.getParameter("_interactionId");
        log.debug("interactionId :: "+interactionId);
        int _interactionId = 0;

        if (interactionId != null) {
            _interactionId = Integer.parseInt(interactionId);
        }
        String result = "success";
        String message = "Survey Started....";

        JSONObject json = new JSONObject();

        SurveyManagement sManagement = new SurveyManagement();
        Interactions iObj = sManagement.getInteraction(_interactionId, channel.getChannelid());

        if (iObj != null) {

            Survey survey = sManagement.getSurveyQuestions(iObj);
            try {
                json.put("result", result);
                json.put("message", message);
                json.put("interactionName", "");
                json.put("greetings", survey.greetings);
                json.put("greetingTags1", survey.greetingsTags[0]);
                json.put("greetingTags2", survey.greetingsTags[1]);
                json.put("offer", survey.offer);
                json.put("offerTags1", survey.offerTags[0]);
                json.put("offerTags2", survey.offerTags[1]);
                json.put("status", iObj.getStatus());
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }

            for (int i = 1; i <= survey.questionAndOptions.length; i++) {

                try { json.put("question" + i, survey.questionAndOptions[i - 1].question);
                }catch(Exception e){
                    log.error("Exception caught :: ",e);
                }
                

                for (int j = 1; j < 4; j++) {
                    
                    try {     
                    json.put("_options" + j + "Msg4que" + i, survey.questionAndOptions[i - 1].options[j - 1]);
                    json.put("_options" + j + "Tag4que" + i, survey.questionAndOptions[i - 1].optionTags[j - 1]);
                    }catch(Exception e){
                        log.error("Exception caught :: ",e);
                    }

                }
            }

        } else {
            result = "error";
            message = "failed to load Survey..";
            try {
                json.put("result", result);
                json.put("message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
        }

        try {
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
