/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.dictum.handler;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Contacts;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.operation.AxiomMessage;
import com.mollatech.axiom.nucleus.db.operation.AxiomOperator;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.ContentFilterSetting;
import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import static com.mollatech.dictum.handler.bulkmsg.VOICE;
import com.mollatech.dictum.management.BulkMSGManagement;
import com.mollatech.dictum.management.ContactManagement;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

public class googlepushnotification extends HttpServlet {
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(googlepushnotification.class.getName());
  public static int ANDROIDPUSHNOTIFICATION = 10;
   public static int IOSPUSHNOTIFICATION = 11;
    final int PENDING = 0;
    final int SENT = 1;
    final int FAIELD = -1;
    final int FAILED = -1;
    final int BLOCKED = -121;
    final String itemType = "BULK_MSG";
 
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      response.setContentType("application/json");
      log.info("Servlet started");
        PrintWriter out = response.getWriter();
        try {
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId :: "+sessionId);
            Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operator :: "+operator.getOperatorid());
            String operatorId = operator.getOperatorid();
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("remoteaccesslogin :: "+remoteaccesslogin);
            String _TagList = request.getParameter("_tagID");
            log.debug("_TagList :: "+_TagList);
            String tag = "";
             int _type = 0;
//            if(_type1 != null){
//             _type = Integer.parseInt(_type1);
//            }
            if(_TagList.equals("IOSPushNotify")){
                _type = 11;
            }else  if(_TagList.equals("AndroidPushNotify")){
                _type = 10;
            }
          
            String _channelId = channel.getChannelid();
            String _msgBody = request.getParameter("_messagebody");
            log.debug("_msgBody :: "+_msgBody);
            String _speed1 = request.getParameter("_speed");
            log.debug("_speed1 :: "+_speed1);
            int _speed = Integer.parseInt(_speed1);
            String _type1 = request.getParameter("_type");
           log.debug("_type1 :: "+_type1);
            Contacts[] cont = null;

            int retValue = -1;
            int result = -1;
            BulkMSGManagement bulksms = new BulkMSGManagement();
//            PushNotificationDeviceManagement bulksms = new BulkMSGManagement();
            AuditManagement audit = new AuditManagement();
            ContactManagement contact = new ContactManagement();
            SettingsManagement sMngmt = new SettingsManagement();
            SendNotification send = new SendNotification();
            TemplateManagement tObj = new TemplateManagement();

            AxiomMessage[] axiommessage = null;
            Object obj = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.GlobalSettings, 1);

            if (obj != null) {
                GlobalChannelSettings cObj = (GlobalChannelSettings) obj;
                String strword = sMngmt.checkcontent( channel.getChannelid(), _msgBody,cObj);
                if (strword != null && cObj.contentstatus == 0) {
                    if (cObj.contentalertstatus == 0) {
                        Templates templatesObj = tObj.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.EMAIL_CONTENT_BLOCKED_ALERT);
                   if(templatesObj.getStatus() == tObj.ACTIVE_STATUS){
                        ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                        String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                        String strsubject = templatesObj.getSubject();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                        if (strmessageBody != null) {
                            // Date date = new Date();
                            strmessageBody = strmessageBody.replaceAll("#name#", operator.getName());
                            strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                            strmessageBody = strmessageBody.replaceAll("#email#", operator.getEmailid());
                            strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                            strmessageBody = strmessageBody.replaceAll("#filterword#", strword);
                            strmessageBody = strmessageBody.replaceAll("#alertmsgbody#", _msgBody);

                        }
                        OperatorsManagement oObj = new OperatorsManagement();
                        AxiomOperator[] aOperator = oObj.ListOperatorsInternal(channel.getChannelid());
                        AxiomOperator channelOp = null;
                        for (int i = 0; i < aOperator.length; i++) {
                            if (aOperator[i].getStrRoleName().equals("admin")) {
                                channelOp = aOperator[i];
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), channelOp.getStrEmail(), strsubject, strmessageBody,
                                        null, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }
                        }
                        result = BLOCKED;
                    }
                   result = BLOCKED;
                    }
                    result = BLOCKED;

                }else{
                cont = contact.SearchContactsByTag(sessionId, _channelId, _TagList,0);
                axiommessage = new AxiomMessage[cont.length];
                }
            } else {
                cont = contact.SearchContactsByTag(sessionId, _channelId, _TagList,0);
                axiommessage = new AxiomMessage[cont.length];
            }


//            AxiomMessage[] axiommessage = new AxiomMessage[cont.length];

            if (cont != null) {
                for (int i = 0; i < cont.length; i++) {
                    String msgTemp = _msgBody;
                    if (cont[i].getPhone() != null) {
                        Date d = new Date();
                        if (cont[i].getName() != null) {
                            msgTemp = msgTemp.replaceAll("#name#", cont[i].getName());
                        }
                        if (d.toString() != null) {
                            msgTemp = msgTemp.replaceAll("#date#", d.toString());
                            msgTemp = msgTemp.replaceAll("#datetime#", d.toString());
                        }
                        if (cont[i].getEmailid() != null) {
                            msgTemp = msgTemp.replaceAll("#email#", cont[i].getEmailid());
                        }

                        if (_type == VOICE) {

                            if (cont[i].getPhone() != null) {
                                String str = cont[i].getPhone();
                                String strpPhone = str.replace("", " ");
                                msgTemp = msgTemp.replaceAll("#phone#", strpPhone);
                            }

                        } else {
                            if (cont[i].getPhone() != null) {
                                msgTemp = msgTemp.replaceAll("#phone#", cont[i].getPhone());
                            }
                        }


                    } else {
                        Date d = new Date();

                        if (d.toString() != null) {
                            msgTemp = msgTemp.replaceAll("#date#", d.toString());
                            msgTemp = msgTemp.replaceAll("#datetime#", d.toString());
                        }
                        if (cont[i].getEmailid() != null) {
                            msgTemp = msgTemp.replaceAll("#email#", cont[i].getEmailid());
                        }
                        if (cont[i].getPhone() != null) {
                            msgTemp = msgTemp.replaceAll("#phone#", cont[i].getPhone());
                        }

                    }

                    axiommessage[i] = new AxiomMessage();
                    //System.out.println(axiommessage);
                    if (cont[i].getName() != null) {

                        axiommessage[i].name = cont[i].getName();
                    }
                    if (cont[i].getPhone() != null) {

                        axiommessage[i].phone = cont[i].getPhone();

                    }
                    if (cont[i].getEmailid() != null) {
                        axiommessage[i].emailid = cont[i].getEmailid();
                    }
                     if (cont[i].getGoogleregisterid() != null) {
                        axiommessage[i].googleregid = cont[i].getGoogleregisterid();
                    }
                    if (msgTemp != null) {
                        axiommessage[i].message = msgTemp;
                    }

                }
                
                  String uniqueid = request.getSession().getId() + channel.getChannelid()+new Date().getTime() + sessionId;
                  String uniqueidforbulk = new String(Base64.encode(UtilityFunctions.SHA1(uniqueid)));
                for (int i = 0; i < axiommessage.length; i++) {
                    retValue = bulksms.ADDAxiomMessage(sessionId, channel.getChannelid(), operator.getOperatorid(), axiommessage[i], PENDING, new Date(), new Date(), _type, _speed,uniqueidforbulk);
                }
                result = bulksms.SendPushNotification(sessionId, _channelId, _type, _speed,uniqueidforbulk);
            }

            String resultstr = "ERROR";
            if (result == 0) {
                resultstr = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operator.getName(), new Date(), "Add and Send Bulk PUSH MSG...!", resultstr, result,
                        "Bulk Push Message Management", "", "Bulk PUSH Msg Added Successfully", itemType, operatorId);
            } else if (result == BLOCKED) {
                resultstr = "Blocked";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operator.getName(), new Date(), "Add and Send Bulk MSG...!", resultstr, result,
                        "Bulk Push Message Management", "", "Message Blocked by Content Filter Setting ...!", itemType, operatorId);
            } else if (result != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operator.getName(), new Date(), "Add and Send Bulk MSG...!", resultstr, result,
                        "Bulk Push Message Management", "", "Failed To Add Bulk PUSH MSG", itemType, operatorId);
            }

            if (result == 0) {
                String strresult = "success";
                String message = "Messages are queued and getting sent in background!!!";
                JSONObject json = new JSONObject();
                try { json.put("_result", strresult);
                json.put("_message", message);
                }catch(Exception e){
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();

            } else if (result == BLOCKED) {
                String strresult = "Blocked";
                String message = "Message Blocked by Content Filter Setting ...!";
                JSONObject json = new JSONObject();
                try { json.put("_result", strresult);
                json.put("_message", message);
                }catch(Exception e){
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();

            } else {
                String message = "error";
                JSONObject json = new JSONObject();
                try { json.put("_result", "error");
                json.put("_message", message);
                }catch(Exception e){
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();
            }


        } catch (Exception ex) {
          log.error("Exception caught :: ",ex);
      } finally {
            out.close();
            log.info("Servlet ended");
        }
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
