package com.mollatech.dictum.handler;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class FileUploadServlet extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FileUploadServlet.class.getName());
    private static final long serialVersionUID = 1L;
    // location to store file uploaded
    private static final String UPLOAD_DIRECTORY = "uploads";
    // upload settings
    private static final int MEMORY_THRESHOLD = 1024 * 1024 * 3;  // 3MB
    private static final int MAX_FILE_SIZE = 1024 * 1024 * 40; // 40MB
    private static final int MAX_REQUEST_SIZE = 1024 * 1024 * 50; // 50MB

    /**
     * Upon receiving file upload submission, parses the request to read upload
     * data and saves the file on disk.
     */
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        // checks if the request actually contains upload file
        log.info("Servlet started");
        if (!ServletFileUpload.isMultipartContent(request)) {
            // if not, we stop here
            PrintWriter writer = response.getWriter();
            writer.println("Error: Form must has enctype=multipart/form-data.");
            writer.flush();
            return;
        }

        // configures upload settings
        DiskFileItemFactory factory = new DiskFileItemFactory();
        // sets memory threshold - beyond which files are stored in disk 
        factory.setSizeThreshold(MEMORY_THRESHOLD);
        // sets temporary location to store files
        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

        ServletFileUpload upload = new ServletFileUpload(factory);

        // sets maximum size of upload file
        upload.setFileSizeMax(MAX_FILE_SIZE);

        // sets maximum size of request (include file + form data)
        upload.setSizeMax(MAX_REQUEST_SIZE);

        // constructs the directory path to store upload file
        // this path is relative to application's directory
//        String uploadPath = getServletContext().getRealPath("")
//                + File.separator + UPLOAD_DIRECTORY;

        //      String uploadPath = "D:\\uploadbulk" + UPLOAD_DIRECTORY;

        String savepath = "";

        savepath = System.getProperty("catalina.home");
        if(savepath == null) { savepath = System.getenv("catalina.home"); }
        savepath += System.getProperty("file.separator");
        savepath += "axiomv2-settings";
        savepath += System.getProperty("file.separator");
        savepath += UPLOAD_DIRECTORY;
        savepath += System.getProperty("file.separator");

        String uploadPath = savepath;

        //String uploadPath = "D:\\" + UPLOAD_DIRECTORY;
//    String uploadPath =  new File(
//                LoadSettings.g_sSettings.getProperty("axiom.bulkmsg.uploadfiledir")
//                          ).getPath();
//        // creates the directory if it does not exist
        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }

        try {
            // parses the request's content to extract file data
            @SuppressWarnings("unchecked")
            List<FileItem> formItems = upload.parseRequest(request);
            List<String> filePathList = new ArrayList<String>();
            List<String> mimeTypeList = new ArrayList<String>();
            if (formItems != null && formItems.size() > 0) {
                // iterates over form's fields
                for (FileItem item : formItems) {

                    // processes only fields that are not form fields
                    if (!item.isFormField()) {

                        String fileName = new File(item.getName()).getName();
                        String filePath = uploadPath + File.separator + fileName;
                        filePathList.add(filePath);
                        File storeFile = new File(filePath);                        
                        String mimeType = storeFile.toURI().toURL().openConnection().getContentType();
                        mimeTypeList.add(mimeType);
                        // saves the file on disk
                        item.write(storeFile);
                        request.setAttribute("message",
                                "Upload has been done successfully!");


                    }
                }
                String attachmentpath = null;
                for (int i = 0; i < filePathList.size() - 1; i++) {
                    if (attachmentpath == null) {
                        attachmentpath = filePathList.get(i);
                    } else {
                        attachmentpath = attachmentpath + "," + filePathList.get(i);
                    }
                }
                String mimeType = null;
                for (int i = 0; i < mimeTypeList.size() - 1; i++) {
                    if (mimeType == null) {
                        mimeType = mimeTypeList.get(i);
                    } else {
                        mimeType = mimeType + "," + mimeTypeList.get(i);
                    }
                }
                HttpSession session = request.getSession(true);
                session.setAttribute("_filepath", filePathList.get(filePathList.size() - 1));
                session.setAttribute("_attchmentuploadPath", attachmentpath);
                session.setAttribute("_mimetype", mimeType);

                //   System.out.println(filePath);
            }
        } catch (Exception ex) {
            request.setAttribute("message",
                    "There was an error: " + ex.getMessage());
            log.error("Servlet ended");
        }
        // redirects client to message page
        getServletContext().getRequestDispatcher("/bulkmsg.jsp").forward(request, response);
        log.info("Servlet ended");
    }
}