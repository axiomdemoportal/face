/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.handler;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Contacts;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.operation.AxiomMessage;
import com.mollatech.axiom.nucleus.db.operation.AxiomOperator;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.ContentFilterSetting;
import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import com.mollatech.dictum.management.BulkMSGManagement;
import com.mollatech.dictum.management.ContactManagement;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech1
 */
public class sendfax extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(sendfax.class.getName());
    final int PENDING = 0;
    final int SENT = 1;
    final int FAIELD = -1;
    final int FAILED = -1;
    final String itemType = "BULK_FAX";
    final int FAX_BY_BODY = 5;
    final int FAX_WITH_ATTACHMENT = 6;
    final int BLOCKED = -121;
    final int faxPENDING = 2;
    final int FAX_WITH_BODY = 5;
    final int SPEED = 1;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        PrintWriter out = response.getWriter();
        try {

            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId :: "+sessionId);
            Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operator :: "+operator.getName());
            String operatorId = operator.getOperatorid();

            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("remoteaccesslogin :: "+remoteaccesslogin);
            String _type1 = request.getParameter("_type");
            log.debug("_type1 :: "+_type1);
            int _type = Integer.parseInt(_type1);
            String _To = request.getParameter("_to");
            log.debug("_To :: "+_To);
//            String _msgBody = request.getParameter("_messageemailbody");
            String _msgBody = null;
               if(_type == FAX_BY_BODY){
                 _msgBody = request.getParameter("emailCotents");
                 log.debug("_msgBody :: "+_msgBody);
                  //System.out.println(_msgBody);
              }
            
            String _fileName = (String) request.getSession().getAttribute("_filepath");
            //System.out.println(_fileName);
            log.debug("_fileName :: "+_fileName);
            

            SendNotification send = new SendNotification();
            AuditManagement audit = new AuditManagement();

            AXIOMStatus astatus = new AXIOMStatus();
            BulkMSGManagement bulksms = new BulkMSGManagement();
            SettingsManagement sMngmt = new SettingsManagement();
//            SendNotification send = new SendNotification();
            TemplateManagement tObj = new TemplateManagement();
            Contacts[] cont = null;
            int retValue = -1;

            Object obj = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.GlobalSettings, 1);
            int result = -1;
            AxiomMessage axiommessage = new AxiomMessage();
            if (obj != null) {
                GlobalChannelSettings cObj = (GlobalChannelSettings) obj;
                String strword = sMngmt.checkcontent(channel.getChannelid(), _msgBody,cObj);
                if (strword != null && cObj.contentstatus == 0) {
                    if (cObj.contentalertstatus == 0) {
                        Templates templatesObj = tObj.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.EMAIL_CONTENT_BLOCKED_ALERT);
                    if(templatesObj.getStatus() == tObj.ACTIVE_STATUS){
                        ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                        String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                        String strsubject = templatesObj.getSubject();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                        if (strmessageBody != null) {
                            // Date date = new Date();
                            strmessageBody = strmessageBody.replaceAll("#name#", operator.getName());
                            strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                            strmessageBody = strmessageBody.replaceAll("#email#", operator.getEmailid());
                            strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                            strmessageBody = strmessageBody.replaceAll("#filterword#", strword);
                            strmessageBody = strmessageBody.replaceAll("#alertmsgbody#", _msgBody);

                        }
                        OperatorsManagement oObj = new OperatorsManagement();
                        AxiomOperator[] aOperator = oObj.ListOperatorsInternal(channel.getChannelid());
                        AxiomOperator channelOp = null;
                        for (int i = 0; i < aOperator.length; i++) {
                            if (aOperator[i].getStrRoleName().equals("admin")) {
                                channelOp = aOperator[i];
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), channelOp.getStrEmail(), strsubject, strmessageBody,
                                        null, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }
                        }
                       
                     astatus.iStatus = BLOCKED;
                    }
                    astatus.iStatus = BLOCKED;
                            
                    }
                    astatus.iStatus = BLOCKED;

                }else{
                    if (_type == FAX_WITH_BODY) {
                        
                    astatus = send.SendOnFAXNoWaiting(channel.getChannelid(), _To, _msgBody, _type, SPEED);
                } else if (_type == FAX_WITH_ATTACHMENT) {
                    astatus = send.SendOnFAXAttachmentNoWaiting(channel.getChannelid(), _To, _fileName, _type, SPEED);
                }
                }
            } else {
                if (_type == FAX_WITH_BODY) {
                    astatus = send.SendOnFAXNoWaiting(channel.getChannelid(), _To, _msgBody, _type, SPEED);
                } else if (_type == FAX_WITH_ATTACHMENT) {
                    astatus = send.SendOnFAXAttachmentNoWaiting(channel.getChannelid(), _To, _fileName, _type, SPEED);
                }
            }

           
            String resultstr = "ERROR";
            if (astatus.iStatus == 0) {
                resultstr = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operator.getName(), new Date(), "Send FAX...!", resultstr, astatus.iStatus,
                        "Bulk Message Management", "", "Fax Msg Added Successfully", itemType, operatorId);
            } else if (astatus.iStatus == BLOCKED) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operator.getName(), new Date(), "Send FAX...!", resultstr, astatus.iStatus,
                        "Bulk Message Management", "", "Message Blocked by Content Filter Setting ...!", itemType, operatorId);
            } else if (astatus.iStatus == faxPENDING) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operator.getName(), new Date(), "Send FAX...!", resultstr, astatus.iStatus,
                        "Bulk Message Management", "", "Fax Pending", itemType, operatorId);
            } else if(astatus.iStatus != 0 && astatus.iStatus != 2) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operator.getName(), new Date(), "Send FAX...!", resultstr, astatus.iStatus,
                        "Bulk Message Management", "", "Fax sending failed", itemType, operatorId);
            }

            if (astatus.iStatus == 0) {

                String strresult = "success";
                String message = "Fax sent";
                JSONObject json = new JSONObject();
                try { json.put("_result", strresult);
                json.put("_message", message);
                }catch(Exception e){
                   log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();

            } else if (astatus.iStatus == BLOCKED) {
                String strresult = "Blocked";
                String message = "Fax Blocked By Content Filter ...!!!";
                JSONObject json = new JSONObject();
                try { json.put("_result", strresult);
                json.put("_message", message);
                }catch(Exception e){
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();

            } else if (astatus.iStatus == faxPENDING) {
                String strresult = "success";
                String message = "Fax Sent..!!!";
                JSONObject json = new JSONObject();
                try { json.put("_result", strresult);
                json.put("_message", message);
                }catch(Exception e){
                   log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();

            } else {
                String message = "error";
                JSONObject json = new JSONObject();
                try { json.put("_result", "error");
                json.put("_message", message);
                }catch(Exception e){
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();
            }

        } catch (Exception ex) {
           log.error("Exception caught :: ",ex);
        } finally {
            out.close();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
