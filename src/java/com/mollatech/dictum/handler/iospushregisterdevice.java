/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.dictum.handler;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.management.PushNotificationDeviceManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;

import com.mollatech.dictum.management.ContactManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author mollatech1
 */
public class iospushregisterdevice extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(iospushregisterdevice.class.getName());
     public static final int USERID = 1;
    public static final int PHONENUMBER = 2;
    public static final int EMAILID = 3;

    public static final int ACTIVE = 1;
    public static final int SUSPENDED = -1;
    public static final int ANDROID = 1;
    public static final int IOS = 2;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
     protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        try {

            String name = request.getParameter("name");
            log.debug("name :: "+name);
            String email = request.getParameter("email");
            log.debug("email :: "+email);
            String gcm_regid = request.getParameter("regId");
            log.debug("gcm_regid :: "+gcm_regid);
            String error = "failed to register!!";
            int errorcode = -1;
            try { json.put("_error", error);
            json.put("_errorcode", errorcode);
            }catch(Exception e){
                log.error("Exception :: ",e);
            }
            /**
             * Registering a user device Store reg id in users table
             *
             */

            if (gcm_regid == null || email == null) {
                error = "Invalid Parameters!!!";
                errorcode = -2;
                try { json.put("_error", error);
                json.put("_errorcode", errorcode);
                }catch(Exception e){
                    log.error("Exception :: ",e);
                }
//                out.print(json);
//                out.flush();
                return;

            }

            String _channelName = this.getServletContext().getContextPath();
            _channelName = _channelName.replaceAll("/", "");

            SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
            Session sChannel = suChannel.openSession();
            ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);

            com.mollatech.axiom.nucleus.db.Channels channel = cUtil.getChannel(_channelName);
            if (channel == null) {
                sChannel.close();
                suChannel.close();
                error = "Channel not found";
                errorcode = -3;
                try { json.put("_error", error);
                json.put("_errorcode", errorcode);
                }catch(Exception e){
                   log.error("Exception :: ",e);
                }
                out.print(json);
                out.flush();
                return;

            }

            SessionManagement sManagement = null;
            String sessionId = null;

            sChannel.close();
            suChannel.close();
            //end of addition

            String channelId = channel.getChannelid();

            AuthUser aUser = null;

            SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
            Session sRemoteAcess = suRemoteAcess.openSession();
            RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);

            String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
            if (credentialInfo == null) {
                sRemoteAcess.close();
                suRemoteAcess.close();
                sChannel.close();
                suChannel.close();
                 error = "Failed to load remote details!!!";
                errorcode = -4;
                try { json.put("_error", error);
                json.put("_errorcode", errorcode);
                }catch(Exception e){
                    log.error("Exception :: ",e);
                }
                out.print(json);
                out.flush();
                return;
            }
            sManagement = new SessionManagement();
            sessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());

            if (sessionId == null) {
                sRemoteAcess.close();
                suRemoteAcess.close();
                sChannel.close();
                suChannel.close();
                 error = "Sessionid is not generated yet!!!";
                errorcode = -5;
                try { json.put("_error", error);
                json.put("_errorcode", errorcode);
                }catch(Exception e){
                    log.error("Exception :: ",e);
                }
                out.print(json);
                out.flush();
                return;

            }

            sRemoteAcess.close();
            suRemoteAcess.close();
            UserManagement uManagement = new UserManagement();
            
            PushNotificationDeviceManagement pManagement = new PushNotificationDeviceManagement();
            int result = -1;
            if(LoadSettings.g_sSettings.getProperty("product.type").equals("3")){
          //  if(1==0){
                //System.out.println("In axiom Protect");
                aUser = uManagement.CheckUserByType(sessionId, channelId, email, EMAILID);
            if (aUser == null) {
                error = "User Not found!!!";
                errorcode = -3;
                try { json.put("_error", error);
                json.put("_errorcode", errorcode);
                }catch(Exception e){
                    log.error("Exception :: ",e);
                }
                out.print(json);
                out.flush();
                return;
            }
             result = pManagement.AddRegisterPushDevice(sessionId, channelId, ACTIVE, email, gcm_regid, aUser.userId,IOS, new Date(), new Date());
            }else if(LoadSettings.g_sSettings.getProperty("product.type").equals("1")){
                 //System.out.println("In dictum Protect");
                  ContactManagement cManagement = new ContactManagement();
             result = cManagement.addPushNotificationContact(sessionId, channelId, email, name, gcm_regid, "IOSPushNotify", ACTIVE, new Date());
           
            }
            if (result == 0) {
                error = "Device Register Successfully!!!";
                errorcode = 0;
                try { json.put("_error", error);
                json.put("_errorcode", errorcode);
                }catch(Exception e){
                    log.error("Exception :: ",e);
                }

            }

        } catch (Exception ex) {
            log.error("Exception :: ",ex);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
