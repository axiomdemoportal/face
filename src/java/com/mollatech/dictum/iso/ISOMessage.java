package com.mollatech.dictum.iso;

import com.mollatech.axiom.nucleus.db.Pushmessagemappers;
import com.mollatech.axiom.nucleus.db.Pushmessages;
import com.mollatech.axiom.nucleus.db.connector.PushMessageUtils;
import com.mollatech.axiom.nucleus.db.connector.PushmessagemappersUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;

/**
 *
 * @author Ideasventure
 */
public class ISOMessage extends Thread {

    public String channelId;
    public int port;
    private Socket socket;
    public ServerSocket serverSocket;
    //private String ClientMessage;
    //private String ISOBitmap;
    public int callerID;
    private boolean bServerController = false;

    public Thread m_tid = null;

    public ISOMessage(String channelId, int port) {
        try {
            this.channelId = channelId;
            this.port = port;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public int StartListeningNow() {
        try {
            this.serverSocket = new ServerSocket();
            this.serverSocket.setReuseAddress(true);
            this.serverSocket.bind(new InetSocketAddress(port));
            this.bServerController = true;
            m_tid = new Thread(this);
            m_tid.start();
            //System.out.println("after StartListeningNow() with thread id::" + m_tid.getId());
            return 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        }

    }

    public int stopListening() {
        try {
            //System.out.println("stopListening() with thread id::" + m_tid.getId());

            //System.out.println("interrupt() with thread id::" + m_tid.getId());
            //m_tid.join();           
            //System.out.println("join() with thread id::" + m_tid.getId());
            bServerController = false;
            //m_tid.interrupt();
            this.serverSocket.close();

            //System.out.println("after stopListening()");
            return 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        }

    }

    @Override
    public void run() {
        try {

            //System.out.println("FX Listener is active and listening to the port " + port);
            while (bServerController == true) {
                try {

                    if (interrupted()) {
                        //System.out.println("isInterrupted()=" + Thread.currentThread().isInterrupted());
                        bServerController = false;
                    } else {
                        //System.out.println("Not interruptted!!!!");
                    }

                    //System.out.println("i am here in run()");
                    socket = null;
                    try {
                        socket = serverSocket.accept(); //blocking call
                        
                        InputStream is = socket.getInputStream();
                        InputStreamReader isr = new InputStreamReader(is);
                        BufferedReader br = new BufferedReader(isr);
                        
                        String ClientMessage = br.readLine();

                        try {
                            Pushmessages selectedPushMessage = checkValidRequest(socket.getRemoteSocketAddress().toString());
                            Pushmessagemappers[] mappers = getpushmappers(selectedPushMessage);
                            if ( mappers != null){
                                
                                for(int i=0;i<mappers.length;i++){
                                    String identifier = mappers[i].getMessageformat();
                                    if ( ClientMessage.startsWith(identifier) == false ) {
                                        
                                    } else {
                                        new ProcessMessage(selectedPushMessage, mappers[i],ClientMessage);
                                        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                                        out.print(selectedPushMessage.getResponsesuccess());
                                        out.flush();
                                    }
                                    
                                }                                                                
                            } else {
                                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                                out.print(selectedPushMessage.getResponcefailure());
                                out.flush();
                            }
                        } catch (Exception e) {
                            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                            out.print(e.getMessage());
                            out.flush();                            
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (socket != null) {
                        socket.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            //System.out.println("outside while()");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                //serverSocket.close();
                //System.out.println("after clsoe()");
            } catch (Exception e) {
            }
        }
    }

    private static Pushmessages[] g_pushmessages;

    public Pushmessages checkValidRequest(String address) throws Exception {
        int port;
        String ip;
        int iIndex = address.indexOf(":");

        ip = address.substring(1, iIndex);

        String strPort = address.substring(iIndex + 1);
        port = Integer.parseInt(strPort);
        if (g_pushmessages == null) {
            SessionFactoryUtil suSession = null;
            Session sSession = null;
            try {
                suSession = new SessionFactoryUtil(SessionFactoryUtil.pushmessages);
                sSession = suSession.openSession();
                PushMessageUtils nUtils = new PushMessageUtils(suSession, sSession);
                g_pushmessages = nUtils.listAllmessages(channelId);
            } catch (Exception eee) {
                eee.printStackTrace();
            }
            suSession.close();
            sSession.close();
        }

        if (g_pushmessages != null) {
            try {
                for (int i = 0; i < g_pushmessages.length; i++) {
                    if (g_pushmessages[i].getCallerIp().equals(ip)) {
                        if (g_pushmessages[i].getStatus() == 1) {
                            return g_pushmessages[i];
                        } else {
                            throw new Exception("invalid request(-3)");
                        }
                        //return g_pushmessages[i].getCallerid();
                    }
                }

            } catch (Exception e) {
                //log.error("Exception caught :: ",e);
                throw e;
            }
            throw new Exception("invalid request(-1)");
        }
        throw new Exception("invalid request(-2)");
    }

    public Pushmessagemappers[] getpushmappers(Pushmessages selectedPushMessage ) {
        SessionFactoryUtil puSettings=null;
        Session pSettings=null;
        try { 
            puSettings = new SessionFactoryUtil(SessionFactoryUtil.pushmessagesmapper);        
            pSettings = puSettings.openSession();
            PushmessagemappersUtils sUtil = new PushmessagemappersUtils(puSettings, pSettings);
            Pushmessagemappers[] Obj = sUtil.getPushmessagemappers(selectedPushMessage.getChannelid(), selectedPushMessage.getCallerid());
            return Obj;            
        }catch(Exception e){            
            e.printStackTrace();
            return null;
        }
         finally {
            pSettings.close();
            puSettings.close();
        }
        //return Pushmessagemappers;
    }
}
