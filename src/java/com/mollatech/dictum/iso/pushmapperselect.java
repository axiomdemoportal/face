/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.iso;

import com.google.gson.Gson;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Pushmessagemappers;
import com.mollatech.axiom.nucleus.db.Pushmessages;
import com.mollatech.axiom.nucleus.db.connector.PushMessageUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.management.PushMessageManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class pushmapperselect extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(pushmapperselect.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        String value = request.getParameter("value");
        log.debug("value :: "+value);
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        try {

            PushMessageManagement pManagement = new PushMessageManagement();
            Pushmessages pObj = pManagement.getPushmessagebyname(sessionId, channel.getChannelid(), value);
            Pushmessagemappers[] mappers = null;
            if (pObj != null) {

                mappers = pManagement.ListPushmappers(sessionId, channel.getChannelid(), pObj.getCallerid());

            }

            Map<String, String> options = new HashMap<String, String>();
            if (mappers != null) {
                for (int i = 0; i < mappers.length; i++) {
                    options.put(mappers[i].getMessageformat(), mappers[i].getMessageformat());
                }
            }else{
                options.put("pushmessages", "pushmessages");
            }

            String jsons = new Gson().toJson(options);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(jsons);

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        }
        log.info("Servlet ended");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
