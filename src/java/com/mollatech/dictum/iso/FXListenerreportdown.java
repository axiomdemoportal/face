/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.iso;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Isologs;
import com.mollatech.axiom.nucleus.db.Pushmessages;
import com.mollatech.axiom.nucleus.db.connector.management.ISOManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PushMessageManagement;
import com.mollatech.axiom.v2.face.handler.operator.getopraudits;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ideasventure
 */
public class FXListenerreportdown extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FXListenerreportdown.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static int PDF_TYPE = 0;
    private static int CSV_TYPE = 1;
    private static final int BUFSIZE = 4096;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        try {
            try {
                Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                log.debug("channel name :: "+ channel.getName());
                String sessionId = (String) request.getSession().getAttribute("_apSessionID");
                log.debug("sessionId :: "+ sessionId);
                String _searchtext = request.getParameter("_pushmessageSearch");
                log.debug("_searchtext :: "+ _searchtext);
                String _startdate = request.getParameter("_startdate");
                log.debug("_startdate :: "+ _startdate);
                String _enddate = request.getParameter("_enddate");
                log.debug("_enddate :: "+ _enddate);
                String _type = request.getParameter("_pushmappersSearch");
                log.debug("_type :: "+ _type);
                String _format = request.getParameter("_reporttype");
                log.debug("_format :: "+ _format);
                
            


               
                int iFormat = -9999;
                if (_type != null && !_type.isEmpty()) {
                    iFormat = Integer.parseInt(_format);
                }

                // int itype = Integer.parseInt(_type);
                DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
                Date startDate = null;
                if (_startdate != null && !_startdate.isEmpty()) {
                    startDate = (Date) formatter.parse(_startdate);
                }
                Date endDate = null;
                if (_enddate != null && !_enddate.isEmpty()) {
                    endDate = (Date) formatter.parse(_enddate);
                }

                if (PDF_TYPE == iFormat) {
                    iFormat = PDF_TYPE;
                } else {
                    iFormat = CSV_TYPE;
                }

                String filepath = null;

                try {
                    try {
                    PushMessageManagement pManagement = new PushMessageManagement();

                        Pushmessages pObj = pManagement.getPushmessagebyname(sessionId, channel.getChannelid(), _searchtext);
                        ISOManagement bMngt = new ISOManagement();
//                        Isologs[] isoobj=bMngt

                        Isologs[] clogobj = bMngt.getisologsbycallerID(channel.getChannelid(), pObj.getCallerid(), _type,startDate, endDate);
                        filepath = bMngt.generateReport(iFormat,channel.getChannelid(), clogobj);
                       
                    } catch (Exception e) {
                        log.error("Exception caught :: ",e);
                    }

                    //  
                    File file = new File(filepath);
                    int length = 0;
                    ServletOutputStream outStream = response.getOutputStream();
                    ServletContext context = getServletConfig().getServletContext();
                    String mimetype = context.getMimeType(filepath);

                    // sets response content type
                    if (mimetype == null) {
                        mimetype = "application/octet-stream";
                    }
                    response.setContentType(mimetype);
                    response.setContentLength((int) file.length());
                    String fileName = (new File(filepath)).getName();

                    // sets HTTP header
                    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

                    byte[] byteBuffer = new byte[BUFSIZE];
                    DataInputStream in = new DataInputStream(new FileInputStream(file));

                    // reads the file's bytes and writes them to the response stream
                    while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                        outStream.write(byteBuffer, 0, length);
                    }

                    in.close();
                    outStream.close();
                    file.delete();

                } catch (Exception ex) {
                    // TODO handle custom exceptions here
                    log.error("Exception caught :: ",ex);
                }

            } catch (Exception ex) {
                log.error("Exception caught :: ",ex);
            }

        } finally {
            //  out.close();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
