package com.mollatech.dictum.iso;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Pushmessagemappers;
import com.mollatech.axiom.nucleus.db.Pushmessages;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.IsoUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import com.mollatech.dictum.connector.iso.MessageDetailsFromImpl;
import java.io.ByteArrayInputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Date;
import org.hibernate.Session;

public class ProcessMessage extends Thread implements Runnable {

    public String ISOMessage;
    public String channelId;
    public String isoBitmap;
    public int CallerID;
    public MessageDetailsFromImpl Data;
    public String implclassname;
    Pushmessages selectedPushMessage = null;
    Pushmessagemappers selectedMapper = null;

    public ProcessMessage(Pushmessages pushMessage, Pushmessagemappers mappers, String ClientMessage) {
        this.ISOMessage = ClientMessage;
        this.channelId = pushMessage.getChannelid();
        this.CallerID = pushMessage.getCallerid();
        this.isoBitmap = mappers.getMessageformat();
        this.implclassname = mappers.getClassname();
        this.selectedPushMessage = pushMessage;
        this.selectedMapper = mappers;
        new Thread(this).start();
    }

    @Override
    public void run() {
        
        //System.out.println();
        
        
        //here  Reflection will be used to load the class which contains client parsing logic..
        //and the parsi
        //1. solution is that the parsing class will send us the template message directly..
        //or 
        //2.The parsing 

        Class c;
        int i;
        Constructor argsConstructor;
        Method[] allMethods = null;
        Object object = null;
//        SessionFactoryUtil muSettings = new SessionFactoryUtil(SessionFactoryUtil.pushmessages);
//        Session mSettings = muSettings.openSession();
//        PushMessageUtils mUtil = new PushMessageUtils(muSettings, mSettings);
//        Pushmessages caller = mUtil.getPushmessages(channelId, CallerID);
//        SessionFactoryUtil puSettings = new SessionFactoryUtil(SessionFactoryUtil.pushmessagesmapper);
//        Session pSettings = puSettings.openSession();
//        PushmessagemappersUtils sUtil = new PushmessagemappersUtils(puSettings, pSettings);
//        Pushmessagemappers[] Obj = sUtil.getPushmessagemappers(channelId, CallerID);
        SessionFactoryUtil tuSettings = new SessionFactoryUtil(SessionFactoryUtil.templates);
        Session tSettings = tuSettings.openSession();
        TemplateUtils tUtil = new TemplateUtils(tuSettings, tSettings);

//        Pushmessagemappers mappers = null;
//        for (int j = 0; j < Obj.length; j++) {
//            if (Obj[j].getMessageformat().equals(isoBitmap)) {
//                mappers = (Pushmessagemappers) Obj[j];
//            }
//        }
        try {
            c = Class.forName(selectedMapper.getClassname());
//            c = Class.forName("external.handler.dictum.iso.ISOInterfaceImplementation");
            //System.out.println("ClassNAME"+c.getName());

            argsConstructor = c.getConstructor();
            object = argsConstructor.newInstance();
            allMethods = c.getDeclaredMethods();

        } catch (Exception ex) {
            ex.printStackTrace();
            //System.out.println(ex);
        }

        Templates templateObj = null;
        templateObj = tUtil.loadtemplate(channelId, selectedMapper.getTemplateid());
        String templateMessage = null;
        String templateSubject = null;
        if (templateObj != null) {
            ByteArrayInputStream bais = new ByteArrayInputStream(templateObj.getTemplatebody());
            templateMessage = (String) UtilityFunctions.deserializeFromObject(bais);            
            templateSubject = new String(templateObj.getSubject());
        }
        
        

        for (Method m : allMethods) {
            String mname = m.getName();
            if (mname.compareTo("GenerateMessage") == 0) {
                m.setAccessible(true);
                try {
                    Data = (MessageDetailsFromImpl) m.invoke(object, ISOMessage, templateMessage,templateSubject);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    return;
                }
            }
        }

        String msg = Data.messagetoSend;


        SendNotification send = new SendNotification();
        
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.isologs);
        Session sSession = suSession.openSession();
        IsoUtils iUtils = new IsoUtils(suSession, sSession);
        
        AXIOMStatus status = null;
        String sendon=null;
        if (Data.phone != null || Data.emailid != null ) {                    
            if (selectedMapper.getSendvia() <= 3) {
                status = send.SendOnMobile(channelId,
                        Data.phone, msg, selectedMapper.getSendvia(),
                        Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                sendon = Data.phone;
            } else if (selectedMapper.getSendvia() == 4) {
                status = send.SendEmail(channelId, Data.emailid, Data.subject, msg, null, null, null, null,
                        Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                
                sendon = Data.emailid;

            }
        }  else { 
            iUtils.addIsologs(channelId, selectedMapper.getCallerid(),selectedPushMessage.getCallerName(),isoBitmap, new Date(), selectedPushMessage.getCallerIp(),"NOT SPECIFIED",selectedPushMessage.getResponcefailure(),-1,1);
        } 
        
        
        
        //SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.isologs);
        //Session sSession = suSession.openSession();
        try {

            //IsoUtils iUtils = new IsoUtils(suSession, sSession);
            if (status.iStatus == 0) {
                iUtils.addIsologs(channelId, selectedMapper.getCallerid(),selectedPushMessage.getCallerName(),isoBitmap, new Date(), selectedPushMessage.getCallerIp(),sendon,selectedPushMessage.getResponcefailure(),1,1);
            } else {
                iUtils.addIsologs(channelId, selectedMapper.getCallerid(),selectedPushMessage.getCallerName(),isoBitmap, new Date(), selectedPushMessage.getCallerIp(),sendon,selectedPushMessage.getResponcefailure(),-1,1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            suSession.close();
            sSession.close();
            tuSettings.close();
            tSettings.close();

        }

    }

//    @Override
//    public String TemplateHandler(String templatename, int type, String[] variables, String[][] dataValuePair, String body) {
//        for (int i = 0; i < dataValuePair.length; i++) {
//            //  for (int j = 0; j < variables.length; j++) {
//            if (dataValuePair[i][0].equals(variables[i])) {
//                body = body.replaceAll("#" + variables[i] + "#", dataValuePair[i][1]);
//            }
//        }
//        //}
//        return body;
//    }
}
