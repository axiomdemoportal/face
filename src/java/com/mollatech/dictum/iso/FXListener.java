/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.iso;

import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.hibernate.Session;

/**
 *
 * @author Ideasventure
 */
public class FXListener implements ServletContextListener {
    public static ISOMessage g_statISOObj;
    public static int g_Port;

    public void contextInitialized(ServletContextEvent sce) {

        try {

            try {
                if (Integer.valueOf(LoadSettings.g_sSettings.getProperty("product.type")).intValue() != 1) {
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();

            }

            ServletContext ch = sce.getServletContext();

            String _channelName = ch.getContextPath();
            _channelName = _channelName.replaceAll("/", "");

            SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
            Session sChannel = suChannel.openSession();
            ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);

            Channels channel = cUtil.getChannel(_channelName);
            if (channel == null) {
                //System.out.println(channel + " could not be found!!!");
                return;
            }

            sChannel.close();
            suChannel.close();

            String channelid = channel.getChannelid();
            g_Port = Integer.valueOf(LoadSettings.g_sSettings.getProperty("dictum.listener.port")).intValue();
            g_statISOObj = new ISOMessage(channelid, g_Port);
            //System.out.println("i am here before StartListeningNow()");
            int iRetValue = g_statISOObj.StartListeningNow();
            if (iRetValue == 0) {
                //System.out.println(_channelName + " FX Listener running at Port " + g_Port);
            } else {
                //System.out.println(_channelName + " FX Listener running at Port " + g_Port + " FAILED!!!");

            }

            //System.out.println("FX Listener Started Successfully!!!");
        } catch (Exception ex) {
            ex.printStackTrace();
        } 

    }

    public void contextDestroyed(ServletContextEvent sce) {

        try {

            if (Integer.valueOf(LoadSettings.g_sSettings.getProperty("product.type")).intValue() != 1) {
                return;
            }
            ServletContext ch = sce.getServletContext();

            SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
            Session sChannel = suChannel.openSession();
            ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);

            String _channelName = ch.getContextPath();
            _channelName = _channelName.replaceAll("/", "");

            Channels channel = cUtil.getChannel(_channelName);

            sChannel.close();
            suChannel.close();

            int iRetValue = g_statISOObj.stopListening();
            if (iRetValue == 0) {
                //System.out.println(_channelName + " FX Listenter stopped at Port " + g_Port);
            } else {
                //System.out.println(_channelName + " FX Listenter stopped at Port " + g_Port + " FAILED!!!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } 

    }
}
