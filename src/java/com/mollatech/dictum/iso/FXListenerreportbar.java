/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.iso;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.face.common.bar;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Pushmessages;
import com.mollatech.axiom.nucleus.db.connector.management.ISOManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PushMessageManagement;
import com.mollatech.axiom.v2.face.handler.bulkmsg.messagereportbar;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ideasventure
 */
public class FXListenerreportbar extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FXListenerreportbar.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final int SMS = 1;
    final int VOICE = 2;
    final int USSD = 3;
    final int EMAIL = 4;
    final int SENT = 0;
    final int PENDING = 2;
    final int FAILED = -1;
    final int BLOCKED = -5;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        log.info("Servlet started");
        try {
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId :: "+sessionId);
            String channelId = channel.getChannelid();
            log.debug("channelId :: "+channelId);
            String _startdate = request.getParameter("_startdate");
            log.debug("_startdate :: "+_startdate);
            String _enddate = request.getParameter("_enddate");
            log.debug("_enddate :: "+_enddate);
            String _searchtext = request.getParameter("_pushmessageSearch");
            log.debug("_searchtext :: "+_searchtext);
            String _type = request.getParameter("_pushmappersSearch");
            log.debug("_type :: "+_type);
           

            DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            Date startDate = null;
            if (_startdate != null && !_startdate.isEmpty()) {
                try {
                    startDate = (Date) formatter.parse(_startdate);
                } catch (ParseException ex) {
                    log.error("Exception caught :: ",ex);
                }
            }
            Date endDate = null;
            if (_enddate != null && !_enddate.isEmpty()) {
                endDate = (Date) formatter.parse(_enddate);
            }

            PushMessageManagement pManagement = new PushMessageManagement();

            Pushmessages pObj = pManagement.getPushmessagebyname(sessionId, channel.getChannelid(),_searchtext);
            ISOManagement iso = new ISOManagement();

         try {
        
////            
               int successcount = iso.getsuccessfailureCount(channelId, pObj.getCallerid(), 1, startDate, endDate, _type);
               log.debug("getsuccessfailureCount :: "+successcount);
                int failure = iso.getsuccessfailureCount(channelId, pObj.getCallerid(), -1, startDate, endDate, _type);
                log.debug("getsuccessfailureCount :: "+failure);


                ArrayList<bar> sample = new ArrayList<bar>();

                sample.add(new bar(successcount, "Success"));
                sample.add(new bar(failure, "Error"));
               
                Gson gson = new Gson();

                JsonElement element = gson.toJsonTree(sample, new TypeToken<List<bar>>() {
                }.getType());

                JsonArray jsonArray = element.getAsJsonArray();
                //response.setContentType("application/json");
                out.print(jsonArray);

            } finally {
                out.close();
                
            }
        } catch (ParseException ex) {
            // Logger.getLogger(BarMSGChart.class.getName()).log(Level.SEVERE, null, ex);
            log.error("Exception caught :: ",ex);
        }
        log.info("Servlet ended");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
