/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.access.snmp.scheduler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Snmpreceiversettings;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SNMPReceiverManagement;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author bluebricks3
 */
public class ChangesnmpStatus extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ChangesnmpStatus.class.getName());
    final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;
   final String itemtype = "PULLSETTINGS";
//    String channelId = "Supriya";


 
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        response.setContentType("application/json");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String _settingstatus = request.getParameter("_status");
        log.debug("_settingstatus :: "+_settingstatus);
        String _receiverName = request.getParameter("_name");
        log.debug("_receiverName :: "+_receiverName);
        //String _receiverName = (String) request.getSession().getAttribute("_snmpip"); 
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operator :: "+operator.getName());
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Status Update Successful!!!";

        int retValue = 0;
        
        
        int isettingstatus = Integer.parseInt(_settingstatus);
     
        
         SNMPReceiverManagement mngt = new SNMPReceiverManagement();
        AuditManagement audit = new AuditManagement();
//        int retValue = 0;
         Snmpreceiversettings rs=new Snmpreceiversettings();
         //rs.setChannelId(channelId);
         Snmpreceiversettings oldOpObj = mngt.getsnmpSetting(sessionId,channel.getChannelid(), _receiverName);
        
        retValue = mngt.ChangeStatus(sessionId,channel.getChannelid(),_receiverName,isettingstatus);
        int istatus = oldOpObj.getSnmpStatus();
        String strStatus = "";
        if (istatus == ACTIVE_STATUS) {
            strStatus = "ACTIVE_STATUS";
        } else if (istatus == SUSPEND_STATUS) {
            strStatus = "SUSPEND_STATUS";
        }
        String strNewStatus = "";
        if (isettingstatus == ACTIVE_STATUS) {
            strNewStatus = "ACTIVE_STATUS";
        } else if (isettingstatus == SUSPEND_STATUS) {
            strNewStatus = "SUSPEND_STATUS";
        }
        String _value = "Active";
        if ( isettingstatus == 0 ) {
            _value = "Suspended";
        }

        String resultString = "ERROR";

        if (retValue == 0) {
            resultString = "SUCCESS";
             audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                        "Change SNMP Server Scheduler Status", resultString, retValue, "SNMP Server Management",
                        "Old Status="+strStatus, "New Status ="+strNewStatus,
                        itemtype, ""+oldOpObj.getSnmpId());
        }
        if (retValue != 0) {
            
                 audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                        "Change SNMP Server Scheduler Setting", resultString, retValue, "SNMP Server Management",
                        "Old Status="+strStatus,"Failed To change Status",
                        itemtype, ""+oldOpObj.getSnmpId());
            result = "error";
            message = "Status Update Failed!!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
