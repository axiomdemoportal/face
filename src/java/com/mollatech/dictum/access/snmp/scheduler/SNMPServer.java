/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.access.snmp.scheduler;

import com.mollatech.axiom.nucleus.db.Snmpreceiversettings;
import com.mollatech.axiom.nucleus.db.connector.management.SNMPReceiverManagement;


import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.PDUv1;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.IpAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.transport.DefaultUdpTransportMapping;

/**
 *
 * @author nilesh
 */
public class SNMPServer implements org.quartz.Job {

    public static final String community = "public";

    // Sending Trap for sysLocation of RFC1213
    public static final String Oid = ".1.3.6.1.2.1.1.8";

    //IP of Local Host
    //public static final String ipAddress = "127.0.0.1";
    //Ideally Port 162 should be used to send receive Trap, any other available Port can be used
    // public static final int port = 11111;
    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        Snmpreceiversettings m = new Snmpreceiversettings();
        try {

            // Create Transport Mapping
            TransportMapping transport = new DefaultUdpTransportMapping();

            transport.listen();

            // Create Target
            CommunityTarget cTarget = new CommunityTarget();

            cTarget.setCommunity(new OctetString(community));
            cTarget.setVersion(SnmpConstants.version1);

            JobDataMap dataMap = jec.getJobDetail().getJobDataMap();
            String channelid = dataMap.getString("channelId");
            //Snmpreceiversettings s=new Snmpreceiversettings();

            SNMPReceiverManagement u = new SNMPReceiverManagement();
            Snmpreceiversettings[] settings = u.getReceiverSettingByStatus(channelid, SNMPReceiverManagement.ACTIVE_STATUS);
            if (settings == null) {
                System.out.println("SNMP Server settings not found!!!");
                return;
            } else {
                for (int i = 0; i < settings.length; i++) {
                    String ip = settings[i].getSnmpIp();
                    String port1 = settings[i].getSnmpPort();

//                    System.out.println("ip : " + ip);
//                    System.out.println("port : " + port1);

                    cTarget.setAddress(new UdpAddress(ip + "/" + port1));

                    cTarget.setTimeout(5000);

                    cTarget.setRetries(2);

                    PDUv1 pdu = new PDUv1();

                    pdu.setType(PDU.V1TRAP);

                    pdu.setEnterprise(new OID(Oid));

                    pdu.setGenericTrap(PDUv1.ENTERPRISE_SPECIFIC);

                    pdu.setSpecificTrap(1);

                    pdu.setAgentAddress(new IpAddress(ip));

                    // Send the PDU
                    Snmp snmp = new Snmp(transport);
                    //System.out.println("Sending V1 Trap... Check Wheather NMS is Listening or not? ");

                    snmp.send(pdu, cTarget);
                    snmp.close();

                    CPUnRAM n = new CPUnRAM();
                    n.DiskInfo();
                    n.Info();
                    n.MemInfo();
                    n.OSname();
                    n.OSversion();
                    n.OsArch();
                    n.OsInfo();
                    cpupercent u1 = new cpupercent();
                    u1.getCPULoad();

                }

            }
        } catch (Exception e) {

           e.printStackTrace();

        }

    }

}
