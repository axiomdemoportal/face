/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.dictum.access.snmp.scheduler;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;

/**
 *
 * @author bluebricks6
 */

public class cpupercent {

      private long prevUpTime, prevProcessCpuTime;

      private RuntimeMXBean rmBean;        

      private com.sun.management.OperatingSystemMXBean sunOSMBean;

      private Result result;

      private class Result {
        long upTime = -1L;
        long processCpuTime = -1L;
        float cpuUsage = 0;
                int nCPUs;    
      }

      {
       try {
        rmBean = ManagementFactory.getRuntimeMXBean();                              
        //reperisco l'MBean relativo al sunOS
        sunOSMBean  = ManagementFactory.newPlatformMXBeanProxy(
                    ManagementFactory.getPlatformMBeanServer(), 
                        ManagementFactory.OPERATING_SYSTEM_MXBEAN_NAME,
                    com.sun.management.OperatingSystemMXBean.class
                );           

       result = new Result();
       result.nCPUs = sunOSMBean.getAvailableProcessors();
       result.upTime = rmBean.getUptime();
       result.processCpuTime = sunOSMBean.getProcessCpuTime();

       }catch(Exception e){
          System.err.println(cpupercent.class.getSimpleName()+" exception: "+e.getMessage());          
       }
  }


  public cpupercent(){ }


  public float getCPULoad(){

    result.upTime = rmBean.getUptime();
    result.processCpuTime = sunOSMBean.getProcessCpuTime();

   if(result.upTime > 0L && result.processCpuTime >= 0L) 
         updateCPUInfo();
      
    return result.cpuUsage;

   }

  public void updateCPUInfo() {
    if (prevUpTime > 0L && result.upTime > prevUpTime) {
        // elapsedCpu is in ns and elapsedTime is in ms.
        long elapsedCpu = result.processCpuTime - prevProcessCpuTime;
        long elapsedTime = result.upTime - prevUpTime;
        // cpuUsage could go higher than 100% because elapsedTime
        // and elapsedCpu are not fetched simultaneously. Limit to
        // 99% to avoid Plotter showing a scale from 0% to 200%.
        result.cpuUsage =
            Math.round(
                Math.min(100F,
                        elapsedCpu / (elapsedTime * 10000F * result.nCPUs) 
                        ) 
                    );     
        
    }
   // System.out.println("CPU percentage"+result.cpuUsage);
    prevUpTime = result.upTime;
    prevProcessCpuTime = result.processCpuTime; 
    //System.out.println("previous time"+prevUpTime);
    //System.out.println("CPU utilization percentage:  "+result.nCPUs+"%");
    //System.out.println("%");
 }

  }