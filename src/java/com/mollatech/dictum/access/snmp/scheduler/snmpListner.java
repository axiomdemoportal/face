/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.access.snmp.scheduler;

import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import java.util.Date;
import java.util.Timer;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.hibernate.Session;
import static org.quartz.JobBuilder.newJob;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import org.quartz.Trigger;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * Web application lifecycle listener.
 *
 * @author nilesh
 */
public class snmpListner implements ServletContextListener {

    private static Timer timer;
    private static int intervalCall;
    SchedulerFactory schedFact = new org.quartz.impl.StdSchedulerFactory();
    Scheduler sched = null;
    private static Channels channel;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

        try {
            SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
            Session sChannel = suChannel.openSession();
            ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);

            ServletContext ch = sce.getServletContext();
            String _channelName = ch.getContextPath();
            //System.out.println("Channel Core Name >>" + _channelName);
            //_channelName = "/ibank2core";
            _channelName = _channelName.replaceAll("/", "");
            if (_channelName.compareTo("base") != 0) {
                _channelName = _channelName.replaceAll("base", "");
            } else {
                _channelName = "access";
            }

            channel = cUtil.getChannel(_channelName);
            if (channel == null) {
                System.out.println((new Date()) + " >> Channel Details could not be found>>" + _channelName);
                return;
            }

            String interval = LoadSettings.g_sSettings.getProperty("scheduler.run.check.time");
            if (interval == null) {
                System.out.println((new Date()) + " >> Please set value for scheduler.run.check.time variable in dbsetting.conf file");
                return;
            }
            if (interval != null) {
                intervalCall = new Integer(interval).intValue();
            } else {
                intervalCall = 30;
            }

            //System.out.println("Scheduler Started");
            String channelid = channel.getChannelid();
//                     JobDetail job = newJob(schedulingjob.class).withIdentity("myJob", "group1").build();
            JobDetail job = newJob(SNMPServer.class).withIdentity("Snmpserver", "ssss").usingJobData("channelId", channelid).build();

            Trigger trigger = newTrigger()
                    .withIdentity("mySNMPSERVER", "group8")
                    .startNow()
                    .withSchedule(simpleSchedule()
                            .withIntervalInMinutes(intervalCall)
                            .repeatForever())
                    .build();

            sched = schedFact.getScheduler();
            sched.scheduleJob(job, trigger);
            sched.start();

        } catch (NumberFormatException | SchedulerException ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        try {

            sched.shutdown(true);
            //sched.shutdown();
            //System.out.println((new Date()) + " schedulerlistner() contextDestroyed called...");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
