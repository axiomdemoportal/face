/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.access.snmp.scheduler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Snmpreceiversettings;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SNMPReceiverManagement;

//import com.mollatech.axiom.nucleus.settings.RecieverSettingEntry;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author bluebricks3
 */
public class EditsnmpSettings extends HttpServlet {
//     String channelId = "Supriya";
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(EditsnmpSettings.class.getName());
    final String itemtype = "PULLSETTINGS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        response.setContentType("application/json");
        try {
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel);
            String channelId = channel.getChannelid();
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId :: "+sessionId);
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("remoteaccesslogin :: "+remoteaccesslogin);
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operatorS :: "+operatorS.getName());
            String OperatorID = operatorS.getOperatorid();
            String ip = request.getParameter("_snmpipedit");
             log.debug("ip :: "+ip);
            Snmpreceiversettings rs = new Snmpreceiversettings();
            JSONObject e = new JSONObject();
            PrintWriter out = response.getWriter();
            String result = "success";
            String message = "Settings updated successfully";
            
              
          

            String port = request.getParameter("_snmpportedit");
            log.debug("port :: "+port);
            String level = request.getParameter("_leveledit");
            log.debug("level :: "+level);
            String alive = request.getParameter("_aliveedit");
            log.debug("alive :: "+alive);
            String error = request.getParameter("_erroredit");
            log.debug("error :: "+error);
            String warning = request.getParameter("_warningedit");
            log.debug("warning :: "+warning);
            String status = request.getParameter("_statusedit");
            log.debug("status :: "+status);
            int istatus = 0;
            if (status != null) {
                istatus = Integer.parseInt(status);
            }
            rs.setSnmpIp(ip);
            rs.setSnmpPort(port);
            rs.setSnmpLevel(level);
            rs.setSnmpAlive(alive);
            rs.setSnmpError(error);
            rs.setSnmpWarning(warning);
            rs.setSnmpStatus(istatus);

            SNMPReceiverManagement mngt = new SNMPReceiverManagement();
            AuditManagement audit = new AuditManagement();

            Snmpreceiversettings oldObj = mngt.getsnmpSetting(sessionId, channel.getChannelid(), ip);

            int retValue = mngt.EditsnmpSettings(sessionId, channelId, ip, port, istatus, level, alive, error, warning);

            String strschedulerStatus = "In-Active";
            if (oldObj.getSnmpStatus() == 1) {
                strschedulerStatus = "Active";
            }

            String strnewschedulerStatus = "In-Active";
            if (status == "Active") {
                strnewschedulerStatus = "Active";
            }

            String resultString = "ERROR";
            if (retValue == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Edit snmp Receiver Setting", resultString, retValue, "SNMP Server Management",
                        "", "SNMPIp=" + ip + "SNMPStatus =" + status,
                        itemtype, "" + oldObj.getSnmpId());
            } else {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Edit snmp Receiver Setting", resultString, retValue, "SNMP Server Management",
                        "", "Failed To add snmp server setting",
                        itemtype, "" + oldObj.getSnmpId());
            }

            if (retValue == 0) {
                result = "success";
                message = "Settings updated successfully!!!";

                e.put("_result", result);
                e.put("_message", message);

                out.print(e);
                out.flush();
                return;
            } else {
                result = "error";
                message = "Failed to update settings!!!";
//                JSONObject e = new JSONObject();
//                PrintWriter out = response.getWriter();
                e.put("_result", result);
                e.put("_message", message);

                out.print(e);
                out.flush();
            }

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        }
        log.info("Servlet ended");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

//    private static SessionFactory factory;
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
