package com.mollatech.dictum.access.snmp.scheduler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Snmpreceiversettings;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SNMPReceiverManagement;
//import com.mollatech.axiom.nucleus.settings.RecieverSettingEntry;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class AddsnmpSettings extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AddsnmpSettings.class.getName());
    final String itemtype = "SNMPSETTINGS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        response.setContentType("application/json");
        try {

            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel.getName());
            String channelId = channel.getChannelid();
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId :: "+sessionId);
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("remoteaccesslogin :: "+remoteaccesslogin);
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operatorS :: "+operatorS.getName());
             String OperatorID = operatorS.getOperatorid();
            String result = "success";
            String message = "Settings added successfully";
            JSONObject json = new JSONObject();
            PrintWriter out = response.getWriter();
            
          
        

            Snmpreceiversettings rs = new Snmpreceiversettings();
             
            String ip=request.getParameter("_snmpip");
            log.debug("ip :: "+ip);
            String port=request.getParameter("_snmpport");
            log.debug("port :: "+port);
            String level=request.getParameter("_level");
            log.debug("level :: "+level);
            String alive=request.getParameter("_alive");
            log.debug("alive :: "+alive);
            String error=request.getParameter("_error");
            log.debug("error :: "+error);
            String warning=request.getParameter("_warning");
            log.debug("warning :: "+warning);
            String status=request.getParameter("_status");
            log.debug("status :: "+status);
            if(ip == null || port == null || level == null || alive == null || error == null || warning == null || status == null){
                result = "error";
                message = "Please fill require details!!!";

                json.put("_result", result);
                json.put("_message", message);

                out.print(json);
                out.flush();
                return;
            }
             if(ip.isEmpty() == true || port.isEmpty() == true || level.isEmpty() == true || alive.isEmpty() == true || error.isEmpty() == true || warning.isEmpty() == true || status.isEmpty() == true){
                result = "error";
                message = "Please fill require details!!!";

                json.put("_result", result);
                json.put("_message", message);

                out.print(json);
                out.flush();
                return;
            }
            
            int istatus = 0;
            if (status != null) {
                istatus = Integer.parseInt(status);
            }
            rs.setSnmpIp(ip);
            rs.setSnmpPort(port);
            rs.setSnmpLevel(level);
            rs.setSnmpAlive(alive);
            rs.setSnmpError(error);
            rs.setSnmpWarning(warning);
            rs.setSnmpStatus(istatus);

            SNMPReceiverManagement mngt = new SNMPReceiverManagement();

            
            int value = mngt.addsnmpReceiversetting(sessionId, channelId, ip, port, istatus, level, alive, error, warning);
            AuditManagement audit = new AuditManagement();
            log.debug("addsnmpReceiversetting :: "+value);
           
           
           
            String resultString = "ERROR";
            if (value == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add snmp Receiver Setting", resultString, value, "SNMP Server Management",
                        "", "SNMPIp=" + ip + "SNMPStatus =" + status 
                       ,
                        itemtype, "-");
            } else {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add snmp Receiver Setting", resultString, value, "SNMP Server Management",
                        "", "Failed To add snmp server setting",
                        itemtype, "-");
            }

            if (value == 0) {
                result = "success";
                message = "Settings added successfully!!!";

                json.put("_result", result);
                json.put("_message", message);

                out.print(json);
                out.flush();
                return;
            } else {
                result = "error";
                message = "Failed to add settings!!!";
//                JSONObject e = new JSONObject();
//                PrintWriter out = response.getWriter();
                json.put("_result", result);
                json.put("_message", message);

                out.print(json);
                out.flush();
            }

        } catch (Exception ex) {
//            Logger.getLogger(AddSettings.class.getName()).log(Level.SEVERE, null, ex);
            log.error("Exception caught :: ",ex);
        }
        log.info("Servlet ended");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

//    private static SessionFactory factory;
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
