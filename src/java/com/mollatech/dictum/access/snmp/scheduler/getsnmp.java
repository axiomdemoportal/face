package com.mollatech.dictum.access.snmp.scheduler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Snmpreceiversettings;
import com.mollatech.axiom.nucleus.db.connector.SNMPReceiverUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.json.JSONObject;

public class getsnmp extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(getsnmp.class.getName());
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        String _sid = request.getParameter("_sid");
        log.debug("_sid :: "+_sid);
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        try {

            SessionFactoryUtil suOpr = new SessionFactoryUtil(SessionFactoryUtil.SNMPreceiver);
            Session sOpr = suOpr.openSession();
            SNMPReceiverUtils oprUtil = new SNMPReceiverUtils(suOpr, sOpr);
            Snmpreceiversettings oprObj = oprUtil.getsnmpSetting(channel.getChannelid(), _sid);
            sOpr.close();
            suOpr.close();
                     
            json.put("_snmpipedit", oprObj.getSnmpIp());
            json.put("_snmpportedit",  oprObj.getSnmpPort());
            json.put("_leveledit", oprObj.getSnmpLevel());
            json.put("_aliveedit", oprObj.getSnmpAlive());
            json.put("_erroredit", oprObj.getSnmpError());
            json.put("_warningedit", oprObj.getSnmpWarning());
            json.put("_statusedit", oprObj.getSnmpStatus());
            //json.put("_result", "success");
            
        } catch (Exception ex) {
            // TODO handle custom exceptions here
            try { json.put("_result", "error");
            json.put("_message", ex.getMessage());}catch(Exception e){log.error("Exception caught",e);}
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
