/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.access.snmp.scheduler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Snmpreceivertracking;
import com.mollatech.axiom.nucleus.db.Snmpreceiversettings;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SNMPReceiverManagement;
//import com.mollatech.axiom.nucleus.settings.RecieverSettingEntry;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author bluebricks3
 */
public class deletesnmpsettingentry extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(deletesnmpsettingentry.class.getName());
    final String itemtype = "SCHEDULER";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        try {
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId :: "+sessionId);
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("remoteaccesslogin :: "+remoteaccesslogin);
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operatorS :: "+operatorS.getName());
            String _sid = request.getParameter("_sid");
            log.debug("_sid :: "+_sid);
            int _recieverid = Integer.parseInt(_sid);
            String result = "success";
            String message = "Scheduler removed successfully....";

             
            SNMPReceiverManagement gManagement = new SNMPReceiverManagement();
            JSONObject e = new JSONObject();
            PrintWriter out = response.getWriter();
            
            AuditManagement audit = new AuditManagement();
            Snmpreceiversettings oldObj = gManagement.getPullSettingByReceiverId(sessionId, channel.getChannelid(), _recieverid);

            int retValue = -1;
            retValue = gManagement.deleteReceiversetting(sessionId, channel.getChannelid(), _recieverid);
            log.debug("retValue :: "+retValue);
            if (retValue == 0) {
                Snmpreceivertracking[] arr = gManagement.getarrReceiverTracking(channel.getChannelid(), oldObj.getSnmpIp());
                if (arr != null) {
                    for (int i = 0; i < arr.length; i++) {
                        int res = gManagement.deletereciverTracking(sessionId, channel.getChannelid(), arr[i].getSnmpTrackingId());
                    }
                    
                }
            }
            String resultString = "ERROR";

            String stroldschedulerStatus = "In-Active";
            if (oldObj.getSnmpStatus()== 1) {
                stroldschedulerStatus = "Active";
            }

           
            if (retValue == 0) {
                resultString = "SUCCESS";

                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Delete SNMP Server Scheduler", resultString, retValue, "SNMP Server Management",
                        "SNMPIp=" + oldObj.getSnmpIp()+ "Status =" + stroldschedulerStatus
                        ,
                        "Deleted Successfuly",
                        itemtype, operatorS.getOperatorid());

            } else {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Edit SNMP Server Setting", resultString, retValue, "SNMP Server Scheduler Management",
                        "SNMPIp=" + oldObj.getSnmpIp()+ "Status =" + stroldschedulerStatus
                       ,
                        "Failed to Delete scheduler settings",
                        itemtype, operatorS.getOperatorid());
                result = "error";
                message = "Failed to remove Scheduler!!";
                e.put("_result", result);
                e.put("_message", message);
                out.print(e);
                out.flush();
                return;
            }
            if (retValue == 0) {
                result = "success";
                message = "Scheduler removed successfully....";
                e.put("_result", result);
                e.put("_message", message);

                out.print(e);
                out.flush();
                return;
            } else {
                result = "error";
                message = "Failed to delete settings entry";
//                JSONObject e = new JSONObject();
//                PrintWriter out = response.getWriter();
                e.put("_result", result);
                e.put("_message", message);

                out.print(e);
                out.flush();
            }
        } catch (Exception ex) {
            log.error("Exception :: ",ex);
        }
            log.info("Servlet ended");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
