/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.dictum.access.monitorsettings;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.MonitorSettingsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author pramod
 */
public class RemoveMonitorSetting extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(RemoveMonitorSetting.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        final String itemtype = "MONITORGROUPS";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "", message = "";
        try {
            response.setContentType("application/json");

            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId :: "+sessionId);
             String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
             log.debug("remoteaccesslogin :: "+remoteaccesslogin);
            Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operator :: "+operator.getName());
            String operatorId = operator.getOperatorid();
            String channelid = channel.getChannelid();
AuditManagement audit = new AuditManagement();
            int moniotorId = Integer.parseInt(request.getParameter("moniotorId"));
            log.debug("moniotorId :: "+moniotorId);
            MonitorSettingsManagement management = new MonitorSettingsManagement();
            int resulta = management.deleteMonitorsetting(sessionId, channelid, moniotorId);
            if (resulta == 0) {
                result = "success";
                message = "Monitor setting deleted successfully...";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorId,
                                    request.getRemoteAddr(),
                                    channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                                    "Delete Monitor setting", result, resulta, "Monitor Management",
                                    "Monitor Id=" +moniotorId + "" + "" + ""+ "" , ""+ "" +"" 
                                    + "" , itemtype, operatorId);
                return;
            } else {
                result = "error";
                message = "Error in deleting setting...";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorId,
                                    request.getRemoteAddr(),
                                    channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                                    "Delete Monitor setting", result, resulta, "Monitor Management",
                                    "Monitor Id=" +moniotorId + "" + "" + ""+ "" , ""+ "" +"" 
                                    + "" , itemtype, operatorId);
                return;
            }

        } catch (Exception e) {
            result = "error";
            message = "Error in deleting setting...";
            return;
        } finally {
            try {
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            } catch (Exception e) {
                    log.error("Exception caught :: ",e);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
