package com.mollatech.dictum.access.monitorsettings;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author pramod
 */
public class RemoveGroupM extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(RemoveGroupM.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        final String itemtype = "MONITORGROUPS";
        response.setContentType("application/json");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        SettingsManagement sMngmt = new SettingsManagement();
        Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operator :: "+operator.getName());
        String operatorId = operator.getOperatorid();
        int retValue = -1;
        String result = "", message = "";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        try {
            AuditManagement audit = new AuditManagement();
            Map keymap = (Map) request.getSession().getAttribute("keymap");
            log.debug("keymap :: "+keymap);
            String key = request.getParameter("_key");
            log.debug("key :: "+key);
            if (key != null) {
                keymap.remove(key);
                Map oldValuemap = null;
                Map oldmap = (Map) sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.MONITORGROUPS, SettingsManagement.PREFERENCE_ONE);
                log.debug("getSetting :: "+oldmap);
                oldValuemap = (Map) oldmap.get(key);
                retValue = sMngmt.changeSetting(sessionId, channel.getChannelid(), SettingsManagement.MONITORGROUPS, SettingsManagement.PREFERENCE_ONE, oldmap, keymap);
                log.debug("changeSetting :: "+retValue);
                if(retValue==0)
                {
                    result="success";
                    message="Group Removed successfully...";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorId,
                                    request.getRemoteAddr(),
                                    channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                                    "Delete group", "SUCCESS", retValue, "Monitor Group Management",
                                    "Name=" + "" + "" + ""+ "" , ""+ "" +"" 
                                    + "" , itemtype, operatorId);
                    
                }
                else
                {
                    result="failure";
                    message="Fail to remove group...";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorId,
                                    request.getRemoteAddr(),
                                    channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                                    "Delete group", "ERROR", retValue, "Monitor Group Management",
                                    "Name=" + "" + "" + ""+ "" , ""+ "" +"" 
                                    + "" , itemtype, operatorId);
                }
            }
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        }
        finally
        {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
