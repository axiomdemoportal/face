/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.access.monitorsettings;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author pramod
 */
public class AddMonitorSettings extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AddMonitorSettings.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        final String itemtype = "MONITORGROUPS";
        response.setContentType("application/json");
        log.info("Servlet started");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operator :: "+operator.getName());
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        String operatorId = operator.getOperatorid();
        AuditManagement audit = new AuditManagement();

        PrintWriter out = response.getWriter();
        String _gname = request.getParameter("_gname");
        log.debug("_gname :: "+_gname);
        String _operatorsList = request.getParameter("_operatorsList");
        log.debug("_operatorsList :: "+_operatorsList);
        String _selectedRoles = request.getParameter("_selectedRoles");
        log.debug("_selectedRoles :: "+_selectedRoles);
        String _vendors = request.getParameter("_vendors");
        log.debug("_vendors :: "+_vendors);
        JSONObject json = new JSONObject();
        String _result = "error";
        String _message = "Fill proper data...";
        int retValue = -1;
        SettingsManagement sMngmt = new SettingsManagement();
        Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.MONITORGROUPS, SettingsManagement.PREFERENCE_ONE);
        Map keymap = null;
        Map valuemap = null;
        boolean settingExist = false;
        boolean bAddSetting = false;

        try {
            String regex = "^[a-zA-Z]+$";
//            boolean ch = _gname.matches(regex);
//            if (_gname.matches(regex) == false) {
//                _result = "error";
//                _message = "Please enter valide group name!!!";
//                try {
//                    json.put("_result", _result);
//                    json.put("_message", _message);
//                } catch (Exception e) {
//                    log.error("Exception caught :: ",e);
//                }
//                out.print(json);
//                out.flush();
//                return;
//            }
            if (!_operatorsList.equals("") && _operatorsList != null && _selectedRoles != null && !_selectedRoles.equals("null") && !_gname.equals("") && _gname.matches(regex) == true) {
                String[] vendors = _vendors.split(",");
                if (settingsObj == null) {
                    keymap = new HashMap();
                    bAddSetting = true;
                } else {
                    keymap = (Map) settingsObj;
                    Iterator i = keymap.entrySet().iterator();
                    while (i.hasNext()) {
                        Map.Entry e = (Map.Entry) i.next();
                        String key = e.getKey().toString();
                        if (key.equalsIgnoreCase(_gname)) {
                            settingExist = true;
                        }
                    }
                }
                if (settingExist) {
                    valuemap = (Map) keymap.get(_gname);
                    _result = "success";
                    _message = "Monitor Group Updated Successfully";
                } else {
                    valuemap = new HashMap();
                    valuemap.put("Created On", new Date());
                    valuemap.put("Created By", operator.getName());
                    _result = "success";
                    _message = "Monitor Group Added Successfully";
                }

                valuemap.put("operatorsList", _operatorsList);
                valuemap.put("selectedRoles", _selectedRoles);
                valuemap.put("vendors", _vendors);

                valuemap.put("Updated On", new Date());
                keymap.put(_gname, valuemap);
                if (bAddSetting == true) {
                    retValue = sMngmt.addSetting(sessionId, channel.getChannelid(), SettingsManagement.MONITORGROUPS, SettingsManagement.PREFERENCE_ONE, keymap);
                    log.debug("retValue :: "+retValue);
                    String resultString = "ERROR";
                    if (retValue == 0) {
                        resultString = "SUCCESS";
                        audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorId,
                                request.getRemoteAddr(),
                                channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                                "Add Monitor Group", resultString, retValue, "Monitor Group Management",
                                "", "GroupName=" + _gname + "Selected Role=" + _selectedRoles
                                + "Vendors=" + _vendors, itemtype, operatorId);

                    } else {
                        _result = "error";
                        _message = "Failed to Add New Monitor Group";
                        audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                                request.getRemoteAddr(),
                                channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                                "Add Monitor Group", resultString, retValue, "Monitor Group Management",
                                "", "Failed To Add Monitor Groups",
                                itemtype, operatorId);
                    }
                } else {
                    Map oldValuemap = null;
                    Map oldmap = (Map) sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.MONITORGROUPS, SettingsManagement.PREFERENCE_ONE);
                    oldValuemap = (Map) oldmap.get(_gname);
                    retValue = sMngmt.changeSetting(sessionId, channel.getChannelid(), SettingsManagement.MONITORGROUPS, SettingsManagement.PREFERENCE_ONE, oldmap, keymap);
                    if (retValue == 0) {
                        String resultString = "Success";

                    } else {
                        _result = "error";
                        _message = "Failed to Update Monitor Group";
                    }
                }
                json.put("_result", _result);
                json.put("_message", _message);
            } else {
                json.put("_result", _result);
                json.put("_message", _message);
            }
        } catch (Exception e) {
            log.error("Exception caught",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
