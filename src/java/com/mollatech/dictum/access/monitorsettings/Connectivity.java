package com.mollatech.dictum.access.monitorsettings;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.TimerTask;
import org.apache.commons.net.telnet.TelnetClient;

class Connectivity extends TimerTask {

    public int Telnet(String ip, int port) {
        try {
            TimerTask con = new Connectivity();
            Timer timer = new Timer();
            timer.scheduleAtFixedRate(con, 1, 1000);
//            String ip="182.70.115.176";
//            int port=8443;
            Socket s1 = new Socket(ip, port);
            InputStream is = s1.getInputStream();
            DataInputStream dis = new DataInputStream(is);
            if (dis != null) {
                //System.out.println("Connected with ip " + ip + " and port " + port);
            } else {
                //System.out.println("Connection invalid");
                return 0;
            }

            dis.close();
            s1.close();

        } catch (Exception e) {
            e.printStackTrace();
            //System.out.println("Not Connected,Please enter proper input");
            return 0;
        }
        return -1;
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub  

    }
}
