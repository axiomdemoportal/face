package com.mollatech.dictum.access.monitorsettings;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.SchedulerManagement;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.settings.NucleusMonitorSettings;
import com.mollatech.axiom.nucleus.db.Monitorsettings;
import com.mollatech.axiom.nucleus.db.connector.management.MonitorSettingsManagement;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class realtime extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(realtime.class.getName());
    private static int counts = 0;
    private Date date = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        Channels _apSChannelDetails = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("_apSChannelDetails ::"+_apSChannelDetails);
        String _settingName = request.getParameter("_settingName");
        log.debug("_settingName ::"+_settingName);
//        System.out.println("");
//        String t=request.getParameter(_settingName);
//        int type = Integer.parseInt(t);
        Map settingsMap = (Map) request.getSession().getAttribute("setting");
        log.debug("settingsMap ::"+settingsMap);
        Monitorsettings monitorsettings = null;
        monitorsettings = (Monitorsettings) settingsMap.get(_settingName);
        NucleusMonitorSettings nms = null;
        if (monitorsettings != null) {
            byte[] obj = monitorsettings.getMonitorSettingEntry();
            byte[] f = AxiomProtect.AccessDataBytes(obj);
            ByteArrayInputStream bais = new ByteArrayInputStream(f);
            Object object = SchedulerManagement.deserializeFromObject(bais);
//            NucleusMonitorSettings nms = null;
            if (object instanceof NucleusMonitorSettings) {
                nms = (NucleusMonitorSettings) object;
            }
        }
        Calendar endDate = Calendar.getInstance();
        Date eDate = endDate.getTime();
        String frequency = nms.getFrequency();
        String f=frequency.replaceAll("min","");
        int freqmin=Integer.parseInt(f);
        int frequensec=freqmin*60;
        endDate.add(Calendar.SECOND, -frequensec);
//        endDate.add(Calendar.SECOND, -60);
        Date sDate = endDate.getTime();
        MonitorSettingsManagement management = new MonitorSettingsManagement();
        if (date == null) {
            counts = management.getMonitorTrackingByNameDuration(_apSChannelDetails.getChannelid(), _settingName, sDate, eDate);
            log.debug("counts :: "+counts);
            date = new Date();
        } else {
            long tinter = System.currentTimeMillis() - date.getTime();
            tinter = (tinter / 1000) % 60;
            if (tinter > 3) {
                counts = management.getMonitorTrackingByNameDuration(_apSChannelDetails.getChannelid(), _settingName, sDate, eDate);
                log.debug("counts :: "+counts);
                date = new Date();
            }
        }
        try (PrintWriter out = response.getWriter()) {
            try {
                json.put("_count", counts);
            } catch (Exception e) {
               log.debug("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
