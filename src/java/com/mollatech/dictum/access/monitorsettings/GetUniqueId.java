/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.access.monitorsettings;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.nucleus.db.Monitorsettings;
import com.mollatech.axiom.nucleus.db.connector.management.MonitorSettingsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ash
 */
public class GetUniqueId extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(GetUniqueId.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        response.setContentType("application/json");
        String webadd = request.getParameter("_webAddress");
        log.debug("webadd :: "+webadd);
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String uniqueId = UtilityFunctions.getUniqueId(sessionId, webadd, "" + new Date(), webadd);
        log.debug("getUniqueId :: "+uniqueId);
        boolean uniqueFlag = true;
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        try {
            while (uniqueFlag) {
                Monitorsettings monitorsettings = new MonitorSettingsManagement().getMonitorSettingByUniqueId(uniqueId);
                if (monitorsettings == null) {
                    uniqueFlag = false;
                } else {
                    uniqueId = UtilityFunctions.getUniqueId(sessionId, webadd, "" + new Date(), webadd);
                }
            }
            json.put("_result", "success");
            json.put("_message", uniqueId);
        } catch (Exception ex) {
            try {
                json.put("_result", "error");
                json.put("_message", ex.getMessage());
                log.error("Exception caught :: ",ex);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
        } finally {
            out.print(json);
            out.flush();
            out.close();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

