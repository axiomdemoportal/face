/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.access.monitorsettings;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Monitorsettings;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.MonitorSettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SchedulerManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.NucleusMonitorSettings;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.xbill.DNS.ARecord;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.MXRecord;
import org.xbill.DNS.NSRecord;

import org.xbill.DNS.Record;
import org.xbill.DNS.Type;

import net.sf.jmimemagic.*;
import org.xbill.DNS.Name;

/**
 *
 * @author pramod
 */
public class ValidateWebSeal extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ValidateWebSeal.class.getName());
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        JSONObject json = new JSONObject();
        //PrintWriter out = response.getWriter();
        String result = "", message = "";
        response.setContentType("image/gif");
        try {
            //Get the IP address of client machine.
            String ipAddress = request.getRemoteAddr();
            String useragent = request.getHeader("user-agent");
            InetAddress addr = InetAddress.getByName(ipAddress);
            String host = addr.getHostName();
            String channelid = LoadSettings.g_sSettings.getProperty("web.seal.channelid");
            //System.out.println("Channel id " + channelid);
            String approve=LoadSettings.g_sSettings.getProperty("approve.image.path");
            //System.out.println("Path for approve image" + approve);
            String disapprove=LoadSettings.g_sSettings.getProperty("reject.image.path");
            //System.out.println("Path for disapprove image" + disapprove);
       
            //for approve
            BufferedImage image = ImageIO.read(new File(approve));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(image, "png", baos);
            byte[] plainImage = baos.toByteArray();
            MagicMatch match = Magic.getMagicMatch(plainImage, true);
            String mimeType = match.getMimeType();

            //for disapprove
            BufferedImage imagefordisapprove = ImageIO.read(new File(disapprove));
            ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
            ImageIO.write(imagefordisapprove, "png", baos1);
            byte[] disapproveImage = baos1.toByteArray();
            MagicMatch matchfordisapprove = Magic.getMagicMatch(disapproveImage, true);
            String mimeTypefordisapprove = matchfordisapprove.getMimeType();

            String uid = request.getParameter("AP");
            log.debug("uid :: "+uid);
            String referer = request.getHeader("Referer");

            MonitorSettingsManagement management = new MonitorSettingsManagement();

            Monitorsettings settings = management.getmonitorsetting(uid);
            byte[] obj = settings.getMonitorSettingEntry();
            byte[] f = AxiomProtect.AccessDataBytes(obj);
            ByteArrayInputStream bais = new ByteArrayInputStream(f);
            Object object = SchedulerManagement.deserializeFromObject(bais);
            NucleusMonitorSettings nms, nms1;
            if (object instanceof NucleusMonitorSettings) {
                nms = (NucleusMonitorSettings) object;
            }

            nms = (NucleusMonitorSettings) object;
            String alert = nms.getSelectedalert();
            //System.out.println("Selected alert" + alert);
          
            String array[] = alert.split(",");
            //System.out.println("Array length" + array.length);
            
            Map keymap = null;
            Map valuemap = null;
            String emailv = "";
            String operatorlist = "";

            String[] emailop = {};
            String[] emails = {};
            OperatorsManagement oManagement = new OperatorsManagement();
            SettingsManagement sMngmt = new SettingsManagement();
            Object settingsObj = sMngmt.getSetting(channelid, SettingsManagement.MONITORGROUPS, SettingsManagement.PREFERENCE_ONE);
            //System.out.println("Setting object" + settingsObj);
            Operators aOperator = null;
            String[] emailList = {};
            if (settingsObj != null) {
                keymap = (Map) settingsObj;
                 //System.out.println("key map" + keymap);
            }

            for (int j = 0; j < array.length; j++) {
                valuemap = (Map) keymap.get(array[j]);
                //System.out.println("valuemap" + valuemap);
                emailv = emailv + valuemap.get("vendors") + ",";
                //System.out.println("emailv" + emailv);
                operatorlist = operatorlist + valuemap.get("operatorsList") + ",";
                //System.out.println("operatorlist" + operatorlist);
            }
            emails = emailv.split(",");
            //System.out.println("emails" + emails);
            emailop = operatorlist.split(",");
            //System.out.println("emailop" + emailop);

            aOperator = oManagement.getOperatorByname(channelid, emailop[0]);
            aOperator.getEmailid();
            //System.out.println("Operator email id" + aOperator.getEmailid());

            emailList = new String[emails.length];
            

            for (int n = 0; n < emails.length; n++) {
                emailList[n] = emails[n];
                //System.out.println("Email list of n" + emailList[n]);
                //len++;
            }
            String address = nms.getWebadd();
            InetAddress address1 = InetAddress.getByName(new URL(address).getHost());
            String ip1 = address1.getHostAddress();

            if (referer != null) {
                InetAddress address2 = InetAddress.getByName(new URL(referer).getHost());
                String ip2 = address2.getHostAddress();
                if (ip1.equals(ip2)) {
                    if (mimeType.startsWith("image")) {
                        response.setContentType(mimeType);
                        response.setContentLength(plainImage.length);
                        OutputStream out = response.getOutputStream();
                        out.write(plainImage, 0, plainImage.length);
                        return;
                    }
                } else {
                    if (mimeTypefordisapprove.startsWith("image")) {
                        response.setContentType(mimeTypefordisapprove);
                        response.setContentLength(disapproveImage.length);
                        OutputStream out = response.getOutputStream();
                        out.write(disapproveImage, 0, disapproveImage.length);
                        return;
                    }
                }
            } else {
                nms.setNotify(useragent);
                nms.setSMTPMailhost(host);
                management.sendsecurityalert(channelid, nms, aOperator.getEmailid(), emailList, 0, "Security Alert", "Your Website " + nms.getWebadd() + " has an attack from ip " + host + " and user agent details are -- > " + useragent);
            }
            Monitorsettings[] arr = management.listwebsites(4);
            if (arr != null) {
                for (int i = 0; i < arr.length; i++) {
                    byte[] obj1 = arr[i].getMonitorSettingEntry();
                    byte[] f1 = AxiomProtect.AccessDataBytes(obj1);
                    ByteArrayInputStream bais1 = new ByteArrayInputStream(f1);
                    Object object1 = SchedulerManagement.deserializeFromObject(bais1);
                    if (object1 instanceof NucleusMonitorSettings) {
                        nms1 = (NucleusMonitorSettings) object1;
                    }
                    nms1 = (NucleusMonitorSettings) object1;

                    Record[] records;

                    if (nms1.getLinktodns().equals(address)) {
                        if (nms1.getSelectedLookup().equals("ns")) {
                            records = new Lookup(nms1.getDnsname(), Type.NS).run();
                            String[] dnsns = new String[records.length];
                            if (!records.equals(null)) {
                                for (i = 0; i < records.length; i++) {
                                    NSRecord ns = (NSRecord) records[i];
                                    //System.out.println("Nameserver " + ns.getTarget());
                                    dnsns[i] = new String();
                                    Name s = ns.getTarget();

                                    String[] dnsn = nms1.getDnsNS();

                                    for (i = 0; i < dnsn.length; i++) {
                                        dnsns[i] = s.toString();
                                        if (dnsn[i].equals(dnsns[i])) {
                                            if (mimeType.startsWith("image")) {
                                                response.setContentType(mimeType);
                                                response.setContentLength(plainImage.length);
                                                OutputStream out = response.getOutputStream();
                                                out.write(plainImage, 0, plainImage.length);
                                                // return;
                                            }
                                        } else {
                                            if (mimeTypefordisapprove.startsWith("image")) {
                                                response.setContentType(mimeTypefordisapprove);
                                                response.setContentLength(disapproveImage.length);
                                                OutputStream out = response.getOutputStream();
                                                out.write(disapproveImage, 0, disapproveImage.length);
                                                //return;
                                            }

                                        }
                                    }
                                }
                            }
                        }
                        if (nms1.getSelectedLookup().equals("a")) {

                            records = new Lookup(nms1.getDnsname(), Type.A).run();
                            String[] dnsa = new String[records.length];
                            if (!records.equals(null)) {
                                for (i = 0; i < records.length; i++) {
                                    ARecord a = (ARecord) records[i];
                                    //System.out.println("A " + a.getAddress());
                                    dnsa[i] = new String();
                                    InetAddress s = a.getAddress();
                                    String[] dnsA = nms1.getDnsA();
                                    for (i = 0; i < dnsA.length; i++) {
                                        dnsa[i] = s.toString();
                                        if (dnsA[i].equals(dnsa[i])) {
                                            if (mimeType.startsWith("image")) {
                                                response.setContentType(mimeType);
                                                response.setContentLength(plainImage.length);
                                                OutputStream out = response.getOutputStream();
                                                out.write(plainImage, 0, plainImage.length);
                                                // return;
                                            }
                                        } else {
                                            if (mimeTypefordisapprove.startsWith("image")) {
                                                response.setContentType(mimeTypefordisapprove);
                                                response.setContentLength(disapproveImage.length);
                                                OutputStream out = response.getOutputStream();
                                                out.write(disapproveImage, 0, disapproveImage.length);
                                                //return;
                                            }
                                        }
                                    }
                                }
                            }

                        }
                        if (nms1.getSelectedLookup().equals("mx")) {
                            records = new Lookup(nms1.getDnsname(), Type.MX).run();
                            String[] dnsmx = new String[records.length];
                            if (!records.equals(null)) {
                                for (i = 0; i < records.length; i++) {
                                    MXRecord mx = (MXRecord) records[i];
                                    //System.out.println("MX " + mx.getTarget());
                                    dnsmx[i] = new String();
                                    Name s = mx.getTarget();

                                    String[] dnsm = nms1.getDnsNS();

                                    for (i = 0; i < dnsm.length; i++) {
                                        dnsmx[i] = s.toString();
                                        if (dnsm[i].equals(dnsmx[i])) {
                                            if (mimeType.startsWith("image")) {
                                                response.setContentType(mimeType);
                                                response.setContentLength(plainImage.length);
                                                OutputStream out = response.getOutputStream();
                                                out.write(plainImage, 0, plainImage.length);
                                                //return;
                                            }
                                        } else {
                                            if (mimeTypefordisapprove.startsWith("image")) {
                                                response.setContentType(mimeTypefordisapprove);
                                                response.setContentLength(disapproveImage.length);
                                                OutputStream out = response.getOutputStream();
                                                out.write(disapproveImage, 0, disapproveImage.length);
                                                //return;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (nms1.getSelectedLookup().equals("all")) {
                            records = new Lookup(nms1.getDnsname(), Type.NS).run();
                            String[] dnsns = new String[records.length];
                            if (!records.equals(null)) {
                                for (i = 0; i < records.length; i++) {
                                    NSRecord ns = (NSRecord) records[i];
                                    //System.out.println("Nameserver " + ns.getTarget());
                                    dnsns[i] = new String();
                                    Name s = ns.getTarget();

                                    String[] dnsn = nms1.getDnsNS();

                                    for (i = 0; i < dnsn.length; i++) {
                                        dnsns[i] = s.toString();
                                        if (dnsn[i].equals(dnsns[i])) {
                                            if (mimeType.startsWith("image")) {
                                                response.setContentType(mimeType);
                                                response.setContentLength(plainImage.length);
                                                OutputStream out = response.getOutputStream();
                                                out.write(plainImage, 0, plainImage.length);
                                                // return;
                                            }
                                        } else {
                                            if (mimeTypefordisapprove.startsWith("image")) {
                                                response.setContentType(mimeTypefordisapprove);
                                                response.setContentLength(disapproveImage.length);
                                                OutputStream out = response.getOutputStream();
                                                out.write(disapproveImage, 0, disapproveImage.length);
                                                //return;
                                            }
                                        }
                                    }
                                }
                            }
                            records = new Lookup(nms1.getDnsname(), Type.A).run();
                            String[] dnsa = new String[records.length];
                            if (!records.equals(null)) {
                                for (i = 0; i < records.length; i++) {
                                    ARecord a = (ARecord) records[i];
                                    //System.out.println("A " + a.getAddress());
                                    dnsa[i] = new String();
                                    InetAddress s = a.getAddress();
                                    String[] dnsA = nms1.getDnsA();
                                    for (i = 0; i < dnsA.length; i++) {
                                        dnsa[i] = s.toString();
                                        if (dnsA[i].equals(dnsa[i])) {
                                            if (mimeType.startsWith("image")) {
                                                response.setContentType(mimeType);
                                                response.setContentLength(plainImage.length);
                                                OutputStream out = response.getOutputStream();
                                                out.write(plainImage, 0, plainImage.length);
                                                // return;
                                            }
                                        } else {
                                            if (mimeTypefordisapprove.startsWith("image")) {
                                                response.setContentType(mimeTypefordisapprove);
                                                response.setContentLength(disapproveImage.length);
                                                OutputStream out = response.getOutputStream();
                                                out.write(disapproveImage, 0, disapproveImage.length);
                                                //return;
                                            }
                                        }
                                    }
                                }
                            }
                            records = new Lookup(nms1.getDnsname(), Type.MX).run();
                            String[] dnsmx = new String[records.length];
                            if (!records.equals(null)) {
                                for (i = 0; i < records.length; i++) {
                                    MXRecord mx = (MXRecord) records[i];
                                    //System.out.println("MX " + mx.getTarget());
                                    dnsmx[i] = new String();
                                    Name s = mx.getTarget();

                                    String[] dnsm = nms1.getDnsNS();

                                    for (i = 0; i < dnsm.length; i++) {
                                        dnsmx[i] = s.toString();
                                        if (dnsm[i].equals(dnsmx[i])) {
                                            if (mimeType.startsWith("image")) {
                                                response.setContentType(mimeType);
                                                response.setContentLength(plainImage.length);
                                                OutputStream out = response.getOutputStream();
                                                out.write(plainImage, 0, plainImage.length);
                                                //return;
                                            }
                                        } else {
                                            if (mimeTypefordisapprove.startsWith("image")) {
                                                response.setContentType(mimeTypefordisapprove);
                                                response.setContentLength(disapproveImage.length);
                                                OutputStream out = response.getOutputStream();
                                                out.write(disapproveImage, 0, disapproveImage.length);
                                                //return;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }

            //for ssl
            Monitorsettings[] arrssl = management.listwebsites(10);
            {
                if (arrssl != null) {
                    for (int i = 0; i < arr.length; i++) {
                        byte[] obj1 = arr[i].getMonitorSettingEntry();
                        byte[] f1 = AxiomProtect.AccessDataBytes(obj1);
                        ByteArrayInputStream bais1 = new ByteArrayInputStream(f1);
                        Object object1 = SchedulerManagement.deserializeFromObject(bais1);
                        if (object1 instanceof NucleusMonitorSettings) {
                            nms1 = (NucleusMonitorSettings) object1;
                        }
                        nms1 = (NucleusMonitorSettings) object1;

                        // configure the SSLContext with a TrustManager
                        SSLContext ctx = null;
                        try {
                            ctx = SSLContext.getInstance("TLS");
                        } catch (NoSuchAlgorithmException ex) {
                            log.error("Exception caught :: ",ex);
                        }
                        try {
                            ctx.init(new KeyManager[0], new TrustManager[]{new DefaultTrustManager()}, new SecureRandom());
                        } catch (KeyManagementException ex) {
                            log.error("Exception caught :: ",ex);
                        }
                        SSLContext.setDefault(ctx);
                        String protocol = nms1.getSelectedprotocol();
                        host = nms1.getSslHost();
                        String port = nms1.getSslPort();
                        String url1 = protocol + "://" + host;
                        //  +":"+ port;
                        long starTime = System.currentTimeMillis();
                        URL url = new URL(url1);//https://mms.nw.ru
                        HttpsURLConnection conn = null;
                        try {
                            conn = (HttpsURLConnection) url.openConnection();

                        } catch (IOException ex) {
                            log.error("Exception caught :: ",ex);
                        }
                        long timetaken = System.currentTimeMillis() - starTime;
                        String timeout = Long.toString(timetaken);
                        conn.setHostnameVerifier(new HostnameVerifier() {
                            @Override
                            public boolean verify(String arg0, SSLSession arg1) {
                                return true;
                            }

                        });
                        
                        try {
                            conn.getResponseCode();
                            //System.out.println();
                        } catch (IOException ex) {
                            log.error("Exception caught :: ",ex);
                        }
                        Certificate[] certs = null;
                        try {
                            certs = conn.getServerCertificates();
                        } catch (SSLPeerUnverifiedException ex) {
                            log.error("Exception caught :: ",ex);
                        }
                        for (Certificate cert : certs) {
                            //System.out.println(cert.getType());
                            //System.out.println(cert);
                            //System.out.println("\n");
                            X509Certificate certi = (X509Certificate) cert;
                            //System.out.println("exp " + certi.getNotAfter());
                            // Setting the pattern
                            SimpleDateFormat sm = new SimpleDateFormat("dd-MM-yyyy");
                            // myDate is the java.util.Date in yyyy-mm-dd format
                            // Converting it into String using formatter

                            Date expiry_date = certi.getNotAfter();
                            String strDate = sm.format(expiry_date);

                            //System.out.println("Expiry date" + strDate);
                            String dates[] = strDate.split("-");
                            int date1 = Integer.parseInt(dates[0]);
                            int month = Integer.parseInt(dates[1]);
                            int year = Integer.parseInt(dates[2]);
                            //System.out.println("Expiry Month= "+month +"Expiry Year= "+year);
                            month = month - 1;
                            if (month == 0) {
                                month = 12;
                                year = year - 1;
                            }
                            //System.out.println("\n***********************");

                            Date current_date = new Date();
                            String currDate = sm.format(current_date);
                            String datescurrent[] = currDate.split("-");
                            int cdate = Integer.parseInt(dates[0]);
                            int cmonth = Integer.parseInt(datescurrent[1]);
                            int cyear = Integer.parseInt(datescurrent[2]);

                            //System.out.println("Current Month= "+cmonth +"Current Year= "+cyear);
                            if (year == cyear && month == cmonth && date1 == cdate) {
                                if (mimeType.startsWith("image")) {
                                    response.setContentType(mimeType);
                                    response.setContentLength(plainImage.length);
                                    OutputStream out = response.getOutputStream();
                                    out.write(plainImage, 0, plainImage.length);
                                    //return;
                                }
                            } else {
                                if (mimeTypefordisapprove.startsWith("image")) {
                                    response.setContentType(mimeTypefordisapprove);
                                    response.setContentLength(disapproveImage.length);
                                    OutputStream out = response.getOutputStream();
                                    out.write(disapproveImage, 0, disapproveImage.length);
                                    //return;
                                }
                            }
                        }

                        conn.disconnect();

                    }

                }
            }

        } catch (Exception e) {
           log.error("Exception caught :: ",e);
        }
        log.info("Servlet ended");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private static class DefaultTrustManager implements X509TrustManager {

        @Override
        public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }
}
