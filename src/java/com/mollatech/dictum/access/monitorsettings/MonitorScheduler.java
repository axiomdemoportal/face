/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.access.monitorsettings;

import static com.mollatech.axiom.common.utils.UtilityFunctions.deserializeFromObject;
import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Monitorsettings;
import com.mollatech.axiom.nucleus.db.Monitortracking;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AxiomMessageManagement;
import com.mollatech.axiom.nucleus.db.connector.management.MonitorReportManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.NucleusMonitorSettings;
import com.mollatech.axiom.nucleus.settings.SendNotification;

import com.mollatech.axiom.nucleus.db.connector.management.MonitorSettingsManagement;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.xbill.DNS.ARecord;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.MXRecord;
import org.xbill.DNS.NSRecord;
import org.xbill.DNS.Name;
import org.xbill.DNS.Record;
import org.xbill.DNS.TextParseException;
import org.xbill.DNS.Type;

/**
 *
 * @author nilesh
 */
public class MonitorScheduler implements org.quartz.Job {

    private static int PDF_TYPE = 0;
    private static int CSV_TYPE = 1;
    Monitorsettings[] arrSettings = {};
    static String channelId = "";
    //static String sessionId = "";
    String pdffilename = null;
    String csvfilename = null;
    Thread th, th1;
    AXIOMStatus axStatus;
    String[] files = null;
    String[] mimetypes = new String[2];

    private static NucleusMonitorSettings receiverSetting;
    private static MonitorSettingsManagement setting;
    private static int monitorSourcID = 0;
    private SendNotification send = new SendNotification();
    private static Operators aOperator = null;
    private static String[] emailList = {};
    public static final String RECEIVER_SOUCRCE_ID = "100";

    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        channelId = LoadSettings.g_sSettings.getProperty("axiom.monitoring.channelId");

        setting = new MonitorSettingsManagement();
        AxiomMessageManagement cObj = new AxiomMessageManagement();
        mimetypes[0] = "application/pdf";
        mimetypes[1] = "application/text";
        arrSettings = setting.getMonitorSettingByStatus(channelId, MonitorSettingsManagement.ACTIVE_STATUS);
        if (arrSettings == null) {
            return;
        } else {
            for (int i = 0; i < arrSettings.length; i++) {
                Monitortracking receiverTrackingObj = setting.getMonitorTracking(channelId, arrSettings[i].getMonitorName());
                ByteArrayInputStream bais = new ByteArrayInputStream(arrSettings[i].getMonitorSettingEntry());
                Object object = deserializeFromObject(bais);
                if (object instanceof NucleusMonitorSettings) {
                    receiverSetting = (NucleusMonitorSettings) object;
                } else {
                    System.out.println("Monitor setting entry is null...!!!");
                    continue;
                }

                //added
                if (receiverTrackingObj != null) {

                    SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                    Date date = new Date();
                    String oldate = format.format(receiverTrackingObj.getExecutionEndOn());
                    String newdate = format.format(date);
                    Date d1, d2 = null;
                    long diffMinutes = 0, diffHours = 0, diffDays = 0;
                    try {
                        d1 = format.parse(oldate);
                        d2 = format.parse(newdate);
                        long diff = d2.getTime() - d1.getTime();
                        diffMinutes = diff / (60 * 1000) % 60;
                        diffHours = diff / (60 * 60 * 1000) % 24;
                        diffDays = diff / (24 * 60 * 60 * 1000);

                    } catch (ParseException ex) {
                        ex.printStackTrace();
                    }

                    if (receiverSetting.getFrequency().equals("1min")) {
                        if (diffMinutes < 1) {
                            continue;
                        }

                    } else if (receiverSetting.getFrequency().equals("5min")) {
                        if (diffMinutes < 5) {
                            continue;
                        }

                    } else if (receiverSetting.getFrequency().equals("10min")) {
                        if (diffMinutes < 10) {
                            continue;
                        }
                    } else if (receiverSetting.getFrequency().equals("15min")) {
                        if (diffMinutes < 15) {
                            continue;
                        }
                    } else if (receiverSetting.getFrequency().equals("30min")) {
                        if (diffMinutes < 30) {
                            continue;
                        }
                    } else if (receiverSetting.getFrequency().equals("1hr")) {
                        if (diffHours < 1) {
                            continue;
                        }
                    } else if (receiverSetting.getFrequency().equals("2hr")) {
                        if (diffHours < 2) {
                            continue;
                        }
                    } else if (receiverSetting.getFrequency().equals("3hr")) {
                        if (diffHours < 3) {
                            continue;
                        }
                    } else if (receiverSetting.getFrequency().equals("6hr")) {
                        if (diffHours < 6) {
                            continue;
                        }
                    } else if (receiverSetting.getFrequency().equals("1day")) {
                        if (diffDays < 1) {
                            continue;
                        }
                    } else {
                        continue;
                    }
                }
                //end
                SettingsManagement sMngmt = new SettingsManagement();
                Object settingsObj = sMngmt.getSettingwithoutsession(channelId, SettingsManagement.MONITORGROUPS, SettingsManagement.PREFERENCE_ONE);

                String alert = receiverSetting.getSelectedalert();
                Map keymap = null;
                Map valuemap = null;
                String emailv = "";
                String operatorlist = "";

                String[] emailop = {};
                String[] emails = {};

                if (settingsObj != null) {
                    keymap = (Map) settingsObj;
                    String array[] = alert.split(",");
                    OperatorsManagement oManagement = new OperatorsManagement();

                    for (int j = 0; j < array.length; j++) {
                        valuemap = (Map) keymap.get(array[j]);
                        emailv = emailv + valuemap.get("vendors") + ",";
                        operatorlist = operatorlist + valuemap.get("operatorsList") + ",";
                    }
                    emails = emailv.split(",");
                    emailop = operatorlist.split(",");

                    aOperator = oManagement.getOperatorByname(channelId, emailop[0]);
                    aOperator.getEmailid();

                    emailList = new String[emails.length];

                    for (int n = 0; n < emails.length; n++) {
                        emailList[n] = emails[n];
                        //len++;
                    }
                    String id = RECEIVER_SOUCRCE_ID + arrSettings[i].getMonitorId();
                    monitorSourcID = Integer.parseInt(id);

                    if (arrSettings[i].getMonitorType() == MonitorSettingsManagement.WEBSITE) {
                        int statusCode = 0;
                        String webaddress = receiverSetting.getWebadd();
                        URL u = null;
                        String timeout = "0";
                        long starTime = System.currentTimeMillis();
                        try {
                            u = new URL(webaddress);
                            long elasedTime = System.currentTimeMillis() - starTime;
                            timeout = Long.toString(elasedTime);
                            //System.out.println("Speed:  " + elasedTime);
                        } catch (MalformedURLException ex) {
                            ex.printStackTrace();
                        }
                        HttpURLConnection conn = null;
                        try {
                            conn = (HttpURLConnection) u.openConnection();
                            statusCode = conn.getResponseCode();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        try {
                            conn.connect();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        long elasedTime = System.currentTimeMillis() - starTime;
                        timeout = Long.toString(elasedTime);
                        //System.out.println("Speed:  " + elasedTime);
                        int compare = timeout.compareToIgnoreCase(receiverSetting.getTimeout());
                        int flag = 0;
                        if (statusCode != 200) {
                            for (int k = 0; k <= Integer.parseInt(receiverSetting.getDropcount()); k++) {
                                flag++;
                                if (flag > Integer.parseInt(receiverSetting.getDropcount())) {
                                    axStatus = setting.sendMessagemonitor(channelId, receiverSetting, aOperator.getEmailid(), emailList, monitorSourcID, "Monitor Alert", "Response time is less");
                                    Date startdate = new Date();
                                    Date enddate = new Date();
                                    int res = setting.AddReceiverTracking(channelId, arrSettings[i].getMonitorName(), "0ms",
                                            arrSettings[i].getMonitorType(), startdate, enddate);

                                    if (res == 0) {
                                        Monitortracking greetObj = setting.getMonitorTracking(channelId, arrSettings[i].getMonitorName());
                                        if (greetObj != null) {

                                            MonitorSettingsManagement management = new MonitorSettingsManagement();
                                            Monitortracking[] monitortracking = management.getMonitorTrackingByName(channelId, greetObj.getMonitorName());
                                            MonitorReportManagement reportManagement = new MonitorReportManagement();
                                            if (arrSettings[i].getReportType().equals(0)) {
                                                files = new String[2];
                                                files[0] = pdffilename;
                                                files[1] = csvfilename;

                                                pdffilename = reportManagement.generateReport(monitortracking, 0, arrSettings[i].getMonitorType(), receiverSetting.getWebadd());
                                                csvfilename = reportManagement.generateReport(monitortracking, 1, arrSettings[i].getMonitorType(), receiverSetting.getWebadd());
                                            }
                                            if (arrSettings[i].getReportType().equals(1)) {
                                                pdffilename = reportManagement.generateReport( monitortracking, 0, arrSettings[i].getMonitorType(), receiverSetting.getWebadd());
                                                //csvfilename = reportManagement.generateReport(sessionId, monitortracking, 0 , arrSettings[i].getMonitorType());
                                                files = new String[1];
                                                files[0] = pdffilename;

                                            }
                                            if (arrSettings[i].getReportType().equals(2)) {
                                                //pdffilename = reportManagement.generateReport(sessionId, monitortracking, 0 , arrSettings[i].getMonitorType());
                                                csvfilename = reportManagement.generateReport(monitortracking, 1, arrSettings[i].getMonitorType(), receiverSetting.getWebadd());
                                                files = new String[1];
                                                files[0] = csvfilename;
                                            }

                                            axStatus = send.SendEmail(channelId, aOperator.getEmailid(), "REPORT FOR MONITOR", "report",
                                                    emailList, null, files, mimetypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                            //System.out.println("Send Report File Status = >" + axStatus.iStatus + " " + axStatus.strStatus);
                                            //System.out.println((new Date()) + ">> Scheduler Tracking Added Successfully!!!");
                                            continue;
                                        }
                                    } else {
                                        System.out.println((new Date()) + ">> Failed to add Scheduler Tracking!!!");
                                        continue;
                                    }
                                }
                            }

                        } else {
                            Date startdate = new Date();
                            Date enddate = new Date();
                            int res = setting.AddReceiverTracking(channelId, arrSettings[i].getMonitorName(), timeout + "ms",
                                    arrSettings[i].getMonitorType(), startdate, enddate);
                        }

                        conn.disconnect();

                    } else if (arrSettings[i].getMonitorType() == MonitorSettingsManagement.DNS) {
                        long starTime = System.currentTimeMillis();
                        String timeout;
                        Record[] records;
                        if (receiverSetting.getSelectedLookup().equals("ns")) {
                            try {

                                records = new Lookup(receiverSetting.getDnsname(), Type.NS).run();
                                String[] dnsns = new String[records.length];
                                if (!records.equals(null)) {
                                    for (i = 0; i < records.length; i++) {
                                        NSRecord ns = (NSRecord) records[i];
                                        //System.out.println("Nameserver " + ns.getTarget());
                                        dnsns[i] = new String();
                                        Name s = ns.getTarget();

                                        String[] dnsn = receiverSetting.getDnsNS();

                                        for (i = 0; i < dnsn.length; i++) {
                                            dnsns[i] = s.toString();
                                            if (dnsn[i].equals(dnsns[i])) {
                                                Date startdate = new Date();
                                                Date enddate = new Date();
                                                long elasedTime = System.currentTimeMillis() - starTime;
                                                timeout = Long.toString(elasedTime);
                                                //System.out.println("Speed:  " + elasedTime);
                                                int res = setting.AddReceiverTracking(channelId, arrSettings[0].getMonitorName(), timeout + "ms",
                                                        arrSettings[0].getMonitorType(), startdate, enddate);
                                            } else {
                                                long elasedTime = System.currentTimeMillis() - starTime;
                                                timeout = Long.toString(elasedTime);
                                                axStatus = setting.sendMessagemonitor(channelId, receiverSetting, aOperator.getEmailid(), emailList, monitorSourcID, "DNS Monitor alert", "Named Servers are changed");
                                                Date startdate = new Date();
                                                Date enddate = new Date();

                                                int res = setting.AddReceiverTracking(channelId, arrSettings[i].getMonitorName(), timeout + "ms",
                                                        arrSettings[i].getMonitorType(), startdate, enddate);

                                                if (res == 0) {
                                                    Monitortracking greetObj = setting.getMonitorTracking(channelId, arrSettings[i].getMonitorName());
                                                    if (greetObj != null) {

                                                        MonitorSettingsManagement management = new MonitorSettingsManagement();
                                                        Monitortracking[] monitortracking = management.getMonitorTrackingByName(channelId, greetObj.getMonitorName());
                                                        MonitorReportManagement reportManagement = new MonitorReportManagement();
                                                        if (arrSettings[i].getReportType().equals(0)) {
                                                            files = new String[2];
                                                            files[0] = pdffilename;
                                                            files[1] = csvfilename;

                                                            pdffilename = reportManagement.generateReport(monitortracking, 0, arrSettings[i].getMonitorType(), receiverSetting.getDnshost());
                                                            csvfilename = reportManagement.generateReport(monitortracking, 1, arrSettings[i].getMonitorType(), receiverSetting.getDnshost());
                                                        }
                                                        if (arrSettings[i].getReportType().equals(1)) {
                                                            pdffilename = reportManagement.generateReport(monitortracking, 0, arrSettings[i].getMonitorType(), receiverSetting.getDnshost());
                                                            //csvfilename = reportManagement.generateReport(sessionId, monitortracking, 0 , arrSettings[i].getMonitorType());
                                                            files = new String[1];
                                                            files[0] = pdffilename;

                                                        }
                                                        if (arrSettings[i].getReportType().equals(2)) {
                                                            //pdffilename = reportManagement.generateReport(sessionId, monitortracking, 0 , arrSettings[i].getMonitorType());
                                                            csvfilename = reportManagement.generateReport(monitortracking, 1, arrSettings[i].getMonitorType(), receiverSetting.getDnshost());
                                                            files = new String[1];
                                                            files[0] = csvfilename;
                                                        }

                                                        axStatus = send.SendEmail(channelId, aOperator.getEmailid(), "REPORT FOR MONITOR", "report",
                                                                emailList, null, files, mimetypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                                        System.out.println((new Date()) + ">> Scheduler Tracking Added Successfully!!!");
                                                        continue;
                                                    }
                                                } else {
                                                    System.out.println((new Date()) + ">> Failed to add Scheduler Tracking!!!");
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                }
                            } catch (TextParseException ex) {
                                ex.printStackTrace();
                            }
                        }
                        if (receiverSetting.getSelectedLookup().equals("a")) {
                            try {
                                records = new Lookup(receiverSetting.getDnsname(), Type.A).run();
                                String[] dnsa = new String[records.length];
                                if (!records.equals(null)) {
                                    for (i = 0; i < records.length; i++) {
                                        ARecord a = (ARecord) records[i];
                                        //System.out.println("A " + a.getAddress());
                                        dnsa[i] = new String();
                                        InetAddress s = a.getAddress();
                                        String[] dnsA = receiverSetting.getDnsA();
                                        for (i = 0; i < dnsA.length; i++) {
                                            dnsa[i] = s.toString();
                                            if (dnsA[i].equals(dnsa[i])) {
                                                Date startdate = new Date();
                                                Date enddate = new Date();
                                                long elasedTime = System.currentTimeMillis() - starTime;
                                                timeout = Long.toString(elasedTime);
                                                //System.out.println("Speed:  " + elasedTime);
                                                int res = setting.AddReceiverTracking(channelId, arrSettings[0].getMonitorName(), timeout + "ms",
                                                        arrSettings[0].getMonitorType(), startdate, enddate);
                                            } else {
                                                long elasedTime = System.currentTimeMillis() - starTime;
                                                timeout = Long.toString(elasedTime);
                                                axStatus = setting.sendMessagemonitor(channelId, receiverSetting, aOperator.getEmailid(), emailList, monitorSourcID, "DNS Monitor alert", "IP is changed changed");
                                                Date startdate = new Date();
                                                Date enddate = new Date();

                                                int res = setting.AddReceiverTracking(channelId, arrSettings[i].getMonitorName(), timeout + "ms",
                                                        arrSettings[i].getMonitorType(), startdate, enddate);

                                                if (res == 0) {
                                                    Monitortracking greetObj = setting.getMonitorTracking(channelId, arrSettings[i].getMonitorName());
                                                    if (greetObj != null) {

                                                        MonitorSettingsManagement management = new MonitorSettingsManagement();
                                                        Monitortracking[] monitortracking = management.getMonitorTrackingByName(channelId, greetObj.getMonitorName());
                                                        MonitorReportManagement reportManagement = new MonitorReportManagement();
                                                        if (arrSettings[i].getReportType().equals(0)) {
                                                            files = new String[2];
                                                            files[0] = pdffilename;
                                                            files[1] = csvfilename;

                                                            pdffilename = reportManagement.generateReport(monitortracking, 0, arrSettings[i].getMonitorType(), receiverSetting.getDnshost());
                                                            csvfilename = reportManagement.generateReport(monitortracking, 1, arrSettings[i].getMonitorType(), receiverSetting.getDnshost());
                                                        }
                                                        if (arrSettings[i].getReportType().equals(1)) {
                                                            pdffilename = reportManagement.generateReport(monitortracking, 0, arrSettings[i].getMonitorType(), receiverSetting.getDnshost());
                                                            //csvfilename = reportManagement.generateReport(sessionId, monitortracking, 0 , arrSettings[i].getMonitorType());
                                                            files = new String[1];
                                                            files[0] = pdffilename;

                                                        }
                                                        if (arrSettings[i].getReportType().equals(2)) {
                                                            //pdffilename = reportManagement.generateReport(sessionId, monitortracking, 0 , arrSettings[i].getMonitorType());
                                                            csvfilename = reportManagement.generateReport(monitortracking, 1, arrSettings[i].getMonitorType(), receiverSetting.getDnshost());
                                                            files = new String[1];
                                                            files[0] = csvfilename;
                                                        }

                                                        axStatus = send.SendEmail(channelId, aOperator.getEmailid(), "REPORT FOR MONITOR", "report",
                                                                emailList, null, files, mimetypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                                        System.out.println((new Date()) + ">> Scheduler Tracking Added Successfully!!!");
                                                        continue;
                                                    }
                                                } else {
                                                    System.out.println((new Date()) + ">> Failed to add Scheduler Tracking!!!");
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                }
                            } catch (TextParseException ex) {
                                ex.printStackTrace();
                            }

                        }
                        if (receiverSetting.getSelectedLookup().equals("mx")) {
                            try {
                                records = new Lookup(receiverSetting.getDnsname(), Type.MX).run();
                                String[] dnsmx = new String[records.length];
                                if (!records.equals(null)) {
                                    for (i = 0; i < records.length; i++) {
                                        MXRecord mx = (MXRecord) records[i];
                                        //System.out.println("MX " + mx.getTarget());
                                        dnsmx[i] = new String();
                                        Name s = mx.getTarget();

                                        String[] dnsm = receiverSetting.getDnsNS();

                                        for (i = 0; i < dnsm.length; i++) {
                                            dnsmx[i] = s.toString();
                                            if (dnsm[i].equals(dnsmx[i])) {
                                                Date startdate = new Date();
                                                Date enddate = new Date();
                                                long elasedTime = System.currentTimeMillis() - starTime;
                                                timeout = Long.toString(elasedTime);
                                                //System.out.println("Speed:  " + elasedTime);
                                                int res = setting.AddReceiverTracking(channelId, arrSettings[0].getMonitorName(), timeout + "ms",
                                                        arrSettings[0].getMonitorType(), startdate, enddate);
                                            } else {
                                                long elasedTime = System.currentTimeMillis() - starTime;
                                                timeout = Long.toString(elasedTime);
                                                axStatus = setting.sendMessagemonitor(channelId, receiverSetting, aOperator.getEmailid(), emailList, monitorSourcID, "DNS Monitor alert", "Mail Servers are changed");
                                                Date startdate = new Date();
                                                Date enddate = new Date();

                                                int res = setting.AddReceiverTracking(channelId, arrSettings[i].getMonitorName(), timeout + "ms",
                                                        arrSettings[i].getMonitorType(), startdate, enddate);

                                                if (res == 0) {
                                                    Monitortracking greetObj = setting.getMonitorTracking(channelId, arrSettings[i].getMonitorName());
                                                    if (greetObj != null) {

                                                        MonitorSettingsManagement management = new MonitorSettingsManagement();
                                                        Monitortracking[] monitortracking = management.getMonitorTrackingByName(channelId, greetObj.getMonitorName());
                                                        MonitorReportManagement reportManagement = new MonitorReportManagement();
                                                        if (arrSettings[i].getReportType().equals(0)) {
                                                            files = new String[2];
                                                            files[0] = pdffilename;
                                                            files[1] = csvfilename;

                                                            pdffilename = reportManagement.generateReport(monitortracking, 0, arrSettings[i].getMonitorType(), receiverSetting.getDnshost());
                                                            csvfilename = reportManagement.generateReport(monitortracking, 1, arrSettings[i].getMonitorType(), receiverSetting.getDnshost());
                                                        }
                                                        if (arrSettings[i].getReportType().equals(1)) {
                                                            pdffilename = reportManagement.generateReport( monitortracking, 0, arrSettings[i].getMonitorType(), receiverSetting.getDnshost());
                                                            //csvfilename = reportManagement.generateReport(sessionId, monitortracking, 0 , arrSettings[i].getMonitorType());
                                                            files = new String[1];
                                                            files[0] = pdffilename;

                                                        }
                                                        if (arrSettings[i].getReportType().equals(2)) {
                                                            //pdffilename = reportManagement.generateReport(sessionId, monitortracking, 0 , arrSettings[i].getMonitorType());
                                                            csvfilename = reportManagement.generateReport(monitortracking, 1, arrSettings[i].getMonitorType(), receiverSetting.getDnshost());
                                                            files = new String[1];
                                                            files[0] = csvfilename;
                                                        }

                                                        axStatus = send.SendEmail(channelId, aOperator.getEmailid(), "REPORT FOR MONITOR", "report",
                                                                emailList, null, files, mimetypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                                        System.out.println((new Date()) + ">> Scheduler Tracking Added Successfully!!!");
                                                        continue;
                                                    }
                                                } else {
                                                    System.out.println((new Date()) + ">> Failed to add Scheduler Tracking!!!");
                                                    continue;
                                                }

                                            }
                                        }
                                    }
                                }
                            } catch (TextParseException ex) {
                                ex.printStackTrace();
                            }
                        }
                        if (receiverSetting.getSelectedLookup().equals("all")) {
                            try {
                                records = new Lookup(receiverSetting.getDnsname(), Type.NS).run();
                                String[] dnsns = new String[records.length];
                                if (!records.equals(null)) {
                                    for (i = 0; i < records.length; i++) {
                                        NSRecord ns = (NSRecord) records[i];
                                        //System.out.println("Nameserver " + ns.getTarget());
                                        dnsns[i] = new String();
                                        Name s = ns.getTarget();

                                        String[] dnsn = receiverSetting.getDnsNS();

                                        for (i = 0; i < dnsn.length; i++) {
                                            dnsns[i] = s.toString();
                                            if (dnsn[i].equals(dnsns[i])) {
                                                Date startdate = new Date();
                                                Date enddate = new Date();
                                                long elasedTime = System.currentTimeMillis() - starTime;
                                                timeout = Long.toString(elasedTime);
                                                //System.out.println("Speed:  " + elasedTime);
                                                int res = setting.AddReceiverTracking(channelId, arrSettings[0].getMonitorName(), timeout + "ms",
                                                        arrSettings[0].getMonitorType(), startdate, enddate);
                                            } else {
                                                long elasedTime = System.currentTimeMillis() - starTime;
                                                timeout = Long.toString(elasedTime);
                                                axStatus = setting.sendMessagemonitor(channelId, receiverSetting, aOperator.getEmailid(), emailList, monitorSourcID, "DNS Monitor alert", "Named Servers are changed");
                                                Date startdate = new Date();
                                                Date enddate = new Date();

                                                int res = setting.AddReceiverTracking(channelId, arrSettings[i].getMonitorName(), timeout + "ms",
                                                        arrSettings[i].getMonitorType(), startdate, enddate);

                                                if (res == 0) {
                                                    Monitortracking greetObj = setting.getMonitorTracking(channelId, arrSettings[i].getMonitorName());
                                                    if (greetObj != null) {

                                                        MonitorSettingsManagement management = new MonitorSettingsManagement();
                                                        Monitortracking[] monitortracking = management.getMonitorTrackingByName(channelId, greetObj.getMonitorName());
                                                        MonitorReportManagement reportManagement = new MonitorReportManagement();
                                                        if (arrSettings[i].getReportType().equals(0)) {
                                                            files = new String[2];
                                                            files[0] = pdffilename;
                                                            files[1] = csvfilename;

                                                            pdffilename = reportManagement.generateReport(monitortracking, 0, arrSettings[i].getMonitorType(), receiverSetting.getDnshost());
                                                            csvfilename = reportManagement.generateReport(monitortracking, 1, arrSettings[i].getMonitorType(), receiverSetting.getDnshost());
                                                        }
                                                        if (arrSettings[i].getReportType().equals(1)) {
                                                            pdffilename = reportManagement.generateReport(monitortracking, 0, arrSettings[i].getMonitorType(), receiverSetting.getDnshost());
                                                            //csvfilename = reportManagement.generateReport(sessionId, monitortracking, 0 , arrSettings[i].getMonitorType());
                                                            files = new String[1];
                                                            files[0] = pdffilename;

                                                        }
                                                        if (arrSettings[i].getReportType().equals(2)) {
                                                            //pdffilename = reportManagement.generateReport(sessionId, monitortracking, 0 , arrSettings[i].getMonitorType());
                                                            csvfilename = reportManagement.generateReport(monitortracking, 1, arrSettings[i].getMonitorType(), receiverSetting.getDnshost());
                                                            files = new String[1];
                                                            files[0] = csvfilename;
                                                        }

                                                        axStatus = send.SendEmail(channelId, aOperator.getEmailid(), "REPORT FOR MONITOR", "report",
                                                                emailList, null, files, mimetypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                                        System.out.println((new Date()) + ">> Scheduler Tracking Added Successfully!!!");
                                                        continue;
                                                    }
                                                } else {
                                                    System.out.println((new Date()) + ">> Failed to add Scheduler Tracking!!!");
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                }
                                records = new Lookup(receiverSetting.getDnsname(), Type.A).run();
                                String[] dnsa = new String[records.length];
                                if (!records.equals(null)) {
                                    for (i = 0; i < records.length; i++) {
                                        ARecord a = (ARecord) records[i];
                                        //System.out.println("A " + a.getAddress());
                                        dnsa[i] = new String();
                                        InetAddress s = a.getAddress();
                                        String[] dnsA = receiverSetting.getDnsA();
                                        for (i = 0; i < dnsA.length; i++) {
                                            dnsa[i] = s.toString();
                                            if (dnsA[i].equals(dnsa[i])) {
                                                Date startdate = new Date();
                                                Date enddate = new Date();
                                                long elasedTime = System.currentTimeMillis() - starTime;
                                                timeout = Long.toString(elasedTime);
                                                //System.out.println("Speed:  " + elasedTime);
                                                int res = setting.AddReceiverTracking(channelId, arrSettings[0].getMonitorName(), timeout + "ms",
                                                        arrSettings[0].getMonitorType(), startdate, enddate);
                                            } else {
                                                long elasedTime = System.currentTimeMillis() - starTime;
                                                timeout = Long.toString(elasedTime);
                                                axStatus = setting.sendMessagemonitor(channelId, receiverSetting, aOperator.getEmailid(), emailList, monitorSourcID, "DNS Monitor alert", "IP is changed");
                                                Date startdate = new Date();
                                                Date enddate = new Date();

                                                int res = setting.AddReceiverTracking(channelId, arrSettings[i].getMonitorName(), timeout + "ms",
                                                        arrSettings[i].getMonitorType(), startdate, enddate);

                                                if (res == 0) {
                                                    Monitortracking greetObj = setting.getMonitorTracking(channelId, arrSettings[i].getMonitorName());
                                                    if (greetObj != null) {

                                                        MonitorSettingsManagement management = new MonitorSettingsManagement();
                                                        Monitortracking[] monitortracking = management.getMonitorTrackingByName(channelId, greetObj.getMonitorName());
                                                        MonitorReportManagement reportManagement = new MonitorReportManagement();
                                                        if (arrSettings[i].getReportType().equals(0)) {
                                                            files = new String[2];
                                                            files[0] = pdffilename;
                                                            files[1] = csvfilename;

                                                            pdffilename = reportManagement.generateReport(monitortracking, 0, arrSettings[i].getMonitorType(), receiverSetting.getDnshost());
                                                            csvfilename = reportManagement.generateReport(monitortracking, 1, arrSettings[i].getMonitorType(), receiverSetting.getDnshost());
                                                        }
                                                        if (arrSettings[i].getReportType().equals(1)) {
                                                            pdffilename = reportManagement.generateReport(monitortracking, 0, arrSettings[i].getMonitorType(), receiverSetting.getDnshost());
                                                            //csvfilename = reportManagement.generateReport(sessionId, monitortracking, 0 , arrSettings[i].getMonitorType());
                                                            files = new String[1];
                                                            files[0] = pdffilename;

                                                        }
                                                        if (arrSettings[i].getReportType().equals(2)) {
                                                            //pdffilename = reportManagement.generateReport(sessionId, monitortracking, 0 , arrSettings[i].getMonitorType());
                                                            csvfilename = reportManagement.generateReport(monitortracking, 1, arrSettings[i].getMonitorType(), receiverSetting.getDnshost());
                                                            files = new String[1];
                                                            files[0] = csvfilename;
                                                        }

                                                        axStatus = send.SendEmail(channelId, aOperator.getEmailid(), "REPORT FOR MONITOR", "report",
                                                                emailList, null, files, mimetypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                                        //System.out.println((new Date()) + ">> Scheduler Tracking Added Successfully!!!");
                                                        continue;
                                                    }
                                                } else {
                                                    //System.out.println((new Date()) + ">> Failed to add Scheduler Tracking!!!");
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                }
                                records = new Lookup(receiverSetting.getDnsname(), Type.MX).run();
                                String[] dnsmx = new String[records.length];
                                if (!records.equals(null)) {
                                    for (i = 0; i < records.length; i++) {
                                        MXRecord mx = (MXRecord) records[i];
                                        //System.out.println("MX " + mx.getTarget());
                                        dnsmx[i] = new String();
                                        Name s = mx.getTarget();

                                        String[] dnsm = receiverSetting.getDnsNS();

                                        for (i = 0; i < dnsm.length; i++) {
                                            dnsmx[i] = s.toString();
                                            if (dnsm[i].equals(dnsmx[i])) {
                                                Date startdate = new Date();
                                                Date enddate = new Date();
                                                long elasedTime = System.currentTimeMillis() - starTime;
                                                timeout = Long.toString(elasedTime);
                                                //System.out.println("Speed:  " + elasedTime);
                                                int res = setting.AddReceiverTracking(channelId, arrSettings[0].getMonitorName(), timeout + "ms",
                                                        arrSettings[0].getMonitorType(), startdate, enddate);
                                            } else {
                                                long elasedTime = System.currentTimeMillis() - starTime;
                                                timeout = Long.toString(elasedTime);
                                                axStatus = setting.sendMessagemonitor(channelId, receiverSetting, aOperator.getEmailid(), emailList, monitorSourcID, "DNS Monitor alert", "Mail Servers are changed");
                                                Date startdate = new Date();
                                                Date enddate = new Date();

                                                int res = setting.AddReceiverTracking(channelId, arrSettings[i].getMonitorName(), timeout + "ms",
                                                        arrSettings[i].getMonitorType(), startdate, enddate);

                                                if (res == 0) {
                                                    Monitortracking greetObj = setting.getMonitorTracking(channelId, arrSettings[i].getMonitorName());
                                                    if (greetObj != null) {

                                                        MonitorSettingsManagement management = new MonitorSettingsManagement();
                                                        Monitortracking[] monitortracking = management.getMonitorTrackingByName(channelId, greetObj.getMonitorName());
                                                        MonitorReportManagement reportManagement = new MonitorReportManagement();
                                                        if (arrSettings[i].getReportType().equals(0)) {
                                                            files = new String[2];
                                                            files[0] = pdffilename;
                                                            files[1] = csvfilename;

                                                            pdffilename = reportManagement.generateReport(monitortracking, 0, arrSettings[i].getMonitorType(), receiverSetting.getDnshost());
                                                            csvfilename = reportManagement.generateReport(monitortracking, 1, arrSettings[i].getMonitorType(), receiverSetting.getDnshost());
                                                        }
                                                        if (arrSettings[i].getReportType().equals(1)) {
                                                            pdffilename = reportManagement.generateReport(monitortracking, 0, arrSettings[i].getMonitorType(), receiverSetting.getDnshost());
                                                            //csvfilename = reportManagement.generateReport(sessionId, monitortracking, 0 , arrSettings[i].getMonitorType());
                                                            files = new String[1];
                                                            files[0] = pdffilename;

                                                        }
                                                        if (arrSettings[i].getReportType().equals(2)) {
                                                            //pdffilename = reportManagement.generateReport(sessionId, monitortracking, 0 , arrSettings[i].getMonitorType());
                                                            csvfilename = reportManagement.generateReport(monitortracking, 1, arrSettings[i].getMonitorType(), receiverSetting.getDnshost());
                                                            files = new String[1];
                                                            files[0] = csvfilename;
                                                        }

                                                        axStatus = send.SendEmail(channelId, aOperator.getEmailid(), "REPORT FOR MONITOR", "report",
                                                                emailList, null, files, mimetypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                                        //System.out.println((new Date()) + ">> Scheduler Tracking Added Successfully!!!");
                                                        continue;
                                                    }
                                                } else {
                                                    //System.out.println((new Date()) + ">> Failed to add Scheduler Tracking!!!");
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                }
                            } catch (TextParseException ex) {
                                ex.printStackTrace();
                            }
                        }
                    } else if (arrSettings[i].getMonitorType() == MonitorSettingsManagement.PORTMONITOR) {
                        int flag = 0;
                        String port = receiverSetting.getPortport();
                        int porti = Integer.parseInt(port);
                        long starTime = System.currentTimeMillis();
                        Connectivity con = new Connectivity();
                        int result = con.Telnet(receiverSetting.getPorthost(), porti);

                        long timetaken = System.currentTimeMillis() - starTime;
                        String timeout = Long.toString(timetaken);
                        //System.out.println("Speed:  " + timetaken);
                        if (result == 0) {
                            flag++;
                            for (int k = 0; k <= Integer.parseInt(receiverSetting.getDropcount()); k++) {
                                flag++;
                                if (flag > Integer.parseInt(receiverSetting.getDropcount())) {
                                    axStatus = setting.sendMessagemonitor(channelId, receiverSetting, aOperator.getEmailid(), emailList, monitorSourcID, "Port Monitor alert", "Unable to reach server");
                                    Date startdate = new Date();
                                    Date enddate = new Date();
                                    int res = setting.AddReceiverTracking(channelId, arrSettings[i].getMonitorName(), "0ms",
                                            arrSettings[i].getMonitorType(), startdate, enddate);

                                    if (res == 0) {
                                        Monitortracking greetObj = setting.getMonitorTracking(channelId, arrSettings[i].getMonitorName());
                                        if (greetObj != null) {

                                            MonitorSettingsManagement management = new MonitorSettingsManagement();
                                            Monitortracking[] monitortracking = management.getMonitorTrackingByName(channelId, greetObj.getMonitorName());
                                            MonitorReportManagement reportManagement = new MonitorReportManagement();
                                            if (arrSettings[i].getReportType().equals(0)) {
                                                files = new String[2];
                                                files[0] = pdffilename;
                                                files[1] = csvfilename;

                                                pdffilename = reportManagement.generateReport(monitortracking, 0, arrSettings[i].getMonitorType(), receiverSetting.getPorthost() + ":" + receiverSetting.getPortport());
                                                csvfilename = reportManagement.generateReport(monitortracking, 1, arrSettings[i].getMonitorType(), receiverSetting.getPorthost() + ":" + receiverSetting.getPortport());
                                            }
                                            if (arrSettings[i].getReportType().equals(1)) {
                                                pdffilename = reportManagement.generateReport(monitortracking, 0, arrSettings[i].getMonitorType(), receiverSetting.getPorthost() + ":" + receiverSetting.getPortport());
                                                //csvfilename = reportManagement.generateReport(sessionId, monitortracking, 0 , arrSettings[i].getMonitorType());
                                                files = new String[1];
                                                files[0] = pdffilename;

                                            }
                                            if (arrSettings[i].getReportType().equals(2)) {
                                                //pdffilename = reportManagement.generateReport(sessionId, monitortracking, 0 , arrSettings[i].getMonitorType());
                                                csvfilename = reportManagement.generateReport(monitortracking, 1, arrSettings[i].getMonitorType(), receiverSetting.getPorthost() + ":" + receiverSetting.getPortport());
                                                files = new String[1];
                                                files[0] = csvfilename;
                                            }

                                            axStatus = send.SendEmail(channelId, aOperator.getEmailid(), "REPORT FOR MONITOR", "report",
                                                    emailList, null, files, mimetypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                            //System.out.println((new Date()) + ">> Scheduler Tracking Added Successfully!!!");
                                            continue;
                                        }
                                    } else {
                                        //System.out.println((new Date()) + ">> Failed to add Scheduler Tracking!!!");
                                        continue;
                                    }
                                }
                            }
                        } else {
                            Date startdate = new Date();
                            Date enddate = new Date();
                            int res = setting.AddReceiverTracking(channelId, arrSettings[i].getMonitorName(), timeout + "ms",
                                    arrSettings[i].getMonitorType(), startdate, enddate);

                        }

                    } else if (arrSettings[i].getMonitorType() == MonitorSettingsManagement.PING) {
                        int statusCode = 0;
                        String pingip = receiverSetting.getPinghost();
                        InetAddress inet = null;
                        //long starTime = System.currentTimeMillis();
                        //System.out.println("starTime   " + starTime);
                        //String timeout = "0";
                        String time = "";
                        try {
                            inet = InetAddress.getByName(pingip);
                        } catch (UnknownHostException ex) {
                            ex.printStackTrace();
                        }
                        String pingCmd = "ping " + pingip;

                        //get the runtime to execute the command
                        Runtime runtime = Runtime.getRuntime();
                        try {
                            Process process = runtime.exec(pingCmd);
                            //Gets the inputstream to read the output of the command
                            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));

                            if (System.getProperty("file.separator").equals("/")) {
                                //reads the outputs
                                String inputLine = in.readLine();
                                while ((inputLine != null)) {
                                    if (inputLine.length() > 0 && inputLine.contains("time")) {
                                        time = inputLine.substring(inputLine.indexOf("time"));
                                        break;
                                    } else {
                                        break;
                                    }

                                }
                                //System.out.println("time --> " + time);
                            } else {
                                //reads the outputs
                                String inputLine = in.readLine();
                                while ((inputLine != null)) {
                                    if (inputLine.length() > 0 && inputLine.contains("time")) {
                                        time = inputLine.substring(inputLine.indexOf("time"));
                                        break;
                                    }
                                    inputLine = in.readLine();
                                }
                                //System.out.println("time --> " + time);
                                time = time.substring(0, time.indexOf(' '));
                            }
                        } catch (Exception ex) {
                            System.out.println(ex);
                        }
                        //System.out.println("Sending Ping Request to " + pingip);
                        try {
                            boolean f = inet.isReachable(5000);
                            if (inet.isReachable(5000)) {
                                //System.out.println(pingip + " is reachable");
                                Date startdate = new Date();
                                Date enddate = new Date();
                                int res = setting.AddReceiverTracking(channelId, arrSettings[i].getMonitorName(), time.substring(5),
                                        arrSettings[i].getMonitorType(), startdate, enddate);
                            } else {
                                int flag = 0;
                                //System.out.println(pingip + " is not reachable");
                                for (int k = 0; k <= Integer.parseInt(receiverSetting.getDropcount()); k++) {
                                    flag++;
                                    if (flag > Integer.parseInt(receiverSetting.getDropcount())) {
                                        axStatus = setting.sendMessagemonitor(channelId, receiverSetting, aOperator.getEmailid(), emailList, monitorSourcID, "Alert", pingip + " is not reachable");
                                        Date startdate = new Date();
                                        Date enddate = new Date();
                                        int res = setting.AddReceiverTracking(channelId, arrSettings[i].getMonitorName(), "0ms",
                                                arrSettings[i].getMonitorType(), startdate, enddate);
                                        if (res == 0) {
                                            Monitortracking greetObj = setting.getMonitorTracking(channelId, arrSettings[i].getMonitorName());
                                            if (greetObj != null) {
                                                MonitorSettingsManagement management = new MonitorSettingsManagement();
                                                Monitortracking[] monitortracking = management.getMonitorTrackingByName(channelId, greetObj.getMonitorName());
                                                MonitorReportManagement reportManagement = new MonitorReportManagement();
                                                if (arrSettings[i].getReportType().equals(0)) {
                                                    files = new String[2];
                                                    files[0] = pdffilename;
                                                    files[1] = csvfilename;

                                                    pdffilename = reportManagement.generateReport(monitortracking, 0, arrSettings[i].getMonitorType(), receiverSetting.getPinghost());
                                                    csvfilename = reportManagement.generateReport(monitortracking, 1, arrSettings[i].getMonitorType(), receiverSetting.getPinghost());
                                                }
                                                if (arrSettings[i].getReportType().equals(1)) {
                                                    pdffilename = reportManagement.generateReport(monitortracking, 0, arrSettings[i].getMonitorType(), receiverSetting.getPinghost());
                                                    //csvfilename = reportManagement.generateReport(sessionId, monitortracking, 0 , arrSettings[i].getMonitorType());
                                                    files = new String[1];
                                                    files[0] = pdffilename;

                                                }
                                                if (arrSettings[i].getReportType().equals(2)) {
                                                    //pdffilename = reportManagement.generateReport(sessionId, monitortracking, 0 , arrSettings[i].getMonitorType());
                                                    csvfilename = reportManagement.generateReport(monitortracking, 1, arrSettings[i].getMonitorType(), receiverSetting.getPinghost());
                                                    files = new String[1];
                                                    files[0] = csvfilename;
                                                }

                                                axStatus = send.SendEmail(channelId, aOperator.getEmailid(), "REPORT FOR MONITOR", "report",
                                                        emailList, null, files, mimetypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                                //System.out.println((new Date()) + ">> Scheduler Tracking Added Successfully!!!");
                                                continue;
                                            }
                                        } //
                                        else {
                                            //System.out.println((new Date()) + ">> Failed to add Scheduler Tracking!!!");
                                            continue;
                                        }
                                    }
                                }
                            }
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                    } else if (arrSettings[i].getMonitorType() == MonitorSettingsManagement.SSL) {
                        try {
                            // configure the SSLContext with a TrustManager
                            SSLContext ctx = null;
                            try {
                                ctx = SSLContext.getInstance("TLS");
                            } catch (NoSuchAlgorithmException ex) {
                                ex.printStackTrace();
                            }
                            try {
                                ctx.init(new KeyManager[0], new TrustManager[]{new DefaultTrustManager()}, new SecureRandom());
                            } catch (KeyManagementException ex) {
                                ex.printStackTrace();
                            }
                            SSLContext.setDefault(ctx);
                            String protocol = receiverSetting.getSelectedprotocol();
                            String host = receiverSetting.getSslHost();
                            String port = receiverSetting.getSslPort();
                            String url1 = protocol + "://" + host;
                            //  +":"+ port;
                            long starTime = System.currentTimeMillis();
                            URL url = new URL(url1);//https://mms.nw.ru
                            HttpsURLConnection conn = null;
                            try {
                                conn = (HttpsURLConnection) url.openConnection();

                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                            long timetaken = System.currentTimeMillis() - starTime;
                            String timeout = Long.toString(timetaken);
                            conn.setHostnameVerifier(new HostnameVerifier() {
                                @Override
                                public boolean verify(String arg0, SSLSession arg1) {
                                    return true;
                                }
                            });
                            try {
                                System.out.println(conn.getResponseCode());
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                            Certificate[] certs = null;
                            try {
                                certs = conn.getServerCertificates();
                            } catch (SSLPeerUnverifiedException ex) {
                                ex.printStackTrace();
                            }
                            for (Certificate cert : certs) {
                                //System.out.println(cert.getType());
                                //System.out.println(cert);
                                System.out.println("\n");
                                X509Certificate certi = (X509Certificate) cert;
                                //System.out.println("exp " + certi.getNotAfter());
                                // Setting the pattern
                                SimpleDateFormat sm = new SimpleDateFormat("dd-MM-yyyy");
                                // myDate is the java.util.Date in yyyy-mm-dd format
                                // Converting it into String using formatter

                                Date expiry_date = certi.getNotAfter();
                                String strDate = sm.format(expiry_date);

                                //System.out.println("Expiry date" + strDate);
                                String dates[] = strDate.split("-");
                                int date1 = Integer.parseInt(dates[0]);
                                int month = Integer.parseInt(dates[1]);
                                int year = Integer.parseInt(dates[2]);
                                //System.out.println("Expiry Month= "+month +"Expiry Year= "+year);
                                month = month - 1;
                                if (month == 0) {
                                    month = 12;
                                    year = year - 1;
                                }
                                //System.out.println("\n***********************");

                                Date current_date = new Date();
                                String currDate = sm.format(current_date);
                                String datescurrent[] = currDate.split("-");
                                int cdate = Integer.parseInt(dates[0]);
                                int cmonth = Integer.parseInt(datescurrent[1]);
                                int cyear = Integer.parseInt(datescurrent[2]);

                                //System.out.println("Current Month= "+cmonth +"Current Year= "+cyear);
                                if (year == cyear && month == cmonth && date1 == cdate) {
                                    axStatus = setting.sendMessagemonitor(channelId, receiverSetting, aOperator.getEmailid(), emailList, monitorSourcID, "SSL alert", "Certificate is going to expire in next 30 days");
                                    Date startdate = new Date();
                                    Date enddate = new Date();
                                    int res = setting.AddReceiverTracking(channelId, arrSettings[i].getMonitorName(), timeout + "ms",
                                            arrSettings[i].getMonitorType(), startdate, enddate);

                                    if (res == 0) {
                                        Monitortracking greetObj = setting.getMonitorTracking(channelId, arrSettings[i].getMonitorName());
                                        if (greetObj != null) {

                                            MonitorSettingsManagement management = new MonitorSettingsManagement();
                                            Monitortracking[] monitortracking = management.getMonitorTrackingByName(channelId, greetObj.getMonitorName());
                                            MonitorReportManagement reportManagement = new MonitorReportManagement();
                                            if (arrSettings[i].getReportType().equals(0)) {
                                                files = new String[2];
                                                files[0] = pdffilename;
                                                files[1] = csvfilename;

                                                pdffilename = reportManagement.generateReport(monitortracking, 0, arrSettings[i].getMonitorType(), receiverSetting.getSelectedprotocol() + "://" + receiverSetting.getSslHost());
                                                csvfilename = reportManagement.generateReport(monitortracking, 1, arrSettings[i].getMonitorType(), receiverSetting.getSelectedprotocol() + "://" + receiverSetting.getSslHost());
                                            }
                                            if (arrSettings[i].getReportType().equals(1)) {
                                                pdffilename = reportManagement.generateReport(monitortracking, 0, arrSettings[i].getMonitorType(), receiverSetting.getSelectedprotocol() + "://" + receiverSetting.getSslHost());
                                                //csvfilename = reportManagement.generateReport(sessionId, monitortracking, 0 , arrSettings[i].getMonitorType());
                                                files = new String[1];
                                                files[0] = pdffilename;

                                            }
                                            if (arrSettings[i].getReportType().equals(2)) {
                                                //pdffilename = reportManagement.generateReport(sessionId, monitortracking, 0 , arrSettings[i].getMonitorType());
                                                csvfilename = reportManagement.generateReport(monitortracking, 1, arrSettings[i].getMonitorType(), receiverSetting.getSelectedprotocol() + "://" + receiverSetting.getSslHost());
                                                files = new String[1];
                                                files[0] = csvfilename;
                                            }

                                            axStatus = send.SendEmail(channelId, aOperator.getEmailid(), "REPORT FOR MONITOR", "report",
                                                    emailList, null, files, mimetypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                            //System.out.println((new Date()) + ">> Scheduler Tracking Added Successfully!!!");
                                            continue;
                                        }
                                    } else {
                                        //System.out.println((new Date()) + ">> Failed to add Scheduler Tracking!!!");
                                        continue;
                                    }

                                } else {
                                    Date startdate = new Date();
                                    Date enddate = new Date();
                                    int res = setting.AddReceiverTracking(channelId, arrSettings[i].getMonitorName(), "Certificate is not going to expire",
                                            arrSettings[i].getMonitorType(), startdate, enddate);
                                }
                            }
                            conn.disconnect();

                        } catch (MalformedURLException ex) {
                            ex.printStackTrace();
                        }

                    }
                }
            }
        }
    }

    private static class DefaultTrustManager implements X509TrustManager {

        @Override
        public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }

}
