/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.access.monitorsettings;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Monitorsettings;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.SettingsUtil;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.MonitorSettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.NucleusMonitorSettings;
import com.mollatech.axiom.nucleus.settings.WebSealSetting;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.URL;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Hashtable;
import javax.naming.NamingEnumeration;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.xbill.DNS.ARecord;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.MXRecord;
import org.xbill.DNS.NSRecord;
import org.xbill.DNS.Name;
import org.xbill.DNS.Record;
import org.xbill.DNS.Type;

/**
 *
 * @author pramod
 */
public class AddMSettings extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AddMSettings.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        final String itemtype = "MONITORSETTINGS";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "", message = "";
        response.setContentType("application/json");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operator :: "+operator.getName());
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        AuditManagement audit = new AuditManagement();
        String operatorId = operator.getOperatorid();
        String name = request.getParameter("_name");
        log.debug("name :: "+name);
        String st = request.getParameter("_selectedType");
        log.debug("st :: "+st);
        int status = Integer.parseInt(request.getParameter("_status"));
        log.debug("status :: "+status);
        int edit = Integer.parseInt(request.getParameter("_edit"));
        log.debug("edit :: "+edit);
        int monitorType = Integer.parseInt(request.getParameter("_selectedType"));
        log.debug("monitorType :: "+monitorType);
        String frequency = request.getParameter("_frequency");
        log.debug("frequency :: "+frequency);
        String responseTime = request.getParameter("_timeout");
        log.debug("responseTime :: "+responseTime);
        String snmp = request.getParameter("_selectedSNMP");
        log.debug("snmp :: "+snmp);
        String monitors = request.getParameter("_servicesalert");
        log.debug("monitors :: "+monitors);
        String alert = request.getParameter("_selectedAlert");
        log.debug("alert :: "+alert);
        String _dropcount = request.getParameter("_dropcount");
        log.debug("_dropcount :: "+_dropcount);
        String uniqueId = null;
        alert = alert.trim();
        int report = Integer.parseInt(request.getParameter("_selectedReoprt"));
        log.debug("report :: "+report);
        NucleusMonitorSettings nms = new NucleusMonitorSettings();
        String template = request.getParameter("_templateName");
        log.debug("template :: "+template);
//        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.WEB_WATCH) != 0) {
//            result = "error";
//            message = "This feature is not available in this license!!!";
//            try {
//                json.put("_result", result);
//                json.put("_message", message);
//            } catch (Exception e) {
//                log.error("Exception caught :: ",e);
//            }
//            out.print(json);
//            out.flush();
//            return;
//        }
        MonitorSettingsManagement management = new MonitorSettingsManagement();
//        int iCount = AxiomProtect.GetCountsAllowed(AxiomProtect.WEB_WATCH_COUNT);
//        int currCount = management.SettingCount(channel.getChannelid());
//        if (iCount != -998 && currCount > iCount) {
//            result = "error";
//            message = "Monitor Addition is not available in this license!!!";
//            try {
//                json.put("_result", result);
//                json.put("_message", message);
//            } catch (Exception e) {
//                log.error("Exception caught :: ",e);
//            }
//            out.print(json);
//            out.flush();
//            return;
//        }

//        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.WEB_WATCH) != 0) {
//            result = "error";
//            message = "This feature is not available in this license!!!";
//            try {
//                json.put("_result", result);
//                json.put("_message", message);
//            } catch (Exception e) {
//                log.error("Exception caught :: ",e);
//            }
//            out.print(json);
//            out.flush();
//            return;
//        }
        if (template == null || template.isEmpty() == true) {
            result = "error";
            message = "Please select template!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        String iTemplateId = request.getParameter("_templateName");
        log.debug("iTemplateId :: "+iTemplateId);
        nms.setTemplateId(iTemplateId);

        try {
            String regex = "^[a-zA-Z]+$";
            if (name.equals("") || alert.equals("null") || _dropcount == null || _dropcount.isEmpty() == true || UtilityFunctions.isValidPhoneNumber(_dropcount) == false || responseTime == null || responseTime.isEmpty() == true || UtilityFunctions.isValidPhoneNumber(responseTime) == false || name.matches(regex) == false) {
                result = "error";
                message = "Enter Correct Data...";
                return;
            } else {
                if (monitorType == 1) {
                    disableSslVerification();
                    String webadd = request.getParameter("_webAddress");
                    log.debug("webadd :: "+webadd);
                    String _imagescript = request.getParameter("_imagescript");
                    log.debug("_imagescript :: "+_imagescript);
                    uniqueId = request.getParameter("_UniqueIdVal");
                    log.debug("uniqueId :: "+uniqueId);
                    if (webadd.equals("")) {
                        result = "error";
                        message = "Enter Correct Data...";
                        return;
                    }
                    if (request.getParameter("_webSealURL").equals("")) {
                        result = "error";
                        message = "Please do setting for WebSeal.";
                        return;
                    } else {
                        nms.setWebadd(webadd);
                        nms.setWebseal(_imagescript);
                    }
                    URL url = new URL(webadd);
                    if (url.getProtocol().equals("https")) {
                        Certificate cert_To_Upload = null;
                        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
                        connection.connect();
                        Certificate[] certs = connection.getServerCertificates();
                        for (int i = 0; i < certs.length; i++) {
                            cert_To_Upload = certs[i];
                            X509Certificate t = (X509Certificate) cert_To_Upload;
                            if (t.getNotAfter().before(new Date())) {
                                throw new Exception();
                            }
                        }
                        connection.disconnect();
                        nms.setWebCert(SettingsUtil.serializeToObject(certs));
                    }
                    SettingsManagement sMngmt = new SettingsManagement();
                    Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.WEBSEAL_SETTINGS, SettingsManagement.PREFERENCE_ONE);
                    if (settingsObj == null) {
                        throw new Exception();
                    }
                    WebSealSetting setting = (WebSealSetting) settingsObj;
                    JSONObject object = new JSONObject(setting.dns);
                    Hashtable<String, String> env = new Hashtable<String, String>();
                    env.put("java.naming.factory.initial", "com.sun.jndi.dns.DnsContextFactory");
                    JSONObject dnsJSON = new JSONObject();
                    boolean dnsFlag = false;
                    for (int i = 0; i < 3; i++) {
                        String dns = (String) object.get("_dnsForWeb" + (i + 1));
                        String ipFromDNS = "";
                        try {
                            if (!dns.equals("")) {
                                env.put("java.naming.provider.url", "dns://" + dns);
                                DirContext ictx = null;

                                ictx = new InitialDirContext(env);
                                Attributes attrs1 = ictx.getAttributes(url.getAuthority(), new String[]{"A"});
                                NamingEnumeration enumeration = attrs1.get("a").getAll();
                                while (enumeration.hasMore()) {
                                    ipFromDNS += enumeration.next().toString() + ",";
                                    dnsFlag = true;
                                }
                            }
                        } catch (Exception e) {
                            ipFromDNS = url.getAuthority();
                            dnsFlag = true;
                        }
                        dnsJSON.put("_dnsForWeb" + (i + 1), ipFromDNS);

                    }
                    if (dnsFlag == false) {
                        String ipFromDNS = "";
                        try {
                            env.put("java.naming.provider.url", "dns://" + "8.8.8.8");
                            DirContext ictx = null;

                            ictx = new InitialDirContext(env);
                            Attributes attrs1 = ictx.getAttributes(url.getAuthority(), new String[]{"A"});
                            NamingEnumeration enumeration = attrs1.get("a").getAll();
                            while (enumeration.hasMore()) {
                                System.out.println("name is : " + enumeration.next().toString());
                                ipFromDNS += enumeration.next().toString() + ",";
                            }
                        } catch (Exception ex) {
                            ipFromDNS = url.getAuthority();
                        }
                        dnsJSON.put("_dnsForWeb1", ipFromDNS);
                    }
                    nms.setWebDNS(dnsJSON.toString());
                } else if (monitorType == 4) {
                    String dnshost = request.getParameter("_dnshost");
                    log.debug("dnshost :: "+dnshost);
                    String dnsport = request.getParameter("_dnsport");
                    log.debug("dnsport :: "+dnsport);
                    String dnsname = request.getParameter("_dnsname");
                    log.debug("dnsname :: "+dnsname);
                    String selectedlookup = request.getParameter("_selectedLookup");
                    log.debug("selectedlookup :: "+selectedlookup);
                    String linktodns = request.getParameter("linktodns");
                    log.debug("linktodns :: "+linktodns);
                    String linkfordns = request.getParameter("linkfordns");
                    log.debug("linkfordns :: "+linkfordns);
                    if (dnshost.equals("") || dnsport.equals("") || dnsname.equals("") || selectedlookup.equals("") || !dnsport.matches("\\d+")) {
                        result = "error";
                        message = "Enter Correct Data...";
                        return;
                    } else {
                        nms.setDnshost(dnshost);
                        nms.setDnsport(dnsport);
                        nms.setDnsname(dnsname);
                        nms.setSelectedLookup(selectedlookup);
                        nms.setLinkselectfordns(linkfordns);
                        if (linkfordns.equals("yes")) {
                            nms.setLinktodns(linktodns);
                        } else {
                            nms.setLinktodns("");
                        }
                        Record[] records;
                        if (selectedlookup.equals("a")) {
                            records = new Lookup(dnsname, Type.A).run();
                            String[] dnsip = new String[records.length];
                            if (!records.equals(null)) {
                                for (int i = 0; i < records.length; i++) {
                                    ARecord a = (ARecord) records[i];
                                    System.out.println("A " + a.getAddress());
                                    dnsip[i] = new String();
                                    InetAddress s = a.getAddress();
                                    dnsip[i] = s.toString();
                                }
                            }
                            nms.setDnsA(dnsip);

                        }
                        if (selectedlookup.equals("ns")) {
                            records = new Lookup(dnsname, Type.NS).run();
                            String[] dnsip = new String[records.length];
                            if (!records.equals(null)) {
                                for (int i = 0; i < records.length; i++) {
                                    NSRecord ns = (NSRecord) records[i];
                                    System.out.println("Nameserver " + ns.getTarget());
                                    dnsip[i] = new String();
                                    Name s = ns.getTarget();
                                    dnsip[i] = s.toString();
                                }
                            }
                            nms.setDnsNS(dnsip);
                        }
                        if (selectedlookup.equals("mx")) {
                            records = new Lookup(dnsname, Type.MX).run();
                            String[] dnsip = new String[records.length];
                            if (!records.equals(null)) {
                                for (int i = 0; i < records.length; i++) {
                                    MXRecord mx = (MXRecord) records[i];
                                    System.out.println("MX " + mx.getTarget());
                                    dnsip[i] = new String();
                                    Name s = mx.getTarget();
                                    dnsip[i] = s.toString();
                                }
                            }
                            nms.setDnsMX(dnsip);
                        }
                        if (selectedlookup.equals("all")) {

                            records = new Lookup(dnsname, Type.A).run();
                            String[] dnsip = new String[records.length];
                            if (!records.equals(null)) {
                                for (int i = 0; i < records.length; i++) {
                                    ARecord a = (ARecord) records[i];
                                    System.out.println("A " + a.getAddress());
                                    dnsip[i] = new String();
                                    InetAddress s = a.getAddress();
                                    dnsip[i] = s.toString();
                                }
                            }
                            nms.setDnsA(dnsip);

                            records = new Lookup(dnsname, Type.NS).run();
                            String[] dnsa = new String[records.length];
                            if (!records.equals(null)) {
                                for (int i = 0; i < records.length; i++) {
                                    NSRecord ns = (NSRecord) records[i];
                                    System.out.println("Nameserver " + ns.getTarget());
                                    dnsa[i] = new String();
                                    Name s = ns.getTarget();
                                    dnsa[i] = s.toString();
                                }
                            }
                            nms.setDnsNS(dnsa);

                            records = new Lookup(dnsname, Type.MX).run();
                            String[] dnsmx = new String[records.length];
                            if (!records.equals(null)) {
                                for (int i = 0; i < records.length; i++) {
                                    MXRecord mx = (MXRecord) records[i];
                                    System.out.println("MX " + mx.getTarget());
                                    dnsmx[i] = new String();
                                    Name s = mx.getTarget();
                                    dnsmx[i] = s.toString();
                                }
                            }
                            nms.setDnsMX(dnsmx);

                        }
                    }
                } else if (monitorType == 6) {
                    String porthost = request.getParameter("_Porthost");
                    log.debug("porthost :: "+porthost);
                    String portport = request.getParameter("_Portport");
                    log.debug("portport :: "+portport);
                    boolean check = management.isValidIP(porthost);

                    if (check == false) {
                        result = "error";
                        message = "Enter valid IP...";
                        return;
                    }
                    String portcommand = request.getParameter("_Portcommand");
                    log.debug("portcommand :: "+portcommand);
                    if (porthost.equals("") || portport.equals("") || portcommand.equals("") || !portport.matches("\\d+")) {
                        result = "error";
                        message = "Enter Correct Data...";
                        return;
                    }
                    if (!portcommand.equals("telnet")) {
                        result = "error";
                        message = "Enter valid command...";
                        return;
                    } else {
                        nms.setPorthost(porthost);
                        nms.setPortport(portport);
                        nms.setPortcommand(portcommand);

                    }
                } else if (monitorType == 9) {
                    String Pinghost = request.getParameter("_Pinghost");
                    log.debug("Pinghost :: "+Pinghost);
                    boolean check = management.isValidIP(Pinghost);

                    if (check == false) {
                        result = "error";
                        message = "Enter valid IP...";
                        return;
                    }
                    if (Pinghost.equals("")) {
                        result = "error";
                        message = "Enter Correct Data...";
                        return;
                    } else {
                        nms.setPinghost(Pinghost);

                    }
                } else if (monitorType == 10) {
                    String selectedprotocol = request.getParameter("_selectedprotocol");
                    log.debug("selectedprotocol :: "+selectedprotocol);
                    String sslHost = request.getParameter("_sslHost");
                    log.debug("sslHost :: "+sslHost);
                    String sslPort = request.getParameter("_sslPort");
                    log.debug("sslPort :: "+sslPort);
                    String linktossl = request.getParameter("linktossl");
                    log.debug("linktossl :: "+linktossl);
                    String notify = request.getParameter("_notify");
                    log.debug("notify :: "+notify);
                    if (sslHost.equals("") || sslPort.equals("") || notify.equals("") || !sslPort.matches("\\d+")) {
                        result = "error";
                        message = "Enter Correct Data...";
                        return;
                    } else {
                        nms.setSelectedprotocol(selectedprotocol);
                        nms.setSslHost(sslHost);
                        nms.setSslPort(sslPort);
                        nms.setNotify(notify);
                        String linkforssl = request.getParameter("linkforssl");
                        log.debug("linkforssl :: "+linkforssl);
                        nms.setLinkselectforssl(linkforssl);
                        if (linkforssl.equals("yes")) {
                            nms.setLinktossl(linktossl);
                        } else {
                            nms.setLinktossl("");
                        }
                    }
                }
                nms.setSelectedalert(alert);

                nms.setTimeout(responseTime);
                nms.setFrequency(frequency);

                nms.setDropcount(_dropcount);
                boolean exist = false;
                Monitorsettings results = null;
                results = management.getmonitorsetting(name);
                if (results != null && edit != 1) {
                    result = "error";
                    message = "Please enter unique name...";
                    return;
                } else if (results != null && edit == 1) {
                    String resultString = "error";
                    int val = management.updateMonitorsetting(sessionId, channel.getChannelid(), name, status, monitorType, report, nms, uniqueId);
                    log.debug("updateMonitorsetting :: "+val);
                    if (val == 0) {
                        if (val == 0) {
                            resultString = "Success";
                            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorId,
                                    request.getRemoteAddr(),
                                    channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                                    "Add Monitor Setting", resultString, val, "Monitor Management",
                                    "monitorName=" + results.getMonitorName() + "monitorStatus =" + results.getMonitorStatus() + "Report Type=" + results.getReportType()
                                    + "Monitor Type=" + results.getMonitorType(), "monitorName=" + name + "monitorStatus =" + status + "Report Type=" + report
                                    + "Monitor Type=" + monitorType, itemtype, operatorId);
                        } else {
                            audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                                    request.getRemoteAddr(),
                                    channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                                    "Add Monitor Setting", resultString, val, "Dictum Receiver Management",
                                    "", "Failed To Add Monitor Setting",
                                    itemtype, operatorId);
                        }
                        result = "success";
                        message = "Setting updated successfully...";
                        return;
                    } else {
                        result = "error";
                        message = "Error in updating...";
                        return;
                    }
                } else {
                    int value = management.addMonitorsetting(sessionId, channel.getChannelid(), name, status, monitorType, report, nms, uniqueId);
                    log.debug("addMonitorsetting :: "+value);
                    String resultString = "Failure";
                    if (value == 0) {
                        resultString = "Success";
                        audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorId,
                                request.getRemoteAddr(),
                                channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                                "Add Monitor Setting", resultString, value, "Monitor Management",
                                "", "monitorName=" + name + "monitorStatus =" + status + "Report Type=" + report
                                + "Monitor Type=" + monitorType, itemtype, operatorId);
                    } else {
                        audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                                request.getRemoteAddr(),
                                channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                                "Add Monitor Setting", resultString, value, "Dictum Receiver Management",
                                "", "Failed To Add Monitor Setting",
                                itemtype, operatorId);
                    }
                    if (value == 0) {
                        result = "success";
                        message = "Settings added successfully!!!";

                        return;
                    } else {
                        result = "error";
                        message = "Failed to add settings!!!";
                        return;
                    }
                }

            }
        } catch (Exception e) {
            result = "error";
            message = e.getMessage();
            log.error("Exception caught :: ",e);
        } finally {
            try {
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }

        }
    }

    private void disableSslVerification() {
        try {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
            };

            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        }
        log.info("Servlet ended");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
