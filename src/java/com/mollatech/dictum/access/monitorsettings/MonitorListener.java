/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.access.monitorsettings;

import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import java.util.Date;
import java.util.Timer;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.hibernate.Session;
import static org.quartz.JobBuilder.newJob;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import org.quartz.Trigger;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * Web application lifecycle listener.
 *
 * @author nilesh
 */
public class MonitorListener implements ServletContextListener {

    private static Timer timer;
    private static int intervalCall;
    SchedulerFactory schedFact = new org.quartz.impl.StdSchedulerFactory();
    Scheduler sched = null;
    private static Channels channel;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
            Session sChannel = suChannel.openSession();
            ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
            ServletContext ch = sce.getServletContext();
            String _channelName = ch.getContextPath();
            _channelName = _channelName.replaceAll("/", "");
            if (_channelName.compareTo("base") != 0) {
                _channelName = _channelName.replaceAll("base", "");
            } else {
                _channelName = "access";
            }
            channel = cUtil.getChannel(_channelName);
            if (channel == null) {
                System.out.println((new Date()) + " >> Channel Details could not be found>>" + _channelName);
                return;
            }
            String interval = LoadSettings.g_sSettings.getProperty("scheduler.run.check.time");
            if (interval == null) {
                System.out.println((new Date()) + " >> Please set value for scheduler.run.check.time variable in dbsetting.conf file");
                return;
            }
            if (interval != null) {
                intervalCall = new Integer(interval).intValue();
            } else {
                intervalCall = 30;
            }
            String channelid = channel.getChannelid();
            JobDetail job = newJob(MonitorScheduler.class).withIdentity("MonitorScheduler", "MonitorScheduler").usingJobData("channelId", channelid).build();

            Trigger trigger = newTrigger()
                    .withIdentity("MonitorSchedulertrigger", "monitor")
                    .startNow()
                    .withSchedule(simpleSchedule()
                            .withIntervalInMinutes(intervalCall)
                            .repeatForever())
                    .build();

            sched = schedFact.getScheduler();
            sched.scheduleJob(job, trigger);
            sched.start();

        } catch (NumberFormatException | SchedulerException ex) {
            System.out.println(ex.getMessage());
        }

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        try {
            sched.shutdown(true);
            //System.out.println((new Date()) + " schedulerlistner() contextDestroyed called...");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
