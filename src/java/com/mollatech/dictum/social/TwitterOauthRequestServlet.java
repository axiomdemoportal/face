package com.mollatech.dictum.social;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import com.mollatech.axiom.face.common.ServletContextUtils;
import com.mollatech.axiom.face.common.ServletContextUtils.TwitterGoogleClientInfo;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.OOBSocialChannelSettings;
import org.bouncycastle.util.encoders.Base64;

public class TwitterOauthRequestServlet extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(TwitterOauthRequestServlet.class.getName());
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.info("Servlet started");
        //System.out.println("TwitterOauthRequestServlet.doPost()");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel);
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        Map<String, TwitterGoogleClientInfo> oauthTokens = ServletContextUtils.getTwitterGoogleClientMemoryStorage(request.getSession(true));
        int port = request.getLocalPort();
        String server = request.getServerName();
        String channelname = request.getContextPath();
//        String twitterCallback="http://".concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("/").concat("twitterAccessTokenRequest");
           
         String URL_TWITTER_OAUTH_CALLBACK = "http://".concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("/").concat("twitterAccessTokenRequest");   //"http://www.mollatech.com:8080/TwitterNotification/twitterAccessTokenRequest";

         SettingsManagement sMngmt = new SettingsManagement();
        OOBSocialChannelSettings socialObj = (OOBSocialChannelSettings) sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.SOCIAL_SETTING, SettingsManagement.PREFERENCE_ONE);
        


        String TWITTER_CONSUMER_KEY = socialObj.getTwitterkey();     //"wruPh262yqy7mrz2IfPZbQ";
        String TWITTER_CONSUMER_SECRET =socialObj.getTwittersecret();   //"BjjTMuOhBUuAHl8YpZAOZUxHYbqoFgnIvusfWS8";
        String TWITTER_OAUTH_SIGNATURE_METHOD = "HMAC-SHA1";
        String URL_TWITTER_OAUTH_TOKEN_REQUEST = "https://api.twitter.com/oauth/request_token";
        String RESPONSE_NO_RESULT = "({\"result\":\"NO_RESULT\"})";
        response.setContentType("application/json");
        final PrintWriter writer = response.getWriter();

        try {
            if (request.getParameter("clientId") != null) {
                Integer clientId = Integer.valueOf(URLDecoder.decode(request.getParameter("clientId")));
//				String text = URLDecoder.decode(request.getParameter("text"));
                String callback = URLDecoder.decode(request.getParameter("callback"));

                String oauth_nonce = UUID.randomUUID().toString().replaceAll("-", "");
                String timestamp = Long.valueOf(((new Date().getTime() + new Date().getTimezoneOffset()) / 1000)).toString();
                String parameter_string = "oauth_callback=" + URLEncoder.encode(URL_TWITTER_OAUTH_CALLBACK) + "&oauth_consumer_key="
                        + TWITTER_CONSUMER_KEY + "&oauth_nonce=" + oauth_nonce + "&oauth_signature_method=" + TWITTER_OAUTH_SIGNATURE_METHOD
                        + "&oauth_timestamp=" + timestamp + "&oauth_version=1.0";
                //System.out.println("parameter_string=" + parameter_string);
                String signature_base_string = "POST&https%3A%2F%2Fapi.twitter.com%2Foauth%2Frequest_token&" + URLEncoder.encode(parameter_string, "UTF-8");
                //System.out.println("signature_base_string=" + signature_base_string);
                String oauth_signature = "";
                try {
                    oauth_signature = computeSignature(signature_base_string, TWITTER_CONSUMER_SECRET + "&"); // note the & at the end. Normally the user access_token would go here, but we don't know it yet for request_token
                    //System.out.println("oauth_signature=" + URLEncoder.encode(oauth_signature, "UTF-8"));
                } catch (GeneralSecurityException e) {
                    log.error("Exception caught :: ",e);
                }
                String authorization_header_string = "OAuth oauth_callback=\"" + URLEncoder.encode(URL_TWITTER_OAUTH_CALLBACK) + "\",oauth_consumer_key=\""
                        + TWITTER_CONSUMER_KEY + "\",oauth_signature_method=\"" + TWITTER_OAUTH_SIGNATURE_METHOD + "\",oauth_timestamp=\""
                        + timestamp + "\",oauth_nonce=\"" + oauth_nonce + "\",oauth_version=\"1.0\",oauth_signature=\""
                        + URLEncoder.encode(oauth_signature, "UTF-8") + "\"";
                //System.out.println("authorization_header_string=" + authorization_header_string);

                HttpClient httpclient = new DefaultHttpClient();
                Map<String, String> responseMap = new HashMap<String, String>();
                try {
                    HttpPost httppost = new HttpPost(URL_TWITTER_OAUTH_TOKEN_REQUEST);
                    httppost.setHeader("Authorization", authorization_header_string);
                    ResponseHandler<String> responseHandler = new BasicResponseHandler();
                    String[] responseBody = httpclient.execute(httppost, responseHandler).split("&");
                    int i = 0;
                    for (String responseParamValue : responseBody) {
                        String[] responseParamValueArr = responseParamValue.split("=");
                        responseMap.put(responseParamValueArr[0], responseParamValueArr[1]);
                        System.out.println("  [" + responseParamValueArr[0] + ":" + responseParamValueArr[1] + "]");

                    }
                   //System.out.println(responseBody);
                    if ("true".equals(responseMap.get("oauth_callback_confirmed"))) {
                        writer.write(callback + "({\"oauth_token\":\"" + responseMap.get("oauth_token") + "\"})");
                        oauthTokens.put(responseMap.get("oauth_token"), new TwitterGoogleClientInfo(clientId));
                    } else {
                        writer.write(callback + RESPONSE_NO_RESULT);
                        //System.out.println("TwitterOauthRequestServlet.doPost() RESPONSE_NO_RESULT");
                        return;
                    }
                } catch (ClientProtocolException cpe) {
                    log.error("Exception caught :: ",cpe);
                } catch (IOException ioe) {
                    log.error("Exception caught :: ",ioe);
                } catch (Exception e) {
                    log.error("Exception caught :: ",e);
                } finally {
                    httpclient.getConnectionManager().shutdown();
                }

                //System.out.println("TwitterOauthRequestServlet.doPost() EXIT");
            }
        } catch (IOException fne) {
            log.error("Exception caught :: ",fne);
        } catch (Exception fne) {
            log.error("Exception caught :: ",fne);
        } catch (Throwable t) {
           log.error("Exception caught :: ",t);
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
        log.info("Servlet ended");
        //System.out.println("TwitterOauthRequestServlet.doPost(), EXIT 2");
    }

    private static String computeSignature(String baseString, String keyString) throws GeneralSecurityException, UnsupportedEncodingException {
        SecretKey secretKey = null;
        byte[] keyBytes = keyString.getBytes();
        secretKey = new SecretKeySpec(keyBytes, "HmacSHA1");
        Mac mac = Mac.getInstance("HmacSHA1");
        mac.init(secretKey);
        byte[] text = baseString.getBytes();
        return new String(Base64.encode(mac.doFinal(text))).trim();
    }
}
