package com.mollatech.dictum.social;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Contacts;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.OOBSocialChannelSettings;
import com.mollatech.dictum.management.ContactManagement;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Set;
import org.hibernate.Session;

public class LinkedinAccessTokenRequestServlet extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(LinkedinAccessTokenRequestServlet.class.getName());
    
    public static final int SOCIAL = 17;
    public static final int PREFERENCE_ONE = 1;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        String _channelName = this.getServletContext().getContextPath();
        _channelName = _channelName.replaceAll("/", "");

        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
        Channels channel = cUtil.getChannel(_channelName);

        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAcess = suRemoteAcess.openSession();
        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);

        String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
        request.getSession().setAttribute("_apSessionRemoteAccessLoginID", credentialInfo[0]);
        SessionManagement sManagement = new SessionManagement();

        String sessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1],request.getSession().getId());
        final PrintWriter writer = response.getWriter();

        int port = request.getLocalPort();
        String server = request.getServerName();
        String channelname = request.getContextPath();
        String scheme = request.getScheme();

//        Map<String, TwitterGoogleClientInfo> oauthTokens = ServletContextUtils.getTwitterGoogleClientMemoryStorage(request.getSession(true));

        Boolean isOk = false;

        String state = null;
        String code = null;
        Set<String> set = request.getParameterMap().keySet();
        SettingsManagement sMngmt = new SettingsManagement();
        OOBSocialChannelSettings socialObj = (OOBSocialChannelSettings) sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.SOCIAL_SETTING, PREFERENCE_ONE);
        String LINKEDIN_API_KEY = socialObj.getLinkedinkey();    //   "75aqxxscbryt71";
        String LINKEDIN_API_SECRET = socialObj.getLinkedinsecret();
        //"H4gP2YCCxPAO1008";

        
        String accessRedirect = scheme.concat("://").concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("/").concat("linkedinAccessTokenRequest");
//      String dasboard=scheme.concat("://").concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("/").concat("socialregister.jsp");
       
        String failboard=scheme.concat("://").concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("/").concat("unsuccessfullregistration.jsp");
        String dasboard=scheme.concat("://").concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("/").concat("successfulRegistration.jsp");
        String URL_DASHBOARD = dasboard;        
        
        
        String LINKEDIN_SERVICE_REDIRECT_URI = accessRedirect; 
        String LINKEDIN_URL_ME = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address)?format=json&oauth2_access_token=%s";

        for (String key : set) {
            //System.out.println("  [" + key + ":" + request.getParameter(key) + "]");
            if ("state".equals(key)) {
                state = request.getParameter(key);
            } else if ("code".equals(key)) {
                code = request.getParameter(key);
            }
        }
        //System.out.println("LinkedinAccessTokenRequestServlet.doPost() [state:" + state + "] [code:" + code + "]");
        try {
            if (state != null && code != null) {
//                Integer clientId = oauthTokens.get(state).getClientId();
                Integer cliendId = 1;
                if (cliendId != null) {
                    HttpClient httpclient = new DefaultHttpClient();
                    Map<String, String> responseMap = new HashMap<String, String>();
                    try {
                        String url = "https://www.linkedin.com/uas/oauth2/accessToken?grant_type=authorization_code"
                                + "&code=" + code
                                + "&redirect_uri=" + LINKEDIN_SERVICE_REDIRECT_URI
                                + "&client_id=" + LINKEDIN_API_KEY
                                + "&client_secret=" + LINKEDIN_API_SECRET;
                        HttpPost httppost = new HttpPost(url);
                        ResponseHandler<String> responseHandler = new BasicResponseHandler();
                        String responseBodyStr = httpclient.execute(httppost, responseHandler);
                        //System.out.println(" responseBodyStr:" + responseBodyStr);
                        String accessToken = (String) ((JSONObject) JSONValue.parse(responseBodyStr)).get("access_token");
                        //System.out.println("url:" + String.format(LINKEDIN_URL_ME, accessToken));
                        HttpGet httpGet = new HttpGet(String.format(LINKEDIN_URL_ME, accessToken));
                        responseBodyStr = httpclient.execute(httpGet, responseHandler);
                        //System.out.println(responseBodyStr);
                        String id = (String) ((JSONObject) JSONValue.parse(responseBodyStr)).get("id");
                        String firstName = (String) ((JSONObject) JSONValue.parse(responseBodyStr)).get("firstName");
                        String lastName = (String) ((JSONObject) JSONValue.parse(responseBodyStr)).get("lastName");
                        String emailaddress = (String) ((JSONObject) JSONValue.parse(responseBodyStr)).get("emailAddress");
                        //System.out.println("[id:" + id + "], [firstName:" + firstName + "], [lastName:" + lastName + "], [lastName:" + emailaddress + "],");
                        //   SocialManagement social = new SocialManagement();


                        ContactManagement contactObj = new ContactManagement();
                        Contacts contact = null;
//                      SocialManagement social = new SocialManagement();
                        contact = contactObj.checkContactbymail(sessionId, channel.getChannelid(), emailaddress);
                        if (contact != null) {
                            
                             if (contact.getFacebookuserid() != null && contact.getTwitteruserid() != null) {
                                contactObj.EditContactforfacebook(sessionId, channel.getChannelid(), emailaddress, responseMap.get("access_token"), id, "facebook,linkedin,twitter", 0, new Date());
                            } else if (contact.getFacebookuserid() != null ) {
                                contactObj.EditContactforlinkedin(sessionId, channel.getChannelid(), emailaddress, responseMap.get("access_token"), id, "facebook,linkedin", 0, new Date());
                            }
                                else  if(contact.getTwitteruserid()!=null)
                               {
                                  contactObj.EditContactforlinkedin(sessionId, channel.getChannelid(), emailaddress, responseMap.get("access_token"), id, "linkedin,twitter", 0, new Date()); 
                            }
                            else
                                {

                           contactObj.EditContactforlinkedin(sessionId, channel.getChannelid(), emailaddress, accessToken, id, "linkedin", 0, new Date());
                                }
          
                            
                        } else {
                            contactObj.addLinkedinContact(sessionId, channel.getChannelid(), emailaddress, firstName, accessToken, id, "linkedin", 0, new Date());
                        }
                     response.sendRedirect(URL_DASHBOARD);
                    } catch (ClientProtocolException cpe) {
                        log.debug("Exception caught :: ",cpe);
                        response.sendRedirect(failboard);
                    } catch (IOException ioe) {
                        log.debug("Exception icaught :: ",ioe);
                        response.sendRedirect(failboard);
                    } catch (Throwable e) {
                        log.debug("Exception caught :: ",e);
                        response.sendRedirect(failboard);
                    } finally {
                        httpclient.getConnectionManager().shutdown();
                    }
                }
            }

            //System.out.println("LinkedinAccessTokenRequestServlet.doPost() EXIT 1");
        } catch (Exception fne) {
            log.debug("Exception caught :: ",fne);
            response.sendRedirect(failboard);
        } catch (Throwable t) {
            log.debug("Exception caught :: ",t);
            response.sendRedirect(failboard);
        }
//        finally {
//            response.sendRedirect(URL_DASHBOARD);
//        }
        //System.out.println("LinkedinAccessTokenRequestServlet.doPost() EXIT 2");
        log.info("Servlet Ended");
    }
}