/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.social;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
//import com.ftgreply.consts.Consts;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Contacts;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.OOBSocialChannelSettings;
import com.mollatech.dictum.management.ContactManagement;
import java.util.Date;
import org.hibernate.Session;

public class FacebookRegisterServlet extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FacebookRegisterServlet.class.getName());
    public static final int SOCIAL = 17;
    public static final int PREFERENCE_ONE = 1;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //System.out.println("FacebookRegisterServlet.doPost()");
        response.setContentType("application/json");
        log.info("Servlet started");
        String _channelName = this.getServletContext().getContextPath();
        _channelName = _channelName.replaceAll("/", "");

        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
        Channels channel = cUtil.getChannel(_channelName);

        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAcess = suRemoteAcess.openSession();
        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);

        String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
        request.getSession().setAttribute("_apSessionRemoteAccessLoginID", credentialInfo[0]);
        SessionManagement sManagement = new SessionManagement();

        String sessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1],request.getSession().getId());
        final PrintWriter writer = response.getWriter();


        Boolean isOk = false;
//        SettingsManagement sMngmt = new SettingsManagement();
        String FACEBOOK_PARAM_CLIENT_ID = "client_id";
        String FACEBOOK_PARAM_CLIENT_SECRET = "client_secret";
        String FACEBOOK_PARAM_GRANT_TYPE = "grant_type";
        String FACEBOOK_PARAM_GRANT_TYPE_VALUE = "fb_exchange_token";
        String FACEBOOK_PARAM_EXCHANGE_TOKEN = "fb_exchange_token";
        String URL_FACEBOOK_LONG_TOKEN_REQUEST = "https://graph.facebook.com/oauth/access_token";
        SettingsManagement sMngmt = new SettingsManagement();
        OOBSocialChannelSettings socialObj = (OOBSocialChannelSettings) sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.SOCIAL_SETTING, PREFERENCE_ONE);

        String FACEBOOK_APPLICATION_ID = socialObj.getFacebookappid();    //"217193035129131";
        String FACEBOOK_APPLICATION_SECRET = socialObj.getFacebookappsecret();           //"87a46a120e082ad085ec4692fba3cce2";

        String callback = URLDecoder.decode(request.getParameter("callback"));
        log.debug("FacebookRegisterServlet::callback::"+callback);
        try {
            Integer clientId = Integer.valueOf(URLDecoder.decode(request.getParameter("clientId")));
            log.debug("clientId::"+clientId);
            String uid = URLDecoder.decode(request.getParameter("uid"));
            log.debug("uid::"+uid);
            String accessToken = URLDecoder.decode(request.getParameter("accessToken"));
            log.debug("accessToken::"+accessToken);
            String name = URLDecoder.decode(request.getParameter("name"));
            log.debug("name::"+name);
            String email = URLDecoder.decode(request.getParameter("email"));
            log.debug("email::"+email);
//            String phone = URLDecoder.decode(request.getParameter("phone"));
            if (clientId != null && uid != null && accessToken != null && name != null) {

                HttpClient httpclient = new DefaultHttpClient();
                Map<String, String> responseMap = new HashMap<String, String>();
                try {
                    List<NameValuePair> nvps = new ArrayList<NameValuePair>();

                    nvps.add(new BasicNameValuePair(FACEBOOK_PARAM_CLIENT_ID, FACEBOOK_APPLICATION_ID));
                    nvps.add(new BasicNameValuePair(FACEBOOK_PARAM_CLIENT_SECRET, FACEBOOK_APPLICATION_SECRET));
                    nvps.add(new BasicNameValuePair(FACEBOOK_PARAM_GRANT_TYPE, FACEBOOK_PARAM_GRANT_TYPE_VALUE));
                    nvps.add(new BasicNameValuePair(FACEBOOK_PARAM_EXCHANGE_TOKEN, accessToken));
                    String paramString = URLEncodedUtils.format(nvps, "utf-8");
                    HttpGet httpGet = new HttpGet(URL_FACEBOOK_LONG_TOKEN_REQUEST + "?" + paramString);
//                    System.out.println("  extend at [" +  "https://graph.facebook.com/oauth/access_token" + "?" + paramString + "]");

                    ResponseHandler<String> responseHandler = new BasicResponseHandler();
                    String responseStr = httpclient.execute(httpGet, responseHandler);
                    String[] responseBody = responseStr.split("&");
                    for (String responseParamValue : responseBody) {
                        String[] responseParamValueArr = responseParamValue.split("=");
                        responseMap.put(responseParamValueArr[0], responseParamValueArr[1]);
                        //System.out.println("  [" + responseParamValueArr[0] + ":" + responseParamValueArr[1] + "]");
                    }


                    //System.out.println(responseStr);
                    if (responseMap.containsKey("access_token")) {
//						SocialDAO.addFacebookAccessToken(clientId, uid, name, responseMap.get("access_token"));
                        isOk = true;
                        ContactManagement contactObj = new ContactManagement();
                        Contacts contact = null;
//                      SocialManagement social = new SocialManagement();
                        contact = contactObj.checkContactbymail(sessionId, channel.getChannelid(), email);
                        if (contact != null) {
                            if (contact.getLinkedinuserid() != null && contact.getTwitteruserid() != null) {
                                contactObj.EditContactforfacebook(sessionId, channel.getChannelid(), email, responseMap.get("access_token"), uid, "facebook,linkedin,twitter", 0, new Date());
                            } else if (contact.getLinkedinuserid() != null ) {
                                contactObj.EditContactforfacebook(sessionId, channel.getChannelid(), email, responseMap.get("access_token"), uid, "facebook,linkedin", 0, new Date());
                            }
                                else  if(contact.getTwitteruserid()!=null)
                               {
                                  contactObj.EditContactforfacebook(sessionId, channel.getChannelid(), email, responseMap.get("access_token"), uid, "facebook,twitter", 0, new Date()); 
                            }
                            else
                                {

                            contactObj.EditContactforfacebook(sessionId, channel.getChannelid(), email, responseMap.get("access_token"), uid, "facebook", 0, new Date());
                                }
                        } else {

                            contactObj.addFacebookContact(sessionId, channel.getChannelid(), email, name, responseMap.get("access_token"), uid, "facebook", 0, new Date());

                        }

//                  social.AddFacebook(sessionId, channel.getChannelid(), "sachin", uid, name, responseMap.get("access_token"), "sachin", "+918983372457", "sachin@mollatech.com");
                        //System.out.println("FacebookRegisterServlet.doPost() EXIT");



                    }



                } catch (ClientProtocolException cpe) {
                    log.error("Exception caught :: ",cpe);
                } catch (IOException ioe) {
                    log.error("Exception caught :: ",ioe);
                } catch (Exception e) {
                    log.error("Exception caught :: ",e);
                } finally {
                    httpclient.getConnectionManager().shutdown();
                }

            }
        } catch (Exception fne) {
            log.error("Exception caught :: ",fne);
        } catch (Throwable t) {
            log.error("Exception is::",t);
        } finally {
            try {
                //writer.write(isOk ? callback + "({\"result\":\"OK\"})" : callback + "({\"result\":\"NO_RESULT\"})");
            } catch (Exception ex) {
                log.error("Exception caught :: ",ex);
            }
            if (writer != null) {
                writer.close();
            }
        }
        log.info("Servlet ended");
        //System.out.println("FacebookRegisterServlet.doPost(), EXIT 2");
    }
}
