package com.mollatech.dictum.social;

import com.mollatech.axiom.face.common.ServletContextUtils;
import com.mollatech.axiom.face.common.ServletContextUtils.TwitterGoogleClientInfo;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.OOBSocialChannelSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.util.Map;
import java.util.UUID;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bouncycastle.util.encoders.Base64;
import org.hibernate.Session;

public class LinkedinOauthRequestServlet extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(LinkedinOauthRequestServlet.class.getName());
    public static final int SOCIAL = 17;
    public static final int PREFERENCE_ONE = 1;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //System.out.println("LinkedinOauthRequestServlet.doPost()");
        log.info("Servlet Started");
        Map<String, TwitterGoogleClientInfo> oauthTokens = ServletContextUtils.getTwitterGoogleClientMemoryStorage(request.getSession(true));
//        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
//        String sessionId = (String) request.getSession().getAttribute("_apSessionID");

        String _channelName = this.getServletContext().getContextPath();
        _channelName = _channelName.replaceAll("/", "");

        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
        Channels channel = cUtil.getChannel(_channelName);

        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAcess = suRemoteAcess.openSession();
        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
        String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
        request.getSession().setAttribute("_apSessionRemoteAccessLoginID", credentialInfo[0]);
        SessionManagement sManagement = new SessionManagement();

        String sessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1],request.getSession().getId());

        SettingsManagement sMngmt = new SettingsManagement();

        int port = request.getLocalPort();
        String server = request.getServerName();
        String channelname = request.getContextPath();
        String scheme = request.getScheme();


//        String accessRedirect = scheme.concat(":").concat(channelname).concat("/").concat("linkedinAccessTokenRequest");
        String accessRedirect = scheme.concat("://").concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("/").concat("linkedinAccessTokenRequest");
        OOBSocialChannelSettings socialObj = (OOBSocialChannelSettings) sMngmt.getSetting(sessionId, channel.getChannelid(), SOCIAL, PREFERENCE_ONE);
        String LINKEDIN_API_KEY = socialObj.getLinkedinkey();  //  "75aqxxscbryt71";
        String LINKEDIN_SERVICE_SCOPE = "r_basicprofile r_emailaddress w_messages rw_nus";

        String LINKEDIN_SERVICE_REDIRECT_URI = accessRedirect;//"http://localhost:8080/face/linkedinAccessTokenRequest";
        response.setContentType("application/json");
        final PrintWriter writer = response.getWriter();

        try {
            if (request.getParameter("clientId") != null) {
                Integer clientId = Integer.valueOf(URLDecoder.decode(request.getParameter("clientId")));
                String callback = URLDecoder.decode(request.getParameter("callback"));

                String stateToken = "linkedin;" + Math.abs(UUID.randomUUID().hashCode());
                String url = "https://www.linkedin.com/uas/oauth2/authorization?response_type=code"
                        + "&client_id=" + LINKEDIN_API_KEY
                        + "&scope=" + URLEncoder.encode(LINKEDIN_SERVICE_SCOPE)
                        + "&state=" + stateToken
                        + "&redirect_uri=" + LINKEDIN_SERVICE_REDIRECT_URI;
                writer.write(callback + "({\"oauth_url\":\"" + url + "\"})");
                oauthTokens.put(stateToken, new TwitterGoogleClientInfo(clientId));

                //System.out.println("LinkedinOauthRequestServlet.doPost() EXIT");
            }
        } catch (Exception fne) {
            log.debug("Exception caught :: ",fne);
        } catch (Throwable t) {
             log.debug("Exception caught :: ",t);
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
        log.info("Servlet Ended");
        //System.out.println("LinkedinOauthRequestServlet.doPost(), EXIT 2");
    }

    private static String computeSignature(String baseString, String keyString) throws GeneralSecurityException, UnsupportedEncodingException {
        SecretKey secretKey = null;
        byte[] keyBytes = keyString.getBytes();
        secretKey = new SecretKeySpec(keyBytes, "HmacSHA1");
        Mac mac = Mac.getInstance("HmacSHA1");
        mac.init(secretKey);
        byte[] text = baseString.getBytes();
        return new String(Base64.encode(mac.doFinal(text))).trim();
    }
    
}
