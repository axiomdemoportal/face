package com.mollatech.dictum.social;

import com.mollatech.axiom.face.common.ServletContextUtils;
import com.mollatech.axiom.face.common.ServletContextUtils.TwitterGoogleClientInfo;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Contacts;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.OOBSocialChannelSettings;
import com.mollatech.dictum.management.ContactManagement;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import org.apache.commons.net.util.Base64;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.bouncycastle.util.encoders.Base64;
import org.hibernate.Session;

public class TwitterAccessTokenRequestServlet extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(TwitterAccessTokenRequestServlet.class.getName());
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //System.out.println("TwitterAccessTokenRequestServlet.doPost()");
        log.info("Servlet started ");
        Map<String, TwitterGoogleClientInfo> oauthTokens = ServletContextUtils.getTwitterGoogleClientMemoryStorage(request.getSession(true));

        //System.out.println("TwitterOauthRequestServlet.doPost()");
        String _channelName = this.getServletContext().getContextPath();
        _channelName = _channelName.replaceAll("/", "");

        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
        Channels channel = cUtil.getChannel(_channelName);

        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAcess = suRemoteAcess.openSession();
        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
        String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
        request.getSession().setAttribute("_apSessionRemoteAccessLoginID", credentialInfo[0]);
        SessionManagement sManagement = new SessionManagement();

        String sessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1],request.getSession().getId());
        log.debug("OpenSession:: "+sessionId);

        Boolean isOk = false;
        SettingsManagement sMngmt = new SettingsManagement();
        OOBSocialChannelSettings socialObj = (OOBSocialChannelSettings) sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.SOCIAL_SETTING, SettingsManagement.PREFERENCE_ONE);
        int port = request.getLocalPort();
        String server = request.getServerName();
        String channelname = request.getContextPath();
        String oauth_token = null;
        String oauth_verifier = null;
        String TWITTER_PARAM_OAUTH_TOKEN = "oauth_token";
        String TWITTER_PARAM_OAUTH_VERIFIER = "oauth_verifier";
        String TWITTER_CONSUMER_KEY = socialObj.getTwitterkey();                     //"wruPh262yqy7mrz2IfPZbQ";
        String TWITTER_OAUTH_SIGNATURE_METHOD = "HMAC-SHA1";
        String TWITTER_CONSUMER_SECRET = socialObj.getTwittersecret(); //   "BjjTMuOhBUuAHl8YpZAOZUxHYbqoFgnIvusfWS8";
        String URL_TWITTER_ACCESS_TOKEN_REQUEST = "https://api.twitter.com/oauth/access_token";
        String TWITTER_PARAM_OAUTH_USER_ID = "user_id";
        String TWITTER_PARAM_OAUTH_SCREEN_NAME = "screen_name";
        String TWITTER_PARAM_OAUTH_TOKEN_SECRET = "oauth_token_secret";
        String scheme = request.getScheme();
//        String dasboard=scheme.concat("://").concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("/").concat("socialregister.jsp");
        String dasboard = "https://axiomprotect.fwd.wf/face/socialregister.jsp";
        String URL_DASHBOARD = dasboard; //"http://".concat(server).concat(":").concat(String.valueOf(port)).concat(channelname);                                      
        Set<String> set = request.getParameterMap().keySet();

//populate set


        for (String key : set) {
            //System.out.println("  [" + key + ":" + request.getParameter(key) + "]");
            if (TWITTER_PARAM_OAUTH_TOKEN.equals(key)) {
                oauth_token = request.getParameter(key);
            } else if (TWITTER_PARAM_OAUTH_VERIFIER.equals(key)) {
                oauth_verifier = request.getParameter(key);
            }
        }
        //System.out.println("TwitterAccessTokenRequestServlet.doPost() [oauth_token:" + oauth_token + "] [oauth_verifier:" + oauth_verifier + "]");
        try {
            if (oauth_token == null || oauth_verifier == null) {
                System.out.println("TwitterAccessTokenRequestServlet.doPost() RESPONSE_NO_RESULT");
                return;
            }
            String oauth_nonce = UUID.randomUUID().toString().replaceAll("-", "");
            String timestamp = Long.valueOf(((new Date().getTime() + new Date().getTimezoneOffset()) / 1000)).toString();
            String parameter_string = "oauth_token=" + oauth_token + "&oauth_consumer_key=" + TWITTER_CONSUMER_KEY + "&oauth_nonce=" + oauth_nonce
                    + "&oauth_signature_method=" + TWITTER_OAUTH_SIGNATURE_METHOD + "&oauth_timestamp=" + timestamp + "&oauth_version=1.0";
            //System.out.println("parameter_string=" + parameter_string);
            String signature_base_string = "POST&https%3A%2F%2Fapi.twitter.com%2Foauth%2Faccess_token&" + URLEncoder.encode(parameter_string, "UTF-8");
            //System.out.println("signature_base_string=" + signature_base_string);
            String oauth_signature = "";
            try {
                oauth_signature = computeSignature(signature_base_string, TWITTER_CONSUMER_SECRET + "&"); // note the & at the end. Normally the user access_token would go here, but we don't know it yet for request_token
                //System.out.println("oauth_signature=" + URLEncoder.encode(oauth_signature, "UTF-8"));
            } catch (GeneralSecurityException e) {
                log.error("Exception caught :: ",e);

            }
            String authorization_header_string = "OAuth oauth_token=\"" + oauth_token + "\",oauth_consumer_key=\"" + TWITTER_CONSUMER_KEY
                    + "\",oauth_signature_method=\"" + TWITTER_OAUTH_SIGNATURE_METHOD + "\",oauth_timestamp=\"" + timestamp + "\",oauth_nonce=\""
                    + oauth_nonce + "\",oauth_version=\"1.0\",oauth_signature=\"" + URLEncoder.encode(oauth_signature, "UTF-8") + "\"";
            //System.out.println("authorization_header_string=" + authorization_header_string);

            HttpClient httpclient = new DefaultHttpClient();
            Map<String, String> responseMap = new HashMap<String, String>();
            try {
                HttpPost httppost = new HttpPost(URL_TWITTER_ACCESS_TOKEN_REQUEST);
                httppost.setHeader("Authorization", authorization_header_string);
                List<NameValuePair> nvps = new ArrayList<NameValuePair>();
                nvps.add(new BasicNameValuePair(TWITTER_PARAM_OAUTH_VERIFIER, oauth_verifier));
                httppost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                String responseBodyStr = httpclient.execute(httppost, responseHandler);
                //System.out.println(" responseBodyStr:" + responseBodyStr);
                String[] responseBody = responseBodyStr.split("&");
                String user_id = null;
                String screen_name = null;
                for (String responseParamValue : responseBody) {
                    String[] responseParamValueArr = responseParamValue.split("=");
                    responseMap.put(responseParamValueArr[0], responseParamValueArr[1]);
                    //System.out.println("  [" + responseParamValueArr[0] + ":" + responseParamValueArr[1] + "]");
                    if (TWITTER_PARAM_OAUTH_USER_ID.equals(responseParamValueArr[0])) {
                        user_id = responseParamValueArr[1];
                    } else if (TWITTER_PARAM_OAUTH_SCREEN_NAME.equals(responseParamValueArr[0])) {
                        screen_name = responseParamValueArr[1];
                    }
                }
                //System.out.println(" user/photo:" + oauthTokens.get(oauth_token));
                if (responseMap.get(TWITTER_PARAM_OAUTH_TOKEN) != null && responseMap.get(TWITTER_PARAM_OAUTH_TOKEN_SECRET) != null
                        && oauthTokens.get(oauth_token) != null && user_id != null && screen_name != null) {
                    Integer clientId = oauthTokens.get(oauth_token).getClientId();
                    String accessToken = responseMap.get(TWITTER_PARAM_OAUTH_TOKEN);
                    String accessTokenSecret = responseMap.get(TWITTER_PARAM_OAUTH_TOKEN_SECRET);

                    isOk = true;

                    ContactManagement contactObj = new ContactManagement();
                    Contacts contact = null;
                    contact = contactObj.checkContactbyscreenname(sessionId, channel.getChannelid(), screen_name);
                    if (contact != null) {
                        
                                              
                             if (contact.getFacebookuserid() != null && contact.getTwitteruserid() != null) {
                               contactObj.EditContactfortwitter(sessionId, channel.getChannelid(), user_id, screen_name, accessToken, accessTokenSecret, "facebook,twitter,linkedin", 0, new Date());
                            } else if (contact.getFacebookuserid() != null ) {
                                contactObj.EditContactfortwitter(sessionId, channel.getChannelid(), user_id, screen_name, accessToken, accessTokenSecret, "facebook,twitter", 0, new Date());
                            }
                                else  if(contact.getLinkedinuserid() !=null)
                               {
                                  contactObj.EditContactfortwitter(sessionId, channel.getChannelid(), user_id, screen_name, accessToken, accessTokenSecret, "twitter,linkedin", 0, new Date());
                            }
                            else
                                {

                           contactObj.EditContactfortwitter(sessionId, channel.getChannelid(), user_id, screen_name, accessToken, accessTokenSecret, "twitter", 0, new Date());
                                }
                        
                        
                        
                        
                        
                    } else {

                        contactObj.addTwitterContact(sessionId, channel.getChannelid(), screen_name, user_id, screen_name, accessToken, accessTokenSecret, "twitter", 0, new Date());
                    }

                } else {
                    System.out.println("TwitterAccessTokenRequestServlet.doPost() RESPONSE_NO_RESULT");
                    return;
                }
            } catch (Throwable e) {
                log.error("Exception caught:: ",e);
                httpclient.getConnectionManager().shutdown();
            }

            //System.out.println("TwitterAccessTokenRequestServlet.doPost() EXIT 1");
        } catch (IOException fne) {
            log.error("Exception caught:: ",fne);
        } catch (Exception fne) {
            log.error("Exception caught:: ",fne);
        } catch (Throwable t) {
            log.error("Exception caught:: ",t);
        } finally {
            response.sendRedirect(URL_DASHBOARD);
        }
        //System.out.println("TwitterAccessTokenRequestServlet.doPost() EXIT 2");
         log.info("Servlet started ");
    }

    private static String computeSignature(String baseString, String keyString) throws GeneralSecurityException, UnsupportedEncodingException {
        SecretKey secretKey = null;
        byte[] keyBytes = keyString.getBytes();
        secretKey = new SecretKeySpec(keyBytes, "HmacSHA1");
        Mac mac = Mac.getInstance("HmacSHA1");
        mac.init(secretKey);
        byte[] text = baseString.getBytes();
        return new String(Base64.encode(mac.doFinal(text))).trim();
    }
}
