package com.mollatech.dictum.contacts;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Contacts;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.dictum.management.ContactManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class addsinglecontact extends HttpServlet {
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(addsinglecontact.class.getName());
final String itemtype = "CONTACT";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        response.setContentType("application/json");
        log.info("Servlet started");
        PrintWriter out = response.getWriter();



        //PrintWriter out = response.getWriter();
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String _channelId = channel.getChannelid();
        Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("sessionId :: "+operator.getName());
         String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
         log.debug("remoteaccesslogin :: "+remoteaccesslogin);
         String operatorId = operator.getOperatorid();
         
        String _emailId = request.getParameter("_emailMC");
        log.debug("_emailId :: "+_emailId);
        String _name = request.getParameter("_nameMC");
        log.debug("_name :: "+_name);
        String _phone = request.getParameter("_phoneMC");
        log.debug("_phone :: "+_phone);
        String[] _tagId = request.getParameterValues("_tagIDMC");
        log.debug("_tagId :: "+_tagId);
        String _fax = request.getParameter("_faxMC");
        log.debug("_fax :: "+_fax);

        if (_emailId == null || _name == null || _phone == null
                || _tagId == null || _emailId.isEmpty() == true
                || _name.isEmpty() == true || _phone.isEmpty() == true
                || _tagId.length == 0) {
            String result = "error";
            //String message = "Success imports: " + iSuccess + " and Failed imports: " + iFailed + ".";
            String message = "Invalid Parameters!!!";
            JSONObject json = new JSONObject();
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;

        }

        StringBuilder tagBuilder = new StringBuilder();
        if (_tagId.length > 0) {
            // StringBuilder tagBuilder = new StringBuilder();

            for (String n : _tagId) {
                //tagBuilder.append("'").append(n.replaceAll("'", "\\\\'")).append("',");
                tagBuilder.append(n.replaceAll("'", "\\\\'")).append(",");
                // can also do the following
                // nameBuilder.append("'").append(n.replaceAll("'", "''")).append("',");
            }
            tagBuilder.deleteCharAt(tagBuilder.length() - 1);
        }

        ContactManagement cmObj = new ContactManagement();
        AuditManagement audit = new AuditManagement();
        try {
            //we will follow google style where duplication is allowed
            
//            Contacts cObj = cmObj.checkContact(sessionId, _channelId, _emailId, _phone);
//            if (cObj != null  ) { // duplicate entries so error
//                String message = "Duplicate Email/Phone!!!";
//                JSONObject json = new JSONObject();
//                json.put("_result", "error");
//                json.put("_message", message);
//                out.print(json);
//                out.flush();
//                return;
//            }
            
            if ( _fax != null && _fax.isEmpty() == true) { 
                _fax = null;
            }
           int result = -1;
            if (UtilityFunctions.isValidEmail(_emailId) == false && 
                    UtilityFunctions.isValidPhoneNumber(_phone) == false) {
                result = -1;
            }
             if (UtilityFunctions.isValidEmail(_emailId) == true && 
                    UtilityFunctions.isValidPhoneNumber(_phone) == true) {
               result = cmObj.addContact(sessionId, _channelId, _emailId, _name, _phone, _fax, tagBuilder.toString() + ",", new Date());
               log.debug("result :: "+result);
             }
           
 
              String resultString = "ERROR";
                     
            if(result == 0){
                      resultString = "SUCCESS";
                            audit.AddAuditTrail(sessionId, _channelId, operator.getOperatorid(), request.getRemoteAddr(),channel.getName(), remoteaccesslogin, 
                                    operator.getName(), new Date(), "Add Contact", resultString, result, 
                                    "Contact Management", 
                                   "",
                                    "Name = "+_name+",Phone = "+_phone+",Email = "+_emailId+",FAX=" +_fax + ",Tags = "+ tagBuilder.toString() + ",",
                                    itemtype, operatorId);
                        
            }else if(result != 0){
                            audit.AddAuditTrail(sessionId, _channelId, operator.getOperatorid(), request.getRemoteAddr(),channel.getName(), remoteaccesslogin, 
                                    operator.getName(), new Date(), "Add Contact", resultString, result, 
                                    "Contact Management","", "Failed To Add Contact...!!",itemtype, operatorId);
                        }
            
            if (result == 0) {
                //String result = "success";
                //String message = "Success imports: " + iSuccess + " and Failed imports: " + iFailed + ".";
                String message = "Successful Added New Contact!!!";
                JSONObject json = new JSONObject();
                try { json.put("_result", "success");
                json.put("_message", message);
                }catch(Exception e){
                    log.error("Exception caught :: ",e);
                            }
                out.print(json);
                out.flush();
            }else if(result == -1){
                String message = "Invalid Phone/EmailID ...!!!";
                JSONObject json = new JSONObject();
                try { json.put("_result", "error");
                json.put("_message", message);
                }catch(Exception e){
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();
            }else{
                 String message = "Failed to add contact...!!!";
                JSONObject json = new JSONObject();
                try { json.put("_result", "error");
                json.put("_message", message);
                }catch(Exception e){
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();
            }
        } finally {
            out.close();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
