package com.mollatech.dictum.contacts;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Contacts;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.dictum.management.ContactManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech1
 */
public class editcontact extends HttpServlet {
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editcontact.class.getName());
final String itemtype = "CONTACT";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        PrintWriter out = response.getWriter();

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String _channelId = channel.getChannelid();
       
        
         Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
         log.debug("operator :: "+operator.getName());
         String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
         log.debug("remoteaccesslogin :: "+remoteaccesslogin);
         String operatorId = operator.getOperatorid();
       
        
        //String _tagID = request.getParameterValues("_tagID");

      
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getName());
        String _contactId = request.getParameter("_contactidEditContact");
        log.debug("_contactId :: "+_contactId);
        int contactId = Integer.parseInt(_contactId);
        String _emailId = request.getParameter("_emailEditContact");
        log.debug("_emailId :: "+_emailId);
        String _name = request.getParameter("_nameEditContact");
        log.debug("_name :: "+_name);
        String _phone = request.getParameter("_phoneEditContact");
        log.debug("_phone :: "+_phone);
        String [] _tagIdArray = request.getParameterValues("_tagIDEditContact");
        log.debug("_tagIdArray :: "+_tagIdArray);
        String _fax = request.getParameter("_faxEditContact");
        log.debug("_fax :: "+_fax);
        
        
        if ( _tagIdArray == null || _tagIdArray.length == 0 ){
            //error
            String message = "No Tag selected, atleast 1 is needed!!!";
            JSONObject json = new JSONObject();
            try { json.put("_result", "error");
            json.put("_message", message);
            }catch(Exception e){log.error("Exception caught :: ",e);}
            out.print(json);
            out.flush();
            return;
        }
        
        if(_phone != null && _phone.isEmpty() == false){
          if (UtilityFunctions.isValidPhoneNumber(_phone) == false) {
            String message = "Phone Number is not valid !!!";
            JSONObject json = new JSONObject();
            try { json.put("_result", "error");
            json.put("_message", message);
            }catch(Exception e){log.error("Exception caught :: ",e);}
            out.print(json);
            out.flush();
            return;
          }
        }
        if(_emailId != null && _emailId.isEmpty() == false){
          if (UtilityFunctions.isValidEmail(_emailId) == false) {
            String message = "Email ID is not valid !!!";
            JSONObject json = new JSONObject();
            try { json.put("_result", "error");
            json.put("_message", message);
            }catch(Exception e){log.error("Exception caught :: ",e);}
            out.print(json);
            out.flush();
            return;
          }
        }
        
        if(_fax != null && _fax.isEmpty() == false){
          if (UtilityFunctions.isValidPhoneNumber(_fax) == false) {
            String message = "FAX Number is not valid !!!";
            JSONObject json = new JSONObject();
            try { json.put("_result", "error");
            json.put("_message", message);
            }catch(Exception e){log.error("Exception caught :: ",e);}
            out.print(json);
            out.flush();
            return;
          }
        }
        
        
        
        String _tagId="";
        for (int j=0;j<_tagIdArray.length;j++){
            _tagId += _tagIdArray[j] + ",";            
        }

        Date _creationTime = new Date();

        ContactManagement contact = new ContactManagement();
        AuditManagement audit = new AuditManagement();
        try {
            //following same strategy as google where duplication is allowed.
            
//            Contacts cObj = contact.checkContact(sessionId, _channelId, _emailId, _phone);
//            if (cObj != null && cObj.getContactid() != contactId) { // duplicate entries so error
//                String message = "Duplicate Email/Phone!!!";
//                JSONObject json = new JSONObject();
//                json.put("_result", "error");
//                json.put("_message", message);
//                out.print(json);
//                out.flush();
//                return;
//            }
                    
             Contacts oldContact = contact.getContact(sessionId, _channelId, contactId);
            int result = contact.EditContact(sessionId, _channelId, contactId, _name, _emailId, _phone, _fax, _tagId, _creationTime);
             Contacts newContact = contact.getContact(sessionId, _channelId, contactId);   
               
              String resultString = "ERROR";
           
//           int cid = oldContact.getContactid();
        //   String _cid = String.valueOf(cid);
            if (result == 0) {
                  resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, _channelId, operatorS.getOperatorid(), request.getRemoteAddr(),channel.getName(), remoteaccesslogin,
                        operatorS.getName(), new Date(), "Edit Contact", resultString, result,
                        "Contact Management","Contact Id="+oldContact.getContactid()+"Name = " + oldContact.getName()
                        + "Phone = " + oldContact.getPhone()+ "Email = " + oldContact.getEmailid() + "Tags = " + oldContact.getTags(),
                        "Contact Id="+newContact.getContactid()+"Name = " + newContact.getName() + "Phone = " + newContact.getPhone() 
                        + "Email = " + newContact.getEmailid() + "Tags = " + newContact.getTags(),itemtype,operatorId );

            } else if (result != 0) {
                audit.AddAuditTrail(sessionId, _channelId, operatorS.getOperatorid(), request.getRemoteAddr(),channel.getName(), remoteaccesslogin,
                        operatorS.getName(), new Date(), "Edit Contact", resultString, result,
                        "Contact Management", "Contact Id="+oldContact.getContactid()+"Name = " + oldContact.getName()
                        + "Phone = " + oldContact.getPhone()+ "Email = " + oldContact.getEmailid() + "Tags = " + oldContact.getTags(),
                        "Failed to edit Contact ...!", itemtype, operatorId);
            }
            
            
            if (result == 0) {
                String message = "Contact Updated Successfully!!!";
                JSONObject json = new JSONObject();
                try { json.put("_result", "success");
                json.put("_message", message);
                }catch(Exception e){log.error("Exception caught :: ",e);}
                out.print(json);
                out.flush();
            } else {                
                String message = "Contact Updated Failed!!!";
                JSONObject json = new JSONObject();
                try { json.put("_result", "error");
                json.put("_message", message);
                }catch(Exception e){log.error("Exception caught :: ",e);}
                out.print(json);
                out.flush();
            }

        } finally {
            out.close();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
