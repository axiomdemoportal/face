/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.dictum.contacts;

import com.mollatech.axiom.common.utils.ContactObj;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.v2.face.handler.operator.getopraudits;
import com.mollatech.dictum.management.ContactManagement;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mollatech1
 */
public class failedtoaddcontact extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(failedtoaddcontact.class.getName());
    private static int PDF_TYPE = 0;
    private static int CSV_TYPE = 1;
    private static final int BUFSIZE = 4096;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        try {
            try {
                Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                log.debug("channel :: "+channel.getName());
                String sessionId = (String) request.getSession().getAttribute("_apSessionID");
                log.debug("sessionId :: "+sessionId);
                String _Format1 = request.getParameter("_Format");
                log.debug("sessionId :: "+_Format1);
                int _Format = Integer.parseInt(_Format1);
                //System.out.println(_Format);
                String _format = request.getParameter("_reporttype");
                log.debug("_format :: "+_format);
                int iFormat = -9999;
                if (_format != null && !_format.isEmpty()) {
                    try { iFormat = Integer.parseInt(_format); }
                    catch(Exception e){
                    log.error("Exception caught :: ",e);
                    }
                }
                
                iFormat = PDF_TYPE; 
//                if (PDF_TYPE == iFormat) {
//                    iFormat = PDF_TYPE;
//                } 
//                else {
//                    iFormat = CSV_TYPE;
//                }

                String filepath = null;
                try {
                    try {
                        ArrayList<ContactObj> conlist = (ArrayList<ContactObj>) request.getSession().getAttribute("_failedContacts");
                        ContactManagement oObj = new ContactManagement();
                         ContactObj[] obj = null;
                        if(conlist != null){
                       obj = conlist.toArray(new ContactObj[conlist.size()]);
                    }

                        filepath = oObj.generateReport(iFormat, sessionId, channel.getChannelid(), obj, _Format);
                        obj = null;
                    } catch (Exception e) {
                        log.error("Exception caught :: ",e);
                    }

                    File file = new File(filepath);
                    int length = 0;
                    ServletOutputStream outStream = response.getOutputStream();
                    ServletContext context = getServletConfig().getServletContext();
                    String mimetype = context.getMimeType(filepath);

                    // sets response content type
                    if (mimetype == null) {
                        mimetype = "application/octet-stream";
                    }
                    response.setContentType(mimetype);
                    response.setContentLength((int) file.length());
                    String fileName = (new File(filepath)).getName();

                    // sets HTTP header
                    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

                    byte[] byteBuffer = new byte[BUFSIZE];
                    DataInputStream in = new DataInputStream(new FileInputStream(file));

                    // reads the file's bytes and writes them to the response stream
                    while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                        outStream.write(byteBuffer, 0, length);
                    }

                    in.close();
                    outStream.close();
                    file.delete();

                } catch (Exception ex) {
                    // TODO handle custom exceptions here
                    log.error("Exception caught :: ",ex);
                }


            } catch (Exception ex) {
                log.error("Exception caught :: ",ex);
            }




        } finally {
            //  out.close();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
