package com.mollatech.dictum.contacts;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Contacts;
import com.mollatech.dictum.management.ContactManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.json.simple.JSONValue;

/**
 *
 * @author mollatech1
 */
public class loadcontacts extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadcontacts.class.getName());
    private List Contacts(String sessionId, String channelId) {
        JSONObject json = new JSONObject();

        ContactManagement contact = new ContactManagement();
        Contacts[] con = contact.getAllContacts(sessionId, channelId);
        List jsonlist = new LinkedList();

        for (int i = 0; i < con.length; i++) {

            Map jsonmap = new HashMap();
            try { 

            jsonmap.put("_name", con[i].getName());
            jsonmap.put("_phone", con[i].getPhone());
            jsonmap.put("_emailId", con[i].getEmailid());
            jsonmap.put("_tags", con[i].getTags());
            jsonmap.put("_createdon", con[i].getCreatedon());
            jsonmap.put("_modifiedon", con[i].getModifiedon());
            }catch(Exception e){log.error("Exception caught :: ",e);}

            jsonlist.add(jsonmap);



        }

         return jsonlist;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        List list = null;
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String _channelId = channel.getChannelid();
        JSONObject json = null;//new JSONObject();
        try {

            list = Contacts(sessionId, _channelId);
            log.debug("Contacts :: "+list);

        } finally {
            String jsonString = JSONValue.toJSONString(list);
       
            out.print(jsonString);
            out.flush();
            out.close();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
