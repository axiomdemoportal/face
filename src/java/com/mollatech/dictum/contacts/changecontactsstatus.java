/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.dictum.contacts;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Contacts;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.dictum.management.ContactManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech1
 */
public class changecontactsstatus extends HttpServlet {
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(changecontactsstatus.class.getName());
final String itemtype = "CONTACT";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
             response.setContentType("application/json");
             log.info("Servlet started");
        PrintWriter out = response.getWriter();

        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
           Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
           log.debug("operator :: "+operator.getName());
         String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
         log.debug("remoteaccesslogin :: "+remoteaccesslogin);
         String operatorId = operator.getOperatorid();
       
        String _channelId = channel.getChannelid();
        String _contactId = request.getParameter("_deleteContactsList");
        log.debug("_contactId :: "+_contactId);
        if (_contactId.startsWith(",") == true) {
            _contactId = _contactId.replaceFirst(",", "");
        }

        String[] strcontId = _contactId.split(",");

        Integer[] contId = new Integer[strcontId.length];
        int i = 0;
        for (String str : strcontId) {
            if (str != null) {
                contId[i] = Integer.parseInt(str);
                i++;
            }
        }

        try {
            ContactManagement contact = new ContactManagement();
            AuditManagement audit = new AuditManagement();
                    
            int result = 0;
            for (int j = 0; j < contId.length; j++) {
                Contacts oldContact = contact.getContact(sessionId, _channelId, contId[j]);
//                result = contact.deleteContacts(sessionId, _channelId, contId[j]);
                 result = contact.ChangeContactStatus(sessionId, _channelId, contId[j]);
                 String strstatus = "Active";
                 if(oldContact.getStatus() == 1){
                     strstatus = "Suspended";
                 }
                 
                 String resultString = "ERROR";
                 
                  if (result == 0) {
                      resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, _channelId, operator.getOperatorid(), request.getRemoteAddr(),channel.getName(), remoteaccesslogin,
                        operator.getName(), new Date(), "Chnage Contact Status", resultString, result,
                        "Contact Management",
                       "Contact Id="+oldContact.getContactid()+"Name = " + oldContact.getName() + "Phone = " + oldContact.getPhone() 
                        + "Email = " + oldContact.getEmailid() + "Tags = " + oldContact.getTags()+"Status ="+strstatus,
                       "Contact Id="+oldContact.getContactid()+"Name = " + oldContact.getName() + "Phone = " + oldContact.getPhone() 
                        + "Email = " + oldContact.getEmailid() + "Tags = " + oldContact.getTags()+"Status ="+strstatus,itemtype,operatorId );

            } else if (result != 0) {
                audit.AddAuditTrail(sessionId, _channelId, operator.getOperatorid(), request.getRemoteAddr(),channel.getName(), remoteaccesslogin,
                        operator.getName(), new Date(), "Delete Contact", resultString, result,
                        "Contact Management", "", "Failed To Edit Status",itemtype, operatorId);
            }
   
                if (result != 0) {
                    break;
                }
            }
            
            if (result == 0) {
                String strresult = "success";
                String message = "Successfully Change Status Of " + contId.length + " contacts!!!";
                JSONObject json = new JSONObject();
                try { json.put("_result", strresult);
                json.put("_message", message);
                }catch(Exception e){
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();
            } else {
                String strresult = "error";
                String message = "Failed to Change sstatus for selected contacts!!!";
                JSONObject json = new JSONObject();
                try { json.put("_result", strresult);
                json.put("_message", message);
                }catch(Exception e){
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();
            }

        } finally {
            out.close();
            log.info("Servlet ended");
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
