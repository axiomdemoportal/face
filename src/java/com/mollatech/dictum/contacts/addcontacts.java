package com.mollatech.dictum.contacts;

import com.mollatech.axiom.common.utils.ContactObj;
import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.dictum.management.ContactManagement;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;

/**
 *
 * @author mollatech1
 */
public class addcontacts extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(addcontacts.class.getName());
    final int Phone = 1;
    final int Email = 2;
    final int NamePhone = 3;
    final int NameEmail = 4;
    final int EmailPhone = 5;
    final int NamePhoneEmail = 6;
    final int FaxNo = 7;
    final int NameFaxNO = 8;
    final int NamePhoneEmailFaxNo = 9;
    static List<String> inputlist;
    final String itemtype = "CONTACT";

    public String getContents(File aFile) {
        //...checks on aFile are elided
        StringBuilder contents = new StringBuilder();

        try {

            BufferedReader input = new BufferedReader(new FileReader(aFile));
            try {
                String line = null; //not declared within while loop

                while ((line = input.readLine()) != null) {
                    contents.append(line);
                    contents.append(System.getProperty("line.separator"));
                }
            } finally {
                input.close();
            }
        } catch (IOException ex) {
            log.debug("Exception caught :: ",ex);
        }

        return contents.toString();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        response.setContentType("application/json");

        PrintWriter out = response.getWriter();

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String _channelId = channel.getChannelid();
        Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operator :: "+operator.getName());
        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        // Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        String operatorId = operator.getOperatorid();

        String[] _tagID = request.getParameterValues("_tagIDCF");
        log.debug("_tagID :: "+_tagID);
        String _Format1 = request.getParameter("_Format");
        log.debug("_Format1 :: "+_Format1);
        int _Format = Integer.parseInt(_Format1);
        String filepath = (String) request.getSession().getAttribute("_filepath");
        log.debug("filepath :: "+filepath);
        String DELIMITER = (String) request.getParameter("_delimiter");
        log.debug("DELIMITER :: "+DELIMITER);
        Date _creationTime = new Date();

        String[] faxNo = null;
        String[] phone = null;
        String[] email = null;
        String[] name = null;
        Integer[] retValue = null;
        ContactManagement contact = new ContactManagement();
        AuditManagement audit = new AuditManagement();
        int iFailed = 0;
        int iSuccess = 0;

        try {
            ContactObj cObj = new ContactObj();
            if (filepath != null) {

                File testFile = new File(filepath);
                String toContact = this.getContents(testFile);
                int result = -1;

                if (_Format == Phone) {

                    inputlist = new ArrayList<String>();
                    for (String s : toContact.split("\n")) {
                        inputlist.add(new String(s));
                    }
                    String str[] = new String[inputlist.size()];

                    phone = new String[inputlist.size()];
                    retValue = new Integer[inputlist.size()];
                    //       ArrayList list = new ArrayList();
                    ArrayList<ContactObj> list = new ArrayList<ContactObj>();

                    for (int i = 0; i < inputlist.size(); i++) {

                        str = inputlist.get(i).split(DELIMITER);
                        phone[i] = str[0];

                        String sIDs = "";
                        for (int j = 0; j < _tagID.length; j++) {
                            sIDs += _tagID[j] + ",";
                        }

                        String phoneno = phone[i].substring(0, phone[i].length() - 1);
                        if (UtilityFunctions.isValidPhoneNumber(phoneno) == false) {
                            result = -1;
                        }
                        if (UtilityFunctions.isValidPhoneNumber(phoneno) != false) {
                            result = contact.addContact(sessionId, _channelId, null, null, phone[i], null, sIDs, _creationTime);
                        }
                        String resultString = "ERROR";

                        if (result == 0) {
                            resultString = "SUCCESS";
                            audit.AddAuditTrail(sessionId, _channelId, operator.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
                                    operator.getName(), new Date(), "Add Contact", resultString, result,
                                    "Contact Management", "", "Phone = " + phone[i] + "Tags = " + sIDs,
                                    itemtype, operatorId);

                        } else if (result != 0) {
                            audit.AddAuditTrail(sessionId, _channelId, operator.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
                                    operator.getName(), new Date(), "Add Contact", resultString, result,
                                    "Contact Management", "", "Failed", itemtype, operatorId);
                        }

                        retValue[i] = result;
                        if (result == 0) {
                            iSuccess++;
                        } else {
                            ContactObj con = new ContactObj();
                            iFailed++;
                            //  list.add(email[i]);
                            con.setPhone(phone[i]);
                            list.add(con);
                        }

                    }
                    HttpSession session = request.getSession(true);
                    session.setAttribute("_failedContacts", list);

                }
                if (_Format == Email) {

                    inputlist = new ArrayList<String>();
                    for (String s : toContact.split("\n")) {
                        inputlist.add(new String(s));
                    }
                    String str[] = new String[inputlist.size()];
                    retValue = new Integer[inputlist.size()];
                    email = new String[inputlist.size()];
                    //     ArrayList list = new ArrayList();
                    ArrayList<ContactObj> list = new ArrayList<ContactObj>();
                    for (int i = 0; i < inputlist.size(); i++) {

                        str = inputlist.get(i).split(DELIMITER);
                        email[i] = str[0];

                        String sIDs = "";

                        for (int j = 0; j < _tagID.length; j++) {
                            sIDs += _tagID[j] + ",";
                        }
//                        boolean bool = UtilityFunctions.isValidEmail(email[i]);
                        String emaiilID = email[i].substring(0, email[i].length() - 1);

                        if (UtilityFunctions.isValidEmail(emaiilID) == false) {
                            result = -1;
                        }

                        if (UtilityFunctions.isValidEmail(emaiilID) != false) {
                            result = contact.addContact(sessionId, _channelId, email[i], null, null, null, sIDs, _creationTime);
                        }
                        String resultString = "ERROR";
                        if (result == 0) {
                            resultString = "SUCCESS";
                            audit.AddAuditTrail(sessionId, _channelId, operator.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
                                    operator.getName(), new Date(), "Add Contact", resultString, result,
                                    "Contact Management", "", "Email = " + email[i] + "Tags = " + sIDs,
                                    itemtype, operatorId);
                        } else if (result != 0) {
                            audit.AddAuditTrail(sessionId, _channelId, operator.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
                                    operator.getName(), new Date(), "Add Contact", resultString, result,
                                    "Contact Management", "", "Failed to Add Contacts ...!!!", itemtype, operatorId);
                        }

                        retValue[i] = result;

                        if (result == 0) {
                            iSuccess++;
                        } else {
                            ContactObj con = new ContactObj();
                            iFailed++;

                            con.setEmail(email[i]);
                            list.add(con);
                        }
                    }
                    HttpSession session = request.getSession(true);
                    session.setAttribute("_failedContacts", list);
                }

                if (_Format == NamePhone) {

                    inputlist = new ArrayList<String>();
                    for (String s : toContact.split("\n")) {
                        inputlist.add(new String(s));

                    }
                    String str[] = new String[inputlist.size()];
                    name = new String[inputlist.size()];
                    phone = new String[inputlist.size()];
                    retValue = new Integer[inputlist.size()];
                    //     ArrayList list = new ArrayList();
                    ArrayList<ContactObj> list = new ArrayList<ContactObj>();
                    for (int i = 0; i < inputlist.size(); i++) {
                        str = inputlist.get(i).split(DELIMITER);
                        name[i] = str[0];
                        phone[i] = str[1];

                        String sIDs = "";
                        for (int j = 0; j < _tagID.length; j++) {
                            sIDs += _tagID[j] + ",";
                        }

                        String phoneno = phone[i].substring(0, phone[i].length() - 1);
                        if (UtilityFunctions.isValidPhoneNumber(phoneno) == false) {
                            result = -1;
                        }

                        if (UtilityFunctions.isValidPhoneNumber(phone[i]) != false) {
                            result = contact.addContact(sessionId, _channelId, null, name[i], phone[i], null, sIDs, _creationTime);
                            log.debug("result :: "+result);
                        }
                        String resultString = "ERROR";
                        if (result == 0) {
                            resultString = "SUCCESS";
                            audit.AddAuditTrail(sessionId, _channelId, operator.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
                                    operator.getName(), new Date(), "Add Contact", resultString, result,
                                    "Contact Management", "",
                                    "name = " + name[i] + "Phone = " + phone[i] + "Tags = " + sIDs,
                                    itemtype, operatorId);
                        } else if (result != 0) {
                            audit.AddAuditTrail(sessionId, _channelId, operator.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
                                    operator.getName(), new Date(), "Add Contact", resultString, result,
                                    "Contact Management", "", "Failed", itemtype, operatorId);
                        }

                        retValue[i] = result;

                        if (result == 0) {
                            iSuccess++;
                        } else {
                            ContactObj con = new ContactObj();
                            iFailed++;
                            con.setPhone(phone[i]);
                            con.setName(name[i]);
                            list.add(cObj);
                        }
                    }
                    HttpSession session = request.getSession(true);
                    session.setAttribute("_failedContacts", list);
                }

                if (_Format == NameEmail) {

                    inputlist = new ArrayList<String>();
                    for (String s : toContact.split("\n")) {
                        inputlist.add(new String(s));

                    }
                    String str[] = new String[inputlist.size()];
                    name = new String[inputlist.size()];
                    email = new String[inputlist.size()];
                    retValue = new Integer[inputlist.size()];
                    //   ArrayList list = new ArrayList();
                    ArrayList<ContactObj> list = new ArrayList<ContactObj>();
                    for (int i = 0; i < inputlist.size(); i++) {
                        str = inputlist.get(i).split(DELIMITER);
                        name[i] = str[0];
                        email[i] = str[1];

                        String sIDs = "";
                        for (int j = 0; j < _tagID.length; j++) {
                            sIDs += _tagID[j] + ",";
                        }
                        String a = email[i];
//                  boolean bool = UtilityFunctions.isValidEmail(a);
                        String emailId = email[i].substring(0, email[i].length() - 1);
                        if (UtilityFunctions.isValidEmail(emailId) == false) {
                            result = -1;
                        }
                        if (UtilityFunctions.isValidEmail(email[i]) == true) {
                            result = contact.addContact(sessionId, _channelId, email[i], name[i], null, null, sIDs, _creationTime);
                        }
                        String resultString = "ERROR";

                        if (result == 0) {
                            resultString = "SUCCESS";
                            audit.AddAuditTrail(sessionId, _channelId, operator.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
                                    operator.getName(), new Date(), "Add Contact", resultString, result,
                                    "Contact Management", "",
                                    "name = " + name[i] + "Email = " + email[i] + "Tags = " + sIDs,
                                    itemtype, operatorId);
                        } else if (result != 0) {
                            audit.AddAuditTrail(sessionId, _channelId, operator.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
                                    operator.getName(), new Date(), "Add Contact", resultString, result,
                                    "Contact Management", "", "Failed To Add Contact ...!!!", itemtype, operatorId);
                        }

                        retValue[i] = result;

                        if (result == 0) {
                            iSuccess++;
                        } else {
                            iFailed++;
                            ContactObj con = new ContactObj();
                            con.setName(name[i]);
                            con.setEmail(email[i]);
                            list.add(con);
//                             list.add(i, cObj);
                        }

                    }
                    HttpSession session = request.getSession(true);
                    session.setAttribute("_failedContacts", list);
                }

                if (_Format == NamePhoneEmail) {

                    inputlist = new ArrayList<String>();
                    for (String s : toContact.split("\n")) {
                        inputlist.add(new String(s));

                    }
                    String str[] = new String[inputlist.size()];
                    name = new String[inputlist.size()];
                    phone = new String[inputlist.size()];
                    email = new String[inputlist.size()];
                    retValue = new Integer[inputlist.size()];
                    //   ArrayList list = new ArrayList();
                    ArrayList<ContactObj> list = new ArrayList<ContactObj>();
                    for (int i = 0; i < inputlist.size(); i++) {
                        str = inputlist.get(i).split(DELIMITER);
                        name[i] = str[0];
                        phone[i] = str[1];
                        email[i] = str[2];

                        String sIDs = "";
                        for (int j = 0; j < _tagID.length; j++) {
                            sIDs += _tagID[j] + ",";
                        }
                        String emailId = email[i].substring(0, email[i].length() - 1);
                        String phoneNo = phone[i].substring(0, phone[i].length() - 1);
                        if (UtilityFunctions.isValidEmail(emailId) == false && UtilityFunctions.isValidPhoneNumber(phoneNo) == false) {
                            result = -1;
                        }
                        if (UtilityFunctions.isValidPhoneNumber(phone[i]) != false && UtilityFunctions.isValidEmail(email[i]) != false) {
                            result = contact.addContact(sessionId, _channelId, email[i], name[i], phone[i], null, sIDs, _creationTime);
                        }
                        String resultString = "ERROR";
                        if (result == 0) {
                            resultString = "SUCCESS";
                        }

                        if (result == 0) {
                            audit.AddAuditTrail(sessionId, _channelId, operator.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
                                    operator.getName(), new Date(), "Add Contact", resultString, result,
                                    "Contact Management", "",
                                    "name = " + name[i] + "phone = " + phone[i] + "Email = " + email[i] + "Tags = " + sIDs,
                                    itemtype, operatorId);
                        } else if (result != 0) {
                            audit.AddAuditTrail(sessionId, _channelId, operator.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
                                    operator.getName(), new Date(), "Add Contact", resultString, result,
                                    "Contact Management", "", "Failed To Add Contacts ...!!", itemtype, operatorId);
                        }

                        retValue[i] = result;

                        if (result == 0) {
                            iSuccess++;
                        } else {
                            ContactObj con = new ContactObj();
                            iFailed++;
                            con.setName(name[i]);
                            con.setPhone(phone[i]);
                            con.setEmail(email[i]);
                            list.add(cObj);
                        }
                    }
                    HttpSession session = request.getSession(true);
                    session.setAttribute("_failedContacts", list);
                }
                //fax

                if (_Format == FaxNo) {

                    inputlist = new ArrayList<String>();
                    for (String s : toContact.split("\n")) {
                        inputlist.add(new String(s));
                    }
                    String str[] = new String[inputlist.size()];
                    retValue = new Integer[inputlist.size()];
                    faxNo = new String[inputlist.size()];
                    //     ArrayList list = new ArrayList();
                    ArrayList<ContactObj> list = new ArrayList<ContactObj>();
                    for (int i = 0; i < inputlist.size(); i++) {

                        str = inputlist.get(i).split(DELIMITER);
                        faxNo[i] = str[0];

                        String sIDs = "";

                        for (int j = 0; j < _tagID.length; j++) {
                            sIDs += _tagID[j] + ",";
                        }

                        String faxNumber = faxNo[i].substring(0, faxNo[i].length() - 1);
                        if (UtilityFunctions.isValidPhoneNumber(faxNumber) == false) {
                            result = -1;
                        }

                        if (UtilityFunctions.isValidPhoneNumber(faxNumber) != false) {
                            result = contact.addContact(sessionId, _channelId, null, null, null, faxNo[i], sIDs, _creationTime);
                            log.debug("result :: "+result);
                        }
                        String resultString = "ERROR";
                        if (result == 0) {
                            resultString = "SUCCESS";
                            audit.AddAuditTrail(sessionId, _channelId, operator.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
                                    operator.getName(), new Date(), "Add Contact", resultString, result,
                                    "Contact Management", "", "Fax No. = " + faxNo[i] + "Tags = " + sIDs,
                                    itemtype, operatorId);
                        } else if (result != 0) {
                            audit.AddAuditTrail(sessionId, _channelId, operator.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
                                    operator.getName(), new Date(), "Add Contact", resultString, result,
                                    "Contact Management", "", "Failed to Add Contacts ...!!!", itemtype, operatorId);
                        }

                        retValue[i] = result;

                        if (result == 0) {
                            iSuccess++;
                        } else {
                            ContactObj con = new ContactObj();
                            iFailed++;
                            con.setFaxNO(faxNo[i]);
                            list.add(con);
                        }
                    }
                    HttpSession session = request.getSession(true);
                    session.setAttribute("_failedContacts", list);
                }

                if (_Format == NameFaxNO) {

                    inputlist = new ArrayList<String>();
                    for (String s : toContact.split("\n")) {
                        inputlist.add(new String(s));

                    }
                    String str[] = new String[inputlist.size()];
                    name = new String[inputlist.size()];
                    faxNo = new String[inputlist.size()];
                    retValue = new Integer[inputlist.size()];
                    //     ArrayList list = new ArrayList();
                    ArrayList<ContactObj> list = new ArrayList<ContactObj>();
                    for (int i = 0; i < inputlist.size(); i++) {
                        str = inputlist.get(i).split(DELIMITER);
                        name[i] = str[0];
                        faxNo[i] = str[1];

                        String sIDs = "";
                        for (int j = 0; j < _tagID.length; j++) {
                            sIDs += _tagID[j] + ",";
                        }

                        String faxNumber = faxNo[i].substring(0, faxNo[i].length() - 1);
                        if (UtilityFunctions.isValidPhoneNumber(faxNumber) == false) {
                            result = -1;
                        }

                        if (UtilityFunctions.isValidPhoneNumber(faxNumber) != false) {
                            result = contact.addContact(sessionId, _channelId, null, name[i], null, faxNo[i], sIDs, _creationTime);
                        }
                        String resultString = "ERROR";
                        if (result == 0) {
                            resultString = "SUCCESS";
                            audit.AddAuditTrail(sessionId, _channelId, operator.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
                                    operator.getName(), new Date(), "Add Contact", resultString, result,
                                    "Contact Management", "",
                                    "name = " + name[i] + "FaxNo = " + faxNo[i] + "Tags = " + sIDs,
                                    itemtype, operatorId);
                        } else if (result != 0) {
                            audit.AddAuditTrail(sessionId, _channelId, operator.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
                                    operator.getName(), new Date(), "Add Contact", resultString, result,
                                    "Contact Management", "", "Failed", itemtype, operatorId);
                        }

                        retValue[i] = result;

                        if (result == 0) {
                            iSuccess++;
                        } else {
                            ContactObj con = new ContactObj();
                            iFailed++;
                            con.setFaxNO(faxNo[i]);
                            con.setName(name[i]);
                            list.add(con);
                        }
                    }
                    HttpSession session = request.getSession(true);
                    session.setAttribute("_failedContacts", list);
                }

                if (_Format == NamePhoneEmailFaxNo) {

                    inputlist = new ArrayList<String>();
                    for (String s : toContact.split("\n")) {
                        inputlist.add(new String(s));

                    }
                    String str[] = new String[inputlist.size()];
                    name = new String[inputlist.size()];
                    phone = new String[inputlist.size()];
                    email = new String[inputlist.size()];
                    faxNo = new String[inputlist.size()];
                    retValue = new Integer[inputlist.size()];
                    //   ArrayList list = new ArrayList();
                    ArrayList<ContactObj> list = new ArrayList<ContactObj>();
                    for (int i = 0; i < inputlist.size(); i++) {
                        str = inputlist.get(i).split(DELIMITER);
                        name[i] = str[0];
                        phone[i] = str[1];
                        email[i] = str[2];
                        faxNo[i] = str[3];

                        String sIDs = "";
                        for (int j = 0; j < _tagID.length; j++) {
                            sIDs += _tagID[j] + ",";
                        }
                        String faxNumber = faxNo[i].substring(0, faxNo[i].length() - 1);
                        if (UtilityFunctions.isValidPhoneNumber(faxNumber) == false) {
                            result = -1;
                        }
                        String phoneNo = phone[i].substring(0, phone[i].length() - 1);
                        if (UtilityFunctions.isValidPhoneNumber(phoneNo) == false) {
                            result = -1;
                        }
                        String emailId = email[i].substring(0, email[i].length() - 1);
                        if (UtilityFunctions.isValidEmail(emailId) == false) {
                            result = -1;
                        }

                        if (UtilityFunctions.isValidPhoneNumber(phoneNo) != false && UtilityFunctions.isValidPhoneNumber(faxNumber) != false && UtilityFunctions.isValidEmail(emailId) != false) {
                            result = contact.addContact(sessionId, _channelId, email[i], name[i], phone[i], faxNo[i], sIDs, _creationTime);
                        }
                        String resultString = "ERROR";
                        if (result == 0) {
                            resultString = "SUCCESS";
                        }

                        if (result == 0) {
                            audit.AddAuditTrail(sessionId, _channelId, operator.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
                                    operator.getName(), new Date(), "Add Contact", resultString, result,
                                    "Contact Management", "",
                                    "name = " + name[i] + "phone = " + phone[i] + "Email = " + email[i] + "Fax No =" + faxNo[i] + "Tags = " + sIDs,
                                    itemtype, operatorId);
                        } else if (result != 0) {
                            audit.AddAuditTrail(sessionId, _channelId, operator.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
                                    operator.getName(), new Date(), "Add Contact", resultString, result,
                                    "Contact Management", "", "Failed To Add Contacts ...!!", itemtype, operatorId);
                        }

                        retValue[i] = result;

                        if (result == 0) {
                            iSuccess++;
                        } else {
                            ContactObj con = new ContactObj();
                            iFailed++;
                            con.setName(name[i]);
                            con.setPhone(phone[i]);
                            con.setEmail(email[i]);
                            con.setFaxNO(faxNo[i]);
                            list.add(con);
                        }
                    }
                    HttpSession session = request.getSession(true);
                    session.setAttribute("_failedContacts", list);
                }

            }
        } catch (Exception ex) {
            log.debug("Exception caught :: ",ex);
        } finally {
            String result = "success";
            String message = "Success imports: " + iSuccess + " and Failed imports: " + iFailed + ".";
            JSONObject json = new JSONObject();
            try { json.put("_result", result);
            json.put("_iformat", _Format);
            json.put("_message", message);
            }catch(Exception e){
                log.debug("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            log.info("Servlet ended");
            return;
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
