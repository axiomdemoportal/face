package com.mollatech.dictum.contacts;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Contacts;
import com.mollatech.dictum.management.ContactManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class loadsinglecontact extends HttpServlet {
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadsinglecontact.class.getName());
    
    private JSONObject SingleContacts(String sessionId, String channelId,int contactid) {
        JSONObject json = new JSONObject();

        ContactManagement contact = new ContactManagement();
        Contacts con = contact.getContact(sessionId, channelId,contactid);
       // List jsonlist = new LinkedList();

     
            //Map jsonmap = new HashMap();

            try { json.put("_name", con.getName());
            json.put("_phone", con.getPhone());
            json.put("_emailId", con.getEmailid());
            //json.put("_tags", con.getTagid());
            json.put("_createdon", con.getCreatedon());
            json.put("_modifiedon", con.getModifiedon());
            }catch(Exception e){log.error("Exception caught :: ",e);}

           return  json;
    }
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      response.setContentType("text/html;charset=UTF-8");
         response.setContentType("application/json");
         log.info("Servlet started");
        PrintWriter out = response.getWriter();
        List list = null;
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String _channelId = channel.getChannelid();
        String contactid = request.getParameter("_contactid");
        log.debug("contactid :: "+contactid);
        
        int _contactId = Integer.parseInt(contactid);
        JSONObject json = null;//new JSONObject();
        try {
                json = SingleContacts(sessionId, _channelId, _contactId);

        }catch(Exception e){log.error("Exception caught :: ",e);} finally {
            
            out.print(json);
            out.flush();
            out.close();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
