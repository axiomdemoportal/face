package com.mollatech.dictum.contacts;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Contacts;
import com.mollatech.dictum.management.ContactManagement;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech1
 */
public class searchcontact extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(searchcontact.class.getName());
    private Contacts[] SearchContactsbyTags(String sessionId, String channelId, String tag) {
        //JSONObject json = new JSONObject();

        ContactManagement contact = new ContactManagement();
        Contacts[] con = contact.SearchContactsByTag(sessionId, channelId, tag);
        //List jsonlist = new LinkedList();
        
        for (int i = 0; i < con.length; i++) {

            //Map jsonmap = new HashMap();
//            jsonmap.put("_no", ""+ (i + 1));
//            jsonmap.put("_name", "\""+con[i].getName()+"\"");
//            jsonmap.put("_phone", "\""+con[i].getPhone()+"\"");
//            jsonmap.put("_email", "\""+con[i].getEmailid()+"\"");
//            jsonmap.put("_tags", "\""+"Insurance payment"+"\"");
//            jsonmap.put("_createdon", "\""+con[i].getCreatedon().toString()+"\"");
//            jsonmap.put("_modifiedon", "\""+con[i].getModifiedon().toString()+"\"");
            
//            jsonmap.put("_no", (i + 1));
//            jsonmap.put("_name", con[i].getName());
//            jsonmap.put("_phone", con[i].getPhone());
//            jsonmap.put("_email", con[i].getEmailid());
//            jsonmap.put("_tags", "Insurance payment");
//            jsonmap.put("_createdon", con[i].getCreatedon().toString());
//            jsonmap.put("_modifiedon", con[i].getModifiedon().toString());
//
//
//            jsonlist.add(jsonmap);

        }

        return con;
    }

    private Contacts[]  SearchContactsbyName(String sessionId, String channelId, String name) {
        //JSONObject json = new JSONObject();

        ContactManagement contact = new ContactManagement();
        Contacts[] conName = contact.SearchContactsByType(sessionId, channelId, name); //name
         //Contacts[] conEmail = contact.SearchContactsByType(sessionId, channelId, name, 2); //email
         //Contacts[] conPhone = contact.SearchContactsByType(sessionId, channelId, name, 3); //phone
//        Contacts [] conTotal = new Contacts[conName.length + conEmail.length + conPhone.length];
//        int j=0;
//        for(int i = 0 ; i < conName.length;i++){
//            conTotal[j]=conName[i];   
//            j++;
//        }
//        for(int i = 0 ; i < conPhone.length;i++){
//            conTotal[j]=conPhone[i];   
//            j++;
//        }
//        for(int i = 0 ; i < conEmail.length;i++){
//            conTotal[j]=conEmail[i];   
//            j++;
//        }
        //return conTotal;
        return null;
        
        
        //List jsonlist = new LinkedList();
//        
//         
//
//        for (int i = 0; i < con.length; i++) {
//
//            Map jsonmap = new HashMap();
//
//            jsonmap.put("_no", ""+ (i + 1));
//            jsonmap.put("_name", "\""+con[i].getName()+"\"");
//            jsonmap.put("_phone", "\""+con[i].getPhone()+"\"");
//            jsonmap.put("_email", "\""+con[i].getEmailid()+"\"");
//            jsonmap.put("_tags", "\""+"Insurance payment"+"\"");
//            jsonmap.put("_createdon", "\""+con[i].getCreatedon().toString()+"\"");
//            jsonmap.put("_modifiedon", "\""+con[i].getModifiedon().toString()+"\"");
//
//
//            jsonlist.add(jsonmap);
//
//        }
//
//        con = contact.SearchContactsByType(sessionId, channelId, name, 2); //email
//        for (int i = 0; i < con.length; i++) {
//
//            Map jsonmap = new HashMap();
//
//           jsonmap.put("_no", ""+ (i + 1));
//              jsonmap.put("_name", "\""+con[i].getName()+"\"");
//            jsonmap.put("_phone", "\""+con[i].getPhone()+"\"");
//            jsonmap.put("_email", "\""+con[i].getEmailid()+"\"");
//            jsonmap.put("_tags", "\""+"Insurance payment"+"\"");
//            jsonmap.put("_createdon", "\""+con[i].getCreatedon().toString()+"\"");
//            jsonmap.put("_modifiedon", "\""+con[i].getModifiedon().toString()+"\"");
//
//            jsonlist.add(jsonmap);
//
//        }
//        con = contact.SearchContactsByType(sessionId, channelId, name, 3); //phone
//        for (int i = 0; i < con.length; i++) {
//
//            Map jsonmap = new HashMap();
//
//           jsonmap.put("_no", ""+ (i + 1));
//              jsonmap.put("_name", "\""+con[i].getName()+"\"");
//            jsonmap.put("_phone", "\""+con[i].getPhone()+"\"");
//            jsonmap.put("_email", "\""+con[i].getEmailid()+"\"");
//            jsonmap.put("_tags", "\""+"Insurance payment"+"\"");
//            jsonmap.put("_createdon", "\""+con[i].getCreatedon().toString()+"\"");
//            jsonmap.put("_modifiedon", "\""+con[i].getModifiedon().toString()+"\"");
//
//            jsonlist.add(jsonmap);
//
//        }
//
//        return jsonlist;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        //response.setContentType("text/html;charset=UTF-8");
        //PrintWriter out = response.getWriter();
        //List list = null;
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String _channelId = channel.getChannelid();
        String _type = request.getParameter("_type");
        log.debug("_type :: "+_type);
        String _keyword = request.getParameter("_keyword");
        log.debug("_keyword :: "+_keyword);
        Contacts [] contacts=null;
        
        JSONObject json = null;//new JSONObject();
        try {

            if (_type.compareTo("2") == 0) {
                contacts = SearchContactsbyTags(sessionId, _channelId, _keyword);
            } else if (_type.compareTo("1") == 0) {
                contacts = SearchContactsbyName(sessionId, _channelId, _keyword);
            }
            log.debug("SearchContactsbyName :: "+contacts);
            request.getSession().setAttribute("_contactsSearched",contacts);
            request.getRequestDispatcher("contactstable.jsp").forward(request, response);
            return;
        } finally {
//            response.setContentType("application/json");        
//            String jsonString = JSONValue.toJSONString(list);
//            out.print(jsonString);        
//            //out.print(json);
//            out.flush();
//            out.close();
        log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
