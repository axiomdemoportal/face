package com.mollatech.initializer;

import com.mollatech.axiom.nucleus.crypto.AES;
import com.mollatech.axiom.nucleus.crypto.PropsFileUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

public class initdb extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(initdb.class.getName());
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        log.info("Servlet started");
        PrintWriter out = response.getWriter();
        try {

            response.setContentType("application/json");

            //get the 4 parameters and encrypt them
            String _db_host = request.getParameter("_ip");
            log.debug("initdb::_db_host::"+_db_host);
            String _db_port = request.getParameter("_p");
            log.debug("initdb::_db_port::"+_db_port);
            String _db_userid = request.getParameter("_u");
            log.debug("initdb::_db_userid::"+_db_userid);
            String _db_password = request.getParameter("_pd");
            log.debug("initdb::_db_password::"+_db_password);
            String _db_passwordC = request.getParameter("_c");
            log.debug("initdb::_db_passwordC::"+_db_passwordC);
            String _dbType = request.getParameter("_dbType");
            log.debug("initdb::_dbType::"+_dbType);
            String _dbName = request.getParameter("_dbName");
            log.debug("initdb::_dbName::"+_dbName);
            if (_db_host == null || _db_port == null || _db_userid == null || _db_password == null
                    || _dbType == null || _dbName == null) {
                String strresult = "error";
                String message = "Invalid Parameters!!!";
                JSONObject json = new JSONObject();
                json.put("_result", strresult);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }

            if (_db_host.isEmpty() == true || _db_port.isEmpty() == true || _db_userid.isEmpty() == true
                    || _db_password.isEmpty() == true
                    || _dbType.isEmpty() == true
                    || _dbName.isEmpty() == true) {
                String strresult = "error";
                String message = "Invalid Parameters!!!";
                JSONObject json = new JSONObject();
                json.put("_result", strresult);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }

            if (_db_passwordC.compareTo(_db_password) != 0) {
                String strresult = "error";
                String message = "Password did not match!!!";
                JSONObject json = new JSONObject();
                json.put("_result", strresult);
                json.put("_message", message);
                out.print(json);
                out.flush();
            }

            //now we encrypt the entries in the dbsetting file
            String sep = System.getProperty("file.separator");
            String usrhome = System.getProperty("catalina.home");
            if (usrhome == null) {
                usrhome = System.getenv("catalina.home");
            }
            if (usrhome == null) {
               usrhome = sep + "var" + sep + "axiomprotect2" + sep + "interface";
            }
            usrhome += sep + "axiomv2-settings";
            //String g_strPath = usrhome + sep;
            String DBSettingfilepath = usrhome + sep + "dbsetting.conf";
            //PropsFileUtil p = new PropsFileUtil();

            //Properties g_sSettings = null;
//            if (p.LoadFile(DBSettingfilepath) == true) {
//                g_sSettings = p.properties;
//                System.out.println("settings file loaded >>" + DBSettingfilepath);
//            } else {
//                System.out.println("settings file failed to load >> " + DBSettingfilepath);
//            }
            PropsFileUtil pfUtilsObj = new PropsFileUtil();
            boolean bResult = false;
            bResult = pfUtilsObj.LoadFile(DBSettingfilepath);
            log.debug("initdb::LoadFile::"+bResult);
            AES aesObj = new AES();
            pfUtilsObj.properties.setProperty("db.password", aesObj.PINEncrypt(_db_password, AES.getSignature()));
            pfUtilsObj.properties.setProperty("db.username", aesObj.PINEncrypt(_db_userid, AES.getSignature()));
            pfUtilsObj.properties.setProperty("db.database", aesObj.PINEncrypt(_dbName, AES.getSignature()));
            pfUtilsObj.properties.setProperty("db.port", aesObj.PINEncrypt(_db_port, AES.getSignature()));
            pfUtilsObj.properties.setProperty("db.host", aesObj.PINEncrypt(_db_host, AES.getSignature()));
            pfUtilsObj.properties.setProperty("db.type", aesObj.PINEncrypt(_dbType, AES.getSignature()));

            HashMap map = new HashMap();

            Enumeration enamObj = pfUtilsObj.properties.propertyNames();
            while (enamObj.hasMoreElements()) {
                String key = (String) enamObj.nextElement();
                String value = (String) pfUtilsObj.properties.getProperty(key);
                map.put(key, value);
            }

            bResult = pfUtilsObj.ReplaceProperties(map, DBSettingfilepath);
            if (bResult == true) {
                String strresult = "success";
                String message = "DB Setting initialized successfully. Restart Interface Now!!!";
                JSONObject json = new JSONObject();
                json.put("_result", strresult);
                json.put("_message", message);
                out.print(json);
                out.flush();
                //out.println("<h1> DB Setting initialized successfully...</h1><br>");
                return;
            } else {
                //out.println("<h1> DB Setting initialized failed...</h1><br>");
                String strresult = "error";
                String message = "DB Setting initialized failed!!!";
                JSONObject json = new JSONObject();
                json.put("_result", strresult);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }

        } catch (Exception e) {
            log.error("Exception caught :: ",e);
            //out.println("<h1> DB Setting initialized failed"+ e.getMessage()+"</h1><br>");
            String strresult = "error";
            String message = "Exception::" + e.getMessage();
            JSONObject json = new JSONObject();
            json.put("_result", strresult);
            json.put("_message", message);
            out.print(json);
            out.flush();
            return;
        } finally {
            out.close();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
