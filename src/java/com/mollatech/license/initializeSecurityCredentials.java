package com.mollatech.license;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.RemoteAccessManagement;
import com.mollatech.axiom.nucleus.db.operation.AxiomChannel;
import com.mollatech.axiom.nucleus.db.operation.AxiomOperator;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;

public class initializeSecurityCredentials extends HttpServlet {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(initializeSecurityCredentials.class.getName());
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        response.setContentType("json/application");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
 
        String type = request.getParameter("type");
        log.debug("type :: " +type);
        if (type == null || type.isEmpty() == true) {
            //error bad data
            try { json.put("_result", "error");
            json.put("_message", "Invalid Type");
            }catch(Exception e){
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        
        HttpSession session = request.getSession(true);

        if (type.compareTo("1") == 0) {
            String password1 = request.getParameter("officer1password");
            log.debug("password1::" +password1);
            String password2 = request.getParameter("officer1passwordC");
            log.debug("password2::" +password2);
            if (password1 == null || password1.isEmpty() == true
                    || password2 == null || password2.isEmpty() == true) {
                
                try { json.put("_result", "error");
                json.put("_message", "Password did not match or is empty.");
                }catch(Exception e){
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();
                return;
            }
            
            if (password1.length() < 10) {
                try { json.put("_result", "error");
                json.put("_message", "Password length is less than 10 characters.");
                }catch(Exception e){
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();
                return;
            }

            if (password1.compareTo(password2) == 0) {
                session.setAttribute("_officer1password", password1);
                try { json.put("_result", "success");
                json.put("_message", "password saved.");
                }catch(Exception e){
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();
                return;
            }
            
            
            
            
            
        } else if (type.compareTo("2") == 0) {
            String password1 = request.getParameter("officer2password");
            log.debug("password1::"+password1);
            String password2 = request.getParameter("officer2passwordC");
            log.debug("password2::"+password2);
            if (password1 == null || password1.isEmpty() == true
                    || password2 == null || password2.isEmpty() == true) {
                //error
                try { json.put("_result", "error");
                json.put("_message", "Password did not match or is empty.");
                }catch(Exception e){
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();
                return;
            }
            
            if (password1.length() < 10) {
                try { json.put("_result", "error");
                json.put("_message", "Password length is less than 10 characters.");
                }catch(Exception e){
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();
                return;
            }

            if (password1.compareTo(password2) == 0) {
                session.setAttribute("_officer2password", password1);
                try { json.put("_result", "success");
                json.put("_message", "password saved.");
                }catch(Exception e){
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();
                return;
            }
        } else if (type.compareTo("3") == 0) {
            String password1 = request.getParameter("officer3password");
            log.debug("password1::"+password1);
            String password2 = request.getParameter("officer3passwordC");
            log.debug("password2::"+password2);
            if (password1 == null || password1.isEmpty() == true
                    || password2 == null || password2.isEmpty() == true) {
                //error
                try { json.put("_result", "error");
                json.put("_message", "Password did not match or is empty.");
                }catch(Exception e){
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();
                return;
            }
            
            if (password1.length() < 10) {
                try { json.put("_result", "error");
                json.put("_message", "Password length is less than 10 characters.");
                }catch(Exception e){
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();
                return;
            }

            if (password1.compareTo(password2) == 0) {
                
                session.setAttribute("_officer3password", password1);
                try { json.put("_result", "success");
                json.put("_message", "password saved.");
                }catch(Exception e){
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();
                return;
            }
        } else if (type.compareTo("0") == 0) {
            String [] passwords = new String[3];
            passwords[0] = (String) session.getAttribute("_officer1password");
            log.debug("passwords[0]::"+passwords[0]);
            passwords[1] = (String) session.getAttribute("_officer2password");
            log.debug("passwords[1]::"+passwords[1]);
            passwords[2] = (String) session.getAttribute("_officer3password");
            log.debug("passwords[2]::"+passwords[2]);
            if ( AxiomProtect.IsSecurityInitialized() == true){
                try { json.put("_result", "error");
                json.put("_message", "System is already initialized. Only system administrator ");
                }catch(Exception e){
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();    
                return; 
            }
            
            int iResult = AxiomProtect.SetPasswords(passwords);
            if ( iResult == 0 ) {                
                OperatorsManagement oprmngObj = new OperatorsManagement();
                ChannelManagement chnmngObj = new ChannelManagement();
                AxiomChannel[] channels = chnmngObj.ListChannelsInternal();
                AxiomOperator [] operators = oprmngObj.ListOperatorsInternal(channels[0].getStrChannelid());
                RemoteAccessManagement ramngObj = new RemoteAccessManagement();
                Date d = new Date();
                long raid = d.getTime()/912;
                long rapass = d.getTime()/3948;
                long oprpass = d.getTime()/352;
                
                ramngObj.SetRemoteAccessCredentialsInternal(channels[0].getStrChannelid(), ""+raid, ""+rapass);
                oprmngObj.SetPassword(channels[0].getStrChannelid(), operators[0].getStrOperatorid(), ""+oprpass);
                try { json.put("_result", "success");
                json.put("_message", "Initialization Successful. Please note System Administrator Password as " + ""+oprpass+ ".");
                }catch(Exception e){
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush(); 
                return;
            }  else {
                try { json.put("_result", "error");
                json.put("_message", "Security Credential Initialization Failed. Error Code: " + iResult);
                }catch(Exception e){
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();    
                log.info("Servlet ended");
                return;                
            }
        }
        //out.flush();
        //return;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
