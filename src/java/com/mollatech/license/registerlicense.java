/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.license;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class registerlicense extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(registerlicense.class.getName());
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        log.info("Servlet started");
        //response.setContentType("json/application");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
       // log.debug("registerlicense::channel is::"+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
       // log.debug("registerlicense::sessionId::"+sessionId);
        PrintWriter out = response.getWriter();
        String strError = "";
        String saveFile = "";
        String savepath = "";
        int failed = 0;
        int success = 0;
        String result = "success";
        String message = "File Upload sucessfully";
        //  String counter  = "0";

        JSONObject json = new JSONObject();

        savepath = LoadSettings.g_strPath;

        savepath += System.getProperty("file.separator");


//        String strUniqueID = new String(request.getSession().getId());
//        strUniqueID = strUniqueID.substring(0, 8);

        String optionalFileName = "";
        FileItem fileItem = null;
        String[] files = new String[1];	 // file names
        String dirName = savepath;
        int retValue = 0;

        int i = 0;



        if (ServletFileUpload.isMultipartContent(request)) {
            try {
                ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                List fileItemsList = servletFileUpload.parseRequest(request);
                Iterator it = fileItemsList.iterator();

                while (it.hasNext()) {
                    FileItem fileItemTemp = (FileItem) it.next();
                    if (fileItemTemp.isFormField()) {
                        if (fileItemTemp.getFieldName().equals("filename")) {
                            optionalFileName = fileItemTemp.getString();
                        } else {
                            //System.out.println("Additional fields in file upload form is  " + fileItemTemp.getFieldName());
                        }
                    } else {
                        fileItem = fileItemTemp;
                    }
                    if (fileItem != null) {
                        String fileName = fileItem.getName();
                        if(fileItem.getSize() == 0){
                            strError = "Please Select File To Upload...!!!";
                            result = "error";
                            try { json.put("result", result);
                            json.put("message", strError);
                            }catch(Exception e){
                                log.error("Exception caught :: ",e);
                            }
                            //out.print(json);
                            out.print("{result:'"+result+"',message:'"+strError+"'}");
                            out.flush();
                            return;
                        }
                        if (fileItem.getSize() > 0 && fileItem.getSize() < 1024*10) { // size cannot be more than 65Kb. We want it light.
                            if (optionalFileName.trim().equals("")) {
                                fileName = FilenameUtils.getName(fileName);
                            } else {
                                fileName = optionalFileName;
                            }
                            files[i++] = dirName + fileName;
                            File saveTo = new File(dirName + fileName);

                            saveFile = fileName;

                            AXIOMStatus axiom[] = null;
                            try {
                                fileItem.write(saveTo);

                                message = "File Upload sucessfully";

                                int iResult = AxiomProtect.CheckLicense(saveTo.getAbsolutePath());
                                log.debug("registerlicense::CheckLicense::"+iResult);
                                if (iResult == 0) {
                                    //dump the file into proper place and rename it license.enc  
                                    String filepath = LoadSettings.g_strPath;
                                    String filename = "license.enc";
                                    filepath = filepath + filename;
                                    File licFile = new File(filepath);
                                    boolean bResult = saveTo.renameTo(licFile);
                                    if (bResult == false) {
                                        strError = "Error: License File import failed. Error Code:-909";
                                        result = "error";
                                        json.put("result", result);
                                        json.put("message", strError);
                                        out.print("{result:'"+result+"',message:'"+strError+"'}");
                                        //out.print(json);
                                        out.flush();
                                        return;
                                    } else if (bResult == true) {
                                        strError = "License File Registered Successfully";
                                        result = "success";
                                        json.put("result", result);
                                        json.put("message", strError);
                                        //out.print(json);
                                        out.print("{result:'"+result+"',message:'"+strError+"'}");
                                        out.flush();
                                        return;
                                    }
                                } else {
                                    strError = "Error: License File is not valid. Error Code:" + iResult;
                                    result = "error";
                                    json.put("result", result);
                                    json.put("message", strError);
                                    //out.print(json);
                                    out.print("{result:'"+result+"',message:'"+strError+"'}");
                                    out.flush();
                                    return;
                                }


                                //request.setAttribute("_filepath", saveTo.getAbsolutePath());
                            } catch (Exception e) {
                                log.error("Exception caught :: ",e);
                            }
                        } else {
                            strError = "Error: " + fileName + " size is more than 10KB. Please upload correct file.";
                            result = "error";
                            try { json.put("result", result);
                            json.put("message", strError);
                            }catch(Exception e){
                                log.error("Exception caught :: ",e);
                            }
                            //out.print(json);
                            out.print("{result:'"+result+"',message:'"+strError+"'}");
                            out.flush();
                            return;
                        }
                    } else {
                        result = "error";
                        message = "Error: No file present...";
                        try { json.put("result", result);
                        json.put("message", message);
                        }catch(Exception e){
                            log.error("Exception caught :: ",e);
                        }
                        //out.print(json);
                        out.print("{result:'"+result+"',message:'"+message+"'}");
                        out.flush();
                        return;
                    }
                }
            } catch (FileUploadException ex) {
                Logger.getLogger(registerlicense.class.getName()).log(Level.SEVERE, null, ex);
                log.error("registerlicense::Exception is::",ex);
            }
        } else {
            result = "error";
            message = "Error: Form Post is invalid!!!";
            try { json.put("result", result);
            json.put("message", message);
            }catch(Exception e){
                log.error("Exception caught :: ",e);
            }
            //out.print(json);
             out.print("{result:'"+result+"',message:'"+message+"'}");
            out.flush();
            log.info("Servlet ended");
            return;
        }
//        try {
//            json.put("result", result);
//                        json.put("message", message);
//           
//
//        } finally {
//            out.print(json);
//            out.flush();
//        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
