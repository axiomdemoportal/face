/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.license;

import com.mollatech.axiom.common.utils.MasterConfiguration;
import com.mollatech.axiom.common.utils.MasterStatus;
import com.mollatech.axiom.nucleus.crypto.AES;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class savedbslave extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(savedbslave.class.getName());
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        response.setContentType("json/application");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        AES aesObj = new AES();
        String databaseName = LoadSettings.g_sSettings.getProperty("db.database");
        databaseName = aesObj.PINDecrypt(databaseName,AES.getSignature());
        String userid = LoadSettings.g_sSettings.getProperty("db.username");
        userid = aesObj.PINDecrypt(userid,AES.getSignature());
        String password = LoadSettings.g_sSettings.getProperty("db.password");
        password = aesObj.PINDecrypt(password,AES.getSignature());
        String host = LoadSettings.g_sSettings.getProperty("db.host");
        host = aesObj.PINDecrypt(host,AES.getSignature());
        String port = LoadSettings.g_sSettings.getProperty("db.port");
        port = aesObj.PINDecrypt(port,AES.getSignature());

        String strSlaveIP = request.getParameter("_slaveip");
        log.debug("savedbslave::strSlaveIP::"+strSlaveIP);
        String filepath = null;
        try {
            MasterConfiguration mConfiguration = new MasterConfiguration(host,port,databaseName, userid, password);

            MasterStatus m = mConfiguration.ConfigMaster(strSlaveIP);
            if (m == null) {
                json.put("_fileid", "");
                json.put("_index", "");
                json.put("_dnid", "");
                json.put("_result", "error");
                json.put("_message", "Configuration Master Instance Failed. ERROR CODE: -1!!!");
                out.print(json);
                out.flush();
                return;
            }

            Date d = new Date();
            String sep = System.getProperty("file.separator");
            filepath = LoadSettings.g_strPath + sep + "temp"+ sep + "" + d.getTime();
            filepath = mConfiguration.GetMasterSQLDump(host,port,databaseName, userid,password, filepath);
            
            if (filepath == null) {
                json.put("_fileid", "");
                json.put("_index", "");
                json.put("_dnid", "");
                json.put("_result", "error");
                json.put("_message", "Configuration Master Instance Failed. ERROR CODE: -2!!!");
                out.print(json);
                out.flush();
                return;
            }

            HttpSession session = request.getSession(true);
            session.setAttribute("" + d.getTime(), filepath);

            try { json.put("_fileid", m.file);
            json.put("_index", m.position);
            json.put("_dnid", "" + d.getTime());
            json.put("_result", "success");
            json.put("_message", "Successfully Configured Master Instance");
            }catch(Exception e){
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;

        } catch (Exception ex) {
            //Logger.getLogger(downloaddbdump.class.getName()).log(Level.SEVERE, null, ex);
            log.error("Exception caught :: ",ex);
            try { json.put("_fileid", "");
            json.put("_index", "");
            json.put("_dnid", "");
            json.put("_result", "error");
            json.put("_message", ex.getMessage());
            }catch(Exception e){
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            log.info("Servlet ended");
            return;
        }



    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
