/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.push;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Registerdevicepush;
import com.mollatech.axiom.nucleus.db.connector.management.PushNotificationDeviceManagement;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class savePushWatch extends HttpServlet {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(savePushWatch.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String UserGroup = request.getParameter("_groupId");
        String PushType = request.getParameter("_type");

        int groupID = 0;
        if (UserGroup != null) {
            groupID = Integer.parseInt(UserGroup);
        }

        log.info("savePushWatch started");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        String dirName = (String) request.getSession().getAttribute("dirName");
        String fileName = (String) request.getSession().getAttribute("fileName");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        log.debug("channel :: " + channel.getName());
        log.debug("sessionId :: " + sessionId);
        log.debug("UserGroup :: " + UserGroup);
        log.debug("PushType :: " + PushType);
        String[] files = new String[1];
        String result = "success";
        String message;
        response.setContentType("text/html;charset=UTF-8");
        Properties prop = new Properties();
        InputStream input = null;
        String DeviceToken = null;
        String Userid = null;
        Map deviceregisterIds = new HashMap();
        try {
            BufferedReader br = new BufferedReader(new FileReader(dirName + fileName));
            if (br == null) {
                result = "error";
                message = "File can not be empty!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                    out.print(json);
                    out.flush();
                } catch (Exception ex) {
                }
            } else {
                String line;
                String[] values;
                String devTokenVal = "";
                while ((line = br.readLine()) != null) {
                    if (!line.equals("")) {
                        String value = line.substring(line.indexOf('=') + 1);
                        deviceregisterIds.put(line.split("=")[0], value);
                    }

                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        int retValue = 0;
        int count = 0;
        int Type = Integer.parseInt(PushType);
        PushNotificationDeviceManagement pushNotificationDeviceManagement = new PushNotificationDeviceManagement();
        Iterator it = deviceregisterIds.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            Userid = (String) pair.getKey();
            Registerdevicepush regDevice = pushNotificationDeviceManagement.GetRegisterPushDeviceByUserID(sessionId, channel.getChannelid(), Userid);

            if (regDevice != null) {
                if (regDevice.getUserid().equals((String) pair.getKey())) {
                    regDevice.setGoogleregisterid((String) pair.getValue());
                    retValue = pushNotificationDeviceManagement.ChangeRegisterPushDevice(sessionId, channel.getChannelid(), Userid, regDevice);
                } else {
                    retValue = pushNotificationDeviceManagement.AddRegisterPushDevicev2(sessionId, channel.getChannelid(), 1, "NA", (String) pair.getValue(), Userid, Type, new Date(), new Date(), groupID);

                }
            } else {

                retValue = pushNotificationDeviceManagement.AddRegisterPushDevicev2(sessionId, channel.getChannelid(), 1, "NA", (String) pair.getValue(), Userid, Type, new Date(), new Date(), groupID);

            }
            if (retValue == 0) {
                count = count + 1;
            }
        }

//        PushWatchManagement pMgnt = new PushWatchManagement();
//      
//        retValue = pMgnt.addPushWatch(sessionId, DeviceToken, Type, Userid, UserGroup);
        log.debug("savePushWatch :: " + retValue);
        String resultString = "Failure";
        if (count > 0) {
            resultString = "Success";
            message = count + " Registration id's imported Successfully!!";
        } else {
            result = "error";
            message = "PushWatch Details added failed!!";

        }

        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (JSONException e) {
            log.error("Exception caught :: ", e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");

        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
