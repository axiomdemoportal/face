/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.push;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Registerdevicepush;
import com.mollatech.axiom.nucleus.db.Usergroups;
import com.mollatech.axiom.nucleus.db.connector.management.PushNotificationDeviceManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserGroupsManagement;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import static com.mollatech.axiom.push.savePushWatch.log;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author pramodchaudhari
 */
public class getDeviceCounts extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            JSONObject json = new JSONObject();
            PushNotificationDeviceManagement pushNotificationDeviceManagement = new PushNotificationDeviceManagement();
            log.info("Getting Push Devices");
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            String dirName = (String) request.getSession().getAttribute("dirName");
            String fileName = (String) request.getSession().getAttribute("fileName");
            Registerdevicepush[] regAndroid = null;
            Registerdevicepush[] regAndIOS = null;
            Registerdevicepush[] regPush = null;
            try {
                regAndroid = pushNotificationDeviceManagement.GetRegisterPushDeviceByType(channel.getChannelid(), SendNotification.ANDROIDPUSH);
                regAndIOS = pushNotificationDeviceManagement.GetRegisterPushDeviceByType(channel.getChannelid(), SendNotification.IPHONEPUSH);
                regPush = pushNotificationDeviceManagement.GetRegisterPushDeviceByType(channel.getChannelid(), SendNotification.WEBPUSH);
                UserGroupsManagement userGroups = new UserGroupsManagement();
                Usergroups[] userGroup = userGroups.getAllGroup(sessionId, channel.getChannelid());
                List listgroupDeviceCount = new ArrayList();
                for (int i = 0; i < userGroup.length; i++) {
                    listgroupDeviceCount.add("");
                    regPush = pushNotificationDeviceManagement.getRegisterPushDeviceByGroup(channel.getChannelid(), userGroup[i].getGroupid());
                    if (regPush == null) {
                        listgroupDeviceCount.add(userGroup[i].getGroupname() + "," + 0);
                    } else {
                        listgroupDeviceCount.add(userGroup[i].getGroupname() + "," + regPush.length);
                    }
                }
                if(regAndroid!=null){
                json.put("_androidDeviceCount", regAndroid.length);
                }else
                {
                 json.put("_androidDeviceCount",0);
                }
                if(regAndIOS!=null){
                json.put("_iosdevicecount", regAndIOS.length);
                }else
                {
                 json.put("_iosdevicecount", 0);
                }
                if(regPush!=null){
                json.put("webpushDeviceCount", regPush.length);
                }else
                {
                 json.put("webpushDeviceCount", 0);
                }
                json.put("_groupwiseCount", new JSONArray(listgroupDeviceCount));
                json.put("_result", "success");
                json.put("_message", "sucess");
                out.print(json);
                return;
            } catch (Exception ex) {
                try {
                    json.put("_result", "error");
                    json.put("_message", "failed");
                    out.print(json);
                    return;
                } catch (Exception ex1) {
                    ex1.printStackTrace();
                }
                ex.printStackTrace();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
