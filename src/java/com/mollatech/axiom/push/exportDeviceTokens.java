/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.push;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Registerdevicepush;
import com.mollatech.axiom.nucleus.db.connector.management.PushNotificationDeviceManagement;
import static com.mollatech.axiom.push.savePushWatch.log;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

/**
 *
 * @author pramodchaudhari
 */
public class exportDeviceTokens extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        String UserGroup = request.getParameter("_groupId");
        String PushType = request.getParameter("_type");

        int type = 0;
        int groupId = 0;
        if (PushType != null) {
            type = Integer.parseInt(PushType);
        }
        if (UserGroup != null) {
            groupId = Integer.parseInt(UserGroup);
        }

        log.info("savePushWatch started");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        String dirName = (String) request.getSession().getAttribute("dirName");
        String fileName = (String) request.getSession().getAttribute("fileName");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        log.debug("channel :: " + channel.getName());
        log.debug("sessionId :: " + sessionId);
        log.debug("UserGroup :: " + UserGroup);
        log.debug("PushType :: " + PushType);
        String[] files = new String[1];
        String result = "success";
        String message;
        response.setContentType("text/html;charset=UTF-8");
        Properties prop = new Properties();
        InputStream input = null;
        String DeviceToken = null;
        String Userid = null;
        try {
            PushNotificationDeviceManagement pushWatchManagement = new PushNotificationDeviceManagement();

            Registerdevicepush[] pushDevices = pushWatchManagement.GetRegisterPushDeviceByTypeandgroupId(sessionId, channel.getChannelid(), type, groupId);
            String[] tokensArray;
            DeviceTokenDetais[] deviceTokensArray;
            if (pushDevices != null) {
               // tokensArray = new String[pushDevices.length];
                deviceTokensArray= new DeviceTokenDetais[pushDevices.length];
                for (int i = 0; i < pushDevices.length; i++) {
                    deviceTokensArray[i]= new DeviceTokenDetais();
                    deviceTokensArray[i].setUserid(pushDevices[i].getUserid());
                    deviceTokensArray[i].setDeviceToken(pushDevices[i].getGoogleregisterid());
                }
                 ObjectMapper mapper = new ObjectMapper();
                 String jsonDeviceTsokens = mapper.writeValueAsString(deviceTokensArray);
                json.put("_result", "success");
                json.put("_message", jsonDeviceTsokens);
                out.print(json);
                return;

            } else {
                json.put("_result", "error");
                json.put("_message", "Failed to export DeviceToken");
                out.print(json);
                return;

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
    
    
   private  class DeviceTokenDetais
           {
           
         private String userid;

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public String getUserid() {
            return userid;
        }

        public String getDeviceToken() {
            return deviceToken;
        }
        private String deviceToken;
       
       
           
           }
    
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
