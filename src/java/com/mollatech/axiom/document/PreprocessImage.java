package com.mollatech.axiom.document;

import com.mollatech.axiom.ocr.RestCallingWrapper;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Shailendra
 */
public class PreprocessImage {
    public String callPreprocessingService(String encodedImage,String templateName, String extension){
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("methodname", "preprocessimage");
            jsonObj.put("rawimage", encodedImage);
            jsonObj.put("docid", "newimage");
            jsonObj.put("docextension", extension);
        } catch (Exception ex) {
            Logger.getLogger(PreprocessImage.class.getName()).log(Level.SEVERE, null, ex);
        }
            String url = "http://128.199.222.136:5000/todo/api/v1.0/tasks";
//String url = "http://192.168.0.119:5000/todo/api/v1.0/tasks";
            RestCallingWrapper callService = new RestCallingWrapper();
            String result = null;
            result = callService.callMyService(jsonObj.toString(), url);
        return result;
    }
    
    public String callserviceForRawdata(String docid,String encodedImage, String extension){
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("methodname", "getrawocr");
            jsonObj.put("rawimage", encodedImage);
            jsonObj.put("docid", docid);
            jsonObj.put("docextension", extension);
        } catch (Exception ex) {
            Logger.getLogger(PreprocessImage.class.getName()).log(Level.SEVERE, null, ex);
        }
            String url = "http://128.199.222.136:5000/todo/api/v1.0/tasks";
//String url = "http://192.168.0.114:5000/todo/api/v1.0/tasks";
        System.out.println("url ============ "+url);
            RestCallingWrapper callService = new RestCallingWrapper();
            String result = null;
            result = callService.callMyService(jsonObj.toString(), url);
        return result;
    }
}
