/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.document;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.DocumentTemplate;
import com.mollatech.axiom.nucleus.db.connector.management.DocsTemplatesManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class SetCordinateValues extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        try {

            String cordinateName = request.getParameter("_cordinateName");
            String templateName = request.getParameter("_templateName");
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            DocsTemplatesManagement docObj = new DocsTemplatesManagement();
            String x1, x2, y1, y2,datatype,w;
            DocumentTemplate docsDetails = docObj.editDocumentDetails(templateName, channel.getChannelid());
            if (docsDetails != null) {
                if (docsDetails.getTemplateDetails() != null) {
                    JSONObject jsonObj = null;

                    jsonObj = new JSONObject(docsDetails.getTemplateDetails());

                    Iterator<?> keys = jsonObj.keys();
                    while (keys.hasNext()) {
                        String key = (String) keys.next();
                        if (key.equals(cordinateName)) {
                            JSONObject jObj;

                            jObj = new JSONObject(jsonObj.get(key).toString());
                            x1 = jObj.getString("x1");
                            x2 = jObj.getString("x2");
                            y1 = jObj.getString("y1");
                            y2 = jObj.getString("y2");
                            w = jObj.getString("w");
                            datatype = jObj.getString("_datatype");
                            json.put("x1", x1);
                            json.put("x2", x2);
                            json.put("y1", y1);
                            json.put("y2", y2);
                            json.put("w", w);
                            json.put("_datatype",datatype);

                        }
                    }
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(SetCordinateValues.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.print(json);
            out.flush();
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
