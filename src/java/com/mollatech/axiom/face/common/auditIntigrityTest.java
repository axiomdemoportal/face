/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.common;

import com.mollatech.axiom.nucleus.db.Audit;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author nilesh
 */
public class auditIntigrityTest extends HttpServlet {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(auditIntigrityTest.class.getName());

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        try {
            response.setContentType("application/json");
//            PrintWriter out = response.getWriter();
            String auditRecordtoRetutn = "";
            String result = "success";
            String message = "Audit Integrity check successful!!!";
            AuditManagement audit = new AuditManagement();
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel::" + channel.getName());
            String _IntegrityID = request.getParameter("_IntegrityID");
            log.debug("_IntegrityID::" + _IntegrityID);
            Audit auditObj = audit.getAuditrailByIntegrityId(channel.getChannelid(), _IntegrityID);
            JSONObject json = new JSONObject();
            PrintWriter out1 = response.getWriter();

            if (auditObj != null) {
                Calendar current = Calendar.getInstance();
                current.setTime(auditObj.getAuditedon());
                auditRecordtoRetutn = "Session Id:" + auditObj.getSessionId() + "\n" + "channel Id:" + auditObj.getChannelid() + "\n"
                        + " Operator Id:" + auditObj.getOperatorid() + "\n" + " Ip Address:" + auditObj.getIpaddress()
                        + "\n" + " Channel Name:" + auditObj.getChannelname() + "\n" + " Remoteaccesslogin:" + auditObj.getRemoteaccesslogin()
                        + "\n" + " Operator Name:" + auditObj.getOperatorname() + "\n" + " Time:" + current.getTime()
                        + "\n" + " Action:" + auditObj.getAction() + "\n" + " Result:" + auditObj.getResult() + "\n" + " Resultcode:"
                        + auditObj.getResultcode() + "\n" + " Category:" + auditObj.getCategory() + "\n" + " Oldvalue:" + auditObj.getOldvalue()
                        + "\n" + " Newvalue:" + auditObj.getNewvalue() + "\n" + " Itemtype:" + auditObj.getItemtype() + "\n" + " Itemid"
                        + auditObj.getItemid();
                byte[] SHA1hash = audit.SHA1(auditObj.getSessionId() + auditObj.getChannelid() + auditObj.getOperatorid() + auditObj.getIpaddress()
                        + auditObj.getChannelname() + auditObj.getRemoteaccesslogin() + auditObj.getOperatorname() + current.getTime()
                        + auditObj.getAction() + auditObj.getResult() + auditObj.getResultcode() + auditObj.getCategory() + auditObj.getOldvalue()
                        + auditObj.getNewvalue() + auditObj.getItemtype() + auditObj.getItemid());

                String integritycheck = new String(Base64.encode(SHA1hash));

                String strStatus = "Failed";
                if (_IntegrityID.equals(integritycheck)) {
                    strStatus = "Success";
                }

                if (strStatus.equals("Success")) {
                    try {

                        json.put("_result", result);
                        json.put("_message", message);

                        json.put("sessionId", auditObj.getSessionId());
                        json.put("channelId", auditObj.getChannelid());
                        json.put("OperatorId", auditObj.getOperatorid());
                        json.put("IpAddress", auditObj.getIpaddress());
                        json.put("ChannelName", auditObj.getChannelname());
                        json.put("Remoteaccesslogin", auditObj.getRemoteaccesslogin());
                        json.put("Time", current.getTime());
                        json.put("Action", auditObj.getAction());
                        json.put("Result", auditObj.getResult());
                        json.put("Resultcode", auditObj.getResultcode());
                        json.put("Category", auditObj.getCategory());
                        json.put("Newvalue", auditObj.getNewvalue());
                        json.put("Oldvalue", auditObj.getOldvalue());
                        json.put("Itemtype", auditObj.getItemtype());
                        json.put("Itemid", auditObj.getItemid());
                        json.put("_auditrecord", auditRecordtoRetutn);
                    } catch (Exception e) {
                        log.error("exception caught :: ", e);
                    }
                    out1.print(json);
                    out1.flush();
                    return;
                } else {
                    result = "error";
                    message = "Audit Integrity check fail!!!";
                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("exception caught :: ", e);
                    }
                    out1.print(json);
                    out1.flush();
                    return;
                }
            } else {
                result = "InvaidID";
                message = "No Record Found!!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    log.error("exception caught :: ", e);
                }
                out1.print(json);
                out1.flush();
                return;
            }
        } catch (Exception ex) {
            log.error("exception caught :: ", ex);
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
