/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.common;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import org.apache.log4j.Logger;
//import org.hibernate.Session;
import org.json.simple.JSONObject;

public class savesetting extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(savesetting.class.getName());

//    static final Logger logger = Logger.getLogger(login.class);
    final String itemType = "LOGIN";
    int SUCCESS = 0;
    int FAILED = -1;
    String BLOCKED = "BLOCKED_IP";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");

//        logger.info("Requested Servlet is login at " + new Date());
//        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        try {
            
          String _operatorName = request.getParameter("path");
          log.debug("_operatorName::"+_operatorName);
           Properties prop = new Properties();
           prop = ( Properties)request.getSession().getAttribute("propObj");
            
             
           
            int i=1;
            String result = "success";
            String message = "DB Setting successfuly Upadated....";

            String url = "showlicenseI.jsp";


            

            String filename = _operatorName;

            try {
                
                String propFileName = filename;
                
             Enumeration enamObj = prop.propertyNames();
            
            while (enamObj.hasMoreElements()) {
                
                String key = (String) enamObj.nextElement();
                String value = request.getParameter(key);
                 prop.setProperty(key, value);

                 
                
            }
                
                

                FileOutputStream f = new FileOutputStream(propFileName);
                
             //   FileOutputStream fileOut = new FileOutputStream(file);
			prop.store(f, "DB SEtting");
			f.close();

      
                


                
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;

            } catch (Exception e) {
                 log.error("exception caught :: ",e);
            }

        } catch (Exception ex) {

//            logger.error("Exception at DoTransform ", ex);
            // TODO handle custom exceptions here
             log.error("exception caught :: ",ex);
            String result = "error";
            String message = ex.getLocalizedMessage();
            try {
                json.put("_result", result);
//                logger.debug("Response of login Servlet's Parameter  result is " + result);
                json.put("_message", message);
//                logger.debug("Response of login Servlet's Parameter  message is " + message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
//            logger.info("Response of login Servlet at " + new Date());
            out.print(json);
            out.flush();
            log.info("is ended :: ");
            return;
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
