/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.axiom.face.common;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nilesh
 */
public class addgeotracking extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(addgeotracking.class.getName());

  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        log.info("is started :: ");
        try{
           
//            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
//            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
//              Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
//            String _latitude = request.getParameter("latitude");
//            String _longitude = request.getParameter("longitude");
//            
//            MobileTrustManagment  trustObj = new MobileTrustManagment();
//           int res = trustObj.addGeoTrack(sessionId, channel.getChannelid(), operatorS.getOperatorid(), _longitude, request.getRemoteAddr(), _latitude, 
//                    "city", "state", "country", "zip code", 1, new Date());
//           
//            System.out.println("Result ="+res);
//            
//            
//            AxiomLocationImpl a = new AxiomLocationImpl();
//            
//           Location locale =  a.getLocationByIp(request.getRemoteAddr());
//       
//            int res1 = trustObj.addGeoTrack(sessionId, channel.getChannelid(), operatorS.getOperatorid(), locale.longitude, request.getRemoteAddr(), _latitude, 
//                    locale.city, locale.state, locale.country, locale.zipcode, 1, new Date());
           
        }catch(Exception ex){
             log.error("exception caught :: ",ex);
        }
         log.info("is ended :: ");
      
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
