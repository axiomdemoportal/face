/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.common;
/**
 *
 * @author mollatech1
 */
public class donut {
    
    int value;
    String label;

    public donut(int value, String label) {
        this.value = value;
        this.label = label;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
    
    
    
}
