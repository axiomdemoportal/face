/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.common;

/**
 *
 * @author bluebricks
 */
public class float_donut {
    String label;
    int data;
    String color;

    public float_donut(String label, int data, String color) {
        this.label = label;
        this.data = data;
        this.color = color;
    }

    
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getValue() {
        return data;
    }

    public void setValue(int data) {
        this.data = data;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
    
}
