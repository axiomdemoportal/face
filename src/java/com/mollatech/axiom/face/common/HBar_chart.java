/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.common;

/**
 *
 * @author bluebricks
 */
public class HBar_chart {
   int y;
   String label;
     String indexLabel;

    public HBar_chart(int y, String label, String indexLabel) {
        this.y = y;
        this.label = label;
        this.indexLabel = indexLabel;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getIndexLabel() {
        return indexLabel;
    }

    public void setIndexLabel(String indexLabel) {
        this.indexLabel = indexLabel;
    }
   
    
    
}
