/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.common;

import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.AxiomMessageManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.hibernate.Session;

/**
 *
 * @author vikramsareen
 */
public class CleanAuditTrailsListener implements ServletContextListener {

    private static int g_intervalCall;
    //private static String g_channelid;
    private static Channels g_channel=null;
    private static Timer g_timer=null;
    
    static org.apache.log4j.Logger logObj = org.apache.log4j.Logger.getLogger(CleanAuditTrailsListener.class.getName());

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
            Session sChannel = suChannel.openSession();
            ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
            ServletContext ch = sce.getServletContext();
            String _channelName = ch.getContextPath();
            //System.out.println("Channel Core Name >>" + _channelName);
            //_channelName = "/ibank2core";
            _channelName = _channelName.replaceAll("/", "");
            if (_channelName.compareTo("core") != 0) {
                _channelName = _channelName.replaceAll("core", "");
            } else {
                _channelName = "face";
            }

            g_channel = cUtil.getChannel(_channelName);
            if (g_channel == null) {
                logObj.error("Channel Details could not be found>>" + _channelName);
                sChannel.close();
                suChannel.close();
                return;
            }

            sChannel.close();
            suChannel.close();

            g_intervalCall = 86400; //1 day in seconds

            try {
                g_timer = new Timer();
                g_timer.schedule(new TimerTask() {
                    public void run() {
                        //start of code
                        String _channelid = g_channel.getChannelid();
                        ChannelManagement chmgnObj = new ChannelManagement();
                        
                        Channels ch = chmgnObj.getChannelByID(_channelid);
                        if(ch!=null){
                        
                        SettingsManagement sMngt = new SettingsManagement();
                        Object ipobj = sMngt.getSettingInner(_channelid, SettingsManagement.GlobalSettings, 1);
                        ChannelProfile channelprofileObj = null;
                        Object channelpobj = sMngt.getSettingInner(_channelid, SettingsManagement.CHANNELPROFILE_SETTING, 1);

                        if (channelpobj == null) {
                            LoadSettings.LoadChannelProfile(channelprofileObj);
                        } else {
                            channelprofileObj = (ChannelProfile) channelpobj;
                            LoadSettings.LoadChannelProfile(channelprofileObj);
                        }

                        String days = LoadSettings.g_sSettings.getProperty("audit.clean.up.duration.days");

                        String _duration = days;

                        String path = LoadSettings.g_sSettings.getProperty("cleanup.log");
                        AuditManagement amanage = new AuditManagement();
                        AxiomMessageManagement amsgmanage = new AxiomMessageManagement();
                        int duration = Integer.parseInt(_duration);
                        try {
                            int auditdeleted = amanage.deleteAuditrail(_channelid, duration);
                            int channellogsdeleted = amsgmanage.deletechannellogs(_channelid, duration);
                            String log = "" + new Date();
                            log += "," + "Channel=" + ch.getName();
                            if (auditdeleted == -1) {
                                log += "," + "Audit-Trail-Status=ERROR";
                                log += "," + "Removed-Trails=0";
                            } else {
                                log += "," + "Audit-Trail-Status=SUCCESS";
                                log += "," + "Removed-Trails=" + auditdeleted;
                            }
                            if (channellogsdeleted == -1) {
                                log += "," + "Message-Logs-Status=ERROR";
                                log += "," + "Removed-Message-Logs=0";
                            } else {
                                log += "," + "Message-Logs-Status=SUCCESS";
                                log += "," + "Removed-Message-Logs=" + channellogsdeleted;
                            }
                            if (path != null && path.isEmpty() == false) {
                                File file = new File(path);
                                //if file doesnt exists, then create it
                                if (!file.exists()) {
                                    file.createNewFile();
                                }
                                //true = append file

                                appendToFile(file.getAbsoluteFile(), log);

//                                Writer writer = null;
//
//                                try {
//                                    writer = new BufferedWriter(new OutputStreamWriter(
//                                            new FileOutputStream(file.getAbsolutePath()), "utf-8"));
//
//                                    writer.append(log + ",\n");
//
//                                } catch (IOException ex) {
//                                    // report
//                                } finally {
//                                    try {
//                                        writer.close();
//                                    } catch (Exception ex) {
//                                    }
//                                }
//                                FileWriter fileWritter = new FileWriter(file.getName(), true);
//                                try (BufferedWriter bufferFileWriter = new BufferedWriter(fileWritter)) {
//                                    bufferFileWriter.append("hello world");
//                                    bufferFileWriter.close();
//                                }
                                logObj.debug(log);
                            }

                        } catch (Exception e) {
                            
                            e.printStackTrace();
                            
                        }
                        
                    }

                        //end of code
                    }
                }, (60000), (g_intervalCall * 1000));
            } catch (Exception ex) {
                logObj.error(ex);
            } finally {
            }

        } catch (Exception ex) {
            logObj.error(ex);
        } finally {
        }

    }

    public int appendToFile(File path, String data) {
        // OTPTokenManagement oManagement = new OTPTokenManagement(channelid);
        // Otptokens[] oTokenList = oManagement.getTokenByCatAndSubCat(channelid, 1, 2);

        PrintWriter out = null;
        try {
            out = new PrintWriter(new BufferedWriter(new FileWriter(path, true)));
            out.println(data);
        } catch (IOException e) {
            System.err.println(e);
        } finally {
            if (out != null) {
                out.close();
            }
        }

        return -1;

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        try {
            if ( g_timer != null) {
                g_timer.purge();
                g_timer.cancel();
            }
        } catch (Exception ex) {
            
        }
    }

}
