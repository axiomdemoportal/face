package com.mollatech.axiom.face.common;

import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.AxiomMessageManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class cleanup extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(cleanup.class.getName());

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        log.info("is started :: ");

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String _channelid = request.getParameter("_channelid");
        log.debug("_channelid::" + _channelid);
        String _duration = request.getParameter("_dateToDelete");
        log.debug("_duration::" + _duration);
        ChannelManagement chmgnObj = new ChannelManagement();
        Channels ch = chmgnObj.getChannelByID(_channelid);
        
        String path = LoadSettings.g_sSettings.getProperty("cleanup.log");

        AuditManagement amanage = new AuditManagement();
        AxiomMessageManagement amsgmanage = new AxiomMessageManagement();
        int duration = Integer.parseInt(_duration);
        try {
            int auditdeleted = amanage.deleteAuditrail(_channelid, duration);
            log.debug("deleteAuditrail::" + auditdeleted);
            int channellogsdeleted = amsgmanage.deletechannellogs(_channelid, duration);
            log.debug("deletechannellogs::" + channellogsdeleted);

            String log = "" + new Date();
            log += "," + "Channel=" + ch.getName();
            if (auditdeleted == -1) {
                log += "," + "Audit Trail Status=ERROR";
                log += "," + "Number of Audit Trails Removed=0";
            } else {
                log += "," + "Audit Trail Status=SUCCESS";
                log += "," + "Number of Audit Trails Removed=" + auditdeleted;
            }

            if (channellogsdeleted == -1) {
                log += "," + "Message Logs Status=ERROR";
                log += "," + "Number of Message Logs Removed=0";
            } else {
                log += "," + "Message Logs Status=SUCCESS";
                log += "," + "Number of Message Logs Removed=" + channellogsdeleted;
            }

            if (path != null && path.isEmpty() == false) {
                File file = new File(path);
                //if file doesnt exists, then create it
                if (!file.exists()) {
                    file.createNewFile();
                }
                //true = append file
                FileWriter fileWritter = new FileWriter(file.getName(), true);
                try (BufferedWriter bufferFileWriter = new BufferedWriter(fileWritter)) {
                    bufferFileWriter.append(log);
                }
                //System.out.println("Done");
                out.println(log);
            }

        } catch (Exception e) {
            log.error("exception caught :: ",e);
            out.println(e.getMessage());
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
