/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.common;

/**
 *
 * @author mollatech1
 */
public class bar {
   
    String label;
     int value;
    int value1;
    int value2;

    public bar(int value,String label){
        this.label = label;
        this.value = value;
    }
    
    public bar(String label, int value, int value1, int value2) {
        this.label = label;
        this.value = value;
        this.value1 = value1;
        this.value2 = value2;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getValue1() {
        return value1;
    }

    public void setValue1(int value1) {
        this.value1 = value1;
    }

    public int getValue2() {
        return value2;
    }

    public void setValue2(int value2) {
        this.value2 = value2;
    }
   

    
}
