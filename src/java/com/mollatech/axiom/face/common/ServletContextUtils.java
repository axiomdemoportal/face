package com.mollatech.axiom.face.common;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

public class ServletContextUtils {

	public static Map<String, TwitterGoogleClientInfo> getTwitterGoogleClientMemoryStorage(HttpSession session) {
		Map<String, TwitterGoogleClientInfo> oauthTokens = (Map<String, TwitterGoogleClientInfo>) session.getServletContext().getAttribute("TWITTER_OAUTH");
		if (oauthTokens == null) {
			oauthTokens = new HashMap<String, TwitterGoogleClientInfo>();
			session.getServletContext().setAttribute("TWITTER_OAUTH", oauthTokens);
		}
		//System.out.println("ServletContextUtils.getTwitterGoogleClientMemoryStorage(), [" + oauthTokens.size() + "]");
		return oauthTokens;
	}

	public static class TwitterGoogleClientInfo {
		private Integer clientId;
		public TwitterGoogleClientInfo(Integer clientId) {
			super();
			this.clientId = clientId;
		}
		public Integer getClientId() {
			return clientId;
		}
		public void setClientId(Integer clientId) {
			this.clientId = clientId;
		}
	}
	
}
