/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.common;

/**
 *
 * @author bluebricks
 */
public class Bar_Horizontal {
    String label;
    int data;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }
    

    public Bar_Horizontal(String label, int data) {
        this.label = label;
        this.data = data;
    }
    
}
