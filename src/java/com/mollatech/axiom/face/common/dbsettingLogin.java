package com.mollatech.axiom.face.common;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import org.apache.log4j.Logger;
//import org.hibernate.Session;
import org.json.simple.JSONObject;

public class dbsettingLogin extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(dbsettingLogin.class.getName());

//    static final Logger logger = Logger.getLogger(login.class);
    final String itemType = "LOGIN";
    int SUCCESS = 0;
    int FAILED = -1;
    String BLOCKED = "BLOCKED_IP";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

       log.info("is started :: ");
//        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        try {
            String _operatorName = request.getParameter("_name");
            log.debug("_operatorName::" + _operatorName);
            String _password = request.getParameter("_passwd");
            
            String _channelName = this.getServletContext().getContextPath();
            _channelName = _channelName.replaceAll("/", "");
            String verify="CCE4FB48-865D-4DCF-A091-6D4511F03B87";
            //        AuditManagement audit = new AuditManagement();
            String result = "success";
            String message = "successful credential verification....";

            String url = "showConfigTable.jsp";

//            Operators operartorObj = null;
            if (_operatorName == null || _password == null) {
                result = "error";
                message = "Invalid Credentials!!";
                url = "showConfig.jsp";
                json.put("_result", result);
//                logger.debug("Response of login Servlet's Parameter  result is " + result);
                json.put("_message", message);
//                logger.debug("Response of login Servlet's Parameter  message is " + message);
                json.put("_url", url);
//                logger.debug("Response of login Servlet's Parameter  url is " + url);
                out.print(json);
                out.flush();
                return;
            }
            
            
            if ( ! _password.equals("CCE4FB48-865D-4DCF-A091-6D4511F03B87"))
            {
             result = "error";
                message = "Invalid Password!!";
                url = "showConfig.jsp";
                json.put("_result", result);
//                logger.debug("Response of login Servlet's Parameter  result is " + result);
                json.put("_message", message);
//                logger.debug("Response of login Servlet's Parameter  message is " + message);
                json.put("_url", url);
//                logger.debug("Response of login Servlet's Parameter  url is " + url);
                out.print(json);
                out.flush();
                return;   
            }

            String filename = _operatorName;

            try {
                Properties prop = new Properties();
                String propFileName = filename;
                

// InputStream  inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
                FileInputStream f = new FileInputStream(propFileName);
                prop.load(f);

//if (inputStream != null) {
//prop.load(inputStream);
//} else {
//throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
//}
                Date time = new Date(System.currentTimeMillis());

// get the property value and print it out
                request.getSession().setAttribute("propObj", prop);
                request.getSession().setAttribute("filepath", propFileName);
                json.put("_result", result);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;

            } catch (Exception e) {
                //System.out.println("Exception: " + e);
            }

        } catch (Exception ex) {
            log.error("exception caught :: ",ex);

//            logger.error("Exception at DoTransform ", ex);
            // TODO handle custom exceptions here
            log.error("Exception caught :: ",ex);
            String result = "error";
            String message = ex.getLocalizedMessage();
            try {
                json.put("_result", result);
//                logger.debug("Response of login Servlet's Parameter  result is " + result);
                json.put("_message", message);
//                logger.debug("Response of login Servlet's Parameter  message is " + message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
//            logger.info("Response of login Servlet at " + new Date());
            out.print(json);
            out.flush();
            log.info("is ended :: ");
            return;
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
