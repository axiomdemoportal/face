/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.externalreports;

/**
 *
 * @author bluebricks
 */
public class DetailsFromExpayBridge {
     private String phoneNo;
    private String name;
   
    private String emailid;
    private String accountNumber;
  
    private String cardNumber;
  
    private Integer type;
    private String ibUserid;
    private String mbUserid;
    private String cifNumber;
    private Integer status;
     private Integer detailsFrom;

    public DetailsFromExpayBridge() {
    }

    public DetailsFromExpayBridge(String phoneNo, String name, String emailid, String accountNumber, String cardNumber, Integer type, String ibUserid, String mbUserid, String cifNumber, Integer status, Integer detailsFrom) {
        this.phoneNo = phoneNo;
        this.name = name;
        this.emailid = emailid;
        this.accountNumber = accountNumber;
        this.cardNumber = cardNumber;
        this.type = type;
        this.ibUserid = ibUserid;
        this.mbUserid = mbUserid;
        this.cifNumber = cifNumber;
        this.status = status;
        this.detailsFrom = detailsFrom;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getIbUserid() {
        return ibUserid;
    }

    public void setIbUserid(String ibUserid) {
        this.ibUserid = ibUserid;
    }

    public String getMbUserid() {
        return mbUserid;
    }

    public void setMbUserid(String mbUserid) {
        this.mbUserid = mbUserid;
    }

    public String getCifNumber() {
        return cifNumber;
    }

    public void setCifNumber(String cifNumber) {
        this.cifNumber = cifNumber;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getDetailsFrom() {
        return detailsFrom;
    }

    public void setDetailsFrom(Integer detailsFrom) {
        this.detailsFrom = detailsFrom;
    }
    
    
    
   

 
   
  
    
    
}
