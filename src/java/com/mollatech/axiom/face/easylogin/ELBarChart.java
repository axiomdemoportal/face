/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.easylogin;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.face.common.bar;
import com.mollatech.axiom.face.common.donut;
import com.mollatech.axiom.nucleus.db.ApEasylogin;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.EasyLoginManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mohanish
 */
public class ELBarChart extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ELBarChart.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        log.info("is started :: ");
        int active = 0;
        int completed = 0;
        int pending = 0;
        int expired = 0;
        PrintWriter out = response.getWriter();
        try {
            response.setContentType("application/json");

            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel::" + channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId::" + sessionId);
            String channelID = channel.getChannelid();
            log.debug("channelID::"+ channelID);
            String _startdate = request.getParameter("_startDate");
            log.debug("_startdate::" + _startdate);
            String _enddate = request.getParameter("_endDate");
            log.debug("_enddate::"+_enddate);
            String _searchtext = request.getParameter("_searchtext");
            log.debug("_searchtext::"+_searchtext);
            String expiresOn = "NA";
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date startDate = null;
    
            if (_startdate != null && !_startdate.isEmpty()) {
                startDate = (Date) formatter.parse(_startdate);
            }
            Date endDate = null;
            if (_enddate != null && !_enddate.isEmpty()) {
                endDate = (Date) formatter.parse(_enddate);
            }
            if (startDate != null && startDate.equals(endDate)) {
                endDate = new Date(startDate.getTime() + TimeUnit.DAYS.toMillis(1));
            }
            ArrayList<bar> sample = new ArrayList<bar>();
            ApEasylogin[] txdetailses = new EasyLoginManagement().getAllEasyLoginByUser(sessionId, channel.getChannelid(), startDate, endDate, _searchtext);
            if (txdetailses != null) {
                for (int i = 0; i < txdetailses.length; i++) {
                    if (txdetailses[i].getExpireson() != null) {
                        expiresOn = format.format(txdetailses[i].getExpireson());
                        if (txdetailses[i].getExpireson().before(new Date())) {
                            expired++;
                        } else {
                            if (txdetailses[i].getStatus() == EasyLoginManagement.ACTIVE_STATUS) {
                                active++;
                            } else if (txdetailses[i].getStatus() == EasyLoginManagement.COMPLETED_STATUS) {
                                completed++;
                            } else if (txdetailses[i].getStatus() == EasyLoginManagement.EXPIRED_STATUS) {
                                expired++;
                            } else if (txdetailses[i].getStatus() == EasyLoginManagement.PENDING_STATUS) {
                                pending++;
                            }
                        }
                    }

                }
            }
            sample.add(new bar(active, "Active"));
            sample.add(new bar(completed, "Completed"));
            sample.add(new bar(pending, "Pending"));
            sample.add(new bar(expired, "Expired"));

            Gson gson = new Gson();

            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<donut>>() {
            }.getType());

            JsonArray jsonArray = element.getAsJsonArray();
            out.print(jsonArray);

        } catch (Exception e) {
            log.error("Exception caught ::",e);
        } finally {
            out.close();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
