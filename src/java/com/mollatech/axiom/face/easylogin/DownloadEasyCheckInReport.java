/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.easylogin;

import com.mollatech.axiom.nucleus.db.ApEasylogin;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.EasyLoginManagement;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mohanish
 */
public class DownloadEasyCheckInReport extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DownloadEasyCheckInReport.class.getName());

    private static int PDF_TYPE = 0;
    private static int CSV_TYPE = 1;
    private static int TXT_TYPE = 2;
    private static final int BUFSIZE = 4096;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        log.info("is started :: ");
        try {
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId :: "+sessionId);
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel::" + channel.getName());
            String _channelId = channel.getChannelid();
            log.debug("_channelId :: " + _channelId);
            
        try {
                ApEasylogin[] txdetailses = (ApEasylogin[]) request.getSession().getAttribute("easylogindetailses");
                String _format = request.getParameter("_reporttype");
                log.debug("_format::" + _format);
                String _startDate = request.getParameter("_startDate");
                log.debug("_startDate :: " + _startDate);
                String _endDate = request.getParameter("_endDate");
                log.debug("_endDate :: "+_endDate);
                
                int iFormat = Integer.parseInt(_format);
                if (PDF_TYPE == iFormat) {
                    iFormat = PDF_TYPE;
                } else if (CSV_TYPE == iFormat) {
                    iFormat = CSV_TYPE;
                } else {
                    iFormat = TXT_TYPE;
                }

                String filepath = null;

                try {
                    try {
                        EasyLoginManagement cObj = new EasyLoginManagement();
                        filepath = cObj.generateReportByDate(iFormat, txdetailses, _startDate, _endDate,_channelId);
                        cObj = null;
                    } catch (Exception e) {
                        log.error("Exception caught :: ",e);
                    }

                    //  
                    File file = new File(filepath);
                    int length = 0;
                    ServletOutputStream outStream = response.getOutputStream();
                    ServletContext context = getServletConfig().getServletContext();
                    String mimetype = context.getMimeType(filepath);

                    // sets response content type
                    if (mimetype == null) {
                        mimetype = "application/octet-stream";
                    }
                    response.setContentType(mimetype);
                    response.setContentLength((int) file.length());
                    String fileName = (new File(filepath)).getName();

                    // sets HTTP header
                    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

                    byte[] byteBuffer = new byte[BUFSIZE];
                    DataInputStream in = new DataInputStream(new FileInputStream(file));

                    // reads the file's bytes and writes them to the response stream
                    while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                        outStream.write(byteBuffer, 0, length);
                    }

                    in.close();
                    outStream.close();
                    file.delete();

                } catch (Exception ex) {
                    // TODO handle custom exceptions here
                    log.error("Exception caught :: ",ex);
                }

            } catch (Exception ex) {
                log.error("Exception caught :: ",ex);
            }

        } finally {
            //  out.close();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
