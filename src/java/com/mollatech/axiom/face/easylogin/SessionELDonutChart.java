/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.easylogin;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.face.common.donut;
import com.mollatech.axiom.nucleus.db.ApEasyloginsession;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.EasyLoginSessionManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mohanish
 */
public class SessionELDonutChart extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SessionELDonutChart.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        int apprived = 0;
        int na = 0;
        int rejected = 0;
        int expired = 0;
        PrintWriter out = response.getWriter();
        try {
            response.setContentType("application/json");

            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel ::" + channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId ::" + sessionId);
            String channelID = channel.getChannelid();
            log.debug("channelID ::"+ channelID);
            String _startdate = request.getParameter("_startDate");
            log.debug("_startdate ::" + _startdate);
            String _enddate = request.getParameter("_endDate");
            log.debug("_enddate ::"+_enddate);
            String _searchtext = request.getParameter("_searchStatus");
            log.debug("_searchtext ::"+_searchtext);
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date startDate = null;
            Integer status = 3;
            
            if (_searchtext != null && !_searchtext.equalsIgnoreCase("3")) {
                status = Integer.parseInt(_searchtext);
            }
            if (_startdate != null && !_startdate.isEmpty()) {
                startDate = (Date) formatter.parse(_startdate);
            }
            Date endDate = null;
            if (_enddate != null && !_enddate.isEmpty()) {
                endDate = (Date) formatter.parse(_enddate);
            }
            if (startDate != null && startDate.equals(endDate)) {
                endDate = new Date(startDate.getTime() + TimeUnit.DAYS.toMillis(1));
            }
            ArrayList<donut> sample = new ArrayList<donut>();
            ApEasyloginsession[] txdetailses = new EasyLoginSessionManagement().getAllEasyLoginSessionByDate(sessionId, channelID, startDate, endDate,status);
            if (txdetailses != null) {
                for (int i = 0; i < txdetailses.length; i++) {
                    if (txdetailses[i].getStatus() == EasyLoginSessionManagement.EAPPROVED_STATUS) {
                        apprived++;
                    } else if (txdetailses[i].getStatus() == EasyLoginSessionManagement.EEXPIRED_STATUS) {
                        expired++;
                    } else if (txdetailses[i].getStatus() == EasyLoginSessionManagement.ENA_STATUS) {
                        na++;
                    } else if (txdetailses[i].getStatus() == EasyLoginSessionManagement.EREJECTED_STATUS) {
                        rejected++;
                    }
                }
            }
            sample.add(new donut(apprived, "Approved"));
            sample.add(new donut(na, "NA"));
            sample.add(new donut(rejected, "Rejected"));
            sample.add(new donut(expired, "Expired"));

            Gson gson = new Gson();

            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<donut>>() {
            }.getType());

            JsonArray jsonArray = element.getAsJsonArray();
            out.print(jsonArray);

        } catch (Exception e) {
            log.error("exception caught :: ",e);
        } finally {
            out.close();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
