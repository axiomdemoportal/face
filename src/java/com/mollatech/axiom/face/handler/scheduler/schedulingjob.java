/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.handler.scheduler;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channellogs;
import com.mollatech.axiom.nucleus.db.Schedulersettings;
import com.mollatech.axiom.nucleus.db.Schedulertracking;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SchedulerManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.operation.AxiomOperator;
import com.mollatech.axiom.nucleus.settings.SchedulerSettingEntry;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import com.mollatech.dictum.management.BulkMSGManagement;
import java.io.ByteArrayInputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.MimetypesFileTypeMap;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author nilesh
 */
public class schedulingjob implements org.quartz.Job {

    private static int searchFromPrevieousExecutionTime = 0;
    private static int searchFromBeginingTime = 1;

    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {

        SchedulerManagement schObj = new SchedulerManagement();
        BulkMSGManagement bulk = new BulkMSGManagement();

        Date d = new Date();
        //System.out.println(d +  " >> Execution Start");
        JobDataMap dataMap = jec.getJobDetail().getJobDataMap();
        String channelid = dataMap.getString("channelId");

        Schedulersettings[] arrSchedulerObj = schObj.getSchedulerSettingByStatus(channelid, SchedulerManagement.SCHEDULER_ACTIVE_STATUS);
        if (arrSchedulerObj != null) {
            if (arrSchedulerObj.length != 0) {
                TemplateManagement tmObj = new TemplateManagement();
                Templates templateObj = tmObj.LoadbyName(channelid, "email.report.schedule.alert");
                if (arrSchedulerObj == null) {
                    return;
                }

                for (int i = 0; i < arrSchedulerObj.length; i++) {
                    try {
                        Schedulertracking trackingObj = schObj.getSchedulerTracking(channelid, arrSchedulerObj[i].getSchedulerName());
//                if (trackingObj == null) {
//                    continue;
//                }
                        ByteArrayInputStream bais = new ByteArrayInputStream(arrSchedulerObj[i].getSchedulerSettingEntry());
                        Object object = SchedulerManagement.deserializeFromObject(bais);
                        SchedulerSettingEntry schedulerSetting = null;
                        if (object instanceof SchedulerSettingEntry) {
                            schedulerSetting = (SchedulerSettingEntry) object;
                        }

                        //Date d1 = new Date();
                        String emailSubject = templateObj.getSubject();
                        bais = new ByteArrayInputStream(templateObj.getTemplatebody());
                        String message = (String) TemplateUtils.deserializeFromObject(bais);

                        emailSubject = emailSubject.replaceAll("#name#", arrSchedulerObj[i].getSchedulerName());
                        emailSubject = emailSubject.replaceAll("#datetime#", "" + new Date());

                        message = message.replaceAll("#name#", arrSchedulerObj[i].getSchedulerName());
                        message = message.replaceAll("#datetime#", "" + new Date());
                        String emailBody = message;

                        if (trackingObj != null) {

                            Date dLastExecutedOn = trackingObj.getExecutedOn();         //10am           
                            Date now = new Date();                                     //10.30am

                            Calendar cNextCheck = Calendar.getInstance();               //11
                            cNextCheck.setTime(dLastExecutedOn);

                            if (schedulerSetting.getDuration() == 0) { //hourly
                                cNextCheck.add(Calendar.HOUR, 1);
                            } else if (schedulerSetting.getDuration() == 1) {//daily
                                cNextCheck.add(Calendar.DAY_OF_YEAR, 1);
                            } else if (schedulerSetting.getDuration() == 7) {//weekly
                                cNextCheck.add(Calendar.DAY_OF_YEAR, 7);
                            } else if (schedulerSetting.getDuration() == 30) {//monthly
                                cNextCheck.add(Calendar.DAY_OF_YEAR, 30);
                            } else if (schedulerSetting.getDuration() == 91) {//quarterly
                                cNextCheck.add(Calendar.DAY_OF_YEAR, 91);
                            } else if (schedulerSetting.getDuration() == 365) {//yearly
                                cNextCheck.add(Calendar.DAY_OF_YEAR, 365);
                            }

                            Date pNextCheck = cNextCheck.getTime();

                            if (pNextCheck.before(now)) {
                                continue;
                            }
                            //go for getting the records and sending email. 

                            try {
                                Channellogs[] arrChanllog = bulk.searchMsgForScheduler(channelid, schedulerSetting.getType(), schedulerSetting.getGatewayStatus(),
                                        schedulerSetting.getPrefrence(), searchFromPrevieousExecutionTime, dLastExecutedOn, now);

                                if (arrChanllog == null) {
                                    //System.out.println("No recore found");
//                                    return;
//                                    break;
                                } else if (arrChanllog != null) {
                                    String pdffilename = null;
                                    String csvfilename = null;

                                    OperatorsManagement oprObj = new OperatorsManagement();
//                            int roleId = 1;
//                            if (schedulerSetting.getRolename().equals("admin")) {
//                                roleId = 2;
//                            } else if (schedulerSetting.getRolename().equals("helpdesk")) {
//                                roleId = 3;
//                            } else if (schedulerSetting.getRolename().equals("reporter")) {
//                                roleId = 4;
//                            }

                                    SendNotification send = new SendNotification();
                                    Integer iRole = Integer.valueOf(schedulerSetting.getRolename());
                                    AxiomOperator[] arrOperator = oprObj.GetOperatorByRoleId(channelid, iRole);

                                    String[] arrcc = schedulerSetting.getSendTo();
                                    String strOprarr = "";
                                    for (int j = 1; j < arrOperator.length - 1; j++) {
                                        strOprarr = strOprarr + "," + arrOperator[j].getStrEmail();
                                    }

                                    for (int j = 0; j < arrcc.length; j++) {
                                        strOprarr = strOprarr + "," + arrcc[j];
                                    }
                                    String[] emailList = strOprarr.split(",");

                                    String[] arrCC = new String[emailList.length - 1];
                                    for (int j = 1; j < emailList.length; j++) {
                                        arrCC[j - 1] = emailList[j];
                                    }

                                    MimetypesFileTypeMap mimetypesFileTypeMap = new MimetypesFileTypeMap();
                                    String filepath = null;
                                    if (schedulerSetting.getReportType() == 99) {
                                        if (arrChanllog != null) {
                                            pdffilename = bulk.createReportForScheduler(0, channelid, schedulerSetting.getType(), arrChanllog);
                                            csvfilename = bulk.createReportForScheduler(1, channelid, schedulerSetting.getType(), arrChanllog);

                                            String[] files = new String[2];
                                            files[0] = pdffilename;
                                            files[1] = csvfilename;

                                            String[] mimetypes = new String[2];
                                            //mimetypes[0] = mimetypesFileTypeMap.getContentType(pdffilename);
                                            mimetypes[0] = "application/pdf";
                                            //mimetypes[1] = mimetypesFileTypeMap.getContentType(csvfilename);
                                            mimetypes[1] = "application/text";

                                            AXIOMStatus axStatus = send.SendEmail(channelid, arrOperator[0].getStrEmail(), emailSubject, emailBody,
                                                    arrCC, null, files, mimetypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));

                                            filepath = pdffilename + "," + csvfilename;
                                        }
                                    } else if (schedulerSetting.getReportType() == 0) {
                                        if (arrChanllog != null) {
                                            pdffilename = bulk.createReportForScheduler(0, channelid, schedulerSetting.getType(), arrChanllog);

                                            String[] files = new String[1];
                                            files[0] = pdffilename;
                                            String[] mimetypes = new String[1];
                                            mimetypes[0] = mimetypesFileTypeMap.getContentType(pdffilename);
                                            AXIOMStatus axStatus = send.SendEmail(channelid, arrOperator[0].getStrEmail(), emailSubject, emailBody,
                                                    arrCC, null, files, mimetypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));

                                            filepath = pdffilename;
                                        }
                                    } else if (schedulerSetting.getReportType() == 1) {
                                        if (arrChanllog != null) {
                                            csvfilename = bulk.createReportForScheduler(1, channelid, schedulerSetting.getType(), arrChanllog);
                                            String[] files = new String[1];
                                            files[0] = pdffilename;
                                            String[] mimetypes = new String[1];
                                            mimetypes[0] = mimetypesFileTypeMap.getContentType(csvfilename);
                                            AXIOMStatus axStatus = send.SendEmail(channelid, arrOperator[0].getStrEmail(), emailSubject, emailBody,
                                                    arrCC, null, files, mimetypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));

                                            filepath = csvfilename;
                                        }
                                    }

                                    if (arrChanllog != null) {
                                        int res = schObj.AddSchedulerTracking(channelid, arrSchedulerObj[i].getSchedulerName(), schedulerSetting.getType(),
                                                filepath, object);
                                        if (res == 0) {
                                            //System.out.println("Scheduler Tracking Added Successfully!!!");
                                        } else {
                                            System.out.println("Failed to add Scheduler Tracking!!!");
                                        }
                                    }
                                }
                            } catch (Exception ex) {
                               // Logger.getLogger(schedulingjob.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        } else if (trackingObj == null) {
                            try {

                                Date now = new Date();                                     //10.30am

                                Calendar cNextCheck = Calendar.getInstance();               //11

                                if (schedulerSetting != null) {
                                    if (schedulerSetting.getDuration() == 0) { //hourly
                                        cNextCheck.roll(Calendar.HOUR, false);
                                    } else if (schedulerSetting.getDuration() == 1) {//daily                            
                                        cNextCheck.roll(Calendar.DAY_OF_YEAR, false);
                                    } else if (schedulerSetting.getDuration() == 7) {//weekly
                                        cNextCheck.roll(Calendar.WEEK_OF_YEAR, false);
                                    } else if (schedulerSetting.getDuration() == 30) {//monthly
                                        cNextCheck.roll(Calendar.MONTH, false);
                                    } else if (schedulerSetting.getDuration() == 91) {//quarterly                            
                                        cNextCheck.roll(Calendar.MONTH, false);
                                        cNextCheck.roll(Calendar.MONTH, false);
                                        cNextCheck.roll(Calendar.MONTH, false);
                                    } else if (schedulerSetting.getDuration() == 365) {//yearly                            
                                        cNextCheck.roll(Calendar.YEAR, false);

                                    }
                                }

                                Date pNextCheck = cNextCheck.getTime();

                                if (pNextCheck.after(now) || pNextCheck.equals(now)) {
                                    continue;
                                }

//                        Calendar polddate = Calendar.getInstance();
//
//                        Date olddate = polddate.getTime();
//                        Date newDate = new Date();
                                Channellogs[] arrChanllog = bulk.searchMsgForScheduler(channelid, schedulerSetting.getType(), schedulerSetting.getGatewayStatus(),
                                        schedulerSetting.getPrefrence(), searchFromBeginingTime, pNextCheck, now);

                                if (arrChanllog != null) {
                                    String pdffilename = null;
                                    String csvfilename = null;

                                    SendNotification send = new SendNotification();
                                    OperatorsManagement oMngt = new OperatorsManagement();
                                    Integer iRole = Integer.valueOf(schedulerSetting.getRolename());
                                    //Roles rolesChosen = oMngt.getRoleById(channelid,iRole);

                                    AxiomOperator[] arrOperator = oMngt.GetOperatorByRoleId(channelid, iRole);
                                    oMngt = null;

                                    String[] arrcc = schedulerSetting.getSendTo();
                                    String strOprarr = "";
                                    for (int j = 1; j < arrOperator.length - 1; j++) {
                                        strOprarr = strOprarr + "," + arrOperator[j].getStrEmail();
                                    }

                                    for (int j = 0; j < arrcc.length; j++) {
                                        strOprarr = strOprarr + "," + arrcc[j];
                                    }
                                    String[] emailList = strOprarr.split(",");

                                    String[] arrCC = new String[emailList.length - 1];
                                    for (int j = 1; j < emailList.length; j++) {
                                        arrCC[j - 1] = emailList[j];
                                    }

                                    MimetypesFileTypeMap mimetypesFileTypeMap = new MimetypesFileTypeMap();
                                    String filepath = null;
                                    if (schedulerSetting.getReportType() == 99) {
                                        if (arrChanllog != null) {
                                            pdffilename = bulk.createReportForScheduler(0, channelid, schedulerSetting.getType(), arrChanllog);
                                            csvfilename = bulk.createReportForScheduler(1, channelid, schedulerSetting.getType(), arrChanllog);

                                            String[] files = new String[2];
                                            files[0] = pdffilename;
                                            files[1] = csvfilename;

                                            String[] mimetypes = new String[2];
                                            mimetypes[0] = mimetypesFileTypeMap.getContentType(pdffilename);
                                            mimetypes[1] = mimetypesFileTypeMap.getContentType(csvfilename);

                                            AXIOMStatus axStatus = send.SendEmail(channelid, arrOperator[0].getStrEmail(), emailSubject, emailBody,
                                                    arrCC, null, files, mimetypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));

                                            filepath = pdffilename + "," + csvfilename;
                                        }
                                    } else if (schedulerSetting.getReportType() == 0) {
                                        if (arrChanllog != null) {
                                            pdffilename = bulk.createReportForScheduler(0, channelid, schedulerSetting.getType(), arrChanllog);

                                            String[] files = new String[1];
                                            files[0] = pdffilename;
                                            String[] mimetypes = new String[1];
                                            mimetypes[0] = mimetypesFileTypeMap.getContentType(pdffilename);
                                            AXIOMStatus axStatus = send.SendEmail(channelid, arrOperator[0].getStrEmail(), emailSubject, emailBody,
                                                    arrCC, null, files, mimetypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));

                                            filepath = pdffilename;
                                        }
                                    } else if (schedulerSetting.getReportType() == 1) {
                                        if (arrChanllog != null) {
                                            csvfilename = bulk.createReportForScheduler(1, channelid, schedulerSetting.getType(), arrChanllog);
                                            String[] files = new String[1];
                                            files[0] = pdffilename;
                                            String[] mimetypes = new String[1];
                                            mimetypes[0] = mimetypesFileTypeMap.getContentType(csvfilename);
                                            AXIOMStatus axStatus = send.SendEmail(channelid, arrOperator[0].getStrEmail(), emailSubject, emailBody,
                                                    arrCC, null, files, mimetypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));

                                            filepath = csvfilename;
                                        }
                                    }
                                    if (arrChanllog != null) {
                                        int res = schObj.AddSchedulerTracking(channelid, arrSchedulerObj[i].getSchedulerName(), schedulerSetting.getType(),
                                                filepath, object);
                                        if (res == 0) {
                                            //System.out.println("Scheduler Tracking Added Successfully!!!");
                                        } else {
                                            System.out.println("Failed to add Scheduler Tracking!!!");
                                        }
                                    }
                                }
                            } catch (Exception ex) {
                                //log.error("Exception caught :: ",ex);
                            }
                        }
                    } catch (Exception ex) {
                       // Logger.getLogger(schedulingjob.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }

        }
    }
}
