/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.handler.scheduler;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Schedulersettings;
import com.mollatech.axiom.nucleus.db.Schedulertracking;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SchedulerManagement;
import com.mollatech.axiom.nucleus.settings.SchedulerSettingEntry;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author nilesh
 */
public class removescheduler extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(removescheduler.class.getName());

    final String itemtype = "SCHEDULER";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        log.info("is started :: ");
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        try {
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel ::" + channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId ::" + sessionId);
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("remoteaccesslogin::" + remoteaccesslogin);
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operatorS ::" + operatorS.getOperatorid());

            String result = "success";
            String message = "Scheduler removed successfully....";

            String _schedulerName = request.getParameter("_schedulerName");
            log.debug("_schedulerName :: " + _schedulerName);
            
            SchedulerManagement schObj = new SchedulerManagement();
            AuditManagement audit = new AuditManagement();

            int retValue = -1;

            Schedulersettings oldObj = schObj.getSetting(sessionId, channel.getChannelid(), _schedulerName);
            ByteArrayInputStream bais = new ByteArrayInputStream(AxiomProtect.AccessDataBytes(oldObj.getSchedulerSettingEntry()));
            Object object = SchedulerManagement.deserializeFromObject(bais);

            SchedulerSettingEntry schedulerSetting = null;
            if (object instanceof SchedulerSettingEntry) {
                schedulerSetting = (SchedulerSettingEntry) object;
            }

            retValue = schObj.deletescheduler(sessionId, channel.getChannelid(), _schedulerName);
            log.debug("deletescheduler ::" + retValue);
            
            if (retValue == 0) {
                Schedulertracking[] arr = schObj.getarrSchedulerTracking(channel.getChannelid(), _schedulerName);
                if (arr != null) {
                    for (int i = 0; i < arr.length; i++) {
                        int res = schObj.deleteschedulerTracking(sessionId, channel.getChannelid(), arr[i].getSchedulerTrackingId());
                    }
                }
            }

            String strgoldatewayType = "";
            if (schedulerSetting.getType() == 0) {
                strgoldatewayType = "ALL";
            } else if (schedulerSetting.getType() == 1) {
                strgoldatewayType = "SMS";
            } else if (schedulerSetting.getType() == 2) {
                strgoldatewayType = "USSD";
            } else if (schedulerSetting.getType() == 3) {
                strgoldatewayType = "VOICE";
            } else if (schedulerSetting.getType() == 4) {
                strgoldatewayType = "EMAIL";
            } else if (schedulerSetting.getType() == 5) {
                strgoldatewayType = "FAX";
            } else if (schedulerSetting.getType() == 6) {
                strgoldatewayType = "FACEBOOK";
            } else if (schedulerSetting.getType() == 7) {
                strgoldatewayType = "LINKEDIN";
            } else if (schedulerSetting.getType() == 8) {
                strgoldatewayType = "TWITTER";
            } else if (schedulerSetting.getType() == 18) {
                strgoldatewayType = "ANDROIDPUSH";
            } else if (schedulerSetting.getType() == 19) {
                strgoldatewayType = "IPHONEPUSH";
            }
            String stroldschedulerStatus = "In-Active";
            if (oldObj.getSchedulerStatus() == 1) {
                stroldschedulerStatus = "Active";
            }
            String stroldgatewayStatus = "";
            if (schedulerSetting.getGatewayStatus() == 99) {
                stroldgatewayStatus = "ALL";
            }
            if (schedulerSetting.getGatewayStatus() == 0) {
                stroldgatewayStatus = "SENT";
            }
            if (schedulerSetting.getGatewayStatus() == 2) {
                stroldgatewayStatus = "PENDING";
            }
            if (schedulerSetting.getGatewayStatus() == -1) {
                stroldgatewayStatus = "FAILED";
            }
            if (schedulerSetting.getGatewayStatus() == -5) {
                stroldgatewayStatus = "BLOCKED";
            }

            String stroldreporttype = "";
            if (schedulerSetting.getReportType() == 99) {
                stroldreporttype = "PDF&CSV";
            } else if (schedulerSetting.getReportType() == 0) {
                stroldreporttype = "PDF";
            } else if (schedulerSetting.getReportType() == 1) {
                stroldreporttype = "CSV";
            }
            String resultString = "ERROR";
            if (retValue == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(), new Date(), "Remove Scheduler", resultString, retValue,
                        "Scheduler Management", "SchedulerName=" + oldObj.getSchedulerName() + "SchedulerStatus =" + stroldschedulerStatus
                        + "Duration =" + schedulerSetting.getDuration() + "Gateway Type=" + strgoldatewayType
                        + "Gateway Status=" + stroldgatewayStatus + "Gateway Preference =" + schedulerSetting.getPrefrence()
                        + "Role=" + schedulerSetting.getRolename() + "Report Type=" + stroldreporttype,
                        "Removed", 
                        itemtype, oldObj.getSettingId().toString());
            } else {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(), new Date(), "Remove Scheduler", resultString, retValue,
                        "Scheduler Management", "SchedulerName=" + oldObj.getSchedulerName() + "SchedulerStatus =" + stroldschedulerStatus
                        + "Duration =" + schedulerSetting.getDuration() + "Gateway Type=" + strgoldatewayType
                        + "Gateway Status=" + stroldgatewayStatus + "Gateway Preference =" + schedulerSetting.getPrefrence()
                        + "Role=" + schedulerSetting.getRolename() + "Report Type=" + stroldreporttype,
                        "Failed to remove scheduler", 
                        itemtype, oldObj.getSettingId().toString());
                result = "error";
                message = "Failed to remove Scheduler!!";
                out.print(json);
                out.flush();
                return;
            }
            if (retValue == 0) {
                result = "success";
                message = "Scheduler removed successfully";
            }

            try {
                json.put("_result", result);
                json.put("_message", message);

            } catch (Exception e) {
               log.error("exception caught :: ",e);
            } finally {
                out.print(json);
                out.flush();
            }

        } catch (Exception ex) {
            log.error("exception caught :: ",ex);
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
