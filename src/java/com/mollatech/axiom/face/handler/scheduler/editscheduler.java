/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.handler.scheduler;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Roles;
import com.mollatech.axiom.nucleus.db.Schedulersettings;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SchedulerManagement;
import com.mollatech.axiom.nucleus.settings.SchedulerSettingEntry;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author nilesh
 */
public class editscheduler extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editscheduler.class.getName());

    final String itemtype = "SCHEDULER";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("is started :: ");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        try {
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel ::" + channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId ::" + sessionId);
            String _channelId = channel.getChannelid();
            log.debug("_channelId ::" + _channelId);
            Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operator ::" + operator.getOperatorid());
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("remoteaccesslogin ::" + remoteaccesslogin);
            //String operatorId = operator.getOperatorid();

            String _gatewayType = request.getParameter("_gatewayType");
            log.debug("_gatewayType ::" +_gatewayType);
            String _gatewayPreference = request.getParameter("_gatewayPreference");
            log.debug("_gatewayPreference ::" + _gatewayPreference);
            String _gatewayStatus = request.getParameter("_gatewayStatus");
            log.debug("_gatewayStatus ::"+ _gatewayStatus);
            String _operatorRoles = request.getParameter("_operatorRoles");
            log.debug("_operatorRoles ::" + _operatorRoles);
            String _schedulerDuration = request.getParameter("_schedulerDuration");
            log.debug("_schedulerDuration ::" + _schedulerDuration);
            String _schedulername = request.getParameter("_schedulername");
            log.debug("_schedulername ::" + _schedulername);
            String _email = request.getParameter("_EditSchedulerContact0");
            log.debug("_email ::" + _email);
            String _reportType = request.getParameter("_reportType");
            log.debug("_reportType ::" + _reportType);
            String result = "success";
            String message = "Scheduler updated successfully!!!";
            int igatewayType = Integer.parseInt(_gatewayType);
//        int ischedulerStatus = Integer.parseInt(_schedulerStatus);
            int igatewayPreference = Integer.parseInt(_gatewayPreference);
            int igatewayStatus = Integer.parseInt(_gatewayStatus);
            int ioperatorRoles = Integer.parseInt(_operatorRoles);
            int ischedulerDuration = Integer.parseInt(_schedulerDuration);
            

//         String[] arr = _email.split(",");
//         List<String> list = new ArrayList<String>();
//        
//         for(int i = 0;i< arr.length;i++){
//            if(arr[i] != null && arr[i].isEmpty() != true){
//                list.add(arr[i]);
//            }
//         }
//        String[] emailList = list.toArray(new String[list.size()]);
//        
            String[] emailList = null;
            if (_email != null) {
                String[] arr = _email.split(",");
                List<String> list = new ArrayList<String>();
                for (int i = 0; i < arr.length; i++) {
                    if (arr[i] != null && arr[i].isEmpty() != true) {
                        list.add(arr[i]);
                    }
                }
                emailList = list.toArray(new String[list.size()]);
                for (int i = 0; i < emailList.length; i++) {
                    if (UtilityFunctions.isValidEmail(emailList[i]) == false) {
                        result = "error";
                        message = "Invalid email id!!!";
                        try {
                            json.put("_result", result);
                            json.put("_message", message);
                        } catch (Exception e) {
                            log.error("exception caught :: ",e);
                        }
                        out.print(json);
                        out.flush();
                        return;
                    }
                }
            }

            int ireportType = Integer.parseInt(_reportType);

//            Integer.parseInt("df");
//            String strRole = null;
//            if (ioperatorRoles == 1) {
//                strRole = "sysadmin";
//            } else if (ioperatorRoles == 2) {
//                strRole = "admin";
//            } else if (ioperatorRoles == 3) {
//                strRole = "helpdesk";
//            } else if (ioperatorRoles == 4) {
//                strRole = "reporter";
//            }
            
             OperatorsManagement oMngt = new OperatorsManagement();
             Roles rolesChosen = oMngt.getRoleById(channel.getChannelid(),ioperatorRoles);
             oMngt=null;
            
            SchedulerSettingEntry shSettingObj = new SchedulerSettingEntry();
            shSettingObj.setCreatedOn(new Date());
            shSettingObj.setDuration(ischedulerDuration);
            shSettingObj.setGatewayStatus(igatewayStatus);
            shSettingObj.setPrefrence(igatewayPreference);
            shSettingObj.setRolename("" + rolesChosen.getRoleid());
            shSettingObj.setStatus(igatewayStatus);
            shSettingObj.setType(igatewayType);
            shSettingObj.setSendTo(emailList);
            shSettingObj.setReportType(ireportType);

            SchedulerManagement schObj = new SchedulerManagement();
            AuditManagement audit = new AuditManagement();
            Schedulersettings oldObj = schObj.getSetting(sessionId, _channelId, _schedulername);
            ByteArrayInputStream bais = new ByteArrayInputStream(AxiomProtect.AccessDataBytes(oldObj.getSchedulerSettingEntry()));
            Object object = SchedulerManagement.deserializeFromObject(bais);
            SchedulerSettingEntry schedulerSetting = null;
            if (object instanceof SchedulerSettingEntry) {
                schedulerSetting = (SchedulerSettingEntry) object;
            }

            int retValue = schObj.editSchedulerSetting(sessionId, _channelId, _schedulername, shSettingObj);
            log.debug("editSchedulerSetting :: " + retValue);

            String strgatewayType = "";
            if (igatewayType == 0) {
                strgatewayType = "ALL";
            } else if (igatewayType == 1) {
                strgatewayType = "SMS";
            } else if (igatewayType == 2) {
                strgatewayType = "USSD";
            } else if (igatewayType == 3) {
                strgatewayType = "VOICE";
            } else if (igatewayType == 4) {
                strgatewayType = "EMAIL";
            } else if (igatewayType == 5) {
                strgatewayType = "FAX";
            } else if (igatewayType == 6) {
                strgatewayType = "FACEBOOK";
            } else if (igatewayType == 7) {
                strgatewayType = "LINKEDIN";
            } else if (igatewayType == 8) {
                strgatewayType = "TWITTER";
            } else if (igatewayType == 18) {
                strgatewayType = "ANDROIDPUSH";
            } else if (igatewayType == 19) {
                strgatewayType = "IPHONEPUSH";
            }

            String strgoldatewayType = "";
            if (schedulerSetting.getType() == 0) {
                strgoldatewayType = "ALL";
            } else if (schedulerSetting.getType() == 1) {
                strgoldatewayType = "SMS";
            } else if (schedulerSetting.getType() == 2) {
                strgoldatewayType = "USSD";
            } else if (schedulerSetting.getType() == 3) {
                strgoldatewayType = "VOICE";
            } else if (schedulerSetting.getType() == 4) {
                strgoldatewayType = "EMAIL";
            } else if (schedulerSetting.getType() == 5) {
                strgoldatewayType = "FAX";
            } else if (schedulerSetting.getType() == 6) {
                strgoldatewayType = "FACEBOOK";
            } else if (schedulerSetting.getType() == 7) {
                strgoldatewayType = "LINKEDIN";
            } else if (schedulerSetting.getType() == 8) {
                strgoldatewayType = "TWITTER";
            } else if (schedulerSetting.getType() == 18) {
                strgoldatewayType = "ANDROIDPUSH";
            } else if (schedulerSetting.getType() == 19) {
                strgoldatewayType = "IPHONEPUSH";
            }

            String strgatewayStatus = "";
            if (igatewayStatus == 99) {
                strgatewayStatus = "ALL";
            }
            if (igatewayStatus == 0) {
                strgatewayStatus = "SENT";
            }
            if (igatewayStatus == 2) {
                strgatewayStatus = "PENDING";
            }
            if (igatewayStatus == -1) {
                strgatewayStatus = "FAILED";
            }
            if (igatewayStatus == -5) {
                strgatewayStatus = "BLOCKED";
            }
            String stroldgatewayStatus = "";
            if (schedulerSetting.getGatewayStatus() == 99) {
                stroldgatewayStatus = "ALL";
            }
            if (schedulerSetting.getGatewayStatus() == 0) {
                stroldgatewayStatus = "SENT";
            }
            if (schedulerSetting.getGatewayStatus() == 2) {
                stroldgatewayStatus = "PENDING";
            }
            if (schedulerSetting.getGatewayStatus() == -1) {
                stroldgatewayStatus = "FAILED";
            }
            if (schedulerSetting.getGatewayStatus() == -5) {
                stroldgatewayStatus = "BLOCKED";
            }

//        String strschedulerStatus = "In-Active";
//        if(ischedulerStatus == 1){
//            strschedulerStatus = "Active";
//        }
            String stroldschedulerStatus = "In-Active";
            if (oldObj.getSchedulerStatus() == 1) {
                stroldschedulerStatus = "Active";
            }

            String strreporttype = "";
            if (ireportType == 99) {
                strreporttype = "PDF&CSV";
            } else if (ireportType == 0) {
                strreporttype = "PDF";
            } else if (ireportType == 1) {
                strreporttype = "CSV";
            }

            String stroldreporttype = "";
            if (schedulerSetting.getReportType() == 99) {
                stroldreporttype = "PDF&CSV";
            } else if (schedulerSetting.getReportType() == 0) {
                stroldreporttype = "PDF";
            } else if (schedulerSetting.getReportType() == 1) {
                stroldreporttype = "CSV";
            }

            String resultString = "ERROR";
            if (retValue == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                        "Edit Scheduler", resultString, retValue, 
                        "Scheduler Management",
                        "SchedulerName=" + oldObj.getSchedulerName() + "SchedulerStatus =" + stroldschedulerStatus
                        + "Duration =" + schedulerSetting.getDuration() + "Gateway Type=" + strgoldatewayType
                        + "Gateway Status=" + stroldgatewayStatus + "Gateway Preference =" + schedulerSetting.getPrefrence()
                        + "Role=" + schedulerSetting.getRolename() + "Report Type=" + stroldreporttype,
                        "SchedulerName=" + _schedulername + "Duration =" + ischedulerDuration + "Gateway Type=" + strgatewayType
                        + "Gateway Status=" + strgatewayStatus + "Gateway Preference =" + igatewayPreference
                        + "Role=" +  rolesChosen.getName() + "Report Type=" + stroldreporttype,
                        itemtype, 
                        "" + oldObj.getSettingId());

            } else {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                        "Edit Scheduler", resultString, retValue, "Scheduler Management",
                        "SchedulerName=" + oldObj.getSchedulerName() + "SchedulerStatus =" + stroldschedulerStatus
                        + "Duration =" + schedulerSetting.getDuration() + "Gateway Type=" + strgoldatewayType
                        + "Gateway Status=" + stroldgatewayStatus + "Gateway Preference =" + schedulerSetting.getPrefrence()
                        + "Role=" + schedulerSetting.getRolename() + "Report Type=" + stroldreporttype,
                        "Failed to edit scheduler settings",
                        itemtype, 
                        ""+oldObj.getSettingId());
                result = "error";
                message = "Scheduler failed to update!!";
            }
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            } finally {
                out.print(json);
                out.flush();
            }

        } catch (Exception ex) {
            log.error("exception caught :: ",ex);
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
