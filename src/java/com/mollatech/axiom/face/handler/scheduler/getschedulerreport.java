/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.axiom.face.handler.scheduler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.SchedulerManagement;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nilesh
 */
public class getschedulerreport extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(getschedulerreport.class.getName());
    
    
   private static final int BUFSIZE = 4096;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        try {
            try {
                 Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                    log.debug("channel ::" + channel.getName());
                 String _trackingId = request.getParameter("_trackingId");
                    log.debug("getschedulerreport::Tracking Id is::" + _trackingId);
                 String _reporttype = request.getParameter("_reporttype");
                    log.debug("getschedulerreport::Type of report::" + _reporttype);
                 String _type = request.getParameter("_type");
                    log.debug("getschedulerreport::Type is::" + _type);
                 
                 int itrackingId = Integer.parseInt(_trackingId);
                 int ireporttype = Integer.parseInt(_reporttype);
                  int itype = Integer.parseInt(_type);
                  
                  SchedulerManagement scheduler = new SchedulerManagement();
                  String getfilepath = scheduler.getReportfilePath(channel.getChannelid(), itrackingId);
                  
                  String filepath = null;
                  String[] arrFilePath = null;
                  if(itype == 0){
                      arrFilePath = getfilepath.split(",");
                      for(int i=0;i<arrFilePath.length;i++){
                          if(ireporttype == 0){
                              if(arrFilePath[i].contains(".pdf")){
                                  filepath = arrFilePath[i];
                              }
                          }else if(ireporttype == 1){
                              if(arrFilePath[i].contains(".csv")){
                                  filepath = arrFilePath[i];
                              }
                          }
                      }
                      
                  }else if(itype == 1){
                      filepath = getfilepath;
                  }
              
                  if(filepath == null){
                      //System.out.println("File Path null :Scheduler Report: "+new Date());
                  }
                  
//                String filepath = null;
             try {
                    File file = new File(filepath);
                    int length = 0;
                    ServletOutputStream outStream = response.getOutputStream();
                    ServletContext context = getServletConfig().getServletContext();
                    String mimetype = context.getMimeType(filepath);

                    // sets response content type
                    if (mimetype == null) {
                        mimetype = "application/octet-stream";
                    }
                    response.setContentType(mimetype);
                    response.setContentLength((int) file.length());
                    String fileName = (new File(filepath)).getName();

                    // sets HTTP header
                    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

                    byte[] byteBuffer = new byte[BUFSIZE];
                    DataInputStream in = new DataInputStream(new FileInputStream(file));

                    // reads the file's bytes and writes them to the response stream
                    while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                        outStream.write(byteBuffer, 0, length);
                    }

                    in.close();
                    outStream.close();
//                    file.delete();

                } catch (Exception ex) {
                    // TODO handle custom exceptions here
                    log.error("exception caught :: ",ex);
                }
             } catch (Exception ex) {
                log.error("exception caught :: ",ex);
            }
        } finally {
            //  out.close();
        }
        log.info("is ended :: ");
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
