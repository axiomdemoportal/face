/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.handler.scheduler;

import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Otptrail;
import com.mollatech.axiom.nucleus.db.connector.management.OtpTrailHisManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OtpTrailManagement;
import java.util.Calendar;
import java.util.Date;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author pramodchaudhari
 */
public class AutoPurgeRemoval implements Job {

    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        System.out.println("Starting Schedular AutoPurgeRemoval" + new Date());
        JobDataMap dataMap = jec.getJobDetail().getJobDataMap();
        String channelid = dataMap.getString("channelId");
        int _purgingPeriod = 0;
        String purgingperiod = LoadSettings.g_sSettings.getProperty("purge.removal.duration");
        if (purgingperiod == null) {
            return;
        }
        _purgingPeriod = Integer.parseInt(purgingperiod);
//        System.out.println("Purging Period"+_purgingPeriod);
        Date endDate = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        cal.add(Calendar.DATE, -_purgingPeriod);
        Date startDate = cal.getTime();
        try {
            OtpTrailHisManagement otpTrailHisMgmt = new OtpTrailHisManagement();
            OtpTrailManagement otpTrailMgmt = new OtpTrailManagement();
            Otptrail[] otpTrail = otpTrailHisMgmt.purgeOtpTrail(channelid, startDate, endDate);
            if (otpTrail != null && otpTrail.length > 0) {
                System.out.println("Number of records to backup " + otpTrail.length);
                int purginRes = otpTrailHisMgmt.addOtpTrail("", otpTrail);
                System.out.println("Number of records to Remove" + otpTrail.length);
                int removalRes = -1;
                if (purginRes == 0) {
                    System.out.println("Records Purged Successfully");
                    removalRes = otpTrailMgmt.deleteOtpTrail("", startDate, endDate);
                    System.out.println("Records Removed Successfully");
                }

            }
            System.out.println("Purging Schedular Ended " + new Date());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
