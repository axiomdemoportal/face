/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.handler.scheduler;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.communication.CASettings;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Certificates;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.RootCertificateSettings;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Date;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import static com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement.CERT_STATUS_REVOKED;


public class CRLRefreshJob implements Job {

//    private static String channelid;
    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //System.out.println("**********************************************CRL SCHEDULER*************************************");
        JobDataMap dataMap = jec.getJobDetail().getJobDataMap();
        String channelid = dataMap.getString("channelId");

        //System.out.println("ChannelId=" + channelid);
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //System.out.println("Schedular Started ~" + new Date());
        SettingsManagement sMngmt = new SettingsManagement();
        RootCertificateSettings caSettings = null;
        Object settingsObj = null;
        String crlURLString = null;
        if (channelid != null) {
            settingsObj = sMngmt.getSetting(channelid, SettingsManagement.RootConfiguration, SettingsManagement.PREFERENCE_ONE);
            if (settingsObj != null) {
                caSettings = (RootCertificateSettings) settingsObj;
                crlURLString = caSettings.getCRL();
            }

            AuditManagement audit = new AuditManagement();

            try {

                
//                CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
//
//                if (crlURLString != null) {
//                    URL crlURL = new URL(crlURLString);
//                    InputStream crlStrem = crlURL.openStream();
//
//                    X509CRL crl = (X509CRL) certFactory.generateCRL(crlStrem);
//                    Set s = crl.getRevokedCertificates();
//
//                  
                    CertificateManagement crObj = new CertificateManagement();
//                    if (s != null && s.isEmpty() == false) {
//                        Iterator t = s.iterator();
//                        while (t.hasNext()) {
//                            X509CRLEntry entry = (X509CRLEntry) t.next();

                           Class c;
                AXIOMStatus status = null;
                Constructor argsConstructor;
                Method[] allMethods = null;
                Object object = null;  
                
                
                 CASettings caSet = new CASettings();
                caSet.channelId = caSettings.getChannelId();
                caSet.ip = caSettings.getIp();
                caSet.password = caSettings.getPassword();
                caSet.port = caSettings.getPort();
                caSet.reserve1 = (String) caSettings.getReserve1();
                caSet.reserve2 = (String) caSettings.getReserve2();
                caSet.reserve3 = (String) caSettings.getReserve3();
                caSet.userId = caSettings.getUserId();
                caSet.keylength = caSettings.getKeyLength();
                caSet.durationDays = caSettings.getValidityDays();
                caSet.CRL=caSettings.getCRL();
                 
                
                try {
                    c = Class.forName(caSettings.getClassName());
                    argsConstructor = c.getConstructor();
                    object = argsConstructor.newInstance();
                    allMethods = c.getDeclaredMethods();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                for (Method m : allMethods) {
                    String mname = m.getName();
                    if (mname.compareTo("Load") == 0) {
                        m.setAccessible(true);
                        try {
                            status = (AXIOMStatus) m.invoke(object, caSet);
                        } catch (Exception ex) {
                            ex.printStackTrace();

                        }
                        break;
                    }
                }
                

                String [] serialnumbers = null;

                for (Method m : allMethods) {
                    String mname = m.getName();
                    if (mname.compareTo("GetCRL") == 0) {
                        m.setAccessible(true);
                        try {
                            serialnumbers = (String[]) m.invoke(object);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        break;
                    }
                }
                
                if(serialnumbers!=null){
                for(int i=0;i<serialnumbers.length;i++){
                            
                            Certificates tempCert = crObj.getCertificatebysrno(serialnumbers[i]);
                            if (tempCert != null) {
                                
                                if (tempCert.getStatus() == 1) {
//                                    System.err.println("************************ change status called **********");
                                     String strDeleteOption = LoadSettings.g_sSettings.getProperty("cert.delete.logical");
                                    if ( strDeleteOption == null || strDeleteOption.isEmpty() == true || strDeleteOption.compareTo("yes") != 0 ) {
                                        strDeleteOption = "no";
                                    } else { 
                                        if(strDeleteOption.compareTo("yes") == 0 ){
                                            strDeleteOption = "yes";                                            
                                        }
                                    }
                                    
                                            
                                    //this will delete the certificate from the system
                                    int result=-1;
                                    if ( strDeleteOption.compareTo("no") == 0 )
                                         result=crObj.DeleteCert(channelid,tempCert.getUserid());
                                    else { 
                                        //this will do logical delete
                                        tempCert.setStatus(-5);
                                         result = crObj.changeStatusbyCRL(channelid, tempCert.getUserid(), CERT_STATUS_REVOKED);
                                    }

                                    if (result == 0) {
                                        audit.AddAuditTrail("Schedular", channelid, "Schedular",
                                                "Schedular",
                                                //                    /ipaddress, 
                                                "Schedular",
                                                "", "", new Date(), "Revoke Certificate", "SUCCESS", 0,
                                                "Certificate Management", "", "Revoke certificate from CA",
                                                "PKITOKEN", tempCert.getUserid());
                                    } else {
                                        audit.AddAuditTrail("Schedular", channelid, "Schedular",
                                                "Schedular",
                                                //                    /ipaddress, 
                                                "Schedular",
                                                "", "", new Date(), "Revoke Certificate", "ERROR", -1,
                                                "Certificate Management", "", "Revoke certificate from CA",
                                                "PKITOKEN", tempCert.getUserid());
                                    }

                                }
                           // }
                       // }
                    }
                }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
