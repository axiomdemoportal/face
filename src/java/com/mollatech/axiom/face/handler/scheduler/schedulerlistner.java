/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.handler.scheduler;

import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.RootCertificateSettings;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.hibernate.Session;
import static org.quartz.JobBuilder.newJob;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import org.quartz.Trigger;
import static org.quartz.TriggerBuilder.newTrigger;

public class schedulerlistner implements ServletContextListener {

    private static Timer timer;
    private static int intervalCall;
    SchedulerFactory schedFact = new org.quartz.impl.StdSchedulerFactory();
    Scheduler sched = null;
    private static Channels channel;

    //FOR CRL AND RENEW OF CERTIFICATES
    private static int CRLintervalCall;
    private static int autoRenewCertintervalCall;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
            Session sChannel = suChannel.openSession();
            ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);

            ServletContext ch = sce.getServletContext();
            String _channelName = ch.getContextPath();
            _channelName = _channelName.replaceAll("/", "");
            if (_channelName.compareTo("core") != 0) {
                _channelName = _channelName.replaceAll("core", "");
            } else {
                _channelName = "face";
            }

            channel = cUtil.getChannel(_channelName);
            if (channel == null) {
                System.out.println("Channel Details could not be found>>" + _channelName);
                return;
            }
            String interval = LoadSettings.g_sSettings.getProperty("scheduler.run.check.time");
            String purgingStartTime = LoadSettings.g_sSettings.getProperty("autopurge.starttime");
              String autopurgeDayInterval = LoadSettings.g_sSettings.getProperty("autopurge.day.interval");
            if (interval == null) {
                System.out.println("Please set value for scheduler.run.check.time variable in dbsetting.conf file");
                return;
            }
            
            if (interval != null) {
              intervalCall = new Integer(interval).intValue();
            } else {
                intervalCall = 30;
            }

            String channelid = channel.getChannelid();
            int CRLinterval = -1;
            SettingsManagement sMngmt = new SettingsManagement();
            RootCertificateSettings caSettings = null;
            Object settingsObj = null;

            if (channelid != null) {
                settingsObj = sMngmt.getSetting(channelid, SettingsManagement.RootConfiguration, SettingsManagement.PREFERENCE_ONE);
                if (settingsObj != null) {
                    caSettings = (RootCertificateSettings) settingsObj;
                    CRLinterval = caSettings.getCrlPoolTime();

                }
            }

//                String CRLinterval = LoadSettings.g_sSettings.getProperty("scheduler.crlrun.check.time");
            if (CRLinterval == -1) {
                System.out.println("Please set value for Pool Time in CA Configuration");
                return;
            }
            if (CRLinterval != -1) {
                CRLintervalCall = (CRLinterval * 60);
            } else {
                CRLintervalCall = 1440; // 24 hours
            }

//                autoRenewCertintervalCall=(1440);  // 720 minutes are 24 hours. this scheduler will run once every day.
            autoRenewCertintervalCall = 1;

//            JobDetail crlRefresh = newJob(CRLRefreshJob.class).withIdentity("reporter1", "mollatech1").usingJobData("channelId", channelid).build();
//            JobDetail certAutoRenew = newJob(AutoRenewCerts.class).withIdentity("reporter2", "mollatech2").usingJobData("channelId", channelid).build();
//            JobDetail job = newJob(schedulingjob.class).withIdentity("reporter", "mollatech").usingJobData("channelId", channelid).build();
          


//added for autoPurging otpTrail
            JobDetail autoPurgeotpTrail = newJob(AutoPurgeRemoval.class).withIdentity("reporter3", "mollatech2").usingJobData("channelId", channelid).build();
//            Trigger trigger = newTrigger()
//                    .withIdentity("myTrigger", "group1")
//                    .startNow()
//                    .withSchedule(simpleSchedule()
//                            .withIntervalInMinutes(intervalCall)
//                            .repeatForever())
//                    .build();
//
//            sched = schedFact.getScheduler();
//            sched.scheduleJob(job, trigger);
//            sched.start();
//
//            Trigger trigger1 = newTrigger()
//                    .withIdentity("myTrigger1", "group1")
//                    .startNow()
//                    .withSchedule(simpleSchedule()
//                            .withIntervalInMinutes(CRLintervalCall)
//                            .repeatForever())
//                    .build();
//
//            sched = schedFact.getScheduler();
//
//            sched.scheduleJob(crlRefresh, trigger1);
//            sched.start();
//            //System.out.println("after trigger()");
//
//            Trigger trigger2 = newTrigger()
//                    .withIdentity("myTrigger2", "group1")
//                    .startNow()
//                    .withSchedule(simpleSchedule()
//                            .withIntervalInMinutes(autoRenewCertintervalCall)
//                            .repeatForever())
//                    .build();
//
//            sched = schedFact.getScheduler();
//            sched.scheduleJob(certAutoRenew, trigger2);
//            sched.start();
          String x=purgingStartTime;
          int dayCount=Integer.parseInt(autopurgeDayInterval);
          int hours=Integer.parseInt(x.split(":")[0]);
          int mins=Integer.parseInt(x.split(":")[1]);
            Calendar cal= Calendar.getInstance();
            cal.setTime(new Date());
            cal.set(Calendar.HOUR_OF_DAY, hours);
            cal.set(Calendar.MINUTE, mins);
          Trigger trigger3 = newTrigger()
                    .withIdentity("myTrigger3", "group1")
                    .startAt(cal.getTime())
                    .withSchedule(simpleSchedule()
                            .withIntervalInMinutes(dayCount*1440)
                            .repeatForever())
                    .build();

//             Trigger trigger3 = newTrigger()
//                    .withIdentity("myTrigger3", "group1")
//                    .startNow()
//                    .withSchedule(simpleSchedule()
//                            .withIntervalInMinutes(autoRenewCertintervalCall)
//                            .repeatForever())
//                    .build();
            sched = schedFact.getScheduler();
            sched.scheduleJob(autoPurgeotpTrail, trigger3);
            System.out.println("Starting Purging Scheduldar");
            sched.start();
            

//           String interval = LoadSettings.g_sSettings.getProperty("scheduler.run.check.time");
//        if(interval != null){
//        intervalCall = new Integer(interval).intValue();
//        }else{
//            intervalCall = 300;
//        }
//         timer = new Timer();
//         timer.schedule(new TimerTask() {
//           
//                public void run() {
//                    try {
//                     //execution method
//                     sched = schedFact.getScheduler();
//                     sched.start();
//                     System.out.println("Scheduler Started");
//                   String channelid =  channel.getChannelid();
////                     JobDetail job = newJob(schedulingjob.class).withIdentity("myJob", "group1").build();
//                     JobDetail job = newJob(schedulingjob.class).withIdentity("myJob", "group1").usingJobData("channelId", channelid).build();
//
//                     
//                     Trigger trigger = newTrigger()
//                             .withIdentity("myTrigger", "group1")
//                             .startNow()
//                             .withSchedule(simpleSchedule()
////                                     .withIntervalInSeconds(5)
//                                     .withIntervalInMinutes(intervalCall)
//                                     .repeatForever())
//                             .build();
//
//                     sched.scheduleJob(job, trigger);
//                 } catch (SchedulerException ex) {
//                     Logger.getLogger(schedulerlistner.class.getName()).log(Level.SEVERE, null, ex);
//                 }
//
//             }
//            }, (60000), (intervalCall * 60000));
        } catch (SchedulerException ex) {
//            Logger.getLogger(schedulerlistner.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        try {
            if (sched != null) {
                sched.shutdown(true);
            }
            //sched.shutdown();
            //System.out.println("in contextDestroyed");
        } catch (Exception ex) {
            //Logger.getLogger(schedulerlistner.class.getName()).log(Level.SEVERE, null, ex);
//            log.error("Exception caught :: ",ex);
        }
    }
}
