/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.axiom.face.handler.scheduler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Schedulersettings;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SchedulerManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author nilesh
 */
public class changeschedulerstatus extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(changeschedulerstatus.class.getName());

   final String itemtype = "SCHEDULER";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         response.setContentType("application/json");
         log.info("is started :: ");
      try{
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel ::" + channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId ::" + sessionId);
           //audit
        Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operator ::" + operator.getOperatorid());
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID"); 
        log.debug("remoteaccesslogin ::" + remoteaccesslogin);
        
        String _schedulerName =request.getParameter("_schedulerName");
        log.debug("_schedulerName ::" + _schedulerName);
        String _status =request.getParameter("_status");
        log.debug("_status ::" + _status);
        
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Status Updated Successfuly!!!";
        
        int retValue = 0;
        int status =Integer.parseInt(_status);
        String _value = "Active";
        if ( status == 0 ) {
            _value = "Suspended";
        }
        
          
        
      
          SchedulerManagement scManagent = new SchedulerManagement();
          AuditManagement audit = new AuditManagement();
          Schedulersettings oldObj = scManagent.getSetting(sessionId, channel.getChannelid(), _schedulerName);
         String _oldvalue = "Active";
        if ( oldObj.getSchedulerStatus() == 0 ) {
            _oldvalue = "Suspended";
        }
          retValue =  scManagent.ChangeSchedulerStatus(sessionId, channel.getChannelid(), _schedulerName, status);
          log.debug("ChangeSchedulerStatus ::" + retValue);
          String resultString="ERROR";
        
        if(retValue == 0){
            resultString ="Success";
             audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                        "Change Status", resultString, retValue, "Scheduler Management",
                        "Old Status="+_oldvalue,"New Status ="+_value,
                        itemtype, oldObj.getSettingId().toString());
        }
        if (retValue != 0) {
               audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                       request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                        "Change Status", resultString, retValue, "Operator Management",
                        "Old Status="+_oldvalue,"Failed To Change Status",
                        itemtype, oldObj.getSettingId().toString());
            result = "error";
            message = "Status Update Failed!!!";
         }
        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);
        } catch (Exception ex) {
          log.error("exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
        } 
          
      }catch(Exception ex){
         log.error("exception caught :: ",ex);
      }
      log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
