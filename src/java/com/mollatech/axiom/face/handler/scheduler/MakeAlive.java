/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.handler.scheduler;

import com.mollatech.axiom.nucleus.db.connector.AccessesUtils;
import com.mollatech.axiom.nucleus.db.connector.AuditUtils;
import com.mollatech.axiom.nucleus.db.connector.AuthorizationUtils;
import com.mollatech.axiom.nucleus.db.connector.BillingUtils;
import com.mollatech.axiom.nucleus.db.connector.BulkMsgUtil;
import com.mollatech.axiom.nucleus.db.connector.CategoryUtils;
import com.mollatech.axiom.nucleus.db.connector.CertificateUtil;
import com.mollatech.axiom.nucleus.db.connector.ChannelLogsUtils;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.ClientDestroyUtils;
import com.mollatech.axiom.nucleus.db.connector.ConnectorStatusauditUtils;
import com.mollatech.axiom.nucleus.db.connector.ContactsUtils;
import com.mollatech.axiom.nucleus.db.connector.CountryListUtils;
import com.mollatech.axiom.nucleus.db.connector.EpdfSignUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import java.util.TimerTask;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author Pramod
 */
public class MakeAlive extends TimerTask {

    static final Logger logger = Logger.getLogger(MakeAlive.class);

    @Override
    public void run() {
    SessionFactoryUtil suAccess = new SessionFactoryUtil(SessionFactoryUtil.accesses);
        Session sAccess = suAccess.openSession();
        AccessesUtils cUtil = new AccessesUtils(suAccess, sAccess);
        cUtil.getCount();

        SessionFactoryUtil suApproval = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
        Session sApproval = suApproval.openSession();
        AuthorizationUtils authUtil = new AuthorizationUtils(suApproval, sApproval);
        authUtil.getCount();

        SessionFactoryUtil suAudit = new SessionFactoryUtil(SessionFactoryUtil.audit);
        Session sAudit = suAudit.openSession();
        AuditUtils auditUtil = new AuditUtils(suAudit, sAudit);
        auditUtil.getCount();

        SessionFactoryUtil suBilling = new SessionFactoryUtil(SessionFactoryUtil.billingManger);
        Session sBilling = suBilling.openSession();
        BillingUtils billingUtil = new BillingUtils(suBilling, sBilling);
        billingUtil.getCount();

//        SessionFactoryUtil suBulk = new SessionFactoryUtil(SessionFactoryUtil.bulkmsg);
//        Session sBulk = suBulk.openSession();
//        BulkMsgUtil bulkUtil = new BulkMsgUtil(suBulk, sBulk);
//        bulkUtil.getCount();

        SessionFactoryUtil suCert = new SessionFactoryUtil(SessionFactoryUtil.cerificates);
        Session sCert = suCert.openSession();
        CertificateUtil CertUtil = new CertificateUtil(suCert, sCert);
        CertUtil.getCount();

        SessionFactoryUtil suChannellogs = new SessionFactoryUtil(SessionFactoryUtil.channellogs);
        Session sChannellogs = suChannellogs.openSession();
        ChannelLogsUtils channellogs = new ChannelLogsUtils(suChannellogs, sChannellogs);
        channellogs.getCount();

        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils channel = new ChannelsUtils(suChannel, sChannel);
        channel.getCount();

        SessionFactoryUtil suClientdestroy = new SessionFactoryUtil(SessionFactoryUtil.clientdestroy);
        Session sClientdestroy = suClientdestroy.openSession();
        ClientDestroyUtils lientdestroy = new ClientDestroyUtils(suClientdestroy, sClientdestroy);
        lientdestroy.getCount();

        SessionFactoryUtil suConnector = new SessionFactoryUtil(SessionFactoryUtil.connectorstatusaudit);
        Session sConnector = suConnector.openSession();
        ConnectorStatusauditUtils Connector = new ConnectorStatusauditUtils(suConnector, sConnector);
        Connector.getCount();

        SessionFactoryUtil suContact = new SessionFactoryUtil(SessionFactoryUtil.contacts);
        Session sContact = suContact.openSession();
        ContactsUtils Contact = new ContactsUtils(suContact, sContact);
        Contact.getCount();

        SessionFactoryUtil sucountrylist = new SessionFactoryUtil(SessionFactoryUtil.countrylist);
        Session scountrylist = sucountrylist.openSession();
        CountryListUtils countrylist = new CountryListUtils(sucountrylist, scountrylist);
        countrylist.getCount();

        SessionFactoryUtil sudocCategory = new SessionFactoryUtil(SessionFactoryUtil.docCategory);
        Session sdocCategory = sudocCategory.openSession();
        CategoryUtils catutils = new CategoryUtils(sudocCategory, sdocCategory);
        catutils.getCount();

        SessionFactoryUtil suepinsessions = new SessionFactoryUtil(SessionFactoryUtil.epinsessions);
        Session sepinsessions = suepinsessions.openSession();
        EpdfSignUtils epinsessions = new EpdfSignUtils(suepinsessions, sepinsessions);
        epinsessions.getCountEpinsessions();

        SessionFactoryUtil suepintracker = new SessionFactoryUtil(SessionFactoryUtil.epintracker);
        Session sepintracker = suepintracker.openSession();
        EpdfSignUtils epintracker = new EpdfSignUtils(suepintracker, sepintracker);
        epintracker.getCountEpinTracker();

        SessionFactoryUtil suerrormessages = new SessionFactoryUtil(SessionFactoryUtil.errormessages);
        Session serrormessages = suerrormessages.openSession();
        EpdfSignUtils errormessages = new EpdfSignUtils(suerrormessages, serrormessages);
        errormessages.getCountErrormessages();

        SessionFactoryUtil sugeofence = new SessionFactoryUtil(SessionFactoryUtil.geofence);
        Session sgeofence = sugeofence.openSession();
        EpdfSignUtils geofence = new EpdfSignUtils(sugeofence, sgeofence);
        geofence.getCountGeofence();

        SessionFactoryUtil sugeotrack = new SessionFactoryUtil(SessionFactoryUtil.geotrack);
        Session sgeotrack = sugeotrack.openSession();
        EpdfSignUtils geotrack = new EpdfSignUtils(sugeotrack, sgeotrack);
        geotrack.getCountGeotrack();

        SessionFactoryUtil suinteractionresponse = new SessionFactoryUtil(SessionFactoryUtil.interactionresponse);
        Session sinteractionresponse = suinteractionresponse.openSession();
        EpdfSignUtils interactionresponse = new EpdfSignUtils(suinteractionresponse, sinteractionresponse);
        interactionresponse.getCountInteractionresponse();

        SessionFactoryUtil suinteractions = new SessionFactoryUtil(SessionFactoryUtil.interactions);
        Session sinteractions = suinteractions.openSession();
        EpdfSignUtils interactions = new EpdfSignUtils(suinteractions, sinteractions);
        interactions.getCountInteractions();

        SessionFactoryUtil suinteractionsexecutions = new SessionFactoryUtil(SessionFactoryUtil.interactionsexecutions);
        Session sinteractionsexecutions = suinteractionsexecutions.openSession();
        EpdfSignUtils interactionsexecutions = new EpdfSignUtils(suinteractionsexecutions, sinteractionsexecutions);
        interactionsexecutions.getCountIInteractionsexecutions();

        SessionFactoryUtil suisologs = new SessionFactoryUtil(SessionFactoryUtil.isologs);
        Session sisologs = suisologs.openSession();
        EpdfSignUtils isologs = new EpdfSignUtils(suisologs, sisologs);
        isologs.getCountIsologs();

        SessionFactoryUtil sukyctable = new SessionFactoryUtil(SessionFactoryUtil.kyctable);
        Session skyctable = sukyctable.openSession();
        EpdfSignUtils kyctable = new EpdfSignUtils(sukyctable, skyctable);
        kyctable.getCountKyctable();

        SessionFactoryUtil sumonitorsettings = new SessionFactoryUtil(SessionFactoryUtil.monitorsettings);
        Session smonitorsettings = sumonitorsettings.openSession();
        EpdfSignUtils monitorsettings = new EpdfSignUtils(sumonitorsettings, smonitorsettings);
        monitorsettings.getCountMonitorsettings();

        SessionFactoryUtil sumonitorTracking = new SessionFactoryUtil(SessionFactoryUtil.monitorTracking);
        Session smonitorTracking = sumonitorTracking.openSession();
        EpdfSignUtils monitorTracking = new EpdfSignUtils(sumonitorTracking, smonitorTracking);
        monitorTracking.getCountMonitortracking();

        SessionFactoryUtil suoperators = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session soperators = suoperators.openSession();
        EpdfSignUtils operators = new EpdfSignUtils(suoperators, soperators);
        operators.getCountOperators();

        SessionFactoryUtil suotptokens = new SessionFactoryUtil(SessionFactoryUtil.otptokens);
        Session sotptokens = suotptokens.openSession();
        EpdfSignUtils otptokens = new EpdfSignUtils(suotptokens, sotptokens);
        otptokens.getCountOtptokens();

        SessionFactoryUtil suotptrail = new SessionFactoryUtil(SessionFactoryUtil.otptrail);
        Session sotptrail = suotptrail.openSession();
        EpdfSignUtils otptrail = new EpdfSignUtils(suotptrail, sotptrail);
        otptrail.getCountOtptrail();

        SessionFactoryUtil supasswordtrail = new SessionFactoryUtil(SessionFactoryUtil.passwordtrail);
        Session spasswordtrail = supasswordtrail.openSession();
        EpdfSignUtils passwordtrail = new EpdfSignUtils(supasswordtrail, spasswordtrail);
        passwordtrail.getPasswordtrail();

        SessionFactoryUtil supkitokens = new SessionFactoryUtil(SessionFactoryUtil.pkitokens);
        Session spkitokens = supkitokens.openSession();
        EpdfSignUtils pkitokens = new EpdfSignUtils(supkitokens, spkitokens);
        pkitokens.getCountPkitokens();

        SessionFactoryUtil supushauth = new SessionFactoryUtil(SessionFactoryUtil.pushbasedauthentication);
        Session spushauth = supushauth.openSession();
        EpdfSignUtils pushauth = new EpdfSignUtils(supushauth, spushauth);
        pushauth.getCountPushbashauthentication();

        SessionFactoryUtil supushmessagesmapper = new SessionFactoryUtil(SessionFactoryUtil.pushmessagesmapper);
        Session spushmessagesmapper = supushmessagesmapper.openSession();
        EpdfSignUtils pushmessagesmapper = new EpdfSignUtils(supushmessagesmapper, spushmessagesmapper);
        pushmessagesmapper.getCountPushmessagemappers();

        SessionFactoryUtil supushmessages = new SessionFactoryUtil(SessionFactoryUtil.pushmessages);
        Session spushmessages = supushmessages.openSession();
        EpdfSignUtils pushmessages = new EpdfSignUtils(supushmessages, spushmessages);
        pushmessages.getCountPushmessages();

        SessionFactoryUtil suquestions = new SessionFactoryUtil(SessionFactoryUtil.questions);
        Session squestions = suquestions.openSession();
        EpdfSignUtils questions = new EpdfSignUtils(suquestions, squestions);
        questions.getCountQuestions();

        SessionFactoryUtil suquestionsandanswers = new SessionFactoryUtil(SessionFactoryUtil.questionsandanswers);
        Session squestionsandanswers = suquestionsandanswers.openSession();
        EpdfSignUtils questionsandanswers = new EpdfSignUtils(suquestionsandanswers, squestionsandanswers);
        questionsandanswers.getCountQuestionsandanswers();

        SessionFactoryUtil supushdevice = new SessionFactoryUtil(SessionFactoryUtil.pushdevice);
        Session spushdevice = supushdevice.openSession();
        EpdfSignUtils pushdevice = new EpdfSignUtils(supushdevice, spushdevice);
        pushdevice.getCountRegisterdevicepush();

        SessionFactoryUtil suregistrationcodetrail = new SessionFactoryUtil(SessionFactoryUtil.registrationcodetrail);
        Session sregistrationcodetrail = suregistrationcodetrail.openSession();
        EpdfSignUtils registrationcodetrail = new EpdfSignUtils(suregistrationcodetrail, sregistrationcodetrail);
        registrationcodetrail.getCountRegistrationcodetrail();

        SessionFactoryUtil suremoteaccess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sremoteaccess = suremoteaccess.openSession();
        EpdfSignUtils remoteaccess = new EpdfSignUtils(suremoteaccess, sremoteaccess);
        remoteaccess.getCountRemoteaccess();

        SessionFactoryUtil suremotesign = new SessionFactoryUtil(SessionFactoryUtil.remotesign);
        Session sremotesign = suremotesign.openSession();
        EpdfSignUtils remotesign = new EpdfSignUtils(suremotesign, sremotesign);
        remotesign.getCountRemotesignature();

        SessionFactoryUtil suresources = new SessionFactoryUtil(SessionFactoryUtil.resources);
        Session sresources = suresources.openSession();
        EpdfSignUtils resources = new EpdfSignUtils(suresources, sresources);
        resources.getCountResources();

        SessionFactoryUtil suroles = new SessionFactoryUtil(SessionFactoryUtil.roles);
        Session sroles = suroles.openSession();
        EpdfSignUtils roles = new EpdfSignUtils(suroles, sroles);
        roles.getCountRoles();

        SessionFactoryUtil suschedulersetting = new SessionFactoryUtil(SessionFactoryUtil.schedulersetting);
        Session sschedulersetting = suschedulersetting.openSession();
        EpdfSignUtils schedulersetting = new EpdfSignUtils(suschedulersetting, sschedulersetting);
        schedulersetting.getCountSchedulersettings();

        SessionFactoryUtil suschedulertracking = new SessionFactoryUtil(SessionFactoryUtil.schedulertracking);
        Session sschedulertracking = suschedulertracking.openSession();
        EpdfSignUtils schedulertracking = new EpdfSignUtils(suschedulertracking, sschedulertracking);
        schedulertracking.getCountSchedulertracking();

        SessionFactoryUtil susecurephrase = new SessionFactoryUtil(SessionFactoryUtil.securephrase);
        Session ssecurephrase = susecurephrase.openSession();
        EpdfSignUtils securephrase = new EpdfSignUtils(susecurephrase, ssecurephrase);
        securephrase.getCountSecurephrase();

        SessionFactoryUtil susecuretrap = new SessionFactoryUtil(SessionFactoryUtil.securetrap);
        Session ssecuretrap = susecuretrap.openSession();
        EpdfSignUtils securetrap = new EpdfSignUtils(susecuretrap, ssecuretrap);
        securetrap.getCountSecuretrap();

        SessionFactoryUtil susessions = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session ssessions = susessions.openSession();
        EpdfSignUtils sessions = new EpdfSignUtils(susessions, ssessions);
        sessions.getCountSession();

        SessionFactoryUtil susettings = new SessionFactoryUtil(SessionFactoryUtil.settings);
        Session ssettings = susettings.openSession();
        EpdfSignUtils settings = new EpdfSignUtils(susettings, ssettings);
        settings.getCountSettings();

        SessionFactoryUtil surequest = new SessionFactoryUtil(SessionFactoryUtil.epdfsigning);
        Session srequest = surequest.openSession();
        EpdfSignUtils signrequest = new EpdfSignUtils(surequest, srequest);
        signrequest.getCountSignRequest();

        SessionFactoryUtil suSNMPreceiver = new SessionFactoryUtil(SessionFactoryUtil.SNMPreceiver);
        Session sSNMPreceiver = suSNMPreceiver.openSession();
        EpdfSignUtils SNMPreceiver = new EpdfSignUtils(suSNMPreceiver, sSNMPreceiver);
        SNMPreceiver.getCountSnmpreceiversettings();

        SessionFactoryUtil suSNMPreceivertracking = new SessionFactoryUtil(SessionFactoryUtil.SNMPreceivertracking);
        Session sSNMPreceivertracking = suSNMPreceivertracking.openSession();
        EpdfSignUtils SNMPreceivertracking = new EpdfSignUtils(suSNMPreceivertracking, sSNMPreceivertracking);
        SNMPreceivertracking.getCountSnmpreceivertracking();

        SessionFactoryUtil suSystemmessageReceivertracking = new SessionFactoryUtil(SessionFactoryUtil.SystemmessageReceivertracking);
        Session sSystemmessageReceivertracking = suSystemmessageReceivertracking.openSession();
        EpdfSignUtils SystemmessageReceivertracking = new EpdfSignUtils(suSystemmessageReceivertracking, sSystemmessageReceivertracking);
        SystemmessageReceivertracking.getCountSystemmessagereceivertracking();

        SessionFactoryUtil susrtracking = new SessionFactoryUtil(SessionFactoryUtil.epdfsigntracking);
        Session ssrtracking = susrtracking.openSession();
        EpdfSignUtils Syssrtracking = new EpdfSignUtils(susrtracking, ssrtracking);
        Syssrtracking.getCountSrtracking();

        SessionFactoryUtil susso = new SessionFactoryUtil(SessionFactoryUtil.SsoDetails);
        Session ssSso = susso.openSession();
        EpdfSignUtils SysSsoDetails = new EpdfSignUtils(susso, ssSso);
        SysSsoDetails.getCountSsodetails();

        SessionFactoryUtil sustatelist = new SessionFactoryUtil(SessionFactoryUtil.statelist);
        Session sstatelist = sustatelist.openSession();
        EpdfSignUtils statelist = new EpdfSignUtils(sustatelist, sstatelist);
        statelist.getCountStateList();

        SessionFactoryUtil suSystemmessageReceiver = new SessionFactoryUtil(SessionFactoryUtil.SystemmessageReceiver);
        Session sSystemmessageReceiver = suSystemmessageReceiver.openSession();
        EpdfSignUtils SystemmessageReceiver = new EpdfSignUtils(suSystemmessageReceiver, sSystemmessageReceiver);
        SystemmessageReceiver.getCountSystemmessagesettings();

        SessionFactoryUtil sutemplates = new SessionFactoryUtil(SessionFactoryUtil.templates);
        Session stemplates = sutemplates.openSession();
        EpdfSignUtils templates = new EpdfSignUtils(sutemplates, stemplates);
        templates.getCountTemplates();

        SessionFactoryUtil sutimestamp = new SessionFactoryUtil(SessionFactoryUtil.timestamp);
        Session stimestamp = sutimestamp.openSession();
        EpdfSignUtils timestamp = new EpdfSignUtils(sutimestamp, stimestamp);
        timestamp.getCountTimestamp();

        SessionFactoryUtil sutrusteddevice = new SessionFactoryUtil(SessionFactoryUtil.trusteddevice);
        Session strusteddevice = sutrusteddevice.openSession();
        EpdfSignUtils trusteddevice = new EpdfSignUtils(sutrusteddevice, strusteddevice);
        trusteddevice.getCountTrusteddevice();

        SessionFactoryUtil sutwowayauth = new SessionFactoryUtil(SessionFactoryUtil.twowayauth);
        Session stwowayauth = sutwowayauth.openSession();
        EpdfSignUtils twowayauth = new EpdfSignUtils(sutwowayauth, stwowayauth);
        twowayauth.getCountTwowayauth();

        SessionFactoryUtil sutwowaysession = new SessionFactoryUtil(SessionFactoryUtil.twowaysession);
        Session stwowaysession = sutwowaysession.openSession();
        EpdfSignUtils twowaysession = new EpdfSignUtils(sutwowaysession, stwowaysession);
        twowaysession.getCountTwowaysession();

        SessionFactoryUtil sutxDetails = new SessionFactoryUtil(SessionFactoryUtil.txDetails);
        Session stxDetails = sutxDetails.openSession();
        EpdfSignUtils txDetails = new EpdfSignUtils(sutxDetails, stxDetails);
        txDetails.getCountTxdetails();

        SessionFactoryUtil suunits = new SessionFactoryUtil(SessionFactoryUtil.units);
        Session sunits = suunits.openSession();
        EpdfSignUtils units = new EpdfSignUtils(suunits, sunits);
        units.getCountUnits();

        SessionFactoryUtil suusergroup = new SessionFactoryUtil(SessionFactoryUtil.usergroup);
        Session susergroup = suusergroup.openSession();
        EpdfSignUtils usergroup = new EpdfSignUtils(suusergroup, susergroup);
        usergroup.getCountUsergroups();

        SessionFactoryUtil suWebresource = new SessionFactoryUtil(SessionFactoryUtil.Webresource);
        Session sWebresource = suWebresource.openSession();
        EpdfSignUtils Webresource = new EpdfSignUtils(suWebresource, sWebresource);
        Webresource.getCountWebresource();
    }
}
