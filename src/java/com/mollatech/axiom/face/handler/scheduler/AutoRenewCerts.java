/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.handler.scheduler;

import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Certificates;
import com.mollatech.axiom.nucleus.db.Remotesignature;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PDFSigningManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author parimal
 */
public class AutoRenewCerts implements Job {

    private static int _autoRenewflag;

    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

        JobDataMap dataMap = jec.getJobDetail().getJobDataMap();
        String channelid = dataMap.getString("channelId");
        String autoRenewflag = LoadSettings.g_sSettings.getProperty("autorenewcerts.days");

        if (autoRenewflag == null) {
            //System.out.println("Please set value for autorenewcerts.days variable in dbsetting.conf file");
            return;
        }
        if (autoRenewflag != null) {
            _autoRenewflag = new Integer(autoRenewflag).intValue();
        } else {
            _autoRenewflag = 2;
        }
        int result;
        CertificateManagement crObj = new CertificateManagement();
        Date sDate = new Date();
        Date eDate = null;
        // String dt = sD;  // Start date
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        String sd = sdf.format(sDate);
        String ed = null;
        try {
            c.setTime(sdf.parse(sd));
            c.add(Calendar.DATE, (_autoRenewflag+1));
            ed = sdf.format(c.getTime());
            eDate = sdf.parse(ed);
        } catch (ParseException ex) {
            Logger.getLogger(AutoRenewCerts.class.getName()).log(Level.SEVERE, null, ex);
        }
         boolean transactionCheck=false;
        if (sDate != null && eDate != null) {
            
            
            
            //SettingsManagement setManagement = new SettingsManagement();
            //CryptoManagement cManagement = new CryptoManagement();
            //RootCertificateSettings rCertSettings = (RootCertificateSettings) setManagement.getSetting(sessionid, channelid, SettingsManagement.RootConfiguration, 1);
//            RootCertificateSettings rCertSettings = (RootCertificateSettings) setManagement.getSetting(channelid, SettingsManagement.RootConfiguration, 1);
            AuditManagement audit = new AuditManagement();
            Certificates[] expiringcerts = crObj.getExpireSoonCertificate(channelid, sDate, eDate);
            if (expiringcerts != null) {
                 UserManagement userObj = new UserManagement();
                for (int i = 0; i < expiringcerts.length; i++) {
                      String userid = expiringcerts[i].getUserid();
                     PDFSigningManagement pdfsMgmt= new PDFSigningManagement();
                    Remotesignature remSign= pdfsMgmt.getCurrentSign(channelid,userid,1);
                     Date referenceDate= new Date();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(referenceDate);
                    cal.add(Calendar.MONTH, -3);
                 if(cal.getTimeInMillis()>=remSign.getTimeOfSigning().getTime())
                 {
                     audit.AddAuditTrail("Schedular", channelid, "Schedular",
                                        "Schedular",
                                        //                    /ipaddress, 
                                        "Schedular",
                                        "", "", new Date(), "Revoke Certificate", "Stopped Cause not Transaction from 3 months", 0,
                                        "Certificate Management", "", "Certificate has not been auto-renewed",
                                        "PKITOKEN", userid);
                 }
                   else
                 {

                    if (expiringcerts[i].getStatus() == 1) {
                        result = crObj.RenewCertificatev2(channelid, userid, expiringcerts[i]);

                        if (result == 0) {
                            if (result == 0) {
                                audit.AddAuditTrail("Schedular", channelid, "Schedular",
                                        "Schedular",
                                        //                    /ipaddress, 
                                        "Schedular",
                                        "", "", new Date(), "Revoke Certificate", "SUCCESS", 0,
                                        "Certificate Management", "", "Certificate has been auto-renewed",
                                        "PKITOKEN", userid);
                            } else {
                                audit.AddAuditTrail("Schedular", channelid, "Schedular",
                                        "Schedular",
                                        //                    /ipaddress, 
                                        "Schedular",
                                        "", "", new Date(), "Revoke Certificate", "ERROR", -1,
                                        "Certificate Management", "", "Certificate could not be auto-renewed",
                                        "PKITOKEN", userid);
                            }

                        }
                    }
                 }
                }
            }
        }

    }

}
