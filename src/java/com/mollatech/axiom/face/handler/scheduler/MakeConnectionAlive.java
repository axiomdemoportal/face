/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.handler.scheduler;


import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import java.util.Timer;
import java.util.TimerTask;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.Logger;

/**
 * Web application lifecycle listener.
 *
 * @author Pramod
 */
//@WebListener()
public class MakeConnectionAlive implements ServletContextListener {

    private static int intervalCall = 1;
    private Timer timer = null;
    static final Logger logger = Logger.getLogger(MakeConnectionAlive.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            String strintervalCall = LoadSettings.g_sSettings.getProperty("scheduler.run.check.time");
            intervalCall = Integer.parseInt(strintervalCall);
            intervalCall = intervalCall * 60;
            TimerTask task = new MakeAlive();
            timer = new Timer();
            timer.schedule(task, 1000, intervalCall * 1000);
            logger.debug("after trigger()");
        } catch (Exception ex) {
            logger.error("Exception at MakeConnectionAlive ", ex);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        try {
            timer.cancel();
            logger.debug("Listener Stooped");
        } catch (Exception ex) {
            logger.error("Exception at MakeConnectionAlive ", ex);
        }
    }
}
