/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.handler.scheduler;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Roles;
import com.mollatech.axiom.nucleus.db.Schedulersettings;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SchedulerManagement;
import com.mollatech.axiom.nucleus.settings.SchedulerSettingEntry;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.management.relation.Role;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;

/**
 *
 * @author nilesh
 */
public class addscheduler extends HttpServlet {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(addscheduler.class.getName());

    final String itemtype = "SCHEDULER";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("is started :: ");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        try {
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel ::" + channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId ::" + sessionId);
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("remoteaccesslogin ::" + remoteaccesslogin);
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operatorS ::" + operatorS.getOperatorid());
            String OperatorID = operatorS.getOperatorid();

            String _gatewayType = request.getParameter("_gatewayType");
            log.debug("_gatewayType ::" +_gatewayType);
            String _schedulerStatus = request.getParameter("_schedulerStatus");
            log.debug("_schedulerStatus ::" + _schedulerStatus);
            String _gatewayPreference = request.getParameter("_gatewayPreference");
            log.debug("_gatewayPreference ::" + _gatewayPreference);
            String _gatewayStatus = request.getParameter("_gatewayStatus");
            log.debug("_gatewayStatus::"+ _gatewayStatus);
            String _operatorRoles = request.getParameter("_operatorRoles");
            log.debug("_operatorRoles ::" + _operatorRoles);
            String _schedulerDuration = request.getParameter("_schedulerDuration");
            log.debug("_schedulerDuration ::" + _schedulerDuration);
            String _schedulername = request.getParameter("_schedulername");
            log.debug("_schedulername ::" + _schedulername);
            String _email = request.getParameter("_tagsList");
            log.debug("_email ::" + _email);
            String _reportType = request.getParameter("_reportType");
            log.debug("_reportType ::" + _reportType);

            String result = "success";
            String message = "Scheduler Settings Added Successfully!!!";
            
            if (AxiomProtect.CheckEnforcementFor(AxiomProtect.REPORT_SCHEDULER) != 0) {
                result = "error";
                message = "This feature is not available in this license!!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                  log.error("exception caught :: ",e);
                }
                out.print(json);
                out.flush();
                return;
            }

            if (_schedulername == null || _schedulerDuration == null
                    || _gatewayPreference == null || _gatewayStatus == null
                    || _email == null || _gatewayType == null || _operatorRoles == null
                    || _reportType == null || _schedulerStatus == null) {
                result = "error";
                message = "Please fill all details!!!";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }
            if (_schedulername.isEmpty() == true || _schedulerDuration.isEmpty() == true
                    || _gatewayPreference.isEmpty() == true || _gatewayStatus.isEmpty() == true
                    || _email.isEmpty() == true || _gatewayType.isEmpty() == true || _operatorRoles.isEmpty() == true
                    || _reportType.isEmpty() == true || _schedulerStatus.isEmpty() == true) {
                result = "error";
                message = "Please fill all details!!!";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }
            Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(_schedulername);
            boolean b = m.find();
            if (StringUtils.isWhitespace(_schedulername) == true || b == true) {
                result = "error";
                message = "Please fill correct details!!!";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }

            int igatewayType = Integer.parseInt(_gatewayType);
            int ischedulerStatus = Integer.parseInt(_schedulerStatus);
            int igatewayPreference = Integer.parseInt(_gatewayPreference);
            int igatewayStatus = Integer.parseInt(_gatewayStatus);
            int ioperatorRoles = Integer.parseInt(_operatorRoles);
            int ischedulerDuration = Integer.parseInt(_schedulerDuration);
            int ireportType = Integer.parseInt(_reportType);
            
//            String[] emailList = _email.split(",");

            String[] emailList = null;

            if (_email != null) {
                String[] arr = _email.split(",");

                List<String> list = new ArrayList<String>();
                for (int i = 0; i < arr.length; i++) {
                    if (arr[i] != null && arr[i].isEmpty() != true) {
                        list.add(arr[i]);
                    }
                }

                emailList = list.toArray(new String[list.size()]);
                for (int i = 0; i < emailList.length; i++) {
                    if (UtilityFunctions.isValidEmail(emailList[i]) == false) {
                        result = "error";
                        message = "Invalid email id!!!";
                        try {
                            json.put("_result", result);
                            json.put("_message", message);
                        } catch (Exception e) {
                           log.error("exception caught :: ",e);
                        }
                        out.print(json);
                        out.flush();
                        return;
                    }
                }
            }

            String[] arr = _email.split(",");
            List<String> list = new ArrayList<String>();

            for (int i = 0; i < arr.length; i++) {
                if (arr[i] != null && arr[i].isEmpty() != true) {
                    list.add(arr[i]);
                }
            }
            //    String[] emailList = list.toArray(new String[list.size()]);
            
            
             OperatorsManagement oMngt = new OperatorsManagement();
             Roles rolesChosen = oMngt.getRoleById(channel.getChannelid(),ioperatorRoles);
             oMngt=null;
                                            
            
            SchedulerSettingEntry shSettingObj = new SchedulerSettingEntry();
            shSettingObj.setCreatedOn(new Date());
            shSettingObj.setDuration(ischedulerDuration);
            shSettingObj.setGatewayStatus(igatewayStatus);
            shSettingObj.setPrefrence(igatewayPreference);
            shSettingObj.setRolename("" + rolesChosen.getRoleid());
            shSettingObj.setStatus(ischedulerStatus);
            shSettingObj.setType(igatewayType);
            shSettingObj.setSendTo(emailList);
            shSettingObj.setReportType(ireportType);
            
           
            SchedulerManagement scheduleObj = new SchedulerManagement();
            AuditManagement audit = new AuditManagement();

            Schedulersettings schd = scheduleObj.getSetting(sessionId, channel.getChannelid(), _schedulername);
            
            
            if (schd != null) {
                result = "error";
                message = "Duplicate Scheduler Name, please enter unique name!!!";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }

            int retValue = scheduleObj.AddSchedulerSetting(sessionId, channel.getChannelid(), _schedulername, ischedulerStatus,
                    shSettingObj);
            
            log.debug("AddSchedulerSetting ::" + retValue);

            String resultString = "ERROR";
            String strschedulerStatus = "In-Active";
            if (ischedulerStatus == 1) {
                strschedulerStatus = "Active";
            }

            String strgatewayStatus = "";
            if (igatewayStatus == 99) {
                strgatewayStatus = "ALL";
            }
            if (igatewayStatus == 0) {
                strgatewayStatus = "SENT";
            }
            if (igatewayStatus == 2) {
                strgatewayStatus = "PENDING";
            }
            if (igatewayStatus == -1) {
                strgatewayStatus = "FAILED";
            }
            if (igatewayStatus == -5) {
                strgatewayStatus = "BLOCKED";
            }

            String strgatewayType = "";
            if (igatewayType == 0) {
                strgatewayType = "ALL";
            } else if (igatewayType == 1) {
                strgatewayType = "SMS";
            } else if (igatewayType == 2) {
                strgatewayType = "USSD";
            } else if (igatewayType == 3) {
                strgatewayType = "VOICE";
            } else if (igatewayType == 4) {
                strgatewayType = "EMAIL";
            } else if (igatewayType == 5) {
                strgatewayType = "FAX";
            } else if (igatewayType == 6) {
                strgatewayType = "FACEBOOK";
            } else if (igatewayType == 7) {
                strgatewayType = "LINKEDIN";
            } else if (igatewayType == 8) {
                strgatewayType = "TWITTER";
            } else if (igatewayType == 18) {
                strgatewayType = "ANDROIDPUSH";
            } else if (igatewayType == 19) {
                strgatewayType = "IPHONEPUSH";
            }

            String strreporttype = "";
            if (ireportType == 99) {
                strreporttype = "PDF&CSV";
            } else if (ireportType == 0) {
                strreporttype = "PDF";
            } else if (ireportType == 1) {
                strreporttype = "CSV";
            }

            if (retValue == 0) {
                result = "success";
                result = "new schedule added successfully!!!";

                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add Scheduler", resultString, retValue,
                        "Scheduler Management",
                        "", 
                        "SchedulerName=" + _schedulername + "SchedulerStatus =" + strschedulerStatus + "Duration =" + ischedulerDuration
                        + "Gateway Type=" + strgatewayType + "Gateway Status=" + strgatewayStatus + "Gateway Preference =" + igatewayPreference
                        + "Role=" + rolesChosen.getName() + "Report Type=" + strreporttype,
                        itemtype,
                        "-");
            } else {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add Scheduler", "ERROR", retValue, "Scheduler Management",
                        "", "Failed To add Scheduler Setting ...!", itemtype,
                        "-");

                result = "error";
                message = "Scheduler Setting Addition Failed!!!";
            }

            try {
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
            } catch (Exception e) {
               log.error("exception caught :: ",e);
            }

        } catch (Exception ex) {
           log.error("exception caught :: ",ex);
            try {
                json.put("_result", "error");
                json.put("_message", "Exception:" + ex.getMessage());
            } catch (Exception e) {
               log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            
        }
        log.info("is ended :: ");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
