/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.webseal;

import com.mollatech.axiom.connector.mobiletrust.Location;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.ApWebseal;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Monitorsettings;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.SettingsUtil;
import com.mollatech.axiom.nucleus.db.connector.management.MonitorSettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SchedulerManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.WebsealManagement;
import com.mollatech.axiom.nucleus.settings.NucleusMonitorSettings;
import com.mollatech.axiom.nucleus.settings.WebSealSetting;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Hashtable;
import javax.naming.NamingEnumeration;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.bouncycastle.util.encoders.Base64;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author Ash
 */
public class GetWebSeal extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(GetWebSeal.class.getName());

    private static String g_channelID = null;
    static final Logger logger = Logger.getLogger(GetWebSeal.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        OutputStream out = response.getOutputStream();
        WebsealManagement web = new WebsealManagement();
        Location loc = null;
        String url = null;
        String ip = null;
        String[] browser = null;
        String[] os = null;
        String domain = null;
        String browserName = "NA";
        String browserVersion = "NA";
        String osName = "NA";
        String osVersion ="NA";
        String issuerName = "NA";
        
        WebSealSetting settingsObj = (WebSealSetting) new SettingsManagement().getSetting(GetChannelID(),SettingsManagement.WEBSEAL_SETTINGS, SettingsManagement.PREFERENCE_ONE);
        JSONObject dnsJSON = new JSONObject();
        try {
            disableSslVerification();
            url = request.getHeader("Referer");
            ip = request.getRemoteAddr();
            String userAgent = request.getHeader("User-Agent");
            loc = GetLocation.getLocationByIp(ip);
            browser = BrowserOSUtil.getBrowserDetail(userAgent);
            
            
            if(browser != null){
                browserName = browser[0];
                browserVersion = browser[1];
            }
            os = BrowserOSUtil.getOsDetail(userAgent);
            if(os != null){
                osName = os[0];
                osVersion = os[1];
            }
           // url = "https://www.facebook.com/";
            String urlUniqueId = request.getParameter("urlUniqueId");
            log.debug("urlUniqueId ::" +urlUniqueId);
            
            Monitorsettings monitorsettings = new MonitorSettingsManagement().getMonitorSettingByUniqueId(urlUniqueId);
            if (monitorsettings == null) {
                ApWebseal seal = new ApWebseal();
                        seal.setChannelid(g_channelID);seal.setIp(ip);seal.setLatitude(loc.lattitude);seal.setLongitude(loc.longitude);seal.setBrowsername(browserName);
                        seal.setCountry(loc.country);seal.setStateL(loc.state);seal.setCity(loc.city);seal.setArea(loc.area);seal.setDns(dnsJSON.toString());
                        seal.setBrowserversion(browserVersion);seal.setZipcode(loc.zipcode);seal.setOsname(osName);seal.setOsversion(osVersion);
                        seal.setReferalUrl(url);seal.setRequestedon(new Date());seal.setStatus(WebsealManagement.WEBSEALCERTNOTVALID);
                        web.addWebSealDetails(g_channelID,seal);
                logger.debug("after get monitorsettings object");
                throw new Exception();
            }
            int monitorid = monitorsettings.getMonitorId();
            NucleusMonitorSettings nms = null;
            byte[] obj = monitorsettings.getMonitorSettingEntry();
            byte[] f = AxiomProtect.AccessDataBytes(obj);
            ByteArrayInputStream bais = new ByteArrayInputStream(f);
            Object object = SchedulerManagement.deserializeFromObject(bais);
            nms = (NucleusMonitorSettings) object;
            String webURL = nms.getWebadd();
            URL originalURL = new URL(webURL);
            
            URL referalURL = new URL(url);
            if (!originalURL.getProtocol().equals(referalURL.getProtocol())) {
                web.addwebSeal(g_channelID, ip, loc.lattitude, loc.longitude, url, loc.area, loc.country, loc.state, loc.city, loc.zipcode, 
                        domain, osName,osVersion, browserName,browserVersion,  WebsealManagement.HTTPPROTOCOLFAILURE, new Date(),monitorid);
                logger.debug("after protocol not match");
                throw new Exception();
            }
            if (!originalURL.getAuthority().equals(referalURL.getAuthority())) {
                web.addwebSeal(g_channelID, ip, loc.lattitude, loc.longitude, url, loc.area, loc.country, loc.state, loc.city, loc.zipcode, 
                        domain, osName,osVersion, browserName,browserVersion,  WebsealManagement.URLNOTMATCH, new Date(), monitorid);
                logger.debug("after URL not match");
                throw new Exception();
            }

            {
                JSONObject json = new JSONObject(settingsObj.dns);
                Hashtable<String, String> env = new Hashtable<String, String>();
                env.put("java.naming.factory.initial", "com.sun.jndi.dns.DnsContextFactory");
                
                JSONObject oriDnsJSON = new JSONObject(nms.getWebDNS());
                boolean dnsFlag = false;
                boolean dnsCheck = false;
                for (int i = 0; i < 3; i++) {
                    if(dnsCheck){
                        
                        web.addwebSeal(g_channelID, ip, loc.lattitude, loc.longitude, url, loc.area, loc.country, loc.state, loc.city, loc.zipcode,dnsJSON.toString(),
                                        osName,osVersion, browserName,browserVersion,  WebsealManagement.DNSNOTFOUND, new Date(), monitorid);  
                        throw new Exception();
                    }
                    String dns = (String) json.get("_dnsForWeb" + (i + 1));
                    try {
                        if (!dns.equals("")) {
                            dnsFlag = true;
                            env.put("java.naming.provider.url", "dns://" + dns);
                            DirContext ictx = null;
                            ictx = new InitialDirContext(env);
                            Attributes attrs1 = ictx.getAttributes(referalURL.getAuthority(), new String[]{"A"});
                            NamingEnumeration enumeration = attrs1.get("a").getAll();
                            while (enumeration.hasMore()) {
                                String ipFromDNS = enumeration.next().toString() + ",";
                                if (!oriDnsJSON.get("_dnsForWeb" + (i + 1)).toString().contains(ipFromDNS)) {
                                    dnsCheck = true;
                                    dnsJSON.put("_dnsForWeb"+ (i + 1) , ipFromDNS);                                  
                                }
                            }                            
                        }
                        logger.debug("after dns not found");
                    } catch (Exception e) {
                        if (!oriDnsJSON.get("_dnsForWeb" + (i + 1)).toString().equals(referalURL.getAuthority())) {
                            dnsCheck = true;
                            dnsJSON.put("_dnsForWeb" + (i + 1), referalURL.getAuthority());
                            logger.error("Exception at GetWebSeal ", e);
                        }
                    }                    
                }
//                if (dnsFlag == false) {
//                    try {
//                        env.put("java.naming.provider.url", "dns://" + "8.8.8.8");
//                        DirContext ictx = null;
//                        ictx = new InitialDirContext(env);
//                        Attributes attrs1 = ictx.getAttributes(referalURL.getAuthority(), new String[]{"A"});
//                        NamingEnumeration enumeration = attrs1.get("a").getAll();
//                        while (enumeration.hasMore()) {
//                            String ipFromDNS = enumeration.next().toString() + ",";
//                            if (oriDnsJSON.get("_dnsForWeb1").toString().contains(ipFromDNS)) {
//                                dnsCheck = true;
//                                dnsJSON.put("_dnsForWeb1" , ipFromDNS);
//                                
//                            }
//                        }
//                    } catch (Exception ex) {
//                        if (oriDnsJSON.get("_dnsForWeb1").toString().equals(referalURL.getAuthority())) {
//                            dnsCheck = true;
//                            dnsJSON.put("_dnsForWeb1" , referalURL.getAuthority());
//                           
//                        }
//                    }
//                }
//                if(dnsCheck){                        
//                        web.addwebSeal(g_channelID, ip, loc.lattitude, loc.longitude, url, loc.area, loc.country, loc.state, loc.city, loc.zipcode,dnsJSON.toString(),
//                                        osName,osVersion, browserName,browserVersion,  WebsealManagement.DNSNAMENOTFOUND, new Date());  
//                        throw new Exception();
//                }
            }
            if (referalURL.getProtocol().equals("https")) {
                Certificate cert_To_Upload = null;
                
                HttpsURLConnection connection = (HttpsURLConnection) referalURL.openConnection();
                connection.connect();
                Certificate[] certs = connection.getServerCertificates();

                for (int i = 0; i < certs.length; i++) {
                    boolean certFlag = false;
                    cert_To_Upload = certs[i];
                    X509Certificate t = (X509Certificate) cert_To_Upload;
                    issuerName = getIssuer(t.getIssuerDN().getName().toLowerCase());
                    
                    if (t.getNotAfter().before(new Date())) {
                        ApWebseal seal = new ApWebseal();
                        seal.setChannelid(g_channelID);seal.setIp(ip);seal.setLatitude(loc.lattitude);seal.setLongitude(loc.longitude);seal.setBrowsername(browserName);
                        seal.setCountry(loc.country);seal.setStateL(loc.state);seal.setCity(loc.city);seal.setArea(loc.area);seal.setDns(dnsJSON.toString());
                        seal.setBrowserversion(browserVersion);seal.setZipcode(loc.zipcode);seal.setOsname(osName);seal.setOsversion(osVersion);seal.setCertificateIssuer(issuerName);seal.setCertificateValid(WebsealManagement.CERTIINVALID);
                        seal.setReferalUrl(url);seal.setRequestedon(new Date());seal.setStatus(WebsealManagement.SSLCERTEXPIRED);seal.setMonitorId(monitorid);
                        web.addWebSealDetails(g_channelID,seal);
                        logger.debug("after certificate expired");
                        throw new Exception();
                    }

                    Certificate[] originalCerts = (Certificate[]) SettingsUtil.deserializeFromObject(new ByteArrayInputStream(nms.getWebCert()));
                    for (int j = 0; j < originalCerts.length; j++) {
                        if (t == originalCerts[j]) {                          
                            certFlag = true;
                        }
                    }
                    if (certFlag == false) {
                         ApWebseal seal = new ApWebseal();
                        seal.setChannelid(g_channelID);seal.setIp(ip);seal.setLatitude(loc.lattitude);seal.setLongitude(loc.longitude);seal.setBrowsername(browserName);
                        seal.setCountry(loc.country);seal.setStateL(loc.state);seal.setCity(loc.city);seal.setArea(loc.area);seal.setZipcode(loc.zipcode);seal.setMonitorId(monitorid);
                        seal.setBrowserversion(browserVersion);seal.setOsname(osName);seal.setOsversion(osVersion);seal.setCertificateIssuer(issuerName);seal.setCertificateValid(WebsealManagement.CERTIVALID);
                        seal.setReferalUrl(url);seal.setRequestedon(new Date());seal.setStatus(WebsealManagement.SSLCERTDIFFERENT);seal.setCertificateDiff(WebsealManagement.CERTIDIFFN);
                        web.addWebSealDetails(g_channelID,seal);
                        logger.debug("after find certificate wrong issuer");
                        throw new Exception();
                    }
                }
                connection.disconnect();
            }
            response.setContentType("image/jpeg");
            byte[] byteArray = null;
            byteArray = Base64.decode(settingsObj.successImage);
            OutputStream sos = response.getOutputStream();
            sos.write(byteArray);
            sos.close();
             ApWebseal seal = new ApWebseal();
                        seal.setChannelid(g_channelID);seal.setIp(ip);seal.setLatitude(loc.lattitude);seal.setLongitude(loc.longitude);seal.setBrowsername(browserName);
                        seal.setCountry(loc.country);seal.setStateL(loc.state);seal.setCity(loc.city);seal.setArea(loc.area);seal.setMonitorId(monitorid);
                        seal.setBrowserversion(browserVersion);seal.setOsname(osName);seal.setOsversion(osVersion);seal.setCertificateIssuer(issuerName);seal.setCertificateValid(WebsealManagement.CERTIVALID);
                        seal.setReferalUrl(url);seal.setZipcode(loc.zipcode);seal.setRequestedon(new Date());seal.setStatus(WebsealManagement.SUCCESS);seal.setCertificateDiff(WebsealManagement.CERTIDIFFY);
                        web.addWebSealDetails(g_channelID,seal);
        } catch (Exception e) {
            response.setContentType("image/jpeg");
            byte[] byteArray = null;
            if (settingsObj.hackerResult.equals("Error Image")) {
                byteArray = Base64.decode(settingsObj.errorImage);
            } else {
                byteArray = Base64.decode(settingsObj.successImage);
            }
            OutputStream sos = response.getOutputStream();
            sos.write(byteArray);
            sos.close();  
            logger.error("Exception at GetWebSeal ", e);
        }
        log.info("is ended :: ");
    }
    
        
     private String GetChannelID() {

        if (g_channelID == null) {

            SessionFactoryUtil suChannel = null;// new SessionFactoryUtil(SessionFactoryUtil.channels);
            Session sChannel = null; //suChannel.openSession();

            try {
                suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
                sChannel = suChannel.openSession();

                String _channelName = this.getServletContext().getContextPath();
                _channelName = _channelName.replaceAll("/", "");

                _channelName = _channelName.replaceAll("/", "");

                if (_channelName.compareTo("core") == 0) {
                    _channelName = "face";
                }

                ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
                Channels channel = cUtil.getChannel(_channelName);
                g_channelID = channel.getChannelid();
                sChannel.close();
                suChannel.close();
            } catch (Exception e) {
                sChannel.close();
                suChannel.close();
                log.error("Exception caught :: ",e);
            }
        }

        return g_channelID;
    }

    private void disableSslVerification() {
        try {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
            };

            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (Exception e) {
            log.error("exception caught :: ",e);
        }
        
    }
    
    private String getIssuer(String issuer){
        try{    
        String subsString = issuer.substring(issuer.indexOf("cn"));
                String[] name = (subsString.split(",")[0]).split("=");
                String issuerName =  name[1];
                return issuerName;
        }catch(Exception e){   
            log.error("exception caught :: ",e);
        }
        return null;
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
