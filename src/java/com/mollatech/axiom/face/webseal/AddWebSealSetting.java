/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.webseal;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.WebSealSetting;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author Ash
 */
public class AddWebSealSetting extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AddWebSealSetting.class.getName());

    final String itemtype = "SETTINGS";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel ::" + channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId ::" + sessionId);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin ::" + remoteaccesslogin);
        String OperatorID = operatorS.getOperatorid();
        log.debug("OperatorID::" + OperatorID);
        String _channelId = channel.getChannelid();
        log.debug("_channelId ::" + _channelId);
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Web Seal Settings Update Successful!!!";
        
 
//        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.GATEWAY_SMS) != 0) {
//            result = "error";
//            message = "This feature is not available in this license!!!";
//
//            try {
//                json.put("_result", result);
//                json.put("_message", message);
//            } catch (Exception e) {
//                log.error("Exception caught :: ",e);
//            }
//
//            out.print(json);
//            out.flush();
//            return;
//        }
        try {
            SettingsManagement sMngmt = new SettingsManagement();
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.WEBSEAL_SETTINGS, SettingsManagement.PREFERENCE_ONE);

            WebSealSetting setting = null;
            boolean bAddSetting = false;

            if (settingsObj == null) {
                setting = new WebSealSetting();
                bAddSetting = true;
                if (request.getSession().getAttribute("_sucessImage") == null) {
                    json.put("_result", "error");
                    json.put("_message", "Please Upload Success Image File");
                    return;
                }
                if (request.getSession().getAttribute("_errorImage") == null) {
                    json.put("_result", "error");
                    json.put("_message", "Please Upload Error Image File");
                    return;
                }
                setting.successImage = new String(Base64.encode(FileUtils.readFileToByteArray(new File(request.getSession().getAttribute("_sucessImage").toString()))));
                setting.errorImage = new String(Base64.encode(FileUtils.readFileToByteArray(new File(request.getSession().getAttribute("_errorImage").toString()))));
            } else {
                setting = (WebSealSetting) settingsObj;
                if (request.getSession().getAttribute("_sucessImage") != null) {
                    setting.successImage = new String(Base64.encode(FileUtils.readFileToByteArray(new File(request.getSession().getAttribute("_sucessImage").toString()))));
                }
                if (request.getSession().getAttribute("_errorImage") != null) {
                    setting.errorImage = new String(Base64.encode(FileUtils.readFileToByteArray(new File(request.getSession().getAttribute("_errorImage").toString()))));
                }

            }
            JSONObject dns = new JSONObject();
            dns.put("_dnsForWeb1", request.getParameter("_dnsForWeb1"));
            dns.put("_dnsForWeb2", request.getParameter("_dnsForWeb2"));
            dns.put("_dnsForWeb3", request.getParameter("_dnsForWeb3"));
            setting.dns = dns.toString();
            setting.hackerResult = request.getParameter("_hackerResult");
            setting.webSealURL = request.getParameter("_webSealURL");
            int retValue = -1;
            AuditManagement audit = new AuditManagement();
            if (bAddSetting == true) {
                retValue = sMngmt.addSetting(sessionId, _channelId, SettingsManagement.WEBSEAL_SETTINGS, SettingsManagement.PREFERENCE_ONE, setting);
                log.debug("addSetting ::" + retValue);
                String resultString = "ERROR";
                if (retValue == 0) {
                    resultString = "SUCCESS";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add SMS Gateway Setting", resultString, retValue, "Setting Management",
                            "", "errorImage = " + setting.errorImage + "hackerResult=" + setting.hackerResult
                            + "successImage=" + setting.successImage + "webSealURL=" + setting.webSealURL + "DNS=" + setting.dns.toString(),
                            itemtype, _channelId);
                } else if (retValue != 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add Web Seal Setting", resultString, retValue, "Setting Management",
                            "", "Failed To Add Web Seal Setting", itemtype,
                            _channelId);
                }
            } else {
                WebSealSetting oldmobileObj = (WebSealSetting) sMngmt.getSetting(sessionId, _channelId, SettingsManagement.WEBSEAL_SETTINGS, SettingsManagement.PREFERENCE_ONE);
                retValue = sMngmt.changeSetting(sessionId, _channelId, SettingsManagement.WEBSEAL_SETTINGS, SettingsManagement.PREFERENCE_ONE, settingsObj, setting);
                log.debug("changeSetting ::" + retValue);
                
                
                String resultString = "ERROR";

                if (retValue == 0) {
                    resultString = "SUCCESS";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change Web Seal Setting", resultString, retValue, "Setting Management",
                            "errorImage = " + oldmobileObj.errorImage + "hackerResult=" + oldmobileObj.hackerResult
                            + "successImage=" + oldmobileObj.successImage + "webSealURL=" + oldmobileObj.webSealURL + "DNS=" + oldmobileObj.dns.toString(),
                            "errorImage = " + setting.errorImage + "hackerResult=" + setting.hackerResult
                            + "successImage=" + setting.successImage + "webSealURL=" + setting.webSealURL + "DNS=" + setting.dns.toString(),
                            itemtype, _channelId);
                } else if (retValue != 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change Web Seal Setting", resultString, retValue, "Setting Management",
                            "errorImage = " + oldmobileObj.errorImage + "hackerResult=" + oldmobileObj.hackerResult
                            + "successImage=" + oldmobileObj.successImage + "webSealURL=" + oldmobileObj.webSealURL + "DNS=" + oldmobileObj.dns.toString(),
                            "Failed To Change Web Seal Setting", itemtype, _channelId);
                }
            }
            if (retValue != 0) {
                result = "error";
                message = "SMS Gateway Settings Update Failed!!!";
                return;
            } else {
                json.put("_result", result);
                json.put("_message", message);
                return;
            }
        } catch (Exception e) {
            
            log.error("exception caught :: ",e);

        } finally {
            out.print(json);
            out.flush();
            out.close();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
