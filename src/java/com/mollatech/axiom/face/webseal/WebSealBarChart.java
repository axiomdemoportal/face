/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.webseal;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.face.common.bar;
import com.mollatech.axiom.face.common.donut;
import com.mollatech.axiom.nucleus.db.ApWebseal;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.WebsealManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mohanish
 */
public class WebSealBarChart extends HttpServlet {
    
        static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(WebSealBarChart.class.getName());


    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        int webSealCertNotValid = 0;
        int httpprotocolfailure = 0;
        int urlfailure = 0;
        int certExpired = 0;
        int dnsFailure = 0;
        int certDiff = 0;
        int success = 0;
        PrintWriter out = response.getWriter();
        try {
            response.setContentType("application/json");

            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel ::" + channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId ::" + sessionId);
            String channelID = channel.getChannelid();
            log.debug("channelID ::" + channelID);
            String _startdate = request.getParameter("_startDate");
            log.debug("_startdate ::" + _startdate);
            String _enddate = request.getParameter("_endDate");
            log.debug("_enddate ::" + _enddate);
            String _monitorname = request.getParameter("_monitername");
            log.debug("_monitorname ::" + _monitorname);
            DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            Date startDate = null;
            
            
            if (_startdate != null && !_startdate.isEmpty()) {
                startDate = (Date) formatter.parse(_startdate);    
            }
            Date endDate = null;
            if (_enddate != null && !_enddate.isEmpty()) {
                endDate = (Date) formatter.parse(_enddate);                
            }
            ArrayList<bar> sample = new ArrayList<bar>();
            WebsealManagement tx = new WebsealManagement();
            ApWebseal[] txd = tx.getCount(channelID, Integer.parseInt(_monitorname), startDate, endDate);
            if (txd != null) {
                for (int i = 0; i < txd.length; i++) {
                    if (txd[i].getStatus() == WebsealManagement.SUCCESS) {
                        success++;
                    } else if (txd[i].getStatus() == WebsealManagement.WEBSEALCERTNOTVALID) {
                        webSealCertNotValid++;
                    } else if (txd[i].getStatus() == WebsealManagement.HTTPPROTOCOLFAILURE) {
                        httpprotocolfailure++;
                    } else if (txd[i].getStatus() == WebsealManagement.URLNOTMATCH) {
                        urlfailure++;
                    }else if (txd[i].getStatus() == WebsealManagement.DNSNOTFOUND) {
                        dnsFailure++;
                    } else if (txd[i].getStatus() == WebsealManagement.SSLCERTDIFFERENT) {
                        certDiff++;
                    } else if (txd[i].getStatus() == WebsealManagement.SSLCERTEXPIRED) {
                        certExpired++;
                    }
                }
            }
            sample.add(new bar(success, "Success"));
            sample.add(new bar(webSealCertNotValid, "Cert failure"));
            sample.add(new bar(httpprotocolfailure, "Protocol failure"));
            sample.add(new bar(urlfailure, "URL failure"));
            sample.add(new bar(dnsFailure, "DNS failure"));
            sample.add(new bar(certDiff, "Wrong Issuer"));
            sample.add(new bar(certExpired, "Certi Expired"));
            Gson gson = new Gson();

            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<donut>>() {
            }.getType());

            JsonArray jsonArray = element.getAsJsonArray();
            out.print(jsonArray);

        } catch (Exception e) {
            log.error("exception caught :: ",e);
        } finally {
            out.close();
        }
        
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
