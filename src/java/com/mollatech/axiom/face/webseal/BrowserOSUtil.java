/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.webseal;

/**
 *
 * @author mohanish
 */
public class BrowserOSUtil {

    public static String[] getBrowserDetail(String userAgent) {

        if (userAgent != null || userAgent != "") {
            try {
                if (userAgent.indexOf("MSIE") != -1) {
                    String subsString = userAgent.substring(userAgent.indexOf("MSIE"));
                    String info[] = (subsString.split(";")[0]).split(" ");
               
                    return info;

                } else if (userAgent.indexOf("Chrome") != -1) {
                    String subsString = userAgent.substring(userAgent.indexOf("Chrome"));
                    String info[] = (subsString.split(" ")[0]).split("/");

                    return info;
                
                } else if (userAgent.indexOf("Safari") != -1) {
                    String subsString = userAgent.substring(userAgent.indexOf("Safari"));
                    String info[] = (subsString.split(" ")[0]).split("/");

                    return info;

                } else if (userAgent.indexOf("Firefox") != -1) {
                    String subsString = userAgent.substring(userAgent.indexOf("Firefox"));
                    String info[] = (subsString.split(" ")[0]).split("/");

                    return info;

                } else if (userAgent.indexOf("Opera") != -1) {
                    String subsString = userAgent.substring(userAgent.indexOf("Opera"));
                    String info[] = (subsString.split(" ")[0]).split("/");

                    return info;

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String[] getOsDetail(String userAgent) {
        if (userAgent != null) {
            try{
            if (userAgent.indexOf("Linux") != -1) {
                String subsString = userAgent.substring(userAgent.indexOf("Linux"));
                String ex[] = subsString.split(" ");
                return ex;
            } else if (userAgent.indexOf("Windows") != -1) {
                String subsString = userAgent.substring(userAgent.indexOf("Windows"));
               String ex[] =  subsString.split(" ");
               return ex;
            } else if (userAgent.indexOf("Mac") != -1) {
                String subsString = userAgent.substring(userAgent.indexOf("Mac"));
               String ex[] =  subsString.split(";");
               return ex;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        }
        return null;
    }
}
