/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.webseal;

import com.mollatech.axiom.connector.mobiletrust.Location;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
public class GetLocation {

    public GetLocation() {
    }

    public static Location getLocationByIp(String ipAddress) {
        
        try {
            
            String url = "http://ipinfo.io/" + ipAddress + "/json";
           
            String city = null;            
            String state = null;
            String country = null;
            String zipcode = null;            
            String[] response = sendReq(url);
            if (response == null && response.length <= 0) {
                return null;
            }
            if (response[1].startsWith("application/json") == true) {

                JSONObject jsonResponse = new JSONObject(response[0]);

                city = jsonResponse.getString("city");
                state = jsonResponse.getString("region");
                country = jsonResponse.getString("country");
                zipcode = jsonResponse.getString("postal");
                Location loc = new Location();

                loc.city = city;
                loc.state = state;
                loc.country = country;
                loc.zipcode = zipcode;
                String location = jsonResponse.getString("loc");
                String[] locationList = location.split(",");
                loc.lattitude = locationList[0];
                loc.longitude = locationList[1];
                loc.short_name = country;
                return loc;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private static String[] sendReq(String ip) {
        String text = "";
        String[] returnStrs = null;
        StringBuilder sb = new StringBuilder();
        try {
            URL track = new URL(ip);
            URLConnection yc = track.openConnection();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            yc.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                sb.append(inputLine + "\n");
            }
            text = sb.toString();
            //System.out.println("text " + text);
            String contentType = yc.getHeaderField("Content-Type");
            //System.out.println("contentType " + contentType);
            returnStrs = new String[2];
            returnStrs[0] = text;
            returnStrs[1] = contentType;
            return returnStrs;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
