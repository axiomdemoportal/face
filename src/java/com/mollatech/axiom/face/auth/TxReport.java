/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.auth;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Txdetails;
import com.mollatech.axiom.nucleus.db.connector.management.TxManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author ashish
 */
public class TxReport extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(TxReport.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        log.info("is started :: ");
        String uid = request.getParameter("_uidtx");
        String type = request.getParameter("_typeTx");
        String status = request.getParameter("_statustx");
        String txid = request.getParameter("_txid");
        String startdate = request.getParameter("startdatetx");
        String enddate = request.getParameter("enddatetx");
        Txdetails txdetails = new Txdetails();
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String _channelId = channel.getChannelid();
        Date dstartdate = null, denddate = null;

        log.debug("channel::" + channel.getName());
        log.debug("_channelId::" + _channelId);
        log.debug("sessionId::"+sessionId);
        log.debug("enddate::"+enddate);
        log.debug("startdate::"+startdate);
        log.debug("txid"+txid);
        log.debug("status::"+status);
        log.debug("type::"+type);
        log.debug("uid::"+uid);

            try {
            if (uid.equals("") && type.equals("") && status.equals("") && txid.equals("") && startdate.equals("") && enddate.equals("")) {
                json.put("_result", "error");
                json.put("_message", "One Field is Mandetory");
                return;
            }
            txdetails.setType(-10);
            txdetails.setStatus(-10);

            if (!uid.equals("")) {
                txdetails.setUserid(uid);
            }
            if (!type.equals("")) {
                txdetails.setType(Integer.parseInt(type));

            }
            if (!status.equals("")) {
                txdetails.setStatus(Integer.parseInt(status));
            }
            if (!txid.equals("")) {
                txdetails.setTransactionId(txid);
            }
            if (!startdate.equals("") && !enddate.equals("")) {
                SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
                dstartdate = format.parse(startdate);
                denddate = format.parse(enddate);
            } else {
                if ((startdate.equals("") && !enddate.equals("")) || (!startdate.equals("") && enddate.equals(""))) {
                    json.put("_result", "error");
                    json.put("_message", "Enter Starting And Ending Date Properly ");
                    return;
                }
            }

            txdetails.setChannelid(_channelId);
            Txdetails[] txdetailses = new TxManagement().getTxDetails(txdetails, sessionId, dstartdate, denddate);
            if (txdetailses == null) {
                json.put("_result", "error");
                json.put("_message", "No Record Found");
                return;
            } else {
                request.getSession().setAttribute("txdetailses", txdetailses);
                json.put("_result", "success");
                json.put("_message", "Record Found");
                return;
            }
        } catch (Exception ex) {
            log.error("exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
            log.info("is ended :: ");
            return;
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
