/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.auth;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Twowayauth;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TwowayauthManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author ashish
 */
public class EditAuth extends HttpServlet {

    final String itemtype = "TWOWAY-AUTHENTICATION";

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(EditAuth.class.getName());

    private final int SMS = 1;
    private final int Callback = 2;
    private final int Missed = 3;
    private final int Push = 4;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        log.info("is started :: ");
        String type = request.getParameter("type");
        int _type = Integer.parseInt(type);
        String status = request.getParameter("status");
        String userid = request.getParameter("userid");
        int _status = Integer.parseInt(status);
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String _channelId = channel.getChannelid();
            
            log.debug("channel::" + channel.getName());
            log.debug("operatorS::"+ operatorS.getOperatorid());
            log.debug("_channelId::"+ _channelId);
            log.debug("sessionId::"+ sessionId);
            log.debug("_type::"+ _type);
            log.debug("_status::"+ _status);
            log.debug("userid::"+ userid);
                
            
        String messageStringInAudit = "";
        
        TwowayauthManagement tm = new TwowayauthManagement();
        try {

            Twowayauth twowayauth = tm.getAuthDetailsByUserId(_channelId, userid, sessionId);
            if (twowayauth == null) {
                twowayauth = new Twowayauth();
                twowayauth.setAttempts(0);
                json.put("SMS", "Suspended");
                json.put("Callback", "Suspended");
                json.put("Missed", "Suspended");
                json.put("Push", "Suspended");
                if (_type == SMS) {
                    if (_status == 1) {
                        json.put("SMS", "Active");
                        messageStringInAudit = "SMS as Active";
                    } else {
                        messageStringInAudit = "SMS as Suspended";
                    }
                } else if (_type == Callback) {
                    if (_status == 1) {
                        json.put("Callback", "Active");
                        messageStringInAudit = "Voice Callback as Active";
                    } else {
                        messageStringInAudit = "Voice Callback as Suspended";
                    }
                } else if (_type == Missed) {
                    if (_status == 1) {
                        json.put("Missed", "Active");
                        messageStringInAudit = "Missed Voice Call as Active";
                    } else {
                        messageStringInAudit = "Missed Voice Call as Suspended";
                    }
                } else if (_type == Push) {
                    if (_status == 1) {
                        json.put("Push", "Active");
                        messageStringInAudit = "Push Notification as Active";
                    } else {
                        messageStringInAudit = "Push Notification as Suspended";
                    }
                }
                twowayauth.setChannelid(_channelId);
                twowayauth.setCreationdatetime(new Date());
                twowayauth.setLastaccessdatetime(new Date());
                twowayauth.setType(json.toString());
                twowayauth.setUserid(userid);
                int res = tm.addAuthDetails(twowayauth, sessionId);
                log.debug("EditAuth::addAuthDetails::"+ res);
                if (res == 0) {
                    json.put("_result", "success");
                    json.put("_message", "Update Successfully!!!");
                    String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
                    log.debug("EditAuth::RemoteAccessLogin::"+ remoteaccesslogin);

                    String resultString = "SUCCESS";
                    AuditManagement audit = new AuditManagement();
                    audit.AddAuditTrail(sessionId, channel.getChannelid(),
                            operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(),
                            remoteaccesslogin,
                            operatorS.getName(),
                            new Date(),
                            "Change Status",
                            resultString,
                            0,
                            "Two Way Authentication",
                            "",
                            messageStringInAudit,
                            itemtype,
                            userid);
                } else {
                    json.put("_result", "error");
                    json.put("_message", "Failed to update!!!");
                    String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
                    log.debug("EditAuth::RemoteAccessLogin::"+ remoteaccesslogin);

                    String resultString = "SUCCESS";
                    AuditManagement audit = new AuditManagement();
                    audit.AddAuditTrail(sessionId, channel.getChannelid(),
                            operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(),
                            remoteaccesslogin,
                            operatorS.getName(),
                            new Date(),
                            "Change Status",
                            resultString,
                            0,
                            "Two Way Authentication",
                            "",
                            messageStringInAudit,
                            itemtype,
                            userid);
                }
            } else {
                twowayauth.setAttempts(0);
                json = new JSONObject(twowayauth.getType());
                if (_type == SMS) {
                    if (_status == 1) {
                        json.put("SMS", "Active");
                        messageStringInAudit = "SMS as Active";
                    } else {
                        json.put("SMS", "Suspended");
                        messageStringInAudit = "SMS as Suspended";
                    }
                } else if (_type == Callback) {
                    if (_status == 1) {
                        json.put("Callback", "Active");
                        messageStringInAudit = "Voice Callback as Active";
                    } else {
                        json.put("Callback", "Suspended");
                        messageStringInAudit = "Voice Callback as Suspended";
                    }
                } else if (_type == Missed) {
                    if (_status == 1) {
                        json.put("Missed", "Active");
                        messageStringInAudit = "Missed Voice Call as Active";
                    } else {
                        json.put("Missed", "Suspended");
                        messageStringInAudit = "Missed Voice Call as Suspended";
                    }
                } else if (_type == Push) {
                    if (_status == 1) {
                        json.put("Push", "Active");
                        messageStringInAudit = "Push Notification as Active";
                    } else {
                        json.put("Push", "Suspended");
                        messageStringInAudit = "Push Notification as Suspended";
                    }
                }

                twowayauth.setChannelid(_channelId);
                twowayauth.setLastaccessdatetime(new Date());
                twowayauth.setType(json.toString());
                int res = tm.editAuthDetails(twowayauth, sessionId);
                log.debug("EditAuth::editAuthDetails::"+ res);
               
                if (res == 0) {
                    json.put("_result", "success");
                    json.put("_message", "Update Successfully!!!");
                    
                    String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
                     log.debug("EditAuth::RemoteAccessLogin::"+ remoteaccesslogin);

                    String resultString = "SUCCESS";
                    AuditManagement audit = new AuditManagement();
                    audit.AddAuditTrail(sessionId, channel.getChannelid(),
                            operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(),
                            remoteaccesslogin,
                            operatorS.getName(),
                            new Date(),
                            "Change Status",
                            resultString,
                            0,
                            "Two Way Authentication",
                            "",
                            messageStringInAudit,
                            itemtype,
                            userid);
                } else {
                    json.put("_result", "error");
                    json.put("_message", "Failed to update!!!");
                    
                    String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
                     log.debug("EditAuth::RemoteAccessLogin::"+ remoteaccesslogin);

                    String resultString = "ERROR";
                    AuditManagement audit = new AuditManagement();
                    audit.AddAuditTrail(sessionId, channel.getChannelid(),
                            operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(),
                            remoteaccesslogin,
                            operatorS.getName(),
                            new Date(),
                            "Change Status",
                            resultString,
                            res,
                            "Two Way Authentication",
                            "",
                            messageStringInAudit,
                            itemtype,
                            userid);
                                
                            
                    
                }
            }
        } catch (Exception ex) {
            log.error("exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
            log.info("is ended :: ");
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
