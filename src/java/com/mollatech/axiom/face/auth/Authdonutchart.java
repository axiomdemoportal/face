/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.auth;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.face.common.donut;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Txdetails;
import com.mollatech.axiom.nucleus.db.connector.management.TxManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mohanish
 */
public class Authdonutchart extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Authdonutchart.class.getName());
  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        log.info("is started :: ");
        int approved = 0;
        int cancelled = 0;
        int denied = 0;
        int responded = 0;
        int pending = 0;
        int expired = 0;
        PrintWriter out = response.getWriter();
        try {
            response.setContentType("application/json");
            
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            String channelID = channel.getChannelid();
            String _startdate = request.getParameter("_startDate");
            String _enddate = request.getParameter("_endDate");
            
            log.debug("channel::" + channel.getName());
            log.debug("channelID::"+ channelID);
            log.debug("sessionId::"+ sessionId);
            log.debug("_startdate::"+ _startdate);
            log.debug("_enddate::"+ _enddate);
            
            DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            Date startDate = null;
            if (_startdate != null && !_startdate.isEmpty()) {
                startDate = (Date) formatter.parse(_startdate);
               
                startDate.setHours(0);
                startDate.setMinutes(0);
                startDate.setSeconds(0);
            }
            Date endDate = null;
            if (_enddate != null && !_enddate.isEmpty()) {
                endDate = (Date) formatter.parse(_enddate);
                endDate.setHours(23);
                endDate.setMinutes(59);
                endDate.setSeconds(59);
            }
            ArrayList<donut> sample = new ArrayList<donut>();
            TxManagement tx = new TxManagement();
            Txdetails[] txd = tx.getTxStatusCount(sessionId, channelID, startDate, endDate);
            if(txd != null){
                for(int i=0; i<txd.length; i++){
                    if(txd[i].getStatus() == TxManagement.APPROVED ){
                        approved++;
                    }else if(txd[i].getStatus() == TxManagement.CANCELED){
                        cancelled++;
                    }else if(txd[i].getStatus() == TxManagement.DENIED){
                        denied++;
                    }else if(txd[i].getStatus() == TxManagement.EXPIRED){
                        expired++;
                    }else if(txd[i].getStatus() == TxManagement.PENDING){
                        pending++;
                    }else if(txd[i].getStatus() == TxManagement.RESPONDED){
                        responded++;
                    }
                }
            }
            
            sample.add(new donut(approved, "Approved"));
            sample.add(new donut(cancelled, "Cancelled"));
            sample.add(new donut(denied, "Denied"));
            sample.add(new donut(responded, "Responded"));
            sample.add(new donut(pending, "Pending"));
            sample.add(new donut(expired, "Expired"));
            
            Gson gson = new Gson();

            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<donut>>() {
            }.getType());

            JsonArray jsonArray = element.getAsJsonArray();
            out.print(jsonArray);
         
        }catch(Exception e){
            log.error("exception caught :: ",e);
        }finally{
            out.close();
        }
        
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
