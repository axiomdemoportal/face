package com.mollatech.axiom.face.auth;

import com.mollatech.axiom.nucleus.db.connector.management.CryptoManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

public class VerifySignature extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(VerifySignature.class.getName());

    public static final int RSA = 1;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        try {
            response.setContentType("application/json");
            JSONObject json = new JSONObject();
            PrintWriter out = response.getWriter();
            String certString = request.getParameter("cert");
            String dataString = request.getParameter("msgverified");
            String signature = request.getParameter("sigverified");
            String userID = request.getParameter("userID");
            CryptoManagement cy = new CryptoManagement();
            int result = cy.verifySignaturePKCS7(signature.getBytes());
            log.debug("certString::"+certString);
            log.debug("dataString::"+dataString);
            log.debug("signature::"+signature);
            log.debug("userID::"+userID);
            //cy.m_signedData
            
            if (result == 0) {
                json.put("_result", "success");
                json.put("_data", cy.m_signedData);                
                out.print(json);
            } else {
                json.put("_result", "error");
                json.put("_resultcode", result);
                json.put("_data", cy.m_signedData);                
                json.put("_data", cy.m_certOfSigner.toString());                
                out.print(json);
            }

        } catch (Exception ex) {
            log.error("exception caught :: ",ex);
        }
        log.info("is ended :: ");

//        if(certString!=null || userID!=null)
//        {
//              SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
//                Session sChannel = suChannel.openSession();
//                ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
//
//                SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
//                Session sRemoteAcess = suRemoteAcess.openSession();
//                RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
//                Channels channel = cUtil.getChannel("face");
//               
//            if (userID != null) {
//                try {
//                    CertificateManagement certMgmt = new CertificateManagement();
//                    Certificates cer = certMgmt.getCertificate1(channel.getChannelid(), userID);
//                    Certificate certoVerify = convertToX509Certificate(cer.getCertificate());
//                    boolean isVerified = verifyDataRSAInner(dataString.getBytes(), hexStringToByteArray(signature), certoVerify.getPublicKey(), RSA);
//                    if (isVerified == true) {
//                        json.put("_result", "success");
//                        out.print(out);
//                    } else {
//                        json.put("_result", "error");
//                        out.print(out);
//                    }
//                } catch (Exception ex) {
//                    log.error("Exception caught :: ",ex);
//                }
//            }
//          else
//          {
//              try {
//
//                  Certificate certoVerify = convertToX509Certificate(certString);
//                  boolean isVerified = verifyDataRSAInner(dataString.getBytes(),hexStringToByteArray(signature), certoVerify.getPublicKey(), RSA);
//                  if (isVerified == true) {
//                 json.put("_result","success");
//                     out.print(out);
//                  }
//                  else
//                  {
//                    json.put("_result","error");
//                     out.print(out);
//                  }
//                  
//               } catch (Exception ex) {
//                  log.error("Exception caught :: ",ex);
//              }
//          } 
    }

//    public static boolean verifySignature(String Signeddata) {
//        boolean verified = false;
//        try {
//            CMSSignedData s = new CMSSignedData(Base64.decode(Signeddata));
//            CertStore certs = s.getCertificatesAndCRLs("Collection", "BC");
//            SignerInformationStore signers = s.getSignerInfos();
//            for (Iterator i = signers.getSigners().iterator(); i.hasNext();) {
//                SignerInformation signer = (SignerInformation) i.next();
//                CertSelector cr = new X509CertSelector();
//                Collection certCollection = certs.getCertificates(cr);
//                if (!certCollection.isEmpty()) {
//                    X509Certificate cert = (X509Certificate) certCollection.iterator().next();
//                    if (signer.verify(cert.getPublicKey(), "BC")) {
//                        verified = true;
//                    }
//                }
//            }
//            System.out.println("verified : " + verified);
//        } catch (Exception ex) {
//            log.error("Exception caught :: ",ex);
//        }
//        return verified;
//    }

//    public static byte[] hexStringToByteArray(String hex) {
//        Pattern replace = Pattern.compile("^0x");
//        String s = replace.matcher(hex).replaceAll("");
//
//        byte[] b = new byte[s.length() / 2];
//        for (int i = 0; i < b.length; i++) {
//            int index = i * 2;
//            int v = Integer.parseInt(s.substring(index, index + 2), 16);
//            b[i] = (byte) v;
//        }
//        return b;
//    }
//
//    private static boolean verifyDataRSAInner(byte[] data, byte[] sigBytes, PublicKey publicKey, int type) {
//        try {
//
//            Signature signature = null;
//            if (type == RSA) {
//                signature = Signature.getInstance("RSA", "BC");
//            } else {
//                signature = Signature.getInstance("SHA1WithRSA");
//            }
//            signature.initVerify(publicKey);
//
//            MessageDigest md = MessageDigest.getInstance("SHA1");
//            md.update(data);
//            byte[] hashedData = md.digest();
//            signature.update(hashedData);
//
//            return signature.verify(sigBytes);
//        } catch (Exception ex) {
//            log.error("Exception caught :: ",ex);
//            return false;
//        }
//    }
//
//    public static X509Certificate convertToX509Certificate(String pem) throws CertificateException, IOException {
//        X509Certificate cert = null;
//        StringReader reader = new StringReader(pem);
//        PEMReader pr = new PEMReader(reader);
//        cert = (X509Certificate) pr.readObject();
//        return cert;
//    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
