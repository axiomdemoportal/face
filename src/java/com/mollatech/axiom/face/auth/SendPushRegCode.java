/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.auth;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.Twowayauth;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TwowayauthManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author ashish
 */
public class SendPushRegCode extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SendPushRegCode.class.getName());

    String _channelId = "";
    String sessionId = "";
    private final int UNUSED = 0;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        sessionId = (String) request.getSession().getAttribute("_apSessionID");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        _channelId = channel.getChannelid();
        String userid = request.getParameter("userid");
        String regcode = "";
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        int reg = 0;
        int exp = Integer.parseInt(LoadSettings.g_sSettings.getProperty("push.regcode.expiry.inminuts"));
        
            log.debug("channel::" + channel.getName());
            log.debug("_channelId::"+ _channelId);
            
            log.debug("sessionId::"+ sessionId);
            log.debug("userid::"+ userid);
           
           try {
            AuthUser user = new UserManagement().getUser(sessionId, _channelId, userid);
            
            if (user == null) {
                json.put("_result", "error");
                json.put("_message", "No User Found");
                return;
            }
            Twowayauth twowayauth = new TwowayauthManagement().getAuthDetailsByUserId(_channelId, userid, sessionId);
            if (twowayauth == null) {
                twowayauth = new Twowayauth();
                twowayauth.setAttempts(0);
                json.put("SMS", "Suspended");
                json.put("Callback", "Suspended");
                json.put("Missed", "Suspended");
                json.put("Push", "Suspended");
                twowayauth.setChannelid(_channelId);
                twowayauth.setRegcodestatus(UNUSED);
                twowayauth.setCreationdatetime(new Date());
                twowayauth.setLastaccessdatetime(new Date());
                twowayauth.setType(json.toString());
                twowayauth.setUserid(userid);
                Twowayauth temp = null;
                do {
                    regcode = new TwowayauthManagement().getRegCode(user, twowayauth);
                    temp = new TwowayauthManagement().validateRegcode(sessionId, _channelId, regcode);

                } while (temp != null);
                twowayauth.setRegcode(AxiomProtect.ProtectData(regcode));
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.MINUTE, exp);
                twowayauth.setExpiry(cal.getTime());
                reg = new TwowayauthManagement().addAuthDetails(twowayauth, sessionId);
                if (reg == 0) {
                    sendCode(regcode, channel, user, cal.getTime().toString());
                    json.put("_result", "success");
                    json.put("_message", "Regcode Sent Successfully");
                    return;
                } else {
                    json.put("_result", "error");
                    json.put("_message", "Error in Sending RegCode");
                    return;
                }
            } else {
                Twowayauth temp = null;
                do {

                    regcode = new TwowayauthManagement().getRegCode(user, twowayauth);
                    temp = new TwowayauthManagement().validateRegcode(sessionId, _channelId, regcode);

                } while (temp != null);
                twowayauth.setRegcodestatus(UNUSED);
                twowayauth.setRegcode(AxiomProtect.ProtectData(regcode));
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.MINUTE, exp);
                twowayauth.setExpiry(cal.getTime());
                reg = new TwowayauthManagement().editAuthDetails(twowayauth, sessionId);
                if (reg == 0) {
                    sendCode(regcode, channel, user, cal.getTime().toString());
                    json.put("_result", "success");
                    json.put("_message", "Regcode Sent Successfully");
                    return;
                } else {
                    json.put("_result", "error");
                    json.put("_message", "Error in Sending RegCode");
                    return;
                }
            }
        } catch (Exception ex) {
            log.error("exception caught :: ",ex);

        } finally {
            out.print(json);
            out.flush();
        }
           log.info("is ended :: ");
    }

    private void sendCode(String registrationCode, Channels channel, AuthUser user, String strExpiry) {
        if (registrationCode != null) {

            Templates temp = null;
            TemplateManagement tObj = new TemplateManagement();
            temp = tObj.LoadbyName(sessionId, _channelId, TemplateNames.MOBILE_SOFTWARE_TOKEN_REGISTER_TEMPLATE);

            if (temp.getStatus() == tObj.ACTIVE_STATUS) {
                ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                Date d = new Date();
                String tmessage = (String) UtilityFunctions.deserializeFromObject(bais);
                tmessage = tmessage.replaceAll("#name#", user.getUserName());
                tmessage = tmessage.replaceAll("#channel#", channel.getName());
                tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));

                tmessage = tmessage.replaceAll("#expiry#", strExpiry);

                tmessage = tmessage.replaceAll("#tokentype#", "MOBILE");
                String strSWOTPType = LoadSettings.g_sSettings.getProperty("sw.otp.type");

                tmessage = tmessage.replaceAll("#regcode#", registrationCode);
                
                //System.out.println("sendregistration sending registration code as " + registrationCode);
                
                AXIOMStatus status = new AXIOMStatus();
                SendNotification send = new SendNotification();
                status = send.SendOnMobileNoWaiting(channel.getChannelid(), user.userId, tmessage, 1, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                
//                send.SendEmail(channel.getChannelid(), user.email, 
//                        "Registation Code For Token", tmessage, null, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
            }
        }
    }   // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
