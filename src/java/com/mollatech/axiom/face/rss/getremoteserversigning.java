/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.rss;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Remotesignature;
import com.mollatech.axiom.nucleus.db.connector.management.PDFSigningManagement;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class getremoteserversigning extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(getremoteserversigning.class.getName());

//   private static int PDF_TYPE = 0;
//    private static int CSV_TYPE = 1;
    private static final int BUFSIZE = 4096;
    public static final int RAW_DATA = 1;
    public static final int PDF = 2;
    public static final int SIGNATURE_FILE = 1;
    public static final int DATATOSIGN = 2;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        try {
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel ::" + channel.getName());
            String filepath = null;
            String type = request.getParameter("type");
            log.debug("type ::" + type);
            String archiveId = request.getParameter("archiveId");
            log.debug("archiveId ::" + archiveId);
            int itype = Integer.parseInt(type);
            PDFSigningManagement pdfObj = new PDFSigningManagement();
            Remotesignature[] rObj = pdfObj.getRemoteSignatureObj(channel.getChannelid(), archiveId, 0);
            
            UtilityFunctions util = new UtilityFunctions();
//added start
            if (rObj[0].getDocType() == PDF) {
                if (itype == SIGNATURE_FILE) {
                    filepath = rObj[0].getSignature();
                } else if (itype == DATATOSIGN) {
                    filepath = rObj[0].getDataToSign();

                }
            }
            //end here
            File file = null;;
            if (rObj[0].getDocType() == RAW_DATA) {
                String strdata = null;
                if (itype == SIGNATURE_FILE) {
                    strdata = rObj[0].getSignature();
                } else if (itype == DATATOSIGN) {
                    strdata = rObj[0].getDataToSign();
                }
                String mainfilepath = LoadSettings.g_sSettings.getProperty("remote.sign.archive");
                String filename = mainfilepath + new Date().getTime() + ".txt";

                filepath = util.writeTofile(filename, strdata);
                file = new File(filepath);
            } else if (rObj[0].getDocType() == PDF) {
                file = new File(filepath);
            }

//                File file = new File(filepath);
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(filepath);

            // sets response content type
            if (mimetype == null) {
                mimetype = "application/octet-stream";
            }
            response.setContentType(mimetype);
            response.setContentLength((int) file.length());
            String fileName = (new File(filepath)).getName();

            // sets HTTP header
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(file));

            // reads the file's bytes and writes them to the response stream
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }

            in.close();
            outStream.close();
            if(rObj[0].getDocType() == RAW_DATA){
            file.delete();
            }

        } catch (Exception ex) {
            // TODO handle custom exceptions here
            log.error("exception caught ::",ex);
        }
        
        log.info("is ended :: ");



    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
