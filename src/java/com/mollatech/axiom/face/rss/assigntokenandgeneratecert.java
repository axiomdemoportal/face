/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.rss;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class assigntokenandgeneratecert extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(assigntokenandgeneratecert.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final String itemType = "OTPTOKENS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        log.info("is started :: ");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        
        String result = "success";
        String message = "Token Assigned Successfully....";
         String _value = "Assigned";
        String _status = "Assigned";


        
        try {
            
        

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel ::" + channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId ::" + sessionId);
        

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin ::" + remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS ::" + operatorS.getOperatorid());

        String _serialnumber = request.getParameter("_hwserialno");
        log.debug("_serialnumber ::" +_serialnumber);
        String _emailid = request.getParameter("_emailid");
        log.debug("_emailid ::" + _emailid);
            
         int subCategory = 1;
        int category = OTPTokenManagement.HARDWARE_TOKEN;
        
        

        if (_emailid == null) {
            result = "error";
            message = "Could Not Assign Token!!";
            json.put("_result", result);
            json.put("_message", message);
            out.print(json);
            out.flush();
            return;
        }

       
        int retValue = -1;
        UserManagement uManagement = new UserManagement();
        String userFlag = LoadSettings.g_sSettings.getProperty("check.user");
        AuthUser aUser = null;
        if (userFlag.equals("1")) {
            aUser = uManagement.CheckUserByType(sessionId, channel.getChannelid(), _emailid, 4);
        } else {
            aUser = uManagement.CheckUserByType(sessionId, channel.getChannelid(), _emailid, 3);
        }
        if (aUser == null) {
            result = "error";
            message = "user not found please enter valid email address!!";
            json.put("_result", result);
            json.put("_message", message);
            out.print(json);
            out.flush();
            return;
        }
        OTPTokenManagement oManagement = new OTPTokenManagement(channel.getChannelid());
        AuditManagement audit = new AuditManagement();
        retValue = oManagement.AssignTokenAndIssueCert(sessionId, channel.getChannelid(), aUser.userId, category, 1, _serialnumber);

        log.debug("AssignTokenAndIssueCert ::" +retValue);
        
        String icategory = String.valueOf(category);
        String resultString = "ERROR";
        String strCategory = "";
        if (category == OTPTokenManagement.SOFTWARE_TOKEN) {
            strCategory = "SOFTWARE_TOKEN";
        } else if (category == OTPTokenManagement.HARDWARE_TOKEN) {
            strCategory = "HARDWARE_TOKEN";
        } else if (category == OTPTokenManagement.OOB_TOKEN) {
            strCategory = "OOB_TOKEN";
        }

        String strSubCategory = "";
        if (subCategory == OTPTokenManagement.SW_WEB_TOKEN) {
            strSubCategory = "SW_WEB_TOKEN";
        } else if (subCategory == OTPTokenManagement.SW_MOBILE_TOKEN) {
            strSubCategory = "SW_MOBILE_TOKEN";
        } else if (subCategory == OTPTokenManagement.OOB__SMS_TOKEN) {
            strSubCategory = "OOB__SMS_TOKEN";
        } else if (subCategory == OTPTokenManagement.OOB__USSD_TOKEN) {
            strSubCategory = "OOB__USSD_TOKEN";
        } else if (subCategory == OTPTokenManagement.OOB__VOICE_TOKEN) {
            strSubCategory = "OOB__VOICE_TOKEN";
        }

        strCategory = "HARDWARE_TOKEN";

        strSubCategory = "HW_MINI_TOKEN";

        if (retValue == 0) {
            resultString = "SUCCESS";

            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(),
                    "Assign Token",
                    resultString,
                    retValue,
                    "Token Management", 
                    "",
                    "Category = " + strCategory + " Sub Category =" + strSubCategory,
                    "OTPTOKENS", aUser.userId);
            
            AuditManagement audit1 = new AuditManagement();      

            audit1.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(),
                    "Issue Certificate",
                    resultString,
                    retValue,
                    "Certificate Management", "",
                    "Category = " + strCategory + " Sub Category =" + strSubCategory,
                    "PKITOKEN", aUser.userId);

        }

        if (retValue != 0) {

            resultString = "ERROR";

            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(),
                    "Assign Token",
                    resultString,
                    retValue,
                    "Token Management", "",
                    "Category = " + strCategory + " Sub Category =" + strSubCategory,
                    "OTPTOKENS", aUser.userId);

            AuditManagement audit1 = new AuditManagement();      
            audit1.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(),
                    "Issue Certificate",
                    resultString,
                    retValue,
                    "Certificate Management", "",
                    "Category = " + strCategory + " Sub Category =" + strSubCategory,
                    "PKITOKEN", aUser.userId);

            if (retValue == -4) {
                message = "Token is already assigned. Two assignments are not allowed!!!!";
            } else if (retValue == -7) {
                message = "Assign OTP Token failed!!!";

            } else if (retValue == -8) {
                message = "Certificate issuance failed!!!";

            } else {
                message = "Could Not Find Serial Number or Invalid Serial Number!!!!";
            }

            result = "error";
            json.put("_result", result);
            json.put("_message", message);
            out.print(json);
            out.flush();
            return;
        }
        
        }catch(Exception e){
            log.error("exception caught ::",e);
            
        }
        
        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);
            json.put("_status", _status);
        } catch(Exception e){
            log.error("exception caught ::",e);
            
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
