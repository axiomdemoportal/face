/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.rss;

import com.mollatech.axiom.connector.user.TokenStatusDetails;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Otptokens;
import com.mollatech.axiom.nucleus.db.connector.OtpTokensUtils;
import com.mollatech.axiom.nucleus.db.connector.management.CryptoManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import static com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement.DATE_FORMAT_NOW;
import static com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement.HARDWARE_TOKEN;
import static com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement.OTP_TOKEN_NOT_FOUND;
import static com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement.SOTP;
import static com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement.TOKEN;
import static com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement.TOKEN_STATUS_ACTIVE;
import com.mollatech.axiom.nucleus.settings.TokenSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Pramod
 */
public class VerifySotpForSigningReport extends HttpServlet {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(VerifySotpForSigningReport.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String resultString = "Failed";
        String result = "success";
        String message = "OTP Verified Successfully....";
        try {
            SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy hh:mm aaa");
            
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            String userid = request.getParameter("_userid");
            String datatosign = request.getParameter("_datatosign");
            String timeOfSIgning = request.getParameter("_timeofsigning");
            String sotp = request.getParameter("_sotp");
            
            log.debug("VerifySotpForSigningReport::channel is::" + channel.getName());
            log.debug("VerifySotpForSigningReport::Operator Id is::" + operatorS.getOperatorid());
            log.debug("VerifySotpForSigningReport::SessionId::" + sessionId);
            log.debug("VerifySotpForSigningReport::datatosign is::" + userid);
            log.debug("VerifySotpForSigningReport::Duration of scheduler is::" + datatosign);
            log.debug("VerifySotpForSigningReport::time Of SIgning is::" + timeOfSIgning);
            log.debug("VerifySotpForSigningReport::SOTP is::" + sotp);
            
            
            String[] data = new String[1];
            data[0] = datatosign;
            Date dateofSiging = df2.parse(timeOfSIgning);
            OTPTokenManagement otpTokenMgmt = new OTPTokenManagement(channel.getChannelid());
            int otpVerificationResult = otpTokenMgmt.VerifySignatureOtpforReports(channel.getChannelid(), userid, sessionId, data, sotp, dateofSiging);
            log.debug("VerifySotpForSigningReport::VerifySignatureOtpforReports::" + otpVerificationResult);
            if (otpVerificationResult != 0) {
                result = "error";
                message = "Otp Verification failed";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
            } else {
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
            }
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        }
    }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
