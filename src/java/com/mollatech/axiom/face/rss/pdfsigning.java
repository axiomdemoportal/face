/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.rss;

import com.itextpdf.text.pdf.PdfReader;
//import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.communication.RemoteSigningInfo;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Certificates;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.CryptoManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PDFSigningManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import org.bouncycastle.util.encoders.Base64;

/**
 *
 * @author mollatech2
 */
public class pdfsigning extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(pdfsigning.class.getName());

    private static final long serialVersionUID = 1L;
    // location to store file uploaded
    private static final String UPLOAD_DIRECTORY = "uploads";
    // upload settings
    private static final int MEMORY_THRESHOLD = 1024 * 1024 * 3;  // 3MB
    private static final int MAX_FILE_SIZE = 1024 * 1024 * 40; // 40MB
    private static final int MAX_REQUEST_SIZE = 1024 * 1024 * 50; // 50MB

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        response.setContentType("text/html;charset=UTF-8");
        response.setContentType("application/json");
        log.info("is started :: ");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String resultString = "Failed";
        String result = "success";
        String message = "Token Assigned Successfully....";
        BufferedWriter bufferedWriter = null;
        try {
                  Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
                  log.debug("operatorS ::" + operatorS.getOperatorid());
      
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel ::" + channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("SessionId::" + sessionId);

            //audit parameter
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("remoteaccesslogin ::" + remoteaccesslogin);
      

            String _fileName = request.getParameter("_filename");
            log.debug("_fileName ::" +_fileName);
            String _emailid = request.getParameter("_emailId");
            log.debug("_emailid ::" + _emailid);
            String _otp = request.getParameter("_otp");
            log.debug("_otp ::" + _otp);
            String _referneceid = request.getParameter("_referneceid");
            log.debug("_referneceid ::"+ _referneceid);

            String _user_name = request.getParameter("savemepost");
            log.debug("pdfsigning::User name is::"+ _user_name);
            String user_name = _user_name.substring(_user_name.indexOf("src"), _user_name.length());
            String finalstring = user_name.substring(user_name.indexOf(",") + 1, user_name.length() - 2);
            //String imagepath = "E:\\imagefile.png";
            String imagepath = LoadSettings.g_sSettings.getProperty("remote.sign.archive") + "imagefile.png";
            
            byte[] data = Base64.decode(finalstring);
            try {
                FileOutputStream fos = new FileOutputStream(imagepath);
                fos.write(data);
                fos.close();
            } catch (Exception ex) {
                log.error("Exception caught :: ",ex);
            }
            String _filepath = LoadSettings.g_sSettings.getProperty("remote.sign.archive") + _fileName;
            PdfReader reader = new PdfReader(_filepath);
            int pages = reader.getNumberOfPages();
            AuditManagement audit = new AuditManagement();
            if (_emailid == null || _otp == null) {
                result = "error";
                message = "fill all details!!";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }

            int retValue = -1;
            int retval = -1;
            UserManagement uManagement = new UserManagement();
            String userFlag = LoadSettings.g_sSettings.getProperty("check.user");
            AuthUser aUser = null;
            if (userFlag.equals("1")) {
                aUser = uManagement.CheckUserByType(sessionId, channel.getChannelid(), _emailid, 4);
            } else {
                aUser = uManagement.CheckUserByType(sessionId, channel.getChannelid(), _emailid, 3);
            }
            if (aUser == null) {
                result = "error";
                message = "user not found please enter valid email address!!";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }
            OTPTokenManagement oManagement = new OTPTokenManagement(channel.getChannelid());
            Date dateOfverifyOtp = new Date();
            retValue = oManagement.VerifyOTPwithAck(channel.getChannelid(), aUser.userId, sessionId, _otp,"");
            log.debug("VerifyOTPwithAck::" +retValue);
//             if (retValue != 0) {
//                    message = "Verify OTP Failed!!";
//
//                    result = "error";
//
//                    json.put("_result", result);
//                    json.put("_message", message);
//                    out.print(json);
//                    out.flush();
//
//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
//                            remoteaccesslogin, operatorS.getName(), new Date(), "Verify OTP ", resultString, -7,
//                            "Token Management", "Verify OTP Failed", "Verify OTP Failed",
//                            "Verify OTP ", aUser.userId);
//                    return;
//                }

            PDFSigningManagement pManagement = new PDFSigningManagement();
            String plain = aUser.userId + channel.getChannelid() + new Date();
            String archiveid = UtilityFunctions.Bas64SHA1(plain);

            String referenceid = null;
               if(_referneceid == null || _referneceid.isEmpty()){
                        String refData = plain + archiveid + new Date();
                        referenceid = UtilityFunctions.Bas64SHA1(refData);
                        
                    }else{
                        referenceid = _referneceid;
                    }
            
            
            String pdfFileB64 = null;
            try {
                pdfFileB64 = pManagement.fileToBase64(_filepath);
            } catch (Exception ex) {
                log.error("Exception caught :: ",ex);
            }
            String strLocation = "";
            String strReason = "";
            boolean sResult = false;
            int res = -1;
            result = "error";
            message = "pdf Signed Successful";
            String signature = "";
            RemoteSigningInfo rsInfo = new RemoteSigningInfo();
            //rsInfo.setDataToSign("To be signed data");
            rsInfo.dataToSign = pdfFileB64;
         
         
          //  rsInfo.clientLocation = strLocation;
            rsInfo.reason = strReason;
            //rsInfo.setType(1); // RAW
            rsInfo.type = 2; // PDF
            
            if (retValue == 0) {
                CertificateManagement certManagement = new CertificateManagement();
                Certificates cert = certManagement.getCertificate(sessionId, channel.getChannelid(), aUser.userId);

                if (cert == null) {
                    message = "Certificate Not Issued!!";

                    result = "error";

                    json.put("_result", result);
                    json.put("_message", message);
                    out.print(json);
                    out.flush();

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), "Verify OTP ", resultString, -7,
                            "Token Management", "OTP = ******", " OTP = ******",
                            "Verify OTP TOKEN", aUser.userId);
                    return;
                }

                String pfxFile = cert.getPfx();
                String pfxPassword = cert.getPfxpassword();
                CryptoManagement cyManagement = new CryptoManagement();
                // String pfxFile = certManagement.sendPFXFile(sessionid, session.getChannelid(), userid);
                //String pfxPassword = certManagement.getPFXPassWord(sessionid, session.getChannelid(), userid);
                //call signData() here 

                Date d = new Date();
                String sourceFileName = aUser.userId + d.getTime();

                String destFileName = LoadSettings.g_sSettings.getProperty("remote.sign.archive") + sourceFileName + "-sign.pdf";
                try {
                    sResult = pManagement.signPdf(aUser.userId,_filepath, destFileName, pfxFile, pfxPassword, rsInfo, imagepath);
                } catch (Exception ex) {
                    Logger.getLogger(pdfsigning.class.getName()).log(Level.SEVERE, null, ex);
                    log.error("exception caught :: ",ex);
                }
                if (sResult == true) {
                    signature = pManagement.fileToBase64(destFileName);
                    res = 0;
                }
                retval = pManagement.addRemoteSignature(sessionId, channel.getChannelid(), aUser.userId, archiveid, _otp, retValue, dateOfverifyOtp, rsInfo.type, _filepath, destFileName, res, new Date(),referenceid);
                log.debug("addRemoteSignature :: "+retValue);
                if (retval != 0) {
                    message = "Add Remote Signature Failed!!";

                    result = "error";

                    json.put("_result", result);
                    json.put("_message", message);
                    out.print(json);
                    out.flush();

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), "Verify OTP ", resultString, -7,
                            "Token Management", "Add Remote Signature Failed", " Add Remote Signature Failed",
                            "Add Remote Signature", aUser.userId);
                    return;
                }

                SendNotification send = new SendNotification();
                TemplateManagement tManagement = new TemplateManagement();
                Templates templates = tManagement.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.EMAIL_PDF_SIGNING_TEMPLATE);

                if (templates.getStatus() == tManagement.ACTIVE_STATUS) {
                    ByteArrayInputStream bais = new ByteArrayInputStream(templates.getTemplatebody());
                    String tmessage = (String) TemplateUtils.deserializeFromObject(bais);
                    String subject = templates.getSubject();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                    File file = new File(_filepath);
                    request.getLocalAddr();

                    if (tmessage != null) {

                        tmessage = tmessage.replaceAll("#name#", aUser.getUserName());
                        tmessage = tmessage.replaceAll("#channel#", channel.getName());
                        tmessage = tmessage.replaceAll("#email#", aUser.getEmail());
                        tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                        tmessage = tmessage.replaceAll("#fileName#", _fileName);
                        tmessage = tmessage.replaceAll("#referenceid#", referenceid);
                    }

                    if (subject != null) {

                        subject = subject.replaceAll("#channel#", channel.getName());
                        subject = subject.replaceAll("#datetime#", sdf.format(d));
                    }
                    //end of addition

                    String fileNames[] = new String[1];
                    fileNames[0] = destFileName;
                    String mimeTypes[] = new String[1];
                    mimeTypes[0] = "application/pdf";
                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), _emailid, subject, tmessage,
                            null, null, fileNames, mimeTypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                    result = "success";
                    message = "Please Check your Email for Details";
                    json.put("_result", result);
                    json.put("_message", message);
                    json.put("_pdffilename", destFileName);
                }
            } else {
                retval = pManagement.addRemoteSignature(sessionId, channel.getChannelid(), aUser.userId, archiveid, _otp, retValue, dateOfverifyOtp, rsInfo.type, _filepath, null, 0, null,referenceid);
                log.debug("addRemoteSignature :: "+retValue);

                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operatorS.getName(), new Date(), "Verify OTP ", resultString, -7,
                        "Token Management", "Add Remote Signature Failed", " Add Remote Signature Failed",
                        "Add Remote Signature", aUser.userId);

                message = "Add Remote Signature Failed Due to Verify OTP Failed!!";
                result = "error";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;

            }

            if (res == 0) {
                resultString = "success";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operatorS.getName(), new Date(), "Verify OTP And Sign Transaction ", resultString, res,
                        "Verify Otp And Sign Transaction Management", "data = ******, otp = ******", " signdata = ******,otp = ******",
                        "Verify OTP TOKEN And Sign Transaction", aUser.userId);

                out.print(json);
                out.flush();
                return;

            } else {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operatorS.getName(), new Date(), "Verify OTP And Sign Transaction ", resultString, res,
                        "Verify Otp And Sign Transaction Management", "data = ******, otp = ******", " signdata = ******,otp = ******",
                        "Verify OTP TOKEN And Sign Transaction", aUser.userId);
                message = "Failed to sign PDF!!";
                result = "error";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;

            }

        } catch (Exception ex) {
            log.error("exception caught :: ",ex);
        } finally {
//            out.print(json);
//            out.flush();
//            return;
        }
        log.info("is ended :: ");
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
