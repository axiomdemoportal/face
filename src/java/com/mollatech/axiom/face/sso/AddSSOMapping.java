/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.face.sso;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import java.util.Map;
import java.util.HashMap;
import org.bouncycastle.util.encoders.Base64;

/**
 *
 * @author Pramod
 */
public class AddSSOMapping extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AddSSOMapping.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("is started :: ");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId ::" + sessionId);
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel ::" + channel.getName());
        String _channelId = channel.getChannelid();
        log.debug("_channelId ::" + _channelId);
        Map map = new HashMap();
        try {
            String userid = request.getParameter("userid");
            log.debug("userid ::" + userid);
            String strLen = request.getParameter("len");
            log.debug("strLen ::" + strLen);
            int len = Integer.parseInt(strLen);
            for (int i = 0; i < len; i++) {
                JSONObject jsono = new JSONObject();
                String apId = request.getParameter("_hiddenApId" + i);
                jsono.put("_field1", request.getParameter("_field1" + i));
                jsono.put("_field2", request.getParameter("_field2" + i));
                jsono.put("_field3", request.getParameter("_field3" + i));
                jsono.put("_field4", request.getParameter("_field4" + i));
                jsono.put("_field5", request.getParameter("_field5" + i));
                jsono.put("_field6", request.getParameter("_field6" + i));
                jsono.put("_field7", request.getParameter("_field7" + i));
                jsono.put("_field8", request.getParameter("_field8" + i));
                jsono.put("_field9", request.getParameter("_field9" + i));
                jsono.put("_field10", request.getParameter("_field10" + i));
                map.put(new String(Base64.encode((apId).getBytes())), jsono.toString());
            }
            AuthUser user = new UserManagement().getUser(sessionId, _channelId, userid);
            user.setSso(UtilityFunctions.serializeToObject(map));
            int res = new UserManagement().EditUserForSSO(sessionId, _channelId, user);
            log.debug("EditUserForSSO :: " +res);
            if (res == 0) {
                json.put("result", "success");
                json.put("message", "Single Sign On(SSO) Mapping Added Successfully");
            } else {
                json.put("result", "error");
                json.put("message", "Error in Adding Single Sign On (SSO) Mapping");
            }
        } catch (Exception ex) {
            log.error("exception caught :: ",ex);
            try {
                json.put("result", "error");
                json.put("message", "Error in Adding Single Sign On Mapping(SSO)");
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
        } finally {
            out.print(json);
            out.flush();
            out.close();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
