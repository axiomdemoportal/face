/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.mobiletrust.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Trusteddevice;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TrustDeviceManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class removeTrustDevice extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(removeTrustDevice.class.getName());
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final String itemtype = "MOBILETRUST";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getName());
        response.setContentType("application/json");
        String _userid = request.getParameter("_userid");
        log.debug("_userid :: "+_userid);
        String _deviceno = request.getParameter("_deviceno");
        log.debug("_deviceno :: "+_deviceno);
        String result = "success";
        String message = "Device removed successfully....";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
       
        if (_userid == null || _deviceno == null) {
            result = "error";
            message = "Fill all Details!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){log.error("Exception caught :: ",e);}
            out.print(json);
            out.flush();
            return;
        }
        int retValue = -1;

        TrustDeviceManagement dObj = new TrustDeviceManagement();
        Trusteddevice device = null;

//        device = dObj.getTrusteddevice(sessionId, channel.getChannelid(), _userid);
        //int deviceNo = Integer.parseInt(_deviceno);
        device = dObj.getTrusteddeviceByDeviceNo(sessionId, channel.getChannelid(), _userid, _deviceno);
        if (device == null) {
            result = "error";
            message = "Device Not Found!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){log.error("Exception caught :: ",e);}
            out.print(json);
            out.flush();
            return;

        }
        retValue = dObj.deleteTrustedDevice(sessionId, channel.getChannelid(), _deviceno);
        AuditManagement audit = new AuditManagement();
        String resultString = "ERROR";
        if (retValue == 0) {
            resultString = "SUCCESS";
        }

        if (retValue == 0) {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Remove Devie", resultString, retValue,
                    "Trusted Device Management", "Device Id = " + device.getDeviceid() + "Finger Prints= " + device.getAxiomid()
                    + "User Name = " + device.getUserid() + "O.S Details= " + device.getOsdetails(),
                    "Device Deleted", itemtype, _userid);
        }

        if (retValue != 0) {
            try {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(), new Date(), "Remove Devie", resultString, retValue,
                        "Trusted Device Management", "Device Id = " + device.getDeviceid() + "Finger Prints= " + device.getAxiomid()
                                + "User Name = " + device.getUserid() + "O.S Details= " + device.getOsdetails(),
                        "Failed To delete Device", itemtype, _userid);
                
                
                result = "error";
                message = "cannot remove user!!";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            } catch (Exception ex) {
                log.error("Exception caught :: ",ex);
            }
        }





        try {
            json.put("_result", result);
            json.put("_message", message);

        }catch(Exception e){log.error("Exception caught :: ",e);} finally {
            out.print(json);
            out.flush();
        }
        log.info("Servlet ended");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
