/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.axiom.v2.mobiletrust.handler;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Trusteddevice;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.MobileTrustManagment;
import java.util.Date;
import org.json.JSONObject;


/**
 *
 * @author nileshkamble
 */
public class eraseMobileTrustCredential extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(eraseMobileTrustCredential.class.getName());
     final String itemtype = "MOBILETRUST";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
         Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
         log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getName());
        String _userid = request.getParameter("_userid");
       log.debug("_userid :: "+_userid);
        String result = "success";
        String _message = "MobileTrust Credential removed successfully....";


        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        if (_userid == null) {
            result = "error";
            _message = "failed to erase MobileTrust Credential!!";
            try { json.put("_result", result);
            json.put("_message", _message);
            }catch(Exception e){log.error("Exception caught :: ",e);}
            out.print(json);
            out.flush();
            return;
        }


        int retValue = -1;

        MobileTrustManagment mManagment = new MobileTrustManagment();
       

        retValue = mManagment.eraseMobileTrustCredential(sessionId, channel.getChannelid(), _userid);
        if (retValue != 0) {
            result = "error";
            _message = "failed to erase MobileTrust Credential!!";
            try { json.put("_result", result);
            json.put("_message", _message);
            }catch(Exception e){log.error("Exception caught :: ",e);}
            out.print(json);
            out.flush();
            return;

        }

       AuditManagement audit = new AuditManagement();

       
        String resultString = "ERROR";
        if (retValue == 0) {
            resultString = "SUCCESS";
        }

        if (retValue == 0) {
//        
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Remove MobileTrust Credential", resultString, retValue,
                    "Trusted Device Management", "Userid = " + _userid,
                    "Device Status Changed", itemtype, _userid);
        }

        if (retValue != 0) {

            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Remove MobileTrust Credential", resultString, retValue,
                    "Trusted Device Management", "Userid = " + _userid,
                    "Device Status Not Changed", itemtype, _userid);


            result = "error";
            _message = "failed to erase MobileTrust Credential!!";
            out.print(json);
            out.flush();
            return;
        }




        try {
         
            json.put("_result", result);
            json.put("_message", _message);
           

        }catch(Exception e){log.error("Exception caught :: ",e);} finally {
            out.print(json);
            out.flush();
        }
        log.info("Servlet ended");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
