/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.mobiletrust.handler;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Geofence;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Usergroups;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.GeoLocationManagement;
import com.mollatech.axiom.nucleus.db.connector.management.GroupManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.settings.MobileTrustSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech1
 */
public class editgeotracking extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editgeotracking.class.getName());
    final String itemtype = "GEOTRACK";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet Started");
//        $("#_userIDEditRS").val(data._userID);
//        $("#_homeCountryEditRS").val(data._homeCountry);                
//        $("#_foreignCountryEditRS").val(data._foreignCountry);                
//        $("#_eroaming").val(data._roamingAllowed);
//        $("#_startdateEditRS").val(data._Startdate);
//        $("#_enddateEditRS").val(data._Enddate);
//        $("#stateListeditRS").val(data.stateList);
//        $("#_groupideditRS").val(_usergpid);
        String _userID = request.getParameter("_userIDEditRS");
        log.debug("_userID :: "+_userID);
        String _homecountry = request.getParameter("_homeCountryEditRS");
        log.debug("_homecountry :: "+_homecountry);
        String _groupid = request.getParameter("_groupideditRS");
        log.debug("_groupid :: "+_groupid);
        String _startdate = request.getParameter("_startdateEditRS");
        log.debug("_startdate :: "+_startdate);
        String _enddate = request.getParameter("_enddateEditRS");
        log.debug("_enddate :: "+_enddate);
        String _roamingID = request.getParameter("_eroaming");
        log.debug("_roamingID :: "+_roamingID);
        String[] _foreignCountry = request.getParameterValues("_foreignCountryEditRS");
        log.debug("_foreignCountry :: "+_foreignCountry);
        String[] stateList = request.getParameterValues("stateListeditRS");
        log.debug("stateList :: "+stateList);
        response.setContentType("application/json");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getName());
        String result = "success";
        String message = " Updated Successfully!!!";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

//        String operatorId = operatorS.getOperatorid();
        if (_userID == null) {
            _userID = request.getParameter("_userID1");
            log.debug("_userID :: "+_userID);
            if (_userID == null) {
                _userID = request.getParameter("_ruserId");
                log.debug("_userID :: "+_userID);
                if (_userID == null) {
                    _userID = request.getParameter("_userID");
                    log.debug("_userID :: "+_userID);
                }
            }
        }

        if (_userID == null) {
            result = "error";
            message = "Invalid  Details!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("Exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        }

        UserManagement userObj = new UserManagement();

        int groupid = -1;
        if (_groupid != null) {
            groupid = Integer.parseInt(_groupid);
        } else {

            AuthUser user = userObj.getUser(sessionId, channel.getChannelid(), _userID);
            groupid = user.getGroupid();
        }

        int roamingid = 2;
        if (_roamingID != null) {
            roamingid = Integer.parseInt(_roamingID);
        }

        String strForeignCountry = "";
        if (_foreignCountry != null && _foreignCountry.length != 0) {
            for (int i = 0; i < _foreignCountry.length; i++) {
                strForeignCountry = strForeignCountry + _foreignCountry[i] + ",";
            }
        }

        String strStateList = "";
        if (stateList != null && stateList.length != 0) {
            for (int i = 0; i < stateList.length; i++) {
                strStateList = strStateList + stateList[i] + ",";
            }
        }

        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date startdate = null;
        if (_startdate != null && !_startdate.isEmpty()) {
            try {
                startdate = (Date) formatter.parse(_startdate);
            } catch (ParseException ex) {
                Logger.getLogger(editgeotracking.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Date endDate = null;
        if (_enddate != null && !_enddate.isEmpty()) {
            try {
                endDate = (Date) formatter.parse(_enddate);
            } catch (ParseException ex) {
                Logger.getLogger(editgeotracking.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        UserManagement uManagement = new UserManagement();
        AuthUser aUser = null;
        if (groupid != -1) {
            aUser = uManagement.getUser(sessionId, channel.getChannelid(), _userID);

        }

        GroupManagement gManagement = new GroupManagement();
        Usergroups uGDetails = null;// gManagement.getGroupByGroupId(sessionId, channel.getChannelid(), aUser.getGroupid());

        if (groupid != -1) {

            uGDetails = gManagement.getGroupByGroupId(sessionId, channel.getChannelid(), groupid);

            if (uGDetails == null) {
                result = "error";
                message = "User Group not found!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception ex) {
                    log.error("Exception caught :: ",ex);
                }
                out.print(json);
                out.flush();
                return;
            }
        }

        int retValue = -1;
        GeoLocationManagement geo = new GeoLocationManagement();
        Geofence oldgeoobj = geo.getGeoFenceObjByUserId(channel.getChannelid(), _userID);
        AuditManagement audit = new AuditManagement();

        String country = null;

        if (uGDetails.getCountry() == null) {
            SettingsManagement sManagement = new SettingsManagement();
            Object obj = sManagement.getSetting(channel.getChannelid(), SettingsManagement.MOBILETRUST_SETTING, SettingsManagement.PREFERENCE_ONE);

            if (obj instanceof MobileTrustSettings) {
                MobileTrustSettings mSettings = (MobileTrustSettings) obj;
                _homecountry = mSettings.homeCountryCode;
            }
        } else if (groupid != -1) {
            country = uGDetails.getCountry().trim();
            if (!country.isEmpty()) {
                _homecountry = uGDetails.getCountry();
            } else {
                _homecountry = oldgeoobj.getHomeCountry();
            }
        }

//        } else {
//            _homecountry = oldgeoobj.getHomeCountry();
//        }
            retValue = geo.EditGeotrackinObj(sessionId, channel.getChannelid(), _userID,
                    _homecountry, strForeignCountry,
                    strStateList, roamingid,
                    startdate,
                    endDate);

            String resultString = "ERROR";

            if (retValue == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
                        operatorS.getName(), new Date(), "Edit Push Message", resultString, retValue, "Push Message Management",
                        "Foreign Country=" + oldgeoobj.getForeignCountry() + "Home Country=" + oldgeoobj.getHomeCountry()
                        + "UserID" + oldgeoobj.getUserid() + "End Date Of Roaming" + oldgeoobj.getEndDateForRoming()
                        + "GeoFenceId" + oldgeoobj.getGeofenceid() + "Roaming Allowed=" + oldgeoobj.getRoamingAllowed()
                        + "Start Date Of Roaming=" + oldgeoobj.getStartDateForRoming(),
                        "Home Country=" + _homecountry + "Forign Country=" + strForeignCountry
                        + "UserID" + _userID + "End Date Of Roaming" + _enddate
                        + "Roaming Allowed=" + _roamingID
                        + "Start Date Of Roaming=" + _startdate, itemtype, _userID);
            } else {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
                        operatorS.getName(), new Date(), "Edit Push Message", resultString, retValue, "Push Message Management",
                        "Foreign Country=" + oldgeoobj.getForeignCountry() + "Home Country=" + oldgeoobj.getHomeCountry()
                        + "UserID" + oldgeoobj.getUserid() + "End Date Of Roaming" + oldgeoobj.getEndDateForRoming()
                        + "GeoFenceId" + oldgeoobj.getGeofenceid() + "Roaming Allowed=" + oldgeoobj.getRoamingAllowed()
                        + "Start Date Of Roaming=" + oldgeoobj.getStartDateForRoming(),
                        "Failed To Edit Geo-Tracking ...!!!", itemtype, _userID);

            }

            if (retValue == 0) {
                result = "success";
            } else {
                result = "error";
                message = "Edit Failed!!!";
            }

            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("Exception caught :: ",ex);
            } finally {
                out.print(json);
                out.flush();
                log.info("Servlet ended");
            }
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet
        (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            processRequest(request, response);
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost
        (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            processRequest(request, response);
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo
        
            () {
        return "Short description";
        }// </editor-fold>
    }
