/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.mobiletrust.handler;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Geofence;
import com.mollatech.axiom.nucleus.db.connector.management.GeoLocationManagement;
import static com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement.DATE_FORMAT_NOW;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech1
 */
public class getgeotracking extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(getgeotracking.class.getName());
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        response.setContentType("application/json");

        String _userId = request.getParameter("_userId");
        log.debug("_userId :: "+_userId);
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
//        int _call = Integer.valueOf(_userId);
        try {
            GeoLocationManagement geo = new GeoLocationManagement();
            Geofence geoobj = geo.getGeoFenceObjByUserId(channel.getChannelid(), _userId);

            SimpleDateFormat df2 = new SimpleDateFormat("MM/dd/yyyy");
            String start = null;
            if ( geoobj.getStartDateForRoming() != null)
                start = df2.format(geoobj.getStartDateForRoming());
            String end = null;
            if ( geoobj.getEndDateForRoming() != null)
                end = df2.format(geoobj.getEndDateForRoming());
            
            String strRoaaming = "1";
            
            strRoaaming = geoobj.getRoamingAllowed()+ "";
            
            UserManagement uManagement = new UserManagement();
            AuthUser aUser = uManagement.getUser(sessionId, channel.getChannelid(), _userId);
            
            if(aUser != null){
                 json.put("_groupid", aUser.getGroupid());
            }
            
            
//            if ( (geoobj.getRoamingAllowed()) == false) {
//                strRoaaming = "2";
//            }

            json.put("_userID", geoobj.getUserid());
            json.put("_Startdate", start);
            json.put("_homeCountry", geoobj.getHomeCountry());
            json.put("_foreignCountry",  geoobj.getForeignCountry());
            json.put("_Enddate",  end);
            json.put("_roamingAllowed", strRoaaming);
            json.put("_result", "success");
        } catch (Exception ex) {
            // TODO handle custom exceptions here
            try { json.put("_result", "error");
            json.put("_message", ex.getMessage());
            }catch(Exception e){log.error("Exception caught :: ",e);}
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
