/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.mobiletrust.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Trusteddevice;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TrustDeviceManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class changedevicestatus extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(changedevicestatus.class.getName());
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public static final int STATUS_ACTIVE = 1;
    public static final int STATUS_SUSPENDED = -2;
    final String itemtype = "MOBILETRUST";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet Started");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel);
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getName());
        String _userid = request.getParameter("_userid");
        log.debug("_userid :: "+_userid);
        String _status = request.getParameter("_status");
        log.debug("_status :: "+_status);
        int status = 0;
        String _value = "Active";
        if (_status != null) {
            status = Integer.parseInt(_status);
        }
        String result = "success";
        String _message = "Status change successfully....";

        if (status == STATUS_ACTIVE) {
            _value = "Active";
        } else if (status == STATUS_SUSPENDED) {
            _value = "Suspended";
        }

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        
        
        if (_userid == null || _status == null) {
            result = "error";
            _message = "Status not changed!!";
            try { json.put("_result", result);
            json.put("_message", _message);
            }catch(Exception e){log.error("Exception caught :: ",e);}
            out.print(json);
            out.flush();
            return;
        }


        int retValue = -1;

        TrustDeviceManagement dObj = new TrustDeviceManagement();
        Trusteddevice device = null;

        device = dObj.getTrusteddevice(sessionId, channel.getChannelid(), _userid);
        if (device == null) {
            result = "error";
            _message = "Status not changed!!";
            try { json.put("_result", result);
            json.put("_message", _message);
            }catch(Exception e){log.error("Exception caught :: ",e);}
            out.print(json);
            out.flush();
            return;

        }

        if (_value == "Active") {
            //device.setStatus(true);
            device.setStatus((byte)(true?1:0));

        } else if (_value == "Suspended") {
            //device.setStatus(false);
            device.setStatus((byte)(false?1:0));

        }

        retValue = dObj.changeDeviceStatus(sessionId, device.getDeviceno(), device);
        AuditManagement audit = new AuditManagement();
        String resultString = "ERROR";
        if (retValue == 0) {
            resultString = "SUCCESS";
        }

        if (retValue == 0) {
//             Trusteddevice devicenew = null;

        Trusteddevice devicenew = dObj.getTrusteddevice(sessionId, channel.getChannelid(), _userid);
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Change Device", resultString, retValue,
                    "Trusted Device Management", "Device Id = " + devicenew.getDeviceid() + "Finger Prints= " + devicenew.getAxiomid()
                    + "User Name = " + device.getUserid() + "O.S Details= " + device.getOsdetails()+"Status "
                            + (devicenew.getStatus()),
                    "Device Status Changed", itemtype, _userid);
        }

        if (retValue != 0) {

            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Change Device", resultString, retValue,
                    "Trusted Device Management", "Device Id = " + device.getDeviceid() + "Finger Prints= " + device.getAxiomid()
                    + "User Name = " + device.getUserid() + "O.S Details= " + device.getOsdetails()+"Status "
                            + (device.getStatus()),
                    "Device Status Not Changed", itemtype, _userid);


            result = "error";
            _message = "Status Not Changed!!";
            out.print(json);
            out.flush();
            return;
        }




        try {
            _message = "Status changed to " + _value + "  State!!";
            json.put("_result", result);
            json.put("_message", _message);
            json.put("_value", _value);

        }catch(Exception e){log.error("Exception caught :: ",e);} finally {
            out.print(json);
            out.flush();
        }
        log.info("Servlet ended ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
