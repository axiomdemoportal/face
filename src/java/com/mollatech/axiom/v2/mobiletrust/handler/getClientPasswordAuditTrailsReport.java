/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.mobiletrust.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Clientdestroy;
//import com.mollatech.axiom.nucleus.db.connector.management.DestroyClientManagement;
import com.mollatech.axiom.nucleus.db.connector.management.MobileTrustManagment;
import com.mollatech.axiom.nucleus.db.connector.management.TrustDeviceManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.TrustDeviceManagement;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mollatech1
 */
public class getClientPasswordAuditTrailsReport extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(getClientPasswordAuditTrailsReport.class.getName());
    private static int PDF_TYPE = 0;
    private static int CSV_TYPE = 1;
    private static final int BUFSIZE = 4096;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
    try {
            try {

                Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                log.debug("channel :: "+channel.getName());
                String sessionId = (String) request.getSession().getAttribute("_apSessionID");
                log.debug("sessionId :: "+sessionId);

                String _userID = request.getParameter("_userId");
                log.debug("_userID :: "+_userID);
                String _duration = request.getParameter("_duration");
                log.debug("_duration :: "+_duration);
                String _format = request.getParameter("_reporttype");
                log.debug("_format :: "+_format);
              //  int iFormat = -9999;
                 int iFormat = 0;
                if (_format != null && !_format.isEmpty()) {
                    iFormat = Integer.parseInt(_format);
                }
                if (PDF_TYPE == iFormat) {
                    iFormat = PDF_TYPE;
                } else {
                    iFormat = CSV_TYPE;
                }

                String filepath = null;


                try {
                    try {
                        //DestroyClientManagement cm = new DestroyClientManagement();
                        MobileTrustManagment cm = new MobileTrustManagment();

                       TrustDeviceManagement tdm = new TrustDeviceManagement();
                       
                      Clientdestroy[] arrObj =  tdm.getDestroyedClient(sessionId, channel.getChannelid(), _userID, _duration);
                        
                     filepath =   cm.generateReport(iFormat, sessionId, channel.getChannelid(), _userID, arrObj);

//                
                        arrObj = null;
                    } catch (Exception e) {
                        log.error("Exception caught :: ",e);
                    }

                    File file = new File(filepath);
                    int length = 0;
                    ServletOutputStream outStream = response.getOutputStream();
                    ServletContext context = getServletConfig().getServletContext();
                    String mimetype = context.getMimeType(filepath);

                    // sets response content type
                    if (mimetype == null) {
                        mimetype = "application/octet-stream";
                    }
                    response.setContentType(mimetype);
                    response.setContentLength((int) file.length());
                    String fileName = (new File(filepath)).getName();

                    // sets HTTP header
                    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

                    byte[] byteBuffer = new byte[BUFSIZE];
                    DataInputStream in = new DataInputStream(new FileInputStream(file));

                    // reads the file's bytes and writes them to the response stream
                    while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                        outStream.write(byteBuffer, 0, length);
                    }

                    in.close();
                    outStream.close();
                    file.delete();

                } catch (Exception ex) {
                    // TODO handle custom exceptions here
                    log.error("Exception caught :: ",ex);
                }


            } catch (Exception ex) {
                log.error("Exception caught :: ",ex);
            }




        } finally {
            //  out.close();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
