/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.signrequest;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Signingrequest;
import com.mollatech.axiom.nucleus.db.Srtracking;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SignReqTrackingManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SignRequestManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;

/**
 *
 * @author ashish
 */
public class Addpdfdocument extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Addpdfdocument.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private final int Status = -1;
//    private final int enabled = -1;
//    private final int restrictionLevel = -1;
    AuditManagement audit = new AuditManagement();
    final String itemType = "ESIGNER";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        try {
            HttpSession session = request.getSession(true);
            String _result = "success";
            String _message = "ePdf Document Created Successfully";
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel.getName());
            String channelId = channel.getChannelid();
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId :: "+sessionId);
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operatorS :: "+operatorS.getName());
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("remoteaccesslogin :: "+remoteaccesslogin);
            String operatorId = operatorS.getOperatorid();
            Calendar c = Calendar.getInstance();
            String docName = request.getParameter("_documentname");
            log.debug("docName :: "+docName);
            String docCategory = request.getParameter("_docCategory");
            log.debug("docCategory :: "+docCategory);
            String docExpiryDay = request.getParameter("_docExp");
            log.debug("docExpiryDay :: "+docExpiryDay);
            int docExpiry = 0;
            if (docExpiryDay != null) {
                docExpiry = Integer.parseInt(docExpiryDay);
            }
            int docCatId = -1;
            int check = 0;
            if (docCategory != null) {
                docCatId = Integer.parseInt(docCategory);
            }
            String docNote = request.getParameter("_docNote");
            log.debug("docNote :: "+docNote);
            String docpath = (String) request.getSession().getAttribute("_pdfsignPath");
            log.debug("docpath :: "+docpath);
            String _count = request.getParameter("_Count");
            log.debug("_count :: "+_count);
            int count = -1;
            if (_count != null) {
                count = Integer.parseInt(_count);
            }
//        validation start
            docName = docName.trim();
            docNote = docNote.trim();
//      start  document name
            if (docName == null) {
                _result = "error";
                _message = "Document name should not be empty!!!";
                try {
                    json.put("_result", _result);
                    json.put("_message", _message);
                } catch (Exception e) {
                    log.error("Exception caught :: ",e);
                }

                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(),
                        new Date(), "Add Document",
                        _result, 0,
                        "Document Management ",
                        "",
                        "Add new document with name as " + docName,
                        itemType,
                        "" );

                return;
            } else {
                if (docName.isEmpty()) {
                    _result = "error";
                    _message = "Document name should not be empty!!!";
                    try {
                        json.put("_result", _result);
                        json.put("_message", _message);
                    } catch (Exception e) {
                        log.error("Exception caught :: ",e);
                    }
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(),
                        new Date(), "Add Document",
                        _result, 0,
                        "Document Management ",
                        "",
                        "Add new document with name as " + docName,
                        itemType,
                        "" );
                    return;
                }
                if (docName.length() < 4) {
                    _result = "error";
                    _message = "Document name should be greater than 4 characters!!!";
                    try {
                        json.put("_result", _result);
                        json.put("_message", _message);
                    } catch (Exception e) {
                       log.error("Exception caught :: ",e);
                    }
                   audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(),
                        new Date(), "Add Document",
                        _result, 0,
                        "Document Management ",
                        "",
                        "Add new document with name as " + docName,
                        itemType,
                        "" );
                    return;
                } else if (docName.length() > 20) {
                    _result = "error";
                    _message = "Document name should not be greater than 20 characters!!!";
                    try {
                        json.put("_result", _result);
                        json.put("_message", _message);
                    } catch (Exception e) {
                        log.error("Exception caught :: ",e);
                    }
                   audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(),
                        new Date(), "Add Document",
                        _result, 0,
                        "Document Management ",
                        "",
                        "Add new document with name as " + docName,
                        itemType,
                        "" );
                    return;
                }
            }
//      end  document name
            //      start  document catergory
            if (docCatId == -1) {
                _result = "error";
                _message = "Please provide proper document category!!!";
                try {
                    json.put("_result", _result);
                    json.put("_message", _message);
                } catch (Exception e) {
                    log.error("Exception caught :: ",e);
                }
              audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(),
                        new Date(), "Add Document",
                        _result, 0,
                        "Document Management ",
                        "",
                        "Add new document with name as " + docName,
                        itemType,
                        "" );
                return;
            }
        //      end  document catergory

            //      start  document note
            if (docNote == null) {
                _result = "error";
                _message = "Document note should not be empty!!!";
                try {
                    json.put("_result", _result);
                    json.put("_message", _message);
                } catch (Exception e) {
                    log.error("Exception caught :: ",e);
                }
               audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(),
                        new Date(), "Add Document",
                        _result, 0,
                        "Document Management ",
                        "",
                        "Add new document with name as " + docName,
                        itemType,
                        "" );
                return;
            } else {
                if (docNote.isEmpty()) {
                    _result = "error";
                    _message = "Document note should be greater than 5 characters !!!";
                    try {
                        json.put("_result", _result);
                        json.put("_message", _message);
                    } catch (Exception e) {
                        log.error("Exception caught :: ",e);
                    }
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(),
                        new Date(), "Add Document",
                        _result, 0,
                        "Document Management ",
                        "",
                        "Add new document with name as " + docName,
                        itemType,
                        "" );
                    return;
                }

                if (docNote.length() <= 5) {
                    _result = "error";
                    _message = "Document note should be greater than 5 characters !!!";
                    try {
                        json.put("_result", _result);
                        json.put("_message", _message);
                    } catch (Exception e) {
                        log.error("Exception caught :: ",e);
                    }
                  audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(),
                        new Date(), "Add Document",
                        _result, 0,
                        "Document Management ",
                        "",
                        "Add new document with name as " + docName,
                        itemType,
                        "" );
                    return;
                }
                if (docNote.length() > 30) {
                    _result = "error";
                    _message = "Document note should not be greater than 30 characters!!!";
                    try {
                        json.put("_result", _result);
                        json.put("_message", _message);
                    } catch (Exception e) {
                        log.error("Exception caught :: ",e);
                    }
               audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(),
                        new Date(), "Add Document",
                        _result, 0,
                        "Document Management ",
                        "",
                        "Add new document with name as " + docName,
                        itemType,
                        "" );
                    return;
                }
            }
            //      end  document note
            //        validation end

            int result = -1;
            String resultString = "Failure";
            Path path = Paths.get(docpath);
            byte[] pdf = Files.readAllBytes(path);
            SignRequestManagement srm = new SignRequestManagement();
            Signingrequest sr = srm.getEpdfbyName(sessionId, channelId, docName);
            if (sr == null) {
                Signingrequest srequest = new Signingrequest();
                srequest.setChannelid(channelId);
                srequest.setDocname(docName);
                srequest.setDoccategory(docCatId);
                srequest.setStatus(0);
                srequest.setExpiryday(docExpiry);
                srequest.setNote(docNote);
                srequest.setDocument(pdf);
                srequest.setUploadedon(new Date());
                srequest.setPublishon(new Date());
                srequest.setLastupdate(new Date());
                String[] tempuname = new String[count];
                String[] tempgname = new String[count];
                for (int i = 1; i < count; i++) {
                    String gpid = request.getParameter("_groupAdd" + i);
                    String uid = request.getParameter("_userAdd" + i);
                    if (gpid == null || gpid.isEmpty() || uid == null || uid.isEmpty()) {
                        _result = "error";
                        _message = "Please provide proper signer details";
                        try {

                            json.put("_result", _result);
                            json.put("_message", _message);
                        } catch (Exception e) {
                            log.error("Exception caught :: ",e);
                        }
                 audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(),
                        new Date(), "Add Document",
                        _result, 0,
                        "Document Management ",
                        "",
                        "Add new document with name as " + docName,
                        itemType,
                        "" );
                        return;
                    }
                    tempgname[i] = gpid;
                    tempuname[i] = uid;
                }
                for (int i = 1; i < count; i++) {
                    for (int j = i + 1; j < count; j++) {
                        if (tempgname[i].equals(tempgname[j])) {
                            if (tempuname[i].equals(tempuname[j])) {
                                _result = "error";
                                _message = "No duplicate signer allowded";
                                check = 1;
                            }
                        }
                    }
                }

                if (check == 0) {
                    result = srm.addSignRequest(sessionId, srequest);
                    if (result == 0) {
                        Signingrequest s = srm.getEpdfbyName(sessionId, channelId, docName);
                        SignReqTrackingManagement srtm = new SignReqTrackingManagement();
                        Srtracking srt = null;
//                    int expiry = docExpiry;
                        for (int i = 1; i < count; i++) {
                            srt = new Srtracking();
                            String groupid = request.getParameter("_groupAdd" + i);
                            log.debug("groupid :: "+groupid);
                            String userid = request.getParameter("_userAdd" + i);
                            log.debug("userid :: "+userid);
                            int _groupid = -1;
                            if (groupid != null) {
                                _groupid = Integer.parseInt(groupid);
                            }
                            srt.setChannelid(channelId);
                            srt.setDocid(s.getSrid());
                            srt.setDocument(pdf);
                            srt.setUserid(userid);
                            srt.setGroupid(_groupid);
                            srt.setDeviceid("1");
                            srt.setStatus(-1);
                            srt.setSignature("amol");
                            int hash = pdf.hashCode();
                            srt.setHashofdoc(Integer.toString(hash));
                            int resulttrack = srtm.addSignRequestTracking(sessionId, srt);
                            if (resulttrack == 0) {
                                resultString = "Success";
                                session.setAttribute("_pdfsignPath", "");
                            }
                        }
                        resultString = "Success";
                        session.setAttribute("_pdfsignPath", "");
                        audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(),
                        new Date(), "Add Document",
                        resultString, 0,
                        "Document Management ",
                        "",
                        "Add new document with name as " + docName,
                        itemType,
                        ""+s.getSrid() );
                    } else {
                        audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(),
                        new Date(), "Add Document",
                        resultString, 0,
                        "Document Management ",
                        "",
                        "Add new document with name as " + docName,
                        itemType,
                       "" );
                    }
                } else {
 audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(),
                        new Date(), "Add Document",
                        resultString, 0,
                        "Document Management ",
                        "",
                        "Add new document with name as " + docName,
                        itemType,
                        "" );
                }
            } else {
        audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(),
                        new Date(), "Add Document",
                        resultString, 0,
                        "Document Management ",
                        "",
                        "Add new document with name as " + docName,
                        itemType,
                        "" );
            }
            if (result == 0) {
                _result = "success";
            } else {
                if (check != 0) {
                    _result = "error";
                    _message = "No duplicate signer is allowded";
                } else {
                    if (sr != null) {
                        _result = "error";
                        _message = "No duplicate document name is allowded";
                    } else {
                        _result = "error";
                        _message = "E-signing  document addition failed";
                    }
                }
            }
            try {
                json.put("_result", _result);
                json.put("_message", _message);
               audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(),
                        new Date(), "Add Document",
                        resultString, 0,
                        "Document Management ",
                        "",
                        "Add new document with name as " + docName,
                        itemType,
                        "" );
            } catch (Exception e) {
                log.error("Exception :: ",e);
            }
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
