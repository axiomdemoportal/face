/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.signrequest;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Signingrequest;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SignRequestManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class ChangepdfdocStatus extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ChangepdfdocStatus.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private int Status = -1;
    final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;
    AuditManagement audit = new AuditManagement();
    final String itemType = "ESIGNER";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel);
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getName());
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        String operatorId = operatorS.getOperatorid();
        String _value = "Active";
        String _status = request.getParameter("_status");
        log.debug("_status :: "+_status);
        String _docId = request.getParameter("_docid");
        log.debug("_docId :: "+_docId);
        int status = -1, docId = -1;
        if (!_status.equals("")) {
            status = Integer.parseInt(_status);
        }
        if (_docId != null) {
            docId = Integer.parseInt(_docId);
        }
        String _result = "";
        String _message = "Status Updated successfully";
        int result = 0;
        SignRequestManagement management = new SignRequestManagement();
        String resultString = "failure";
//        change status message
        SignRequestManagement srmngt = new SignRequestManagement();
        Signingrequest srequest = srmngt.getEpdfbyId(sessionId, channel.getChannelid(), docId);
        int istatus = srequest.getStatus();
        String strstaus = "Removed";
        if (istatus == ACTIVE_STATUS) {
            strstaus = "Active";
        } else if (istatus == SUSPEND_STATUS) {
            strstaus = "Suspended";
        }
        String strstaus1 = "Removed";
        if (status == ACTIVE_STATUS) {
            strstaus1 = "Active";
        } else if (status == SUSPEND_STATUS) {
            strstaus1 = "Suspended";
        }
        //        change status message
        if (istatus == status) {
            try {
                _result = "error";
                _message = "Status Already " + strstaus;
                json.put("_result", result);
                json.put("_message", _message);
                json.put("_value", _value);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            } finally {
                out.print(json);
                out.flush();
            }        
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(), "Edit Document",
                    _result, 0,
                    "Document Management ",
                    "",
                    "Error:: Status already "+strstaus1+" for Document=" + srequest.getDocname() ,
                    itemType,
                    "" + _docId);
            return;
        }
        result = management.changeEpdfStatus(sessionId, channel.getChannelid(), docId, status);
        if (result == 0) {
            resultString = "Success";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(), "Change Document Status",
                    _result, 0,
                    "Document Management",
                    "Document Name =" + srequest.getDocname() + ", Status = " + strstaus,
                    " Success:: status changed for Document =" + srequest.getDocname() + ", Status = " + strstaus1,
                    itemType,
                    "" + _docId);
        } else {
            resultString = "Success";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(), "Change Document Status",
                    _result, 0,
                    "Document Management",
                    "Document Name =" + srequest.getDocname() + ", Status = " + strstaus,
                    " Error:: status changing for Document =" + srequest.getDocname() + ", Status = " + strstaus1,
                    itemType,
                    "" + _docId);
        }       
        try {
            json.put("_result", _result);
            json.put("_message", _message);
        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
