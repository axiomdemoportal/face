/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.signrequest;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Signingrequest;
import com.mollatech.axiom.nucleus.db.Srtracking;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SignReqTrackingManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SignRequestManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;

public class Editpdfdocument extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Editpdfdocument.class.getName());
    private int Status = -1;
    private int enabled = -1;
    AuditManagement audit = new AuditManagement();
    final String itemType = "ESIGNER";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet Started");
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        try {
            HttpSession session = request.getSession(true);
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel.getName());
            String channelId = channel.getChannelid();
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId :: "+sessionId);
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operatorS :: "+operatorS.getName());
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("remoteaccesslogin :: "+remoteaccesslogin);
            String operatorId = operatorS.getOperatorid();
            String _docId = request.getParameter("_docId");
            log.debug("_docId :: "+_docId);
            int docId = -1;
            if (_docId != null) {
                docId = Integer.parseInt(_docId);
            }
            String docName = request.getParameter("_edocumentname");
            log.debug("docName :: "+docName);
            String docCategory = request.getParameter("_edocCategory");
            log.debug("docCategory :: "+docCategory);
            String _docExpDay = request.getParameter("_edocExp");
            log.debug("_docExpDay :: "+_docExpDay);
            int docExpday = -1;
            if (_docExpDay != null) {
                docExpday = Integer.parseInt(_docExpDay);
            }
            int docCatId = -1;
            int check = 0;
            if (docCategory != null) {
                docCatId = Integer.parseInt(docCategory);
            }
            String docNote = request.getParameter("_edocNote");
            log.debug("docNote :: "+docNote);
            String docpath = (String) request.getSession().getAttribute("_pdfsignPath");
            log.debug("docpath :: "+docpath);
            byte[] pdf = null;

            if (docpath != null) {
                if (!docpath.equals("")) {
                    Path path = Paths.get(docpath);
                    pdf = Files.readAllBytes(path);
                }
            }
            String _count = request.getParameter("_Count");
            int count = -1;
            if (_count != null) {
                count = Integer.parseInt(_count);
            }

            String _result = "success";
            String _message = "ePdf Document Updated Successfully";
            //        validation start
            docName = docName.trim();
            docNote = docNote.trim();
//      start  document name
            if (docName == null) {
                _result = "error";
                _message = "Document name should not be empty!!!";
                try {
                    json.put("_result", _result);
                    json.put("_message", _message);
                } catch (Exception e) {
                    log.error("Exception caught :: ",e);

                }
   audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                                request.getRemoteAddr(),
                                channel.getName(),
                                remoteaccesslogin, operatorS.getName(),
                                new Date(), "Edit Document",
                                _result, 0,
                                "Document Management ",
                                "",
                                "Error::Failed to edit document",
                                itemType,
                                "" + _docId);
                return;
            } else {
                if (docName.isEmpty()) {
                    _result = "error";
                    _message = "Document name should not be empty!!!";
                    try {
                        json.put("_result", _result);
                        json.put("_message", _message);
                    } catch (Exception e) {
                        log.error("Exception caught :: ",e);
                    }
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                                request.getRemoteAddr(),
                                channel.getName(),
                                remoteaccesslogin, operatorS.getName(),
                                new Date(), "Edit Document",
                                _result, 0,
                                "Document Management ",
                                "",
                                "Error::Failed to edit document",
                                itemType,
                                "" + _docId);
                    return;
                }
                if (docName.length() < 4) {
                    _result = "error";
                    _message = "Document name should be grater than 4 characters!!!";
                    try {
                        json.put("_result", _result);
                        json.put("_message", _message);
                    } catch (Exception e) {
                        log.error("Exception caught :: ",e);
                    }
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                                request.getRemoteAddr(),
                                channel.getName(),
                                remoteaccesslogin, operatorS.getName(),
                                new Date(), "Edit Document",
                                _result, 0,
                                "Document Management ",
                                "",
                                "Error::Failed to edit document",
                                itemType,
                                "" + _docId);
                    return;
                } else if (docName.length() > 20) {
                    _result = "error";
                    _message = "Document name should not be grater than 20 characters!!!";
                    try {
                        json.put("_result", _result);
                        json.put("_message", _message);
                    } catch (Exception e) {
                        log.error("Exception caught :: ",e);
                    }
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                                request.getRemoteAddr(),
                                channel.getName(),
                                remoteaccesslogin, operatorS.getName(),
                                new Date(), "Edit Document",
                                _result, 0,
                                "Document Management ",
                                "",
                                "Error::Failed to edit document",
                                itemType,
                                "" + _docId);
                    return;
                }
            }

            //      start  document catergory
            if (docCatId == -1) {
                _result = "error";
                _message = "Please provide proper document category!!!";
                try {
                    json.put("_result", _result);
                    json.put("_message", _message);
                } catch (Exception e) {
                    log.error("Exception caught :: ",e);
                }
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                                request.getRemoteAddr(),
                                channel.getName(),
                                remoteaccesslogin, operatorS.getName(),
                                new Date(), "Edit Document",
                                _result, 0,
                                "Document Management ",
                                "",
                                "Error::Failed to edit document",
                                itemType,
                                "" + _docId);
                return;
            }
//      end  document catergory
//      start  document note
            if (docNote == null) {
                _result = "error";
                _message = "Document note should not be empty!!!";
                try {
                    json.put("_result", _result);
                    json.put("_message", _message);
                } catch (Exception e) {
                    log.error("Exception caught :: ",e);
                }
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                                request.getRemoteAddr(),
                                channel.getName(),
                                remoteaccesslogin, operatorS.getName(),
                                new Date(), "Edit Document",
                                _result, 0,
                                "Document Management ",
                                "",
                                "Error::Failed to edit document",
                                itemType,
                                "" + _docId);
                return;
            } else {
                if (docNote.isEmpty()) {
                    _result = "error";
                    _message = "Document note should not be empty !!!";
                    try {
                        json.put("_result", _result);
                        json.put("_message", _message);
                    } catch (Exception e) {
                        log.error("Exception caught :: ",e);
                    }
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                                request.getRemoteAddr(),
                                channel.getName(),
                                remoteaccesslogin, operatorS.getName(),
                                new Date(), "Edit Document",
                                _result, 0,
                                "Document Management ",
                                "",
                                "Error::Failed to edit document",
                                itemType,
                                "" + _docId);
                    return;
                }
                if (docNote.length() <= 5) {
                    _result = "error";
                    _message = "Document note should be greater than 5 characters !!!";
                    try {
                        json.put("_result", _result);
                        json.put("_message", _message);
                    } catch (Exception e) {
                        log.error("Exception caught :: ",e);
                    }
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                                request.getRemoteAddr(),
                                channel.getName(),
                                remoteaccesslogin, operatorS.getName(),
                                new Date(), "Edit Document",
                                _result, 0,
                                "Document Management ",
                                "",
                                "Error::Failed to edit document",
                                itemType,
                                "" + _docId);
                    return;
                }
                if (docNote.length() > 30) {
                    _result = "error";
                    _message = "Document note should not be greater than 30 characters!!!";
                    try {
                        json.put("_result", _result);
                        json.put("_message", _message);
                    } catch (Exception e) {
                        log.error("Exception caught :: ",e);
                    }
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                                request.getRemoteAddr(),
                                channel.getName(),
                                remoteaccesslogin, operatorS.getName(),
                                new Date(), "Edit Document",
                                _result, 0,
                                "Document Management ",
                                "",
                                "Error::Failed to edit document",
                                itemType,
                                "" + _docId);
                    return;
                }
            }
            //      end  document note
            //        validation end
            int result = -1, duplicate = 0;
            String resultString = "Failure";
            SignRequestManagement srm = new SignRequestManagement();
            Signingrequest srequest = srm.getEpdfbyId(sessionId, channelId, docId);
            String currentdocName = srequest.getDocname();
            Signingrequest[] sgrequest = srm.getAllEpdf(sessionId, channelId);
            if (sgrequest != null) {
                for (Signingrequest sgrequest1 : sgrequest) {
                    if ((currentdocName == null ? sgrequest1.getDocname() == null : currentdocName.equals(sgrequest1.getDocname())) && (currentdocName == null ? srequest.getDocname() != null : !currentdocName.equals(srequest.getDocname()))) {
                        duplicate = 1;
                    }
                }
            }
            if (duplicate == 0) {
                srequest.setChannelid(channelId);
                srequest.setDocname(docName);
                srequest.setDoccategory(docCatId);
                srequest.setStatus(0);
                srequest.setNote(docNote);
                srequest.setExpiryday(docExpday);
                boolean flag = false;

                if (docpath != null) {
                    if (!docpath.isEmpty()) {
                        flag = true;
                        srequest.setDocument(pdf);
                    }
                }
                srequest.setLastupdate(new Date());
                String[] tempuname = new String[count];
                String[] tempgname = new String[count];
                for (int i = 0; i < count; i++) {
                    String gpid = request.getParameter("_groupAdd" + (i + 1));
                    String uid = request.getParameter("_userAdd" + (i + 1));
                    tempgname[i] = gpid;
                    tempuname[i] = uid;
                }
                for (int i = 0; i < count; i++) {
                    for (int j = i + 1; j < count; j++) {
                        if (tempgname[i].equals(tempgname[j])) {
                            if (tempuname[i].equals(tempuname[j])) {
                                _result = "error";
                                _message = "No duplicate Signer allowded";
                                check = 1;
                            }
                        }
                    }
                }
                if (check == 0) {
                    result = srm.editEpdfdoc(sessionId, channelId, srequest);
                    if (result == 0) {
                        Signingrequest s = srm.getEpdfbyName(sessionId, channelId, docName);
                        SignReqTrackingManagement srtm = new SignReqTrackingManagement();
                        Srtracking srt = null;
                        Srtracking[] stracking = srtm.getAllSRTrackingDetailsbydocId(sessionId, channelId, docId);
                        int _groupid = -1;
                        int resulttrack = -1;
                        if (count > stracking.length) {
                            for (int i = 0; i < stracking.length; i++) {
                                if (tempgname[i] != null) {
                                    _groupid = Integer.parseInt(tempgname[i]);
                                }
                                stracking[i].setUserid(tempuname[i]);
                                stracking[i].setGroupid(_groupid);
                                resulttrack = srtm.editSignRequestTracking(sessionId, stracking[i]);
                            }
                            for (int j = stracking.length; j < count; j++) {
                                srt = new Srtracking();
                                if (tempgname[j] != null) {
                                    _groupid = Integer.parseInt(tempgname[j]);
                                }
                                srt.setChannelid(channelId);
                                srt.setDocid(s.getSrid());
                                srt.setDocument(srequest.getDocument());
                                srt.setUserid(tempuname[j]);
                                srt.setGroupid(_groupid);
                                srt.setStatus(-1);
                                srt.setDeviceid("1");
                                srt.setSignature("blue bricks");
                                srt.setHashofdoc("digest");
                                resulttrack = srtm.addSignRequestTracking(sessionId, srt);
                            }
                        } else {
                            for (int k = 0; k < count; k++) {
                                if (tempgname[k] != null) {
                                    _groupid = Integer.parseInt(tempgname[k]);
                                }
                                stracking[k].setUserid(tempuname[k]);
                                stracking[k].setGroupid(_groupid);
                                stracking[k].setChannelid(channelId);
                                stracking[k].setDocid(s.getSrid());
                                stracking[k].setDocument(srequest.getDocument());
                                stracking[k].setDeviceid("1");
                                stracking[k].setStatus(-1);
                                stracking[k].setSignature("blue bricks");
                                resulttrack = srtm.editSignRequestTracking(sessionId, stracking[k]);
                            }
                            for (int l = count; l < stracking.length; l++) {
                                srtm.removeSRTracking(sessionId, channelId, stracking[l]);
                            }
                        }
                        resultString = "Success";
                        session.setAttribute("_pdfsignPath", "");
                         audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                                request.getRemoteAddr(),
                                channel.getName(),
                                remoteaccesslogin, operatorS.getName(),
                                new Date(), "Edit Document",
                                resultString, 0,
                                "Document Management ",
                                "",
                                "Doc Name=" + docName + " edited",
                                itemType,
                                "" + _docId);
                    } else {
                         audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                                request.getRemoteAddr(),
                                channel.getName(),
                                remoteaccesslogin, operatorS.getName(),
                                new Date(), "Edit Document",
                                resultString, 0,
                                "Document Management ",
                                "",
                                "Error::Failed to edit document",
                                itemType,
                                "" + _docId);
                    }
                } else {
                  audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                                request.getRemoteAddr(),
                                channel.getName(),
                                remoteaccesslogin, operatorS.getName(),
                                new Date(), "Edit Document",
                                resultString, 0,
                                "Document Management ",
                                "",
                                "Error::Failed to edit document",
                                itemType,
                                "" + _docId);
                }
            }
            if (result == 0) {
                _result = "success";
            } else {
                if (check != 0) {
                    _result = "error";
                    _message = "No duplication signer is not allowded";
                } else {
                    if (duplicate == 0) {
                        _result = "error";
                        _message = "epdf  Addition Failed";
                    } else {
                        _result = "error";
                        _message = " No duplicate Document Name is allowded";
                    }
                }
            }
            try {
                json.put("_result", _result);
                json.put("_message", _message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
