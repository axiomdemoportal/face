/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.signrequest;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;

/**
 *
 * @author WALE
 */
public class uploadpdfsigndoc extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(uploadpdfsigndoc.class.getName());
    final String itemType = "ESIGNER";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        log.info("Servlet started");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getName());
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        String operatorId = operatorS.getOperatorid();
        PrintWriter out = response.getWriter();
        String strError = "";
        String saveFile = "";
        String savepath = "";
        AuditManagement audit = new AuditManagement();
        String result = "success";
        String message = "File uploaded sucessfully";
        JSONObject json = new JSONObject();
        savepath = System.getProperty("catalina.home");
        if (savepath == null) {
            savepath = System.getenv("catalina.home");
        }
        savepath += System.getProperty("file.separator");
        savepath += "axiomv2-settings";
        savepath += System.getProperty("file.separator");
        savepath += "uploads";
        savepath += System.getProperty("file.separator");
        String optionalFileName = "";
        String fileName = null;
        FileItem fileItem = null;
        String[] files = new String[1];	 // file names
        String dirName = savepath;
        int retValue = 0;
        int i = 0;
        if (ServletFileUpload.isMultipartContent(request)) {
            try {
                ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                List fileItemsList = servletFileUpload.parseRequest(request);
                Iterator it = fileItemsList.iterator();
                while (it.hasNext()) {
                    FileItem fileItemTemp = (FileItem) it.next();
                    if (fileItemTemp.isFormField()) {
                        if (fileItemTemp.getFieldName().equals("filename")) {
                            optionalFileName = fileItemTemp.getString();
                        } else {
                            System.out.println("Additional fields in file upload form is  " + fileItemTemp.getFieldName());
                        }
                    } else {
                        fileItem = fileItemTemp;
                    }
                    if (fileItem != null) {
                        fileName = fileItem.getName();

                        if (!fileName.toLowerCase().endsWith("pdf")) {
                            strError = "Please select only pdf File to upload";
                            result = "error";
                            out.print("{result:'" + result + "',message:'" + strError + "'}");
                            out.flush();
                            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                                    request.getRemoteAddr(),
                                    channel.getName(),
                                    remoteaccesslogin, operatorS.getName(),
                                    new Date(), "Upload Document",
                                    result, 0,
                                    "Document Management ",
                                    "",
                                    "Error::" + strError,
                                    itemType,
                                    "");
                            return;
                        }
                        if (fileItem.getSize() == 0) {
                            strError = "Please select file to upload";
                            result = "error";
                            try {
                                json.put("result", result);
                                json.put("message", strError);
                            } catch (Exception e) {
                                log.error("Exception caught :: ",e);
                            }
                            //out.print(json);
                            out.print("{result:'" + result + "',message:'" + strError + "'}");
                            out.flush();
                            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                                    request.getRemoteAddr(),
                                    channel.getName(),
                                    remoteaccesslogin, operatorS.getName(),
                                    new Date(), "Upload Document",
                                    result, 0,
                                    "Document Management ",
                                    "",
                                    "Error::" + strError,
                                    itemType,
                                    "");
                            return;
                        }
                        if (fileItem.getSize() > 0 && fileItem.getSize() < 1024000 * 5) {
                            // size cannot be more than 65Kb. We want it light.
                            if (optionalFileName.trim().equals("")) {
                                fileName = FilenameUtils.getName(fileName);
                            } else {
                                fileName = optionalFileName;
                            }
                            files[i++] = dirName + fileName;
                            File saveTo = new File(dirName + fileName);
                            saveFile = fileName;
                            try {
                                fileItem.write(saveTo);
                                HttpSession session = request.getSession(true);
                                session.setAttribute("_pdfsignPath", saveTo.getAbsolutePath());
                            } catch (Exception e) {
                                log.error("Exception caught :: ",e);
                            }
                        } else {
                            strError = "Error: " + fileName + " size is more than 5MB. Please upload correct file size.";
                            result = "error";
                            try {
                                json.put("result", result);
                                json.put("message", strError);
                            } catch (Exception e) {
                                log.error("Exception caught :: ",e);
                            }
                            //out.print(json);
                            out.print("{result:'" + result + "',message:'" + strError + "'}");
                            out.flush();
                            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                                    request.getRemoteAddr(),
                                    channel.getName(),
                                    remoteaccesslogin, operatorS.getName(),
                                    new Date(), "Upload Document",
                                    result, 0,
                                    "Document Management ",
                                    "",
                                    "Error::" + strError,
                                    itemType,
                                    "");
                            return;
                        }
                    } else {
                        result = "error";
                        message = "Error: No File Present";
                        try {
                            json.put("result", result);
                            json.put("message", message);
                        } catch (Exception e) {
                            log.error("Exception caught :: ",e);
                        }
                        //out.print(json);
                        out.print("{result:'" + result + "',message:'" + message + "'}");
                        out.flush();
                        audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                                request.getRemoteAddr(),
                                channel.getName(),
                                remoteaccesslogin, operatorS.getName(),
                                new Date(), "Upload Document",
                                result, 0,
                                "Document Management ",
                                "",
                                "Error::" + strError,
                                itemType,
                                "");
                        return;
                    }
                }
            } catch (FileUploadException ex) {
                log.error("Exception caught :: ",ex);
            }
        } else {
            result = "error";
            message = "Error: Form post is invalid!!!";
            try {
                json.put("result", result);
                json.put("message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            //out.print(json);
            out.print("{result:'" + result + "',message:'" + message + "'}");
            out.flush();
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(), "Upload Document",
                    result, 0,
                    "Document Management ",
                    "",
                    "Error::" + strError,
                    itemType,
                    "");
            return;
        }
        String _pdfsignPath = (String) request.getSession().getAttribute("_pdfsignPath");
        if (_pdfsignPath != null) {
            strError = "File Uploaded Successfully";
            result = "success";
            try {
                json.put("filename", fileName);
                json.put("result", result);
                json.put("message", strError);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print("{filename:'" + fileName + "',result:'" + result + "',message:'" + strError + "'}");
            out.flush();
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(), "Upload Document",
                    result, 0,
                    "Document Management ",
                    "",
                    "Sucess::" + strError,
                    itemType,
                    "");
            return;
        }
        log.info("Servlet ended");
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
