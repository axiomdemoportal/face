/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.signrequest;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Signingrequest;
import com.mollatech.axiom.nucleus.db.Srtracking;
import com.mollatech.axiom.nucleus.db.connector.management.SignReqTrackingManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SignRequestManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ashish
 */
public class DownloadEpdf extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DownloadEpdf.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        response.setContentType("application/json");
        response.setHeader("Access-Control-Allow-Origin", "*");
        PrintWriter out = response.getWriter();
        int docid = -1;
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String _docid = (String) request.getParameter("_docId");
        log.debug("_docid :: "+_docid);
        String _type = (String) request.getParameter("_type");
        log.debug("_type :: "+_type);
        int type = -1;
        if (_type != null) {
            type = Integer.parseInt(_type);
        }
        if (_docid != null) {
            docid = Integer.parseInt(_docid);
        }
        if (type == 1) {
            SignRequestManagement srm = new SignRequestManagement();
            try {
                Signingrequest sr = srm.getEpdfbyId(sessionId, channel.getChannelid(), docid);
                if (srm != null) {
                    byte[] pdffile = sr.getDocument();
                    response.reset();
                    response.setContentType("application/pdf");
                    response.getOutputStream().write(pdffile, 0, pdffile.length);
                    response.getOutputStream().flush();
                }

            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
        }
        if (type == 2) {
            SignReqTrackingManagement srtm = new SignReqTrackingManagement();
            try {
                Srtracking srt = srtm.getEpdfbyId(sessionId, channel.getChannelid(), docid);
                if (srt != null) {
                    byte[] pdffile = srt.getDocument();
                    response.reset();
                    response.setContentType("application/pdf");
                    response.getOutputStream().write(pdffile, 0, pdffile.length);
                    response.getOutputStream().flush();
                }

            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
        }
        log.info("Servlet ended");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
