/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.signrequest;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Registerdevicepush;
import com.mollatech.axiom.nucleus.db.Signingrequest;
import com.mollatech.axiom.nucleus.db.Srtracking;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PushNotificationDeviceManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SignReqTrackingManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SignRequestManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;

public class Starttracking extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Starttracking.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
//    private final int Status = -1;
//    private final int enabled = -1;
    AuditManagement audit = new AuditManagement();
    final String itemType = "ESIGNER";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException {
        log.info("Servlet started");
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        String _result = "success";
        String _message = "Tracking started";
        String docName = "";
        JSONObject json = new JSONObject();
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String channelId = channel.getChannelid();
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Calendar cal = Calendar.getInstance();
        String _docId = request.getParameter("_docId");
        log.debug("_docId :: "+_docId);
        int docId = -1;
        if (_docId != null) {
            docId = Integer.parseInt(_docId);
        }
        SignRequestManagement st = new SignRequestManagement();
        Signingrequest sr = st.getEpdfbyId(sessionId, channelId, docId);
        docName = sr.getDocname();
        AXIOMStatus axStatus = null;
        int c = st.changeEpdfStatus(sessionId, channelId, docId, 1);
        if (c == 0) {
            SignReqTrackingManagement srtm = new SignReqTrackingManagement();
            Srtracking[] srt = srtm.getAllSRTrackingDetailsbydocId(sessionId, channelId, docId);
            srt[0].setStatus(0);
            srt[0].setStartedonon(new Date());
            cal.setTime(new Date());
            cal.add(Calendar.DATE, sr.getExpiryday());
            srt[0].setExpiredon(cal.getTime());
            srtm.editSignRequestTracking(sessionId, srt[0]);
            srtm.changetrackStatus(sessionId, channelId, docId, 0);
            UserManagement usmngt = new UserManagement();
            AuthUser user = null;
            user = usmngt.getUser(channelId, srt[0].getUserid());
            int pendingcountofnextsigner = srtm.getCountSRTrackingbystatus(channelId, srt[0].getUserid(), 0);
            SendNotification send = new SendNotification();
            try {
                PushNotificationDeviceManagement psManagement = new PushNotificationDeviceManagement();
                Registerdevicepush[] rDevice = psManagement.GetRegisterPushDeviceByEmailID(sessionId, channelId, user.email);
                String pushMessage = "You have " + pendingcountofnextsigner + " pending request(s)";
                for (int i = 0; i < rDevice.length; i++) {
                    if (rDevice[i].getType() == 1) {
                        axStatus = send.SendOnPush(channelId, rDevice[i].getGoogleregisterid(), user.email, pushMessage, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                    } else {
                        axStatus = send.SendOnPushIOS(channelId, rDevice[i].getGoogleregisterid(), user.email, pushMessage, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                    }
                }
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }

            if (axStatus.iStatus == 0 || axStatus.iStatus == 2) {
                json.put("_pushmessage", "Push send successfully");
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(),
                        new Date(), "Start Tracking of Document",
                        _result, 0,
                        "Document Management",
                        "",
                        "Success::Tracking started and push sent for document" + docName,
                        itemType,
                        "" + _docId);
            } else if (axStatus.iStatus == -22) {//blocked by prefix flter
                json.put("_pushmessage", "blocked by prefix flter");
                  audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(),
                        new Date(), "Start Tracking of Document",
                        _result, 0,
                        "Document Management",
                        "",
                        "Success::Tracking started and push blocked by prefix flter for document" + docName,
                        itemType,
                        "" + _docId);
            } else {
                json.put("_pushmessage", "Push send failed");
                 audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(),
                        new Date(), "Start Tracking of Document",
                        _result, 0,
                        "Document Management",
                        "",
                        "Success::Tracking started and push send failed for document" + docName,
                        itemType,
                        "" + _docId);
            }
        }
        try {
            json.put("_result", _result);
            json.put("_message", _message);
        } catch (Exception e) {
            log.error("Exception caught :: ",e);
            _result = "error";
            _message = "Could not start the tracking";
            json.put("_result", _result);
            json.put("_message", _message);
             audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(),
                        new Date(), "Start Tracking of Document",
                        _result, 0,
                        "Document Management",
                        "",
                        "Error::Tracking Could not started for document" + docName,
                        itemType,
                        "" + _docId);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(Starttracking.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(Starttracking.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
