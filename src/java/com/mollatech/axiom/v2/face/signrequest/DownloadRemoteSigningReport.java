/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.signrequest;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Remotesignature;
import com.mollatech.axiom.nucleus.db.connector.management.ChallengeResReport;
import com.mollatech.axiom.nucleus.db.connector.management.PDFSigningManagement;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mohanish
 */
public class DownloadRemoteSigningReport extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DownloadRemoteSigningReport.class.getName());
    private static int PDF_TYPE = 0;
    private static int CSV_TYPE = 1;
    private static int TXT_TYPE = 2;
    private static final int BUFSIZE = 4096;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        try {
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId :: "+sessionId);
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel.getName());
            String _channelId = channel.getChannelid();
            try {
                Remotesignature[] remoteSign = (Remotesignature[]) request.getSession().getAttribute("remoteSignReport");
                log.debug("remoteSign :: "+remoteSign);
                AuthUser[] Users = (AuthUser[]) request.getSession().getAttribute("remoteSignReportUser");
                String _format = request.getParameter("_reporttype");
                log.debug("_format :: "+_format);
                String _startDate = request.getParameter("_startDate");
                log.debug("_startDate :: "+_startDate);
                String _endDate = request.getParameter("_endDate");
                log.debug("_endDate :: "+_endDate);
                String _searchType = request.getParameter("_searchType");
                log.debug("_searchType :: "+_searchType);
                int iFormat = Integer.parseInt(_format);
                if (PDF_TYPE == iFormat) {
                    iFormat = PDF_TYPE;
                } else if (CSV_TYPE == iFormat) {
                    iFormat = CSV_TYPE;
                } else {
                    iFormat = TXT_TYPE;
                }

                String filepath = null;

                try {
                    try {
                        PDFSigningManagement cObj = new PDFSigningManagement();
                        filepath = cObj.generateReport(iFormat, remoteSign, Users, _startDate, _endDate,_channelId,_searchType);
//                        cObj = null;
                    } catch (Exception e) {
                        log.error("Exception cught ::",e);
                    }

                    //  
                    File file = new File(filepath);
                    int length = 0;
                    ServletOutputStream outStream = response.getOutputStream();
                    ServletContext context = getServletConfig().getServletContext();
                    String mimetype = context.getMimeType(filepath);

                    // sets response content type
                    if (mimetype == null) {
                        mimetype = "application/octet-stream";
                    }
                    response.setContentType(mimetype);
                    response.setContentLength((int) file.length());
                    String fileName = (new File(filepath)).getName();

                    // sets HTTP header
                    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

                    byte[] byteBuffer = new byte[BUFSIZE];
                    DataInputStream in = new DataInputStream(new FileInputStream(file));

                    // reads the file's bytes and writes them to the response stream
                    while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                        outStream.write(byteBuffer, 0, length);
                    }

                    in.close();
                    outStream.close();
                    file.delete();

                } catch (Exception ex) {
                    // TODO handle custom exceptions here
                    log.error("Exception cught ::",ex);
                }

            } catch (Exception ex) {
                log.error("Exception cught ::",ex);
            }

        } finally {
            //  out.close();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
