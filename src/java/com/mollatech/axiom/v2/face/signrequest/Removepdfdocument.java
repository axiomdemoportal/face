/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.signrequest;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Signingrequest;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SignRequestManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class Removepdfdocument extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Removepdfdocument.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    AuditManagement audit = new AuditManagement();
    final String itemType = "ESIGNER";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        response.setContentType("application/json");

        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Epdf Document Removed Successfully!!!";

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String channelId = channel.getChannelid();

        String docid = request.getParameter("_docid");
        log.debug("docid :: "+docid);
        int did = -1;
        if (docid != null) {
            did = Integer.parseInt(docid);
        }
        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS);
        String operatorId = operatorS.getOperatorid();
        JSONObject json = new JSONObject();
        int retValue = -1;
        SignRequestManagement srm = new SignRequestManagement();
        Signingrequest sr = srm.getEpdfbyId(sessionId, channelId, did);

        if (sr != null) {
            retValue = srm.deleteEpdfdoc(sessionId, channelId, did);
            log.debug("deleteEpdfdoc :: "+retValue);
        }
        String resultString = "failure";
        if (retValue == 0) {
            resultString = "Success";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(), "Remove Document",
                    resultString, 0,
                    "Document Management ",
                    "",
                    "Success::  document Removed",
                    itemType,
                    "" + docid);

        } else if (retValue != 0) {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(), "Remove Document",
                    resultString, 0,
                    "Document Management ",
                    "",
                    "Error::Failed to Remove document",
                    itemType,
                    "" + docid);
        }
        if (retValue == 0) {
            result = "success";
        } else {
            result = "error";
            message = "Epdf Document Removal Failed!!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
