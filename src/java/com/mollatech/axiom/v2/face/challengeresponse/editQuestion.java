/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.challengeresponse;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.QuestionsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class editQuestion extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editQuestion.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final String itemtype = "CHALLENGERESPONSE";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel ::" + channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
         log.debug("sessionId ::" + sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin::" + remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        
        String operatorId = operatorS.getOperatorid();
        log.debug("operatorId::" + operatorId);
        String _qid = request.getParameter("_eqid");
        log.debug("_qid::" + _qid);
        String _question = request.getParameter("_equestion");
        log.debug("_question::" + _question);
        String _ch_weightage = request.getParameter("_e_weightage");
        log.debug("_ch_weightage::" + _ch_weightage);
        String _ch_status = request.getParameter("_e_status");
        log.debug("_ch_status::" + _ch_status);
        int status = Integer.parseInt(_ch_status);
        int _weightage = Integer.parseInt(_ch_weightage);
        
    
        String result = "success";
        String message = "Question edited successfully....";

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        if (StringUtils.isWhitespace(_question)) {
            result = "error";
            message = "Question Can't be blank!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        if (_question == null || _ch_weightage == null) {
            result = "error";
            message = "Question edit failure!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);

            }
            out.print(json);
            out.flush();
            return;
        }

        int retValue = -1;

        QuestionsManagement mgmt = new QuestionsManagement();
        AuditManagement audit = new AuditManagement();
        retValue = mgmt.EditQuestion(sessionId, channel.getChannelid(), _qid, _question, _weightage, status);
        log.debug("EditQuestion::" + retValue);
        String resultString = "ERROR";

        if (retValue == 0) {
            resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Edit Question", resultString, retValue,
                    "Question Management", "", "QuestionId =" + _qid
                    + "Question =" + _question + ",Weightage = " + _weightage + ",Status =" + status,
                    itemtype, _qid);
        }

        if (retValue != 0) {
            result = "ERROR";

            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Add Question", resultString, retValue,
                    "Question Management", "", "QuestionId =" + _qid
                    + "Question =" + _question + ",Weightage = " + _weightage + ",Status =" + status,
                    itemtype, _qid);
        }

        if (retValue != 0) {
            result = "error";
            message = "Question edit failure!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            
            log.error("exception caught :: ",e);

        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
