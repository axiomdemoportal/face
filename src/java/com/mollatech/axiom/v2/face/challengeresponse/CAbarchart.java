/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.challengeresponse;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.face.common.bar;
import com.mollatech.axiom.nucleus.db.Audit;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Txdetails;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TxManagement;
import static com.mollatech.axiom.v2.face.challengeresponse.CAdonutchart.SUCCESS;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mohanish
 */
public class CAbarchart extends HttpServlet {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CAbarchart.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        PrintWriter out = response.getWriter();
        int success = 0;
        int failure = 0;

        try {
            response.setContentType("application/json");

            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel::" + channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId ::" + sessionId);
            String channelID = channel.getChannelid();
            log.debug("channelID::" + channelID);
            String _startdate = request.getParameter("_startDate");
            log.debug("_startdate::" + _startdate);
            String _enddate = request.getParameter("_endDate");
            log.debug("_enddate::" + _enddate);
            String _itemType = request.getParameter("_itemType");
            log.debug("_itemType::" + _itemType);
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date startDate = null;

            if (_startdate != null && !_startdate.isEmpty()) {
                startDate = (Date) formatter.parse(_startdate);
                startDate.setHours(0);
                startDate.setMinutes(0);
                startDate.setSeconds(0);
            }
            Date endDate = null;
            if (_enddate != null && !_enddate.isEmpty()) {
                endDate = (Date) formatter.parse(_enddate);
                endDate.setHours(23);
                endDate.setMinutes(59);
                endDate.setSeconds(59);
            }
            ArrayList<bar> sample = new ArrayList<bar>();
            AuditManagement au = new AuditManagement();
            Audit[] audit = au.getAuditResultCount(channelID, startDate, endDate, _itemType);
            if (audit != null) {
                for (int i = 0; i < audit.length; i++) {
                    if (audit[i].getResultcode() == SUCCESS) {
                        success++;
                    } else {
                        failure++;
                    }
                }
            }
            sample.add(new bar(success, "Success"));
            sample.add(new bar(failure, "Failure"));

            Gson gson = new Gson();

            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<bar>>() {
            }.getType());

            JsonArray jsonArray = element.getAsJsonArray();

            out.print(jsonArray);

        } catch (Exception e) {
            log.error("exception caught :: ", e);
        } finally {
            out.close();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
