/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.challengeresponse;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Questions;
import com.mollatech.axiom.nucleus.db.connector.management.QuestionsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class loadQuestionDetails extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadQuestionDetails.class.getName());

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel::" + channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId ::" + sessionId);
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String _qid = request.getParameter("_qid");
        log.debug("_qid ::" + _qid);
        
        
        
         if ( AxiomProtect.CheckEnforcementFor(AxiomProtect.USER_CHALLENGERESPONSE) != 0 ) {
            String result = "error";
            String message = "This feature is not available in this license!!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
       
        
        response.setContentType("application/json");
        try {
            Questions question = null;
            QuestionsManagement qObj = new QuestionsManagement();
            
            
            question = qObj.getQuestionById(sessionId, channel.getChannelid(), _qid);
            json.put("_qid", _qid);
            json.put("_equestion", question.getQuestion());
            json.put("_e_weightage", question.getWeightage());
            json.put("_e_status", question.getStatus());
            json.put("_result", "success");

        } catch (Exception ex) {
            log.error("exception caught :: ",ex);
            // TODO handle custom exceptions here
            try { json.put("_result", "error");
            json.put("_message", ex.getMessage());
            } catch(Exception e){
                log.error("exception caught :: ",e);
            
        }
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
