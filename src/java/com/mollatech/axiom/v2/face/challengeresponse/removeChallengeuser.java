/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.challengeresponse;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Questionsandanswers;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ChallengeResponsemanagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class removeChallengeuser extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(removeChallengeuser.class.getName());

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final String itemtype = "CHALLENGERESPONSE";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
         log.debug("channel::" + channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId::" + sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin ::" + remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS::" + operatorS.getOperatorid());

        response.setContentType("application/json");
        String _userid = request.getParameter("_userid");
        log.debug("_userid::" + _userid);
        String result = "success";
        String message = "Challenge removed successfully....";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
     
        if (_userid == null) {
            result = "error";
            message = "Fill all Details!!";
            try { json.put("_result", result);
            json.put("_message", message);}catch(Exception e){
                log.error("exception caught :: ",e);
            
        }
            out.print(json);
            out.flush();
            return;
        }
        int retValue = -1;
        
        ChallengeResponsemanagement cManagement = new ChallengeResponsemanagement();
        Questionsandanswers QandA = cManagement.getRegisterUser(sessionId, channel.getChannelid(), _userid);
        UserManagement uManagement = new UserManagement();
        AuthUser olduser = uManagement.getUser(sessionId, channel.getChannelid(), _userid);
        AuditManagement audit = new AuditManagement();
        retValue = cManagement.RemoveChallengeuser(sessionId, channel.getChannelid(), _userid);
        log.debug("RemoveChallengeuser::" + retValue);
        String resultString = "ERROR";
        
        if (retValue == 0) {
            resultString = "Success";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Remove Challenge", resultString, retValue,
                    "Challenge Management", "User ID=" + QandA.getUserid() + ",Name=" + olduser.getUserName()
                    + ",Phone=" + olduser.getPhoneNo() + ",Email=" + olduser.getEmail(),
                    "Removed", itemtype, _userid);
        }

        if (retValue != 0) {

            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Remove Challenge", resultString, retValue,
                    "Challenge Management", "User=" + olduser.getUserId() + ",Name=" + olduser.getUserName()
                    + ",Phone=" + olduser.getPhoneNo() + ",Email=" + olduser.getEmail(),
                    "User ID=" + olduser.getUserId() + ",Name = " + olduser.getUserName()
                    + "Phone=" + olduser.getPhoneNo() + ",Email=" + olduser.getEmail(),
                    itemtype, _userid);

            result = "error";
            message = "Cannot remove Challenge Response for user!!";
            out.print(json);
            out.flush();
            return;
        }
        try {
            json.put("_result", result);
            json.put("_message", message);

        }catch(Exception e){
            log.error("exception caught :: ",e);
            
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
