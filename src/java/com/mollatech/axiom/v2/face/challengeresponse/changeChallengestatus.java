/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.challengeresponse;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Questionsandanswers;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ChallengeResponsemanagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class changeChallengestatus extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(changeChallengestatus.class.getName());

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;
    final int LOCKED_STATUS = -1;
    final String itemtype = "CHALLENGERESPONSE";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel::" + channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId::" + sessionId);

        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin ::" + remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS::" + operatorS.getOperatorid());

        String _userid = request.getParameter("_userid");
        log.debug("_userid ::" + _userid);
        String _status = request.getParameter("_status");
        log.debug("_status ::" + _status);
        int status = Integer.parseInt(_status);
        String result = "success";
        String message = " status updated!!!";
        String _value = "Active";
        
   
        if (status == ACTIVE_STATUS) {
            _value = "Active ";
        } else if (status == SUSPEND_STATUS) {
            _value = "Suspended ";
        }
        message = _value + message;
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
       
        int retValue = -1;
        ChallengeResponsemanagement cManagement = new ChallengeResponsemanagement();
        ChallengeResponsemanagement mgmt = new ChallengeResponsemanagement();
        Questionsandanswers QandA = mgmt.getRegisterUser(sessionId, channel.getChannelid(),_userid);
        retValue = cManagement.ChangeStatus(sessionId, channel.getChannelid(), _userid, status);
        log.debug("ChangeStatus::"+ retValue);
        AuditManagement audit = new AuditManagement();
              
        String resultString = "ERROR";
        
        int istatus = QandA.getStatus();
            String strstaus = "Removed";
            if(istatus == ACTIVE_STATUS){
                strstaus = "Active";
            }else if(istatus ==SUSPEND_STATUS){
                strstaus = "Suspended";
            }else if(istatus == LOCKED_STATUS){
                strstaus = "Locked";
            
            }
            
            
            
        if (retValue == 0) {
            resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), 
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Change Status", resultString, retValue,
                    "Challenge Response Management", "Old Status=" + strstaus, "New Status=" + _value,
                    itemtype, QandA.getQaid().toString());

        }
        if (retValue != 0) {
            result = "ERROR";
            message = "Status update failed!!!";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), 
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Change Status", resultString, retValue,
                    "Challenge Response Managementt", "Old Status=" + strstaus, "New Status=" + _value,
                    itemtype, QandA.getQaid().toString());
            try {json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);
            }catch(Exception e){
                log.error("exception caught :: ",e);
            
        }
            out.print(json);
            out.flush();
            return;
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);

        } catch(Exception e){
            log.error("exception caught :: ",e);
            
        }finally {
            out.print(json);
            out.flush();

        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
