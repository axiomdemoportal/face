/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.srtracking;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Registerdevicepush;
import com.mollatech.axiom.nucleus.db.Signingrequest;
import com.mollatech.axiom.nucleus.db.Srtracking;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PushNotificationDeviceManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SignReqTrackingManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SignRequestManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author AMOL
 */
public class SendNotifytoUser extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SendNotifytoUser.class.getName());
    final String itemType = "ESIGNER";
    AuditManagement audit = new AuditManagement();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String channelId = channel.getChannelid();
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String _trackid = request.getParameter("_trackId");
        log.debug("_trackid :: "+_trackid);
        String _status = request.getParameter("_status");
        log.debug("_status :: "+_status);
        String _docId = request.getParameter("_docId");
        log.debug("_docId :: "+_docId);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getName());
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        int trackid = -1;
        if (_trackid != null) {
            trackid = Integer.parseInt(_trackid);
        }
        int docid = -1;
        if (_docId != null) {
            docid = Integer.parseInt(_docId);
        }
        String Status = "";
        int status = -1;
        if (_status != null) {
            status = Integer.parseInt(_status);
            if (status == 0) {
                Status = "Active";
            } else if (status == -1) {
                Status = "Pending";
            } else if (status == -2) {
                Status = "Suspended";
            } else if (status == 2) {
                Status = "Rejected";
            } else if (status == 1) {
                Status = "Approved";
            } else if (status == 3) {
                Status = "Completed";
            } else if (status == -3) {
                Status = "Expired";
            }
        }
        SignRequestManagement srm = new SignRequestManagement();
        Signingrequest srequest = srm.getEpdfbyId(channelId, docid);
        String documentname = "";
        if (srequest != null) {
            documentname = srequest.getDocname();
        }
        SignReqTrackingManagement srtm = new SignReqTrackingManagement();
        AXIOMStatus axStatus = null;
        SendNotification send = new SendNotification();
        Srtracking sr = srtm.getSignRequestTracking(sessionId, channelId, trackid);
        String userid = sr.getUserid();
        UserManagement um = new UserManagement();
        AuthUser user = um.getUser(sessionId, channelId, userid);
        Registerdevicepush[] rDevice = null;
        try {
            PushNotificationDeviceManagement psManagement = new PushNotificationDeviceManagement();
            rDevice = psManagement.GetRegisterPushDeviceByEmailID(sessionId, channelId, user.email);
            String pushMessage = "Your document " + documentname + " for signing is currently " + Status + ", please check documents..";
            if (rDevice != null) {
                for (int i = 0; i < rDevice.length; i++) {
                    if (rDevice[i].getType() == 1) {
                        axStatus = send.SendOnPush(channelId, rDevice[i].getGoogleregisterid(), user.email, pushMessage, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                    } else {
                        axStatus = send.SendOnPushIOS(channelId, rDevice[i].getGoogleregisterid(), user.email, pushMessage, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                    }
                }
            }
        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        }

        try {
            if (rDevice != null) {
                if (axStatus.iStatus == 0 || axStatus.iStatus == 2) {
                    json.put("_result", "success");
                    json.put("_pushmessage", "Push send successfully");
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(),
                            remoteaccesslogin, operatorS.getName(),
                            new Date(), "Send Notification of Document",
                            "success", 0,
                            "Document Management",
                            "",
                            "Success:: Push send successfully",
                            itemType,
                            "" + _docId);
                } else if (axStatus.iStatus == -22) {//blocked by prefix flter
                    json.put("_result", "error");
                    json.put("_pushmessage", "blocked by prefix flter");
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(),
                            remoteaccesslogin, operatorS.getName(),
                            new Date(), "Send Notification of Document",
                            "success", 0,
                            "Document Management",
                            "",
                            "Error:: blocked by prefix flter",
                            itemType,
                            "" + _docId);
                } else {
                    json.put("_result", "error");
                    json.put("_pushmessage", "Push send failed");
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(),
                            remoteaccesslogin, operatorS.getName(),
                            new Date(), "Send Notification of Document",
                            "success", 0,
                            "Document Management",
                            "",
                            "Error:: Push send failed",
                            itemType,
                            "" + _docId);
                }
            } else {
                json.put("_result", "error");
                json.put("_pushmessage", "Push send failed");
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(),
                        new Date(), "Send Notification of Document",
                        "success", 0,
                        "Document Management",
                        "",
                        "Error:: Push send failed",
                        itemType,
                        "" + _docId);
            }
        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
