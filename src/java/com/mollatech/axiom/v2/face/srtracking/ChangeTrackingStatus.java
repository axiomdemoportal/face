/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.srtracking;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Registerdevicepush;
import com.mollatech.axiom.nucleus.db.Srtracking;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PushNotificationDeviceManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SignReqTrackingManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class ChangeTrackingStatus extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ChangeTrackingStatus.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private int Status = -1;

    final int COMPLETED_STATUS = 3;
    final int REJECTED_STATUS = 2;
    final int APPROVED_STATUS = 1;
    final int ACTIVE_STATUS = 0;
    final int PENDING_STATUS = -1;
    final int SUSPEND_STATUS = -2;
    final int EXPIRED_STATUS = -3;
    final String itemType = "ESIGNER";
    AuditManagement audit = new AuditManagement();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getName());
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        String operatorId = operatorS.getOperatorid();
        String _value = "Active";
        String _status = request.getParameter("_status");
        log.debug("_status :: "+_status);
        String _trackid = request.getParameter("_trackid");
        log.debug("_trackid :: "+_trackid);
        int status = -1, trackid = -1;
        if (!_status.equals("")) {
            status = Integer.parseInt(_status);
        }
        if (_trackid != null) {
            trackid = Integer.parseInt(_trackid);
        }
        String _result = "";
        String _message = "Status Updated successfully";
        int result = 0;
        String resultString = "failure";
//        change status message
        SignReqTrackingManagement srtm = new SignReqTrackingManagement();
        Srtracking srtrack = srtm.getSignRequestTracking(sessionId, channel.getChannelid(), trackid);
        int istatus = srtrack.getStatus();
        String strstatus = null;
        if (istatus == ACTIVE_STATUS) {
            strstatus = "Active";
        } else if (istatus == SUSPEND_STATUS) {
            strstatus = "Suspended";
        } else if (istatus == PENDING_STATUS) {
            strstatus = "Pending";
        } else if (istatus == APPROVED_STATUS) {
            strstatus = "Approved";
        } else if (istatus == REJECTED_STATUS) {
            strstatus = "Rejected";
        } else if (istatus == COMPLETED_STATUS) {
            strstatus = "Completed";
        } else if (istatus == EXPIRED_STATUS) {
            strstatus = "Expired";
        }

        String strstatus1 = "Removed";
        if (istatus == ACTIVE_STATUS) {
            strstatus1 = "Active";
        } else if (istatus == SUSPEND_STATUS) {
            strstatus1 = "Suspended";
        } else if (istatus == PENDING_STATUS) {
            strstatus1 = "Pending";
        } else if (istatus == APPROVED_STATUS) {
            strstatus1 = "Approved";
        } else if (istatus == REJECTED_STATUS) {
            strstatus1 = "Rejected";
        } else if (istatus == COMPLETED_STATUS) {
            strstatus1 = "Completed";
        } else if (istatus == EXPIRED_STATUS) {
            strstatus1 = "Expired";
        }
        //        change status message
        if (istatus == status) {
            try {
                _result = "error";
                _message = "Status Already " + strstatus;
                json.put("_result", result);
                json.put("_message", _message);
                json.put("_value", _value);
            } catch (Exception e) {
               log.error("Exception caught :: ",e);
            } finally {
                out.print(json);
                out.flush();
            }
            return;
        }
        AXIOMStatus axStatus = new AXIOMStatus();
        String channelid = channel.getChannelid();
        result = srtm.changetrackStatus(sessionId, channel.getChannelid(), trackid, status);
        String UserId = srtrack.getUserid();
        UserManagement um = new UserManagement();
        AuthUser au = um.getUser(sessionId, channelid, UserId);
        if (result != -1) {
            SendNotification send = new SendNotification();
            try {
                PushNotificationDeviceManagement psManagement = new PushNotificationDeviceManagement();
                Registerdevicepush[] rDevice = psManagement.GetRegisterPushDeviceByEmailID(sessionId, channelid, au.getEmail());
                String pushMessage = "You have new pending request";
                for (int i = 0; i < rDevice.length; i++) {
                    if (rDevice[i].getType() == 1) {
                        axStatus = send.SendOnPush(channelid, rDevice[i].getGoogleregisterid(), au.getEmail(), pushMessage, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                    } else {
                        axStatus = send.SendOnPushIOS(channelid, rDevice[i].getGoogleregisterid(), au.getEmail(), pushMessage, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                    }
                }
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            try {
                if (axStatus.iStatus == 0 || axStatus.iStatus == 2) {
                    json.put("_pushmessage", "Push send successfully");
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(),
                            remoteaccesslogin, operatorS.getName(),
                            new Date(), "Start Tracking of Document",
                            _result, 0,
                            "Document Management",
                            "",
                            "Success::Tracking started and push sent ",
                            itemType,
                            "" + _trackid);
                } else if (axStatus.iStatus == -22) {//blocked by prefix flter
                    json.put("_pushmessage", "blocked by prefix flter");
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(),
                            remoteaccesslogin, operatorS.getName(),
                            new Date(), "Start Tracking of Document",
                            _result, 0,
                            "Document Management",
                            "",
                            "Success::Tracking started and push blocked by prefix ",
                            itemType,
                            "" + _trackid);
                } else {
                    json.put("_pushmessage", "Push send failed");
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(),
                            remoteaccesslogin, operatorS.getName(),
                            new Date(), "Start Tracking of Document",
                            _result, 0,
                            "Document Management",
                            "",
                            "Success::Tracking started and push send failed",
                            itemType,
                            "" + _trackid);
                }
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            resultString = "Success";
            _result = "success";
            _message = "Status activated and expiry time increased by " + result + " day/s";
        } else {
            _result = "error";
            _message = "Error status not updated";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(), "Start Tracking of Document",
                    _result, 0,
                    "Document Management",
                    "",
                    "Error::Tracking Could not started",
                    itemType,
                    "" + _trackid);
        }
        try {
            json.put("_result", _result);
            json.put("_message", _message);
        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
