/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.signreqCategory;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Doccategory;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.CategoryManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author administrator
 */
public class removeCategory extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(removeCategory.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final String itemType = "ESIGNER";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        log.info("Servlet started");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            response.setContentType("application/json");
            AuditManagement audit = new AuditManagement();
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId :: "+sessionId);
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("remoteaccesslogin :: "+remoteaccesslogin);
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operatorS :: "+operatorS.getName());
            String _categoryid = request.getParameter("_cid");
            log.debug("_categoryid :: "+_categoryid);
            int id = Integer.parseInt(_categoryid);
            String result = "success";
            String message = "Document Category deleted successfully";
            JSONObject json = new JSONObject();
            int retValue = -1;
            CategoryManagement cManagement = new CategoryManagement();
            Doccategory doc = cManagement.getCategorybyName(sessionId, channel.getChannelid(), _categoryid);
            retValue = cManagement.removeCategorybyID(sessionId, channel.getChannelid(), id);
            if (retValue == -1) {
                result = "error";
                message = "Category could not deleted!!";
            }
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            } finally {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(),
                        new Date(), "Remove Category",
                        result, 0,
                        "Document Management ",
                        "",
                        message,
                        itemType,
                        "" + id);
                out.print(json);
                out.flush();
                log.info("Servlet ended");
                return;
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
