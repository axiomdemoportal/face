/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.signreqCategory;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.CategoryManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author administrator
 */
public class addCategory extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(addCategory.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final String itemType = "ESIGNER";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        response.setContentType("application/json");
        AuditManagement audit = new AuditManagement();
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getName());
        String _category_name = request.getParameter("_Name");
        log.debug("_category_name :: "+_category_name);
        String _note = request.getParameter("_Note");
        log.debug("_note :: "+_note);
        String _status = request.getParameter("_Status");
        log.debug("_status :: "+_status);
        int status = Integer.parseInt(_status);
        String result = "success";
        String message = "Category added successfully....";
        boolean spaces = false;
        if (_category_name != null) {
            for (int i = 0; i < _category_name.length(); i++) {
                if (Character.isWhitespace(_category_name.charAt(i))) {
                    spaces = true;
                }
            }
        }
        //end 
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(_category_name);
        boolean b = m.find();
        Pattern p1 = Pattern.compile("[0-9]", Pattern.CASE_INSENSITIVE);
        Matcher m1 = p1.matcher(_category_name);
        boolean b1 = m1.find();
//        category name validation
        if (_category_name == null) {
            result = "error";
            message = "Category name should not Empty !!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("Exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(), "Add Document Category",
                    result, 0,
                    "Document Management ",
                    "Category Name =" + _category_name + ", Category Note = " + _note,
                    "Error:: Category name should not Empty",
                    itemType,
                    "");
            return;
        } else if (_category_name != null) {
            if (_category_name.isEmpty()) {
                result = "error";
                message = "Category name should not Empty !!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception ex) {
                    log.error("Exception caught :: ",ex);
                }
                out.print(json);
                out.flush();
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(),
                        new Date(), "Add Document Category",
                        result, 0,
                        "Document Management ",
                        "Category Name =" + _category_name + ", Category Note = " + _note,
                        "Error:: Category name should not Empty",
                        itemType,
                        "");
                return;
            } else if (_category_name.length() < 3) {
                result = "error";
                message = "Category name should have atleast 3 characters !!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception ex) {
                    log.error("Exception caught :: ",ex);
                }
                out.print(json);
                out.flush();
              audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(), "Add Document Category",
                    result, 0,
                    "Document Management ",
                      "Category Name =" + _category_name + ", Category Note = " + _note,
                    "Error:: Category name should have atleast 3 characters",
                    itemType,
                    "");
                return;
            } else if (_category_name.length() > 20) {
                result = "error";
                message = "Category name should not greater than 20 characters!!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception ex) {
                    log.error("Exception caught :: ",ex);
                }
                out.print(json);
                out.flush();
                 audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(), "Add Document Category",
                    result, 0,
                    "Document Management ",
                      "Category Name =" + _category_name + ", Category Note = " + _note,
                    "Error:: Category name should not greater than 20 characters",
                    itemType,
                    "");
                return;
            }
        }
//        end category name
//        category note validation
        if (_note == null) {
            result = "error";
            message = "Category note should not empty !!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("Exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(), "Add Document Category",
                    result, 0,
                    "Document Management ",
                      "Category Name =" + _category_name + ", Category Note = " + _note,
                    "Error:: Category note should not empty",
                    itemType,
                    "");
            return;
        } else if (_note != null) {
            if (_note.isEmpty()) {
                result = "error";
                message = "Category note should not empty !!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception ex) {
                    log.error("Exception caught :: ",ex);
                }
                out.print(json);
                out.flush();
               audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(), "Add Document Category",
                    result, 0,
                    "Document Management ",
                      "Category Name =" + _category_name + ", Category Note = " + _note,
                    "Error:: Category note should not empty",
                    itemType,
                    "");
                return;
            } else if (_note.length() < 10) {
                result = "error";
                message = "Category note should have at least 10 characters !!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception ex) {
                    log.error("Exception caught :: ",ex);
                }
                out.print(json);
                out.flush();
               audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(), "Add Document Category",
                    result, 0,
                    "Document Management ",
                      "Category Name =" + _category_name + ", Category Note = " + _note,
                    "Error:: Category note should have at least 10 characters",
                    itemType,
                    "");
                return;
            } else if (_note.length() > 30) {
                result = "error";
                message = "Category note should not be greater than 30 characters!!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception ex) {
                    log.error("Exception caught :: ",ex);
                }
                out.print(json);
                out.flush();
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(), "Add Document Category",
                    result, 0,
                    "Document Management ",
                      "Category Name =" + _category_name + ", Category Note = " + _note,
                    "Error:: Category note should not be greater than 30 characters",
                    itemType,
                    "");
                return;
            }
        } //        end category note
        else if (b) {
            result = "error";
            message = "Name should not contain any symbols!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("Exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(), "Add Document Category",
                    result, 0,
                    "Document Management ",
                      "Category Name =" + _category_name + ", Category Note = " + _note,
                    "Error:: Name should not contain any symbols",
                    itemType,
                    "");
            return;
        } else if (b1) {
            result = "error";
            message = "Name should not contain only digits!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("Exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
           audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(), "Add Document Category",
                    result, 0,
                    "Document Management ",
                      "Category Name =" + _category_name + ", Category Note = " + _note,
                    "Error:: Name should not contain only digits",
                    itemType,
                    "");
            return;
        }

        CategoryManagement uManagement = new CategoryManagement();
        int retValue = uManagement.CreateCategory(sessionId, channel.getChannelid(), _category_name, status, _note);
        String resultString = "Failure";
        if (retValue == 0) {
            resultString = "Success";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(), "Add Document Category",
                    result, 0,
                    "Document Management ",
                      "Category Name =" + _category_name + ", Category Note = " + _note,
                    "Success:: Document Category added successfully",
                    itemType,
                    "");
        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
