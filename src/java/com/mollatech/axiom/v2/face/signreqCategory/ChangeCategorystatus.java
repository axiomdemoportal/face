/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.signreqCategory;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Doccategory;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Signingrequest;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.CategoryManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SignRequestManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author administrator
 */
public class ChangeCategorystatus extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ChangeCategorystatus.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;
    final int DOC_ACTIVE_STATUS = 1;
    final String itemType = "ESIGNER";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        response.setContentType("application/json");
        AuditManagement audit = new AuditManagement();
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getName());
        String _res = request.getParameter("_categoryid");
        log.debug("_res :: "+_res);
        int _categoryid = Integer.parseInt(_res);
        String _status = request.getParameter("_status");
        log.debug("_status :: "+_status);
        int status = Integer.parseInt(_status);
        String result = "success";
        String message = " status updated!!!";
        String _value = "Active";

        if (status == ACTIVE_STATUS) {
            _value = "Active ";
        } else if (status == SUSPEND_STATUS) {
            _value = "Suspended ";
        }
        message = _value + message;
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        if (_res == null || _status == null) {
            result = "error";
            message = "Unable to update status!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(), "Change Document Category Status",
                    result, 0,
                    "Document Management ",
                    "",
                    "Error:: " + message,
                    itemType,
                    "" + _categoryid);
            return;
        }
        int retValue = -1;
//        GroupManagement rmngt = new GroupManagement();
        CategoryManagement categorymngt = new CategoryManagement();
        Doccategory categories = categorymngt.getCategoryDetail(sessionId, channel.getChannelid(), _categoryid);
        if (categories == null) {
            result = "error";
            message = "Unable to find the Category!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(), "Change Document Category Status",
                    result, 0,
                    "Document Management ",
                    "",
                    "Error:: " + message,
                    itemType,
                    "" + _categoryid);
            return;
        }

        int istatus = categories.getStatus();
        String strstaus = "Removed";
        if (istatus == ACTIVE_STATUS) {
            strstaus = "Active";
        } else if (istatus == SUSPEND_STATUS) {
            strstaus = "Suspended";
        }

        String strstaus1 = "Removed";
        if (status == ACTIVE_STATUS) {
            strstaus1 = "Active";
        } else if (status == SUSPEND_STATUS) {
            strstaus1 = "Suspended";
        }
        boolean document_active = false;
        if (status == SUSPEND_STATUS) {
            SignRequestManagement srmngt = new SignRequestManagement();
            Signingrequest[] doclist = srmngt.getdocsbyCatId(channel.getChannelid(), _categoryid);
            if (doclist != null) {
                if (doclist.length != 0) {
                    for (int i = 0; i < doclist.length; i++) {
                        if (doclist[i].getStatus() == DOC_ACTIVE_STATUS) {
                            document_active = true;
                            break;
                        }
                    }
                }
            }
        }
        if (istatus == status) {
            try {
                result = "error";
                message = "Status Already " + strstaus;
                json.put("_result", result);
                json.put("_message", message);
                json.put("_value", _value);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            } finally {
                out.print(json);
                out.flush();
            }
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(), "Change Document Category Status",
                    result, 0,
                    "Document Management ",
                    "Category Name =" + categories.getCategoryname() + ",Old Category Status = " + strstaus,
                    "Error:: Category Status Already " + _value,
                    itemType,
                    "" + _categoryid);
            return;
        }
        if (document_active == false) {
            retValue = categorymngt.ChangeCategoryStatus(sessionId, channel.getChannelid(), _categoryid, status);
        } else {
            result = "error";
            message = " Unable to change status, document associated with the category is active!!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(),
                    new Date(), "Change Document Category Status",
                    result, 0,
                    "Document Management ",
                    "Category Name =" + categories.getCategoryname() + ",Old Category Status = " + strstaus,
                    "Success:: Category Status ChangedCategory Name =" + categories.getCategoryname() + " Category Status = " + _value,
                    itemType,
                    "" + _categoryid);
        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
