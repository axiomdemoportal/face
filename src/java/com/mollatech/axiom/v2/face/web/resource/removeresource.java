/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.axiom.v2.face.web.resource;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.ResourceManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author WALE
 */
public class removeresource extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(removeresource.class.getName());
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");    
        log.debug("operatorS :: "+operatorS.getName());
        response.setContentType("application/json");
        String _res= request.getParameter("_resid");
        log.debug("_res :: "+_res);
        int _resid=Integer.parseInt(_res);
        String result = "success";
        String message = "Resource removed successfully....";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
               
        if (_res == null) {
            result = "error";
            message = "Fill all Details!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){log.error("Exception caught :: ",e);}
            out.print(json);
            out.flush();
            return;
        }
        else{                 
        int retValue = -1;
            ResourceManagement rmngt=new ResourceManagement();
             retValue = rmngt.DeleteResource(sessionId, channel.getChannelid(), _resid);
           if(retValue==0)
        {
            result = "error";
            message = "Resources cannot be removed !!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;              
        }           
        }
         try {
            json.put("_result", result);
            json.put("_message", message);
            
        }catch(Exception e){
            log.error("Exception caught :: ",e);
        }
         finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
