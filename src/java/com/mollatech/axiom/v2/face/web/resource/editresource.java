/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.web.resource;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.ResourceManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class editresource extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editresource.class.getName());
//    static final int PHONENUMBER = 2;
//    static final int EMAILID = 3;
    final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;
    final int LOCKED_STATUS = -1;
    final int REMOVE_STATUS = -99;


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet satrted");
        response.setContentType("application/json");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getName());
        String _resource_id=request.getParameter("_resid");
        log.debug("_resource_id :: "+_resource_id);
        int _resid=Integer.parseInt(_resource_id);
        String _resource_name = request.getParameter("_Name");
        log.debug("_resource_name :: "+_resource_name);
        String _weburl = request.getParameter("_Weburl");
        log.debug("_weburl :: "+_weburl);
        String saveto=request.getSession().getAttribute("_savepath").toString();
        log.debug("saveto :: "+saveto);
        String _imageurl = saveto;        
        request.getSession().setAttribute("_savepath",null);         
        String _status = request.getParameter("_Status");
        log.debug("_status :: "+_status);
//        int _status=Integer.parseInt(status);
        
        String _group = request.getParameter("_groupName");
        log.debug("_group :: "+_group);
        String result = "success";
        String message = "Resource edited successfully....";
//        int groupid = 0;
//        if (_user_group != null) {
//            groupid = Integer.parseInt(_user_group);
//        }
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        //amol
        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(_resource_name);
        boolean b = m.find();
        Pattern p1 = Pattern.compile("[0-9]", Pattern.CASE_INSENSITIVE);
        Matcher m1 = p1.matcher(_resource_name);
        boolean b1 = m1.find();
        boolean spaces = false;
        if (_resource_name != null) {
            for (int i = 0; i < _resource_name.length(); i++) {
                if (Character.isWhitespace(_resource_name.charAt(i))) {
                    spaces = true;
                }
            }
        }
        //end amol

        if (_resource_name == null || _weburl == null || _imageurl == null
                || _resource_name.isEmpty() == true || _weburl.isEmpty() == true || _imageurl.isEmpty() == true) {
            result = "error";
            message = "Fill all Details!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        // amol
        else if (_resource_name == null) {
            result = "error";
            message = "Name could not Empty !!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("Exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        } else if (_resource_name.length() < 3) {
            result = "error";
            message = "Name should be more than 3 characters!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("Exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        } else if (b) {
            result = "error";
            message = "Name should not contain any symbols!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
               log.error("Exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        } 
//        else if (b1) {
//            result = "error";
//            message = "Name should not contain only digits!!!";
//            try {
//                json.put("_result", result);
//                json.put("_message", message);
//            } catch (Exception ex) {
//                log.error("Exception caught :: ",ex);
//            }
//            out.print(json);
//            out.flush();
//            return;
//        }
        else if (spaces) {
            result = "error";
            message = "Name should not contain any blank spaces!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("Exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        }
        int retValue = -1;
        
        ResourceManagement rManagement = new ResourceManagement();
        
        retValue = rManagement.EditResource(sessionId,channel.getChannelid(),_resid,_resource_name,_weburl,_imageurl,_status,_group);
        log.debug("EditResource ::"+retValue);
        // end by amol
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
