/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.axiom.v2.face.web.resource;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Webresource;
import com.mollatech.axiom.nucleus.db.connector.management.ResourceManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author WALE
 */
public class loadResource extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadResource.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String _res = request.getParameter("_resid");
        log.debug("_res :: "+_res);
        int _resid=Integer.parseInt(_res);
        
        response.setContentType("application/json");
        try {
            Webresource resource = null;
            ResourceManagement rmngt=new ResourceManagement();
            resource=rmngt.getResource(sessionId, sessionId, _resid);
            try { 
//                json.put("_resid", _resid);
//            json.put("_Name", resource.getResourcename());
//            json.put("_Weburl", resource.getWeburl());
//            json.put("_Status", resource.getStatus());
//            json.put("_Phone", resource.getImageUrl());
//            json.put("_Groups", resource.getGroups());
//            json.put("_Image", resource.getImageUrl());
            json.put("_result", "success");
                request.getSession().setAttribute("_res", resource);
                return;
            }
            catch(Exception e){
                log.error("Exception caught :: ",e);
            }

        } catch (Exception ex) {
            // TODO handle custom exceptions here
            try { 
                json.put("_result", "error");
            json.put("_message", ex.getMessage());
            }catch(Exception e){
                log.error("Exception caught :: ",e);}
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
