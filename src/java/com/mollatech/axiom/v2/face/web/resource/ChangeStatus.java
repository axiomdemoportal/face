/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.axiom.v2.face.web.resource;

import com.mollatech.axiom.connector.access.controller.ApprovalSetting;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Webresource;
import com.mollatech.axiom.nucleus.db.connector.management.ResourceManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author WALE
 */
public class ChangeStatus extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ChangeStatus.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
          final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;
    final int LOCKED_STATUS = -1;
    final int REMOVE_STATUS = -99;
    final String itemtype = "USERPASSWORD";
    final String itemTypeAUTH = "AUTHORIZATION";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.info("Servlet started");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getName());
        String _res = request.getParameter("_resourceid");
        log.debug("_res :: "+_res);
        int _resid=Integer.parseInt(_res);
        String _status = request.getParameter("_status");
        log.debug("_status :: "+_status);
        int status = Integer.parseInt(_status);
        String result = "success";
        String message = " status updated!!!";
        String _value = "Active";

        if (status == ACTIVE_STATUS) {
            _value = "Active ";
        } else if (status == LOCKED_STATUS) {
            _value = "Locked ";
        } else if (status == SUSPEND_STATUS) {
            _value = "Suspended ";
        } else if (status == REMOVE_STATUS) {
            _value = "Removed ";
        }
        message = _value + message;
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
       
        if (_res == null || _status == null) {
            result = "error";
            message = "Fill all details!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        int retValue = -1;
//        UserManagement uManagement = new UserManagement();
        ResourceManagement rmngt=new ResourceManagement();
        Webresource resource=rmngt.getResource(sessionId, channel.getChannelid(), _resid);
        if(resource==null){
           result = "error";
            message = "Unable to find the resources!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        int istatus=resource.getStatus();
        ApprovalSetting approvalSetting = null;
        String strstaus = "Removed";
        if (istatus == ACTIVE_STATUS) {
            strstaus = "Active";
        } else if (istatus == SUSPEND_STATUS) {
            strstaus = "Suspended";
        } else if (istatus == LOCKED_STATUS) {
            strstaus = "Locked";
        }

        String strstaus1 = "Removed";
        if (status == ACTIVE_STATUS) {
            strstaus1 = "Active";
        } else if (status == SUSPEND_STATUS) {
            strstaus1 = "Suspended";
        } else if (status == LOCKED_STATUS) {
            strstaus1 = "Locked";
        }

            retValue = rmngt.ChangeResourceStatus(sessionId, channel.getChannelid(), _resid, status);
            log.debug("ChangeResourceStatus :: "+retValue);
        
        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);

        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
