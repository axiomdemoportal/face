/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.web.resource;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.ResourceManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;

public class addresource extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(addresource.class.getName());
//    static final int PHONENUMBER = 2;
//    static final int EMAILID = 3;
    final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException {
        log.info("Servlet started");
        response.setContentType("application/json");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getName());
        String _resource_name = request.getParameter("_Name");
        log.debug("_resource_name :: "+_resource_name);
        String _weburl = request.getParameter("_URL");
        log.debug("_weburl :: "+_weburl);
        String saveto = request.getSession().getAttribute("_savepath").toString();
        log.debug("saveto :: "+_weburl);
        String _imageurl = saveto;
        request.getSession().setAttribute("_savepath", null);
        String _status = request.getParameter("_Status");
        log.debug("_status :: "+_status);
        // String _ssoString=request.getParameter("_field1")+request.getParameter("_field2")+request.getParameter("_field3")+request.getParameter("_field4")+request.getParameter("_field5");
        JSONObject _ssoJson = new JSONObject();
        _ssoJson.put("_field1", request.getParameter("_field1"));
        _ssoJson.put("_field2", request.getParameter("_field2"));
        _ssoJson.put("_field3", request.getParameter("_field3"));
        _ssoJson.put("_field4", request.getParameter("_field4"));
        _ssoJson.put("_field5", request.getParameter("_field5"));
        _ssoJson.put("_field6", request.getParameter("_field6"));
        _ssoJson.put("_field7", request.getParameter("_field7"));
        _ssoJson.put("_field8", request.getParameter("_field8"));
        _ssoJson.put("_field9", request.getParameter("_field9"));
        _ssoJson.put("_field10", request.getParameter("_field10"));

        String _group = request.getParameter("_groupName");
        String result = "success";
        String message = "Resource added successfully....";

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

//        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.sso_enabled) != 0) {
//            result = "error";
//            message = "This feature is not available in this license!!!";
//            try {
//                json.put("_result", result);
//                json.put("_message", message);
//            } catch (Exception e) {
//                log.error("Exception caught :: ",e);
//            }
//            out.print(json);
//            out.flush();
//            return;
//        }
        ResourceManagement rManagement = new ResourceManagement();
        int resource = rManagement.ResoucresCounts(channel.getChannelid());
        log.debug("ResoucresCounts :: "+resource);
//
//        if (resource != -998 && resource < AxiomProtect.GetCountsAllowed(AxiomProtect.WEB_RESOURCES_COUNT)) {
//            result = "error";
//            message = "Operator Addition is not available in this license!!!";
//            try {
//                json.put("_result", result);
//                json.put("_message", message);
//            } catch (Exception e) {
//                log.error("Exception caught :: ",e);
//            }
//            out.print(json);
//            out.flush();
//            return;
//        }
//        if (resource != -998 && resource < AxiomProtect.GetCountsAllowed(AxiomProtect.sso_applications_count)) {
//            result = "error";
//            message = "Operator Addition is not available in this license!!!";
//            try {
//                json.put("_result", result);
//                json.put("_message", message);
//            } catch (Exception e) {
//                log.error("Exception caught :: ",e);
//            }
//            out.print(json);
//            out.flush();
//            return;
//        }
        //amol
        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(_resource_name);
        boolean b = m.find();
        Pattern p1 = Pattern.compile("[0-9]", Pattern.CASE_INSENSITIVE);
        Matcher m1 = p1.matcher(_resource_name);
        boolean b1 = m1.find();
        boolean spaces = false;
        if (_resource_name != null) {
            for (int i = 0; i < _resource_name.length(); i++) {
                if (Character.isWhitespace(_resource_name.charAt(i))) {
                    spaces = true;
                }
            }
        }
        //end amol

        if (_resource_name == null || _weburl == null || _imageurl == null
                || _resource_name.isEmpty() == true || _weburl.isEmpty() == true || _imageurl.isEmpty() == true) {
            result = "error";
            message = "Fill all Details!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        } // amol
        else if (_resource_name == null) {
            result = "error";
            message = "Name could not Empty !!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("Exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        } else if (_resource_name.length() < 3) {
            result = "error";
            message = "Name should be more than 3 characters!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("Exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        } else if (b) {
            result = "error";
            message = "Name should not contain any symbols!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("Exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        } 
//        else if (b1) {
//            result = "error";
//            message = "Name should not contain only digits!!!";
//            try {
//                json.put("_result", result);
//                json.put("_message", message);
//            } catch (Exception ex) {
//                log.error("Exception caught :: ",ex);
//            }
//            out.print(json);
//            out.flush();
//            return;
//        } 
        else if (spaces) {
            result = "error";
            message = "Name should not contain any blank spaces!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("Exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        }
        int retValue = -1;

        int check = -1;
        check = rManagement.checkIfExists(channel.getChannelid(), _resource_name, _weburl);
        if (check == 0) {
            retValue = rManagement.CreateResource(sessionId, channel.getChannelid(), _resource_name, _weburl, _imageurl, _status, _group, _ssoJson.toString());
            log.debug("CreateResource :: "+retValue);
        } else if (check == -1) {
            result = "error";
            message = "Resource can not added !!!";
        } else if (check == -2) {
            result = "error";
            message = "Resource name alreay exist!!!";
        } else if (check == -3) {
            result = "error";
            message = "URL already exist!!!";
        }

        // end by amol
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(addresource.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(addresource.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
