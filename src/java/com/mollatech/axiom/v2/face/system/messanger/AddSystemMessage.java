package com.mollatech.axiom.v2.face.system.messanger;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Systemmessagesettings;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SystemMessageManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class AddSystemMessage extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AddSystemMessage.class.getName());
    final String itemtype = "SystemMessage";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        response.setContentType("application/json");
        try {

            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel.getName());
            String channelId = channel.getChannelid();
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId :: "+sessionId);
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("remoteaccesslogin :: "+remoteaccesslogin);
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operatorS :: "+operatorS.getName());
            String OperatorID = operatorS.getOperatorid();
            String result = "success";
            String message = "Settings added successfully";
            JSONObject json = new JSONObject();
            PrintWriter out = response.getWriter();

            Systemmessagesettings rs = new Systemmessagesettings();

            //String messageno = request.getParameter("_snmpip");
            String messagebody = request.getParameter("_messagebody");
            log.debug("messagebody :: "+messagebody);
            String alertto = request.getParameter("_alertto");
            log.debug("alertto :: "+alertto);
            String status = request.getParameter("_status");
            log.debug("status :: "+status);
             if (messagebody == null ||  alertto == null || status == null) {
                result = "success";
                message = "Please fill details all details!!!";

                json.put("_result", result);
                json.put("_message", message);

                out.print(json);
                out.flush();
                return;
            } 
                if (messagebody.isEmpty() == true ||  alertto.isEmpty() == true || status.isEmpty()) {
                result = "success";
                message = "Please fill details all details!!!";

                json.put("_result", result);
                json.put("_message", message);

                out.print(json);
                out.flush();
                return;
            } 
            int istatus = 0;
            if (status != null) {
                istatus = Integer.parseInt(status);
            }
            
           
            rs.setMessage(message);
            rs.setAlertTo(alertto);
            rs.setStatus(istatus);
            
            SystemMessageManagement mngt = new SystemMessageManagement();

            int value = mngt.addsytemmessagesetting(sessionId, channelId,messagebody,alertto,istatus);
            AuditManagement audit = new AuditManagement();

            String resultString = "ERROR";
            if (value == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add system alert Setting", resultString, value, "System Messenger",
                        "", "Message=" + messagebody + "Status =" + status,
                        itemtype, OperatorID);
            } else {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add sytem alert Setting", resultString, value, "System Messenger",
                        "", "Failed To Add system alert Setting",
                        itemtype, OperatorID);
            }

            if (value == 0) {
                result = "success";
                message = "Settings added successfully!!!";

                json.put("_result", result);
                json.put("_message", message);

                out.print(json);
                out.flush();
                return;
            } else {
                result = "error";
                message = "Failed to add settings!!!";
//                JSONObject e = new JSONObject();
//                PrintWriter out = response.getWriter();
                json.put("_result", result);
                json.put("_message", message);

                out.print(json);
                out.flush();
            }

        } catch (Exception ex) {
           log.error("Exception caught :: ",ex);
        }
        log.info("Servlet ended");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

//    private static SessionFactory factory;
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
