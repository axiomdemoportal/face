package com.mollatech.axiom.v2.face.system.messanger;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Systemmessagesettings;
import com.mollatech.axiom.nucleus.db.connector.management.SystemMessageManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class GetSystemMessage extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(GetSystemMessage.class.getName());
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        String _sid = request.getParameter("_sid");
        log.debug("_sid :: "+_sid);
        int messageid = 0;
        if (!_sid.equals("")) {
            messageid = Integer.parseInt(_sid);
        }
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        try {

            SystemMessageManagement smang = new SystemMessageManagement();
            Systemmessagesettings oprObj = smang.getMessageByMessageid(sessionId, channel.getChannelid(), messageid);
            json.put("_messagenoedit", oprObj.getMessageId());
            json.put("_messagebodyedit", oprObj.getMessage());
            json.put("_alerttoedit", oprObj.getAlertTo());
            //json.put("_alerttoedit", oprObj.getAlertTo());

        } catch (Exception ex) {
            // TODO handle custom exceptions here
            try {
                json.put("_result", "error");
                json.put("_message", ex.getMessage());
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
