/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.system.messanger;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Systemmessagereceivertracking;
import com.mollatech.axiom.nucleus.db.Systemmessagesettings;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SystemMessageManagement;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author bluebricks3
 */
public class Deletemessagesettingentry extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Deletemessagesettingentry.class.getName());
    final String itemtype = "SCHEDULER";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        try {
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId :: "+sessionId);
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("remoteaccesslogin :: "+remoteaccesslogin);
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operatorS :: "+operatorS.getName());
            String _sid = request.getParameter("_sid");
            log.debug("_sid :: "+_sid);
            int _recieverid = Integer.parseInt(_sid);
            String result = "success";
            String message = "Message removed successfully....";

            SystemMessageManagement gManagement = new SystemMessageManagement();
            JSONObject e = new JSONObject();
            PrintWriter out = response.getWriter();
            AuditManagement audit = new AuditManagement();
            Systemmessagesettings oldObj = gManagement.getMessageByMessageid(sessionId, channel.getChannelid(), _recieverid);

            int retValue = -1;
            retValue = gManagement.deletemessagesetting(sessionId, channel.getChannelid(), _recieverid);
            log.debug("deletemessagesetting :: "+retValue);
            if (retValue == 0) {
                Systemmessagereceivertracking arr = gManagement.getarrmessageTracking(channel.getChannelid(), oldObj.getMessageId(),oldObj.getAlertTo());
                if (arr != null) {
                    
                        retValue = gManagement.deletemessageTracking(sessionId, channel.getChannelid(), arr.getMessageTrackingId());
                   
                }
            }
            String resultString = "ERROR";

            String stroldschedulerStatus = "In-Active";
            if (oldObj.getStatus()== 1) {
                stroldschedulerStatus = "Active";
            }

           
            if (retValue == 0) {
                 resultString = "SUCCESS";

                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Delete System Message settings", resultString, retValue, "System Messenger",
                        "Message Id=" + oldObj.getMessageId()+ "Status =" + stroldschedulerStatus
                        ,
                        "Deleted Successfuly",
                        itemtype, "" + _recieverid);

            } else {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Delete System Message settings", resultString, retValue, "System Messenger",
                        "Message Id=" + oldObj.getMessageId()+ "Status =" + stroldschedulerStatus
                       ,
                        "Failed to Delete scheduler settings",
                        itemtype, "" + _recieverid);
                result = "error";
                message = "Failed to remove message!!";
                e.put("_result", result);
                e.put("_message", message);
                out.print(e);
                out.flush();
                return;
            }
            if (retValue == 0) {
                result = "success";
                message = "Message removed successfully....";
                e.put("_result", result);
                e.put("_message", message);

                out.print(e);
                out.flush();
                return;
            } else {
                result = "error";
                message = "Failed to delete settings entry";
//                JSONObject e = new JSONObject();
//                PrintWriter out = response.getWriter();
                e.put("_result", result);
                e.put("_message", message);

                out.print(e);
                out.flush();
            }
        } catch (Exception ex) {
            log.debug("Exception caught :: ",ex);
        }
        log.info("Servlet ended");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
