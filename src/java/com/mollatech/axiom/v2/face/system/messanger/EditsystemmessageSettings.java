/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.system.messanger;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Systemmessagesettings;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SystemMessageManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author bluebricks3
 */
public class EditsystemmessageSettings extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(EditsystemmessageSettings.class.getName());
    final String itemtype = "SystemMessage";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        try {
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel.getName());
            String channelId = channel.getChannelid();
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId :: "+sessionId);
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("remoteaccesslogin :: "+remoteaccesslogin);
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operatorS :: "+operatorS.getName());
            String OperatorID = operatorS.getOperatorid();
            String _messageno = request.getParameter("_messagenoedit");
            log.debug("_messageno :: "+_messageno);
            int imessageno = 0;
            if (_messageno != null) {
                imessageno = Integer.parseInt(_messageno);
            }

            JSONObject e = new JSONObject();
            PrintWriter out = response.getWriter();
            String result = "success";
            String message = "Message updated successfully";

            String mb = request.getParameter("_messagebodyedit");
            log.debug("mb :: "+mb);
            String at = request.getParameter("_alerttoedit");
            log.debug("at :: "+at);
            SystemMessageManagement mngt = new SystemMessageManagement();
            AuditManagement audit = new AuditManagement();

            Systemmessagesettings oldObj = mngt.getMessageByMessageid(sessionId, channel.getChannelid(), imessageno);

            int retValue = mngt.EditmessageSettings(sessionId, channelId, imessageno, mb, at, oldObj.getStatus());
           
            String resultString = "ERROR";
            if (retValue == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Edit sytem message Setting", resultString, retValue, "System messenger",
                        "", "message no=" + _messageno + "status =" + oldObj.getStatus(),
                        itemtype, "" + imessageno);
            } else {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Edit sytem message Setting", resultString, retValue, "System messenger",
                        "", "Failed To add sytem message Setting",
                        itemtype, "" + imessageno);
            }

            if (retValue == 0) {
                result = "success";
                message = "Message updated successfully!!!";

                e.put("_result", result);
                e.put("_message", message);

                out.print(e);
                out.flush();
                return;
            } else {
                result = "error";
                message = "Failed to update Message!!!";
//                JSONObject e = new JSONObject();
//                PrintWriter out = response.getWriter();
                e.put("_result", result);
                e.put("_message", message);

                out.print(e);
                out.flush();
            }

        } catch (Exception ex) {
            log.debug("Exception caught :: ",ex);
        }
        log.info("Servlet ended");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

//    private static SessionFactory factory;
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
