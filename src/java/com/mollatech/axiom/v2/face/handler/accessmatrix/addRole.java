/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.accessmatrix;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Roles;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author nilesh
 */
public class addRole extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(addRole.class.getName());

    final String itemType = "ROLES";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel::" + channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId::" + sessionId);

        //audit
        Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin::" + remoteaccesslogin);

        String operatorId = operator.getOperatorid();
        log.debug("operatorId::" + operator.getOperatorid());

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Role Added Successfully!!!";
        int retValue = 0;

        String _roleName = request.getParameter("_rolename");
        log.debug("_roleName::" + _roleName);
        String _rolestatus = request.getParameter("_rolestatus");
        log.debug("_rolestatus::" + _rolestatus);
            

          // amol
        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(_roleName);
        boolean b = m.find();
        Pattern p1 = Pattern.compile("[0-9]", Pattern.CASE_INSENSITIVE);

        Matcher m1 = p1.matcher(_roleName);
        //boolean b1 = m1.find();
        
        if (_roleName == null || _roleName.isEmpty() == true ) {
            result = "error";
            message = "Name Should not be Empty !!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        } else if (_roleName.length() < 3) {
            result = "error";
            message = "Name should be more than 3 characters!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        } else if (b) {
            result = "error";
            message = "Name should not contain any symbols!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
               log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        } 
//        else if (b1) {
//            result = "error";
//            message = "Name should not contain only digits!!!";
//            try {
//                json.put("_result", result);
//                json.put("_message", message);
//            } catch (Exception ex) {
//                log.error("Exception caught :: ",ex);
//            }
//            out.print(json);
//            out.flush();
//            return;
//        }
         // end by amol

        if (_roleName.length() > 25) {
            result = "error";
            message = "Role Name less than 25 characters!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        }
        int irolestatus = Integer.parseInt(_rolestatus);

        OperatorsManagement oprMngt = new OperatorsManagement();

        Roles roleObj = oprMngt.getRoleByName(channel.getChannelid(), _roleName);
        

        if (roleObj != null) {
            //System.out.println(roleObj.getName());
            result = "error";
            message = "Role with same name already exists!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        } else { 
            //System.out.println(_roleName + " not found");
        }
        retValue = oprMngt.AddRole(sessionId, channel.getChannelid(), _roleName, irolestatus);
        log.debug("AddRole::" + retValue);
        String resultString = "ERROR";
        AuditManagement audit = new AuditManagement();
        if (retValue == 0) {
            String strStatus = "";
            if (irolestatus == OperatorsManagement.ACTIVE_STATUS) {
                strStatus = "ACTIVE_STATUS";
            } else if (irolestatus == OperatorsManagement.SUSPEND_STATUS) {
                strStatus = "SUSPEND_STATUS";
            }
            resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                    "Add Role", resultString, retValue, "Role Management",
                    "", "Role Name=" + _roleName + " Status =" + strStatus,
                    itemType, "-");

            result = "success";
            message = "Role Added Successfully!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        } else {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                    "Add Role", resultString, retValue, "Role Management",
                    "", "Failed To Add Role",
                    itemType, "-");

            result = "error";
            message = "Failed To Add Role!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            log.info("is ended :: ");
            return;
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
