package com.mollatech.axiom.v2.face.handler.users;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class resenduserpassword extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(resenduserpassword.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final String itemTypeAUTH = "AUTHORIZATION";
    final String itemtype = "USERPASSWORD";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());

        String _userid = request.getParameter("_userid");
        log.debug("_userid :: "+_userid);

        response.setContentType("application/json");

        String result = "success";
        String message = "Password send please check your mobile....";
        String strPassword = null;
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String msgTemp = null;

        if (_userid == null) {
            result = "error";
            message = "Fill all Details!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught ::",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.USER_PASSWORD) != 0) {
            result = "error";
            message = "This feature is not available in this license!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
               log.error("exception caught ::",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        int retValue = -1;
        UserManagement uManagement = new UserManagement();
        strPassword = uManagement.GetPassword(sessionId, channel.getChannelid(), _userid);
        AuthUser user = null;

        user = uManagement.getUser(sessionId, channel.getChannelid(), _userid);

        if (user == null || strPassword == null) {
            result = "error";
            message = "Password Not Sent Details!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught ::",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        //added for template based messages
        AXIOMStatus status = new AXIOMStatus();
        status.iStatus = -1;
        status.strStatus = "FAILED";
        TemplateManagement tManagement = new TemplateManagement();

        Templates templates = tManagement.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.MOBILE_USER_PASSWORD_TEMPLATE);
        if (templates.getStatus() == tManagement.ACTIVE_STATUS) {
            ByteArrayInputStream bais = new ByteArrayInputStream(templates.getTemplatebody());
            String tmessage = (String) TemplateUtils.deserializeFromObject(bais);
            //String tsubject = templates.getSubject();
            ChannelManagement cmObj = new ChannelManagement();
            //Channels channel = cmObj.getChannelByID(channelId);
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
            if (message != null) {
                Date d = new Date();
                tmessage = tmessage.replaceAll("#name#", user.getUserName());
                tmessage = tmessage.replaceAll("#channel#", channel.getName());
                tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                tmessage = tmessage.replaceAll("#password#", strPassword);
            }

            //end of addition
            SendNotification send = new SendNotification();

            status = send.SendOnMobileNoWaiting(channel.getChannelid(),
                    user.getPhoneNo(),
                    tmessage,
                    1,
                    Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
        }
//        }
        //end of authorization

        AuditManagement audit = new AuditManagement();
        String resultString = "Failure";
        if (status.iStatus == SendNotification.PENDING) {
            resultString = "Success";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Resend User Password", resultString, retValue,
                    "User Management", "Old Password =*****", "New Password =*****",
                    itemtype, _userid);

            

        } else {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Resend User Password", resultString, retValue,
                    "User Management", "Old Password =*****", "Failed To Send Password",
                    itemtype, _userid);

//        if (status.iStatus != SendNotification.PENDING) { //pending
            result = "error";
            message = "Password Not Sent!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught ::",e);
            }
            out.print(json);
            out.flush();
            return;
//        }

        }

//        if (strPassword != null) {
//            retValue = 0;
//        }
//        if (retValue != 0) {
//            result = "error";
//            message = "password not send!!";
//        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            log.error("exception caught ::",e);
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

//    public static Object deserializeFromObject(ByteArrayInputStream binaryObject) {
//        ObjectInputStream ins;
//        try {
//            ins = new ObjectInputStream(binaryObject);
//            Object object = (Object) ins.readObject();
//            ins.close();
//            return object;
//        } catch (Exception e) {
//            log.error("Exception caught :: ",e);
//        }
//        return null;
//    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static Object createObject(Constructor constructor, Object[] arguments) {

        //System.out.println("Constructor: " + constructor.toString());
        Object object = null;

        try {
            object = constructor.newInstance(arguments);
            //System.out.println("Object: " + object.toString());
            return object;
        } catch (Exception e) {
            log.error("exception caught ::",e);
        }
        return object;
    }
}
