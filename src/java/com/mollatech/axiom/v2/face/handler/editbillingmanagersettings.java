/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.settings.BillingManagerSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class editbillingmanagersettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editbillingmanagersettings.class.getName());

    final String itemtype = "SETTINGS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String OperatorID = operatorS.getOperatorid();

        String _channelId = channel.getChannelid();
        SettingsManagement sMngmt = new SettingsManagement();
        int retValue = -1;
        
            log.debug("channel :: " + channel.getName());
            log.debug(" operatorS :: " + operatorS.getOperatorid());
            log.debug("sessionId :: " + sessionId);
            log.debug("remoteaccesslogin :: " + remoteaccesslogin);
            log.debug("getChannelid :: " + channel.getChannelid());

        String result = "success";
        String message = "Billing Manager Settings Update Successful!!!";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.BILLING_MANAGER_SETTINGS) != 0) {
            result = "error";
            message = "This feature is not available in this license!!!";

            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }

            out.print(json);
            out.flush();
            return;
        }
        boolean ballowBillingManager = false;
        boolean ballowAlert = false;
        
        int icurrancyType = 0;
        int igatewayType = 0;
        int ialertAttempt = 0;

        //swotp
        int iselectBillTypeSWOTP = 0;
        int iselectBillingDurationSWOTP = 0;
        int iselectAlertBeforeSWOTP = 0;
        double icostSWOTP = 0;
        boolean ballowBillingManagerSWOTP = false;
        //hwotp
        int iselectBillTypeHWOTP = 0;
        int iselectBillingDurationHWOTP = 0;
        int iselectAlertBeforeHWOTP = 0;
        double icostHWOTP = 0;
        boolean ballowBillingManagerHWOTP = false;
        //SWPKIOTP
        int iselectBillTypeSWPKIOTP = 0;
        int iselectBillingDurationSWPKIOTP = 0;
        int iselectAlertBeforeSWPKIOTP = 0;
        double icostSWPKIOTP = 0;
        boolean ballowBillingManagerSWPKIOTP = false;
        //HWPKIOTP
        int iselectBillTypeHWPKIOTP = 0;
        int iselectBillingDurationHWPKIOTP = 0;
        int iselectAlertBeforeHWPKIOTP = 0;
        double icostHWPKIOTP = 0;
        boolean ballowBillingManagerHWPKIOTP = false;
        //HWPKIOTP
        //Certificate
        int iselectBillTypeCertificate = 0;
        int iselectBillingDurationCertificate = 0;
        int iselectAlertBeforeCertificate = 0;
        double icostCertificate = 0;
        boolean ballowBillingManagerCertificate = false;
        //HWPKIOTP
        int iselectBillTypeOOBOTP = 0;
        int iselectBillingDurationOOBOTP = 0;
        int iselectAlertBeforeOOBOTP = 0;
        double icostOOBOTP = 0;
        double ithresholdcostOOBOTP = 0;
        double icostperTxOOBOTP = 0;
        boolean ballowBillingManagerOOBOTP = false;
        int itxPerDayOOBOTP = 0;

        if (request.getParameter("_allowBillingManager") != null) {
            String _allowBillingManager = request.getParameter("_allowBillingManager");
            if (_allowBillingManager.equals("1")) {
                ballowBillingManager = true;
            }
        }
//         if (request.getParameter("_currancyType") != null) {
//            icurrancyType = Integer.parseInt(request.getParameter("_currancyType"));
//        }

        if (request.getParameter("_allowRepetationCharacterB") != null) {
            String _allowRepetationCharacterB = request.getParameter("_allowRepetationCharacterB");
            if (_allowRepetationCharacterB.equals("1")) {
                ballowAlert = true;
            }
        }
        if (request.getParameter("_gatewayTypeB") != null) {
            igatewayType = Integer.parseInt(request.getParameter("_gatewayTypeB"));
        }
        if (request.getParameter("_alertAttemptB") != null) {
            ialertAttempt = Integer.parseInt(request.getParameter("_alertAttemptB"));
        }

//        swotp
        if (request.getParameter("_allowBillingManagerSWOTP") != null) {
            String _allowBillingManagerSWOTP = request.getParameter("_allowBillingManagerSWOTP");
            if (_allowBillingManagerSWOTP.equals("1")) {
                ballowBillingManagerSWOTP = true;
            }
        }
        if (request.getParameter("_selectBillTypeSWOTP") != null) {
            iselectBillTypeSWOTP = Integer.parseInt(request.getParameter("_selectBillTypeSWOTP"));
        }
        if (request.getParameter("_selectBillTypeSWOTP") != null) {
            iselectBillingDurationSWOTP = Integer.parseInt(request.getParameter("_selectBillingDurationSWOTP"));
        }
        if (request.getParameter("_selectAlertBeforeSWOTP") != null) {
            iselectAlertBeforeSWOTP = Integer.parseInt(request.getParameter("_selectAlertBeforeSWOTP"));
        }
        if (request.getParameter("_costSWOTP").isEmpty() == false) {
            icostSWOTP = Double.parseDouble(request.getParameter("_costSWOTP"));

        }

//        hwotp
        if (request.getParameter("_allowBillingManagerHWOTP") != null) {
            String _allowBillingManagerHWOTP = request.getParameter("_allowBillingManagerHWOTP");
            if (_allowBillingManagerHWOTP.equals("1")) {
                ballowBillingManagerHWOTP = true;
            }
        }
        if (request.getParameter("_selectBillTypeHWOTP") != null) {
            iselectBillTypeHWOTP = Integer.parseInt(request.getParameter("_selectBillTypeHWOTP"));
        }
        if (request.getParameter("_selectBillTypeHWOTP") != null) {
            iselectBillingDurationHWOTP = Integer.parseInt(request.getParameter("_selectBillingDurationHWOTP"));
        }
        if (request.getParameter("_selectAlertBeforeHWOTP") != null) {
            iselectAlertBeforeHWOTP = Integer.parseInt(request.getParameter("_selectAlertBeforeHWOTP"));
        }
        if (request.getParameter("_costHWOTP").isEmpty() == false) {
            icostHWOTP = Double.parseDouble(request.getParameter("_costHWOTP"));
        }
//        SWPKIOTP
        if (request.getParameter("_allowBillingManagerSWPKIOTP") != null) {
            String _allowBillingManagerSWPKIOTP = request.getParameter("_allowBillingManagerSWPKIOTP");
            if (_allowBillingManagerSWPKIOTP.equals("1")) {
                ballowBillingManagerSWPKIOTP = true;
            }
        }
        if (request.getParameter("_selectBillTypeSWPKIOTP") != null) {
            iselectBillTypeSWPKIOTP = Integer.parseInt(request.getParameter("_selectBillTypeSWPKIOTP"));
        }
        if (request.getParameter("_selectBillTypeSWPKIOTP") != null) {
            iselectBillingDurationSWPKIOTP = Integer.parseInt(request.getParameter("_selectBillingDurationSWPKIOTP"));
        }
        if (request.getParameter("_selectAlertBeforeSWPKIOTP") != null) {
            iselectAlertBeforeSWPKIOTP = Integer.parseInt(request.getParameter("_selectAlertBeforeSWPKIOTP"));
        }
        if (request.getParameter("_costSWPKIOTP").isEmpty() == false) {
            icostSWPKIOTP = Double.parseDouble(request.getParameter("_costSWPKIOTP"));
        }

//        HWPKIOTP
        if (request.getParameter("_allowBillingManagerHWPKIOTP") != null) {
            String _allowBillingManagerHWPKIOTP = request.getParameter("_allowBillingManagerHWPKIOTP");
            if (_allowBillingManagerHWPKIOTP.equals("1")) {
                ballowBillingManagerHWPKIOTP = true;
            }
        }
        if (request.getParameter("_selectBillTypeHWPKIOTP") != null) {
            iselectBillTypeHWPKIOTP = Integer.parseInt(request.getParameter("_selectBillTypeHWPKIOTP"));
        }
        if (request.getParameter("_selectBillTypeHWPKIOTP") != null) {
            iselectBillingDurationHWPKIOTP = Integer.parseInt(request.getParameter("_selectBillingDurationHWPKIOTP"));
        }
        if (request.getParameter("_selectAlertBeforeHWPKIOTP") != null) {
            iselectAlertBeforeHWPKIOTP = Integer.parseInt(request.getParameter("_selectAlertBeforeHWPKIOTP"));
        }
        if (request.getParameter("_costHWPKIOTP").isEmpty() == false) {
            icostHWPKIOTP = Double.parseDouble(request.getParameter("_costHWPKIOTP"));
        }

        //        Certifacate
        if (request.getParameter("_allowBillingManagerCertificate") != null) {
            String _allowBillingManagerCertificate = request.getParameter("_allowBillingManagerCertificate");
            if (_allowBillingManagerCertificate.equals("1")) {
                ballowBillingManagerCertificate = true;
            }
        }
        if (request.getParameter("_selectBillTypeCertificate") != null) {
            iselectBillTypeCertificate = Integer.parseInt(request.getParameter("_selectBillTypeCertificate"));
        }
        if (request.getParameter("_selectBillingDurationCertificate") != null) {
            iselectBillingDurationCertificate = Integer.parseInt(request.getParameter("_selectBillingDurationCertificate"));
        }
        if (request.getParameter("_selectAlertBeforeCertificate") != null) {
            iselectAlertBeforeCertificate = Integer.parseInt(request.getParameter("_selectAlertBeforeCertificate"));
        }
        if (request.getParameter("_costCertificate").isEmpty() == false) {
            icostCertificate = Double.parseDouble(request.getParameter("_costCertificate"));
        }

        // OOBOTP
        if (request.getParameter("_allowBillingManagerOOBOTP") != null) {
            String _allowBillingManagerOOBOTP = request.getParameter("_allowBillingManagerOOBOTP");
            if (_allowBillingManagerOOBOTP.equals("1")) {
                ballowBillingManagerOOBOTP = true;
            }
        }
        if (request.getParameter("_selectBillTypeOOBOTP") != null) {
            iselectBillTypeOOBOTP = Integer.parseInt(request.getParameter("_selectBillTypeOOBOTP"));
        }
        if (request.getParameter("_selectBillTypeOOBOTP") != null) {
            iselectBillingDurationOOBOTP = Integer.parseInt(request.getParameter("_selectBillingDurationOOBOTP"));
        }
        if (request.getParameter("_selectAlertBeforeOOBOTP") != null) {
            iselectAlertBeforeOOBOTP = Integer.parseInt(request.getParameter("_selectAlertBeforeOOBOTP"));
        }
        if (request.getParameter("_txPerDayOOBOTP") != null) {
            itxPerDayOOBOTP = Integer.parseInt(request.getParameter("_txPerDayOOBOTP"));
        }
        if (request.getParameter("_costOOBOTP").isEmpty() == false) {
            icostOOBOTP = Double.parseDouble(request.getParameter("_costOOBOTP"));
        }
        if (request.getParameter("_costperTxOOBOTP").isEmpty() == false) {
            icostperTxOOBOTP = Double.parseDouble(request.getParameter("_costperTxOOBOTP"));
        }
//         if (request.getParameter("_thresholdCostOOBOTP").isEmpty() == false) {
//            ithresholdcostOOBOTP = Double.parseDouble(request.getParameter("_thresholdCostOOBOTP"));
//        }
        String _templateName = request.getParameter("_templateName");
        int itemplateId = 0;
//        if (_templateName != null) {
//            itemplateId = Integer.parseInt(_templateName);
//        }

        if (icostSWOTP < 0 || icostHWOTP < 0 || icostHWPKIOTP < 0
                || icostOOBOTP < 0 || icostSWOTP < 0 || icostSWPKIOTP < 0 || icostperTxOOBOTP < 0) {
            result = "error";
            message = "Cost must be +ve value!!!";

            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }

            out.print(json);
            out.flush();
            return;
        }
        
        String curtype = request.getParameter("_currancyType");
        if (curtype == null || curtype.isEmpty() == true) {
            result = "error";
            message = "Currency is not valid.";

            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            log.info("Servlet ended");
            return;
        }

        
        
        

        int iType = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.BILLING_MANAGER_SETTING;
        int iPreference = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE;
        Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), iType, iPreference);
        BillingManagerSettings billingObj = null;
        boolean bAddSetting = false;
        if (settingsObj == null) {
            billingObj = new BillingManagerSettings();
            bAddSetting = true;
        } else {
            billingObj = (BillingManagerSettings) settingsObj;
        }

        billingObj.alert = ballowAlert;
        billingObj.igatewayType = igatewayType;
        billingObj.ialertAttempt = ialertAttempt;

        billingObj.ballowBillingManager = ballowBillingManager;
        billingObj.ballowBillingManagerHWOTP = ballowBillingManagerHWOTP;
        billingObj.ballowBillingManagerHWPKIOTP = ballowBillingManagerHWPKIOTP;
        billingObj.ballowBillingManagerOOBOTP = ballowBillingManagerOOBOTP;
        billingObj.ballowBillingManagerSWOTP = ballowBillingManagerSWOTP;
        billingObj.ballowBillingManagerSWPKIOTP = ballowBillingManagerSWPKIOTP;
        billingObj.icostHWOTP = icostHWOTP;
        billingObj.icostHWPKIOTP = icostHWPKIOTP;
        billingObj.icostOOBOTP = icostOOBOTP;
        billingObj.icostSWOTP = icostSWOTP;
        billingObj.icostSWPKIOTP = icostSWPKIOTP;
        billingObj.icostperTxOOBOTP = icostperTxOOBOTP;
        billingObj.strCurrencyType = request.getParameter("_currancyType");
        billingObj.iselectAlertBeforeHWOTP = iselectAlertBeforeHWOTP;
        billingObj.iselectAlertBeforeHWPKIOTP = iselectAlertBeforeHWPKIOTP;
        billingObj.iselectAlertBeforeOOBOTP = iselectAlertBeforeOOBOTP;
        billingObj.iselectAlertBeforeSWOTP = iselectAlertBeforeSWOTP;
        billingObj.iselectAlertBeforeSWPKIOTP = iselectBillingDurationSWPKIOTP;
        billingObj.iselectBillTypeHWOTP = iselectBillTypeHWOTP;
        billingObj.iselectBillTypeHWPKIOTP = iselectBillTypeHWPKIOTP;
        billingObj.iselectBillTypeOOBOTP = iselectBillTypeOOBOTP;
        billingObj.iselectBillTypeSWOTP = iselectBillTypeSWOTP;
        billingObj.iselectBillTypeSWPKIOTP = iselectBillTypeSWPKIOTP;
        billingObj.iselectBillingDurationHWOTP = iselectBillingDurationHWOTP;
        billingObj.iselectBillingDurationHWPKIOTP = iselectBillingDurationHWPKIOTP;
        billingObj.iselectBillingDurationOOBOTP = iselectBillingDurationOOBOTP;
        billingObj.iselectBillingDurationSWOTP = iselectBillingDurationSWOTP;
        billingObj.iselectBillingDurationSWPKIOTP = iselectBillingDurationSWPKIOTP;
//        billingObj.ithresholdcostOOBOTP = ithresholdcostOOBOTP;
        billingObj.itxPerDayOOBOTP = itxPerDayOOBOTP;

        //changed by vikram
        TemplateManagement tempObj = new TemplateManagement();
        Templates tObj = tempObj.LoadbyName(_channelId, _templateName);
        itemplateId = tObj.getTemplateid();
        billingObj.iTemplateId = itemplateId;
        //end of change 

        billingObj.iselectBillTypeCertificate = iselectBillTypeCertificate;
        billingObj.iselectBillingDurationCertificate = iselectBillingDurationCertificate;
        billingObj.iselectAlertBeforeCertificate = iselectAlertBeforeCertificate;
        billingObj.icostCertificate = icostCertificate;
        billingObj.ballowBillingManagerCertificate = ballowBillingManagerCertificate;
        AuditManagement audit = new AuditManagement();
        if (bAddSetting == true) {
            retValue = sMngmt.addSetting(sessionId, _channelId, iType, iPreference, billingObj);
            log.debug("editbillingmanagersettings::addSetting::" + retValue);
            
            String resultString = "ERROR";
            if (retValue == 0) {
                resultString = "SUCCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add Billing Manager Settings", resultString, retValue, "Setting Management",
                        "", "Allow Billing Manager=" + ballowBillingManager + "Allow Billing Manager SWOTP=" + ballowBillingManagerSWOTP + ",Currency Type=" + icurrancyType + ",Bill Type SWOTP=" + iselectBillTypeSWOTP
                        + "Billing Duration =" + iselectBillingDurationSWOTP + "Alert Before SWOTP=" + iselectAlertBeforeSWOTP + "Cost SWOTP=" + icostSWOTP
                        + "Allow Billing Manager HWOTP=" + ballowBillingManagerHWOTP + ",Bill Type HWOTP=" + iselectBillTypeHWOTP
                        + "Billing Duration HWOTP=" + iselectBillingDurationHWOTP + "Alert Before HWOTP=" + iselectAlertBeforeHWOTP + "Cost HWOTP=" + icostHWOTP
                        + "Allow Billing Manager SWPKIOTP=" + ballowBillingManagerSWPKIOTP + ",Bill Type SWPKIOTP=" + iselectBillTypeSWPKIOTP
                        + "Billing Duration SWPKIOTP=" + iselectBillingDurationSWPKIOTP + "Alert Before SWPKIOTP=" + iselectAlertBeforeSWPKIOTP + "Cost SWPKIOTP=" + icostSWPKIOTP
                        + "Allow Billing Manager HWPKIOTP=" + ballowBillingManagerHWPKIOTP + ",Bill Type HWPKIOTP=" + iselectBillTypeHWPKIOTP
                        + "Billing Duration HWPKIOTP=" + iselectBillingDurationHWPKIOTP + "Alert Before HWPKIOTP=" + iselectAlertBeforeHWPKIOTP + "Cost HWPKIOTP=" + icostHWPKIOTP
                        + "Allow Billing Manager Certificate=" + ballowBillingManagerCertificate + ",Bill Type Certificate=" + iselectBillTypeCertificate
                        + "Billing Duration Certificate=" + iselectBillingDurationCertificate + "Alert Before Certificate=" + iselectAlertBeforeCertificate + "Cost Certificate=" + icostCertificate
                        + "Allow Billing Manager OOBOTP=" + ballowBillingManagerOOBOTP + ",Bill Type OOBOTP=" + iselectBillTypeOOBOTP
                        + "Billing Duration OOBOTP=" + iselectBillingDurationOOBOTP + "Alert Before OOBOTP=" + iselectAlertBeforeOOBOTP + "Cost HWPKIOTP=" + icostOOBOTP
                        + "Billing cost per Tx OOBOTP=" + icostperTxOOBOTP + "Template ID" + itemplateId,
                        itemtype, channel.getChannelid());

            } else if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add Billing Manager Settings", resultString, retValue, "Setting Management",
                        "", "Failed to add Password Policy Settings",
                        itemtype, channel.getChannelid());
            }

        } else {
            BillingManagerSettings oldbillingObj = (BillingManagerSettings) sMngmt.getSetting(sessionId, _channelId, iType, iPreference);
            retValue = sMngmt.changeSetting(sessionId, _channelId, SettingsManagement.BILLING_MANAGER_SETTING, SettingsManagement.PREFERENCE_ONE, settingsObj, billingObj);
            log.debug("editbillingmanagersettings::changeSetting::" + retValue);
            
            String resultString = "ERROR";
            if (retValue == 0) {

                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Change Billing Manager Settings", resultString, retValue, "Setting Management",
                        "Allow Billing Manager=" + oldbillingObj.ballowBillingManager + "Allow Billing Manager SWOTP=" + oldbillingObj.ballowBillingManagerSWOTP
                        + ",Currency Type=" + oldbillingObj.strCurrencyType + ",Bill Type SWOTP=" + oldbillingObj.iselectBillTypeSWOTP
                        + "Billing Duration =" + oldbillingObj.iselectBillingDurationSWOTP + "Alert Before SWOTP=" + oldbillingObj.iselectAlertBeforeSWOTP
                        + "Cost SWOTP=" + oldbillingObj.icostSWOTP + "Allow Billing Manager HWOTP=" + oldbillingObj.ballowBillingManagerHWOTP
                        + ",Bill Type HWOTP=" + oldbillingObj.iselectBillTypeHWOTP + "Billing Duration HWOTP=" + oldbillingObj.iselectBillingDurationHWOTP
                        + "Alert Before HWOTP=" + oldbillingObj.iselectAlertBeforeHWOTP + "Cost HWOTP=" + oldbillingObj.icostHWOTP
                        + "Allow Billing Manager SWPKIOTP=" + oldbillingObj.ballowBillingManagerSWPKIOTP + ",Bill Type SWPKIOTP=" + oldbillingObj.iselectBillTypeSWPKIOTP
                        + "Billing Duration SWPKIOTP=" + oldbillingObj.iselectBillingDurationSWPKIOTP + "Alert Before SWPKIOTP=" + oldbillingObj.iselectAlertBeforeSWPKIOTP
                        + "Cost SWPKIOTP=" + oldbillingObj.icostSWPKIOTP + "Allow Billing Manager HWPKIOTP=" + oldbillingObj.ballowBillingManagerHWPKIOTP
                        + ",Bill Type HWPKIOTP=" + oldbillingObj.iselectBillTypeHWPKIOTP + "Billing Duration HWPKIOTP=" + oldbillingObj.iselectBillingDurationHWPKIOTP
                        + "Alert Before HWPKIOTP=" + oldbillingObj.iselectAlertBeforeHWPKIOTP + "Cost HWPKIOTP=" + oldbillingObj.icostHWPKIOTP
                        + ",Bill Type Certificate=" + oldbillingObj.iselectBillTypeCertificate + "Billing Duration Certificate=" + oldbillingObj.iselectBillingDurationCertificate
                        + "Alert Before Certificate=" + oldbillingObj.iselectAlertBeforeCertificate + "Cost Certificate=" + oldbillingObj.icostCertificate
                        + "Allow Billing Manager OOBOTP=" + oldbillingObj.ballowBillingManagerOOBOTP + ",Bill Type OOBOTP=" + oldbillingObj.iselectBillTypeOOBOTP
                        + "Billing Duration OOBOTP=" + oldbillingObj.iselectBillingDurationOOBOTP + "Alert Before OOBOTP=" + oldbillingObj.iselectAlertBeforeOOBOTP
                        + "Cost HWPKIOTP=" + oldbillingObj.icostOOBOTP + "Billing cost per Tx OOBOTP=" + oldbillingObj.icostperTxOOBOTP + "Template ID" + oldbillingObj.iTemplateId,
                        "Allow Billing Manager=" + ballowBillingManager + "Allow Billing Manager SWOTP=" + ballowBillingManagerSWOTP + ",Currency Type=" + icurrancyType + ",Bill Type SWOTP=" + iselectBillTypeSWOTP
                        + "Billing Duration =" + iselectBillingDurationSWOTP + "Alert Before SWOTP=" + iselectAlertBeforeSWOTP + "Cost SWOTP=" + icostSWOTP
                        + "Allow Billing Manager HWOTP=" + ballowBillingManagerHWOTP + ",Bill Type HWOTP=" + iselectBillTypeHWOTP
                        + "Billing Duration HWOTP=" + iselectBillingDurationHWOTP + "Alert Before HWOTP=" + iselectAlertBeforeHWOTP + "Cost HWOTP=" + icostHWOTP
                        + "Allow Billing Manager SWPKIOTP=" + ballowBillingManagerSWPKIOTP + ",Bill Type SWPKIOTP=" + iselectBillTypeSWPKIOTP
                        + "Billing Duration SWPKIOTP=" + iselectBillingDurationSWPKIOTP + "Alert Before SWPKIOTP=" + iselectAlertBeforeSWPKIOTP + "Cost SWPKIOTP=" + icostSWPKIOTP
                        + "Allow Billing Manager HWPKIOTP=" + ballowBillingManagerHWPKIOTP + ",Bill Type HWPKIOTP=" + iselectBillTypeHWPKIOTP
                        + "Billing Duration HWPKIOTP=" + iselectBillingDurationHWPKIOTP + "Alert Before HWPKIOTP=" + iselectAlertBeforeHWPKIOTP + "Cost HWPKIOTP=" + icostHWPKIOTP
                        + "Allow Billing Manager Certificate=" + ballowBillingManagerCertificate + ",Bill Type Certificate=" + iselectBillTypeCertificate
                        + "Billing Duration Certificate=" + iselectBillingDurationCertificate + "Alert Before Certificate=" + iselectAlertBeforeCertificate + "Cost Certificate=" + icostCertificate
                        + "Allow Billing Manager OOBOTP=" + ballowBillingManagerOOBOTP + ",Bill Type OOBOTP=" + iselectBillTypeOOBOTP
                        + "Billing Duration OOBOTP=" + iselectBillingDurationOOBOTP + "Alert Before OOBOTP=" + iselectAlertBeforeOOBOTP + "Cost HWPKIOTP=" + icostOOBOTP
                        + "Billing cost per Tx OOBOTP=" + icostperTxOOBOTP + "Threshold Cost OOBOTP =" + ithresholdcostOOBOTP + "Template ID" + itemplateId,
                        itemtype, channel.getChannelid());
            } else if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Change Billing Manager Settings", resultString, retValue, "Setting Management",
                        "Allow Billing Manager=" + oldbillingObj.ballowBillingManager + "Allow Billing Manager SWOTP=" + oldbillingObj.ballowBillingManagerSWOTP
                        + ",Currency Type=" + oldbillingObj.strCurrencyType + ",Bill Type SWOTP=" + oldbillingObj.iselectBillTypeSWOTP
                        + "Billing Duration =" + oldbillingObj.iselectBillingDurationSWOTP + "Alert Before SWOTP=" + oldbillingObj.iselectAlertBeforeSWOTP
                        + "Cost SWOTP=" + oldbillingObj.icostSWOTP + "Allow Billing Manager HWOTP=" + oldbillingObj.ballowBillingManagerHWOTP
                        + ",Bill Type HWOTP=" + oldbillingObj.iselectBillTypeHWOTP + "Billing Duration HWOTP=" + oldbillingObj.iselectBillingDurationHWOTP
                        + "Alert Before HWOTP=" + oldbillingObj.iselectAlertBeforeHWOTP + "Cost HWOTP=" + oldbillingObj.icostHWOTP
                        + "Allow Billing Manager SWPKIOTP=" + oldbillingObj.ballowBillingManagerSWPKIOTP + ",Bill Type SWPKIOTP=" + oldbillingObj.iselectBillTypeSWPKIOTP
                        + "Billing Duration SWPKIOTP=" + oldbillingObj.iselectBillingDurationSWPKIOTP + "Alert Before SWPKIOTP=" + oldbillingObj.iselectAlertBeforeSWPKIOTP
                        + "Cost SWPKIOTP=" + oldbillingObj.icostSWPKIOTP + "Allow Billing Manager HWPKIOTP=" + oldbillingObj.ballowBillingManagerHWPKIOTP
                        + ",Bill Type HWPKIOTP=" + oldbillingObj.iselectBillTypeHWPKIOTP + "Billing Duration HWPKIOTP=" + oldbillingObj.iselectBillingDurationHWPKIOTP
                        + "Alert Before HWPKIOTP=" + oldbillingObj.iselectAlertBeforeHWPKIOTP + "Cost HWPKIOTP=" + oldbillingObj.icostHWPKIOTP
                        + ",Bill Type Certificate=" + oldbillingObj.iselectBillTypeCertificate + "Billing Duration Certificate=" + oldbillingObj.iselectBillingDurationCertificate
                        + "Alert Before Certificate=" + oldbillingObj.iselectAlertBeforeCertificate + "Cost Certificate=" + oldbillingObj.icostCertificate
                        + "Allow Billing Manager OOBOTP=" + oldbillingObj.ballowBillingManagerOOBOTP + ",Bill Type OOBOTP=" + oldbillingObj.iselectBillTypeOOBOTP
                        + "Billing Duration OOBOTP=" + oldbillingObj.iselectBillingDurationOOBOTP + "Alert Before OOBOTP=" + oldbillingObj.iselectAlertBeforeOOBOTP
                        + "Cost HWPKIOTP=" + oldbillingObj.icostOOBOTP + "Billing cost per Tx OOBOTP=" + oldbillingObj.icostperTxOOBOTP + "Template ID" + oldbillingObj.iTemplateId,
                        "Failed To Edit Billing Manager Settings",
                        itemtype, channel.getChannelid());
            }
        }

        if (retValue != 0) {
            result = "error";
            message = "Billing Manager Settings Update Failed!!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
