/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.settings.TokenSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 *
 * @author mollatech1
 */
public class editotpsettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editotpsettings.class.getName());

    final String itemtype = "SETTINGS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //      PrintRequestParameters(request);
        log.info("Servlet started");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String OperatorID = operatorS.getOperatorid();

        String _channelId = channel.getChannelid();
        SettingsManagement sMngmt = new SettingsManagement();
        int retValue = -1;

        String result = "success";
        String message = "OTP Token Settings Update Successful!!!";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        
            log.debug("channel :: " + channel.getName());
            log.debug("getChannelid :: " + channel.getChannelid());
            log.debug("operatorS :: " + operatorS.getOperatorid());
            log.debug("sessionId :: " + sessionId);
            log.debug("remoteaccesslogin :: " + remoteaccesslogin);

        // String _preference = request.getParameter("_perference");
        int _iPreference = 1;

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_HARDWARE) != 0
                && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_OOB) != 0
                && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
            result = "error";
            message = "This feature is not available in this license!!!";

            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }

            out.print(json);
            out.flush();
            return;
        }

        int _type = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.Token;
        //  String strType = String.valueOf(_type);
        Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _type, _iPreference);

        TokenSettings tokenObj = null;
        boolean bAddSetting = false;

        if (settingsObj == null) {
            tokenObj = new TokenSettings();
            tokenObj.setChannelId(_channelId);
            //  emailObj.setChannelId(_channelId);
            //emailObj.setPreference(_iPreference);
            bAddSetting = true;
        } else {
            tokenObj = (TokenSettings) settingsObj;
        }
        if (_type == com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.Token) {
            if (_iPreference == com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE) {
                //OOBEmailChannelSettings emailObj = new OOBEmailChannelSettings();
                
                String OTPLength = request.getParameter("_OTPLength");
                String hashalgo=request.getParameter("_hashingalgo");
                log.debug("OTPLength :: " +OTPLength);
                int _OTPLength = 0;
                if (OTPLength != null) {
                    _OTPLength = Integer.valueOf(OTPLength);
                }

                String SOTPLength = request.getParameter("_SOTPLength");
                log.debug("SOTPLength :: " +SOTPLength);
                int _SOTPLength = 0;
                if (SOTPLength != null) {
                    _SOTPLength = Integer.valueOf(SOTPLength);
                }
                String OATHAlgoType = request.getParameter("_OATHAlgoType");
                log.debug("OATHAlgoType :: " +OATHAlgoType);
                int _OATHAlgoType = 0;
                if (OATHAlgoType != null) {
                    _OATHAlgoType = Integer.valueOf(OATHAlgoType);
                }
                //SW   
                String SWOTPLength = request.getParameter("_SWOTPLength");
                log.debug("SWOTPLength :: " +SWOTPLength);
                int _SWOTPLength = 0;
                if (SWOTPLength != null) {
                    _SWOTPLength = Integer.valueOf(SWOTPLength);
                }

                String SWSOTPLength = request.getParameter("_SWSOTPLength");
                log.debug("SWSOTPLength :: " +SWSOTPLength);
                int _SWSOTPLength = 0;
                if (SWSOTPLength != null) {
                    _SWSOTPLength = Integer.valueOf(SWSOTPLength);
                }
                String SWOATHAlgoType = request.getParameter("_SWOATHAlgoType");
                log.debug("SWOATHAlgoType :: " +SWOATHAlgoType);
                int _SWOATHAlgoType = 0;
                if (SWOATHAlgoType != null) {
                    _SWOATHAlgoType = Integer.valueOf(SWOATHAlgoType);
                }

                //HW   
                String HWOTPLength = request.getParameter("_HWOTPLength");
                log.debug("HWOTPLength :: " +HWOTPLength);
                int _HWOTPLength = 0;
                if (HWOTPLength != null) {
                    _HWOTPLength = Integer.valueOf(HWOTPLength);
                }

                String HWSOTPLength = request.getParameter("_HWSOTPLength");
                log.debug("HWSOTPLength :: " +HWSOTPLength);
                int _HWSOTPLength = 0;
                if (HWSOTPLength != null) {
                    _HWSOTPLength = Integer.valueOf(HWSOTPLength);
                }
                String HWOATHAlgoType = request.getParameter("_HWOATHAlgoType");
                log.debug("HWOATHAlgoType :: " +HWOATHAlgoType);
                int _HWOATHAlgoType = 0;
                if (HWOATHAlgoType != null) {
                    _HWOATHAlgoType = Integer.valueOf(HWOATHAlgoType);
                }

                String _SWWebTokenExpiryTime = request.getParameter("_SWWebTokenExpiryTime");
                log.debug("_SWWebTokenExpiryTime :: " +_SWWebTokenExpiryTime);
                int iSWWebTokenExpiryTime = 5;
                if (_SWWebTokenExpiryTime != null) {
                    iSWWebTokenExpiryTime = Integer.valueOf(_SWWebTokenExpiryTime);
                }

                String OTPAttempts = request.getParameter("_OTPAttempts");
                log.debug("OTPAttempts :: " +OTPAttempts);
                int _OTPAttempts = 0;
                if (OTPAttempts != null) {
                    _OTPAttempts = Integer.valueOf(OTPAttempts);
                }
//                 String SOTPAttempts = request.getParameter("_SOTPAttempts");
//                 int _SOTPAttempts= 0;
//                if(SOTPAttempts != null){
//                  _SOTPAttempts = Integer.valueOf(SOTPAttempts);
//                }
                String ValidationSteps = request.getParameter("_ValidationSteps");
                log.debug("ValidationSteps :: " +ValidationSteps);
                int _ValidationSteps = 0;
                if (ValidationSteps != null) {
                    _ValidationSteps = Integer.valueOf(ValidationSteps);
                }
                String duration = request.getParameter("_duration");
                log.debug("duration :: " +duration);
                int _duration = 0;
                if (duration != null) {
                    _duration = Integer.valueOf(duration);
                }

                String MultipleToken = request.getParameter("_MultipleToken");
                log.debug("MultipleToken :: " +MultipleToken);
                if (MultipleToken != null) {
                    boolean _MultipleToken = Boolean.valueOf(MultipleToken);
                    tokenObj.setResetAttemptsForAllTokens(_MultipleToken);
                }

                String _SWWebTokenPinAttempt = request.getParameter("_SWWebTokenPinAttempt");
                log.debug("_SWWebTokenPinAttempt :: " +_SWWebTokenPinAttempt);
                int iSWWebTokenPinAttempt = 3;
                if (_SWWebTokenExpiryTime != null) {
                    iSWWebTokenPinAttempt = Integer.valueOf(_SWWebTokenPinAttempt);
                }
                String EnforceMasking = request.getParameter("_EnforceMasking");
                log.debug("EnforceMasking :: " +EnforceMasking);
                if (EnforceMasking != null) {
                    boolean bEnforceMasking = Boolean.valueOf(EnforceMasking);
                    tokenObj.setbEnforceMasking(bEnforceMasking);
                }

                String _autoUnlockAfter = request.getParameter("_autoUnlockAfter");
                log.debug("_autoUnlockAfter :: " +_autoUnlockAfter);
                int i_autoUnlockAfter = 3;
//                if (_autoUnlockAfter != null) {
//                    i_autoUnlockAfter = Integer.valueOf(_autoUnlockAfter);
//                }

                String RegistrationExpiryTime = request.getParameter("_RegistrationExpiryTime");
                log.debug("RegistrationExpiryTime :: " +RegistrationExpiryTime);
                int _RegistrationExpiryTime = 0;
                if (RegistrationExpiryTime != null) {
                    _RegistrationExpiryTime = Integer.valueOf(RegistrationExpiryTime);
                }

                String _allowAlertT = request.getParameter("_allowAlertT");
                log.debug("_allowAlertT :: " +_allowAlertT);
                boolean allowAlert = false;
                if (_allowAlertT.equals("1")) {
                    allowAlert = true;
                }
                int gatewayType = 0;
                String _gatewayType = request.getParameter("_gatewayType");
                log.debug("_gatewayType :: " +_gatewayType);
                if (_gatewayType != null) {
                    gatewayType = Integer.parseInt(_gatewayType);
                }
//                  int thresholdCount = 0;
//                String _thresholdCount = request.getParameter("_thresholdCount");
//                if(_thresholdCount != null){
//                    thresholdCount = Integer.parseInt(_thresholdCount);
//                }
                int templateNameT = 0;
                String _templateNameT = request.getParameter("_templateNameT");
                log.debug("_templateNameT :: " +_templateNameT);

//                     if(_templateNameT != null){
//                    templateNameT = Integer.parseInt(_templateNameT);
//                }
                int iotpExpiryAfterSession = 0;
                String _otpExpiryAfterSession = request.getParameter("_otpExpiryAfterSession");
                log.debug("_otpExpiryAfterSession :: " +_otpExpiryAfterSession);

                if (_otpExpiryAfterSession != null) {
                    iotpExpiryAfterSession = Integer.parseInt(_otpExpiryAfterSession);
                }

                int iotpLockedAfter = 0;
                String _otpLockedAfter = request.getParameter("_otpLockedAfter");
                log.debug("_otpLockedAfter :: " +_otpLockedAfter);
                if (_otpLockedAfter != null) {
                    iotpLockedAfter = Integer.parseInt(_otpLockedAfter);
                }
                int iotpExpiryAfterSessionSW = 0;
                String _otpExpiryAfterSessionSW = request.getParameter("_otpExpiryAfterSessionSW");
                log.debug("_otpExpiryAfterSessionSW :: " +_otpExpiryAfterSessionSW);
                if (_otpExpiryAfterSessionSW != null) {
                    iotpExpiryAfterSessionSW = Integer.parseInt(_otpExpiryAfterSessionSW);
                }

                int iotpLockedAfterSW = 0;
                String _otpLockedAfterSW = request.getParameter("_otpLockedAfterSW");
                log.debug("_otpLockedAfterSW :: " +_otpLockedAfterSW);
                if (_otpLockedAfterSW != null) {
                    iotpLockedAfterSW = Integer.parseInt(_otpLockedAfterSW);
                }

                int iotpExpiryAfterSessionHW = 0;
                String _otpExpiryAfterSessionHW = request.getParameter("_otpExpiryAfterSessionHW");
                log.debug("_otpExpiryAfterSessionHW :: " +_otpExpiryAfterSessionHW);
                if (_otpExpiryAfterSessionHW != null) {
                    iotpExpiryAfterSessionHW = Integer.parseInt(_otpExpiryAfterSessionHW);
                }

                int iotpLockedAfterHW = 0;
                String _otpLockedAfterHW = request.getParameter("_otpLockedAfterHW");
                log.debug("_otpLockedAfterHW :: " +_otpLockedAfterHW);
                if (_otpLockedAfterHW != null) {
                    iotpLockedAfterHW = Integer.parseInt(_otpLockedAfterHW);
                }

                String _autoUnlockOOBAfter = request.getParameter("_autoUnlockOOBAfter");
                log.debug("_autoUnlockOOBAfter :: " +_autoUnlockOOBAfter);
                int i_autoUnlockOOBAfter = 3;
                if (_autoUnlockOOBAfter != null) {
                    i_autoUnlockOOBAfter = Integer.valueOf(_autoUnlockOOBAfter);
                }
                String _autoUnlockSWAfter = request.getParameter("_autoUnlockSWAfter");
                log.debug("_autoUnlockSWAfter :: " +_autoUnlockSWAfter);
                int i_autoUnlockSWAfter = 3;
                if (_autoUnlockSWAfter != null) {
                    i_autoUnlockSWAfter = Integer.valueOf(_autoUnlockSWAfter);
                }
                String _autoUnlockHWAfter = request.getParameter("_autoUnlockHWAfter");
                log.debug("_autoUnlockHWAfter :: " +_autoUnlockHWAfter);
                int i_autoUnlockHWAfter = 3;
                if (_autoUnlockHWAfter != null) {
                    i_autoUnlockHWAfter = Integer.valueOf(i_autoUnlockHWAfter);
                }

                String _pukCodeLengthHW = request.getParameter("_pukCodeLengthHW");
                log.debug("_pukCodeLengthHW :: " +_pukCodeLengthHW);
                int i__pukCodeLengthHW = 8;
                if (_pukCodeLengthHW != null) {
                    i__pukCodeLengthHW = Integer.valueOf(_pukCodeLengthHW);
                }

                String _pukCodeValidityHW = request.getParameter("_pukCodeValidityHW");
                log.debug("_pukCodeValidityHW :: " +_pukCodeValidityHW);
                int i_pukCodeValidityHW = 3;
                if (_pukCodeValidityHW != null) {
                    i_pukCodeValidityHW = Integer.valueOf(_pukCodeValidityHW);
                }

                tokenObj.setPukCodeLengthHW(i__pukCodeLengthHW);
                tokenObj.setPukCodeValidityHW(i_pukCodeValidityHW);
                tokenObj.hashAlgo=hashalgo;
                tokenObj.setOtpLockAfterXDaysHW(iotpLockedAfterHW);
                tokenObj.setOtpExpiryAfterSessionHW(iotpExpiryAfterSessionHW);

                tokenObj.setOtpLockAfterXDaysSW(iotpLockedAfterSW);
                tokenObj.setOtpExpiryAfterSessionSW(iotpExpiryAfterSessionSW);

                tokenObj.setOtpLockAfterXDays(iotpLockedAfter);
                tokenObj.setOtpExpiryAfterSession(iotpExpiryAfterSession);

                tokenObj.setTokenAlert(allowAlert);
                tokenObj.setTokenAlertVia(gatewayType);

                //changed by vikram
                TemplateManagement tempObj = new TemplateManagement();
                Templates tObj = tempObj.LoadbyName(_channelId, _templateNameT);
                templateNameT = tObj.getTemplateid();
                tokenObj.setTokenAlertTemplateID(templateNameT);
            //end of change 

                //tokenObj.setTokenThresholdCount(thresholdCount);
                tokenObj.setOtpLengthOOBToken(_OTPLength);
                tokenObj.setSotpLengthOOBToken(_SOTPLength);
                tokenObj.setOOBAlgoType(_OATHAlgoType);

                tokenObj.setSotpLengthSWToken(_SWSOTPLength);
                tokenObj.setOtpLengthSWToken(_SWOTPLength);
                tokenObj.setSwAlgoType(_SWOATHAlgoType);

                tokenObj.setSotpLengthHWToken(_HWSOTPLength);
                tokenObj.setOtpLengthHWToken(_HWOTPLength);
                tokenObj.setHWAlgoType(_HWOATHAlgoType);

                tokenObj.setOtpAllowed(_OTPAttempts);
                //tokenObj.setSotpAllowed(_SOTPAttempts);
                tokenObj.setOtpSteps(_ValidationSteps);
                tokenObj.setOtpDurationInSeconds(_duration);
                tokenObj.setiSWWebTokenExpiryTime(iSWWebTokenExpiryTime);
                tokenObj.setiSWWebTokenPinAttempt(iSWWebTokenPinAttempt);

                tokenObj.setRegistrationValidity(_RegistrationExpiryTime);

                tokenObj.setAutounlockWEBAfter(i_autoUnlockAfter);
                tokenObj.setAutounlockOOBAfter(i_autoUnlockOOBAfter);
                tokenObj.setAutounlockSWAfter(i_autoUnlockSWAfter);
                tokenObj.setAutounlockHWAfter(i_autoUnlockHWAfter);

            } else {

            }
        }
        AuditManagement audit = new AuditManagement();
        if (bAddSetting == true) {
            retValue = sMngmt.addSetting(sessionId, _channelId, _type, _iPreference, tokenObj);
            log.debug("editotpsettings::addSetting" +retValue);
            String resultString = "ERROR";
            if (retValue == 0) {
                resultString = "SUCCESS";
                //audit.setIP("127.0.0.1");

                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add OTP Token Setting", resultString, retValue, "Setting Management",
                        "", "OOB OTP Token Length" + tokenObj.getOtpLengthOOBToken() + "OOB SOTP Token Length" + tokenObj.getSotpLengthOOBToken()
                        + "OOB ALgo Type" + tokenObj.getOOBAlgoType() + "SW OTP Token Length" + tokenObj.getOtpLengthSWToken()
                        + "SW SOTP Token Length" + tokenObj.getSotpLengthSWToken() + "SW Algorithm Type =" + tokenObj.getSwAlgoType()
                        + "OTP Token Attempts =" + tokenObj.getOtpAllowed()
                        + "Validation Steps =" + tokenObj.getOtpSteps()
                        + "OTP Duration" + tokenObj.getOtpDurationInSeconds() + "Allowed Multiple =" + tokenObj.IsResetAttemptsForAllTokens()
                        + "Registration Validity" + tokenObj.getRegistrationValidity(),
                        itemtype, _channelId);
            } else if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add OTP Token Setting", resultString, retValue, "Setting Management",
                        "", "Failed to add OTP Token Settings",
                        itemtype, _channelId);
            }

        } else {
            TokenSettings oldtokenObj = (TokenSettings) sMngmt.getSetting(sessionId, _channelId, _type, _iPreference);
            retValue = sMngmt.changeSetting(sessionId, _channelId, _type, _iPreference, settingsObj, tokenObj);
            log.debug("retValue :: " +retValue);
                
            String resultString = "ERROR";

            if (retValue == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Change OTP Token Setting", resultString, retValue, "Setting Management",
                        "OOB OTP Token Length=" + oldtokenObj.getOtpLengthOOBToken() + "OOB SOTP Token Length" + oldtokenObj.getSotpLengthOOBToken()
                        + "OOB ALgo Type" + oldtokenObj.getOOBAlgoType() + "SW OTP Token Length" + oldtokenObj.getOtpLengthSWToken()
                        + "SW SOTP Token Length" + oldtokenObj.getSotpLengthSWToken() + "SW Algorithm Type =" + oldtokenObj.getSwAlgoType()
                        + "OTP Token Attempts =" + oldtokenObj.getOtpAllowed()
                        + "Validation Steps =" + oldtokenObj.getOtpSteps()
                        + "OTP Duration" + oldtokenObj.getOtpDurationInSeconds() + "Allowed Multiple =" + oldtokenObj.IsResetAttemptsForAllTokens(),
                        "OOB OTP Token Length" + tokenObj.getOtpLengthOOBToken() + "OOB SOTP Token Length" + tokenObj.getSotpLengthOOBToken()
                        + "OOB ALgo Type" + tokenObj.getOOBAlgoType() + "SW OTP Token Length" + tokenObj.getOtpLengthSWToken()
                        + "SW SOTP Token Length" + tokenObj.getSotpLengthSWToken() + "SW Algorithm Type =" + tokenObj.getSwAlgoType()
                        + "OTP Token Attempts =" + tokenObj.getOtpAllowed()
                        + "Validation Steps =" + tokenObj.getOtpSteps()
                        + "OTP Duration" + tokenObj.getOtpDurationInSeconds() + "Allowed Multiple =" + tokenObj.IsResetAttemptsForAllTokens()
                        + "Registration Validity" + tokenObj.getRegistrationValidity(),
                        itemtype, _channelId);

            } else if (retValue != 0) {

                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Change OTP Token Setting", resultString, retValue, "Setting Management",
                        "OOB OTP Token Length=" + oldtokenObj.getOtpLengthOOBToken() + "OOB SOTP Token Length" + oldtokenObj.getSotpLengthOOBToken()
                        + "OOB ALgo Type" + oldtokenObj.getOOBAlgoType() + "SW OTP Token Length" + oldtokenObj.getOtpLengthSWToken()
                        + "SW SOTP Token Length" + oldtokenObj.getSotpLengthSWToken() + "SW Algorithm Type =" + oldtokenObj.getSwAlgoType()
                        + "OTP Token Attempts =" + oldtokenObj.getOtpAllowed()
                        + "Validation Steps =" + oldtokenObj.getOtpSteps()
                        + "OTP Duration" + oldtokenObj.getOtpDurationInSeconds() + "Allowed Multiple =" + oldtokenObj.IsResetAttemptsForAllTokens()
                        + "Registration Validity" + tokenObj.getRegistrationValidity(),
                        "Failed TO Edit Setting", itemtype, _channelId);
            }

        }

        if (retValue != 0) {
            result = "error";
            message = "OTP Settings Update Failed!!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
