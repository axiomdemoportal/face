/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.users;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SecurePhraseManagment;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class removeSecurePhrase extends HttpServlet {
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(removeSecurePhrase.class.getName());

    final String itemtype = "IMAGEAUTH";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());

        response.setContentType("application/json");

        String _userid = request.getParameter("_userid");
        log.debug("_userid :: "+_userid);
        String result = "success";
        String message = "User removed successfully....";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        if (_userid == null) {
            result = "error";
            message = "User id not found!!!";

            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }

            out.print(json);
            out.flush();
            return;
        }
        SecurePhraseManagment secPhrase = new SecurePhraseManagment();
        AuditManagement audit = new AuditManagement();
        int retValue = secPhrase.DeleteSecurePhrase(sessionId, channel.getChannelid(), _userid);
        log.debug("DeleteSecurePhrase :: "+retValue);
        String resultString = "Failure";
        if (retValue == 0) {
            resultString = "Success";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Remove Secure Phrase", resultString, retValue,
                    "User Management", "User ID=" + _userid,
                    "Removed", itemtype, _userid);
            result = "success";
            message = "Secure Phrase Removed successfully!!!";

            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }

            out.print(json);
            out.flush();
            return;

        } else {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Remove Secure Phrase", resultString, retValue,
                    "User Management", "User ID=" + _userid,
                    "Failed to removed", itemtype, _userid);
            result = "error";
            message = "Secure Phrase Removed successfully!!!";

            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }

            out.print(json);
            out.flush();
            log.info("is ended :: ");
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
