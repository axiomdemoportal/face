/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.OOBSocialChannelSettings;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class loadsocialsettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadsocialsettings.class.getName());

    private JSONObject SettingsWhenEmpty(int _type1, int _preference1, Object settingsObj, String facebookURL, String linkedinWebURL, String accessRedirect, String twitterWebURL, String twitterCallback) {
        JSONObject json = new JSONObject();
        if (_type1 == editsettings.SOCIAL && _preference1 == editsettings.PREFERENCE_ONE) {
            try { json.put("_ip", "");
            json.put("_port", "");
            json.put("_facebookappid", "");
            json.put("_facebookappsecret", "");
            json.put("linkedinkey", "");
            json.put("linkedinsecret", "");
            json.put("linkedintoken", "");
            json.put("linkedinaccesssecret", "");
            json.put("_twitterkey", "");
            json.put("_twittersecret", "");
            json.put("_facebookURL", facebookURL);
            json.put("_linkedinURL", linkedinWebURL);
            json.put("_linkedinAccessURL", accessRedirect);
            json.put("_twitterURL", twitterWebURL);
            json.put("_twitterCallbackURL", twitterCallback);
            json.put("_reserve1", "");
            json.put("_reserve2", "");
            json.put("_reserve3", "");
            json.put("_status", "");
            json.put("_retries", 2);
            json.put("_retryduration", 10);
            }catch(Exception e){}
        } else if (_type1 == editsettings.SOCIAL && _preference1 == editsettings.PREFERENCE_TWO) {
            try { json.put("_ip", "");
            json.put("_port", "");
            json.put("_facebookappid", "");
            json.put("_facebookappsecret", "");
            json.put("linkedinkey", "");
            json.put("linkedinsecret", "");
            json.put("linkedintoken", "");
            json.put("linkedinaccesssecret", "");
            json.put("_twitterkey", "");
            json.put("_twittersecret", "");

            json.put("_facebookURL", facebookURL);
            json.put("_linkedinURL", linkedinWebURL);
            json.put("_linkedinAccessURL", accessRedirect);
            json.put("_twitterURL", twitterWebURL);
            json.put("_twitterCallbackURL", twitterCallback);
            json.put("_reserve1", "");
            json.put("_reserve2", "");
            json.put("_reserve3", "");
            json.put("_status", "");
            json.put("_retries", 2);
            json.put("_retryduration", 10);
            }catch(Exception e){}
        }
        return json;
    }

    private JSONObject SettingsWhenPresent(Object settingsObj, String facebookURL, String linkedinWebURL, String accessRedirect, String twitterWebURL, String twitterCallback) {
        JSONObject json = new JSONObject();
        if (settingsObj instanceof OOBSocialChannelSettings) {
            OOBSocialChannelSettings social = (OOBSocialChannelSettings) settingsObj;
            if (social.getPreference() == 1) {
                try { json.put("_ip", social.getIp());
                json.put("_port", social.getPort());
                json.put("_facebookappid", social.getFacebookappid());
                json.put("_facebookappsecret", social.getFacebookappsecret());
                json.put("_linkedinkey", social.getLinkedinkey());
                json.put("_linkedinsecret", social.getLinkedinsecret());
                json.put("_linkedintoken", social.getLinkedintoken());
                json.put("_linkedinaccesssecret", social.getLinkedinaccesssecret());
                json.put("_twitterkey", social.getTwitterkey());
                json.put("_twittersecret", social.getTwittersecret());
                json.put("_facebookURL", facebookURL);
                json.put("_linkedinURL", linkedinWebURL);
                json.put("_linkedinAccessURL", accessRedirect);
                json.put("_twitterURL", twitterWebURL);
                json.put("_twitterCallbackURL", twitterCallback);
                json.put("_reserve1", social.getReserve1());
                json.put("_reserve2", social.getReserve2());
                json.put("_reserve3", social.getReserve3());
                json.put("_status", social.getStatus());
                json.put("_retries", social.getRetrycount());
                json.put("_retryduration", social.getRetryduration());
                }catch(Exception e){}

            } else {
                try { 
                json.put("_ip", social.getIp());
                json.put("_port", social.getPort());
                json.put("_facebookappid", social.getFacebookappid());
                json.put("_facebookappsecret", social.getFacebookappsecret());
                json.put("_linkedinkey", social.getLinkedinkey());
                json.put("_linkedinsecret", social.getLinkedinsecret());
                json.put("_linkedintoken", social.getLinkedintoken());
                json.put("_linkedinaccesssecret", social.getLinkedinaccesssecret());
                json.put("_twitterkey", social.getTwitterkey());
                json.put("_twittersecret", social.getTwittersecret());
                json.put("_facebookURL", facebookURL);
                json.put("_linkedinURL", linkedinWebURL);
                json.put("_linkedinAccessURL", accessRedirect);
                json.put("_twitterURL", twitterWebURL);
                json.put("_twitterCallbackURL", twitterCallback);
                json.put("_reserve1", social.getReserve1());
                json.put("_reserve2", social.getReserve2());
                json.put("_reserve3", social.getReserve3());
                json.put("_status", social.getStatus());
                json.put("_retries", social.getRetrycount());
                json.put("_retrydurationS", social.getRetryduration());
                }catch(Exception e){}
            }
        }
        return json;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        String _type = request.getParameter("_type");
        String _preference = request.getParameter("_preference");
        int _type1 = Integer.parseInt(_type);
        int _preference1 = Integer.parseInt(_preference);
        
        log.debug("loadsocialsettings::channel is::"+channel.getName());
        log.debug("loadsocialsettings::sessionid::"+sessionId);
        log.debug("loadsocialsettings::type is::"+_type1);
        log.debug("loadsocialsettings::preference is::"+_preference1);
        
        int port = request.getLocalPort();
        String server = request.getServerName();
        String channelname = request.getContextPath();
        String scheme = request.getScheme();
        String facebookURL = scheme.concat("://").concat(server).concat(":").concat(String.valueOf(port)).concat(channelname);
        String linkedinWebURL = scheme.concat("://").concat(server).concat(":").concat(String.valueOf(port)).concat(channelname);
        String accessRedirect = scheme.concat("://").concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("/").concat("linkedinAccessTokenRequest");
        String twitterWebURL = scheme.concat("://").concat(server).concat(":").concat(String.valueOf(port)).concat(channelname);
        String twitterCallback = scheme.concat("://").concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("/").concat("twitterAccessTokenRequest");


        JSONObject json = null;//new JSONObject();
        PrintWriter out = response.getWriter();

        try {
            SettingsManagement sMngmt = new SettingsManagement();
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _type1, _preference1);
            if (settingsObj != null) {
                json = SettingsWhenPresent(settingsObj, facebookURL, linkedinWebURL, accessRedirect, twitterWebURL, twitterCallback);
            } else {
                json = SettingsWhenEmpty(_type1, _preference1, settingsObj, facebookURL, linkedinWebURL, accessRedirect, twitterWebURL, twitterCallback);
            }

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
            // TODO handle custom exceptions here
        }
        try {
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
