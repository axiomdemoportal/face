/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.certificates;

import com.mollatech.axiom.connector.communication.ThirPartyAccountInfo;
import com.mollatech.axiom.connector.communication.UserCertificate;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.connector.user.OrganizationDetails;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement;
import com.mollatech.axiom.v2.face.handler.validateSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Pramod
 */
public class certificateGenerator extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(certificateGenerator.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        final String itemType = "CERTIFICATEMANAGEMENT";
        final String itemTypeAUTH = "GENERATION";
        String result = "";
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel ::" + channel.getName());
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin ::"+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS::" + operatorS.getOperatorid());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId::"+ sessionId);
        String _certType = request.getParameter("_certType");
        log.debug("_certType::"+_certType);
        int _caType = Integer.parseInt(request.getParameter("_caType"));
        log.debug("_caType ::"+_caType);
        String _product_type = request.getParameter("_product_type");
        log.debug("_product_type::"+_product_type);
        String common_name = request.getParameter("common_name");
        log.debug("common_name::"+common_name);
        String _organizationName = request.getParameter("_txtorganizationName");
        log.debug("_organizationName ::"+_organizationName);
        String OrganizationUnit = request.getParameter("_txtOrganizationUnit");
        log.debug("OrganizationUnit::"+OrganizationUnit);
        String _txtLocalityName = request.getParameter("_txtLocalityName");
        log.debug("_txtLocalityName::"+_txtLocalityName);
        String _txtState = request.getParameter("_txtState");
        log.debug("_txtState::"+_txtState);
        String _txtCountry = request.getParameter("_txtCountry");
        log.debug("_txtCountry ::"+_txtCountry);
        String _txtEmail = request.getParameter("_txtEmail");
        log.debug("_txtEmail::"+_txtEmail);
        String _txtApproverEamil = request.getParameter("selApproverEmailTP");
        log.debug("_txtApproverEamil ::"+_txtApproverEamil);
        String phonno = request.getParameter("_txtphon");
        log.debug("phonno ::"+phonno);
        String _txtLastName = request.getParameter("_txtLastName");
        log.debug("_txtLastName ::"+_txtLastName);
        String _txtFirstName = request.getParameter("_txtFirstName");
        log.debug("_txtFirstName ::"+_txtFirstName);
        int _certValidity=0;
        String _validityInDays=request.getParameter("_txtval");
        if(_validityInDays!=null){ _certValidity=Integer.parseInt(_validityInDays);}
        String _keySize=request.getParameter("_keysize");
        int _keySizeInBits=0;
        if(_keySize!=null)
        {
        _keySizeInBits=Integer.parseInt(_keySize);
        }
        String _city = "";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        

        if (_certType.equals("1")) {
            _certType = "APPCERTS";
        } else if (_certType.equals("2")) {
            _certType = "SSLCERTS";

        }
        AuditManagement audit = new AuditManagement();
        String resultJson = "success";
        String resultString = "";
        String _messageJson = "Certificate generated successfully for domain or ip:" + common_name;
        int retval = -1;
        
         
          
         
         
         
        try {
            if (_caType == 1) {
              
                  CertificateManagement certMgmt = new CertificateManagement();
           int  cert_count= certMgmt.getApplicationCertificateCount(channel.getChannelid());
              
                if(AxiomProtect.GetAllowedSSLCertifcateCount(AxiomProtect.SSL_CERTIFICATE_COUNT)<cert_count)
                {
                    log.debug("Certifcate generation blocked due to invalid license");
                 try {
                     resultJson="error";
                     _messageJson="Application certificates limit has been reached,Please upgrade your license..!!!";
                        json.put("_result", resultJson);
                        json.put("_message", _messageJson);
                    } catch (Exception e) {
                        log.error("exception caught :: ",e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                  
                }
            result = certMgmt.GenerateCertificateFaceforSSLandApplication(sessionId, channel.getChannelid(), _txtCountry, _txtState, _organizationName, OrganizationUnit, _txtEmail, common_name, _certType, _product_type,_certValidity,_keySizeInBits);
            log.debug("certificateGenerator::GenerateCertificateFaceforSSLandApplication::"+result);
                if (result != null && !result.isEmpty()) {
                    resultString = "SUCCESS";
                    retval = 0;
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            //ipaddress,
                            channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), "Generate Certificate(v3)", resultString, retval,
                            "Certificate Management",
                            "", " ",
                            itemType, common_name);
                    try {
                        json.put("_result", resultJson);
                        json.put("_message", _messageJson);
                    } catch (Exception e) {
                        log.error("exception caught :: ",e);
                    }
                    out.print(json);
                    out.flush();

                } else {
                    resultJson = "error";
                    _messageJson = "Certificate generation failed!!!";
                    try {
                        retval = -1;
                        audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                                request.getRemoteAddr(),
                                //ipaddress,
                                channel.getName(),
                                remoteaccesslogin, operatorS.getName(), new Date(), "Generate Certificate(v3)", resultJson, retval,
                                "Certificate Management",
                                "", " ",
                                itemType, common_name);

                        json.put("_result", resultJson);
                        json.put("_message", _messageJson);
                    } catch (Exception e) {
                        log.error("exception caught :: ",e);
                        
                    }
                    out.print(json);
                    out.flush();

                }
            } else {
                ThirPartyAccountInfo thidPartyAccountInfo = new ThirPartyAccountInfo();
                common_name = request.getParameter("common_nameTP");
                _organizationName = request.getParameter("_txtorganizationNameTP");
                OrganizationUnit = request.getParameter("_txtOrganizationUnitTP");
                _txtLocalityName = request.getParameter("_txtLocalityNameTP");
                _txtState = request.getParameter("_txtStateTP");
                _txtCountry = request.getParameter("_txtCountryTP");
                _txtEmail = request.getParameter("_txtEmailTP");
                _city = request.getParameter("_txtCountryTP");
                log.debug("certificateGenerator::Common Name is::"+common_name);
                log.debug("certificateGenerator::Name of organization is::"+_organizationName);
                log.debug("certificateGenerator::Unit of organization is::"+OrganizationUnit);
                log.debug("certificateGenerator::Locality name of text is::"+_txtLocalityName);
                log.debug("certificateGenerator::text State::"+_txtState);
                log.debug("certificateGenerator::text Country::"+_txtCountry);
                log.debug("certificateGenerator::text Email::"+_txtEmail);
                log.debug("certificateGenerator::city is::"+_city);
                
                AuthUser auser = new AuthUser();
                thidPartyAccountInfo.setProductCode("DV");
                thidPartyAccountInfo.setApproverEmail(_txtApproverEamil);
                thidPartyAccountInfo.setOrderKind("NEW");
                thidPartyAccountInfo.setContactinfo(_txtFirstName + _txtLastName);
                thidPartyAccountInfo.setContactinfo2(_txtFirstName + _txtLastName);
                thidPartyAccountInfo.setLicenses("99");
                auser.setLocation(_txtCountry + _txtCountry);
                auser.setPhoneNo(phonno);
                auser.setUserName(_txtFirstName + _txtLastName);
                OrganizationDetails orgDetails = new OrganizationDetails();
                orgDetails.Organisation = _organizationName;
                orgDetails.City = _city;
                orgDetails.pincode = "411036";
                orgDetails.Country = _txtCountry;
                CertificateManagement certMgmt = new CertificateManagement();
                UserCertificate uCert = null;
                uCert = (UserCertificate) request.getSession().getAttribute("CSR");
                int res = certMgmt.GenerateCertificateForThirdParty(uCert, sessionId, channel.getChannelid(), common_name, auser, orgDetails, thidPartyAccountInfo);
                log.debug("GenerateCertificateForThirdParty::"+res);
                if (res != 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            //ipaddress,
                            channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), "Generate Certificate(v3)", resultJson, retval,
                            "Certificate Management",
                            "", " ",
                            itemType, common_name);

                    try {
                        json.put("_result", "error");
                        json.put("_message", "Failed to generae certificate");
                    } catch (Exception e) {
                        log.error("exception caught :: ",e);
                        
                    }
                    out.print(json);
                    out.flush();

                } else {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            //ipaddress,
                            channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), "Generate Certificate(v3)", resultJson, retval,
                            "Certificate Management",
                            "", "",
                            itemType, common_name);

                    try {
                        json.put("_result", "success");
                        json.put("_message", "Certificate has been emailed to account holder!!");
                    } catch (Exception e) {
                        log.error("exception caught :: ",e);
                        
                    }
                    out.print(json);
                    out.flush();
                    return;

                }

            }
        } catch (Exception ex) {
            log.error("exception caught :: ",ex);
            
        }
        log.info("is ended :: ");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
