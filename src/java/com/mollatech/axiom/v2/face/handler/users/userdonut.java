/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.users;

import com.mollatech.axiom.face.common.donut;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mollatech1
 */
public class userdonut extends HttpServlet {
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(userdonut.class.getName());

   // String channelId = "Pygvy6iTPgOmWm3JKifeg5aWHi8=";
     final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;
    final int LOCKED_STATUS = -1;
    final int REMOVE_STATUS = -99;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
          response.setContentType("application/json");
          log.info("is started :: ");
        PrintWriter out = response.getWriter();
        try {
            String _searchtext = request.getParameter("_searchtext");
            log.debug("_searchtext :: "+_searchtext);
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("_searchtext :: "+_searchtext);
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel.getName());
            String _fromDate = request.getParameter("_fromDate");
            log.debug("_fromDate :: "+_fromDate);
            String _toDate = request.getParameter("_toDate");
            log.debug("_toDate :: "+_toDate);
            String channelId = channel.getChannelid();
            log.debug("channelId :: "+channelId);

            UserManagement uMngt = new UserManagement();
            AuthUser user[] = null;
            Date dstartdate= null; Date denddate=null;
            if (_fromDate !=null&&_toDate!=null && !_fromDate.equals("") && !_toDate.equals("")) {
                SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
                try {
                    dstartdate = format.parse(_fromDate);
                     denddate = format.parse(_toDate);
                } catch (ParseException ex) {
                   log.error("exception caught ",ex);
                }               
            }
            int iActiveCount = 0;
            int iSuspendCount = 0;
            int iLockedCount = 0;
            int iRemovedCount = 0;
//            int iActiveCount = uMngt.getCountOfUserByStatus(channelId, ACTIVE_STATUS,_searchtext);
//            int iSuspendCount = uMngt.getCountOfUserByStatus(channelId, SUSPEND_STATUS,_searchtext);
//            int iLockedCount = uMngt.getCountOfUserByStatus(channelId, LOCKED_STATUS,_searchtext);
//            int iRemovedCount = uMngt.getCountOfUserByStatus(channelId, REMOVE_STATUS,_searchtext);
            user = uMngt.SearchUserByStatusBetDate(sessionId, channel.getChannelid(), _searchtext, ACTIVE_STATUS,dstartdate,denddate);
            if(user != null){
                iActiveCount =user.length;
            }
            user = uMngt.SearchUserByStatusBetDate(sessionId, channel.getChannelid(), _searchtext, SUSPEND_STATUS,dstartdate,denddate);
            if(user != null){
                iSuspendCount = user.length;
            }
            user = uMngt.SearchUserByStatusBetDate(sessionId, channel.getChannelid(), _searchtext, LOCKED_STATUS,dstartdate,denddate);
            if(user != null){
                iLockedCount = user.length;
            }
            user = uMngt.SearchUserByStatusBetDate(sessionId, channel.getChannelid(), _searchtext, REMOVE_STATUS,dstartdate,denddate);
            if(user != null){
                iRemovedCount = user.length;
            }
            ArrayList<donut> sample = new ArrayList<donut>();
          
              sample.add(new donut(iActiveCount, "Active"));
              sample.add(new donut(iSuspendCount, "Suspended"));
              sample.add(new donut(iLockedCount, "Locked"));
              sample.add(new donut(iRemovedCount, "Removed"));
             
//               for (int i = 0; i < sample.size(); i++) {
//                System.out.println(sample.get(i));
//                }
                Gson gson = new Gson();

            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<donut>>() {
            }.getType());

            JsonArray jsonArray = element.getAsJsonArray();
            //response.setContentType("application/json");
            out.print(jsonArray);
        
           
        } finally {            
            out.close();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
