package com.mollatech.axiom.v2.face.handler.otptokens;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Otptokens;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class resynctoken extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(resynctoken.class.getName());

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final String itemType = "OTPTOKENS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());

        String _userid = request.getParameter("_userIDS");
        log.debug("_userid :: "+_userid);
        if (_userid == null || _userid.isEmpty() == true) {
            _userid = request.getParameter("_userIDHW");
        }

        String _otp1 = request.getParameter("_FirstOtp");
        log.debug("_otp1 :: "+_otp1);
        String _otp2 = request.getParameter("_SecondOtp");
        log.debug("_otp2 :: "+_otp2);
        String _srno = request.getParameter("_srno");
        log.debug("_srno :: "+_srno);
        if (_srno == null || _srno.isEmpty() == true) {
            _srno = request.getParameter("_srnoHW");
        }

        String _category = request.getParameter("_categoryS");
        log.debug("_category :: "+_category);

        if (_category == null || _category.isEmpty() == true) {
            _category = request.getParameter("_categoryHW");
        }


//        String _subcategory = request.getParameter("_subcategory");
//        int subCategory = 0;
//        if (_subcategory != null) {
//            subCategory = Integer.parseInt(_subcategory);
//        }

        //String _category = request.getParameter("_category");
        int category = 0;
        if (_category != null) {
            category = Integer.parseInt(_category);
        }
        String resultString = "ERROR";
        String result = "success";
        String message = "Token Resynchronization Successful";
        String strPassword = null;
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        
        if (_userid == null || _otp1 == null || _otp2 == null || _srno == null) {
            result = "error";
            message = "Invalid Parameters!!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){log.error("Exception caught :: ",e);}
            out.print(json);
            out.flush();
            return;
        }
        int retValue = -1;

        OTPTokenManagement oManagement = new OTPTokenManagement(channel.getChannelid());
        AuditManagement audit = new AuditManagement();
        Otptokens oldtoken = oManagement.getOtpObjByUserId(sessionId, channel.getChannelid(), _userid, category);
        int oldTimeDiff = oldtoken.getTimediff();

        retValue = oManagement.resyncOTP(sessionId, channel.getChannelid(), _userid, category, _otp1, _otp2);
        log.debug("resyncOTP :: "+retValue);

        Otptokens newtoken = oManagement.getOtpObjByUserId(sessionId, channel.getChannelid(), _userid, category);
        int newTimeDiff = newtoken.getTimediff();


        if (retValue == 0) {
            resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Resynchronize Token",
                    resultString, retValue, "Token Management",
                    "Old Time Difference =" + oldTimeDiff, "New Time Difference =" + newTimeDiff,
                    itemType, 
                    _userid);
        }else{
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), 
                    "Resynchronize Token",
                    resultString, retValue, 
                    "Token Management",
                    "Old Time Difference =" + oldTimeDiff, 
                    "Failed to resynchronize Token",
                    itemType, 
                    _userid);

            result = "error";
            message = "Resynchronization failed!!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        try {
            json.put("_result", result);
            json.put("_message", message);
        }catch(Exception e){
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
