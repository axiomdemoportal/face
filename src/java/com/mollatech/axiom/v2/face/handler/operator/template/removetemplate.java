package com.mollatech.axiom.v2.face.handler.operator.template;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Pushmessagemappers;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PushMessageManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class removetemplate extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(removetemplate.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final String itemType = "MESSAGES";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        
        log.info("is started :: ");

        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Template Removed Successfully!!!";

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String operatorId = operatorS.getOperatorid();
        log.debug("operatorId :: "+operatorId);

        JSONObject json = new JSONObject();
        int retValue = -1;
        String _tid = request.getParameter("_tid");
        int _templateid = Integer.valueOf(_tid);
        log.debug("_templateid :: "+_templateid);

        if (_tid == null) {
            result = "error";
            message = "Invalid Template Details!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        TemplateManagement tObj = new TemplateManagement();
        AuditManagement audit = new AuditManagement();

        //TemplateManagement tObj=new TemplateManagement();
        Pushmessagemappers mapper = new Pushmessagemappers();
        PushMessageManagement pObj = new PushMessageManagement();
        mapper = pObj.getPushmapperbyTemplateid(sessionId, channel.getChannelid(), _templateid);

        if (mapper != null) {
            result = "error";
            message = "Remove Failed. Reason: This template is being used in FX messages!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        Templates tempObj = tObj.LoadTemplate(sessionId, channel.getChannelid(), _templateid);
        byte b = tempObj.getErasable();
        boolean b239823 = b != 0;
        //boolean b239823 = tempObj.isErasable();
        if (b239823 == false) {
            result = "error";
            message = "Error: This is system template, cannot be removed!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        retValue = tObj.deletetemplate(sessionId, channel.getChannelid(), _templateid);
        log.debug("deletetemplate :: "+retValue);

        byte[] bTemplate = tempObj.getTemplatebody();
        String oldValue = new String(bTemplate, "UTF-8");
        String resultString = "ERROR";
        if (retValue == 0) {
            resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(),
                    "Delete Template", resultString, retValue,
                    "Template Management", "Template Name = " + tempObj.getTemplatename() + "Template Subject = " + tempObj.getSubject()
                    + "Tag With = " + tempObj.getTaggedWith() + "Template Variables =" + tempObj.getTemplatevariables()
                    + "Created Date = " + tempObj.getCreatedOn() + "Product Type = " + tempObj.getProducttype()
                    + "Type = " + tempObj.getType() + "Is Html = "
                    + (tempObj.getEmailHtml())
                    + "Template Body =" + oldValue,
                    "Template Removed Successfully",
                    itemType,
                    "" + _templateid);

        } else {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(),
                    "Delete Template", resultString, retValue,
                    "Template Management", "Template Name = " + tempObj.getTemplatename() + "Template Subject = " + tempObj.getSubject()
                    + "Tag With = " + tempObj.getTaggedWith() + "Template Variables =" + tempObj.getTemplatevariables()
                    + "Created Date = " + tempObj.getCreatedOn() + "Product Type = " + tempObj.getProducttype()
                    + "Type = " + tempObj.getType() + "Is Html = "
                    + (tempObj.getEmailHtml())
                    + "Template Body =" + oldValue,
                    "Failed To remove Template...!!!", itemType,
                    "" + _templateid);
        }

        if (retValue == 0) {
            result = "success";
        } else {
            result = "error";
            message = "Template Removal Failed!!!";
        }

        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();

        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
