/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.PasswordPolicySetting;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.json.JSONArray;


public class loadPasswordPolicySettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadPasswordPolicySettings.class.getName());

 private JSONObject SettingsWhenEmpty(int _type1) {
            log.info("Servlet started");
            JSONObject json = new JSONObject();
            
            try { json.put("_passwordLength",8);
            json.put("_passwordExpiryTime",30);
            json.put("_passwordIssueLimit", 3);
            json.put("_enforceDormancyTime", 1);
            json.put("_passwordInvalidAttempts", 5);
            json.put("_oldPasswordReuseTime", 3);
            json.put("_passwordType", 1);
            json.put("_changePasswordAfterLogin", true);
            json.put("_epinResetReissue", true);
            json.put("_crResetReissue", true);
            json.put("_allowRepetationCharacter", true);
            json.put("_genearationPassword", 6);
            json.put("_alertUserPassword", true);
            json.put("_blockCommonWords", true);
            json.put("_gatewayType", 1);
            json.put("_alertAttempt", 1);
            json.put("_templateName",1);
            json.put("_allowAlert",true);
            json.put("_allowAlertFor",1);
//            json.put("_userValidityDays", 1825);
            }catch(Exception e){log.error("Exception caught :: ",e);}
        
        return json;
    }

    private JSONObject SettingsWhenPresent(Object settingsObj) {
        JSONObject json = new JSONObject();
        if (settingsObj instanceof PasswordPolicySetting) {
            PasswordPolicySetting passwordObj = (PasswordPolicySetting) settingsObj;
            try { json.put("_passwordLength",passwordObj.passwordLength);
            json.put("_passwordExpiryTime",passwordObj.passwordExpiryTime);
            json.put("_passwordIssueLimit", passwordObj.issuingLimitDuration);
            json.put("_enforceDormancyTime", passwordObj.enforcementDormancytime);
            json.put("_passwordInvalidAttempts", passwordObj.invalidAttempts);
            json.put("_oldPasswordReuseTime", passwordObj.oldPasswordCannotbeReused);
            json.put("_passwordType",passwordObj.passwordTye);
            json.put("_changePasswordAfterLogin", passwordObj.changePasswordAfterFirstlogin);
            json.put("_epinResetReissue", passwordObj.allowEPINforResetandReissue);
            json.put("_crResetReissue", passwordObj.allowCRQAforResetandReissue);
            
            json.put("_allowRepetationCharacter", passwordObj.allowRepetationOfChars);
            json.put("_genearationPassword",passwordObj.passwordGenerations);
//            json.put("_alertUserPassword", passwordObj.passwordAlert);''

            ArrayList<String> lisp= new ArrayList<String>();
           for(int i=0;i<passwordObj.arrCommonPassword.length;i++)
           {
            lisp.add(passwordObj.arrCommonPassword[i]);
           }
              json.put("_arrCommonPassword",new JSONArray(lisp));
            json.put("_blockCommonWords", passwordObj.allowCommonPassword);
            json.put("_gatewayType", passwordObj.passwordAlertVia);
            json.put("_alertAttempt",passwordObj.passwordAlertAttempts);
            json.put("_templateName",passwordObj.passwordAlertTemplateID);
            json.put("_allowAlert",passwordObj.passwordAlert);
             json.put("_allowAlertFor",passwordObj.passwordAlertFor);
//              json.put("_userValidityDays", passwordObj.userValidityDays);
            }catch(Exception e){log.error("Exception caught :: ",e);}
        }
        return json;
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       response.setContentType("application/json");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("channel is::"+channel.getName());
        log.debug("sessionid::"+sessionId);
        JSONObject json = null;
        PrintWriter out = response.getWriter();
  try {
            SettingsManagement sMngmt = new SettingsManagement();
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), sMngmt.PASSWORD_POLICY_SETTING, sMngmt.PREFERENCE_ONE);
            if (settingsObj != null) {
                json = SettingsWhenPresent(settingsObj);
            } else {
                json = SettingsWhenEmpty(sMngmt.PASSWORD_POLICY_SETTING);
            }

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        }
        try {
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
