/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.operator;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import org.json.JSONException;
import org.json.JSONObject;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class editoperator extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editoperator.class.getName());

    final String itemTypeOp = "OPERATOR";
    
  

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        
        log.info("is started :: ");

        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Operator updated successfully!!!";

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);

        JSONObject json = new JSONObject();
        int retValue = 0;
        int checkValue = 0;

        String _op_name = request.getParameter("_oprnameE");
        log.debug("_op_name :: "+_op_name);
        String _op_phone = request.getParameter("_oprphoneE");
        log.debug("_op_phone :: "+_op_phone);
        String _op_email = request.getParameter("_opremailE");
        log.debug("_op_email :: "+_op_email);
        String _op_roleid_str = request.getParameter("_oprroleidE");
        String _op_status_str = request.getParameter("_oprstatusE");
        String _unitsEDIT = request.getParameter("_unitsEDIT");
        log.debug("_unitsEDIT :: "+_unitsEDIT);
        String _operatorTypeE = request.getParameter("_operatorTypeE");
        log.debug("_operatorTypeE :: "+_operatorTypeE);
      
        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());
      
        int _op_roleid = Integer.parseInt(_op_roleid_str);
        log.debug("_op_roleid :: "+_op_roleid);
        int _op_status = Integer.parseInt(_op_status_str);
        log.debug("_op_status :: "+_op_status);

        if (_op_name == null || _op_phone == null || _op_email == null || _operatorTypeE == null
                || _op_name.length() == 0
                || _op_phone.length() == 0
                || _op_email.length() == 0
                || _operatorTypeE.length() == 0
                || _op_name.isEmpty() == true
                || _op_phone.isEmpty() == true
                || _op_email.isEmpty() == true
                || _operatorTypeE.isEmpty() == true) {
            result = "error";
            message = "Invalid Parameters!!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        int iUnitEdit = Integer.parseInt(_unitsEDIT);
        int ioperatorTypeE = Integer.parseInt(_operatorTypeE);
        if (UtilityFunctions.isValidEmail(_op_email) == false) {
            result = "error";
            message = "Invalid email id!!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        if (UtilityFunctions.isValidPhoneNumber(_op_phone) == false) {
            result = "error";
            message = "Invalid phone number!!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(_op_name);
        boolean b = m.find();

        if (b) {
            result = "error";
            message = "Name should not contain any symbols !!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
               log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        }
        
        String _operId = request.getParameter("_opridE");
        OperatorsManagement oManagement = new OperatorsManagement();
        AuditManagement audit = new AuditManagement();
        Operators operator = oManagement.getOperatorById(channel.getChannelid(), _operId);

//        retValue = oManagement.checkIsUniqueInChannel(channel.getChannelid(), _op_name, _op_email, _op_phone);
//        if ( retValue != 0) {
//            result = "error";
//            message = "Duplication, please enter unqiue values!!!";
//            json.put("_result", result);
//            json.put("_message", message);
//            out.print(json);
//            out.flush();
//            return;
//
//        }
        Date d = new Date();
//        Operators newoperator = new Operators(operator.getOperatorid(),
//                channel.getChannelid(),
//                _op_name,
//                _op_phone,
//                _op_email,
//                operator.getPasssword(),
//                _op_roleid,
//                _op_status,
//                operator.getCurrentAttempts(),
//                operator.getCreatedOn(),
//                d,new Date());
        
        Operators newoperator = new Operators(
                operator.getOperatorid(),
                channel.getChannelid(),
                _op_name,
                _op_phone,
                _op_email,
                operator.getPasssword(),
                iUnitEdit,
                ioperatorTypeE,
                _op_roleid,
                _op_status,
                operator.getCurrentAttempts(),                
                operator.getCreatedOn(),
                d,
                new Date(),
                operator.getChangePassword()
                );

//         checkValue = oManagement.checkIsUniqueInChannel( channel.getChannelid(),_op_name, _op_email, _op_phone);
//         if(checkValue == 0){
//        String roleNames = null;
//          if(_op_roleid == ADMIN){
//           roleNames = "ADMIN";
//       }else if(_op_roleid == HELPDESK){
//           roleNames = "HELPDESK";
//       }else if(_op_roleid == REPORTER){
//           roleNames = "REPORTER";
//       }else if(_op_roleid == SYS_ADMIN){
//           roleNames = "SYS_ADMIN";
//       }
//        
//         Roles roleObj = oManagement.getRoleByRoleId(channel.getChannelid(), operatorS.getRoleid());
//        
//        if(roleObj.getName().equals(OperatorsManagement.admin)  && roleNames.equals("SYS_ADMIN")){
//            result = "error";
//            message = "Admin not have permission to change role to Sysadmin!!";
//            try {
//                json.put("_result", result);
//                 json.put("_message", message);
//            } catch (Exception ex) {
//                Logger.getLogger(resendpassword.class.getName()).log(Level.SEVERE, null, ex);
//            } finally {
//                out.print(json);
//                out.flush();
//                return;
//            }
//        }
        
        
        retValue = oManagement.EditOperator(sessionId, channel.getChannelid(),
                operator.getOperatorid(),
                operator, newoperator);

        String resultString = "ERROR";
        if (retValue == -4) {
            result = "error";
            message = "Details are already taken, provide unique credentials!!";

        } else {

            if (retValue == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Edit Operator", resultString, retValue, "Operator Management",
                        operator.toString(),
                        newoperator.toString(),
                        itemTypeOp, _operId);

            }else  {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Edit Operator", resultString, retValue, "Operator Management",
                        "",
                        "Failed To Edit Operator." + newoperator.toString(),                        
                        itemTypeOp, 
                        _operId);
                
                result = "error";
                message = "Operator update failed!!!";
            }
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        }catch(Exception e){
            log.error("exception caught :: ",e);
        }
        finally {
            out.print(json);
            out.flush();
        }
         log.info("is ended :: ");

//         }else {
//             result = "error";
//            message = "Details are also taken, provide unique credentials!!";
//            try {
//                json.put("_result", result);
//                json.put("_message", message);
//            } catch (Exception ex) {
//                Logger.getLogger(resendpassword.class.getName()).log(Level.SEVERE, null, ex);
//            } finally {
//                out.print(json);
//                out.flush();
//            }
//         }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
