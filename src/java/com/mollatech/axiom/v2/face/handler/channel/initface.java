package com.mollatech.axiom.v2.face.handler.channel;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Roles;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.RemoteAccessManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class initface extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(initface.class.getName());

    static final int ACTIVE = 1;
    final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;
    final int FIRST_UNIT_AS_0 = 0 ;
    final int FIRST_OPERATOR_AS_0 = 0 ;
//    final String itemType = "CHANNEL";
//    final String itemTypeOp = "OPERATOR";
//    final String itemTypeRE = "REMOTEACCESS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel::"+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
         log.debug("sessionId::"+sessionId);

        //audit parameter
        //String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String operatorId = operatorS.getOperatorid();
        log.debug("operatorId :: "+operatorId);

        String _ch_name = "face";
        String _ch_virtual_path = "face";
        String _ch_status = "1";
        int status = Integer.parseInt(_ch_status);
        String _op_name = request.getParameter("_op_name");
        log.debug("_op_name::"+_op_name);
        String _op_email = request.getParameter("_op_email");
        log.debug("_op_email::"+_op_email);
        String _op_phone = request.getParameter("_op_phone");
        log.debug("_op_phone::"+_op_phone);

        String result = "success";
        String message = "Channel Added successfully....";

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        if (_ch_status == null || _ch_name == null || _ch_virtual_path == null) {
            result = "error";
            message = "Invalid Channel Details!!";
            try { 
            json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
                
                log.error("exception caught ::",e);
            
        }
            out.print(json);
            out.flush();
            return;
        }

        if (_op_name == null || _op_email == null || _op_phone == null) {
            result = "error";
            message = "Invalid Administrator Details!!";
            try { 
            json.put("_result", result);
            json.put("_message", message);
            } catch(Exception e){
                
                log.error("exception caught ::",e);
            
        }
            out.print(json);
            out.flush();
            return;
        }

        int retValue = -1;

        try {
            ChannelManagement cManagement = new ChannelManagement();
            AuditManagement aAudit = new AuditManagement();
            String resultStr = "Fail";
            String strStatus = "SUSPEND_STATUS";
            
            retValue = cManagement.AddChannel(sessionId, _ch_name, _ch_virtual_path, status);
            log.debug("AddChannel::"+retValue);
            
            SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
            Session sChannel = suChannel.openSession();
            ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
            Channels newchannelObj = cUtil.getChannel(_ch_name);
            suChannel.close();
            sChannel.close();

            OperatorsManagement oManagement = new OperatorsManagement();
            retValue = oManagement.CreateResourses(newchannelObj.getChannelid());
            log.debug("CreateResourses::"+retValue);
            retValue = oManagement.CreateRolesAll(newchannelObj.getChannelid());
            log.debug("CreateRolesAll::"+retValue);
            retValue = oManagement.createAccessAll(newchannelObj.getChannelid());
            log.debug("createAccessAll::"+retValue);

            Roles[] roles = oManagement.getAllRoles(newchannelObj.getChannelid());
            int roleid = 0;

            if (roles != null) {
                for (int i = 0; i < roles.length; i++) {
                    if (roles[i].getName().equalsIgnoreCase("sysadmin")) {
                        roleid = roles[i].getRoleid();
                    }
                }
            }
            
            UnitsManagemet u = new UnitsManagemet();
            retValue = u.CreatetUnitss(newchannelObj.getChannelid());
            log.debug("CreatetUnitss::"+retValue);  
                //added by nilesh for units.

            Date d = new Date();
            UtilityFunctions utilsFns = new UtilityFunctions();
            String strPassword = utilsFns.HexSHA1(d.toString() + _op_name + _op_email + _op_phone + roleid);
            strPassword = strPassword.substring(0, 7);
            retValue = oManagement.AddOperator(sessionId, newchannelObj.getChannelid(),
                    _op_name, strPassword,
                    _op_phone, _op_email,
                    roleid, ACTIVE,FIRST_UNIT_AS_0,FIRST_OPERATOR_AS_0);
            log.debug("AddOperator::"+retValue);


            strPassword = utilsFns.HexSHA1(_op_name + _op_email + _op_phone + strPassword);
            strPassword = strPassword.substring(0, 9);

            String login = utilsFns.HexSHA1(_op_name + _op_email + _op_phone + strPassword);
            login = login.substring(0, 11);

            RemoteAccessManagement ramObj = new RemoteAccessManagement();
            retValue = ramObj.SetRemoteAccessCredentials(sessionId, newchannelObj.getChannelid(), login, strPassword);
            log.debug("SetRemoteAccessCredentials::"+retValue);

            TemplateManagement tmObj = new TemplateManagement();

            retValue = tmObj.Createtemplates(newchannelObj.getChannelid());
            log.debug("Createtemplates::"+retValue);
        } catch (Exception ex) {
            log.error("exception caught :: ",ex);
            
        }
        if (retValue != 0) {
            result = "error";            
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch(Exception e){
            log.error("exception caught ::",e);
        
        }finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
        public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
