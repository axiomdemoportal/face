/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Connectorstatusaudit;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.ConnectorStatusAuditManagement;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Human
 */
public class getconnectorstatusaudit extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(getconnectorstatusaudit.class.getName());

    private static int PDF_TYPE = 0;
    private static int CSV_TYPE = 1;
    private static final int BUFSIZE = 4096;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        try {
            //response.setContentType("application/json");
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            Operators operartorObj = (Operators) request.getSession().getAttribute("_apOprDetail");
            String _duration = request.getParameter("_duration");
            String type = request.getParameter("type");
            String preference = request.getParameter("preference");
            String _format = request.getParameter("_format");
            
            log.debug("channel is :: " + channel.getName());
            log.debug("_duration :: " + _duration);
            log.debug("operartorObj :: " + operartorObj.getOperatorid());
            log.debug("sessionId :: " + sessionId);
            log.debug("type :: " + type);
            log.debug("preference :: " + preference);
            log.debug("_format :: " + _format);
            
            

            int iFormat = CSV_TYPE;
            if (_format.compareTo("pdf") == 0) {
                iFormat = PDF_TYPE;
            }

            String filepath = null;

            int iType = -1;
            if (type != null) {
                iType = Integer.parseInt(type);
            }
            int iPreference = -1;
            if (preference != null) {
                iPreference = Integer.parseInt(preference);
            }
            try {
                try {
                    ConnectorStatusAuditManagement cManagement = new ConnectorStatusAuditManagement();
                    Connectorstatusaudit[] cAudit = cManagement.getConnectorstatusauditrail(channel.getChannelid(), iType, iPreference, _duration);
                    filepath = cManagement.generateReport(iFormat, channel.getChannelid(),cAudit);
                    cAudit = null;
                } catch (Exception e) {
                    log.error("Exception caught :: ",e);
                }

                File file = new File(filepath);
                int length = 0;
                ServletOutputStream outStream = response.getOutputStream();
                ServletContext context = getServletConfig().getServletContext();
                String mimetype = context.getMimeType(filepath);

                // sets response content type
                if (mimetype == null) {
                    mimetype = "application/octet-stream";
                }
                response.setContentType(mimetype);
                response.setContentLength((int) file.length());
                String fileName = (new File(filepath)).getName();

                // sets HTTP header
                response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

                byte[] byteBuffer = new byte[BUFSIZE];
                DataInputStream in = new DataInputStream(new FileInputStream(file));

                // reads the file's bytes and writes them to the response stream
                while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                    outStream.write(byteBuffer, 0, length);
                }

                in.close();
                outStream.close();
                file.delete();

            } catch (Exception ex) {
                // TODO handle custom exceptions here
                log.error("Exception caught :: ",ex);
            }

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        }
        log.info("Servlet ended");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
