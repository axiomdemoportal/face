/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.operator;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Pramod
 */
public class removeoperator extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(removeoperator.class.getName());
    
final String itemTypeOp = "OPERATOR";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       response.setContentType("application/json");
       
       log.info("is started :: ");

        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Operator removed successfully!!!";
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);

        JSONObject json = new JSONObject();
        int retValue = 0;
        int checkValue = 0;
        String _operatorid = request.getParameter("operatorid");
        log.debug("_operatorid :: "+_operatorid);
     
      
        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());
      
       
      int opEid=0;
        if (_operatorid==null) {
            result = "error";
            message = "Invalid Parameters!!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
                log.error("exception caught :: ",e);
                    }
            out.print(json);
            out.flush();
            return;
        }
        OperatorsManagement oManagement = new OperatorsManagement();
        AuditManagement audit = new AuditManagement();
        int results = oManagement.deleteoperatorLogical(sessionId,channel.getChannelid(),_operatorid);

//        retValue = oManagement.checkIsUniqueInChannel(channel.getChannelid(), _op_name, _op_email, _op_phone);
//        if ( retValue != 0) {
//            result = "error";
//            message = "Duplication, please enter unqiue values!!!";
//            json.put("_result", result);
//            json.put("_message", message);
//            out.print(json);
//            out.flush();
//            return;
//
//        }
        Date d = new Date();
//        Operators newoperator = new Operators(operator.getOperatorid(),
//                channel.getChannelid(),
//                _op_name,
//                _op_phone,
//                _op_email,
//                operator.getPasssword(),
//                _op_roleid,
//                _op_status,
//                operator.getCurrentAttempts(),
//                operator.getCreatedOn(),
//                d,new Date());
        
       

//         checkValue = oManagement.checkIsUniqueInChannel( channel.getChannelid(),_op_name, _op_email, _op_phone);
//         if(checkValue == 0){
//        String roleNames = null;
//          if(_op_roleid == ADMIN){
//           roleNames = "ADMIN";
//       }else if(_op_roleid == HELPDESK){
//           roleNames = "HELPDESK";
//       }else if(_op_roleid == REPORTER){
//           roleNames = "REPORTER";
//       }else if(_op_roleid == SYS_ADMIN){
//           roleNames = "SYS_ADMIN";
//       }
//        
//         Roles roleObj = oManagement.getRoleByRoleId(channel.getChannelid(), operatorS.getRoleid());
//        
//        if(roleObj.getName().equals(OperatorsManagement.admin)  && roleNames.equals("SYS_ADMIN")){
//            result = "error";
//            message = "Admin not have permission to change role to Sysadmin!!";
//            try {
//                json.put("_result", result);
//                 json.put("_message", message);
//            } catch (Exception ex) {
//                Logger.getLogger(resendpassword.class.getName()).log(Level.SEVERE, null, ex);
//            } finally {
//                out.print(json);
//                out.flush();
//                return;
//            }
//        }
        
        
        String resultString = "ERROR";
        if (results!=0) {
            result = "error";
            message = "Operator Deletion Failed!!";

        } else {

            if (results == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Remove Operator", resultString, retValue, "Operator Management",
                        "",
                       "",
                        itemTypeOp, _operatorid);

            }else  {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Remove Operator", resultString, retValue, "Operator Management",
                        "",
                        "Failed To Remove Operator." + _operatorid,                        
                        itemTypeOp, 
                        _operatorid);
                
                result = "error";
                message = "Operator update failed!!!";
            }
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        }catch(Exception e){
            log.error("exception caught :: ",e);
        }
        finally {
            out.print(json);
            out.flush();
        }
        
        log.info("is ended :: ");
        
        
        }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
