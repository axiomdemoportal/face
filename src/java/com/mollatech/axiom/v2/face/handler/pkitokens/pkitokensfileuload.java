package com.mollatech.axiom.v2.face.handler.pkitokens;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement;
import com.mollatech.axiom.nucleus.db.operation.TokenLicenseDetails;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;

/**
 *
 * @author mollatech1
 */
public class pkitokensfileuload extends HttpServlet {
    
     static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(pkitokensfileuload.class.getName());

    final String itemType = "PKITOKEN";
    final String itemTypeOp = "OPERATOR";

    public String getContents(File aFile) {
        //...checks on aFile are elided
        StringBuilder contents = new StringBuilder();

        try {

            BufferedReader input = new BufferedReader(new FileReader(aFile));
            try {
                String line = null; //not declared within while loop

                while ((line = input.readLine()) != null) {
                    contents.append(line);
                    contents.append(System.getProperty("line.separator"));
                }
            } finally {
                input.close();
            }
        } catch (IOException ex) {
            log.error("Exception caught :: ",ex);
        }

        return contents.toString();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        log.info("is started :: ");
        PrintWriter out = response.getWriter();
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());
//        String _password = request.getParameter("_password");
        String strError = "";
        //String saveFile = "";
        String savepath = "";
        int failed = 0;
        int success = 0;
        String result = "success";
         String _UnitId = request.getParameter("_UnitIdPKI");
         log.debug("_UnitId :: "+_UnitId);
        int unitID = 0;
        if(_UnitId != null){
            unitID = Integer.parseInt(_UnitId);
        }
        String message = "File Upload sucessfully," + success + "Entry added and " + failed + " Entry failed to add";
        //  String counter  = "0";

        JSONObject json = new JSONObject();
        
       

        savepath = System.getProperty("catalina.home");
        if(savepath == null) { savepath = System.getenv("catalina.home"); }
        savepath += System.getProperty("file.separator");
        savepath += "axiomv2-settings";
        savepath += System.getProperty("file.separator");
        savepath += "uploads";
        savepath += System.getProperty("file.separator");

        String strUniqueID = new String(request.getSession().getId());
        strUniqueID = strUniqueID.substring(0, 8);

        String optionalFileName = "";
        FileItem fileItem = null;
        String[] files = new String[1];	 // file names
        String dirName = savepath;
        int retValue = 0;

        int i = 0;

        AuditManagement audit = new AuditManagement();

        if (ServletFileUpload.isMultipartContent(request)) {
            try {
                ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                List fileItemsList = servletFileUpload.parseRequest(request);
                Iterator it = fileItemsList.iterator();

                while (it.hasNext()) {
                    FileItem fileItemTemp = (FileItem) it.next();
                    if (fileItemTemp.isFormField()) {
                        if (fileItemTemp.getFieldName().equals("filename")) {
                            optionalFileName = fileItemTemp.getString();
                        } else {
                            //System.out.println("Additional fields in file upload form is  " + fileItemTemp.getFieldName());
                        }
                    } else {
                        fileItem = fileItemTemp;
                    }
                    if (fileItem != null) {
                        String fileName = fileItem.getName();
                        if(fileItem.getSize() == 0){
                            strError = "Please Select File To Upload...!!!";
                            result = "error";
                            try { json.put("result", result);
                            json.put("message", strError);
                            }catch(Exception e){
                                log.error("exception caught :: ",e);
                            }
                            //out.print(json);
                            out.print("{result:'"+result+"',message:'"+strError+"'}");
                            out.flush();
                            return;
                        }
                        if (fileItem.getSize() > 0 && fileItem.getSize() < 1024000 * 10) { // size cannot be more than 65Kb. We want it light.
                            if (optionalFileName.trim().equals("")) {
                                fileName = FilenameUtils.getName(fileName);
                            } else {
                                fileName = optionalFileName;
                            }
                            files[i++] = dirName + fileName;
                            File saveTo = new File(dirName + fileName);

                            //saveFile = fileName;
                            //String secret = null;
                            AXIOMStatus axiom[] = null;
                            try {
                                fileItem.write(saveTo);
                                PKITokenManagement pki = new PKITokenManagement();
                                File testFile = new File(saveTo.getAbsolutePath());
                                String pkitoken = this.getContents(testFile);
                               

                                axiom = pki.ADDPKITOKENS(sessionId, channel.getChannelid(), pkitoken ,unitID);
                                if (axiom == null) {
                                    

                                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                                            request.getRemoteAddr(),
                                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                                            "Upload PKI Token File", result, retValue, "PKI TOKEN Management",
                                            "", "Success Entries =" + success + "Failed Entries=" + failed,
                                            itemType, 
                                            operatorS.getOperatorid());
                                    result = "error";
                                    message = "Fail To Upload!!";
                                    json.put("_result", result);
                                    json.put("_message", message);
                                    json.put("_success", success);
                                    out.print(json);
                                    out.flush();
                                    return;

                                }

                                for (int j = 0; j < axiom.length; j++) {
                                    TokenLicenseDetails otptoken1 = new TokenLicenseDetails();
                                    if (axiom[j].iStatus == 0) {
                                        success++;
                                    } else {
                                        failed++;
                                        result = "ERROR";
                                    }
                                }
//                                message = "File Upload sucessfully," + success + "Entry added and " + failed + " Entry failed to add";
                                message = "Token Import Result is " + success + " entries successfully added and " + failed + " entries are duplicate/invalid";
                                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                                        request.getRemoteAddr(),
                                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                                        "Upload PKI TOKEN  File", result, retValue, "PKI TOKEN Management",
                                        "", "Success Entries =" + success + "Failed Entries=" + failed,
                                        itemTypeOp, operatorS.getOperatorid());

//                              
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                        } else {
                            strError = "Error: " + fileName + " size is more than 10MB. Please upload lighter files.";
                            result = "error";
                            try { json.put("_result", result);
                            json.put("_message", strError);
                            json.put("_failed", failed);
                            json.put("_success", success);
                            }catch(Exception e){
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                    } else {
                        result = "error";
                        message = "Fail To Upload !!";
                        try { json.put("_result", result);
                        json.put("_message", message);
                        json.put("_failed", failed);
                        json.put("_success", success);
                        }catch(Exception e){
                            log.error("exception caught :: ",e);
                        }
                        out.print(json);
                        out.flush();
                        return;
                    }
                }
            } catch (FileUploadException ex) {
                //Logger.getLogger(uploadHardwareOTPTokens.class.getName()).log(Level.SEVERE, null, ex);
                log.error("exception caught :: ",ex);
            }
        } else {
            result = "error";
            message = "Fail To Upload!!";
            try { json.put("_result", result);
            json.put("_message", message);
            json.put("_failed", failed);
            json.put("_success", success);
            }catch(Exception e){
                log.error("exception caught :: ",e);
            
            }
            out.print(json);
            out.flush();
            return;
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_failed", failed);
            json.put("_success", success);

        } catch(Exception e){
            log.error("exception caught :: ",e);
        }
        finally {
            out.print(json);
            out.flush();
            log.info("is ended :: ");
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
