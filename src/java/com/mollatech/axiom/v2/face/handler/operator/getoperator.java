package com.mollatech.axiom.v2.face.handler.operator;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.OperatorsUtil;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.json.JSONObject;

public class getoperator extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(addoperator.class.getName());

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        
        log.info("is started :: ");
        
        String _oprid = request.getParameter("_oprid");
        log.debug("_oprid :: "+_oprid);

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        try {

            SessionFactoryUtil suOpr = new SessionFactoryUtil(SessionFactoryUtil.operators);
            Session sOpr = suOpr.openSession();
            OperatorsUtil oprUtil = new OperatorsUtil(suOpr, sOpr);
            Operators oprObj = oprUtil.GetOperatorsById(channel.getChannelid(), _oprid);
            sOpr.close();
            suOpr.close();
                       
            json.put("_id", oprObj.getOperatorid());
            json.put("_name",  oprObj.getName());
            json.put("_email", oprObj.getEmailid());
            json.put("_phone", oprObj.getPhone());
            json.put("_roleid", oprObj.getRoleid());
            json.put("_status", oprObj.getStatus());
            json.put("_unitsEDIT", oprObj.getUnits());
            json.put("_operatorTypeE", oprObj.getOperatorType());
            json.put("_result", "success");
            
        } catch (Exception ex) {
            log.error("exception caught :: ",ex);
            // TODO handle custom exceptions here
            try { json.put("_result", "error");
            json.put("_message", ex.getMessage());}
            catch(Exception e){
                log.error("exception caught :: ",e);
            }
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
