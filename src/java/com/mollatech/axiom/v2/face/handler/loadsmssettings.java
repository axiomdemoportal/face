/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.OOBEmailChannelSettings;
import com.mollatech.axiom.nucleus.settings.OOBMobileChannelSettings;
import com.mollatech.axiom.nucleus.settings.RadiusServerSettings;
import com.mollatech.axiom.nucleus.settings.RootCertificateSettings;
import com.mollatech.axiom.nucleus.settings.TokenSettings;
import com.mollatech.axiom.nucleus.settings.UserSourceSettings;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class loadsmssettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadsmssettings.class.getName());

    private JSONObject SettingsWhenEmpty(int _type1, int _preference1, Object settingsObj) {
        JSONObject json = new JSONObject();
        if (_type1 == editsettings.SMS && _preference1 == editsettings.PREFERENCE_ONE) {
            try { json.put("_className", "");
            json.put("_ip", "");
            json.put("_port", "");
            json.put("_userId", "");
            json.put("_password", "");
            json.put("_phoneNumber", "");
            json.put("_reserve1", "");
            json.put("_messgeLength", 0);
            json.put("_reserve2", "");
            json.put("_reserve3", "");
            json.put("_status", "Disabled");
            json.put("_logConfirmation", true);
            json.put("_autofailover", 0);
            json.put("_retries", 2);
            json.put("_retryduration", 10);
            }catch(Exception e){}
        } else if (_type1 == editsettings.SMS && _preference1 == editsettings.PREFERENCE_TWO) {
            try { json.put("_classNameS", "");
            json.put("_ipS", "");
            json.put("_portS", "");
            json.put("_userIdS", "");
            json.put("_messgeLengthS", 0);
            json.put("_passwordS", "");
            json.put("_phoneNumberS", "");
            json.put("_reserve1S", "");
            json.put("_reserve2S", "");
            json.put("_reserve3S", "");
            json.put("_statusS", "Disabled");
            json.put("_logConfirmationS", true);
            json.put("_retriesS", 2);
            json.put("_retrydurationS", 10);
            }catch(Exception e){}
        }
        return json;
    }

    private JSONObject SettingsWhenPresent(Object settingsObj) {
        JSONObject json = new JSONObject();
        if (settingsObj instanceof OOBMobileChannelSettings) {
            OOBMobileChannelSettings mobile = (OOBMobileChannelSettings) settingsObj;
            if (mobile.getPreference() == 1) {
                try { 
                json.put("_className", mobile.getClassName());
                json.put("_ip", mobile.getIp());
                json.put("_port", mobile.getPort());
                json.put("_userId", mobile.getUserid());
                json.put("_password", mobile.getPassword());
                json.put("_messgeLength", mobile.getMessageLength());
                json.put("_phoneNumber", mobile.getPhoneNumber());
                json.put("_reserve1", mobile.getReserve1());
                json.put("_reserve2", mobile.getReserve2());
                json.put("_reserve3", mobile.getReserve3());
                json.put("_status", mobile.getStatus());
                json.put("_logConfirmation", mobile.isLogConfirmation());
                json.put("_retries", mobile.getRetrycount());
                json.put("_retryduration", mobile.getRetryduration());
                json.put("_autofailover", mobile.getAutofailover());
                }catch(Exception e){}
            } else {
                try { 
                json.put("_classNameS", mobile.getClassName());
                json.put("_ipS", mobile.getIp());
                json.put("_portS", mobile.getPort());
                json.put("_userIdS", mobile.getUserid());
                json.put("_passwordS", mobile.getPassword());
                json.put("_messgeLengthS", mobile.getMessageLength());
                json.put("_phoneNumberS", mobile.getPhoneNumber());
                json.put("_reserve1S", mobile.getReserve1());
                json.put("_reserve2S", mobile.getReserve2());
                json.put("_reserve3S", mobile.getReserve3());
                json.put("_statusS", mobile.getStatus());
                json.put("_logConfirmationS", mobile.isLogConfirmation());
                json.put("_retriesS", mobile.getRetrycount());
                json.put("_retrydurationS", mobile.getRetryduration());
                }catch(Exception e){}
            }
        }
        return json;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        String _type = request.getParameter("_type");
        String _preference = request.getParameter("_preference");
        int _type1 = Integer.parseInt(_type);
        int _preference1 = Integer.parseInt(_preference);
        
        log.debug("loadsmssettings::channel is::"+channel.getName());
        log.debug("loadsmssettings::sessionid::"+sessionId);
        log.debug("loadsmssettings::type is::"+_type1);
        log.debug("loadsmssettings::preference is::"+_preference1);

        JSONObject json = null;//new JSONObject();
        PrintWriter out = response.getWriter();

        try {
            SettingsManagement sMngmt = new SettingsManagement();
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _type1, _preference1);
            if (settingsObj != null) {
                json = SettingsWhenPresent(settingsObj);
            } else {
                json = SettingsWhenEmpty(_type1, _preference1, settingsObj);
            }

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
            // TODO handle custom exceptions here
        }
        try {
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
