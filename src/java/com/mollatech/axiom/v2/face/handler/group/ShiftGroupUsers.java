/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.group;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Usergroups;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.GroupManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author ashish
 */
public class ShiftGroupUsers extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ShiftGroupUsers.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final String itemType = "GROUPS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        
        log.info("is started :: ");

        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Group's user(s) moved successfully!!!";

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);

        JSONObject json = new JSONObject();
        int retValue = 0;
        String _unitId = request.getParameter("_unitIdS");
        String _oldunitNameE = request.getParameter("_oldGroupNameS");
        log.debug("_oldunitNameE :: "+_oldunitNameE);
        String _unitS = request.getParameter("_unitS");
        int ioldunitId = Integer.parseInt(_unitId);
        log.debug("ioldunitId :: "+ioldunitId);
        int inewunitId = Integer.parseInt(_unitS);
        log.debug("inewunitId :: "+inewunitId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());
        GroupManagement management = new GroupManagement();
        Usergroups usergroups = management.getGroupByGroupId(sessionId, channel.getChannelid(), inewunitId);
        String _unit_name = usergroups.getGroupname();
        UserManagement um = new UserManagement();
        AuthUser[] user = um.getAllUserByGroup(sessionId, channel.getChannelid(), ioldunitId);
        int res = -1;
        int iResultSuccess = 0;
        int iResultFailure = 0;

        if (user != null) {
            for (int i = 0; i < user.length; i++) {
                res = um.EditUser(sessionId, channel.getChannelid(), user[i].getUserId(), user[i].getUserName(), user[i].getPhoneNo(), user[i].getEmail(), inewunitId,
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-",
                        "-");
                log.debug("EditUser :: "+res);

                if (res >= 0) {
                    iResultSuccess++;                    
                } else {
                    iResultFailure++;                    
                }

            }

        } else {
            result = "error";
            message = "No Users present!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        String resultString = "ERROR";
        System.out.println("user.length="  +user.length);
        System.out.println(iResultSuccess);
        System.out.println(iResultFailure);

        if (iResultFailure > 0) {
            retValue = -1 * iResultFailure;
            
            if (iResultSuccess > 0) {
                message = "Successfully moved " + iResultSuccess + " user(s) and Failed to move " + iResultFailure + " from " + _oldunitNameE + " Group to new Group " + _unit_name + "!!!";
            } else {                
                message = "Failed to moved " + user.length + " user(s) from " + _oldunitNameE + " to new Group " + _unit_name + "!!!";
            }
        } else {
            retValue = 0;
            message = "Successfully moved " + iResultSuccess + " user(s) from " + _oldunitNameE + " Group to new Group " + _unit_name + "!!!";
        }

        if (retValue == 0) {
            AuditManagement audit = new AuditManagement();
            resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                    "Shift Group's Users", resultString, retValue, "Group Management",
                    "",
                    message,
                    itemType,
                    channel.getChannelid());

        } else {
            AuditManagement audit = new AuditManagement();
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                    "Shift Group's Users", resultString, retValue, "Group Management",
                    "",
                    message,
                    itemType, channel.getChannelid());
            result = "error";
        }

        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            log.error("exception caught :: ",e);
            
        } finally {
            out.print(json);
            out.flush();
        }
        
        log.info("is ended :: ");
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
