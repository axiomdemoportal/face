package com.mollatech.axiom.v2.face.handler.pkitokens;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.AuthorizationManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class assignnewtoken extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(assignnewtoken.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final int SOFTWAREMOBILE_PKITOKEN = 1;
    final int HARDWARE_PKITOKEN = 2;
    final String itemType = "PKITOKEN";
    final String itemTypeAUTH = "AUTHORIZATION";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        log.info("is started :: ");

        String _value = "Assigned";
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
String _newvalue = request.getParameter("newvalue");
        log.debug("new value = "+_newvalue);
        String _oldvalue = request.getParameter("oldvalue");
        log.debug("oldvalue = "+_oldvalue);
        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());

        String _serialnumber = request.getParameter("_ser");
        log.debug("_serialnumber :: "+_serialnumber);
        String _userid = request.getParameter("_userid");
        log.debug("_userid :: "+_userid);

        String result = "success";
        String message = "Token Assign successfully....";

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        String _category = request.getParameter("_category");
        log.debug("_category :: "+_category);
        int category = 0;
        if (_category != null) {
            category = Integer.parseInt(_category);
        }

        if (category == PKITokenManagement.SOFTWARE_TOKEN) {
           
            if (AxiomProtect.CheckEnforcementFor(AxiomProtect.PKI_SOFTWARE) != 0) {
                result = "error";
                message = "PKI Software is not available in this license!!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    log.error("exception caught :: ",e);
                }
                out.print(json);
                out.flush();
                return;
            }
        } else if (category == PKITokenManagement.HARDWARE_TOKEN) {
             
            if (AxiomProtect.CheckEnforcementFor(AxiomProtect.PKI_HARDWARE) != 0) {
                result = "error";
                message = "PKI hardware is not available in this license!!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    log.error("exception caught :: ",e);
                }
                out.print(json);
                out.flush();
                return;
            }
        }

        if (_userid == null) {
            result = "error";
            message = "Could Not Assign Token!!";

            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        String resultString = null;
        int retValue = -1;
        PKITokenManagement pManagement = new PKITokenManagement();
        AuditManagement audit = new AuditManagement();

        String ipaddress = request.getRemoteAddr();
        
         //start of authorization - Nilesh
//        if (operatorS.getRoleid() != 6|| operatorS.getRoleid() != 5) {
//           if (operatorS.getRoleid() >= 3) {
//                result = "error";
//                message = "Sorry, But you don't have permissions for this action!!!";
//                try {
//                    json.put("_result", result);
//                    json.put("_message", message);
//                } catch (Exception e) {
//                    log.error("Exception caught :: ",e);
//                }
//                out.print(json);
//                out.flush();
//                return;
//            }
//        }
        String _approvalId = request.getParameter("_approvalId");
        log.debug("_approvalId :: "+_approvalId);
         SettingsManagement sMngmt = new SettingsManagement();
        Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
        ChannelProfile chSettingObj = null;
        if (settingsObj != null) {
            chSettingObj = (ChannelProfile) settingsObj;
        }
        String _unitId = request.getParameter("_unitId");
        log.debug("_unitId :: "+_unitId);
        if (_unitId != null) {
            if (chSettingObj != null) {

                if (chSettingObj.authorizationunit == 1) {

                    int iUnitId = Integer.parseInt(_unitId);
                    if (iUnitId != operatorS.getUnits()) {
                        result = "error";
                        message = "Sorry, Must be marked by same branch operator!!!";
                        try {
                            json.put("_result", result);
                            json.put("_message", message);
                        } catch (Exception e) {
                            log.error("exception caught :: ",e);
                        }
                        out.print(json);
                        out.flush();
                        return;
                    }
                }
            }
        }
         OperatorsManagement oprMngt = new OperatorsManagement();
        UserManagement uMngt = new UserManagement();
        AuthUser uObj = uMngt.getUser(sessionId, channel.getChannelid(), _userid);
        AuthorizationManagement auth = new AuthorizationManagement();
        String strOpName = "-";
        String struserName = "-";
        String strAction = "-";
        int iapprovalID = -1;
//        ApprovalSetting approvalSetting = null;
        if (_approvalId != null) {

          if (operatorS.getRoleid() != 6) {
                if (operatorS.getRoleid() >= 3) {
                    result = "error";
                    message = "Sorry, But you don't have permissions for this action!!!";
                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("exception caught :: ",e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
            }
// iapprovalID = Integer.parseInt(_approvalId);
           String _markerID = request.getParameter("_markerID");
           log.debug("_markerID :: "+_markerID);
            Operators op = oprMngt.getOperatorById(channel.getChannelid(), _markerID);
            if (op != null) {
                strOpName = op.getName();
            }
         
            if (op != null) {
                strOpName = op.getName();
            }
            if (uObj != null) {
                struserName = uObj.getUserName();
            }
            

        }
     
//         if (chSettingObj != null && _approvalId == null && operatorS.getRoleid() == OperatorsManagement.Requester) {
//
//            if (chSettingObj.authorizationStatus == SettingsManagement.ACTIVE_STATUS) {
//                AuthorizationManagement authMngt = new AuthorizationManagement();
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = AuthorizationManagement.ASSIGN_HW_PKI_TOKEN;
//                if(category == 2){
//                approval.itemid = "Assign New HW PKI token to "+uObj.getUserName();
//                }else{
//                    approval.itemid = "Assign New SW PKI token to "+uObj.getUserName();
//                }
//                strAction = approval.itemid;
//                approval.makerid = operatorS.getOperatorid();
//                approval.userid = _userid;
//                approval.tokenCategory = category;
//                approval.tokenSerialNo = _serialnumber;
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.HOUR, chSettingObj.authorizationDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//
////                retValue = authMngt.addAuthorizationSetting(sessionId, channel.getChannelid(), AuthorizationManagement.AUTORIZATION_PENDING_STATUS, dexpiredOn, approval
//                retValue = authMngt.addAuthorizationSetting(sessionId, channel.getChannelid(),operatorS.getOperatorid(), AuthorizationManagement.AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (retValue == 0) {
//                    retValue = AuthorizationManagement.RETURN_AUTORIZATION_RESULT;
//                }
//            } else {
//                    retValue = pManagement.AssignToken(sessionId, channel.getChannelid(),
//                _userid, category, _serialnumber);
//            }
//        } else {
               retValue = pManagement.AssignToken(sessionId, channel.getChannelid(),
                _userid, category, _serialnumber);
               log.debug("AssignToken :: "+retValue);

//        }
        //end of authorization

//        retValue = pManagement.AssignToken(sessionId, channel.getChannelid(),
//                _userid, category, _serialnumber);

        if (retValue == 0) {
            resultString = "Success";
            //(String sessionId, String channelid, String operatorid,String ipAddress, String channelname, String remoteaccesslogin, String operatorname, Date auditedon, String action, String result, int resultcode, String category, String oldvalue, String newvalue, String itemtype, String id)

            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    ipaddress,
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Assign Token", resultString, retValue,
                    "PKI TOKEN Management", "", "Category = " + _category + " Serial No =" + _serialnumber,
                    itemType, _userid);
               if (_approvalId != null) {
                //int res = auth.removeAuthorizationRequest(sessionId, channel.getChannelid(), iapprovalID);
                   int res = auth.removeAuthorizationRequest(sessionId, channel.getChannelid(),operatorS.getOperatorid(), _approvalId,_newvalue, _oldvalue);
                   log.debug("removeAuthorizationRequest :: "+res);
                if (res == 0) {
                    resultString = "Success";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), "Remove Authorization Request",
                            resultString, retValue,
                            "Authorization Management", "Action =" + strAction + ",Action Marked By=" + strOpName
                            + ",Action Marked On=" + struserName, "Removed successfully!!!",
                            itemTypeAUTH, _userid);
                }
                }

        }else if (retValue == AuthorizationManagement.RETURN_AUTORIZATION_RESULT) {
            resultString = "SUCCESS";
//            if (_approvalId != null) {

                
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operatorS.getName(), new Date(), "Add Authorization Request :Assign New Token",
                        resultString, retValue,
                        "Authorization Management","Action =" + strAction + ",Action Marked By=" + strOpName
                            + ",Action Marked On=" + struserName,
                        "Added successfully!!!",
                        itemTypeAUTH, _userid);

            //}

            result = "success";
            message = "Request is pending for approval!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
               log.error("exception caught :: ",ex);
            }
            
            out.print(json);
            out.flush();
            return;
        }else {

            resultString = "ERROR";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    ipaddress,
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Assign Token", resultString, retValue,
                    "PKI TOKEN Management", "", "Failed to assign Token", itemType, _userid);

            result = "error";
            message = "Could Not Assign Token!!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
                json.put("_value", _value);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        
        
        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);
        } catch (Exception e) {
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
