package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.OOBMobileChannelSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class editsmssettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editsmssettings.class.getName());

    //Static variables
    public static final int SMS = 1;
    public static final int ACTIVE_STATUS = 1;
    public static final int SUSPENDED_STATUS = 1;
    public static final int PREFERENCE_ONE = 1;   //primary
    public static final int PREFERENCE_TWO = 2;
    final String itemtype = "SETTINGS";
//    private void PrintRequestParameters(HttpServletRequest req) {
//        Enumeration<String> paramNames = req.getParameterNames();
//        while (paramNames.hasMoreElements()) {
//            String paramName = paramNames.nextElement();
//            System.out.println(paramName + "=" + req.getParameter(paramName));
//        }
//    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        //PrintRequestParameters(request);
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String OperatorID = operatorS.getOperatorid();

        String _channelId = channel.getChannelid();
        SettingsManagement sMngmt = new SettingsManagement();
        int retValue = -1;

        String result = "success";
        String message = "SMS Gateway Settings Update Successful!!!";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        
            log.debug("channelgetName :: " + channel.getName());
            log.debug("channel :: " + channel.getChannelid());
            log.debug("operatorS :: " + operatorS.getOperatorid());
            log.debug("sessionId :: " + sessionId);
            log.debug("remoteaccesslogin :: " + remoteaccesslogin);

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.GATEWAY_SMS) != 0) {
            result = "error";
            message = "This feature is not available in this license!!!";

            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }

            out.print(json);
            out.flush();
            return;
        }

        String _className = null;
        String _ip = null;
        String _logConfirmation1 = null;
        String _password = null;
        String _phoneNo = null;
        String _port = null;
        Object reserve1 = null;
        Object reserve2 = null;
        Object reserve3 = null;
        String _status1 = null;
        String _userId = null;
        String _autofailover = null;
        String _retrycount = null;
        String _retryduration = null;
        String _messgeLength = null;

        String _preference = request.getParameter("_perference");
        int _iPreference = Integer.parseInt(_preference);
        log.debug("_iPreference :: " + _iPreference);

        //request parameter for mobile primary
        String _type1 = request.getParameter("_type");
        int _type = Integer.parseInt(_type1);
        log.debug("_type :: " + _type);
        if (_type == SMS) {
            if (_iPreference == PREFERENCE_ONE) {   //primary
                _className = request.getParameter("_className");
                log.debug("_className :: " + _className);
                _ip = request.getParameter("_ip");
                log.debug("_ip :: " + _ip);
                _logConfirmation1 = request.getParameter("_logConfirmation");
                log.debug("_logConfirmation1 :: " + _logConfirmation1);
                if (_logConfirmation1 == null) {
                    _logConfirmation1 = "false";
                } else {
                    _logConfirmation1 = "true";
                }
                _password = request.getParameter("_password");
                log.debug("_password :: " + _password);
                _phoneNo = request.getParameter("_phoneNumber");
                log.debug("_phoneNo :: " + _phoneNo);
                _port = request.getParameter("_port");
                log.debug("_port :: " + _port);
                reserve1 = request.getParameter("_reserve1");
                log.debug("reserve1 :: " + reserve1);
                reserve2 = request.getParameter("_reserve2");
                log.debug("reserve2 :: " + reserve2);
                reserve3 = request.getParameter("_reserve3");
                log.debug("reserve3 :: " + reserve3);
                _status1 = request.getParameter("_status");
                log.debug("_status1 :: " + _status1);
                _userId = request.getParameter("_userId");
                log.debug("_userId :: " + _userId);
                _autofailover = request.getParameter("_autofailover");
                log.debug("_autofailover :: " + _autofailover);
                _retrycount = request.getParameter("_retries");
                log.debug("_retrycount :: " + _retrycount);
                _retryduration = request.getParameter("_retryduration");
                log.debug("_retryduration :: " + _retryduration);
                _messgeLength = request.getParameter("_messgeLength");
                log.debug("_messgeLength :: " + _messgeLength);
               

            } else if (_iPreference == PREFERENCE_TWO) {   //secondary
                //request parameter for mobile secondary
                _className = request.getParameter("_classNameS");
                log.debug("_className :: " + _className);
                _ip = request.getParameter("_ipS");
                log.debug("_ip :: " + _ip);
                _logConfirmation1 = request.getParameter("_logConfirmationS");
                log.debug("_logConfirmation1 :: " + _logConfirmation1);
                if (_logConfirmation1 == null) {
                    _logConfirmation1 = "false";
                } else {
                    _logConfirmation1 = "true";
                }
                _password = request.getParameter("_passwordS");
                log.debug("_password :: " + _password);
                _phoneNo = request.getParameter("_phoneNumberS");
                log.debug("_phoneNo :: " + _phoneNo);
                _port = request.getParameter("_portS");
                log.debug("_port :: " + _port);
                reserve1 = request.getParameter("_reserve1S");
                log.debug("reserve1 :: " + reserve1);
                reserve2 = request.getParameter("_reserve2S");
                log.debug("reserve2 :: " + reserve2);
                reserve3 = request.getParameter("_reserve3S");
                log.debug("reserve3 :: " + reserve3);
                _status1 = request.getParameter("_statusS");
                log.debug("_status1 :: " + _status1);
                _userId = request.getParameter("_userIdS");
                log.debug("_userId :: " + _userId);
                _retrycount = request.getParameter("_retriesS");
                log.debug("_retrycount :: " + _retrycount);
                _retryduration = request.getParameter("_retrydurationS");
                log.debug("_retryduration :: " + _retryduration);
                _messgeLength = request.getParameter("_messgeLengthS");
                log.debug("_messgeLength :: " + _messgeLength);
            }
              
                if (_className == null &&  _ip == null && _port  == null 
                        && _className.isEmpty() == true  &&  _ip.isEmpty() == true && _port.isEmpty() == true ) {
                        result = "error";
                        message = "Please provide neccessary details!!!";
                    try {
                        json.put("_result", result);
                        json.put("_message", message);

                    } catch (Exception e) { log.error("Exception caught :: ",e);
                    } finally {
                        out.print(json);
                        out.flush();
                    }

                }
                

            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _type, _iPreference);

            OOBMobileChannelSettings mobile = null;
            boolean bAddSetting = false;

            if (settingsObj == null) {
                mobile = new OOBMobileChannelSettings();
                mobile.setChannelId(_channelId);
                mobile.setType(SMS);
                mobile.setPreference(_iPreference);
                bAddSetting = true;
            } else {
                mobile = (OOBMobileChannelSettings) settingsObj;
            }

            mobile.setClassName(_className);
            mobile.setIp(_ip);
            if (_logConfirmation1 != null) {
                boolean _logConfirmation = Boolean.parseBoolean(_logConfirmation1);
                mobile.setLogConfirmation(_logConfirmation);
            }
            mobile.setPassword(_password);
            mobile.setPhoneNumber(_phoneNo);
            if (_port != null || _port.isEmpty() == true) {
                int _port1 = Integer.parseInt(_port);
                mobile.setPort(_port1);
            }
            mobile.setReserve1(reserve1);
            mobile.setReserve2(reserve2);
            mobile.setReserve3(reserve3);
            if (_status1 != null) {
                int _status = Integer.parseInt(_status1);
                mobile.setStatus(_status);
            }
            mobile.setUserid(_userId);
            if (_autofailover != null) {
                int _iAutofailover = Integer.parseInt(_autofailover);
                mobile.setAutofailover(_iAutofailover);
            }

            if (_retrycount != null) {
                int _iRetrycount = Integer.parseInt(_retrycount);
                mobile.setRetrycount(_iRetrycount);
            }

            if (_retryduration != null) {
                int _iRetryDuration = Integer.parseInt(_retryduration);
                mobile.setRetryduration(_iRetryDuration);
            }

            if (_messgeLength != null) {
                int _imessgeLength = Integer.parseInt(_messgeLength);
                mobile.setMessageLength(_imessgeLength);
            }

            AuditManagement audit = new AuditManagement();
            if (bAddSetting == true) {
                retValue = sMngmt.addSetting(sessionId, _channelId, _type, _iPreference, mobile);
                log.debug("editsmssettings::addSetting::" + retValue);

                String resultString = "ERROR";
                if (retValue == 0) {
                    resultString = "SUCCESS";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add SMS Gateway Setting", resultString, retValue, "Setting Management",
                            "", "ChannelID=" + mobile.getChannelId() + "Class Name=" + mobile.getClassName()
                            + "IP=" + mobile.getIp() + "Password=***" + "Phone Number=" + mobile.getPhoneNumber() + "User ID=" + mobile.getUserid()
                            + "Autofailover=" + mobile.getAutofailover() + "Port=" + mobile.getPort() + "Prefernece=" + mobile.getPreference()
                            + "Reserve1=" + mobile.getReserve1() + "Reserve2" + mobile.getReserve2() + "Reserve3=" + mobile.getReserve3()
                            + "RetryCount=" + mobile.getRetrycount() + "Status=" + mobile.getStatus() + "Type=" + mobile.getType()
                            + "LogConfirmation=" + mobile.isLogConfirmation() + "Retry Duration" + mobile.getRetryduration(),
                            itemtype, _channelId);
                } else if (retValue != 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add SMS Gateway Setting", resultString, retValue, "Setting Management",
                            "", "Failed To Add SMS Gateway Setting", itemtype,
                            _channelId);
                }
            } else {
                OOBMobileChannelSettings oldmobileObj = (OOBMobileChannelSettings) sMngmt.getSetting(sessionId, _channelId, _type, _iPreference);
                retValue = sMngmt.changeSetting(sessionId, _channelId, _type, _iPreference, settingsObj, mobile);
                log.debug("changeSetting :: " + retValue);

                String resultString = "ERROR";

                if (retValue == 0) {
                    resultString = "SUCCESS";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change SMS Gateway Setting", resultString, retValue, "Setting Management",
                            "ChannelID=" + oldmobileObj.getChannelId() + " Class Name=" + oldmobileObj.getClassName() + "ImplementationJar="
                            + " IP=" + oldmobileObj.getIp() + " Password=***" + " Phone Number=" + oldmobileObj.getPhoneNumber() + " User ID=" + oldmobileObj.getUserid()
                            + " Autofailover=" + oldmobileObj.getAutofailover() + " Port=" + oldmobileObj.getPort() + " Prefernece=" + oldmobileObj.getPreference()
                            + " Reserve1=" + oldmobileObj.getReserve1() + " Reserve2" + oldmobileObj.getReserve2() + " Reserve3=" + oldmobileObj.getReserve3()
                            + " RetryCount=" + oldmobileObj.getRetrycount() + " Status=" + oldmobileObj.getStatus() + " Type=" + oldmobileObj.getType()
                            + " LogConfirmation=" + oldmobileObj.isLogConfirmation() + " Retry Duration" + oldmobileObj.getRetryduration(),
                            " ChannelID=" + mobile.getChannelId() + " Class Name=" + mobile.getClassName() + " ImplementationJar="
                            + " IP=" + mobile.getIp() + " Password=***" + " Phone Number=" + mobile.getPhoneNumber() + " User ID=" + mobile.getUserid()
                            + " Autofailover=" + mobile.getAutofailover() + " Port=" + mobile.getPort() + " Prefernece=" + mobile.getPreference()
                            + " Reserve1=" + mobile.getReserve1() + " Reserve2" + mobile.getReserve2() + " Reserve3=" + mobile.getReserve3()
                            + " RetryCount=" + mobile.getRetrycount() + " Status=" + mobile.getStatus() + " Type=" + mobile.getType()
                            + " LogConfirmation=" + mobile.isLogConfirmation() + " Retry Duration" + mobile.getRetryduration(),
                            itemtype, _channelId);
                } else if (retValue != 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change SMS Gateway Setting", resultString, retValue, "Setting Management",
                            "ChannelID=" + oldmobileObj.getChannelId() + " Class Name=" + oldmobileObj.getClassName() + "ImplementationJar="
                            + " IP=" + oldmobileObj.getIp() + " Password=***" + " Phone Number=" + oldmobileObj.getPhoneNumber() + " User ID=" + oldmobileObj.getUserid()
                            + " Autofailover=" + oldmobileObj.getAutofailover() + " Port=" + oldmobileObj.getPort() + " Prefernece=" + oldmobileObj.getPreference()
                            + " Reserve1=" + oldmobileObj.getReserve1() + " Reserve2" + oldmobileObj.getReserve2() + " Reserve3=" + oldmobileObj.getReserve3()
                            + " RetryCount=" + oldmobileObj.getRetrycount() + " Status=" + oldmobileObj.getStatus() + " Type=" + oldmobileObj.getType()
                            + " LogConfirmation=" + oldmobileObj.isLogConfirmation() + " Retry Duration" + oldmobileObj.getRetryduration(),
                            "Failed To Change SMS Gateway Setting", itemtype, _channelId);
                }

            }
        }

        if (retValue != 0) {
            result = "error";
            message = "SMS Gateway Settings Update Failed!!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }

    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";

    }// </editor-fold>
}
