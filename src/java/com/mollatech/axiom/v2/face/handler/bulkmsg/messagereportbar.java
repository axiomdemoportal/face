package com.mollatech.axiom.v2.face.handler.bulkmsg;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.face.common.bar;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.dictum.management.BulkMSGManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mollatech1
 */
public class messagereportbar extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(messagereportbar.class.getName());

    final int SMS = 1;
    final int VOICE = 2;
    final int USSD = 3;
    final int EMAIL = 4;
    final int SENT = 0;
    final int PENDING = 2;
    final int FAILED = -1;
    final int BLOCKED = -5;
    

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        log.info("is started ::");

        try {
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel ::" + channel.getName());
            String channelId = channel.getChannelid();
            log.debug("channelId ::"+ channelId);
            String _startdate = request.getParameter("_startdate");
            log.debug("_startdate ::" + _startdate);
            String _enddate = request.getParameter("_enddate");
            log.debug("_enddate ::"+_enddate);
            String _searchtext = request.getParameter("_searchtext");
            log.debug("_searchtext ::"+_searchtext);
            String _type = request.getParameter("_type");
            log.debug("_type ::"+_type);
            
        
            int type = -9999;
            if(_type != null){
                type = Integer.parseInt(_type);
            }
            
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date startDate = null;
            if (_startdate != null && !_startdate.isEmpty()) {
                try {
                    startDate = (Date) formatter.parse(_startdate);
                } catch (ParseException ex) {
                    Logger.getLogger(messagereportbar.class.getName()).log(Level.SEVERE, null, ex);
                    log.error("exception caught ::",ex);
                }
            }
            Date endDate = null;
            if (_enddate != null && !_enddate.isEmpty()) {
                endDate = (Date) formatter.parse(_enddate);
            }

            try {

                BulkMSGManagement mngt = new BulkMSGManagement();

////            
                int sentcount = mngt.getMessageCount(channelId, type, SENT, startDate, endDate,_searchtext);
                int pendingcount = mngt.getMessageCount(channelId, type, PENDING, startDate, endDate,_searchtext);
                int failedcount = mngt.getMessageCount(channelId, type, FAILED, startDate, endDate,_searchtext);
                int blockedcount = mngt.getMessageCount(channelId, type, BLOCKED, startDate, endDate,_searchtext);

//           int sentcount = mngt.getMessageCount(channelId,SENT,"1month");
//           int pendingcount = mngt.getMessageCount(channelId,PENDING, "1month");
//           int failedcount = mngt.getMessageCount(channelId,FAILED,"1month");
//           int blockedcount = mngt.getMessageCount(channelId,BLOCKED,"1month");


                ArrayList<bar> sample = new ArrayList<bar>();

                sample.add(new bar(sentcount, "SENT"));
                sample.add(new bar(pendingcount, "PENDING"));
                // sample.add(new Bar("MSG", sentcount, pendingcount, failedcount));
                sample.add(new bar(failedcount, "FAILED"));
                sample.add(new bar(blockedcount, "BLOCKED"));

                // sample.add(new Bar(blockedcount, "BLOCKED"));

//                for (int i = 0; i < sample.size(); i++) {
//                    System.out.println(sample.get(i));
//                }
                Gson gson = new Gson();

                JsonElement element = gson.toJsonTree(sample, new TypeToken<List<bar>>() {
                }.getType());

                JsonArray jsonArray = element.getAsJsonArray();
                //response.setContentType("application/json");
                out.print(jsonArray);

            } finally {
                out.close();
            }
        } catch (ParseException ex) {
           // Logger.getLogger(BarMSGChart.class.getName()).log(Level.SEVERE, null, ex);
            log.error("exception caught ::",ex);
        }
        
        log.info("is ended ::");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
