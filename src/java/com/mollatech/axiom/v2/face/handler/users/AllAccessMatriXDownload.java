
package com.mollatech.axiom.v2.face.handler.users;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.connector.access.controller.AccessMatrixSettings;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Roles;
import com.mollatech.axiom.nucleus.db.connector.management.AllAccessMatrixManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.v2.face.handler.operator.getopraudits;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AllAccessMatriXDownload extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AllAccessMatriXDownload.class.getName());

    
    private static final int BUFSIZE = 4096;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        log.info("is started :: ");
        try {
            AccessMatrixSettings  accessSettings = null;
            
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: " + channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId :: " + sessionId);
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
//            String _format = request.getParameter("_reporttype");
            String _accesstype = "ROLE";
            Roles [] roleObj = null;
            OperatorsManagement opMngt = new OperatorsManagement();
            int iRoleID = -1;
            if (!_accesstype.equals("OPERATOR")) {
                roleObj = opMngt.getAllRoles(channel.getChannelid());
            }
            String formatType = request.getParameter("_reporttype");
            OperatorsManagement opsMngt = new OperatorsManagement();
            OperatorsManagement opDetails = new OperatorsManagement();
            Roles role;
//            role = opDetails.getRoleById(channel.getChannelid(), roleid);
            
            int iFormat = Integer.parseInt(formatType);
            log.debug("iFormat :: " + iFormat);
            List<String> nameList = new ArrayList<String>();
            AccessMatrixSettings [] accessSettings1 = new AccessMatrixSettings[roleObj.length];
            for (int ii = 0; ii < roleObj.length; ii++) {
                byte[] accessByte = roleObj[ii].getAccessentry();
                nameList.add(roleObj[ii].getName());
                ByteArrayInputStream bais = null;
                Object object = null;
                if (accessByte != null) {
                    bais = new ByteArrayInputStream(AxiomProtect.AccessDataBytes(accessByte));
                    object = UtilityFunctions.deserializeFromObject(bais);
                }   if (object != null) {
                    accessSettings =  (AccessMatrixSettings) object;
                }
                accessSettings1[ii] = accessSettings;
            }
            

            String filepath = null;


            try {
                try {
                    AllAccessMatrixManagement acees = new AllAccessMatrixManagement();
                    filepath = acees.generateAccessMatrix(iFormat, nameList, accessSettings1);
                } catch (Exception e) {
                    log.error("exception caught :: ", e);
                }

                File file = new File(filepath);
                int length = 0;
                ServletOutputStream outStream = response.getOutputStream();
                ServletContext context = getServletConfig().getServletContext();
                String mimetype = context.getMimeType(filepath);

                // sets response content type
                if (mimetype == null) {
                    mimetype = "application/octet-stream";
                }
                response.setContentType(mimetype);
                response.setContentLength((int) file.length());
                String fileName = (new File(filepath)).getName();

                // sets HTTP header
                response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

                byte[] byteBuffer = new byte[BUFSIZE];
                DataInputStream in = new DataInputStream(new FileInputStream(file));

                // reads the file's bytes and writes them to the response stream
                while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                    outStream.write(byteBuffer, 0, length);
                }

                in.close();
                outStream.close();
                file.delete();

            } catch (Exception ex) {
                // TODO handle custom exceptions here
                log.error("exception caught :: ", ex);
            }

        } catch (Exception ex) {
            Logger.getLogger(getopraudits.class.getName()).log(Level.SEVERE, null, ex);
            log.error("exception caught :: ", ex);
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
