/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.AxiomRadiusConfiguration;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class loadradiussettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadradiussettings.class.getName());

    private JSONObject SettingsWhenEmpty(int _type1, int _preference1, Object settingsObj) {
        try {
            JSONObject json = new JSONObject();
            json.put("_accountEnabled","");
            json.put("_authEnabled", "");
            json.put("_authIp", "");
            json.put("_authPort", "");
            json.put("_accountIp", "");
            json.put("_accountPort", "");
            json.put("_ldapServerIp", "");
            json.put("_ldapServerPort", "");
            json.put("_ldapSearchPath","");
            json.put("_ldapUsername", "");
            json.put("_ldapPassword", "");
            json.put("_ldapSearchinitials", "");
            return json;
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        }
        return null;
    }

    private JSONObject SettingsWhenPresent(Object settingsObj) {
        JSONObject json = new JSONObject();
        if (settingsObj instanceof AxiomRadiusConfiguration) {
            try {
                AxiomRadiusConfiguration radius = (AxiomRadiusConfiguration) settingsObj;
                json.put("_accountEnabled", radius.isAccountEnabled());
                json.put("_authEnabled", radius.isAuthEnabled());
                json.put("_authIp", radius.getAuthIp());
                json.put("_authPort", radius.getAuthPort());
                json.put("_accountIp", radius.getAccountIp());
                json.put("_accountPort", radius.getAccountPort());
                json.put("_ldapServerIp", radius.getLdapServerIp());
                json.put("_ldapServerPort", radius.getLdapServerPort());
                json.put("_ldapSearchInitial", radius.getLdapSearchInitial());
                json.put("_ldapSearchPath", radius.getLdapSearchPath());
                json.put("_ldapUsername", radius.getLdapServerUsername());
                json.put("_ldapPassword", radius.getLdapServerPassword());
                json.put("_ldapValidate", radius.isLdapValidate());
                
                
                
            } catch (Exception ex) {
                log.error("Exception caught :: ",ex);
            }

        }
        return json;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        String _type = request.getParameter("_type");
        String _preference = request.getParameter("_preference");
        int _type1 = Integer.parseInt(_type);
        int _preference1 = Integer.parseInt(_preference);
        
        log.debug("loadradiussettings::channel is::"+channel.getName());
        log.debug("loadradiussettings::sessionid::"+sessionId);
        log.debug("loadradiussettings::type is::"+_type1);
        log.debug("loadradiussettings::preference is::"+_preference1);

        JSONObject json = null;//new JSONObject();
        PrintWriter out = response.getWriter();

        try {
            SettingsManagement sMngmt = new SettingsManagement();
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _type1, _preference1);
            if (settingsObj != null) {
                json = SettingsWhenPresent(settingsObj);
            } else {
                json = SettingsWhenEmpty(_type1, _preference1, settingsObj);
            }

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
            // TODO handle custom exceptions here
        }
        try {
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
