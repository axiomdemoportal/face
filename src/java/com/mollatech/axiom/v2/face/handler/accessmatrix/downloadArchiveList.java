/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.accessmatrix;

import com.mollatech.axiom.nucleus.db.Approvalsettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuthorizationManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.ArrayUtils;

/**
 *
 * @author nilesh
 */
public class downloadArchiveList extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(downloadArchiveList.class.getName());

    private static int PDF_TYPE = 0;
    private static int CSV_TYPE = 1;
    private static int TEXT_TYPE = 2;
    private static final int BUFSIZE = 4096;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        try {
            try {

                Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                log.debug("channel::" + channel.getName());
                String sessionId = (String) request.getSession().getAttribute("_apSessionID");
                log.debug("sessionId::" + sessionId);
                Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
                log.debug("operatorS::" + operatorS.getOperatorid());
                String _startAtTime = request.getParameter("_startAtTime");
                log.debug("_startAtTime::" + _startAtTime);
                String _endAtTime = request.getParameter("_endAtTime");
                log.debug("_endAtTime::" + _endAtTime);
                String _format = request.getParameter("_reporttype");
                log.debug("_format::" + _format);
                String _searchType = request.getParameter("_searchType");
                log.debug("_searchType::"+_searchType);
                String _statusA = request.getParameter("_statusA");
                log.debug("_statusA ::"+_statusA);
                String _unitIDs = request.getParameter("_unitIDs");
                log.debug("_unitIDs ::"+_unitIDs);
                String _operatorType = request.getParameter("_operatorType");
                log.debug("_operatorType::"+_operatorType);

                int isearchType = -9999;
                if (_searchType != null && !_searchType.isEmpty()) {
                    isearchType = Integer.parseInt(_searchType);
                }
                int iFormat = -9999;
                if (_format != null && !_format.isEmpty()) {
                    iFormat = Integer.parseInt(_format);
                }
                  int ioperatorType = -9999;
                if (_operatorType != null && !_operatorType.isEmpty()) {
                    ioperatorType = Integer.parseInt(_operatorType);
                }
                 if (PDF_TYPE == iFormat) {
                    iFormat = PDF_TYPE;
                } else if (CSV_TYPE == iFormat) {
                    iFormat = CSV_TYPE;
                } else {
                    iFormat = TEXT_TYPE;
                }

                String filepath = null;

                try {
                    try {

                        AuthorizationManagement aObj = new AuthorizationManagement();
                        Approvalsettings[] arrRequestAuth = null;
                        Approvalsettings[] arrRequest = null;
                        Approvalsettings[] arrRequestReq = null;
                        if (isearchType == 0) {
                            if(ioperatorType == OperatorsManagement.AUTHORIZER ){//authorizer
                            arrRequestAuth = aObj.getALLArchiveRequestAuthorizer(sessionId, operatorS.getOperatorid(), channel.getChannelid());
                            }else if(ioperatorType == OperatorsManagement.REQUESTER){//requester
                            arrRequestReq = aObj.getALLArchiveRequest(sessionId, operatorS.getOperatorid(), channel.getChannelid());
                            }
                            arrRequest = (Approvalsettings[]) ArrayUtils.addAll(arrRequestAuth, arrRequestReq);
                        
                        } else {
                            int reqStatus = Integer.parseInt(_statusA);
                            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                            Date startDate = null;
                            if (_startAtTime != null && !_startAtTime.isEmpty()) {
                                startDate = (Date) formatter.parse(_startAtTime);
                            }
                            Date endDate = null;
                            if (_endAtTime != null && !_endAtTime.isEmpty()) {
                                endDate = (Date) formatter.parse(_endAtTime);
                            }
                            if(ioperatorType == OperatorsManagement.AUTHORIZER){//authorizer
                            arrRequestAuth = aObj.getArchiveRequestReqV2(sessionId, channel.getChannelid(), operatorS.getOperatorid(), startDate, endDate,
                                    reqStatus);
                            }else if(ioperatorType == OperatorsManagement.REQUESTER){//requester
                                arrRequestReq = aObj.getArchiveRequestReq(sessionId, channel.getChannelid(),operatorS.getOperatorid(), startDate, endDate,
                               reqStatus); 
                            }
                             arrRequest=(Approvalsettings[])ArrayUtils.addAll(arrRequestAuth, arrRequestReq);
                               
                           // }
                        }
                       filepath = aObj.generateReport(iFormat, sessionId, channel.getChannelid(), arrRequest, _unitIDs,isearchType,ioperatorType);
                        arrRequest = null;
                    } catch (Exception e) {
                        log.error("exception caught :: ",e);
                    }

                    File file = new File(filepath);
                    int length = 0;
                    ServletOutputStream outStream = response.getOutputStream();
                    ServletContext context = getServletConfig().getServletContext();
                    String mimetype = context.getMimeType(filepath);

                    // sets response content type
                    if (mimetype == null) {
                        mimetype = "application/octet-stream";
                    }
                    
                    response.setContentType(mimetype);
                    response.setContentLength((int) file.length());
                    String fileName = (new File(filepath)).getName();

                    // sets HTTP header
                    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

                    byte[] byteBuffer = new byte[BUFSIZE];
                    DataInputStream in = new DataInputStream(new FileInputStream(file));

                    // reads the file's bytes and writes them to the response stream
                    while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                        outStream.write(byteBuffer, 0, length);
                    }

                    in.close();
                    outStream.close();
                    file.delete();

                } catch (Exception ex) {
                    // TODO handle custom exceptions here
                    log.error("exception caught :: ",ex);
                }

            } catch (Exception ex) {
                log.error("exception caught :: ",ex);
            }

        } finally {
            //  out.close();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
