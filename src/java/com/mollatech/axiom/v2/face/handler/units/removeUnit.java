package com.mollatech.axiom.v2.face.handler.units;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;

import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.operation.AxiomOperator;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet;
import com.mollatech.axiom.nucleus.db.Units;

/**
 *
 * @author nilesh
 */
public class removeUnit extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(removeUnit.class.getName());

    final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;
    final String itemType = "UNITS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        
        log.info("is started :: ");

        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Unit Removed Successfully!!!";

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String operatorId = operatorS.getOperatorid();
        log.debug("operatorId :: "+operatorId);

        JSONObject json = new JSONObject();
        int retValue = -1;
        String _tid = request.getParameter("_tid");
        int _unitid = Integer.valueOf(_tid);
        log.debug("_unitid :: "+_unitid);

        if (_tid == null) {
            result = "error";
            message = "Invalid Unit Details!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        UnitsManagemet unMngt = new UnitsManagemet();
        AuditManagement audit = new AuditManagement();
        Units oldUnit = unMngt.getUnitByUnitId(sessionId, channel.getChannelid(), _unitid);

        OperatorsManagement oMngt = new OperatorsManagement();

        AxiomOperator[] opr = oMngt.getOperatorByUnitStatus(sessionId, channel.getChannelid(), _unitid, 2);
          if (opr != null) {
//                for (int i = 0; i < opr.length; i++) {
//                    if (opr[i].getStrName().equals("sysadmin")) {
                        result = "error";
                        message = "Removed  Failed. Reason: Operators belongs to unit!!!";
                        try {
                            json.put("_result", result);
                            json.put("_message", message);
                        } catch (Exception e) {
                            log.error("exception caught :: ",e);
                        }
                        out.print(json);
                        out.flush();
                        return;
//                    }
//                }
            }
    

//        if (opr != null) {
//            for (int i = 0; i < opr.length; i++) {
//                if (opr[i].getStrName().equals("sysadmin")) {
//                    result = "error";
//                    message = "Remove Failed. Reason: Sysadmin belongs to same unit!!!";
//                    try {
//                        json.put("_result", result);
//                        json.put("_message", message);
//                    } catch (Exception e) {
//                        log.error("Exception caught :: ",e);
//                    }
//                    out.print(json);
//                    out.flush();
//                    return;
//                }
//            }
//        }

        Operators opera = oMngt.getOperatorByUnitId(sessionId, channel.getChannelid(), oldUnit.getUnitid());

        if (opera != null) {
            result = "error";
            message = "Remove Failed. Reason: Operators are present!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        String oldStatus = "Suspended";
        if (oldUnit.getStatus() == ACTIVE_STATUS) {
            oldStatus = "Active ";
        }
        retValue = unMngt.deleteUnit(sessionId, channel.getChannelid(), _unitid);
        log.debug("deleteUnit :: "+retValue);

        String resultString = "ERROR";
        if (retValue == 0) {
            resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Delete Unit", resultString, retValue,
                    "Units Management", "Unit Name =" + oldUnit.getUnitname() + "Status =" + oldStatus
                    + "Created On =" + oldUnit.getCreatedOn() + "Updated On=" + oldUnit.getLastupOn(),
                    "Unit Removed Successfully...!!!", itemType, 
                    "" + _unitid);

        } else if (retValue != 0) {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Delete Template", resultString, retValue,
                    "Units Management", "Unit Name =" + oldUnit.getUnitname() + "Status =" + oldStatus
                    + "Created On =" + oldUnit.getCreatedOn() + "Updated On=" + oldUnit.getLastupOn(),
                    "Failed To remove Unit...!!!", itemType, 
                    "" + _unitid);
        }

        if (retValue == 0) {
            result = "success";
        } else {
            result = "error";
            message = "Unit Removal Failed!!!";
        }

        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();

        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
