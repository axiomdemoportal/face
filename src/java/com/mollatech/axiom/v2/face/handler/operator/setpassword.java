/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.operator;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PasswordTrailManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import static com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PASSWORD_POLICY_SETTING;
import static com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE;
import com.mollatech.axiom.nucleus.settings.PasswordPolicySetting;
import java.util.Calendar;
import java.util.Date;

public class setpassword extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(setpassword.class.getName());

    final String itemTypeOp = "OPERATOR";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        PrintWriter out = response.getWriter();

        String _manuallyOrAuto = request.getParameter("_manuallyOrAuto");
        log.debug("_manuallyOrAuto :: "+_manuallyOrAuto);
        String _operId = request.getParameter("_operId");
        log.debug("_operId :: "+_operId);

        Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operator :: "+operator);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        String _old_passwd = request.getParameter("_old_passwd");
        log.debug("_old_passwd :: "+_old_passwd);

        String strPassword = null;
        boolean check = Boolean.parseBoolean(_manuallyOrAuto);
        int retValue;
        String result = "success";
        String message = "Password Set sucessfully...!!!";

        JSONObject json = new JSONObject();

        OperatorsManagement oManagement = new OperatorsManagement();
        AuditManagement audit = new AuditManagement();
        //Operators operator = oManagement.getOperatorById(sessionId, channel.getChannelid(), _operId);
        
            PasswordTrailManagement setObj = new PasswordTrailManagement();

        if (check == false) {   // manual

            strPassword = (String) request.getParameter("_passwd");

            if (strPassword.equals("")) {
                result = "error";
                message = "password not set successfully!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);

                } catch (Exception ex) {
                    Logger.getLogger(setpassword.class.getName()).log(Level.SEVERE, null, ex);
                    log.error("exception caught :: ",ex);
                } finally {
                    out.print(json);
                    out.flush();
                }

            } else {

                Operators op = oManagement.getOperatorById(channel.getChannelid(), _operId);
                SettingsManagement settObj = new SettingsManagement();
                Object obj = settObj.getSettingInner(channel.getChannelid(), PASSWORD_POLICY_SETTING, PREFERENCE_ONE);
                if (obj != null) {
                
                    String password = setObj.GeneratePassword(channel.getChannelid(), strPassword);
                    if (password != null && !password.isEmpty()) {
                        strPassword = password;
                    }

                    if (obj instanceof PasswordPolicySetting) {
                        PasswordPolicySetting passwordSetting = (PasswordPolicySetting) obj;
                        //PasswordTrail pass = new PasswordTrail();
                        Date pUpdateDate = op.getPasswordupdatedOn();
                        Calendar pcurrent = Calendar.getInstance();
                        if(pUpdateDate != null){
                        pcurrent.setTime(pUpdateDate);
                        }else{
                             pcurrent.setTime(new Date());
                        }
                        pcurrent.set(Calendar.AM_PM, Calendar.AM);
                        pcurrent.add(Calendar.MONTH, 1);//1 month
                        Date pendDate = pcurrent.getTime();

                        int issuePasswordCount = setObj.getcountOfIssuePassword(channel.getChannelid(), _operId, pendDate, pUpdateDate);
                       if(passwordSetting.issuingLimitDuration != 99){  //no password issue limit
                        if (passwordSetting.issuingLimitDuration <= issuePasswordCount) {
                            result = "error";
                            message = "Password Issue Limit Reach...!!!";
                            try { json.put("_result", result);
                            json.put("_message", message);}catch(Exception e){log.error("Exception caught :: ",e);}
                            out.print(json);
                            out.flush();
                            return;
                        }
                       }
                    }
                }

                retValue = oManagement.SetPassword(sessionId, channel.getChannelid(), _operId, strPassword, check);
                log.debug("SetPassword :: "+retValue);

                if (retValue == 0) {
                    //PasswordTrail pass = new PasswordTrail();
//            AxiomOperator op = oManagement.GetOperatorByName(sessionId, channel.getChannelid(), _op_name);
                    String pas = setObj.MD5HashPassword(strPassword);
                    setObj.AddPasswordTrail(channel.getChannelid(), op.getOperatorid(), pas);
                }

                String resultString = "ERROR";
                if (retValue == 0) {
                    resultString = "SUCCESS";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                            "Set Password", resultString, retValue, "Operator Management",
                            "Old Password =*****", "New Password =*****", itemTypeOp, 
                            _operId);

                } else if (retValue != 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                            "Set Password", resultString, retValue, "Operator Management",
                            "Old Password =*****", "Failed To Set Password", itemTypeOp, 
                            _operId);

                }

                if (retValue != 0) {

                    result = "error";
                    message = "password not set successfully!!";

                }
                try {
                    json.put("_result", result);
                    json.put("_message", message);

                }catch(Exception e)
                {
                    log.error("exception caught :: ",e);
                } finally {
                    out.print(json);
                    out.flush();
                }
            }
        } else {

            UtilityFunctions u = new UtilityFunctions();
            Date d = new Date();
            strPassword = u.Bas64SHA1(sessionId + channel.getChannelid() + _operId + d.toString());
            u = null;

            retValue = oManagement.SetPassword(sessionId, channel.getChannelid(), _operId, strPassword, check);

            String resultString = "ERROR";
            if (retValue == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                        "Set Operator Password", resultString, retValue, "Operator Management",
                        "Old Password =*****", "New Password =*****", itemTypeOp, 
                        _operId);

            } else if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                        "Set Operator Password", resultString, retValue, "Operator Management",
                        "Old Password =*****", "Failed To Set Password", itemTypeOp, 
                        _operId);

            }

            if (retValue != 0) {
                result = "error";
                message = "password not set successfully!!";

            }
            try {
                json.put("_result", result);
                json.put("_message", message);

            }catch(Exception e){
                log.error("exception caught :: ",e);
            } finally {
                out.print(json);
                out.flush();
            }
            log.info("is ended :: ");

        }

    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
