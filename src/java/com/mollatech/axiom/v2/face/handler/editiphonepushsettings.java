/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.IphonePushNotificationSettings;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;

/**
 *
 * @author mollatech1
 */
public class editiphonepushsettings extends HttpServlet {
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editiphonepushsettings.class.getName());

    final String itemtype = "SETTINGS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String OperatorID = operatorS.getOperatorid();

        String _channelId = channel.getChannelid();
        SettingsManagement sMngmt = new SettingsManagement();
        int retValue = -1;

        String result = "success";
        String message = "Apple Push Notification Gateway Settings Update Successful!!!";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
       
            log.debug("channel :: " + channel.getName());
            log.debug("operatorS :: " + operatorS.getOperatorid());
            log.debug("sessionId :: " + sessionId);
            log.debug(" remoteaccesslogin :: " + remoteaccesslogin);
            log.debug("getChannelid :: " + channel.getChannelid());
            
//         if (AxiomProtect.CheckEnforcementFor(AxiomProtect.MOBILETRUST_PUSH) != 0 
//                 
//                  
//          ) {
//            result = "error";
//            message = "This feature is not available in this license!!!";
//
//            try {
//                json.put("_result", result);
//                json.put("_message", message);
//            } catch (Exception e) {
//                log.error("Exception caught :: ",e);
//            }
//            out.print(json);
//            out.flush();
//            return;
//        }
         
         
        

        String _className = null;
        String _ip = null;
        String _logConfirmation1 = null;
        String _port = null;
        Object reserve1 = null;
        Object reserve2 = null;
        Object reserve3 = null;
        String _status1 = null;
//        String _autofailover = null;
        String _retrycount = null;
        String _retryduration = null;
    
        String _bundleID = null;
        String _certpassowrd = null;
        String _applicationType = null;
       // int type =1;
//        String _preference = request.getParameter("_perference");
        int _iPreference = 1;
//        if (_preference != null) {
//            _iPreference = Integer.parseInt(_preference);
//        }
        String[] files = new String[1];	 // file names

         int _type = SettingsManagement.IOS_PUSH_NOTIFICATION_SETTING;
      
         if (_type == SettingsManagement.IOS_PUSH_NOTIFICATION_SETTING) {   //secondary
                 _ip = request.getParameter("_ipS");
                 log.debug("_ip :: "+_ip);
                _port = request.getParameter("_portS");
                log.debug("_port :: "+_port);
                _status1 = request.getParameter("_statusS");
                log.debug("_status1 :: "+_status1);
//                _autofailover = request.getParameter("_autofailoverS");
                _retrycount = request.getParameter("_retriesS");
                log.debug("_retrycount :: "+_retrycount);
                _retryduration = request.getParameter("_retrydurationS");
                log.debug("_retryduration :: "+_retryduration);
                _applicationType = request.getParameter("_delayWhileIdleS");
                log.debug("_applicationType :: "+_applicationType);
                _certpassowrd = request.getParameter("_password");
                log.debug("_certpassowrd :: "+_certpassowrd);
                _bundleID = request.getParameter("_bundleID");
                log.debug("_bundleID :: "+_bundleID);
                _className = request.getParameter("_classNameS");
                log.debug("_className :: "+_className);
                reserve1 = request.getParameter("_reserve1S");
                log.debug("reserve1 :: "+reserve1);
                reserve2 = request.getParameter("_reserve2S");
                log.debug("reserve2 :: "+reserve2);
                reserve3 = request.getParameter("_reserve3S");
                log.debug("reserve3 :: "+reserve3);
                _logConfirmation1 = request.getParameter("_logConfirmationS");
                log.debug("_logConfirmation1 :: "+_logConfirmation1);
                if (_logConfirmation1 == null) {
                    _logConfirmation1 = "false";
                } else {
                    _logConfirmation1 = "true";
                }
                //Upload Certificate
                String savepath = "";
                savepath = System.getProperty("catalina.home");
                if(savepath == null) { savepath = System.getenv("catalina.home"); }
                savepath += System.getProperty("file.separator");
                savepath += "axiomv2-settings";
                savepath += System.getProperty("file.separator");
                savepath += "uploads";
                savepath += System.getProperty("file.separator");
                String optionalFileName = "";
                FileItem fileItem = null;

                String dirName = savepath;
                int i = 0;
                if (ServletFileUpload.isMultipartContent(request)) {
                    try {
                        ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                        List fileItemsList = servletFileUpload.parseRequest(request);
                        Iterator it = fileItemsList.iterator();
                        while (it.hasNext()) {
                            FileItem fileItemTemp = (FileItem) it.next();
                            if (fileItemTemp.isFormField()) {
                                if (fileItemTemp.getFieldName().equals("filename")) {
                                    optionalFileName = fileItemTemp.getString();
                                } else {
                                    System.out.println("Additional fields in file upload form is  " + fileItemTemp.getFieldName());
                                }
                            } else {
                                fileItem = fileItemTemp;
                                
                            }
                        }
                        if (fileItem != null) {
                            String fileName = fileItem.getName();
                            if(fileItem.getSize() == 0){
                           String strError = "Please Select File To Upload...!!!";
                            result = "error";
                            try { json.put("result", result);
                            json.put("message", strError);
                            }catch(Exception e){log.error("Exception caught :: ",e);}
                            //out.print(json);
                            out.print("{result:'"+result+"',message:'"+strError+"'}");
                            out.flush();
                            return;
                        }
                            if(fileItem.getSize() == 0){
                           String strError = "Please Select File To Upload...!!!";
                            result = "error";
                            try { json.put("result", result);
                            json.put("message", strError);
                            }catch(Exception e){log.error("Exception caught :: ",e);}
                            //out.print(json);
                            out.print("{result:'"+result+"',message:'"+strError+"'}");
                            out.flush();
                            return;
                        }
                            if (fileItem.getSize() > 0 && fileItem.getSize() < 1024000 * 5) { // size cannot be more than 65Kb. We want it light.
                                if (optionalFileName.trim().equals("")) {
                                    fileName = FilenameUtils.getName(fileName);
                                } else {
                                    fileName = optionalFileName;
                                }
                                files[i++] = dirName + fileName;
                                File saveTo = new File(dirName + fileName);
                                fileItem.write(saveTo);
                            } else {
                                result = "error";
                                message = "Error: " + fileName + " size is more than 5MB. Please upload lighter files.";
                                json.put("_result", result);
                                json.put("_message", message);
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }

                    } catch (Exception ex) {
                        log.error("Exception caught :: ",ex);
                    }
                }

            }

            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _type, _iPreference);

            IphonePushNotificationSettings iphonepushSetting = null;
            boolean bAddSetting = false;

            if (settingsObj == null) {
                iphonepushSetting = new IphonePushNotificationSettings();
                iphonepushSetting.setChannelId(_channelId);
                bAddSetting = true;
            } else {
                iphonepushSetting = (IphonePushNotificationSettings) settingsObj;
            }

            //apple
            if (ServletFileUpload.isMultipartContent(request)) {
                iphonepushSetting.setCertificatePath(files[0]);
                iphonepushSetting.setCertPassword(_certpassowrd);
            }
            if(_bundleID != null){
            iphonepushSetting.setBundleId(_bundleID);
            }

            if (_applicationType != null) {
                int iAppType = Integer.parseInt(_applicationType);
                if (iAppType == 1) {
                    iphonepushSetting.setApplicationType(true);
                } else {
                    iphonepushSetting.setApplicationType(false);
                }

            }

            iphonepushSetting.setPreference(_iPreference);
            if(_className != null){
            iphonepushSetting.setClassName(_className);
            }
            if(_ip != null){
            iphonepushSetting.setIp(_ip);
            }
            if (_logConfirmation1 != null) {
                boolean _logConfirmation = Boolean.parseBoolean(_logConfirmation1);
                iphonepushSetting.setLogConfirmation(_logConfirmation);
            }
//            if (_port != null || _port.isEmpty() == true) {
//                int _port1 = Integer.parseInt(_port);
//                iphonepushSetting.setPort(_port1);
//            }
            
             if (_port != null  ) {
            if(_port.isEmpty() == false){
            int _port1 = Integer.parseInt(_port);
            iphonepushSetting.setPort(_port1);
            }
         }
             
             
            if(reserve1 != null){
            iphonepushSetting.setReserve1(reserve1);
            }
            if(reserve2 != null){
            iphonepushSetting.setReserve2(reserve2);
            }
            if(reserve3 != null){
            iphonepushSetting.setReserve3(reserve3);
            }
            if (_status1 != null) {
                int _status = Integer.parseInt(_status1);
                iphonepushSetting.setStatus(_status);
            }

             iphonepushSetting.setAutofailover(0);


            if (_retrycount != null) {
                int _iRetrycount = Integer.parseInt(_retrycount);
                iphonepushSetting.setRetrycount(_iRetrycount);
            }

            if (_retryduration != null) {
                int _iRetryDuration = Integer.parseInt(_retryduration);
                iphonepushSetting.setRetryduration(_iRetryDuration);
            }

             AuditManagement audit = new AuditManagement();
            if (bAddSetting == true) {
                retValue = sMngmt.addSetting(sessionId, _channelId, _type, _iPreference, iphonepushSetting);
                log.debug("addSetting :: " +retValue);

                String resultString = "ERROR";
              
                     if (retValue == 0) {
                    resultString = "SUCCESS";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add Apple Push Notification Setting", resultString, retValue, "Setting Management",
                            "", "ChannelID=" + iphonepushSetting.getChannelId() + "Class Name=" + iphonepushSetting.getClassName()
                            + "IP=" + iphonepushSetting.getIp() + " Certificate Password=***"
                            + "Autofailover=" + iphonepushSetting.getAutofailover() + "Port=" + iphonepushSetting.getPort() + "Prefernece=" + 1
                            + "Reserve1=" + iphonepushSetting.getReserve1() + "Reserve2" + iphonepushSetting.getReserve2() + "Reserve3=" + iphonepushSetting.getReserve3()
                            + "RetryCount=" + iphonepushSetting.getRetrycount() + "Status=" + iphonepushSetting.getStatus()
                            + "LogConfirmation=" + iphonepushSetting.isLogConfirmation() + "Retry Duration" + iphonepushSetting.getRetryduration()
                            + "Application Type=" + _applicationType +"Bundle Id"+_bundleID+"Certificate Path ="+files[0]
                           ,
                            itemtype, OperatorID);
                } else if (retValue != 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add Apple Push Notification Setting", resultString, retValue, "Setting Management",
                            "", "Failed To add Apple Push Notification Setting ...!", itemtype,
                            OperatorID);
                }
                
            } else {
                IphonePushNotificationSettings oldpushObj = (IphonePushNotificationSettings) sMngmt.getSetting(sessionId, _channelId, _type, _iPreference);
                retValue = sMngmt.changeSetting(sessionId, _channelId, _type, _iPreference, settingsObj, iphonepushSetting);
                log.debug("editiphonepushsettings::changeSetting::" +retValue);

                String resultString = "ERROR";
                
                String _oldappType = "Production";
                 String _appType = "Production";
                if(oldpushObj.isApplicationType() == true){
                    _oldappType = "Sandbox";
                }
                if(iphonepushSetting.isApplicationType() == true){
                    _appType = "Sandbox";
                }
        
                    if (retValue == 0) {
                    resultString = "Success";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change Push Notification Settings", resultString, retValue, "Setting Management",
                            "ChannelID=" + oldpushObj.getChannelId() + " Class Name=" + oldpushObj.getClassName() + "ImplementationJar="
                            + " IP=" + oldpushObj.getIp() + " Password=***" + " Autofailover=" + oldpushObj.getAutofailover() + " Port=" + oldpushObj.getPort()
                            + " Prefernece=" + 1 + " Reserve1=" + oldpushObj.getReserve1() + " Reserve2" + oldpushObj.getReserve2() + " Reserve3=" + oldpushObj.getReserve3()
                            + " RetryCount=" + oldpushObj.getRetrycount() + " Status=" + oldpushObj.getStatus()
                            + " LogConfirmation=" + oldpushObj.isLogConfirmation() + " Retry Duration" + oldpushObj.getRetryduration()
                             + "Application Type=" + _oldappType +"Bundle Id"+oldpushObj.getBundleId()+"Certificate Path ="+oldpushObj.getCertificatePath(),
                            "ChannelID=" + iphonepushSetting.getChannelId() + "Class Name=" + iphonepushSetting.getClassName()
                            + "IP=" + iphonepushSetting.getIp() + "Password=***"
                            + "Autofailover=" + iphonepushSetting.getAutofailover() + "Port=" + iphonepushSetting.getPort() + "Prefernece=" + 1
                            + "Reserve1=" + iphonepushSetting.getReserve1() + "Reserve2" + iphonepushSetting.getReserve2() + "Reserve3=" + iphonepushSetting.getReserve3()
                            + "RetryCount=" + iphonepushSetting.getRetrycount() + "Status=" + iphonepushSetting.getStatus()
                            + "LogConfirmation=" + iphonepushSetting.isLogConfirmation() + "Retry Duration" + iphonepushSetting.getRetryduration()
                            + "Application Type=" + _appType +"Bundle Id"+iphonepushSetting.getBundleId()+"Certificate Path ="+iphonepushSetting.getCertificatePath(),
                            itemtype, channel.getChannelid());
                } else if (retValue != 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change  Push Notification Settings", resultString, retValue, "Setting Management",
                            "ChannelID=" + oldpushObj.getChannelId() + " Class Name=" + oldpushObj.getClassName() + "ImplementationJar="
                            + " IP=" + oldpushObj.getIp() + " Password=***" + " Autofailover=" + oldpushObj.getAutofailover() + " Port=" + oldpushObj.getPort()
                            + " Prefernece=" + 1 + " Reserve1=" + oldpushObj.getReserve1() + " Reserve2" + oldpushObj.getReserve2() + " Reserve3=" + oldpushObj.getReserve3()
                            + " RetryCount=" + oldpushObj.getRetrycount() + " Status=" + oldpushObj.getStatus()
                            + " LogConfirmation=" + oldpushObj.isLogConfirmation() + " Retry Duration" + oldpushObj.getRetryduration()
                             + "Application Type=" + _oldappType +"Bundle Id"+oldpushObj.getBundleId()+"Certificate Path ="+oldpushObj.getCertificatePath(),
                            "Failed To Edit Setting...!!!", itemtype, channel.getChannelid());
                }   
            
        }

        if (retValue != 0) {
            result = "error";
            message = "Push Notification Gateway Settings Update Failed!!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
