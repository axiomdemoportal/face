/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.operator;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Passwordtrail;
import com.mollatech.axiom.nucleus.db.Sessions;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.OperatorsUtil;
import com.mollatech.axiom.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PasswordTrailManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import static com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PASSWORD_POLICY_SETTING;
import static com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE;
import com.mollatech.axiom.nucleus.settings.PasswordPolicySetting;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
//import org.json.JSONException;
import org.json.JSONObject;

public class changeoprpassword extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(changeoprpassword.class.getName());

    final String itemTypeOp = "OPERATOR";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        
        log.info("is started :: ");

        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Operator updated successfully!!!";

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);

        JSONObject json = new JSONObject();
        int retValue = 0;

        String _op_oldpassword = request.getParameter("_oldPassword");
        log.debug("_op_oldpassword :: "+_op_oldpassword);
        String _op_newpassword = request.getParameter("_newPassword");
        log.debug("_op_newpassword :: "+_op_newpassword);
        String _op_confirmpassword = request.getParameter("_confirmPassword");
        log.debug("_op_confirmpassword :: "+_op_confirmpassword);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS);

        if (!_op_newpassword.equals(_op_confirmpassword)) {
            result = "error";
            message = "Password & its confirmation did not match!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
               log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);

//        if (_op_oldpassword == null || _op_newpassword == null || _op_confirmpassword == null
//                || _op_oldpassword.length() == 0
//                || _op_newpassword.length() == 0
//                || _op_confirmpassword.length() == 0) {
//            result = "error";
//            message = "Invalid Parameters!!!";
//            try {
//                json.put("_result", result);
//                json.put("_message", message);
//            } catch (Exception e) {
//                log.error("Exception caught :: ",e);
//            }
//            out.print(json);
//            out.flush();
//            return;
//        }
        String _operId = operatorS.getOperatorid();
        log.debug("_operId :: "+_operId);
        OperatorsManagement oManagement = new OperatorsManagement();
        SettingsManagement settObj = new SettingsManagement();
        AuditManagement audit = new AuditManagement();
         Operators operator = oManagement.getOperatorById(channel.getChannelid(), _operId);
         log.debug("operator :: "+operator.getOperatorid());
//        SessionFactoryUtil suOperators = new SessionFactoryUtil(SessionFactoryUtil.operators);
//        Session sOperators = suOperators.openSession();
//
//        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
//        Session sChannel = suChannel.openSession();
//        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
//
//        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
//        Session sRemoteAcess = suRemoteAcess.openSession();
//        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
//
//         channel = cUtil.getChannel("face");
//
//        OperatorsUtil opUtil = new OperatorsUtil(suOperators, sOperators);
//        Operators operator = opUtil.GetByName(channel.getChannelid(), "sysadmin");
         String strOldPassword = operator.getPasssword();
        Object obj = settObj.getSettingInner(channel.getChannelid(), PASSWORD_POLICY_SETTING, PREFERENCE_ONE);
        PasswordPolicySetting passwordSetting = null;
        if (obj != null) {
            if (obj instanceof PasswordPolicySetting) {
                passwordSetting = (PasswordPolicySetting) obj;
                PasswordTrailManagement pass = new PasswordTrailManagement();
//                if (passwordSetting.changePasswordAfterFirstlogin == false) {
//                    result = "error";
//                    message = "Change Password is Disabled ...!!!";
//                    try { json.put("_result", result);
//                    json.put("_message", message);
//                    }catch(Exception e){log.error("Exception caught :: ",e);}
//                    out.print(json);
//                    out.flush();
//                    return;
//                }

                if (passwordSetting.allowCommonPassword == false) {
                    if (passwordSetting.arrCommonPassword != null) {
                        for (int i = 0; i<passwordSetting.arrCommonPassword.length; i++) {
                            if (passwordSetting.arrCommonPassword[i].equals(_op_newpassword)) {
                                result = "error";
                                message = "Please select Strong Password!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                   log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }

                }

                Date pUpdateDate = operator.getPasswordupdatedOn();

                Calendar pcurrent = Calendar.getInstance();
                if (pUpdateDate != null) {
                    pcurrent.setTime(pUpdateDate);
                } else {
                    pcurrent.setTime(new Date());
                }
                pcurrent.set(Calendar.AM_PM, Calendar.AM);
                pcurrent.add(Calendar.MONTH, 1);//1 month
                Date pendDate = pcurrent.getTime();

                if (passwordSetting.issuingLimitDuration != 99) {//any no of password
                    int issuePasswordCount = pass.getcountOfIssuePassword(channel.getChannelid(), operator.getOperatorid(), pendDate, pUpdateDate);
                    if (passwordSetting.issuingLimitDuration <= issuePasswordCount) {
                        result = "error";
                        message = "Password Issue Limit Reach...!!!";
                        try {
                            json.put("_result", result);
                            json.put("_message", message);
                        } catch (Exception e) {
                            log.error("exception caught :: ",e);
                        }
                        out.print(json);
                        out.flush();
                        return;
                    }
                }
                String[] strOldPassword1 = pass.getPasswordUsingOpId(channel.getChannelid(), operator.getOperatorid(), passwordSetting.passwordGenerations);
                String pas = pass.MD5HashPassword(_op_newpassword);
                String str = "";
                if (strOldPassword1 != null) {
                    for (int i = 0; i < strOldPassword1.length; i++) {
                        str = str + "," + strOldPassword1[i];
                    }
                }
                if (str.contains(pas)) {
                    result = "error";
                    message = "Password already used in previous generations...!!!";
                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("exception caught :: ",e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
                Date date = operator.getPasswordupdatedOn();
                int oldpasswordreusein = passwordSetting.oldPasswordCannotbeReused;

                Calendar current = Calendar.getInstance();
                if (date != null) {
                    current.setTime(date);
                } else {
                    current.setTime(new Date());
                }
                current.set(Calendar.AM_PM, Calendar.AM);
                current.add(Calendar.MONTH, oldpasswordreusein);//add a date
                Date endDate = current.getTime();

                Calendar current1 = Calendar.getInstance();
                current1.setTime(new Date());
                current1.set(Calendar.AM_PM, Calendar.AM);
//                current1.add(Calendar.MONTH, oldpasswordreusein);//add a date
                Date currentDate = current1.getTime();

                if (currentDate.before(endDate)) {

                    Passwordtrail[] arrObj = pass.getPassword(channel.getChannelid(), operator.getOperatorid(), endDate, currentDate);
//                    String strOldPassword = "";
                    if (arrObj != null) {
                        for (int i = 0; i < arrObj.length; i++) {
                            strOldPassword = strOldPassword + arrObj[i].getOldpasswordhash();
                        }
                    }
                    String opnewPassword = pass.MD5HashPassword(_op_newpassword);
                    if (strOldPassword != null) {
                        if (strOldPassword.contains(opnewPassword)) {
                            result = "error";
                            message = "Password Already Used...!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                    }

                }
                if (passwordSetting.allowRepetationOfChars == false) {
                    char[] characters = _op_newpassword.toCharArray();
                    Map<Character, Integer> charMap = new HashMap<Character, Integer>();
                    int a = -1;
                    for (Character ch : characters) {
                        if (charMap.containsKey(ch)) {
                            charMap.put(ch, charMap.get(ch) + 1);
                            a = 0;
                            break;
                        } else {
                            charMap.put(ch, 1);
                        }
                    }
                    if (a == 0) {
                        result = "error";
                        message = "Password contains repeated characters!!!";
                        try {
                            json.put("_result", result);
                            json.put("_message", message);
                        } catch (Exception e) {
                            log.error("exception caught :: ",e);
                        }
                        out.print(json);
                        out.flush();
                        return;
                    }
                }

                int validPassword = pass.ValidatePassword(channel.getChannelid(), _op_newpassword, passwordSetting);
                log.debug("ValidatePassword :: "+validPassword);
                if (validPassword != 0) {
                    result = "error";
                    if (validPassword == -1) {
                        message = "Password Length Not Match to criteria...!!!";
                    } else if (validPassword == -2) {
                        message = "Password Must Have at least one Digit(e.g. 0,1 etc.)";
                    } else if (validPassword == -3) {
                        message = "Password Must Have at least one small-case letter(e.g. a,b,c etc.)";
                    } else if (validPassword == -4) {
                        message = "Password Must Have at least one upper-case letter(e.g. A,B,C etc.)";
                    } else if (validPassword == -5) {
                        message = "Password Must Have at least one Special character(e.g. !,@,# etc.)";
                    } else {
                        message = "Invalid Password...!!!";
                    }
                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("exception caught :: ",e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }

            }
        }

        Date d = new Date();
        if (!_op_newpassword.equals(_op_confirmpassword) || !operator.getPasssword().equals(_op_oldpassword)) {

            result = "error";
            message = "Old Password invalid OR New password/ its confirmation did not match!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;

        }

        Operators newoperator = new Operators(operator.getOperatorid(),
                channel.getChannelid(),
                operator.getName(),
                operator.getPhone(),
                operator.getEmailid(),
                _op_newpassword,
                operator.getUnits(),
                operator.getRoleid(),
                operator.getOperatorType(),
                OperatorsManagement.ACTIVE_STATUS,
                0,
                operator.getCreatedOn(),
                d,
                new Date(),
                operator.getChangePassword());

//        Operators newoperator = new Operators(operator.getOperatorid(),
//                channel.getChannelid(),
//                operator.getName(),
//                operator.getPhone(),
//                operator.getEmailid(),
//                _op_newpassword,
//                operator.getRoleid(),
//                operator.getStatus(),
//                0, 
//                OperatorsManagement.ACTIVE_STATUS,
//                operator.getCreatedOn(),
//                d, new Date());
//        Operators newoperator = new Operators(operator.getOperatorid(),
//                channel.getChannelid(),
//                operator.getName(),
//                operator.getPhone(),
//                operator.getEmailid(),
//                _op_newpassword,
//                operator.getRoleid(),
//                operator.getStatus(),
//                0,
//                operator.getCreatedOn(),
//                d,new Date(),);
        SessionManagement sManagement = new SessionManagement();
//        String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
//         sessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], 
//                 credentialInfo[1], request.getSession().getId());
        retValue = oManagement.EditOperatorPassword(sessionId, channel.getChannelid(),
                operator.getOperatorid(),
                operator, newoperator);
        log.debug("EditOperatorPassword :: "+retValue);

        if (retValue == 0) {
            SessionManagement smngt = new SessionManagement();
            Sessions[] arrSession = smngt.getUserSessionByStatus(operator.getOperatorid(), smngt.ACTIVE_STATUS);
            if (arrSession != null) {
                for (int i = 0; i < arrSession.length; i++) {
                    smngt.UpdateSession(arrSession[i].getSessionid(), smngt.SUSPEND_STATUS);
                }
            }
            PasswordTrailManagement pass = new PasswordTrailManagement();
            String pas = pass.MD5HashPassword(_op_newpassword);
            pass.AddPasswordTrail(channel.getChannelid(), operator.getOperatorid(), pas);
        }

        String resultString = "ERROR";
        if (retValue == 0) {
            resultString = "SUCCESS";
            message = "Password changed successfully. You will be logged out now!!!";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                    "Change Password", resultString, retValue, "Operator Management",
                    "",
                    "Changed to new password",
                    itemTypeOp, 
                    operator.getOperatorid());

        }
        else {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                    "Change Password", resultString, retValue, "Operator Management",
                    "Name=" + operator.getName() + ",Email=" + operator.getEmailid() + ",Phone =" + operator.getPhone(),
                    "Failed To Edit Operator's Password",
                    itemTypeOp, 
                    operator.getOperatorid());
        }

        //String resultString="Failure";
        if (retValue == 0) {
            resultString = "success";
            message = "Password changed successfully. Please relogin.";

        }

        if (retValue != 0) {

            result = "error";
            message = "Operator's Password failed to change!!!";
        } else {
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                
                log.error("exception caught :: ",ex);

            } finally {
                out.print(json);
                out.flush();
            }
        }
         log.info("is ended :: ");

    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
