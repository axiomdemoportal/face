/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.accessmatrix;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
import com.mollatech.axiom.nucleus.db.Approvalsettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.AuthorizationManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.connector.access.controller.ApprovalSetting;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author nilesh
 */
public class removeAuthorizationRequest extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(removeAuthorizationRequest.class.getName());

    final String itemType = "AUTHORIZATION";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel ::" + channel.getName());
        String channelId = channel.getChannelid();
        log.debug("channelId::"+channelId);
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId::" + sessionId);
        String oldvalue = request.getParameter("_oldvalue");
        log.debug("oldvalue :: "+oldvalue);
        String newvalue = request.getParameter("_newvalue");
        log.debug("newvalue :: "+newvalue);
        
        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
         log.debug("remoteaccesslogin::" + remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS ::" + operatorS.getOperatorid());
        String result = "success";
        String message = "Request Approved!!!";

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        String _approvalID = request.getParameter("_approvalID");
        log.debug("_approvalID::" + _approvalID);
        String _description = request.getParameter("_description");
        log.debug("_description::" + _description);
//        int iapprovalID = Integer.parseInt(_approvalID);
//        

        AuthorizationManagement authrizationObj = new AuthorizationManagement();
        AuditManagement audit = new AuditManagement();
        OperatorsManagement oprMngt = new OperatorsManagement();
        UserManagement uMngt = new UserManagement();

        Approvalsettings approldObj = authrizationObj.getApprovalSettingByApprovalId(sessionId, channelId, _approvalID);

            byte[] obj = approldObj.getApprovalSettingEntry();
        byte[] f = AxiomProtect.AccessDataBytes(obj);
        ByteArrayInputStream bais = new ByteArrayInputStream(f);
        Object object = AuthorizationManagement.deserializeFromObject(bais);

        ApprovalSetting approvalSetting = null;
        if (object instanceof ApprovalSetting) {
            approvalSetting = (ApprovalSetting) object;
        }
        if (approvalSetting != null) {
            String _RejectionType = request.getParameter("_RejectionType");
            log.debug("_RejectionType ::" + _RejectionType);
            int retValue = -1;
            approvalSetting.reason = _description;
            if (_RejectionType != null) {
                retValue = authrizationObj.rejectAuthorizationRequest(sessionId, channel.getChannelid(), operatorS.getOperatorid(), _approvalID, approvalSetting, AuthorizationManagement.AUTORIZATION_BLOCK_STATUS,oldvalue,newvalue);
                log.debug("rejectAuthorizationRequest::" + retValue);
                if (retValue == 0) {
                    retValue = 1;
                }
            } else {
                SettingsManagement sMngmt = new SettingsManagement();
                Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                ChannelProfile chSettingObj = null;
                if (settingsObj != null) {
                    chSettingObj = (ChannelProfile) settingsObj;
                }
                String _unitId = request.getParameter("_unitId");
                log.debug("_unitId :: "+_unitId);
                if (_unitId != null) {
                    if (chSettingObj != null) {
                        if (chSettingObj.authorizationunit == 1) {

                            int iUnitId = Integer.parseInt(_unitId);
                            if (iUnitId != operatorS.getUnits()) {
                                result = "error";
                                message = "Sorry, Must be marked by same branch operator!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                   log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                }
                retValue = authrizationObj.rejectAuthorizationRequest(sessionId, channel.getChannelid(), operatorS.getOperatorid(), _approvalID, approvalSetting, AuthorizationManagement.AUTORIZATION_REJECT_STATUS,oldvalue,newvalue);
                log.debug("rejectAuthorizationRequest::" + retValue);
            }
            String resultString = "ERROR";

            if (retValue == 0) {
                String strAction = "-";
                Operators op = oprMngt.getOperatorById(channelId, approvalSetting.makerid);
                AuthUser u = uMngt.getUser(sessionId, channelId, approvalSetting.userId);
                String strOpName = "-";
                String struserName = "-";
                if (op != null) {
                    strOpName = op.getName();
                }
                if (u != null) {
                    struserName = u.getUserName();
                }

                resultString = "Success";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operatorS.getName(), new Date(), "Reject Authorization Request",
                        resultString, retValue,
                        "Authorization Management", "Action =" + strAction + ",Action Marked By=" + strOpName
                        + ",Action Marked On=" + struserName, "Removed successfully!!!",
                        itemType, 
                        approvalSetting.userId);

                result = "success";
                message = "Request removed successfully!!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    log.error("exception caught :: ",e);
                }
                out.print(json);
                out.flush();
                return;

            } else if (retValue == 1) {
                resultString = "Success";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operatorS.getName(), new Date(), "Block Authorization Request",
                        resultString, retValue,
                        "Authorization Management", "", "Request block due to reson =" + _description,
                        itemType, approvalSetting.userId);
                result = "error";
                message = "Request removed successfully!!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    log.error("exception caught :: ",e);
                }
                out.print(json);
                out.flush();
                return;
            } else {
                resultString = "Success";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operatorS.getName(), new Date(), "Reject Authorization Request",
                        resultString, retValue,
                        "Authorization Management", "", "Failed to remove!!!",
                        itemType, approvalSetting.userId);
                result = "success";
                message = "Failed to remove request!!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    log.error("exception caught :: ",e);
                }
                out.print(json);
                out.flush();
                return;
            }
        } else {
            result = "error";
            message = "Invalid Request!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            log.info("is ended :: ");
            return;
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
