/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.operator;

import com.mollatech.axiom.nucleus.db.Channels;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import org.json.JSONException;
import org.json.JSONObject;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 *
 * @author pramodchaudhari
 */
public class changeOperatortype extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(changeOperatortype.class.getName());
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
          response.setContentType("application/json");
         final String itemTypeOp = "OPERATOR";
        log.info("is started :: ");

        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Operator updated successfully!!!";

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);

        JSONObject json = new JSONObject();
        int retValue = 0;
        int checkValue = 0;
        String _operatorId = request.getParameter("_operatorId");
        String _opearatortype = request.getParameter("_opearatortype");
        int opertaorType=1;
        if (_opearatortype.equals("Authorizer")) {
            opertaorType = 2;
        } else {
            opertaorType = 1;
        }
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());
     
        if (_operatorId==null|| _operatorId.isEmpty() == true) {
            result = "error";
            message = "Invalid Parameters!!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
      
       
        OperatorsManagement oManagement = new OperatorsManagement();
        AuditManagement audit = new AuditManagement();
        Operators operator = oManagement.getOperatorById(channel.getChannelid(), _operatorId);
     if(operator==null)
         
     {   
         try{
           json.put("_result", "error");
            json.put("_message", "Operator can not be find!!!");
            out.print(json);
            return;
         }catch(Exception ex)
         {
         ex.printStackTrace();
         }
     }

        Date d = new Date();
//        Operators newoperator = new Operators(operator.getOperatorid(),
//                channel.getChannelid(),
//                _op_name,
//                _op_phone,
//                _op_email,
//                operator.getPasssword(),
//                _op_roleid,
//                _op_status,
//                operator.getCurrentAttempts(),
//                operator.getCreatedOn(),
//                d,new Date());
        
        Operators newoperator = new Operators(
                operator.getOperatorid(),
                channel.getChannelid(),
                operator.getName(),
                operator.getPhone(),
                operator.getEmailid(),
                operator.getPasssword(),
                operator.getUnits(),
                opertaorType,
                operator.getRoleid(),
                operator.getStatus(),
                operator.getCurrentAttempts(),                
                operator.getCreatedOn(),
                d,
                new Date(),
                operator.getChangePassword()
                );


        
        
        retValue = oManagement.EditOperator(sessionId, channel.getChannelid(),
                operator.getOperatorid(),
                operator, newoperator);

        String resultString = "ERROR";
        if (retValue == -4) {
            result = "error";
            message = "Details are already taken, provide unique credentials!!";

        } else {

            if (retValue == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Edit Operator", resultString, retValue, "Operator Management",
                        operator.toString(),
                        newoperator.toString(),
                        itemTypeOp, operator.getOperatorid());

            }else  {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Edit Operator", resultString, retValue, "Operator Management",
                        "",
                        "Failed To Edit Operator." + newoperator.toString(),                        
                        itemTypeOp, 
                        operator.getOperatorid());
                
                result = "error";
                message = "Operator update failed!!!";
            }
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        }catch(Exception e){
            log.error("exception caught :: ",e);
        }
        finally {
            out.print(json);
            out.flush();
        }
         log.info("is ended :: ");


    }
        
        
        
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
