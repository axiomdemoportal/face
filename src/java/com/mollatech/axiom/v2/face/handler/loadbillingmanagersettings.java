/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.BillingManagerSettings;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class loadbillingmanagersettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadbillingmanagersettings.class.getName());

    private JSONObject SettingsWhenEmpty() {
        JSONObject json = new JSONObject();
        log.info("Servlet started");
        try {
            json.put("_allowBillingManager", true);
            json.put("_currancyType","INR");

            json.put("_allowBillingManagerSWOTP", false);
            json.put("_selectBillTypeSWOTP", 2);
            json.put("_selectBillingDurationSWOTP", 1);
            json.put("_selectAlertBeforeSWOTP", 1);

            json.put("_allowBillingManagerHWOTP", false);
            json.put("_selectBillTypeHWOTP", 2);
            json.put("_selectBillingDurationHWOTP", 1);
            json.put("_selectAlertBeforeHWOTP", 1);

            json.put("_allowBillingManagerSWPKIOTP", false);
            json.put("_selectBillTypeSWPKIOTP", 2);
            json.put("_selectBillingDurationSWPKIOTP", 1);
            json.put("_selectAlertBeforeSWPKIOTP", 1);

            json.put("_allowBillingManagerHWPKIOTP", false);
            json.put("_selectBillTypeHWPKIOTP",2);
            json.put("_selectBillingDurationHWPKIOTP", 1);
            json.put("_selectAlertBeforeHWPKIOTP", 1);
            
             json.put("_allowBillingManagerCertificate", false);
            json.put("_selectBillTypeCertificate",2);
            json.put("_selectBillingDurationCertificate", 1);
            json.put("_selectAlertBeforeCertificate", 1);
            
            json.put("_allowBillingManagerOOBOTP", false);
            json.put("_selectBillTypeOOBOTP", 2);
            json.put("_selectBillingDurationOOBOTP", 1);
            json.put("_selectAlertBeforeOOBOTP", 1);
            json.put("_txPerDayOOBOTP", 3);
            

            json.put("_allowRepetationCharacterB", 0);
            json.put("_gatewayTypeB", 0);
            json.put("_alertAttemptB", 0);

        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        }

        return json;
    }

    private JSONObject SettingsWhenPresent(Object settingsObj) {
        JSONObject json = new JSONObject();
        if (settingsObj instanceof BillingManagerSettings) {
            BillingManagerSettings billingObj = (BillingManagerSettings) settingsObj;

            try {
                json.put("_allowBillingManager", billingObj.ballowBillingManager);
                json.put("_currancyType", billingObj.strCurrencyType);

            json.put("_allowBillingManagerSWOTP", billingObj.ballowBillingManagerSWOTP);
            json.put("_selectBillTypeSWOTP", billingObj.iselectBillTypeSWOTP);
            json.put("_selectBillingDurationSWOTP", billingObj.iselectBillingDurationSWOTP);
            json.put("_selectAlertBeforeSWOTP", billingObj.iselectAlertBeforeSWOTP);
            json.put("_costSWOTP", billingObj.icostSWOTP);

             json.put("_allowBillingManagerHWOTP", billingObj.ballowBillingManagerHWOTP);
            json.put("_selectBillTypeHWOTP", billingObj.iselectBillTypeHWOTP);
            json.put("_selectBillingDurationHWOTP", billingObj.iselectBillingDurationHWOTP);
            json.put("_selectAlertBeforeHWOTP", billingObj.iselectAlertBeforeHWOTP);
            json.put("_costHWOTP", billingObj.icostHWOTP);

             json.put("_allowBillingManagerSWPKIOTP", billingObj.ballowBillingManagerSWPKIOTP);
            json.put("_selectBillTypeSWPKIOTP", billingObj.iselectBillTypeSWPKIOTP);
            json.put("_selectBillingDurationSWPKIOTP", billingObj.iselectBillingDurationSWPKIOTP);
            json.put("_selectAlertBeforeSWPKIOTP", billingObj.iselectAlertBeforeSWPKIOTP);
            json.put("_costSWPKIOTP", billingObj.icostSWPKIOTP);
         
            json.put("_allowBillingManagerHWPKIOTP", billingObj.ballowBillingManagerHWPKIOTP);
            json.put("_selectBillTypeHWPKIOTP", billingObj.iselectBillTypeHWPKIOTP);
            json.put("_selectBillingDurationHWPKIOTP", billingObj.iselectBillingDurationHWPKIOTP);
            json.put("_selectAlertBeforeHWPKIOTP", billingObj.iselectAlertBeforeHWPKIOTP);
            json.put("_costHWPKIOTP", billingObj.icostHWPKIOTP);
            
            json.put("_allowBillingManagerCertificate", billingObj.ballowBillingManagerCertificate);
            json.put("_selectBillTypeCertificate", billingObj.iselectBillTypeCertificate);
            json.put("_selectBillingDurationCertificate", billingObj.iselectBillingDurationCertificate);
            json.put("_selectAlertBeforeCertificate", billingObj.iselectAlertBeforeCertificate);
            json.put("_costCertificate", billingObj.icostCertificate);
            
             json.put("_allowBillingManagerOOBOTP", billingObj.ballowBillingManagerOOBOTP);
            json.put("_selectBillTypeOOBOTP", billingObj.iselectBillTypeOOBOTP);
            json.put("_selectBillingDurationOOBOTP", billingObj.iselectBillingDurationOOBOTP);
            json.put("_selectAlertBeforeOOBOTP", billingObj.iselectAlertBeforeOOBOTP);
            json.put("_txPerDayOOBOTP", billingObj.itxPerDayOOBOTP);
            json.put("_costOOBOTP", billingObj.icostOOBOTP);

            json.put("_allowRepetationCharacterB", billingObj.alert);
            json.put("_gatewayTypeB", billingObj.igatewayType);
            json.put("_alertAttemptB",billingObj.ialertAttempt);
            json.put("_gatewayTypeB", billingObj.igatewayType);
            
            
            json.put("_costperTxOOBOTP",billingObj.icostperTxOOBOTP);
//            json.put("_thresholdCostOOBOTP",billingObj.ithresholdcostOOBOTP);
            } catch (Exception e) {
               log.error("Exception caught :: ",e);
            }
        }
        return json;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        
        log.debug("channel is::"+channel.getName());
        log.debug("sessionid::"+sessionId);
        JSONObject json = null;
        PrintWriter out = response.getWriter();
        try {
            SettingsManagement sMngmt = new SettingsManagement();
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), sMngmt.BILLING_MANAGER_SETTING, sMngmt.PREFERENCE_ONE);
            if (settingsObj != null) {
                json = SettingsWhenPresent(settingsObj);
            } else {
                json = SettingsWhenEmpty();
            }

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
            // TODO handle custom exceptions here
        }
        try {
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
            log.info("servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
