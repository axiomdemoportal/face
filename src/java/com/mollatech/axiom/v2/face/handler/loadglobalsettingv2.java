/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

/**
 *
 * @author mollatech1
 */
public class loadglobalsettingv2 extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadglobalsettingv2.class.getName());

    final int SMS = 4;
    final int VOICE = 5;
    final int USSD = 6;
    final int EMAIL = 7;
    final int FAX = 8;
    final int PUSH = 9;
    final int USER_TYPE = 10;
    final int MSG_TYPE = 12;
     final int CR_SETTINGS = 13;

    private JSONObject SettingsWhenEmpty(int _type1) {
        JSONObject json = new JSONObject();
        int iglobal = SettingsManagement.GlobalSettings;

        if (_type1 == iglobal) {
            try {
                json.put("_smslimit", 0);
                json.put("_smscost", 0);
                json.put("_smsStatus", 0);
                json.put("_smsAlertStatus", 0);
                json.put("_smsRepeatStatus", 1);
//            json.put("_smsRepeatDuration", 1);
                json.put("_smsThresholdlimit", 0);
                json.put("_fastPaceSMS", 15);
                json.put("_hyperPaceSMS", 35);
                json.put("_normalPaceSMS", 6);
                json.put("_slowPaceSMS", 1);
                json.put("_smsFooter", "Mollatech");
                json.put("_smsFooterStatus", 0);
                
                json.put("_dayRestrictionSMS", 1);
                json.put("_timeFromRestrictionSMS", 0);
                json.put("_timeToRestrictionSMS", 24);
                
                //voice
                json.put("_voicelimit", 0);
                json.put("_voicecost", 0);
                json.put("_voiceStatus", 0);
                json.put("_voiceAlertStatus", 0);
                json.put("_voiceRepeatStatus", 1);
//            json.put("_voiceRepeatDuration", 1);
                json.put("_voiceThresholdlimit", 0);
                json.put("_fastPaceVOICE", 15);
                json.put("_hyperPaceVOICE", 35);
                json.put("_normalPaceVOICE", 6);
                json.put("_slowPaceVOICE", 1);
                json.put("_voiceFooter", "Mollatech");
                json.put("_voiceFooterStatus", 0);
                
                json.put("_dayRestrictionVOICE", 1);
                json.put("_timeFromRestrictionVOICE", 0);
                json.put("_timeToRestrictionVOICE", 24);
                
                //ussd
                json.put("_ussdlimit", 0);
                json.put("_ussdcost", 0);
                json.put("_ussdStatus", 0);
                json.put("_ussdAlertStatus", 0);
                json.put("_ussdRepeatStatus", 1);
//            json.put("_ussdRepeatDuration", 1);
                json.put("_ussdThresholdlimit", 0);
                json.put("_fastPaceUSSD", 15);
                json.put("_hyperPaceUSSD", 35);
                json.put("_normalPaceUSSD", 6);
                json.put("_slowPaceUSSD", 1);
                json.put("_ussdFooter", "Mollatech");
                json.put("_ussdFooterStatus", 0);
                
                json.put("_dayRestrictionUSSD", 1);
                json.put("_timeFromRestrictionUSSD", 0);
                json.put("_timeToRestrictionUSSD", 24);
                
                //email
                json.put("_emaillimit", 0);
                json.put("_emailcost", 0);
                json.put("_emailStatus", 0);
                json.put("_emailAlertStatus", 0);
                json.put("_emailRepeatStatus", 1);
//            json.put("_emailRepeatDuration", 1);
                json.put("_emailThresholdlimit", 0);
                json.put("_fastPaceEMAIL", 15);
                json.put("_hyperPaceEMAIL", 35);
                json.put("_normalPaceEMAIL", 6);
                json.put("_slowPaceEMAIL", 1);
                json.put("_emailFooter", "Mollatech");
                json.put("_emailFooterStatus", 0);
                
                json.put("_dayRestrictionEMAIL", 1);
                json.put("_timeFromRestrictionEMAIL", 0);
                json.put("_timeToRestrictionEMAIL", 24);
                
                //fax
                json.put("_faxlimit", 0);
                json.put("_faxcost", 0);
                json.put("_faxStatus", 0);
                json.put("_faxThresholdlimit", 0);
                json.put("_faxAlertStatus", 0);
                json.put("_faxRepeatStatus", 1);
//            json.put("_faxRepeatDuration", 1);
                
                json.put("_fastPaceFAX", 15);
                json.put("_hyperPaceFAX", 35);
                json.put("_normalPaceFAX", 6);
                json.put("_slowPaceFAX", 1);
                json.put("_faxFooter", "Mollatech");
                json.put("_faxFooterStatus", 0);
                
                json.put("_dayRestrictionFAX", 1);
                json.put("_timeFromRestrictionFAX", 0);
                json.put("_timeToRestrictionFAX", 24);
                
                //push
                json.put("_pushlimit", 0);
                json.put("_pushcost", 0);
                json.put("_pushStatus", 0);
                json.put("_pushAlertStatus", 0);
                json.put("_pushRepeatStatus", 1);
//            json.put("_pushRepeatDuration", 1);
                json.put("_pushThresholdlimit", 0);
                json.put("_fastPacePUSH", 15);
                json.put("_hyperPacePUSH", 35);
                json.put("_normalPacePUSH", 6);
                json.put("_slowPacePUSH", 1);
                json.put("_pushFooter", "Mollatech");
                json.put("_pushFooterStatus", 0);
                
                json.put("_dayRestrictionPUSH", 1);
                json.put("_timeFromRestrictionPUSH", 0);
                json.put("_timeToRestrictionPUSH", 24);
                
                json.put("_weigtage",75);
                json.put("_answersattempts",3);
                
//            //user type
//            json.put("_userType", 0);
//             json.put("_msgType", 0);
            } catch(Exception ex){
                log.error("Exception caught :: ",ex);
            }
        }
        return json;
    }

    private JSONObject SettingsWhenPresent(int itype, Object settingsObj) {
        JSONObject json = new JSONObject();
        if (settingsObj instanceof GlobalChannelSettings) {
            GlobalChannelSettings globalObj = (GlobalChannelSettings) settingsObj;
            if (itype == SMS) {

                if (globalObj.smssettingobj == null) {
                    try {
                        json.put("_smslimit", 0);
                        json.put("_smscost", 0);
                        json.put("_smsStatus", 0);
                        json.put("_smsAlertStatus", 0);
                        json.put("_smsRepeatStatus", 1);
                        json.put("_smsFooter", "Mollatech");
                        json.put("_smsThresholdlimit", 0);
                        json.put("_fastPaceSMS", 15);
                        json.put("_hyperPaceSMS", 35);
                        json.put("_normalPaceSMS", 6);
                        json.put("_slowPaceSMS", 1);
                        json.put("_dayRestrictionSMS", 1);
                        json.put("_timeFromRestrictionSMS", 0);
                        json.put("_timeToRestrictionSMS", 24);
                        json.put("_smsFooterStatus", 0);
                    } catch (Exception ex) {
                        Logger.getLogger(loadglobalsettingv2.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    try {
                        json.put("_smslimit", globalObj.smssettingobj.smslimit);
                        json.put("_smscost", globalObj.smssettingobj.smscost);
                        json.put("_smsThresholdlimit", globalObj.smssettingobj.thresholdlimit);
                        json.put("_smsStatus", globalObj.smssettingobj.smslimitstatus);
                        json.put("_smsAlertStatus", globalObj.smssettingobj.smsalertstatus);
                        json.put("_smsRepeatStatus", globalObj.smssettingobj.repeat);
                        json.put("_smsFooter", globalObj.smssettingobj._smsFooter);
                        json.put("_fastPaceSMS", globalObj.smssettingobj._fastPaceSMS);
                        json.put("_hyperPaceSMS", globalObj.smssettingobj._hyperPaceSMS);
                        json.put("_normalPaceSMS", globalObj.smssettingobj._normalPaceSMS);
                        json.put("_slowPaceSMS", globalObj.smssettingobj._slowPaceSMS);
                        json.put("_dayRestrictionSMS", globalObj.smssettingobj._dayRestrictionSMS);
                        json.put("_timeFromRestrictionSMS", globalObj.smssettingobj._timeFromRestrictionSMS);
                        json.put("_timeToRestrictionSMS", globalObj.smssettingobj._timeToRestrictionSMS);
                        json.put("_smsFooterStatus", globalObj.smssettingobj._smsFooterStatus);
                    } catch (Exception ex) {
                        Logger.getLogger(loadglobalsettingv2.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            if (itype == VOICE) {
                if (globalObj.voicesettingobj == null) {
                    try {
                        json.put("_voicelimit", 0);
                        json.put("_voicecost", 0);
                        json.put("_voiceStatus", 0);
                        json.put("_voiceAlertStatus", 0);
                        json.put("_voiceRepeatStatus", 1);
                        json.put("_voiceFooter", "Mollatech");
                        json.put("_voiceThresholdlimit", 0);
                        json.put("_fastPaceVOICE", 15);
                        json.put("_hyperPaceVOICE", 35);
                        json.put("_normalPaceVOICE", 6);
                        json.put("_slowPaceVOICE", 1);
                        json.put("_dayRestrictionVOICE", 1);
                        json.put("_timeFromRestrictionVOICE", 0);
                        json.put("_timeToRestrictionVOICE", 24);
                        json.put("_voiceFooterStatus", 0);
                    } catch (Exception ex) {
                        Logger.getLogger(loadglobalsettingv2.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    try {
                        json.put("_voicelimit", globalObj.voicesettingobj.voicemsglimit);
                        json.put("_voicecost", globalObj.voicesettingobj.voicemsgcost);
                        json.put("_voiceStatus", globalObj.voicesettingobj.voicemsglimitstatus);
                        json.put("_voiceAlertStatus", globalObj.voicesettingobj.voicemsgalertstatus);
                        json.put("_voiceRepeatStatus", globalObj.voicesettingobj.repeat);
//                    json.put("_voiceRepeatDuration", globalObj.voicesettingobj.duration);
                        json.put("_voiceThresholdlimit", globalObj.voicesettingobj.thresholdlimit);
                        json.put("_fastPaceVOICE", globalObj.voicesettingobj._fastPaceVOICE);
                        json.put("_hyperPaceVOICE", globalObj.voicesettingobj._hyperPaceVOICE);
                        json.put("_normalPaceVOICE", globalObj.voicesettingobj._normalPaceVOICE);
                        json.put("_slowPaceVOICE", globalObj.voicesettingobj._slowPaceVOICE);
                        json.put("_voiceFooter", globalObj.voicesettingobj._voiceFooter);
                        json.put("_dayRestrictionVOICE", globalObj.voicesettingobj._dayRestrictionVOICE);
                        json.put("_timeFromRestrictionVOICE", globalObj.voicesettingobj._timeFromRestrictionVOICE);
                        json.put("_timeToRestrictionVOICE", globalObj.voicesettingobj._timeToRestrictionVOICE);
                        json.put("_voiceFooterStatus", globalObj.voicesettingobj._voiceFooterStatus);
                    } catch (Exception ex) {
                        Logger.getLogger(loadglobalsettingv2.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            if (itype == USSD) {

                if (globalObj.ussdsettingobj == null) {
                    try {
                        json.put("_ussdlimit", 0);
                        json.put("_ussdcost", 0);
                        json.put("_ussdStatus", 0);
                        json.put("_ussdAlertStatus", 0);
                        json.put("_ussdRepeatStatus", 1);
//                    json.put("_ussdRepeatDuration", 1);
                        json.put("_ussdThresholdlimit", 0);
                        json.put("_fastPaceUSSD", 15);
                        json.put("_hyperPaceUSSD", 35);
                        json.put("_normalPaceUSSD", 6);
                        json.put("_slowPaceUSSD", 1);
                        json.put("_ussdFooter", "Mollatech");
                        json.put("_dayRestrictionUSSD", 1);
                        json.put("_timeFromRestrictionUSSD", 0);
                        json.put("_timeToRestrictionUSSD", 24);
                        json.put("_ussdFooterStatus", 0);
                    } catch (Exception ex) {
                        Logger.getLogger(loadglobalsettingv2.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    try {
                        json.put("_ussdlimit", globalObj.ussdsettingobj.ussdlimit);
                        json.put("_ussdcost", globalObj.ussdsettingobj.ussdcost);
                        json.put("_ussdStatus", globalObj.ussdsettingobj.ussdlimitstatus);
                        json.put("_ussdAlertStatus", globalObj.ussdsettingobj.ussdalertstatus);
                        json.put("_ussdRepeatStatus", globalObj.ussdsettingobj.repeat);
//                    json.put("_ussdRepeatDuration", globalObj.ussdsettingobj.duration);
                        json.put("_ussdThresholdlimit", globalObj.ussdsettingobj.thresholdlimit);
                        json.put("_fastPaceUSSD", globalObj.ussdsettingobj._fastPaceUSSD);
                        json.put("_hyperPaceUSSD", globalObj.ussdsettingobj._hyperPaceUSSD);
                        json.put("_normalPaceUSSD", globalObj.ussdsettingobj._normalPaceUSSD);
                        json.put("_slowPaceUSSD", globalObj.ussdsettingobj._slowPaceUSSD);
                        json.put("_ussdFooter", globalObj.ussdsettingobj._ussdFooter);
                        json.put("_dayRestrictionUSSD", globalObj.ussdsettingobj._dayRestrictionUSSD);
                        json.put("_timeFromRestrictionUSSD", globalObj.ussdsettingobj._timeFromRestrictionUSSD);
                        json.put("_timeToRestrictionUSSD", globalObj.ussdsettingobj._timeToRestrictionUSSD);
                        json.put("_ussdFooterStatus", globalObj.ussdsettingobj._ussdFooterStatus);
                    } catch (Exception ex) {
                        Logger.getLogger(loadglobalsettingv2.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            if (itype == EMAIL) {
                if (globalObj.emailsettingobj == null) {
                    try {
                        json.put("_emaillimit", 0);
                        json.put("_emailcost", 0);
                        json.put("_emailStatus", 0);
                        json.put("_emailAlertStatus", 0);
                        json.put("_emailRepeatStatus", 1);
//                    json.put("_emailRepeatDuration", 1);
                        json.put("_emailThresholdlimit", 0);
                        json.put("_fastPaceEMAIL", 15);
                        json.put("_hyperPaceEMAIL", 35);
                        json.put("_normalPaceEMAIL", 6);
                        json.put("_slowPaceEMAIL", 1);
                        json.put("_emailFooter", "Mollatech");
                        json.put("_dayRestrictionEMAIL", 1);
                        json.put("_timeFromRestrictionEMAIL", 0);
                        json.put("_timeToRestrictionEMAIL", 24);
                        json.put("_emailFooterStatus", 0);
                    } catch (Exception ex) {
                        Logger.getLogger(loadglobalsettingv2.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    try {
                        json.put("_emaillimit", globalObj.emailsettingobj.emaillimit);
                        json.put("_emailcost", globalObj.emailsettingobj.emailcost);
                        json.put("_emailStatus", globalObj.emailsettingobj.emaillimitstatus);
                        json.put("_emailAlertStatus", globalObj.emailsettingobj.emailalertstatus);
                        json.put("_emailRepeatStatus", globalObj.emailsettingobj.repeat);
//                    json.put("_emailRepeatDuration", globalObj.emailsettingobj.duration);
                        json.put("_emailThresholdlimit", globalObj.emailsettingobj.thresholdlimit);
                        json.put("_fastPaceEMAIL", globalObj.emailsettingobj._fastPaceEMAIL);
                        json.put("_hyperPaceEMAIL", globalObj.emailsettingobj._hyperPaceEMAIL);
                        json.put("_normalPaceEMAIL", globalObj.emailsettingobj._normalPaceEMAIL);
                        json.put("_slowPaceEMAIL", globalObj.emailsettingobj._slowPaceEMAIL);
                        json.put("_emailFooter", globalObj.emailsettingobj._emailFooter);
                        json.put("_dayRestrictionEMAIL", globalObj.emailsettingobj._dayRestrictionEMAIL);
                        json.put("_timeFromRestrictionEMAIL", globalObj.emailsettingobj._timeFromRestrictionEMAIL);
                        json.put("_timeToRestrictionEMAIL", globalObj.emailsettingobj._timeToRestrictionEMAIL);
                        json.put("_emailFooterStatus", globalObj.emailsettingobj._emailFooterStatus);
                    } catch (Exception ex) {
                        Logger.getLogger(loadglobalsettingv2.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            if (itype == FAX) {
                if (globalObj.faxlimitsettingobj == null) {
                    try {
                        json.put("_faxlimit", 0);
                        json.put("_faxcost", 0);
                        json.put("_faxStatus", 0);
                        json.put("_faxAlertStatus", 0);
                        json.put("_faxRepeatStatus", 1);
//                    json.put("_faxRepeatDuration", 1);
                        json.put("_faxThresholdlimit", 0);
                        json.put("_fastPaceFAX", 15);
                        json.put("_hyperPaceFAX", 35);
                        json.put("_normalPaceFAX", 6);
                        json.put("_slowPaceFAX", 1);
                        json.put("_faxFooter", "Mollatech");
                        json.put("_dayRestrictionFAX", 1);
                        json.put("_timeFromRestrictionFAX", 0);
                        json.put("_timeToRestrictionFAX", 24);
                        json.put("_faxFooterStatus", 0);
                    } catch (Exception ex) {
                        Logger.getLogger(loadglobalsettingv2.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    try {
                        json.put("_faxlimit", globalObj.faxlimitsettingobj.faxlimit);
                        json.put("_faxcost", globalObj.faxlimitsettingobj.faxcost);
                        json.put("_faxStatus", globalObj.faxlimitsettingobj.faxlimitstatus);
                        json.put("_faxAlertStatus", globalObj.faxlimitsettingobj.faxalertstatus);
                        json.put("_faxRepeatStatus", globalObj.faxlimitsettingobj.repeat);
//                    json.put("_faxRepeatDuration", globalObj.faxlimitsettingobj.duration);
                        json.put("_faxThresholdlimit", globalObj.faxlimitsettingobj.thresholdlimit);
                        json.put("_fastPaceFAX", globalObj.faxlimitsettingobj._fastPaceFAX);
                        json.put("_hyperPaceFAX", globalObj.faxlimitsettingobj._hyperPaceFAX);
                        json.put("_normalPaceFAX", globalObj.faxlimitsettingobj._normalPaceFAX);
                        json.put("_slowPaceFAX", globalObj.faxlimitsettingobj._slowPaceFAX);
                        json.put("_faxFooter", globalObj.faxlimitsettingobj._faxFooter);
                        
                        json.put("_dayRestrictionFAX", globalObj.faxlimitsettingobj._dayRestrictionFAX);
                        json.put("_timeFromRestrictionFAX", globalObj.faxlimitsettingobj._timeFromRestrictionFAX);
                        json.put("_timeToRestrictionFAX", globalObj.faxlimitsettingobj._timeToRestrictionFAX);
                        json.put("_faxFooterStatus", globalObj.faxlimitsettingobj._faxFooterStatus);
                    } catch (Exception ex) {
                        Logger.getLogger(loadglobalsettingv2.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }

            if (itype == PUSH) {
                if (globalObj.pushsettingobj == null) {
                    try {
                        json.put("_pushlimit", 0);
                        json.put("_pushcost", 0);
                        json.put("_pushStatus", 0);
                        json.put("_pushAlertStatus", 0);
                        json.put("_pushRepeatStatus", 1);
//                    json.put("_pushRepeatDuration", 1);
                        json.put("_pushThresholdlimit", 0);
                        json.put("_fastPacePUSH", 15);
                        json.put("_hyperPacePUSH", 35);
                        json.put("_normalPacePUSH", 6);
                        json.put("_slowPacePUSH", 1);
                        json.put("_pushFooter", "Mollatech");
                        
                        json.put("_dayRestrictionPUSH", 1);
                        json.put("_timeFromRestrictionPUSH", 0);
                        json.put("_timeToRestrictionPUSH", 24);
                        json.put("_pushFooterStatus", 0);
                    } catch (Exception ex) {
                        Logger.getLogger(loadglobalsettingv2.class.getName()).log(Level.SEVERE, null, ex);
                    }

                } else {
                    try {
                        json.put("_pushlimit", globalObj.pushsettingobj.pushlimit);
                        json.put("_pushcost", globalObj.pushsettingobj.pushcost);
                        json.put("_pushStatus", globalObj.pushsettingobj.pushlimitstatus);
                        json.put("_pushAlertStatus", globalObj.pushsettingobj.pushalertstatus);
                        json.put("_pushRepeatStatus", globalObj.pushsettingobj.repeat);
//                    json.put("_pushRepeatDuration", globalObj.pushsettingobj.duration);
                        json.put("_pushThresholdlimit", globalObj.pushsettingobj.thresholdlimit);
                        json.put("_fastPacePUSH", globalObj.pushsettingobj._fastPacePUSH);
                        json.put("_hyperPacePUSH", globalObj.pushsettingobj._hyperPacePUSH);
                        json.put("_normalPacePUSH", globalObj.pushsettingobj._normalPacePUSH);
                        json.put("_slowPacePUSH", globalObj.pushsettingobj._slowPacePUSH);
                        json.put("_pushFooter", globalObj.pushsettingobj._pushFooter);
                        
                        json.put("_dayRestrictionPUSH", globalObj.pushsettingobj._dayRestrictionPUSH);
                        json.put("_timeFromRestrictionPUSH", globalObj.pushsettingobj._timeFromRestrictionPUSH);
                        json.put("_timeToRestrictionPUSH", globalObj.pushsettingobj._timeToRestrictionPUSH);
                        json.put("_pushFooterStatus", globalObj.pushsettingobj._pushFooterStatus);
                    } catch (Exception ex) {
                        Logger.getLogger(loadglobalsettingv2.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            if(itype == CR_SETTINGS){
                if(globalObj.weightageForCR == 0){
                    try { json.put("_weigtage",75);                    
                    }catch(Exception e){log.error("Exception caught :: ",e);}
//                    json.put("_answersattempts",3);
                }else{
                    try {json.put("_weigtage",globalObj.weightageForCR);
                    }catch(Exception e){log.error("Exception caught :: ",e);} 
//                    json.put("_answersattempts",globalObj.answersattempts);
                }
                  if(globalObj.answersattempts == 0){
//                    json.put("_weigtage",75);
                    try { json.put("_answersattempts",3);
                    }catch(Exception e){log.error("Exception caught :: ",e);}
                    
                }else{
//                    json.put("_weigtage",globalObj.weightageForCR);
                    try { json.put("_answersattempts",globalObj.answersattempts);
                    }catch(Exception e){log.error("Exception caught :: ",e);}
                }
            }
        }
        return json;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        response.setContentType("application/json");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        String _type = request.getParameter("_type");
        int itype = Integer.parseInt(_type);
        
        log.debug("channel is::"+channel.getName());
        log.debug("sessionid::"+sessionId);
        log.debug("type is::"+itype);

        JSONObject json = null;
        PrintWriter out = response.getWriter();

        try {
            SettingsManagement sMngmt = new SettingsManagement();
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), sMngmt.GlobalSettings, sMngmt.PREFERENCE_ONE);
            if (settingsObj != null) {
                json = SettingsWhenPresent(itype, settingsObj);
            } else {
                json = SettingsWhenEmpty(sMngmt.GlobalSettings);
            }

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
            // TODO handle custom exceptions here
        }
        try {
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
