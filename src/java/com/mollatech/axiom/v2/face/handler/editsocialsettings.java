/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.OOBSocialChannelSettings;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class editsocialsettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editsocialsettings.class.getName());

    public static final int SOCIAL = 17;
    public static final int ACTIVE_STATUS = 1;
    public static final int SUSPENDED_STATUS = 1;
    public static final int PREFERENCE_ONE = 1;   //primary
    public static final int PREFERENCE_TWO = 2;
    final String itemtype = "SETTINGS";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.SOCIAL_MESSAGING) != 0) {
            String result = "error";
            String message = "This feature is not available in this license!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        String OperatorID = operatorS.getOperatorid();
        String _channelId = channel.getChannelid();
        SettingsManagement sMngmt = new SettingsManagement();
        int retValue = -1;
        
            log.debug("channel :: " + channel.getName());
            log.debug("getChannelid :: " + channel.getChannelid());
            log.debug("Operator :: " + operatorS.getOperatorid());
            log.debug("SessionId :: " + sessionId);
            log.debug("remoteaccesslogin :: " + remoteaccesslogin);

        String result = "success";
        String message = "Social Gateway Settings Update Successful!!!";
        String _ip = null;
        String _port = null;
        String _facebookappid = null;
        String _facebookappsecret = null;
        String _linkedinkey = null;
        String _linkedinsecret = null;
        String _linkedintoken = null;
        String _linkedinaccesssecret = null;
        String _twitterkey = null;
        String _twittersecret = null;
//        String _googlekey = null;
        Object reserve1 = null;
        Object reserve2 = null;
        Object reserve3 = null;
        String _status = null;
        String _retrycount = null;
        String _retryduration = null;
        String _messgeLength = null;

        String _preference = request.getParameter("_perference");
        int _iPreference = Integer.parseInt(_preference);
        log.debug("_iPreference :: " + _iPreference);

        //request parameter for mobile primary
        String _type1 = request.getParameter("_type");
        int _type = Integer.parseInt(_type1);
        log.debug("_type :: " + _type);
        if (_type == SOCIAL) {
            if (_iPreference == PREFERENCE_ONE) {   //primary
                _ip = request.getParameter("_ip");
                log.debug("_ip :: "+_ip);
                _port = request.getParameter("_port");
                log.debug("_port :: "+_port);
                _facebookappid = request.getParameter("_facebookappid");
                log.debug("_facebookappid :: "+_facebookappid);
                _facebookappsecret = request.getParameter("_facebookappsecret");
                log.debug("_facebookappsecret :: "+_facebookappsecret);
                _linkedinkey = request.getParameter("_linkedinkey");
                log.debug("_linkedinkey :: "+_linkedinkey);
                _linkedinsecret = request.getParameter("_linkedinsecret");
                log.debug("_linkedinsecret :: "+_linkedinsecret);
                _linkedintoken = request.getParameter("_linkedintoken");
                log.debug("_linkedintoken :: "+_linkedintoken);
                _linkedinaccesssecret = request.getParameter("_linkedinaccesssecret");
                log.debug("_linkedinaccesssecret :: "+_linkedinaccesssecret);
                _twitterkey = request.getParameter("_twitterkey");
                log.debug("_twitterkey :: "+_twitterkey);
                _twittersecret = request.getParameter("_twittersecret");
                log.debug("_twittersecret :: "+_twittersecret);
//                _googlekey = request.getParameter("_googlekey");
                reserve1 = request.getParameter("_reserve1");
                log.debug("reserve1 :: "+reserve1);
                reserve2 = request.getParameter("_reserve2");
                log.debug("reserve2 :: "+reserve2);
                reserve3 = request.getParameter("_reserve3");
                log.debug("reserve3 :: "+reserve3);
                _status = request.getParameter("_status");
                log.debug("_status :: "+_status);
                _retrycount = request.getParameter("_retries");
                log.debug("_retrycount :: "+_retrycount);
                _retryduration = request.getParameter("_retryduration");
                log.debug("_retryduration :: "+_retryduration);
            } else if (_iPreference == PREFERENCE_TWO) {   //secondary
                //request parameter for mobile secondary
                _ip = request.getParameter("_ip");
                log.debug("_ip :: "+_ip);
                _port = request.getParameter("_port");
                log.debug("_port :: "+_port);
                _facebookappid = request.getParameter("_facebookappid");
                log.debug("_facebookappid :: "+_facebookappid);
                _facebookappsecret = request.getParameter("_facebookappsecret");
                log.debug("_facebookappsecret :: "+_facebookappsecret);
                _linkedinkey = request.getParameter("_linkedinkey");
                log.debug("_linkedinkey :: "+_linkedinkey);
                _linkedinsecret = request.getParameter("_linkedinsecret");
                log.debug("_linkedinsecret :: "+_linkedinsecret);
                _linkedintoken = request.getParameter("_linkedintoken");
                log.debug("_linkedintoken :: "+_linkedintoken);
                _linkedinaccesssecret = request.getParameter("_linkedinaccesssecret");
                log.debug("_linkedinaccesssecret :: "+_linkedinaccesssecret);
                _twitterkey = request.getParameter("_twitterkey");
                log.debug("_twitterkey :: "+_twitterkey);
                _twittersecret = request.getParameter("_twittersecret");
//                _googlekey = request.getParameter("_googlekey");
                    log.debug("_twittersecret :: "+_twittersecret);
                reserve1 = request.getParameter("_reserve1");
                log.debug("reserve1 :: "+reserve1);
                reserve2 = request.getParameter("_reserve2");
                log.debug("reserve2 :: "+reserve2);
                reserve3 = request.getParameter("_reserve3");
                log.debug("reserve3 :: "+reserve3);
                _status = request.getParameter("_status");
                log.debug("_status :: "+_status);
                _retrycount = request.getParameter("_retries");
                log.debug("_retrycount :: "+_retrycount);
                _retryduration = request.getParameter("_retryduration");
                log.debug("_retryduration :: "+_retryduration);
            }

            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _type, _iPreference);

            OOBSocialChannelSettings social = null;
            boolean bAddSetting = false;

            if (settingsObj == null) {
                social = new OOBSocialChannelSettings();
                social.setChannelId(_channelId);
                social.setType(SOCIAL);
                social.setPreference(_iPreference);
                bAddSetting = true;
            } else {
                social = (OOBSocialChannelSettings) settingsObj;
            }

            if (_port != null || _port.isEmpty() == true) {
                int _port1 = Integer.parseInt(_port);
                social.setPort(_port1);
            }
            social.setIp(_ip);
            social.setReserve1(reserve1);
            social.setReserve2(reserve2);
            social.setReserve3(reserve3);
            social.setFacebookappid(_facebookappid);
            social.setFacebookappsecret(_facebookappsecret);
            social.setLinkedinkey(_linkedinkey);
            social.setLinkedinsecret(_linkedinsecret);
            social.setLinkedintoken(_linkedintoken);
            social.setLinkedinaccesssecret(_linkedinaccesssecret);
            social.setTwitterkey(_twitterkey);
            social.setTwittersecret(_twittersecret);
//            social.setGooglekey(_googlekey);
            if (_status != null) {
                int _status1 = Integer.parseInt(_status);
                social.setStatus(_status1);
            }
            if (_retryduration != null) {
                int _iRetryDuration = Integer.parseInt(_retryduration);
                social.setRetryduration(_iRetryDuration);
            }

            if (_retrycount != null) {
                int _iRetrycount = Integer.parseInt(_retrycount);
                social.setRetrycount(_iRetrycount);
            }

            AuditManagement audit = new AuditManagement();
            if (bAddSetting == true) {
                retValue = sMngmt.addSetting(sessionId, _channelId, _type, _iPreference, social);
                log.debug("editsocialsettings::addSetting::" + retValue);

                String resultString = "ERROR";
                if (retValue == 0) {
                    resultString = "SUCCESS";
//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
//                            request.getRemoteAddr(),
//                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
//                            "Add SMS Setting", resultString, retValue, "Setting Management",
//                            "", "ChannelID=" + social.getChannelId() + "Class Name=" + social.getClassName()
//                            + "IP=" + mobile.getIp() + "Password=***" + "Phone Number=" + mobile.getPhoneNumber() + "User ID=" + mobile.getUserid()
//                            + "Autofailover=" + mobile.getAutofailover() + "Port=" + mobile.getPort() + "Prefernece=" + mobile.getPreference()
//                            + "Reserve1=" + mobile.getReserve1() + "Reserve2" + mobile.getReserve2() + "Reserve3=" + mobile.getReserve3()
//                            + "RetryCount=" + mobile.getRetrycount() + "Status=" + mobile.getStatus() + "Type=" + mobile.getType()
//                            + "LogConfirmation=" + mobile.isLogConfirmation() + "Retry Duration" + mobile.getRetryduration(),
//                            itemtype, OperatorID);
                } else if (retValue != 0) {
//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
//                            request.getRemoteAddr(),
//                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
//                            "Add SMS Setting", resultString, retValue, "Setting Management",
//                            "", "Failed To add SMS Setting ...!", itemtype,
//                            OperatorID);
                }
            } else {
                OOBSocialChannelSettings oldsocialObj = (OOBSocialChannelSettings) sMngmt.getSetting(sessionId, _channelId, _type, _iPreference);
                retValue = sMngmt.changeSetting(sessionId, _channelId, _type, _iPreference, settingsObj, social);
                log.debug("retValue :: " + retValue);

                String resultString = "ERROR";

                if (retValue == 0) {
                    resultString = "SUCCESS";
//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
//                            request.getRemoteAddr(),
//                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
//                            "Change SMS Setting", resultString, retValue, "Setting Management",
//                            "ChannelID=" + oldmobileObj.getChannelId() + " Class Name=" + oldmobileObj.getClassName() + "ImplementationJar="
//                            + " IP=" + oldmobileObj.getIp() + " Password=***" + " Phone Number=" + oldmobileObj.getPhoneNumber() + " User ID=" + oldmobileObj.getUserid()
//                            + " Autofailover=" + oldmobileObj.getAutofailover() + " Port=" + oldmobileObj.getPort() + " Prefernece=" + oldmobileObj.getPreference()
//                            + " Reserve1=" + oldmobileObj.getReserve1() + " Reserve2" + oldmobileObj.getReserve2() + " Reserve3=" + oldmobileObj.getReserve3()
//                            + " RetryCount=" + oldmobileObj.getRetrycount() + " Status=" + oldmobileObj.getStatus() + " Type=" + oldmobileObj.getType()
//                            + " LogConfirmation=" + oldmobileObj.isLogConfirmation() + " Retry Duration" + oldmobileObj.getRetryduration(),
//                            " ChannelID=" + mobile.getChannelId() + " Class Name=" + mobile.getClassName() + " ImplementationJar="
//                            + " IP=" + mobile.getIp() + " Password=***" + " Phone Number=" + mobile.getPhoneNumber() + " User ID=" + mobile.getUserid()
//                            + " Autofailover=" + mobile.getAutofailover() + " Port=" + mobile.getPort() + " Prefernece=" + mobile.getPreference()
//                            + " Reserve1=" + mobile.getReserve1() + " Reserve2" + mobile.getReserve2() + " Reserve3=" + mobile.getReserve3()
//                            + " RetryCount=" + mobile.getRetrycount() + " Status=" + mobile.getStatus() + " Type=" + mobile.getType()
//                            + " LogConfirmation=" + mobile.isLogConfirmation() + " Retry Duration" + mobile.getRetryduration(),
//                            itemtype, OperatorID);
                } else if (retValue != 0) {
//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
//                            request.getRemoteAddr(),
//                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
//                            "Change SMS Setting", resultString, retValue, "Setting Management",
//                            "ChannelID=" + oldmobileObj.getChannelId() + " Class Name=" + oldmobileObj.getClassName() + "ImplementationJar="
//                            + " IP=" + oldmobileObj.getIp() + " Password=***" + " Phone Number=" + oldmobileObj.getPhoneNumber() + " User ID=" + oldmobileObj.getUserid()
//                            + " Autofailover=" + oldmobileObj.getAutofailover() + " Port=" + oldmobileObj.getPort() + " Prefernece=" + oldmobileObj.getPreference()
//                            + " Reserve1=" + oldmobileObj.getReserve1() + " Reserve2" + oldmobileObj.getReserve2() + " Reserve3=" + oldmobileObj.getReserve3()
//                            + " RetryCount=" + oldmobileObj.getRetrycount() + " Status=" + oldmobileObj.getStatus() + " Type=" + oldmobileObj.getType()
//                            + " LogConfirmation=" + oldmobileObj.isLogConfirmation() + " Retry Duration" + oldmobileObj.getRetryduration(),
//                            "Failed To Edit Setting...!!!", itemtype, OperatorID);
                }

            }
        }

        if (retValue != 0) {
            result = "error";
            message = "Social Gateway Settings Update Failed!!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
