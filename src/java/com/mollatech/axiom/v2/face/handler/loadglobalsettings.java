/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
import com.mollatech.axiom.nucleus.settings.PINDeliverySetting;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech1
 */
public class loadglobalsettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadglobalsettings.class.getName());

    private JSONObject SettingsWhenEmpty(int _type1) {
        JSONObject json = new JSONObject();
        int iglobal = SettingsManagement.GlobalSettings;

        if (_type1 == iglobal) {
            try { json.put("_slowPaceMobile", 1);
            json.put("_normalPaceMobile", 6);
            json.put("_fastPaceMobile", 15);
            json.put("_hyperPaceMobile", 35);
            json.put("_slowPaceEmail", 1);
            json.put("_normalPaceEmail", 6);
            json.put("_fastPaceEmail", 15);
            json.put("_hyperPaceEmail", 35);
            json.put("_FXListner", 5555);
            json.put("_vduration", 30);
            json.put("_voiceSetting", 1);
            json.put("_weigtage",75);
             json.put("_answersattempts",3);
             }catch(Exception e){log.error("Exception caught :: ",e);}
            
        }
        return json;
    }

    private JSONObject SettingsWhenPresent(Object settingsObj) {
        JSONObject json = new JSONObject();
        if (settingsObj instanceof GlobalChannelSettings) {
            GlobalChannelSettings globalObj = (GlobalChannelSettings) settingsObj;
            try { json.put("_slowPaceMobile", globalObj._slowPaceMobile);
            json.put("_normalPaceMobile", globalObj._normalPaceMobile);
            json.put("_fastPaceMobile", globalObj._fastPaceMobile);
            json.put("_hyperPaceMobile", globalObj._hyperPaceMobile);
            json.put("_slowPaceEmail", globalObj._slowPaceEmail);
            json.put("_normalPaceEmail", globalObj._normalPaceEmail);
            json.put("_fastPaceEmail", globalObj._fastPaceEmail);
            json.put("_hyperPaceEmail", globalObj._hyperPaceEmail);
            json.put("_FXListner", globalObj._FXListner);
            json.put("_vduration", globalObj._vduration);
            json.put("_voiceSetting", globalObj._voiceSetting);
             json.put("_weigtage",globalObj.weightageForCR);
            json.put("_answersattempts",globalObj.answersattempts);
            }catch(Exception e){}


        }
        return json;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("loadglobalsettings::channel is::"+channel.getName());
        log.debug("loadglobalsettings::sessionid::"+sessionId);
        JSONObject json = null;
        PrintWriter out = response.getWriter();

        try {
            SettingsManagement sMngmt = new SettingsManagement();
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), sMngmt.GlobalSettings, sMngmt.PREFERENCE_ONE);
            if (settingsObj != null) {
                json = SettingsWhenPresent(settingsObj);
            } else {
                json = SettingsWhenEmpty(sMngmt.GlobalSettings);
            }

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
            // TODO handle custom exceptions here
        }
        try {
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
