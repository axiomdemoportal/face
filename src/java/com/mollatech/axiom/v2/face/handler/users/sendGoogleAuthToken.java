/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.users;

import static com.google.common.io.BaseEncoding.base32;
import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mollatech.axiom.nucleus.db.Otptokens;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import static com.mollatech.axiom.v2.face.handler.users.setandresenduserpassword.log;
import org.json.JSONObject;
import com.mollatech.axiom.mobiletrust.crypto.CryptoManager;
import static com.mollatech.axiom.nucleus.crypto.HWSignature.asHex;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import static com.mollatech.axiom.nucleus.crypto.LoadSettings.g_sSettings;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.hibernate.Session;
import qrcodegenerator.QrcodeGenerator;

/**
 *
 * @author Pramod
 */
public class sendGoogleAuthToken extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: " + channel.getName());
        String sessionid = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: " + sessionid);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: " + remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: " + operatorS.getOperatorid());
        OTPTokenManagement otp = new OTPTokenManagement(channel.getChannelid());
        final String itemtype = "GoogleTokenQR";

        String userid = request.getParameter("_userid");
        log.debug("_userid :: " + userid);
        String result = "success";
        String message = "QR Code sent successfully, please check your email....";
        // String strPassword = null;
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        if (userid == null) {
            result = "error";
            message = "Fill all Details!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ", e);
            }
            out.print(json);
            out.flush();
            return;
        }
        Otptokens details = otp.getOtpObjByUserId(sessionid, channel.getChannelid(), userid, 1);
        String secret = details.getSecret();
        System.out.println("Secret>> " + secret);

        byte[] secret1 = CryptoManager.hexStringToByteArray(secret);

        //  byte[] decoded = org.apache.commons.codec.binary.Base64.decodeBase64(secret.getBytes());
        String secrestforQR = base32().encode(secret1);
        String url = "otpauth://totp/" + channel.getName() + ":" + userid + "?secret=" + secrestforQR + "&issuer=" + channel.getName();
        System.out.println("url is :" + url);
        QrcodeGenerator Qrimage = new QrcodeGenerator();
        String image = Qrimage.CreateQRCode(url);

        UserManagement userobj = new UserManagement();
        AuthUser userDetails = userobj.getUser(channel.getChannelid(), userid);
        SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
        Session sTemplate = suTemplate.openSession();
        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
        Templates templatesObj = tUtil.loadbyName(channel.getChannelid(), TemplateNames.EMAIL_GOOGLE_AUTH_TEMPLATE);
        //    OperatorsManagement oManagement = new OperatorsManagement();

        if (templatesObj.getStatus() == 1) {
            ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
            String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
            String strsubject = templatesObj.getSubject();
            String ip = null;
            String port = null;
            String https = null;
            String http = "http";
            try {

                if (g_sSettings.getProperty("googauthenticator.ipaddress") != null && g_sSettings.getProperty("googauthenticator.secured") != null && g_sSettings.getProperty("googauthenticator.port") != null) {
                    ip = g_sSettings.getProperty("googauthenticator.ipaddress");
                    http = g_sSettings.getProperty("googauthenticator.secured");
                    port = g_sSettings.getProperty("googauthenticator.port");

                    if (http.equals("yes")) {
                        https = "https";
                    } else {
                        https = "http";
                    }
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            String password = "E1BB465D57CAE7ACDBBE8091F9CE83DF";

            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey = cmObj.generateKeyAES(password.getBytes(), 128);
            byte[] byteEncryptedData = cmObj.encryptAES(aeskey, userid.getBytes());

            String QRurl = https + "://" + ip + ":" + port + "/face/GoogleAuthQRImage.jsp?userid=" + asHex(byteEncryptedData);
            if (ip != null && port != null) {
                QRurl = https + "://" + ip + ":" + port + "/face/GoogleAuthQRImage.jsp?userid=" + asHex(byteEncryptedData);
            }
            System.out.println("QRurl" + QRurl);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            if (strmessageBody != null) {
                strmessageBody = strmessageBody.replaceAll("#name#", userDetails.getUserName());
                strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                strmessageBody = strmessageBody.replaceAll("#email#", userDetails.getEmail());
                strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                strmessageBody = strmessageBody.replaceAll("#image#", QRurl);
                strmessageBody = strmessageBody + "\n\n<br/><img src=\"data:image/png;base64," + image + "\"alt=\" Encoded Image\"/>";
                System.out.println("Base64 Image   data:image/png;base64" + image);
            }
            SendNotification send = new SendNotification();
            AXIOMStatus status = send.SendEmail(channel.getChannelid(), userDetails.getEmail(), strsubject, strmessageBody, null, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
            if (status != null) {
                if (status.iStatus == SendNotification.SENT) {
                    result = "success";
                    message = "QR Code sent successfully, please check your email....";

                    AuditManagement ad = new AuditManagement();
                    ad.AddAuditTrail(sessionid, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), "send QRCode", "SUCCESS", status.iStatus,
                            "User Management",
                            "",
                            "google QRCode sent successfully",
                            itemtype,
                            userid);
                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("exception caught :: ", e);
                    }
                    out.print(json);
                    out.flush();
                    return;

                } else if (status.iStatus == SendNotification.PENDING) {

                    AuditManagement ad = new AuditManagement();
                    ad.AddAuditTrail(sessionid, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), "send QRCode", "SUCCESS", status.iStatus,
                            "User Management",
                            "",
                            "google QRCode sent to respective gateway",
                            itemtype,
                            userid);
                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("exception caught :: ", e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                } else if (status.iStatus != SendNotification.PENDING) {

                    AuditManagement ad = new AuditManagement();
                    ad.AddAuditTrail(sessionid, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), "Send QRCode", "ERROR", status.iStatus,
                            "User Management",
                            "",
                            "Failed To Send google QRCode" + status.iStatus,
                            itemtype,
                            userid);

//            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(),
//                    channel.getName(),
//                    remoteaccesslogin, operatorS.getName(), new Date(),
//                    "Resend Password", "Failure", status.iStatus,
//                    "User Management", "", "Failed To Send Password" + status.iStatus,
//                    itemtype, _userid);
                    result = "error";
                    message = "Qr Code Not Sent!!";
                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("exception caught :: ", e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
            } else {
                result = "error";
                message = "Qr Code Not Sent!!";

                try {
                    json.put("_result", result);
                    json.put("_message", message);

                } catch (Exception e) {
                    log.error("exception caught :: ", e);
                } finally {
                    out.print(json);
                    out.flush();
                }
                log.info("send Google AuthToken is ended :: ");

            }

        } else {
            result = "success";
            message = "users software Token status is not active";
            try {
                json.put("_result", result);
                json.put("_message", message);

            } catch (Exception e) {
                log.error("exception caught :: ", e);
            } finally {
                out.print(json);
                out.flush();
            }
            log.info("is ended :: ");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
