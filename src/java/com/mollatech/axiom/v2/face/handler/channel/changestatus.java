package com.mollatech.axiom.v2.face.handler.channel;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class changestatus extends HttpServlet {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(changestatus.class.getName());
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;
    final String itemType = "CHANNEL";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
         log.debug("sessionId :: "+sessionId);

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String operatorId = operatorS.getOperatorid();
        log.debug("operatorId :: "+operatorId);

        String _ch_status = request.getParameter("_ch_status");
        log.debug("changestatus::status is"+_ch_status);
        String _ch_channelid_for_change = request.getParameter("_channelid");
        log.debug("_ch_channelid_for_change :: "+_ch_channelid_for_change);
        int status = Integer.parseInt(_ch_status);
        
  
        String result = "success";
        String message = "Status Updated Successfully....";
        String _value = "Active";
        if (status == 0) {
            _value = "Suspended";
        }
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
       
        if (_ch_status == null || _ch_channelid_for_change == null) {
            result = "error";
            message = "Status not updated!!";
            try { 
            json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);
            }catch(Exception e){
                log.error("exception caught ::",e);
            
        }
            out.print(json);
            out.flush();
            return;
        }

        int retValue = -1;

        try {
            ChannelManagement cManagement = new ChannelManagement();
            AuditManagement audit = new AuditManagement();
            Channels ch = cManagement.getChannelByID(sessionId, _ch_channelid_for_change);
            int istatus = ch.getStatus();
            String strStatus = "";
            if (istatus == ACTIVE_STATUS) {
                strStatus = "ACTIVE_STATUS";
            } else if (istatus != SUSPEND_STATUS) {
                strStatus = "SUSPEND_STATUS";
            }

            String strNewStatus = "";
            if (status == ACTIVE_STATUS) {
                strNewStatus = "ACTIVE_STATUS";
            } else if (status != SUSPEND_STATUS) {
                strNewStatus = "SUSPEND_STATUS";
            }

            retValue = cManagement.ChangeStatus(sessionId, _ch_channelid_for_change, status);
            log.debug("ChangeStatus::"+retValue);
            String resultStr = "ERROR";
            if (retValue == 0) {
                resultStr = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorId,
                        request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operatorS.getName(), new Date(), 
                        "Change Channel Status...!!!", resultStr, retValue, 
                        "ChannelManagement", "Old Status =" + strStatus, 
                        "New Status =" + strNewStatus,
                        itemType, _ch_channelid_for_change);
            } else if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorId,
                        request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operatorS.getName(), new Date(), "Change Channel Status...!!!", resultStr, 
                        retValue, "ChannelManagement", "Old Status =" + strStatus, 
                        "Failed To Change Status",
                        itemType, _ch_channelid_for_change);
            }


        } catch (Exception ex) {
            // TODO handle custom exceptions here
            log.error("exception caught :: ",ex);
            
        }


        if (retValue != 0) {
            result = "error";
            message = "Error: Status Not Updated!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);
        } catch(Exception e){
            log.error("exception caught ::",e);
            
        }finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
