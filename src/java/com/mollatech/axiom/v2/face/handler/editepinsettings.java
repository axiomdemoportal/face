package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.PINDeliverySetting;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class editepinsettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editepinsettings.class.getName());

    //Static variables
    //public static final int SMS = 1;
    public static final int ACTIVE_STATUS = 1;
    public static final int SUSPENDED_STATUS = 1;
    public static final int PREFERENCE_ONE = 1;   //primary
    public static final int PREFERENCE_TWO = 2;
    final String itemtype = "SETTINGS";
//    private void PrintRequestParameters(HttpServletRequest req) {
//        Enumeration<String> paramNames = req.getParameterNames();
//        while (paramNames.hasMoreElements()) {
//            String paramName = paramNames.nextElement();
//            System.out.println(paramName + "=" + req.getParameter(paramName));
//        }
//    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        //PrintRequestParameters(request);
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String OperatorID = operatorS.getOperatorid();

        String _channelId = channel.getChannelid();
        SettingsManagement sMngmt = new SettingsManagement();
        int retValue = -1;

        String result = "success";
        String message = "EPIN Settings Update Successful!!!";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        
            log.debug("channel ::" + channel.getName());
            log.debug("getOperatorid ::" + operatorS.getOperatorid());
            log.debug("sessionId ::" + sessionId);
            log.debug("remoteaccesslogin ::" + remoteaccesslogin);
            log.debug("getChannelid ::" + channel.getChannelid());
       
        
        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.EPIN) != 0) {
            result = "error";
            message = "This feature is not available in this license!!!";

            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
               log.error("Exception caught :: ",e);
            }

            out.print(json);
            out.flush();
            return;
        }

        int pinDeliveryType = Integer.valueOf(request.getParameter("_pinDeliveryType")).intValue();
        int channelType = Integer.valueOf(request.getParameter("_channelType")).intValue();
        int channelType2 = Integer.valueOf(request.getParameter("_channelType2")).intValue();
        int duration = Integer.valueOf(request.getParameter("_splitduration")).intValue();
        int dayRestriction = Integer.valueOf(request.getParameter("_dayRestriction")).intValue();
        int timeFromInHour = Integer.valueOf(request.getParameter("_timeFromRestriction")).intValue();
        int timeToInHour = Integer.valueOf(request.getParameter("_timeToRestriction")).intValue();
        int pinRequestCountDuration = Integer.valueOf(request.getParameter("_pinRequestCountDuration")).intValue();
        int pinRequestCount = Integer.valueOf(request.getParameter("_pinRequestCount")).intValue();
        int operatorController = Integer.valueOf(request.getParameter("_operatorController")).intValue();
        int expiryTime = Integer.valueOf(request.getParameter("_expiryTime")).intValue();
        boolean isAlertOperatorOnFailure = Boolean.valueOf(request.getParameter("_isAlertOperatorOnFailure")).booleanValue();
        String smstext = request.getParameter("_smstext");
        log.debug("smstext :: " + smstext);
        //recently changed 2/5/14
        int PINSource = Integer.valueOf(request.getParameter("_PINSource")).intValue();
        String PinSourceClassNameclassname = request.getParameter("_PinSourceClassNameclassname");
        int ChallengeResponse = Integer.valueOf(request.getParameter("_ChallengeResponse")).intValue();
        String ChallengeResponseclassname = request.getParameter("_ChallengeResponseclassname");
        int smsurlStatus = Integer.valueOf(request.getParameter("_smsurlStatus")).intValue();
        String smsurl = request.getParameter("_smsurl");
        int voiceurlStatus = Integer.valueOf(request.getParameter("_voiceurlStatus")).intValue();
        String voiceurl = request.getParameter("_voiceurl");
        int ussdurlStatus = Integer.valueOf(request.getParameter("_ussdurlStatus")).intValue();
        String ussdurl = request.getParameter("_ussdurl");
        int weburlStatus = Integer.valueOf(request.getParameter("_weburlStatus")).intValue();
        String webeurl = request.getParameter("_weburl");
        
        log.debug("PinSourceClassNameclassname ::" + PinSourceClassNameclassname);
        log.debug("ChallengeResponseclassname ::" + ChallengeResponseclassname);
        log.debug("smsurl ::" + smsurl);
        log.debug("voiceurl ::" + voiceurl);
        log.debug("ussdurl :: " + ussdurl);
        log.debug("webeurl :: " + webeurl);

        int iType = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.Epin;
        int iPreference = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE;
        if(ChallengeResponseclassname == null || ChallengeResponseclassname.isEmpty() == true){
            ChallengeResponseclassname = "com.mollatech.internal.handler.epin.cr.EpinCRValidationInterfaceImpl";
        }
        if(PinSourceClassNameclassname == null|| PinSourceClassNameclassname.isEmpty() == true){
            PinSourceClassNameclassname = "external.handler.pin.generation.EPINGenerator";
        }
         
        
        String strType = String.valueOf(iType);

        Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), iType, iPreference);

        PINDeliverySetting epinObj = null;
        boolean bAddSetting = false;

        if (settingsObj == null) {
            epinObj = new PINDeliverySetting();
            bAddSetting = true;
        } else {
            epinObj = (PINDeliverySetting) settingsObj;
        }

        epinObj.pinDeliveryType = pinDeliveryType;
        epinObj.channelType = channelType;
        epinObj.channelType2 = channelType2;
        epinObj.duration = duration;
        epinObj.dayRestriction = dayRestriction;
        epinObj.timeFromInHour = timeFromInHour;
        epinObj.timeToInHour = timeToInHour;
        epinObj.pinRequestCountDuration = pinRequestCountDuration;
        epinObj.pinRequestCount = pinRequestCount;
        epinObj.operatorController = operatorController;
        epinObj.expiryTime = expiryTime;
        epinObj.isAlertOperatorOnFailure = isAlertOperatorOnFailure;
        epinObj.PINSource = PINSource;
        epinObj.classname = PinSourceClassNameclassname;
        epinObj.challangeResponseSource = ChallengeResponse;
        epinObj.challangeResponseClassName = ChallengeResponseclassname;
        epinObj.isSMSEnable = smsurlStatus;
        epinObj.smsUrl = smsurl;
        epinObj.isVoiceEnable = voiceurlStatus;
        epinObj.voiceUrl = voiceurl;
        epinObj.isUSSDEnable = ussdurlStatus;
        epinObj.ussdUrl = ussdurl;
        epinObj.isWebEnable = weburlStatus;
        epinObj.webUrl = webeurl;
        epinObj.smsText = smstext;
        String strPINSource = "External Source";
        if(PINSource == 1){
            strPINSource = "Internally by Axiom";
        }
         String strChallengeResponse = "External Source";
        if(ChallengeResponse == 1){
            strChallengeResponse = "Internally by Axiom";
        }
         String strsmsurlStatus = "In-Active";
        if(smsurlStatus == 1){
            strsmsurlStatus = "Active";
        }
         String strvoiceurlStatus = "In-Active";
        if(voiceurlStatus == 1){
            strvoiceurlStatus = "Active";
        }
         String strussdurlStatus = "In-Active";
        if(ussdurlStatus == 1){
            strussdurlStatus = "Active";
        }
        String strweburlStatus = "In-Active";
        if(weburlStatus == 1){
            strweburlStatus = "Active";
        }

        AuditManagement audit = new AuditManagement();
        if (bAddSetting == true) {
            retValue = sMngmt.addSetting(sessionId, _channelId, iType, iPreference, epinObj);
            log.debug("editepinsettings::addSetting::" + retValue);
            String resultString = "ERROR";
            if (retValue == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add EPIN Settings", resultString, retValue, "Setting Management",
                        "", "PinDeliveryType=" + epinObj.pinDeliveryType + "ChannelType=" + epinObj.channelType + "ChannelType2="
                        + epinObj.channelType2 + "Duration=" + epinObj.duration + "DayRestriction=" + epinObj.dayRestriction
                        + "TimeFromInHour=" + epinObj.timeFromInHour + "TimeToInHour" + epinObj.timeToInHour + "PinRequestCountDuration"
                        + epinObj.pinRequestCountDuration + "PinRequestCount" + epinObj.pinRequestCount + "OperatorController" + epinObj.operatorController
                        + "ExpiryTime" + epinObj.expiryTime + "IsAlertOperatorOnFailure" + epinObj.isAlertOperatorOnFailure
                        +  "smsText" + epinObj.smsText 
                        + ",PINSource"+strPINSource+",Pin Source Class Name ="+PinSourceClassNameclassname
                        + "Challenge Response ="+strChallengeResponse+", ChallegeResponse Class Name="+ChallengeResponseclassname
                        +", SMS Url ="+smsurl+ ", Sms Url Alert Status ="+strsmsurlStatus
                        +", VOICE Url ="+voiceurl+", Voice url alert Status ="+strvoiceurlStatus
                        +", USSD Url ="+voiceurl+", Voice url alert Status ="+strussdurlStatus
                        +", WEB Url ="+webeurl+", Voice url alert Status ="+strweburlStatus,
                        //  +"PINLength"+epinObj.PINLength+"PINValidity"+epinObj.PINValidity+"Classname"+ epinObj.classname+"smsText"+ epinObj.smsText+"smsUrl"+ epinObj.smsUrl+"voiceUrl"+ epinObj.voiceUrl,
                        itemtype, _channelId);
            } else if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add EPIN Setting", resultString, retValue, "Setting Management",
                        "", "Failed to Add EPIN Settings",
                        itemtype, OperatorID);
            }

        } else {
            PINDeliverySetting oldpinObj = (PINDeliverySetting) sMngmt.getSetting(sessionId, _channelId, iType, iPreference);
            retValue = sMngmt.changeSetting(sessionId, _channelId, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.Epin, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE, settingsObj, epinObj);
            log.debug("editepinsettings::changeSetting::" + retValue);
            String stroldPINSource = "External Source";
        if(oldpinObj.PINSource == 1){
            strPINSource = "Internally by Axiom";
        }
         String stroldChallengeResponse = "External Source";
        if(oldpinObj.challangeResponseSource == 1){
            strChallengeResponse = "Internally by Axiom";
        }
         String stroldsmsurlStatus = "In-Active";
        if(oldpinObj.isSMSEnable == 1){
            strsmsurlStatus = "Active";
        }
         String stroldvoiceurlStatus = "In-Active";
        if(oldpinObj.isVoiceEnable == 1){
            strvoiceurlStatus = "Active";
        }
         String stroldussdurlStatus = "In-Active";
        if(oldpinObj.isUSSDEnable == 1){
            strussdurlStatus = "Active";
        }
        String stroldweburlStatus = "In-Active";
        if(oldpinObj.isWebEnable == 1){
            strweburlStatus = "Active";
        }
            
            String resultString = "ERROR";
            if (retValue == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Change EPIN Settings", resultString, retValue, "Setting Management",
                        " PinDeliveryType=" + oldpinObj.pinDeliveryType + " ChannelType=" + oldpinObj.channelType + " ChannelType2="
                        + oldpinObj.channelType2 + " Duration=" + oldpinObj.duration + " DayRestriction=" + oldpinObj.dayRestriction
                        + " TimeFromInHour=" + oldpinObj.timeFromInHour + " TimeToInHour" + oldpinObj.timeToInHour + " PinRequestCountDuration"
                        + oldpinObj.pinRequestCountDuration + " PinRequestCount" + oldpinObj.pinRequestCount + " OperatorController" + oldpinObj.operatorController
                        + " ExpiryTime" + oldpinObj.expiryTime + " IsAlertOperatorOnFailure" + oldpinObj.isAlertOperatorOnFailure 
                        + "smsText" + oldpinObj.smsText
                        +",PINSource"+stroldPINSource+",Pin Source Class Name ="+oldpinObj.classname
                        + "Challenge Response ="+stroldChallengeResponse+", ChallegeResponse Class Name="+oldpinObj.challangeResponseClassName
                        +", SMS Url ="+oldpinObj.smsUrl+ ", Sms Url Alert Status ="+stroldsmsurlStatus
                        +", VOICE Url ="+oldpinObj.voiceUrl+", Voice url alert Status ="+stroldvoiceurlStatus
                        +", USSD Url ="+oldpinObj.ussdUrl+", Voice url alert Status ="+stroldussdurlStatus
                        +", WEB Url ="+oldpinObj.webUrl+", Voice url alert Status ="+stroldweburlStatus,
                        "PinDeliveryType=" + epinObj.pinDeliveryType + "ChannelType=" + epinObj.channelType + "ChannelType2="
                        + epinObj.channelType2 + "Duration=" + epinObj.duration + "DayRestriction=" + epinObj.dayRestriction
                        + "TimeFromInHour=" + epinObj.timeFromInHour + "TimeToInHour" + epinObj.timeToInHour + "PinRequestCountDuration"
                        + epinObj.pinRequestCountDuration + "PinRequestCount" + epinObj.pinRequestCount + "OperatorController" + epinObj.operatorController
                        + "ExpiryTime" + epinObj.expiryTime + "IsAlertOperatorOnFailure" + epinObj.isAlertOperatorOnFailure 
                        + "smsText" + epinObj.smsText 
                          + ",PINSource"+strPINSource+",Pin Source Class Name ="+PinSourceClassNameclassname
                        + "Challenge Response ="+strChallengeResponse+", ChallegeResponse Class Name="+ChallengeResponseclassname
                        +", SMS Url ="+smsurl+ ", Sms Url Alert Status ="+strsmsurlStatus
                        +", VOICE Url ="+voiceurl+", Voice url alert Status ="+strvoiceurlStatus
                        +", USSD Url ="+voiceurl+", Voice url alert Status ="+strussdurlStatus
                        +", WEB Url ="+webeurl+", Voice url alert Status ="+strweburlStatus,
                        itemtype, channel.getChannelid());
            } else if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Change EPIN Settings", resultString, retValue, "Setting Management",
                        " PinDeliveryType=" + oldpinObj.pinDeliveryType + " ChannelType=" + oldpinObj.channelType + " ChannelType2="
                        + oldpinObj.channelType2 + " Duration=" + oldpinObj.duration + " DayRestriction=" + oldpinObj.dayRestriction
                        + " TimeFromInHour=" + oldpinObj.timeFromInHour + " TimeToInHour" + oldpinObj.timeToInHour + " PinRequestCountDuration"
                        + oldpinObj.pinRequestCountDuration + " PinRequestCount" + oldpinObj.pinRequestCount + " OperatorController" + oldpinObj.operatorController
                        + " ExpiryTime" + oldpinObj.expiryTime + " IsAlertOperatorOnFailure" + oldpinObj.isAlertOperatorOnFailure 
                        + "smsText" + epinObj.smsText 
                        +",PINSource"+stroldPINSource+",Pin Source Class Name ="+oldpinObj.classname
                        + "Challenge Response ="+stroldChallengeResponse+", ChallegeResponse Class Name="+oldpinObj.challangeResponseClassName
                        +", SMS Url ="+oldpinObj.smsUrl+ ", Sms Url Alert Status ="+stroldsmsurlStatus
                        +", VOICE Url ="+oldpinObj.voiceUrl+", Voice url alert Status ="+stroldvoiceurlStatus
                        +", USSD Url ="+oldpinObj.ussdUrl+", Voice url alert Status ="+stroldussdurlStatus
                        +", WEB Url ="+oldpinObj.webUrl+", Voice url alert Status ="+stroldweburlStatus,
                        "Failed To Edit EPIN Settings",
                        itemtype, channel.getChannelid());
            }
        }

        if (retValue != 0) {
            result = "error";
            message = "EPIN Settings Update Failed!!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);

        }catch(Exception e){log.error("Exception caught :: ",e);} finally {
            out.print(json);
            out.flush();
        }
        log.info("Servlet ended");
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";

    }// </editor-fold>
}
