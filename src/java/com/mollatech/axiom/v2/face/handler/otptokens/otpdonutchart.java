package com.mollatech.axiom.v2.face.handler.otptokens;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.face.common.donut;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mollatech1
 */
public class otpdonutchart extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(otpdonutchart.class.getName());

    public static final int TOKEN_STATUS_ACTIVE = OTPTokenManagement.TOKEN_STATUS_ACTIVE;//1;
    public static final int TOKEN_STATUS_LOCKEd = OTPTokenManagement.TOKEN_STATUS_LOCKEd;//-1;
    public static final int TOKEN_STATUS_ASSIGNED = OTPTokenManagement.TOKEN_STATUS_ASSIGNED;
    public static final int TOKEN_STATUS_UNASSIGNED  = OTPTokenManagement.TOKEN_STATUS_UNASSIGNED;
    public static final int TOKEN_STATUS_SUSPENDED  = OTPTokenManagement.TOKEN_STATUS_SUSPENDED;
    public static final int TOKEN_STATUS_ALL  = OTPTokenManagement.TOKEN_STATUS_ALL;
    public static final int TOKEN_STATUS_LOST  = OTPTokenManagement.TOKEN_STATUS_LOST;
    public static final int TOKEN_STATUS_FREE  = OTPTokenManagement.TOKEN_STATUS_FREE;
    
    public static final int SOFTWARE_TOKEN = 1;
    public static final int HARDWARE_TOKEN = 2;
    public static final int OOB_TOKEN = 3;
//sw sub categories
    public int OTP_TOKEN_SOFTWARE_MOBILE = 2;
    public int OTP_TOKEN_SOFTWARE_PC = 3;
    public int OTP_TOKEN_SOFTWARE_WEB = 1;
    //subcategory of OOB
    public static final int OOB__SMS_TOKEN = 1;
    public static final int OOB__VOICE_TOKEN = 2;
    public static final int OOB__USSD_TOKEN = 3;
    public static final int OOB__EMAIL_TOKEN = 4;
    //HW Token
    public int OTP_TOKEN_HARDWARE_MINI = 1;
    public int OTP_TOKEN_HARDWARE_CR = 2;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("is started :: ");
        PrintWriter out = response.getWriter();
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String channelID = channel.getChannelid();
        log.debug("channelID :: "+channelID);
        String _category = request.getParameter("_changeCategory");
        log.debug("_category :: "+_category);
        String _status = request.getParameter("_changeOTPStatus");
        log.debug("_status :: "+_status);
        String _UnitId = request.getParameter("_UnitId");
        log.debug("_UnitId :: "+_UnitId);
        String _sdate  = request.getParameter("_otpstartdate");
        log.debug("_sdate :: "+_sdate);
        String _eDate  = request.getParameter("_otpenddate");
        log.debug("_eDate :: "+_eDate);
        Date fromDate = null; Date endDate = null;
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        try {
            int iCategory = 0;
            if(_sdate!=null && !_sdate.isEmpty()){
            fromDate = formatter.parse(_sdate);
            }
            if(_eDate!=null && !_eDate.isEmpty()){
                endDate = formatter.parse(_eDate);
            }
            if (_category != null) {
                iCategory = Integer.parseInt(_category);
            }
            int istatus = 0;
            if (_status != null) {
                istatus = Integer.parseInt(_status);
            }
            int iUnitId = 0;
            if (_UnitId != null) {
                iUnitId = Integer.parseInt(_UnitId);
                if ( iUnitId == -1) {
                    iUnitId=0;//ALL units
                }
            }
            OTPTokenManagement oObj = new OTPTokenManagement(channel.getChannelid());

            ArrayList<donut> sample = new ArrayList<donut>();
            if (iCategory == SOFTWARE_TOKEN) {

                if (istatus != TOKEN_STATUS_ALL) {
                    int mobile = oObj.getUserOTPTokenCountV2(channelID, SOFTWARE_TOKEN, OTP_TOKEN_SOFTWARE_MOBILE, istatus,iUnitId,fromDate,endDate);
                    int pc = oObj.getUserOTPTokenCountV2(channelID, SOFTWARE_TOKEN, OTP_TOKEN_SOFTWARE_PC, istatus,iUnitId,fromDate,endDate);
                    int web = oObj.getUserOTPTokenCountV2(channelID, SOFTWARE_TOKEN, OTP_TOKEN_SOFTWARE_WEB, istatus,iUnitId,fromDate,endDate);
                    sample.add(new donut(mobile, "Mobile"));
                    sample.add(new donut(pc, "PC"));
                    sample.add(new donut(web, "Web"));
                } else if (istatus == TOKEN_STATUS_ALL) {
                    int iactive = oObj.getUserOTPTokenCountByCategoryV2(channelID, SOFTWARE_TOKEN, OTPTokenManagement.TOKEN_STATUS_ACTIVE,iUnitId,fromDate,endDate);
                    int iassign = oObj.getUserOTPTokenCountByCategoryV2(channelID, SOFTWARE_TOKEN, OTPTokenManagement.TOKEN_STATUS_ASSIGNED,iUnitId,fromDate,endDate);
                    int ilocked = oObj.getUserOTPTokenCountByCategoryV2(channelID, SOFTWARE_TOKEN, OTPTokenManagement.TOKEN_STATUS_LOCKEd,iUnitId,fromDate,endDate);
                    int isuspended = oObj.getUserOTPTokenCountByCategoryV2(channelID, SOFTWARE_TOKEN, OTPTokenManagement.TOKEN_STATUS_SUSPENDED,iUnitId,fromDate,endDate);

                    sample.add(new donut(iactive, "Active"));
                    sample.add(new donut(iassign, "Assign"));
                    sample.add(new donut(ilocked, "Locked"));
                    sample.add(new donut(isuspended, "Suspended"));                    
                }
            } else if (iCategory == HARDWARE_TOKEN) {
                if (istatus != TOKEN_STATUS_ALL) {
                    int hw = oObj.getUserOTPTokenCountV2(channelID, HARDWARE_TOKEN, OTP_TOKEN_HARDWARE_MINI, istatus,iUnitId,fromDate,endDate);
                    int cr = oObj.getUserOTPTokenCountV2(channelID, HARDWARE_TOKEN, OTP_TOKEN_HARDWARE_CR, istatus,iUnitId,fromDate,endDate);
                    sample.add(new donut(hw, "Mini Token"));
                    sample.add(new donut(cr, "CR Token"));
                } else if (istatus == TOKEN_STATUS_ALL) {
                    int iactive = oObj.getUserOTPTokenCountByCategoryV2(channelID, HARDWARE_TOKEN, OTPTokenManagement.TOKEN_STATUS_ACTIVE,iUnitId,fromDate,endDate);
                    int iassign = oObj.getUserOTPTokenCountByCategoryV2(channelID, HARDWARE_TOKEN, OTPTokenManagement.TOKEN_STATUS_ASSIGNED,iUnitId,fromDate,endDate);
                    int ilocked = oObj.getUserOTPTokenCountByCategoryV2(channelID, HARDWARE_TOKEN, OTPTokenManagement.TOKEN_STATUS_LOCKEd,iUnitId,fromDate,endDate);
                    int isuspended = oObj.getUserOTPTokenCountByCategoryV2(channelID, HARDWARE_TOKEN, OTPTokenManagement.TOKEN_STATUS_SUSPENDED,iUnitId,fromDate,endDate);
                    int ifree = oObj.getUserOTPTokenCountByCategoryV2(channelID, HARDWARE_TOKEN, OTPTokenManagement.TOKEN_STATUS_FREE,iUnitId,fromDate,endDate);
                    int ilost = oObj.getUserOTPTokenCountByCategoryV2(channelID, HARDWARE_TOKEN, OTPTokenManagement.TOKEN_STATUS_LOST,iUnitId,fromDate,endDate);

                    sample.add(new donut(iactive, "Active"));
                    sample.add(new donut(iassign, "Assign"));
                    sample.add(new donut(ilocked, "Locked"));
                    sample.add(new donut(isuspended, "Suspended"));
                    sample.add(new donut(ifree, "Free"));
                    sample.add(new donut(ilost, "Lost"));

                }
            } else if (iCategory == OOB_TOKEN) {

                if (istatus != TOKEN_STATUS_ALL) {
                    int email = oObj.getUserOTPTokenCountV2(channelID, OOB_TOKEN, OTPTokenManagement.OOB__EMAIL_TOKEN, istatus,iUnitId,fromDate,endDate);
                    int sms = oObj.getUserOTPTokenCountV2(channelID, OOB_TOKEN, OTPTokenManagement.OOB__SMS_TOKEN, istatus,iUnitId,fromDate,endDate);
                    int ussd = oObj.getUserOTPTokenCountV2(channelID, OOB_TOKEN, OTPTokenManagement.OOB__USSD_TOKEN, istatus,iUnitId,fromDate,endDate);
                    int voice = oObj.getUserOTPTokenCountV2(channelID, OOB_TOKEN, OTPTokenManagement.OOB__VOICE_TOKEN, istatus,iUnitId,fromDate,endDate);
                    sample.add(new donut(sms, "SMS"));
                    sample.add(new donut(ussd, "USSD"));
                    sample.add(new donut(voice, "VOICE"));
                    sample.add(new donut(email, "EMAIL"));
                } else if (istatus == TOKEN_STATUS_ALL) {
                    int iactive = oObj.getUserOTPTokenCountByCategoryV2(channelID, OOB_TOKEN, OTPTokenManagement.TOKEN_STATUS_ACTIVE,iUnitId,fromDate,endDate);
                    int iassign = oObj.getUserOTPTokenCountByCategoryV2(channelID, OOB_TOKEN, OTPTokenManagement.TOKEN_STATUS_ASSIGNED,iUnitId,fromDate,endDate);
                    int ilocked = oObj.getUserOTPTokenCountByCategoryV2(channelID, OOB_TOKEN, OTPTokenManagement.TOKEN_STATUS_LOCKEd,iUnitId,fromDate,endDate);
                    int isuspended = oObj.getUserOTPTokenCountByCategoryV2(channelID, OOB_TOKEN, OTPTokenManagement.TOKEN_STATUS_SUSPENDED,iUnitId,fromDate,endDate);

                    sample.add(new donut(iactive, "Active"));
                    sample.add(new donut(iassign, "Assign"));
                    sample.add(new donut(ilocked, "Locked"));
                    sample.add(new donut(isuspended, "Suspended"));

                }


            }
            Gson gson = new Gson();

            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<donut>>() {
            }.getType());

            JsonArray jsonArray = element.getAsJsonArray();
            out.print(jsonArray);
        } catch(Exception e){
            log.error("exception caught :: ",e);
        }finally {
            out.close();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
