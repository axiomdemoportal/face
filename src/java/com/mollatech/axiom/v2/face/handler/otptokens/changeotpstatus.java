/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.otptokens;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.AuthorizationManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.connector.access.controller.ApprovalSetting;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.ByteArrayInputStream;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class changeotpstatus extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(changeotpstatus.class.getName());

    public static final int HW_MINI_TOKEN = 1;
    public static final int HW_CR_TOKEN = 2;
    final int SW_PC_TOKEN = 3;
    final int OOB__EMAIL_TOKEN = 4;
    final int SOFTWARE_TOKEN = 1;
    final int SW_WEB_TOKEN = 1;
    final int SW_MOBILE_TOKEN = 2;
    final int HARDWARE_TOKEN = 2;
    final int OOB_TOKEN = 3;
    final int OOB__SMS_TOKEN = 1;
    final int OOB__VOICE_TOKEN = 2;
    final int OOB__USSD_TOKEN = 3;
    public static final int TOKEN_STATUS_ACTIVE = 1;
    public static final int TOKEN_STATUS_LOCKEd = -1;
    public static final int TOKEN_STATUS_ASSIGNED = 0;
    public static final int TOKEN_STATUS_UNASSIGNED = -10;
    public static final int TOKEN_STATUS_SUSPENDED = -2;
    public static final int TOKEN_STATUS_LOST = -5;

    final String itemType = "OTPTOKENS";
    final String itemTypeAUTH = "AUTHORIZATION";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String _serialno = request.getParameter("_serialno");
        String message = null;

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS);

        String _userid = request.getParameter("_userid");
        log.debug("_userid :: "+_userid);
        String _status = request.getParameter("_status");
        log.debug("_status :: "+_status);
        int status = 0;
        String _value = "Active";
        if (_status != null) {
            status = Integer.parseInt(_status);
        }
        String _subcategory = request.getParameter("_subcategory");
        log.debug("_subcategory :: "+_subcategory);
        int subCategory = 0;
        if (_subcategory != null) {
            subCategory = Integer.parseInt(_subcategory);
        }
        String _newvalue = request.getParameter("newvalue");
        log.debug("new value = "+_newvalue);
        String _oldvalue = request.getParameter("oldvalue");
        log.debug("oldvalue = "+_oldvalue);
        String _category = request.getParameter("_category");
        log.debug("_category :: "+_category);
        int category = 0;
        if (_category != null) {
            category = Integer.parseInt(_category);
        }
        String result = "success";
        String _message = "status changed successfully!!!";

        if (status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
            _value = "Active ";
        } else if (status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
            _value = "Suspended ";
        } else if (status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
            _value = "Unassigned ";
        } else if (status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
            _value = "LOCK ";
        } else if (status == OTPTokenManagement.TOKEN_STATUS_LOST) {
            _value = "LOST ";
        }

        _message = _value + _message;

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        if (_userid == null || _status == null) {
            result = "error";
            _message = "Invalid Request!!!";
            try {
                json.put("_result", result);
                json.put("_message", _message);
            } catch (Exception e) {
               log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        int retValue = -1;

        OTPTokenManagement oManagement = new OTPTokenManagement(channel.getChannelid());
        AuditManagement audit = new AuditManagement();
        int istatus = oManagement.getStatus(sessionId, channel.getChannelid(), _userid, category, subCategory);
        log.debug("getStatus :: "+istatus);

        if (istatus == status) {
            result = "error";
            _message = "Status is already " + _value + "!!!";
            try {
                json.put("_result", result);
                json.put("_message", _message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;

        } else if (istatus == OTPTokenManagement.TOKEN_STATUS_LOST && status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
            result = "error";
            //_message = "Status is already" + _value +"!!!";
            _message = "Status could not be changed as it is marked " + _value + "!!!";
            try {
                json.put("_result", result);
                json.put("_message", _message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;

        } else if (istatus == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED && status == OTPTokenManagement.TOKEN_STATUS_ACTIVE
                || istatus == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED && status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
            result = "error";
            _message = "Status could not changed as Token is not assigned!!!";
            try {
                json.put("_result", result);
                json.put("_message", _message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;

        }

        //   String serialNo =  oManagement.getSerialNo(sessionId, channel.getChannelid(), _userid, category);
        UserManagement uMngt = new UserManagement();
        AuthUser uObj = uMngt.getUser(sessionId, channel.getChannelid(), _userid);
        //start of authorization - Nilesh

//                      if (operatorS.getRoleid() != 6|| operatorS.getRoleid() != 5) {
//            if (operatorS.getRoleid() >= 3) {
//                result = "error";
//                message = "Sorry, But you don't have permissions for this action!!!";
//                try {
//                    json.put("_result", result);
//                    json.put("_message", message);
//                } catch (Exception e) {
//                    log.error("Exception caught :: ",e);
//                }
//                out.print(json);
//                out.flush();
//                return;
//            }
//              }
        String _approvalId = request.getParameter("_approvalId");
        log.debug("_approvalId :: "+_approvalId);
        SettingsManagement sMngmt = new SettingsManagement();
        Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
        ChannelProfile chSettingObj = null;
        if (settingsObj != null) {
            chSettingObj = (ChannelProfile) settingsObj;
        }
        String _unitId = request.getParameter("_unitId");
        log.debug("_unitId :: "+_unitId);
        if (_unitId != null) {
            if (chSettingObj != null) {

                if (chSettingObj.authorizationunit == 1) {

                    int iUnitId = Integer.parseInt(_unitId);
                    if (iUnitId != operatorS.getUnits()) {
                        result = "error";
                        message = "Sorry, Must be marked by same branch operator!!!";
                        try {
                            json.put("_result", result);
                            json.put("_message", message);
                        } catch (Exception e) {
                            log.error("exception caught :: ",e);
                        }
                        out.print(json);
                        out.flush();
                        return;
                    }
                }
            }
        }
        OperatorsManagement oprMngt = new OperatorsManagement();

        AuthorizationManagement auth = new AuthorizationManagement();
        String strOpName = "-";
        String struserName = "-";
        String strAction = "-";
        int iapprovalID = -1;
//        ApprovalSetting approvalSetting = null;
        if (_approvalId != null) {
            if (operatorS.getRoleid() != 6) {
                if (operatorS.getRoleid() >= 3) {
                    result = "error";
                    message = "Sorry, But you don't have permissions for this action!!!";
                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("exception caught :: ",e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
            }

//            iapprovalID = Integer.parseInt(_approvalId);

            String _markerID = request.getParameter("_markerID");
            log.debug("_markerID :: "+_markerID);
            Operators op = oprMngt.getOperatorById(channel.getChannelid(), _markerID);
            if (op != null) {
                strOpName = op.getName();
            }
            if (uObj != null) {
                struserName = uObj.getUserName();
            }

        }
        String strStatus = "";
        if (istatus == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
            strStatus = "TOKEN_STATUS_ACTIVE";
        } else if (istatus == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
            strStatus = "TOKEN_STATUS_ASSIGNED";
        } else if (istatus == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
            strStatus = "TOKEN_STATUS_LOCKEd";
        } else if (istatus == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
            strStatus = "TOKEN_STATUS_SUSPENDED";
        } else if (istatus == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
            strStatus = "TOKEN_STATUS_UNASSIGNED";
        } else if (istatus == OTPTokenManagement.TOKEN_STATUS_LOST) {
            strStatus = "TOKEN_STATUS_LOST";
        }
        String strnewStatus = "";
        String strnewStatusA = "";
        if (status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
            strnewStatus = "TOKEN_STATUS_ACTIVE";
            strnewStatusA = "Active";
        } else if (status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
            strnewStatus = "TOKEN_STATUS_ASSIGNED";
            strnewStatusA = "Assign";
        } else if (status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
            strnewStatus = "TOKEN_STATUS_LOCKEd";
            strnewStatusA = "Locked";
        } else if (status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
            strnewStatus = "TOKEN_STATUS_SUSPENDED";
            strnewStatusA = "Suspended";
        } else if (status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
            strnewStatus = "TOKEN_STATUS_UNASSIGNED";
            strnewStatusA = "Un-Assign";
        } else if (status == OTPTokenManagement.TOKEN_STATUS_LOST) {
            strnewStatus = "TOKEN_STATUS_LOST";
            strnewStatusA = "Lost";
        }
        String strCategory = "";
        String strSubCategory = "";

        String tokentype = "";
        String tokensubtype = "";
        if (category == OTPTokenManagement.SOFTWARE_TOKEN) {
            strCategory = "SOFTWARE_TOKEN";
            tokentype = "SW-Token";

            if (subCategory == OTPTokenManagement.SW_WEB_TOKEN) {
                strSubCategory = "SW_WEB_TOKEN";
                tokensubtype = "Web Token";
            } else if (subCategory == OTPTokenManagement.SW_MOBILE_TOKEN) {
                strSubCategory = "SW_MOBILE_TOKEN";
                tokensubtype = "Mobile Token";
            } else if (subCategory == OTPTokenManagement.SW_PC_TOKEN) {
                strSubCategory = "SW_PC_TOKEN";
                tokensubtype = "PC Token";
            }

        } else if (category == OTPTokenManagement.HARDWARE_TOKEN) {
            tokentype = "HW-Token";
            strCategory = "HARDWARE_TOKEN";
            if (subCategory == OTPTokenManagement.HW_MINI_TOKEN) {
                strSubCategory = "HW_MINI_TOKEN";
                tokensubtype = "HW Mini Token";
            } else if (subCategory == OTPTokenManagement.HW_CR_TOKEN) {
                strSubCategory = "HW_CR_TOKEN";
                tokensubtype = "HW CR Token";
            }
        } else if (category == OTPTokenManagement.OOB_TOKEN) {
            strCategory = "OOB_TOKEN";
            tokentype = "OOB-Token";
            if (subCategory == OTPTokenManagement.OOB__SMS_TOKEN) {
                strSubCategory = "OOB__SMS_TOKEN";
                tokensubtype = "Sms";
            } else if (subCategory == OTPTokenManagement.OOB__USSD_TOKEN) {
                strSubCategory = "Ussd";
            } else if (subCategory == OTPTokenManagement.OOB__VOICE_TOKEN) {
                strSubCategory = "Voice";
            } else if (subCategory == OTPTokenManagement.OOB__EMAIL_TOKEN) {
                strSubCategory = "Email";
            }
        }
//        if (chSettingObj != null && _approvalId == null && operatorS.getRoleid() == OperatorsManagement.Requester) {
//            if (chSettingObj.authorizationStatus == SettingsManagement.ACTIVE_STATUS) {
//                AuthorizationManagement authMngt = new AuthorizationManagement();
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = AuthorizationManagement.CHANGE_OTP_TOKEN_STATUS;
//                approval.itemid = "Chage Status of " + tokentype + " : " + tokensubtype + " token to " + strnewStatusA + " for =" + uObj.getUserName();
//                strAction = approval.itemid;
//                approval.makerid = operatorS.getOperatorid();
//                approval.userid = _userid;
//                approval.tokenCategory = category;
//                approval.tokenSubCategory = subCategory;
//                approval.tokenStatus = status;
////                approval.tokenSerialNo = _serialnumber;
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
////                pexpiredOn.set(Calendar.A, Calendar.AM);
//                pexpiredOn.add(Calendar.HOUR, chSettingObj.authorizationDuration);
////                pexpiredOn.set(Calendar.MINUTE, 0);
////                pexpiredOn.set(Calendar.SECOND, 0);
//                Date dexpiredOn = pexpiredOn.getTime();
//
////                retValue = authMngt.addAuthorizationSetting(sessionId, channel.getChannelid(), AuthorizationManagement.AUTORIZATION_PENDING_STATUS, dexpiredOn, approval
////                );
//                retValue = authMngt.addAuthorizationSetting(sessionId, channel.getChannelid(), operatorS.getOperatorid(), AuthorizationManagement.AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (retValue == 0) {
//                    retValue = AuthorizationManagement.RETURN_AUTORIZATION_RESULT;
//                }
//            } else {
//                retValue = oManagement.ChangeStatus(sessionId, channel.getChannelid(),
//                        _userid, status, category, subCategory);
//            }
//        } else {
        String _suspendreason = request.getParameter("_suspendreason");
        log.debug("_suspendreason :: "+_suspendreason);
        if (_suspendreason == null) {
            retValue = oManagement.ChangeStatus(sessionId, channel.getChannelid(),
                    _userid, status, category, subCategory);
            log.debug("ChangeStatus :: "+retValue);
        } else {
            retValue = oManagement.ChangeStatuswithreason(sessionId, channel.getChannelid(),
                    _userid, status, category, subCategory, _suspendreason);
            log.debug("ChangeStatuswithreason :: "+retValue);
        }

//        }
        //end of authorization
//        retValue = oManagement.ChangeStatus(sessionId, channel.getChannelid(), 
//                _userid, status, category, subCategory);
        if (retValue == -6) { //same stats
            result = "error";
            _message = "Invalid Request: Current and New State are same.";
            try {
                json.put("_result", result);
                json.put("_message", _message);
            } catch (Exception e) {
               log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        String resultString = "ERROR";

//        String strStatus = "";
//        if (istatus == TOKEN_STATUS_ACTIVE) {
//            strStatus = "TOKEN_STATUS_ACTIVE";
//        } else if (istatus == TOKEN_STATUS_ASSIGNED) {
//            strStatus = "TOKEN_STATUS_ASSIGNED";
//        } else if (istatus == TOKEN_STATUS_LOCKEd) {
//            strStatus = "TOKEN_STATUS_LOCKEd";
//        } else if (istatus == TOKEN_STATUS_SUSPENDED) {
//            strStatus = "TOKEN_STATUS_SUSPENDED";
//        } else if (istatus == TOKEN_STATUS_UNASSIGNED) {
//            strStatus = "TOKEN_STATUS_UNASSIGNED";
//        }
//        String strnewStatus = "";
//        if (status == TOKEN_STATUS_ACTIVE) {
//            strnewStatus = "TOKEN_STATUS_ACTIVE";
//        } else if (status == TOKEN_STATUS_ASSIGNED) {
//            strnewStatus = "TOKEN_STATUS_ASSIGNED";
//        } else if (status == TOKEN_STATUS_LOCKEd) {
//            strnewStatus = "TOKEN_STATUS_LOCKEd";
//        } else if (status == TOKEN_STATUS_SUSPENDED) {
//            strnewStatus = "TOKEN_STATUS_SUSPENDED";
//        } else if (status == TOKEN_STATUS_UNASSIGNED) {
//            strnewStatus = "TOKEN_STATUS_UNASSIGNED";
//        }
//        String strCategory = "";
//        if (category == SOFTWARE_TOKEN) {
//            strCategory = "SOFTWARE_TOKEN";
//        } else if (category == HARDWARE_TOKEN) {
//            strCategory = "HARDWARE_TOKEN";
//        } else if (category == OOB_TOKEN) {
//            strCategory = "OOB_TOKEN";
//        }
//
//        String strSubCategory = "";
//        if (subCategory == SW_WEB_TOKEN) {
//            strSubCategory = "SW_WEB_TOKEN";
//        } else if (subCategory == SW_MOBILE_TOKEN) {
//            strSubCategory = "SW_MOBILE_TOKEN";
//        } else if (subCategory == OOB__SMS_TOKEN) {
//            strSubCategory = "OOB__SMS_TOKEN";
//        } else if (subCategory == OOB__USSD_TOKEN) {
//            strSubCategory = "OOB__USSD_TOKEN";
//        } else if (subCategory == OOB__VOICE_TOKEN) {
//            strSubCategory = "OOB__VOICE_TOKEN";
//        }
        if (retValue == 0) {
            resultString = "SUCCESS";
            String entity = "User phone no:"+ uObj.getPhoneNo()+"User Name:"+uObj.getUserName();
            if(_serialno != null || _serialno != "" || !_serialno.equalsIgnoreCase("undefined")){
                
            audit.AddAuditTrailWithEntity(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Change OTP Status", resultString, retValue,
                    "Token Management","UserName"+"uObj"+"Category =" + strCategory + "SubCategory =" + strSubCategory + "Old Status =" + strStatus,
                    "Category =" + strCategory + "SubCategory =" + strSubCategory + "New Status =" + strnewStatus+"serial no="+_serialno,
                    itemType,entity, _userid);
            }else{
            audit.AddAuditTrailWithEntity(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Change OTP Status", resultString, retValue,
                    "Token Management","UserName"+"uObj"+"Category =" + strCategory + "SubCategory =" + strSubCategory + "Old Status =" + strStatus,
                    "Category =" + strCategory + "SubCategory =" + strSubCategory + "New Status =" + strnewStatus,
                    itemType,entity, _userid);
            }
            

            if (_approvalId != null) {
                
                int res = auth.removeAuthorizationRequest(sessionId, channel.getChannelid(), operatorS.getOperatorid(), _approvalId,_newvalue,_oldvalue);
                log.debug("removeAuthorizationRequest :: "+res);
                if (res == 0) {
                    resultString = "SUCCESS";
                    AuditManagement audit1 = new AuditManagement();
                     entity = "User phone no:"+ uObj.getPhoneNo()+"User Name:"+ uObj.getUserName();
                    audit1.AddAuditTrailWithEntity(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), 
                            "Reject Authorization Request",
                            resultString, retValue,
                            "Authorization Management", "Action =" + strAction + ",Action Marked By=" + strOpName
                            + ",Action Marked On=" + struserName,
                            "Removed successfully!"+"serial no="+_serialno,
                            itemTypeAUTH,entity, _userid);
                }
                else { 
                    resultString = "ERROR";
                    AuditManagement audit1 = new AuditManagement();
                    entity = "User phone no:"+ uObj.getPhoneNo()+"User Name:"+ uObj.getUserName();
                    audit1.AddAuditTrailWithEntity(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), 
                            "Reject Authorization Request",
                            resultString, retValue,
                            "Authorization Management", "Action =" + strAction + ",Action Marked By=" + strOpName
                            + ",Action Marked On=" + struserName+"serial no="+_serialno,
                            "Removed failed!",
                            itemTypeAUTH,entity, _userid);
                }

            }

            if (status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                AuthUser user = null;
                UserManagement userObj = new UserManagement();
                user = userObj.getUser(sessionId, channel.getChannelid(), _userid);
                Templates templates = null;
                ByteArrayInputStream bais = null;
                String subject = null;
                TemplateManagement tManagement = new TemplateManagement();
                if (category == OTPTokenManagement.SOFTWARE_TOKEN) {

                    templates = tManagement.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.MOBILE_SOFTWARE_TOKEN_UNASSIGN_TEMPLATE);

                    bais = new ByteArrayInputStream(templates.getTemplatebody());
                    message = (String) TemplateUtils.deserializeFromObject(bais);
                    //added for template based messages
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                    if (message != null) {
                        Date d = new Date();

                        message = message.replaceAll("#name#", user.getUserName());
                        message = message.replaceAll("#channel#", channel.getName());
                        message = message.replaceAll("#datetime#", sdf.format(d));
                        if (subCategory == OTPTokenManagement.SW_MOBILE_TOKEN) {
                            message = message.replaceAll("#tokentype#", "Mobile");
                        } else if (subCategory == OTPTokenManagement.SW_WEB_TOKEN) {
                            message = message.replaceAll("#tokentype#", "Web");
                        } else if (subCategory == OTPTokenManagement.SW_PC_TOKEN) {
                            message = message.replaceAll("#tokentype#", "PC");
                        }
                    }
                } else if (category == OTPTokenManagement.OOB_TOKEN) {
                    if (subCategory != OTPTokenManagement.OOB__EMAIL_TOKEN) {
                        templates = tManagement.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.MOBILE_OOB_OTP_UNASSIGN_TOKEN_TEMPLATE);
                        bais = new ByteArrayInputStream(templates.getTemplatebody());
                        message = (String) TemplateUtils.deserializeFromObject(bais);
                    } else {
                        templates = tManagement.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.EMAIL_OOB_OTP_UNASSIGN_TOKEN_TEMPLATE);
                        bais = new ByteArrayInputStream(templates.getTemplatebody());
                        message = (String) TemplateUtils.deserializeFromObject(bais);
                        subject = templates.getSubject();
                    }

                    if (subCategory != OTPTokenManagement.OOB__EMAIL_TOKEN) {
                        //added for template based messages
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                        if (message != null) {
                            Date d = new Date();

                            message = message.replaceAll("#name#", user.getUserName());
                            message = message.replaceAll("#channel#", channel.getName());
                            message = message.replaceAll("#datetime#", sdf.format(d));
                            if (subCategory == OTPTokenManagement.OOB__SMS_TOKEN) {
                                message = message.replaceAll("#tokentype#", "SMS");
                            } else if (subCategory == OTPTokenManagement.OOB__USSD_TOKEN) {
                                message = message.replaceAll("#tokentype#", "USSD");
                            } else if (subCategory == OTPTokenManagement.OOB__VOICE_TOKEN) {
                                message = message.replaceAll("#tokentype#", "Voice");
                            }
                        }
                    } else if (subCategory == OTPTokenManagement.OOB__EMAIL_TOKEN) {
                        //added for template based messages
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                        if (message != null) {
                            Date d = new Date();

                            message = message.replaceAll("#name#", user.getUserName());
                            message = message.replaceAll("#channel#", channel.getName());
                            message = message.replaceAll("#datetime#", sdf.format(d));
                            message = message.replaceAll("#tokentype#", "EMAIL");
                            message = message.replaceAll("#email#", user.getEmail());
                            subject = subject.replaceAll("#name#", user.getUserName());
                            subject = subject.replaceAll("#channel#", channel.getName());
                            subject = subject.replaceAll("#datetime#", sdf.format(d));
                            subject = subject.replaceAll("#tokentype#", "EMAIL");
                        }

                    }
                }

                SendNotification send = new SendNotification();
                //     AXIOMStatus axiomStatus = send.SendOnMobile(channel.getChannelid(), user.getPhoneNo(), templatebody, 1, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));

                if (message != null && user != null) {
                    Date d = new Date();

                    if (category == OTPTokenManagement.SOFTWARE_TOKEN) {
                        if (user.phoneNo != null) {
                            if (templates.getStatus() == tManagement.ACTIVE_STATUS) {
                                send.SendOnMobileNoWaiting(channel.getChannelid(), user.phoneNo, message, SendNotification.SMS, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }
                        }
                    } else {
                        if (subCategory == OTPTokenManagement.OOB__SMS_TOKEN || subCategory == OTPTokenManagement.OOB__VOICE_TOKEN || subCategory == OTPTokenManagement.OOB__USSD_TOKEN) {
                            if (templates.getStatus() == tManagement.ACTIVE_STATUS) {
                                if (user.phoneNo != null) {
                                    send.SendOnMobileNoWaiting(channel.getChannelid(), user.phoneNo, message, subCategory, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                            }
                        } else {
                            if (templates.getStatus() == tManagement.ACTIVE_STATUS) {
                                if (user.email != null) {
                                    send.SendEmail(channel.getChannelid(), user.email, subject, message, null, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                            }
                        }
                    }
                }
            }
            } else if (retValue == AuthorizationManagement.RETURN_AUTORIZATION_RESULT) {
            resultString = "SUCCESS";
//            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
//                    request.getRemoteAddr(), channel.getName(),
//                    remoteaccesslogin, operatorS.getName(), new Date(), "Assign Token",
//                    resultString, retValue,
//                    "Token Management", "", "Pending for approval for Category = " + strCategory + " Sub Category =" + strSubCategory + " Serial No =" + _serialnumber,
//                    itemType, _userid);

//            if (_approvalId != null) {
            resultString = "Success";
            String entity = "User phone no:"+ uObj.getPhoneNo()+"User Name:"+ uObj.getUserName();
            audit.AddAuditTrailWithEntity(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), 
                    "Add Authorization Request :Change Token Status",
                    resultString, retValue,
                    "Authorization Management", "Category = " + strCategory + "Category =" + strCategory + "SubCategory =" + strSubCategory
                    + "New Status =" + strnewStatus+"serial no="+_serialno,
                    "Added successfully!!!",
                    itemTypeAUTH,entity, 
                    _userid);

//            }
            result = "success";
            message = "Request is pending for approval!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("exception caught :: ",ex);
            }

//            json.put("_value", _value);
//            json.put("_status", _status);
            out.print(json);
            out.flush();
            return;
        } else {
            String entity = "User phone no:"+ uObj.getPhoneNo()+"User Name:"+ uObj.getUserName();    
            audit.AddAuditTrailWithEntity(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Change OTP Status", resultString, retValue,
                    "Token Management", "Category =" + strCategory + "SubCategory =" + strSubCategory + "Old Status =" + strStatus,
                    "Category =" + strCategory + "SubCategory =" + strSubCategory + "New Status =" + strnewStatus+"serial no="+_serialno,
                    itemType,entity, _userid);

            result = "error";
            _message = "Status could not be change (" + retValue + ")!!!";
            try {
                json.put("_result", result);
                json.put("_message", _message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        
        try {
            json.put("_result", result);
            json.put("_message", _message);
            json.put("_value", _value);

        } catch (Exception e) {
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
