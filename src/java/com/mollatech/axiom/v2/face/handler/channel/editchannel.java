package com.mollatech.axiom.v2.face.handler.channel;

//package com.mollatech.axiom.v2.face.handler.channel;
//
import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement;
import com.mollatech.axiom.nucleus.db.operation.AxiomChannel;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class editchannel extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editchannel.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final String itemType = "CHANNEL";
    final String itemTypeREMOTEACCESS = "REMOTEACCESS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");

        //Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId::"+sessionId);

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin::"+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String operatorId = operatorS.getOperatorid();
        log.debug("operatorId::"+operatorId);
        String _ch_name = request.getParameter("_ch_nameE");
        log.debug("_ch_name::"+_ch_name);
        String _ch_status = request.getParameter("_ch_statusE");
        log.debug("_ch_status::"+_ch_status);
        String _channelid = request.getParameter("_channelidE");
        log.debug("_channelid::"+_channelid);
        String _ch_virtual_path = request.getParameter("_ch_virtual_pathE");
        log.debug("_ch_virtual_path::"+_ch_virtual_path);
        String _rem_expirymin = request.getParameter("_rem_expiryminE");
        log.debug("_rem_expirymin::"+_rem_expirymin);
        String _rem_loginid = request.getParameter("_rem_loginidE");
        log.debug("_rem_loginid::"+_rem_loginid);
        String _rem_password = request.getParameter("_rem_passwordE");
        log.debug("_rem_password::"+_rem_password);
        String _rem_passwordC = request.getParameter("_rem_passwordEC");
        log.debug("_rem_passwordC::"+_rem_passwordC);

        int status = Integer.valueOf(_ch_status);
        int expirymin = Integer.parseInt(_rem_expirymin);
    
        String result = "success";
        String message = "Channel Updated Successfully....";

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        if (_ch_name == null || _ch_virtual_path == null
                || _channelid == null) {
            result = "error";
            message = "Invalid Parameters!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                
                log.error("exception caught ::",e);

            }
            out.print(json);
            out.flush();
            return;
        }

        if (_rem_loginid != null) {
            if (_rem_password == null || _rem_passwordC == null) {
                result = "error";
                message = "Remote Access Parameters are invalid!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    
                    log.error("exception caught ::",e);

                }
                out.print(json);
                out.flush();
                return;
            }
            if (_rem_password.compareTo(_rem_passwordC) != 0) {
                result = "error";
                message = "Password did not match!!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    
                    log.error("exception caught ::",e);

                }
                out.print(json);
                out.flush();
                return;
            }
            if (expirymin < 0) {
                result = "error";
                message = "Expiry Time is invalid!!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    
                    log.error("exception caught ::",e);

                }
                out.print(json);
                out.flush();
                return;
            }
        }

        //also needs to be changed from loadsetting db config
        File serverDir = new File(LoadSettings.g_sSettings.getProperty("axiom.dir"));
        File sourcePath = new File(LoadSettings.g_sSettings.getProperty("axiom.dir") + System.getProperty("file.separator") + LoadSettings.g_sSettings.getProperty("axiom.template"));
        File destinationPath = new File(LoadSettings.g_sSettings.getProperty("axiom.dir") + System.getProperty("file.separator") + _ch_virtual_path + ".war");
        //end of change

        int retValue = -1;

        try {
            ChannelManagement cManagement = new ChannelManagement();
            AuditManagement audit = new AuditManagement();
            String channelOldVPath = "";
            AxiomChannel[] channels = cManagement.ListChannelsInternal();
            for (int i = 0; i < channels.length; i++) {
                if (channels[i].getStrChannelid().compareTo(_channelid) == 0) {
                    channelOldVPath = channels[i].strVirtualPath;
                    break;
                }
            }
            retValue = cManagement.checkIsUnique4Channel(_channelid, _ch_name, _ch_virtual_path);
            log.debug("checkIsUnique4Channel::"+retValue);
            if (retValue == 0) {
                Channels oldChannelObj = cManagement.getChannelByID(sessionId, _channelid);
                retValue = cManagement.editChannel(sessionId, _channelid, _ch_name, _ch_virtual_path, status);
                Channels newChannelObj = cManagement.getChannelByID(sessionId, _channelid);
                String resultStr = "ERROR";
                if (retValue == 0) {
                    resultStr = "SUCCESS";
                    audit.AddAuditTrail(sessionId, _channelid, operatorS.getOperatorid(),
                            request.getRemoteAddr(), _ch_name, remoteaccesslogin,
                            operatorS.getName(), new Date(),
                            "Edit Channel", resultStr, retValue, "Channel Management",
                            "Channel Name =" + oldChannelObj.getName() + "Channel V irtual Path =" + oldChannelObj.getVpath() + "Old Status =" + oldChannelObj.getStatus(),
                            "Channel Name =" + newChannelObj.getName() + "Channel V irtual Path =" + newChannelObj.getVpath() + "Old Status =" + newChannelObj.getStatus(),
                            itemType,
                            _channelid);
                } else if (retValue != 0) {
                    audit.AddAuditTrail(sessionId, _channelid, operatorS.getOperatorid(),
                            request.getRemoteAddr(), _ch_name, remoteaccesslogin,
                            operatorS.getName(), new Date(), "Edit Channel", resultStr, retValue, "Channel Management",
                            "Channel Name =" + oldChannelObj.getName() + "Channel V irtual Path =" + oldChannelObj.getVpath() + "Old Status =" + oldChannelObj.getStatus(),
                            "Failed To Edit Channel", itemType,
                            _channelid);
                }

                int retValue1 = cManagement.SetAuthentication(sessionId, _channelid, true,
                        _rem_loginid, _rem_password,
                        expirymin);
                log.debug("SetAuthentication::"+retValue);
                resultStr = "ERROR";
                if (retValue1 == 0) {
                    resultStr = "SUCCESS";

                    audit.AddAuditTrail(sessionId, _channelid, operatorId,
                            request.getRemoteAddr(), _ch_name, remoteaccesslogin,
                            operatorS.getName(), new Date(), "Change Remote Access", resultStr, retValue,
                            "Remote Access Management",
                            "",
                            "Authentication Enable =" + true + "Login Id =" + _rem_loginid
                            + "Password =*****" + "Expiry Minuites =" + expirymin,
                            itemTypeREMOTEACCESS,
                            _channelid);

                } else if (retValue1 != 0) {
                    audit.AddAuditTrail(sessionId, _channelid, operatorId,
                            request.getRemoteAddr(), _ch_name, remoteaccesslogin,
                            operatorS.getName(), new Date(), "Change Remote Access", resultStr, retValue,
                            "Remote Access Management",
                            "",
                            "Failed To Set Remote Access Details",
                            itemTypeREMOTEACCESS,
                            _channelid);

                }

                try {
                    UtilityFunctions copy = new UtilityFunctions();
                    //Remove old file.
//                    copy.delete(serverDir);
//                //System.out.println("The vpath to be deleted" + fRemoveOldOne);
//                    //copy.delete(fRemoveOldOne);
//                    copy.copyFile(sourcePath, destinationPath);
//                    File fOldWAR = new File(LoadSettings.g_sSettings.getProperty("axiom.dir") + System.getProperty("file.separator") + channelOldVPath + ".war");
//                    File fNewWAR = new File(LoadSettings.g_sSettings.getProperty("axiom.dir") + System.getProperty("file.separator") + _ch_virtual_path + ".war");
//                    boolean success = fOldWAR.renameTo(fNewWAR);
//                    if (!success) {
//                        //System.out.println(fNewWAR.getAbsolutePath() + "failed as new name!!!");
//                    }
                }
                    catch (Exception ex) {
            // TODO handle custom exceptions here
            log.error("exception caught :: ",ex);
          
        }

                }else {
                result = "error";
                message = "Name/Virtual Path are already taken!!!";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }

            }catch (Exception ex) {
            // TODO handle custom exceptions here
            log.error("Exception caught :: ",ex);
          
        }

            if (retValue != 0) {
                result = "error";
                message = "Failed to update Channel!!";
            }
            try {
                json.put("_result", result);
                json.put("_message", message);

            } catch (Exception e) {
                
                log.error("exception caught ::",e);

            } finally {
                out.print(json);
                out.flush();
            }
            log.info("is ended :: ");
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet
        (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            processRequest(request, response);
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost
        (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            processRequest(request, response);
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo
        
            () {
        return "Short description";
        }// </editor-fold>
    }
