package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.FaxSetting;
import com.mollatech.axiom.nucleus.settings.OOBMobileChannelSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class editfaxsettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editfaxsettings.class.getName());

    //Static variables
//    public static final int SMS = 1;
    public static final int ACTIVE_STATUS = 1;
    public static final int SUSPENDED_STATUS = 1;
    public static final int PREFERENCE_ONE = 1;   //primary
    public static final int PREFERENCE_TWO = 2;
    final String itemtype = "SETTINGS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
         String OperatorID = operatorS.getOperatorid();
        String _channelId = channel.getChannelid();
        SettingsManagement sMngmt = new SettingsManagement();
        int retValue = -1;
         String result = "success";
        String message = "FAX Gateway Settings Update Successful!!!";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
            log.debug("editfaxsettings::channel is::" + channel.getName());
            log.debug("editfaxsettings::Operator Id is::" + operatorS.getOperatorid());
            log.debug("editfaxsettings::SessionId::" + sessionId);
            log.debug("editfaxsettings::RemoteAccessLogin::" + remoteaccesslogin);
            log.debug("editfaxsettings::Channel Id is::" + channel.getChannelid());
        
          if (AxiomProtect.CheckEnforcementFor(AxiomProtect.GATEWAY_FAX) != 0) {
            result = "error";
            message = "This feature is not available in this license!!!";

            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }

            out.print(json);
            out.flush();
            return;
        }

        String _className = null;
        String _ip = null;
        String _logConfirmation1 = null;
        String _password = null;
        String _phoneNo = null;
        String _port = null;
        Object reserve1 = null;
        Object reserve2 = null;
        Object reserve3 = null;
        String _status1 = null;
        String _userId = null;
        String _autofailover = null;
        String _retrycount = null;
        String _retryduration = null;

        String _preference = request.getParameter("_perference");
        int _iPreference = Integer.parseInt(_preference);
        log.debug("editfaxsettings::Preference is::" + _iPreference);
         
        //request parameter for mobile primary
//        String _type1 = request.getParameter("_type");
        int _type = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.FAX_SETTING;
        if (_type == com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.FAX_SETTING) {
            if (_iPreference == com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE) {   //primary
                
                if(request.getParameter("_className")==null&&request.getParameter("_className").isEmpty())
                {
                    result="error";
                    message="Implementation classname can not be null";
                     try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
           
                }
                if(request.getParameter("_ip")==null&&request.getParameter("_ip").isEmpty())
                {
                    result="error";
                    message="Ip Address can not be null";
                     try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
           
                }
                
                 if(request.getParameter("_port")==null&&request.getParameter("_port").isEmpty())
                {
                    result="error";
                    message="Port can not be null";
                     try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
               log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
           
                }
                       if(request.getParameter("_phoneNumber")==null&&request.getParameter("_phoneNumber").isEmpty())
                {
                    result="error";
                    message="Fax no can not be null";
                     try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
               log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
           
                }
                        if(request.getParameter("_password")==null&&request.getParameter("_password").isEmpty())
                {
                    result="error";
                    message="Password  can not be null";
                     try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
           
                }
                 
                
                
                
                
                _className = request.getParameter("_className");
                _ip = request.getParameter("_ip");
                _logConfirmation1 = request.getParameter("_logConfirmation");
                if (_logConfirmation1 == null) {
                    _logConfirmation1 = "false";
                } else {
                    _logConfirmation1 = "true";
                }
                _password = request.getParameter("_password");
                _phoneNo = request.getParameter("_phoneNumber");
                _port = request.getParameter("_port");
                reserve1 = request.getParameter("_reserve1");
                reserve2 = request.getParameter("_reserve2");
                reserve3 = request.getParameter("_reserve3");
                _status1 = request.getParameter("_status");
                _userId = request.getParameter("_userId");
                _autofailover = request.getParameter("_autofailover");
                _retrycount = request.getParameter("_retries");
                _retryduration = request.getParameter("_retryduration");
            } else if (_iPreference == com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_TWO) {   //secondary
                //request parameter for mobile 
                
                   if(request.getParameter("_classNameS")==null&&request.getParameter("_classNameS").isEmpty())
                {
                    result="error";
                    message="Class Name  can not be null";
                     try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
           
                }
                   
                     if(request.getParameter("_ipS")==null&&request.getParameter("_ipS").isEmpty())
                {
                    result="error";
                    message="IP Address  can not be null";
                     try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
           
                }
                     
                         if(request.getParameter("_passwordS")==null&&request.getParameter("_passwordS").isEmpty())
                {
                    result="error";
                    message="Password  can not be null";
                     try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
           
                }
            if(request.getParameter("_phoneNumberS")==null&&request.getParameter("_phoneNumberS").isEmpty())
                {
                    result="error";
                    message="Fax no  can not be null";
                     try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
            }
                    
                         
                
                _className = request.getParameter("_classNameS");
                _ip = request.getParameter("_ipS");
                _logConfirmation1 = request.getParameter("_logConfirmationS");
                if (_logConfirmation1 == null) {
                    _logConfirmation1 = "false";
                } else {
                    _logConfirmation1 = "true";
                }
                _password = request.getParameter("_passwordS");
                _phoneNo = request.getParameter("_phoneNumberS");
                _port = request.getParameter("_portS");
                reserve1 = request.getParameter("_reserve1S");
                reserve2 = request.getParameter("_reserve2S");
                reserve3 = request.getParameter("_reserve3S");
                _status1 = request.getParameter("_statusS");
                _userId = request.getParameter("_userIdS");
                _retrycount = request.getParameter("_retriesS");
                _retryduration = request.getParameter("_retrydurationS");
            }

            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _type, _iPreference);

            FaxSetting fax = null;
            boolean bAddSetting = false;

            if (settingsObj == null) {
                fax = new FaxSetting();
                fax.setChannelId(_channelId);
//                fax.setType(SMS);
                fax.setPreference(_iPreference);
                bAddSetting = true;
            } else {
                fax = (FaxSetting) settingsObj;
            }

            fax.setClassName(_className);
            fax.setIp(_ip);
            if (_logConfirmation1 != null) {
                boolean _logConfirmation = Boolean.parseBoolean(_logConfirmation1);
                fax.setLogConfirmation(_logConfirmation);
            }
            fax.setPassword(_password);
            fax.setPhoneNumber(_phoneNo);
            if (_port != null || _port.isEmpty() == true) {
                int _port1 = Integer.parseInt(_port);
                fax.setPort(_port1);
            }
            fax.setReserve1(reserve1);
            fax.setReserve2(reserve2);
            fax.setReserve3(reserve3);
            if (_status1 != null) {
                int _status = Integer.parseInt(_status1);
                fax.setStatus(_status);
            }
            fax.setUserid(_userId);
            if (_autofailover != null) {
                int _iAutofailover = Integer.parseInt(_autofailover);
                fax.setAutofailover(_iAutofailover);
            }

            if (_retrycount != null) {
                int _iRetrycount = Integer.parseInt(_retrycount);
                fax.setRetrycount(_iRetrycount);
            }

            if (_retryduration != null) {
                int _iRetryDuration = Integer.parseInt(_retryduration);
                fax.setRetryduration(_iRetryDuration);
            }
            AuditManagement audit = new AuditManagement();
            if (bAddSetting == true) {
                retValue = sMngmt.addSetting(sessionId, _channelId, _type, _iPreference, fax);
                log.debug("editfaxsettings::addSetting::" + retValue);

                String resultString = "ERROR";
                if (retValue == 0) {
                    resultString = "SUCCESS";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add FAX Gateway Settings", resultString, retValue, 
                            "Setting Management",
                            "", "ChannelID=" + fax.getChannelId() + "Class Name=" + fax.getClassName()
                            + "IP=" + fax.getIp() + "Password=***" + "Phone Number=" + fax.getPhoneNumber() + "User ID=" + fax.getUserid()
                            + "Autofailover=" + fax.getAutofailover() + "Port=" + fax.getPort() + "Prefernece=" + fax.getPreference()
                            + "Reserve1=" + fax.getReserve1() + "Reserve2" + fax.getReserve2() + "Reserve3=" + fax.getReserve3()
                            + "RetryCount=" + fax.getRetrycount() + "Status=" + fax.getStatus() + "Type=" + fax.getType()
                            + "LogConfirmation=" + fax.isLogConfirmation() + "Retry Duration" + fax.getRetryduration(),
                            itemtype, _channelId);
                } else if (retValue != 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add FAX Gateway Settings", resultString, retValue, 
                            "Setting Management",
                            "", "Failed To Addd FAX Gateway Settings", itemtype,
                            _channelId);
                }
            } else {
                FaxSetting oldfaxObj = (FaxSetting) sMngmt.getSetting(sessionId, _channelId, _type, _iPreference);
                retValue = sMngmt.changeSetting(sessionId, _channelId, _type, _iPreference, settingsObj, fax);
                log.debug("editfaxsettings::changeSetting::" +retValue);

                String resultString = "ERROR";

                if (retValue == 0) {
                    resultString = "SUCCESS";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change FAX Gateway Settings", resultString, retValue, "Setting Management",
                            "ChannelID=" + oldfaxObj.getChannelId() + " Class Name=" + oldfaxObj.getClassName() + "ImplementationJar="
                            + " IP=" + oldfaxObj.getIp() + " Password=***" + " Phone Number=" + oldfaxObj.getPhoneNumber() + " User ID=" + oldfaxObj.getUserid()
                            + " Autofailover=" + oldfaxObj.getAutofailover() + " Port=" + oldfaxObj.getPort() + " Prefernece=" + oldfaxObj.getPreference()
                            + " Reserve1=" + oldfaxObj.getReserve1() + " Reserve2" + oldfaxObj.getReserve2() + " Reserve3=" + oldfaxObj.getReserve3()
                            + " RetryCount=" + oldfaxObj.getRetrycount() + " Status=" + oldfaxObj.getStatus() + " Type=" + oldfaxObj.getType()
                            + " LogConfirmation=" + oldfaxObj.isLogConfirmation() + " Retry Duration" + oldfaxObj.getRetryduration(),
                            " ChannelID=" + fax.getChannelId() + " Class Name=" + fax.getClassName() + " ImplementationJar="
                            + " IP=" + fax.getIp() + " Password=***" + " Phone Number=" + fax.getPhoneNumber() + " User ID=" + fax.getUserid()
                            + " Autofailover=" + fax.getAutofailover() + " Port=" + fax.getPort() + " Prefernece=" + fax.getPreference()
                            + " Reserve1=" + fax.getReserve1() + " Reserve2" + fax.getReserve2() + " Reserve3=" + fax.getReserve3()
                            + " RetryCount=" + fax.getRetrycount() + " Status=" + fax.getStatus() + " Type=" + fax.getType()
                            + " LogConfirmation=" + fax.isLogConfirmation() + " Retry Duration" + fax.getRetryduration(),
                            itemtype, _channelId
                            );
                } else if (retValue != 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change FAX Gateway Settings", resultString, retValue, "Setting Management",
                            "ChannelID=" + oldfaxObj.getChannelId() + " Class Name=" + oldfaxObj.getClassName() + "ImplementationJar="
                            + " IP=" + oldfaxObj.getIp() + " Password=***" + " Phone Number=" + oldfaxObj.getPhoneNumber() + " User ID=" + oldfaxObj.getUserid()
                            + " Autofailover=" + oldfaxObj.getAutofailover() + " Port=" + oldfaxObj.getPort() + " Prefernece=" + oldfaxObj.getPreference()
                            + " Reserve1=" + oldfaxObj.getReserve1() + " Reserve2" + oldfaxObj.getReserve2() + " Reserve3=" + oldfaxObj.getReserve3()
                            + " RetryCount=" + oldfaxObj.getRetrycount() + " Status=" + oldfaxObj.getStatus() + " Type=" + oldfaxObj.getType()
                            + " LogConfirmation=" + oldfaxObj.isLogConfirmation() + " Retry Duration" + oldfaxObj.getRetryduration(),
                            "Failed To Change FAX Gateway Settings", itemtype,_channelId );
                }



            }
        }

        if (retValue != 0) {
            result = "error";
            message = "FAX Gateway Settings Update Failed!!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch(Exception e){}finally {
            out.print(json);
            out.flush();
        }

    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);


    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);


    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";

    }// </editor-fold>
}
