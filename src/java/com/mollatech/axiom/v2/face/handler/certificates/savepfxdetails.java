/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.certificates;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.RootCertificateSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class savepfxdetails extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(savepfxdetails.class.getName());
    

    //final String itemType = "CHANNEL";
    //final String itemTypeRE = "REMOTEACCESS";
    public static final int PREFERENCE_ONE = 1;   //primary
    final String itemtype = "SETTINGS";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        log.info("is started :: ");
        PrintWriter out = response.getWriter();
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel::"+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId::"+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin::"+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS::"+operatorS.getOperatorid());

        AuditManagement audit = new AuditManagement();
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Root Certificate Details Added Sucessfully";

        String _pfxpassword = request.getParameter("_pfxpassword");
       
        String _pfxalias = request.getParameter("_pfxalias");
        log.debug("_pfxalias::"+_pfxalias);
        String xmlFileName = (String) request.getSession().getAttribute("_pfxfileupload");
        log.debug("xmlFileName::"+xmlFileName);
        

        if (_pfxpassword != null || xmlFileName != null || _pfxalias != null) {

            ChannelManagement cManagement = new ChannelManagement();
            String pfx = cManagement.getPFXINString(xmlFileName, _pfxpassword);

            if (pfx == null) {
                result = "error";
                message = "Failed to generate pfx!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    log.error("exception caught :: ",e);
                    
                }
                out.print(json);
                out.flush();
                return;
            }
            String resultStr = "ERROR";
            int res = cManagement.editRootCert(sessionId, channel.getChannelid(), pfx, _pfxpassword);
            log.debug("editRootCert::"+res);

            SettingsManagement sManagement = new SettingsManagement();

            RootCertificateSettings settingsObj = (RootCertificateSettings) sManagement.getSetting(sessionId, channel.getChannelid(), SettingsManagement.RootConfiguration, SettingsManagement.PREFERENCE_ONE);

            RootCertificateSettings certSetting = settingsObj;
            System.out.println("");
            int retValue = -2;
            if (settingsObj == null) {
                settingsObj = new RootCertificateSettings();
                settingsObj.reserve1 = xmlFileName;
                settingsObj.reserve2 = _pfxpassword;
                settingsObj.reserve3 = _pfxalias;
                retValue = sManagement.addSetting(sessionId, channel.getChannelid(), SettingsManagement.RootConfiguration, SettingsManagement.PREFERENCE_ONE, settingsObj);
                log.debug("savepfxdetails::addSetting::"+retValue);
            } else {
                settingsObj.setReserve1(xmlFileName); 
                settingsObj.setReserve2(_pfxpassword); 
                settingsObj.setReserve3(_pfxalias); 
                retValue = sManagement.changeSetting(sessionId, channel.getChannelid(), SettingsManagement.RootConfiguration, SettingsManagement.PREFERENCE_ONE, certSetting, settingsObj);
                log.debug("changeSetting::"+retValue);
            }

            if (res == 0) {

                String resultString = "ERROR";
               try {
                if (retValue == 0) {
                     
                      result = "SUCCESS";
                      
                    message = "Root Certificate Details Changed Sucessfully";
                    json.put("result", result);
                    json.put("message", message);
                    out.print(json);
                     out.flush();
                    
                    resultString = "SUCCESS";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change Certificate Setting", resultString, retValue, "Setting Management",
                            "ChannelID=" + certSetting.getChannelId() + " Class Name=" + certSetting.getClassName()
                            + " IP=" + certSetting.getIp() + " Password=***" + " User ID=" + certSetting.getUserId()
                            + " Port=" + certSetting.getPort()
                            + " Reserve1=" + certSetting.getReserve1() + " Reserve2" + certSetting.getReserve2() + " Reserve3=" + certSetting.getReserve3()
                            + " Status=" + certSetting.getStatus(),
                            " ChannelID=" + settingsObj.getChannelId() + " Class Name=" + settingsObj.getClassName() + " ImplementationJar="
                            + " IP=" + settingsObj.getIp() + " Password=***" + " Phone Number=" + "" + " User ID=" + settingsObj.getUserId()
                            + " Port=" + settingsObj.getPort()
                            + " Reserve1=" + settingsObj.getReserve1() + " Reserve2" + settingsObj.getReserve2() + " Reserve3=" + settingsObj.getReserve3() + "CAEmailides =" + settingsObj.CAEmailides
                            + "CRL link=" + settingsObj.CRL + "OCSPUrl= "
                            + settingsObj.OCSPUrl + "crlPassword= "
                            + settingsObj.crlPassword + "crlUserid= "
                            + settingsObj.crlUserid + "crlPoolTime= "
                            + settingsObj.crlPoolTime,
                            itemtype, "-");
                    
                } else if (retValue != 0) {
                      result = "ERROR";
                    message = "Root Certificate Details Change Failed";
                    json.put("result", result);
                    json.put("message", message);
                    out.print(json);
                     out.flush();
                     
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change Certificate Setting", resultString, retValue, "Setting Management",
                            "ChannelID=" + certSetting.getChannelId() + " Class Name=" + certSetting.getClassName()
                            + " IP=" + certSetting.getIp() + " Password=***" + " Phone Number=" + "" + " User ID=" + certSetting.getUserId()
                            + " Port=" + certSetting.getPort()
                            + " Reserve1=" + certSetting.getReserve1() + " Reserve2" + certSetting.getReserve2() + " Reserve3=" + certSetting.getReserve3()
                            + " RetryCount=" + "" + " Status=" + certSetting.getStatus() + "CAEmailides =" + settingsObj.CAEmailides
                            + "CRL link=" + settingsObj.CRL + "OCSPUrl= "
                            + settingsObj.OCSPUrl + "crlPassword= "
                            + settingsObj.crlPassword + "crlUserid= "
                            + settingsObj.crlUserid + "crlPoolTime= "
                            + settingsObj.crlPoolTime,
                            "Failed To Edit Setting...!!!", itemtype, 
                            "-");
                }

               
                  
//                    AuditManagement audit1 = new AuditManagement();
//                    resultStr = "SUCCESS";
//                    audit1.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
//                            request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
//                            operatorS.getName(), new Date(), "Edit Channel", resultStr, res, "Channel Management",
//                            "Channel Name =" + channel.getName() + "Channel V irtual Path =" + channel.getVpath() + " Status =" + channel.getStatus(),
//                            "Channel Name =" + channel.getName() + "Channel V irtual Path =" + channel.getVpath() + " Status =" + channel.getStatus() + "pfx= *************" + "pfxpassword= ********",
//                            itemType, operatorS.getOperatorid());
                    return;
                } catch (Exception e) {
                    log.error("exception caught :: ",e);
                    
                }
            } else {
                result = "error";
                message = "Failed to update root settingsObj!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);

//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
//                            request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
//                            operatorS.getName(), new Date(), "Edit Channel", resultStr, res, "Channel Management",
//                            "Channel Name =" + channel.getName() + "Channel V irtual Path =" + channel.getVpath() + "Old Status =" + channel.getStatus(),
//                            "Failed To Edit Channel", itemType, operatorS.getOperatorid());
                    
                } catch (Exception e) {
                    log.error("exception caught :: ",e);
                    
                }
                out.print(json);
                out.flush();
                return;
            }

        } else {
            result = "error";
            message = "Invalid Data!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
               
            }
            out.print(json);
            out.flush();
            log.info("is ended :: ");
            return;
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
