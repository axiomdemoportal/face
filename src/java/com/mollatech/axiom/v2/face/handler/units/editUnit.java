package com.mollatech.axiom.v2.face.handler.units;


import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;

import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 *
 * @author nilesh
 */
public class editUnit extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editUnit.class.getName());

 final String itemType = "UNITS";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    response.setContentType("application/json");
    
    log.info("is started :: ");

        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Unit updated successfully!!!";

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());
        JSONObject json = new JSONObject();
         
        int retValue = 0;
        String _unit_name = request.getParameter("_unitNameE");
        log.debug("_unit_name :: "+_unit_name);
         String _unitId = request.getParameter("_unitId");
         log.debug("_unitId :: "+_unitId);
         String _oldunitNameE = request.getParameter("_oldunitNameE");
         log.debug("_oldunitNameE :: "+_oldunitNameE);
//         String _newunitS = request.getParameter("_unitS");
//         int inewunitS = Integer.parseInt(_newunitS);
         
         String _thresholdLimitO = request.getParameter("_thresholdLimitO");
         log.debug("_thresholdLimitO :: "+_thresholdLimitO);
         String _thresholdLimit = request.getParameter("_thresholdLimitE");
         log.debug("_thresholdLimit :: "+_thresholdLimit);
        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(_unit_name);
        boolean b = m.find();
         
      
        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
      
        if (_unit_name == null || _oldunitNameE == null || _thresholdLimitO == null|| _unitId == null) {
            result = "error";
            message = "Invalid Parameters!!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){log.error("Exception caught :: ",e);}
            out.print(json);
            out.flush();
            return;
        }if (_unit_name.isEmpty() == true || _oldunitNameE.isEmpty() == true 
                || _thresholdLimitO.isEmpty() == true|| _unitId.isEmpty() == true) {
            result = "error";
            message = "Invalid Parameters!!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
         if (_unit_name.length() < 2) {
            result = "error";
            message = "Unit Name should be more than 3 characters !!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        } else if (b) {
            result = "error";
            message = "Unit name should not contain any symbols !!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        }
        int iunitId = Integer.parseInt(_unitId);
        
//        int ithresholdLimit = Integer.parseInt(_thresholdLimit);
        int ithresholdLimit = 0;
        try {
            ithresholdLimit = Integer.parseInt(_thresholdLimit);
        } catch (Exception ex) {
            log.error("exception caught :: ",ex);
            result = "error";
            message = "Threshold Limit must be integer!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        if(ithresholdLimit < 0){
            result = "error";
            message = "Threshold Limit must be +ve no!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        UnitsManagemet unMngt = new UnitsManagemet();
//        Units checkduplicate = unMngt.getUnitByUnitName(sessionId, channel.getChannelid(), _unit_name);
//        
//        if (checkduplicate != null) {      // 0 is present so it is duplication. report error
//            result = "error";
//            message = "Duplicate Name!!";
//            try {json.put("_result", result);
//            json.put("_message", message);
//            }catch(Exception e){log.error("Exception caught :: ",e);}
//            out.print(json);
//            out.flush();
//            return;
//        }
//        
        
        AuditManagement audit = new AuditManagement();
        
        retValue = unMngt.EditUnits(sessionId, channel.getChannelid(),iunitId, _unit_name,ithresholdLimit);
        log.debug("EditUnits :: "+retValue);

        String resultString = "Failure";
        

            if (retValue == 0) {
                resultString = "Success";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Edit Units", resultString, retValue, "Units Management",
                        "Name=" + _oldunitNameE+"Threshold Limit="+_thresholdLimitO,
                        "Name=" + _unit_name+"Threshold Limit="+ithresholdLimit,
                        itemType, operatorS.getOperatorid());

            }

            if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Edit Units", resultString, retValue, "Units Management",
                        "Name=" + _oldunitNameE+"Threshold Limit="+_thresholdLimitO,
                        "Failed To Edit Unit",
                        itemType, operatorS.getOperatorid());
                result = "error";
                message = "Units update failed!!!";
            }
        
        try {
            json.put("_result", result);
            json.put("_message", message);
        }catch(Exception e){
            log.error("exception caught :: ",e);
        }
        finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");

    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
