/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.operator;

import com.mollatech.axiom.nucleus.db.Audit;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.v2.face.handler.operator.getopraudits;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mollatech1
 */
public class sessionaudit extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(sessionaudit.class.getName());

    private static int PDF_TYPE = 0;
    private static int CSV_TYPE = 1;
    private static final int BUFSIZE = 4096;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        log.info("is started :: ");
        try {
            //response.setContentType("application/json");
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel.getName());
            //  String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            Operators operartorObj = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operartorObj :: "+operartorObj.getOperatorid());
            String _op_duration = request.getParameter("_duration");
            log.debug("_op_duration :: "+_op_duration);
            String sessionId1 = request.getParameter("_sessionId");
            log.debug("sessionId1 :: "+sessionId1);
            String _typeofId=request.getParameter("_sessionType");
             log.debug("_typeofId :: "+_typeofId);
            int tyepofSessionif=0;
            if(_typeofId!=null)
            {
            tyepofSessionif=Integer.parseInt(_typeofId);
            }
            sessionId1 = sessionId1.trim();
            sessionId1 = sessionId1.replaceAll(" ", "");
            sessionId1 = sessionId1.replaceAll("\n", "");

            String _format = request.getParameter("_format");
            log.debug("_format :: "+_format);

            int iFormat = CSV_TYPE;
            if (_format.compareTo("pdf") == 0) {
                iFormat = PDF_TYPE;
            }

            String filepath = null;

//            if (_operId == null || _op_duration == null
//                    || _operId.length() <= 0 || _op_duration.length() <= 0) {
//                return;
//            }
 Audit[] audit=null;
            try {
                try {
                    AuditManagement aManagement = new AuditManagement();
                    if(tyepofSessionif==0)
                    {
                    audit = aManagement.getSessionAuditrail(sessionId1);
                    }else
                    {
                     audit = aManagement.getSessionAuditrailActiveStatus(tyepofSessionif);
                    }
                    filepath = aManagement.generateReport(iFormat, operartorObj.getName() + "[" + operartorObj.getEmailid() + "]",
                            audit, _op_duration, channel.getName(), channel.getChannelid());
                    audit = null;
                } catch (Exception e) {
                    log.error("exception caught :: ",e);
                }

                File file = new File(filepath);
                int length = 0;
                ServletOutputStream outStream = response.getOutputStream();
                ServletContext context = getServletConfig().getServletContext();
                String mimetype = context.getMimeType(filepath);

                // sets response content type
                if (mimetype == null) {
                    mimetype = "application/octet-stream";
                }
                response.setContentType(mimetype);
                response.setContentLength((int) file.length());
                String fileName = (new File(filepath)).getName();

                // sets HTTP header
                response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

                byte[] byteBuffer = new byte[BUFSIZE];
                DataInputStream in = new DataInputStream(new FileInputStream(file));

                // reads the file's bytes and writes them to the response stream
                while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                    outStream.write(byteBuffer, 0, length);
                }

                in.close();
                outStream.close();
                file.delete();

            } catch (Exception ex) {
                
                log.error("exception caught :: ",ex);
                // TODO handle custom exceptions here
                log.error("Exception caught :: ",ex);
            }

        } catch (Exception ex) {
            Logger.getLogger(getopraudits.class.getName()).log(Level.SEVERE, null, ex);
            log.error("exception caught :: ",ex);
        }
        
        log.info("is ended :: ");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
