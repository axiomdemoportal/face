/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.axiom.v2.face.handler.channel;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import static com.mollatech.axiom.common.utils.UtilityFunctions.context;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Roles;
import com.mollatech.axiom.nucleus.db.Units;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PasswordTrailManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import static com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PASSWORD_POLICY_SETTING;
import static com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE;
import com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet;
import com.mollatech.axiom.nucleus.db.operation.AxiomOperator;
import com.mollatech.axiom.nucleus.settings.ChannelSettings;
import com.mollatech.axiom.nucleus.settings.PasswordPolicySetting;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class addldapoperator extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(addldapoperator.class.getName());

  
    
    final int SYS_ADMIN = 1;
    final int ADMIN = 2;
    final int HELPDESK = 3;
    final int REPORTER = 4;
    final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;
    final int LOCKED_STATUS = -1;
    final int REMOVE_STATUS = -99;
    
    final String itemTypeOp = "OPERATOR";
    final int FIRST_OPERATOR_AS_0 = 0 ;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException {
        response.setContentType("application/json");
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel::"+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId::"+sessionId);
        Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin::"+remoteaccesslogin);
        String operatorId = operator.getOperatorid();
        log.debug("operatorId::"+operatorId);
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "SUCCESS";
        String message = "Operator Added Successfully!!!";
        int retValue = 0;
        String _op_name = request.getParameter("_operatorname");
        log.debug("_op_name::"+_op_name);
        String _op_phone = request.getParameter("_operatormobile");
        log.debug("_op_phone::"+_op_phone);
        String _op_email = request.getParameter("_operatoremail");
        log.debug("_op_email::"+_op_email);
        String _op_role = request.getParameter("_role");
        log.debug("_op_role::"+_op_role);
        String _units = request.getParameter("_NameE");
        log.debug("_units::"+_units);
        String commonName = "";
        String email = "";
        String phonno = "";
        
     
        
        if (_op_name == null || _op_phone == null || _op_email == null
                || _op_name.isEmpty() == true || _op_phone.isEmpty() == true || _op_email.isEmpty() == true
                || _units == null
                ) {
            result = "error";
            message = "Invalid OR Empty Data!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("exception caught :: ",ex);
                
            }
            out.print(json);
            out.flush();
            return;
        }
                SettingsManagement sMngt = new SettingsManagement();
                Object ipobj = sMngt.getSettingInner(channel.getChannelid(), SettingsManagement.GlobalSettings, 1);
                ChannelSettings chSettings = new ChannelSettings();
                Object settingsObj = null;
                boolean type = true;
                settingsObj = sMngt.getSetting(channel.getChannelid(), SettingsManagement.CHANNEL_SETTINGS, SettingsManagement.PREFERENCE_ONE);
                if (settingsObj != null) {
                    chSettings = (ChannelSettings) settingsObj;
                    type = chSettings.isSourceType();
                }
//              if(type==false)
//              {
//                    String host = chSettings.getExternalhost();
//                    String port = String.valueOf(chSettings.getPort());
//                    String dn = chSettings.getDatabaseName();
//                    String table = chSettings.getTableName();
//                    String _username=chSettings.getUserId();
//                    String _upass=chSettings.getPassword();
//             
//                    DirContext Dircontext = null;
//                    Hashtable env = new Hashtable();
//                    String provider_url = "ldap://".concat(host).concat(":").concat((port));
//                    env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
//                    env.put(Context.SECURITY_AUTHENTICATION, "simple");
//                    env.put(Context.SECURITY_PRINCIPAL, _username);
//                    env.put(Context.SECURITY_CREDENTIALS, _upass);
//                    env.put(Context.PROVIDER_URL, provider_url);
//                    env.put("java.naming.ldap.factory.socket", "com.mollatech.axiom.common.utils.MySSLSocketFactory");
//                     
//                    try {
//                        Dircontext = new InitialDirContext(env);
//                        if (Dircontext != null) {
//
//                        }
//                    } catch (NamingException e) {
//                        result = "error";
//                        message = "LDAP user failed to authenticate.";
//                        json.put("_result", result);
//                        json.put("_message", message);
//                        out.print(json);
//                        out.flush();
//                         return;
//                    }
//                  NamingEnumeration results = null;
//                  try {
//                      Attributes matchAttrs = new BasicAttributes(true);
//                      matchAttrs.put(new BasicAttribute(table, _op_name));
//                      NamingEnumeration answer = Dircontext.search(_username, matchAttrs);
//                    
//                     if(answer==null)
//                     {
//                        result = "error";
//                        message = "User Does not present Ldap";
//                        json.put("_result", result);
//                        json.put("_message", message);
//                        out.print(json);
//                        out.flush();
//                        return;
//                         
//                     }
//                      while (answer.hasMore()) {
//                          SearchResult o = (SearchResult) answer.next();
//                          commonName = o.getAttributes().get(chSettings.getOperatorname()).toString();
//                          email = o.getAttributes().get(chSettings.getMail()).toString();
//                          phonno = o.getAttributes().get(chSettings.getMobile()).toString();
//                      }
//                     
//                                     
//                    }catch(Exception ex)
//                    {
//                        log.error("Exception caught :: ",ex);
//                    }
//                 
//                  
//              }
          UnitsManagemet umanage= new UnitsManagemet();
       Units Templates=umanage.getUnitByUnitName(sessionId, channel.getChannelid(), _units);
        if(Templates==null)
        {
            result = "error";
            message = "The Unit Not found..!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("exception caught :: ",ex);
                
            }
            out.print(json);
            out.flush();
            return;
        }
             OperatorsManagement om = new OperatorsManagement();
        AxiomOperator[] ops = om.ListOperatorsInternal(channel.getChannelid());
        int iCurrentOperatorCount = ops.length;

        int iOperatorCount = AxiomProtect.GetOperatorsAllowed();

        if (iCurrentOperatorCount < 1) {
            result = "error";
            message = "Operator Addition is not available in this license!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
                
            }
            out.print(json);
            out.flush();
            return;
        }

        
        /// licensec check
        if ( iOperatorCount != -998 ) 
            if (iCurrentOperatorCount >= iOperatorCount) {
                result = "error";
                message = "Operator Addition already reached its limit as per this license!!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    log.error("exception caught :: ",e);
                    
                }
                out.print(json);
                out.flush();
                return;
            }

        UtilityFunctions u = new UtilityFunctions();
        // Date d = new Date();
        Date dDated = new Date();
        String _op_password = u.HexSHA1(_op_name + _op_phone + _op_email + dDated.toString());//request.getParameter("_op_password");
        _op_password = _op_password.substring(0, 8);

//        SettingsManagement setObj = new SettingsManagement();
        PasswordTrailManagement pass = new PasswordTrailManagement();

        String password = pass.GeneratePassword(channel.getChannelid(), _op_password);
        PasswordTrailManagement setObj = new PasswordTrailManagement();
         SettingsManagement settObj = new SettingsManagement();
            Object obj = settObj.getSettingInner(channel.getChannelid(), PASSWORD_POLICY_SETTING, PREFERENCE_ONE);
        if (obj != null) {
            if (obj instanceof PasswordPolicySetting) {
                PasswordPolicySetting passwordSetting = (PasswordPolicySetting) obj;
                  String[] strOldPassword = setObj.getPasswordUsingOpId(channel.getChannelid(),operator.getOperatorid() , passwordSetting.passwordGenerations);
              String pas = setObj.MD5HashPassword(password);
              String str = "";
              if(strOldPassword != null){
                  for(int i = 0;i<strOldPassword.length;i++){
                      str = str +","+strOldPassword[i];
                  }
              }
              if(str.contains(pas)){
                    result = "error";
                    message = "Password already used in previous generations...!!!";
                    try { json.put("_result", result);
                    json.put("_message", message);}catch(Exception e){log.error("Exception caught :: ",e);}
                    out.print(json);
                    out.flush();
                    return;
              }
            }
        }
        

//        if(password != null){
//            _op_password = password;
//        }
        if (password != null || password.isEmpty() == true) {
            _op_password = password;
        }

        String roleNames = null;
        int _op_status = 1; //active by default
        int checkValue = 0;
       
        
        
        int _op_roleID = Integer.valueOf(_op_role.split(",")[0]);
        roleNames=_op_role.split(",")[1];
//        if (_op_roleID == ADMIN) {
//            roleNames = "ADMIN";
//        } else if (_op_roleID == HELPDESK) {
//            roleNames = "HELPDESK";
//        } else if (_op_roleID == REPORTER) {
//            roleNames = "REPORTER";
//        } else if (_op_roleID == SYS_ADMIN) {
//            roleNames = "SYS_ADMIN";
//        }
        //System.out.println(password);
        OperatorsManagement oManagement = new OperatorsManagement();

        Roles roleObj = oManagement.getRoleByRoleId(channel.getChannelid(), operator.getRoleid());

        if (roleObj.getName().equals(OperatorsManagement.admin) && roleNames.equals("SYS_ADMIN")) {
            result = "error";
            message = "Admin Don't Have Permissions to create Sysadmin!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
                
            } finally {
                out.print(json);
                out.flush();
                return;
            }
        }
        
//        int iUnit = Integer.parseInt(_units);

        checkValue = oManagement.checkIsUniqueInChannelforLdap(channel.getChannelid(), _op_name, _op_email, _op_phone);
        if (checkValue == 0) {
//            
//            if(type==true)
//            {
            
            retValue = oManagement.AddOperator(sessionId, channel.getChannelid(),
                    _op_name,
                    _op_password,
                    _op_phone,
                    _op_email,
                    _op_roleID,
                    _op_status,
                    Templates.getUnitid(),FIRST_OPERATOR_AS_0);
            log.debug("AddOperator::"+retValue);
//            }else{
//            
//                 retValue = oManagement.AddOperator(sessionId, channel.getChannelid(),
//                    commonName,
//                    _op_password,
//                    phonno,
//                    email,
//                    _op_roleID,
//                    _op_status,
//                    Templates.getUnitid(),FIRST_OPERATOR_AS_0);
//            
//            }
            if (retValue == 0) {
                AxiomOperator op = oManagement.GetOperatorByName(sessionId, channel.getChannelid(), _op_name);
                String pas = pass.MD5HashPassword(_op_password);
                pass.AddPasswordTrail(channel.getChannelid(), op.getStrOperatorid(), pas);
            }

            AuditManagement audit = new AuditManagement();
            String strRole = "";

            if (_op_roleID == ADMIN) {
                strRole = "ADMIN";
            } else if (_op_roleID == HELPDESK) {
                strRole = "HELPDESK";
            } else if (_op_roleID == REPORTER) {
                strRole = "REPORTER";
            } else if (_op_roleID == SYS_ADMIN) {
                strRole = "SYS_ADMIN";
            }

            String strStatus = "";
            if (_op_status == ACTIVE_STATUS) {
                strStatus = "ACTIVE_STATUS";
            } else if (_op_status == LOCKED_STATUS) {
                strStatus = "LOCKED_STATUS";
            } else if (_op_status == REMOVE_STATUS) {
                strStatus = "REMOVE_STATUS";
            } else if (_op_status == SUSPEND_STATUS) {
                strStatus = "SUSPEND_STATUS";
            }

            String resultString = "ERROR";

            if (retValue == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                        "Add Operator", resultString, retValue, "Operator Management",
                        "", "Name=" + _op_name + ",Password=*****" +/*_op_password+*/ ",Phone" + _op_phone
                        + ",Email=" + _op_email + ",Role=" + strRole + ",Status=" + strStatus,
                        itemTypeOp,
                        operatorId);
            }

            if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                        "Add Operator", resultString, retValue, "Operator Management",
                        "", "Failed To add Operator",
                        itemTypeOp, 
                        operatorId);
                result = "error";
                message = "Adding Operator failed!!";
            }
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                //Logger.getLogger(resendpassword.class.getName()).log(Level.SEVERE, null, ex);
                log.error("exception caught :: ",ex);
                
            } finally {
                out.print(json);
                out.flush();
            }
        } else {
            result = "error";
            message = "Details are also taken, provide unique credentials!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
                
            } finally {
                out.print(json);
                out.flush();
            }
        }
        log.info("is ended :: ");
        
        
    }
    
        
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(addldapoperator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(addldapoperator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
