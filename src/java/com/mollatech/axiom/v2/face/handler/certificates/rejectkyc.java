/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.certificates;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.KYCManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.RootCertificateSettings;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class rejectkyc extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(rejectkyc.class.getName());

    final String itemType = "PKITOKEN";
    //final String itemTypeAUTH = "AUTHORIZATION";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        log.info("is started :: ");
        PrintWriter out = response.getWriter();
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel::"+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId::"+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin::"+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS::"+operatorS.getOperatorid());

        AuditManagement audit = new AuditManagement();
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "KYC request reject sucessfully!!!";

        String _usersid = request.getParameter("_usersid");
        log.debug("_usersid::"+_usersid);
        String authenticationtype = request.getParameter("authenticationtype");
        log.debug("authenticationtype::"+authenticationtype);
        String _description = request.getParameter("_description");
        log.debug("_description::"+_description);
        
      
        
        try {
            if (_usersid == null || authenticationtype == null || authenticationtype == null) {
                message = "Details not found!!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    log.error("exception caught :: ",e);
                    
                }
                out.print(json);
                out.flush();
                return;
            }

             SettingsManagement  sManagement = new SettingsManagement();
            RootCertificateSettings rSettings =(RootCertificateSettings) sManagement.getSetting(channel.getChannelid(), SettingsManagement.RootConfiguration, SettingsManagement.PREFERENCE_ONE);
            
            if(rSettings == null){
                result = "error";
                message = "Certificate Settings are Not Configured!!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    log.error("exception caught :: ",e);
                    
                }
                out.print(json);
                out.flush();
                return;
            }
            
            int res = -1;
            KYCManagement kManagement = new KYCManagement();
            if (authenticationtype.equals("Needmoredoc")) {
                res = kManagement.changeKycStatus(sessionId, channel.getChannelid(), _usersid, CertificateManagement.KYC_NEEDMOREDOCS, authenticationtype, _description);
                log.debug("changeKycStatus::"+res);
            } else {
                res = kManagement.changeKycStatus(sessionId, channel.getChannelid(), _usersid, CertificateManagement.KYC_REJECTED, authenticationtype, _description);
                log.debug("changeKycStatus::"+res);
            }
            UserManagement uManagement = new UserManagement();
            AuthUser aUser = uManagement.getUser(sessionId, channel.getChannelid(), _usersid);

            if (aUser == null) {
                result = "error";
                message = "User Not Found!!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    log.error("exception caught :: ",e);
                    
                }
                out.print(json);
                out.flush();
                return;
            }
             Templates temp = null;
            TemplateManagement tObj = new TemplateManagement();
            if(rSettings.kycalert == CertificateManagement.SMS || rSettings.kycalert == CertificateManagement.SMS_AND_EMAIL){
            temp = tObj.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.MOBILE_ASSIGN_KYC_CERT_APPROVE);

            if (temp.getStatus() == tObj.ACTIVE_STATUS ) {
                ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                Date d = new Date();
                String tmessage = (String) TemplateUtils.deserializeFromObject(bais);
                tmessage = tmessage.replaceAll("#name#", aUser.getUserName());
                tmessage = tmessage.replaceAll("#channel#", channel.getName());
                tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                tmessage = tmessage.replaceAll("#password#", "");
             
                SendNotification send = new SendNotification();
                AXIOMStatus status = send.SendOnMobileNoWaiting(channel.getChannelid(), aUser.getPhoneNo(), tmessage, 1, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
            }
               
            }else if(rSettings.kycalert == CertificateManagement.EMAIL || rSettings.kycalert == CertificateManagement.SMS_AND_EMAIL){
                 temp = tObj.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.EMAIL_ASSIGN_KYC_CERT_APPROVE);

            if (temp.getStatus() == tObj.ACTIVE_STATUS ) {
                ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                Date d = new Date();
                String subject = temp.getSubject();
                String tmessage = (String) TemplateUtils.deserializeFromObject(bais);
                tmessage = tmessage.replaceAll("#name#", aUser.getUserName());
                tmessage = tmessage.replaceAll("#channel#", channel.getName());
                tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                tmessage = tmessage.replaceAll("#email#", aUser.getEmail());
                subject = subject.replaceAll("#name#", aUser.getUserName());
                subject = subject.replaceAll("#channel#", channel.getName());
                subject = subject.replaceAll("#datetime#", sdf.format(d));            
                SendNotification send = new SendNotification();
                AXIOMStatus status = send.SendEmail(channel.getChannelid(), aUser.getEmail(), subject, tmessage, null, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
            
            }
            }
                String resultString = "ERROR";
                String strCategory = "";
                String ipaddress = request.getRemoteAddr();
                if (res == 0) {
                    resultString = "Success";

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            //ipaddress,
                            channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), "REJECT KYC", resultString, res,
                            "Certificate Management", "", " ",
                            itemType, _usersid);

                } else {
                    resultString = "ERROR";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            //ipaddress, 
                            channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), "REJECT KYC", resultString, res,
                            "Certificate Management", "", " ",
                            itemType, _usersid);

                    result = "error";
                    message = "Failed to reject Pending Request!!!";
                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("exception caught :: ",e);
                        
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    log.error("exception caught :: ",e);
                    
                } finally {
                    out.print(json);
                    out.flush();
                }

            }catch (Exception ex) {
            log.error("exception caught :: ",ex);
            
        }finally {
            out.close();
        }
        log.info("is ended :: ");

        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet
        (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            processRequest(request, response);
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost
        (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            processRequest(request, response);
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo
        
            () {
        return "Short description";
        }// </editor-fold>

    }
