package com.mollatech.axiom.v2.face.handler.otptokens;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Otptokens;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import com.mollatech.axiom.v2.face.handler.operator.getopraudits;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mollatech1
 */
public class otptokensreport extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(otptokensreport.class.getName());

    private static int PDF_TYPE = 0;
    private static int CSV_TYPE = 1;
    private static int TEXT_TYPE = 2;
    private static final int BUFSIZE = 4096;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        log.info("is started :: ");
        try {
            try {

                Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                log.debug("channel :: "+channel.getName());
                String sessionId = (String) request.getSession().getAttribute("_apSessionID");
                log.debug("sessionId :: "+sessionId);
                Otptokens[] obj = (Otptokens[])request.getSession().getAttribute("otpReportRecord");
                log.debug("sessionId :: "+sessionId);

                String _category = request.getParameter("_changeCategory");
                log.debug("_category :: "+_category);
                String _changeStatus = request.getParameter("_changeOTPStatus");
                log.debug("_changeStatus :: "+_changeStatus);
                String _format = request.getParameter("_reporttype");
                log.debug("_format :: "+_format);
                 String _UnitId = request.getParameter("_UnitId");
                 log.debug("_UnitId :: "+_UnitId);

                int icategory = -9999;
                if (_category != null && !_category.isEmpty()) {
                    icategory = Integer.parseInt(_category);
                }
                int istatus = -9999;
                if (_changeStatus != null && !_changeStatus.isEmpty()) {
                    istatus = Integer.parseInt(_changeStatus);
                }
                int iFormat = -9999;
                if (_format != null && !_format.isEmpty()) {
                    iFormat = Integer.parseInt(_format);
                }
                int iUnitId = 0;
            if (_UnitId != null) {
                iUnitId = Integer.parseInt(_UnitId);
            }
                if (PDF_TYPE == iFormat) {
                    iFormat = PDF_TYPE;
                } else if (CSV_TYPE == iFormat){
                    iFormat = CSV_TYPE;
                }else{
                    iFormat = TEXT_TYPE;
                }

                String filepath = null;


                try {
                    try {
                        OTPTokenManagement oObj = new OTPTokenManagement(channel.getChannelid());

//                        OtpReport[] obj = oObj.getOtpReportObj(sessionId, channel.getChannelid(), icategory, istatus);
                        //Otptokens[]  obj = oObj.searchOtpObjByStatus(channel.getChannelid(), icategory, istatus, iUnitId);
                        filepath = oObj.generateReport(iFormat, sessionId, channel.getChannelid(), obj,icategory);
                        obj = null;
                    } catch (Exception e) {
                        log.error("exception caught :: ",e);
                    }

                    File file = new File(filepath);
                    int length = 0;
                    ServletOutputStream outStream = response.getOutputStream();
                    ServletContext context = getServletConfig().getServletContext();
                    String mimetype = context.getMimeType(filepath);

                    // sets response content type
                    if (mimetype == null) {
                        mimetype = "application/octet-stream";
                    }
                    response.setContentType(mimetype);
                    response.setContentLength((int) file.length());
                    String fileName = (new File(filepath)).getName();

                    // sets HTTP header
                    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

                    byte[] byteBuffer = new byte[BUFSIZE];
                    DataInputStream in = new DataInputStream(new FileInputStream(file));

                    // reads the file's bytes and writes them to the response stream
                    while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                        outStream.write(byteBuffer, 0, length);
                    }

                    in.close();
                    outStream.close();
                    file.delete();

                } catch (Exception ex) {
                    // TODO handle custom exceptions here
                    log.error("exception caught :: ",ex);
                }


            } catch (Exception ex) {
                Logger.getLogger(getopraudits.class.getName()).log(Level.SEVERE, null, ex);
                log.error("exception caught :: ",ex);
            }




        } finally {
            //  out.close();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
