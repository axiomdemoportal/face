/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.TokenSettings;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 *
 * @author mollatech1
 */
public class loadotpsettings extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadotpsettings.class.getName());
    private JSONObject SettingsWhenEmpty(int _type1, int _preference1, Object settingsObj) {
        JSONObject json = new JSONObject();
        if (_type1 == com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.Token && _preference1 == com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE) {
            TokenSettings tokenObj = (TokenSettings) settingsObj;
            try {
                json.put("_OTPLength", "");
                json.put("_SOTPLength", "");
                json.put("_OATHAlgoType", "");
                json.put("_SWOTPLength", "");
                json.put("_SWSOTPLength", "");
                json.put("_SWOATHAlgoType", "");
                json.put("_OTPAttempts", "");
                json.put("_hashalgo", "SHA1");
                //json.put("_SOTPAttempts", "");       
                json.put("_ValidationSteps", "");
                json.put("_duration", "");
                json.put("_MultipleToken", "");
                json.put("_RegistrationExpiryTime", "");
                json.put("_EnforceMasking", "");

                json.put("_allowAlertT", false);
                json.put("_gatewayTypeT", 1);
                //json.put("_thresholdCount", "");  
                json.put("_SWWebTokenExpiryTime", 5);
                json.put("_SWWebTokenPinAttempt", 3);
                json.put("_autoUnlockAfter", 3);
                
                json.put("_otpExpiryAfterSession", 0);
                json.put("_otpLockedAfter", 10);
                
                json.put("_otpExpiryAfterSessionSW", 0);
                json.put("_otpLockedAfterSW", 10);
                
                json.put("_HWOTPLength", "");
                json.put("_HWSOTPLength", "");
                json.put("_HWOATHAlgoType", "");
                json.put("_otpExpiryAfterSessionHW", 0);
                json.put("_otpLockedAfterHW", 10);
                
                json.put("_autoUnlockOOBAfter", 3);
                json.put("_autoUnlockSWAfter", 3);
                json.put("_autoUnlockHWAfter", 3);
                
                json.put("_pukCodeLengthHW", 4);
                json.put("_pukCodeValidityHW", 60);
            } catch (Exception e) {
            }

        }
        return json;
    }

    private JSONObject SettingsWhenPresent(Object settingsObj) {
        JSONObject json = new JSONObject();
        if (settingsObj instanceof TokenSettings) {
            TokenSettings tokenObj = (TokenSettings) settingsObj;
                   try {
                json.put("_OTPLength", tokenObj.getOtpLengthOOBToken());
                json.put("_SOTPLength", tokenObj.getSotpLengthOOBToken());
                json.put("_OATHAlgoType", tokenObj.getOOBAlgoType());
                json.put("_SWOTPLength", tokenObj.getOtpLengthSWToken());
                json.put("_SWSOTPLength", tokenObj.getSotpLengthSWToken());
                json.put("_SWOATHAlgoType", tokenObj.getSwAlgoType());
                json.put("_OTPAttempts", tokenObj.getOtpAllowed());
                 json.put("_hashalgo", tokenObj.hashAlgo);
                //json.put("_SOTPAttempts", tokenObj.getSotpAllowed());       
                json.put("_ValidationSteps", tokenObj.getOtpSteps());
                json.put("_duration", tokenObj.getOtpDurationInSeconds());
                json.put("_MultipleToken", tokenObj.IsResetAttemptsForAllTokens());
                json.put("_RegistrationExpiryTime", tokenObj.getRegistrationValidity());
                json.put("_EnforceMasking", tokenObj.isbEnforceMasking());

                json.put("_allowAlertT", tokenObj.isTokenAlert());
                json.put("_gatewayTypeT", tokenObj.getTokenAlertVia());
//              json.put("_thresholdCount", tokenObj.getTokenThresholdCount()); 
                json.put("_SWWebTokenExpiryTime", tokenObj.getiSWWebTokenExpiryTime());
                json.put("_SWWebTokenPinAttempt", tokenObj.getiSWWebTokenPinAttempt());
                json.put("_autoUnlockAfter", tokenObj.getAutounlockWEBAfter());
                
                json.put("_otpExpiryAfterSession", tokenObj.getOtpExpiryAfterSession());
                json.put("_otpLockedAfter", tokenObj.getOtpLockAfterXDays());
                
                json.put("_otpExpiryAfterSessionSW", tokenObj.getOtpExpiryAfterSessionSW());
                json.put("_otpLockedAfterSW", tokenObj.getOtpLockAfterXDaysSW());
                
                json.put("_HWOTPLength", tokenObj.getOtpLengthHWToken());
                json.put("_HWSOTPLength", tokenObj.getSotpLengthHWToken());
                json.put("_HWOATHAlgoType", tokenObj.getHWAlgoType());
                json.put("_otpExpiryAfterSessionHW", tokenObj.getOtpExpiryAfterSessionHW());
                json.put("_otpLockedAfterHW", tokenObj.getOtpLockAfterXDaysHW());
                
                json.put("_autoUnlockOOBAfter", tokenObj.getAutounlockOOBAfter());
                json.put("_autoUnlockSWAfter",  tokenObj.getAutounlockSWAfter());
                json.put("_autoUnlockHWAfter",  tokenObj.getAutounlockHWAfter());
                
                json.put("_pukCodeLengthHW", tokenObj.getPukCodeLengthHW());
                json.put("_pukCodeValidityHW", tokenObj.getPukCodeValidityHW());
            } catch (Exception e) {
            }

        }

        return json;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        // String _preference = request.getParameter("_preference");
        int _preference1 = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE;
        int _type1 = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.Token;

        JSONObject json = null;//new JSONObject();
        PrintWriter out = response.getWriter();

        try {
            SettingsManagement sMngmt = new SettingsManagement();
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _type1, _preference1);
            if (settingsObj != null) {
                json = SettingsWhenPresent(settingsObj);
            } else {
                TokenSettings tokenObj = new TokenSettings();
                tokenObj.setChannelId(channel.getChannelid());
                //json = SettingsWhenEmpty(_type1, _preference1, settingsObj);
                json = SettingsWhenPresent(tokenObj);
            }

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
            // TODO handle custom exceptions here
        }
        try {
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
