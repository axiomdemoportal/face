/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.otptokens;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author nilesh
 */
public class losttoken extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(losttoken.class.getName());

  final String itemType = "OTPTOKENS";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);

        String message = null;

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());

        String _userid = request.getParameter("_userid");
        log.debug("_userid :: "+_userid);
        String _status = request.getParameter("_status");
        log.debug("_status :: "+_status);
        int status = 0;
        String _value = "Active";
        if (_status != null) {
            status = Integer.parseInt(_status);
        }
        String _subcategory = request.getParameter("_subcategory");
        log.debug("_subcategory :: "+_subcategory);
        int subCategory = 0;
        if (_subcategory != null) {
            subCategory = Integer.parseInt(_subcategory);
        }

        String _category = request.getParameter("_category");
        log.debug("_category :: "+_category);
        int category = 0;
        if (_category != null) {
            category = Integer.parseInt(_category);
        }
        String result = "success";
        String _message = "Token Removed successful!!!";
        
        OTPTokenManagement otpMngt = new OTPTokenManagement(channel.getChannelid());
        AuditManagement audit = new AuditManagement();
        
        int istatus = otpMngt.getStatus(sessionId, channel.getChannelid(), _userid, category, subCategory);
        log.debug("getStatus :: "+istatus);
        String strStatus = "";
        if (istatus == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
            strStatus = "TOKEN_STATUS_ACTIVE";
        } else if (istatus == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
            strStatus = "TOKEN_STATUS_ASSIGNED";
        } else if (istatus == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
            strStatus = "TOKEN_STATUS_LOCKEd";
        } else if (istatus == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
            strStatus = "TOKEN_STATUS_SUSPENDED";
        } else if (istatus == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
            strStatus = "TOKEN_STATUS_UNASSIGNED";
        } else if (istatus == OTPTokenManagement.TOKEN_STATUS_LOST) {
            strStatus = "TOKEN_STATUS_LOST";
        }
        String strnewStatus = "";
        String strnewStatusA = "";
        if (status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
            strnewStatus = "TOKEN_STATUS_ACTIVE";
            strnewStatusA = "Active";
        } else if (status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
            strnewStatus = "TOKEN_STATUS_ASSIGNED";
            strnewStatusA = "Assign";
        } else if (status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
            strnewStatus = "TOKEN_STATUS_LOCKEd";
            strnewStatusA = "Locked";
        } else if (status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
            strnewStatus = "TOKEN_STATUS_SUSPENDED";
            strnewStatusA = "Suspended";
        } else if (status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
            strnewStatus = "TOKEN_STATUS_UNASSIGNED";
            strnewStatusA = "Un-Assign";
        } else if (status == OTPTokenManagement.TOKEN_STATUS_LOST) {
            strnewStatus = "TOKEN_STATUS_LOST";
            strnewStatusA = "Lost";
        }
        String strCategory = "";
        String strSubCategory = "";

        String tokentype = "";
        String tokensubtype = "";
        if (category == OTPTokenManagement.SOFTWARE_TOKEN) {
            strCategory = "SOFTWARE_TOKEN";
            tokentype = "SW-Token";

            if (subCategory == OTPTokenManagement.SW_WEB_TOKEN) {
                strSubCategory = "SW_WEB_TOKEN";
                tokensubtype = "Web Token";
            } else if (subCategory == OTPTokenManagement.SW_MOBILE_TOKEN) {
                strSubCategory = "SW_MOBILE_TOKEN";
                tokensubtype = "Mobile Token";
            } else if (subCategory == OTPTokenManagement.SW_PC_TOKEN) {
                strSubCategory = "SW_PC_TOKEN";
                tokensubtype = "PC Token";
            }

        } else if (category == OTPTokenManagement.HARDWARE_TOKEN) {
            tokentype = "HW-Token";
            strCategory = "HARDWARE_TOKEN";
            if (subCategory == OTPTokenManagement.HW_MINI_TOKEN) {
                strSubCategory = "HW_MINI_TOKEN";
                tokensubtype = "HW Mini Token";
            } else if (subCategory == OTPTokenManagement.HW_CR_TOKEN) {
                strSubCategory = "HW_CR_TOKEN";
                tokensubtype = "HW CR Token";
            }
        } else if (category == OTPTokenManagement.OOB_TOKEN) {
            strCategory = "OOB_TOKEN";
            tokentype = "OOB-Token";
            if (subCategory == OTPTokenManagement.OOB__SMS_TOKEN) {
                strSubCategory = "OOB__SMS_TOKEN";
                tokensubtype = "Sms";
            } else if (subCategory == OTPTokenManagement.OOB__USSD_TOKEN) {
                strSubCategory = "Ussd";
            } else if (subCategory == OTPTokenManagement.OOB__VOICE_TOKEN) {
                strSubCategory = "Voice";
            } else if (subCategory == OTPTokenManagement.OOB__EMAIL_TOKEN) {
                strSubCategory = "Email";
            }
        }
       int retValue =  otpMngt.LostToken(sessionId, channel.getChannelid(), _userid, status, category, subCategory);
       log.debug("LostToken :: "+retValue);
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String resultString = "ERROR";
       if(retValue == 0){
           resultString = "SUCCESS";
             audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Change OTP token Status as Lost", resultString, retValue,
                    "Token Management", "Category =" + strCategory + "SubCategory =" + strSubCategory + "Old Status =" + strStatus,
                    "Category =" + strCategory + "SubCategory =" + strSubCategory + "New Status =" + strnewStatus,
                    itemType, 
                    _userid);

             result = "success";
            _message = "Change OTP token Status As Lost Successful!!!";
            try {
                json.put("_result", result);
                json.put("_message", _message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
             
       }else{
           
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Change OTP token Status as Lost", resultString, retValue,
                    "Token Management", "Category =" + strCategory + "SubCategory =" + strSubCategory + "Old Status =" + strStatus,
                    "Category =" + strCategory + "SubCategory =" + strSubCategory + "New Status =" + strnewStatus,
                    itemType, _userid);

            result = "error";
            _message = "Failed To Change OTP token Status as Lost Token!!!";
            try {
                json.put("_result", result);
                json.put("_message", _message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            log.info("is ended :: ");
            return;
       }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
