package com.mollatech.axiom.v2.face.handler.pkitokens;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.AuthorizationManagement;
import com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class revokecertificate extends HttpServlet {
    
     static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(revokecertificate.class.getName());

    final String itemType = "PKITOKEN";
    final String itemTypeAUTH = "AUTHORIZATION";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());
        String _newvalue = request.getParameter("newvalue");
        log.debug("new value = "+_newvalue);
        String _oldvalue = request.getParameter("oldvalue");
        log.debug("oldvalue = "+_oldvalue);
        String _userid = request.getParameter("_userCR");
        log.debug("_userid :: "+_userid);
        String _reason = request.getParameter("_reason");
        log.debug("_reason :: "+_reason);

        String result = "success";
        String message = "Revoked Certificate successfully....";

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        if (_userid == null) {
            result = "error";
            message = "Could Not Revoke Certificate!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

//        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.PKI_CERTIFICATE) != 0) {
//                result = "error";
//                message = "Digital Certificate is not available in this license!!!";
//                try {
//                    json.put("_result", result);
//                    json.put("_message", message);
//                } catch (Exception e) {
//                    log.error("Exception caught :: ",e);
//                }
//                out.print(json);
//                out.flush();
//                return;
//            }
        String resultString = "ERROR";

        String ipaddress = request.getRemoteAddr();

        int retValue = -1;

        AuditManagement audit = new AuditManagement();

        CertificateManagement cManagement = new CertificateManagement();

        //start of authorization - Nilesh
//           if (operatorS.getRoleid() != 6|| operatorS.getRoleid() != 5) {
//           if (operatorS.getRoleid() >= 3) {
//                result = "error";
//                message = "Sorry, But you don't have permissions for this action!!!";
//                try {
//                    json.put("_result", result);
//                    json.put("_message", message);
//                } catch (Exception e) {
//                    log.error("Exception caught :: ",e);
//                }
//                out.print(json);
//                out.flush();
//                return;
//            }
//        }
        String _approvalId = request.getParameter("_approvalId");
        log.debug("_approvalId :: "+_approvalId);
        SettingsManagement sMngmt = new SettingsManagement();
        Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
        ChannelProfile chSettingObj = null;
        if (settingsObj != null) {
            chSettingObj = (ChannelProfile) settingsObj;
        }
        String _unitId = request.getParameter("_unitId");
        log.debug("_unitId :: "+_unitId);
        if (_unitId != null) {
            if (chSettingObj != null) {

                if (chSettingObj.authorizationunit == 1) {

                    int iUnitId = Integer.parseInt(_unitId);
                    if (iUnitId != operatorS.getUnits()) {
                        result = "error";
                        message = "Sorry, Must be marked by same branch operator!!!";
                        try {
                            json.put("_result", result);
                            json.put("_message", message);
                        } catch (Exception e) {
                            log.error("exception caught :: ",e);
                        }
                        out.print(json);
                        out.flush();
                        return;
                    }
                }
            }
        }
        OperatorsManagement oprMngt = new OperatorsManagement();
        UserManagement uMngt = new UserManagement();
        AuthUser uObj = uMngt.getUser(sessionId, channel.getChannelid(), _userid);
        AuthorizationManagement auth = new AuthorizationManagement();
        String strOpName = "-";
        String struserName = "-";
        String strAction = "-";
        int iapprovalID = -1;
//        ApprovalSetting approvalSetting = null;
        if (_approvalId != null) {
            if (operatorS.getRoleid() != 6) {
                if (operatorS.getRoleid() >= 3) {
                    result = "error";
                    message = "Sorry, But you don't have permissions for this action!!!";
                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("exception caught :: ",e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
            }
           // iapprovalID = Integer.parseInt(_approvalId);
            String _markerID = request.getParameter("_markerID");
            log.debug("_markerID :: "+_markerID);
            Operators op = oprMngt.getOperatorById(channel.getChannelid(), _markerID);
            if (op != null) {
                strOpName = op.getName();
            }

            if (op != null) {
                strOpName = op.getName();
            }
            if (uObj != null) {
                struserName = uObj.getUserName();
            }
        }

//         if (chSettingObj != null && _approvalId == null && operatorS.getRoleid() == OperatorsManagement.Requester) {
//
//            if (chSettingObj.authorizationStatus == SettingsManagement.ACTIVE_STATUS) {
//                AuthorizationManagement authMngt = new AuthorizationManagement();
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = AuthorizationManagement.REVOKE_CERTIFICATE;
//                approval.itemid = "Revoke Certificate from "+uObj.getUserName();
//                strAction = approval.itemid;
//                approval.makerid = operatorS.getOperatorid();
//                approval.userid = _userid;
//                approval.cerRevokeReason = _reason;
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.HOUR, chSettingObj.authorizationDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//
//                retValue = authMngt.addAuthorizationSetting(sessionId, channel.getChannelid(), operatorS.getOperatorid(),AuthorizationManagement.AUTORIZATION_PENDING_STATUS, dexpiredOn, approval
//                );
//                if (retValue == 0) {
//                    retValue = AuthorizationManagement.RETURN_AUTORIZATION_RESULT;
//                }
//            } else {
//                    retValue = cManagement.RevokeCertificate(sessionId, channel.getChannelid(), _userid, _reason);
//            }
//        } else {
            retValue = cManagement.RevokeCertificate(sessionId, channel.getChannelid(), _userid, _reason);
             log.debug("RevokeCertificate :: "+retValue);

//        }
        //end of authorization
        
//        retValue = cManagement.RevokeCertificate(sessionId, channel.getChannelid(), _userid, _reason);
        if (retValue == 0) {
            resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    //                    /ipaddress, 
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(),
                    "Revoke Certificate", resultString, retValue,
                    "Certificate Management", "", 
                    "Revoke certificate from =" + uObj.getUserName() + " with reasons as " + _reason,
                    itemType,
                    _userid);
            int retValue2 = cManagement.RevokeCertificate(sessionId, channel.getChannelid(), _userid,_reason);
            log.debug("RevokeCertificate :: "+retValue2);
            if (_approvalId != null) {
                int res = auth.removeAuthorizationRequest(sessionId, channel.getChannelid(), operatorS.getOperatorid(), _approvalId,_newvalue,_oldvalue);
                log.debug("removeAuthorizationRequest :: "+res);
                if (res == 0) {
                    resultString = "SUCCESS";
                    AuditManagement ad = new AuditManagement();
                    ad.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), "Remove Authorization Request",
                            resultString, retValue,
                            "Authorization Management", "Action =" + strAction + ",Action Marked By=" + strOpName
                            + ",Action Marked On=" + struserName, "Removed successfully!!!",
                            itemTypeAUTH, _userid);
                }

            }
            if (retValue2 == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        //                    /ipaddress, 
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(), new Date(), "Delete Certificate", resultString, retValue,
                        "Certificate Management", "", 
                        "Deleting certificate with reason as " + _reason,
                        itemType, _userid);
            } else {
                resultString = "ERROR";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        //ipaddress, 
                        channel.getName(),
                        remoteaccesslogin, operatorS.getName(), new Date(), 
                        "Delete Certificate", resultString, retValue,
                        "Certificate Management", "", 
                        "Error: Deleting certificate with reason as " + _reason,
                        itemType, _userid);

            }

        } else if (retValue == AuthorizationManagement.RETURN_AUTORIZATION_RESULT) {
            resultString = "SUCCESS";
//            if (_approvalId != null) {

            resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Add Authorization Request :Revoke Certificate",
                    resultString, retValue,
                    "Authorization Management", "Action =" + strAction + ",Action Marked By=" + strOpName
                    + ",Action Marked On=" + struserName,
                    "Added successfully!!!",
                    itemTypeAUTH, _userid);

//            }
            result = "success";
            message = "Request is pending for approval!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("exception caught :: ",ex);
            }

            out.print(json);
            out.flush();
            return;
        } else {

            resultString = "ERROR";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    //ipaddress, 
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Revoke Certificate", resultString, retValue,
                    "Certificate Management", "", 
                    "Deleting certificate with reason as " + _reason,
                    itemType, 
                    _userid);
            result = "error";
            message = "Could Not Revoke Certificate!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
