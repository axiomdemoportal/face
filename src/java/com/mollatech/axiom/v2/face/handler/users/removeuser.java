/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.users;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class removeuser extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(removeuser.class.getName());

    final String itemtype = "USERPASSWORD";
    final String itemTypeAUTH = "AUTHORIZATION";
    int FAILED_TO_SEND_ALERT = 9;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());

        response.setContentType("application/json");

        String _userid = request.getParameter("_userid");
        log.debug("_userid :: "+_userid);
        String result = "success";
        String message = "User removed successfully....";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        if (_userid == null) {
            result = "error";
            message = "Fill all Details!!";

            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }

            out.print(json);
            out.flush();
            return;
        }

//        
        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.USER_PASSWORD) != 0) {
            result = "error";
            message = "This feature is not available in this license!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        int retValue = -1;
        UserManagement uManagement = new UserManagement();
        AuthUser olduser = uManagement.getUser(sessionId, channel.getChannelid(), _userid);

        retValue = uManagement.CheckIfDelete(sessionId, channel.getChannelid(), _userid);
         log.debug("CheckIfDelete :: "+retValue);
        if (retValue == 0) {
            result = "error";
            message = "User cannot be removed as token/certificate is associated with it!!!";

            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }

            out.print(json);
            out.flush();
            return;

        }

        retValue = uManagement.DeleteUser(sessionId, channel.getChannelid(), _userid);

        AuditManagement audit = new AuditManagement();

        String resultString = "Failure";
        if (retValue == 0) {
            resultString = "Success";
        }
        if (retValue == 0) {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Remove User", resultString, retValue,
                    "User Management", "User ID=" + olduser.getUserId() + ",Name=" + olduser.getUserName()
                    + ",Phone=" + olduser.getPhoneNo() + ",Email=" + olduser.getEmail() + ",State=" + olduser.getStatePassword(),
                    "Removed", itemtype, _userid);

        } else if (retValue == FAILED_TO_SEND_ALERT) {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Remove User", resultString, retValue,
                    "User Management", "User ID=" + olduser.getUserId() + ",Name=" + olduser.getUserName()
                    + ",Phone=" + olduser.getPhoneNo() + ",Email=" + olduser.getEmail() + ",State=" + olduser.getStatePassword(),
                    "User Removed But Fails to send Alert...!!!", itemtype, _userid);

        } else {

            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Remove User", resultString, retValue,
                    "User Management", "User=" + olduser.getUserId() + ",Name=" + olduser.getUserName()
                    + ",Phone=" + olduser.getPhoneNo() + ",Email=" + olduser.getEmail() + ",State=" + olduser.getStatePassword(),
                    "User ID=" + olduser.getUserId() + ",Name = " + olduser.getUserName()
                    + "Phone=" + olduser.getPhoneNo() + ",Email=" + olduser.getEmail() + ",State=" + olduser.getStatePassword(),
                    itemtype, _userid);

            result = "error";
            message = "cannot remove user!!";
            out.print(json);
            out.flush();
            return;
        }
        if (retValue == 0) {
            result = "success";
            message = "User removed successfully";
        } else if (retValue == FAILED_TO_SEND_ALERT) {
            result = "success";
            message = "User removed successfully  but failed to send alert";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
            
        } catch (Exception e) {
            log.error("exception caught :: ",e);
        } finally {
             out.print(json);
             out.flush();
        }
        log.info("is ended :: ");
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static Object createObject(Constructor constructor, Object[] arguments) {

        //System.out.println("Constructor: " + constructor.toString());
        Object object = null;

        try {
            object = constructor.newInstance(arguments);
            //System.out.println("Object: " + object.toString());
            return object;
        } catch (Exception e) {
            log.error("exception caught :: ",e);
        }
        return object;
    }
}
