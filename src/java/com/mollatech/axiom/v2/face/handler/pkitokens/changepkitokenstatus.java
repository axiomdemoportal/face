/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.pkitokens;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.AuthorizationManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.connector.access.controller.ApprovalSetting;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class changepkitokenstatus extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(changepkitokenstatus.class.getName());

    final int SOFTWAREMOBILE_PKITOKEN = 1;
    final int HARDWARE_PKITOKEN = 2;
    public static final int TOKEN_STATUS_ACTIVE = 1;
    public static final int TOKEN_STATUS_LOCKEd = -1;
    public static final int TOKEN_STATUS_ASSIGNED = 0;
    public static final int TOKEN_STATUS_UNASSIGNED = -10;
    public static final int TOKEN_STATUS_SUSPENDED = -2;
    public static final int TOKEN_STATUS_LOST = -5;
    final String itemType = "PKITOKEN";
    final String itemTypeAUTH = "AUTHORIZATION";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String _newvalue = request.getParameter("newvalue");
        log.debug("new value = "+_newvalue);
        String _oldvalue = request.getParameter("oldvalue");
        log.debug("oldvalue = "+_oldvalue);
        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());

        String _userid = request.getParameter("_userid");
        log.debug("_userid :: "+_userid);
        String _status = request.getParameter("_status");
        log.debug("_status :: "+_status);
        int status = 0;
        if (_status != null) {
            status = Integer.parseInt(_status);
        }

        String _category = request.getParameter("_category");
        log.debug("_category :: "+_category);
        int category = 0;
        if (_category != null) {
            category = Integer.parseInt(_category);
        }
        String result = "success";
        String message = "Status change successfully....";

        String _value = "Active";

        if (status == TOKEN_STATUS_ACTIVE) {
            _value = "Active";
        } else if (status == TOKEN_STATUS_SUSPENDED) {
            _value = "Suspended";
        } else if (status == TOKEN_STATUS_UNASSIGNED) {
            _value = "Unassigned";
        } else if (status == TOKEN_STATUS_LOCKEd) {
            _value = "LOCKED";
        } else if (status == TOKEN_STATUS_LOST) {
            _value = "LOST";
        }

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
         
        if (_userid == null || _status == null) {
            result = "error";
            message = "Status not change!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        int retValue = -1;

        PKITokenManagement pManagement = new PKITokenManagement();
        AuditManagement audit = new AuditManagement();
//         int istatus = oManagement.getStatus(sessionId, channel.getChannelid(), _userid, category, subCategory)
        int istatus = 0;
        //   String serialNo =  oManagement.getSerialNo(sessionId, channel.getChannelid(), _userid, category);
        String strnewStatus = "";
        if (status == TOKEN_STATUS_ACTIVE) {
            strnewStatus = "TOKEN_STATUS_ACTIVE";
        } else if (status == TOKEN_STATUS_ASSIGNED) {
            strnewStatus = "TOKEN_STATUS_ASSIGNED";
        } else if (status == TOKEN_STATUS_LOCKEd) {
            strnewStatus = "TOKEN_STATUS_LOCKEd";
        } else if (status == TOKEN_STATUS_SUSPENDED) {
            strnewStatus = "TOKEN_STATUS_SUSPENDED";
        } else if (status == TOKEN_STATUS_UNASSIGNED) {
            strnewStatus = "TOKEN_STATUS_UNASSIGNED";
        } else if (status == TOKEN_STATUS_LOST) {
            strnewStatus = "TOKEN_STATUS_LOST";
        }
        //start of authorization - Nilesh
//           if (operatorS.getRoleid() != 6|| operatorS.getRoleid() != 5) {
//           if (operatorS.getRoleid() >= 3) {
//                result = "error";
//                message = "Sorry, But you don't have permissions for this action!!!";
//                try {
//                    json.put("_result", result);
//                    json.put("_message", message);
//                } catch (Exception e) {
//                    log.error("Exception caught :: ",e);
//                }
//                out.print(json);
//                out.flush();
//                return;
//            }
//        }
        String _approvalId = request.getParameter("_approvalId");
        log.debug("_approvalId :: "+_approvalId);
        SettingsManagement sMngmt = new SettingsManagement();
        Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
        ChannelProfile chSettingObj = null;
        if (settingsObj != null) {
            chSettingObj = (ChannelProfile) settingsObj;
        }
        String _unitId = request.getParameter("_unitId");
        log.debug("_unitId :: "+_unitId);

        if (_unitId != null) {
            if (chSettingObj != null) {

                if (chSettingObj.authorizationunit == 1) {

                    int iUnitId = Integer.parseInt(_unitId);
                    if (iUnitId != operatorS.getUnits()) {
                        result = "error";
                        message = "Sorry, Must be marked by same branch operator!!!";
                        try {
                            json.put("_result", result);
                            json.put("_message", message);
                        } catch (Exception e) {
                            log.error("exception caught :: ",e);
                        }
                        out.print(json);
                        out.flush();
                        return;
                    }
                }
            }
        }
        OperatorsManagement oprMngt = new OperatorsManagement();
        UserManagement uMngt = new UserManagement();
        AuthUser uObj = uMngt.getUser(sessionId, channel.getChannelid(), _userid);
        AuthorizationManagement auth = new AuthorizationManagement();
        String strOpName = "-";
        String struserName = "-";
        String strAction = "-";
        int iapprovalID = -1;
//        ApprovalSetting approvalSetting = null;
        if (_approvalId != null) {
            if (operatorS.getRoleid() != 6) {
                if (operatorS.getRoleid() >= 3) {
                    result = "error";
                    message = "Sorry, But you don't have permissions for this action!!!";
                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("exception caught :: ",e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
            }
          //  iapprovalID = Integer.parseInt(_approvalId);
            String _markerID = request.getParameter("_markerID");
            log.debug("_markerID :: "+_markerID);
            Operators op = oprMngt.getOperatorById(channel.getChannelid(), _markerID);
            if (op != null) {
                strOpName = op.getName();
            }
            if (op != null) {
                strOpName = op.getName();
            }
            if (uObj != null) {
                struserName = uObj.getUserName();
            }

        }

//        if (chSettingObj != null && _approvalId == null && operatorS.getRoleid() == OperatorsManagement.Requester) {
//
//            if (chSettingObj.authorizationStatus == SettingsManagement.ACTIVE_STATUS) {
//                AuthorizationManagement authMngt = new AuthorizationManagement();
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = AuthorizationManagement.CHANGE_HW_PKI_TOKEN_STATUS;
//                approval.itemid = "Change HW PKI token status to " + strnewStatus + " for " + uObj.getUserName();
//                strAction = approval.itemid;
//                approval.makerid = operatorS.getOperatorid();
//                approval.userid = _userid;
//                approval.tokenCategory = category;
//                approval.tokenStatus = status;
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.HOUR, chSettingObj.authorizationDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//
//                //retValue = authMngt.addAuthorizationSetting(sessionId, channel.getChannelid(), AuthorizationManagement.AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                retValue = authMngt.addAuthorizationSetting(sessionId, channel.getChannelid(), operatorS.getOperatorid(), AuthorizationManagement.AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (retValue == 0) {
//                    retValue = AuthorizationManagement.RETURN_AUTORIZATION_RESULT;
//                }
//            } else {
//                retValue = pManagement.ChangeStatus(sessionId, channel.getChannelid(), _userid, status, category);
//            }
//        } else {
            retValue = pManagement.ChangeStatus(sessionId, channel.getChannelid(), _userid, status, category);
            log.debug("ChangeStatus :: "+retValue);

//        }
        //end of authorization
//        retValue = pManagement.ChangeStatus(sessionId, channel.getChannelid(), _userid, 
//                status, category);

        String resultString = "ERROR";

        String strStatus = "";
        if (istatus == TOKEN_STATUS_ACTIVE) {
            strStatus = "TOKEN_STATUS_ACTIVE";
        } else if (istatus == TOKEN_STATUS_ASSIGNED) {
            strStatus = "TOKEN_STATUS_ASSIGNED";
        } else if (istatus == TOKEN_STATUS_LOCKEd) {
            strStatus = "TOKEN_STATUS_LOCKEd";
        } else if (istatus == TOKEN_STATUS_SUSPENDED) {
            strStatus = "TOKEN_STATUS_SUSPENDED";
        } else if (istatus == TOKEN_STATUS_UNASSIGNED) {
            strStatus = "TOKEN_STATUS_UNASSIGNED";
        } else if (istatus == TOKEN_STATUS_LOST) {
            strStatus = "TOKEN_STATUS_LOST";
        }

        String strCategory = "";
        if (category == SOFTWAREMOBILE_PKITOKEN) {
            strCategory = "SOFTWARE_TOKEN";
        } else if (category == HARDWARE_PKITOKEN) {
            strCategory = "HARDWARE_TOKEN";
        }

        //String ipaddress = request.getRemoteAddr();
        //String strSubCategory = "";
        if (retValue == 0) {
            resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    //ipaddress, 
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Change Status", resultString, retValue,
                    "PKI TOKEN Management", 
                    "",
                    "Category = " + _category + " New Status =" + strnewStatus,
                    itemType, 
                    _userid);

            if (_approvalId != null) {

                int res = auth.removeAuthorizationRequest(sessionId, channel.getChannelid(), operatorS.getOperatorid(), _approvalId,_newvalue,_oldvalue);
                log.debug("removeAuthorizationRequest :: "+res);
                if (res == 0) {
                    
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), "Remove Authorization Request",
                            resultString, retValue,
                            "Authorization Management", "Action =" + strAction + ",Action Marked By=" + strOpName
                            + ",Action Marked On=" + struserName, "Removed successfully!!!",
                            itemTypeAUTH, 
                            _userid);
                }
            }

            if (status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
                AuthUser user = null;
                UserManagement userObj = new UserManagement();
                user = userObj.getUser(sessionId, channel.getChannelid(), _userid);
                Templates templates = null;
                ByteArrayInputStream bais = null;
                String tmessage = null;
                TemplateManagement tManagement = new TemplateManagement();
                if (category == OTPTokenManagement.SOFTWARE_TOKEN) {

                    templates = tManagement.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.MOBILE_SOFTWARE_TOKEN_UNASSIGN_TEMPLATE);

                    bais = new ByteArrayInputStream(templates.getTemplatebody());
                    tmessage = (String) TemplateUtils.deserializeFromObject(bais);
                    //added for template based messages
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                    if (tmessage != null) {
                        Date d = new Date();

                        tmessage = tmessage.replaceAll("#name#", user.getUserName());
                        tmessage = tmessage.replaceAll("#channel#", channel.getName());
                        tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                        tmessage = tmessage.replaceAll("#tokentype#", "Mobile");
//                        if (subCategory == OTPTokenManagement.SW_MOBILE_TOKEN) {
//                            message = message.replaceAll("#tokentype#", "Mobile");
//                        } else if (subCategory == OTPTokenManagement.SW_WEB_TOKEN) {
//                            message = message.replaceAll("#tokentype#", "Web");
//                        } else if (subCategory == OTPTokenManagement.SW_PC_TOKEN) {
//                            message = message.replaceAll("#tokentype#", "PC");
//                        }
                    }
                }

                SendNotification send = new SendNotification();
                //     AXIOMStatus axiomStatus = send.SendOnMobile(channel.getChannelid(), user.getPhoneNo(), templatebody, 1, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));

                if (tmessage != null && user != null) {
                    Date d = new Date();

                    if (category == PKITokenManagement.SOFTWARE_TOKEN) {
                        if (user.phoneNo != null) {
                            if (templates.getStatus() == tManagement.ACTIVE_STATUS) {
                                send.SendOnMobileNoWaiting(channel.getChannelid(), user.phoneNo, message,
                                        SendNotification.SMS,
                                        Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }
                        }
                    }
                }
            }
        } else if (retValue == AuthorizationManagement.RETURN_AUTORIZATION_RESULT) {
            resultString = "SUCCESS";
//            if (_approvalId != null) {

                //resultString = "Success";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operatorS.getName(), new Date(), "Add Authorization Request :Change PKI token status",
                        resultString, retValue,
                        "Authorization Management", "Action =" + strAction + ",Action Marked By=" + strOpName
                        + ",Action Marked On=" + struserName,
                        "Added successfully!!!",
                        itemTypeAUTH, _userid);

//            }

            result = "success";
            message = "Request is pending for approval!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
           } catch (Exception ex) {
               log.error("exception caught :: ",ex);
            }
            
            out.print(json);
            out.flush();
            return;
        } else {

            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    //ipaddress, 
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Change Status", resultString, retValue,
                    "PKI TOKEN Management", "", "Category = " + _category + " New Status =" + strnewStatus,
                    itemType, _userid);

            result = "error";
            message = "Status not change!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
                json.put("_value", _value);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);

        } catch (Exception e) {
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
