/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.operator;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PasswordTrailManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import static com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PASSWORD_POLICY_SETTING;
import static com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE;
import com.mollatech.axiom.nucleus.settings.PasswordPolicySetting;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

public class sendrandompassword extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(sendrandompassword.class.getName());

    final String itemTypeOp = "OPERATOR";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("is started :: ");
        
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String _operId = request.getParameter("_oprid");
        log.debug("_operId :: "+_operId);
        String _sendOrNot = request.getParameter("_send");
        log.debug("_sendOrNot :: "+_sendOrNot);
        boolean bsendOrNot = Boolean.parseBoolean(_sendOrNot);

        //audit parameter
        Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operator :: "+operator.getOperatorid());
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        //String _oldPassword  = request.getParameter("_oldPassword");


        OperatorsManagement oManagement = new OperatorsManagement();
        AuditManagement audit = new AuditManagement();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Random Password Sent Sucessfully!!!";

        int retValue = 0;
        JSONObject json = new JSONObject();

        UtilityFunctions u = new UtilityFunctions();
        Date d = new Date();
        String strPassword = u.HexSHA1(sessionId + channel.getChannelid() + _operId + d.toString());
        strPassword = strPassword.substring(0, 9);
        u = null;

        PasswordTrailManagement setObj = new PasswordTrailManagement();
        String password = setObj.GeneratePassword(channel.getChannelid(), strPassword);
        
        if(password != null && !password.isEmpty()){
            strPassword = password;
        }
        SettingsManagement settObj = new SettingsManagement();
            Object obj = settObj.getSettingInner(channel.getChannelid(), PASSWORD_POLICY_SETTING, PREFERENCE_ONE);
        if (obj != null) {
            if (obj instanceof PasswordPolicySetting) {
                PasswordPolicySetting passwordSetting = (PasswordPolicySetting) obj;
               // PasswordTrail pass = new PasswordTrail();
                 Date pUpdateDate = operator.getPasswordupdatedOn();
                
                 Calendar pcurrent = Calendar.getInstance();
                  if(pUpdateDate != null){
                pcurrent.setTime(pUpdateDate);
                  }else{
                      pcurrent.setTime(new Date());
                  }
                pcurrent.set(Calendar.AM_PM, Calendar.AM);
                pcurrent.add(Calendar.MONTH, 1);//1 month
                Date pendDate = pcurrent.getTime();
                
              String[] strOldPassword = setObj.getPasswordUsingOpId(channel.getChannelid(),operator.getOperatorid() , passwordSetting.passwordGenerations);
              String pas = setObj.MD5HashPassword(strPassword);
              String str = "";
              if(strOldPassword != null){
                  for(int i = 0;i<strOldPassword.length;i++){
                      str = str +","+strOldPassword[i];
                  }
              }
              if(str.contains(pas)){
                    result = "error";
                    message = "Password already used in previous generations...!!!";
                    try { json.put("_result", result);
                    json.put("_message", message);}catch(Exception e){log.error("Exception caught :: ",e);}
                    out.print(json);
                    out.flush();
                    return;
              }
               int issuePasswordCount = setObj.getcountOfIssuePassword(channel.getChannelid(), _operId, pendDate, pUpdateDate);
              if( passwordSetting.issuingLimitDuration != 99){//no issue limit
               if(passwordSetting.issuingLimitDuration <= issuePasswordCount )  {
                    result = "error";
                    message = "Password Issue Limit Reach...!!!";
                    try { json.put("_result", result);
                    json.put("_message", message);
                    }catch(Exception e){log.error("Exception caught :: ",e);}
                    out.print(json);
                    out.flush();
                    return;
                }
            }
            }
        }
                
        retValue = oManagement.SetPassword(sessionId, channel.getChannelid(), _operId, strPassword, bsendOrNot);
        log.debug("SetPassword :: "+retValue);
        
        if(retValue == 0){    
            //PasswordTrail pass = new PasswordTrail();
//            AxiomOperator op = oManagement.GetOperatorByName(sessionId, channel.getChannelid(), _op_name);
            String pas = setObj.MD5HashPassword(strPassword);
            setObj.AddPasswordTrail(channel.getChannelid(), _operId, pas);
        }
//        System.out.println("kjfnks"+strPassword);
        String resultString = "ERROR";
        if (retValue == 0) {
            resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                    "Set Password", resultString, retValue, "Operator Management",
                    "Old Password =*****", "New Password =*****", itemTypeOp, 
                    _operId);

        } else if (retValue != 0) {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                    "Set Password", resultString, retValue, "Operator Management",
                    "Old Password =*****", "Failed To Set Password ...!", itemTypeOp, 
                    _operId);

        }

        if (retValue == 0) {
//            if (_sendOrNot.compareTo("true") == 0) {
//                retValue = oManagement.ResendPassword(sessionId, channel.getChannelid(), _operId, true);
//
//                if (retValue == 0) {
//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
//                            request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
//                            operator.getName(), new Date(), "Resend Password", "Success", retValue,
//                            "Operator Management", "", "Password Sent Successfully", itemTypeOp, result);
//                }
//
//                if (retValue != 0) {
//                    result = "error";
//                    message = "Password Sending Failed!!!";
//                    try { json.put("_result", result);
//                    json.put("_message", message);}catch(Exception e){log.error("Exception caught :: ",e);}
//
//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
//                            request.getRemoteAddr(), channel.getName(), remoteaccesslogin,
//                            operator.getName(), new Date(), "Resend Password", "Success", retValue,
//                            "Operator Management", "", "Failed To Send Password", itemTypeOp, result);
//
//                }
//            }
          result = "success";
          if ( bsendOrNot == true)
            message = "Random Password Sent Sucessfully!!!";
          else 
              message = "Random Password Set Sucessfully!!!";

        } else {
            result = "error";
            message = "Random Password Update Failed!!!";
            try { json.put("_result", result);
            json.put("_message", message);}catch(Exception e)
            {
                log.error("exception caught :: ",e);
                    }
        }

        try {
            json.put("_result", result);
            json.put("_message", message);

        }catch(Exception e){
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");

    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
