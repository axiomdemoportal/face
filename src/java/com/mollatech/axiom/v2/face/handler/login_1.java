//package com.mollatech.axiom.v2.face.handler;
//
//import com.mollatech.axiom.common.utils.UtilityFunctions;
//import com.mollatech.axiom.connector.communication.AXIOMStatus;
//import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
//import com.mollatech.axiom.nucleus.crypto.LoadSettings;
//import com.mollatech.axiom.nucleus.db.Channels;
//import com.mollatech.axiom.nucleus.db.Operators;
//import com.mollatech.axiom.nucleus.db.Templates;
//import com.mollatech.axiom.nucleus.db.Units;
//import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
//import com.mollatech.axiom.nucleus.db.connector.OperatorsUtil;
//import com.mollatech.axiom.nucleus.db.connector.RemoteAccessUtils;
//import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
//import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
//import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.PasswordTrailManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet;
//
//import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
//import com.mollatech.axiom.nucleus.settings.ChannelSettings;
//import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
//import com.mollatech.axiom.nucleus.settings.PasswordPolicySetting;
//import com.mollatech.axiom.nucleus.settings.SendNotification;
//import java.io.ByteArrayInputStream;
//import static com.mollatech.axiom.common.utils.UtilityFunctions.context;
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Date;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//import org.hibernate.Session;
//import org.json.JSONObject;
//import javax.naming.NamingEnumeration;
//import javax.naming.directory.Attribute;
//import javax.naming.directory.Attributes;
//import javax.naming.directory.SearchControls;
//import javax.naming.directory.SearchResult;
//public class login_1 extends HttpServlet {
//
//    final String itemType = "LOGIN";
//    int SUCCESS = 0;
//    int FAILED = -1;
//    String BLOCKED = "BLOCKED_IP";
//     public static final int ACTIVE_STATUS = 1;
//    public static final int admin = 2;
//     public static final int unit = 0;
//     final int FIRST_OPERATOR_AS_0 = 0 ;
////      public static DirContext ctx;
//
//    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//
//        response.setContentType("application/json");
//        JSONObject json = new JSONObject();
//        PrintWriter out = response.getWriter();
//
//        try {
//
//            String _operatorName = request.getParameter("_name");
//            String _password = request.getParameter("_passwd");
//            String _channelName = this.getServletContext().getContextPath();
//            _channelName = _channelName.replaceAll("/", "");
//            AuditManagement audit = new AuditManagement();
//            String result = "success";
//            String message = "successful credential verification....";
//
//            String url = "home.jsp";
//
//            Operators operartorObj = null;
//
//            if (_operatorName == null || _password == null) {
//                result = "error";
//                message = "Invalid Credentials!!";
//                url = "index.jsp";
//                json.put("_result", result);
//                json.put("_message", message);
//                json.put("_url", url);
//                out.print(json);
//                out.flush();
//                return;
//            }
//
//            int retValue = -1;
//
//            try {
//                SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
//                Session sChannel = suChannel.openSession();
//                ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
//
//                SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
//                Session sRemoteAcess = suRemoteAcess.openSession();
//                RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
//
//                Channels channel = cUtil.getChannel(_channelName);
//                if (channel == null) {
//                    result = "error";
//                    message = "Invalid channel!!";
//                    url = "index.jsp";
//                    json.put("_result", result);
//                    json.put("_message", message);
//                    json.put("_url", url);
//                    out.print(json);
//                    out.flush();
//                    sRemoteAcess.close();
//                    suRemoteAcess.close();
//                    sChannel.close();
//                    suChannel.close();
//                    return;
//                }
//
//                SessionFactoryUtil suOperators = new SessionFactoryUtil(SessionFactoryUtil.operators);
//                Session sOperators = suOperators.openSession();
//                OperatorsUtil opUtil = new OperatorsUtil(suOperators, sOperators);
//                Operators opObj = opUtil.GetByName(channel.getChannelid(), _operatorName);
//
//                String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
//                if (credentialInfo == null) {
//                    result = "error";
//                    message = "Remote Access is not configured properly!!";
//                    url = "index.jsp";
//                    json.put("_result", result);
//                    json.put("_message", message);
//                    json.put("_url", url);
//                    out.print(json);
//                    out.flush();
//                    sRemoteAcess.close();
//                    suRemoteAcess.close();
//                    sChannel.close();
//                    suChannel.close();
//                    sOperators.close();
//                    suOperators.close();
//                    return;
//                }
//
//                request.getSession().setAttribute("_apSessionRemoteAccessLoginID", credentialInfo[0]);
//
//                SessionManagement sManagement = new SessionManagement();
//                OperatorsManagement oManagement = new OperatorsManagement();
//                SettingsManagement sMngt = new SettingsManagement();
////            TemplateManagement tObj = new TemplateManagement();
//                SendNotification send = new SendNotification();
//                String sessionId = null;
//                Object ipobj = sMngt.getSettingInner(channel.getChannelid(), SettingsManagement.GlobalSettings, 1);
//
//                OperatorsManagement oObj = new OperatorsManagement();
//
//                if (ipobj != null) {
//
//                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                    int checkIp = 1;
//                    if (request.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                        checkIp = sMngt.checkIP(channel.getChannelid(), request.getRemoteAddr());
//                    } else {
//                        checkIp = 1;
//                    }
//
//                    if (iObj.ipstatus == 0 && checkIp != 1) {
//                        if (iObj.ipalertstatus == 0) {
//
//                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                            Session sTemplate = suTemplate.openSession();
//                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                            Templates templatesObj = tUtil.loadbyName(channel.getChannelid(), TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                String strsubject = templatesObj.getSubject();
//                                SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
//                                if (strmessageBody != null) {
//                                    // Date date = new Date();
//                                    strmessageBody = strmessageBody.replaceAll("#name#", opObj.getName());
//                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                    strmessageBody = strmessageBody.replaceAll("#email#", opObj.getEmailid());
//                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                    strmessageBody = strmessageBody.replaceAll("#filterword#", request.getRemoteAddr());
//                                }
//
//                                Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
//                                if (aOperator != null) {
//                                    String[] emailList = new String[aOperator.length - 1];
//                                    for (int i = 1; i < aOperator.length; i++) {
//                                        emailList[i - 1] = aOperator[i].getEmailid();
//                                    }
//
//                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                                }
//                                sessionId = BLOCKED;
//                                suTemplate.close();
//                                sTemplate.close();
//                            }
//                            sessionId = BLOCKED;
//                            suTemplate.close();
//                            sTemplate.close();
//                        }
//                        sessionId = BLOCKED;
//                    } else {
//                        sessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());
//                    }
//
//                } else {
//                    sessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());
//                }
//                String resultStr = "Failure";
//
//                if (sessionId == null) {
//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), "",
//                            request.getRemoteAddr(), channel.getName(), credentialInfo[0],
//                            _operatorName, new Date(), "Open Session", resultStr, retValue,
//                            "Login", "", "Failed To Open Session", itemType,
//                            credentialInfo[0]);
//
//                } else if (sessionId.equals(BLOCKED)) {
//                    retValue = -121;
//                    resultStr = "Blocked By Ip Filter ...!!!";
//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), "",
//                            request.getRemoteAddr(), channel.getName(), credentialInfo[0],
//                            _operatorName, new Date(), "Open Session", resultStr, retValue,
//                            "Login", "", "Failed To open Session because Ip is blocked...!!!" + sessionId, itemType,
//                            credentialInfo[0]);
//
//                } else {
//                    //if (sessionId != null) {
//                    retValue = 0;
//                    resultStr = "Success";
//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), "",
//                            request.getRemoteAddr(), channel.getName(), credentialInfo[0],
//                            _operatorName, new Date(), "Open Session", resultStr, retValue,
//                            "Login", "", "Open Session successfully with Session Id =" + sessionId, itemType,
//                            credentialInfo[0]);
//
//                }
//
//                if (sessionId == null) {
//                    result = "error";
//                    message = "Remote Access is disabled for this channel...!!!";
//                    url = "index.jsp";
//                    json.put("_result", result);
//                    json.put("_message", message);
//                    json.put("_url", url);
//                    out.print(json);
//                    out.flush();
//                    sRemoteAcess.close();
//                    suRemoteAcess.close();
//                    sChannel.close();
//                    suChannel.close();
//                    sOperators.close();
//                    suOperators.close();
//                    return;
//                }
//
//                if (sessionId == BLOCKED) {
//                    result = "Blocked";
//                    message = "IP is blocked by IP filter...!!!";
//                    url = "index.jsp";
//                    json.put("_result", result);
//                    json.put("_message", message);
//                    json.put("_url", url);
//                    out.print(json);
//                    out.flush();
//                    sRemoteAcess.close();
//                    suRemoteAcess.close();
//                    sChannel.close();
//                    suChannel.close();
//                    sOperators.close();
//                    suOperators.close();
//                    return;
//                }
//
////               operartorObj = oManagement.VerifyCredential(sessionId, channel.getChannelid(), _operatorName, _password);
//               //sachin
//                
////                SettingsManagement sMngmt = new SettingsManagement();
////                Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.CHANNEL_SETTINGS, SettingsManagement.PREFERENCE_ONE);
//
////                ChannelSettings chSettings = (ChannelSettings) settingsObj;
////                boolean type = chSettings.isSourceType();
//                
//                
//                 SettingsManagement sMngmt = new SettingsManagement();
//                Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.CHANNEL_SETTINGS, SettingsManagement.PREFERENCE_ONE);
//                ChannelSettings chSettings = new ChannelSettings();
//                boolean type = true;
//                if (settingsObj != null) {
//                    chSettings = (ChannelSettings) settingsObj;
//                    type = chSettings.isSourceType();
//                }
//                if(settingsObj==null){
//                    type=true;
//                }
//                
//              if (type==true||_operatorName.equals("sysadmin")) {
//                    operartorObj = oManagement.VerifyCredential(sessionId, channel.getChannelid(), _operatorName, _password);
//                } else {
//                    String host = chSettings.getExternalhost();
//                    String port = String.valueOf(chSettings.getPort());
//                    String dn = chSettings.getDatabaseName();
//                    String table = chSettings.getTableName();
//                    context = new UtilityFunctions().createConnection(host, port);
//                    SearchControls searchControls = new SearchControls();
//                    searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
//                    String searchFilter = "(&(objectClass=" + table + ")((userid=" + _operatorName + ")(userPassword=" + _password + ")))";
//                    NamingEnumeration results = context.search(dn, searchFilter, searchControls);
//                    SearchResult searchResult = null;
//                    if (results.hasMore()) {
//                        searchResult = (SearchResult) results.next();
//                        Attributes attributes = searchResult.getAttributes();
//                        Attribute uid = attributes.get("userid");
//                        Attribute cn = attributes.get("cn");
//                        Attribute mobile = attributes.get("mobile");
//                        Attribute mail = attributes.get("mail");
//
//                        operartorObj = oManagement.VerifyCredential(sessionId, channel.getChannelid(), _operatorName, _password);
//                        if (operartorObj == null) {
//                            oManagement.AddOperator(sessionId, channel.getChannelid(), _operatorName, _password, mobile.get().toString(), mail.get().toString(), admin, ACTIVE_STATUS, unit,FIRST_OPERATOR_AS_0);
//                            operartorObj = oManagement.VerifyCredential(sessionId, channel.getChannelid(), _operatorName, _password);
//                        } else {
//                            boolean flag = false;
//                            Operators op1 = new Operators();
//
//                            Operators passwordOpr = new Operators();
//
//                            if (!operartorObj.getName().equalsIgnoreCase(_operatorName)) {
//                                flag = true;
//                                op1.setName(cn.get().toString());
//                            }
//                            if (!operartorObj.getEmailid().equalsIgnoreCase(mail.get().toString())) {
//                                flag = true;
//                                op1.setEmailid(mail.get().toString());
//                            }
//                            if (!operartorObj.getPasssword().equalsIgnoreCase(_password)) {
//                                flag = true;
//                                op1.setPasssword(_password);
//                            }
//                            if (!operartorObj.getPhone().equalsIgnoreCase(mobile.get().toString())) {
//                                flag = true;
//                                op1.setPhone(mobile.get().toString());
//                            }
//                            if (flag) {
//                                oManagement.EditOperator(sessionId, channel.getChannelid(), operartorObj.getOperatorid(), operartorObj, op1);
//                            }
//                        }
//                    } else {
//                        System.out.println("Invalid Credential...");
//                        operartorObj = oManagement.VerifyCredential(sessionId, channel.getChannelid(), _operatorName, _password);
//                        if (operartorObj != null) {
//                            oManagement.deleteoperator(sessionId, channel.getChannelid(), operartorObj.getOperatorid());
//                            operartorObj=null;
//                        }
//                    }
//                }
//                //sachin end
//                sRemoteAcess.close();
//                suRemoteAcess.close();
//                sChannel.close();
//                suChannel.close();
//                sOperators.close();
//                suOperators.close();
//                //new addition
//                ChannelProfile channelprofileObj = null;
//                Object channelpobj = sMngt.getSettingInner(channel.getChannelid(), SettingsManagement.CHANNELPROFILE_SETTING, 1);
//
//                if (channelpobj == null) {
//                    LoadSettings.LoadChannelProfile(channelprofileObj);
//                } else {
//                    channelprofileObj = (ChannelProfile) channelpobj;
//                    LoadSettings.LoadChannelProfile(channelprofileObj);
//
//                }
//                //end of new addition
//
//                
//                
//                if (operartorObj != null) {
//
//                    UnitsManagemet unMngt = new UnitsManagemet();
//                    int iUnitId = operartorObj.getUnits();
//                    Units un = unMngt.getUnitByUnitId(sessionId, channel.getChannelid(), iUnitId);
//                    if (un != null) {
//                        if (un.getStatus() == unMngt.SUSPEND_STATUS) {
//                            result = "error";
//                            message = un.getUnitname() + " unit is suspended!!!";
//                            json.put("_result", result);
//                            json.put("_message", message);
//                            out.print(json);
//                            out.flush();
//                            return;
//                        }
//                    }
//                    if (operartorObj.getRoleid() != 1) {
//                        int retVal = sManagement.getServerStatus(channelprofileObj);
//                        if (retVal != 0) {
//                            result = "error";
//                            message = "Server Down for maintainance,Please try later!!!";
//                            json.put("_result", result);
//                            json.put("_message", message);
//                            out.print(json);
//                            out.flush();
//                            return;
//                        }
//                    }
//
//                    if (operartorObj.getStatus() != -1) {
//                        retValue = 0;
//                    } else {
//                        retValue = -1;
//                    }
//                } else {
//                    retValue = -1;
//                }
//                PasswordPolicySetting passObj = null;
//                if (operartorObj != null) {
//                    if (operartorObj.getStatus() != 1) {    //not active
//                        result = "error";
//                        //added by vikram
//                        if (operartorObj.getStatus() == -1) //locked
//                        {
//                            message = "Account is locked as many wrong attempts were made!!";
//                        } else if (operartorObj.getStatus() == 0) //suspended
//                        {
//                            message = "Account is suspended!!";
//                        } else if (operartorObj.getStatus() == -99) //removed
//                        {
//                            message = "Account is removed, please contact administrator!!";
//                        }
//                        //end of addition
//
//                        audit.AddAuditTrail(sessionId, channel.getChannelid(), operartorObj.getOperatorid(),
//                                request.getRemoteAddr(), channel.getName(), credentialInfo[0], operartorObj.getName(),
//                                new Date(), "Login", message, FAILED,
//                                "Login", "", "Login Failed", "Login", operartorObj.getOperatorid());
//
//                        url = "index.jsp";
//                    }
//
//                    PasswordTrailManagement passwordTrail = new PasswordTrailManagement();
//                    Object obj = sMngt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.PASSWORD_POLICY_SETTING, SettingsManagement.PREFERENCE_ONE);
//                    if (obj != null) {
//                        if (obj instanceof PasswordPolicySetting) {
//                            passObj = (PasswordPolicySetting) obj;
//                            if (passObj.changePasswordAfterFirstlogin == true) {
//                                int count = passwordTrail.getPassword(channel.getChannelid(), operartorObj.getOperatorid());
////                                 count =1;
//                                if (operartorObj.getChangePassword() == OperatorsManagement.CHANGE_PASSWORD_STATUS) {
//                                    //if (count == 1) {
//                                    result = "1sLogin";
//                                    message = "First time user, please change password now!!!";
//                                    json.put("_result", result);
//                                    json.put("_message", message);
//                                    json.put("_url", "#ChangePassword");
//                                    HttpSession session = request.getSession(true);
//                                    session.setAttribute("_apOprAuth", "yes");
//                                    session.setAttribute("_apOprDetail", operartorObj);
//                                    session.setAttribute("_apSessionID", sessionId);
//                                    session.setAttribute("_apSChannelDetails", channel);
//                                    session.setAttribute("_apOprDetail", operartorObj);
////                                        json.put("_val", 1);//for change password for 
//                                    out.print(json);
//                                    out.flush();
//                                    return;
//                                }
//
//                            }
//                            if (operartorObj.getRoleid() != 0) {
//                                if (passObj.passwordExpiryTime != 99) {//password never expire
//                                    Date passupdateDate = operartorObj.getPasswordupdatedOn();
//                                    Calendar current = Calendar.getInstance();
//                                    Calendar current1 = Calendar.getInstance();
//                                    if (passupdateDate != null) {
//                                        current.setTime(passupdateDate);
//                                        current.add(Calendar.DATE, passObj.passwordExpiryTime);
//                                        Date d = new Date();
//                                        current1.setTime(d);
//                                        Date currentDate = current1.getTime();
//
//                                        Date expireDate = current.getTime();
//                                        if (currentDate.after(expireDate)) {
//                                            result = "1sLogin";
//                                            message = "Password Expired!!!,Please change password now!!!";
//                                            json.put("_result", result);
//                                            json.put("_message", message);
//                                            json.put("_url", "#ChangePassword");
//                                            HttpSession session = request.getSession(true);
//                                            session.setAttribute("_apOprAuth", "yes");
//                                            session.setAttribute("_apOprDetail", operartorObj);
//                                            session.setAttribute("_apSessionID", sessionId);
//                                            session.setAttribute("_apSChannelDetails", channel);
//                                            session.setAttribute("_apOprDetail", operartorObj);
////                                        json.put("_val", 1);//for change password for 
//                                            out.print(json);
//                                            out.flush();
//                                            return;
//                                        }
//                                    }
//
//                                }
//                            }
//                        }
//                    }
//
//                } else {
//                    if (passObj.passwordAlert == true) {
//                        TemplateManagement tMngt = new TemplateManagement();
//                        Templates templatesObj = tMngt.LoadTemplate(sessionId, channel.getChannelid(), passObj.passwordAlertTemplateID);
////                            Templates templatesObj = tUtil.loadbyName( Templates templatesObj, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                        if (templatesObj.getStatus() == tMngt.ACTIVE_STATUS) {
//                            ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                            String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                            String strsubject = templatesObj.getSubject();
//                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
//                            if (strmessageBody != null) {
//                                // Date date = new Date();
//                                strmessageBody = strmessageBody.replaceAll("#name#", opObj.getName());
//                                strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                strmessageBody = strmessageBody.replaceAll("#email#", opObj.getEmailid());
//                                strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                strmessageBody = strmessageBody.replaceAll("#filterword#", request.getRemoteAddr());
//                            }
//                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
//                            if (aOperator != null) {
//                                String[] emailList = new String[aOperator.length - 1];
//                                for (int i = 1; i < aOperator.length; i++) {
//                                    emailList[i - 1] = aOperator[i].getEmailid();
//                                }
//
//                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                            }
//                        }
//                    }
//                }
//
//                if (retValue == 0) {
//
//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operartorObj.getOperatorid(),
//                            request.getRemoteAddr(), channel.getName(), credentialInfo[0], operartorObj.getName(),
//                            new Date(), "Login", message, SUCCESS,
//                            "Login", "", "Login Successful", "Login", operartorObj.getOperatorid());
//
//                    HttpSession session = request.getSession(true);
//                    session.setAttribute("_apOprAuth", "yes");
//                    session.setAttribute("_apOprDetail", operartorObj);
//                    session.setAttribute("_apSessionID", sessionId);
//                    session.setAttribute("_apSChannelDetails", channel);
//
//                } else {
//                    HttpSession session = request.getSession(true);
//                    session.setAttribute("_apOprAuth", null);
//                }
//
//            } catch (Exception ex) {
//                // TODO handle custom exceptions here
//                log.error("Exception caught :: ",ex);
//            }
//
//            if (retValue != 0) {
//                result = "error";
//                if (operartorObj != null && operartorObj.getStatus() != -1) { //active status to request password
//                    message = "Invalid Credentials!!! Click here to <a href=\"#\" onclick=\"forgotpassword()\" >Get Password</a>!!!";
//                } else {
//                    message = "Invalid Credentials!!!";
//                }
//                url = "index.jsp";
//            }
//
//            try {
//                json.put("_result", result);
//                json.put("_message", message);
//                json.put("_url", url);
//            } finally {
//                out.print(json);
//                out.flush();
//            }
//        } catch (Exception ex) {
//            // TODO handle custom exceptions here
//            log.error("Exception caught :: ",ex);
//            String result = "error";
//            String message = ex.getLocalizedMessage();
//            try {
//                json.put("_result", result);
//                json.put("_message", message);
//            } catch (Exception e) {
//            }
//            out.print(json);
//            out.flush();
//        }
//    }
//
//    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
//    /**
//     * Handles the HTTP <code>GET</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /**
//     * Handles the HTTP <code>POST</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /**
//     * Returns a short description of the servlet.
//     *
//     * @return a String containing servlet description
//     */
//    @Override
//    public String getServletInfo() {
//        return "Short description";
//    }// </editor-fold>
//}
