package com.mollatech.axiom.v2.face.handler.operator.template;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
import com.mollatech.axiom.nucleus.settings.OOBMobileChannelSettings;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;

import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class savetemplates extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(savetemplates.class.getName());

    final int SMS = 1;
    final int USSD = 2;
    final int VOICE = 3;
    final int EMAIL = 4;
    final String itemType = "MESSAGES";
    final int BLOCKED = -121;
final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);

        //  Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        String operatorId = operatorS.getOperatorid();
        log.debug("operatorId :: "+operatorId);

        String _types = request.getParameter("_types");
        int _templatetype = Integer.valueOf(_types);
        log.debug("_templatetype :: "+_templatetype);
        String _templatename = null;
        String _templatebody = null;
        String _templatesubject = null;

        String _taggedWith = null;
        String _templatevaribles = null;

        int _prdtype = 1;  //Integer.valueOf(_type);


        if (_templatetype == 1) {
            _templatename = request.getParameter("_templatename");
            log.debug("_templatename :: "+_templatename);
            _templatebody = request.getParameter("_templatebody");
            log.debug("_templatebody :: "+_templatebody);
            _templatesubject = request.getParameter("_templatesubject");
            log.debug("_templatesubject :: "+_templatesubject);
            _taggedWith = _templatename;
            _templatevaribles = request.getParameter("_templatevariables");
            log.debug("_templatevaribles :: "+_templatevaribles);
        } else {
            _templatename = request.getParameter("_templatenameS");
            log.debug("_templatename :: "+_templatename);
            _templatebody = request.getParameter("emailCotents");
            log.debug("_templatebody :: "+_templatebody);
            _templatesubject = request.getParameter("_templatesubjectS");
            log.debug("_templatesubject :: "+_templatesubject);
            _taggedWith = _templatename;
            _templatevaribles = request.getParameter("_templatevariablesS");
            log.debug("_templatevaribles :: "+_templatevaribles);
        }
            

        String result = "success";
        String message = "Template Added successfully....";

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
       
        if (_templatename == null || _templatebody == null || _templatevaribles == null
                || _templatename.length() == 0 || _templatebody.length() == 0 || _templatevaribles.length() == 0) {
            result = "error";
            message = "Invalid Template Details!!";
            try {json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        if (StringUtils.isWhitespace(_templatename) == true 
                || StringUtils.isWhitespace(_templatebody) == true
                || StringUtils.isWhitespace(_templatevaribles) == true) {
                 result = "error";
            message = "Invalid Template Details!!";
            try {json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
         
        }
        if (_templatetype == 2
                && _templatesubject == null) {
            result = "error";
            message = "Invalid Template Details!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(_templatename);
        boolean b = m.find();
//        if (b) {
//            result = "error";
//            message = "Username should not contain any symbols !!!";
//            try {
//                json.put("_result", result);
//                json.put("_message", message);
//            } catch (Exception ex) {
//                log.error("Exception caught :: ",ex);
//            }
//            out.print(json);
//            out.flush();
//            return;
//        } 
        if(StringUtils.isWhitespace(_templatename)){
             result = "error";
            message = "Username should not contain any symbols !!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        }
        
        boolean _allow = false;
        
        if (_templatetype == SMS) {
            try {
                SettingsManagement sMngmt = new SettingsManagement();
                Object settingsObjP = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.SMS, SettingsManagement.PREFERENCE_ONE);
                Object settingsObjS = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.SMS, SettingsManagement.PREFERENCE_TWO);
                
                OOBMobileChannelSettings mobileP = (OOBMobileChannelSettings) settingsObjP;
                OOBMobileChannelSettings mobileS = (OOBMobileChannelSettings) settingsObjS;
                
                if (mobileP.getMessageLength() == 1 || mobileS.getMessageLength() == 1) {
                    _allow = true;
                }
                
            } catch (Exception ex) {
               log.error("exception caught :: ",ex);                
            }

            if (_templatebody.length() >= 160 && _allow == false) {
                result = "error";
                message = "Template Body Length must be less than 160 characters!!!";
                try {json.put("_result", result);
                json.put("_message", message);
                }catch(Exception e){
                    log.error("exception caught :: ",e);
                }
                out.print(json);
                out.flush();
                return;
            }
        }

        int retValue = -1;
        Templates checkduplicate = null;
        TemplateManagement tObj = new TemplateManagement();
        AuditManagement audit = new AuditManagement();
        checkduplicate = tObj.CheckTemplate(sessionId, channel.getChannelid(), _templatename);


        if (checkduplicate != null) {      // 0 is present so it is duplication. report error
            result = "error";
            message = "Duplicate Name!!";
            try {json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        boolean erasable = true;


        SettingsManagement sMngmt = new SettingsManagement();
        SendNotification send = new SendNotification();


        Object obj = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.GlobalSettings, 1);

        if (obj != null) {
            GlobalChannelSettings cObj = (GlobalChannelSettings) obj;
            String strword = sMngmt.checkcontent(channel.getChannelid(), _templatebody,cObj);
            if (strword != null && cObj.contentstatus == 0) {
                if (cObj.contentalertstatus == 0) {

                    retValue = BLOCKED;
                }

                retValue = BLOCKED;

            } else {
                retValue = tObj.Addtemplate(sessionId, channel.getChannelid(), _templatebody, _templatesubject, _templatename, _templatetype, _taggedWith, false, _prdtype, _templatevaribles, erasable,ACTIVE_STATUS, new Date(), new Date());
                log.debug("Addtemplate :: "+retValue);
            }
        } else {
            retValue = tObj.Addtemplate(sessionId, channel.getChannelid(), _templatebody, _templatesubject, _templatename, _templatetype, _taggedWith, false, _prdtype, _templatevaribles, erasable,ACTIVE_STATUS, new Date(), new Date());
            log.debug("Addtemplate :: "+retValue);
        }
        //retValue = tObj.Addtemplate(sessionId, channel.getChannelid(), _templatebody, _templatesubject, _templatename, _templatetype, _taggedWith, false, _prdtype, _templatevaribles, new Date(), new Date());
        Templates tempObj = tObj.LoadbyName(sessionId, channel.getChannelid(), _templatename);
        String resultString = "ERROR";
        if (retValue == 0) {
            resultString = "SUCCESS";

            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Add Template", resultString, retValue,
                    "Template Management", "", "Template Name = " + tempObj.getTemplatename() + "Template Subject = " + tempObj.getSubject()
                    + "Tag With = " + tempObj.getTaggedWith() + "Template Variables =" + tempObj.getTemplatevariables()
                    + "Created Date = " + tempObj.getCreatedOn() + "Product Type = " + tempObj.getProducttype()
                    + "Type = " + tempObj.getType() + "Is Html = " + (tempObj.getEmailHtml()),
                    itemType, "-");

        } else if (retValue == BLOCKED) {
            //resultString = "BLOCKED";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Add Template", resultString, retValue,
                    "Template Management", "", 
                    "Add Template Blocked by Content Filter",
                    itemType, "-");
        } else  {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Add Template", resultString, retValue,
                    "Template Management", "", 
                    "Failed To Add Template",
                    itemType, "-");
        }


        if (retValue == 0) {
            result = "success";
        } else if (retValue == BLOCKED) {
            result = "BLOCKED";
            message = "Blocked By Content Filter!!!";

        } else {
            result = "error";
            message = "Template Addition Failed";

        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        }catch(Exception e){
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();

        }
        log.info("is ended :: ");
        
        

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
