/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.pkitokens;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.face.common.donut;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mollatech1
 */
public class pkidonutchart extends HttpServlet {
    
     static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(pkidonutchart.class.getName());

    public static final int SOFTWARE_TOKEN = 1;
    public static final int HARDWARE_TOKEN = 2;
//sw sub categories
    public int OTP_TOKEN_SOFTWARE_MOBILE = 2;
    public int OTP_TOKEN_SOFTWARE_PC = 3;
    public int OTP_TOKEN_SOFTWARE_WEB = 1;
    //HW Token
    public int OTP_TOKEN_HARDWARE_MINI = 1;
    public int OTP_TOKEN_HARDWARE_CR = 2;
    
     public static final int TOKEN_STATUS_ACTIVE = 1;
    public static final int TOKEN_STATUS_LOCKEd = -1;
    public static final int TOKEN_STATUS_ASSIGNED = 0;
    public static final int TOKEN_STATUS_UNASSIGNED = -10;
    public static final int TOKEN_STATUS_SUSPENDED = -2;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("is started :: ");
        PrintWriter out = response.getWriter();

        try {
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel.getName());
            String channelId = channel.getChannelid();
            log.debug("channelId :: "+channelId);
            String category = request.getParameter("_changePkiCategory");
            log.debug("category :: "+category);
            String _startDate = request.getParameter("_startDate");
            log.debug("_startDate :: "+_startDate);
            String _endDate   = request.getParameter("_endDate");
            log.debug("_endDate :: "+_endDate);
            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            Date sDate = null; Date eDate=null;
            if(_startDate != null && !_startDate.isEmpty()){
                sDate = formatter.parse(_startDate);
            }
            if(_endDate != null && !_endDate.isEmpty()){
                eDate = formatter.parse(_endDate);
            }
            int icategory = -9999;
            if (category != null & category.isEmpty() != true) {
                icategory = Integer.parseInt(category);
            }
            ArrayList<donut> sample = new ArrayList<donut>();
            PKITokenManagement pObj = new PKITokenManagement();
           
                int iActive = pObj.getPkiTokenCountV2(channelId, icategory,TOKEN_STATUS_ACTIVE,sDate,eDate);
                int iAssign = pObj.getPkiTokenCountV2(channelId, icategory, TOKEN_STATUS_ASSIGNED,sDate,eDate);
                int iLocked = pObj.getPkiTokenCountV2(channelId, icategory, TOKEN_STATUS_LOCKEd,sDate,eDate);
                int iSuspend = pObj.getPkiTokenCountV2(channelId,icategory, TOKEN_STATUS_SUSPENDED,sDate,eDate);
                int iUnAssign = pObj.getPkiTokenCountV2(channelId,icategory, TOKEN_STATUS_UNASSIGNED,sDate,eDate);
                sample.add(new donut(iActive, "Active"));
                sample.add(new donut(iAssign, "Assign"));
                sample.add(new donut(iLocked, "Locked"));
                sample.add(new donut(iSuspend, "Suspended"));
                sample.add(new donut(iUnAssign, "UnAssign"));
                
          
                // sample.add(new Bar(blockedcount, "BLOCKED"));

                for (int i = 0; i < sample.size(); i++) {
                    //System.out.println(sample.get(i));
                }
                Gson gson = new Gson();

                JsonElement element = gson.toJsonTree(sample, new TypeToken<List<donut>>() {
                }.getType());

                JsonArray jsonArray = element.getAsJsonArray();
                //response.setContentType("application/json");
                out.print(jsonArray);

            }catch(Exception e){
                log.error("ecxeption caught :: ",e);
            }finally {
            out.close();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
