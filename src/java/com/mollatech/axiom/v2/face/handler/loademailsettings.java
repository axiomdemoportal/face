/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.OOBEmailChannelSettings;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class loademailsettings extends HttpServlet {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loademailsettings.class.getName());
    private JSONObject SettingsWhenEmpty(int _type1, int _preference1, Object settingsObj) {
        log.info("Servlet started");
        JSONObject json = new JSONObject();
        if (_type1 == com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.EMAIL && _preference1 == com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE) {
            try { json.put("_className", "");
            json.put("_ip", "");
            json.put("_port", 25);
            json.put("_userId", "");
            json.put("_password", "");
            json.put("_reserve1", "");
            json.put("_reserve2", "");
            json.put("_reserve3", "");
            json.put("_status", 0);
            json.put("_retries", 2);
            json.put("_autofailover", 1);
            json.put("_fromname", "");
            json.put("_fromemail", "");
            json.put("_isAuthRequired", false);
            json.put("_isSsl", false);
            json.put("_retryduration", 10 );
            }catch(Exception e){log.error("Exception caught :: ",e);}
        } else if (_type1 == com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.EMAIL && _preference1 == com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_TWO) {
            try { json.put("_className", "");
            json.put("_ip", "");
            json.put("_port", 25);
            json.put("_userId", "");
            json.put("_password", "");
            json.put("_reserve1", "");
            json.put("_reserve2", "");
            json.put("_reserve3", "");
            json.put("_status", 0);
            json.put("_retries", 2);
            json.put("_autofailover", 1);
            json.put("_fromname", "");
            json.put("_fromemail", "");
            json.put("_isAuthRequired", false);
            json.put("_isSsl", false);
            json.put("_retryduration", 10 );
            }catch(Exception e){log.error("Exception caught :: ",e);}
        }
        return json;
    }

    private JSONObject SettingsWhenPresent(Object settingsObj) {
        JSONObject json = new JSONObject();
        if (settingsObj instanceof OOBEmailChannelSettings) {
            OOBEmailChannelSettings emailObj = (OOBEmailChannelSettings) settingsObj;
            if (emailObj.getPreference() == com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE) {
                try { json.put("_className", emailObj.getClassName());
                json.put("_ip", emailObj.getSmtpIp());
                json.put("_port", emailObj.getPort());
                json.put("_userId", emailObj.getUserId());
                json.put("_password", emailObj.getPassword());
                json.put("_reserve1", emailObj.getReserve1());
                json.put("_reserve2", emailObj.getReserve2());
                json.put("_reserve3", emailObj.getReserve3());
                json.put("_status", emailObj.getStatus());
                json.put("_retries", emailObj.getRetrycount());
                json.put("_autofailover", emailObj.getAutofailover());
                json.put("_fromname", emailObj.getFromName());
                json.put("_fromemail", emailObj.getFromEmail());
                json.put("_isAuthRequired", emailObj.isAuthRequired());
                json.put("_isSsl", emailObj.isSsl());
                json.put("_retryduration", emailObj.getRetryduration());    
                }catch(Exception e){log.error("Exception caught :: ",e);}
            } else {
                try { json.put("_className", emailObj.getClassName());
                json.put("_ip", emailObj.getSmtpIp());
                json.put("_port", emailObj.getPort());
                json.put("_userId", emailObj.getUserId());
                json.put("_password", emailObj.getPassword());
                json.put("_reserve1", emailObj.getReserve1());
                json.put("_reserve2", emailObj.getReserve2());
                json.put("_reserve3", emailObj.getReserve3());
                json.put("_status", emailObj.getStatus());
                json.put("_retries", emailObj.getRetrycount());
                //json.put("_autofailover", emailObj.getAutofailover());
                json.put("_fromname", emailObj.getFromName());
                json.put("_fromemail", emailObj.getFromEmail());
                json.put("_isAuthRequired", emailObj.isAuthRequired());
                json.put("_isSsl", emailObj.isSsl());
                json.put("_retryduration", emailObj.getRetryduration());
                }catch(Exception e){log.error("Exception caught :: ",e);}
            }
        }
        return json;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");

        String _preference = request.getParameter("_preference");
        int _preference1 = Integer.parseInt(_preference);
        
        log.debug("channel is::"+channel.getName());
        log.debug("sessionid::"+sessionId);
        log.debug("preference is::"+_preference1);
                
        int _type1 = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.EMAIL;

        JSONObject json = null;//new JSONObject();
        PrintWriter out = response.getWriter();

        try {
            SettingsManagement sMngmt = new SettingsManagement();
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _type1, _preference1);
            if (settingsObj != null) {
                json = SettingsWhenPresent(settingsObj);
            } else {
                json = SettingsWhenEmpty(_type1, _preference1, settingsObj);
            }

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
            // TODO handle custom exceptions here
        }
        try {
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet stoped");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
