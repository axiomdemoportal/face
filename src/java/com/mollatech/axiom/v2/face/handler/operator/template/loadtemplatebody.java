package com.mollatech.axiom.v2.face.handler.operator.template;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class loadtemplatebody extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadtemplatebody.class.getName());

   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        log.info("is started :: ");
        PrintWriter out = response.getWriter();
          Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
          log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        
          String _tid  = request.getParameter("_tid");
          int tid = Integer.parseInt(_tid);
          log.debug("tid :: "+tid);
          Templates tObj = null;
        try {
            TemplateManagement template = new TemplateManagement();
          tObj =  template.LoadTemplate(sessionId, channel.getChannelid(), tid);
           
        } finally {   
            
            ByteArrayInputStream bais = new ByteArrayInputStream(tObj.getTemplatebody());
            String templatebody = (String) UtilityFunctions.deserializeFromObject(bais);
            out.print(templatebody);
            out.close();
        }
        
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
