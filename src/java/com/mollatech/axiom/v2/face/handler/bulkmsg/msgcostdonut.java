/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.axiom.v2.face.handler.bulkmsg;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.face.common.bar;
import com.mollatech.axiom.face.common.donut;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.dictum.management.BulkMSGManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mollatech1
 */
public class msgcostdonut extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(msgcostdonut.class.getName());
//traditional
    final int SMS = 1;
    final int VOICE = 2;
    final int USSD = 3;
    final int EMAIL = 4;
    final int FAX = 5;
//social
    final int FACEBOOK = 6;
    final int LINKEDIN = 7;
    final int TWITTER = 8;
 //push
     final int PUSHANDROID = 18;
     final int PUSHIPHONE = 19;
     
      int DICTUM = 1;
    int ECOPIN = 2;
    int AXIOM = 3;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        log.info("is started :: ");

        try {
            response.setContentType("application/json");
            PrintWriter out = response.getWriter();
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel::" + channel.getName());
            String channelId = channel.getChannelid();
            log.debug("channelId ::"+ channelId);
            String _startdate = request.getParameter("_startdate");
            log.debug("_startdate ::" + _startdate);
            String _enddate = request.getParameter("_enddate");
            log.debug("_enddate ::"+_enddate);
            String _type = request.getParameter("_type");
            log.debug("_type::"+_type);
            

            int itype = 0;
            if (_type != null) {
                itype = Integer.parseInt(_type);
            }

            DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            Date startDate = null;
            if (_startdate != null && !_startdate.isEmpty()) {
                startDate = (Date) formatter.parse(_startdate);
            }
            Date endDate = null;
            if (_enddate != null && !_enddate.isEmpty()) {
                endDate = (Date) formatter.parse(_enddate);
            }

            try {

                BulkMSGManagement mngt = new BulkMSGManagement();
                ArrayList<donut> sample = new ArrayList<donut>();
                if (itype == 1) {
                    Float smscost = mngt.getMessageCost(channelId, SMS, startDate, endDate);
                    log.debug("getMessageCost::"+smscost);
                    Float voicecost = mngt.getMessageCost(channelId, VOICE, startDate, endDate);
                    log.debug("getMessageCost::"+voicecost);
                    Float ussdcost = mngt.getMessageCost(channelId, USSD, startDate, endDate);
                    log.debug("getMessageCost::"+ussdcost);
                    Float emailcost = mngt.getMessageCost(channelId, EMAIL, startDate, endDate);
                    log.debug("getMessageCost::"+emailcost);
                       String strProductType = LoadSettings.g_sSettings.getProperty("product.type");
                       if(strProductType != null){
                       int iPRODUCT = (new Integer(strProductType)).intValue();
                    if(iPRODUCT == DICTUM){
                    Float faxcost = mngt.getMessageCost(channelId, FAX, startDate, endDate);
                    log.debug("getMessageCost::"+faxcost);
                    sample.add(new donut(Math.round(faxcost), "FAX Cost"));
                    }
                    }
                    sample.add(new donut(Math.round(smscost), "SMS Cost"));
                    sample.add(new donut(Math.round(voicecost), "VOICE Cost"));
                    sample.add(new donut(Math.round(ussdcost), "USSD Cost"));
                    sample.add(new donut(Math.round(emailcost), "EMAIL Cost"));
                   
                } else if (itype == 2) {
                    Float facebookcost = mngt.getMessageCost(channelId, FACEBOOK, startDate, endDate);
                    log.debug("getMessageCost::"+facebookcost);
                    Float twittercost = mngt.getMessageCost(channelId, TWITTER, startDate, endDate);
                    log.debug("getMessageCost::"+twittercost);
                    Float linkedincost = mngt.getMessageCost(channelId, LINKEDIN, startDate, endDate);
                    log.debug("getMessageCost::"+linkedincost);

                    sample.add(new donut(Math.round(facebookcost), "FACEBOOK Cost"));
                    sample.add(new donut(Math.round(twittercost), "TWITTER Cost"));
                    sample.add(new donut(Math.round(linkedincost), "LINKEDIN Cost"));
                    
                }else if (itype == 3) {
                    Float pushandroidcost = mngt.getMessageCost(channelId, PUSHANDROID, startDate, endDate);
                    log.debug("getMessageCost::"+pushandroidcost);
                    Float pushiphonecost = mngt.getMessageCost(channelId, PUSHIPHONE, startDate, endDate);
                    log.debug("getMessageCost::"+pushiphonecost);
                  
                    sample.add(new donut(Math.round(pushandroidcost), "ANDROID PUSH Cost"));
                    sample.add(new donut(Math.round(pushiphonecost), "IPHONE PUSH Cost"));
                   
                }

                Gson gson = new Gson();

                JsonElement element = gson.toJsonTree(sample, new TypeToken<List<donut>>() {
                }.getType());

                JsonArray jsonArray = element.getAsJsonArray();
                out.print(jsonArray);

            } finally {
                out.close();
            }
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
