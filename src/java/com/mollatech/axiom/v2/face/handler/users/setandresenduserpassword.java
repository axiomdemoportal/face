/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.users;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.AuthorizationManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PasswordTrailManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.connector.access.controller.ApprovalSetting;
import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
import com.mollatech.axiom.nucleus.settings.PasswordPolicySetting;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class setandresenduserpassword extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(setandresenduserpassword.class.getName());

    final String itemtype = "USERPASSWORD";
    final String itemTypeAUTH = "AUTHORIZATION";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());

        String _userid = request.getParameter("_userid");
        log.debug("_userid :: "+_userid);
        String result = "success";
        String message = "Password set and send successfully, please check your phone/email....";
        // String strPassword = null;
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        int iapprovalID = -1;
        if (_userid == null) {
            result = "error";
            message = "Fill all Details!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.USER_PASSWORD) != 0) {
            result = "error";
            message = "This feature is not available in this license!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
               log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        int retValue = -1;
        //new addition for password policy enforcement
        UserManagement uManagement = new UserManagement();
        AuthUser user = uManagement.getUser(sessionId, channel.getChannelid(), _userid);
        UtilityFunctions u = new UtilityFunctions();
        Date d = new Date();
        String strPassword = u.HexSHA1(sessionId + channel.getChannelid() + _userid + d.toString());
        strPassword = strPassword.substring(0, 9);
        u = null;
        PasswordTrailManagement setObj = new PasswordTrailManagement();
        String password = setObj.GeneratePassword(channel.getChannelid(), strPassword);
        if (password != null && !password.isEmpty()) {
            strPassword = password;
        }
        String resultString = "ERROR";
        SettingsManagement settObj = new SettingsManagement();
        AuditManagement audit = new AuditManagement();
        Object obj = settObj.getSettingInner(channel.getChannelid(), SettingsManagement.PASSWORD_POLICY_SETTING,
                SettingsManagement.PREFERENCE_ONE);
        String _approvalId = request.getParameter("_approvalId");
        AuthorizationManagement auth = new AuthorizationManagement();
        ApprovalSetting approvalSetting = null;
        if (obj != null) {
            if (obj instanceof PasswordPolicySetting) {
                PasswordPolicySetting passwordSetting = (PasswordPolicySetting) obj;

                Date pUpdateDate = user.getPasswordupdatedOn();
                Calendar pcurrent = Calendar.getInstance();
                if (pUpdateDate == null) {
                    pcurrent.setTime(new Date());
                } else {
                    pcurrent.setTime(pUpdateDate);
                }
                pcurrent.set(Calendar.AM_PM, Calendar.AM);
                pcurrent.add(Calendar.MONTH, 1);//1 month
                Date pendDate = pcurrent.getTime();

                Calendar tDate = Calendar.getInstance();
                if (pUpdateDate == null) {
                    tDate.setTime(new Date());
                } else {
                    tDate.setTime(pUpdateDate);
                }
                Date tdate = tDate.getTime();

                int issuePasswordCount = setObj.getcountOfIssuePassword(channel.getChannelid(),
                        user.getUserId(), pendDate, tdate);

                if (passwordSetting.issuingLimitDuration != 99) { //no issue limit
                    if (passwordSetting.issuingLimitDuration <= issuePasswordCount) {
                        result = "error";
                        message = "Password Issue Limit Reach...!!!";
                        audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                                remoteaccesslogin, operatorS.getName(), new Date(), "Set Password", resultString, retValue,
                                "User Management", "Current Password = ******", "Current Password = ******", itemtype, _userid);
                        try {
                            json.put("_result", result);
                            json.put("_message", message);
                        } catch (Exception e) {
                            log.error("exception caught :: ",e);
                        }
                        out.print(json);
                        out.flush();
                        return;
                    }
                }
                Date date = user.getPasswordupdatedOn();
                int oldpasswordreusein = passwordSetting.oldPasswordCannotbeReused;
                Calendar current = Calendar.getInstance();
                if (date == null) {
                    current.setTime(new Date());
                } else {
                    current.setTime(date);
                }
                current.set(Calendar.AM_PM, Calendar.AM);
                Calendar current1 = Calendar.getInstance();
                current1.setTime(new Date());
                current1.set(Calendar.AM_PM, Calendar.AM);
                current1.add(Calendar.MONTH, oldpasswordreusein);//add a date

                int validPassword = setObj.ValidatePassword(channel.getChannelid(), strPassword,
                        passwordSetting);
                AuthorizationManagement authMngt = new AuthorizationManagement();
                if (validPassword == 0) {

                    OperatorsManagement oprMngt = new OperatorsManagement();
                    UserManagement uMngt = new UserManagement();

                    retValue = uManagement.AssignPassword(sessionId, channel.getChannelid(),
                            _userid, strPassword);
                    log.debug("AssignPassword :: "+retValue);

                    if (retValue == 0) {

                        String pas = setObj.MD5HashPassword(strPassword);
                        setObj.AddPasswordTrail(channel.getChannelid(), _userid, pas);
                    }
                }

                if (validPassword != 0) {
                    result = "error";
                    if (validPassword == -1) {
                        message = "Password Length Not Match to criteria...!!!";
                    } else if (validPassword == -2) {
                        message = "Password Must Have at least one Digit(e.g. 0,1 etc.)";
                    } else if (validPassword == -3) {
                        message = "Password Must Have at least one small-case letter(e.g. a,b,c etc.)";
                    } else if (validPassword == -4) {
                        message = "Password Must Have at least one upper-case letter(e.g. A,B,C etc.)";
                    } else if (validPassword == -4) {
                        message = "Password Must Have at least one Special character(e.g. !,@,# etc.)";
                    } else {
                        message = "Invalid Password!!!";
                    }
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), "Set Password", resultString, retValue,
                            "User Management",
                            "",
                            message, itemtype, _userid);

                    result = "error";
                    //message = "password reset failed!!";
                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("exception caught :: ",e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }

            }
        }
        //end of addition

        if (retValue == 0) {
            resultString = "SUCCESS";
        }
        if (retValue == 0) {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Set  Password", resultString, retValue,
                    "User Management",
                    "",
                    "New password set", itemtype, _userid);

        } else {

            result = "error";
            message = "password reset failed!!";
            resultString = "ERROR";

            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Set Password", resultString, retValue,
                    "User Management",
                    "",
                    message,
                    itemtype, _userid);

            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        }

        strPassword = uManagement.GetPassword(sessionId, channel.getChannelid(), _userid);

        TemplateManagement tManagement = new TemplateManagement();
        AXIOMStatus status = new AXIOMStatus();
        status.iStatus = -1;
        status.strStatus = "Failed";
        Templates templates = tManagement.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.MOBILE_USER_PASSWORD_TEMPLATE);
        if (templates.getStatus() == tManagement.ACTIVE_STATUS) {
            ByteArrayInputStream bais = new ByteArrayInputStream(templates.getTemplatebody());
            String tmessage = (String) TemplateUtils.deserializeFromObject(bais);
            //String tsubject = templates.getSubject();
            //ChannelManagement cmObj = new ChannelManagement();
            //Channels channel = cmObj.getChannelByID(channelId);
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
            if (message != null) {
                d = new Date();
                tmessage = tmessage.replaceAll("#name#", user.getUserName());
                tmessage = tmessage.replaceAll("#channel#", channel.getName());
                tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                tmessage = tmessage.replaceAll("#password#", strPassword);
            }

            //end of addition
            if (user == null || strPassword == null) {
                result = "error";
                message = "Password Not Sent!!";
                AuditManagement ad = new AuditManagement();
                ad.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operatorS.getName(), new Date(), "Re/Send Password", resultString, retValue,
                        "User Management",
                        "",
                        message,
                        itemtype,
                        _userid);
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    log.error("exception caught :: ",e);
                }
                out.print(json);
                out.flush();
                return;
            }

            SendNotification send = new SendNotification();
            send.SendEmail(channel.getChannelid(),user.email,null, tmessage, null, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
            //EnforceGlobalSettings()
            status = send.SendOnMobileNoWaiting(channel.getChannelid(), user.getPhoneNo(), tmessage, 1, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
        }
        if (status.iStatus == SendNotification.PENDING) {

            AuditManagement ad = new AuditManagement();
            ad.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Re/Send Password", "SUCCESS", status.iStatus,
                    "User Management",
                    "",
                    "Password sent to respective gateway",
                    itemtype,
                    _userid);
        } else if (status.iStatus != SendNotification.PENDING) {

            AuditManagement ad = new AuditManagement();
            ad.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Send Password", "ERROR", status.iStatus,
                    "User Management",
                    "",
                    "Failed To Send Password" + status.iStatus,
                    itemtype,
                    _userid);

//            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(),
//                    channel.getName(),
//                    remoteaccesslogin, operatorS.getName(), new Date(),
//                    "Resend Password", "Failure", status.iStatus,
//                    "User Management", "", "Failed To Send Password" + status.iStatus,
//                    itemtype, _userid);
            result = "error";
            message = "Password Not Sent!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception e) {
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    private int m_prefixCheck = -1;
    private int m_domainCheck = -1;
    private int m_ipCheck = -1;
    private int m_contentCheck = -1;

    private int EnforceglobalSettings(String channelid, String uphonenumber, String uemail, String uip, String dataTosend) {
        SettingsManagement sManagement = new SettingsManagement();
        Object objec = sManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, SettingsManagement.PREFERENCE_ONE);
        GlobalChannelSettings globalSetting = (GlobalChannelSettings) objec;

//        if ( uphonenumber != null)
//             m_prefixCheck = sManagement.checkPrefixV2(globalSetting, uphonenumber);
//        
//        if ( uphonenumber != null)
//         m_domainCheck = sManagement.checkDomain(channelid, uemail,globalSetting);
//        
//        if ( uphonenumber != null)
//            m_ipCheck = sManagement.checkIP(channelid, uip);
//        
//        if ( uphonenumber != null)
//           m_contentCheck = sManagement.checkContent()
        return -100;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static Object createObject(Constructor constructor, Object[] arguments) {

        //System.out.println("Constructor: " + constructor.toString());
        Object object = null;

        try {
            object = constructor.newInstance(arguments);
            //System.out.println("Object: " + object.toString());
            return object;
        } catch (Exception e) {
            log.error("exception caught :: ",e);
        }
        return object;
    }
}
