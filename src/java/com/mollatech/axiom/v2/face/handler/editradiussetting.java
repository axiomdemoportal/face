/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.AxiomRadiusClient;
import com.mollatech.axiom.nucleus.settings.AxiomRadiusConfiguration;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class editradiussetting extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editradiussetting.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final String itemtype = "SETTINGS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String OperatorID = operatorS.getOperatorid();

        String _channelId = channel.getChannelid();
        SettingsManagement sMngmt = new SettingsManagement();
        int retValue = -1;

        String result = "success";
        String message = "Radius Setting Update Successful!!!";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
  
        boolean _accountEnabled =true;  //request.getParameter("_accountEnabled") != null;
        String _accountIp ="localhost"; //request.getParameter("_accountIp");
        int _accountPort =11111; // Integer.parseInt(request.getParameter("_accountPort"));
        boolean _authEnabled = request.getParameter("_authEnabled") != null;
        String _authIp = request.getParameter("_authIp");
        int _authPort = Integer.parseInt(request.getParameter("_authPort"));
        String _ldapUsername = ""; //request.getParameter("_ldapUsername");
        String _ldapPassword = ""; //request.getParameter("_ldapPassword");
        String _ldapServerIp = request.getParameter("_ldapServerIp");
        int _ldapServerPort = Integer.parseInt(request.getParameter("_ldapServerPort"));
        String _ldapSearchinitials = request.getParameter("_ldapSearchinitials");
        String _ldapSearchPath = request.getParameter("_ldapSearchPath");

        String _ldapValidate = request.getParameter("_ldapValidate");
        
            log.debug("channelgetName :: " + channel.getName());
            log.debug("channelgetChannelid :: " + channel.getChannelid());
            log.debug("operatorS :: " + operatorS.getOperatorid());
            log.debug("sessionId :: " + sessionId);
            log.debug("remoteaccesslogin :: " + remoteaccesslogin);
            log.debug("_authIp :: "+_authIp);
            log.debug("_authPort :: " + _authPort);
            log.debug("_ldapServerIp :: " + _ldapServerIp);
            log.debug("_ldapServerPort :: " + _ldapServerPort);
            log.debug("_ldapSearchinitials :: " + _ldapSearchinitials);
            log.debug("_ldapSearchPath :: " + _ldapSearchPath);
            
            
        int iType = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.Radius;
        int iPreference = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE;

        //   String strType = String.valueOf(iType);
        Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), iType, iPreference);

        AxiomRadiusConfiguration RadiusServerObj = null;
        boolean bAddSetting = false;

        if (settingsObj == null) {
            RadiusServerObj = new AxiomRadiusConfiguration();
            bAddSetting = true;
        } else {
            RadiusServerObj = (AxiomRadiusConfiguration) settingsObj;
        }
        RadiusServerObj.setAccountEnabled(_accountEnabled);
        RadiusServerObj.setAccountIp(_accountIp);
        RadiusServerObj.setAccountPort(_accountPort);
        RadiusServerObj.setAuthEnabled(_authEnabled);
        RadiusServerObj.setAuthIp(_authIp);
        RadiusServerObj.setAuthPort(_authPort);
        RadiusServerObj.setLdapServerUsername(_ldapUsername);
        RadiusServerObj.setLdapServerPassword(_ldapPassword);
        RadiusServerObj.setLdapSearchInitial(_ldapSearchinitials);
        RadiusServerObj.setLdapSearchPath(_ldapSearchPath);
        RadiusServerObj.setLdapServerIp(_ldapServerIp);
        RadiusServerObj.setLdapServerPort(_ldapServerPort);

        if (_ldapValidate.equalsIgnoreCase("ldap")) {
            RadiusServerObj.setLdapValidate(true);
        } else {
            RadiusServerObj.setLdapValidate(false);
        }

        if (_ldapValidate.equalsIgnoreCase("axiom")) {
            RadiusServerObj.setAxiomValidate(true);
        } else {
            RadiusServerObj.setAxiomValidate(false);
        }

        if (bAddSetting == false) {
            AxiomRadiusClient[] radiusClient = null;
            radiusClient = RadiusServerObj.getRadiusClient();
            RadiusServerObj.setRadiusClient(radiusClient);
        }

        AuditManagement audit = new AuditManagement();
        if (bAddSetting == true) {
            String resultString = "ERROR";
            retValue = sMngmt.addSetting(sessionId, _channelId, iType, iPreference, RadiusServerObj);
            log.debug("retValue :: " + retValue);

            if (retValue == 0) {

                result = "success";
                message = "Radius Settings added Successfully!!!";
                resultString = "SUCCCESS";

                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add Radius Settings", resultString, retValue, "Setting Management",
                        "", "Accounting=" + RadiusServerObj.isAccountEnabled() + ",AccountIP=" + RadiusServerObj.getAccountIp() + ",AccountPort=" + RadiusServerObj.getAccountPort()
                        + ",Authentication=" + RadiusServerObj.isAuthEnabled() + ",AuthIP=" + RadiusServerObj.getAuthIp() + ",AuthPort=" + RadiusServerObj.getAuthPort()
                        + ",AxiomValidate=" + RadiusServerObj.isAxiomValidate()
                        + " LDAPValidate =" + RadiusServerObj.isLdapValidate() + ",LDAPIP =" + RadiusServerObj.getLdapServerIp() + ",LDAPPort =" + RadiusServerObj.getLdapServerPort() + ",LDAPSearchpath =" + RadiusServerObj.getLdapSearchPath() + "LDAPUSER =" + RadiusServerObj.getLdapServerUsername() + "LDAPPassword =" + RadiusServerObj.getLdapServerPassword() + "otpspecification =",
                        itemtype, _channelId);

            } 
                else {
                result = "error";
                message = "Radius Settings Addition Failed!!!";
                resultString = "ERROR";

                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add Radius Settings", resultString, retValue, "Setting Management",
                        "", 
                        "Failed To Add Radius Settings",
                        itemtype, _channelId);

            }

        } else {
            AxiomRadiusConfiguration oldglobalObj = (AxiomRadiusConfiguration) sMngmt.getSetting(sessionId, _channelId, iType, iPreference);
            retValue = sMngmt.changeSetting(sessionId, _channelId, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.Radius, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE, settingsObj, RadiusServerObj);
            log.debug("retValue :: " + retValue);
            
            String resultString = "ERROR";
            if (retValue == 0) {

                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Change Radius Settings", resultString, retValue, "Setting Management",
                        "", "Accounting=" + oldglobalObj.isAccountEnabled() + ",AccountIP=" + oldglobalObj.getAccountIp() + ",AccountPort=" + oldglobalObj.getAccountPort()
                        + ",Authentication=" + oldglobalObj.isAuthEnabled() + ",AuthIP=" + oldglobalObj.getAuthIp() + ",AuthPort=" + oldglobalObj.getAuthPort()
                        + ",AxiomValidate=" + oldglobalObj.isAxiomValidate()
                        + " LDAPValidate =" + oldglobalObj.isLdapValidate() + ",LDAPIP =" + oldglobalObj.getLdapServerIp() + ",LDAPPort =" + oldglobalObj.getLdapServerPort() + ",LDAPSearchpath =" + oldglobalObj.getLdapSearchPath() + "LDAPUSER =" + oldglobalObj.getLdapServerUsername() + "LDAPPassword =******"  + "otpspecification ="
                        + " Accounting=" + RadiusServerObj.isAccountEnabled() + ",AccountIP=" + RadiusServerObj.getAccountIp() + ",AccountPort=" + RadiusServerObj.getAccountPort()
                        + ",Authentication=" + RadiusServerObj.isAuthEnabled() + ",AuthIP=" + RadiusServerObj.getAuthIp() + ",AuthPort=" + RadiusServerObj.getAuthPort()
                        + ",AxiomValidate=" + RadiusServerObj.isAxiomValidate() + " LDAPValidate =" + RadiusServerObj.isLdapValidate() + ",LDAPIP =" + RadiusServerObj.getLdapServerIp() + ",LDAPPort =" + RadiusServerObj.getLdapServerPort() + ",LDAPSearchpath =" + RadiusServerObj.getLdapSearchPath() + "LDAPUSER =" + RadiusServerObj.getLdapServerUsername() + "LDAPPassword =*******",
                        itemtype, _channelId);
            } else if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Change Radius Settings", resultString, retValue, "Setting Management",
                        "", 
                         "Failed To Change Radius Settings",
                        itemtype, _channelId);
            }
        }
        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
