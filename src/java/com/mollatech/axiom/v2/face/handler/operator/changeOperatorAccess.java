/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.axiom.v2.face.handler.operator;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author nilesh
 */
public class changeOperatorAccess extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(changeOperatorAccess.class.getName());
    
    final String itemTypeOp = "OPERATOR";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       response.setContentType("application/json");
        log.info("is started :: ");
       
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String _op_status =request.getParameter("_op_type");
        
        String _operId =request.getParameter("_oprid");
        log.debug("_operId :: "+_operId);
         //audit
        Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operator :: "+operator);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
//        String _old_op_status = request.getParameter("_op_status");

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Access Type Update Successfully!!!";

        int retValue = 0;
        int status =Integer.parseInt(_op_status);
        log.debug("status :: "+status);
        String _value = "Role";
        if ( status == OperatorsManagement.OPERATOR_ACCESS ) {
            _value = "Operator";
        }
      
        OperatorsManagement oManagement = new OperatorsManagement();   
        AuditManagement audit = new AuditManagement();
         Operators oldOpObj = oManagement.getOperatorById(channel.getChannelid(), _operId);
        retValue = oManagement.ChangeAccess(sessionId, channel.getChannelid(), _operId,status);
        log.debug("ChangeAccess :: "+retValue);
         
       int istatus =  oldOpObj.getAccessType();   
       String strStatus = "";
       if(istatus == OperatorsManagement.ROLE_ACCESS){
           strStatus = "ROLE ACCESS";
       }else if(istatus == OperatorsManagement.OPERATOR_ACCESS){
           strStatus = "OPERATOR ACCESS";
       }
        String strNewStatus = "";
       if(status == OperatorsManagement.ROLE_ACCESS){
           strNewStatus = "ROLE ACCESS";
       }else if(status == OperatorsManagement.OPERATOR_ACCESS){
           strNewStatus = "OPERATOR ACCESS";
       }
      
        String resultString="ERROR";
        
        if(retValue == 0){
            resultString ="SUCCESS";
             audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                     request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                        "Change Access Type", resultString, retValue, "Operator Management",
                        "Old Type="+strStatus,"New Type ="+strNewStatus,
                        itemTypeOp, _operId);
        }
        else {
               audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                       request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                        "Change Access Status", resultString, retValue, "Operator Management",
                        "Old Type="+strStatus,"Failed To Change Access Type",
                        itemTypeOp, _operId);
            result = "error";
            message = "Access Update Failed!!!";
         }
        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);
        } catch (Exception ex) {
           log.error("exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
