/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.IPConfigFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech1
 */
public class editipfiltersetting extends HttpServlet {
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editipfiltersetting.class.getName());

    final String itemtype = "SETTINGS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        try {
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");

            //audit parameter
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
            String OperatorID = operatorS.getOperatorid();

            String istatus = request.getParameter("_statusIP");
            int _istatus = Integer.parseInt(istatus);

            String ialertstatus = request.getParameter("_alertOperator");
            int _ialertstatus = Integer.parseInt(ialertstatus);

            String _channelId = channel.getChannelid();
            SettingsManagement sMngmt = new SettingsManagement();
            int retValue = -1;

            String result = "success";
            String message = "Allowed IP Updated Successfully!!!";
            JSONObject json = new JSONObject();

            
                log.debug("channel :: " + channel.getName());
                log.debug("operatorS :: " + operatorS.getOperatorid());
                log.debug("sessionId :: " + sessionId);
                log.debug("remoteaccesslogin :: " + remoteaccesslogin);
                log.debug("getChannelid :: " + channel.getChannelid());
                log.debug("_istatus :: "+_istatus);
                log.debug("_ialertstatus :: "+_ialertstatus);
            //String _preference = request.getParameter("_perferenceContent");
            int _iPreference = 1;//Integer.parseInt(_preference);

            //String _type = request.getParameter("_type");
            int _itype = SettingsManagement.IP_FILTER;//Integer.parseInt(_type);
            String strType = String.valueOf(_itype);
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _itype, _iPreference);

            IPConfigFilter ipObj = null;
            boolean bAddSetting = false;

            if (settingsObj == null) {
                ipObj = new IPConfigFilter();
                ipObj.setChannelId(_channelId);
                ipObj.setStatus(_istatus);
                ipObj.setAlertstatus(_ialertstatus);

                bAddSetting = true;
            } else {
                ipObj = (IPConfigFilter) settingsObj;
            }

            String _ip = request.getParameter("_ip");
            String[] _ipArray = _ip.split(",");
            if (_itype == com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.IP_FILTER) {
                ipObj.setChannelId(_channelId);
                ipObj.setIP(_ipArray);
                ipObj.setStatus(_istatus);
                ipObj.setAlertstatus(_ialertstatus);
            }
            AuditManagement audit = new AuditManagement();
            if (bAddSetting == true) {

                String[] arrIp = ipObj.getIP();
                String strContent = null;
                for (int i = 0; i < arrIp.length; i++) {
                    strContent += arrIp[i] + ",";
                }
                int iStatus = ipObj.getStatus();
                String strStatus = "INACTIVE";
                if (iStatus == 0) {
                    strStatus = "ACTIVE";
                }
                int iopalertstatus = ipObj.getAlertstatus();
                String stropalertstatus = "INACTIVE";
                if (iopalertstatus == 0) {
                    stropalertstatus = "ACTIVE";
                }
                retValue = sMngmt.addSetting(sessionId, _channelId, _itype, _iPreference, ipObj);
                log.debug("editipfiltersetting::addSetting" +retValue);
                String resultString = "ERROR";

                if (retValue == 0) {

                    resultString = "SUCCESS";

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add IP List", resultString, retValue, "Setting Management",
                            "", "New Content =" + strContent + "Status =" + strStatus + "Operator Alert Status =" + stropalertstatus,
                            itemtype, channel.getChannelid());

                } else if (retValue != 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add IP List", resultString, retValue, "Setting Management",
                            "", "Failed To Add IP List", itemtype, channel.getChannelid());

                }

            } else {
                IPConfigFilter ipOldObj = (IPConfigFilter) sMngmt.getSetting(sessionId, _channelId, _itype, _iPreference);
                retValue = sMngmt.changeSetting(sessionId, _channelId, _itype, _iPreference, settingsObj, ipObj);
                log.debug("editipfiltersetting::changeSetting" +retValue);
                String resultString = "ERROR";

                String[] arroldContent = ipOldObj.getIP();
                String strOldIp = null;
                for (int i = 0; i < arroldContent.length; i++) {
                    strOldIp += arroldContent[i] + ",";
                }
                int iStatus = ipOldObj.getStatus();
                String strStatus = "INACTIVE";
                if (iStatus == 0) {
                    strStatus = "ACTIVE";
                }
                String[] arrnewIp = ipObj.getIP();
                String strnewIp = null;
                for (int i = 0; i < arrnewIp.length; i++) {
                    strnewIp += arrnewIp[i] + ",";
                }
                int inewStatus = ipOldObj.getStatus();
                String strnewStatus = "INACTIVE";
                if (inewStatus == 0) {
                    strnewStatus = "ACTIVE";
                }
                int ialertopStatus = ipOldObj.getAlertstatus();
                String stralertopStatus = "INACTIVE";
                if (ialertopStatus == 0) {
                    stralertopStatus = "ACTIVE";
                }
                int inewAlertOpStatus = ipObj.getAlertstatus();
                String strnewAlertOpStatus = "INACTIVE";
                if (inewAlertOpStatus == 0) {
                    strnewAlertOpStatus = "ACTIVE";
                }

                if (retValue == 0) {
                    resultString = "SUCCESS";

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Edit IP List", resultString, retValue, "Setting Management",
                            "Old Content =" + strOldIp + "Status =" + strStatus + "Old Alert Op Status =" + stralertopStatus,
                            "New Content =" + strnewIp + "Status =" + strnewStatus + "New Alert Op. Status =" + strnewAlertOpStatus,
                            itemtype, channel.getChannelid());
                }

                if (retValue != 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Edit IP List", resultString, retValue, "Setting Management",
                            "Old Content =" + strOldIp + "Status =" + strStatus + "Alert Op. Status =" + stralertopStatus, "Failed To Edit Content..!!",
                            itemtype, channel.getChannelid());
                }

            }

            if (retValue != 0) {
                result = "error";
                //message = "Content Filter Gateway Settings Update Failed!!!";
                message = "Allowed IP List Update Failed!!!";
            }

            try {
                json.put("_result", result);
                json.put("_message", message);

            } finally {
                out.print(json);
                out.close();
            }
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        }
        log.info("Servlet ended");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
