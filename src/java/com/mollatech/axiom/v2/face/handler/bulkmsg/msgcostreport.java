/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.axiom.v2.face.handler.bulkmsg;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.AxiomMessageManagement;
import com.mollatech.axiom.v2.face.handler.operator.getopraudits;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mollatech1
 */
public class msgcostreport extends HttpServlet {

    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(msgcostreport.class.getName());
    
    
    private static int PDF_TYPE = 0;
    private static int CSV_TYPE = 1;
    private static final int BUFSIZE = 4096;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
      try {
            try {
                Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
                log.debug("channel ::" + channel.getName());
                String _startdate = request.getParameter("_startdate");
                log.debug("_startdate ::" + _startdate);
                String _enddate = request.getParameter("_enddate");
                log.debug("_enddate ::"+_enddate);
                String _type = request.getParameter("_type");
                log.debug("_type ::"+_type);
                String _format = request.getParameter("_reporttype");
                log.debug("_format ::"+ _format);
                

                int itype = -9999;
                if (_type != null && !_type.isEmpty()) {
                    itype = Integer.parseInt(_type);
                }
                int iFormat = -9999;
                if (_type != null && !_type.isEmpty()) {
                   iFormat = Integer.parseInt(_format);
                }
                
               
               // int itype = Integer.parseInt(_type);
                DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
                Date startDate = null;
                if (_startdate != null && !_startdate.isEmpty()) {
                    startDate = (Date) formatter.parse(_startdate);
                }
                Date endDate = null;
                if (_enddate != null && !_enddate.isEmpty()) {
                    endDate = (Date) formatter.parse(_enddate);
                }

                if (PDF_TYPE == iFormat) {
                    iFormat = PDF_TYPE;
                } else {
                    iFormat = CSV_TYPE;
                }

                String filepath = null;


                try {
                    try {
                        AxiomMessageManagement cObj = new AxiomMessageManagement();
                       filepath = cObj.generateCostReport(iFormat, channel.getChannelid(),itype,startDate,endDate);
                        cObj = null;
                    } catch (Exception e) {
                        log.error("exception caught :: ",e);
                    }
                    
                  //  
                    File file = new File(filepath);
                    int length = 0;
                    ServletOutputStream outStream = response.getOutputStream();
                    ServletContext context = getServletConfig().getServletContext();
                    String mimetype = context.getMimeType(filepath);

                    // sets response content type
                    if (mimetype == null) {
                        mimetype = "application/octet-stream";
                    }
                    response.setContentType(mimetype);
                    response.setContentLength((int) file.length());
                    String fileName = (new File(filepath)).getName();

                    // sets HTTP header
                    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

                    byte[] byteBuffer = new byte[BUFSIZE];
                    DataInputStream in = new DataInputStream(new FileInputStream(file));

                    // reads the file's bytes and writes them to the response stream
                    while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                        outStream.write(byteBuffer, 0, length);
                    }

                    in.close();
                    outStream.close();
                    file.delete();

                } catch (Exception ex) {
                    // TODO handle custom exceptions here
                    log.error("exception caught :: ",ex);
                }


            } catch (Exception ex) {
                Logger.getLogger(getopraudits.class.getName()).log(Level.SEVERE, null, ex);
                log.error("exception caught :: ",ex);
            }




        } finally {
            //  out.close();
        }
      log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
