/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.RootCertificateSettings;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class loadcasetttings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadcasetttings.class.getName());

    private JSONObject SettingsWhenEmpty(int _type1, int _preference1, Object settingsObj) {
        JSONObject json = new JSONObject();
        log.info("Servlet started");
        if (_type1 == editsettings.RootConfiguration && _preference1 == editsettings.PREFERENCE_ONE) {
            try {
                json.put("_className", "");
                json.put("_ip", "");
                json.put("_port", "");
                json.put("_userId", "");
                json.put("_password", "");
                json.put("_validityDays", "");
                json.put("_keyLength", "");
                json.put("_reserve1", "");
                json.put("_reserve2", "");
                json.put("_reserve3", "");
                json.put("_crllink", "");
                json.put("_validityDays", 30);
                 json.put("_pulledtime", 6);
                json.put("_crlusername", "");
                json.put("_crlpassword", "");
                json.put("ocspurl", "");
                json.put("_tagsList", "");
                json.put("_status", 0);
                json.put("_result", "error");
                json.put("_message", "The Data Not Found");
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
        }
        return json;
    }

    private JSONObject SettingsWhenPresent(Object settingsObj) {
        JSONObject json = new JSONObject();
        if (settingsObj instanceof RootCertificateSettings) {
            RootCertificateSettings certificate = (RootCertificateSettings) settingsObj;
            try {
                json.put("_className", certificate.getClassName());
                json.put("_ip", certificate.getIp());
                json.put("_port", certificate.getPort());
                json.put("_userId", certificate.getUserId());
                json.put("_password", certificate.getPassword());
                json.put("_validityDays", certificate.getValidityDays());
                json.put("_keyLength", certificate.getKeyLength());
                json.put("_reserve1", certificate.getReserve1());
                json.put("_reserve2", certificate.getReserve2());
                json.put("_reserve3", certificate.getReserve3());
                json.put("_crllink", certificate.getCRL());
                json.put("_pulledtime", certificate.getCrlPoolTime());
                json.put("_crlusername", certificate.getCrlUserid());
                json.put("_crlpassword", certificate.getCrlPassword());
                json.put("ocspurl", certificate.getOCSPUrl());
                json.put("_tagsList", certificate.getCAEmailides());
                json.put("_statuskyc", certificate.getKycStatus());
                json.put("_status", certificate.getStatus());
                json.put("_alertsource", certificate.getKycalert());
                json.put("_result", "success");
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }

        }
        return json;
    }

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        String _type = request.getParameter("_type");
        String _preference = request.getParameter("_preference");
        int _type1 = Integer.parseInt(_type);
        int _preference1 = Integer.parseInt(_preference);
        
        log.debug("loadcasetttings::channel is::"+channel.getName());
        log.debug("loadcasetttings::sessionid::"+sessionId);
        log.debug("loadcasetttings::type is::"+_type1);
        log.debug("loadcasetttings::preference is::"+_preference1);

        JSONObject json = null;//new JSONObject();
        PrintWriter out = response.getWriter();

        try {
            SettingsManagement sMngmt = new SettingsManagement();
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _type1, _preference1);
            if (settingsObj != null) {
                json = SettingsWhenPresent(settingsObj);
            } else {
                json = SettingsWhenEmpty(_type1, _preference1, settingsObj);
            }

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
            // TODO handle custom exceptions here
        }
        try {
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
