/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.accessmatrix;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Roles;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author nilesh
 */
public class removeAccessRole extends HttpServlet {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(removeAccessRole.class.getName());

    final String itemTypeOp = "Roles";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel::" + channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId::" + sessionId);
        //audit
        Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operator::" + operator.getOperatorid());
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin::" + remoteaccesslogin);
        // String _old_op_status = request.getParameter("_op_status");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Status Update Successful!!!";

        String _roleId = request.getParameter("_roleId");
        log.debug("_roleId::" + _roleId);
        
        
        
        
        
        

        if (_roleId == null) {
            result = "error";
            message = "Invalid OR Empty Data!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
               log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        }

        int i_roleId = Integer.parseInt(_roleId);
        OperatorsManagement opMgt = new OperatorsManagement();
        int res = opMgt.checkOperatorInRole(sessionId, channel.getChannelid(), i_roleId);
        log.debug("checkOperatorInRole::" + res);
        if (res == 0) {
            result = "error";
            message = "Can't remove role since operstors belongs to role!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        } else {
            Roles roleObj = opMgt.getRoleByRoleId(channel.getChannelid(), i_roleId);
            if (roleObj.getName().equals("sysadmin")) {
                result = "error";
                message = "Can't remove SYSADMIN!!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception ex) {
                    log.error("exception caught :: ",ex);
                }
                out.print(json);
                out.flush();
                return;
            }
            int retValue = opMgt.deleteRole(sessionId, channel.getChannelid(), i_roleId);
            log.debug("deleteRole::" + retValue);

            String resultString = "ERROR";
            AuditManagement audit = new AuditManagement();
            if (retValue == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                        "Remove Role", resultString, retValue, "Role Management",
                        "Role Name=" + roleObj.getName(),
                        "Role " + roleObj.getName() + " Removed",
                        itemTypeOp,
                        roleObj.getRoleid().toString());
                result = "success";
                message = "Role removed successfully!!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception ex) {
                    log.error("exception caught :: ",ex);
                }
                out.print(json);
                out.flush();
                return;
            } else {

                audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                        "Remove Role", resultString, retValue, "Role Management",
                        "Role Name=" + roleObj.getName(),
                        "Failed to remove " + roleObj.getName() + " role",
                        itemTypeOp,
                        roleObj.getRoleid().toString());

                result = "error";
                message = "Failed to remove role!!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception ex) {
                    log.error("exception caught :: ",ex);
                }
                out.print(json);
                out.flush();
                log.info("is ended :: ");
                return;
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
