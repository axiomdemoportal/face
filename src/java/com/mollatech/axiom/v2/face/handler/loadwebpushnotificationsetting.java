/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.PushNotificationSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class loadwebpushnotificationsetting extends HttpServlet {

     static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadwebpushnotificationsetting.class.getName());

       private JSONObject SettingsWhenEmpty(int _type1, int _preference1, Object settingsObj) {
        JSONObject json = new JSONObject();
        if (_type1 == SettingsManagement.WEB_PUSH_NOTIFICATION_SETTING && _preference1 == editsettings.PREFERENCE_THREE) {
            try {
               json.put("_classNameT", "");
                json.put("_ipT", "");
                json.put("_portT", "");
                json.put("_reserve1T", "");
                json.put("_reserve2T", "");
                json.put("_reserve3T", "");
                json.put("_statusT", 0);
                json.put("_logConfirmationT", true);
                json.put("_autofailoverT", 0);
                json.put("_retriesT", 2);
                json.put("_retrydurationT", 10);
                
                json.put("_retriesFromGoogleT", 2);
                json.put("_timetoliveT", 10);
                json.put("_delayWhileIdleT", 0);
                json.put("_gcmurlT", "");
                json.put("_apikeyT", "");
                json.put("_googleSenderKeyT", "");
            } catch (Exception ex) {
                Logger.getLogger(loadpushnotificationsetting.class.getName()).log(Level.SEVERE, null, ex);
            }


        } else if (_type1 == SettingsManagement.IOS_PUSH_NOTIFICATION_SETTING  && _preference1 == editsettings.PREFERENCE_TWO) {
            try {
                json.put("_classNameS", "");
                json.put("_ipS", "");
                json.put("_portS", "");
                json.put("_reserve1S", "");
                json.put("_reserve2S", "");
                json.put("_reserve3S", "");
                json.put("_statusS", 0);
                json.put("_logConfirmationS", true);
                json.put("_retriesS", 2);
                json.put("_retrydurationS", 10);
                json.put("_retriesFromGoogleS", 2);
                json.put("_bundleID", "");
                json.put("_delayWhileIdleS", 0);
                json.put("_certpassowrd", "");
            } catch (Exception ex) {
                Logger.getLogger(loadpushnotificationsetting.class.getName()).log(Level.SEVERE, null, ex);
            }
           
        }
        return json;
    }

    private JSONObject SettingsWhenPresent(Object settingsObj) {
        JSONObject json = new JSONObject();
        if (settingsObj instanceof PushNotificationSettings) {
            PushNotificationSettings pushObj = (PushNotificationSettings) settingsObj;
            if (pushObj.getPreference()== 3) {
                try {
                   json.put("_classNameT", pushObj.getClassName());
                    json.put("_ipT", pushObj.getIp());
                    json.put("_portT",pushObj.getPort());
                    json.put("_reserve1T", pushObj.getReserve1());
                    json.put("_reserve2T", pushObj.getReserve2());
                    json.put("_reserve3T", pushObj.getReserve3());
                    json.put("_statusT", pushObj.getStatus());
                    json.put("_logConfirmationT", true);
                    json.put("_autofailoverT", pushObj.getAutofailover());
                    json.put("_retriesT",pushObj.getRetrycount());
                    json.put("_retrydurationT", pushObj.getRetryduration());
                    json.put("_retriesFromGoogleT", pushObj.getRetriesFromGoogle());
                    json.put("_timetoliveT", pushObj.getTimeToLive());
                    if(pushObj.isDelayWhileIdle()== true){
                        json.put("_delayWhileIdleT", 1);
                    }else{
                        json.put("_delayWhileIdleT", 0);
                    }
                    json.put("_gcmurlT", pushObj.getGcmUrl());
                    json.put("_apikeyT", pushObj.getApiKey());
                    json.put("_googleSenderKeyT", pushObj.getGoogleSenderId());
                } catch (Exception ex) {
                    Logger.getLogger(loadwebpushnotificationsetting.class.getName()).log(Level.SEVERE, null, ex);
                }
//            } else {
//                try {
//                    json.put("_classNameS", pushObj.getClassNameT());
//                    json.put("_ipS", pushObj.getIp());
//                    json.put("_portS", pushObj.getPort());
//                    json.put("_reserve1S", pushObj.getReserve1());
//                    json.put("_reserve2S", pushObj.getReserve2());
//                    json.put("_reserve3S", pushObj.getReserve3());
//                    json.put("_statusS", pushObj.getStatus());
//                    json.put("_logConfirmationS", true);
//                    json.put("_retriesS",pushObj.getRetrycount());
//                    json.put("_retrydurationS", pushObj.getRetryduration());
//                    if(pushObj.isApplicationType() == true){
//                        json.put("_delayWhileIdleS", 1);
//                    }else{
//                        json.put("_delayWhileIdleS",0);
//                    }
//                    json.put("_bundleID",  pushObj.getBundleId());
//                    json.put("_certpassowrd", pushObj.getCertPassword());
//                    
//                    if(pushObj.getCertificatePath() != null){
//                        json.put("uploadStatus", "File Already Uploaded!!!");
//                    }   } catch (Exception ex) {
//                    Logger.getLogger(loadpushnotificationsetting.class.getName()).log(Level.SEVERE, null, ex);
//                }
            }
        }
        return json;
    }

  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
          response.setContentType("application/json");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        String _type = request.getParameter("_type");
        String _preference = request.getParameter("_preference");
        int _type1 = Integer.parseInt(_type);
        int _preference1 = Integer.parseInt(_preference);
        
        log.debug("loadpushnotificationsetting::channel is::"+channel.getName());
        log.debug("loadpushnotificationsetting::sessionid::"+sessionId);
        log.debug("loadpushnotificationsetting::type is::"+_type1);
        log.debug("loadpushnotificationsetting::preference is::"+_preference1);

        JSONObject json = null;//new JSONObject();
        PrintWriter out = response.getWriter();

        try {
            SettingsManagement sMngmt = new SettingsManagement();
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _type1, _preference1);
            if (settingsObj != null) {
                json = SettingsWhenPresent(settingsObj);
            } else {
                json = SettingsWhenEmpty(_type1, _preference1, settingsObj);
            }

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
            // TODO handle custom exceptions here
        }
        try {
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
        }
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
