/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.pkitokens;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Manoj Sherkhane
 */
public class checkocspurl extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(checkocspurl.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        
        log.info("is started :: ");

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String ocspUrl = request.getParameter("ocspurl");
        log.debug("ocspUrl :: "+ocspUrl);
        String result = "success";
        String message = "Connection sucessful!!!";

        int retValue = -1;

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        if (ocspUrl == null) {
            try {
                result = "error";
                message = "Please Enter URL";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            } catch (Exception ex) {
                Logger.getLogger(checkocspurl.class.getName()).log(Level.SEVERE, null, ex);
                log.error("exception caught :: ",ex);
            }

        }

        AXIOMStatus aStatus = new AXIOMStatus();
        try {
            CertificateManagement cManagement = new CertificateManagement();
            aStatus = cManagement.GetOCSPResponse(sessionId, channel.getChannelid());
        } catch (Exception ex) {
            log.error("exception caught :: ",ex);
        }

        if(aStatus == null){
            try {
                result = "error";
                message = "Invalid OCSP URL";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            } catch (Exception ex) {
                Logger.getLogger(checkocspurl.class.getName()).log(Level.SEVERE, null, ex);
                log.error("exception caught :: ",ex);
            }

        }else if (aStatus.iStatus == 0) {
            result = "success";
            message = aStatus.strStatus;
        } else {
            result = "error";
            message =  aStatus.strStatus;
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception ex) {
            log.error("exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
