/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.pkitokens;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.connector.user.OrganizationDetails;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.security.cert.X509Certificate;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class loadUserDetails extends HttpServlet {
    
     static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadUserDetails.class.getName());

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String _user = request.getParameter("_uid");
        log.debug("_user :: "+_user);
        try {
            AuthUser user = null;
            UserManagement uObj = new UserManagement();
            user = uObj.getUser(sessionId, channel.getChannelid(), _user);
            OrganizationDetails orgObj = uObj.GetOrganizationDetails(sessionId, channel.getChannelid(), _user);
            if (user == null && orgObj == null) {
                json.put("_result", "error");
                json.put("_message", "User Not Found");
                out.print(json);
                out.flush();
                return;
            }
            
            
            CertificateManagement cManagement = new CertificateManagement();
            String certificate = cManagement.GenerateCertificate(sessionId, channel.getChannelid(), _user);
            if (certificate != null) {
                byte[] certBytes = Base64.decode(certificate);
                 //byte[] certBytes = Base64.decode(usercertObjFilled.cert);
                    javax.security.cert.X509Certificate usercertObj1 = javax.security.cert.X509Certificate.getInstance(certBytes);
                        //usercertObjFilled.validtill = usercertObj1.getNotAfter().toString();
                    //usercertObjFilled.validfrom = usercertObj1.getNotBefore().toString(); 
            }
            
            
            
            json.put("_uid", _user);
            json.put("_Name", user.getUserName());
            json.put("_Email", user.getEmail());
            json.put("_Phone", user.getPhoneNo());
            json.put("_city", orgObj.City);
            json.put("_Country", orgObj.Country);
            json.put("_Organisation", orgObj.Organisation);
            json.put("_OrganisationalUnit", orgObj.OrganisationalUnit);
            json.put("_State", orgObj.State);
             json.put("_pincode", orgObj.pincode);
            json.put("_result", "success");

        } catch (Exception ex) {
            log.error("exception caught :: ",ex);
            // TODO handle custom exceptions here
            try { json.put("_result", "error");
            json.put("_message", ex.getMessage());
            }catch(Exception e){
                log.error("exception caught :: ",e);
            }
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
