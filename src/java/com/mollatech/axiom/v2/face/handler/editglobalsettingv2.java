/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
import com.mollatech.axiom.nucleus.settings.emailsetting;
import com.mollatech.axiom.nucleus.settings.faxlimitsetting;
import com.mollatech.axiom.nucleus.settings.pushsetting;
import com.mollatech.axiom.nucleus.settings.smssetting;
import com.mollatech.axiom.nucleus.settings.ussdsetting;
import com.mollatech.axiom.nucleus.settings.voicesetting;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author mollatech1
 */
public class editglobalsettingv2 extends HttpServlet {
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editglobalsettingv2.class.getName());

    final String itemtype = "SETTINGS";
    final int CONTENT_FILTER = 1;
    final int IP_FILTER = 2;
    final int PREFIX_FILTER = 3;
    final int SMS = 4;
    final int VOICE = 5;
    final int USSD = 6;
    final int EMAIL = 7;
    final int FAX = 8;
    final int PUSH = 9;
    final int USER_ALERT = 10;
    final int DOMAIN_FILTER = 11;
    final int MSG_TYPE = 12;
    final int CR_SETTINGS = 13;

    //duration
//        final int WEEK = 1;
    final int MONTH = 1;
    final int THREE_MONTH = 2;
    final int SIX_MONTH = 3;

    //Repeat status
    final int REPEAT_ACTIVE = 1;
    final int REPEAT_in_ACTIVE = 1;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        String OperatorID = operatorS.getOperatorid();
        String _channelId = channel.getChannelid();
        SettingsManagement sMngmt = new SettingsManagement();
        int retValue = -1;
        String result = "success";
        String message = "Updated Successfully!!!";
        JSONObject json = new JSONObject();
        int _iPreference = 1;//Integer.parseInt(_preference);
        int _itype = SettingsManagement.GlobalSettings;//Integer.parseInt(_type);
        String strType = String.valueOf(_itype);
      
            log.debug("channel ::" + channel.getName());
            log.debug("getOperatorid ::" + operatorS.getOperatorid());
            log.debug("sessionId ::" + sessionId);
            log.debug("remoteaccesslogin ::" + remoteaccesslogin);
            log.debug("getChannelid ::" + channel.getChannelid());
        String _settype = request.getParameter("_settype");
        int _isettype = Integer.parseInt(_settype);
        log.debug("_isettype :: "+_isettype);
        Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _itype, _iPreference);
        
        GlobalChannelSettings globalObj = null;
        boolean bAddSetting = false;

        if (settingsObj == null) {
            globalObj = new GlobalChannelSettings();
            globalObj.channelId = _channelId;
            bAddSetting = true;
        } else {
            globalObj = (GlobalChannelSettings) settingsObj;
        }

//         if (_isettype == USER_ALERT) {
//            //content filter
////            String _userAlertStatus = request.getParameter("_userAlertStatus");
////            int _iuserAlertStatus = Integer.parseInt(_userAlertStatus);
//             int _iuserAlertStatus =  0;
//            globalObj.alertuser = _iuserAlertStatus;
//            
//        }
        if (_isettype == CONTENT_FILTER) {
            //content filter
            String contentalertstatus = request.getParameter("_alertOperator");
            log.debug("contentalertstatus :: "+contentalertstatus);
            int _icontentalertstatus = Integer.parseInt(contentalertstatus);
            String contentstatus = request.getParameter("_statusContent");
            log.debug("contentstatus :: "+contentstatus);
            int _icontentstatus = Integer.parseInt(contentstatus);
            String _content = request.getParameter("_keywords");
            log.debug("_content :: "+_content);
            String[] _contentsArray = _content.split(",");

            globalObj.content = _contentsArray;
            globalObj.contentstatus = _icontentstatus;
            globalObj.contentalertstatus = _icontentalertstatus;
        }
        if (_isettype == IP_FILTER) {
            //ipfilter
            String ipstatus = request.getParameter("_statusIP");
            log.debug("ipstatus :: "+ipstatus);
            int _ipstatus = Integer.parseInt(ipstatus);
            String ipalertstatus = request.getParameter("_alertipOperator");
            log.debug("ipalertstatus :: "+ipalertstatus);
            int _ipalertstatus = Integer.parseInt(ipalertstatus);
            String _ip = request.getParameter("_ip");
            log.debug("_ip :: "+_ip);
            String[] _ipArray = _ip.split(",");

            globalObj.IP = _ipArray;
            globalObj.ipstatus = _ipstatus;
            globalObj.ipalertstatus = _ipalertstatus;
        }

        if (_isettype == PREFIX_FILTER) {
            //prefix filter  
            String prefixstatus = request.getParameter("_statusPrefix");
            log.debug("prefixstatus :: "+prefixstatus);
            int _prefixstatus = Integer.parseInt(prefixstatus);
            String prefixalertstatus = request.getParameter("_alertPrefixOperator");
            log.debug("prefixalertstatus :: "+prefixalertstatus);
            int _prefixalertstatus = Integer.parseInt(prefixalertstatus);
            String _prefix = request.getParameter("_prefix");
            log.debug("_prefix :: "+_prefix);
            String[] _prefixArray = _prefix.split(",");

            globalObj.prefix = _prefixArray;
            globalObj.prefixstatus = _prefixstatus;
            globalObj.prefixalertstatus = _prefixalertstatus;

        }
        if (_isettype == CR_SETTINGS) {
            //prefix filter  
            String strPercentage = request.getParameter("_weigtage");
            log.debug("strPercentage :: "+strPercentage);
            String _answersattempts = request.getParameter("_answersattempts");
            log.debug("_answersattempts :: "+_answersattempts);
            if(strPercentage != null){
                int istrPercentage = Integer.parseInt(strPercentage);
                globalObj.weightageForCR = istrPercentage;
             }
            if(_answersattempts != null){
                 int ianswersattempts = Integer.parseInt(_answersattempts);
                 globalObj.answersattempts = ianswersattempts;
            }
     }

        if (_isettype == DOMAIN_FILTER) {
            //prefix filter  
            String domainstatus = request.getParameter("_statusDomain");
            log.debug("domainstatus :: "+domainstatus);
            int _domainstatus = Integer.parseInt(domainstatus);
            String domainalertstatus = request.getParameter("_alertDomainOperator");
            log.debug("domainalertstatus :: "+domainalertstatus);
            int _domainalertstatus = Integer.parseInt(domainalertstatus);
            String _domain = request.getParameter("_domain");
            log.debug("_domain :: "+_domain);
            String[] _domainArray = _domain.split(",");

            globalObj.domain = _domainArray;
            globalObj.domainstatus = _domainstatus;
            globalObj.domainalertstatus = _domainalertstatus;

        }
        Date startdate = channel.getCreatedOn();
        Calendar c = Calendar.getInstance();
        if (_isettype == SMS) {
            String _smslimit = request.getParameter("_smslimit");
            log.debug("_smslimit :: "+_smslimit);
            String _smscost = request.getParameter("_smscost");
            log.debug("_smscost :: "+_smscost);
            String _smsStatus = request.getParameter("_smsStatus");
            log.debug("_smsStatus :: "+_smsStatus);
            String _smsAlertStatus = request.getParameter("_smsAlertStatus");
            log.debug("_smsAlertStatus :: "+_smsAlertStatus);
            String _smsRepeatStatus = request.getParameter("_smsRepeatStatus");
            log.debug("_smsRepeatStatus :: "+_smsRepeatStatus);
            String _smsThresholdlimit = request.getParameter("_smsThresholdlimit");
            log.debug("_smsThresholdlimit :: "+_smsThresholdlimit);
            String _fastPaceSMS = request.getParameter("_fastPaceSMS");
            log.debug("_fastPaceSMS :: "+_fastPaceSMS);
            String _hyperPaceSMS = request.getParameter("_hyperPaceSMS");
            log.debug("_hyperPaceSMS :: "+_hyperPaceSMS);
            String _normalPaceSMS = request.getParameter("_normalPaceSMS");
            log.debug("_normalPaceSMS :: "+_normalPaceSMS);
            String _slowPaceSMS = request.getParameter("_slowPaceSMS");
            log.debug("_slowPaceSMS :: "+_slowPaceSMS);
            //date time restriction
            String _dayRestrictionSMS = request.getParameter("_dayRestrictionSMS");
            log.debug("_dayRestrictionSMS :: "+_dayRestrictionSMS);
            String _timeFromInHourSMS = request.getParameter("_timeFromRestrictionSMS");
            log.debug("_timeFromInHourSMS :: "+_timeFromInHourSMS);
            String _timeToInHourSMS = request.getParameter("_timeToRestrictionSMS");
            log.debug("_timeToInHourSMS :: "+_timeToInHourSMS);
            //Footer Status
            String _smsFooter = request.getParameter("_smsFooter");
            log.debug("_smsFooter :: "+_smsFooter);
            String _smsFooterStatus = request.getParameter("_smsFooterStatus");
            log.debug("_smsFooterStatus :: "+_smsFooterStatus);
            globalObj.smssettingobj = new smssetting();

            if (_smsFooterStatus != null) {
                globalObj.smssettingobj._smsFooterStatus = Integer.parseInt(_smsFooterStatus);
            }

            if (_smsRepeatStatus != null) {
                c.setTime(startdate);
                if (Integer.parseInt(_smsRepeatStatus) == MONTH) {
                    c.add(Calendar.DATE, 30);
                    startdate = c.getTime();
                    globalObj.smssettingobj.smsenddate = startdate;
                    globalObj.smssettingobj.repeat = Integer.parseInt(_smsRepeatStatus);
                } else if (Integer.parseInt(_smsRepeatStatus) == THREE_MONTH) {
                    c.add(Calendar.DATE, 90);
                    startdate = c.getTime();
                    globalObj.smssettingobj.smsenddate = startdate;
                    globalObj.smssettingobj.repeat = Integer.parseInt(_smsRepeatStatus);
                } else if (Integer.parseInt(_smsRepeatStatus) == SIX_MONTH) {
                    c.add(Calendar.DATE, 180);
                    startdate = c.getTime();
                    globalObj.smssettingobj.smsenddate = startdate;
                    globalObj.smssettingobj.repeat = Integer.parseInt(_smsRepeatStatus);
                }
                if (globalObj.smssettingobj.smsenddate.before(new Date())) {
                    c.setTime(new Date());
                    if (Integer.parseInt(_smsRepeatStatus) == MONTH) {
                        c.add(Calendar.DATE, 30);
                        startdate = c.getTime();
                        globalObj.smssettingobj.smsenddate = startdate;
                        globalObj.smssettingobj.repeat = Integer.parseInt(_smsRepeatStatus);
                    } else if (Integer.parseInt(_smsRepeatStatus) == THREE_MONTH) {
                        c.add(Calendar.DATE, 90);
                        startdate = c.getTime();
                        globalObj.smssettingobj.smsenddate = startdate;
                        globalObj.smssettingobj.repeat = Integer.parseInt(_smsRepeatStatus);
                    } else if (Integer.parseInt(_smsRepeatStatus) == SIX_MONTH) {
                        c.add(Calendar.DATE, 180);
                        startdate = c.getTime();
                        globalObj.smssettingobj.smsenddate = startdate;
                        globalObj.smssettingobj.repeat = Integer.parseInt(_smsRepeatStatus);
                    }

                }

            }

            if (_smsAlertStatus != null) {
                globalObj.smssettingobj.smsalertstatus = Integer.parseInt(_smsAlertStatus);
            }
            if (_smscost != null) {
                globalObj.smssettingobj.smscost = Float.parseFloat(_smscost);
            }
            if (_smsStatus != null) {
                globalObj.smssettingobj.smslimitstatus = Integer.parseInt(_smsStatus);
            }
            if (_smsThresholdlimit != null) {
                globalObj.smssettingobj.thresholdlimit = Integer.parseInt(_smsThresholdlimit);
            }
            if (_smsAlertStatus != null) {
                globalObj.smssettingobj.smslimit = Integer.parseInt(_smslimit);
            }
            if (_fastPaceSMS != null) {
                globalObj.smssettingobj._fastPaceSMS = Integer.parseInt(_fastPaceSMS);
            }
            if (_hyperPaceSMS != null) {
                globalObj.smssettingobj._hyperPaceSMS = Integer.parseInt(_hyperPaceSMS);
            }

            if (_normalPaceSMS != null) {
                globalObj.smssettingobj._normalPaceSMS = Integer.parseInt(_normalPaceSMS);
            }
            if (_slowPaceSMS != null) {
                globalObj.smssettingobj._slowPaceSMS = Integer.parseInt(_slowPaceSMS);
            }
            globalObj.smssettingobj._smsFooter = _smsFooter;

            //date & time restriction
            if (_dayRestrictionSMS != null) {
                globalObj.smssettingobj._dayRestrictionSMS = Integer.parseInt(_dayRestrictionSMS);
            }
            if (_dayRestrictionSMS != null) {
                globalObj.smssettingobj._timeFromRestrictionSMS = Integer.parseInt(_timeFromInHourSMS);
            }

            if (_dayRestrictionSMS != null) {
                globalObj.smssettingobj._timeToRestrictionSMS = Integer.parseInt(_timeToInHourSMS);
            }

        }

        if (_isettype == VOICE) {
            String _voicelimit = request.getParameter("_voicelimit");
            log.debug("_voicelimit :: "+_voicelimit);
            String _voicecost = request.getParameter("_voicecost");
            log.debug("_voicecost :: "+_voicecost);
            String _voiceStatus = request.getParameter("_voiceStatus");
            log.debug("_voiceStatus :: "+_voiceStatus);
            String _voiceAlertStatus = request.getParameter("_voiceAlertStatus");
            log.debug("_voiceAlertStatus :: "+_voiceAlertStatus);
            String _voiceRepeatStatus = request.getParameter("_voiceRepeatStatus");
            log.debug("_voiceRepeatStatus :: "+_voiceRepeatStatus);
//            String _voiceRepeatDuration = request.getParameter("_voiceRepeatDuration");
            String _voiceThresholdlimit = request.getParameter("_voiceThresholdlimit");
            log.debug("_voiceThresholdlimit :: "+_voiceThresholdlimit);
            String _fastPaceVOICE = request.getParameter("_fastPaceVOICE");
            log.debug("_fastPaceVOICE :: "+_fastPaceVOICE);
            String _hyperPaceVOICE = request.getParameter("_hyperPaceVOICE");
            log.debug("_hyperPaceVOICE :: "+_hyperPaceVOICE);
            String _normalPaceVOICE = request.getParameter("_normalPaceVOICE");
            log.debug("_normalPaceVOICE :: "+_normalPaceVOICE);
            String _slowPaceVOICE = request.getParameter("_slowPaceVOICE");
            log.debug("_slowPaceVOICE :: "+_slowPaceVOICE);
            String _voiceFooter = request.getParameter("_voiceFooter");
            log.debug("_voiceFooter :: "+_voiceFooter);
            //date time restriction
            String _dayRestrictionVOICE = request.getParameter("_dayRestrictionVOICE");
            log.debug("_dayRestrictionVOICE :: "+_dayRestrictionVOICE);
            String _timeFromInHourVOICE = request.getParameter("_timeFromRestrictionVOICE");
            log.debug("_timeFromInHourVOICE :: "+_timeFromInHourVOICE);
            String _timeToInHourVOICE = request.getParameter("_timeToRestrictionVOICE");
            log.debug("_timeToInHourVOICE :: "+_timeToInHourVOICE);
            String _voiceFooterStatus = request.getParameter("_voiceFooterStatus");
            log.debug("_voiceFooterStatus :: "+_voiceFooterStatus);
            globalObj.voicesettingobj = new voicesetting();

            if (_voiceFooterStatus != null) {
                globalObj.voicesettingobj._voiceFooterStatus = Integer.parseInt(_voiceFooterStatus);
            }

            if (_voiceRepeatStatus != null) {
                c.setTime(startdate);
                if (Integer.parseInt(_voiceRepeatStatus) == MONTH) {
                    c.add(Calendar.DATE, 30);
                    startdate = c.getTime();
                    globalObj.voicesettingobj.voiceenddate = startdate;
                    globalObj.voicesettingobj.repeat = Integer.parseInt(_voiceRepeatStatus);
                } else if (Integer.parseInt(_voiceRepeatStatus) == THREE_MONTH) {
                    c.add(Calendar.DATE, 90);
                    startdate = c.getTime();
                    globalObj.voicesettingobj.voiceenddate = startdate;
                    globalObj.voicesettingobj.repeat = Integer.parseInt(_voiceRepeatStatus);
                } else if (Integer.parseInt(_voiceRepeatStatus) == SIX_MONTH) {
                    c.add(Calendar.DATE, 180);
                    startdate = c.getTime();
                    globalObj.voicesettingobj.voiceenddate = startdate;
                    globalObj.voicesettingobj.repeat = Integer.parseInt(_voiceRepeatStatus);
                }

                if (globalObj.voicesettingobj.voiceenddate.before(new Date())) {
                    c.setTime(new Date());
                    if (Integer.parseInt(_voiceRepeatStatus) == MONTH) {
                        c.add(Calendar.DATE, 30);
                        startdate = c.getTime();
                        globalObj.voicesettingobj.voiceenddate = startdate;
                        globalObj.voicesettingobj.repeat = Integer.parseInt(_voiceRepeatStatus);
                    } else if (Integer.parseInt(_voiceRepeatStatus) == THREE_MONTH) {
                        c.add(Calendar.DATE, 90);
                        startdate = c.getTime();
                        globalObj.voicesettingobj.voiceenddate = startdate;
                        globalObj.voicesettingobj.repeat = Integer.parseInt(_voiceRepeatStatus);
                    } else if (Integer.parseInt(_voiceRepeatStatus) == SIX_MONTH) {
                        c.add(Calendar.DATE, 180);
                        startdate = c.getTime();
                        globalObj.voicesettingobj.voiceenddate = startdate;
                        globalObj.voicesettingobj.repeat = Integer.parseInt(_voiceRepeatStatus);
                    }

                }

            }
            if (_voiceAlertStatus != null) {
                globalObj.voicesettingobj.voicemsgalertstatus = Integer.parseInt(_voiceAlertStatus);
            }
            if (_voicecost != null) {
                globalObj.voicesettingobj.voicemsgcost = Float.parseFloat(_voicecost);
            }
            if (_voiceStatus != null) {
                globalObj.voicesettingobj.voicemsglimitstatus = Integer.parseInt(_voiceStatus);
            }
            if (_voiceThresholdlimit != null) {
                globalObj.voicesettingobj.thresholdlimit = Integer.parseInt(_voiceThresholdlimit);
            }
            if (_voicelimit != null) {
                globalObj.voicesettingobj.voicemsglimit = Integer.parseInt(_voicelimit);
            }
            if (_fastPaceVOICE != null) {
                globalObj.voicesettingobj._fastPaceVOICE = Integer.parseInt(_fastPaceVOICE);
            }
            if (_hyperPaceVOICE != null) {
                globalObj.voicesettingobj._hyperPaceVOICE = Integer.parseInt(_hyperPaceVOICE);
            }

            if (_normalPaceVOICE != null) {
                globalObj.voicesettingobj._normalPaceVOICE = Integer.parseInt(_normalPaceVOICE);
            }
            if (_slowPaceVOICE != null) {
                globalObj.voicesettingobj._slowPaceVOICE = Integer.parseInt(_slowPaceVOICE);
            }

            globalObj.voicesettingobj._voiceFooter = _voiceFooter;

              //date time restriction
            if (_slowPaceVOICE != null) {
                globalObj.voicesettingobj._slowPaceVOICE = Integer.parseInt(_slowPaceVOICE);
            }

            //date & time restriction
            if (_dayRestrictionVOICE != null) {
                globalObj.voicesettingobj._dayRestrictionVOICE = Integer.parseInt(_dayRestrictionVOICE);
            }
            if (_dayRestrictionVOICE != null) {
                globalObj.voicesettingobj._timeFromRestrictionVOICE = Integer.parseInt(_timeFromInHourVOICE);
            }

            if (_dayRestrictionVOICE != null) {
                globalObj.voicesettingobj._timeToRestrictionVOICE = Integer.parseInt(_timeToInHourVOICE);
            }

        }
        if (_isettype == USSD) {
            String _ussdlimit = request.getParameter("_ussdlimit");
            log.debug("_ussdlimit :: "+_ussdlimit);
            String _ussdcost = request.getParameter("_ussdcost");
            log.debug("_ussdcost :: "+_ussdcost);
            String _ussdStatus = request.getParameter("_ussdStatus");
            log.debug("_ussdStatus :: "+_ussdStatus);
            String _ussdAlertStatus = request.getParameter("_ussdAlertStatus");
            log.debug("_ussdAlertStatus :: "+_ussdAlertStatus);
            String _ussdRepeatStatus = request.getParameter("_ussdRepeatStatus");
            log.debug("_ussdRepeatStatus :: "+_ussdRepeatStatus);
            String _ussdFooter = request.getParameter("_ussdFooter");
            log.debug("_ussdFooter :: "+_ussdFooter);
            String _ussdThresholdlimit = request.getParameter("_ussdThresholdlimit");
            log.debug("_ussdThresholdlimit :: "+_ussdThresholdlimit);
            String _fastPaceUSSD = request.getParameter("_fastPaceUSSD");
            log.debug("_fastPaceUSSD :: "+_fastPaceUSSD);
            String _hyperPaceUSSD = request.getParameter("_hyperPaceUSSD");
            log.debug("_hyperPaceUSSD :: "+_hyperPaceUSSD);
            String _normalPaceUSSD = request.getParameter("_normalPaceUSSD");
            log.debug("_normalPaceUSSD :: "+_normalPaceUSSD);
            String _slowPaceUSSD = request.getParameter("_slowPaceUSSD");
            log.debug("_slowPaceUSSD :: "+_slowPaceUSSD);

            //date time restriction
            String _dayRestrictionUSSD = request.getParameter("_dayRestrictionUSSD");
            log.debug("_dayRestrictionUSSD :: "+_dayRestrictionUSSD);
            String _timeFromInHourUSSD = request.getParameter("_timeFromRestrictionUSSD");
            log.debug("_timeFromInHourUSSD :: "+_timeFromInHourUSSD);
            String _timeToInHourUSSD = request.getParameter("_timeToRestrictionUSSD");
            log.debug("_timeToInHourUSSD :: "+_timeToInHourUSSD);
            String _ussdFooterStatus = request.getParameter("_ussdFooterStatus");
            log.debug("_ussdFooterStatus :: "+_ussdFooterStatus);
            globalObj.ussdsettingobj = new ussdsetting();

            if (_ussdFooterStatus != null) {
                globalObj.ussdsettingobj._ussdFooterStatus = Integer.parseInt(_ussdFooterStatus);
            }

            //date & time restriction
            if (_dayRestrictionUSSD != null) {
                globalObj.ussdsettingobj._dayRestrictionUSSD = Integer.parseInt(_dayRestrictionUSSD);
            }
            if (_timeFromInHourUSSD != null) {
                globalObj.ussdsettingobj._timeFromRestrictionUSSD = Integer.parseInt(_timeFromInHourUSSD);
            }

            if (_timeToInHourUSSD != null) {
                globalObj.ussdsettingobj._timeToRestrictionUSSD = Integer.parseInt(_timeToInHourUSSD);
            }

            if (_ussdRepeatStatus != null) {

                c.setTime(startdate);
                if (Integer.parseInt(_ussdRepeatStatus) == MONTH) {
                    c.add(Calendar.DATE, 30);
                    startdate = c.getTime();
                    globalObj.ussdsettingobj.ussdenddate = startdate;
                    globalObj.ussdsettingobj.repeat = Integer.parseInt(_ussdRepeatStatus);
                } else if (Integer.parseInt(_ussdRepeatStatus) == THREE_MONTH) {
                    c.add(Calendar.DATE, 90);
                    startdate = c.getTime();
                    globalObj.ussdsettingobj.ussdenddate = startdate;
                    globalObj.ussdsettingobj.repeat = Integer.parseInt(_ussdRepeatStatus);
                } else if (Integer.parseInt(_ussdRepeatStatus) == SIX_MONTH) {
                    c.add(Calendar.DATE, 180);
                    startdate = c.getTime();
                    globalObj.ussdsettingobj.ussdenddate = startdate;
                    globalObj.ussdsettingobj.repeat = Integer.parseInt(_ussdRepeatStatus);
                }

                if (globalObj.ussdsettingobj.ussdenddate.before(new Date())) {
                    c.setTime(new Date());
                    if (Integer.parseInt(_ussdRepeatStatus) == MONTH) {
                        c.add(Calendar.DATE, 30);
                        startdate = c.getTime();
                        globalObj.ussdsettingobj.ussdenddate = startdate;
                        globalObj.ussdsettingobj.repeat = Integer.parseInt(_ussdRepeatStatus);
                    } else if (Integer.parseInt(_ussdRepeatStatus) == THREE_MONTH) {
                        c.add(Calendar.DATE, 90);
                        startdate = c.getTime();
                        globalObj.ussdsettingobj.ussdenddate = startdate;
                        globalObj.ussdsettingobj.repeat = Integer.parseInt(_ussdRepeatStatus);
                    } else if (Integer.parseInt(_ussdRepeatStatus) == SIX_MONTH) {
                        c.add(Calendar.DATE, 180);
                        startdate = c.getTime();
                        globalObj.ussdsettingobj.ussdenddate = startdate;
                        globalObj.ussdsettingobj.repeat = Integer.parseInt(_ussdRepeatStatus);
                    }

                }

            }
            if (_ussdAlertStatus != null) {
                globalObj.ussdsettingobj.ussdalertstatus = Integer.parseInt(_ussdAlertStatus);
            }
            if (_ussdcost != null) {
                globalObj.ussdsettingobj.ussdcost = Float.parseFloat(_ussdcost);
            }
            if (_ussdStatus != null) {
                globalObj.ussdsettingobj.ussdlimitstatus = Integer.parseInt(_ussdStatus);
            }
            if (_ussdThresholdlimit != null) {
                globalObj.ussdsettingobj.thresholdlimit = Integer.parseInt(_ussdThresholdlimit);
            }
            if (_ussdlimit != null) {
                globalObj.ussdsettingobj.ussdlimit = Integer.parseInt(_ussdlimit);
            }
            if (_fastPaceUSSD != null) {
                globalObj.ussdsettingobj._fastPaceUSSD = Integer.parseInt(_fastPaceUSSD);
            }
            if (_hyperPaceUSSD != null) {
                globalObj.ussdsettingobj._hyperPaceUSSD = Integer.parseInt(_hyperPaceUSSD);
            }

            if (_normalPaceUSSD != null) {
                globalObj.ussdsettingobj._normalPaceUSSD = Integer.parseInt(_normalPaceUSSD);
            }
            if (_slowPaceUSSD != null) {
                globalObj.ussdsettingobj._slowPaceUSSD = Integer.parseInt(_slowPaceUSSD);
            }

            globalObj.ussdsettingobj._ussdFooter = _ussdFooter;

        }

        if (_isettype == EMAIL) {
            String _emaillimit = request.getParameter("_emaillimit");
            log.debug("_emaillimit :: "+_emaillimit);
            String _emailcost = request.getParameter("_emailcost");
            log.debug("_emailcost :: "+_emailcost);
            String _emailStatus = request.getParameter("_emailStatus");
            log.debug("_emailStatus :: "+_emailStatus);
            String _emailAlertStatus = request.getParameter("_emailAlertStatus");
            log.debug("_emailAlertStatus :: "+_emailAlertStatus);
            String _emailRepeatStatus = request.getParameter("_emailRepeatStatus");
            log.debug("_emailRepeatStatus :: "+_emailRepeatStatus);
            String _emailFooter = request.getParameter("_emailFooter");
            log.debug("_emailFooter :: "+_emailFooter);
            String _emailThresholdlimit = request.getParameter("_emailThresholdlimit");
            log.debug("_emailThresholdlimit :: "+_emailThresholdlimit);
            String _fastPaceEMAIL = request.getParameter("_fastPaceEMAIL");
            log.debug("_fastPaceEMAIL :: "+_fastPaceEMAIL);
            String _hyperPaceEMAIL = request.getParameter("_hyperPaceEMAIL");
            log.debug("_hyperPaceEMAIL :: "+_hyperPaceEMAIL);
            String _normalPaceEMAIL = request.getParameter("_normalPaceEMAIL");
            log.debug("_normalPaceEMAIL :: "+_normalPaceEMAIL);
            String _slowPaceEMAIL = request.getParameter("_slowPaceEMAIL");
            //date time restriction
            log.debug("_slowPaceEMAIL :: "+_slowPaceEMAIL);
            String _dayRestrictionEMAIL = request.getParameter("_dayRestrictionEMAIL");
            log.debug("_dayRestrictionEMAIL :: "+_dayRestrictionEMAIL);
            String _timeFromInHourEMAIL = request.getParameter("_timeFromRestrictionEMAIL");
            log.debug("_timeFromInHourEMAIL :: "+_timeFromInHourEMAIL);
            String _timeToInHourEMAIL = request.getParameter("_timeToRestrictionEMAIL");
            log.debug("_timeToInHourEMAIL :: "+_timeToInHourEMAIL);
            String _emailFooterStatus = request.getParameter("_emailFooterStatus");
            log.debug("_emailFooterStatus :: "+_emailFooterStatus);
//           String  _templatebody = request.getParameter("emailCotents");
//               System.out.println(_templatebody);
        
             
            globalObj.emailsettingobj = new emailsetting();
             
            if (_emailFooterStatus != null) {
                globalObj.emailsettingobj._emailFooterStatus = Integer.parseInt(_emailFooterStatus);
            }

            //date & time restriction
            if (_dayRestrictionEMAIL != null) {
                globalObj.emailsettingobj._dayRestrictionEMAIL = Integer.parseInt(_dayRestrictionEMAIL);
            }
            if (_timeFromInHourEMAIL != null) {
                globalObj.emailsettingobj._timeFromRestrictionEMAIL = Integer.parseInt(_timeFromInHourEMAIL);
            }

            if (_timeToInHourEMAIL != null) {
                globalObj.emailsettingobj._timeToRestrictionEMAIL = Integer.parseInt(_timeToInHourEMAIL);
            }

            if (_emailRepeatStatus != null) {

                c.setTime(startdate);
                if (Integer.parseInt(_emailRepeatStatus) == MONTH) {
                    c.add(Calendar.DATE, 30);
                    startdate = c.getTime();
                    globalObj.emailsettingobj.emailenddate = startdate;
                    globalObj.emailsettingobj.repeat = Integer.parseInt(_emailRepeatStatus);
                } else if (Integer.parseInt(_emailRepeatStatus) == THREE_MONTH) {
                    c.add(Calendar.DATE, 90);
                    startdate = c.getTime();
                    globalObj.emailsettingobj.emailenddate = startdate;
                    globalObj.emailsettingobj.repeat = Integer.parseInt(_emailRepeatStatus);
                } else if (Integer.parseInt(_emailRepeatStatus) == SIX_MONTH) {
                    c.add(Calendar.DATE, 180);
                    startdate = c.getTime();
                    globalObj.emailsettingobj.emailenddate = startdate;
                    globalObj.emailsettingobj.repeat = Integer.parseInt(_emailRepeatStatus);
                }

                if (globalObj.emailsettingobj.emailenddate.before(new Date())) {
                    c.setTime(new Date());
                    if (Integer.parseInt(_emailRepeatStatus) == MONTH) {
                        c.add(Calendar.DATE, 30);
                        startdate = c.getTime();
                        globalObj.emailsettingobj.emailenddate = startdate;
                        globalObj.emailsettingobj.repeat = Integer.parseInt(_emailRepeatStatus);
                    } else if (Integer.parseInt(_emailRepeatStatus) == THREE_MONTH) {
                        c.add(Calendar.DATE, 90);
                        startdate = c.getTime();
                        globalObj.emailsettingobj.emailenddate = startdate;
                        globalObj.emailsettingobj.repeat = Integer.parseInt(_emailRepeatStatus);
                    } else if (Integer.parseInt(_emailRepeatStatus) == SIX_MONTH) {
                        c.add(Calendar.DATE, 180);
                        startdate = c.getTime();
                        globalObj.emailsettingobj.emailenddate = startdate;
                        globalObj.emailsettingobj.repeat = Integer.parseInt(_emailRepeatStatus);
                    }

                }
            }
            if (_emailAlertStatus != null) {
                globalObj.emailsettingobj.emailalertstatus = Integer.parseInt(_emailAlertStatus);
            }
            if (_emailcost != null) {
                globalObj.emailsettingobj.emailcost = Float.parseFloat(_emailcost);
            }
            if (_emailStatus != null) {
                globalObj.emailsettingobj.emaillimitstatus = Integer.parseInt(_emailStatus);
            }
            if (_emailThresholdlimit != null) {
                globalObj.emailsettingobj.thresholdlimit = Integer.parseInt(_emailThresholdlimit);
            }
            if (_emaillimit != null) {
                globalObj.emailsettingobj.emaillimit = Integer.parseInt(_emaillimit);
            }
            if (_fastPaceEMAIL != null) {
                globalObj.emailsettingobj._fastPaceEMAIL = Integer.parseInt(_fastPaceEMAIL);
            }
            if (_hyperPaceEMAIL != null) {
                globalObj.emailsettingobj._hyperPaceEMAIL = Integer.parseInt(_hyperPaceEMAIL);
            }

            if (_normalPaceEMAIL != null) {
                globalObj.emailsettingobj._normalPaceEMAIL = Integer.parseInt(_normalPaceEMAIL);
            }
            if (_slowPaceEMAIL != null) {
                globalObj.emailsettingobj._slowPaceEMAIL = Integer.parseInt(_slowPaceEMAIL);
            }
            globalObj.emailsettingobj._emailFooter = _emailFooter;
        }

        if (_isettype == FAX) {
            String _faxlimit = request.getParameter("_faxlimit");
            log.debug("_faxlimit :: "+_faxlimit);
            String _faxcost = request.getParameter("_faxcost");
            log.debug("_faxcost :: "+_faxcost);
            String _faxStatus = request.getParameter("_faxStatus");
            log.debug("_faxStatus :: "+_faxStatus);
            String _faxAlertStatus = request.getParameter("_faxAlertStatus");
            log.debug("_faxAlertStatus :: "+_faxAlertStatus);
            String _faxRepeatStatus = request.getParameter("_faxRepeatStatus");
            log.debug("_faxRepeatStatus :: "+_faxRepeatStatus);
            String _faxFooter = request.getParameter("_faxFooter");
            log.debug("_faxFooter :: "+_faxFooter);
            String _faxThresholdlimit = request.getParameter("_faxThresholdlimit");
            log.debug("_faxThresholdlimit :: "+_faxThresholdlimit);
            String _fastPaceFAX = request.getParameter("_fastPaceFAX");
            log.debug("_fastPaceFAX :: "+_fastPaceFAX);
            String _hyperPaceFAX = request.getParameter("_hyperPaceFAX");
            log.debug("_hyperPaceFAX :: "+_hyperPaceFAX);
            String _normalPaceFAX = request.getParameter("_normalPaceFAX");
            log.debug("_normalPaceFAX :: "+_normalPaceFAX);
            String _slowPaceFAX = request.getParameter("_slowPaceFAX");
            log.debug("_slowPaceFAX :: "+_slowPaceFAX);

            //date time restriction
            String _dayRestrictionFAX = request.getParameter("_dayRestrictionFAX");
            log.debug("_dayRestrictionFAX :: "+_dayRestrictionFAX);
            String _timeFromInHourFAX = request.getParameter("_timeFromRestrictionFAX");
            log.debug("_timeFromInHourFAX :: "+_timeFromInHourFAX);
            String _timeToInHourFAX = request.getParameter("_timeToRestrictionFAX");
            log.debug("_timeToInHourFAX :: "+_timeToInHourFAX);
            String _faxFooterStatus = request.getParameter("_faxFooterStatus");
            log.debug("_faxFooterStatus :: "+_faxFooterStatus);
            globalObj.faxlimitsettingobj = new faxlimitsetting();

            if (_faxFooterStatus != null) {
                globalObj.faxlimitsettingobj._faxFooterStatus = Integer.parseInt(_faxFooterStatus);
            }

            //date & time restriction
            if (_dayRestrictionFAX != null) {
                globalObj.faxlimitsettingobj._dayRestrictionFAX = Integer.parseInt(_dayRestrictionFAX);
            }
            if (_timeFromInHourFAX != null) {
                globalObj.faxlimitsettingobj._timeFromRestrictionFAX = Integer.parseInt(_timeFromInHourFAX);
            }

            if (_timeToInHourFAX != null) {
                globalObj.faxlimitsettingobj._timeToRestrictionFAX = Integer.parseInt(_timeToInHourFAX);
            }

            if (_faxRepeatStatus != null) {
                c.setTime(startdate);
                if (Integer.parseInt(_faxRepeatStatus) == MONTH) {
                    c.add(Calendar.DATE, 30);
                    startdate = c.getTime();
                    globalObj.faxlimitsettingobj.faxenddate = startdate;
                    globalObj.faxlimitsettingobj.repeat = Integer.parseInt(_faxRepeatStatus);
                } else if (Integer.parseInt(_faxRepeatStatus) == THREE_MONTH) {
                    c.add(Calendar.DATE, 90);
                    startdate = c.getTime();
                    globalObj.faxlimitsettingobj.faxenddate = startdate;
                    globalObj.faxlimitsettingobj.repeat = Integer.parseInt(_faxRepeatStatus);
                } else if (Integer.parseInt(_faxRepeatStatus) == SIX_MONTH) {
                    c.add(Calendar.DATE, 180);
                    startdate = c.getTime();
                    globalObj.faxlimitsettingobj.faxenddate = startdate;
                    globalObj.faxlimitsettingobj.repeat = Integer.parseInt(_faxRepeatStatus);
                }

                if (globalObj.faxlimitsettingobj.faxenddate.before(new Date())) {
                    c.setTime(new Date());
                    if (Integer.parseInt(_faxRepeatStatus) == MONTH) {
                        c.add(Calendar.DATE, 30);
                        startdate = c.getTime();
                        globalObj.faxlimitsettingobj.faxenddate = startdate;
                        globalObj.faxlimitsettingobj.repeat = Integer.parseInt(_faxRepeatStatus);
                    } else if (Integer.parseInt(_faxRepeatStatus) == THREE_MONTH) {
                        c.add(Calendar.DATE, 90);
                        startdate = c.getTime();
                        globalObj.faxlimitsettingobj.faxenddate = startdate;
                        globalObj.faxlimitsettingobj.repeat = Integer.parseInt(_faxRepeatStatus);
                    } else if (Integer.parseInt(_faxRepeatStatus) == SIX_MONTH) {
                        c.add(Calendar.DATE, 180);
                        startdate = c.getTime();
                        globalObj.faxlimitsettingobj.faxenddate = startdate;
                        globalObj.faxlimitsettingobj.repeat = Integer.parseInt(_faxRepeatStatus);
                    }

                }

            }
            if (_faxAlertStatus != null) {
                globalObj.faxlimitsettingobj.faxalertstatus = Integer.parseInt(_faxAlertStatus);
            }
            if (_faxcost != null) {
                globalObj.faxlimitsettingobj.faxcost = Float.parseFloat(_faxcost);
            }
            if (_faxStatus != null) {
                globalObj.faxlimitsettingobj.faxlimitstatus = Integer.parseInt(_faxStatus);
            }
            if (_faxThresholdlimit != null) {
                globalObj.faxlimitsettingobj.thresholdlimit = Integer.parseInt(_faxThresholdlimit);
            }
            if (_faxlimit != null) {
                globalObj.faxlimitsettingobj.faxlimit = Integer.parseInt(_faxlimit);
            }
            if (_fastPaceFAX != null) {
                globalObj.faxlimitsettingobj._fastPaceFAX = Integer.parseInt(_fastPaceFAX);
            }
            if (_hyperPaceFAX != null) {
                globalObj.faxlimitsettingobj._hyperPaceFAX = Integer.parseInt(_hyperPaceFAX);
            }

            if (_normalPaceFAX != null) {
                globalObj.faxlimitsettingobj._normalPaceFAX = Integer.parseInt(_normalPaceFAX);
            }
            if (_slowPaceFAX != null) {
                globalObj.faxlimitsettingobj._slowPaceFAX = Integer.parseInt(_slowPaceFAX);
            }
            globalObj.faxlimitsettingobj._faxFooter = _faxFooter;
        }
        if (_isettype == PUSH) {
            String _pushlimit = request.getParameter("_pushlimit");
            log.debug("_pushlimit :: "+_pushlimit);
            String _pushcost = request.getParameter("_pushcost");
            log.debug("_pushcost :: "+_pushcost);
            String _pushStatus = request.getParameter("_pushStatus");
            log.debug("_pushStatus :: "+_pushStatus);
            String _pushAlertStatus = request.getParameter("_pushAlertStatus");
            log.debug("_pushAlertStatus :: "+_pushAlertStatus);
            String _pushRepeatStatus = request.getParameter("_pushRepeatStatus");
            log.debug("_pushRepeatStatus :: "+_pushRepeatStatus);
            String _pushFooter = request.getParameter("_pushFooter");
            log.debug("_pushFooter :: "+_pushFooter);
            String _pushThresholdlimit = request.getParameter("_pushThresholdlimit");
            log.debug("_pushThresholdlimit :: "+_pushThresholdlimit);
            String _fastPacePUSH = request.getParameter("_fastPacePUSH");
            log.debug("_fastPacePUSH :: "+_fastPacePUSH);
            String _hyperPacePUSH = request.getParameter("_hyperPacePUSH");
            log.debug("_hyperPacePUSH :: "+_hyperPacePUSH);
            String _normalPacePUSH = request.getParameter("_normalPacePUSH");
            log.debug("_normalPacePUSH :: "+_normalPacePUSH);
            String _slowPacePUSH = request.getParameter("_slowPacePUSH");
            log.debug("_slowPacePUSH :: "+_slowPacePUSH);
            //date time restriction
            String _dayRestrictionPUSH = request.getParameter("_dayRestrictionPUSH");
            log.debug("_dayRestrictionPUSH :: "+_dayRestrictionPUSH);
            String _timeFromInHourPUSH = request.getParameter("_timeFromRestrictionPUSH");
            log.debug("_timeFromInHourPUSH :: "+_timeFromInHourPUSH);
            String _timeToInHourPUSH = request.getParameter("_timeToRestrictionPUSH");
            log.debug("_timeToInHourPUSH :: "+_timeToInHourPUSH);
            String _pushFooterStatus = request.getParameter("_pushFooterStatus");
            log.debug("_pushFooterStatus :: "+_pushFooterStatus);
            globalObj.pushsettingobj = new pushsetting();

            if (_pushFooterStatus != null) {
                globalObj.pushsettingobj._pushFooterStatus = Integer.parseInt(_pushFooterStatus);
            }
            //date & time restriction
            if (_dayRestrictionPUSH != null) {
                globalObj.pushsettingobj._dayRestrictionPUSH = Integer.parseInt(_dayRestrictionPUSH);
            }
            if (_timeFromInHourPUSH != null) {
                globalObj.pushsettingobj._timeFromRestrictionPUSH = Integer.parseInt(_timeFromInHourPUSH);
            }

            if (_timeToInHourPUSH != null) {
                globalObj.pushsettingobj._timeToRestrictionPUSH = Integer.parseInt(_timeToInHourPUSH);
            }
            if (_pushRepeatStatus != null) {
                c.setTime(startdate);
                if (Integer.parseInt(_pushRepeatStatus) == MONTH) {
                    c.add(Calendar.DATE, 30);
                    startdate = c.getTime();
                    globalObj.pushsettingobj.pushenddate = startdate;
                    globalObj.pushsettingobj.repeat = Integer.parseInt(_pushRepeatStatus);
                } else if (Integer.parseInt(_pushRepeatStatus) == THREE_MONTH) {
                    c.add(Calendar.DATE, 90);
                    startdate = c.getTime();
                    globalObj.pushsettingobj.pushenddate = startdate;
                    globalObj.pushsettingobj.repeat = Integer.parseInt(_pushRepeatStatus);
                } else if (Integer.parseInt(_pushRepeatStatus) == SIX_MONTH) {
                    c.add(Calendar.DATE, 180);
                    startdate = c.getTime();
                    globalObj.pushsettingobj.pushenddate = startdate;
                    globalObj.pushsettingobj.repeat = Integer.parseInt(_pushRepeatStatus);
                }

                if (globalObj.pushsettingobj.pushenddate.before(new Date())) {
                    c.setTime(new Date());
                    if (Integer.parseInt(_pushRepeatStatus) == MONTH) {
                        c.add(Calendar.DATE, 30);
                        startdate = c.getTime();
                        globalObj.pushsettingobj.pushenddate = startdate;
                        globalObj.pushsettingobj.repeat = Integer.parseInt(_pushRepeatStatus);
                    } else if (Integer.parseInt(_pushRepeatStatus) == THREE_MONTH) {
                        c.add(Calendar.DATE, 90);
                        startdate = c.getTime();
                        globalObj.pushsettingobj.pushenddate = startdate;
                        globalObj.pushsettingobj.repeat = Integer.parseInt(_pushRepeatStatus);
                    } else if (Integer.parseInt(_pushRepeatStatus) == SIX_MONTH) {
                        c.add(Calendar.DATE, 180);
                        startdate = c.getTime();
                        globalObj.pushsettingobj.pushenddate = startdate;
                        globalObj.pushsettingobj.repeat = Integer.parseInt(_pushRepeatStatus);
                    }

                }

            }
            if (_pushAlertStatus != null) {
                globalObj.pushsettingobj.pushalertstatus = Integer.parseInt(_pushAlertStatus);
            }
            if (_pushcost != null) {
                globalObj.pushsettingobj.pushcost = Float.parseFloat(_pushcost);
            }
            if (_pushStatus != null) {
                globalObj.pushsettingobj.pushlimitstatus = Integer.parseInt(_pushStatus);
            }
            if (_pushThresholdlimit != null) {
                globalObj.pushsettingobj.thresholdlimit = Integer.parseInt(_pushThresholdlimit);
            }
            if (_pushlimit != null) {
                globalObj.pushsettingobj.pushlimit = Integer.parseInt(_pushlimit);
            }
            if (_fastPacePUSH != null) {
                globalObj.pushsettingobj._fastPacePUSH = Integer.parseInt(_fastPacePUSH);
            }
            if (_hyperPacePUSH != null) {
                globalObj.pushsettingobj._hyperPacePUSH = Integer.parseInt(_hyperPacePUSH);
            }

            if (_normalPacePUSH != null) {
                globalObj.pushsettingobj._normalPacePUSH = Integer.parseInt(_normalPacePUSH);
            }
            if (_slowPacePUSH != null) {
                globalObj.pushsettingobj._slowPacePUSH = Integer.parseInt(_slowPacePUSH);
            }
            globalObj.pushsettingobj._pushFooter = _pushFooter;

        }

        AuditManagement audit = new AuditManagement();
        if (bAddSetting == true) {
            
            
//            String struseralertstatus = "INACTIVE";
//            if (_isettype == USER_ALERT) {
//               
//                int iStatus = globalObj.alertuser;
//                if (iStatus == 0) {
//                    struseralertstatus = "ACTIVE";
//                }else{
//                     struseralertstatus = "In-ACTIVE";
//                }
//               
//            }
            
            
            String strContent = null;
            String strContentStatus = "INACTIVE";
            String strcontentalertstatus = "INACTIVE";
            if (_isettype == CONTENT_FILTER) {
                String[] arrCotent = globalObj.content;
                for (int i = 0; i < arrCotent.length; i++) {
                    strContent += arrCotent[i] + ",";
                }
                int iStatus = globalObj.contentstatus;
                if (iStatus == 0) {
                    strContentStatus = "ACTIVE";
                }
                int iopalertstatus = globalObj.contentalertstatus;
                if (iopalertstatus == 0) {
                    strContentStatus = "ACTIVE";
                }
            }
            String strIp = null;
            String strIpStatus = "INACTIVE";
            String stripalertstatus = "INACTIVE";
            if (_isettype == IP_FILTER) {
                String[] arrIp = globalObj.IP;
                for (int i = 0; i < arrIp.length; i++) {
                    strIp += arrIp[i] + ",";
                }
                int iStatus = globalObj.ipstatus;
                strIpStatus = "INACTIVE";
                if (iStatus == 0) {
                    strIpStatus = "ACTIVE";
                }
                int iopalertstatus = globalObj.ipalertstatus;
                if (iopalertstatus == 0) {
                    stripalertstatus = "ACTIVE";
                }
            }

            String strPrefix = null;
            String strPrefixStatus = "INACTIVE";
            String strPrefixalertstatus = "INACTIVE";
            if (_isettype == PREFIX_FILTER) {
                String[] arrPrefix = globalObj.prefix;
                for (int i = 0; i < arrPrefix.length; i++) {
                    strPrefix += arrPrefix[i] + ",";
                }
                int iStatus = globalObj.prefixstatus;
                strPrefixStatus = "INACTIVE";
                if (iStatus == 0) {
                    strPrefixStatus = "ACTIVE";
                }
                int iPrefixalertstatus = globalObj.prefixalertstatus;
                if (iPrefixalertstatus == 0) {
                    strPrefixalertstatus = "ACTIVE";
                }
            }

            String strDomain = null;
            String strDomainStatus = "INACTIVE";
            String strDomainalertstatus = "INACTIVE";
            if (_isettype == DOMAIN_FILTER) {
                String[] arrDomain = globalObj.domain;
                for (int i = 0; i < arrDomain.length; i++) {
                    strPrefix += arrDomain[i] + ",";
                }
                int iStatus = globalObj.domainstatus;
                strDomainStatus = "INACTIVE";
                if (iStatus == 0) {
                    strDomainStatus = "ACTIVE";
                }
                int iDomainalertstatus = globalObj.domainalertstatus;
                if (iDomainalertstatus == 0) {
                    strDomainalertstatus = "ACTIVE";
                }
            }
//sms audit
//            String strduration = "-";
            String strrepeat = "-";
            String strsmslimitstatus = "In-Active";
            String strsmsalertstatus = "In-Active";
            int smslimit = 0;
            int smsthresholdlimit = 0;
            float smscost = 0;
            int _fastPaceSMS = 0;
            int _hyperPaceSMS = 0;
            int _normalPaceSMS = 0;
            int _slowPaceSMS = 0;

            String _dayRestrictionSMS = "-";
            String _timeFromRestrictionSMS = "-";
            String _timeToRestrictionSMS = "-";
            String _smsFooter = "-";
              String _smsFooterStatus = "-";

            Date smsenddate = channel.getCreatedOn();

            if (_isettype == SMS) {
                smslimit = globalObj.smssettingobj.smslimit;
                smsthresholdlimit = globalObj.smssettingobj.thresholdlimit;
                smscost = globalObj.smssettingobj.smscost;

                _fastPaceSMS = globalObj.smssettingobj._fastPaceSMS;
                _hyperPaceSMS = globalObj.smssettingobj._hyperPaceSMS;
                _normalPaceSMS = globalObj.smssettingobj._normalPaceSMS;
                _slowPaceSMS = globalObj.smssettingobj._slowPaceSMS;

//                int duration = globalObj.smssettingobj.duration;
                int repeat = globalObj.smssettingobj.repeat;
                int smsalertstatus = globalObj.smssettingobj.smsalertstatus;
                int smslimitstatus = globalObj.smssettingobj.smslimitstatus;

                smsenddate = globalObj.smssettingobj.smsenddate;

                _smsFooter = globalObj.smssettingobj._smsFooter;
                if(globalObj.smssettingobj._smsFooterStatus == 1){
                   _smsFooterStatus = "Active";
                }else if(globalObj.smssettingobj._smsFooterStatus == 0){
                     _smsFooterStatus = "Suspended";
                }
                if (globalObj.smssettingobj._dayRestrictionSMS == 1) {
                    _dayRestrictionSMS = "Week days only";
                } else if (globalObj.smssettingobj._dayRestrictionSMS == 2) {
                    _dayRestrictionSMS = "Whole Week (including Weekend)";
                }

                if (globalObj.smssettingobj._timeFromRestrictionSMS == 7) {
                    _timeFromRestrictionSMS = "7AM";
                } else if (globalObj.smssettingobj._timeFromRestrictionSMS == 8) {
                    _timeFromRestrictionSMS = "8AM";
                } else if (globalObj.smssettingobj._timeFromRestrictionSMS == 9) {
                    _timeFromRestrictionSMS = "9AM";
                } else if (globalObj.smssettingobj._timeFromRestrictionSMS == 10) {
                    _timeFromRestrictionSMS = "10AM";
                } else if (globalObj.smssettingobj._timeFromRestrictionSMS == 11) {
                    _timeFromRestrictionSMS = "11AM";
                } else if (globalObj.smssettingobj._timeFromRestrictionSMS == 0) {
                    _timeFromRestrictionSMS = "Any";
                }

                if (globalObj.smssettingobj._timeToRestrictionSMS == 12) {
                    _timeToRestrictionSMS = "12PM";
                } else if (globalObj.smssettingobj._timeToRestrictionSMS == 13) {
                    _timeToRestrictionSMS = "1PM";
                } else if (globalObj.smssettingobj._timeToRestrictionSMS == 14) {
                    _timeToRestrictionSMS = "2PM";
                } else if (globalObj.smssettingobj._timeToRestrictionSMS == 15) {
                    _timeToRestrictionSMS = "3PM";
                } else if (globalObj.smssettingobj._timeToRestrictionSMS == 16) {
                    _timeToRestrictionSMS = "4PM";
                } else if (globalObj.smssettingobj._timeToRestrictionSMS == 17) {
                    _timeToRestrictionSMS = "5PM";
                } else if (globalObj.smssettingobj._timeToRestrictionSMS == 18) {
                    _timeToRestrictionSMS = "6PM";
                } else if (globalObj.smssettingobj._timeToRestrictionSMS == 19) {
                    _timeToRestrictionSMS = "7PM";
                } else if (globalObj.smssettingobj._timeToRestrictionSMS == 24) {
                    _timeToRestrictionSMS = "Any";
                }

                if (repeat == 1) {
                    strrepeat = "1 Month";
                } else if (repeat == 2) {
                    strrepeat = "3 Months";
                } else if (repeat == 3) {
                    strrepeat = "6 Months";
                }

                if (smsalertstatus == 1) {
                    strsmsalertstatus = "ACTIVE";
                }
                if (smslimitstatus == 1) {
                    strsmslimitstatus = "ACTIVE";
                }

            }
    //voice audit        
            //  String strvoiceduration = "-";
            String strvoicerepeat = "In-Active";
            String strvoicelimitstatus = "In-Active";
            String strvoicealertstatus = "In-Active";
            int voicelimit = 0;
            int voicethresholdlimit = 0;
            float voicecost = 0;
            int _fastPaceVOICE = 0;
            int _hyperPaceVOICE = 0;
            int _normalPaceVOICE = 0;
            int _slowPaceVOICE = 0;

            String _dayRestrictionVOICE = "-";
            String _timeFromRestrictionVOICE = "-";
            String _timeToRestrictionVOICE = "-";
            String _voiceFooter = "-";
            String _voiceFooterStatus = "-";

            Date voiceenddate = channel.getCreatedOn();
            if (_isettype == VOICE) {
                voicelimit = globalObj.voicesettingobj.voicemsglimit;
                voicethresholdlimit = globalObj.voicesettingobj.thresholdlimit;
                voicecost = globalObj.voicesettingobj.voicemsgcost;
                _fastPaceVOICE = globalObj.voicesettingobj._fastPaceVOICE;
                _hyperPaceVOICE = globalObj.voicesettingobj._hyperPaceVOICE;
                _normalPaceVOICE = globalObj.voicesettingobj._normalPaceVOICE;
                _slowPaceVOICE = globalObj.voicesettingobj._slowPaceVOICE;

//                int duration = globalObj.voicesettingobj.duration;
                int repeat = globalObj.voicesettingobj.repeat;
                int voicealertstatus = globalObj.voicesettingobj.voicemsgalertstatus;
                int voicelimitstatus = globalObj.voicesettingobj.voicemsglimit;

                voiceenddate = globalObj.voicesettingobj.voiceenddate;

                    _voiceFooter = globalObj.voicesettingobj._voiceFooter;
                if(globalObj.voicesettingobj._voiceFooterStatus == 1){
                   _smsFooterStatus = "Active";
                }else if(globalObj.voicesettingobj._voiceFooterStatus == 0){
                     _voiceFooterStatus = "Suspended";
                }
                if (globalObj.voicesettingobj._dayRestrictionVOICE == 1) {
                    _dayRestrictionVOICE = "Week days only";
                } else if (globalObj.voicesettingobj._dayRestrictionVOICE == 2) {
                    _dayRestrictionVOICE = "Whole Week (including Weekend)";
                }

                if (globalObj.voicesettingobj._timeFromRestrictionVOICE == 7) {
                    _timeFromRestrictionVOICE = "7AM";
                } else if (globalObj.voicesettingobj._timeFromRestrictionVOICE == 8) {
                    _timeFromRestrictionVOICE = "8AM";
                } else if (globalObj.voicesettingobj._timeFromRestrictionVOICE == 9) {
                    _timeFromRestrictionVOICE = "9AM";
                } else if (globalObj.voicesettingobj._timeFromRestrictionVOICE == 10) {
                    _timeFromRestrictionVOICE = "10AM";
                } else if (globalObj.voicesettingobj._timeFromRestrictionVOICE == 11) {
                    _timeFromRestrictionVOICE = "11AM";
                } else if (globalObj.voicesettingobj._timeFromRestrictionVOICE == 0) {
                    _timeFromRestrictionVOICE = "Any";
                }

                if (globalObj.voicesettingobj._timeToRestrictionVOICE == 12) {
                    _timeToRestrictionVOICE = "12PM";
                } else if (globalObj.voicesettingobj._timeToRestrictionVOICE == 13) {
                    _timeToRestrictionVOICE = "1PM";
                } else if (globalObj.voicesettingobj._timeToRestrictionVOICE == 14) {
                    _timeToRestrictionVOICE = "2PM";
                } else if (globalObj.voicesettingobj._timeToRestrictionVOICE == 15) {
                    _timeToRestrictionVOICE = "3PM";
                } else if (globalObj.voicesettingobj._timeToRestrictionVOICE == 16) {
                    _timeToRestrictionVOICE = "4PM";
                } else if (globalObj.voicesettingobj._timeToRestrictionVOICE == 17) {
                    _timeToRestrictionVOICE = "5PM";
                } else if (globalObj.voicesettingobj._timeToRestrictionVOICE == 18) {
                    _timeToRestrictionVOICE = "6PM";
                } else if (globalObj.voicesettingobj._timeToRestrictionVOICE == 19) {
                    _timeToRestrictionVOICE = "7PM";
                } else if (globalObj.voicesettingobj._timeToRestrictionVOICE == 24) {
                    _timeToRestrictionVOICE = "Any";
                }

                if (repeat == 1) {
                    strrepeat = "1 Month";
                } else if (repeat == 2) {
                    strrepeat = "3 Months";
                } else if (repeat == 3) {
                    strrepeat = "6 Months";
                }

                if (voicealertstatus == 1) {
                    strvoicealertstatus = "ACTIVE";
                }
                if (voicelimitstatus == 1) {
                    strvoicelimitstatus = "ACTIVE";
                }

            }

            //ussd audit        
//            String strussdduration = "-";
            String strussdrepeat = "In-Active";
            String strussdlimitstatus = "In-Active";
            String strussdalertstatus = "In-Active";
            int ussdlimit = 0;
            int ussdthresholdlimit = 0;
            float ussdcost = 0;
            int _fastPaceUSSD = 0;
            int _hyperPaceUSSD = 0;
            int _normalPaceUSSD = 0;
            int _slowPaceUSSD = 0;

            String _dayRestrictionUSSD = "-";
            String _timeFromRestrictionUSSD = "-";
            String _timeToRestrictionUSSD = "-";
             String _ussdFooter = "-";
              String _ussdFooterStatus = "-";

            Date ussdenddate = channel.getCreatedOn();
            if (_isettype == USSD) {
                
                    _ussdFooter = globalObj.ussdsettingobj._ussdFooter;
                if(globalObj.ussdsettingobj._ussdFooterStatus == 1){
                   _ussdFooterStatus = "Active";
                }else if(globalObj.ussdsettingobj._ussdFooterStatus == 0){
                     _ussdFooterStatus = "Suspended";
                }
                
                ussdlimit = globalObj.ussdsettingobj.ussdlimit;
                ussdthresholdlimit = globalObj.ussdsettingobj.thresholdlimit;
                ussdcost = globalObj.ussdsettingobj.ussdcost;
                _fastPaceUSSD = globalObj.ussdsettingobj._fastPaceUSSD;
                _hyperPaceUSSD = globalObj.ussdsettingobj._hyperPaceUSSD;
                _normalPaceUSSD = globalObj.ussdsettingobj._normalPaceUSSD;
                _slowPaceUSSD = globalObj.ussdsettingobj._slowPaceUSSD;

//                int duration = globalObj.ussdsettingobj.duration;
                int repeat = globalObj.ussdsettingobj.repeat;
                int ussdalertstatus = globalObj.ussdsettingobj.ussdalertstatus;
                int ussdlimitstatus = globalObj.ussdsettingobj.ussdlimit;

                ussdenddate = globalObj.ussdsettingobj.ussdenddate;

                if (globalObj.ussdsettingobj._dayRestrictionUSSD == 1) {
                    _dayRestrictionUSSD = "Week days only";
                } else if (globalObj.ussdsettingobj._dayRestrictionUSSD == 2) {
                    _dayRestrictionUSSD = "Whole Week (including Weekend)";
                }

                if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 7) {
                    _timeFromRestrictionUSSD = "7AM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 8) {
                    _timeFromRestrictionUSSD = "8AM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 9) {
                    _timeFromRestrictionUSSD = "9AM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 10) {
                    _timeFromRestrictionUSSD = "10AM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 11) {
                    _timeFromRestrictionUSSD = "11AM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 0) {
                    _timeFromRestrictionUSSD = "Any";
                }

                if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 12) {
                    _timeToRestrictionUSSD = "12PM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 13) {
                    _timeToRestrictionUSSD = "1PM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 14) {
                    _timeToRestrictionUSSD = "2PM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 15) {
                    _timeToRestrictionUSSD = "3PM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 16) {
                    _timeToRestrictionUSSD = "4PM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 17) {
                    _timeToRestrictionUSSD = "5PM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 18) {
                    _timeToRestrictionUSSD = "6PM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 19) {
                    _timeToRestrictionUSSD = "7PM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 24) {
                    _timeToRestrictionUSSD = "Any";
                }
                if (repeat == 1) {
                    strrepeat = "1 Month";
                } else if (repeat == 2) {
                    strrepeat = "3 Months";
                } else if (repeat == 3) {
                    strrepeat = "6 Months";
                }

                if (ussdalertstatus == 1) {
                    strussdalertstatus = "ACTIVE";
                }
                if (ussdlimitstatus == 1) {
                    strussdlimitstatus = "ACTIVE";
                }

            }

            //EMAIL audit        
//            String stremailduration = "-";
            String stremailrepeat = "In-Active";
            String stremaillimitstatus = "In-Active";
            String stremailalertstatus = "In-Active";
            int emaillimit = 0;
            int emailthresholdlimit = 0;
            float emailcost = 0;
            int _fastPaceEMAIL = 0;
            int _hyperPaceEMAIL = 0;
            int _normalPaceEMAIL = 0;
            int _slowPaceEMAIL = 0;

            String _dayRestrictionEMAIL = "-";
            String _timeFromRestrictionEMAIL = "-";
            String _timeToRestrictionEMAIL = "-";
             String _emailFooter = "-";
              String _emailFooterStatus = "-";

            Date emailenddate = channel.getCreatedOn();

            if (_isettype == EMAIL) {
                
                  _emailFooter = globalObj.emailsettingobj._emailFooter;
                if(globalObj.emailsettingobj._emailFooterStatus == 1){
                   _emailFooterStatus = "Active";
                }else if(globalObj.emailsettingobj._emailFooterStatus == 0){
                     _emailFooterStatus = "Suspended";
                }
                emaillimit = globalObj.emailsettingobj.emaillimit;
                emailthresholdlimit = globalObj.emailsettingobj.thresholdlimit;
                emailcost = globalObj.emailsettingobj.emailcost;
                _fastPaceEMAIL = globalObj.emailsettingobj._fastPaceEMAIL;
                _hyperPaceEMAIL = globalObj.emailsettingobj._hyperPaceEMAIL;
                _normalPaceEMAIL = globalObj.emailsettingobj._normalPaceEMAIL;
                _slowPaceEMAIL = globalObj.emailsettingobj._slowPaceEMAIL;

//                int duration = globalObj.emailsettingobj.duration;
                int repeat = globalObj.emailsettingobj.repeat;
                int emailalertstatus = globalObj.emailsettingobj.emailalertstatus;
                int emaillimitstatus = globalObj.emailsettingobj.emaillimit;

                emailenddate = globalObj.emailsettingobj.emailenddate;

                if (globalObj.emailsettingobj._dayRestrictionEMAIL == 1) {
                    _dayRestrictionEMAIL = "Week days only";
                } else if (globalObj.emailsettingobj._dayRestrictionEMAIL == 2) {
                    _dayRestrictionEMAIL = "Whole Week (including Weekend)";
                }

                if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 7) {
                    _timeFromRestrictionEMAIL = "7AM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 8) {
                    _timeFromRestrictionEMAIL = "8AM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 9) {
                    _timeFromRestrictionEMAIL = "9AM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 10) {
                    _timeFromRestrictionEMAIL = "10AM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 11) {
                    _timeFromRestrictionEMAIL = "11AM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 0) {
                    _timeFromRestrictionEMAIL = "Any";
                }

                if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 12) {
                    _timeToRestrictionEMAIL = "12PM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 13) {
                    _timeToRestrictionEMAIL = "1PM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 14) {
                    _timeToRestrictionEMAIL = "2PM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 15) {
                    _timeToRestrictionEMAIL = "3PM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 16) {
                    _timeToRestrictionEMAIL = "4PM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 17) {
                    _timeToRestrictionEMAIL = "5PM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 18) {
                    _timeToRestrictionEMAIL = "6PM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 19) {
                    _timeToRestrictionEMAIL = "7PM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 24) {
                    _timeToRestrictionEMAIL = "Any";
                }

                if (repeat == 1) {
                    strrepeat = "1 Month";
                } else if (repeat == 2) {
                    strrepeat = "3 Months";
                } else if (repeat == 3) {
                    strrepeat = "6 Months";
                }

                if (emailalertstatus == 1) {
                    stremailalertstatus = "ACTIVE";
                }
                if (emaillimitstatus == 1) {
                    stremaillimitstatus = "ACTIVE";
                }

            }

            //FAX audit        
//            String strfaxduration = "-";
            String strfaxrepeat = "In-Active";
            String strfaxlimitstatus = "In-Active";
            String strfaxalertstatus = "In-Active";
            int faxlimit = 0;
            int faxthresholdlimit = 0;
            float faxcost = 0;
            int _fastPaceFAX = 0;
            int _hyperPaceFAX = 0;
            int _normalPaceFAX = 0;
            int _slowPaceFAX = 0;

           String _dayRestrictionFAX = "-";
            String _timeFromRestrictionFAX = "-";
            String _timeToRestrictionFAX = "-";
            String _faxFooter = "-";
            String _faxFooterStatus = "-";

            Date faxenddate = channel.getCreatedOn();
            if (_isettype == FAX) {
                
                 _faxFooter = globalObj.faxlimitsettingobj._faxFooter;
                if(globalObj.faxlimitsettingobj._faxFooterStatus == 1){
                   _faxFooterStatus = "Active";
                }else if(globalObj.faxlimitsettingobj._faxFooterStatus == 0){
                     _faxFooterStatus = "Suspended";
                }
                faxlimit = globalObj.faxlimitsettingobj.faxlimit;
                faxthresholdlimit = globalObj.faxlimitsettingobj.thresholdlimit;
                faxcost = globalObj.faxlimitsettingobj.faxcost;
                _fastPaceFAX = globalObj.faxlimitsettingobj._fastPaceFAX;
                _hyperPaceFAX = globalObj.faxlimitsettingobj._hyperPaceFAX;
                _normalPaceFAX = globalObj.faxlimitsettingobj._normalPaceFAX;
                _slowPaceFAX = globalObj.faxlimitsettingobj._slowPaceFAX;

//                int duration = globalObj.faxlimitsettingobj.duration;
                int repeat = globalObj.faxlimitsettingobj.repeat;
                int faxalertstatus = globalObj.faxlimitsettingobj.faxalertstatus;
                int faxlimitstatus = globalObj.faxlimitsettingobj.faxlimit;

                faxenddate = globalObj.faxlimitsettingobj.faxenddate;

                if (globalObj.faxlimitsettingobj._dayRestrictionFAX == 1) {
                    _dayRestrictionFAX = "Week days only";
                } else if (globalObj.faxlimitsettingobj._dayRestrictionFAX == 2) {
                    _dayRestrictionFAX = "Whole Week (including Weekend)";
                }

                if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 7) {
                    _timeFromRestrictionFAX = "7AM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 8) {
                    _timeFromRestrictionFAX = "8AM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 9) {
                    _timeFromRestrictionFAX = "9AM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 10) {
                    _timeFromRestrictionFAX = "10AM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 11) {
                    _timeFromRestrictionFAX = "11AM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 0) {
                    _timeFromRestrictionFAX = "Any";
                }

                if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 12) {
                    _timeToRestrictionFAX = "12PM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 13) {
                    _timeToRestrictionFAX = "1PM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 14) {
                    _timeToRestrictionFAX = "2PM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 15) {
                    _timeToRestrictionFAX = "3PM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 16) {
                    _timeToRestrictionFAX = "4PM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 17) {
                    _timeToRestrictionFAX = "5PM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 18) {
                    _timeToRestrictionFAX = "6PM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 19) {
                    _timeToRestrictionFAX = "7PM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 24) {
                    _timeToRestrictionFAX = "Any";
                }

                if (repeat == 1) {
                    strrepeat = "1 Month";
                } else if (repeat == 2) {
                    strrepeat = "3 Months";
                } else if (repeat == 3) {
                    strrepeat = "6 Months";
                }

                if (faxalertstatus == 1) {
                    strfaxalertstatus = "ACTIVE";
                }
                if (faxlimitstatus == 1) {
                    strfaxlimitstatus = "ACTIVE";
                }

            }

            //PUSH audit        
//            String strpushduration = "-";
            String strpushrepeat = "In-Active";
            String strpushlimitstatus = "In-Active";
            String strpushalertstatus = "In-Active";
            int pushlimit = 0;
            int pushthresholdlimit = 0;
            float pushcost = 0;
            int _fastPacePUSH = 0;
            int _hyperPacePUSH = 0;
            int _normalPacePUSH = 0;
            int _slowPacePUSH = 0;

            String _dayRestrictionPUSH = "-";
            String _timeFromRestrictionPUSH = "-";
            String _timeToRestrictionPUSH = "-";
             String _pushFooter = "-";
              String _pushFooterStatus = "-";

            Date pushenddate = channel.getCreatedOn();

            if (_isettype == PUSH) {
                
                  _pushFooter = globalObj.pushsettingobj._pushFooter;
                if(globalObj.pushsettingobj._pushFooterStatus == 1){
                   _pushFooterStatus = "Active";
                }else if(globalObj.pushsettingobj._pushFooterStatus == 0){
                     _pushFooterStatus = "Suspended";
                }
                pushlimit = globalObj.pushsettingobj.pushlimit;
                pushthresholdlimit = globalObj.pushsettingobj.thresholdlimit;
                pushcost = globalObj.pushsettingobj.pushcost;
                _fastPacePUSH = globalObj.pushsettingobj._fastPacePUSH;
                _hyperPacePUSH = globalObj.pushsettingobj._hyperPacePUSH;
                _normalPacePUSH = globalObj.pushsettingobj._normalPacePUSH;
                _slowPacePUSH = globalObj.pushsettingobj._slowPacePUSH;

//                int duration = globalObj.pushsettingobj.duration;
                int repeat = globalObj.pushsettingobj.repeat;
                int pushalertstatus = globalObj.pushsettingobj.pushalertstatus;
                int pushlimitstatus = globalObj.pushsettingobj.pushlimit;

                pushenddate = globalObj.pushsettingobj.pushenddate;

                if (globalObj.pushsettingobj._dayRestrictionPUSH == 1) {
                    _dayRestrictionPUSH = "Week days only";
                } else if (globalObj.pushsettingobj._dayRestrictionPUSH == 2) {
                    _dayRestrictionPUSH = "Whole Week (including Weekend)";
                }

                if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 7) {
                    _timeFromRestrictionPUSH = "7AM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 8) {
                    _timeFromRestrictionPUSH = "8AM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 9) {
                    _timeFromRestrictionPUSH = "9AM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 10) {
                    _timeFromRestrictionPUSH = "10AM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 11) {
                    _timeFromRestrictionPUSH = "11AM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 0) {
                    _timeFromRestrictionPUSH = "Any";
                }

                if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 12) {
                    _timeToRestrictionFAX = "12PM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 13) {
                    _timeToRestrictionFAX = "1PM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 14) {
                    _timeToRestrictionFAX = "2PM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 15) {
                    _timeToRestrictionFAX = "3PM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 16) {
                    _timeToRestrictionFAX = "4PM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 17) {
                    _timeToRestrictionFAX = "5PM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 18) {
                    _timeToRestrictionFAX = "6PM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 19) {
                    _timeToRestrictionFAX = "7PM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 24) {
                    _timeToRestrictionFAX = "Any";
                }

                if (repeat == 1) {
                    strrepeat = "1 Month";
                } else if (repeat == 2) {
                    strrepeat = "3 Months";
                } else if (repeat == 3) {
                    strrepeat = "6 Months";
                }

                if (pushalertstatus == 1) {
                    strpushalertstatus = "ACTIVE";
                }
                if (pushlimitstatus == 1) {
                    strpushlimitstatus = "ACTIVE";
                }

            }

            retValue = sMngmt.addSetting(sessionId, _channelId, _itype, _iPreference, globalObj);
            String resultString = "ERROR";
            log.debug("editglobalsettingv2::addSetting::"+retValue);

            if (retValue == 0) {
                resultString = "SUCCESS";

                if (_isettype == CONTENT_FILTER) {

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add Content Filter ", resultString, retValue, "Setting Management",
                            "", "New Content =" + strContent + "Content Filter Status =" + strContentStatus + "Operator Alert Status =" + strcontentalertstatus,
                            itemtype,_channelId );
                }
                if (_isettype == IP_FILTER) {

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add IP Filter", resultString, retValue, "Setting Management",
                            "", "New IP =" + strIp + "IP Filter Status =" + strIpStatus + "Operator Alert Status =" + stripalertstatus,
                            itemtype, _channelId);
                }
                if (_isettype == PREFIX_FILTER) {

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add Prefix Filter", resultString, retValue, "Setting Management",
                            "", "New Prefix =" + strPrefix + "Prefix Filter Status =" + strPrefixStatus + "Operator Alert Status =" + strPrefixalertstatus,
                            itemtype, _channelId);
                }
                if (_isettype == DOMAIN_FILTER) {

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add Domain Filter", resultString, retValue, "Setting Management",
                            "", "New Domain =" + strDomain + "Domain Filter Status =" + strDomainStatus + "Operator Alert Status =" + strDomainalertstatus,
                            itemtype, _channelId);
                }
                if (_isettype == SMS) {

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add SMS Filter", resultString, retValue, "Setting Management",
                            "", "Limit =" + smslimit + "Cost =" + smscost + "Threshold Limit =" + smsthresholdlimit
                            + "Status =" + strsmslimitstatus + "Alert Status =" + strsmsalertstatus + "Repeat=" + strrepeat
                            + "SMS package End Date =" + smsenddate + "Slow Space = " + _slowPaceSMS + "Normal Space =" + _normalPaceSMS
                            + "Fast Space =" + _fastPaceSMS + "Hyper Space =" + _hyperPaceSMS
                            +"Day Restriction ="+_dayRestrictionSMS+"From Time ="+_timeFromRestrictionSMS+"Time To="+_timeToRestrictionSMS
                            +"SMS Footer ="+_smsFooter+"SMS Footer Alert Status ="+_smsFooterStatus,
                            itemtype, _channelId);
                }
                if (_isettype == VOICE) {

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add VOICE Filter", resultString, retValue, "Setting Management",
                            "", "Limit =" + voicelimit + "Cost =" + voicecost + "Threshold Limit =" + voicethresholdlimit
                            + "Status =" + strvoicelimitstatus + "Alert Status =" + strvoicealertstatus + "Repeat=" + strvoicerepeat
                            + "VOICE package End Date =" + voiceenddate + "Slow Space = " + _slowPaceVOICE + "Normal Space =" + _normalPaceVOICE
                            + "Fast Space =" + _fastPaceVOICE + "Hyper Space =" + _hyperPaceVOICE
                             +"Day Restriction ="+_dayRestrictionVOICE+"From Time ="+_timeFromRestrictionVOICE+"Time To="+_timeToRestrictionVOICE
                            +"VOICE Footer ="+_voiceFooter+"VOICE Footer Alert Status ="+_voiceFooterStatus,
                            itemtype, _channelId);
                }

                if (_isettype == USSD) {

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add USSD Filter", resultString, retValue, "Setting Management",
                            "", "Limit =" + ussdlimit + "Cost =" + ussdcost + "Threshold Limit =" + ussdthresholdlimit
                            + "Status =" + strussdlimitstatus + "Alert Status =" + strussdalertstatus + "Repeat=" + strussdrepeat
                            + "USSD package End Date =" + ussdenddate + "Slow Space = " + _slowPaceUSSD + "Normal Space =" + _normalPaceUSSD
                            + "Fast Space =" + _fastPaceUSSD + "Hyper Space =" + _hyperPaceUSSD
                            +"Day Restriction ="+_dayRestrictionUSSD+"From Time ="+_timeFromRestrictionUSSD+"Time To="+_timeToRestrictionUSSD
                            +"USSD Footer ="+_ussdFooter+"USSD Footer Alert Status ="+_ussdFooterStatus,
                            itemtype, _channelId);
                }

                if (_isettype == EMAIL) {

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add EMAIL Filter", resultString, retValue, "Setting Management",
                            "", "Limit =" + emaillimit + "Cost =" + emailcost + "Threshold Limit =" + emailthresholdlimit
                            + "Status =" + stremaillimitstatus + "Alert Status =" + stremailalertstatus + "Repeat=" + stremailrepeat
                            + "EMAIL package End Date =" + emailenddate + "Slow Space = " + _slowPaceEMAIL + "Normal Space =" + _normalPaceEMAIL
                            + "Fast Space =" + _fastPaceEMAIL + "Hyper Space =" + _hyperPaceEMAIL
                             +"Day Restriction ="+_dayRestrictionEMAIL+"From Time ="+_timeFromRestrictionEMAIL+"Time To="+_timeToRestrictionEMAIL
                            +"EMAIL Footer ="+_emailFooter+"EMAIL Footer Alert Status ="+_emailFooterStatus,
                            itemtype, _channelId);
                }
                if (_isettype == FAX) {

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add FAX Filter", resultString, retValue, "Setting Management",
                            "", "Limit =" + faxlimit + "Cost =" + faxcost + "Threshold Limit =" + faxthresholdlimit
                            + "Status =" + strfaxlimitstatus + "Alert Status =" + strfaxalertstatus + "Repeat=" + strfaxrepeat
                            + "FAX package End Date =" + faxenddate + "Slow Space = " + _slowPaceFAX + "Normal Space =" + _normalPaceFAX
                            + "Fast Space =" + _fastPaceFAX + "Hyper Space =" + _hyperPaceFAX
                             +"Day Restriction ="+_dayRestrictionFAX+"From Time ="+_timeFromRestrictionFAX+"Time To="+_timeToRestrictionFAX
                            +"FAX Footer ="+_faxFooter+"FAX Footer Alert Status ="+_faxFooterStatus,
                            itemtype, _channelId);
                }
                if (_isettype == PUSH) {

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add PUSH Notification Filter", resultString, retValue, "Setting Management",
                            "", "Limit =" + pushlimit + "Cost =" + pushcost + "Threshold Limit =" + pushthresholdlimit
                            + "Status =" + strpushlimitstatus + "Alert Status =" + strpushalertstatus + "Repeat=" + strpushrepeat
                            + "PUSH package End Date =" + pushenddate + "Slow Space = " + _slowPacePUSH + "Normal Space =" + _normalPacePUSH
                            + "Fast Space =" + _fastPacePUSH + "Hyper Space =" + _hyperPacePUSH
                              +"Day Restriction ="+_dayRestrictionPUSH+"From Time ="+_timeFromRestrictionPUSH+"Time To="+_timeToRestrictionPUSH
                            +"PUSH Footer ="+_pushFooter+"PUSH Footer Alert Status ="+_pushFooterStatus,
                            itemtype, _channelId);
                }
            } else if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add Channel Global Settings", resultString, retValue, "Setting Management",
                        "", "Failed To Add Channel Global Settings", itemtype, _channelId);

            }

        } else {
            GlobalChannelSettings goldObj = (GlobalChannelSettings) sMngmt.getSetting(sessionId, _channelId, _itype, _iPreference);
            retValue = sMngmt.changeSetting(sessionId, _channelId, _itype, _iPreference, settingsObj, globalObj);
            log.debug("editglobalsettingv2::changeSetting::"+retValue);
            String resultString = "ERROR";
            
            String stroldContent = null;
            String stroldContentStatus = "INACTIVE";
            String stroldcontentalertstatus = "INACTIVE";
            if (_isettype == CONTENT_FILTER) {
                if (goldObj.content != null) {
                    String[] arroldCotent = goldObj.content;
                    for (int i = 0; i < arroldCotent.length; i++) {
                        stroldContent += arroldCotent[i] + ",";
                    }
                }

                int iStatus = goldObj.contentstatus;
                if (iStatus == 0) {
                    stroldContentStatus = "ACTIVE";
                }
                int iopalertstatus = goldObj.contentalertstatus;
                if (iopalertstatus == 0) {
                    stroldContentStatus = "ACTIVE";
                }

            }
            String stroldIp = null;
            String stroldIpStatus = "INACTIVE";
            String stroldipalertstatus = "INACTIVE";
            if (_isettype == IP_FILTER) {
                String[] arroldIp = goldObj.IP;
                if (goldObj.IP != null) {
                    for (int i = 0; i < arroldIp.length; i++) {
                        stroldIp += arroldIp[i] + ",";
                    }
                }
                int iStatus = goldObj.ipstatus;
                stroldIpStatus = "INACTIVE";
                if (iStatus == 0) {
                    stroldIpStatus = "ACTIVE";
                }
                int iopalertstatus = goldObj.ipalertstatus;
                if (iopalertstatus == 0) {
                    stroldipalertstatus = "ACTIVE";
                }
            }

            String stroldPrefix = null;
            String stroldPrefixStatus = "INACTIVE";
            String stroldPrefixalertstatus = "INACTIVE";
            if (_isettype == PREFIX_FILTER) {
                if (goldObj.prefix != null) {
                    String[] arroldPrefix = goldObj.prefix;
                    for (int i = 0; i < arroldPrefix.length; i++) {
                        stroldPrefix += arroldPrefix[i] + ",";
                    }
                }
                int iStatus = goldObj.prefixstatus;
                stroldPrefixStatus = "INACTIVE";
                if (iStatus == 0) {
                    stroldPrefixStatus = "ACTIVE";
                }
                int iPrefixalertstatus = goldObj.prefixalertstatus;
                if (iPrefixalertstatus == 0) {
                    stroldPrefixalertstatus = "ACTIVE";
                }
            }
            String stroldDomain = null;
            String stroldDomainStatus = "INACTIVE";
            String stroldDomainalertstatus = "INACTIVE";
            if (_isettype == DOMAIN_FILTER) {
                if (goldObj.domain != null) {
                    String[] arroldDomain = goldObj.domain;
                    for (int i = 0; i < arroldDomain.length; i++) {
                        stroldPrefix += arroldDomain[i] + ",";
                    }
                }
                int iStatus = goldObj.domainstatus;
                stroldDomainStatus = "INACTIVE";
                if (iStatus == 0) {
                    stroldDomainStatus = "ACTIVE";
                }
                int iDomainalertstatus = goldObj.domainalertstatus;
                if (iDomainalertstatus == 0) {
                    stroldDomainalertstatus = "ACTIVE";
                }
            }
//              String stroldduration = "-";
            String stroldrepeat = "In-Active";
            String stroldsmslimitstatus = "In-Active";
            String stroldsmsalertstatus = "In-Active";
            int smsoldlimit = 0;
            int smsoldthresholdlimit = 0;
            float smsoldcost = 0;
            int _fastoldPaceSMS = 0;
            int _hyperoldPaceSMS = 0;
            int _normaloldPaceSMS = 0;
            int _slowoldPaceSMS = 0;
            
                  String _olddayRestrictionSMS = "-";
            String _oldtimeFromRestrictionSMS = "-";
            String _oldtimeToRestrictionSMS = "-";
            String _oldsmsFooter = "-";
              String _oldsmsFooterStatus = "-";

            Date smsoldendate = new Date();
            if (_isettype == SMS && goldObj.smssettingobj != null) {
                
                  _oldsmsFooter = goldObj.smssettingobj._smsFooter;
                if(goldObj.smssettingobj._smsFooterStatus == 1){
                   _oldsmsFooterStatus = "Active";
                }else if(goldObj.smssettingobj._smsFooterStatus == 0){
                     _oldsmsFooterStatus = "Suspended";
                }
                if (goldObj.smssettingobj._dayRestrictionSMS == 1) {
                    _olddayRestrictionSMS = "Week days only";
                } else if (goldObj.smssettingobj._dayRestrictionSMS == 2) {
                    _olddayRestrictionSMS = "Whole Week (including Weekend)";
                }

                if (goldObj.smssettingobj._timeFromRestrictionSMS == 7) {
                    _oldtimeFromRestrictionSMS = "7AM";
                } else if (goldObj.smssettingobj._timeFromRestrictionSMS == 8) {
                    _oldtimeFromRestrictionSMS = "8AM";
                } else if (goldObj.smssettingobj._timeFromRestrictionSMS == 9) {
                    _oldtimeFromRestrictionSMS = "9AM";
                } else if (goldObj.smssettingobj._timeFromRestrictionSMS == 10) {
                    _oldtimeFromRestrictionSMS = "10AM";
                } else if (goldObj.smssettingobj._timeFromRestrictionSMS == 11) {
                    _oldtimeFromRestrictionSMS = "11AM";
                } else if (goldObj.smssettingobj._timeFromRestrictionSMS == 0) {
                    _oldtimeFromRestrictionSMS = "Any";
                }

                if (goldObj.smssettingobj._timeToRestrictionSMS == 12) {
                    _oldtimeToRestrictionSMS = "12PM";
                } else if (goldObj.smssettingobj._timeToRestrictionSMS == 13) {
                    _oldtimeToRestrictionSMS = "1PM";
                } else if (goldObj.smssettingobj._timeToRestrictionSMS == 14) {
                    _oldtimeToRestrictionSMS = "2PM";
                } else if (goldObj.smssettingobj._timeToRestrictionSMS == 15) {
                    _oldtimeToRestrictionSMS = "3PM";
                } else if (goldObj.smssettingobj._timeToRestrictionSMS == 16) {
                    _oldtimeToRestrictionSMS = "4PM";
                } else if (goldObj.smssettingobj._timeToRestrictionSMS == 17) {
                    _oldtimeToRestrictionSMS = "5PM";
                } else if (goldObj.smssettingobj._timeToRestrictionSMS == 18) {
                    _oldtimeToRestrictionSMS = "6PM";
                } else if (goldObj.smssettingobj._timeToRestrictionSMS == 19) {
                    _oldtimeToRestrictionSMS = "7PM";
                } else if (goldObj.smssettingobj._timeToRestrictionSMS == 24) {
                    _oldtimeToRestrictionSMS = "Any";
                }

                
                smsoldlimit = goldObj.smssettingobj.smslimit;
                smsoldthresholdlimit = goldObj.smssettingobj.thresholdlimit;
                smsoldcost = goldObj.smssettingobj.smscost;

                _fastoldPaceSMS = goldObj.smssettingobj._fastPaceSMS;
                _hyperoldPaceSMS = goldObj.smssettingobj._hyperPaceSMS;
                _normaloldPaceSMS = goldObj.smssettingobj._normalPaceSMS;
                _slowoldPaceSMS = goldObj.smssettingobj._slowPaceSMS;

//                int duration = goldObj.smssettingobj.duration;
                int repeat = goldObj.smssettingobj.repeat;
                int smsalertstatus = goldObj.smssettingobj.smsalertstatus;
                int smslimitstatus = goldObj.smssettingobj.smslimitstatus;
                
                 
              
                smsoldendate = goldObj.smssettingobj.smsenddate;
                if (repeat == 1) {
                    stroldrepeat = "1 Month";
                } else if (repeat == 2) {
                    stroldrepeat = "3 Months";
                } else if (repeat == 3) {
                    stroldrepeat = "6 Months";
                }

                if (smsalertstatus == 1) {
                    stroldsmsalertstatus = "ACTIVE";
                }
                if (smslimitstatus == 1) {
                    stroldsmslimitstatus = "ACTIVE";
                }

            }
            //voice audit        
//            String stroldvoiceduration = "-";
            String stroldvoicerepeat = "In-Active";
            String stroldvoicelimitstatus = "In-Active";
            String stroldvoicealertstatus = "In-Active";
            int voiceoldlimit = 0;
            int voiceoldthresholdlimit = 0;
            float voiceoldcost = 0;
            int _fastoldPaceVOICE = 0;
            int _hyperoldPaceVOICE = 0;
            int _normaloldPaceVOICE = 0;
            int _slowoldPaceVOICE = 0;
            
                 String _olddayRestrictionVOICE = "-";
            String _oldtimeFromRestrictionVOICE = "-";
            String _oldtimeToRestrictionVOICE = "-";
            String _oldvoiceFooter = "-";
              String _oldvoiceFooterStatus = "-";

            Date voiceoldendate = new Date();

            if (_isettype == VOICE && goldObj.voicesettingobj != null) {
                
                
                     _oldvoiceFooter = goldObj.voicesettingobj._voiceFooter;
                if(goldObj.voicesettingobj._voiceFooterStatus == 1){
                   _oldvoiceFooterStatus = "Active";
                }else if(goldObj.voicesettingobj._voiceFooterStatus == 0){
                     _oldvoiceFooterStatus = "Suspended";
                }
                if (goldObj.voicesettingobj._dayRestrictionVOICE == 1) {
                    _olddayRestrictionVOICE = "Week days only";
                } else if (goldObj.voicesettingobj._dayRestrictionVOICE == 2) {
                    _olddayRestrictionVOICE = "Whole Week (including Weekend)";
                }

                if (goldObj.voicesettingobj._timeFromRestrictionVOICE == 7) {
                    _oldtimeFromRestrictionVOICE = "7AM";
                } else if (goldObj.voicesettingobj._timeFromRestrictionVOICE == 8) {
                    _oldtimeFromRestrictionVOICE = "8AM";
                } else if (goldObj.voicesettingobj._timeFromRestrictionVOICE == 9) {
                    _oldtimeFromRestrictionVOICE = "9AM";
                } else if (goldObj.voicesettingobj._timeFromRestrictionVOICE == 10) {
                    _oldtimeFromRestrictionVOICE = "10AM";
                } else if (goldObj.voicesettingobj._timeFromRestrictionVOICE == 11) {
                    _oldtimeFromRestrictionVOICE = "11AM";
                } else if (goldObj.voicesettingobj._timeFromRestrictionVOICE == 0) {
                    _oldtimeFromRestrictionVOICE = "Any";
                }

                if (goldObj.voicesettingobj._timeToRestrictionVOICE == 12) {
                    _oldtimeToRestrictionVOICE = "12PM";
                } else if (goldObj.voicesettingobj._timeToRestrictionVOICE== 13) {
                    _oldtimeToRestrictionVOICE = "1PM";
                } else if (goldObj.voicesettingobj._timeToRestrictionVOICE == 14) {
                    _oldtimeToRestrictionVOICE = "2PM";
                } else if (goldObj.voicesettingobj._timeToRestrictionVOICE == 15) {
                    _oldtimeToRestrictionVOICE = "3PM";
                } else if (goldObj.voicesettingobj._timeToRestrictionVOICE == 16) {
                    _oldtimeToRestrictionVOICE = "4PM";
                } else if (goldObj.voicesettingobj._timeToRestrictionVOICE == 17) {
                    _oldtimeToRestrictionVOICE = "5PM";
                } else if (goldObj.voicesettingobj._timeToRestrictionVOICE == 18) {
                    _oldtimeToRestrictionVOICE = "6PM";
                } else if (goldObj.voicesettingobj._timeToRestrictionVOICE == 19) {
                    _oldtimeToRestrictionVOICE = "7PM";
                } else if (goldObj.voicesettingobj._timeToRestrictionVOICE == 24) {
                    _oldtimeToRestrictionVOICE = "Any";
                }

                
                voiceoldlimit = goldObj.voicesettingobj.voicemsglimit;
                voiceoldthresholdlimit = goldObj.voicesettingobj.thresholdlimit;
                voiceoldcost = goldObj.voicesettingobj.voicemsgcost;
                _fastoldPaceVOICE = goldObj.voicesettingobj._fastPaceVOICE;
                _hyperoldPaceVOICE = goldObj.voicesettingobj._hyperPaceVOICE;
                _normaloldPaceVOICE = goldObj.voicesettingobj._normalPaceVOICE;
                _slowoldPaceVOICE = goldObj.voicesettingobj._slowPaceVOICE;

//                int duration = goldObj.voicesettingobj.duration;
                int repeat = goldObj.voicesettingobj.repeat;
                int voicealertstatus = goldObj.voicesettingobj.voicemsgalertstatus;
                int voicelimitstatus = goldObj.voicesettingobj.voicemsglimit;

                voiceoldendate = goldObj.voicesettingobj.voiceenddate;
                if (repeat == 1) {
                    stroldrepeat = "1 Month";
                } else if (repeat == 2) {
                    stroldrepeat = "3 Months";
                } else if (repeat == 3) {
                    stroldrepeat = "6 Months";
                }

                if (voicealertstatus == 1) {
                    stroldvoicealertstatus = "ACTIVE";
                }
                if (voicelimitstatus == 1) {
                    stroldvoicelimitstatus = "ACTIVE";
                }

            }

            //ussd audit        
//            String stroldussdduration = "-";
            String stroldussdrepeat = "In-Active";
            String stroldussdlimitstatus = "In-Active";
            String stroldussdalertstatus = "In-Active";
            int ussdoldlimit = 0;
            int ussdoldthresholdlimit = 0;
            float ussdoldcost = 0;
            int _fastoldPaceUSSD = 0;
            int _hyperoldPaceUSSD = 0;
            int _normaloldPaceUSSD = 0;
            int _slowoldPaceUSSD = 0;
            
              String _olddayRestrictionUSSD = "-";
            String _oldtimeFromRestrictionUSSD = "-";
            String _oldtimeToRestrictionUSSD = "-";
            String _oldussdFooter = "-";
              String _oldussdFooterStatus = "-";

            Date ussdoldendate = new Date();

            if (_isettype == USSD && goldObj.ussdsettingobj != null) {
                
                       _oldussdFooter = goldObj.ussdsettingobj._ussdFooter;
                if(goldObj.ussdsettingobj._ussdFooterStatus == 1){
                   _oldussdFooterStatus = "Active";
                }else if(goldObj.ussdsettingobj._ussdFooterStatus == 0){
                     _oldussdFooterStatus = "Suspended";
                }
                if (goldObj.ussdsettingobj._dayRestrictionUSSD == 1) {
                    _olddayRestrictionVOICE = "Week days only";
                } else if (goldObj.ussdsettingobj._dayRestrictionUSSD == 2) {
                    _olddayRestrictionVOICE = "Whole Week (including Weekend)";
                }

                if (goldObj.ussdsettingobj._timeFromRestrictionUSSD == 7) {
                    _oldtimeFromRestrictionUSSD = "7AM";
                } else if (goldObj.ussdsettingobj._timeFromRestrictionUSSD == 8) {
                    _oldtimeFromRestrictionUSSD = "8AM";
                } else if (goldObj.ussdsettingobj._timeFromRestrictionUSSD == 9) {
                    _oldtimeFromRestrictionUSSD = "9AM";
                } else if (goldObj.ussdsettingobj._timeFromRestrictionUSSD == 10) {
                    _oldtimeFromRestrictionUSSD = "10AM";
                } else if (goldObj.ussdsettingobj._timeFromRestrictionUSSD == 11) {
                    _oldtimeFromRestrictionUSSD = "11AM";
                } else if (goldObj.ussdsettingobj._timeFromRestrictionUSSD == 0) {
                    _oldtimeFromRestrictionUSSD = "Any";
                }

                if (goldObj.ussdsettingobj._timeToRestrictionUSSD == 12) {
                    _oldtimeToRestrictionUSSD = "12PM";
                } else if (goldObj.ussdsettingobj._timeToRestrictionUSSD== 13) {
                    _oldtimeToRestrictionUSSD = "1PM";
                } else if (goldObj.ussdsettingobj._timeToRestrictionUSSD == 14) {
                    _oldtimeToRestrictionUSSD = "2PM";
                } else if (goldObj.ussdsettingobj._timeToRestrictionUSSD == 15) {
                    _oldtimeToRestrictionUSSD = "3PM";
                } else if (goldObj.ussdsettingobj._timeToRestrictionUSSD == 16) {
                    _oldtimeToRestrictionUSSD = "4PM";
                } else if (goldObj.ussdsettingobj._timeToRestrictionUSSD == 17) {
                    _oldtimeToRestrictionUSSD = "5PM";
                } else if (goldObj.ussdsettingobj._timeToRestrictionUSSD == 18) {
                    _oldtimeToRestrictionUSSD = "6PM";
                } else if (goldObj.ussdsettingobj._timeToRestrictionUSSD == 19) {
                    _oldtimeToRestrictionUSSD = "7PM";
                } else if (goldObj.ussdsettingobj._timeToRestrictionUSSD == 24) {
                    _oldtimeToRestrictionUSSD = "Any";
                }

                
                ussdoldlimit = goldObj.ussdsettingobj.ussdlimit;
                ussdoldthresholdlimit = goldObj.ussdsettingobj.thresholdlimit;
                ussdoldcost = goldObj.ussdsettingobj.ussdcost;
                _fastoldPaceUSSD = goldObj.ussdsettingobj._fastPaceUSSD;
                _hyperoldPaceUSSD = goldObj.ussdsettingobj._hyperPaceUSSD;
                _normaloldPaceUSSD = goldObj.ussdsettingobj._normalPaceUSSD;
                _slowoldPaceUSSD = goldObj.ussdsettingobj._slowPaceUSSD;

//                int duration = goldObj.ussdsettingobj.duration;
                int repeat = goldObj.ussdsettingobj.repeat;
                int ussdoldalertstatus = goldObj.ussdsettingobj.ussdalertstatus;
                int ussdoldlimitstatus = goldObj.ussdsettingobj.ussdlimit;

                ussdoldendate = goldObj.ussdsettingobj.ussdenddate;

                if (repeat == 1) {
                    stroldrepeat = "1 Month";
                } else if (repeat == 2) {
                    stroldrepeat = "3 Months";
                } else if (repeat == 3) {
                    stroldrepeat = "6 Months";
                }

                if (ussdoldalertstatus == 1) {
                    stroldussdalertstatus = "ACTIVE";
                }
                if (ussdoldlimitstatus == 1) {
                    stroldussdlimitstatus = "ACTIVE";
                }

            }

            //EMAIL audit        
//            String stroldemailduration = "-";
            String stroldemailrepeat = "In-Active";
            String stroldemaillimitstatus = "In-Active";
            String stroldemailalertstatus = "In-Active";
            int emailoldlimit = 0;
            int emailoldthresholdlimit = 0;
            float emailoldcost = 0;
            int _fastoldPaceEMAIL = 0;
            int _hyperoldPaceEMAIL = 0;
            int _normaloldPaceEMAIL = 0;
            int _slowoldPaceEMAIL = 0;
                String _olddayRestrictionEMAIL = "-";
            String _oldtimeFromRestrictionEMAIL = "-";
            String _oldtimeToRestrictionEMAIL = "-";
            String _oldemailFooter = "-";
              String _oldemailFooterStatus = "-";
              int oldCRpercentage = goldObj.weightageForCR;

            Date emailoldendate = new Date();

            if (_isettype == EMAIL && goldObj.emailsettingobj != null) {
                
                      _oldemailFooter = goldObj.emailsettingobj._emailFooter;
                if(goldObj.emailsettingobj._emailFooterStatus == 1){
                   _oldemailFooterStatus = "Active";
                }else if(goldObj.emailsettingobj._emailFooterStatus == 0){
                     _oldemailFooterStatus = "Suspended";
                }
                if (goldObj.emailsettingobj._dayRestrictionEMAIL == 1) {
                    _olddayRestrictionEMAIL = "Week days only";
                } else if (goldObj.emailsettingobj._dayRestrictionEMAIL == 2) {
                    _olddayRestrictionEMAIL = "Whole Week (including Weekend)";
                }

                if (goldObj.emailsettingobj._timeFromRestrictionEMAIL == 7) {
                    _oldtimeFromRestrictionEMAIL = "7AM";
                } else if (goldObj.emailsettingobj._timeFromRestrictionEMAIL == 8) {
                    _oldtimeFromRestrictionEMAIL = "8AM";
                } else if (goldObj.emailsettingobj._timeFromRestrictionEMAIL == 9) {
                    _oldtimeFromRestrictionEMAIL = "9AM";
                } else if (goldObj.emailsettingobj._timeFromRestrictionEMAIL == 10) {
                    _oldtimeFromRestrictionEMAIL = "10AM";
                } else if (goldObj.emailsettingobj._timeFromRestrictionEMAIL == 11) {
                    _oldtimeFromRestrictionEMAIL = "11AM";
                } else if (goldObj.emailsettingobj._timeFromRestrictionEMAIL == 0) {
                    _oldtimeFromRestrictionEMAIL = "Any";
                }

                if (goldObj.emailsettingobj._timeToRestrictionEMAIL == 12) {
                    _oldtimeToRestrictionEMAIL = "12PM";
                } else if (goldObj.emailsettingobj._timeToRestrictionEMAIL== 13) {
                    _oldtimeToRestrictionEMAIL = "1PM";
                } else if (goldObj.emailsettingobj._timeToRestrictionEMAIL == 14) {
                    _oldtimeToRestrictionEMAIL = "2PM";
                } else if (goldObj.emailsettingobj._timeToRestrictionEMAIL == 15) {
                    _oldtimeToRestrictionEMAIL = "3PM";
                } else if (goldObj.emailsettingobj._timeToRestrictionEMAIL == 16) {
                    _oldtimeToRestrictionEMAIL = "4PM";
                } else if (goldObj.emailsettingobj._timeToRestrictionEMAIL == 17) {
                    _oldtimeToRestrictionEMAIL = "5PM";
                } else if (goldObj.emailsettingobj._timeToRestrictionEMAIL == 18) {
                    _oldtimeToRestrictionEMAIL = "6PM";
                } else if (goldObj.emailsettingobj._timeToRestrictionEMAIL == 19) {
                    _oldtimeToRestrictionEMAIL = "7PM";
                } else if (goldObj.emailsettingobj._timeToRestrictionEMAIL == 24) {
                    _oldtimeToRestrictionEMAIL = "Any";
                }
                
                emailoldlimit = goldObj.emailsettingobj.emaillimit;
                emailoldthresholdlimit = goldObj.emailsettingobj.thresholdlimit;
                emailoldcost = goldObj.emailsettingobj.emailcost;
                _fastoldPaceEMAIL = goldObj.emailsettingobj._fastPaceEMAIL;
                _hyperoldPaceEMAIL = goldObj.emailsettingobj._hyperPaceEMAIL;
                _normaloldPaceEMAIL = goldObj.emailsettingobj._normalPaceEMAIL;
                _slowoldPaceEMAIL = goldObj.emailsettingobj._slowPaceEMAIL;

//                int duration = goldObj.emailsettingobj.duration;
                int repeat = goldObj.emailsettingobj.repeat;
                int emailoldalertstatus = goldObj.emailsettingobj.emailalertstatus;
                int emailoldlimitstatus = goldObj.emailsettingobj.emaillimit;

                emailoldendate = goldObj.emailsettingobj.emailenddate;

                if (repeat == 1) {
                    stroldrepeat = "1 Month";
                } else if (repeat == 2) {
                    stroldrepeat = "3 Months";
                } else if (repeat == 3) {
                    stroldrepeat = "6 Months";
                }

                if (emailoldalertstatus == 1) {
                    stroldemailalertstatus = "ACTIVE";
                }
                if (emailoldlimitstatus == 1) {
                    stroldemaillimitstatus = "ACTIVE";
                }

            }

            //FAX audit        
//            String stroldfaxduration = "-";
            String stroldfaxrepeat = "In-Active";
            String stroldfaxlimitstatus = "In-Active";
            String stroldfaxalertstatus = "In-Active";
            int faxoldlimit = 0;
            int faxoldthresholdlimit = 0;
            float faxoldcost = 0;
            int _fastoldPaceFAX = 0;
            int _hyperoldPaceFAX = 0;
            int _normaloldPaceFAX = 0;
            int _slowoldPaceFAX = 0;
            
               String _olddayRestrictionFAX = "-";
            String _oldtimeFromRestrictionFAX = "-";
            String _oldtimeToRestrictionFAX = "-";
            String _oldfaxFooter = "-";
              String _oldfaxFooterStatus = "-";

            Date faxoldendate = new Date();

            if (_isettype == FAX && goldObj.faxlimitsettingobj != null) {
                
                
                
                         _oldfaxFooter = goldObj.faxlimitsettingobj._faxFooter;
                if(goldObj.faxlimitsettingobj._faxFooterStatus == 1){
                   _oldfaxFooterStatus = "Active";
                }else if(goldObj.faxlimitsettingobj._faxFooterStatus == 0){
                     _oldfaxFooterStatus = "Suspended";
                }
                if (goldObj.faxlimitsettingobj._dayRestrictionFAX == 1) {
                    _olddayRestrictionFAX = "Week days only";
                } else if (goldObj.faxlimitsettingobj._dayRestrictionFAX == 2) {
                    _olddayRestrictionFAX = "Whole Week (including Weekend)";
                }

                if (goldObj.faxlimitsettingobj._timeFromRestrictionFAX == 7) {
                    _oldtimeFromRestrictionFAX = "7AM";
                } else if (goldObj.faxlimitsettingobj._timeFromRestrictionFAX == 8) {
                    _oldtimeFromRestrictionFAX = "8AM";
                } else if (goldObj.faxlimitsettingobj._timeFromRestrictionFAX == 9) {
                    _oldtimeFromRestrictionFAX = "9AM";
                } else if (goldObj.faxlimitsettingobj._timeFromRestrictionFAX == 10) {
                    _oldtimeFromRestrictionFAX = "10AM";
                } else if (goldObj.faxlimitsettingobj._timeFromRestrictionFAX == 11) {
                    _oldtimeFromRestrictionFAX = "11AM";
                } else if (goldObj.faxlimitsettingobj._timeFromRestrictionFAX == 0) {
                    _oldtimeFromRestrictionFAX = "Any";
                }

                if (goldObj.faxlimitsettingobj._timeToRestrictionFAX == 12) {
                    _oldtimeToRestrictionFAX = "12PM";
                } else if (goldObj.faxlimitsettingobj._timeToRestrictionFAX== 13) {
                    _oldtimeToRestrictionFAX = "1PM";
                } else if (goldObj.faxlimitsettingobj._timeToRestrictionFAX == 14) {
                    _oldtimeToRestrictionFAX = "2PM";
                } else if (goldObj.faxlimitsettingobj._timeToRestrictionFAX == 15) {
                    _oldtimeToRestrictionFAX = "3PM";
                } else if (goldObj.faxlimitsettingobj._timeToRestrictionFAX == 16) {
                    _oldtimeToRestrictionFAX = "4PM";
                } else if (goldObj.faxlimitsettingobj._timeToRestrictionFAX == 17) {
                    _oldtimeToRestrictionFAX = "5PM";
                } else if (goldObj.faxlimitsettingobj._timeToRestrictionFAX == 18) {
                    _oldtimeToRestrictionFAX = "6PM";
                } else if (goldObj.faxlimitsettingobj._timeToRestrictionFAX == 19) {
                    _oldtimeToRestrictionFAX = "7PM";
                } else if (goldObj.faxlimitsettingobj._timeToRestrictionFAX == 24) {
                    _oldtimeToRestrictionFAX = "Any";
                }
                
                faxoldlimit = goldObj.faxlimitsettingobj.faxlimit;
                faxoldthresholdlimit = goldObj.faxlimitsettingobj.thresholdlimit;
                faxoldcost = goldObj.faxlimitsettingobj.faxcost;
                _fastoldPaceFAX = goldObj.faxlimitsettingobj._fastPaceFAX;
                _hyperoldPaceFAX = goldObj.faxlimitsettingobj._hyperPaceFAX;
                _normaloldPaceFAX = goldObj.faxlimitsettingobj._normalPaceFAX;
                _slowoldPaceFAX = goldObj.faxlimitsettingobj._slowPaceFAX;

//                int duration = goldObj.faxlimitsettingobj.duration;
                int repeat = goldObj.faxlimitsettingobj.repeat;
                int faxoldalertstatus = goldObj.faxlimitsettingobj.faxalertstatus;
                int faxoldlimitstatus = goldObj.faxlimitsettingobj.faxlimit;

                faxoldendate = goldObj.faxlimitsettingobj.faxenddate;
                if (repeat == 1) {
                    stroldrepeat = "1 Month";
                } else if (repeat == 2) {
                    stroldrepeat = "3 Months";
                } else if (repeat == 3) {
                    stroldrepeat = "6 Months";
                }

                if (faxoldalertstatus == 1) {
                    stroldfaxalertstatus = "ACTIVE";
                }
                if (faxoldlimitstatus == 1) {
                    stroldfaxlimitstatus = "ACTIVE";
                }

            }

            //PUSH audit        
//            String stroldpushduration = "-";
            String stroldpushrepeat = "In-Active";
            String stroldpushlimitstatus = "In-Active";
            String stroldpushalertstatus = "In-Active";
            int pusholdlimit = 0;
            int pusholdthresholdlimit = 0;
            float pusholdcost = 0;
            int _fastoldPacePUSH = 0;
            int _hyperoldPacePUSH = 0;
            int _normaloldPacePUSH = 0;
            int _slowoldPacePUSH = 0;
            
                String _olddayRestrictionPUSH = "-";
            String _oldtimeFromRestrictionPUSH = "-";
            String _oldtimeToRestrictionPUSH = "-";
            String _oldpushFooter = "-";
              String _oldpushFooterStatus = "-";

            Date pusholdendate = new Date();

            if (_isettype == PUSH && goldObj.pushsettingobj != null) {
                
               _oldpushFooter = goldObj.pushsettingobj._pushFooter;
                if(goldObj.pushsettingobj._pushFooterStatus == 1){
                   _oldpushFooterStatus = "Active";
                }else if(goldObj.pushsettingobj._pushFooterStatus == 0){
                     _oldpushFooterStatus = "Suspended";
                }
                if (goldObj.pushsettingobj._dayRestrictionPUSH == 1) {
                    _olddayRestrictionPUSH = "Week days only";
                } else if (goldObj.pushsettingobj._dayRestrictionPUSH == 2) {
                    _olddayRestrictionPUSH = "Whole Week (including Weekend)";
                }

                if (goldObj.pushsettingobj._timeFromRestrictionPUSH == 7) {
                    _oldtimeFromRestrictionPUSH = "7AM";
                } else if (goldObj.pushsettingobj._timeFromRestrictionPUSH == 8) {
                    _oldtimeFromRestrictionPUSH = "8AM";
                } else if (goldObj.pushsettingobj._timeFromRestrictionPUSH == 9) {
                    _oldtimeFromRestrictionPUSH = "9AM";
                } else if (goldObj.pushsettingobj._timeFromRestrictionPUSH == 10) {
                    _oldtimeFromRestrictionPUSH = "10AM";
                } else if (goldObj.pushsettingobj._timeFromRestrictionPUSH == 11) {
                    _oldtimeFromRestrictionPUSH = "11AM";
                } else if (goldObj.pushsettingobj._timeFromRestrictionPUSH == 0) {
                    _oldtimeFromRestrictionPUSH = "Any";
                }

                if (goldObj.pushsettingobj._timeToRestrictionPUSH == 12) {
                    _oldtimeToRestrictionPUSH = "12PM";
                } else if (goldObj.pushsettingobj._timeToRestrictionPUSH== 13) {
                    _oldtimeToRestrictionPUSH = "1PM";
                } else if (goldObj.pushsettingobj._timeToRestrictionPUSH == 14) {
                    _oldtimeToRestrictionPUSH = "2PM";
                } else if (goldObj.pushsettingobj._timeToRestrictionPUSH == 15) {
                    _oldtimeToRestrictionPUSH = "3PM";
                } else if (goldObj.pushsettingobj._timeToRestrictionPUSH == 16) {
                    _oldtimeToRestrictionPUSH = "4PM";
                } else if (goldObj.pushsettingobj._timeToRestrictionPUSH == 17) {
                    _oldtimeToRestrictionPUSH = "5PM";
                } else if (goldObj.pushsettingobj._timeToRestrictionPUSH == 18) {
                    _oldtimeToRestrictionPUSH = "6PM";
                } else if (goldObj.pushsettingobj._timeToRestrictionPUSH == 19) {
                    _oldtimeToRestrictionPUSH = "7PM";
                } else if (goldObj.pushsettingobj._timeToRestrictionPUSH == 24) {
                    _oldtimeToRestrictionPUSH = "Any";
                }
                
                
                pusholdlimit = goldObj.pushsettingobj.pushlimit;
                pusholdthresholdlimit = goldObj.pushsettingobj.thresholdlimit;
                pusholdcost = goldObj.pushsettingobj.pushcost;
                _fastoldPacePUSH = goldObj.pushsettingobj._fastPacePUSH;
                _hyperoldPacePUSH = goldObj.pushsettingobj._hyperPacePUSH;
                _normaloldPacePUSH = goldObj.pushsettingobj._normalPacePUSH;
                _slowoldPacePUSH = goldObj.pushsettingobj._slowPacePUSH;

//                int duration = goldObj.pushsettingobj.duration;
                int repeat = goldObj.pushsettingobj.repeat;
                int pusholdalertstatus = goldObj.pushsettingobj.pushalertstatus;
                int pusholdlimitstatus = goldObj.pushsettingobj.pushlimit;
                pusholdendate = goldObj.pushsettingobj.pushenddate;
                if (repeat == 1) {
                    stroldrepeat = "1 Month";
                } else if (repeat == 2) {
                    stroldrepeat = "3 Months";
                } else if (repeat == 3) {
                    stroldrepeat = "6 Months";
                }

                if (pusholdalertstatus == 1) {
                    stroldpushalertstatus = "ACTIVE";
                }
                if (pusholdlimitstatus == 1) {
                    stroldpushlimitstatus = "ACTIVE";
                }

            }

            //new
            
             
                String struseralertstatus = "INACTIVE";
//            if (_isettype == USER_ALERT) {
//               
//                int iStatus = globalObj.alertuser;
//                if (iStatus == 0) {
//                    struseralertstatus = "ACTIVE";
//                }else{
//                     strolduseralertstatus = "In-ACTIVE";
//                }
//               
//            }
            String strContent = null;
            String strContentStatus = "INACTIVE";
            String strcontentalertstatus = "INACTIVE";
            if (_isettype == CONTENT_FILTER) {
                String[] arrCotent = globalObj.content;
                for (int i = 0; i < arrCotent.length; i++) {
                    strContent += arrCotent[i] + ",";
                }
                int iStatus = globalObj.contentstatus;
                if (iStatus == 0) {
                    strContentStatus = "ACTIVE";
                }
                int iopalertstatus = globalObj.contentalertstatus;
                if (iopalertstatus == 0) {
                    strContentStatus = "ACTIVE";
                }
            }
            String strIp = null;
            String strIpStatus = "INACTIVE";
            String stripalertstatus = "INACTIVE";
            if (_isettype == IP_FILTER) {
                String[] arrIp = globalObj.IP;
                for (int i = 0; i < arrIp.length; i++) {
                    strIp += arrIp[i] + ",";
                }
                int iStatus = globalObj.ipstatus;
                strIpStatus = "INACTIVE";
                if (iStatus == 0) {
                    strIpStatus = "ACTIVE";
                }
                int iopalertstatus = globalObj.ipalertstatus;
                if (iopalertstatus == 0) {
                    stripalertstatus = "ACTIVE";
                }
            }

            String strPrefix = null;
            String strPrefixStatus = "INACTIVE";
            String strPrefixalertstatus = "INACTIVE";
            if (_isettype == PREFIX_FILTER) {
                String[] arrPrefix = globalObj.prefix;
                for (int i = 0; i < arrPrefix.length; i++) {
                    strPrefix += arrPrefix[i] + ",";
                }
                int iStatus = globalObj.prefixstatus;
                strPrefixStatus = "INACTIVE";
                if (iStatus == 0) {
                    strPrefixStatus = "ACTIVE";
                }
                int iPrefixalertstatus = globalObj.prefixalertstatus;
                if (iPrefixalertstatus == 0) {
                    strPrefixalertstatus = "ACTIVE";
                }
            }
            String strDomain = null;
            String strDomainStatus = "INACTIVE";
            String strDomainalertstatus = "INACTIVE";
            if (_isettype == DOMAIN_FILTER) {
                String[] arrDomain = globalObj.domain;
                for (int i = 0; i < arrDomain.length; i++) {
                    strPrefix += arrDomain[i] + ",";
                }
                int iStatus = globalObj.domainstatus;
                strDomainStatus = "INACTIVE";
                if (iStatus == 0) {
                    strDomainStatus = "ACTIVE";
                }
                int iDomainalertstatus = globalObj.domainalertstatus;
                if (iDomainalertstatus == 0) {
                    strDomainalertstatus = "ACTIVE";
                }
            }
//sms audit
//            String strduration = "-";
            String strrepeat = "In-Active";
            String strsmslimitstatus = "In-Active";
            String strsmsalertstatus = "In-Active";
            int smslimit = 0;
            int smsthresholdlimit = 0;
            float smscost = 0;
            int _fastPaceSMS = 0;
            int _hyperPaceSMS = 0;
            int _normalPaceSMS = 0;
            int _slowPaceSMS = 0;
            
                  String _dayRestrictionSMS = "-";
            String _timeFromRestrictionSMS = "-";
            String _timeToRestrictionSMS = "-";
            String _smsFooter = "-";
              String _smsFooterStatus = "-";
              

            Date smsendate = new Date();
            if (_isettype == SMS) {
                
                        _smsFooter = globalObj.smssettingobj._smsFooter;
                if(globalObj.smssettingobj._smsFooterStatus == 1){
                   _smsFooterStatus = "Active";
                }else if(globalObj.smssettingobj._smsFooterStatus == 0){
                     _smsFooterStatus = "Suspended";
                }
                if (globalObj.smssettingobj._dayRestrictionSMS == 1) {
                    _dayRestrictionSMS = "Week days only";
                } else if (globalObj.smssettingobj._dayRestrictionSMS == 2) {
                    _dayRestrictionSMS = "Whole Week (including Weekend)";
                }

                if (globalObj.smssettingobj._timeFromRestrictionSMS == 7) {
                    _timeFromRestrictionSMS = "7AM";
                } else if (globalObj.smssettingobj._timeFromRestrictionSMS == 8) {
                    _timeFromRestrictionSMS = "8AM";
                } else if (globalObj.smssettingobj._timeFromRestrictionSMS == 9) {
                    _timeFromRestrictionSMS = "9AM";
                } else if (globalObj.smssettingobj._timeFromRestrictionSMS == 10) {
                    _timeFromRestrictionSMS = "10AM";
                } else if (globalObj.smssettingobj._timeFromRestrictionSMS == 11) {
                    _timeFromRestrictionSMS = "11AM";
                } else if (globalObj.smssettingobj._timeFromRestrictionSMS == 0) {
                    _timeFromRestrictionSMS = "Any";
                }

                if (globalObj.smssettingobj._timeToRestrictionSMS == 12) {
                    _timeToRestrictionSMS = "12PM";
                } else if (globalObj.smssettingobj._timeToRestrictionSMS == 13) {
                    _timeToRestrictionSMS = "1PM";
                } else if (globalObj.smssettingobj._timeToRestrictionSMS == 14) {
                    _timeToRestrictionSMS = "2PM";
                } else if (globalObj.smssettingobj._timeToRestrictionSMS == 15) {
                    _timeToRestrictionSMS = "3PM";
                } else if (globalObj.smssettingobj._timeToRestrictionSMS == 16) {
                    _timeToRestrictionSMS = "4PM";
                } else if (globalObj.smssettingobj._timeToRestrictionSMS == 17) {
                    _timeToRestrictionSMS = "5PM";
                } else if (globalObj.smssettingobj._timeToRestrictionSMS == 18) {
                    _timeToRestrictionSMS = "6PM";
                } else if (globalObj.smssettingobj._timeToRestrictionSMS == 19) {
                    _timeToRestrictionSMS = "7PM";
                } else if (globalObj.smssettingobj._timeToRestrictionSMS == 24) {
                    _timeToRestrictionSMS = "Any";
                }

                
                smslimit = globalObj.smssettingobj.smslimit;
                smsthresholdlimit = globalObj.smssettingobj.thresholdlimit;
                smscost = globalObj.smssettingobj.smscost;

                _fastPaceSMS = globalObj.smssettingobj._fastPaceSMS;
                _hyperPaceSMS = globalObj.smssettingobj._hyperPaceSMS;
                _normalPaceSMS = globalObj.smssettingobj._normalPaceSMS;
                _slowPaceSMS = globalObj.smssettingobj._slowPaceSMS;

//                int duration = globalObj.smssettingobj.duration;
                int repeat = globalObj.smssettingobj.repeat;
                int smsalertstatus = globalObj.smssettingobj.smsalertstatus;
                int smslimitstatus = globalObj.smssettingobj.smslimitstatus;

                smsendate = globalObj.smssettingobj.smsenddate;
                if (repeat == 1) {
                    strrepeat = "1 Month";
                } else if (repeat == 2) {
                    strrepeat = "3 Months";
                } else if (repeat == 3) {
                    strrepeat = "6 Months";
                }

                if (smsalertstatus == 1) {
                    strsmsalertstatus = "ACTIVE";
                }
                if (smslimitstatus == 1) {
                    strsmslimitstatus = "ACTIVE";
                }

            }
            //voice audit        
//            String strvoiceduration = "-";
            String strvoicerepeat = "In-Active";
            String strvoicelimitstatus = "In-Active";
            String strvoicealertstatus = "In-Active";
            int voicelimit = 0;
            int voicethresholdlimit = 0;
            float voicecost = 0;
            int _fastPaceVOICE = 0;
            int _hyperPaceVOICE = 0;
            int _normalPaceVOICE = 0;
            int _slowPaceVOICE = 0;
            
               String _dayRestrictionVOICE = "-";
            String _timeFromRestrictionVOICE = "-";
            String _timeToRestrictionVOICE = "-";
            String _voiceFooter = "-";
            String _voiceFooterStatus = "-";

            Date voiceendate = new Date();

            if (_isettype == VOICE) {
                
                       _voiceFooter = globalObj.voicesettingobj._voiceFooter;
                if(globalObj.voicesettingobj._voiceFooterStatus == 1){
                   _smsFooterStatus = "Active";
                }else if(globalObj.voicesettingobj._voiceFooterStatus == 0){
                     _voiceFooterStatus = "Suspended";
                }
                if (globalObj.voicesettingobj._dayRestrictionVOICE == 1) {
                    _dayRestrictionVOICE = "Week days only";
                } else if (globalObj.voicesettingobj._dayRestrictionVOICE == 2) {
                    _dayRestrictionVOICE = "Whole Week (including Weekend)";
                }

                if (globalObj.voicesettingobj._timeFromRestrictionVOICE == 7) {
                    _timeFromRestrictionVOICE = "7AM";
                } else if (globalObj.voicesettingobj._timeFromRestrictionVOICE == 8) {
                    _timeFromRestrictionVOICE = "8AM";
                } else if (globalObj.voicesettingobj._timeFromRestrictionVOICE == 9) {
                    _timeFromRestrictionVOICE = "9AM";
                } else if (globalObj.voicesettingobj._timeFromRestrictionVOICE == 10) {
                    _timeFromRestrictionVOICE = "10AM";
                } else if (globalObj.voicesettingobj._timeFromRestrictionVOICE == 11) {
                    _timeFromRestrictionVOICE = "11AM";
                } else if (globalObj.voicesettingobj._timeFromRestrictionVOICE == 0) {
                    _timeFromRestrictionVOICE = "Any";
                }

                if (globalObj.voicesettingobj._timeToRestrictionVOICE == 12) {
                    _timeToRestrictionVOICE = "12PM";
                } else if (globalObj.voicesettingobj._timeToRestrictionVOICE == 13) {
                    _timeToRestrictionVOICE = "1PM";
                } else if (globalObj.voicesettingobj._timeToRestrictionVOICE == 14) {
                    _timeToRestrictionVOICE = "2PM";
                } else if (globalObj.voicesettingobj._timeToRestrictionVOICE == 15) {
                    _timeToRestrictionVOICE = "3PM";
                } else if (globalObj.voicesettingobj._timeToRestrictionVOICE == 16) {
                    _timeToRestrictionVOICE = "4PM";
                } else if (globalObj.voicesettingobj._timeToRestrictionVOICE == 17) {
                    _timeToRestrictionVOICE = "5PM";
                } else if (globalObj.voicesettingobj._timeToRestrictionVOICE == 18) {
                    _timeToRestrictionVOICE = "6PM";
                } else if (globalObj.voicesettingobj._timeToRestrictionVOICE == 19) {
                    _timeToRestrictionVOICE = "7PM";
                } else if (globalObj.voicesettingobj._timeToRestrictionVOICE == 24) {
                    _timeToRestrictionVOICE = "Any";
                }

                voicelimit = globalObj.voicesettingobj.voicemsglimit;
                voicethresholdlimit = globalObj.voicesettingobj.thresholdlimit;
                voicecost = globalObj.voicesettingobj.voicemsgcost;
                _fastPaceVOICE = globalObj.voicesettingobj._fastPaceVOICE;
                _hyperPaceVOICE = globalObj.voicesettingobj._hyperPaceVOICE;
                _normalPaceVOICE = globalObj.voicesettingobj._normalPaceVOICE;
                _slowPaceVOICE = globalObj.voicesettingobj._slowPaceVOICE;

//                int duration = globalObj.voicesettingobj.duration;
                int repeat = globalObj.voicesettingobj.repeat;
                int voicealertstatus = globalObj.voicesettingobj.voicemsgalertstatus;
                int voicelimitstatus = globalObj.voicesettingobj.voicemsglimit;

                voiceendate = globalObj.voicesettingobj.voiceenddate;
                if (repeat == 1) {
                    strrepeat = "1 Month";
                } else if (repeat == 2) {
                    strrepeat = "3 Months";
                } else if (repeat == 3) {
                    strrepeat = "6 Months";
                }

                if (voicealertstatus == 1) {
                    strvoicealertstatus = "ACTIVE";
                }
                if (voicelimitstatus == 1) {
                    strvoicelimitstatus = "ACTIVE";
                }

            }

            //ussd audit        
//            String strussdduration = "-";
            String strussdrepeat = "In-Active";
            String strussdlimitstatus = "In-Active";
            String strussdalertstatus = "In-Active";
            int ussdlimit = 0;
            int ussdthresholdlimit = 0;
            float ussdcost = 0;
            int _fastPaceUSSD = 0;
            int _hyperPaceUSSD = 0;
            int _normalPaceUSSD = 0;
            int _slowPaceUSSD = 0;
             String _dayRestrictionUSSD = "-";
            String _timeFromRestrictionUSSD = "-";
            String _timeToRestrictionUSSD = "-";
             String _ussdFooter = "-";
              String _ussdFooterStatus = "-";
            

            Date ussdendate = new Date();

            if (_isettype == USSD) {
                    if(globalObj.ussdsettingobj._ussdFooterStatus == 1){
                   _ussdFooterStatus = "Active";
                }else if(globalObj.ussdsettingobj._ussdFooterStatus == 0){
                     _ussdFooterStatus = "Suspended";
                }
                 if (globalObj.ussdsettingobj._dayRestrictionUSSD == 1) {
                    _dayRestrictionUSSD = "Week days only";
                } else if (globalObj.ussdsettingobj._dayRestrictionUSSD == 2) {
                    _dayRestrictionUSSD = "Whole Week (including Weekend)";
                }

                if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 7) {
                    _timeFromRestrictionUSSD = "7AM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 8) {
                    _timeFromRestrictionUSSD = "8AM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 9) {
                    _timeFromRestrictionUSSD = "9AM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 10) {
                    _timeFromRestrictionUSSD = "10AM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 11) {
                    _timeFromRestrictionUSSD = "11AM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 0) {
                    _timeFromRestrictionUSSD = "Any";
                }

                if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 12) {
                    _timeToRestrictionUSSD = "12PM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 13) {
                    _timeToRestrictionUSSD = "1PM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 14) {
                    _timeToRestrictionUSSD = "2PM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 15) {
                    _timeToRestrictionUSSD = "3PM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 16) {
                    _timeToRestrictionUSSD = "4PM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 17) {
                    _timeToRestrictionUSSD = "5PM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 18) {
                    _timeToRestrictionUSSD = "6PM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 19) {
                    _timeToRestrictionUSSD = "7PM";
                } else if (globalObj.ussdsettingobj._timeFromRestrictionUSSD == 24) {
                    _timeToRestrictionUSSD = "Any";
                }
                ussdlimit = globalObj.ussdsettingobj.ussdlimit;
                ussdthresholdlimit = globalObj.ussdsettingobj.thresholdlimit;
                ussdcost = globalObj.ussdsettingobj.ussdcost;
                _fastPaceUSSD = globalObj.ussdsettingobj._fastPaceUSSD;
                _hyperPaceUSSD = globalObj.ussdsettingobj._hyperPaceUSSD;
                _normalPaceUSSD = globalObj.ussdsettingobj._normalPaceUSSD;
                _slowPaceUSSD = globalObj.ussdsettingobj._slowPaceUSSD;

//                int duration = globalObj.ussdsettingobj.duration;
                int repeat = globalObj.ussdsettingobj.repeat;
                int ussdalertstatus = globalObj.ussdsettingobj.ussdalertstatus;
                int ussdlimitstatus = globalObj.ussdsettingobj.ussdlimit;

                ussdendate = globalObj.ussdsettingobj.ussdenddate;

                if (repeat == 1) {
                    strrepeat = "1 Month";
                } else if (repeat == 2) {
                    strrepeat = "3 Months";
                } else if (repeat == 3) {
                    strrepeat = "6 Months";
                }

                if (ussdalertstatus == 1) {
                    strussdalertstatus = "ACTIVE";
                }
                if (ussdlimitstatus == 1) {
                    strussdlimitstatus = "ACTIVE";
                }

            }

            //EMAIL audit        
//            String stremailduration = "-";
            String stremailrepeat = "In-Active";
            String stremaillimitstatus = "In-Active";
            String stremailalertstatus = "In-Active";
            int emaillimit = 0;
            int emailthresholdlimit = 0;
            float emailcost = 0;
            int _fastPaceEMAIL = 0;
            int _hyperPaceEMAIL = 0;
            int _normalPaceEMAIL = 0;
            int _slowPaceEMAIL = 0;

              
            String _dayRestrictionEMAIL = "-";
            String _timeFromRestrictionEMAIL = "-";
            String _timeToRestrictionEMAIL = "-";
             String _emailFooter = "-";
              String _emailFooterStatus = "-";
            Date emailendate = new Date();

            if (_isettype == EMAIL) {
                
                   if(globalObj.emailsettingobj._emailFooterStatus == 1){
                   _emailFooterStatus = "Active";
                }else if(globalObj.emailsettingobj._emailFooterStatus == 0){
                     _emailFooterStatus = "Suspended";
                }
                
                  if (globalObj.emailsettingobj._dayRestrictionEMAIL == 1) {
                    _dayRestrictionEMAIL = "Week days only";
                } else if (globalObj.emailsettingobj._dayRestrictionEMAIL == 2) {
                    _dayRestrictionEMAIL = "Whole Week (including Weekend)";
                }

                if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 7) {
                    _timeFromRestrictionEMAIL = "7AM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 8) {
                    _timeFromRestrictionEMAIL = "8AM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 9) {
                    _timeFromRestrictionEMAIL = "9AM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 10) {
                    _timeFromRestrictionEMAIL = "10AM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 11) {
                    _timeFromRestrictionEMAIL = "11AM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 0) {
                    _timeFromRestrictionEMAIL = "Any";
                }

                if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 12) {
                    _timeToRestrictionEMAIL = "12PM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 13) {
                    _timeToRestrictionEMAIL = "1PM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 14) {
                    _timeToRestrictionEMAIL = "2PM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 15) {
                    _timeToRestrictionEMAIL = "3PM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 16) {
                    _timeToRestrictionEMAIL = "4PM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 17) {
                    _timeToRestrictionEMAIL = "5PM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 18) {
                    _timeToRestrictionEMAIL = "6PM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 19) {
                    _timeToRestrictionEMAIL = "7PM";
                } else if (globalObj.emailsettingobj._timeFromRestrictionEMAIL == 24) {
                    _timeToRestrictionEMAIL = "Any";
                }

                emaillimit = globalObj.emailsettingobj.emaillimit;
                emailthresholdlimit = globalObj.emailsettingobj.thresholdlimit;
                emailcost = globalObj.emailsettingobj.emailcost;
                _fastPaceEMAIL = globalObj.emailsettingobj._fastPaceEMAIL;
                _hyperPaceEMAIL = globalObj.emailsettingobj._hyperPaceEMAIL;
                _normalPaceEMAIL = globalObj.emailsettingobj._normalPaceEMAIL;
                _slowPaceEMAIL = globalObj.emailsettingobj._slowPaceEMAIL;

//                int duration = globalObj.emailsettingobj.duration;
                int repeat = globalObj.emailsettingobj.repeat;
                int emailalertstatus = globalObj.emailsettingobj.emailalertstatus;
                int emaillimitstatus = globalObj.emailsettingobj.emaillimit;

                emailendate = globalObj.emailsettingobj.emailenddate;

                if (repeat == 1) {
                    strrepeat = "1 Month";
                } else if (repeat == 2) {
                    strrepeat = "3 Months";
                } else if (repeat == 3) {
                    strrepeat = "6 Months";
                }

                if (emailalertstatus == 1) {
                    stremailalertstatus = "ACTIVE";
                }
                if (emaillimitstatus == 1) {
                    stremaillimitstatus = "ACTIVE";
                }

            }

            //FAX audit        
//            String strfaxduration = "-";
            String strfaxrepeat = "In-Active";
            String strfaxlimitstatus = "In-Active";
            String strfaxalertstatus = "In-Active";
            int faxlimit = 0;
            int faxthresholdlimit = 0;
            float faxcost = 0;
            int _fastPaceFAX = 0;
            int _hyperPaceFAX = 0;
            int _normalPaceFAX = 0;
            int _slowPaceFAX = 0;
    String _dayRestrictionFAX = "-";
            String _timeFromRestrictionFAX = "-";
            String _timeToRestrictionFAX = "-";
            String _faxFooter = "-";
            String _faxFooterStatus = "-";
            Date faxendate = new Date();

            if (_isettype == FAX) {
                
                   if(globalObj.faxlimitsettingobj._faxFooterStatus == 1){
                   _faxFooterStatus = "Active";
                }else if(globalObj.faxlimitsettingobj._faxFooterStatus == 0){
                     _faxFooterStatus = "Suspended";
                }
                
                   if (globalObj.faxlimitsettingobj._dayRestrictionFAX == 1) {
                    _dayRestrictionFAX = "Week days only";
                } else if (globalObj.faxlimitsettingobj._dayRestrictionFAX == 2) {
                    _dayRestrictionFAX = "Whole Week (including Weekend)";
                }

                if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 7) {
                    _timeFromRestrictionFAX = "7AM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 8) {
                    _timeFromRestrictionFAX = "8AM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 9) {
                    _timeFromRestrictionFAX = "9AM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 10) {
                    _timeFromRestrictionFAX = "10AM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 11) {
                    _timeFromRestrictionFAX = "11AM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 0) {
                    _timeFromRestrictionFAX = "Any";
                }

                if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 12) {
                    _timeToRestrictionFAX = "12PM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 13) {
                    _timeToRestrictionFAX = "1PM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 14) {
                    _timeToRestrictionFAX = "2PM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 15) {
                    _timeToRestrictionFAX = "3PM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 16) {
                    _timeToRestrictionFAX = "4PM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 17) {
                    _timeToRestrictionFAX = "5PM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 18) {
                    _timeToRestrictionFAX = "6PM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 19) {
                    _timeToRestrictionFAX = "7PM";
                } else if (globalObj.faxlimitsettingobj._timeFromRestrictionFAX == 24) {
                    _timeToRestrictionFAX = "Any";
                }

                faxlimit = globalObj.faxlimitsettingobj.faxlimit;
                faxthresholdlimit = globalObj.faxlimitsettingobj.thresholdlimit;
                faxcost = globalObj.faxlimitsettingobj.faxcost;
                _fastPaceFAX = globalObj.faxlimitsettingobj._fastPaceFAX;
                _hyperPaceFAX = globalObj.faxlimitsettingobj._hyperPaceFAX;
                _normalPaceFAX = globalObj.faxlimitsettingobj._normalPaceFAX;
                _slowPaceFAX = globalObj.faxlimitsettingobj._slowPaceFAX;

//                int duration = globalObj.faxlimitsettingobj.duration;
                int repeat = globalObj.faxlimitsettingobj.repeat;
                int faxalertstatus = globalObj.faxlimitsettingobj.faxalertstatus;
                int faxlimitstatus = globalObj.faxlimitsettingobj.faxlimit;

                faxendate = globalObj.faxlimitsettingobj.faxenddate;

                if (repeat == 1) {
                    strrepeat = "1 Month";
                } else if (repeat == 2) {
                    strrepeat = "3 Months";
                } else if (repeat == 3) {
                    strrepeat = "6 Months";
                }

                if (faxalertstatus == 1) {
                    strfaxalertstatus = "ACTIVE";
                }
                if (faxlimitstatus == 1) {
                    strfaxlimitstatus = "ACTIVE";
                }

            }

            //PUSH audit        
//            String strpushduration = "-";
            String strpushrepeat = "In-Active";
            String strpushlimitstatus = "In-Active";
            String strpushalertstatus = "In-Active";
            int pushlimit = 0;
            int pushthresholdlimit = 0;
            float pushcost = 0;
            int _fastPacePUSH = 0;
            int _hyperPacePUSH = 0;
            int _normalPacePUSH = 0;
            int _slowPacePUSH = 0;
            
              String _dayRestrictionPUSH = "-";
            String _timeFromRestrictionPUSH = "-";
            String _timeToRestrictionPUSH = "-";
             String _pushFooter = "-";
              String _pushFooterStatus = "-";
              int newCRPercentage = globalObj.weightageForCR;

            Date pushendate = new Date();

            if (_isettype == PUSH) {
                
                
                  if(globalObj.pushsettingobj._pushFooterStatus == 1){
                   _pushFooterStatus = "Active";
                }else if(globalObj.pushsettingobj._pushFooterStatus == 0){
                     _pushFooterStatus = "Suspended";
                }
                
                  if (globalObj.pushsettingobj._dayRestrictionPUSH == 1) {
                    _dayRestrictionPUSH = "Week days only";
                } else if (globalObj.pushsettingobj._dayRestrictionPUSH == 2) {
                    _dayRestrictionPUSH = "Whole Week (including Weekend)";
                }

                if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 7) {
                    _timeFromRestrictionPUSH = "7AM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 8) {
                    _timeFromRestrictionPUSH = "8AM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 9) {
                    _timeFromRestrictionPUSH = "9AM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 10) {
                    _timeFromRestrictionPUSH = "10AM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 11) {
                    _timeFromRestrictionPUSH = "11AM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 0) {
                    _timeFromRestrictionPUSH = "Any";
                }

                if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 12) {
                    _timeToRestrictionPUSH = "12PM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 13) {
                    _timeToRestrictionPUSH = "1PM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 14) {
                    _timeToRestrictionPUSH = "2PM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 15) {
                    _timeToRestrictionPUSH = "3PM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 16) {
                    _timeToRestrictionPUSH = "4PM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 17) {
                    _timeToRestrictionPUSH = "5PM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 18) {
                    _timeToRestrictionPUSH = "6PM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 19) {
                    _timeToRestrictionPUSH = "7PM";
                } else if (globalObj.pushsettingobj._timeFromRestrictionPUSH == 24) {
                    _timeToRestrictionPUSH = "Any";
                }
                pushlimit = globalObj.pushsettingobj.pushlimit;
                pushthresholdlimit = globalObj.pushsettingobj.thresholdlimit;
                pushcost = globalObj.pushsettingobj.pushcost;
                _fastPacePUSH = globalObj.pushsettingobj._fastPacePUSH;
                _hyperPacePUSH = globalObj.pushsettingobj._hyperPacePUSH;
                _normalPacePUSH = globalObj.pushsettingobj._normalPacePUSH;
                _slowPacePUSH = globalObj.pushsettingobj._slowPacePUSH;

//                int duration = globalObj.pushsettingobj.duration;
                int repeat = globalObj.pushsettingobj.repeat;
                int pushalertstatus = globalObj.pushsettingobj.pushalertstatus;
                int pushlimitstatus = globalObj.pushsettingobj.pushlimit;

                pushendate = globalObj.pushsettingobj.pushenddate;

                if (repeat == 1) {
                    strrepeat = "1 Month";
                } else if (repeat == 2) {
                    strrepeat = "3 Months";
                } else if (repeat == 3) {
                    strrepeat = "6 Months";
                }

                if (pushalertstatus == 1) {
                    strpushalertstatus = "ACTIVE";
                }
                if (pushlimitstatus == 1) {
                    strpushlimitstatus = "ACTIVE";
                }

            }

            if (retValue == 0) {

//                if (_isettype == USER_ALERT) {
//
//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
//                            request.getRemoteAddr(),
//                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
//                            "Change Global User Alert Setting", resultString, retValue, "Setting Management",
//                            "Old Status =" + strolduseralertstatus ,
//                            "New Status =" + struseralertstatus ,
//                            itemtype, OperatorID);
//                }
                if (_isettype == CONTENT_FILTER) {

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change Content Filter", resultString, retValue, "Setting Management",
                            "Old Content =" + stroldContent + "Content Filter Status =" + stroldContentStatus + "Operator Alert Status =" + stroldcontentalertstatus,
                            "New Content =" + strContent + "Content Filter Status =" + strContentStatus + "Operator Alert Status =" + strcontentalertstatus,
                            itemtype, channel.getChannelid());
                }
                if (_isettype == IP_FILTER) {

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change IP Filter", resultString, retValue, "Setting Management",
                            "OLD IP =" + stroldIp + "IP Filter Status =" + stroldIpStatus + "Operator Alert Status =" + stroldipalertstatus,
                            "New IP =" + strIp + "IP Filter Status =" + strIpStatus + "Operator Alert Status =" + stripalertstatus,
                            itemtype, channel.getChannelid());
                }
                if (_isettype == PREFIX_FILTER) {

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change Prefix Filter", resultString, retValue, "Setting Management",
                            "Old Prefix =" + stroldPrefix + "Prefix Filter Status =" + stroldPrefixStatus + "Operator Alert Status =" + stroldPrefixalertstatus,
                            "New Prefix =" + strPrefix + "Prefix Filter Status =" + strPrefixStatus + "Operator Alert Status =" + strPrefixalertstatus,
                            itemtype, channel.getChannelid());
                }
                if (_isettype == DOMAIN_FILTER) {

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change Domain Filter", resultString, retValue, "Setting Management",
                            "Old Domain =" + stroldDomain + "Domain Filter Status =" + stroldDomainStatus + "Operator Alert Status =" + stroldDomainalertstatus,
                            "New Prefix =" + strDomain + "Domain Filter Status =" + strDomainStatus + "Operator Alert Status =" + strDomainalertstatus,
                            itemtype,channel.getChannelid() );
                }
                if (_isettype == SMS) {

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change SMS Filter", resultString, retValue, "Setting Management",
                            "Limit =" + smsoldlimit + "Cost =" + smsoldcost + "Threshold Limit =" + smsoldthresholdlimit
                            + "Status =" + stroldsmslimitstatus + "Alert Status =" + stroldsmsalertstatus + "Repeat=" + stroldrepeat
                            + "SMS PACK End Date =" + smsoldendate + "Slow Space = " + _slowoldPaceSMS + "Normal Space =" + _normaloldPaceSMS
                            + "Fast Space =" + _fastoldPaceSMS + "Hyper Space =" + _hyperoldPaceSMS
                            +"Day Restriction ="+_olddayRestrictionSMS+"From Time ="+_oldtimeFromRestrictionSMS+"Time To="+_oldtimeToRestrictionSMS
                            +"SMS Footer ="+_oldsmsFooter+"SMS Footer Alert Status ="+_oldsmsFooterStatus
                            ,
                            "Limit =" + smslimit + "Cost =" + smscost + "Threshold Limit =" + smsthresholdlimit
                            + "Status =" + strsmslimitstatus + "Alert Status =" + strsmsalertstatus + "Repeat=" + strrepeat
                            + "SMS PACK End Date =" + smsendate + "Slow Space = " + _slowPaceSMS + "Normal Space =" + _normalPaceSMS
                            + "Fast Space =" + _fastPaceSMS + "Hyper Space =" + _hyperPaceSMS
                            +"Day Restriction ="+_dayRestrictionSMS+"From Time ="+_timeFromRestrictionSMS+"Time To="+_timeToRestrictionSMS
                            +"SMS Footer ="+_smsFooter+"SMS Footer Alert Status ="+_smsFooterStatus,
                            itemtype, channel.getChannelid());
                }
                if (_isettype == VOICE) {

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change VOICE Filter", resultString, retValue, "Setting Management",
                            "Limit =" + voiceoldlimit + "Cost =" + voiceoldcost + "Threshold Limit =" + voiceoldthresholdlimit
                            + "Status =" + stroldvoicelimitstatus + "Alert Status =" + stroldvoicealertstatus + "Repeat=" + stroldvoicerepeat
                            + "VOICE PACK End Date =" + voiceoldendate + "Slow Space = " + _slowoldPaceVOICE + "Normal Space =" + _normaloldPaceVOICE
                            + "Fast Space =" + _fastoldPaceVOICE + "Hyper Space =" + _hyperoldPaceVOICE
                            +"Day Restriction ="+_olddayRestrictionVOICE+"From Time ="+_oldtimeFromRestrictionVOICE+"Time To="+_oldtimeToRestrictionVOICE
                            +"SMS Footer ="+_oldvoiceFooter+"SMS Footer Alert Status ="+_oldvoiceFooterStatus,
                            "Limit =" + voicelimit + "Cost =" + voicecost + "Threshold Limit =" + voicethresholdlimit
                            + "VOICE PACK End Date =" + voiceendate + "Status =" + strvoicelimitstatus + "Alert Status =" + strvoicealertstatus + "Repeat=" + strvoicerepeat
                            + "Slow Space = " + _slowPaceVOICE + "Normal Space =" + _normalPaceVOICE
                            + "Fast Space =" + _fastPaceVOICE + "Hyper Space =" + _hyperPaceVOICE
                            +"Day Restriction ="+_dayRestrictionVOICE+"From Time ="+_timeFromRestrictionVOICE+"Time To="+_timeToRestrictionVOICE
                            +"VOICE Footer ="+_voiceFooter+"VOICE Footer Alert Status ="+_voiceFooterStatus,
                            itemtype, channel.getChannelid());
                }

                if (_isettype == USSD) {

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change USSD Filter", resultString, retValue, "Setting Management",
                            "Limit =" + ussdoldlimit + "Cost =" + ussdoldcost + "Threshold Limit =" + ussdoldthresholdlimit
                            + "Status =" + stroldussdlimitstatus + "Alert Status =" + stroldussdalertstatus + "Repeat=" + stroldussdrepeat
                            + "USSD PACK End Date =" + ussdoldendate + "Slow Space = " + _slowoldPaceUSSD + "Normal Space =" + _normaloldPaceUSSD
                            + "Fast Space =" + _fastoldPaceUSSD + "Hyper Space =" + _hyperoldPaceUSSD
                             +"Day Restriction ="+_olddayRestrictionUSSD+"From Time ="+_oldtimeFromRestrictionUSSD+"Time To="+_oldtimeToRestrictionUSSD
                            +"USSD Footer ="+_oldussdFooter+"USSD Footer Alert Status ="+_oldussdFooterStatus,
                            "Limit =" + ussdlimit + "Cost =" + ussdcost + "Threshold Limit =" + ussdthresholdlimit
                            + "Status =" + strussdlimitstatus + "Alert Status =" + strussdalertstatus + "Repeat=" + strussdrepeat
                            + "USSD PACK End Date =" + ussdendate + "Slow Space = " + _slowPaceUSSD + "Normal Space =" + _normalPaceUSSD
                            + "Fast Space =" + _fastPaceUSSD + "Hyper Space =" + _hyperPaceUSSD
                              +"Day Restriction ="+_dayRestrictionUSSD+"From Time ="+_timeFromRestrictionUSSD+"Time To="+_timeToRestrictionUSSD
                            +"USSD Footer ="+_ussdFooter+"USSD Footer Alert Status ="+_ussdFooterStatus,
                            itemtype, channel.getChannelid());
                }

                if (_isettype == EMAIL) {

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change EMAIL Filter", resultString, retValue, "Setting Management",
                            "Limit =" + emailoldlimit + "Cost =" + emailoldcost + "Threshold Limit =" + emailoldthresholdlimit
                            + "Status =" + stroldemaillimitstatus + "Alert Status =" + stroldemailalertstatus + "Repeat=" + stroldemailrepeat
                            + "EMAIL PACK End Date =" + emailoldendate + "Slow Space = " + _slowoldPaceEMAIL + "Normal Space =" + _normaloldPaceEMAIL
                            + "Fast Space =" + _fastoldPaceEMAIL + "Hyper Space =" + _hyperoldPaceEMAIL
                            +"Day Restriction ="+_olddayRestrictionEMAIL+"From Time ="+_oldtimeFromRestrictionEMAIL+"Time To="+_oldtimeToRestrictionEMAIL
                            +"EMAIL Footer ="+_oldemailFooter+"EMAIL Footer Alert Status ="+_oldemailFooterStatus,
                            "Limit =" + emaillimit + "Cost =" + emailcost + "Threshold Limit =" + emailthresholdlimit
                            + "Status =" + stremaillimitstatus + "Alert Status =" + stremailalertstatus + "Repeat=" + stremailrepeat
                            + "EMAIL PACK End Date =" + emailendate + "Slow Space = " + _slowPaceEMAIL + "Normal Space =" + _normalPaceEMAIL
                            + "Fast Space =" + _fastPaceEMAIL + "Hyper Space =" + _hyperPaceEMAIL
                            +"Day Restriction ="+_dayRestrictionEMAIL+"From Time ="+_timeFromRestrictionEMAIL+"Time To="+_timeToRestrictionEMAIL
                            +"EMAIL Footer ="+_emailFooter+"EMAIL Footer Alert Status ="+_emailFooterStatus,
                            itemtype, channel.getChannelid());
                }
                if (_isettype == FAX) {

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change FAX Filter", resultString, retValue, "Setting Management",
                            "Limit =" + faxoldlimit + "Cost =" + faxoldcost + "Threshold Limit =" + faxoldthresholdlimit
                            + "Status =" + stroldfaxlimitstatus + "Alert Status =" + stroldfaxalertstatus + "Repeat=" + stroldfaxrepeat
                            + "FAX PACK End Date =" + faxoldendate + "Slow Space = " + _slowoldPaceFAX + "Normal Space =" + _normaloldPaceFAX
                            + "Fast Space =" + _fastoldPaceFAX + "Hyper Space =" + _hyperoldPaceFAX
                            +"Day Restriction ="+_olddayRestrictionFAX+"From Time ="+_oldtimeFromRestrictionFAX+"Time To="+_oldtimeToRestrictionFAX
                            +"FAX Footer ="+_oldfaxFooter+"FAX Footer Alert Status ="+_oldfaxFooterStatus,
                            "Limit =" + faxlimit + "Cost =" + faxcost + "Threshold Limit =" + faxthresholdlimit
                            + "Status =" + strfaxlimitstatus + "Alert Status =" + strfaxalertstatus + "Repeat=" + strfaxrepeat
                            + "FAX PACK End Date =" + faxendate + "Slow Space = " + _slowPaceFAX + "Normal Space =" + _normalPaceFAX
                            + "Fast Space =" + _fastPaceFAX + "Hyper Space =" + _hyperPaceFAX
                             +"Day Restriction ="+_dayRestrictionFAX+"From Time ="+_timeFromRestrictionFAX+"Time To="+_timeToRestrictionFAX
                            +"FAX Footer ="+_faxFooter+"FAX Footer Alert Status ="+_faxFooterStatus,
                            itemtype,channel.getChannelid() );
                }
                if (_isettype == PUSH) {

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change PUSH Notification Filter", resultString, retValue, "Setting Management",
                            "Limit =" + pusholdlimit + "Cost =" + pusholdcost + "Threshold Limit =" + pusholdthresholdlimit
                            + "Status =" + stroldpushlimitstatus + "Alert Status =" + stroldpushalertstatus + "Repeat=" + stroldpushrepeat
                            + "PUSH MSG PACK End Date =" + pusholdendate + "Slow Space = " + _slowoldPacePUSH + "Normal Space =" + _normaloldPacePUSH
                            + "Fast Space =" + _fastoldPacePUSH + "Hyper Space =" + _hyperoldPacePUSH
                               +"Day Restriction ="+_olddayRestrictionPUSH+"From Time ="+_oldtimeFromRestrictionPUSH+"Time To="+_oldtimeToRestrictionPUSH
                            +"PUSH Footer ="+_oldpushFooter+"PUSH Footer Alert Status ="+_oldpushFooterStatus,
                            "Limit =" + pushlimit + "Cost =" + pushcost + "Threshold Limit =" + pushthresholdlimit
                            + "Status =" + strpushlimitstatus + "Alert Status =" + strpushalertstatus + "Repeat=" + strpushrepeat
                            + "PUSH MSG PACK End Date =" + pushendate + "Slow Space = " + _slowPacePUSH + "Normal Space =" + _normalPacePUSH
                            + "Fast Space =" + _fastPacePUSH + "Hyper Space =" + _hyperPacePUSH
                             +"Day Restriction ="+_dayRestrictionPUSH+"From Time ="+_timeFromRestrictionPUSH+"Time To="+_timeToRestrictionPUSH
                            +"PUSH Footer ="+_pushFooter+"PUSH Footer Alert Status ="+_pushFooterStatus,
                            itemtype, channel.getChannelid());
                }
                 if (_isettype == CR_SETTINGS) {

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change Challenge Response Filter", resultString, retValue, "Setting Management",
                            "PercenTagesForCR = "+oldCRpercentage,
                            "PercenTagesForCR = "+newCRPercentage,
                            itemtype, channel.getChannelid());
                }
            }

            if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Change Channel Global Settings", resultString, retValue, "Setting Management",
                        "", "Failed To Edit Channel Global Setting",
                        itemtype, channel.getChannelid());
            }

        }

        if (retValue != 0) {
            result = "error";
            //message = "Content Filter Gateway Settings Update Failed!!!";
            message = "Update Failed!!!";
        }

        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.close();
            log.info("end servlet");
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
