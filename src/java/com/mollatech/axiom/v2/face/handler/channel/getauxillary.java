package com.mollatech.axiom.v2.face.handler.channel;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class getauxillary extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(getauxillary.class.getName());

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel ::"+channel.getName());

        int activeSessions = 0;
        int operatorCount = 0;
        int userCount = 0;
        int oTPTokenCount = 0;
        int pKITokenCount = 0;
        int certificateCount = 0;


        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        try {
            ChannelManagement cManagement = new ChannelManagement();
            activeSessions = cManagement.getTotalSessions(channel.getChannelid());
            operatorCount = cManagement.getOperatorCount(channel.getChannelid());
            //userCount = cManagement.getUserCount(channel.getChannelid());

            //to be done later
            //oTPTokenCount = cManagement.getOTPTokenCount(channel.getChannelid());
            //pKITokenCount = cManagement.getPKITokenCount(channel.getChannelid());
            //certificateCount = cManagement.getCertificateCount(channel.getChannelid());

            json.put("_userCount", userCount);
            json.put("_activeSessions", activeSessions);
            json.put("_operatorCount", operatorCount);
            json.put("_oTPTokenCount", oTPTokenCount);
            json.put("_pKITokenCount", pKITokenCount);
            json.put("_certificateCount", certificateCount);

        } catch (Exception ex) {
            log.error("exception caught ::",ex);
            // TODO handle custom exceptions here
            try { 
            json.put("_userCount", userCount);
            json.put("_activeSessions", activeSessions);
            json.put("_operatorCount", operatorCount);
            json.put("_oTPTokenCount", oTPTokenCount);
            json.put("_pKITokenCount", pKITokenCount);
            json.put("_certificateCount", certificateCount);
            }catch(Exception e){ 
                log.error("exception caught ::",e);
            }
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
