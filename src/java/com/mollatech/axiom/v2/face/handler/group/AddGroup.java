/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.group;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Usergroups;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.GroupManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;

/**
 *
 * @author ashish
 */
public class AddGroup extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AddGroup.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private int Status = -1;
    private int enabled = -1;
    private int restrictionLevel = -1;
    AuditManagement audit = new AuditManagement();
    final String itemType = "GROUPS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("is started :: ");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        String operatorId = operatorS.getOperatorid();
        log.debug("operatorId :: "+operatorId);

        String _unitName = request.getParameter("_unitName");
        log.debug("_unitName :: "+_unitName);
        String _unitStatus = request.getParameter("_unitStatus");
        log.debug("v :: "+_unitStatus);
        String _selectedType = request.getParameter("_selectedType");
        log.debug("_selectedType :: "+_selectedType);
//        String _selectedCheck = request.getParameter("_selectedCheck");
        String _CountryName = request.getParameter("_CountryName");
        log.debug("_CountryName :: "+_CountryName);
        String _StateName = request.getParameter("_StateName");
        log.debug("_StateName :: "+_StateName);
        String _cityName = request.getParameter("_cityName");
        log.debug("_cityName :: "+_cityName);
        String _PinCode = request.getParameter("_PinCode");
        log.debug("_PinCode :: "+_PinCode);
        
        if (_unitStatus.equals("1")) {
            Status = 1;
        }
        //  if (_selectedCheck.equals("Allow")) {
        enabled = 1;
       // }

        
        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(_unitName);
        boolean b = m.find();
        Pattern p1 = Pattern.compile("[0-9]", Pattern.CASE_INSENSITIVE);
        Matcher m1 = p1.matcher(_unitName);
        //boolean b1 = m1.find();
        boolean spaces = false;
            

        String _result = "success";
        String _message = "Group Created Successfully!!!";
        Usergroups usergroups = new Usergroups();

        if (_unitName == null) {
            _result = "error";
            _message = "Name could not Empty !!!";
            try {
                json.put("_result", _result);
                json.put("_message", _message);
            } catch (Exception ex) {
                
                log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        } else if (_unitName.isEmpty()) {
            try {
                json.put("_result", "error");
                json.put("_message", "Group Name is mandatory");
                out.print(json);
                out.flush();
                return;
            } catch (Exception e) {
                
                log.error("exception caught :: ",e);
            }
        } else if (StringUtils.isWhitespace(_unitName) == true) {
            _result = "error";
            _message = "Name could not Empty !!!";
            try {
                json.put("_result", _result);
                json.put("_message", _message);
            } catch (Exception ex) {
              
                log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        } else if (_unitName.length() <= 3) {
            _result = "error";
            _message = "Name should be more than 3 characters!!!";
            try {
                json.put("_result", _result);
                json.put("_message", _message);
            } catch (Exception ex) {
               
                log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        } else if (b) {
            _result = "error";
            _message = "Name should not contain any symbols!!!";
            try {
                json.put("_result", _result);
                json.put("_message", _message);
            } catch (Exception ex) {
                
                log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        } 
//        else if (b1) {
//            _result = "error";
//            _message = "Name should not contain only digits!!!";
//            try {
//                json.put("_result", _result);
//                json.put("_message", _message);
//            } catch (Exception ex) {
//                log.error("Exception caught :: ",ex);
//            }
//            out.print(json);
//            out.flush();
//            return;
//        } 
        else if (spaces) {
            _result = "error";
            _message = "Name should not contain any blank spaces!!!";
            try {
                json.put("_result", _result);
                json.put("_message", _message);
            } catch (Exception ex) {
               
                log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        }
         

        usergroups.setCreatedOn(new Date());
        usergroups.setChannelid(channel.getChannelid());
        usergroups.setGroupname(_unitName);
        usergroups.setIsEnabled(enabled);
        usergroups.setLastupOn(new Date());
        usergroups.setStatus(Status);
        if (_selectedType.equals("No Restriction")) {

            usergroups.setListOfLocationOrPincodes("");
            usergroups.setRestrictionLevel(1);
            usergroups.setState("");
            usergroups.setCountry("");
        } else if (_selectedType.equals("Country")) {
            usergroups.setListOfLocationOrPincodes("");
            usergroups.setRestrictionLevel(2);
            usergroups.setCountry(_CountryName);
            usergroups.setState("");
        } else if (_selectedType.equals("State")) {
            usergroups.setListOfLocationOrPincodes("");
            usergroups.setRestrictionLevel(3);
            usergroups.setCountry(_CountryName);
            usergroups.setState(_StateName);
        } else if (_selectedType.equals("City")) {
            usergroups.setListOfLocationOrPincodes(_cityName);
            usergroups.setRestrictionLevel(4);
            usergroups.setCountry(_CountryName);
            usergroups.setState(_StateName);
        } else if (_selectedType.equals("Pincode")) {
            usergroups.setListOfLocationOrPincodes(_PinCode);
            usergroups.setRestrictionLevel(5);
            usergroups.setCountry(_CountryName);
            usergroups.setState(_StateName);
        }
        int result = -1;
        String resultString = "ERROR";
        GroupManagement management = new GroupManagement();
        Usergroups usergroups1 = management.getGroupByGroupName(sessionId, channel.getChannelid(), _unitName);
        if (usergroups1 == null) {
            result = management.addGroup(sessionId, usergroups);
            log.debug("addGroup :: "+result);
            if (result == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add Group", resultString, result,
                        "Group Management", "", "Group Name=" + _unitName + "Group Status" + Status + "created on =" + new Date()
                        + "Updated On = " + new Date(), itemType,
                        channel.getChannelid());
            } else {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add Group", resultString, result,
                        "Group Management", "", "Failed To Add Group...!!!",
                        itemType, channel.getChannelid());
            }
        } else {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), 
                    "Add Group", resultString, result,
                    "Group Management", "", 
                    "Failed To Add Group",
                    itemType, channel.getChannelid());
        }
        if (result == 0) {
            _result = "success";
        } else {
            _result = "error";
            _message = "Group Addition Failed!!!";

        }
        try {
            json.put("_result", _result);
            json.put("_message", _message);
        } catch (Exception e) {
           
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();

        }
        
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
