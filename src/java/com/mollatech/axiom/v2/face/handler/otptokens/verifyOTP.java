package com.mollatech.axiom.v2.face.handler.otptokens;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class verifyOTP extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(verifyOTP.class.getName());

     final String itemType = "OTPTOKENS";
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
         Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
         log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);

        
        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());

        String _otp = request.getParameter("_oobotp");
        log.debug("_otp :: "+_otp);
        String _userid = request.getParameter("_userIDOV");
        log.debug("_userid :: "+_userid);
//        String _usertypeIDS = request.getParameter("_usertypeIDS");
//        String _subcategory = request.getParameter("_subcategory");
//        int subCategory = 0;
//        if (_subcategory != null) {
//            subCategory = Integer.parseInt(_subcategory);
//        }

        String _category = request.getParameter("_usertypeIDS");
        log.debug("_usertypeIDS :: "+_category);
//        Integer.parseInt("skjfhs");
        int category = 0;
        if (_category != null) {
            category = Integer.parseInt(_category);
        }

        String result = "success";
        String message = "Verified Successfully!!!";

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
       
        if (_userid == null) {
            result = "error";
            message = "Could Not Verify OTP!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        
        
       
        int retValue = -1;
        OTPTokenManagement oManagement = new OTPTokenManagement(channel.getChannelid());
        AuditManagement audit = new AuditManagement();
        retValue = oManagement.VerifyOTPByType(channel.getChannelid(), _userid, sessionId, _otp,category);
        log.debug("VerifyOTPByType :: "+retValue);
        //System.out.println("VerifyOTPByType returned::" + retValue + " for otp::" + _otp);
    //    String icategory = String.valueOf(category);
        
       
         String resultString = "Failed";
        if(retValue == 0){
                resultString = "Success";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), 
                    request.getRemoteAddr(),channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Verify OTP",
                    resultString, retValue,
                    "Token Management", "OTP=*****", "OTP =*****",
                  itemType, _userid);
        }else if(retValue == OTPTokenManagement.OTP_ALREADY_EXIST){
              audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), 
                    request.getRemoteAddr(),channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Verify OTP",
                    resultString, retValue,
                    "Token Management", "", "OTP Already Verified",
                  itemType, _userid);
             result = "error";
               message = "OTP Already Verified ...!!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }else {
            
                resultString = "Failed";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), 
                    request.getRemoteAddr(),channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Verify OTP",resultString, retValue,
                    "Token Management", "", "Failed to Verify Token", itemType, _userid);
            
            
            if ( retValue == -9 ) {
                message = "OTP Expired!!!!";
            } else {
                message = "Verification Failed!!!!";
            }
            
            result = "error";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
          
        }catch(Exception e){
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
