/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author nilesh
 */
public class checkServerStatus extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(checkServerStatus.class.getName());


    public final int WEEKDAYS_RESTRICTION = 1;
    public final int WHOLE_WEEK_RESTRICTION = 2;
    public final int AM = 1;
    public final int PM = 2;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");
        try {

            JSONObject json = new JSONObject();
            PrintWriter out = response.getWriter();
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            SettingsManagement sMngt = new SettingsManagement();
            ChannelProfile channelprofileObj = null;
          //  log.debug("checkServerStatus::channel is::" + channel.getName());

            if (channel != null) {
                Object channelpobj = sMngt.getSettingInner(channel.getChannelid(), SettingsManagement.CHANNELPROFILE_SETTING, 1);
                if (channelpobj == null) {
                    return;
                } else {
                    channelprofileObj = (ChannelProfile) channelpobj;
                    if (channelprofileObj._multipleSession != 1) {
                        return;
                    } else {
                        SessionManagement smngt = new SessionManagement();
                        int retVal = smngt.getServerStatus(channelprofileObj);
                        
                        log.debug("checkServerStatus::getServerStatus::" + retVal);
            
                        if (retVal != 0) {
                            String result = "error";
                            String message = "Server Down for maintainance,Please try later!!!";
                            json.put("_result", result);
                            json.put("_message", message);
                            out.print(json);
                            out.flush();
                            return;
                        }

                    }

                }
            }

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
