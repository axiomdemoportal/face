/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.pkitokens;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Kyctable;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.KYCManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.RootCertificateSettings;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import com.mollatech.license.registerlicense;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class kycfileupload extends HttpServlet {
    
     static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(kycfileupload.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        log.info("is started :: ");
        //response.setContentType("json/application");
        PrintWriter out = response.getWriter();
        String strError = "";
        String saveFile = "";
        String savepath = "";
        String result = "success";
        String message = "File Upload sucessfully";

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());

        JSONObject json = new JSONObject();
         

        savepath = System.getProperty("catalina.home");
        if (savepath == null) {
            savepath = System.getenv("catalina.home");
        }
        savepath += System.getProperty("file.separator");
        savepath += "axiomv2-settings";
        savepath += System.getProperty("file.separator");
        savepath += "uploads";
        savepath += System.getProperty("file.separator");

        String _usersid = request.getParameter("_usersid");
        log.debug("_usersid :: "+_usersid);

        int itype = 0;
        String temp = _usersid;

        String optionalFileName = "";
        FileItem fileItem = null;
        String[] files = new String[1];	 // file names
        String dirName = savepath;
        int retValue = 0;

        int i = 0;

        if (ServletFileUpload.isMultipartContent(request)) {
            try {
                ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                List fileItemsList = servletFileUpload.parseRequest(request);
                Iterator it = fileItemsList.iterator();

                while (it.hasNext()) {
                    FileItem fileItemTemp = (FileItem) it.next();
                    if (fileItemTemp.isFormField()) {
                        if (fileItemTemp.getFieldName().equals("filename")) {
                            optionalFileName = fileItemTemp.getString();
                        } else {
                            //System.out.println("Additional fields in file upload form is  " + fileItemTemp.getFieldName());
                        }
                    } else {
                        fileItem = fileItemTemp;
                    }
                    if (fileItem != null) {
                        String fileName = fileItem.getName();

                        if (fileItem.getSize() == 0) {
                            strError = "Please Select File To Upload...!!!";
                            result = "error";
                            try {
                                json.put("result", result);
                                json.put("message", strError);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            //out.print(json);
                            out.print("{result:'" + result + "',message:'" + strError + "'}");
                            out.flush();
                            return;
                        }

                        if (fileItem.getSize() > 0 && fileItem.getSize() < 1024000 * 5) {
                            // size cannot be more than 65Kb. We want it light.
                            if (optionalFileName.trim().equals("")) {
                                fileName = FilenameUtils.getName(fileName);
                            } else {
                                fileName = optionalFileName;
                            }
                            files[i++] = dirName + fileName;
                            File saveTo = new File(dirName + fileName);

                            saveFile = fileName;

                            AXIOMStatus axiom[] = null;
                            try {
                                fileItem.write(saveTo);
                                HttpSession session = request.getSession(true);
                                session.setAttribute("_kycfileupload", saveTo.getAbsolutePath());

                            } catch (Exception e) {
                               log.error("exception caught :: ",e);
                            }
                        } else {
                            strError = "Error: " + fileName + " size is more than 5MB. Please upload correct file.";
                            result = "error";
                            try {
                                json.put("result", result);
                                json.put("message", strError);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            //out.print(json);
                            out.print("{result:'" + result + "',message:'" + strError + "'}");
                            out.flush();
                            return;
                        }
                    } else {
                        result = "error";
                        message = "Error: No file present...";
                        try {
                            json.put("result", result);
                            json.put("message", message);
                        } catch (Exception e) {
                            log.error("exception caught :: ",e);
                        }
                        //out.print(json);
                        out.print("{result:'" + result + "',message:'" + message + "'}");
                        out.flush();
                        return;
                    }
                }
            } catch (FileUploadException ex) {
                Logger.getLogger(registerlicense.class.getName()).log(Level.SEVERE, null, ex);
                log.error("exception caught :: ",ex);
            }
        } else {
            result = "error";
            message = "Error: Form Post is invalid!!!";
            try {
                json.put("result", result);
                json.put("message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            //out.print(json);
            out.print("{result:'" + result + "',message:'" + message + "'}");
            out.flush();
            return;
        }

        String _kycfileupload = (String) request.getSession().getAttribute("_kycfileupload");
        if (_kycfileupload != null) {
            CertificateManagement cManagement = new CertificateManagement();
//            int res = cManagement.addKYCDocs(sessionId, channel.getChannelid(), _usersid, _kycfileupload);
             KYCManagement kManagement = new KYCManagement();
            Kyctable kyc = kManagement.getKyctable(sessionId, sessionId, _usersid);
             int res = -1;
            if(kyc == null){
             res = cManagement.addKYCDocs(sessionId, channel.getChannelid(), _usersid, _kycfileupload);
             log.debug("addKYCDocs :: "+res);
            }else{
                res = kManagement.changeKycDocs(sessionId, channel.getChannelid(), _usersid, _kycfileupload);
                log.debug("changeKycDocs :: "+res);
            }
          if(res == 0){
               
                SettingsManagement setManagement = new SettingsManagement();
                RootCertificateSettings rCertSettings = (RootCertificateSettings) setManagement.getSetting(sessionId, channel.getChannelid(), SettingsManagement.RootConfiguration, 1);
                if (rCertSettings == null) {
                    result = "error";
                    message = "Certificate Settings are Not Configured!!!";
                }
                
                if(rCertSettings.CAEmailides == null){
                      result = "error";
                    message = "CA Emails Are Unavailable!!!";
                }
                           
              String []emailsOfCA = rCertSettings.CAEmailides.split(",");
              
              TemplateManagement tManagement = new TemplateManagement();
                            Templates templates = tManagement.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.EMAIL_KYC_DOC_UPLOAD_NOTIFYCA);
                          if(templates.getStatus() == tManagement.ACTIVE_STATUS){
                            ByteArrayInputStream bais = new ByteArrayInputStream(templates.getTemplatebody());
                            String tmessage = (String) TemplateUtils.deserializeFromObject(bais);
                            String subject = templates.getSubject();
                            SendNotification send = new SendNotification();
                           String[] emailList = new String[emailsOfCA.length - 1];

                        for (int j = 1; j < emailsOfCA.length; j++) {
                            emailList[j-1] = emailsOfCA[j];
                        }
                                SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                                if (message != null) {
                                    Date d = new Date();
                                   
                                    tmessage = tmessage.replaceAll("#name#", operatorS.getName());
                                    tmessage = tmessage.replaceAll("#channel#", channel.getName());
                                    tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                                    tmessage = tmessage.replaceAll("#email#", emailsOfCA[0]);
                                   
                                  //  tmessage = tmessage.replaceAll("#expiry#", sdf.format(exipryDate));
                                }

                                if (subject != null) {
                                    Date d = new Date();
                                    subject = subject.replaceAll("#channel#", channel.getName());
                                    subject = subject.replaceAll("#datetime#", sdf.format(d));
                                }
                                
                                send.SendEmail(channel.getChannelid(), emailsOfCA[0], subject, tmessage, emailList, null, null, null, 3);
                          }
            strError = "File uploaded successfully";
            result = "success";
          }
            try {
                json.put("result", result);
                json.put("message", strError);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            //out.print(json);
            out.print("{result:'" + result + "',message:'" + strError + "'}");
            out.flush();
            return;

        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
