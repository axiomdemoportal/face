/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.otptokens;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Human
 */
public class checkTokenSecrete extends HttpServlet {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(checkTokenSecrete.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());

        String _serialno = request.getParameter("_serialno");
        log.debug("_serialno :: "+_serialno);

        String result = "success";
        String message = "Token secret is valid!!!";

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        if (_serialno == null) {
            result = "error";
            message = "Enter Serial Number!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        int retValue = -1;
        OTPTokenManagement oManagement = new OTPTokenManagement(channel.getChannelid());
        retValue = oManagement.isValidSecrete(sessionId, _serialno);
        log.debug("isValidSecrete :: "+retValue);
        if (retValue == -5) {
            result = "error";
            message = "Token Secret is empty!!";

        } else if (retValue == -6) {
            result = "error";
            message = "Token Secret format is invalid !!";
        } else if (retValue == -4) {
            result = "error";
            message = "Invalid Serial Number/Token Not Found!!";
        } else if (retValue == 0) {
            result = "success";
            message = "Token secret is valid!!!";
        } else {
            result = "error";
            message = "Token Secret is invalid !!";
        }

        try {
            json.put("_result", result);
            json.put("_message", message);

        }catch(Exception e){
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
