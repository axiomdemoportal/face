package com.mollatech.axiom.v2.face.handler.pkitokens;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.AuthorizationManagement;
import com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class sendpfxfile extends HttpServlet {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(sendpfxfile.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final String itemType = "PKITOKEN";
    final String strFAILED = "FAILED";
    final int FAILED = -1;
    final String itemTypeAUTH = "AUTHORIZATION";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: " + channel.getChannelid());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: " + sessionId);
        //audit parameter
        String _newvalue = request.getParameter("newvalue");
        log.debug("new value = "+_newvalue);
        String _oldvalue = request.getParameter("oldvalue");
        log.debug("oldvalue = "+_oldvalue);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: " + remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: " + operatorS.getOperatorid());
        String _userid = request.getParameter("_userid");
        log.debug("_userid :: " + _userid);
        String _category = request.getParameter("_category");
        log.debug("_category :: " + _category);
        int category = 0;
        if (_category != null) {
            category = Integer.parseInt(_category);
        }
        String result = "success";
        String message = "PFX File send on your Email....";
        String strPassword = null;
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        if (_userid == null) {
            result = "error";
            message = "PFX File not sent ....";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ", e);
            }
            out.print(json);
            out.flush();
            return;
        }

//        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.PKI_CERTIFICATE) != 0) {
//            result = "error";
//            message = "Digital Certificate is not available in this license!!!";
//            try {
//                json.put("_result", result);
//                json.put("_message", message);
//            } catch (Exception e) {
//                log.error("Exception caught :: ",e);
//            }
//            out.print(json);
//            out.flush();
//            return;
//        }
//           if (operatorS.getRoleid() != 6|| operatorS.getRoleid() != 5) {
//           if (operatorS.getRoleid() >= 3) {
//                result = "error";
//                message = "Sorry, But you don't have permissions for this action!!!";
//                try {
//                    json.put("_result", result);
//                    json.put("_message", message);
//                } catch (Exception e) {
//                    log.error("Exception caught :: ",e);
//                }
//                out.print(json);
//                out.flush();
//                return;
//            }
//        }
        int retValue = -1;

        AXIOMStatus status = new AXIOMStatus();
        status.iStatus = FAILED;
        status.strStatus = strFAILED;
        CertificateManagement cManagement = new CertificateManagement();
        AuditManagement audit = new AuditManagement();

        String filepath = cManagement.getPFXFile(sessionId, channel.getChannelid(), _userid);
        String _approvalId = null;
        AuthUser user = null;

        OperatorsManagement oprMngt = new OperatorsManagement();
        UserManagement uMngt = new UserManagement();
        user = uMngt.getUser(sessionId, channel.getChannelid(), _userid);
        AuthorizationManagement auth = new AuthorizationManagement();
        String strOpName = "-";
        String struserName = "-";
        String strAction = "-";
        int iapprovalID = -1;
        String resultString = "ERROR";
//         ApprovalSetting approvalSetting = null;
//        String resultString = "Failure";
        if (filepath != null) {
            retValue = 0;

            Templates temp = null;
            TemplateManagement tObj = new TemplateManagement();
            temp = tObj.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.EMAIL_HARDWARE_PFX_FILE_TEMPLATE);
            if (temp.getStatus() == tObj.ACTIVE_STATUS) {
                ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                Date d = new Date();
                String tmessage = (String) TemplateUtils.deserializeFromObject(bais);
                String subject = temp.getSubject();
                tmessage = tmessage.replaceAll("#name#", user.getUserName());
                tmessage = tmessage.replaceAll("#channel#", channel.getName());
                tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                tmessage = tmessage.replaceAll("#email#", user.getEmail());
                subject = subject.replaceAll("#name#", user.getUserName());
                subject = subject.replaceAll("#channel#", channel.getName());
                subject = subject.replaceAll("#datetime#", sdf.format(d));

                String[] files = new String[1];
                files[0] = filepath;

                String[] filemimetypes = new String[1];
                filemimetypes[0] = "application/x-pkcs12";

                SendNotification send = new SendNotification();

                //start of authorization - Nilesh
                _approvalId = request.getParameter("_approvalId");
                log.debug("_approvalId :: " + _approvalId);
                SettingsManagement sMngmt = new SettingsManagement();

                Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                ChannelProfile chSettingObj = null;
                if (settingsObj != null) {
                    chSettingObj = (ChannelProfile) settingsObj;
                }
                String _unitId = request.getParameter("_unitId");
                log.debug("_unitId :: " + _unitId);
                if (_unitId != null) {
                    if (chSettingObj != null) {

                        if (chSettingObj.authorizationunit == 1) {

                            int iUnitId = Integer.parseInt(_unitId);
                            if (iUnitId != operatorS.getUnits()) {
                                result = "error";
                                message = "Sorry, Must be marked by same branch operator!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ", e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                }

                if (_approvalId != null) {
                    if (operatorS.getRoleid() != 6) {
                        if (operatorS.getRoleid() >= 3) {
                            result = "error";
                            message = "Sorry, But you don't have permissions for this action!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ", e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                    }
// iapprovalID = Integer.parseInt(_approvalId);
                    String _markerID = request.getParameter("_markerID");
                    log.debug("_markerID :: " + _markerID);
                    Operators op = oprMngt.getOperatorById(channel.getChannelid(), _markerID);
                    if (op != null) {
                        strOpName = op.getName();
                    }

                    if (op != null) {
                        strOpName = op.getName();
                    }
                    if (user != null) {
                        struserName = user.getUserName();
                    }
                }

//         if (chSettingObj != null && _approvalId == null && operatorS.getRoleid() == OperatorsManagement.Requester) {
//
//            if (chSettingObj.authorizationStatus == SettingsManagement.ACTIVE_STATUS) {
//                AuthorizationManagement authMngt = new AuthorizationManagement();
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = AuthorizationManagement.SEND_PFX_FILE;
//                approval.itemid = "Send PFX to "+user.getUserName();
//                strAction = approval.itemid;
//                approval.makerid = operatorS.getOperatorid();
//                approval.userid = _userid;
//                approval.tokenCategory = category;
////                approval.sendCertTo = _emailRecipient;
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.HOUR, chSettingObj.authorizationDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//
//                retValue = authMngt.addAuthorizationSetting(sessionId, channel.getChannelid(), operatorS.getOperatorid(),AuthorizationManagement.AUTORIZATION_PENDING_STATUS, dexpiredOn, approval
//                );
//                if (retValue == 0) {
//                    retValue = AuthorizationManagement.RETURN_AUTORIZATION_RESULT;
//                    status.iStatus = retValue;
//                }
//            } else {
//                       status = send.SendEmail(channel.getChannelid(), user.getEmail(), subject, tmessage, null, null, files, filemimetypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//            }
//        } else {
                status = send.SendEmail(channel.getChannelid(), user.getEmail(), subject, tmessage, null, null, files, filemimetypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));

//        }
                //end of authorization
//                status = send.SendEmail(channel.getChannelid(), user.getEmail(), subject, tmessage, null, null, files, filemimetypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
            }

        } else if (filepath == null) {
        }

        if (status.iStatus == 0) {
            resultString = "Success";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    //ipaddress, 
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "SendPFXFile",
                    resultString, retValue, "PKI TOKEN Management", "", "Send pfx file to =" + user.getUserName(), itemType, _userid);

            if (_approvalId != null) {
                int res = auth.removeAuthorizationRequest(sessionId, channel.getChannelid(), operatorS.getOperatorid(), _approvalId,_newvalue,_oldvalue);
                log.debug("removeAuthorizationRequest :: " + res);
                if (res == 0) {
                    resultString = "SUCCESS";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(),
                            "Remove Authorization Request",
                            resultString, retValue,
                            "Authorization Management", "Action =" + strAction + ",Action Marked By=" + strOpName
                            + ",Action Marked On=" + struserName, "Removed successfully!!!",
                            itemTypeAUTH, _userid);
                }

            }

        } else if (status.iStatus == AuthorizationManagement.RETURN_AUTORIZATION_RESULT) {
            //resultString = "Success";
//            if (_approvalId != null) {

            resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(),
                    "Add Authorization Request :Send pfx file",
                    resultString, retValue,
                    "Authorization Management", "Cert send successfully to " + user.getUserName(),
                    "Added successfully!!!",
                    itemTypeAUTH, _userid);

//            }
            result = "success";
            message = "Request is pending for approval!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("exception caught :: ", ex);
            }

            out.print(json);
            out.flush();
            return;
        } else {
            resultString = "ERROR";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    //ipaddress, 
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Send PFX File",
                    resultString, retValue, "PKI TOKEN Management", "", "Send pfx file to =" + user.getUserName(),
                    itemType, _userid);

            result = "error";
            message = "PFX File Not Sent!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ", e);
            }
            out.print(json);
            out.flush();
            return;

        }

        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            log.error("exception caught :: ", e);
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
