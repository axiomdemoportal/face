/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.settings.PasswordPolicySetting;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

public class editPassordPolicySettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editPassordPolicySettings.class.getName());

    final String itemtype = "SETTINGS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String OperatorID = operatorS.getOperatorid();

        String _channelId = channel.getChannelid();
        SettingsManagement sMngmt = new SettingsManagement();
        int retValue = -1;

        String result = "success";
        String message = "Password Policy Settings Update Successful!!!";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

            log.debug("channel ::" + channel.getName());
            log.debug("operatorS ::" + operatorS.getOperatorid());
            log.debug("SessionId::" + sessionId);
            log.debug("remoteaccesslogin::" + remoteaccesslogin);
            log.debug("Channel Id is::" + channel.getChannelid());
//nilesh 20-10
        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.USER_PASSWORD) != 0) {
            result = "error";
            message = "This feature is not available in this license!!!";

            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }

            out.print(json);
            out.flush();
            return;
        }//nilesh 20-10

        int ipasswordLength = Integer.parseInt(request.getParameter("_passwordLength"));
        int ipasswordExpiryTime = Integer.parseInt(request.getParameter("_passwordExpiryTime"));
        int ipasswordIssueLimit = Integer.parseInt(request.getParameter("_passwordIssueLimit"));
        int ienforceDormancyTime = Integer.parseInt(request.getParameter("_enforceDormancyTime"));
        int ipasswordInvalidAttempts = Integer.parseInt(request.getParameter("_passwordInvalidAttempts"));
        int ioldPasswordReuseTime = Integer.parseInt(request.getParameter("_oldPasswordReuseTime"));
        String _changePasswordAfterLogin = request.getParameter("_changePasswordAfterLogin");
        String _epinResetReissue = request.getParameter("_epinResetReissue");
        String _crResetReissue = request.getParameter("_crResetReissue");
        int ipasswordType = Integer.parseInt(request.getParameter("_passwordType"));
        String _allowRepetationCharacter = request.getParameter("_allowRepetationCharacter");
        String _genearationPassword = request.getParameter("_genearationPassword");
        String _alertUserPassword = request.getParameter("_alertUserPassword");
        String _blockCommonWords = request.getParameter("_blockCommonWords");
        String _allowAlert = request.getParameter("_allowAlert");
        String _gatewayType = request.getParameter("_gatewayType");
        String _alertAttempt = request.getParameter("_alertAttempt");
        String _templateName = request.getParameter("_templateName");
        String _arrCommonPassword = request.getParameter("_arrCommonPassword");
        String _allowAlertFor = request.getParameter("_allowAlertFor");

        String _userValidityDays = request.getParameter("_userValidityDays");
        
            log.debug("_changePasswordAfterLogin :: " + _changePasswordAfterLogin);
            log.debug("_epinResetReissue :: " + _epinResetReissue);
            log.debug("_crResetReissue :: " + _crResetReissue);
            log.debug("_allowRepetationCharacter :: " + _allowRepetationCharacter);
            log.debug("_genearationPassword :: " + _genearationPassword);
            log.debug("_alertUserPassword :: " + _alertUserPassword);
            log.debug("_blockCommonWords :: " + _blockCommonWords);
            log.debug("_allowAlert :: " + _allowAlert);
            log.debug("_gatewayType :: " + _gatewayType);
            log.debug("_alertAttempt :: " + _alertAttempt);
            log.debug("_templateName :: " + _templateName);
            log.debug("_arrCommonPassword :: " + _arrCommonPassword);
            log.debug("_allowAlertFor :: " + _allowAlertFor);
            log.debug("_userValidityDays :: " + _userValidityDays);
            

        int iuserValidityDays = -1;
        if (_userValidityDays != null) {
            iuserValidityDays = Integer.parseInt(_userValidityDays);
        }

        boolean allowRepetationCharacter = false;
        if (_allowRepetationCharacter.equals("1")) {
            allowRepetationCharacter = true;
        }
        int igenearationPassword = Integer.parseInt(_genearationPassword);

        boolean alertUserPassword = false;
        if (_alertUserPassword.equals("1")) {
            alertUserPassword = true;
        }
        boolean blockCommonWords = false;
        if (_blockCommonWords.equals("1")) {
            blockCommonWords = true;
        }

        String[] arrCommonPassword = null;
        if (_arrCommonPassword != null) {
            arrCommonPassword = _arrCommonPassword.split(",");
        }
        boolean allowAlert = false;
        if (_allowAlert.equals("1")) {
            allowAlert = true;
        }

        int ialertAttempt = Integer.parseInt(_alertAttempt);
        //int itemplateId = Integer.parseInt(_templateName);
        int igatewayType = Integer.parseInt(_gatewayType);
        int iAlertFor = Integer.parseInt(_allowAlertFor);

        boolean changePasswordAfterLogin = false;
        if (_changePasswordAfterLogin.equals("1")) {
            changePasswordAfterLogin = true;
        }
        boolean epinResetReissue = false;
        if (_epinResetReissue.equals("1")) {
            epinResetReissue = true;
        }
        boolean crResetReissue = false;
        if (_crResetReissue.equals("1")) {
            crResetReissue = true;
        }
        int iType = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PASSWORD_POLICY_SETTING;
        int iPreference = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE;
        Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), iType, iPreference);
        PasswordPolicySetting passwordObj = null;
        boolean bAddSetting = false;
        if (settingsObj == null) {
            passwordObj = new PasswordPolicySetting();
            bAddSetting = true;
        } else {
            passwordObj = (PasswordPolicySetting) settingsObj;
        }

        passwordObj.enforcementDormancytime = ienforceDormancyTime;
        passwordObj.invalidAttempts = ipasswordInvalidAttempts;
        passwordObj.issuingLimitDuration = ipasswordIssueLimit;
        passwordObj.oldPasswordCannotbeReused = ioldPasswordReuseTime;
        passwordObj.passwordExpiryTime = ipasswordExpiryTime;
        passwordObj.passwordLength = ipasswordLength;
        passwordObj.allowCRQAforResetandReissue = crResetReissue;
        passwordObj.allowEPINforResetandReissue = epinResetReissue;
        passwordObj.changePasswordAfterFirstlogin = changePasswordAfterLogin;
        passwordObj.passwordTye = ipasswordType;

        passwordObj.allowRepetationOfChars = allowRepetationCharacter;
        passwordObj.allowCommonPassword = blockCommonWords;
        passwordObj.passwordGenerations = igenearationPassword;
        passwordObj.arrCommonPassword = arrCommonPassword;
        passwordObj.passwordAlert = allowAlert;
        passwordObj.passwordAlertVia = igatewayType;
        passwordObj.passwordAlertAttempts = ialertAttempt;

        //changed by vikram
        TemplateManagement tempObj = new TemplateManagement();
        Templates tObj = tempObj.LoadbyName(_channelId, _templateName);
        passwordObj.passwordAlertTemplateID = tObj.getTemplateid();
        int itemplateId = passwordObj.passwordAlertTemplateID ;
            //end of change 

        passwordObj.passwordAlertFor = iAlertFor;

        String strPasswordType = "Numeric";
        if (ipasswordType == 2) {
            strPasswordType = "AlphaNumeric";
        } else if (ipasswordType == 3) {
            strPasswordType = "AlphaNumeric With Special Characters";
        }
        
        
        passwordObj.allowRepetationOfChars = allowRepetationCharacter;
        passwordObj.allowCommonPassword = blockCommonWords;
        passwordObj.passwordGenerations = igenearationPassword;
        passwordObj.arrCommonPassword = arrCommonPassword;
        passwordObj.passwordAlert = allowAlert;
        passwordObj.passwordAlertVia = igatewayType;
        passwordObj.passwordAlertAttempts = ialertAttempt;

        //passwordObj.passwordAlertTemplateID = itemplateId;
//      
        //passwordObj.userValidityDays = iuserValidityDays;
        
        
        AuditManagement audit = new AuditManagement();
        String strPasswordsv = "";
        for (int i = 0; i < arrCommonPassword.length; i++) {
            strPasswordsv = "," + arrCommonPassword[i];
        }

        if (bAddSetting == true) {
            retValue = sMngmt.addSetting(sessionId, _channelId, iType, iPreference, passwordObj);
            log.debug("addSetting :: "+ retValue);
            

            String resultString = "ERROR";
            if (retValue == 0) {
                resultString = "SUCCESS";

                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add Password Policy Settings", resultString, retValue, "Setting Management",
                        "", "Password Type=" + strPasswordType + "Enforcement Dormancy Time =" + ienforceDormancyTime + "Invalid Attempts =" + ipasswordInvalidAttempts
                        + "Password Issue Limit Duration =" + ipasswordIssueLimit + "Old Password Cannot be Used =" + ioldPasswordReuseTime
                        + "Password Expiry Time =" + ipasswordExpiryTime + "Password Length =" + ipasswordLength
                        + "Allow CR reser and reissue =" + crResetReissue + "Allow Epin Reset and Reissue =" + epinResetReissue
                        + "Change Password After 1St Login" + changePasswordAfterLogin
                        + "Allow Repetation of Chars=" + allowRepetationCharacter + "Allow common password =" + blockCommonWords + "Password Generations =" + igenearationPassword
                        + "Common Passwords=" + strPasswordsv + "Password Alert=" + allowAlert + "Alert via=" + igatewayType + "Password Attempts=" + ialertAttempt
                        + "Template Id =" + itemplateId + "Alert For =" + iAlertFor,
                        itemtype, channel.getChannelid());

            } else if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add Password Policy Settings", resultString, retValue, "Setting Management",
                        "", "Failed To Add Password Policy Settings",
                        itemtype, channel.getChannelid());
            }

        } else {
            PasswordPolicySetting oldpasswordObj = (PasswordPolicySetting) sMngmt.getSetting(sessionId, _channelId, iType, iPreference);
            retValue = sMngmt.changeSetting(sessionId, _channelId, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PASSWORD_POLICY_SETTING, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE, settingsObj, passwordObj);

            log.debug("changeSetting :: "+ retValue);
            
            String stroldPasswordType = "numeric";
            if (oldpasswordObj.passwordTye == 2) {
                stroldPasswordType = "AlphaNumeric";
            } else if (oldpasswordObj.passwordTye == 3) {
                stroldPasswordType = "AlphaNumeric With Special Characters";
            }
            String stroldPasswordsv = "";
            for (int i = 0; i < oldpasswordObj.arrCommonPassword.length; i++) {
                stroldPasswordsv = "," + arrCommonPassword[i];
            }
            String resultString = "ERROR";
            if (retValue == 0) {

                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Change Password Policy Settings", resultString, retValue, "Setting Management",
                        "Password Type=" + stroldPasswordType
                        + "Enforcement Dormancy Time =" + oldpasswordObj.enforcementDormancytime + "Invalid Attempts =" + oldpasswordObj.invalidAttempts
                        + "Password Issue Limit Duration =" + oldpasswordObj.issuingLimitDuration + "Old Password Cannot be Used =" + oldpasswordObj.oldPasswordCannotbeReused
                        + "Password Expiry Time =" + oldpasswordObj.passwordExpiryTime + "Password Length =" + oldpasswordObj.passwordLength
                        + "Allow CR reser and reissue =" + oldpasswordObj.allowCRQAforResetandReissue + "Allow Epin Reset and Reissue =" + oldpasswordObj.allowEPINforResetandReissue
                        + "Change Password After 1St Login" + oldpasswordObj.changePasswordAfterFirstlogin + "Alert For =" + oldpasswordObj.passwordAlertFor,
                        "Password Type=" + strPasswordType + "Enforcement Dormancy Time =" + ienforceDormancyTime + "Invalid Attempts =" + ipasswordInvalidAttempts
                        + "Password Issue Limit Duration =" + ipasswordIssueLimit + "Old Password Cannot be Used =" + ioldPasswordReuseTime
                        + "Password Expiry Time =" + ipasswordExpiryTime + "Password Length =" + ipasswordLength
                        + "Allow CR reser and reissue =" + crResetReissue + "Allow Epin Reset and Reissue =" + epinResetReissue
                        + "Change Password After 1St Login" + changePasswordAfterLogin
                        + "Allow Repetation of Chars=" + allowRepetationCharacter + "Allow common password =" + blockCommonWords + "Password Generations =" + igenearationPassword
                        + "Common Passwords=" + strPasswordsv + "Password Alert=" + allowAlert + "Alert via=" + igatewayType + "Password Attempts=" + ialertAttempt
                        + "Template Id =" + itemplateId + "Alert For =" + iAlertFor,
                        itemtype, channel.getChannelid());
            } else if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Change Password Policy Settings", resultString, retValue, "Setting Management",
                        "Password Type=" + stroldPasswordType + "Enforcement Dormancy Time =" + oldpasswordObj.enforcementDormancytime + "Invalid Attempts =" + oldpasswordObj.invalidAttempts
                        + "Password Issue Limit Duration =" + oldpasswordObj.issuingLimitDuration + "Old Password Cannot be Used =" + oldpasswordObj.oldPasswordCannotbeReused
                        + "Password Expiry Time =" + oldpasswordObj.passwordExpiryTime + "Password Length =" + oldpasswordObj.passwordLength
                        + "Allow CR reser and reissue =" + oldpasswordObj.allowCRQAforResetandReissue + "Allow Epin Reset and Reissue =" + oldpasswordObj.allowEPINforResetandReissue
                        + "Change Password After 1St Login" + oldpasswordObj.changePasswordAfterFirstlogin
                        + "Allow Repetation of Chars=" + oldpasswordObj.allowRepetationOfChars + "Allow common password =" + oldpasswordObj.arrCommonPassword
                        + "Password Generations =" + oldpasswordObj.passwordGenerations
                        + "Common Passwords=" + stroldPasswordsv + "Password Alert=" + oldpasswordObj.passwordAlert + "Alert via=" + oldpasswordObj.passwordAlertVia
                        + "Password Attempts=" + oldpasswordObj.passwordAlertAttempts
                        + "Template Id =" + oldpasswordObj.passwordAlertTemplateID,
                        "Failed To Password Policy Settings",
                        itemtype, channel.getChannelid());
            }
        }

        if (retValue != 0) {
            result = "error";
            message = "Password Policy Settings Update Failed!!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
           log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
