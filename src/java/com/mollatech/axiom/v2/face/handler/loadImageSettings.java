/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.ImageSettings;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author nilesh
 */
public class loadImageSettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadImageSettings.class.getName());

    private JSONObject SettingsWhenEmpty() {
        log.info("Servlet started");
        JSONObject json = new JSONObject();

        try {
            json.put("_sweetSpot", 1);
            json.put("_sweetSpotDeviation", 1);
            json.put("_xDeviation", 1);
            json.put("_yDeviation", 1);
            json.put("_geoCheck", 1);
            json.put("_deviceProfile", 1);
            json.put("_honeyTrap", 1);

        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        }

        return json;
    }

    private JSONObject SettingsWhenPresent(Object settingsObj) {
        JSONObject json = new JSONObject();
        if (settingsObj instanceof ImageSettings) {
            ImageSettings imageObj = (ImageSettings) settingsObj;
            try {
                json.put("_sweetSpot", imageObj._sweetSpot);
                json.put("_sweetSpotDeviation", imageObj._sweetSpotDeviation);
                json.put("_xDeviation", imageObj._xDeviation);
                json.put("_yDeviation", imageObj._yDeviation);
                json.put("_geoCheck", imageObj._geoCheck);
                json.put("_deviceProfile", imageObj._deviceProfile);
                 json.put("_honeyTrap", imageObj._honeyTrap);
            } catch (Exception e) {
               log.error("Exception caught :: ",e);
            }
        }
        return json;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("channel is :: "+channel.getName());
        log.debug("sessionid :: "+sessionId);
        JSONObject json = null;
        PrintWriter out = response.getWriter();
        try {
            SettingsManagement sMngmt = new SettingsManagement();
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), sMngmt.IMAGE_SETTINGS, sMngmt.PREFERENCE_ONE);
            if (settingsObj != null) {
                json = SettingsWhenPresent(settingsObj);
            } else {
                json = SettingsWhenEmpty();
            }

        } catch (Exception ex) {
           log.error("Exception caught :: ",ex);
            // TODO handle custom exceptions here
        }
        try {
        } catch (Exception ex) {
           log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
