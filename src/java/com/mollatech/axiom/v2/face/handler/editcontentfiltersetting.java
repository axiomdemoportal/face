/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.ContentFilterSetting;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class editcontentfiltersetting extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editcontentfiltersetting.class.getName());

    final String itemtype = "SETTINGS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        PrintWriter out = response.getWriter();

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String ialertstatus = request.getParameter("_alertOperator");
        int _ialertstatus = Integer.parseInt(ialertstatus);



        String OperatorID = operatorS.getOperatorid();

        String istatus = request.getParameter("_statusContent");
        int _istatus = Integer.parseInt(istatus);

        String _channelId = channel.getChannelid();
        SettingsManagement sMngmt = new SettingsManagement();
        int retValue = -1;

        String result = "success";
        String message = "Content Filter Updated Successfully!!!";
        JSONObject json = new JSONObject();
        
            log.debug("channel :: " + channel.getName());
            log.debug("operatorS :: " + operatorS.getOperatorid());
            log.debug("sessionId :: " + sessionId);
            log.debug("remoteaccesslogin :: " + remoteaccesslogin);
            log.debug("getChannelid :: " + channel.getChannelid());
            log.debug("ialertstatus :: "+ ialertstatus);
            log.debug("istatus :: "+ istatus);
            

        //String _preference = request.getParameter("_perferenceContent");
        int _iPreference = 1;//Integer.parseInt(_preference);

        //String _type = request.getParameter("_type");
        int _itype = SettingsManagement.CONTENT_FILTER;//Integer.parseInt(_type);
        String strType = String.valueOf(_itype);
        Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _itype, _iPreference);

        ContentFilterSetting contentObj = null;
        boolean bAddSetting = false;

        if (settingsObj == null) {
            contentObj = new ContentFilterSetting();
            contentObj.setChannelId(_channelId);
            contentObj.setStatus(_istatus);

            bAddSetting = true;
        } else {
            contentObj = (ContentFilterSetting) settingsObj;
        }

        String _content = request.getParameter("_keywords");
        String[] _contentsArray = _content.split(",");
        if (_itype == com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.CONTENT_FILTER) {
            contentObj.setChannelId(_channelId);
            contentObj.setContent(_contentsArray);
            contentObj.setStatus(_istatus);
            contentObj.setAlertstatus(_ialertstatus);
        }
        AuditManagement audit = new AuditManagement();
        if (bAddSetting == true) {

            String[] arrCotent = contentObj.getContent();
            String strContent = null;
            for (int i = 0; i < arrCotent.length; i++) {
                strContent += arrCotent[i] + ",";
            }
            int iStatus = contentObj.getStatus();
            String strStatus = "INACTIVE";
            if (iStatus == 0) {
                strStatus = "ACTIVE";
            }


            int iopalertstatus = contentObj.getAlertstatus();
            String stropalertstatus = "INACTIVE";
            if (iopalertstatus == 0) {
                stropalertstatus = "ACTIVE";
            }


            retValue = sMngmt.addSetting(sessionId, _channelId, _itype, _iPreference, contentObj);
            log.debug("addSetting :: "+ retValue);
            String resultString = "ERROR";

            if (retValue == 0) {

                resultString = "SUCCESS";

                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add Content Filter Settings", resultString, retValue, "Setting Management",
                        "", "New Content =" + strContent + "Status =" + strStatus + "Operator Alert Status =" + stropalertstatus,
                        itemtype, channel.getChannelid());

            } else if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add Content Filter Settings", resultString, retValue, "Setting Management",
                        "", "Failed To Add Content Filter Settings", itemtype, channel.getChannelid());

            }



        } else {
            ContentFilterSetting cOldObj = (ContentFilterSetting) sMngmt.getSetting(sessionId, _channelId, _itype, _iPreference);
            retValue = sMngmt.changeSetting(sessionId, _channelId, _itype, _iPreference, settingsObj, contentObj);
            log.debug("changeSetting :: "+ retValue);
            
            String resultString = "ERROR";

            String[] arroldContent = cOldObj.getContent();
            String strOldContent = null;
            for (int i = 0; i < arroldContent.length; i++) {
                strOldContent += arroldContent[i] + ",";
            }
            int iStatus = cOldObj.getStatus();
            String strStatus = "INACTIVE";
            if (iStatus == 0) {
                strStatus = "ACTIVE";
            }
            String[] arrnewContent = contentObj.getContent();
            String strnewContent = null;
            for (int i = 0; i < arrnewContent.length; i++) {
                strnewContent += arrnewContent[i] + ",";
            }
            int inewStatus = cOldObj.getStatus();
            String strnewStatus = "INACTIVE";
            if (inewStatus == 0) {
                strnewStatus = "ACTIVE";
            }

            int ialertopStatus = cOldObj.getAlertstatus();
            String stralertopStatus = "INACTIVE";
            if (ialertopStatus == 0) {
                stralertopStatus = "ACTIVE";
            }

            int inewAlertOpStatus = cOldObj.getAlertstatus();
            String strnewAlertOpStatus = "INACTIVE";
            if (inewAlertOpStatus == 0) {
                strnewAlertOpStatus = "ACTIVE";
            }

            if (retValue == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Change Content Filter Settings", resultString, retValue, "Setting Management",
                        "Old Content =" + strOldContent + "Status =" + strStatus, "New Content =" + strnewContent + "Status =" + strnewStatus
                        + "Alert Op. Status =" + strnewAlertOpStatus,
                        itemtype, 
                        channel.getChannelid());
            }

            if (retValue != 0) {
                resultString = "ERROR";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Change Content Filter Settings", resultString, retValue, "Setting Management",
                        "Old Content =" + strOldContent + "Status =" + strStatus + "Alert Op. Status =" + stralertopStatus, "Failed To Edit Content..!!",
                        itemtype, 
                        channel.getChannelid());
            }



        }

        if (retValue != 0) {
            result = "error";
            //message = "Content Filter Gateway Settings Update Failed!!!";
            message = "Content Filter Settings Update Failed!!!";
        }


        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch(Exception e){ log.error("Exception caught :: ",e);} finally {
            out.print(json);
            out.close();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
