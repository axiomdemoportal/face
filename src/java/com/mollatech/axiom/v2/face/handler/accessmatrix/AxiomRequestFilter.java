/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.accessmatrix;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.nucleus.db.Approvalsettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuthorizationManagement;
import com.mollatech.axiom.nucleus.db.connector.management.AxiomAccessControlManagment;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class AxiomRequestFilter implements Filter {

    private static final boolean debug = true;

    private FilterConfig filterConfig = null;
    public static final int REQUESTER = 1;
    public static final int AUTHORIZER = 2;
//    int pending = 1;
//    String strPending = "Pending";
    public static final int pending = 1;
    public static final int success = 0;
    public static final int duplicate = 11;

    public AxiomRequestFilter() {
    }

    private void doBeforeProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {

        if (debug) {
            log("AxiomRequestFilter:DoBeforeProcessing");
        }

    }

    private void doAfterProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        if (debug) {
            log("AxiomRequestFilter:DoAfterProcessing");
        }

    }

    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain)
            throws IOException, ServletException {

        try {
            HttpServletRequest request = (HttpServletRequest) req;
            HttpServletResponse httpResponse = (HttpServletResponse) res;
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
            StringBuffer url = request.getRequestURL();
            AxiomAccessControlManagment accesMngt = new AxiomAccessControlManagment();
            Map map = req.getParameterMap();
            if (channel != null && sessionId != null && operatorS != null) {
                if (url.toString().contains("validateSession") || url.toString().contains("signout.jsp")
                        || url.toString().length() <= 32 || url.toString().contains("checkServerStatus")
                        || url.toString().contains("assets/js") || url.toString().contains("home.jsp")
                        || url.toString().contains("login") || url.toString().contains("signout.jsp")
                        || url.toString().contains("changeoprpassword")
                        || url.toString().contains("MonitorHandling")) {
                    chain.doFilter(req, res);
                } else {
                    if (operatorS.getRoleid() == 1) {
                        
                        
                        chain.doFilter(req, res);
                    } else {
                        map = req.getParameterMap();
//                        AXIOMStatus aStatus = accesMngt.checkAccess(sessionId, channel.getChannelid(), map, url.toString(), operatorS.getRoleid());
                      String xmlpath = null;
                      String passwordpath = null;
                      if(url.toString().contains("hwotptokeuploadsubmit")){
                         xmlpath = (String) request.getSession().getAttribute("_otpXmlFliePath");
                         passwordpath = (String) request.getSession().getAttribute("_otpPasswordFliePath");
                      }
                       AuthorizationManagement aObj = new AuthorizationManagement();
                        Approvalsettings[] arrRequest = null;    //ALL 
                        arrRequest = aObj.getALLPendingRequest(sessionId, channel.getChannelid(), AuthorizationManagement.AUTORIZATION_PENDING_STATUS);
                        
                        
                        
                        
                        
                        AXIOMStatus aStatus = accesMngt.checkAccess(sessionId, channel.getChannelid(), map, url.toString(), 
                                operatorS ,xmlpath,passwordpath);
                        if (aStatus == null) {
                            chain.doFilter(req, res);
                            return;
                        }
                        if (aStatus.iStatus == success) {
                            chain.doFilter(req, res);
                             return;
                        }
                        if (aStatus.iStatus == pending) {
//                            if (operatorS.getOperatorType() == REQUESTER) {
                                JSONObject json = new JSONObject();
                                PrintWriter out = httpResponse.getWriter();
                                String result = "error";
                                String message = "Request Added for Approval!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                out.print(json);
                                out.flush();
                                out.close();
                                return;
//                            } else {
//                                chain.doFilter(req, res);
//                                 return;
//                            }
                        }else if(aStatus.iStatus == duplicate){
                            JSONObject json = new JSONObject();
                            PrintWriter out = httpResponse.getWriter();
                            String result = "error";
                            String message = "Can't request the same action because it pending for approval now";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            out.print(json);
                            out.flush();
                            out.close();
                            return;
                        }
                        
                        else { 
                            JSONObject json = new JSONObject();
                            PrintWriter out = httpResponse.getWriter();
                            String result = "error";
                            String message = "You do not access right to execute this operation!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            out.print(json);
                            out.flush();
                            out.close();
                            return;

                        }
                    }
                }
            } else {
                chain.doFilter(req, res);
                 return;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Return the filter configuration object for this filter.
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    public void destroy() {
    }

    /**
     * Init method for this filter
     */
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            if (debug) {
                log("AxiomRequestFilter:Initializing filter");
            }
        }
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("AxiomRequestFilter()");
        }
        StringBuffer sb = new StringBuffer("AxiomRequestFilter(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }

    private void sendProcessingError(Throwable t, ServletResponse response) {
        String stackTrace = getStackTrace(t);

        if (stackTrace != null && !stackTrace.equals("")) {
            try {
                response.setContentType("text/html");
                PrintStream ps = new PrintStream(response.getOutputStream());
                PrintWriter pw = new PrintWriter(ps);
                pw.print("<html>\n<head>\n<title>Error</title>\n</head>\n<body>\n"); //NOI18N

                // PENDING! Localize this for next official release
                pw.print("<h1>The resource did not process correctly</h1>\n<pre>\n");
                pw.print(stackTrace);
                pw.print("</pre></body>\n</html>"); //NOI18N
                pw.close();
                ps.close();
                response.getOutputStream().close();
            } catch (Exception ex) {
            }
        } else {
            try {
                PrintStream ps = new PrintStream(response.getOutputStream());
                t.printStackTrace(ps);
                ps.close();
                response.getOutputStream().close();
            } catch (Exception ex) {
            }
        }
    }

    public static String getStackTrace(Throwable t) {
        String stackTrace = null;
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            sw.close();
            stackTrace = sw.getBuffer().toString();
        } catch (Exception ex) {
        }
        return stackTrace;
    }

    public void log(String msg) {
        filterConfig.getServletContext().log(msg);
    }

}
