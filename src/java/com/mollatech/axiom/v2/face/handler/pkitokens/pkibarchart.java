/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.pkitokens;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.face.common.bar;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mollatech1
 */
public class pkibarchart extends HttpServlet {
     static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(pkibarchart.class.getName());

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       response.setContentType("application/json");
       log.info("is started :: ");
        PrintWriter out = response.getWriter();
        try {
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel.getName());
            String channelId = channel.getChannelid();
            log.debug("channelId :: "+channelId);
            String category = request.getParameter("_changePkiCategory");
            log.debug("category :: "+category);
            String _startDate = request.getParameter("_startDate");
            log.debug("_startDate :: "+_startDate);
            String _endDate   = request.getParameter("_endDate");
            log.debug("_endDate :: "+_endDate);
            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            Date sDate = null; Date eDate=null;
            if(_startDate != null && !_startDate.isEmpty()){
                sDate = formatter.parse(_startDate);
            }
            if(_endDate != null && !_endDate.isEmpty()){
                eDate = formatter.parse(_endDate);
            }
            int icategory = -9999;
            if(category != null & category.isEmpty() != true){
                icategory = Integer.parseInt(category);
            }
            
              PKITokenManagement pObj = new PKITokenManagement();
            
           int iActive = pObj.getPkiTokenCountV2(channelId, icategory,PKITokenManagement.TOKEN_STATUS_ACTIVE,sDate,eDate);
             int iAssign =  pObj.getPkiTokenCountV2(channelId, icategory,PKITokenManagement.TOKEN_STATUS_ASSIGNED,sDate,eDate);
              int iLocked =  pObj.getPkiTokenCountV2(channelId, icategory,PKITokenManagement.TOKEN_STATUS_LOCKEd,sDate,eDate);
                 int iSuspend = pObj.getPkiTokenCountV2(channelId, icategory,PKITokenManagement.TOKEN_STATUS_SUSPENDED,sDate,eDate);
                 int iUnassign = pObj.getPkiTokenCountV2(channelId, icategory,PKITokenManagement.TOKEN_STATUS_UNASSIGNED,sDate,eDate);
              
              ArrayList<bar> sample = new ArrayList<bar>();

               
                 sample.add(new bar(iActive, "Active"));
                sample.add(new bar(iAssign, "Assign"));
                sample.add(new bar(iLocked, "Locked"));
                sample.add(new bar(iSuspend, "Suspend"));
                sample.add(new bar(iUnassign, "Unassign"));
               

                // sample.add(new Bar(blockedcount, "BLOCKED"));

                for (int i = 0; i < sample.size(); i++) {
                    //System.out.println(sample.get(i));
                }
                Gson gson = new Gson();

                JsonElement element = gson.toJsonTree(sample, new TypeToken<List<bar>>() {
                }.getType());

                JsonArray jsonArray = element.getAsJsonArray();
                //response.setContentType("application/json");
                out.print(jsonArray);
          
        } catch(Exception ex){
            log.error("exception caught :: ",ex);
        }finally {            
            out.close();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
