/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.channel;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.ChannelSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class editChannelSettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editChannelSettings.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public static final int Chsettings = 30;
    public static final int preference = 1;

    final String itemtype = "SETTINGS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("is started ::");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel::"+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId::"+sessionId);

        Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin::"+remoteaccesslogin);

        String operatorId = operator.getOperatorid();
        log.debug("operatorId::"+operatorId);

        String _datasource = request.getParameter("_datasource");
        log.debug("_datasource::"+_datasource);
        String _ipSource = request.getParameter("_ipSource");
        log.debug("_ipSource::"+_ipSource);
        String _portSource = request.getParameter("_portSource");
        log.debug("_portSource::"+_portSource);
        String _sslSource = request.getParameter("_sslSource");
        log.debug("_sslSource::"+_sslSource);
        String _databaseNameSource = request.getParameter("_databaseNameSource");
        log.debug("_databaseNameSource::"+_databaseNameSource);
        String _tableNameSource = request.getParameter("_tableNameSource");
        log.debug("_tableNameSource::"+_tableNameSource);
        String _userIdSource = request.getParameter("_userIdSource");
        log.debug("_userIdSource::"+_userIdSource);
        String _passwordSource = request.getParameter("_passwordSource");
        log.debug("_passwordSource::"+_passwordSource);
        String _operatorname = request.getParameter("_operatorname");
        log.debug("_operatorname::"+_operatorname);
        String _mobile = request.getParameter("_mobile");
        log.debug("_mobile::"+_mobile);
        String _mail = request.getParameter("_mail");
        log.debug("_mail::"+_mail);
        int datasource = Integer.valueOf(_datasource);
        
   
        String source="";
        
        boolean externalsource = false;
        boolean sslenabled = false;
        if (datasource == 1) {
            externalsource = true;
            source="Internal Source";

        } else if (datasource == 0) {
            externalsource = false;
            source="External Source";
        }

        if (_sslSource.equalsIgnoreCase("true")) {
            sslenabled = true;
        } else {
            sslenabled = false;
        }

        int portSource = Integer.parseInt(_portSource);

        String result = "success";
        String message = "Channel settings update Successfully....";

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        if (_ipSource == null || _portSource == null
                || _databaseNameSource == null || _tableNameSource == null) {
            result = "error";
            message = "Invalid Parameters!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught ::",e);

            }
            out.print(json);
            out.flush();
            return;
        }

        int retValue = -1;
        SettingsManagement sMngmt = new SettingsManagement();

        Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), Chsettings, preference);

        ChannelSettings channelsettings = null;
        boolean bAddSetting = false;

        if (settingsObj == null) {
            channelsettings = new ChannelSettings();
            channelsettings.setChannelId(channel.getChannelid());
            bAddSetting = true;
        } else {
            channelsettings = (ChannelSettings) settingsObj;
        }

        channelsettings.setDatabaseName(_databaseNameSource);
        channelsettings.setExternalhost(_ipSource);
        channelsettings.setPassword(_passwordSource);
        channelsettings.setPort(portSource);
        channelsettings.setSourceType(externalsource);
        channelsettings.setSslEnabled(sslenabled);
        channelsettings.setTableName(_tableNameSource);
        channelsettings.setUserId(_userIdSource);
        
        channelsettings.setOperatorname(_operatorname);
        channelsettings.setMobile(_mobile);
        channelsettings.setMail(_mail);
        
        
        AuditManagement audit = new AuditManagement();
        if (bAddSetting == true) {
            retValue = sMngmt.addSetting(sessionId, channel.getChannelid(), Chsettings, 1, channelsettings);
            log.debug("addSetting::"+retValue);
            String resultString = "ERROR";
            if (retValue == 0) {
                resultString = "SUCCESS";
                //audit.setIP("127.0.0.1");
                
                 audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                            "Add Channel Profile Settings", resultString, retValue, "Setting Management",
                           "","ChannelID=" + channel.getChannelid() + "Database source=" + _databaseNameSource + "IP=" + _ipSource + "Password=***" + "User ID=" + _userIdSource
                            +"Port=" + portSource  + "TableName=" + _tableNameSource 
                            + "Sourcetype=" + source + "SSLEnabled" + sslenabled,
                            itemtype, channelsettings.getChannelId());

            } else if (retValue != 0) { 
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                            "Add Channel Profile Settings", resultString, retValue, "Setting Management",
                            "", "Failed to Add Channel Profile Settings",
                            itemtype,channelsettings.getChannelId());

            }

        } else {
            ChannelSettings oldObj = (ChannelSettings) sMngmt.getSetting(sessionId, channel.getChannelid(), Chsettings, 1);
            retValue = sMngmt.changeSetting(sessionId, channel.getChannelid(), Chsettings, 1, settingsObj, channelsettings);
            log.debug("changeSetting::"+retValue);
            
            String resultString = "ERROR";

            if (retValue == 0) {
                resultString = "SUCCESS";
                 audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                            "Change Channel Profile Settings", resultString, retValue, "Setting Management",
                            "","ChannelID=" + channel.getChannelid() + "Database source=" + _databaseNameSource + "IP=" + _ipSource + "Password=***" + "User ID=" + _userIdSource
                            +"Port=" + portSource  + "TableName=" + _tableNameSource 
                            + "Sourcetype=" + source + "SSLEnabled" + sslenabled,
                           itemtype,operator.getOperatorid());
                

            } else if (retValue != 0) {

                audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                            "Change Channel Profile Settings", resultString, retValue, "Setting Management","ChannelID=" + channel.getChannelid() + "Database source=" + oldObj.getDatabaseName() + "IP=" + oldObj.getExternalhost() + "Password=***" + "User ID=" + oldObj.getUserId()
                            +"Port=" + oldObj.getPort()  + "TableName=" + oldObj.getTableName()
                            + "Sourcetype=" + oldObj.isSourceType() + "SSLEnabled" + oldObj.isSslEnabled()
                            ,
                           "Failed to Edit Channel Profile Settings",
                            itemtype, operator.getOperatorid());
            }

        }
        if (retValue != 0) {
            result = "error";
            message = "Failed to update Channel Settings!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception e) {
            log.error("exception caught ::",e);

        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended ::");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
