/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

//import com.google.appengine.labs.repackaged.org.json.JSONObject;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class testConnection extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(testConnection.class.getName());

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        String _type = request.getParameter("_type");
        String _preference = request.getParameter("_preference");
        String _testphone = null;
        String _testmsg = null;

        log.debug("testConnection::channel is::"+channel.getName());
        log.debug("testConnection::sessionid::"+sessionId);
        log.debug("testConnection::type is::"+_type);
        log.debug("testConnection::preference is::"+_preference);
        if (_preference.compareTo("1") == 0) {
            _testphone = request.getParameter("_testphone");
            _testmsg = request.getParameter("_testmsg");
        } else if (_preference.compareTo("2") == 0) {
            _testphone = request.getParameter("_testphoneS");
            _testmsg = request.getParameter("_testmsgS");
        }else if (_preference.compareTo("3") == 0){
            _testphone = request.getParameter("_testweb");
            _testmsg = request.getParameter("_testmsg");
        }

        int _type1 = Integer.parseInt(_type);
        int _preference1 = Integer.parseInt(_preference);


        String result = "success";
        String message = "Connection sucessful!!!";

        int retValue = -1;

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        try {
            SettingsManagement sMngmt = new SettingsManagement();
          
            retValue = sMngmt.testSetting(sessionId, channel.getChannelid(), _type1, _preference1, _testphone, _testmsg);
        log.debug("testSetting :: type"+_type1);
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        }

        if(retValue == 0){
            result = "success";
            message = "Connection successful!!!";
        }else if(retValue == -3){//Prefix Filter
            result = "error";
            message = "Message blocked by Prefix Filter!!!";
        }else if(retValue == -5){//Domain Filter
            result = "error";
            message = "Message blocked by Content Filter!!!";
        }else if(retValue == -4){//Domain Filter
            result = "error";
            message = "Message blocked by Domain Filter!!!";
        }else{
            if(_type.equals("8"))
            {
             message = "Failed to connect to usersource. Check Settings!!!";   
            }else
            {
            result = "error";
            message = "Failed to send test message. Check Settings!!!";
            }
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
