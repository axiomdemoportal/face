/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.accessmatrix;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.connector.access.controller.AccessMatrixSettings;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Roles;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author amol
 */
public class saveAccessMatrix extends HttpServlet {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(saveAccessMatrix.class.getName());

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        try {
            response.setContentType("application/json");
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel::"+channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId::"+sessionId);
            Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operator::"+operator.getOperatorid());
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("remoteaccesslogin::"+remoteaccesslogin);
            OperatorsManagement opMngt = new OperatorsManagement();
            String _addList = request.getParameter("_addList");
            log.debug("_addList::"+_addList);
            String _deleteList = request.getParameter("_deleteList");
            log.debug("_deleteList::"+_deleteList);
            String _type = request.getParameter("_type");
            log.debug("_type::"+_type);
            String roleID = request.getParameter("_roleID");
            log.debug("roleID::"+roleID);
            String _operatoName = request.getParameter("_accesstype");
            log.debug("_operatoName::"+_operatoName);
            int iRoleID = Integer.parseInt(roleID);
            int iType = Integer.parseInt(_type);
            JSONObject json = new JSONObject();
            PrintWriter out = response.getWriter();
            String result = "success";
            String message = "Setting updated Successfully!!!";
            
            

//             System.out.println(_deleteList);
            Roles roleObj = opMngt.getRoleByRoleId(channel.getChannelid(), iRoleID);
            AccessMatrixSettings access = null;
            if (roleObj != null) {
                byte[] accessByte = roleObj.getAccessentry();
                ByteArrayInputStream bais = null;
                Object object = null;
                if (accessByte != null) {
                    bais = new ByteArrayInputStream(AxiomProtect.AccessDataBytes(accessByte));
                    object = UtilityFunctions.deserializeFromObject(bais);
                }
                if (object != null) {
                    access = (AccessMatrixSettings) object;
                } else {
                    access = new AccessMatrixSettings();
                }

            } else {
                OperatorsManagement opsMngt = new OperatorsManagement();
                Operators operator1 = opsMngt.getOperatorByName(sessionId, channel.getChannelid(), _operatoName);
                byte[] accessByte = operator1.getAccessentry();
                ByteArrayInputStream bais = null;
                Object object = null;
                if (accessByte != null) {
                    bais = new ByteArrayInputStream(AxiomProtect.AccessDataBytes(accessByte));
                    object = UtilityFunctions.deserializeFromObject(bais);
                }
                if (object != null) {
                    access = (AccessMatrixSettings) object;
                } else {
                    access = new AccessMatrixSettings();
                }
            }

//            System Managment
            if (iType == OperatorsManagement.MatrixSystemManagment) {

                
                //changes by Pramod for AccessMatrix
                 if (_addList != null) {
                    if (access.viewaccessMatrix == false) {
                        if (!_addList.contains("viewAccessMatrix")) {
                            if (_addList.contains("EditAccessMatrix") || _addList.contains("removeAccessMatrix")||_addList.contains("addAccessMatrix")) {
                                result = "error";
                                message = "Please Select View option for Matrix Managemet option!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("viewAccessMatrix")) {
                        access.viewaccessMatrix = true;
                    }
                    if (_addList.contains("EditAccessMatrix")) {
                        access.editaccessMatrix = true;
                    }
                    if (_addList.contains("removeAccessMatrix")) {
                        access.removeaccessMatrix = true;
                    }
                    if (_addList.contains("addAccessMatrix")) {
                        access.addaccessMatrix = true;
                    }

                }
                 
                 
                 if (_deleteList != null) {
                    if (_deleteList.contains("viewAccessMatrix")) {
                        if (_deleteList.contains("EditAccessMatrix") || _deleteList.contains("removeAccessMatrix")) {
                            access.listChannel = false;
                        } else if (_addList.contains("EditAccessMatrix") || _addList.contains("removeAccessMatrix")) {
                            result = "error";
                            message = "Please Select list option for  Access Matrix Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.editaccessMatrix == true || access.removeaccessMatrix == true) {
                            result = "error";
                            message = "Please Select list option for Access Matrix Configuration !!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.viewaccessMatrix = false;
                    }
                    if (_deleteList.contains("EditAccessMatrix")) {
                        access.editaccessMatrix = false;
                    }
                    if (_deleteList.contains("removeAccessMatrix")) {
                        access.removeaccessMatrix = false;
                    }
                     if (_deleteList.contains("addAccessMatrix")) {
                        access.addaccessMatrix = false;
                    }
                }
                
             
                
                //channel
// changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ChannelLists")) {
                        if (_deleteList.contains("AddChannels") || _deleteList.contains("EditChannels")) {
                            access.listChannel = false;
                        } else if (_addList.contains("AddChannels") || _addList.contains("EditChannels")) {
                            result = "error";
                            message = "Please Select list option for Channel Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addChannel == true || access.editChannel == true) {
                            result = "error";
                            message = "Please Select list option for Channel Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listChannel = false;
                    }
                    if (_deleteList.contains("AddChannels")) {
                        access.addChannel = false;
                    }
                    if (_deleteList.contains("EditChannels")) {
                        access.editChannel = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {
                    if (access.listChannel == false) {
                        if (!_addList.contains("ChannelLists")) {
                            if (_addList.contains("AddChannels") || _addList.contains("EditChannels")) {
                                result = "error";
                                message = "Please Select list option for channel managment!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                   log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ChannelLists")) {
                        access.listChannel = true;
                    }
                    if (_addList.contains("AddChannels")) {
                        access.addChannel = true;
                    }
                    if (_addList.contains("EditChannels")) {
                        access.editChannel = true;
                    }

                }
                
                
                
                //operators
                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListOperator")) {
                        if (_deleteList.contains("AddOperator") || _deleteList.contains("EditOperator")) {
                            access.listOperator = false;
                        } else if (_addList.contains("AddOperator") || _addList.contains("EditOperator")) {
                            result = "error";
                            message = "Please Select list option for Operator Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addOperator == true || access.editOperator == true) {
                            result = "error";
                            message = "Please Select list option for Operator Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listOperator = false;
                    }
                    if (_deleteList.contains("AddOperator")) {
                        access.addOperator = false;
                    }
                    if (_deleteList.contains("EditOperator")) {
                        access.editOperator = false;
                    }
//                    if (_deleteList.contains("RemoveOperator")) {
//                        access.removeOperator = false;
//                    }
                    
                }
//                 end amol changes
                if (_addList != null) {
                    if (access.listOperator == false) {
                        if (!_addList.contains("ListOperator")) {
                            if (_addList.contains("AddOperator") || _addList.contains("EditOperator")) {
                                result = "error";
                                message = "Please Select list option for operator managment!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ListOperator")) {
                        access.listOperator = true;
                    }
                    if (_addList.contains("AddOperator")) {
                        access.addOperator = true;
                    }
                    if (_addList.contains("EditOperator")) {
                        access.editOperator = true;
                    }
//                   if (_addList.contains("RemoveOperator")) {
//                        access.removeOperator = true;
//                    } 
                   
                }

                //Error Messages
                if (_addList != null) {
                    if (access.listErrorMessages == false) {
                        if (!_addList.contains("listErrorMessages")) {
                            if (_addList.contains("editErrorMessages")) {
                                result = "error";
                                message = "Please Select list option for Eroor Messages!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("listErrorMessages")) {
                        access.listErrorMessages = true;
                    }
                    if (_addList.contains("editErrorMessages")) {
                        access.editErrorMessages = true;
                    }

                }

                if (_deleteList != null) {
                    if (_deleteList.contains("listErrorMessages")) {
                        if (_deleteList.contains("editErrorMessages")) {
                            access.listErrorMessages = false;
                        } else if (access.editErrorMessages == true) {
                            result = "error";
                            message = "Please Select list option for Eroor Messages!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }

                        if (_deleteList.contains("editErrorMessages")) {
                            access.editErrorMessages = false;
                        }

                    }

                }

                //3. Units
                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListUnits")) {
                        if (_deleteList.contains("AddUnits") || _deleteList.contains("EditUnits") || _deleteList.contains("DeleteUnits")) {
                            access.listUnits = false;
                        } else if (_addList.contains("AddUnits") || _addList.contains("EditUnits") || _addList.contains("DeleteUnits")) {
                            result = "error";
                            message = "Please Select list option for Units Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addUnits == true || access.editUnits == true || access.deleteUnits == true) {
                            result = "error";
                            message = "Please Select list option for Units Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listUnits = false;
                    }
                    if (_deleteList.contains("AddUnits")) {
                        access.addUnits = false;
                    }
                    if (_deleteList.contains("EditUnits")) {
                        access.editUnits = false;
                    }
                    if (_deleteList.contains("DeleteUnits")) {
                        access.deleteUnits = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {
                    if (access.listUnits == false) {
                        if (!_addList.contains("ListUnits")) {
                            if (_addList.contains("AddUnits") || _addList.contains("EditUnits") || _addList.contains("DeleteUnits")) {
                                result = "error";
                                message = "Please Select list option for unit managment!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ListUnits")) {
                        access.listUnits = true;
                    }
                    if (_addList.contains("AddUnits")) {
                        access.addUnits = true;
                    }
                    if (_addList.contains("EditUnits")) {
                        access.editUnits = true;
                    }
                    if (_addList.contains("DeleteUnits")) {
                        access.deleteUnits = true;
                    }

                }

                //    4. Templates :
                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListTemplate")) {
                        if (_deleteList.contains("AddTemplate") || _deleteList.contains("EditTemplate") || _deleteList.contains("RemoveTemplate")) {
                            access.listTemplate = false;
                        } else if (_addList.contains("AddTemplate") || _addList.contains("EditTemplate") || _addList.contains("RemoveTemplate")) {
                            result = "error";
                            message = "Please Select list option for Template Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addTemplate == true || access.editTemplate == true || access.removeTemplate == true) {
                            result = "error";
                            message = "Please Select list option for Template Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listTemplate = false;
                    }
                    if (_deleteList.contains("AddTemplate")) {
                        access.addTemplate = false;
                    }
                    if (_deleteList.contains("EditTemplate")) {
                        access.editTemplate = false;
                    }
                    if (_deleteList.contains("RemoveTemplate")) {
                        access.removeTemplate = false;
                    }
                }
//                 end amol changes

                if (_addList != null) {
                    if (access.listTemplate == false) {
                        if (!_addList.contains("ListTemplate")) {
                            if (_addList.contains("AddTemplate") || _addList.contains("AddTemplate") || _addList.contains("RemoveTemplate")) {
                                result = "error";
                                message = "Please Select list option for Template managment!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ListTemplate")) {
                        access.listTemplate = true;
                    }
                    if (_addList.contains("AddTemplate")) {
                        access.addTemplate = true;
                    }
                    if (_addList.contains("EditTemplate")) {
                        access.editTemplate = true;
                    }
                    if (_addList.contains("RemoveTemplate")) {
                        access.removeTemplate = true;
                    }

                }
                //5. User Password :
                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("UserLists")) {
                        if (_deleteList.contains("AddUsers") || _deleteList.contains("EditUsers") || _deleteList.contains("RemoveUsers")) {
                            access.ListUsers = false;
                        } else if (_addList.contains("AddUsers") || _addList.contains("EditUsers") || _addList.contains("RemoveUsers")) {
                            result = "error";
                            message = "Please Select list option for User Managment!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addUser == true || access.editUser == true || access.removeUser == true) {
                            result = "error";
                            message = "Please Select list option for User Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.ListUsers = false;
                    }
                    if (_deleteList.contains("AddUsers")) {
                        access.addUser = false;
                    }
                    if (_deleteList.contains("EditUsers")) {
                        access.editUser = false;
                    }
                    if (_deleteList.contains("RemoveUsers")) {
                        access.removeUser = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {
                    if (access.ListUsers == false) {
                        if (!_addList.contains("UserLists")) {
                            if (_addList.contains("AddUsers") || _addList.contains("EditUsers") || _addList.contains("RemoveUsers")) {
                                result = "error";
                                message = "Please Select list option for User managment!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("UserLists")) {
                        access.ListUsers = true;
                    }
                    if (_addList.contains("AddUsers")) {
                        access.addUser = true;
                    }
                    if (_addList.contains("EditUsers")) {
                        access.editUser = true;
                    }
                    if (_addList.contains("RemoveUsers")) {
                        access.removeUser = true;
                    }
                }
                //user group
                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListUserGroup")) {
                        if (_deleteList.contains("AddUserGroup") || _deleteList.contains("EditUserGroup") || _deleteList.contains("RemoveUserGroup")) {
                            access.ListUsersGroup = false;
                        } else if (_addList.contains("AddUserGroup") || _addList.contains("EditUserGroup") || _addList.contains("RemoveUserGroup")) {
                            result = "error";
                            message = "Please Select list option for User Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                               log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addUserGroup == true || access.editUserGroup == true || access.removeUserGroup == true) {
                            result = "error";
                            message = "Please Select list option for User Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.ListUsersGroup = false;
                    }
                    if (_deleteList.contains("AddUserGroup")) {
                        access.addUserGroup = false;
                    }
                    if (_deleteList.contains("EditUserGroup")) {
                        access.editUserGroup = false;
                    }
                    if (_deleteList.contains("RemoveUserGroup")) {
                        access.removeUserGroup = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {
                    if (access.ListUsersGroup == false) {
                        if (!_addList.contains("ListUserGroup")) {
                            if (_addList.contains("AddUserGroup") || _addList.contains("EditUserGroup") || _addList.contains("RemoveUserGroup")) {
                                result = "error";
                                message = "Please Select list option for User Group managment!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ListUserGroup")) {
                        access.ListUsersGroup = true;
                    }
                    if (_addList.contains("AddUserGroup")) {
                        access.addUserGroup = true;
                    }
                    if (_addList.contains("EditUserGroup")) {
                        access.editUserGroup = true;
                    }
                    if (_addList.contains("RemoveUserGroup")) {
                        access.removeUserGroup = true;
                    }
                }
                //6. Challenge Response
                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListChallengeResponse")) {
                        if (_deleteList.contains("AddChallengeResponse") || _deleteList.contains("EditChallengeResponse") || _deleteList.contains("RemoveChallengeResponse")) {
                            access.listChallengeResponse = false;
                        } else if (_addList.contains("AddChallengeResponse") || _addList.contains("EditChallengeResponse") || _addList.contains("RemoveChallengeResponse")) {
                            result = "error";
                            message = "Please Select list option for ChallengeResponse Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addChallengeResponse == true || access.editChallengeResponse == true || access.removeChallengeResponse == true) {
                            result = "error";
                            message = "Please Select list option for ChallengeResponse Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listChallengeResponse = false;
                    }
                    if (_deleteList.contains("AddChallengeResponse")) {
                        access.addChallengeResponse = false;
                    }
                    if (_deleteList.contains("EditChallengeResponse")) {
                        access.editChallengeResponse = false;
                    }
                    if (_deleteList.contains("RemoveChallengeResponse")) {
                        access.removeChallengeResponse = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {
                    if (access.listChallengeResponse == false) {
                        if (!_addList.contains("ListChallengeResponse")) {
                            if (_addList.contains("AddChallengeResponse") || _addList.contains("EditChallengeResponse") || _addList.contains("RemoveChallengeResponse")) {
                                result = "error";
                                message = "Please Select list option for Challenge Response managment!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ListChallengeResponse")) {
                        access.listChallengeResponse = true;
                    }
                    if (_addList.contains("AddChallengeResponse")) {
                        access.addChallengeResponse = true;
                    }
                    if (_addList.contains("EditChallengeResponse")) {
                        access.editChallengeResponse = true;
                    }
                    if (_addList.contains("RemoveChallengeResponse")) {
                        access.removeChallengeResponse = true;
                    }
                }
                //7. Trusted Device
                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListTrustedDevice")) {
                        if (_deleteList.contains("AddTrustedDevice") || _deleteList.contains("EditTrustedDevice") || _deleteList.contains("RemoveTrustedDevice")) {
                            access.listTrustedDevice = false;
                        } else if (_addList.contains("AddTrustedDevice") || _addList.contains("EditTrustedDevice") || _addList.contains("RemoveTrustedDevice")) {
                            result = "error";
                            message = "Please Select list option for TrustedDevice Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addTrustedDevice == true || access.editTrustedDevice == true || access.removeDestroyClients == true) {
                            result = "error";
                            message = "Please Select list option for TrustedDevice Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                               log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listTrustedDevice = false;
                    }
                    if (_deleteList.contains("AddTrustedDevice")) {
                        access.addTrustedDevice = false;
                    }
                    if (_deleteList.contains("EditTrustedDevice")) {
                        access.editTrustedDevice = false;
                    }
                    if (_deleteList.contains("RemoveTrustedDevice")) {
                        access.removeDestroyClients = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {
                    if (access.listTrustedDevice == false) {
                        if (!_addList.contains("ListTrustedDevice")) {
                            if (_addList.contains("AddTrustedDevice") || _addList.contains("EditTrustedDevice") || _addList.contains("RemoveTrustedDevice")) {
                                result = "error";
                                message = "Please Select list option for Trusted Device managment!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                   log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ListTrustedDevice")) {
                        access.listTrustedDevice = true;
                    }
                    if (_addList.contains("AddTrustedDevice")) {
                        access.addTrustedDevice = true;
                    }
                    if (_addList.contains("EditTrustedDevice")) {
                        access.editTrustedDevice = true;
                    }
                    if (_addList.contains("RemoveTrustedDevice")) {
                        access.removeDestroyClients = true;
                    }
                }

                //10. Geo - location
                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListGeoLocation")) {
                        if (_deleteList.contains("AddGeoLocation") || _deleteList.contains("EditGeoLocation") || _deleteList.contains("RemoveGeoLocation")) {
                            access.listGeoLocation = false;
                        } else if (_addList.contains("AddGeoLocation") || _addList.contains("EditGeoLocation") || _addList.contains("RemoveGeoLocation")) {
                            result = "error";
                            message = "Please Select list option for Geolocation Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addGeoLocation == true || access.editGeoLocation == true || access.removeGeoLocation == true) {
                            result = "error";
                            message = "Please Select list option for Geolocation Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listGeoLocation = false;
                    }
                    if (_deleteList.contains("AddGeoLocation")) {
                        access.addGeoLocation = false;
                    }
                    if (_deleteList.contains("EditGeoLocation")) {
                        access.editGeoLocation = false;
                    }
                    if (_deleteList.contains("RemoveGeoLocation")) {
                        access.removeGeoLocation = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {
                    if (access.listGeoLocation == false) {
                        if (!_addList.contains("ListGeoLocation")) {
                            if (_addList.contains("AddGeoLocation") || _addList.contains("EditGeoLocation") || _addList.contains("RemoveGeoLocation")) {
                                result = "error";
                                message = "Please Select list option for Geo location managment!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ListGeoLocation")) {
                        access.listGeoLocation = true;
                    }
                    if (_addList.contains("AddGeoLocation")) {
                        access.addGeoLocation = true;
                    }
                    if (_addList.contains("EditGeoLocation")) {
                        access.editGeoLocation = true;
                    }
                    if (_addList.contains("RemoveGeoLocation")) {
                        access.removeGeoLocation = true;
                    }
                }
                //11. System message
                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListSystemMessage")) {
                        if (_deleteList.contains("AddSystemMessage") || _deleteList.contains("EditSystemMessage") || _deleteList.contains("RemoveSystemMessage")) {
                            access.listSystemMessage = false;
                        } else if (_addList.contains("AddSystemMessage") || _addList.contains("EditSystemMessage") || _addList.contains("RemoveSystemMessage")) {
                            result = "error";
                            message = "Please Select list option for SystemMessage Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addSystemMessage == true || access.editSystemMessage == true || access.removeSystemMessage == true) {
                            result = "error";
                            message = "Please Select list option for SystemMessage Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listSystemMessage = false;
                    }
                    if (_deleteList.contains("AddSystemMessage")) {
                        access.addSystemMessage = false;
                    }
                    if (_deleteList.contains("EditSystemMessage")) {
                        access.editSystemMessage = false;
                    }
                    if (_deleteList.contains("RemoveSystemMessage")) {
                        access.removeSystemMessage = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {
                    if (access.listSystemMessage == false) {
                        if (!_addList.contains("ListSystemMessage")) {
                            if (_addList.contains("AddSystemMessage") || _addList.contains("EditSystemMessage") || _addList.contains("RemoveSystemMessage")) {
                                result = "error";
                                message = "Please Select list option for System message managment!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ListSystemMessage")) {
                        access.listSystemMessage = true;
                    }
                    if (_addList.contains("AddSystemMessage")) {
                        access.addSystemMessage = true;
                    }
                    if (_addList.contains("EditSystemMessage")) {
                        access.editSystemMessage = true;
                    }
                    if (_addList.contains("RemoveSystemMessage")) {
                        access.removeSystemMessage = true;
                    }
                }
                //12. SNMP message
                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListSnmpManagement")) {
                        if (_deleteList.contains("AddSnmpManagement") || _deleteList.contains("EditSnmpManagement") || _deleteList.contains("RemoveSnmpManagement")) {
                            access.listSnmpManagement = false;
                        } else if (_addList.contains("AddSnmpManagement") || _addList.contains("EditSnmpManagement") || _addList.contains("RemoveSnmpManagement")) {
                            result = "error";
                            message = "Please Select list option for SnmpManagement Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addSnmpManagement == true || access.editSnmpManagement == true || access.removeSnmpManagement == true) {
                            result = "error";
                            message = "Please Select list option for Snmp Management Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listSnmpManagement = false;
                    }
                    if (_deleteList.contains("AddSnmpManagement")) {
                        access.addSnmpManagement = false;
                    }
                    if (_deleteList.contains("EditSnmpManagement")) {
                        access.editSnmpManagement = false;
                    }
                    if (_deleteList.contains("RemoveSnmpManagement")) {
                        access.removeSnmpManagement = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {
                    if (access.listSnmpManagement == false) {
                        if (!_addList.contains("ListSnmpManagement")) {
                            if (_addList.contains("AddSnmpManagement") || _addList.contains("EditSnmpManagement") || _addList.contains("RemoveSnmpManagement")) {
                                result = "error";
                                message = "Please Select list option for SNMP managment!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ListSnmpManagement")) {
                        access.listSnmpManagement = true;
                    }
                    if (_addList.contains("AddSnmpManagement")) {
                        access.addSnmpManagement = true;
                    }
                    if (_addList.contains("EditSnmpManagement")) {
                        access.editSnmpManagement = true;
                    }
                    if (_addList.contains("RemoveSnmpManagement")) {
                        access.removeSnmpManagement = true;
                    }
                }
                //12. report schedular message
                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListReportSchedulerManagement")) {
                        if (_deleteList.contains("AddReportSchedulerManagement") || _deleteList.contains("EditReportSchedulerManagement") || _deleteList.contains("RemoveReportSchedulerManagement")) {
                            access.listReportSchedulerManagement = false;
                        } else if (_addList.contains("AddReportSchedulerManagement") || _addList.contains("EditReportSchedulerManagement") || _addList.contains("RemoveReportSchedulerManagement")) {
                            result = "error";
                            message = "Please Select list option for ReportSchedularManagement Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addReportSchedulerManagement == true || access.editReportSchedulerManagement == true || access.removeReportSchedulerManagement == true) {
                            result = "error";
                            message = "Please Select list option for ReportSchedularManagement Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listReportSchedulerManagement = false;
                    }
                    if (_deleteList.contains("AddReportSchedulerManagement")) {
                        access.addReportSchedulerManagement = false;
                    }
                    if (_deleteList.contains("EditReportSchedulerManagement")) {
                        access.editReportSchedulerManagement = false;
                    }
                    if (_deleteList.contains("RemoveReportSchedulerManagement")) {
                        access.removeReportSchedulerManagement = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {
                    if (access.listReportSchedulerManagement == false) {
                        if (!_addList.contains("ListReportSchedulerManagement")) {
                            if (_addList.contains("AddReportSchedulerManagement") || _addList.contains("EditReportSchedulerManagement") || _addList.contains("RemoveReportSchedulerManagement")) {
                                result = "error";
                                message = "Please Select list option for SNMP managment!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ListReportSchedulerManagement")) {
                        access.listReportSchedulerManagement = true;
                    }
                    if (_addList.contains("AddReportSchedulerManagement")) {
                        access.addReportSchedulerManagement = true;
                    }
                    if (_addList.contains("EditReportSchedulerManagement")) {
                        access.editReportSchedulerManagement = true;
                    }
                    if (_addList.contains("RemoveReportSchedulerManagement")) {
                        access.removeReportSchedulerManagement = true;
                    }
                }
                //13. Image message
                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListImageManagement")) {
                        if (_deleteList.contains("AddImageManagement") || _deleteList.contains("EditImageManagement") || _deleteList.contains("RemoveImageManagement")) {
                            access.listImageManagement = false;
                        } else if (_addList.contains("AddImageManagement") || _addList.contains("EditImageManagement") || _addList.contains("RemoveImageManagement")) {
                            result = "error";
                            message = "Please Select list option for ImageManagement Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addImageManagement == true || access.editImageManagement == true || access.removeImageManagement == true) {
                            result = "error";
                            message = "Please Select list option for ImageManagement Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listImageManagement = false;
                    }
                    if (_deleteList.contains("AddImageManagement")) {
                        access.addImageManagement = false;
                    }
                    if (_deleteList.contains("EditImageManagement")) {
                        access.editImageManagement = false;
                    }
                    if (_deleteList.contains("RemoveImageManagement")) {
                        access.removeImageManagement = false;
                    }
                }
//                 end amol changes

                if (_addList != null) {
                    if (access.listImageManagement == false) {
                        if (!_addList.contains("ListImageManagement")) {
                            if (_addList.contains("AddImageManagement") || _addList.contains("EditImageManagement") || _addList.contains("RemoveImageManagement")) {
                                result = "error";
                                message = "Please Select list option for Image managment!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ListImageManagement")) {
                        access.listImageManagement = true;
                    }
                    if (_addList.contains("AddImageManagement")) {
                        access.addImageManagement = true;
                    }
                    if (_addList.contains("EditImageManagement")) {
                        access.editImageManagement = true;
                    }
                    if (_addList.contains("RemoveImageManagement")) {
                        access.removeImageManagement = true;
                    }
                }
                if (_addList != null) {
                    if (access.listTwoWayAuthManagement == false) {
                        if (!_addList.contains("ListTwoWayAuthManagement")) {
                            if (_addList.contains("AddTwoWayAuthManagement") || _addList.contains("EditTwoWayAuthManagement") || _addList.contains("RemoveTwoWayAuthManagement")) {
                                result = "error";
                                message = "Please Select list option for Image managment!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ListTwoWayAuthManagement")) {
                        access.listTwoWayAuthManagement = true;
                    }
                    if (_addList.contains("AddTwoWayAuthManagement")) {
                        access.addTwoWayAuthManagement = true;
                    }
                    if (_addList.contains("EditTwoWayAuthManagement")) {
                        access.editTwoWayAuthManagement = true;
                    }
                    if (_addList.contains("RemoveTwoWayAuthManagement")) {
                        access.removeTwoWayAuthManagement = true;
                    }
                }
                if (_deleteList != null) {
                    if (_deleteList.contains("ListTwoWayAuthManagement")) {
                        if (_deleteList.contains("AddTwoWayAuthManagement") || _deleteList.contains("EditTwoWayAuthManagement") || _deleteList.contains("RemoveTwoWayAuthManagement")) {
                            access.listTwoWayAuthManagement = false;
                        } else if (_addList.contains("AddTwoWayAuthManagement") || _addList.contains("EditTwoWayAuthManagement") || _addList.contains("RemoveTwoWayAuthManagement")) {
                            result = "error";
                            message = "Please Select list option for TwoWayAuthManagement Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addTwoWayAuthManagement == true || access.editTwoWayAuthManagement == true || access.removeTwoWayAuthManagement == true) {
                            result = "error";
                            message = "Please Select list option for TwoWayAuthManagement Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                              log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listTwoWayAuthManagement = false;
                    }
                    if (_deleteList.contains("AddTwoWayAuthManagement")) {
                        access.addTwoWayAuthManagement = false;
                    }
                    if (_deleteList.contains("EditTwoWayAuthManagement")) {
                        access.editTwoWayAuthManagement = false;
                    }
                    if (_deleteList.contains("RemoveTwoWayAuthManagement")) {
                        access.removeTwoWayAuthManagement = false;
                    }
                }
                //web watch
                if (_addList != null) {
                    if (access.listWebWatch == false) {
                        if (!_addList.contains("ListWebWatch")) {
                            if (_addList.contains("AddWebWatch") || _addList.contains("EditWebWatch") || _addList.contains("RemoveWebWatch")) {
                                result = "error";
                                message = "Please Select list option for Web Watch managment!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ListWebWatch")) {
                        access.listWebWatch = true;
                    }
                    if (_addList.contains("AddWebWatch")) {
                        access.addWebWatch = true;
                    }
                    if (_addList.contains("EditWebWatch")) {
                        access.editWebWatch = true;
                    }
                    if (_addList.contains("RemoveWebWatch")) {
                        access.removeWebWatch = true;
                    }
                }
                if (_deleteList != null) {
                    if (_deleteList.contains("ListWebWatch")) {
                        if (_deleteList.contains("AddWebWatch") || _deleteList.contains("EditWebWatch") || _deleteList.contains("RemoveWebWatch")) {
                            access.listWebWatch = false;
                        } else if (_addList.contains("AddWebWatch") || _addList.contains("EditWebWatch") || _addList.contains("RemoveWebWatch")) {
                            result = "error";
                            message = "Please Select list option for We0bWatch Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addWebWatch == true || access.editWebWatch == true || access.removeWebWatch == true) {
                            result = "error";
                            message = "Please Select list option for WebWatch Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listWebWatch = false;
                    }
                    if (_deleteList.contains("AddWebWatch")) {
                        access.addWebWatch = false;
                    }
                    if (_deleteList.contains("EditWebWatch")) {
                        access.editWebWatch = false;
                    }
                    if (_deleteList.contains("RemoveWebWatch")) {
                        access.removeWebWatch = false;
                    }
                } //web Resource 
                if (_addList != null) {
                    if (access.listWebResource == false) {
                        if (!_addList.contains("ListWebResource")) {
                            if (_addList.contains("AddWebResource") || _addList.contains("EditWebResource") || _addList.contains("RemoveWebResource")) {
                                result = "error";
                                message = "Please Select list option for Web Watch managment!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                   log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ListWebResource")) {
                        access.listWebResource = true;
                    }
                    if (_addList.contains("AddWebResource")) {
                        access.addWebResource = true;
                    }
                    if (_addList.contains("EditWebResource")) {
                        access.editWebResource = true;
                    }
                    if (_addList.contains("RemoveWebResource")) {
                        access.removeWebResource = true;
                    }
                }
                if (_deleteList != null) {
                    if (_deleteList.contains("ListWebResource")) {
                        if (_deleteList.contains("AddWebResource") || _deleteList.contains("EditWebResource") || _deleteList.contains("RemoveWebResource")) {
                            access.listWebResource = false;
                        } else if (_addList.contains("AddWebResource") || _addList.contains("EditWebResource") || _addList.contains("RemoveWebResource")) {
                            result = "error";
                            message = "Please Select list option for WebResource Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addWebResource == true || access.editWebResource == true || access.removeWebResource == true) {
                            result = "error";
                            message = "Please Select list option for WebResource Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listWebResource = false;
                    }
                    if (_deleteList.contains("AddWebResource")) {
                        access.addWebResource = false;
                    }
                    if (_deleteList.contains("EditWebResource")) {
                        access.editWebResource = false;
                    }
                    if (_deleteList.contains("RemoveWebResource")) {
                        access.removeWebResource = false;
                    }
                }

            } else if (iType == OperatorsManagement.MatrixSystemConfigurations) {
                //1. sms gateway

                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListSmsGateway")) {
                        if (_deleteList.contains("AddSmsGateway") || _deleteList.contains("EditSmsGateway") || _deleteList.contains("RemoveSmsGateway")) {
                            access.listSmsGateway = false;
                        } else if (_addList.contains("AddSmsGateway") || _addList.contains("EditSmsGateway") || _addList.contains("RemoveSmsGateway")) {
                            result = "error";
                            message = "Please Select list option for SmsGateway Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addSmsGateway == true || access.editSmsGateway == true || access.removeSmsGateway == true) {
                            result = "error";
                            message = "Please Select list option for SmsGateway Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listSmsGateway = false;
                    }
                    if (_deleteList.contains("AddSmsGateway")) {
                        access.addSmsGateway = false;
                    }
                    if (_deleteList.contains("EditSmsGateway")) {
                        access.editSmsGateway = false;
                    }
                    if (_deleteList.contains("RemoveSmsGateway")) {
                        access.removeSmsGateway = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {
                    if (access.listSmsGateway == false) {
                        if (!_addList.contains("ListSmsGateway")) {
                            if (_addList.contains("AddSmsGateway") || _addList.contains("EditSmsGateway")
                                    || _addList.contains("RemoveSmsGateway")) {
                                result = "error";
                                message = "Please Select list option for SMS Gateway Configuration!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ListSmsGateway")) {
                        access.listSmsGateway = true;
                    }
                    if (_addList.contains("AddSmsGateway")) {
                        access.addSmsGateway = true;
                    }
                    if (_addList.contains("EditSmsGateway")) {
                        access.editSmsGateway = true;
                    }
                    if (_addList.contains("RemoveSmsGateway")) {
                        access.removeSmsGateway = true;
                    }
                }
                // 2 ussd gateway

                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListUSSDGateway")) {
                        if (_deleteList.contains("AddUSSDGateway") || _deleteList.contains("EditUSSDGateway") || _deleteList.contains("RemoveUSSDGateway")) {
                            access.listUSSDGateway = false;
                        } else if (_addList.contains("AddUSSDGateway") || _addList.contains("EditUSSDGateway") || _addList.contains("RemoveUSSDGateway")) {
                            result = "error";
                            message = "Please Select list option for USSDGateway Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addUSSDGateway == true || access.editUSSDGateway == true || access.removeUSSDGateway == true) {
                            result = "error";
                            message = "Please Select list option for USSDGateway Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                               log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listUSSDGateway = false;
                    }
                    if (_deleteList.contains("AddUSSDGateway")) {
                        access.addUSSDGateway = false;
                    }
                    if (_deleteList.contains("EditUSSDGateway")) {
                        access.editUSSDGateway = false;
                    }
                    if (_deleteList.contains("RemoveUSSDGateway")) {
                        access.removeUSSDGateway = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {
                    if (access.listUSSDGateway == false) {
                        if (!_addList.contains("ListUSSDGateway")) {
                            if (_addList.contains("AddUSSDGateway") || _addList.contains("EditUSSDGateway")
                                    || _addList.contains("RemoveUSSDGateway")) {
                                result = "error";
                                message = "Please Select list option for USSD Configuration!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ListUSSDGateway")) {
                        access.listUSSDGateway = true;
                    }
                    if (_addList.contains("AddUSSDGateway")) {
                        access.addUSSDGateway = true;
                    }
                    if (_addList.contains("EditUSSDGateway")) {
                        access.editUSSDGateway = true;
                    }
                    if (_addList.contains("RemoveUSSDGateway")) {
                        access.removeUSSDGateway = true;
                    }
                }
                // 3 voice gateway

                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListVOICEGateway")) {
                        if (_deleteList.contains("AddVOICEGateway") || _deleteList.contains("EditVOICEGateway") || _deleteList.contains("RemoveVOICEGateway")) {
                            access.listVOICEGateway = false;
                        } else if (_addList.contains("AddVOICEGateway") || _addList.contains("EditVOICEGateway") || _addList.contains("RemoveVOICEGateway")) {
                            result = "error";
                            message = "Please Select list option for VOICEGateway Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addVOICEGateway == true || access.editVOICEGateway == true || access.removeVOICEGateway == true) {
                            result = "error";
                            message = "Please Select list option for VOICEGateway Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listVOICEGateway = false;
                    }
                    if (_deleteList.contains("AddVOICEGateway")) {
                        access.addVOICEGateway = false;
                    }
                    if (_deleteList.contains("EditVOICEGateway")) {
                        access.editVOICEGateway = false;
                    }
                    if (_deleteList.contains("RemoveVOICEGateway")) {
                        access.removeVOICEGateway = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {
                    if (access.listVOICEGateway == false) {
                        if (!_addList.contains("ListVOICEGateway")) {
                            if (_addList.contains("AddVOICEGateway") || _addList.contains("EditVOICEGateway")
                                    || _addList.contains("RemoveVOICEGateway")) {
                                result = "error";
                                message = "Please Select list option for VOICE Configuration!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ListVOICEGateway")) {
                        access.listVOICEGateway = true;
                    }
                    if (_addList.contains("AddVOICEGateway")) {
                        access.addVOICEGateway = true;
                    }
                    if (_addList.contains("EditVOICEGateway")) {
                        access.editVOICEGateway = true;
                    }
                    if (_addList.contains("RemoveVOICEGateway")) {
                        access.removeVOICEGateway = true;
                    }
                }
                // 4 email gateway

                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListEMAILGateway")) {
                        if (_deleteList.contains("AddEMAILGateway") || _deleteList.contains("EditEMAILGateway") || _deleteList.contains("RemoveEMAILGateway")) {
                            access.listEMAILGateway = false;
                        } else if (_addList.contains("AddEMAILGateway") || _addList.contains("EditEMAILGateway") || _addList.contains("RemoveEMAILGateway")) {
                            result = "error";
                            message = "Please Select list option for EMAILGateway Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addEMAILGateway == true || access.editEMAILGateway == true || access.removeEMAILGateway == true) {
                            result = "error";
                            message = "Please Select list option for EMAILGateway Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listEMAILGateway = false;
                    }
                    if (_deleteList.contains("AddEMAILGateway")) {
                        access.addEMAILGateway = false;
                    }
                    if (_deleteList.contains("EditEMAILGateway")) {
                        access.editEMAILGateway = false;
                    }
                    if (_deleteList.contains("RemoveEMAILGateway")) {
                        access.removeEMAILGateway = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {
                    if (access.listEMAILGateway == false) {
                        if (!_addList.contains("ListEMAILGateway")) {
                            if (_addList.contains("AddEMAILGateway") || _addList.contains("EditEmailGateway")
                                    || _addList.contains("RemoveEmailGateway")) {
                                result = "error";
                                message = "Please Select list option for Email Configuration!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                   log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ListEMAILGateway")) {
                        access.listEMAILGateway = true;
                    }
                    if (_addList.contains("AddEMAILGateway")) {
                        access.addEMAILGateway = true;
                    }
                    if (_addList.contains("EditEMAILGateway")) {
                        access.editEMAILGateway = true;
                    }
                    if (_addList.contains("RemoveEMAILGateway")) {
                        access.removeEMAILGateway = true;
                    }
                }
                // 5 Billing manager gateway

                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListBillingManager")) {
                        if (_deleteList.contains("AddBillingManager") || _deleteList.contains("EditBillingManagerGateway") || _deleteList.contains("RemoveBillingManagerGateway")) {
                            access.listBillingManager = false;
                        } else if (_addList.contains("AddBillingManager") || _addList.contains("EditBillingManagerGateway") || _addList.contains("RemoveBillingManagerGateway")) {
                            result = "error";
                            message = "Please Select list option for BillingManagerGatewayGateway Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addBillingManager == true || access.editBillingManager == true || access.removeBillingManager == true) {
                            result = "error";
                            message = "Please Select list option for BillingManagerGatewayGateway Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listBillingManager = false;
                    }
                    if (_deleteList.contains("AddBillingManager")) {
                        access.addBillingManager = false;
                    }
                    if (_deleteList.contains("EditBillingManagerGateway")) {
                        access.editBillingManager = false;
                    }
                    if (_deleteList.contains("RemoveBillingManagerGateway")) {
                        access.removeBillingManager = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {
                    if (access.listBillingManager == false) {
                        if (!_addList.contains("ListBillingManager")) {
                            if (_addList.contains("AddBillingManager") || _addList.contains("EditBillingManagerGateway")
                                    || _addList.contains("RemoveBillingManagerGateway")) {
                                result = "error";
                                message = "Please Select list option for Billing Manager Configuration!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }

                    if (_addList.contains("ListBillingManager")) {
                        access.listBillingManager = true;
                    }
                    if (_addList.contains("AddBillingManager")) {
                        access.addBillingManager = true;
                    }
                    if (_addList.contains("EditBillingManagerGateway")) {
                        access.editBillingManager = true;
                    }
                    if (_addList.contains("RemoveBillingManagerGateway")) {
                        access.removeBillingManager = true;
                    }
                }
                // 5 push gateway

                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListPushGatewayGateway")) {
                        if (_deleteList.contains("AddPushGateway") || _deleteList.contains("EditPushGateway") || _deleteList.contains("RemovePushGateway")) {
                            access.listPushGateway = false;
                        } else if (_addList.contains("AddPushGateway") || _addList.contains("EditPushGateway") || _addList.contains("RemovePushGateway")) {
                            result = "error";
                            message = "Please Select list option for PushGateway Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addPushGateway == true || access.editPushGateway == true || access.removePushGateway == true) {
                            result = "error";
                            message = "Please Select list option for PushGateway Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listPushGateway = false;
                    }
                    if (_deleteList.contains("AddPushGateway")) {
                        access.addPushGateway = false;
                    }
                    if (_deleteList.contains("EditPushGateway")) {
                        access.editPushGateway = false;
                    }
                    if (_deleteList.contains("RemovePushGateway")) {
                        access.removePushGateway = false;
                    }
                }

                //for fax
                if (_deleteList != null) {
                    if (_deleteList.contains("ListFax")) {
                        if (_deleteList.contains("AddFax") || _deleteList.contains("EditFax") || _deleteList.contains("RemoveFax")) {
                            access.listPushGateway = false;
                        } else if (access.addFAXGateway == true || access.editFAXGateway == true || access.removeFAXGateway == true) {
                            result = "error";
                            message = "Please Select list option for PushGateway Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listFAXGateway = false;
                    }
                    if (_deleteList.contains("AddFax")) {
                        access.addFAXGateway = false;
                    }
                    if (_deleteList.contains("EditFax")) {
                        access.editFAXGateway = false;
                    }
                    if (_deleteList.contains("RemoveFax")) {
                        access.removeFAXGateway = false;
                    }
                }

//                 end amol changes
                if (_addList != null) {
                    if (access.listPushGateway == false) {
                        if (!_addList.contains("ListPushGatewayGateway")) {
                            if (_addList.contains("AddPushGatewayGateway") || _addList.contains("EditPushGatewayGateway")
                                    || _addList.contains("RemovePushGatewayGateway")) {
                                result = "error";
                                message = "Please Select list option for Push GateWay Configuration!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ListPushGatewayGateway")) {
                        access.listPushGateway = true;
                    }
                    if (_addList.contains("AddPushGatewayGateway")) {
                        access.addPushGateway = true;
                    }
                    if (_addList.contains("EditPushGatewayGateway")) {
                        access.editPushGateway = true;
                    }
                    if (_addList.contains("RemovePushGatewayGateway")) {
                        access.removePushGateway = true;
                    }
                }

                if (_addList != null) {
                    if (access.listFAXGateway == false) {
                        if (!_addList.contains("ListFax")) {
                            if (_addList.contains("AddFax") || _addList.contains("EditFax")
                                    || _addList.contains("RemoveFax")) {
                                result = "error";
                                message = "Please Select list option for Push GateWay Configuration!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ListFax")) {
                        access.listFAXGateway = true;
                    }
                    if (_addList.contains("AddFax")) {
                        access.addFAXGateway = true;
                    }
                    if (_addList.contains("EditFax")) {
                        access.editFAXGateway = true;
                    }
                    if (_addList.contains("RemoveFax")) {
                        access.removeFAXGateway = true;
                    }
                }

                // 6 User Source Configuration
                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListUserSourceSettings")) {
                        if (_deleteList.contains("AddUserSourceSettings") || _deleteList.contains("EditUserSourceSettings") || _deleteList.contains("RemoveUserSourceSettings")) {
                            access.listUserSourceSettings = false;
                        } else if (_addList.contains("AddUserSourceSettings") || _addList.contains("EditUserSourceSettings") || _addList.contains("RemoveUserSourceSettings")) {
                            result = "error";
                            message = "Please Select list option for UserSourceSettings Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addUserSourceSettings == true || access.editUserSourceSettings == true || access.removeUserSourceSettings == true) {
                            result = "error";
                            message = "Please Select list option for UserSourceSettingsGateway Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listUserSourceSettings = false;
                    }
                    if (_deleteList.contains("AddUserSourceSettings")) {
                        access.addUserSourceSettings = false;
                    }
                    if (_deleteList.contains("EditUserSourceSettings")) {
                        access.editUserSourceSettings = false;
                    }
                    if (_deleteList.contains("RemoveUserSourceSettings")) {
                        access.removeUserSourceSettings = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {
                    if (access.listUserSourceSettings == false) {
                        if (!_addList.contains("ListUserSourceSettings")) {
                            if (_addList.contains("AddUserSourceSettings") || _addList.contains("EditUserSourceSettings")
                                    || _addList.contains("RemoveUserSourceSettings")) {
                                result = "error";
                                message = "Please Select list option for User Source Setting Configuration!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ListUserSourceSettings")) {
                        access.listUserSourceSettings = true;
                    }
                    if (_addList.contains("AddUserSourceSettings")) {
                        access.addUserSourceSettings = true;
                    }
                    if (_addList.contains("EditUserSourceSettings")) {
                        access.editUserSourceSettings = true;
                    }
                    if (_addList.contains("RemoveUserSourceSettings")) {
                        access.removeUserSourceSettings = true;
                    }
                }
                // 7 Password Policy Configuration

                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListPasswordPolicySettings")) {
                        if (_deleteList.contains("AddPasswordPolicySettings") || _deleteList.contains("EditPasswordPolicySettings") || _deleteList.contains("RemovePasswordPolicySettings")) {
                            access.listPasswordPolicySettings = false;
                        } else if (_addList.contains("AddPasswordPolicySettings") || _addList.contains("EditPasswordPolicySettings") || _addList.contains("RemovePasswordPolicySettings")) {
                            result = "error";
                            message = "Please Select list option for PasswordPolicySettings Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addPasswordPolicySettings == true || access.editPasswordPolicySettings == true || access.removePasswordPolicySettings == true) {
                            result = "error";
                            message = "Please Select list option for PasswordPolicySettingsGateway Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listPasswordPolicySettings = false;
                    }
                    if (_deleteList.contains("AddPasswordPolicySettings")) {
                        access.addPasswordPolicySettings = false;
                    }
                    if (_deleteList.contains("EditPasswordPolicySettings")) {
                        access.editPasswordPolicySettings = false;
                    }
                    if (_deleteList.contains("RemovePasswordPolicySettings")) {
                        access.removePasswordPolicySettings = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {

                    if (access.listPasswordPolicySettings == false) {
                        if (!_addList.contains("ListPasswordPolicySettings")) {
                            if (_addList.contains("AddPasswordPolicySettings") || _addList.contains("EditPasswordPolicySettings")
                                    || _addList.contains("RemovePasswordPolicySettings")) {
                                result = "error";
                                message = "Please Select list option for Password Policy Setting Configuration!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("PasswordPolicySettings")) {
                        access.listPasswordPolicySettings = true;
                    }
                    if (_addList.contains("AddPasswordPolicySettings")) {
                        access.addPasswordPolicySettings = true;
                    }
                    if (_addList.contains("EditPasswordPolicySettings")) {
                        access.editPasswordPolicySettings = true;
                    }
                    if (_addList.contains("RemovePasswordPolicySettings")) {
                        access.removePasswordPolicySettings = true;
                    }
                }

                // 8 Channel Profile Configuration
                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListChannelProfileSettings")) {
                        if (_deleteList.contains("AddChannelProfileSettings") || _deleteList.contains("EditChannelProfileSettings") || _deleteList.contains("RemoveChannelProfileSettings")) {
                            access.listChannelProfileSettings = false;
                        } else if (_addList.contains("AddChannelProfileSettings") || _addList.contains("EditChannelProfileSettings") || _addList.contains("RemoveChannelProfileSettings")) {
                            result = "error";
                            message = "Please Select list option for ChannelProfileSettings Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addChannelProfileSettings == true || access.editChannelProfileSettings == true || access.removeChannelProfileSettings == true) {
                            result = "error";
                            message = "Please Select list option for ChannelProfileSettings Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listChannelProfileSettings = false;
                    }
                    if (_deleteList.contains("AddChannelProfileSettings")) {
                        access.addChannelProfileSettings = false;
                    }
                    if (_deleteList.contains("EditChannelProfileSettings")) {
                        access.editChannelProfileSettings = false;
                    }
                    if (_deleteList.contains("RemoveChannelProfileSettings")) {
                        access.removeChannelProfileSettings = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {

                    if (access.listChannelProfileSettings == false) {
                        if (!_addList.contains("ListChannelProfileSettings")) {
                            if (_addList.contains("AddChannelProfileSettings") || _addList.contains("EditChannelProfileSettings")
                                    || _addList.contains("RemoveChannelProfileSettings")) {
                                result = "error";
                                message = "Please Select list option for Channel Profile Setting Configuration!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ListChannelProfileSettings")) {
                        access.listChannelProfileSettings = true;
                    }
                    if (_addList.contains("AddChannelProfileSettings")) {
                        access.addChannelProfileSettings = true;
                    }
                    if (_addList.contains("EditChannelProfileSettings")) {
                        access.editChannelProfileSettings = true;
                    }
                    if (_addList.contains("RemoveChannelProfileSettings")) {
                        access.removeChannelProfileSettings = true;
                    }
                }

                // 9 OtpTokensSettings
                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListOtpTokensSettings")) {
                        if (_deleteList.contains("AddOtpTokensSettings") || _deleteList.contains("EditOtpTokensSettings") || _deleteList.contains("RemoveOtpTokensSettings")) {
                            access.listOtpTokensSettings = false;
                        } else if (_addList.contains("AddOtpTokensSettings") || _addList.contains("EditOtpTokensSettings") || _addList.contains("RemoveOtpTokensSettings")) {
                            result = "error";
                            message = "Please Select list option for OtpTokensSettings Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                               log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addOtpTokensSettings == true || access.editOtpTokensSettings == true || access.removeOtpTokensSettings == true) {
                            result = "error";
                            message = "Please Select list option for OtpTokensSettings Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listOtpTokensSettings = false;
                    }
                    if (_deleteList.contains("ListOtpTokensSettings")) {
                        access.addOtpTokensSettings = false;
                    }
                    if (_deleteList.contains("EditOtpTokensSettings")) {
                        access.editOtpTokensSettings = false;
                    }
                    if (_deleteList.contains("RemoveOtpTokensSettings")) {
                        access.removeOtpTokensSettings = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {
                    if (access.listOtpTokensSettings == false) {
                        if (!_addList.contains("ListOtpTokensSettings")) {
                            if (_addList.contains("AddOtpTokensSettings") || _addList.contains("EditOtpTokensSettings")
                                    || _addList.contains("RemoveOtpTokensSettings")) {
                                result = "error";
                                message = "Please Select list option for OTP Token Setting!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }

                    if (_addList.contains("ListOtpTokensSettings")) {
                        access.listOtpTokensSettings = true;
                    }
                    if (_addList.contains("AddOtpTokensSettings")) {
                        access.addOtpTokensSettings = true;
                    }
                    if (_addList.contains("EditOtpTokensSettings")) {
                        access.editOtpTokensSettings = true;
                    }
                    if (_addList.contains("RemoveOtpTokensSettings")) {
                        access.removeOtpTokensSettings = true;
                    }
                }

                // 11 CertificateSettings
                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListCertificateSettings")) {
                        if (_deleteList.contains("AddCertificateSettings") || _deleteList.contains("EditCertificateSettings") || _deleteList.contains("RemoveCertificateSettings")) {
                            access.listCertificateSettings = false;
                        } else if (_addList.contains("AddCertificateSettings") || _addList.contains("EditCertificateSettings") || _addList.contains("RemoveCertificateSettings")) {
                            result = "error";
                            message = "Please Select list option for Certificate Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addCertificateSettings == true || access.editCertificateSettings == true || access.removeCertificateSettings == true) {
                            result = "error";
                            message = "Please Select list option for Certificate  Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listCertificateSettings = false;
                    }
                    if (_deleteList.contains("AddCertificateSettings")) {
                        access.addCertificateSettings = false;
                    }
                    if (_deleteList.contains("EditCertificateSettings")) {
                        access.editCertificateSettings = false;
                    }
                    if (_deleteList.contains("RemoveCertificateSettings")) {
                        access.removeCertificateSettings = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {

                    if (access.listCertificateSettings == false) {
                        if (!_addList.contains("ListCertificateSettings")) {
                            if (_addList.contains("AddCertificateSettings") || _addList.contains("EditCertificateSettings")
                                    || _addList.contains("RemoveCertificateSettings")) {
                                result = "error";
                                message = "Please Select list option for Certificate Setting!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ListCertificateSettings")) {
                        access.listCertificateSettings = true;
                    }
                    if (_addList.contains("AddCertificateSettings")) {
                        access.addCertificateSettings = true;
                    }
                    if (_addList.contains("EditCertificateSettings")) {
                        access.editCertificateSettings = true;
                    }
                    if (_addList.contains("RemoveCertificateSettings")) {
                        access.removeCertificateSettings = true;
                    }
                }
                // 12 Mobile Trust

                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListMobileTrustSettings")) {
                        if (_deleteList.contains("AddMobileTrustSettings") || _deleteList.contains("EditMobileTrustSettings") || _deleteList.contains("RemoveMobileTrustSettings")) {
                            access.listMobileTrustSettings = false;
                        } else if (_addList.contains("AddMobileTrustSettings") || _addList.contains("EditMobileTrustSettings") || _addList.contains("RemoveMobileTrustSettings")) {
                            result = "error";
                            message = "Please Select list option for MobileTrust Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                               log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addMobileTrustSettings == true || access.editMobileTrustSettings == true || access.removeMobileTrustSettings == true) {
                            result = "error";
                            message = "Please Select list option for MobileTrust Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listMobileTrustSettings = false;
                    }
                    if (_deleteList.contains("AddMobileTrustSettings")) {
                        access.addMobileTrustSettings = false;
                    }
                    if (_deleteList.contains("EditMobileTrustSettings")) {
                        access.editMobileTrustSettings = false;
                    }
                    if (_deleteList.contains("RemoveMobileTrustSettings")) {
                        access.removeMobileTrustSettings = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {

                    if (access.listMobileTrustSettings == false) {
                        if (!_addList.contains("ListMobileTrustSettings")) {
                            if (_addList.contains("AddMobileTrustSettings") || _addList.contains("EditMobileTrustSettings")
                                    || _addList.contains("RemoveMobileTrustSettings")) {
                                result = "error";
                                message = "Please Select list option for Mobile Trust Settings!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("ListMobileTrustSettings")) {
                        access.listMobileTrustSettings = true;
                    }
                    if (_addList.contains("AddMobileTrustSettings")) {
                        access.addMobileTrustSettings = true;
                    }
                    if (_addList.contains("EditMobileTrustSettings")) {
                        access.editMobileTrustSettings = true;
                    }
                    if (_addList.contains("RemoveMobileTrustSettings")) {
                        access.removeMobileTrustSettings = true;
                    }
                }
                // 13 Radius

                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListRadiusConfigSettings")) {
                        if (_deleteList.contains("AddRadiusConfigSettings") || _deleteList.contains("EditRadiusConfigSettings") || _deleteList.contains("RemoveRadiusConfigSettings")) {
                            access.listRadiusConfigSettings = false;
                        } else if (_addList.contains("AddRadiusConfigSettings") || _addList.contains("EditRadiusConfigSettings") || _addList.contains("RemoveRadiusConfigSettings")) {
                            result = "error";
                            message = "Please Select list option for Radius Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addRadiusConfigSettings == true || access.editRadiusConfigSettings == true || access.removeRadiusConfigSettings == true) {
                            result = "error";
                            message = "Please Select list option for Radius Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listRadiusConfigSettings = false;
                    }
                    if (_deleteList.contains("AddRadiusConfigSettings")) {
                        access.addRadiusConfigSettings = false;
                    }
                    if (_deleteList.contains("EditRadiusConfigSettings")) {
                        access.editRadiusConfigSettings = false;
                    }
                    if (_deleteList.contains("RemoveRadiusConfigSettings")) {
                        access.removeRadiusConfigSettings = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {
                    if (access.listRadiusConfigSettings == false) {
                        if (!_addList.contains("ListRadiusConfigSettings")) {
                            if (_addList.contains("AddRadiusConfigSettings") || _addList.contains("EditRadiusConfigSettings")
                                    || _addList.contains("RemoveRadiusConfigSettings")) {
                                result = "error";
                                message = "Please Select list option for Radius Configuration Settings !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }

                    if (_addList.contains("ListRadiusConfigSettings")) {
                        access.listRadiusConfigSettings = true;
                    }
                    if (_addList.contains("AddRadiusConfigSettings")) {
                        access.addRadiusConfigSettings = true;
                    }
                    if (_addList.contains("EditRadiusConfigSettings")) {
                        access.editRadiusConfigSettings = true;
                    }
                    if (_addList.contains("RemoveRadiusConfigSettings")) {
                        access.removeRadiusConfigSettings = true;
                    }
                }
                // 14 Global Settings

                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListGlobalSettings")) {
                        if (_deleteList.contains("AddGlobalSettings") || _deleteList.contains("EditGlobalSettings") || _deleteList.contains("RemoveGlobalSettings")) {
                            access.listGlobalSettings = false;
                        } else if (_addList.contains("AddGlobalSettings") || _addList.contains("EditGlobalSettings") || _addList.contains("RemoveGlobalSettings")) {
                            result = "error";
                            message = "Please Select list option for Global Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                               log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addGlobalSettings == true || access.editGlobalSettings == true || access.removeGlobalSettings == true) {
                            result = "error";
                            message = "Please Select list option for Global Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listGlobalSettings = false;
                    }
                    if (_deleteList.contains("AddGlobalSettings")) {
                        access.addGlobalSettings = false;
                    }
                    if (_deleteList.contains("EditGlobalSettings")) {
                        access.editGlobalSettings = false;
                    }
                    if (_deleteList.contains("RemoveGlobalSettings")) {
                        access.removeGlobalSettings = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {
                    if (access.listGlobalSettings == false) {
                        if (!_addList.contains("ListGlobalSettings")) {
                            if (_addList.contains("AddGlobalSettings") || _addList.contains("EditGlobalSettings")
                                    || _addList.contains("RemoveGlobalSettings")) {
                                result = "error";
                                message = "Please Select list option for Global Settings!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }

                    if (_addList.contains("ListGlobalSettings")) {
                        access.listGlobalSettings = true;
                    }
                    if (_addList.contains("AddGlobalSettings")) {
                        access.addGlobalSettings = true;
                    }
                    if (_addList.contains("EditGlobalSettings")) {
                        access.editGlobalSettings = true;
                    }
                    if (_addList.contains("RemoveGlobalSettings")) {
                        access.removeGlobalSettings = true;
                    }
                }

                // 15 Epin Settings
                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("ListEpinConfigSettings")) {
                        if (_deleteList.contains("AddEpinConfigSettings") || _deleteList.contains("EditEpinConfigSettings") || _deleteList.contains("RemoveEpinConfigSettings")) {
                            access.listEpinConfigSettings = false;
                        } else if (_addList.contains("AddEpinConfigSettings") || _addList.contains("EditEpinConfigSettings") || _addList.contains("RemoveEpinConfigSettings")) {
                            result = "error";
                            message = "Please Select list option for Epin Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addEpinConfigSettings == true || access.editEpinConfigSettings == true || access.removeEpinConfigSettings == true) {
                            result = "error";
                            message = "Please Select list option for Epin Global Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listEpinConfigSettings = false;
                    }
                    if (_deleteList.contains("AddEpinConfigSettings")) {
                        access.addEpinConfigSettings = false;
                    }
                    if (_deleteList.contains("EditEpinConfigSettings")) {
                        access.editEpinConfigSettings = false;
                    }
                    if (_deleteList.contains("RemoveEpinConfigSettings")) {
                        access.removeEpinConfigSettings = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {

                    if (access.listEpinConfigSettings == false) {
                        if (!_addList.contains("ListEpinConfigSettings")) {
                            if (_addList.contains("AddEpinConfigSettings") || _addList.contains("EditEpinConfigSettings")
                                    || _addList.contains("RemoveEpinConfigSettings")) {
                                result = "error";
                                message = "Please Select list option for Epin Settings !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                   log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }

                    if (_addList.contains("ListEpinConfigSettings")) {
                        access.listEpinConfigSettings = true;
                    }
                    if (_addList.contains("AddEpinConfigSettings")) {
                        access.addEpinConfigSettings = true;
                    }
                    if (_addList.contains("EditEpinConfigSettings")) {
                        access.editEpinConfigSettings = true;
                    }
                    if (_addList.contains("RemoveEpinConfigSettings")) {
                        access.removeEpinConfigSettings = true;
                    }
                }

                //Image
                if (_deleteList != null) {
                    if (_deleteList.contains("ListImageSettings")) {
                        if (_deleteList.contains("AddImageConfigSettings") || _deleteList.contains("EditImageConfigSettings") || _deleteList.contains("RemoveImageConfigSettings")) {
                            access.listImageSettings = false;
                        } else if (_addList.contains("AddImageConfigSettings") || _addList.contains("EditImageConfigSettings") || _addList.contains("RemoveImageConfigSettings")) {
                            result = "error";
                            message = "Please Select list option for Image Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.addEpinConfigSettings == true || access.editEpinConfigSettings == true || access.removeEpinConfigSettings == true) {
                            result = "error";
                            message = "Please Select list option for Epin Configuration!!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listImageSettings = false;
                    }
                    if (_deleteList.contains("AddImageConfigSettings")) {
                        access.addImageConfigSettings = false;
                    }
                    if (_deleteList.contains("EditImageConfigSettings")) {
                        access.editImageConfigSettings = false;
                    }
                    if (_deleteList.contains("RemoveImageConfigSettings")) {
                        access.removeImageConfigSettings = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {

                    if (access.listImageSettings == false) {
                        if (!_addList.contains("ListImageSettings")) {
                            if (_addList.contains("AddImageConfigSettings") || _addList.contains("EditImageConfigSettings")
                                    || _addList.contains("RemoveImageConfigSettings")) {
                                result = "error";
                                message = "Please Select list option for Epin Settings !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }

                    if (_addList.contains("ListImageSettings")) {
                        access.listImageSettings = true;
                    }
                    if (_addList.contains("AddImageConfigSettings")) {
                        access.addImageConfigSettings = true;
                    }
                    if (_addList.contains("EditImageConfigSettings")) {
                        access.editImageConfigSettings = true;
                    }
                    if (_addList.contains("RemoveImageConfigSettings")) {
                        access.removeImageConfigSettings = true;
                    }
                }

            } else if (iType == OperatorsManagement.MatrixSystemSecurity) {
                if (_addList != null) {
                    if (_addList.contains("addOOBAssignToken")) {
                        access.addoobAssignToken = true;
                    }
                    if (_addList.contains("addChangeTokenType")) {
                        access.addoobChangetokenType = true;
                    }
                    if (_addList.contains("addOOBmarkActive")) {
                        access.addoobMarkAsActive = true;
                    }
                    if (_addList.contains("OOBmarksuspend")) {
                        access.addoobMarkAsSuspended = true;
                    }
                    if (_addList.contains("OOBverifyOtp")) {
                        access.addoobVerifyOTP = true;
                    }
                    if (_addList.contains("OOBunassignToken")) {
                        access.addoobUnAssignToken = true;
                    }
                    if (_addList.contains("addSoftAssignToken")) {
                        access.addsoftAssignToken = true;
                    }
                    if (_addList.contains("addSoftResendCode")) {
                        access.addsoftResendActCode = true;
                    }
                    if (_addList.contains("addSWmarkActive")) {
                        access.addsoftMarkAsActive = true;
                    }
                    if (_addList.contains("addSoftMarkActive")) {
                        access.addsoftMarkSuspende = true;
                    }
                    if (_addList.contains("addSoftOTP")) {
                        access.addsoftVOneTimePass = true;
                    }
                    if (_addList.contains("addSWUnAssignTok")) {
                        access.addsoftUnAssignToken = true;
                    }
                    if (_addList.contains("addHassignToken")) {
                        access.addhardAssignToken = true;
                    }
                    if (_addList.contains("HWmarkActive")) {
                        access.addhardMarkAsActive = true;
                    }
                    if (_addList.contains("AddHWsuspend")) {
                        access.addHWMarkAsSuspended = true;
                    }
                    if (_addList.contains("AddHWverOTP")) {
                        access.addhardVerifyOTP = true;
                    }
                    if (_addList.contains("addHWrec")) {
                        access.addHWresyncToken = true;
                    }
                    if (_addList.contains("addHWrepToken")) {
                        access.addHWReplaceToken = true;
                    }
                    if (_addList.contains("addHWpukToken")) {
                        access.addHWPukCode = true;
                    }
                    if (_addList.contains("addHWmarkLost")) {
                        access.addHWMarkAsLost = true;
                    }
                    if (_addList.contains("addHWunassign")) {
                        access.addHwUnAssignToken = true;
                    }
                }
                if(_deleteList != null) {
                    if (_deleteList.contains("addOOBAssignToken")) {
                        access.addoobAssignToken = false;
                    }
                    if (_deleteList.contains("addChangeTokenType")) {
                        access.addoobChangetokenType = false;
                    }
                    if (_deleteList.contains("addOOBmarkActive")) {
                        access.addoobMarkAsActive = false;
                    }
                    if (_deleteList.contains("OOBmarksuspend")) {
                        access.addoobMarkAsSuspended = false;
                    }
                    if (_deleteList.contains("OOBverifyOtp")) {
                        access.addoobVerifyOTP = false;
                    }
                    if (_deleteList.contains("OOBunassignToken")) {
                        access.addoobUnAssignToken = false;
                    }
                    if (_deleteList.contains("addSoftAssignToken")) {
                        access.addsoftAssignToken = false;
                    }
                    if (_deleteList.contains("addSoftResendCode")) {
                        access.addsoftResendActCode = false;
                    }
                    if (_deleteList.contains("addSWmarkActive")) {
                        access.addsoftMarkAsActive = false;
                    }
                    if (_deleteList.contains("addSoftMarkActive")) {
                        access.addsoftMarkSuspende = false;
                    }
                    if (_deleteList.contains("addSoftOTP")) {
                        access.addsoftVOneTimePass = false;
                    }
                    if (_deleteList.contains("addSWUnAssignTok")) {
                        access.addsoftUnAssignToken = false;
                    }
                    if (_deleteList.contains("remSWUnassignTok")) {
                        access.addsoftUnAssignToken = false;
                    }
                    if (_deleteList.contains("addHassignToken")) {
                        access.addhardAssignToken = false;
                    }
                    if (_deleteList.contains("HWmarkActive")) {
                        access.addhardMarkAsActive = false;
                    }
                    if (_deleteList.contains("AddHWsuspend")) {
                        access.addHWMarkAsSuspended = false;
                    }
                    if (_deleteList.contains("AddHWverOTP")) {
                        access.addhardVerifyOTP = false;
                    }
                    if (_deleteList.contains("addHWrec")) {
                        access.addHWresyncToken = false;
                    }
                    if (_deleteList.contains("addHWrepToken")) {
                        access.addHWReplaceToken = false;
                    }
                    if (_deleteList.contains("addHWpukToken")) {
                        access.addHWPukCode = false;
                    }
                    if (_deleteList.contains("addHWmarkLost")) {
                        access.addHWMarkAsLost = false;
                    }
                    if (_deleteList.contains("addHWunassign")) {
                        access.addHwUnAssignToken = false;
                    }
                }
            } else if (iType == OperatorsManagement.MatrixSystemReport) {
                //1.  user report
                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("listUserReport")) {
                        if (_deleteList.contains("viewUserReport") || _deleteList.contains("downloadUserReport")) {
                            access.listUserReport = false;
                        } else if (_addList.contains("viewUserReport") || _addList.contains("downloadUserReport")) {
                            result = "error";
                            message = "Please Select list option for User report !!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                               log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.viewUserReport == true || access.downloadUserReport == true) {
                            result = "error";
                            message = "Please Select list option for User report !!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listUserReport = false;
                    }
                    if (_deleteList.contains("viewUserReport")) {
                        access.viewUserReport = false;
                    }
                    if (_deleteList.contains("downloadUserReport")) {
                        access.downloadUserReport = false;
                    }
                }
//                 end amol changes
                if (_addList != null) {
                    if (access.listUserReport == false) {
                        if (!_addList.contains("listUserReport")) {
                            if (_addList.contains("viewUserReport") || _addList.contains("downloadUserReport")) {
                                result = "error";
                                message = "Please Select list option for User Report!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("listUserReport")) {
                        access.listUserReport = true;
                    }
                    if (_addList.contains("viewUserReport")) {
                        access.viewUserReport = true;
                    }
                    if (_addList.contains("downloadUserReport")) {
                        access.downloadUserReport = true;
                    }
                }
                //2.  Message report

                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("MessageReportList")) {
                        if (_deleteList.contains("viewMessageReport") || _deleteList.contains("downloadMessageReport")) {
                            access.listMessageReport = false;
                        } else if (_addList.contains("viewMessageReport") || _addList.contains("downloadMessageReport")) {
                            result = "error";
                            message = "Please Select list option for Message report !!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.viewMessageReport == true || access.downloadMessageReport == true) {
                            result = "error";
                            message = "Please Select list option for Message report !!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listMessageReport = false;
                    }
                    if (_deleteList.contains("viewMessageReport")) {
                        access.viewMessageReport = false;
                    }
                    if (_deleteList.contains("downloadMessageReport")) {
                        access.downloadMessageReport = false;
                    }
                }
//                 end amol changes

                if (_addList != null) {
                    if (access.listMessageReport == false) {
                        if (!_addList.contains("MessageReportList")) {
                            if (_addList.contains("viewMessageReport") || _addList.contains("downloadMessageReport")) {
                                result = "error";
                                message = "Please Select list option for Message Report!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("MessageReportList")) {
                        access.listMessageReport = true;
                    }
                    if (_addList.contains("viewMessageReport")) {
                        access.viewMessageReport = true;
                    }
                    if (_addList.contains("downloadMessageReport")) {
                        access.downloadMessageReport = true;
                    }
                }
                //3.  Otp report

                // changes by amol
                if (_deleteList != null) {
                    if (_deleteList.contains("OtpReportList")) {
                        if (_deleteList.contains("viewOtpReport") || _deleteList.contains("downloadOtpReport")) {
                            access.listOtpReport = false;
                        } else if (_addList.contains("viewOtpReport") || _addList.contains("downloadOtpReport")) {
                            result = "error";
                            message = "Please Select list option for Otp report !!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } else if (access.viewOtpReport == true || access.downloadOtpReport == true) {
                            result = "error";
                            message = "Please Select list option for Otp report !!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        access.listOtpReport = false;
                    }
                    if (_deleteList.contains("viewOtpReport")) {
                        access.viewOtpReport = false;
                    }
                    if (_deleteList.contains("downloadOtpReport")) {
                        access.downloadOtpReport = false;
                    }
                }
//                 end amol changes

                if (_addList != null) {
                    if (access.listOtpReport == false) {
                        if (!_addList.contains("OtpReportList")) {
                            if (_addList.contains("viewOtpReport") || _addList.contains("downloadOtpReport")) {
                                result = "error";
                                message = "Please Select list option for OTP Report!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("OtpReportList")) {
                        access.listOtpReport = true;
                    }
                    if (_addList.contains("viewOtpReport")) {
                        access.viewOtpReport = true;
                    }
                    if (_addList.contains("downloadOtpReport")) {
                        access.downloadOtpReport = true;
                    }
                }
//changes by Pramod for Certificate Reports
                if (_addList != null) {
                    if (access.listCertificateReport == false) {
                        if (!_addList.contains("listCertificateReport")) {
                            if (_addList.contains("viewCertificateReport") || _addList.contains("downloadlistCertificateReport")) {
                                result = "error";
                                message = "Please Select list option for Message Report!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("listCertificateReport")) {
                        access.listCertificateReport = true;
                    }
                    if (_addList.contains("viewCertificateReport")) {
                        access.viewCertificateReport = true;
                    }
                    if (_addList.contains("downloadCertificateReport")) {
                        access.downloadCertificateReport = true;
                    }
                }

                //for twowayauth report
                if (_addList != null) {
                    if (access.listTwoAuthRports == false) {
                        if (!_addList.contains("listTwoAuthRports")) {
                            if (_addList.contains("viewlistTwoAuthRports") || _addList.contains("downloadlistTwoAuthRports")) {
                                access.listTwoAuthRports=true;
                              }else if(access.viewTwoAuthRports==true||access.downloadTwoAuthRports==true)
                                    {
                                result = "error";
                                message = "Please Select list option for Message Report!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                    if (_addList.contains("listTwoAuthRports")) {
                        access.listTwoAuthRports = true;
                    }
                    if (_addList.contains("viewTwoAuthRports")) {
                        access.viewTwoAuthRports = true;
                    }
                    if (_addList.contains("downloadTwoAuthRports")) {
                        access.downloadTwoAuthRports = true;
                    }
                }

                if (_addList != null) {
                    if (access.listEasyCheckinReport == true) {
                        if (_addList.contains("listEasyCheckinReport")) {
                            if (_addList.contains("viewEasyCheckinReport") || _addList.contains("downloadasyCheckinReport")) {
                                access.listEasyCheckinReport = true;

                            } else if (access.viewEasyCheckinReport == true || access.downloadasyCheckinReport == true) {

                                result = "error";
                                message = "Please Select list option for EasyCheckin Report!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;

                            }

                        }

                    }
                    if (_addList.contains("listEasyCheckinReport")) {
                        access.listEasyCheckinReport = true;
                    }
                    if (_addList.contains("viewEasyCheckinReport")) {
                        access.viewEasyCheckinReport = true;
                    }
                    if (_addList.contains("downloadasyCheckinReport")) {
                        access.downloadasyCheckinReport = true;
                    }
                }

                //4.  Otp Failure report
                    // changes by amol
                    if (_deleteList != null) {
                        if (_deleteList.contains("OtpFailureReportList")) {
                            if (_deleteList.contains("viewOtpFailureReport") || _deleteList.contains("downloadOtpFailureReport")) {
                                access.listOtpFailureReport = false;
                            } else if (_addList.contains("viewOtpFailureReport") || _addList.contains("downloadOtpFailureReport")) {
                                result = "error";
                                message = "Please Select list option for OtpFailure report !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            } else if (access.viewOtpFailureReport == true || access.downloadOtpFailureReport == true) {
                                result = "error";
                                message = "Please Select list option for OtpFailure report !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                            access.listOtpFailureReport = false;
                        }
                        if (_deleteList.contains("viewOtpFailureReport")) {
                            access.viewOtpFailureReport = false;
                        }
                        if (_deleteList.contains("downloadOtpFailureReport")) {
                            access.downloadOtpFailureReport = false;
                        }
                    }
//                 end amol changes

                    if (_addList != null) {
                        if (access.listOtpFailureReport == false) {
                            if (!_addList.contains("OtpFailureReportList")) {
                                if (_addList.contains("viewOtpFailureReport") || _addList.contains("downloadOtpFailureReport")) {
                                    result = "error";
                                    message = "Please Select list option for OTP Failure Report!!!";
                                    try {
                                        json.put("_result", result);
                                        json.put("_message", message);
                                    } catch (Exception e) {
                                        log.error("exception caught :: ",e);
                                    }
                                    out.print(json);
                                    out.flush();
                                    return;
                                }
                            }
                        }
                        if (_addList.contains("OtpFailureReportList")) {
                            access.listOtpFailureReport = true;
                        }
                        if (_addList.contains("viewOtpFailureReport")) {
                            access.viewOtpFailureReport = true;
                        }
                        if (_addList.contains("downloadOtpFailureReport")) {
                            access.downloadOtpFailureReport = true;
                        }
                    }

                //for error messages
                    //4.  Pki report
                    // changes by amol
                    if (_deleteList != null) {
                        if (_deleteList.contains("PkiReportList")) {
                            if (_deleteList.contains("viewPkiReport") || _deleteList.contains("downloadPkiReport")) {
                                access.listPkiReport = false;
                            } else if (_addList.contains("viewPkiReport") || _addList.contains("downloadPkiReport")) {
                                result = "error";
                                message = "Please Select list option for Pki report !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                   log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            } else if (access.viewPkiReport == true || access.downloadPkiReport == true) {
                                result = "error";
                                message = "Please Select list option for Pki report !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                            access.listPkiReport = false;
                        }
                        if (_deleteList.contains("viewPkiReport")) {
                            access.viewPkiReport = false;
                        }
                        if (_deleteList.contains("downloadPkiReport")) {
                            access.downloadPkiReport = false;
                        }
                    }
//                 end amol changes

                    if (_addList != null) {
                        if (access.listPkiReport == false) {
                            if (!_addList.contains("PkiReportList")) {
                                if (_addList.contains("viewPkiReport") || _addList.contains("downloadPkiReport")) {
                                    result = "error";
                                    message = "Please Select list option for PKI Report!!!";
                                    try {
                                        json.put("_result", result);
                                        json.put("_message", message);
                                    } catch (Exception e) {
                                        log.error("exception caught :: ",e);
                                    }
                                    out.print(json);
                                    out.flush();
                                    return;
                                }
                            }
                        }
                        if (_addList.contains("PkiReportList")) {
                            access.listPkiReport = true;
                        }
                        if (_addList.contains("viewPkiReport")) {
                            access.viewPkiReport = true;
                        }
                        if (_addList.contains("downloadPkiReport")) {
                            access.downloadPkiReport = true;
                        }
                    }
                //5.  EpinSystemSpecific report

                    // changes by amol
                    if (_deleteList != null) {
                        if (_deleteList.contains("EpinSystemReportList")) {
                            if (_deleteList.contains("viewEpinSystemReport") || _deleteList.contains("downloadEpinSystemReport")) {
                                access.listEpinSystemReport = false;
                            } else if (_addList.contains("viewEpinSystemReport") || _addList.contains("downloadEpinSystemReport")) {
                                result = "error";
                                message = "Please Select list option for EpinSystem report !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            } else if (access.viewEpinSystemReport == true || access.downloadEpinSystemReport == true) {
                                result = "error";
                                message = "Please Select list option for EpinSystem report !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                            access.listEpinSystemReport = false;
                        }
                        if (_deleteList.contains("viewEpinSystemReport")) {
                            access.viewEpinSystemReport = false;
                        }
                        if (_deleteList.contains("downloadEpinSystemReport")) {
                            access.downloadEpinSystemReport = false;
                        }
                    }
//                 end amol changes

                    if (_addList != null) {
                        if (access.listEpinSystemReport == false) {
                            if (!_addList.contains("EpinSystemReportList")) {
                                if (_addList.contains("viewEpinSystemReport") || _addList.contains("downloadEpinSystemReport")) {
                                    result = "error";
                                    message = "Please Select list option for Epin System Report!!!";
                                    try {
                                        json.put("_result", result);
                                        json.put("_message", message);
                                    } catch (Exception e) {
                                        log.error("exception caught :: ",e);
                                    }
                                    out.print(json);
                                    out.flush();
                                    return;
                                }
                            }
                        }
                        if (_addList.contains("EpinSystemReportList")) {
                            access.listEpinSystemReport = true;
                        }
                        if (_addList.contains("viewEpinSystemReport")) {
                            access.viewEpinSystemReport = true;
                        }
                        if (_addList.contains("downloadEpinSystemReport")) {
                            access.downloadEpinSystemReport = true;
                        }
                    }
                //6.  EpinUserSpecific report

                    // changes by amol
                    if (_deleteList != null) {
                        if (_deleteList.contains("EpinUserSpecificReportList")) {
                            if (_deleteList.contains("viewEpinUserSpecificReport") || _deleteList.contains("downloadEpinUserSpecificReport")) {
                                access.listEpinUserSpecificReport = false;
                            } else if (_addList.contains("viewEpinUserSpecificReport") || _addList.contains("downloadEpinUserSpecificReport")) {
                                result = "error";
                                message = "Please Select list option for EpinUserSpecific report !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            } else if (access.viewEpinUserSpecificReport == true || access.downloadEpinUserSpecificReport == true) {
                                result = "error";
                                message = "Please Select list option for EpinUserSpecific report !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                            access.listEpinUserSpecificReport = false;
                        }
                        if (_deleteList.contains("viewEpinUserSpecificReport")) {
                            access.viewEpinUserSpecificReport = false;
                        }
                        if (_deleteList.contains("downloadEpinUserSpecificReport")) {
                            access.downloadEpinUserSpecificReport = false;
                        }
                    }
//                 end amol changes

                    if (_addList != null) {
                        if (access.listEpinUserSpecificReport == false) {
                            if (!_addList.contains("EpinUserSpecificReportList")) {
                                if (_addList.contains("viewEpinUserSpecificReport") || _addList.contains("downloadEpinUserSpecificReport")) {
                                    result = "error";
                                    message = "Please Select list option for Epin User Specific Report!!!";
                                    try {
                                        json.put("_result", result);
                                        json.put("_message", message);
                                    } catch (Exception e) {
                                        log.error("exception caught :: ",e);
                                    }
                                    out.print(json);
                                    out.flush();
                                    return;
                                }
                            }
                        }
                        if (_addList.contains("EpinUserSpecificReportList")) {
                            access.listEpinUserSpecificReport = true;
                        }
                        if (_addList.contains("viewEpinUserSpecificReport")) {
                            access.viewEpinUserSpecificReport = true;
                        }
                        if (_addList.contains("downloadEpinUserSpecificReport")) {
                            access.downloadEpinUserSpecificReport = true;
                        }
                    }
                //.  RemoteSigning report

                    // changes by amol
                    if (_deleteList != null) {
                        if (_deleteList.contains("RemoteSigningReportList")) {
                            if (_deleteList.contains("viewRemoteSigningReport") || _deleteList.contains("downloadRemoteSigningReport")) {
                                access.listRemoteSigningReport = false;
                            } else if (_addList.contains("viewRemoteSigningReport") || _addList.contains("downloadRemoteSigningReport")) {
                                result = "error";
                                message = "Please Select list option for RemoteSigning report !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            } else if (access.viewRemoteSigningReport == true || access.downloadRemoteSigningReport == true) {
                                result = "error";
                                message = "Please Select list option for RemoteSigning report !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                            access.listRemoteSigningReport = false;
                        }
                        if (_deleteList.contains("viewRemoteSigningReport")) {
                            access.viewRemoteSigningReport = false;
                        }
                        if (_deleteList.contains("downloadRemoteSigningReport")) {
                            access.downloadRemoteSigningReport = false;
                        }
                    }
//                 end amol changes

                    if (_addList != null) {
                        if (access.listRemoteSigningReport == false) {
                            if (!_addList.contains("RemoteSigningReportList")) {
                                if (_addList.contains("viewRemoteSigningReport") || _addList.contains("downloadRemoteSigningReport")) {
                                    result = "error";
                                    message = "Please Select list option for Remote Signing Report!!!";
                                    try {
                                        json.put("_result", result);
                                        json.put("_message", message);
                                    } catch (Exception e) {
                                        log.error("exception caught :: ",e);
                                    }
                                    out.print(json);
                                    out.flush();
                                    return;
                                }
                            }
                        }
                        if (_addList.contains("RemoteSigningReportList")) {
                            access.listRemoteSigningReport = true;
                        }
                        if (_addList.contains("viewRemoteSigningReport")) {
                            access.viewRemoteSigningReport = true;
                        }
                        if (_addList.contains("downloadRemoteSigningReport")) {
                            access.downloadRemoteSigningReport = true;
                        }
                    }
                //.  Billing  report subscription
                    // changes by amol
                    if (_deleteList != null) {
                        if (_deleteList.contains("subscriptionBaseBillingReportList")) {
                            if (_deleteList.contains("viewsubscriptionBaseBillingReport") || _deleteList.contains("downloadsubscriptionBaseBillingReport")) {
                                access.listsubscriptionBaseBillingReport = false;
                            } else if (_addList.contains("viewsubscriptionBaseBillingReport") || _addList.contains("downloadsubscriptionBaseBillingReport")) {
                                result = "error";
                                message = "Please Select list option for subscriptionBaseBilling report !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            } else if (access.viewsubscriptionBaseBillingReport == true || access.downloadsubscriptionBaseBillingReport == true) {
                                result = "error";
                                message = "Please Select list option for subscriptionBaseBilling report !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                            access.listsubscriptionBaseBillingReport = false;
                        }
                        if (_deleteList.contains("viewsubscriptionBaseBillingReport")) {
                            access.viewsubscriptionBaseBillingReport = false;
                        }
                        if (_deleteList.contains("downloadsubscriptionBaseBillingReport")) {
                            access.downloadsubscriptionBaseBillingReport = false;
                        }
                    }
//                 end amol changes

                    if (_addList != null) {
                        if (access.listsubscriptionBaseBillingReport == false) {
                            if (!_addList.contains("subscriptionBaseBillingReportList")) {
                                if (_addList.contains("viewsubscriptionBaseBillingReport") || _addList.contains("downloadsubscriptionBaseBillingReport")) {
                                    result = "error";
                                    message = "Please Select list option for Subscription Base Billing Report!!!";
                                    try {
                                        json.put("_result", result);
                                        json.put("_message", message);
                                    } catch (Exception e) {
                                        log.error("exception caught :: ",e);
                                    }
                                    out.print(json);
                                    out.flush();
                                    return;
                                }
                            }
                        }
                        if (_addList.contains("subscriptionBaseBillingReportList")) {
                            access.listsubscriptionBaseBillingReport = true;
                        }
                        if (_addList.contains("viewsubscriptionBaseBillingReport")) {
                            access.viewsubscriptionBaseBillingReport = true;
                        }
                        if (_addList.contains("downloadsubscriptionBaseBillingReport")) {
                            access.downloadsubscriptionBaseBillingReport = true;
                        }
                    }
                //.  Billing  report transaction

                    // changes by amol
                    if (_deleteList != null) {
                        if (_deleteList.contains("transcationBaseBillingReportList")) {
                            if (_deleteList.contains("viewtranscationBaseBillingReport") || _deleteList.contains("downloadtranscationBaseBillingReport")) {
                                access.listtranscationBaseBillingReport = false;
                            } else if (_addList.contains("viewtranscationBaseBillingReport") || _addList.contains("downloadtranscationBaseBillingReport")) {
                                result = "error";
                                message = "Please Select list option for transcationBaseBilling report !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            } else if (access.viewtranscationBaseBillingReport == true || access.downloadtranscationBaseBillingReport == true) {
                                result = "error";
                                message = "Please Select list option for transcationBaseBilling report !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                            access.listtranscationBaseBillingReport = false;
                        }
                        if (_deleteList.contains("viewtranscationBaseBillingReport")) {
                            access.viewtranscationBaseBillingReport = false;
                        }
                        if (_deleteList.contains("downloadtranscationBaseBillingReport")) {
                            access.downloadtranscationBaseBillingReport = false;
                        }
                    }
//                 end amol changes

                    if (_addList != null) {
                        if (access.listtranscationBaseBillingReport == false) {
                            if (!_addList.contains("transcationBaseBillingReportList")) {
                                if (_addList.contains("viewtranscationBaseBillingReport") || _addList.contains("downloadtranscationBaseBillingReport")) {
                                    result = "error";
                                    message = "Please Select list option for Transaction Base Billing Report!!!";
                                    try {
                                        json.put("_result", result);
                                        json.put("_message", message);
                                    } catch (Exception e) {
                                        log.error("Exception caught :: ",e);
                                    }
                                    out.print(json);
                                    out.flush();
                                    return;
                                }
                            }
                        }
                        if (_addList.contains("transcationBaseBillingReportList")) {
                            access.listtranscationBaseBillingReport = true;
                        }
                        if (_addList.contains("viewtranscationBaseBillingReport")) {
                            access.viewtranscationBaseBillingReport = true;
                        }
                        if (_addList.contains("downloadtranscationBaseBillingReport")) {
                            access.downloadtranscationBaseBillingReport = true;
                        }
                    }
                  //.  Billing  report transaction

                    // changes by amol
                    if (_deleteList != null) {
                        if (_deleteList.contains("transcationReportList")) {
                            if (_deleteList.contains("viewtranscationReport") || _deleteList.contains("downloadtranscationReport")) {
                                access.listtranscationReport = false;
                            } else if (_addList.contains("viewtranscationReport") || _addList.contains("downloadtranscationReport")) {
                                result = "error";
                                message = "Please Select list option for transcation report !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            } else if (access.viewtranscationReport == true || access.downloadtranscationReport == true) {
                                result = "error";
                                message = "Please Select list option for transcation report !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                            access.listtranscationReport = false;
                        }
                        if (_deleteList.contains("viewtranscationReport")) {
                            access.viewtranscationReport = false;
                        }
                        if (_deleteList.contains("downloadtranscationReport")) {
                            access.downloadtranscationReport = false;
                        }
                    }
//                 end amol changes
////////// Shailendra
                    if (_addList != null) {
                        if (access.listRoleRports == false) {
                            if (!_addList.contains("listRoleRports")) {
                                if (_addList.contains("viewRoleRports") || _addList.contains("downloadRoleRports")) {
                                    result = "error";
                                    message = "Please Select list option for role Report!!!";
                                    try {
                                        json.put("_result", result);
                                        json.put("_message", message);
                                    } catch (Exception e) {
                                       log.error("exception caught :: ",e);
                                    }
                                    out.print(json);
                                    out.flush();
                                    return;
                                }
                            }
                        }
//                        if (access.viewRoleRports == false) {
//                            if (!_addList.contains("downloadRoleRports")) {
////                                if (_addList.contains("downloadRoleRports")) {
//                                    result = "error";
//                                    message = "Please Select view option for role Report!!!";
//                                    try {
//                                        json.put("_result", result);
//                                        json.put("_message", message);
//                                    } catch (Exception e) {
//                                       log.error("exception caught :: ",e);
//                                    }
//                                    out.print(json);
//                                    out.flush();
//                                    return;
////                                }
//                            }
//                        }
                        if (_addList.contains("listRoleRports")) {
                            access.listRoleRports = true;
                        }
                        if (_addList.contains("viewRoleRports")) {
                            access.viewRoleRports = true;
                        }
                        if (_addList.contains("downloadRoleRports")) {
                            access.downloadRoleRports = true;
                        }
                    }
                    
                    if (_deleteList != null) {
                        if (_deleteList.contains("listRoleRports")) {
                            if (_deleteList.contains("viewRoleRports") || _deleteList.contains("downloadRoleRports")) {
                                access.listRoleRports = false;
                            } else if (_addList.contains("viewRoleRports") || _addList.contains("downloadRoleRports")) {
                                result = "error";
                                message = "Please Select list option for role report !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            } else if (access.viewRoleRports == true || access.downloadRoleRports == true) {
                                result = "error";
                                message = "Please Select list option for role report !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                            access.listRoleRports = false;
                        }
                        if (_deleteList.contains("viewRoleRports")) {
                            access.viewRoleRports = false;
                        }
                        if (_deleteList.contains("downloadRoleRports")) {
                            access.downloadRoleRports = false;
                        }
                    }
                    
                    
                    if (_addList != null) {
                        if (access.listUnitRports == false) {
                            if (!_addList.contains("listUnitRports")) {
                                if (_addList.contains("viewUnitRports") || _addList.contains("downloadUnitReports")) {
                                    result = "error";
                                    message = "Please Select list option for unit Report!!!";
                                    try {
                                        json.put("_result", result);
                                        json.put("_message", message);
                                    } catch (Exception e) {
                                       log.error("exception caught :: ",e);
                                    }
                                    out.print(json);
                                    out.flush();
                                    return;
                                }
                            }
                        }
                        if (_addList.contains("listUnitRports")) {
                            access.listUnitRports = true;
                        }
                        if (_addList.contains("viewUnitRports")) {
                            access.viewUnitRports = true;
                        }
                        if (_addList.contains("downloadUnitReports")) {
                            access.downloadUnitReports= true;
                        }
                    }
                    
                    if (_deleteList != null) {
                        if (_deleteList.contains("listUnitRports")) {
                            if (_deleteList.contains("viewUnitRports") || _deleteList.contains("downloadUnitReports")) {
                                access.listUnitRports = false;
                            } else if (_addList.contains("viewUnitRports") || _addList.contains("downloadUnitReports")) {
                                result = "error";
                                message = "Please Select list option for unit report !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            } else if (access.viewUnitRports == true || access.downloadUnitReports == true) {
                                result = "error";
                                message = "Please Select list option for unit report !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                            access.listUnitRports = false;
                        }
                        if (_deleteList.contains("viewRoleRports")) {
                            access.viewUnitRports = false;
                        }
                        if (_deleteList.contains("downloadRoleRports")) {
                            access.downloadUnitReports = false;
                        }
                    }
                    
///////// end shailendra changes                    
                    if (_addList != null) {
                        if (access.listtranscationReport == false) {
                            if (!_addList.contains("transcationReportList")) {
                                if (_addList.contains("viewtranscationReport") || _addList.contains("downloadtranscationReport")) {
                                    result = "error";
                                    message = "Please Select list option for Transaction Base Billing Report!!!";
                                    try {
                                        json.put("_result", result);
                                        json.put("_message", message);
                                    } catch (Exception e) {
                                       log.error("exception caught :: ",e);
                                    }
                                    out.print(json);
                                    out.flush();
                                    return;
                                }
                            }
                        }
                        if (_addList.contains("transcationReportList")) {
                            access.listtranscationReport = true;
                        }
                        if (_addList.contains("viewtranscationReport")) {
                            access.viewtranscationReport = true;
                        }
                        if (_addList.contains("downloadtranscationReport")) {
                            access.downloadtranscationReport = true;
                        }
                    }

                    if (_deleteList != null) {
                        if (_deleteList.contains("HoneyTrapReportList")) {
                            if (_deleteList.contains("viewHoneyTrapReport") || _deleteList.contains("downloadHoneyTrapReport")) {
                                access.listHoneyTrapReport = false;
                            } else if (_addList.contains("viewHoneyTrapReport") || _addList.contains("downloadHoneyTrapReport")) {
                                result = "error";
                                message = "Please Select list option for transcation report !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            } else if (access.viewHoneyTrapReport == true || access.downloadHoneyTrapReport == true) {
                                result = "error";
                                message = "Please Select list option for transcation report !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                            access.listHoneyTrapReport = false;
                        }
                        if (_deleteList.contains("viewHoneyTrapReport")) {
                            access.viewHoneyTrapReport = false;
                        }
                        if (_deleteList.contains("downloadHoneyTrapReport")) {
                            access.downloadHoneyTrapReport = false;
                        }
                    }

                    //change by Pramod for DeleteListNewly added componenets
                    if (_deleteList.contains("listEasyCheckinReport")) {
                        if (_deleteList.contains("viewEasyCheckinReport") == true  ||_deleteList.contains("downloadasyCheckinReport") == true) {
                          
                            access.listEasyCheckinReport=false;
                            }else if(access.viewEasyCheckinReport==true&&access.downloadasyCheckinReport==true)
                            {
                            result = "error";
                            message = "Please Select list option for transcation report !!!";
                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                            } catch (Exception e) {
                                log.error("exception caught :: ",e);
                            }
                            out.print(json);
                            out.flush();
                            return;
                        } 

                    }
                    if (_deleteList.contains("viewEasyCheckinReport")) {
                        access.viewEasyCheckinReport = false;
                    }
                    if (_deleteList.contains("downloadasyCheckinReport")) {
                        access.downloadasyCheckinReport = false;
                    }

                    if (_deleteList != null) {
                        if (_deleteList.contains("listCertificateReport")) {
                            if (_deleteList.contains("viewCertificateReport") && _deleteList.contains("downloadCertificateReport")) {
                                access.listCertificateReport = false;
                            } else if (access.viewCertificateReport == true || access.downloadCertificateReport == true) {
                                result = "error";
                                message = "Please Select list option for transcation report !!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;

                            }

                        }
                        if (_deleteList.contains("viewCertificateReport")) {
                            access.viewCertificateReport = false;
                        }
                        if (_deleteList.contains("downloadCertificateReport")) {
                            access.downloadCertificateReport = false;
                        }

                    }

                    if (_deleteList != null) {
                        if (_deleteList.contains("listTwoAuthRports")) {
                            if (!_deleteList.contains("viewTwoAuthRports") || _deleteList.contains("downloadTwoAuthRports")) {
                              access.listTwoAuthRports=false;   
                            }
                                
                              else  if (access.viewTwoAuthRports == true || access.downloadTwoAuthRports == true) {

                                    result = "error";
                                    message = "Please Select list option for transcation report !!!";
                                    try {
                                        json.put("_result", result);
                                        json.put("_message", message);
                                    } catch (Exception e) {
                                       log.error("exception caught :: ",e);
                                    }
                                    out.print(json);
                                    out.flush();
                                    return;

                                }
                            } 
                        if (_deleteList.contains("viewTwoAuthRports")) {
                            access.viewTwoAuthRports = false;
                        }
                        if (_deleteList.contains("downloadTwoAuthRports")) {
                            access.downloadTwoAuthRports = false;
                        }

                    }

//                 end amol changes
                    if (_addList != null) {
                        if (access.listHoneyTrapReport == false) {
                            if (!_addList.contains("HoneyTrapReportList")) {
                                if (_addList.contains("viewHoneyTrapReport") || _addList.contains("downloadHoneyTrapReport")) {
                                    result = "error";
                                    message = "Please Select list option for Transaction Base Billing Report!!!";
                                    try {
                                        json.put("_result", result);
                                        json.put("_message", message);
                                    } catch (Exception e) {
                                        log.error("exception caught :: ",e);
                                    }
                                    out.print(json);
                                    out.flush();
                                    return;
                                }
                            }
                        }
                        if (_addList.contains("HoneyTrapReportList")) {
                            access.listHoneyTrapReport = true;
                        }
                        if (_addList.contains("viewHoneyTrapReport")) {
                            access.viewHoneyTrapReport = true;
                        }
                        if (_addList.contains("downloadHoneyTrapReport")) {
                            access.downloadHoneyTrapReport = true;
                        }
                    }
                    
                } else if (iType == OperatorsManagement.MatrixSystemSpecific) {
                    if (_deleteList != null) {
                        if (_deleteList.contains("listlicenseDetails")) {
                            access.listlicenseDetails = false;
                        }
                    }
                    if (_addList != null) {
                        if (_addList.contains("listlicenseDetails")) {
                            access.listlicenseDetails = true;
                        }
                    }
                    if (_deleteList != null) {
                        if (_deleteList.contains("listhwOTPTokenUpload")) {
                            access.listhwOTPTokenUpload = false;
                        }
                    }
                    if (_addList != null) {
                        if (_addList.contains("listhwOTPTokenUpload")) {
                            access.listhwOTPTokenUpload = true;
                        }
                    }
                    if (_deleteList != null) {
                        if (_deleteList.contains("listhwPKITokenUpload")) {
                            access.listhwPKITokenUpload = false;
                        }
                    }
                    if (_addList != null) {
                        if (_addList.contains("listhwPKITokenUpload")) {
                            access.listhwPKITokenUpload = true;
                        }
                    }
                    if (_deleteList != null) {
                        if (_deleteList.contains("signPDFDOC")) {
                            access.signPDFDOC = false;
                        }
                    }
                    if (_addList != null) {
                        if (_addList.contains("signPDFDOC")) {
                            access.signPDFDOC = true;
                        }
                    }
                    if (_deleteList != null) {
                        if (_deleteList.contains("changeSecurityCredentials")) {
                            access.changeSecurityCredentials = false;
                        }
                    }
                    if (_addList != null) {
                        if (_addList.contains("changeSecurityCredentials")) {
                            access.changeSecurityCredentials = true;
                        }
                    }
                    if (_deleteList != null) {
                        if (_deleteList.contains("downloadAuditTrail")) {
                            access.downloadAuditTrail = false;
                        }
                    }
                    if (_addList != null) {
                        if (_addList.contains("downloadAuditTrail")) {
                            access.downloadAuditTrail = true;
                        }
                    }
                    if (_deleteList != null) {
                        if (_deleteList.contains("downloadSessionAuditTrail")) {
                            access.downloadSessionAuditTrail = false;
                        }
                    }
                    if (_addList != null) {
                        if (_addList.contains("downloadSessionAuditTrail")) {
                            access.downloadSessionAuditTrail = true;
                        }
                    }
                    if (_deleteList != null) {
                        if (_deleteList.contains("killSessionID")) {
                            access.killSessionID = false;
                        }
                    }
                    if (_addList != null) {
                        if (_addList.contains("killSessionID")) {
                            access.killSessionID = true;
                        }
                    }
                    if (_deleteList != null) {
                        if (_deleteList.contains("downloadLogs")) {
                            access.downloadLogs = false;
                        }
                    }
                    if (_addList != null) {
                        if (_addList.contains("downloadLogs")) {
                            access.downloadLogs = true;
                        }
                    }
                    if (_deleteList != null) {
                        if (_deleteList.contains("downloadCleanupLogs")) {
                            access.downloadCleanupLogs = false;
                        }
                    }
                    if (_addList != null) {
                        if (_addList.contains("downloadCleanupLogs")) {
                            access.downloadCleanupLogs = true;
                        }
                    }
                    if (_deleteList != null) {
                        if (_deleteList.contains("connectorStatus")) {
                            access.connectorStatus = false;
                        }
                    }
                    if (_addList != null) {
                        if (_addList.contains("connectorStatus")) {
                            access.connectorStatus = true;
                        }
                    }
                    if (_deleteList != null) {
                        if (_deleteList.contains("auditIntegrity")) {
                            access.auditIntegrity = false;
                        }
                    }
                    if (_addList != null) {
                        if (_addList.contains("auditIntegrity")) {
                            access.auditIntegrity = true;
                        }
                    }

                    if (_deleteList != null) {
                        if (_deleteList.contains("changePassword")) {
                            access.changePassword = false;
                        }
                    }
                    if (_addList != null) {
                        if (_addList.contains("changePassword")) {
                            access.changePassword = true;
                        }
                    }
                    if (_deleteList != null) {
                        if (_deleteList.contains("requesterArchiveRequest")) {
                            access.requesterArchiveRequest = false;
                        }
                    }
                    if (_addList != null) {
                        if (_addList.contains("requesterArchiveRequest")) {
                            access.requesterArchiveRequest = true;
                        }
                    }
                    if (_deleteList != null) {
                        if (_deleteList.contains("requesterPendingRequest")) {
                            access.requesterPendingRequest = false;
                        }
                    }
                    if (_addList != null) {
                        if (_addList.contains("requesterPendingRequest")) {
                            access.requesterPendingRequest = true;
                        }
                    }
                    if (_deleteList != null) {
                        if (_deleteList.contains("authorizerArchiveRequest")) {
                            access.authorizerArchiveRequest = false;
                        }
                    }
                    if (_addList != null) {
                        if (_addList.contains("authorizerArchiveRequest")) {
                            access.authorizerArchiveRequest = true;
                        }
                    }
                    if (_deleteList != null) {
                        if (_deleteList.contains("authorizerPendingRequest")) {
                            access.authorizerPendingRequest = false;
                        }
                    }
                    if (_addList != null) {
                        if (_addList.contains("authorizerPendingRequest")) {
                            access.authorizerPendingRequest = true;
                        }
                    }
                    if (_deleteList != null) {
                        if (_deleteList.contains("requesterOperatorList")) {
                            access.requesterOperatorList = false;
                        }
                    }
                    if (_addList != null) {
                        if (_addList.contains("requesterOperatorList")) {
                            access.requesterOperatorList = true;
                        }
                    }
                    if (_deleteList != null) {
                        if (_deleteList.contains("kycRequests")) {
                            access.kycRequests = false;
                        }
                    }
                    if (_addList != null) {
                        if (_addList.contains("kycRequests")) {
                            access.kycRequests = true;
                        }
                    }
                }

//            int res = opMngt.EditRole1(sessionId, channel.getChannelid(), iRoleID, access);
                int res = -1;
                if (_operatoName.equals("null")) {
                    res = opMngt.EditRole1(sessionId, channel.getChannelid(), iRoleID, access);
                    log.debug("EditRole1 :: "+res);
                } else {
                    res = opMngt.EditOperatorAccess(sessionId, channel.getChannelid(), _operatoName, access);
                    log.debug("EditOperatorAccess :: "+res);
                }

                if (res == 0) {
                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("exception caught :: ",e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                } else {
                    result = "error";
                    message = "Failed To update settings!!!";
                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("exception caught :: ",e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }

            }catch (Exception ex) {
            log.error("exception caught :: ",ex);
        }
        log.info("is ended :: ");
        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet
        (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            processRequest(request, response);
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost
        (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            processRequest(request, response);
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo
        
            () {
        return "Short description";
        }// </editor-fold>

    }
