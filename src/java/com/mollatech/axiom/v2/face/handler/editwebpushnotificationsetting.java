/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.PushNotificationSettings;

import static com.mollatech.axiom.v2.face.handler.editsmssettings.PREFERENCE_TWO;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class editwebpushnotificationsetting extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editwebpushnotificationsetting.class.getName());

    final String itemtype = "SETTINGS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("editwebpushnotificationsetting started");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
           String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        
        log.debug("channel :: " +channel.getName());
        log.debug("sessionId :: " +sessionId);
        log.debug("remoteaccesslogin :: " +remoteaccesslogin);
        log.debug("operatorS :: " +operatorS.getOperatorid());
       
        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.MOBILETRUST_PUSH) != 0 
                //|| AxiomProtect.CheckEnforcementFor(AxiomProtect.GATEWAY_PUSH_GOOGLE) != 0 
             ) {                    
            String result = "error";
            String message = "This feature is not available in this license!!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){log.error("Exception caught :: ",e);}
            out.print(json);
            out.flush();
            return;
        }

        //audit parameter
     

        String OperatorID = operatorS.getOperatorid();

        String _channelId = channel.getChannelid();
        log.debug("editWebPushnotificationsetting::channelid is" +channel.getChannelid());
        SettingsManagement sMngmt = new SettingsManagement();
        int retValue = -1;

        String result = "success";
        String message = "Web Push Notification Gateway Settings Update Successful!!!";
        
        String _className = null;
        String _ip = null;
        String _logConfirmation1 = null;
        String _port = null;
        Object reserve1 = null;
        Object reserve2 = null;
        Object reserve3 = null;
        String _status1 = null;
        String _autofailover = null;
        String _retrycount = null;
        String _retryduration = null;
        String _retriesFromGoogle = null;
        String _timetolive = null;
        String _delayWhileIdle = null;
        String _gcmurl = null;
        String _apikey = null;
        String _googleSenderKey = null;

        String _bundleID = null;
        String _certpassowrd = null;
        String _applicationType = null;
       // int type =1;
        String _preference = request.getParameter("_perference");
        int _iPreference = 2;
        if (_preference != null) {
            _iPreference = Integer.parseInt(_preference);
        }
        String[] files = new String[1];	 // file names

        //request parameter for push notification primary
//        String _type1 = request.getParameter("_type");
//           String _type1 = request.getParameter("_type");
         int _type = SettingsManagement.WEB_PUSH_NOTIFICATION_SETTING;
        if(_iPreference == SettingsManagement.PREFERENCE_TWO){
            _type = SettingsManagement.IOS_PUSH_NOTIFICATION_SETTING;
        }
       
        if (
                _type == SettingsManagement.WEB_PUSH_NOTIFICATION_SETTING 
                ||_type == SettingsManagement.IOS_PUSH_NOTIFICATION_SETTING) {
            if (_iPreference == SettingsManagement.PREFERENCE_THREE) {   //Ternory

                _status1 = request.getParameter("_statusT");
                log.debug("_status1 :: " +_status1);
                _autofailover = request.getParameter("_autofailoverT");
                log.debug("_autofailover :: " +_autofailover);
                _retrycount = request.getParameter("_retriesT");
                log.debug("_retrycount :: " +_retrycount);
                _retryduration = request.getParameter("_retrydurationT");
                log.debug("_retryduration :: " +_retryduration);
                _retriesFromGoogle = request.getParameter("_retriesFromGoogleT");
                log.debug("_retriesFromGoogle :: " +_retriesFromGoogle);
                _timetolive = request.getParameter("_timetoliveT");
                log.debug("_timetolive :: " +_timetolive);
                _delayWhileIdle = request.getParameter("_delayWhileIdleT");
                log.debug("_delayWhileIdle :: " +_delayWhileIdle);
                _ip = request.getParameter("_ipT");
                log.debug("_ip :: " +_ip);
                _port = request.getParameter("_portT");
                log.debug("_port :: " +_port);
                _gcmurl = request.getParameter("_gcmurlT");
                log.debug("_gcmurl :: " +_gcmurl);
                _apikey = request.getParameter("_apikeyT");
                log.debug("_apikey :: " +_apikey);
                _googleSenderKey = request.getParameter("_googleSenderKeyT");
                log.debug("_googleSenderKey :: " +_googleSenderKey);
                _className = request.getParameter("_classNameT");
                log.debug("_className :: " +_className);
                reserve1 = request.getParameter("_reserve1T");
                log.debug("reserve1 :: " +reserve1);
                reserve2 = request.getParameter("_reserve2T");
                log.debug("reserve2 :: " +reserve2);
                reserve3 = request.getParameter("_reserve3T");
                log.debug("reserve3 :: " +reserve3);
                _logConfirmation1 = request.getParameter("_logConfirmationT");
                log.debug("_logConfirmation1 :: " +_logConfirmation1);
                if (_logConfirmation1 == null) {
                    _logConfirmation1 = "false";
                } else {
                    _logConfirmation1 = "true";
                }

            } else if (_iPreference == PREFERENCE_TWO) {   //secondary
                 _ip = request.getParameter("_ipS");
                 log.debug("_ip :: " +_ip);
                _port = request.getParameter("_portS");
                log.debug("_port :: " +_port);
                _status1 = request.getParameter("_statusS");
                log.debug("_status1 :: " +_status1);
                _autofailover = request.getParameter("_autofailoverS");
                log.debug("_autofailover :: " +_autofailover);
                _retrycount = request.getParameter("_retriesS");
                log.debug("_retrycount :: " +_retrycount);
                _retryduration = request.getParameter("_retrydurationS");
                log.debug("_retryduration :: " +_retryduration);
                _applicationType = request.getParameter("_delayWhileIdleS");
                log.debug("_applicationType :: " +_applicationType);
                _certpassowrd = request.getParameter("_password");
                log.debug("_certpassowrd :: " +_certpassowrd);
                _bundleID = request.getParameter("_bundleID");
                log.debug("_bundleID :: " +_bundleID);
                _className = request.getParameter("_classNameS");
                log.debug("_className :: " +_className);
                reserve1 = request.getParameter("_reserve1S");
                log.debug("reserve1 :: " +reserve1);
                reserve2 = request.getParameter("_reserve2S");
                log.debug("reserve2 :: " +reserve2);
                reserve3 = request.getParameter("_reserve3S");
                log.debug("reserve3 :: " +reserve3);
                _logConfirmation1 = request.getParameter("_logConfirmationS");
                log.debug("_logConfirmation1 :: " +_logConfirmation1);
                if (_logConfirmation1 == null) {
                    _logConfirmation1 = "false";
                } else {
                    _logConfirmation1 = "true";
                }
                //Upload Certificate
                String savepath = "";
                savepath = System.getProperty("catalina.home");
                if(savepath == null) { savepath = System.getenv("catalina.home"); }
                savepath += System.getProperty("file.separator");
                savepath += "axiomv2-settings";
                savepath += System.getProperty("file.separator");
                savepath += "uploads";
                savepath += System.getProperty("file.separator");
                String optionalFileName = "";
                FileItem fileItem = null;

                String dirName = savepath;
                int i = 0;
                if (ServletFileUpload.isMultipartContent(request)) {
                    try {
                        ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                        List fileItemsList = servletFileUpload.parseRequest(request);
                        Iterator it = fileItemsList.iterator();
                        while (it.hasNext()) {
                            FileItem fileItemTemp = (FileItem) it.next();
                            if (fileItemTemp.isFormField()) {
                                if (fileItemTemp.getFieldName().equals("filename")) {
                                    optionalFileName = fileItemTemp.getString();
                                } else {
                                    //System.out.println("Additional fields in file upload form is  " + fileItemTemp.getFieldName());
                                }
                            } else {
                                fileItem = fileItemTemp;
                            }
                        }
                        if (fileItem != null) {
                            String fileName = fileItem.getName();
                            if(fileItem.getSize() == 0){
                           String strError = "Please Select File To Upload...!!!";
                            result = "error";
                            try { json.put("result", result);
                            json.put("message", strError);
                            }catch(Exception e){log.error("Exception caught :: ",e);}
                            //out.print(json);
                            out.print("{result:'"+result+"',message:'"+strError+"'}");
                            out.flush();
                            return;
                        }
                            if (fileItem.getSize() > 0 && fileItem.getSize() < 1024000 * 5) { // size cannot be more than 65Kb. We want it light.
                                if (optionalFileName.trim().equals("")) {
                                    fileName = FilenameUtils.getName(fileName);
                                } else {
                                    fileName = optionalFileName;
                                }
                                files[i++] = dirName + fileName;
                                File saveTo = new File(dirName + fileName);
                                fileItem.write(saveTo);
                            } else {
                                result = "error";
                                message = "Error: " + fileName + " size is more than 5MB. Please upload lighter files.";
                                json.put("_result", result);
                                json.put("_message", message);
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }

                    } catch (Exception ex) {
                       log.error("Exception caught :: ",ex);
                    }
                }

            }

            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _type, _iPreference);

            PushNotificationSettings pushSetting = null;
            boolean bAddSetting = false;

            if (settingsObj == null) {
                pushSetting = new PushNotificationSettings();
                pushSetting.setChannelId(_channelId);
                bAddSetting = true;
            } else {
                pushSetting = (PushNotificationSettings) settingsObj;
            }

            //apple
            if (ServletFileUpload.isMultipartContent(request)) {
                pushSetting.setCertificatePath(files[0]);
                pushSetting.setCertPassword(_certpassowrd);
            }
            if(_bundleID != null){
            pushSetting.setBundleId(_bundleID);
            }

            if (_applicationType != null) {
                int iAppType = Integer.parseInt(_applicationType);
                if (iAppType == 1) {
                    pushSetting.setApplicationType(true);
                } else {
                    pushSetting.setApplicationType(false);
                }

            }

            pushSetting.setPreference(_iPreference);
            if(_className != null){
            pushSetting.setClassName(_className);
            }
            if(_ip != null){
            pushSetting.setIp(_ip);
            }
            if (_logConfirmation1 != null) {
                boolean _logConfirmation = Boolean.parseBoolean(_logConfirmation1);
                pushSetting.setLogConfirmation(_logConfirmation);
            }
            if (_port != null || _port.isEmpty() == true) {
                int _port1 = Integer.parseInt(_port);
                pushSetting.setPort(_port1);
            }
            if(reserve1 != null){
            pushSetting.setReserve1(reserve1);
            }
            if(reserve2 != null){
            pushSetting.setReserve2(reserve2);
            }
            if(reserve3 != null){
            pushSetting.setReserve3(reserve3);
            }
            if (_status1 != null) {
                int _status = Integer.parseInt(_status1);
                pushSetting.setStatus(_status);
            }

//            if (_autofailover != null && !_autofailover.isEmpty()) {
//                int _iAutofailover = Integer.parseInt(_autofailover);
                pushSetting.setAutofailover(0);
//            }

            if (_retrycount != null) {
                int _iRetrycount = Integer.parseInt(_retrycount);
                pushSetting.setRetrycount(_iRetrycount);
            }

            if (_retryduration != null) {
                int _iRetryDuration = Integer.parseInt(_retryduration);
                pushSetting.setRetryduration(_iRetryDuration);
            }

            if (_retriesFromGoogle != null) {
                pushSetting.setRetriesFromGoogle(Integer.parseInt(_retriesFromGoogle));
            }
            if (_timetolive != null) {
                pushSetting.setTimeToLive(Integer.parseInt(_timetolive));
            }
            if (_delayWhileIdle != null) {
                int idelay = Integer.parseInt(_delayWhileIdle);
                if (idelay == 1) {
                    pushSetting.setDelayWhileIdle(true);
                } else {
                    pushSetting.setDelayWhileIdle(false);
                }

            }
            if(_gcmurl != null){
            pushSetting.setGcmUrl(_gcmurl);
            }
            if(_apikey != null){
            pushSetting.setApiKey(_apikey);
            }
            if(_googleSenderKey != null){
            pushSetting.setGoogleSenderId(_googleSenderKey);
            }

            AuditManagement audit = new AuditManagement();
            if (bAddSetting == true) {
                retValue = sMngmt.addSetting(sessionId, _channelId, _type, _iPreference, pushSetting);
                log.debug("retValue :: " +retValue);

                String resultString = "ERROR";
                if(_iPreference == 1){
                if (retValue == 0) {
                    resultString = "SUCCESS";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add Web Push Notification", resultString, retValue, "Setting Management",
                            "", "ChannelID=" + pushSetting.getChannelId()+ "Class Name=" + pushSetting.getClassName()
                            + "IP=" + pushSetting.getIp()+ "Password=***"
                            + "Autofailover=" + pushSetting.getAutofailover()+ "Port=" + pushSetting.getPort()+ "Prefernece=" + _preference
                            + "Reserve1=" + pushSetting.getReserve1()+ "Reserve2" + pushSetting.getReserve2()+ "Reserve3=" + pushSetting.getReserve3()
                            + "RetryCount=" + pushSetting.getRetrycount()+ "Status=" + pushSetting.getStatus()
                            + "LogConfirmation=" + pushSetting.isLogConfirmation()+ "Retry Duration" + pushSetting.getRetryduration()
                            + "Retries From Google=" + _retriesFromGoogle + "Time To Live=" + _timetolive + "Delay While Idle=" + _delayWhileIdle
                            + "GCM Url=" + _gcmurl + "Api Key=" + _apikey + "Google Sender Key = ****** ",
                            itemtype,_channelId );
                } else if (retValue != 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add Web Push Notification", resultString, retValue, "Setting Management",
                            "", "Failed To add Web Push Notification", itemtype,_channelId
                            );
                }}else{
                     if (retValue == 0) {
                    resultString = "Success";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add Apple Push Notification", resultString, retValue, "Setting Management",
                            "", "ChannelID=" + pushSetting.getChannelId() + "Class Name=" + pushSetting.getClassName()
                            + "IP=" + pushSetting.getIp()+ " Certificate Password=***"
                            + "Autofailover=" + pushSetting.getAutofailover()+ "Port=" + pushSetting.getPort()+ "Prefernece=" + _preference
                            + "Reserve1=" + pushSetting.getReserve1()+ "Reserve2" + pushSetting.getReserve2()+ "Reserve3=" + pushSetting.getReserve3()
                            + "RetryCount=" + pushSetting.getRetrycount()+ "Status=" + pushSetting.getStatus()
                            + "LogConfirmation=" + pushSetting.isLogConfirmation()+ "Retry Duration" + pushSetting.getRetryduration()
                            + "Application Type=" + _applicationType +"Bundle Id"+_bundleID+"Certificate Path ="+files[0]
                           ,
                            itemtype, _channelId);
                } else if (retValue != 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add Apple Push Notification Setting", resultString, retValue, "Setting Management",
                            "", "Failed To add Android Push Notification Setting", itemtype,_channelId
                            );
                }
                }
            } else {
                PushNotificationSettings oldpushObj = (PushNotificationSettings) sMngmt.getSetting(sessionId, _channelId, _type, _iPreference);
                retValue = sMngmt.changeSetting(sessionId, _channelId, _type, _iPreference, settingsObj, pushSetting);
                log.debug("editpushnotificationsetting::changeSetting" +retValue);
                
                String resultString = "ERROR";
                
                String _oldappType = "Production";
                 String _appType = "Production";
                if(oldpushObj.isApplicationType()== true){
                    _oldappType = "Sandbox";
                }
                if(pushSetting.isApplicationType()== true){
                    _appType = "Sandbox";
                }
                
                if(_iPreference == 1){
                if (retValue == 0) {
                    resultString = "SUCCESS";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change Web Push Notification Setting", resultString, retValue, "Setting Management",
                            "ChannelID=" + oldpushObj.getChannelId() + " Class Name=" + oldpushObj.getClassName()+ "ImplementationJar="
                            + " IP=" + oldpushObj.getIp()+ " Password=***" + " Autofailover=" + oldpushObj.getAutofailover()+ " Port=" + oldpushObj.getPort()
                            + " Prefernece=" + _preference + " Reserve1=" + oldpushObj.getReserve1()+ " Reserve2" + oldpushObj.getReserve2()+ " Reserve3=" + oldpushObj.getReserve3()
                            + " RetryCount=" + oldpushObj.getRetrycount()+ " Status=" + oldpushObj.getStatus()
                            + " LogConfirmation=" + oldpushObj.isLogConfirmation()+ " Retry Duration" + oldpushObj.getRetryduration(),
                            "ChannelID=" + pushSetting.getChannelId() + "Class Name=" + pushSetting.getClassName()
                            + "IP=" + pushSetting.getIp()+ "Password=***"
                            + "Autofailover=" + pushSetting.getAutofailover()+ "Port=" + pushSetting.getPort()+ "Prefernece=" + _preference
                            + "Reserve1=" + pushSetting.getReserve1()+ "Reserve2" + pushSetting.getReserve2()+ "Reserve3=" + pushSetting.getReserve3()
                            + "RetryCount=" + pushSetting.getRetrycount()+ "Status=" + pushSetting.getStatus()
                            + "LogConfirmation=" + pushSetting.isLogConfirmation()+ "Retry Duration" + pushSetting.getRetryduration()
                            + "Retries From Google=" + _retriesFromGoogle + "Time To Live=" + _timetolive + "Delay While Idle=" + _delayWhileIdle
                            + "GCM Url=" + _gcmurl + "Api Key=" + _apikey + "Google Sender Key =" + _googleSenderKey,
                            itemtype, 
                            _channelId);
                } else if (retValue != 0) {
                    resultString = "ERROR";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change Web Push notification Setting", resultString, retValue, "Setting Management",
                            "ChannelID=" + oldpushObj.getChannelId() + " Class Name=" + oldpushObj.getClassName()+ "ImplementationJar="
                            + " IP=" + oldpushObj.getIp()+ " Password=***" + " Autofailover=" + oldpushObj.getAutofailover()+ " Port=" + oldpushObj.getPort()
                            + " Prefernece=" + _preference + " Reserve1=" + oldpushObj.getReserve1()+ " Reserve2" + oldpushObj.getReserve2()+ " Reserve3=" + oldpushObj.getReserve3()
                            + " RetryCount=" + oldpushObj.getRetrycount()+ " Status=" + oldpushObj.getStatus()
                            + " LogConfirmation=" + oldpushObj.isLogConfirmation()+ " Retry Duration" + oldpushObj.getRetryduration(),
                            "Failed To Edit Change Web Push notification Setting", itemtype, 
                            _channelId);
                }}else{
                    if (retValue == 0) {
                    resultString = "SUCCESS";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change Apple Push Notification Setting", resultString, retValue, "Setting Management",
                            "ChannelID=" + oldpushObj.getChannelId() + " Class Name=" + oldpushObj.getClassName()+ "ImplementationJar="
                            + " IP=" + oldpushObj.getIp()+ " Password=***" + " Autofailover=" + oldpushObj.getAutofailover()+ " Port=" + oldpushObj.getPort()
                            + " Prefernece=" + _preference + " Reserve1=" + oldpushObj.getReserve1()+ " Reserve2" + oldpushObj.getReserve2()+ " Reserve3=" + oldpushObj.getReserve3()
                            + " RetryCount=" + oldpushObj.getRetrycount()+ " Status=" + oldpushObj.getStatus()
                            + " LogConfirmation=" + oldpushObj.isLogConfirmation()+ " Retry Duration" + oldpushObj.getRetryduration()
                             + "Application Type=" + _oldappType +"Bundle Id"+oldpushObj.getBundleId()+"Certificate Path ="+oldpushObj.getCertificatePath(),
                            "ChannelID=" + pushSetting.getChannelId() + "Class Name=" + pushSetting.getClassName()
                            + "IP=" + pushSetting.getIp()+ "Password=***"
                            + "Autofailover=" + pushSetting.getAutofailover()+ "Port=" + pushSetting.getPort()+ "Prefernece=" + _preference
                            + "Reserve1=" + pushSetting.getReserve1()+ "Reserve2" + pushSetting.getReserve2()+ "Reserve3=" + pushSetting.getReserve3()
                            + "RetryCount=" + pushSetting.getRetrycount()+ "Status=" + pushSetting.getStatus()
                            + "LogConfirmation=" + pushSetting.isLogConfirmation()+ "Retry Duration" + pushSetting.getRetryduration()
                            + "Application Type=" + _appType +"Bundle Id"+pushSetting.getBundleId()+"Certificate Path ="+pushSetting.getCertificatePath(),
                            itemtype, _channelId);
                } else if (retValue != 0) {
                    resultString = "ERROR";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change Apple Push notification Setting", resultString, retValue, "Setting Management",
                            "ChannelID=" + oldpushObj.getChannelId() + " Class Name=" + oldpushObj.getClassName()+ "ImplementationJar="
                            + " IP=" + oldpushObj.getIp()+ " Password=***" + " Autofailover=" + oldpushObj.getAutofailover()+ " Port=" + oldpushObj.getPort()
                            + " Prefernece=" + _preference + " Reserve1=" + oldpushObj.getReserve1()+ " Reserve2" + oldpushObj.getReserve2()+ " Reserve3=" + oldpushObj.getReserve3()
                            + " RetryCount=" + oldpushObj.getRetrycount()+ " Status=" + oldpushObj.getStatus()
                            + " LogConfirmation=" + oldpushObj.isLogConfirmation()+ " Retry Duration" + oldpushObj.getRetryduration()
                             + "Application Type=" + _oldappType +"Bundle Id"+oldpushObj.getBundleId()+"Certificate Path ="+oldpushObj.getCertificatePath(),
                            "Failed To Change Apple Push notification Setting", itemtype, _channelId);
                }   
                }

            }
        }

        if (retValue != 0) {
            result = "error";
            message = "Web Push Notification Gateway Settings Update Failed!!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }

    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
