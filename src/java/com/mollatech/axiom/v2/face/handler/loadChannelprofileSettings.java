/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech1
 */
public class loadChannelprofileSettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadChannelprofileSettings.class.getName());

    private JSONObject SettingsWhenEmpty(int _type1) {
        log.info("Servlet Started");
        JSONObject json = new JSONObject();
        int iglobal = SettingsManagement.CHANNELPROFILE_SETTING;

        String sep = System.getProperty("file.separator");
        String usrhome = System.getProperty("catalina.home");
        if (usrhome == null) {
            usrhome = System.getenv("catalina.home");
        }
        usrhome += sep + "axiomv2-settings";
        String rssarchivepath = usrhome + sep + "rss-archive";
        String _cleanuppath = usrhome + sep + "cleanup.log";
        String _bulkattachmentpath = usrhome + sep + "uploads" + sep;
        if (_type1 == iglobal) {
            try {
                json.put("_checkuser", 1);
                json.put("_alertuser", 1);
                json.put("_edituser", 1);
                json.put("_deleteuser", 1);
                json.put("_tokenload", "yes");
                json.put("_alertmedia", "sms");
                json.put("_softwaretype", "simple");
                json.put("_cleanuppath", _cleanuppath);
                json.put("_rssarchive", rssarchivepath);
                json.put("_uploads", _bulkattachmentpath);
                json.put("_otpspecification", "OCRA-1\\:HOTP-SHA1-6\\:QN08");
                json.put("_certspecification", "OCRA-1\\:HOTP-SHA1-8\\:QA08");
                json.put("_ocraspecification", "OCRA-1\\:HOTP-SHA1-8\\:QA08");
                json.put("_locationclassName", "com.mollatech.internal.handler.geolocation.AxiomLocationImpl");
                json.put("_resetuser", 1);
                json.put("_connectorstatus", 300);
                json.put("_cleanupdays", 180);
                json.put("_authorizer", 0);
                json.put("_Authorizer_PeningDuration", 3);
                json.put("_Authorizer_Units", 1);
//                json.put("_userValidityDays", 1825);
//                 json.put("_sweetSpotDeviation", 0);
//                json.put("_xDeviation", 0);
//                json.put("_yDeviation", 0);
                

            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }

        }
        return json;
    }

    private JSONObject SettingsWhenPresent(Object settingsObj) {
        JSONObject json = new JSONObject();
        if (settingsObj instanceof ChannelProfile) {
            ChannelProfile channelObj = (ChannelProfile) settingsObj;

            try {
                json.put("_checkuser", channelObj.checkUser);
                json.put("_alertuser", channelObj.alertUSer);
                json.put("_edituser", channelObj.editUser);
                json.put("_deleteuser", channelObj.deleteUser);
                json.put("_tokenload", channelObj.tokensettingload);
                json.put("_alertmedia", channelObj.alertmedia);
                json.put("_softwaretype", channelObj.swotptype);
                json.put("_cleanuppath", channelObj.cleanuppath);
                json.put("_rssarchive", channelObj.remotesignarchive);
                json.put("_uploads", channelObj.bulkemailattachment);
                json.put("_otpspecification", channelObj.otpspecification);
                json.put("_certspecification", channelObj.certspecification);
                json.put("_ocraspecification", channelObj.signspecification);
                json.put("_locationclassName", channelObj.locationclassname);

                json.put("_resetuser", channelObj.resetUser);
                json.put("_connectorstatus", channelObj.connectorStatus);
                json.put("_cleanupdays", channelObj.cleanupdays);
                json.put("_authorizer", channelObj.authorizationStatus);
                json.put("_Authorizer_PeningDuration", channelObj.authorizationDuration);
                json.put("_Authorizer_Units", channelObj.authorizationunit);
                
                json.put("_serverRestriction", channelObj._serverRestriction);
                json.put("_multipleSession", channelObj._multipleSession);
                json.put("_dayrestriction", channelObj._dayRestriction);
                json.put("_timerange", channelObj._timeFromInHour);
                json.put("_timefromampm", channelObj._timfromampm);
                json.put("_totimerange", channelObj._timeToInHour);
                json.put("_timetoampm", channelObj._timetoampm);
                if(channelObj._HardWareTokenUnitAutherization==true)
                    json.put("_UnitAutherizationForToken",1);
                else
                   json.put("_UnitAutherizationForToken",2);
//                json.put("_userValidityDays", channelObj._userValidityDays);
//                json.put("_sweetSpotDeviation", channelObj._sweetSpotDeviation);
//                json.put("_xDeviation", channelObj._xDeviation);
//                json.put("_yDeviation", channelObj._yDeviation);

            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
        }
        return json;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        JSONObject json = null;
        PrintWriter out = response.getWriter();
        log.debug("channel is::"+channel.getName());
        log.debug("sessionid::"+sessionId);

        try {
            SettingsManagement sMngmt = new SettingsManagement();
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), sMngmt.CHANNELPROFILE_SETTING, sMngmt.PREFERENCE_ONE);
            if (settingsObj != null) {
                json = SettingsWhenPresent(settingsObj);
            } else {
                json = SettingsWhenEmpty(sMngmt.CHANNELPROFILE_SETTING);
            }

        } catch (Exception ex) {
           log.error("Exception caught :: ",ex);
            // TODO handle custom exceptions here
        }
        try {
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
