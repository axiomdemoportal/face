package com.mollatech.axiom.v2.face.handler.operator;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PasswordTrailManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import static com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PASSWORD_POLICY_SETTING;
import static com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE;
import com.mollatech.axiom.nucleus.db.operation.AxiomOperator;
import com.mollatech.axiom.nucleus.settings.PasswordPolicySetting;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.json.JSONObject;

public class forgotpassword extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(forgotpassword.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final String itemTypeOp = "OPERATOR";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        
        log.info("is started :: ");

        String _operName = request.getParameter("_name");
        log.debug("_operName :: "+_operName);
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "New password is emailed sucessfully!!!";
        JSONObject json = new JSONObject();

        String _channelName = this.getServletContext().getContextPath();
        _channelName = _channelName.replaceAll("/", "");

        Channels channel = null;
        String sessionId = null;
        String[] credentialInfo = null;

        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAcess = suRemoteAcess.openSession();

        try {
            ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
            RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);

            channel = cUtil.getChannel(_channelName);
            if (channel == null) {
                result = "error";
                message = "Invalid channel!!";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                sRemoteAcess.close();
                suRemoteAcess.close();
                sChannel.close();
                suChannel.close();
                return;
            }

            credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
            if (credentialInfo == null) {
                result = "error";
                message = "Remote Access is not configured properly!!";
                try { json.put("_result", result);
                json.put("_message", message);}catch(Exception e){
                    log.error("exception caught :: ",e);
                }
                out.print(json);
                out.flush();
                sRemoteAcess.close();
                suRemoteAcess.close();
                sChannel.close();
                suChannel.close();
                return;
            }

            request.getSession().setAttribute("_apSessionRemoteAccessLoginID", credentialInfo[0]);

            SessionManagement sManagement = new SessionManagement();

            sessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());
            //String resultStr = ;

            if (sessionId == null) {
                result = "error";
                message = "Remote Access is disabled for this channel!!";
                try { json.put("_result", result);
                json.put("_message", message);}
                catch(Exception e){
                    log.error("exception caught :: ",e);
                }
                out.print(json);
                out.flush();
                sRemoteAcess.close();
                suRemoteAcess.close();
                sChannel.close();
                suChannel.close();
                return;
            }

        } catch (Exception e) {
            log.error("exception caught :: ",e);
        } finally {
            sRemoteAcess.close();
            suRemoteAcess.close();
            sChannel.close();
            suChannel.close();

        }

        //Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        //String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        AxiomOperator oper = null;
        OperatorsManagement operObj = new OperatorsManagement();
        oper = operObj.GetOperatorByName(sessionId, channel.getChannelid(), _operName);
        if (oper == null) {
            result = "error";
            message = "User Not Found...!!!";
            try { json.put("_result", result);
            json.put("_message", message);}
            catch(Exception e){
                log.error("exception caught :: ",e);
            }
            return;
        }

        //audit parameter
        //Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        String remoteaccesslogin = credentialInfo[0];//(String) request.getSession().getAttribute("_apSloginId");

        String _sendOrNot = "true";

        OperatorsManagement oManagement = new OperatorsManagement();
        AuditManagement audit = new AuditManagement();
        int retValue = 0;

        UtilityFunctions u = new UtilityFunctions();

        Date d = new Date();
        String strPassword = u.HexSHA1(sessionId + channel.getChannelid() + oper.getStrOperatorid() + d.toString());
        strPassword = strPassword.substring(0, 9);
        u = null;

        PasswordTrailManagement setObj = new PasswordTrailManagement();
        String password = setObj.GeneratePassword(channel.getChannelid(), strPassword);
        if (password != null && !password.isEmpty()) {
            strPassword = password;
        }
        SettingsManagement settObj = new SettingsManagement();
        Object obj = settObj.getSettingInner(channel.getChannelid(), PASSWORD_POLICY_SETTING, PREFERENCE_ONE);
        if (obj != null) {
            if (obj instanceof PasswordPolicySetting) {
                Operators op = oManagement.getOperatorById(channel.getChannelid(), oper.getStrOperatorid());
                PasswordPolicySetting passwordSetting = (PasswordPolicySetting) obj;
                //PasswordTrail pass = new PasswordTrail();
                Date pUpdateDate = op.getPasswordupdatedOn();
                Calendar pcurrent = Calendar.getInstance();
                if(pUpdateDate != null){
                pcurrent.setTime(pUpdateDate);
                }else{
                     pcurrent.setTime(new Date());
                }
                 String[] strOldPassword = setObj.getPasswordUsingOpId(channel.getChannelid(),op.getOperatorid() , passwordSetting.passwordGenerations);
              String pas = setObj.MD5HashPassword(strPassword);
              String str = "";
              if(strOldPassword != null){
                  for(int i = 0;i<strOldPassword.length;i++){
                      str = str +","+strOldPassword[i];
                  }
              }
              if(str.contains(pas)){
                    result = "error";
                    message = "Password already used in previous generations...!!!";
                    try { json.put("_result", result);
                    json.put("_message", message);}
                    catch(Exception e){
                       log.error("exception caught :: ",e);
                    }
                    out.print(json);
                    out.flush();
                    return;
              }
                
                pcurrent.set(Calendar.AM_PM, Calendar.AM);
                pcurrent.add(Calendar.MONTH, 1);//1 month
                Date pendDate = pcurrent.getTime();

                int issuePasswordCount = setObj.getcountOfIssuePassword(channel.getChannelid(), oper.getStrOperatorid(), pendDate, pUpdateDate);
                if (passwordSetting.issuingLimitDuration <= issuePasswordCount) {
                    result = "error";
                    message = "Password Issue Limit Reach...!!!";
                    try { json.put("_result", result);
                    json.put("_message", message);
                    }catch(Exception e){
                        log.error("exception caught :: ",e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
            }
        }

        retValue = oManagement.SetPassword(sessionId, channel.getChannelid(), oper.getStrOperatorid(), strPassword, false);
        log.debug("SetPassword :: "+retValue);

        if (retValue == 0) {
            //PasswordTrail pass = new PasswordTrail();
//            AxiomOperator op = oManagement.GetOperatorByName(sessionId, channel.getChannelid(), _op_name);
            String pas = setObj.MD5HashPassword(strPassword);
            setObj.AddPasswordTrail(channel.getChannelid(), oper.getStrOperatorid(), pas);
        }
        String resultString = "ERROR";
        if (retValue == 0) {
            resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), oper.getStrOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, oper.getStrName(), new Date(),
                    "Forgot Password", resultString, retValue, 
                    "Operator Management",
                    "Old Password =*****", "New Password =*****", 
                    itemTypeOp, 
                    oper.getStrOperatorid());

        } else if (retValue != 0) {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), oper.getStrOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, oper.getStrName(), new Date(),
                    "Forgot Password", resultString, retValue, 
                    "Operator Management",
                    "Old Password =*****", "Failed To execute forgot password", itemTypeOp, 
                    oper.getStrOperatorid());

        }

        if (retValue == 0) {
            if (_sendOrNot.compareTo("true") == 0) {
                retValue = oManagement.ResendPassword(sessionId, channel.getChannelid(), oper.getStrOperatorid(), true);
                log.debug("ResendPassword :: "+retValue);
                
                if (retValue != 0) {
                    result = "error";
                    message = "New password could not be issued!!!";
                    try { json.put("_result", result);
                    json.put("_message", message);
                    }catch(Exception e){
                        log.error("exception caught :: ",e);
                    }
                    return;
                }
            }

        } else {
            result = "error";
            message = "New password could not be issued!!!";
            try { json.put("_result", result);
            json.put("_message", message);}
            catch(Exception e){
                log.error("exception caught :: ",e);
            }
            return;
        }

        try {
            json.put("_result", result);
            json.put("_message", message);

        }catch(Exception e)
        {
            log.error("exception caught :: ",e);
        } 
        finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
