/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.otptokens;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Otptokens;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Manoj Sherkhane
 */
public class replaceToken extends HttpServlet {
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(replaceToken.class.getName());

    final String itemType = "OTPTOKENS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        log.info("is started :: ");
        
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());

        String _userid = request.getParameter("_userIDReplaceHW");
        log.debug("_userid :: "+_userid);
        String _oldTokenSerialNo = request.getParameter("_oldTokenSerialNo");
        log.debug("_oldTokenSerialNo :: "+_oldTokenSerialNo);
        String _newTokenSerialNo = request.getParameter("_newTokenSerialNo");
        log.debug("_newTokenSerialNo :: "+_newTokenSerialNo);
        String _oldTokenStatus = request.getParameter("_oldTokenStatus");
        log.debug("_oldTokenStatus :: "+_oldTokenStatus);

        String _categoryReplaceHW = request.getParameter("_categoryReplaceHW");
        log.debug("_categoryReplaceHW :: "+_categoryReplaceHW);
        String _subcategoryReplaceHW = request.getParameter("_subcategoryReplaceHW");
        log.debug("_subcategoryReplaceHW :: "+_subcategoryReplaceHW);

        String resultString = "ERROR";
        String result = "success";
        String message = "Token Replaced Successfully....";
        String strPassword = null;
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        if (_userid == null || _oldTokenSerialNo == null
                || _newTokenSerialNo == null || _categoryReplaceHW == null
                || _oldTokenStatus == null || _subcategoryReplaceHW == null) {
            result = "error";
            message = "Invalid Parameters!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        int category = 0;
        if (_categoryReplaceHW != null) {
            category = Integer.parseInt(_categoryReplaceHW);
        }
        int subcategory = 0;
        if (_subcategoryReplaceHW != null) {
            subcategory = Integer.parseInt(_subcategoryReplaceHW);
        }
        int ioldTokenStatus = 0;
        if (_oldTokenStatus != null) {
            ioldTokenStatus = Integer.parseInt(_oldTokenStatus);
        }

        int retValue = -1;

        OTPTokenManagement oManagement = new OTPTokenManagement(channel.getChannelid());
        AuditManagement audit = new AuditManagement();
        Otptokens oldtoken = oManagement.getOtpObjByUserId(sessionId, channel.getChannelid(), _userid, category);
//        int oldTimeDiff = oldtoken.getTimediff();
        if (oldtoken == null) {
            result = "error";
            message = "Token not found!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }


        int status = 0;
        //unassighn token current
         status = oManagement.ChangeStatus(channel.getChannelid(), _userid, ioldTokenStatus, category, subcategory);
         log.debug("ChangeStatus :: "+status);

        if (status == 0 || status == -6) {
            resultString = "SUCCESS";
            AuditManagement a = new AuditManagement();
            a.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Replace Token (Change Status)",
                    resultString, retValue, "Token Management",
                    "", 
                    "category=" + category + ", subcategory=" + subcategory + ",ioldTokenStatus=" + ioldTokenStatus,
                    itemType, 
                    _userid);
            //Assign new
            retValue = oManagement.AssignToken(sessionId, channel.getChannelid(), _userid, category, subcategory, _newTokenSerialNo);
            log.debug("AssignToken :: "+retValue);
        }
        
        if (retValue == 0) {
            resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Replace Token (Assign Token)",
                    resultString, retValue, "Token Management",
                    "", 
                    "category=" + category + ", subcategory=" + subcategory + ",_newTokenSerialNo=" + _newTokenSerialNo,
                    itemType, _userid);
        }
        else {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), 
                    "Replace Token",
                    resultString, retValue,
                    "Token Management",
                    "", 
                    "Failed to Replace Token",
                    itemType,
                    _userid);

            result = "error";
            message = "Replace token failed!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
