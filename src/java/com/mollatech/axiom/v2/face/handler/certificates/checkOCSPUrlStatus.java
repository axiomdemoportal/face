/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.certificates;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author pramodchaudhari
 */
public class checkOCSPUrlStatus extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("json/application");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        try {

            String ocspUrl = request.getParameter("ocspurl");
            URL u = null;
            if (ocspUrl != null) {
                u = new URL(ocspUrl);
            } else {
                json.put("_result", "error");
                json.put("_messsage", "Invalid Ocsp url");
                out.print(json);
                return;
            }
            HttpURLConnection huc = (HttpURLConnection) u.openConnection();
            huc.setRequestMethod("GET");
            huc.connect();
            OutputStream os = huc.getOutputStream();
            int code = huc.getResponseCode();
            System.out.println("Resp Code "+code);
            if (code != 404) {
                json.put("_result", "success");
                json.put("_messsage", "Valid Ocsp url");

                out.print(json);

            } else {
                json.put("_result", "error");
                json.put("_messsage", "Invalid Ocsp url");
                out.print(json);
                return;

            }
        } catch (Exception ex) {
//            ex.printStackTrace();
            try {
                json.put("_result", "error");
                json.put("_messsage", "Invalid Ocsp url");
                out.print(json);
                return;
            } catch (Exception ex1) {
                ex1.printStackTrace();
            }

        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
