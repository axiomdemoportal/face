/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.operator;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.operation.AxiomOperator;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class listoperators extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(listoperators.class.getName());

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        PrintWriter out = response.getWriter();
        AxiomOperator[] AxiomOperartor = null;
        ArrayList<AxiomOperator> AxiomOperartorlist = new ArrayList<AxiomOperator>();
        JsonArray jsonArray = new JsonArray();
        try {
            OperatorsManagement oManagement = new OperatorsManagement();
            AxiomOperator[] axiomoperator = oManagement.ListOperators(sessionId, channel.getChannelid());
            Collections.addAll(AxiomOperartorlist, AxiomOperartor);
            Gson gson = new Gson();
            JsonElement element = gson.toJsonTree(AxiomOperartorlist, new TypeToken<List<AxiomOperator>>() {
            }.getType());
            jsonArray = element.getAsJsonArray();
            // json.put("user", uObj);

        } catch (Exception ex) {
            log.error("exception caught:: ",ex);
            // TODO handle custom exceptions here
        } finally {
            out.print(jsonArray);
            out.flush();
        }
        log.info("is ended :: ");
    }
}
