package com.mollatech.axiom.v2.face.handler.channel;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Roles;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.Units;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ErrorMessageManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.RemoteAccessManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet;
import com.mollatech.axiom.nucleus.db.operation.AxiomChannel;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.ContactTagsSetting;
import com.mollatech.axiom.nucleus.settings.IPConfigFilter;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class addchannel extends HttpServlet {
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(addchannel.class.getName());
    static final int ADMIN = 1;
    static final int HELP_DESK = 2;
    static final int REPORTOR = 3;
    static final int ACTIVE = 1;
    final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;
    final String itemType = "CHANNEL";
    final String itemTypeOp = "OPERATOR";
    final String itemTypeRE = "REMOTEACCESS";
    final String itemTypeUnits = "UNITS";

    final String itemtypetags = "SETTINGS";
    final int FIRST_UNIT_AS_0 = 0;
    final int FIRST_OPERATOR_AS_0 = 0;
    

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        log.debug("#addchannel# :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel::"+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId::"+sessionId);
        //audit parameter
        //String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        
        String operatorId = operatorS.getOperatorid();
        log.debug("operatorId::"+operatorId);
        String _ch_name = request.getParameter("_ch_name");
        log.debug("_ch_name::"+_ch_name);
        String _ch_virtual_path = request.getParameter("_ch_virtual_path");
        log.debug("_ch_virtual_path::"+_ch_virtual_path);
        String _ch_status = request.getParameter("_ch_status");
        log.debug("addchannel::Status is::"+_ch_status);
        int status = Integer.parseInt(_ch_status);
        String _op_name = request.getParameter("_op_name");
        log.debug("_op_name::"+_op_name);
        String _op_email = request.getParameter("_op_email");
         log.debug("_op_email::"+_op_email);
        String _op_phone = request.getParameter("_op_phone");
        log.debug("_op_phone::"+_op_phone);
        
   
        
        File sourcePath = new File(
                LoadSettings.g_sSettings.getProperty("axiom.dir")
                + System.getProperty("file.separator")
                + LoadSettings.g_sSettings.getProperty("axiom.face.template"));
        File destinationPath = new File(
                LoadSettings.g_sSettings.getProperty("axiom.dir")
                + System.getProperty("file.separator")
                + _ch_virtual_path
                + ".war");
        File coresourcePath = new File(
                LoadSettings.g_sSettings.getProperty("axiom.dir")
                + System.getProperty("file.separator")
                + LoadSettings.g_sSettings.getProperty("axiom.core.template"));
        File coreDestinationPath = new File(
                LoadSettings.g_sSettings.getProperty("axiom.dir")
                + System.getProperty("file.separator")
                + _ch_virtual_path + "core"
                + ".war");
        
          File moibleTrustV2sourcePath = new File(
                LoadSettings.g_sSettings.getProperty("axiom.dir")
                + System.getProperty("file.separator")
                + LoadSettings.g_sSettings.getProperty("axiom.MobileTrustV2.template"));
        File mobileTrustv2ServiceDestinationPath = new File(
                LoadSettings.g_sSettings.getProperty("axiom.dir")
                + System.getProperty("file.separator")
                + _ch_virtual_path + "MobileTrustv2Service"
                + ".war");
        
        String result = "success";
        String message = "Channel added successfully....";

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
     
        if (_ch_status == null || _ch_name == null || _ch_virtual_path == null) {
            result = "error";
            message = "Invalid Channel Details!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught ::",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        int iChannelCount = AxiomProtect.GetChannelsAllowed();

        if (iChannelCount < 1) {
            result = "error";
            message = "Channel Addition is not available in this license!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
                
            }
            out.print(json);
            out.flush();
            return;
        }

        ChannelManagement cm = new ChannelManagement();
        AxiomChannel[] ch = cm.ListChannelsInternal();
        int iCurrentChannelCount = ch.length;

        if (iCurrentChannelCount >= iChannelCount) {
            result = "error";
            message = "Channel Addition already reached its limit as per this license!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
                
            }
            out.print(json);
            out.flush();
            return;
        }

        if (_op_name == null || _op_email == null || _op_phone == null) {
            result = "error";
            message = "Invalid Administrator Details!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught ::",e);

            }
            out.print(json);
            out.flush();
            return;
        }

        if (UtilityFunctions.isValidEmail(_op_email) == false) {
            result = "error";
            message = "Invalid email id!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);

            }
            out.print(json);
            out.flush();
            return;
        }

        if (UtilityFunctions.isValidPhoneNumber(_op_phone) == false) {
            result = "error";
            message = "Invalid phone number!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);

            }
            out.print(json);
            out.flush();
            return;
        }

        int retValue = -1;

        try {
            ChannelManagement cManagement = new ChannelManagement();
            AuditManagement aAudit = new AuditManagement();
            Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
            String RAloginid = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("RAloginid is::"+RAloginid);

            int iIsUnique = cManagement.checkIsUnique(_ch_name, _ch_virtual_path);
            if (iIsUnique == 0) {
                retValue = cManagement.AddChannel(sessionId, _ch_name, _ch_virtual_path, status);
                log.debug("addchannel::AddChannel::"+retValue);
                String resultStr = "Fail";

                String strStatus = "SUSPEND_STATUS";
                if (status == ACTIVE_STATUS) {
                    strStatus = "ACTIVE_STATUS";
                }

                if (retValue == 0) {
                    resultStr = "Success";
                    aAudit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(),
                            RAloginid, operator.getName(), new Date(), "Add", resultStr, retValue,
                            "Channel Management", "", "Name =" + _ch_name + ",Virtual Path =" + _ch_virtual_path + ",Status =" + strStatus,
                            itemType, operatorId);
                } else if (retValue != 0) {
                    aAudit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(),
                            RAloginid, operator.getName(), new Date(), "Add", resultStr, retValue,
                            "Channel Management", "-", "Failed To add new channel",
                            itemType, operatorId);

                }

                SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
                Session sChannel = suChannel.openSession();
                ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
                Channels newchannelObj = cUtil.getChannel(_ch_name);
                suChannel.close();
                sChannel.close();

                OperatorsManagement oManagement = new OperatorsManagement();
                retValue = oManagement.CreateResourses(newchannelObj.getChannelid());
                 log.debug("CreateResourses::"+retValue);
                retValue = oManagement.CreateRolesAll(newchannelObj.getChannelid());
                 log.debug("CreateResourses::"+retValue);
                retValue = oManagement.createAccess(newchannelObj.getChannelid());
                 log.debug("CreateResourses::"+retValue);
                Roles[] roles = oManagement.getAllRoles(newchannelObj.getChannelid());
                int roleid = 0;

                if (roles != null) {
                    for (int i = 0; i < roles.length; i++) {
                        if (roles[i].getName().equalsIgnoreCase("sysadmin")) {
                            roleid = roles[i].getRoleid();
                        }
                    }
                }

                UnitsManagemet u = new UnitsManagemet();
                retValue = u.CreatetUnitss(newchannelObj.getChannelid());
                log.debug("CreatetUnitss::"+retValue);

                if (retValue == 0) {
                    resultStr = "Success";
                    aAudit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                            request.getRemoteAddr(), channel.getName(), RAloginid,
                            operator.getName(), new Date(), "Add Units", resultStr, retValue,
                            "Units Management",
                            "-", "-", itemTypeUnits, operatorId);
                } else if (retValue != 0) {
                    aAudit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                            request.getRemoteAddr(), channel.getName(), RAloginid,
                            operator.getName(), new Date(), "Add Units", resultStr, retValue,
                            "Units Management",
                            "-", "Failed to add Units", itemTypeUnits, operatorId);
                }
                //added by nilesh for units.
                Units[] units=u.ListUnitss(sessionId, channel.getChannelid());
                int unitId=0;
                if(units!=null)
                {
                unitId=units[0].getUnitid();
                }
                Date d = new Date();
                UtilityFunctions utilsFns = new UtilityFunctions();
                String strPassword = utilsFns.HexSHA1(d.toString() + _op_name + _op_email + _op_phone + roleid);
                strPassword = strPassword.substring(0, 7);
                
                retValue = oManagement.AddOperator(sessionId, newchannelObj.getChannelid(),
                        _op_name, strPassword,
                        _op_phone, _op_email,
                        roleid, ACTIVE, unitId,FIRST_OPERATOR_AS_0);
                log.debug("AddOperator::"+retValue);

                resultStr = "Fail";

                if (retValue == 0) {
                    resultStr = "Success";
                    aAudit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), RAloginid, operator.getName(), new Date(), "Add",
                            resultStr, retValue, "Operator",
                            "-", "Name=" + _op_name + ";Email=" + _op_email + ";Phone=" + _op_phone,
                            itemTypeOp, operatorId);

                    //added to send the email to newly created 
                    try {
                        Operators firstOper = oManagement.GetOperatorInner(sessionId, newchannelObj.getChannelid(), _op_name);
                        SendNotification send = new SendNotification();
                        TemplateManagement tManagement = new TemplateManagement();
                        Templates templates = tManagement.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.EMAIL_OPERATOR_PASSWORD_TEMPLATE);
                        if (templates.getStatus() == ACTIVE_STATUS) {
                            ByteArrayInputStream bais = new ByteArrayInputStream(templates.getTemplatebody());
                            String messageBody = (String) TemplateUtils.deserializeFromObject(bais);
                            String subject = templates.getSubject();
                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                            if (messageBody != null) {
                                // Date date = new Date();
                                messageBody = messageBody.replaceAll("#name#", firstOper.getName());
                                messageBody = messageBody.replaceAll("#channel#", _ch_name);
                                messageBody = messageBody.replaceAll("#email#", firstOper.getEmailid());
                                messageBody = messageBody.replaceAll("#datetime#", sdf.format(d));
                                messageBody = messageBody.replaceAll("#password#", firstOper.getPasssword());
                            }

                            if (subject != null) {
                                //   Date d = new Date();
                                subject = subject.replaceAll("#channel#", channel.getName());
                                subject = subject.replaceAll("#datetime#", sdf.format(d));
                            }

                            /*String [] files = new String[1];
                             files[0] = "/Users/vikramsareen/Desktop/all/tasklist.txt";
                             String [] mimes = new String[1];
                             mimes[0] = "text/plain";
                             */
                            AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), _op_email, subject, messageBody,
                                    null, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                        }
                    } catch (Exception e) {
                        log.error("exception caught :: ",e);
                       
                    }
                    //end to addition

                } else if (retValue != 0) {
                    aAudit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), RAloginid, operator.getName(), new Date(), "Add",
                            resultStr, retValue, "Operator",
                            "-", "Failed to add operator",
                            itemTypeOp, operatorId);
                }

                strPassword = utilsFns.HexSHA1(_op_name + _op_email + _op_phone + strPassword);
                strPassword = strPassword.substring(0, 9);

                String login = utilsFns.HexSHA1(_op_name + _op_email + _op_phone + strPassword);
                login = login.substring(0, 11);

                RemoteAccessManagement ramObj = new RemoteAccessManagement();
                retValue = ramObj.SetRemoteAccessCredentials(sessionId, newchannelObj.getChannelid(), login, strPassword);
                log.debug("SetRemoteAccessCredentials::"+retValue);
                //retValue = ramObj.enableRemoteAccess(sessionId, newchannelObj.getChannelid(), false);
                resultStr = "Fail";
                if (retValue == 0) {
                    resultStr = "Success";
                    aAudit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                            request.getRemoteAddr(), channel.getName(), RAloginid,
                            operator.getName(), new Date(), "Add RemoteAccess", resultStr, retValue,
                            "RemoteAccess",
                            "-", "login=" + login + ";password=*****", itemTypeRE, operatorId);
                } else if (retValue != 0) {
                    aAudit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                            request.getRemoteAddr(), channel.getName(), RAloginid,
                            operator.getName(), new Date(), "Add RemoteAccess", resultStr, retValue,
                            "RemoteAccess",
                            "-", "login=" + login + ";password=*****", itemTypeRE, operatorId);
                }

                //adding for social tags 
                int _iPreference = 1;//Integer.parseInt(_preference);

                //String _type = request.getParameter("_type");
                int _itype = SettingsManagement.CONTACT_TAGS;//Integer.parseInt(_type);
                SettingsManagement sMngmt = new SettingsManagement();
                ContactTagsSetting contentObj = null;
                Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _itype, _iPreference);

                if (settingsObj == null) {
                    contentObj = new ContactTagsSetting();
                    contentObj.setChannelId(channel.getChannelid());

                } else {
                    contentObj = (ContactTagsSetting) settingsObj;
                }
                String _content = "facebook,twitter,linkedin";
                String[] _contentsArray = _content.split(",");

                contentObj.setTags(_contentsArray);
                IPConfigFilter ipConfig = new IPConfigFilter();

                retValue = sMngmt.addSetting(sessionId, channel.getChannelid(), _itype, _iPreference, contentObj);
                log.debug("addSetting::"+retValue);
                retValue = sMngmt.addSetting(sessionId, channel.getChannelid(), SettingsManagement.IP_FILTER, _iPreference, ipConfig);
                log.debug("addSetting::"+retValue);
                AuditManagement audit = new AuditManagement();
                String[] arrTags = contentObj.getTags();
                String strTags = null;
                for (int i = 0; i < arrTags.length; i++) {
                    strTags += arrTags[i] + ",";
                }
                if (retValue == 0) {
                    resultStr = "Success";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), RAloginid, operatorS.getName(), new Date(),
                            "Add Tags Setting", resultStr, retValue, "Setting Management",
                            "", "New Tags =" + strTags, itemtypetags, operatorId);
                } else if (retValue != 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), RAloginid, operatorS.getName(), new Date(),
                            "Add Tags Setting", resultStr, retValue, "Setting Management",
                            "", "Failed to add Tags Setting..!!!", itemtypetags, operatorId);
                }
                //end of addition

                //new addition
                TemplateManagement tmObj = new TemplateManagement();
                ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
                retValue =errmsgObj.Createerrormessages(newchannelObj.getChannelid());
                log.debug("Createerrormessages::"+retValue);
                retValue = tmObj.Createtemplates(newchannelObj.getChannelid());
                log.debug("Createtemplates::"+retValue);
                resultStr = "Fail";
                if (retValue == 0) {
                    resultStr = "Success";
                    aAudit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                            request.getRemoteAddr(), channel.getName(), RAloginid,
                            operator.getName(), new Date(), "Add Templates", resultStr, retValue,
                            "Templates",
                            "-", "-", itemTypeRE, operatorId);
                } else if (retValue != 0) {
                    aAudit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                            request.getRemoteAddr(), channel.getName(), RAloginid,
                            operator.getName(), new Date(), "Add Templates", resultStr, retValue,
                            "Templates",
                            "-", "Failed to add templates", itemTypeRE, operatorId);
                }
                //end of addition

                utilsFns.copyFile(sourcePath, destinationPath);
                utilsFns.copyFile(coresourcePath, coreDestinationPath);
                utilsFns.copyFile(moibleTrustV2sourcePath, mobileTrustv2ServiceDestinationPath);
            } else {
                message = "Name/Virtual Path is already taken!!";
            }
        } catch (Exception ex) {
            log.error("exception caught :: ",ex);
            
        }
        if (retValue != 0) {
            result = "error";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            log.error("exception caught :: ",e);

        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

//    private String SHA1(String message) {
//        try {
//            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
//            byte[] array = sha1.digest(message.getBytes());
//            return arrayToString(array);
//        } catch (Exception e) {
//            return null;
//        }
//    }
//    private String arrayToString(byte[] array) {
//        StringBuffer sb = new StringBuffer();
//        for (int i = 0; i < array.length; ++i) {
//            sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
//        }
//        return sb.toString();
//    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
