/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.axiom.v2.face.handler.operator.errormessages;

import static com.mollatech.axiom.common.utils.UtilityFunctions.deserializeFromObject;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Errormessages ;
import com.mollatech.axiom.nucleus.db.connector.ErrorMessageUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class loademessage extends HttpServlet {
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loademessage.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       response.setContentType("text/html;charset=UTF-8");
       
       log.info("is started :: ");
       
        String _tid = request.getParameter("_tid");
        int _templateid = Integer.valueOf(_tid);
        log.debug("_templateid :: "+_templateid);
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        
        try {
            /* TODO output your page here. You may use following sample code. */
            
            SessionFactoryUtil suTem = new SessionFactoryUtil(SessionFactoryUtil.errormessages);
            Session sTem = suTem.openSession();
            ErrorMessageUtils tmpU= new ErrorMessageUtils(suTem, sTem);
            Errormessages tObj = tmpU.getTemplates(channel.getChannelid(), _templateid);
            sTem.close();
            suTem.close();
            
            //ByteArrayInputStream bais = new ByteArrayInputStream(tObj.getErrorMessage());
            //String templatebody = (String) deserializeFromObject(bais);
            String templatebody = tObj.getUsermessage();
            
            json.put("_tid", _templateid); 
            
            json.put("_name",  tObj.getErrorname());
            //mobile
                json.put("_body", templatebody);
           
            json.put("_result", "success");
            
            
        } catch (Exception ex) {
            // TODO handle custom exceptions here
            try { json.put("_result", "error");
            json.put("_message", ex.getMessage());}catch(Exception e){
            log.error("exception caught :: ",e);
            }
        } finally {
            out.print(json);
            out.flush();
        }
        
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
