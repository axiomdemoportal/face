/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.users;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
//import com.mollatech.internal.handler.user.source.AxiomUser;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class adduser extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(adduser.class.getName());

    static final int PHONENUMBER = 2;
    static final int EMAILID = 3;
    final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;
    final int LOCKED_STATUS = -1;
    final int REMOVE_STATUS = -99;
    final String itemtype = "USERPASSWORD";
    final String itemTypeAUTH = "AUTHORIZATION";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());

        String _user_name = request.getParameter("_Name");
        log.debug("_user_name :: "+_user_name);
        String _user_email = request.getParameter("_Email");
        log.debug("_user_email :: "+_user_email);
        String _user_phone = request.getParameter("_Phone");
        log.debug("_user_phone :: "+_user_phone);
        String _user_group = request.getParameter("_groupid");
        log.debug("_user_group :: "+_user_group);
        String result = "success";
        String message = "User added successfully....";
        int groupid = 0;
        if (_user_group != null) {
            groupid = Integer.parseInt(_user_group);
        }
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        //amol
        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(_user_name);
        boolean b = m.find();
        Pattern p1 = Pattern.compile("[0-9]", Pattern.CASE_INSENSITIVE);
        Matcher m1 = p1.matcher(_user_name);
        boolean b1 = m1.find();
        boolean spaces = false;
        if (_user_name != null) {
            for (int i = 0; i < _user_name.length(); i++) {
                if (Character.isWhitespace(_user_name.charAt(i))) {
                    spaces = true;
                }
            }
        }
        //end amol

        if (_user_name == null || _user_email == null || _user_phone == null
                || _user_name.isEmpty() == true || _user_email.isEmpty() == true || _user_phone.isEmpty() == true) {
            result = "error";
            message = "Fill all Details!!";

            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        } // amol
        else if (_user_name == null) {
            result = "error";
            message = "Name could not Empty !!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        } else if (_user_name.length() < 3) {
            result = "error";
            message = "Name should be more than 3 characters!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        } else if (b) {
            result = "error";
            message = "Name should not contain any symbols!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        } 
//        else if (b1) {
//            result = "error";
//            message = "Name should not contain only digits!!!";
//            try {
//                json.put("_result", result);
//                json.put("_message", message);
//            } catch (Exception ex) {
//                log.error("Exception caught :: ",ex);
//            }
//            out.print(json);
//            out.flush();
//            return;
//        } 
        else if (spaces) {
            result = "error";
            message = "Name should not contain any blank spaces!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        }
        // end by amol

        if (UtilityFunctions.isValidEmail(_user_email) == false) {
            result = "error";
            message = "Invalid email id!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
               log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        if (UtilityFunctions.isValidPhoneNumber(_user_phone) == false) {
            result = "error";
            message = "Invalid phone number!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        
        if(_user_phone.length() < 6 || _user_phone.length() > 16){
             result = "error";
            message = "Invalid phone number!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        

        int retValue = -1;

        UserManagement uManagement = new UserManagement();

        ///License Changes are Added Here 
      
        int iUserCount = uManagement.getCountOfLicenseUser(channel.getChannelid());
//        if (iUserCount < 1) {
//            result = "error";
//            message = "User Addition is not available in this license!!!";
//            try {
//                json.put("_result", result);
//                json.put("_message", message);
//            } catch (Exception e) {
//                log.error("Exception caught :: ",e);
//            }
//            out.print(json);
//            out.flush();
//            return;
//        }
        int licensecount = AxiomProtect.GetUsersAllowed(); //AxiomProtect Function here now default 0. 
        if (licensecount == -998) {
            //unlimited licensing 
        } else if (iUserCount >= licensecount) {
            result = "error";
            message = "User Addition already reached its limit as per this license!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        //start of authorization - Nilesh

//        retValue = uManagement.CreateUser(sessionId, channel.getChannelid(), _user_name, _user_phone, _user_email, null, groupid, "MSC Trustgate.com Sdn Bhd.", "140541211211", "Passport", "Business Development", "MY", "KL", "Mont Kiara", "Manager");
        retValue = uManagement.CreateUserFace(sessionId, channel.getChannelid(), _user_name, _user_phone, _user_email, null, groupid, "MSC Trustgate.com Sdn Bhd.", "140541211211", "Passport", "Business Development", "MY", "KL", "Mont Kiara", "Manager");
        log.debug("CreateUserFace :: "+retValue);
        AuditManagement audit = new AuditManagement();
        String resultString = "Failure";
        if (retValue == 0) {
            resultString = "Success";
        }
        String userFlag = LoadSettings.g_sSettings.getProperty("check.user");
        AuthUser user = null;
        if (userFlag.equals("1")) {
            user = uManagement.CheckUserByType(sessionId, channel.getChannelid(), _user_name, 4);
        } else {
            user = uManagement.CheckUserByType(sessionId, channel.getChannelid(), _user_phone, PHONENUMBER);
        }
        if (retValue == 0) {

           

            if (user != null) {
                String strStatus = "";
                if (user.getStatePassword() == ACTIVE_STATUS) {
                    strStatus = "ACTIVE_STATUS";
                } else if (user.getStatePassword() == SUSPEND_STATUS) {
                    strStatus = "SUSPEND_STATUS";
                } else if (user.getStatePassword() == LOCKED_STATUS) {
                    strStatus = "LOCKED_STATUS";
                } else if (user.getStatePassword() == REMOVE_STATUS) {
                    strStatus = "REMOVE_STATUS";
                }

                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operatorS.getName(), new Date(), "Create User", resultString, retValue,
                        "User Management", "",
                        "User Id =" + user.getUserId() + ",Name = " + _user_name + ",Phone =" + _user_phone + ",Email =" + _user_email + ",Password State=" + strStatus,
                        itemtype, user.getUserId());
            }

        } else {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Create User", resultString, retValue,
                    "User Management", "", "Failed To Create User", itemtype, "Failed");
            result = "error";
            message = "Duplicate Entries for User Name/Email/Phone) is not allowed!!!";
            if (retValue == -2) {
                result = "error";
                message = "Please Check the Usersource Settings it is not Configured Properly!!!";
            }
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
