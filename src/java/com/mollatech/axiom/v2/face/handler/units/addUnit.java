package com.mollatech.axiom.v2.face.handler.units;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Units;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;

/**
 *
 * @author nilesh
 */
public class addUnit extends HttpServlet {
     static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(addUnit.class.getName());

    final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;
    final String itemType = "UNITS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);

        //  Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());
        String operatorId = operatorS.getOperatorid();
        String _unitName = request.getParameter("_unitName");
        log.debug("_unitName :: "+_unitName);
        String _status = request.getParameter("_unitStatus");
        log.debug("_status :: "+_status);
        String _thresholdLimit = request.getParameter("_thresholdLimit");
        log.debug("_thresholdLimit :: "+_thresholdLimit);
        String result = "success";
        String message = "Units Added successfully....";

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        if (StringUtils.isWhitespace(result)) {
            result = "error";
            message = "Please, Fill correct data!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        if (_unitName == null || _status == null || _thresholdLimit == null) {
            result = "error";
            message = "Please, Fill correct data!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        
        Pattern pattern = Pattern.compile("[^a-zA-Z0-9*]");
        
        
//        Matcher matcher = pattern.matcher(_unitName);
        if (_unitName.isEmpty() == true || _status.isEmpty() == true || _thresholdLimit.isEmpty() == true ) {
            result = "error";
            message = "Please, Fill correct data!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        int ithresholdLimit = 0;
        try {
            ithresholdLimit = Integer.parseInt(_thresholdLimit);
        } catch (Exception ex) {
            log.error("exception caught :: ",ex);
            result = "error";
            message = "Threshold Limit must be integer!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
          if(ithresholdLimit < 0){
            result = "error";
            message = "Threshold Limit must be +ve no!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        int iStatus = Integer.valueOf(_status);

        int retValue = -1;
        Units checkduplicate = null;
        UnitsManagemet tObj = new UnitsManagemet();
        AuditManagement audit = new AuditManagement();
//        checkduplicate = tObj.CheckTemplate(sessionId, channel.getChannelid(), _templatename);

        checkduplicate = tObj.getUnitByUnitName(sessionId, channel.getChannelid(), _unitName);

        if (checkduplicate != null) {      // 0 is present so it is duplication. report error
            result = "error";
            message = "Duplicate Name!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        retValue = tObj.addUnit(sessionId, channel.getChannelid(), _unitName, iStatus, ithresholdLimit, new Date(), new Date());
        log.debug("addUnit :: "+retValue);

        String oldStatus = "Active";
//         if (oldUnit.getStatus() == ACTIVE_STATUS) {
//            oldStatus = "Active ";
//        } 
        String resultString = "ERROR";
        if (retValue == 0) {
            resultString = "SUCCESS";

            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Add Unit", resultString, retValue,
                    "Units Management", "", "Unit Name=" + _unitName + "Unit Status" + oldStatus + "Threshold Limit=" + ithresholdLimit + "created on =" + new Date()
                    + "Updated On = " + new Date(),
                    itemType, operatorId);

        } else if (retValue != 0) {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Add Unit", resultString, retValue,
                    "Units Management", "", "Failed To Add Unit...!!!",
                    itemType, operatorId);
        }

        if (retValue == 0) {
            result = "success";
        } else {
            result = "error";
            message = "Unit Addition Failed!!!";

        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();

        }
        log.info("is ended :: ");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
