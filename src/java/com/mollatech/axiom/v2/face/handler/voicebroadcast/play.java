
package com.mollatech.axiom.v2.face.handler.voicebroadcast;

import com.twilio.sdk.verbs.Play;
import com.twilio.sdk.verbs.Say;
import com.twilio.sdk.verbs.TwiMLResponse;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mollatech2
 */
public class play extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(play.class.getName());

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
      //  response.setContentType("text/html;charset=UTF-8");
        TwiMLResponse twiml = new TwiMLResponse();
        Say say = null;
        PrintWriter out = response.getWriter();
        String message = request.getParameter("_url");
        log.debug("message :: "+message);
        try {
            /* TODO output your page here. You may use following sample code. */
            try {
                if (message == null || message.equalsIgnoreCase("null")) {
                    say = new Say("There is No Recording Found");
                    twiml.append(say);
                  
                } else {
                    say = new Say("Here is what I heard");
                    Play play = new Play(message);
                    twiml.append(say);
                    twiml.append(play);
                    //  message = message;
                }
                response.setContentType("application/xml");
                response.getWriter().print(twiml.toXML());
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
        } finally {
            out.close();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
