/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.PushWatchManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class sendCustomisePush extends HttpServlet {
  
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(sendCustomisePush.class.getName());

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        String _type = request.getParameter("_Pushtype");
        String _groupname = request.getParameter("_groupname");
        String _testmsg = request.getParameter("_Pushmsg");
        int _preference1=0;
        String _testphone = null;
       

        log.debug("testConnection::channel is::"+channel.getName());
        log.debug("testConnection::sessionid::"+sessionId);
        log.debug("testConnection::type is::"+_type);
        log.debug("testConnection::groupname is::"+_groupname);
         log.debug("testConnection::Pushmsg is::"+_testmsg);
       
        if (_type.compareTo("18") == 0) {
            _preference1 = 1;
          
        } else if (_type.compareTo("19") == 0) {
         _preference1 =  2;
        }else if (_type.compareTo("33") == 0){
           _preference1 =3;
        }

        int _type1 = Integer.parseInt(_type);
     


        String result = "success";
        String message = "Connection sucessful!!!";

        int retValue = -1;

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        try {
            PushWatchManagement pMngmt = new PushWatchManagement();
            retValue = pMngmt.testSettingCustomisePush(sessionId, channel.getChannelid(), _type1, _preference1, _groupname, _testmsg);
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        }

        if(retValue == 0){
            result = "success";
            message = "Send Push Started!!!";
        }else if(retValue == -3){//Prefix Filter
            result = "error";
            message = "Message blocked by Prefix Filter!!!";
        }else if(retValue == -5){//Domain Filter
            result = "error";
            message = "Message blocked by Content Filter!!!";
        }else if(retValue == -4){//Domain Filter
            result = "error";
            message = "Message blocked by Domain Filter!!!";
        }else if(retValue == -66){//Domain Filter
            result = "error";
            message = "User Not Found!!!";
        }else{
            if(_type.equals("8"))
            {
             message = "Failed to connect to usersource. Check Settings!!!";   
            }
           else
            {
            result = "error";
            message = "Failed to send test message. Check Settings!!!";
            }
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
