package com.mollatech.axiom.v2.face.handler.units;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet;
import com.mollatech.axiom.nucleus.db.Units;

/**
 *
 * @author nilesh
 */
public class shiftUnitsOperators extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(shiftUnitsOperators.class.getName());

    final String itemType = "UNITS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        
        log.info("is started :: ");

        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Unit's operators moved successfully!!!";

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);

        JSONObject json = new JSONObject();
        int retValue = 0;
//        String _unit_name = request.getParameter("_unitNameS");
        String _unitId = request.getParameter("_unitIdS");
        log.debug("_unitId :: "+_unitId);
        String _oldunitNameE = request.getParameter("_oldunitNameS");
        log.debug("_oldunitNameE :: "+_oldunitNameE);
        String _unitS = request.getParameter("_unitS");
        
        int ioldunitId = Integer.parseInt(_unitId);
        log.debug("ioldunitId :: "+ioldunitId);
        int inewunitId = Integer.parseInt(_unitS);
        log.debug("inewunitId :: "+inewunitId);

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());

        if (_unitS == null) {
            result = "error";
            message = "Invalid Parameters!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        UnitsManagemet unMngt = new UnitsManagemet();
//        Units checkduplicate = unMngt.getUnitByUnitName(sessionId, channel.getChannelid(), _unit_name);
//        
//        if (checkduplicate != null) {      // 0 is present so it is duplication. report error
//            result = "error";
//            message = "Duplicate Name!!";
//            try {json.put("_result", result);
//            json.put("_message", message);
//            }catch(Exception e){log.error("Exception caught :: ",e);}
//            out.print(json);
//            out.flush();
//            return;
//        }
        Units uN = unMngt.getUnitByUnitId(sessionId, channel.getChannelid(), inewunitId);
        String _unit_name = uN.getUnitname();
        OperatorsManagement oMngt = new OperatorsManagement();
        Operators[] opr = oMngt.arrGetOperatorByUnitId(sessionId, channel.getChannelid(), ioldunitId);
        int res = -1;

        int iResultSuccess = 0;
        int iResultFailure = 0;

        if (opr != null) {
            for (int i = 0; i < opr.length; i++) {

                res = oMngt.ChangeUnits(sessionId, channel.getChannelid(), opr[i].getOperatorid(), inewunitId);
                log.debug("ChangeUnits :: "+res);
                if (res == 0) {
                    iResultSuccess++;
                    //message = "Successfully moved "+opr.length+" operators from "+ _oldunitNameE +" Unit to new unit "+ _unit_name+"!!!";
                } else {
                    iResultFailure++;
                    //message = "Failed to moved "+opr.length+" operators from "+ _oldunitNameE +"to new unit "+ _unit_name+"!!!";
                }

            }

        } else {
            result = "error";
            message = "No Operators present!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        
//        System.out.println("opr.length="  +opr.length);
//        System.out.println(iResultSuccess);
//        System.out.println(iResultFailure);

        if (iResultFailure != 0) {
            //retValue = -1 * iResultFailure;
            if (iResultSuccess != 0) {
                message = "Successfully moved " + iResultSuccess + " operators and Failed to move " + iResultFailure + " from unit " + _oldunitNameE + "  to new unit " + _unit_name + "!!!";
            } else {
                message = "Failed to moved " + opr.length + " operators from unit " + _oldunitNameE + " to new unit " + _unit_name + "!!!";
            }
        } else {
            retValue = 0;
            message = "Successfully moved " + iResultSuccess + " users from unit " + _oldunitNameE + " Group to new unit " + _unit_name + "!!!";
        }

        String resultString = "ERROR";

        if (retValue == 0) {
            AuditManagement audit = new AuditManagement();
            resultString = "Success";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                    "Shift Operators", resultString, retValue, "Unit Management",
                    "",
                    message,
                    itemType, operatorS.getOperatorid());

        }

        if (retValue != 0) {
            AuditManagement audit = new AuditManagement();
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                    "Shift Operators", resultString, retValue, "Unit Management",
                    "",
                    message,
                    itemType,
                    operatorS.getOperatorid());
            result = "error";
//            message = "Units update failed!!!";
        }

        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
