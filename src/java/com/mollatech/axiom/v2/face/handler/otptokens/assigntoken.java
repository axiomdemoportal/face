package com.mollatech.axiom.v2.face.handler.otptokens;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Otptokens;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class assigntoken extends HttpServlet {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(assigntoken.class.getName());

    public static final int HW_MINI_TOKEN = 1;
    public static final int HW_CR_TOKEN = 2;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final int SOFTWARE_TOKEN = 1;
    final int SW_WEB_TOKEN = 1;
    final int SW_MOBILE_TOKEN = 2;
    final int HARDWARE_TOKEN = 2;
    final int OOB_TOKEN = 3;

    final int OOB__SMS_TOKEN = 1;
    final int OOB__VOICE_TOKEN = 2;
    final int OOB__USSD_TOKEN = 3;
    final int OOB__EMAIL_TOKEN = 4;
    final int OOB__PUSH_TOKEN = 5;

    final int SW_PC_TOKEN = 3;

    final String itemType = "OTPTOKENS";
    final String itemTypeAUTH = "AUTHORIZATION";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: " + channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: " + sessionId);
        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: " + remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: " + operatorS.getOperatorid());

        String _serialnumber = request.getParameter("_serialnumber");
        log.debug("_serialnumber :: " + _serialnumber);
        String _userid = request.getParameter("_userid");
        log.debug("_userid :: " + _userid);
        String _subcategory = request.getParameter("_subcategory");
        log.debug("_subcategory :: " + _subcategory);
        int subCategory = 0;
        if (_subcategory != null) {
            subCategory = Integer.parseInt(_subcategory);
        }

        String _category = request.getParameter("_category");
        log.debug("_category :: " + _category);
        int category = 0;
        if (_category != null) {
            category = Integer.parseInt(_category);
        }

        String result = "success";
        String message = "Token Assigned Successfully....";

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        if (_userid == null) {
            result = "error";
            message = "Could Not Assign Token!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ", e);
            }
            out.print(json);
            out.flush();
            return;
        }

        String _value = "Assigned";
        String _status = "Assigned";

//nilesh
        if (category == OOB_TOKEN) {
            if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_OOB) != 0) {
                result = "error";
                message = "OOB Token is not available in this license!!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    log.error("exception caught :: ", e);
                }
                out.print(json);
                out.flush();
                return;
            }
        } else if (category == SOFTWARE_TOKEN) {
            if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
                result = "error";
                message = "Software Token is not available in this license!!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    log.error("exception caught :: ", e);
                }
                out.print(json);
                out.flush();
                return;
            }
        } else if (category == HARDWARE_TOKEN) {
            if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_HARDWARE) != 0) {
                result = "error";
                message = "Hardware Token is not available in this license!!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    log.error("exception caught :: ", e);
                }
                out.print(json);
                out.flush();
                return;
            }
        }

        int retValue = -1;
        OTPTokenManagement oManagement = new OTPTokenManagement(channel.getChannelid());
        SettingsManagement sMngmt = new SettingsManagement();
        Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
        ChannelProfile channelprofileObj = null;
        if (settingsObj == null) {
            channelprofileObj = new ChannelProfile();

        } else {
            channelprofileObj = (ChannelProfile) settingsObj;
        }

        int tokenCountCurrentDBState = oManagement.getTokenCountForLicenseCheck(sessionId, channel.getChannelid(), category);
//        if (tokenCountCurrentDBState < 1) {
//            result = "error";
//            message = "Assign Token is not available in this license for the token !!!";
//            try {
//                json.put("_result", result);
//                json.put("_message", message);
//            } catch (Exception e) {
//                log.error("Exception caught :: ",e);
//            }
//            out.print(json);
//            out.flush();
//            return;
//        }

        int licensecount = 0; // Axiom Protect fuction call here 

        if (category == OOB_TOKEN) {
            licensecount = AxiomProtect.GetOOBTokensAllowed();

            if (licensecount == -998) {
                //unlimited licensing 
            } else {
                if (tokenCountCurrentDBState >= licensecount) {
                    result = "error";
                    message = "OOB Token already reached its limit as per this license!!!";
                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("exception caught :: ", e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
            }
        } else if (category == SOFTWARE_TOKEN) {
            licensecount = AxiomProtect.GetSoftwareTokensAllowed();
            if (licensecount == -998) {
                //unlimited licensing 
            } else {
                if (tokenCountCurrentDBState >= licensecount) {
                    result = "error";
                    message = "SOFTWARE Token already reached its limit as per this license!!!";
                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("exception caught :: ", e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
            }

        } else if (category == HARDWARE_TOKEN) {
            licensecount = AxiomProtect.GetHardwareTokensAllowed();
            if (licensecount == -998) {
                //unlimited licensing 
            } else {
                if (tokenCountCurrentDBState >= licensecount) {
                    result = "error";
                    message = "Hardware Token already reached its limit as per this license!!!";
                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("exception caught :: ", e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
            }
        }

        //added by vikram - 21jan 2016 for making the check that operator to specific unit is only assigning the token (from that unit only(.
        //get unitid of operator
        if (category == HARDWARE_TOKEN && channelprofileObj._HardWareTokenUnitAutherization == true) {
            int unitidOfOperator = operatorS.getUnits();
            Otptokens otUnderObservation = oManagement.getOtpObjBySerialNo(sessionId, _serialnumber);
            if (otUnderObservation == null) {
                result = "error";
                message = "Hardware Token's Serial Number is not present!!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    log.error("exception caught :: ", e);
                }
                out.print(json);
                out.flush();
                return;
            }

            int unitOFToken = otUnderObservation.getUnitid();
            if (unitidOfOperator != unitOFToken) {
                result = "error";
                message = "Mismatch between operator's unit and hardware token's unit!!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    log.error("exception caught :: ", e);
                }
                out.print(json);
                out.flush();
                return;
            }
        }

        //get unitid of token
        //end of addition
        AuditManagement audit = new AuditManagement();
        UserManagement uMngt = new UserManagement();
        AuthUser userObj = uMngt.getUser(sessionId, channel.getChannelid(), _userid);
        retValue = oManagement.AssignToken(sessionId, channel.getChannelid(), _userid, category, subCategory, _serialnumber);
        log.debug("AssignToken :: " + retValue);
        String resultString = "ERROR";

        String strCategory = "";
        String strSubCategory = "";
        if (category == SOFTWARE_TOKEN) {
            strCategory = "SOFTWARE_TOKEN";
            if (subCategory == SW_WEB_TOKEN) {
                strSubCategory = "SW_WEB_TOKEN";
            } else if (subCategory == SW_MOBILE_TOKEN) {
                strSubCategory = "SW_MOBILE_TOKEN";
            } else if (subCategory == SW_PC_TOKEN) {
                strSubCategory = "SW_PC_TOKEN";
            }

        } else if (category == HARDWARE_TOKEN) {
            strCategory = "HARDWARE_TOKEN";
            if (subCategory == HW_MINI_TOKEN) {
                strSubCategory = "HW_MINI_TOKEN";
            } else if (subCategory == HW_CR_TOKEN) {
                strSubCategory = "HW_CR_TOKEN";
            }
        } else if (category == OOB_TOKEN) {
            strCategory = "OOB_TOKEN";
            if (subCategory == OOB__SMS_TOKEN) {
                strSubCategory = "OOB__SMS_TOKEN";
            } else if (subCategory == OOB__USSD_TOKEN) {
                strSubCategory = "OOB__USSD_TOKEN";
            } else if (subCategory == OOB__VOICE_TOKEN) {
                strSubCategory = "OOB__VOICE_TOKEN";
            } else if (subCategory == OOB__EMAIL_TOKEN) {
                strSubCategory = "OOB__EMAIL_TOKEN";
            } else if(subCategory == OOB__PUSH_TOKEN){
                strSubCategory = "OOB__PUSH_TOKEN";
                _value = "PUSH";
            }
        }

        if (retValue == 0) {
            resultString = "SUCCESS";
            String entity = "User phone no:" + userObj.getPhoneNo() + "User Name:" + userObj.getUserName();
            audit.AddAuditTrailWithEntity(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(),
                    "Assign Token",
                    resultString, retValue,
                    "Token Management", "", "Category = " + strCategory + " Sub Category =" + strSubCategory + " Serial No =" + _serialnumber,
                    itemType,
                    entity,
                    _userid);

        } else {
            resultString = "ERROR";
            String entity = "User phone no:" + userObj.getPhoneNo() + "User Name:" + userObj.getUserName();
            audit.AddAuditTrailWithEntity(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(),
                    "Assign Token", resultString, retValue,
                    "Token Management", "",
                    "FAILED: Category = " + strCategory + " Sub Category =" + strSubCategory + " Serial No =" + _serialnumber,
                    itemType, entity, _userid);

            if (retValue == -4) {
                message = "Token is already assigned. Two assignments of same type is not allowed!!!!";
                // message = "Could Not Find Serial Number or Invalid Serial Number!!!!";
            }
            if (retValue == -2) {
                result = "error";
                message = "Could Not Find Serial Number or Invalid Serial!!!!";

            }

            result = "error";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ", e);
            }
            out.print(json);
            out.flush();
            return;
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);
            json.put("_status", _status);
        } catch (Exception e) {
            log.error("exception caught :: ", e);
        } finally {
            out.print(json);
            out.flush();
            log.info("is ended :: ");
            return;
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
