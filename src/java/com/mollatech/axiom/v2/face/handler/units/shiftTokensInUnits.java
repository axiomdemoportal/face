/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.axiom.v2.face.handler.units;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Units;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author nilesh
 */
public class shiftTokensInUnits extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(shiftTokensInUnits.class.getName());
final String itemType = "OTPTOKENS";
    public String getContents(File aFile) {
        //...checks on aFile are elided
        StringBuilder contents = new StringBuilder();

        try {

            BufferedReader input = new BufferedReader(new FileReader(aFile));
            try {
                String line = null; //not declared within while loop

                while ((line = input.readLine()) != null) {
                    contents.append(line);
                    contents.append(System.getProperty("line.separator"));
                }
            } finally {
                input.close();
            }
        } catch (IOException ex) {
            log.error("Exception caught :: ",ex);
        }

        return contents.toString();
    }
    static List<String> inputlist;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
      try {
            response.setContentType("application/json");
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId :: "+sessionId);
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("remoteaccesslogin :: "+remoteaccesslogin);
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operatorS :: "+operatorS.getOperatorid());

            String filepath = (String) request.getSession().getAttribute("_shiftTokenFliePath");
            log.debug("filepath :: "+filepath);
            String _unitId = request.getParameter("_unitIdT");
            log.debug("_unitId :: "+_unitId);
            String _unit_name = request.getParameter("_oldunitNameT");
            log.debug("_unit_name :: "+_unit_name);
            String new_unitID = request.getParameter("_unitIdNameT");
            log.debug("new_unitID :: "+new_unitID);
            String result = "success";
            String message = "Selected tokens assigned successfully!!";
            PrintWriter out = response.getWriter();
            JSONObject json = new JSONObject();
         
            if (filepath == null) {
                result = "error";
                message = "File Not Found!!";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }
            if (_unitId == null ) {
                result = "error";
                message = "Unit Id Not Found!!";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }
             if (new_unitID == null) {
                result = "error";
                message = "New Unit Id Not Found!!";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }
            int iunitId = Integer.parseInt(_unitId);
             int inewunitId = Integer.parseInt(new_unitID);
            File testFile = new File(filepath);
            String toContact = this.getContents(testFile);
            request.getSession().setAttribute("_shiftTokenFliePath", null);
            UnitsManagemet unMngt = new UnitsManagemet();
             Units uN = unMngt.getUnitByUnitId(sessionId, channel.getChannelid(), inewunitId);
            String new_unit_name = uN.getUnitname();
            OTPTokenManagement otpMngt = new OTPTokenManagement(channel.getChannelid());
            AuditManagement audit = new AuditManagement();
            inputlist = new ArrayList<String>();
            for (String s : toContact.split("\n")) {
                inputlist.add(new String(s));
            }
            String tokenSerilalNo = null;
            int[] iResponse = new int[inputlist.size()];
            for (int i = 0; i < inputlist.size(); i++) {
                
                tokenSerilalNo = inputlist.get(i).trim();
                
                int retVal = otpMngt.shiftHwTokenUnit(sessionId, channel.getChannelid(), OTPTokenManagement.HARDWARE_TOKEN,
                        OTPTokenManagement.TOKEN_STATUS_FREE, tokenSerilalNo, iunitId,inewunitId);
                log.debug("shiftHwTokenUnit :: "+retVal);
                iResponse[i] = retVal;
                
                String resultString = "ERROR";
                
                if (retVal == 0) {
                    resultString = "SUCCESS";
                    
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                         request.getRemoteAddr(),
                         channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                         "Shift Tokens between Units", 
                         resultString, 
                         retVal, 
                         "Token Management",
                         "",
                        "Shift Serial No=" + tokenSerilalNo+ " from Unit (" + _unit_name + " ) to New Unit ("+new_unit_name + ")",
                         itemType, 
                         tokenSerilalNo);

               }else{
                    
                    
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                         request.getRemoteAddr(),
                         channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                         "Shift Tokens between Units", 
                         "ERROR", 
                         retVal, 
                         "Token Management",
                         "",
                         "Failed: Serial No=" + tokenSerilalNo+ " from Unit (" + _unit_name + " ) to New Unit ("+new_unit_name + ")",
                         itemType, 
                         tokenSerilalNo);
                    
                }
                
            }
            int successCount = 0;
            int failedCount = 0;
            boolean bAnyFailure=false;
            for(int i = 0 ; i< iResponse.length;i++){
                if(iResponse[i] == 0){
                    successCount = successCount + 1;
                }else{
                    failedCount = failedCount +1;
                    bAnyFailure=true;
                }
            }
             
             result = "success";
            
             message =  successCount + " tokens shift successfully and " + failedCount + "token failed to shift!!";
             json.put("_result", result);
             json.put("_message", message);
             out.print(json);
             out.flush();
             
              if ( bAnyFailure == true ) {  
                AuditManagement audit1 = new AuditManagement();
                audit1.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                           request.getRemoteAddr(),
                           channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                           "Shift Tokens between Units", 
                           "ERROR", 
                            -1, 
                           "Units Management",
                           "",
                           "Failed: To shift " +  failedCount + "tokens from Unit (" + _unit_name + " ) to New Unit ("+new_unit_name + ")",
                           "UNITS", 
                           channel.getChannelid() );
              } 
              
              AuditManagement audit2 = new AuditManagement();
                audit2.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                           request.getRemoteAddr(),
                           channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                           "Shift Tokens between Units", 
                           "SUCCESS", 
                            0, 
                           "Units Management",
                           "",
                           "To shift " +  successCount + "tokens from Unit (" + _unit_name + " ) to New Unit ("+new_unit_name + ")",
                           "UNITS", 
                           channel.getChannelid() );
             
             
             return;

        } catch (Exception ex) {
            log.error("exception caught :: ",ex);
        }
      log.info("is ended :: ");

    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
