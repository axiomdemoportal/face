/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.channel;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.ChannelSettings;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class loadchannelsettings_2 extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadchannelsettings_2.class.getName());

    private JSONObject SettingsWhenEmpty(int _type1, int _preference1, Object settingsObj) {
        JSONObject json = new JSONObject();
        if (_type1 == com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.CHANNEL_SETTINGS && _preference1 == com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE) {

            try {
                json.put("_datasource", "");
                json.put("_ipSource", "");
                json.put("_portSource", "");
                json.put("_sslSource", "");
                json.put("_databaseNameSource", "");
                json.put("_tableNameSource", "");
                json.put("_userIdSource", "");
                json.put("_passwordSource", "");

            } catch (Exception e) {
                
                log.error("exception caught ::",e);
            }
        }
        return json;
    }

    private JSONObject SettingsWhenPresent(Object settingsObj) {
        JSONObject json = new JSONObject();
        if (settingsObj instanceof ChannelSettings) {
            ChannelSettings chObj = (ChannelSettings) settingsObj;
            try {
                json.put("_datasource", chObj.isSourceType());
                json.put("_ipSource", chObj.getExternalhost());
                json.put("_portSource", chObj.getPort() );
                json.put("_sslSource", chObj.isSslEnabled());
                json.put("_databaseNameSource", chObj.getDatabaseName() );
                json.put("_tableNameSource", chObj.getTableName());
                json.put("_userIdSource", chObj.getUserId());
                json.put("_passwordSource", chObj.getPassword() );
            } catch (Exception e) {
                
                log.error("exception caught ::",e);
           }
        }
        return json;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
         log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
         log.debug("channel ::"+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
         log.debug("sessionId ::"+sessionId);

//        String _preference = request.getParameter("_preference");
        int _preference1 = 1;
        int _type1 = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.CHANNEL_SETTINGS;

        JSONObject json = null;//new JSONObject();
        PrintWriter out = response.getWriter();

        try {
            SettingsManagement sMngmt = new SettingsManagement();
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _type1, _preference1);
            if (settingsObj != null) {
                json = SettingsWhenPresent(settingsObj);
            } else {
                json = SettingsWhenEmpty(_type1, _preference1, settingsObj);
            }

        } catch (Exception ex) {
            log.error("exception caught :: ",ex);
           
            // TODO handle custom exceptions here
        }
        try {
        } catch (Exception ex) {
            log.error("exception caught :: ",ex);
         
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
