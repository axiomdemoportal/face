/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Epintracker;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.ecopin.management.EPINManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class changeepinstatus extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(changeepinstatus.class.getName());
    
    
    final int APPROVED_STATUS = 1;
    final int REJECTED_STATUS = -2;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        response.setContentType("application/json");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");

        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        String _e_status = request.getParameter("_e_status");
        String _epinId = request.getParameter("_epinid");
        int epinid =0 ;
        int status= 0;
        if(_e_status != null){
            status = Integer.parseInt(_e_status);
        }
        if(_epinId != null){
            epinid = Integer.parseInt(_epinId);
        }
        String _userid = request.getParameter("_userid");
        String _reason = request.getParameter("_reason");
        
        
        if ( AxiomProtect.CheckEnforcementFor(AxiomProtect.EPIN) != 0 ) {
            String result = "error";
            String message = "This feature is not available in this license!!!";
            JSONObject json = new JSONObject();
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){log.error("Exception caught :: ",e);}
            PrintWriter out = response.getWriter();
            out.print(json);
            out.flush();
            return;
        }
        //audit
        
        
        Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        
        log.debug("channel is::" + channel.getName());
        log.debug("Operator Id is::" + operator.getOperatorid());
        log.debug("SessionId::" + sessionId);
        log.debug("RemoteAccessLogin::" + remoteaccesslogin);

        EPINManagement epinObj = new EPINManagement();
        Epintracker eTracker = epinObj.GetEpintrackerById(sessionId, channel.getChannelid(), epinid);
        int retValue = -1;
        
         if(eTracker.getOperatorsstatus() == EPINManagement.SINGLE_OPERATOR_CONTROLLED || eTracker.getOperatorsstatus() == EPINManagement.NO_OPERATOR_CONTROLLED){
         retValue = epinObj.OperatorHandler(sessionId, channel.getChannelid(), operator.getOperatorid(), _userid, eTracker, status, _reason);
       
        log.debug("OperatorHandler::" + retValue);
            
         }else{
            retValue = epinObj.DualOperatorHandler(sessionId, channel.getChannelid(), operator.getOperatorid(), _userid, eTracker, status, _reason);
        }
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
     
         String _value = "Approved";
            String message = "ECOPIN APPROVED Successful!!!";
       if(retValue == 0){
        
        if (status == REJECTED_STATUS) {
              message = "ECOPIN REJECTED Successful!!!";
            _value = "Rejected";
        }
       }else{
           result = "error";
           if(status == APPROVED_STATUS){
               message = "APPROVAL Of ECOPIN is Failed";
           }else if(status == REJECTED_STATUS){
               message = "REJECTION Of ECOPIN is Failed";
           }
           _value = "PENDING";
            try { json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);
            }catch(Exception e){log.error("Exception caught :: ",e);}
            out.print(json);
            out.flush();
            return;
       }
        

        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
