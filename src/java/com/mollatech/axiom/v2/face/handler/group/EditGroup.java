/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.group;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Usergroups;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.GroupManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author ashish
 */
public class EditGroup extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(EditGroup.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private int Status = -1;
    private int enabled = -1;
    private int restrictionLevel = -1;
    AuditManagement audit = new AuditManagement();
    final String itemType = "GROUPS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        
        log.info("is started");
        
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionid :: "+sessionId);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        String operatorId = operatorS.getOperatorid();
        log.debug("operatorId :: "+operatorId);

        String _unitName = request.getParameter("_unitNameE");
        log.debug("_unitName :: "+_unitName);
        String _unitStatus = request.getParameter("_unitStatusE");
        log.debug("_unitStatus :: "+_unitStatus);
        String _selectedType = request.getParameter("_selectedTypeE");
        log.debug("_selectedType :: "+_selectedType);
        String _selectedCheck = request.getParameter("_selectedCheckE");
        log.debug("_selectedCheck :: "+_selectedCheck);
        String _CountryName = request.getParameter("_CountryNameE");
        log.debug("_CountryName :: "+_CountryName);
        String _StateName = request.getParameter("_StateName");
        log.debug("_StateName :: "+_StateName);
        String _cityName = request.getParameter("_cityNameE");
        log.debug("_cityName :: "+_cityName);
        String _PinCode = request.getParameter("_PinCodeE");
        log.debug("_PinCode :: "+_PinCode);
        
        if (_unitStatus.equals("1")) {
            Status = 1;
        }
//        if (_selectedCheck.equals("Allow")) {
//            enabled = 1;
//        }
        enabled = 1;
        String _result = "success";
        String _message = "Group Updated Successfully ...!";
        if (_unitName.equals("")) {
            try {
                json.put("_result", "error");
                json.put("_message", "Group Name is mandetory ..!");
                out.print(json);
                out.flush();
                return;
            } catch (Exception e) {
                
            log.error("exception caught :: ",e);
            }
        }
        GroupManagement management = new GroupManagement();
        Usergroups usergroups1 = management.getGroupByGroupName(sessionId, channel.getChannelid(), _unitName);
        if (usergroups1 != null) {
            usergroups1.setGroupname(_unitName);
            usergroups1.setIsEnabled(enabled);
            usergroups1.setLastupOn(new Date());
            usergroups1.setStatus(Status);
            if (_selectedType.equals("No Restriction")) {

                usergroups1.setListOfLocationOrPincodes("");
                usergroups1.setRestrictionLevel(1);
                usergroups1.setState("");
                usergroups1.setCountry("");
            } else if (_selectedType.equals("Country")) {
                usergroups1.setListOfLocationOrPincodes("");
                usergroups1.setRestrictionLevel(2);
                usergroups1.setCountry(_CountryName);
                usergroups1.setState("");
            } else if (_selectedType.equals("State")) {
                usergroups1.setListOfLocationOrPincodes("");
                usergroups1.setRestrictionLevel(3);
                usergroups1.setCountry(_CountryName);
                usergroups1.setState(_StateName);
            } else if (_selectedType.equals("City")) {
                usergroups1.setListOfLocationOrPincodes(_cityName);
                usergroups1.setRestrictionLevel(4);
                usergroups1.setCountry(_CountryName);
                usergroups1.setState(_StateName);
            } else if (_selectedType.equals("Pincode")) {
                usergroups1.setListOfLocationOrPincodes(_PinCode);
                usergroups1.setRestrictionLevel(5);
                usergroups1.setCountry(_CountryName);
                usergroups1.setState(_StateName);
            }
        }
        int result = -1;
        String resultString = "ERROR";

        result = management.editGroup(sessionId, usergroups1);
        log.debug("editGroup :: "+result);
        if (result == 0) {
            resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), 
                    "Edit Group", resultString, result,
                    "Group Management", "", "Group Name=" + _unitName + "Group Status" + Status + "created on =" + new Date()
                    + "Updated On = " + new Date(), itemType, 
                    channel.getChannelid());
        } else {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), 
                    "Edit Group", resultString, result,
                    "Group Management", "", 
                    "Failed To Edit Group",
                    itemType, operatorId);
        }
        if (result == 0) {
            _result = "success";
        } else {
            _result = "error";
            _message = "Group Addition Failed";

        }
        try {
            json.put("_result", _result);
            json.put("_message", _message);
        } catch (Exception e) {
            log.error("exception caught :: ",e);
            
        } finally {
            out.print(json);
            out.flush();

        }
        log.info("is ended");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
