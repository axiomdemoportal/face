/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.operator;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.ChannelSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchResult;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;

/**
 *
 * @author Pramod
 */
public class checkLdapUserAvailability extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(checkLdapUserAvailability.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        log.info("is started :: ");
        String _channelName = this.getServletContext().getContextPath();
        _channelName = _channelName.replaceAll("/", "");
        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
        Channels channel = cUtil.getChannel(_channelName);
        SettingsManagement sMngmt = new SettingsManagement();
        ChannelSettings chSettings = new ChannelSettings();
        Object settingsObj = null;
        boolean type = true;

                settingsObj = sMngmt.getSetting(channel.getChannelid(), SettingsManagement.CHANNEL_SETTINGS, SettingsManagement.PREFERENCE_ONE);
                if (settingsObj != null) {
                    chSettings = (ChannelSettings) settingsObj;
                    type = chSettings.isSourceType();
                }
                
                    String operatorName=request.getParameter("");
                    String host = chSettings.getExternalhost();
                    String port = String.valueOf(chSettings.getPort());
                    String dn = chSettings.getDatabaseName();
                    String table = chSettings.getTableName();
                    String ldapPassword=chSettings.getPassword();
                    String ldapBasedn=chSettings.getUserId();
                    Hashtable env = new Hashtable();
                    String provider_url = "ldap://".concat(host).concat(":").concat((port));
                    env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
                    env.put(Context.SECURITY_AUTHENTICATION, "simple");
                    env.put(Context.SECURITY_PRINCIPAL, ldapBasedn);
                    env.put(Context.SECURITY_CREDENTIALS, ldapPassword);
                    env.put(Context.PROVIDER_URL, provider_url);
                    env.put("java.naming.ldap.factory.socket", "com.mollatech.axiom.common.utils.MySSLSocketFactory");
                  try {
                 DirContext ldapDir = new InitialDirContext(env);
                 Attributes matchAttrs = new BasicAttributes(true);
                 
  matchAttrs.put(new BasicAttribute("commonName", "Abhishek"));

NamingEnumeration answer = ldapDir.search("uid=admin,ou=system",matchAttrs);
        while(answer.hasMore())
        {
            SearchResult  o=(SearchResult)answer.next();
            o.getAttributes().get("sn");
            System.out.println("Sirname"+ o.getAttributes().get("sn"));
        }
            
        } catch (Exception ex) {
           log.error("exception caught :: ",ex);
        }
                   log.info("is ended :: ");
        
        }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
