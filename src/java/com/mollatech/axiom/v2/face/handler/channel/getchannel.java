package com.mollatech.axiom.v2.face.handler.channel;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement;
import com.mollatech.axiom.nucleus.db.connector.management.RemoteAccessManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.json.JSONObject;

public class getchannel extends HttpServlet {
    
        static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(getchannel.class.getName());


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("is started :: ");
        
        String _chid = request.getParameter("_chid");
        log.debug("_chid ::"+_chid);

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel ::"+channel.getName());
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        try {

            SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
            Session sChannel = suChannel.openSession();
            ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
            Channels channelObj = cUtil.getChannelbyID(_chid);
            suChannel.close();
            sChannel.close();
            
            suChannel = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
            sChannel = suChannel.openSession();
            RemoteAccessUtils  raUtil = new RemoteAccessUtils(suChannel, sChannel);
            String [] lp = raUtil.GetRemoteAccessCredentials(_chid);
            suChannel.close();
            sChannel.close();



            json.put("_id", channelObj.getChannelid());
            json.put("_name",  channelObj.getName());
            json.put("_status", channelObj.getStatus());
            json.put("_vpath", channelObj.getVpath());
            json.put("_raloginid", lp[0]);
            json.put("_rapassword", lp[1]);
            json.put("_rem_expirymin", lp[2]);
            json.put("_result", "success");
            
        } catch (Exception ex) {
            
            log.error("exception caught ::",ex);
            // TODO handle custom exceptions here
            try { 
            json.put("_result", "error");
            json.put("_message", ex.getMessage());
            }catch(Exception e){
                
            log.error("exception caught::",e);
            
        }
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
