/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.group;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Usergroups;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.GroupManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author ashish
 */
public class RemoveGroup extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(RemoveGroup.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    AuditManagement audit = new AuditManagement();
    final String itemType = "GROUPS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        
        log.info("is started");

        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Group Removed Successfully!!!";

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionid ::"+sessionId);
        String groupname = request.getParameter("groupname");
        log.debug("groupname :: "+groupname);

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
       
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String operatorId = operatorS.getOperatorid();
        log.debug("operatorid :: "+operatorId);
        
        
        
        
        
        

        JSONObject json = new JSONObject();
        int retValue = -1;

        GroupManagement management = new GroupManagement();
        Usergroups usergroups = management.getGroupByGroupName(sessionId, channel.getChannelid(), groupname);
        if (usergroups != null) {
            UserManagement uMngt = new UserManagement();
            AuthUser[] arrUser = uMngt.getAllUserByGroup(sessionId, channel.getChannelid(), usergroups.getGroupid());
            if (arrUser != null) {
                result = "error";
                message = "ERROR: Users are present in this group, it cannot be removal!!!";
//                try {
//                    json.put("_result", result);
//                    json.put("_message", message);
//                } catch (Exception e) {
//                    log.error("Exception caught :: ",e);
//                } finally {
//                    out.print(json);
//                    out.flush();
                }
            else{
                retValue=management.deleteGroup(sessionId, channel.getChannelid(), usergroups.getGroupid());
                log.debug("deleteGroup :: "+retValue);
            
            }
            
            
        }else{ result = "error";
               message = "ERROR: UsersGroup does not exist!!!";}
        String resultString = "ERROR";
        if (retValue == 0) {
            resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Delete Unit", resultString, retValue,
                    "Group Management", "Group Name =" + usergroups.getGroupname() + "Status =" + usergroups.getStatus()
                    + "Created On =" + usergroups.getCreatedOn() + "Updated On=" + usergroups.getLastupOn(),
                    "Group Removed Successfully", itemType,  channel.getChannelid());

        } else if (retValue != 0) {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), 
                    "Delete Group", resultString, retValue,
                    "Group Management", "",
                    "Failed To Remove Group", itemType,  channel.getChannelid());
        }
//        if (retValue == 0) {
//            result = "success";
//        } else {
//            result = "error";
//            message = "Group Removal Failed!!!";
//        }

        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();

        }
        log.info("is ended");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
