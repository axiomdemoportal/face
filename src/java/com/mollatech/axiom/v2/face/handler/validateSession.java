/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Sessions;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author _nilesh
 */
public class validateSession extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(validateSession.class.getName());

    final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;

    private static String g_channelID = null;

    private String GetChannelID() {

        if (g_channelID == null) {

            SessionFactoryUtil suChannel = null;// new SessionFactoryUtil(SessionFactoryUtil.channels);
            Session sChannel = null; //suChannel.openSession();

            try {
                suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
                sChannel = suChannel.openSession();

                String _channelName = this.getServletContext().getContextPath();
                _channelName = _channelName.replaceAll("/", "");

                //MessageContext mc = wsContext.getMessageContext();
                //ServletContext sc = (ServletContext) mc.get(MessageContext.SERVLET_CONTEXT);
                //String _channelName = sc.getContextPath();
                //_channelName = _channelName.replaceAll("/", "");

                if (_channelName.compareTo("core") == 0) {
                    _channelName = "face";
                }

                ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
                Channels channel = cUtil.getChannel(_channelName);
                g_channelID = channel.getChannelid();
                sChannel.close();
                suChannel.close();
            } catch (Exception e) {
                sChannel.close();
                suChannel.close();
                log.error("Exception caught :: ",e);
            }
        }

        return g_channelID;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        OperatorsManagement oManagement = new OperatorsManagement();
        try {
            
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
            
            String _operatorID = request.getParameter("_oprid");
            
        log.debug("validateSession::channel is::"+channel.getName());
        log.debug("validateSession::RemoteAccessLogin::"+remoteaccesslogin);
        log.debug("validateSession::Operator id is::"+operatorS.getOperatorid());

            if (channel == null && remoteaccesslogin == null && operatorS == null) {    
                if ( _operatorID == null || _operatorID.isEmpty() == true) { 
                    String result = "error";
                    String message = "Session Expired, Please Login Again ...!!!";
                    json.put("_result", result);
                    json.put("_message", message);
                    out.print(json);
                    out.flush();
                    return;
                }
                else { 
                    oManagement.ChangeLoginStatus(GetChannelID(),_operatorID, OperatorsManagement.LOGIN_INACTIVE);
                     String result = "error";
                    String message = "Session Expired, Please Login Again ...!!!";
                    json.put("_result", result);
                    json.put("_message", message);
                    out.print(json);
                    out.flush();
                    return;
                }
            }

            String sessionId = (String) request.getSession().getAttribute("_apSessionID");

            SessionManagement sessObj = new SessionManagement();
            Sessions status = sessObj.getSessionById(sessionId);
            
            if (status == null) {
                
                if ( operatorS.getOperatorid() != null  )
                    oManagement.ChangeLoginStatus(GetChannelID(), operatorS.getOperatorid(), OperatorsManagement.LOGIN_INACTIVE);
                else if ( _operatorID != null  )                    
                    oManagement.ChangeLoginStatus(GetChannelID(), _operatorID, OperatorsManagement.LOGIN_INACTIVE);
                String result = "error";
                String message = "Session Expired,You will be logged out now!!!";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }
            
            
            int sessionStatus = status.getStatus(); //sessObj.GetSessionStatus(sessionId);

            if (sessionStatus != ACTIVE_STATUS) {
                oManagement.ChangeLoginStatus(GetChannelID(),operatorS.getOperatorid(), OperatorsManagement.LOGIN_INACTIVE);
                String result = "error";
                String message = "Session Expired,You will be logged out now!!!";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }

//           status =null;
            
//            
//            System.out.println(sessionId);

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
