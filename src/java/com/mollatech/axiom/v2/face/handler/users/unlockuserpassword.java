/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.users;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class unlockuserpassword extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(unlockuserpassword.class.getName());

    final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;
    final int LOCKED_STATUS = -1;
    final int REMOVE_STATUS = -99;
    final String itemtype = "USERPASSWORD";
    final String itemTypeAUTH = "AUTHORIZATION";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());

        String _userid = request.getParameter("_userid");
        log.debug("_userid :: "+_userid);
        String _status = request.getParameter("_status");
        
        int status = Integer.parseInt(_status);
        log.debug("status :: "+status);
        String result = "success";
        String message = "Password Unlock successfully....";

        String _value = "Active";
        if (status == -1) {
            _value = "Suspended";
        }

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        if (_userid == null || _status == null) {
            result = "error";
            message = "Could not unlock password!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.USER_PASSWORD) != 0) {
            result = "error";
            message = "This feature is not available in this license!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        int retValue = -1;
        UserManagement uManagement = new UserManagement();
        AuthUser user = uManagement.getUser(sessionId, channel.getChannelid(), _userid);

        retValue = uManagement.ChangeStatus(sessionId, channel.getChannelid(), _userid, status);
        log.debug("ChangeStatus :: "+retValue);

        AuditManagement audit = new AuditManagement();

        String resultString = "ERROR";
        
        int istatus = user.getStatePassword();
        String strstaus = "Removed";
        if (istatus == ACTIVE_STATUS) {
            strstaus = "Active";
        } else if (istatus == SUSPEND_STATUS) {
            strstaus = "Suspended";
        } else if (istatus == LOCKED_STATUS) {
            strstaus = "Locked";
        }
        String strstaus1 = "Removed";
        if (status == ACTIVE_STATUS) {
            strstaus1 = "Active";
        } else if (status == SUSPEND_STATUS) {
            strstaus1 = "Suspended";
        } else if (status == LOCKED_STATUS) {
            strstaus1 = "Locked";
        }
        
        if (retValue == 0) {
            resultString = "SUCCESS";

            audit.AddAuditTrail(sessionId, channel.getChannelid(), 
                    operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), 
                    "Unlock Password", resultString, retValue,
                    "User Management", 
                    "Current State=" + strstaus, "New State=" + strstaus1,
                    "USERPASSWORD", 
                    _userid);
          
        }  else {

            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Unlock Password", resultString, retValue,
                    "User Management", "Current State=" + strstaus, "Current State=" + strstaus,
                    "USERPASSWORD", _userid);

            result = "error";
            message = "unlock password failed!!";
            out.print(json);
            out.flush();
            return;
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);

        } catch (Exception e) {
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static Object createObject(Constructor constructor, Object[] arguments) {

        //System.out.println("Constructor: " + constructor.toString());
        Object object = null;

        try {
            object = constructor.newInstance(arguments);
            //System.out.println("Object: " + object.toString());
            return object;
        } catch (Exception e) {
            log.error("exception caught :: ",e);
        }
        return object;
    }
}
