package com.mollatech.axiom.v2.face.handler.operator.template;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.v2.face.handler.operator.setpassword;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class editemailtemplate extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editemailtemplate.class.getName());

   final String itemType = "MESSAGES";    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        
        log.info("is started :: ");

        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Template updated successfully!!!";

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);

             //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        
        
        String operatorId = operatorS.getOperatorid();
        log.debug("operatorId :: "+operatorId);
        
        JSONObject json = new JSONObject();
        int retValue = -1;
        String _tid = request.getParameter("idEmailTemplateId");
        int _templateid = Integer.valueOf(_tid);
        log.debug("_templateid :: "+_templateid);
        String _templatename = request.getParameter("_templateE_name");
        log.debug("_templatename :: "+_templatename);
        String _templatesubject = request.getParameter("_templateE_subject");
        log.debug("_templatesubject :: "+_templatesubject);
        String _templatebody = request.getParameter("emailContents");
        log.debug("_templatebody :: "+_templatebody);
        String _templateVariables = request.getParameter("_templateE_variable");
        log.debug("_templateVariables :: "+_templateVariables);
        Templates checkObj=null;
        
      if (_templatename == null || _templatebody == null || _templatesubject == null||_templateVariables == null) {
            result = "error";
            message = "Invalid Template Details!!";
            try{
            json.put("_result", result);
            json.put("_message", message);}catch(Exception e){}
            out.print(json);
            out.flush();
            return;
        }
      if (_templatename.isEmpty() == true || _templatebody.isEmpty() == true 
              || _templatesubject.isEmpty() == true||_templateVariables.isEmpty() == true) {
            result = "error";
            message = "Invalid Template Details!!";
            try{
            json.put("_result", result);
            json.put("_message", message);}catch(Exception e){
                 log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        TemplateManagement tManagement = new TemplateManagement();
        AuditManagement audit = new AuditManagement();
        
        checkObj = tManagement.CheckTemplate(sessionId, channel.getChannelid(), _templatename);
        
         
        if (checkObj != null && checkObj.getTemplateid() != _templateid ) {
            result = "error";
            message = "Duplicate Name!!";
            try { json.put("_result", result);
            json.put("_message", message);}catch(Exception e){}
            out.print(json);
            out.flush();
            return;

        }
        
        
        
         Templates oldTObj = tManagement.LoadTemplate(sessionId, channel.getChannelid(), _templateid);
        retValue = tManagement.EditTemplate(sessionId, channel.getChannelid(),_templateid,_templatename, _templatebody,_templateVariables, _templatesubject);
        log.debug("EditTemplate :: "+retValue);

               byte[] bTemplate =  oldTObj.getTemplatebody();
               String tName = oldTObj.getTemplatename();
               String tVariables = oldTObj.getTemplatevariables();
               String tSubject = oldTObj.getSubject();
         String tBody = new String(bTemplate, "UTF-8");
          String resultString = "ERROR";
        
          if(retValue == 0){
              resultString = "SUCCESS";
              audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                      request.getRemoteAddr(),
                      channel.getName(),
                      
                  remoteaccesslogin, operatorS.getName(), new Date(),
                  "Edit Template", resultString, retValue,
                  "Template Management", 
                  " Name ="+tName+" Subject ="+tSubject+" Variables ="+tVariables+" Body ="+tBody,
                  " Name ="+_templatename+" Subject ="+_templatesubject+" Variables ="+_templateVariables+" Body ="+_templatebody,
                  itemType, _tid);

          }else if(retValue != 0){
               audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                       request.getRemoteAddr(),
                       channel.getName(),
                  remoteaccesslogin, operatorS.getName(), new Date(),
                  "Edit Template", resultString, retValue,
                  "Template Management", 
                  "Template Name ="+tName+" Subject ="+tSubject+" Variables ="+tVariables+" Body ="+tBody,
                  "Failed Edit Template...!!!",
                  itemType, _tid);

          }
          
          

        
        
        if (retValue != 0) {
            result = "error";
            message = "Template update failed!!!";
        } else {
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                Logger.getLogger(setpassword.class.getName()).log(Level.SEVERE, null, ex);
                log.error("exception caught :: ",ex);
            } finally {
                out.print(json);
                out.flush();
            }
        }
        log.info("is ended :: ");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
