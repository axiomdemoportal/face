/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.certificates;

import com.mollatech.axiom.connector.communication.UserCertificate;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.CryptoManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.RootCertificateSettings;
//import com.mollatech.csrdecoder.AuthToken;
//import com.mollatech.csrdecoder.CSRData;
//import com.mollatech.csrdecoder.DecodeCsrTP;
//import com.mollatech.csrdecoder.QbV1DecodeCsrRequest;
//import com.mollatech.csrdecoder.QbV1DecodeCsrResponse;
//import com.mollatech.csrdecoder.QueryRequestHeader;
//
//import com.mollatech.globalsignconnector.Approver;
//import com.mollatech.globalsignconnector.Approvers;
//import com.mollatech.globalsignconnector.QbV1GetApproverListRequest;
//import com.mollatech.globalsignconnector.QbV1GetApproverListResponse;
//import com.mollatech.globalsignconnector.globalSignCAConnector;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBElement;
import org.json.JSONObject;

/**
 *
 * @author Pramod
 */
public class generateCSR extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(generateCSR.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel::"+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId::"+sessionId);
        String _certType = request.getParameter("_certType");
        log.debug("_certType::"+_certType);
        String _caType = request.getParameter("_caType");
        log.debug("_caType::"+_caType);
        String _product_type = request.getParameter("_product_type");
        log.debug("_product_type::"+_product_type);
        String common_name = request.getParameter("common_nameTP");
         log.debug("common_name::"+common_name);
        String _organizationName = request.getParameter("_txtOrganizationTP");
        log.debug("_organizationName::"+_organizationName);
        String OrganizationUnit = request.getParameter("_txtOrganizationUnitTP");
        log.debug("OrganizationUnit::"+OrganizationUnit);
        String _txtLocalityName = request.getParameter("_txtLocalityNameTP");
        log.debug("_txtLocalityName::"+_txtLocalityName);
        String _txtState = request.getParameter("_txttpStateTP");
        log.debug("_txtState::"+_txtState);
        String _txtCountry = request.getParameter("_CountryNameTP");
        log.debug("_txtCountry::"+_txtCountry);
        String _txtEmail = request.getParameter("_txtEmailTP");
        log.debug("_txtEmail::"+_txtEmail);
         String _keysize = request.getParameter("_keysizeTP");
         log.debug("_keysize ::"+_keysize);
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        
 
        if (_product_type.equals("1")) {
            _product_type = "DV";
        }
        if (_product_type.equals("2")) {
            _product_type = "EV";
        }
        if (_certType.equals("1")) {
            _certType = "APPCERTS";
        } else if (_certType.equals("2")) {
            _certType = "SSLCERTS";
        }
        String resultJson = "success";
        String _messageJson = "Certificate generated successfulyy for domain or ip:" + common_name;
        SettingsManagement setManagement = new SettingsManagement();
        RootCertificateSettings rCertSettings = (RootCertificateSettings) setManagement.getSetting(sessionId, channel.getChannelid(), SettingsManagement.RootConfiguration, 2);
        if (rCertSettings == null) {

        }
        int iLength=0;
         if(_keysize!=null)
         {
         iLength =Integer.parseInt(_keysize);
         }else{
          iLength=rCertSettings.keyLength;
         }
        CryptoManagement cManagement = new CryptoManagement();
        Date d = new Date();
        String srno = "" + d.getTime() + cManagement.hashCode();
        UserCertificate uCert = cManagement.GenerateCSRforSSLandApplication(_txtCountry, _txtLocalityName, _organizationName, OrganizationUnit, _txtEmail, common_name, cManagement.GenerateRSAKeyPair(iLength), srno);
        if (uCert.CSR != null && !uCert.CSR.isEmpty()) {
            request.getSession().setAttribute("CSR", uCert);
            try {
//                QbV1DecodeCsrRequest decodeReq = new QbV1DecodeCsrRequest();
//                AuthToken auToken = new AuthToken();
//                auToken.setUserName(rCertSettings.userId);
//                auToken.setPassword(rCertSettings.password);
//                QueryRequestHeader reqHeader = new QueryRequestHeader();
//                reqHeader.setAuthToken(auToken);
//                decodeReq.setQueryRequestHeader(reqHeader);
//                decodeReq.setCSR(uCert.CSR);
//                decodeReq.setProductType(_product_type);
//                QbV1DecodeCsrResponse csrDecodeResp = DecodeCsrTP.decodeCSR(decodeReq);//  Dec.decodeCSR(decodeReq);
//                CSRData csrData = csrDecodeResp.getCSRData();
//                JAXBElement<String> commonName = csrData.getCommonName();
//                com.mollatech.globalsignconnector.QueryRequestHeader requestHeaderforSSl = new com.mollatech.globalsignconnector.QueryRequestHeader();
//                com.mollatech.globalsignconnector.AuthToken autokenSSL = new com.mollatech.globalsignconnector.AuthToken();
//                autokenSSL.setPassword(rCertSettings.password);
//                autokenSSL.setUserName(rCertSettings.userId);
//                requestHeaderforSSl.setAuthToken(autokenSSL);
//                QbV1GetApproverListRequest approveListRrq = new QbV1GetApproverListRequest();
//                approveListRrq.setFQDN(commonName.getValue().toString());
//                approveListRrq.setQueryRequestHeader(requestHeaderforSSl);
//                QbV1GetApproverListResponse approveList = globalSignCAConnector.getApproverList(approveListRrq);
//                JAXBElement<Approvers> x = approveList.getApprovers();
//
//                List<Approver> approverList = x.getValue().getSearchOrderDetail();
//                Iterator iter = approverList.iterator();
//                JSONObject jsonApprEmail = new JSONObject();
//
//                List<String> approverEmail = new ArrayList<String>();
//
//                while (iter.hasNext()) {
//                    Approver ap = (Approver) iter.next();
//                    approverEmail.add(ap.getApproverEmail());
//                }
//                request.getSession().setAttribute("ApproverEmailsJSONOBJ", approverEmail);
                json.put("_result", resultJson);
                json.put("_message", _messageJson);
               json.put("_csr", uCert.CSR);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
                
            }
            out.print(json);
            out.flush();

        } else {
            resultJson = "error";
            _messageJson = "Certificate generation failed!!!";
            try {
                json.put("_result", resultJson);
                json.put("_message", _messageJson);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
                
            }
            out.print(json);
            out.flush();

        }
        log.info("is ended :: ");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
