/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.users;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Usergroups;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserGroupsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author nilesh
 */
public class changeusergroup extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(changeusergroup.class.getName());

    final String itemtype = "USERPASSWORD";
    int FAILED_TO_SEND_ALERT = 9;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());
        String _userid = request.getParameter("_userIdE");
        log.debug("_userid :: "+_userid);
        String _user_group = request.getParameter("_groupidE");
        
        String _groupname = request.getParameter("_groupname");
        log.debug("_groupname :: "+_groupname);

        int groupid = 0;
        if (_user_group != null) {
            groupid = Integer.parseInt(_user_group);
            log.debug("groupid :: "+groupid);
        }
        String result = "success";
        String message = "User group updated sucessfully";

        UserManagement uManagement = new UserManagement();
        AuditManagement audit = new AuditManagement();
        UserGroupsManagement userGrMgmt= new UserGroupsManagement();
       
        
        AuthUser olduser = uManagement.getUser(sessionId, channel.getChannelid(), _userid);
      
        
         Usergroups OldGroup=null;
         Usergroups userNewgroup=null;
         OldGroup= userGrMgmt.getGroupByGroupId(sessionId, channel.getChannelid(), olduser.getGroupid());
       
           String oldGroupName="";
        if(OldGroup==null)
         oldGroupName="NA";
        else
           oldGroupName=OldGroup.getGroupname();
         
         int retValue = uManagement.EditUser(sessionId, channel.getChannelid(), _userid, olduser.userName,
                olduser.phoneNo, olduser.email, groupid,
                "MSC Trustgate.com Sdn Bhd.", "140541211211", "Passport",
                "Business Development", "MY", "KL",
                "Mont Kiara",
                "Manager");
        log.debug("getGroupByGroupId :: "+retValue);
        String resultString = "ERROR";
        if (retValue == 0) {
              
            
            resultString = "SUCCESS";
        }
        
      userNewgroup=  userGrMgmt.getGroupByGroupId(sessionId, channel.getChannelid(), groupid);
        
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        if (retValue == 0) {
            try {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operatorS.getName(), new Date(), "Edit User Group", resultString, retValue,
                        "User Management", "User ID=" + olduser.getUserId() + ",Name=" + olduser.getUserName()
                        + "Old Group Name " + oldGroupName,
                        "User ID=" + olduser.getUserId() + ",Name=" + olduser.getUserName()
                        + "Group id " + userNewgroup.getGroupname(),
                        itemtype, _userid);

                result = "success";
                message = "User group updated Successfully!!";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_value", _groupname);
                out.print(json);
                out.flush();
                return;
            } catch (JSONException ex) {
                log.error("exception caught :: ",ex);
            }

        } else if (retValue == FAILED_TO_SEND_ALERT) {

            try {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operatorS.getName(), new Date(), 
                        "Edit User Group", "SUCCESS", retValue,
                        "User Management", "User ID=" + olduser.getUserId() + ",Name=" + olduser.getUserName()
                        + "Old Group Name " + oldGroupName,
                        "User ID=" + olduser.getUserId() + ",Name=" + olduser.getUserName()
                        + "New Group Name " + userNewgroup.getGroupname() + " but could not send email",
                        itemtype, _userid);

                result = "success";
                message = "User group updated Successfully!!";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_value", _groupname);
                out.print(json);
                out.flush();
                return;
            } catch (JSONException ex) {
                log.error("exception caught :: ",ex);
            }

        } else {
            try {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                        remoteaccesslogin, operatorS.getName(), new Date(), "Edit User Group", 
                        "FAILURE", retValue,
                        "User Management", "User ID=" + olduser.getUserId() + ",Name=" + olduser.getUserName()
                        + "Group Name" + olduser.getGroupid(),
                        "Failed to  change user group",
                        itemtype, _userid);
                result = "error";
                message = "Failed to update user group!!";
                json.put("_result", result);
                json.put("_message", message);

                out.print(json);
                out.flush();
                return;
            } catch (JSONException ex) {
                log.error("exception caught :: ",ex);
            }
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
