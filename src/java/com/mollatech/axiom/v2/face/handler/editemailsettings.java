package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.OOBEmailChannelSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class editemailsettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editemailsettings.class.getName());

    //Static variables
    //public static final int EMAIL = 2;
    public static final int ACTIVE_STATUS = 1;
    public static final int SUSPENDED_STATUS = 1;
    public static final int PREFERENCE_ONE = 1;   //primary
    public static final int PREFERENCE_TWO = 2;
    final String itemtype = "SETTINGS";
//    private void PrintRequestParameters(HttpServletRequest req) {
//        Enumeration<String> paramNames = req.getParameterNames();
//        while (paramNames.hasMoreElements()) {
//            String paramName = paramNames.nextElement();
//            System.out.println(paramName + "=" + req.getParameter(paramName));
//        }
//    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        //PrintRequestParameters(request);
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String OperatorID = operatorS.getOperatorid();

        String _channelId = channel.getChannelid();
        SettingsManagement sMngmt = new SettingsManagement();
        int retValue = -1;
        
            log.debug("channel :: " + channel.getName());
            log.debug("operatorS :: " + operatorS.getOperatorid());
            log.debug("sessionId :: " + sessionId);
            log.debug("remoteaccesslogin :: " + remoteaccesslogin);
            log.debug("getChannelid :: " + channel.getChannelid());

        String result = "success";
        String message = "Email SMTP Gateway Settings Update Successful!!!";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.GATEWAY_EMAIL) != 0) {
            result = "error";
            message = "This feature is not available in this license!!!";

            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }

            out.print(json);
            out.flush();
            return;
        }

        String _preference = request.getParameter("_perferenceEMAIL");
        int _iPreference = Integer.parseInt(_preference);
        log.debug("_iPreference :: " + _iPreference);

        

        int _type = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.EMAIL;
        String strType = String.valueOf(_type);
        Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _type, _iPreference);

        OOBEmailChannelSettings emailObj = null;
        boolean bAddSetting = false;

        if (settingsObj == null) {
            emailObj = new OOBEmailChannelSettings();
            emailObj.setChannelId(_channelId);
            emailObj.setPreference(_iPreference);
            bAddSetting = true;
        } else {
            emailObj = (OOBEmailChannelSettings) settingsObj;
        }

        if (_type == com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.EMAIL) {
            if (_iPreference == com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE) {
                //OOBEmailChannelSettings emailObj = new OOBEmailChannelSettings();
                String ssl = request.getParameter("_sslEMAIL");
                boolean bSSLEnabled = false;
                if (ssl != null) {
                    if (ssl.equals("sslEnabled")) {
                        bSSLEnabled = true;
                    }

                }

                String _auth = request.getParameter("_authRequiredEMAIL");
                log.debug("_auth :: " +_auth);

                boolean bAuthRequired = false;
                if (_auth != null) {
                    if (_auth.equals("authEnabled")) {
                        bAuthRequired = true;
                    }
                }
                String emailport = request.getParameter("_portEMAIL");
                log.debug("emailport :: " +emailport);
              
                if (emailport == null) {
                    result = "error";
                  
                    message = "Plese enter port no!!!";

                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                       log.error("Exception caught :: ",e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
                 if (emailport.isEmpty() == true) {
                    result = "error";
                  
                    message = "Plese enter port no!!!";

                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                       log.error("Exception caught :: ",e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
                 String classname = request.getParameter("_classNameEMAIL");
                  if (classname == null) {
                    result = "error";
                  
                    message = "Plese enter Implementation Class Name!!!";

                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("Exception caught :: ",e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }if (classname.isEmpty() == true) {
                    result = "error";
                  
                    message = "Plese enter Implementation Class Name!!!";

                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("Exception caught :: ",e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
                //boolean bAuthRequired = Boolean.valueOf(request.getParameter("_authRequiredEMAIL")).booleanValue();
                //boolean bSSLEnabled = Boolean.valueOf(request.getParameter("_sslEMAIL")).booleanValue();
                int _autofailoverEMAIL = Integer.valueOf(request.getParameter("_autofailoverEMAIL")).intValue();
                int _port = Integer.valueOf(request.getParameter("_portEMAIL")).intValue();
                int _retryduration = Integer.valueOf(request.getParameter("_retrydurationEMAIL")).intValue();
                int _retrycount = Integer.valueOf(request.getParameter("_retriesEMAIL")).intValue();
                int _status = Integer.valueOf(request.getParameter("_statusEMAIL")).intValue();

                emailObj.setAuthRequired(bAuthRequired);
                emailObj.setSsl(bSSLEnabled);
                emailObj.setPreference(_iPreference);
                emailObj.setAutofailover(_autofailoverEMAIL);
                emailObj.setChannelId(_channelId);
                emailObj.setClassName(request.getParameter("_classNameEMAIL"));
                emailObj.setFromEmail(request.getParameter("_fromEmailEMAIL"));
                emailObj.setFromName(request.getParameter("_fromNameEMAIL"));
                emailObj.setImplementationJAR("");
                emailObj.setPassword(request.getParameter("_passwordEMAIL"));
                emailObj.setPort(_port);
                emailObj.setReserve1(request.getParameter("_reserve1EMAIL"));
                emailObj.setReserve2(request.getParameter("_reserve2EMAIL"));
                emailObj.setReserve3(request.getParameter("_reserve3EMAIL"));
                emailObj.setRetrycount(_retrycount);
                emailObj.setRetryduration(_retryduration);
                emailObj.setSmtpIp(request.getParameter("_ipEMAIL"));
                emailObj.setStatus(_status);
                emailObj.setUserId(request.getParameter("_userIdEMAIL"));
            } else {

                //OOBEmailChannelSettings emailObj = new OOBEmailChannelSettings();
                String ssl = request.getParameter("_sslEMAILS");
                boolean bSSLEnabled = false;
                if (ssl != null) {
                    if (ssl.equals("sslEnabled")) {
                        bSSLEnabled = true;
                    }

                }

                String _auth = request.getParameter("_authRequiredEMAILS");

                boolean bAuthRequired = false;
                if (_auth != null) {
                    if (_auth.equals("authEnabled")) {
                        bAuthRequired = true;
                    }
                }
                String emailport2 = request.getParameter("_portEMAILS");
                 if (emailport2 == null) {
                    result = "error";
                  
                    message = "Plese enter port no!!!";

                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("Exception caught :: ",e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
                    if (emailport2.isEmpty() == true) {
                    result = "error";
                  
                    message = "Plese enter port no!!!";

                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("Exception caught :: ",e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
                    String classname = request.getParameter("_classNameEMAILS");
                  if (classname == null) {
                    result = "error";
                  
                    message = "Plese enter Implementation Class Name!!!";

                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("Exception caught :: ",e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                } if (classname.isEmpty() == true) {
                    result = "error";
                  
                    message = "Plese enter Implementation Class Name!!!";

                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("Exception caught :: ",e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
                //boolean bAuthRequired = Boolean.valueOf(request.getParameter("_authRequiredEMAILS")).booleanValue();
                //boolean bSSLEnabled = Boolean.valueOf(request.getParameter("_sslEMAILS")).booleanValue();
                //int _autofailoverEMAIL = Integer.valueOf(request.getParameter("_autofailoverEMAILS")).intValue();
                int _port = Integer.valueOf(request.getParameter("_portEMAILS")).intValue();
                int _retryduration = Integer.valueOf(request.getParameter("_retrydurationEMAILS")).intValue();
                int _retrycount = Integer.valueOf(request.getParameter("_retriesEMAILS")).intValue();
                int _status = Integer.valueOf(request.getParameter("_statusEMAILS")).intValue();
                log.debug("_port :: "+_port);
                log.debug("_retryduration :: "+_retryduration);
                log.debug("_retrycount :: "+_retrycount);
                log.debug("_status :: "+_status);
                emailObj.setAuthRequired(bAuthRequired);
                emailObj.setSsl(bSSLEnabled);
                emailObj.setPreference(_iPreference);
                //emailObj.setAutofailover(_autofailoverEMAIL);
                emailObj.setAutofailover(0); //inactive 
                emailObj.setChannelId(_channelId);
                emailObj.setClassName(request.getParameter("_classNameEMAILS"));
                emailObj.setFromEmail(request.getParameter("_fromEmailEMAILS"));
                emailObj.setFromName(request.getParameter("_fromNameEMAILS"));
                emailObj.setImplementationJAR("");
                emailObj.setPassword(request.getParameter("_passwordEMAILS"));
                emailObj.setPort(_port);
                emailObj.setReserve1(request.getParameter("_reserve1EMAILS"));
                emailObj.setReserve2(request.getParameter("_reserve2EMAILS"));
                emailObj.setReserve3(request.getParameter("_reserve3EMAILS"));
                emailObj.setRetrycount(_retrycount);
                emailObj.setRetryduration(_retryduration);
                emailObj.setSmtpIp(request.getParameter("_ipEMAILS"));
                emailObj.setStatus(_status);
                emailObj.setUserId(request.getParameter("_userIdEMAILS"));

            }
        }
        AuditManagement audit = new AuditManagement();
        if (bAddSetting == true) {
            retValue = sMngmt.addSetting(sessionId, _channelId, _type, _iPreference, emailObj);
            log.debug("editemailsettings::addSetting::" + retValue);
            String resultString = "ERROR";
            if (retValue == 0) {
                resultString = "SUCCESS";
                //audit.setIP("127.0.0.1");

                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add Email Gateway Settings", resultString, retValue, "Setting Management",
                        "", " Auth Required=" + emailObj.isAuthRequired() + " Secure=" + emailObj.isSsl() + " Preference=" + emailObj.getPreference()
                        + " AutoFailOver=" + emailObj.getAutofailover() + " Channel Id=" + emailObj.getChannelId() + " Class Name=" + emailObj.getClassName()
                        + " From Email=" + emailObj.getFromEmail() + " From Name=" + emailObj.getFromName() + " Implementation Jar=" + emailObj.getImplementationJAR()
                        + " Password=*****" + " Port=" + emailObj.getPort() + " Reserve1=" + emailObj.getReserve1() + " Reserve2=" + emailObj.getReserve2()
                        + " Reserve3=" + emailObj.getReserve3() + " Retry Count=" + emailObj.getRetrycount() + " Retry Duration=" + emailObj.getRetryduration()
                        + " SMTP Ip=" + emailObj.getSmtpIp() + " Status=" + emailObj.getStatus() + " User Id" + emailObj.getUserId(),
                        itemtype, channel.getChannelid());
            } else if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add Email Gateway Settings", resultString, retValue, "Setting Management",
                        "", "Failed to Add Email Gateway Settings",
                        itemtype, channel.getChannelid());
            }

        } else {
            OOBEmailChannelSettings oldemailObj = (OOBEmailChannelSettings) sMngmt.getSetting(sessionId, _channelId, _type, _iPreference);
            retValue = sMngmt.changeSetting(sessionId, _channelId, _type, _iPreference, settingsObj, emailObj);
            log.debug("changeSetting :: " + retValue);
            
            String resultString = "ERROR";

            if (retValue == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Change Email Gateway Settings", resultString, retValue, "Setting Management",
                        " Auth Required=" + oldemailObj.isAuthRequired() + " Secure=" + oldemailObj.isSsl() + " Preference=" + oldemailObj.getPreference()
                        + " AutoFailOver=" + oldemailObj.getAutofailover() + " Channel Id=" + oldemailObj.getChannelId() + " Class Name=" + oldemailObj.getClassName()
                        + " From Email=" + oldemailObj.getFromEmail() + " From Name=" + oldemailObj.getFromName() + " Implementation Jar=" + oldemailObj.getImplementationJAR()
                        + " Password=*****" + " Port=" + oldemailObj.getPort() + " Reserve1=" + oldemailObj.getReserve1() + " Reserve2=" + oldemailObj.getReserve2()
                        + " Reserve3=" + oldemailObj.getReserve3() + " Retry Count=" + oldemailObj.getRetrycount() + " Retry Duration=" + oldemailObj.getRetryduration()
                        + " SMTP Ip=" + oldemailObj.getSmtpIp() + " Status=" + oldemailObj.getStatus() + " User Id" + oldemailObj.getUserId(),
                        " Auth Required=" + emailObj.isAuthRequired() + " Secure=" + emailObj.isSsl() + " Preference=" + emailObj.getPreference()
                        + " AutoFailOver=" + emailObj.getAutofailover() + " Channel Id=" + emailObj.getChannelId() + " Class Name=" + emailObj.getClassName()
                        + " From Email=" + emailObj.getFromEmail() + " From Name=" + emailObj.getFromName() + " Implementation Jar=" + emailObj.getImplementationJAR()
                        + " Password=*****" + " Port=" + emailObj.getPort() + " Reserve1=" + emailObj.getReserve1() + " Reserve2=" + emailObj.getReserve2()
                        + " Reserve3=" + emailObj.getReserve3() + " Retry Count=" + emailObj.getRetrycount() + " Retry Duration=" + emailObj.getRetryduration()
                        + " SMTP Ip=" + emailObj.getSmtpIp() + " Status=" + emailObj.getStatus() + " User Id" + emailObj.getUserId(),
                        itemtype, channel.getChannelid());

            } else if (retValue != 0) {

                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Change Email Gateway Settings", resultString, retValue, "Setting Management",
                        " Auth Required=" + oldemailObj.isAuthRequired() + " Secure=" + oldemailObj.isSsl() + " Preference=" + oldemailObj.getPreference()
                        + " AutoFailOver=" + oldemailObj.getAutofailover() + " Channel Id=" + oldemailObj.getChannelId() + " Class Name=" + oldemailObj.getClassName()
                        + " From Email=" + oldemailObj.getFromEmail() + " From Name=" + oldemailObj.getFromName() + " Implementation Jar=" + oldemailObj.getImplementationJAR()
                        + " Password=*****" + " Port=" + oldemailObj.getPort() + " Reserve1=" + oldemailObj.getReserve1() + " Reserve2=" + oldemailObj.getReserve2()
                        + " Reserve3=" + oldemailObj.getReserve3() + " Retry Count=" + oldemailObj.getRetrycount() + " Retry Duration=" + oldemailObj.getRetryduration()
                        + " SMTP Ip=" + oldemailObj.getSmtpIp() + " Status=" + oldemailObj.getStatus() + " User Id" + oldemailObj.getUserId(),
                        "Failed To Email Gateway Settings", itemtype, channel.getChannelid());
            }

        }

        if (retValue != 0) {
            result = "error";
            message = "Email Gateway Settings Update Failed!!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception e) {log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }

    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";

    }// </editor-fold>
}
