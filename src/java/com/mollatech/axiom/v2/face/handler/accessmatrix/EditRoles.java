/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.accessmatrix;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Roles;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class EditRoles extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(EditRoles.class.getName());

    final String itemTypeOp = "ROLES";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("is started :: ");

        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Role updated successfully!!!";

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel ::" + channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId::" + sessionId);

        JSONObject json = new JSONObject();
        int retValue = 0;
        int checkValue = 0;

        String _roleIDE = request.getParameter("_roleIDE");
        log.debug("_roleIDE::" + _roleIDE);
        String _rolenameE = request.getParameter("_rolenameE");
        log.debug("_rolenameE::" + _rolenameE);
        //String _rolestatusE = request.getParameter("_rolestatusE");

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin ::" + remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS::" + operatorS.getOperatorid());
        

        if (_roleIDE == null || _rolenameE == null) {
            result = "error";
            message = "Invalid Parameters!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
               log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        if (_rolenameE.length() > 25) {
            result = "error";
            message = "Role Name less than 25 characters!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        }
        int iroleIDE = Integer.parseInt(_roleIDE);
        //  int irolestatusE = Integer.parseInt(_rolestatusE);

        OperatorsManagement oManagement = new OperatorsManagement();
        AuditManagement audit = new AuditManagement();
        Roles role = oManagement.getRoleByRoleId(channel.getChannelid(), iroleIDE);

        Roles roleObj = oManagement.getRoleByName(channel.getChannelid(), _rolenameE);

        if (roleObj != null) {
            result = "error";
            message = "Role with same name already exists!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                log.error("exception caught :: ",ex);
            }
            out.print(json);
            out.flush();
            return;
        }
        retValue = oManagement.EditRole(sessionId, channel.getChannelid(),
                
                iroleIDE,
                _rolenameE);
        log.debug("EditRole::" + retValue);

        String resultString = "ERROR";
        String strStatus = "";
        if (role.getStatus() == OperatorsManagement.ACTIVE_STATUS) {
            strStatus = "ACTIVE_STATUS";
        } else if (role.getStatus() == OperatorsManagement.SUSPEND_STATUS) {
            strStatus = "SUSPEND_STATUS";
        }
        if (retValue == 0) {
            resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                    "Edit Role", resultString, retValue, "Role Management",
                    "Role Name=" + role.getName(),
                    "Role Name=" + _rolenameE,
                    itemTypeOp, ""+role.getRoleid());
            result = "success";

        } else {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                    "Edit Role", resultString, retValue, "Role Management",
                    "Role Name=" + role.getName() + " Role Status=" + strStatus,
                    "Failed To Edit Operator",
                    itemTypeOp, roleObj.getRoleid().toString());
            result = "error";
            message = "Role update failed!!!";
        }

        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
