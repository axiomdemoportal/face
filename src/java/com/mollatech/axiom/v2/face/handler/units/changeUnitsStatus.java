package com.mollatech.axiom.v2.face.handler.units;


import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet;
import com.mollatech.axiom.nucleus.db.Units;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.operation.AxiomOperator;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author nilesh
 */
public class changeUnitsStatus extends HttpServlet {
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(changeUnitsStatus.class.getName());
  final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;
    final String itemType = "UNITS";
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
          response.setContentType("application/json");
          
          log.info("is started :: ");

        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Units updated successfully!!!";
        String _value = "Active";
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String operatorId = operatorS.getOperatorid();
        log.debug("operatorId :: "+operatorId);

        JSONObject json = new JSONObject();
         
        int retValue = -1;
        String _tid = request.getParameter("_unitId");
        int _unitId = Integer.valueOf(_tid);
        log.debug("operatorId :: "+_unitId);
        String _status = request.getParameter("_status");
        int status = Integer.parseInt(_status);
        log.debug("operatorId :: "+status);

//        Templates checkObj = null;

        if (_tid == null || _status == null) {
            result = "error";
            message = "Invalid Units Details!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        if (status == ACTIVE_STATUS) {
            _value = "Active ";
        } else if (status == SUSPEND_STATUS) {
            _value = "Suspended ";
        }
//        SessionFactoryUtil suTem = new SessionFactoryUtil(SessionFactoryUtil.templates);
//        Session sTem = suTem.openSession();
//        TemplateUtils tmpU = new TemplateUtils(suTem, sTem);
//        Templates tObj = tmpU.getTemplates(channel.getChannelid(), _templateid);
//        sTem.close();
//        suTem.close();

//        message = _value + message;
        if (status != ACTIVE_STATUS) {
         OperatorsManagement oMngt = new OperatorsManagement();
         AxiomOperator[] opr = oMngt.getOperatorByUnitStatus(sessionId, channel.getChannelid(), _unitId, 2);

            if (opr != null) {
//                for (int i = 0; i < opr.length; i++) {
//                    if (opr[i].getStrName().equals("sysadmin")) {
                        result = "error";
                        message = "Change status Failed. Reason: Operators belongs to unit!!!";
                        try {
                            json.put("_result", result);
                            json.put("_message", message);
                        } catch (Exception e) {
                            log.error("exception caught :: ",e);
                        }
                        out.print(json);
                        out.flush();
                        return;
//                    }
//                }
            }
        }
        
        String channelId = channel.getChannelid();
        UnitsManagemet unMngt = new UnitsManagemet();
       Units oldObj =  unMngt.getUnitByUnitId(sessionId, channelId, _unitId);
        

//        TemplateManagement tManagement = new TemplateManagement();
        AuditManagement audit = new AuditManagement();

        retValue = unMngt.ChangeStatus(sessionId,channelId, _unitId, status);
        log.debug("ChangeStatus :: "+retValue);

        String resultString = "ERROR";
        if (retValue == 0) {
            resultString = "SUCCESS";
        }
        
         String oldStatus = "In-Active";
        if (oldObj.getStatus() == 0) {
            oldStatus = "SUCCESS";
        }
         String newStatus = "In-Active";
        if (status == 0) {
            newStatus = "SUCCESS";
        }

        if (retValue == 0) {
            resultString = "Success";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Change Status", resultString, retValue,
                    "Units Management",
                    "Old Status=" + oldStatus, "New Status=" + newStatus+"Updated On ="+new Date(),
                    itemType, "" + _unitId);

        } else if (retValue != 0) {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Change Status", resultString, retValue,
                    "Units Management",
                    "Old Status=" + oldStatus, "New Status=" + newStatus,
                    itemType, "" + _unitId);
            result = "error";
            message = "Status update failed!!!";
//            out.print(json);
//            out.flush();
//            return;

        }

        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);

        } catch (Exception e) {
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("is ended :: ");
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
