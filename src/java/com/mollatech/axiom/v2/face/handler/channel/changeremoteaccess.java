package com.mollatech.axiom.v2.face.handler.channel;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.RemoteAccessManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.json.JSONObject;

public class changeremoteaccess extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(changeremoteaccess.class.getName());

    static final boolean ACTIVE = true;
    static final boolean IN_ACTIVE = false;
    final String itemTypeRE = "REMOTEACCESS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        //Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());

        //here problem is when we are passsing channelid special character are get removed. so channelid get changed.
        String _ch_status = request.getParameter("_ch_rem_status");
        log.debug("_ch_status :: "+_ch_status);
        String _channelid = request.getParameter("_channelid");
        log.debug("_channelid :: "+_channelid);

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String operatorId = operatorS.getOperatorid();
         log.debug("operatorId :: "+operatorId);
        int status = Integer.parseInt(_ch_status);
        
  
        String result = "success";
        String message = "Status Updated successfully....";
        String _value = "Enable";
        if (status == 0) {
            _value = "Disabled";
        }
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        if (_ch_status == null || _channelid == null) {
            result = "error";
            message = "Status not updated!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
                json.put("_value", _value);
            } catch (Exception e) {
                log.error("exception caught ::",e);

            }
            out.print(json);
            out.flush();
            return;
        }

//        if (Integer.valueOf((String) request.getSession().getAttribute("_apOprRole")).intValue() < 3) {
//            result = "error";
//            message = "Permission Denied!!";
//            try {
//                json.put("_result", result);
//                json.put("_message", message);
//                json.put("_value", _value);
//            } catch (Exception e) {
//
//            }
//            out.print(json);
//            out.flush();
//            return;
//        }
        int retValue = -1;

        try {
            RemoteAccessManagement rManagement = new RemoteAccessManagement();
            AuditManagement audit = new AuditManagement();
            if (status == 1) {
                retValue = rManagement.enableRemoteAccess(sessionId, _channelid, ACTIVE);
                log.debug("enableRemoteAccess::"+operatorId);
                String resultString = "ERROR";
                if (retValue == 0) {
                    resultString = "SUCCESS";
                    audit.AddAuditTrail(sessionId, _channelid, operatorS.getOperatorid(),
                            request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), "Change Remote Access",
                            resultString, retValue, "Remote Access Management",
                            "INACTIVE",
                            "ACTIVE",
                            itemTypeRE,
                            _channelid);
                } else if (retValue != 0) {
                    audit.AddAuditTrail(sessionId, _channelid, operatorS.getOperatorid(),
                            request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), "Change Remote Access",
                            resultString, retValue, "Remote Access Management",
                            "", "Failed to Change to active", itemTypeRE,
                            _channelid);

                }

                result = "success";
                message = "Enabled Successfully!!";
            } else {
                retValue = rManagement.enableRemoteAccess(sessionId, _channelid, IN_ACTIVE);
                log.debug("enableRemoteAccess::"+operatorId);
                String resultString = "ERROR";
                if (retValue == 0) {
                    resultString = "Success";
                    audit.AddAuditTrail(sessionId, _channelid, operatorS.getOperatorid(),
                            request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), "Change Remote Access",
                            resultString, retValue, "Remote Access Management",
                            "ACTIVE", "INACTIVE", itemTypeRE,
                            _channelid);
                } else if (retValue != 0) {
                    audit.AddAuditTrail(sessionId, _channelid, operatorS.getOperatorid(),
                            request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), "Change Remote Access",
                            resultString, retValue, "Remote Access Management",
                            "", "Failed To Change to inactive", itemTypeRE,
                            _channelid);

                }

                result = "success";
                message = "Disabled successfully!!";
            }

            SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
            Session sChannel = suChannel.openSession();
            ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
            Date d = new Date();
            cUtil.updateLastChannelOn(_channelid, d);
            suChannel.close();
            sChannel.close();

        } catch (Exception ex) {
            // TODO handle custom exceptions here
            log.error("exception caught :: ",ex);
            
        }

        if (retValue != 0) {
            result = "error";
            message = "Remote Access not updated!!";
            try {
                json.put("_value", _value);
            } catch (Exception e) {
                log.error("exception caught ::",e);

            }
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);
        } catch (Exception e) {
            log.error("exception caught ::",e);

        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
