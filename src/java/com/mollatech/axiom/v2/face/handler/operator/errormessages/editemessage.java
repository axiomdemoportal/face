/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.operator.errormessages;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Errormessages;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ErrorMessageManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class editemessage extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editemessage.class.getName());

    final String itemType = "ERRORS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        
        log.info("is started :: ");

        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Error Message Updated successfully!!!";

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String operatorId = operatorS.getOperatorid();
        log.debug("operatorId :: "+operatorId);

        JSONObject json = new JSONObject();

        String _tid = request.getParameter("idMessageTemplateId");
        int _templateid = Integer.valueOf(_tid);
        log.debug("_templateid :: "+_templateid);
        String _templatename = request.getParameter("_templateM_name");
        log.debug("_templatename :: "+_templatename);
        String _templatebody = request.getParameter("_templateM_body");
        log.debug("_templatebody :: "+_templatebody);

        if (_templatename == null || _templatebody == null || _templatename.length() == 0 || _templatebody.length() == 0) {
            result = "error";
            message = "Invalid Message Details!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        int retValue = 0;
        Errormessages checkObj = null;
        ErrorMessageManagement tManagement = new ErrorMessageManagement();
        AuditManagement audit = new AuditManagement();

        checkObj = tManagement.CheckTemplate(sessionId, channel.getChannelid(), _templatename);

        if (checkObj != null && checkObj.getErrorid() != _templateid) {
            result = "error";
            message = "Duplicate Name!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;

        }

        boolean _allow = false;

        Errormessages oldTObj = tManagement.LoadTemplate(sessionId, channel.getChannelid(), _templateid);

        SettingsManagement sMngmt = new SettingsManagement();
        SendNotification send = new SendNotification();

        retValue = tManagement.EditErrorMessage(sessionId, channel.getChannelid(), _templateid, _templatename, _templatebody);
        log.debug("EditErrorMessage :: "+retValue);

        //ByteArrayInputStream bais = new ByteArrayInputStream(oldTObj.getErrorMessage());
        //String tBody = (String) UtilityFunctions.deserializeFromObject(bais);
        String tBody = oldTObj.getUsermessage();

        String tName = oldTObj.getErrorname();

        String resultString = "ERROR";

        if (retValue == 0) {

            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), 
                    "Edit ErrorMessage", resultString, retValue,
                    "ErrorMessage Management", "Error Message Name =" + tName + "Message  =" + tBody,
                    "Error Name =" + _templatename + "Error Body =" + _templatebody,
                    itemType, 
                    _tid);

            result = "success";
            message = "Error Message updated successfully!!!";

        } else {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Edit Error", resultString, retValue,
                    "Error Message Management", "Error Name =" + tName + "Error Body =" + tBody,
                    "Failed To Edit Error Template",
                    itemType, 
                    _tid);

            result = "error";
            message = "Error Message Update Failed!!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception ex) {
            log.error("exception caught :: ",ex);

        } finally {
            out.print(json);
            out.flush();
        }
        
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
