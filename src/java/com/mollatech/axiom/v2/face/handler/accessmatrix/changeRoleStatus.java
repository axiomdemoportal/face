package com.mollatech.axiom.v2.face.handler.accessmatrix;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Roles;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author nilesh
 */
public class changeRoleStatus extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(changeRoleStatus.class.getName());

    final String itemTypeOp = "ROLES";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel::" + channel.getName());

        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId::" + sessionId);
        String _role_status = request.getParameter("_role_status");
         log.debug("_role_status::" + _role_status);
        String _roleId = request.getParameter("_roleId");
        log.debug("_roleId::" + _roleId);
        //audit
        Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operator::" + operator.getOperatorid());
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin::" + remoteaccesslogin);
        // String _old_op_status = request.getParameter("_op_status");

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Status Update Successful!!!";

        int retValue = 0;
        int status = Integer.parseInt(_role_status);
        int iroleId = Integer.parseInt(_roleId);
            
  
        String _value = "Active";
        if (status == 0) {
            _value = "Suspended";
        }

        OperatorsManagement oManagement = new OperatorsManagement();
        AuditManagement audit = new AuditManagement();
        Roles oldroleObj = oManagement.getRoleByRoleId(channel.getChannelid(), iroleId);
        retValue = oManagement.ChangeRolesStatus(sessionId, channel.getChannelid(), iroleId, status);
        log.debug("ChangeRolesStatus::" + retValue);

        int istatus = oldroleObj.getStatus();
        String strStatus = "";
        if (istatus == OperatorsManagement.ACTIVE_STATUS) {
            strStatus = "ACTIVE_STATUS";
        } else if (istatus == OperatorsManagement.SUSPEND_STATUS) {
            strStatus = "SUSPEND_STATUS";
        }
        String strNewStatus = "";
        if (status == OperatorsManagement.ACTIVE_STATUS) {
            strNewStatus = "ACTIVE_STATUS";
        } else if (status == OperatorsManagement.SUSPEND_STATUS) {
            strNewStatus = "SUSPEND_STATUS";
        }

        String resultString = "ERROR";

        if (retValue == 0) {
            resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                    "Change Status", resultString, retValue, "Roles Management",
                    "Old Status=" + strStatus, "New Status =" + strNewStatus,
                    itemTypeOp,
                    oldroleObj.getRoleid().toString());
        }
        if (retValue != 0) {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                    "Change Status", resultString, retValue, "Roles Management",
                    "Old Status=" + strStatus, "Failed To Change Status",
                    itemTypeOp, 
                    oldroleObj.getRoleid().toString());
            result = "error";
            message = "Status Update Failed!!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);
        } catch (Exception ex) {
           log.error("exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
