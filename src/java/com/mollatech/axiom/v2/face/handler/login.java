package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.Units;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.OperatorsUtil;
import com.mollatech.axiom.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PasswordTrailManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UnitsManagemet;

import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.ChannelSettings;
import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
import com.mollatech.axiom.nucleus.settings.PasswordPolicySetting;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.ByteArrayInputStream;
import static com.mollatech.axiom.common.utils.UtilityFunctions.context;
import com.mollatech.axiom.connector.access.controller.AccessMatrixSettings;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Roles;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.logging.Level;
import javax.naming.Context;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;
import org.json.JSONObject;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import java.util.logging.Logger;

public class login extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(login.class.getName());

    final String itemType = "LOGIN";
    int SUCCESS = 0;
    int FAILED = -1;
    String BLOCKED = "BLOCKED_IP";
    public static final int ACTIVE_STATUS = 1;
    public static final int admin = 1;
    public static final int unit = 3;
//      public static DirContext ctx;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        String sessionId = "NA";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        try {
            String _operatorName = request.getParameter("_name");
            String _password = request.getParameter("_passwd");
            log.debug("login::operator name::"+_operatorName);
           
            String _channelName = this.getServletContext().getContextPath();
            _channelName = _channelName.replaceAll("/", "");
            AuditManagement audit = new AuditManagement();
            String result = "success";
            String message = "successful credential verification....";
            String url = "home.jsp";
        OperatorsManagement oManagement = new OperatorsManagement();
            Operators operartorObj = null;
           
            if (_operatorName == null || _password == null) {
                result = "error";
                message = "Invalid Credentials!!";
                url = "index.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }

           
            
            
            int retValue = -1;

            try {
                SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
                Session sChannel = suChannel.openSession();
                ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
                SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
                Session sRemoteAcess = suRemoteAcess.openSession();
                RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
                Channels channel = cUtil.getChannel(_channelName);
                if (channel == null) {
                    result = "error";
                    message = "Invalid channel!!";
                    url = "index.jsp";
                    json.put("_result", result);
                    json.put("_message", message);
                    json.put("_url", url);
                    out.print(json);
                    out.flush();
                    sRemoteAcess.close();
                    suRemoteAcess.close();
                    sChannel.close();
                    suChannel.close();
                    return;
                }

                
                
                SessionFactoryUtil suOperators = new SessionFactoryUtil(SessionFactoryUtil.operators);
                Session sOperators = suOperators.openSession();

                OperatorsUtil opUtil = new OperatorsUtil(suOperators, sOperators);
                Operators opObj = opUtil.GetByName(channel.getChannelid(), _operatorName);
                if (opObj == null) {
                    result = "error";
                    message = "operator not found!!";
                    url = "index.jsp";
                    json.put("_result", result);
                    json.put("_message", message);
                    json.put("_url", url);
                    out.print(json);
                    out.flush();
                    suOperators.close();
                    sOperators.close();
                    sRemoteAcess.close();
                    suRemoteAcess.close();
                    sChannel.close();
                    suChannel.close();
                    return;
                }

                String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
                if (credentialInfo == null) {
                    result = "error";
                    message = "Remote Access is not configured properly!!";
                    url = "index.jsp";
                    json.put("_result", result);
                    json.put("_message", message);
                    json.put("_url", url);
                    out.print(json);
                    out.flush();
                    sRemoteAcess.close();
                    suRemoteAcess.close();
                    sChannel.close();
                    suChannel.close();
                    sOperators.close();
                    suOperators.close();
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operartorObj.getOperatorid(),
                            request.getRemoteAddr(), channel.getName(), credentialInfo[0], operartorObj.getName(),
                            new Date(), "Login", "FAILED", FAILED,
                            "Operator Management", "", message, "OPERATOR", operartorObj.getOperatorid());
                    return;
                }
                request.getSession().setAttribute("_apSessionRemoteAccessLoginID", credentialInfo[0]);
                SessionManagement sManagement = new SessionManagement();
               
                if (opObj.getRoleid() != 1) {
                    Roles rolesObj = oManagement.getRoleById(channel.getChannelid(), opObj.getRoleid());
                    if (rolesObj.getStatus() == 0) {
                        result = "error";
                        message = "Operator Role is suspended!!!";
                        url = "index.jsp";
                        json.put("_result", result);
                        json.put("_message", message);
                        json.put("_url", url);
                        out.print(json);
                        out.flush();
                        audit.AddAuditTrail(sessionId, channel.getChannelid(), operartorObj.getOperatorid(),
                                request.getRemoteAddr(), channel.getName(), credentialInfo[0], operartorObj.getName(),
                                new Date(), "Login", "FAILED", FAILED,
                                "Operator Management", "", message, "OPERATOR", operartorObj.getOperatorid());
                        return;
                    }
                }
                SettingsManagement sMngt = new SettingsManagement();
                SendNotification send = new SendNotification();
                Object ipobj = sMngt.getSettingInner(channel.getChannelid(), SettingsManagement.GlobalSettings, 1);
                // OperatorsManagement oObj = new OperatorsManagement();
                SettingsManagement sMngmt = new SettingsManagement();
                ChannelSettings chSettings = new ChannelSettings();
                Object settingsObj = null;
                boolean type = true;

                settingsObj = sMngmt.getSetting(channel.getChannelid(), SettingsManagement.CHANNEL_SETTINGS, SettingsManagement.PREFERENCE_ONE);
                if (settingsObj != null) {
                    chSettings = (ChannelSettings) settingsObj;
                    type = chSettings.isSourceType();
                }
                //new addition
                ChannelProfile channelprofileObj = null;
                Object channelpobj = sMngt.getSettingInner(channel.getChannelid(), SettingsManagement.CHANNELPROFILE_SETTING, 1);

                if (channelpobj == null) {
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                } else {
                    channelprofileObj = (ChannelProfile) channelpobj;
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                }
                if (opObj.getRoleid() != 1) {// 1- sysadmin
                    if (channelprofileObj != null && channelprofileObj._dayRestriction == 1) { // 1- -active
                        retValue = sManagement.getServerStatus(channelprofileObj);
                        if (retValue != 0) {
                            result = "error";
                            message = "Server Down for maintainance,Please try later!!!!!!";
                            url = "index.jsp";
                            json.put("_result", result);
                            json.put("_message", message);
                            json.put("_url", url);
                            out.print(json);
                            out.flush();
                            audit.AddAuditTrail(sessionId, channel.getChannelid(), operartorObj.getOperatorid(),
                                    request.getRemoteAddr(), channel.getName(), credentialInfo[0], operartorObj.getName(),
                                    new Date(), "Login", "FAILED", FAILED,
                                    "Operator Management", "", message, "OPERATOR", operartorObj.getOperatorid());
                            return;
                        }
                    }
                }
                //nilesh no to session active for user 

                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (request.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        
                        checkIp = sMngt.checkIP(channel.getChannelid(), request.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    //need to uncomment later
                    iObj.ipstatus = 1;
                    checkIp=1;
                    //end of uncomment
//                    iObj.ipstatus = 1;
//                    checkIp = 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
//need to uncomment
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channel.getChannelid(), TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", opObj.getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", opObj.getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", request.getRemoteAddr());
                                }

                                Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                                if (aOperator != null) {
                                    String[] emailList = new String[aOperator.length - 1];
                                    for (int i = 1; i < aOperator.length; i++) {
                                        emailList[i - 1] = aOperator[i].getEmailid();
                                    }

                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                              sessionId = BLOCKED;
                                suTemplate.close();
                                sTemplate.close();
                            }


//end of uncomment
                        }
                        sessionId = BLOCKED;
                    } else {
                        sessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());
                    }

                } else {
                    sessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());
                }
                String resultStr = "ERROR";

                if (sessionId.equals("NA")) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), "",
                            request.getRemoteAddr(), channel.getName(), credentialInfo[0],
                            credentialInfo[0], new Date(), "Open Session", resultStr, retValue,
                            "Login", "", "Failed To Open Session",
                            "SESSION",
                            channel.getChannelid());

                } else if (sessionId.equals(BLOCKED)) {
                    retValue = -121;
                    resultStr = "Blocked By IP Filter";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), "",
                            request.getRemoteAddr(), channel.getName(), credentialInfo[0],
                            _operatorName, new Date(), "Open Session", resultStr, retValue,
                            "Session Management", "",
                            "Failed To Open Session because ip is in block list. IP was " + request.getRemoteAddr(),
                            "SESSION",
                            "-"
                    );

                } else {
                    retValue = 0;
                    resultStr = "SUCCESS";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), "",
                            request.getRemoteAddr(), channel.getName(), credentialInfo[0],
                            credentialInfo[0], new Date(),
                            "Open Session",
                            resultStr, retValue,
                            "Session Management",
                            "",
                            "Open Session Success",
                            "SESSION",
                            sessionId);

                }

                if (sessionId == null) {
                    result = "error";
                    message = "Remote Access is disabled for this channel.";
                    url = "index.jsp";
                    json.put("_result", result);
                    json.put("_message", message);
                    json.put("_url", url);
                    out.print(json);
                    out.flush();
                    sRemoteAcess.close();
                    suRemoteAcess.close();
                    sChannel.close();
                    suChannel.close();
                    sOperators.close();
                    suOperators.close();
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), opObj.getOperatorid(),
                            request.getRemoteAddr(), channel.getName(), credentialInfo[0], opObj.getName(),
                            new Date(), "Login", "FAILED", FAILED,
                            "Operator Management", "", message, "OPERATOR", opObj.getOperatorid());
                    return;
                }
                if (sessionId.equalsIgnoreCase(BLOCKED)) {
                    result = "Blocked";
                    message = "IP is blocked by IP filter. Please contact system administrator.";
                    url = "index.jsp";
                    json.put("_result", result);
                    json.put("_message", message);
                    json.put("_url", url);
                    out.print(json);
                    out.flush();
                    sRemoteAcess.close();
                    suRemoteAcess.close();
                    sChannel.close();
                    suChannel.close();
                    sOperators.close();
                    suOperators.close();
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), opObj.getOperatorid(),
                            request.getRemoteAddr(), channel.getName(), credentialInfo[0], opObj.getName(),
                            new Date(), "Login", "FAILED", FAILED,
                            "Operator Management", "", message, "OPERATOR", opObj.getOperatorid());
                    return;
                }

                if (settingsObj == null) {
                    type = true;
                }
                PasswordPolicySetting passObj = null;
                PasswordTrailManagement passwordTrail = new PasswordTrailManagement();
                Object obj = sMngt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.PASSWORD_POLICY_SETTING, SettingsManagement.PREFERENCE_ONE);
                int passwordpolicyAttempt = 0;
                boolean passwordAlert = false;
                int passwordAlertType = 0;
                if (obj != null && obj instanceof PasswordPolicySetting) {
                    passObj = (PasswordPolicySetting) obj;
                    passwordpolicyAttempt = passObj.invalidAttempts;
                    passwordAlert = passObj.passwordAlert;
                    passwordAlertType = passObj.passwordAlertVia;
                }
                if (type == true || _operatorName.equals("sysadmin")) {
                    operartorObj = oManagement.VerifyCredential(sessionId, channel.getChannelid(), _operatorName, _password, passwordpolicyAttempt);
                    if (operartorObj != null) {
                        if (operartorObj.getStatus() == -99) {//Permanent Inactive
                            result = "error";
                            message = "Account is Permanent Inactive !!!";
                            json.put("_result", result);
                            json.put("_message", message);
                            out.print(json);
                            out.flush();
                            audit.AddAuditTrail(sessionId, channel.getChannelid(), operartorObj.getOperatorid(),
                                    request.getRemoteAddr(), channel.getName(), credentialInfo[0], operartorObj.getName(),
                                    new Date(), "Login", "FAILED", FAILED,
                                    "Operator Management", "", message, "OPERATOR", operartorObj.getOperatorid());
                            return;
                        }
                        if (passwordAlert == true && operartorObj.getStatus() == -9) {
                            if(passObj.passwordAlertAttempts==operartorObj.getCurrentAttempts())
                            {
                            TemplateManagement templateObj = new TemplateManagement();
                            //removing template from UI - 28han2015
                            //Templates templateobj = templateObj.LoadTemplate(sessionId, channel.getChannelid(), passObj.passwordAlertTemplateID);
                            Templates templateobj = templateObj.LoadbyName(sessionId, channel.getChannelid(), "email.password.policy.alert");
                            //end of change
                            if (templateobj != null && templateobj.getStatus() == templateObj.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templateobj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                 strmessageBody=strmessageBody.replace("#name#", operartorObj.getName());
                                 strmessageBody=strmessageBody.replace("#channel#", channel.getName());
                                  strmessageBody=strmessageBody.replace("#attempts#",""+operartorObj.getCurrentAttempts());
                                String strsubject = templateobj.getSubject();
                                if (passwordAlertType == SendNotification.SMS || passwordAlertType == SendNotification.USSD || passwordAlertType == SendNotification.VOICE) {
                                    send.SendOnMobile(channel.getChannelid(), opObj.getPhone(), strmessageBody, SendNotification.SMS, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                } else if (passwordAlertType == SendNotification.EMAIL) {
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), operartorObj.getEmailid(), strsubject, strmessageBody,
                                     null, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                            }}
                        }
                        if (operartorObj.getStatus() == -9) {//locked
                            result = "error";
                            message = "Invalid Credentials!!!";
                            json.put("_result", result);
                            json.put("_message", message);
                            out.print(json);
                            out.flush();
                            audit.AddAuditTrail(sessionId, channel.getChannelid(), operartorObj.getOperatorid(),
                                    request.getRemoteAddr(), channel.getName(), credentialInfo[0], operartorObj.getName(),
                                    new Date(), "Login", "FAILED", FAILED,
                                    "Operator Management", "", message, "OPERATOR", operartorObj.getOperatorid());
                            return;
                        }
                        if (operartorObj.getStatus() == -1) {//locked
                            result = "error";
                            message = "Account locked due to invalid attempts!!!";
                            json.put("_result", result);
                            json.put("_message", message);
                            out.print(json);
                            out.flush();
                            audit.AddAuditTrail(sessionId, channel.getChannelid(), operartorObj.getOperatorid(),
                                    request.getRemoteAddr(), channel.getName(), credentialInfo[0], operartorObj.getName(),
                                    new Date(), "Login", "FAILED", FAILED,
                                    "Operator Management", "", message, "OPERATOR", operartorObj.getOperatorid());
                            return;
                        }
                        if (passwordpolicyAttempt == 0) {
                            if (operartorObj.getCurrentAttempts() + 1 == passwordpolicyAttempt) {//locked
                                result = "error";
                                message = "Next wrong attept will lock your account!!!";
                                json.put("_result", result);
                                json.put("_message", message);
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
//                        if (passwordpolicyAttempt == 99) {
//                            
//                        }
                        HttpSession session = request.getSession(true);
                        session.setAttribute("_lastlogintime", opObj.getLastAccessOn());
                        session.setAttribute("_lastLoginOn", "yes");
                    } else {
                        result = "error";
                        message = "Operator Not Found!!!";
                        json.put("_result", result);
                        json.put("_message", message);
                        out.print(json);
                        out.flush();
                        return;
                    }
                } else {

                    String host = chSettings.getExternalhost();
                    String port = String.valueOf(chSettings.getPort());
                    String dn = chSettings.getDatabaseName();
                    String table = chSettings.getTableName();

//                    DirContext context = null;
                    Hashtable env = new Hashtable();
                    String provider_url = "ldap://".concat(host).concat(":").concat((port));
                    env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
                    env.put(Context.SECURITY_AUTHENTICATION, "simple");
                    env.put(Context.SECURITY_PRINCIPAL, _operatorName);
                    env.put(Context.SECURITY_CREDENTIALS, _password);
                    if (chSettings.isSslEnabled() == true) {
                        env.put("java.naming.ldap.factory.socket", "com.mollatech.axiom.common.utils.MySSLSocketFactory");
                    }
                    env.put(Context.PROVIDER_URL, provider_url);
                    try {
                        context = new InitialDirContext(env);
                        if (context != null) {

                        }
                    } catch (NamingException e) {
                        result = "error";
                        message = "LDAP user failed to authenticate.";
                        json.put("_result", result);
                        json.put("_message", message);
                        out.print(json);
                        out.flush();
                        sRemoteAcess.close();
                        suRemoteAcess.close();
                        sChannel.close();
                        suChannel.close();
                        sOperators.close();
                        suOperators.close();

                        audit.AddAuditTrail(sessionId, channel.getChannelid(), operartorObj.getOperatorid(),
                                request.getRemoteAddr(), channel.getName(), credentialInfo[0], operartorObj.getName(),
                                new Date(), "Login", "FAILED", FAILED,
                                "Operator Management", "", message, "OPERATOR", operartorObj.getOperatorid());
                        return;
                    }
                    NamingEnumeration results = null;
                    try {

                        SearchControls controls = new SearchControls();
                        controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
                        String searchFilter = "(&(objectClass=person)((" + table + "=" + _operatorName + ")(userPassword=" + _password + ")))";
                        results = context.search(dn, searchFilter, controls);
                        SearchResult searchResult = (SearchResult) results.next();
                        Attribute cn = null;
                        Attribute mobile = null;
                        Attribute mail = null;
                        if (searchResult != null) {
                            Attributes attributes = searchResult.getAttributes();
                            cn = attributes.get(chSettings.getOperatorname());
                            mobile = attributes.get(chSettings.getMobile());
                            mail = attributes.get(chSettings.getMail());
                        } else {
                            result = "error";
                            message = "LDAP user failed to authenticate.";
                            json.put("_result", result);
                            json.put("_message", message);
                            out.print(json);
                            out.flush();
                            sRemoteAcess.close();
                            suRemoteAcess.close();
                            sChannel.close();
                            suChannel.close();
                            sOperators.close();
                            suOperators.close();
                            audit.AddAuditTrail(sessionId, channel.getChannelid(), operartorObj.getOperatorid(),
                                    request.getRemoteAddr(), channel.getName(), credentialInfo[0], operartorObj.getName(),
                                    new Date(), "Login", "FAILED", FAILED,
                                    "Operator Management", "", message, "OPERATOR", operartorObj.getOperatorid());
                            return;

                        }
                        operartorObj = oManagement.SearchOperators(sessionId, channel.getChannelid(), _operatorName);

//                        operartorObj = oManagement.VerifyCredential(sessionId, channel.getChannelid(), _operatorName, _password);
                        if (operartorObj == null) {
                            result = "error";
                            message = "LDAP Authentication Success but Operator is not yet added locally.";
                            json.put("_result", result);
                            json.put("_message", message);
                            out.print(json);
                            out.flush();
                            sRemoteAcess.close();
                            suRemoteAcess.close();
                            sChannel.close();
                            suChannel.close();
                            sOperators.close();
                            suOperators.close();
                            audit.AddAuditTrail(sessionId, channel.getChannelid(), operartorObj.getOperatorid(),
                                    request.getRemoteAddr(), channel.getName(), credentialInfo[0], operartorObj.getName(),
                                    new Date(), "Login", "FAILED", FAILED,
                                    "Operator Management", "", message, "OPERATOR", operartorObj.getOperatorid());
                            return;

//                            oManagement.AddOperator(sessionId, channel.getChannelid(), _operatorName, _password, mobile.get().toString(), mail.get().toString(), admin, ACTIVE_STATUS, unit);
//                            operartorObj = oManagement.VerifyCredential(sessionId, channel.getChannelid(), _operatorName, _password);
                        } else {
                            boolean flag = false;
                            Operators op1 = new Operators();
                            Operators passwordOpr = new Operators();
                            if (!operartorObj.getName().equalsIgnoreCase(_operatorName)) {
                                flag = true;
                                op1.setName(cn.get().toString());
                            }
                            if (!operartorObj.getEmailid().equalsIgnoreCase(mail.get().toString())) {
                                flag = true;
                                op1.setEmailid(mail.get().toString());
                            }
//                            if (!operartorObj.getPasssword().equalsIgnoreCase(_password)) {
//                                flag = true;
//                                op1.setPasssword(_password);
//                            }
                            if (!operartorObj.getPhone().equalsIgnoreCase(mobile.get().toString())) {
                                flag = true;
                                op1.setPhone(mobile.get().toString());
                            }
                            if (flag) {
                                oManagement.EditOperator(sessionId, channel.getChannelid(), operartorObj.getOperatorid(), operartorObj, op1);
                            }
                        }

                    } catch (Exception ex) {
                        Logger.getLogger(login.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        if (results != null) {
                            try {
                                results.close();
                            } catch (Exception e) {
                                System.out.println("Error : " + e);
                                log.error("Exception caught :: ",e);
                            }
                        }
                        if (context != null) {
                            try {
                                context.close();
                            } catch (Exception e) {
                                //System.out.println("Error : " + e);
                                log.error("Exception caught :: ",e);
                            }
                        }
                    }

                }

                sRemoteAcess.close();
                suRemoteAcess.close();
                sChannel.close();
                suChannel.close();
                sOperators.close();
                suOperators.close();

                if (operartorObj != null) {
                    if (operartorObj.getAccessFrom() != null && operartorObj.getAccessTill() != null) {

                        //start not allow access if access expire
                        if (operartorObj.getRoleid() != 1) {//1 -sysdmin
                            Date currentDate = new Date();
                            Date startDate = operartorObj.getAccessFrom();
                            Date endDate = operartorObj.getAccessTill();

                            boolean bAllow = false;
                            if (currentDate.after(startDate) == true
                                    && currentDate.before(endDate) == true) {
                                bAllow = true;
                            }

                            if (currentDate.compareTo(startDate) == 0
                                    || currentDate.compareTo(endDate) == 0) {

                                bAllow = true;

                            }

//                            Calendar current = Calendar.getInstance();
//                            current.setTime(currentDate);
//                            current.set(Calendar.AM_PM, Calendar.AM);
//                            current.set(Calendar.HOUR, 0);
//                            current.set(Calendar.MINUTE, 0);
//                            current.set(Calendar.SECOND, 0);
//                            current.set(Calendar.MILLISECOND, 0);
//
//                            Calendar current1 = Calendar.getInstance();
//                            current1.setTime(operartorObj.getAccessFrom());
//                            current1.set(Calendar.AM_PM, Calendar.AM);
//                            current1.set(Calendar.HOUR, 0);
//                            current1.set(Calendar.MINUTE, 0);
//                            current1.set(Calendar.SECOND, 0);
//                            current1.set(Calendar.MILLISECOND, 0);
//
//                            Calendar current11 = Calendar.getInstance();
//                            current11.setTime(operartorObj.getAccessFrom());
//                            current11.set(Calendar.AM_PM, Calendar.AM);
//                            current11.set(Calendar.HOUR, 0);
//                            current11.set(Calendar.MINUTE, 0);
//                            current11.set(Calendar.SECOND, 0);
//                            current11.set(Calendar.MILLISECOND, 0);
//
//                            Date today = current.getTime();
//                            Date startDate = current1.getTime();
//                            Date endDate = current11.getTime();
//                            if (!today.equals(startDate) && !today.equals(endDate)) {
//                                if (today.before(startDate) || today.after(endDate)) {
//                                    result = "error";
//                                    message = "Error: Access denied becuase of date and time restriction!!!";
//                                    json.put("_result", result);
//                                    json.put("_message", message);
//                                    out.print(json);
//                                    out.flush();
//                                    return;
//                                }
//                            }
                            if (bAllow == false) {
                                result = "error";
                                message = "Access denied due to date restriction!!!";
                                json.put("_result", result);
                                json.put("_message", message);
                                out.print(json);
                                out.flush();
                                audit.AddAuditTrail(sessionId, channel.getChannelid(), operartorObj.getOperatorid(),
                                        request.getRemoteAddr(), channel.getName(), credentialInfo[0], operartorObj.getName(),
                                        new Date(), "Login", "FAILED", FAILED,
                                        "Operator Management", "", message, "OPERATOR", operartorObj.getOperatorid());
                                return;
                            }

                        }

                        //start not allow access if access expire
//                        if (operartorObj.getRoleid() != 1) {//1 -sysdmin
//                            Date currentDate = new Date();
//                            if (currentDate.before(operartorObj.getAccessFrom())) {
//                                result = "error";
//                                message = "Access duration expired!!!";
//                                json.put("_result", result);
//                                json.put("_message", message);
//                                out.print(json);
//                                out.flush();
//                                return;
//                            }
//                            if (currentDate.after(operartorObj.getAccessTill())) {
//                                result = "error";
//                                message = "Access duration expired!!!";
//                                json.put("_result", result);
//                                json.put("_message", message);
//                                out.print(json);
//                                out.flush();
//                                return;
//                            }
//                        }
                    } //end not allow access if access expire
                    UnitsManagemet unMngt = new UnitsManagemet();
                    int iUnitId = operartorObj.getUnits();
                    Units un = unMngt.getUnitByUnitId(sessionId, channel.getChannelid(), iUnitId);
                    if (un != null) {
                        if (un.getStatus() == unMngt.SUSPEND_STATUS) {
                            result = "error";
                            message = un.getUnitname() + " unit is suspended!!!";
                            json.put("_result", result);
                            json.put("_message", message);
                            out.print(json);
                            out.flush();
                            audit.AddAuditTrail(sessionId, channel.getChannelid(), operartorObj.getOperatorid(),
                                    request.getRemoteAddr(), channel.getName(), credentialInfo[0], operartorObj.getName(),
                                    new Date(), "Login", "FAILED", FAILED,
                                    "Operator Management", "", message, "OPERATOR", operartorObj.getOperatorid());
                            return;
                        }
                    }
                    if (operartorObj.getStatus() != -1) {
                        retValue = 0;
                    } else {
                        retValue = -1;
                    }
                } else {
                    retValue = -1;
                }

                if (operartorObj != null) {
                    if (operartorObj.getStatus() != 1) {    //not active
                        result = "error";
                        //added by vikram
                        if (operartorObj.getStatus() == -1) //locked
                        {
                            message = "Account is locked as many wrong attempts were made!!";
                        } else if (operartorObj.getStatus() == 0) //suspended
                        {
                            message = "Account is suspended!!";
                        } else if (operartorObj.getStatus() == -99) //removed
                        {
                            message = "Account is removed, please contact administrator!!";
                        }
                        //end of addition

                        audit.AddAuditTrail(sessionId, channel.getChannelid(), operartorObj.getOperatorid(),
                                request.getRemoteAddr(), channel.getName(), credentialInfo[0], operartorObj.getName(),
                                new Date(), "Login", message, FAILED,
                                "Operator Management", "", message, "OPERATOR", operartorObj.getOperatorid());

                        url = "index.jsp";
                    }

                    if (passObj != null) {
//                        if (obj instanceof PasswordPolicySetting) {

                        if (passObj.changePasswordAfterFirstlogin == true) {
                            int count = passwordTrail.getPassword(channel.getChannelid(), operartorObj.getOperatorid());
//                                 count =1;
                            if (operartorObj.getChangePassword() == OperatorsManagement.CHANGE_PASSWORD_STATUS) {
                                //if (count == 1) {
                                result = "1sLogin";
                                message = "First time user, please change password now!!!";
                                json.put("_result", result);
                                json.put("_message", message);
                                json.put("_url", "#ChangePassword");
                                HttpSession session = request.getSession(true);
                                session.setAttribute("_apOprAuth", "yes");
                                session.setAttribute("_apOprDetail", operartorObj);
                                session.setAttribute("_apSessionID", sessionId);
                                session.setAttribute("_apSChannelDetails", channel);
                                session.setAttribute("_apOprDetail", operartorObj);
//                                        json.put("_val", 1);//for change password for 
                                out.print(json);
                                out.flush();
                                return;
                            }

                        }
                        if (operartorObj.getRoleid() != 0) {
                            if (passObj.passwordExpiryTime != 99) {//password never expire
                                Date passupdateDate = operartorObj.getPasswordupdatedOn();
                                Calendar current = Calendar.getInstance();
                                Calendar current1 = Calendar.getInstance();
                                if (passupdateDate != null) {
                                    current.setTime(passupdateDate);
                                    current.add(Calendar.DATE, passObj.passwordExpiryTime);
                                    Date d = new Date();
                                    current1.setTime(d);
                                    Date currentDate = current1.getTime();

                                    Date expireDate = current.getTime();
                                    if (currentDate.after(expireDate)) {
                                        result = "1sLogin";
                                        message = "Password Expired!!!,Please change password now!!!";
                                        json.put("_result", result);
                                        json.put("_message", message);
                                        json.put("_url", "#ChangePassword");
                                        HttpSession session = request.getSession(true);
                                        session.setAttribute("_apOprAuth", "yes");
                                        session.setAttribute("_apOprDetail", operartorObj);
                                        session.setAttribute("_apSessionID", sessionId);
                                        session.setAttribute("_apSChannelDetails", channel);
                                        session.setAttribute("_apOprDetail", operartorObj);
//                                        json.put("_val", 1);//for change password for 
                                        out.print(json);
                                        out.flush();
                                        return;
                                    }
                                }

                            }
                        }

                    }

                } else {
                    if (passObj.passwordAlert == true) {
                        TemplateManagement tMngt = new TemplateManagement();
                        Templates templatesObj = tMngt.LoadTemplate(sessionId, channel.getChannelid(), passObj.passwordAlertTemplateID);
//                            Templates templatesObj = tUtil.loadbyName( Templates templatesObj, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        if (templatesObj.getStatus() == tMngt.ACTIVE_STATUS) {
                            ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                            String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                            String strsubject = templatesObj.getSubject();
                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                            if (strmessageBody != null) {
                                // Date date = new Date();
                                strmessageBody = strmessageBody.replaceAll("#name#", opObj.getName());
                                strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                strmessageBody = strmessageBody.replaceAll("#email#", opObj.getEmailid());
                                strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                strmessageBody = strmessageBody.replaceAll("#filterword#", request.getRemoteAddr());
                            }
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }

                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }
                        }
                    }
                }

                if (retValue == 0) {
                    message = "successful credential verification....!!! ";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(),
                            operartorObj.getOperatorid(),
                            request.getRemoteAddr(), channel.getName(), credentialInfo[0], operartorObj.getName(),
                            new Date(),
                            "Login",
                            "SUCCESS", 0,
                            "Operator Management", "", message, "OPERATOR", operartorObj.getOperatorid());

                    HttpSession session = request.getSession(true);
                    session.setAttribute("_apOprAuth", "yes");
                    session.setAttribute("_apOprDetail", operartorObj);
                    session.setAttribute("_apSessionID", sessionId);
                    session.setAttribute("_apSChannelDetails", channel);

                    //ashish
                    if (operartorObj.getAccessType() == OperatorsManagement.ROLE_ACCESS) { //role
                        Roles roleObj = new OperatorsManagement().getRoleByRoleId(channel.getChannelid(), operartorObj.getRoleid());
                        byte[] accessByte = roleObj.getAccessentry();
                        AccessMatrixSettings accessSettings = null;
                        if (accessByte != null) {
                            ByteArrayInputStream bais = new ByteArrayInputStream(AxiomProtect.AccessDataBytes(accessByte));
                            Object object = UtilityFunctions.deserializeFromObject(bais);

                            if (object != null) {
                                accessSettings = (AccessMatrixSettings) object;
                            }
                        }

                        if (accessSettings == null) {
                            accessSettings = new AccessMatrixSettings();
                        }
                        session.setAttribute("_apAccessEntry", accessSettings);
                    } else {
                        byte[] accessByte = operartorObj.getAccessentry();
                        AccessMatrixSettings accessSettings = null;
                        if (accessByte != null) {
                            ByteArrayInputStream bais = new ByteArrayInputStream(AxiomProtect.AccessDataBytes(accessByte));
                            Object object = UtilityFunctions.deserializeFromObject(bais);

                            if (object != null) {
                                accessSettings = (AccessMatrixSettings) object;
                            }
                        }

                        if (accessSettings == null) {
                            accessSettings = new AccessMatrixSettings();
                        }
                        session.setAttribute("_apAccessEntry", accessSettings);
                    }

                } else {
                    HttpSession session = request.getSession(true);
                    session.setAttribute("_apOprAuth", null);
                    if (operartorObj != null) {
                        audit.AddAuditTrail(sessionId, channel.getChannelid(),
                                operartorObj.getOperatorid(),
                                request.getRemoteAddr(), channel.getName(), credentialInfo[0], operartorObj.getName(),
                                new Date(),
                                "Login",
                                "ERROR",
                                -1,
                                "Login Attempt",
                                "",
                                "Login Failed",
                                "LOGIN",
                                operartorObj.getOperatorid());
                    }
                }

            } catch (Exception ex) {
                // TODO handle custom exceptions here
                log.error("Exception caught :: ",ex);
            }

            if (retValue != 0) {
                result = "error";
                if (operartorObj != null && operartorObj.getStatus() != -1) { //active status to request password
                    message = "Invalid Credentials!!! Click here to <a href=\"#\" onclick=\"forgotpassword()\" >Get Password</a>!!!";
                } else {
                    message = "Invalid Credentials!!!";
                }
                url = "index.jsp";
            }

            try {
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
            } finally {
                out.print(json);
                out.flush();
            }
        } catch (Exception ex) {
            // TODO handle custom exceptions here
            log.error("Exception caught :: ",ex);
            String result = "error";
            String message = ex.getLocalizedMessage();
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
            }
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
