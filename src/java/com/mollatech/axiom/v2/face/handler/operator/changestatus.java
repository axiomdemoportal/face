/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.operator;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import org.json.JSONException;
import org.json.JSONObject;

public class changestatus extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(changestatus.class.getName());
    
   final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;
    final int LOCKED_STATUS = -1;
    final int REMOVE_STATUS = -99;
    final String itemTypeOp = "OPERATOR";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());

        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("channel :: "+channel.getName());
        String _op_status =request.getParameter("_op_status");
       
        String _operId =request.getParameter("_oprid");
        log.debug("_operId :: "+_operId);
        
         //audit
         Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
         log.debug("operator :: "+operator.getOperatorid());
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID"); 
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        String _old_op_status = request.getParameter("_op_status");
        log.debug("_old_op_status :: "+_old_op_status);

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Status Update Successful!!!";

        int retValue = 0;
        int status =Integer.parseInt(_op_status);
         log.debug("channel :: "+status);
        String _value = "Active";
        if ( status == 0 ) {
            _value = "Suspended";
        }
      
        OperatorsManagement oManagement = new OperatorsManagement();   
         AuditManagement audit = new AuditManagement();
         Operators oldOpObj = oManagement.getOperatorById(channel.getChannelid(), _operId);
        retValue = oManagement.ChangeStatus(sessionId, channel.getChannelid(), _operId,status);
        log.debug("ChangeStatus :: "+retValue);
         
       int istatus =  oldOpObj.getStatus(); 
       log.debug("istatus :: "+istatus);
       String strStatus = "";
       if(istatus == ACTIVE_STATUS){
           strStatus = "ACTIVE_STATUS";
       }else if(istatus == LOCKED_STATUS){
           strStatus = "LOCKED_STATUS";
       }else if(istatus == REMOVE_STATUS){
           strStatus = "REMOVE_STATUS";
       }else if(istatus == SUSPEND_STATUS){
           strStatus = "SUSPEND_STATUS";
       }
        String strNewStatus = "";
       if(status == ACTIVE_STATUS){
           strNewStatus = "ACTIVE_STATUS";
       }else if(status == LOCKED_STATUS){
           strNewStatus = "LOCKED_STATUS";
       }else if(status == REMOVE_STATUS){
           strNewStatus = "REMOVE_STATUS";
       }else if(status == SUSPEND_STATUS){
           strNewStatus = "SUSPEND_STATUS";
       }
      
        String resultString="ERROR";
        
        if(retValue == 0){
            resultString ="SUCCESS";
             audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                     request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                        "Change Status", resultString, retValue, "Operator Management",
                        "Old Status="+strStatus,"New Status ="+strNewStatus,
                        itemTypeOp, 
                        _operId);
        }else  {
               audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                       request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                        "Change Status", resultString, retValue, "Operator Management",
                        "Old Status="+strStatus,"Failed To Change Status",
                        itemTypeOp, 
                        _operId);
            result = "error";
            message = "Status Update Failed!!!";
         }
        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);
        } catch (Exception ex) {
            Logger.getLogger(resendpassword.class.getName()).log(Level.SEVERE, null, ex);
            log.error("exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
        }
        
        log.info("is ended :: ");

    }
     // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
