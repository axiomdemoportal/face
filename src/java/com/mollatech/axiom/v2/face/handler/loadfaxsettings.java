/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.FaxSetting;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class loadfaxsettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadfaxsettings.class.getName());

    private JSONObject SettingsWhenEmpty(int _type1, int _preference1, Object settingsObj) {
        JSONObject json = new JSONObject();
        if (_type1 == editsettings.FAX && _preference1 == editsettings.PREFERENCE_ONE) {
            try { json.put("_className", "");
            json.put("_ip", "");
            json.put("_port", "");
            json.put("_userId", "");
            json.put("_password", "");
            json.put("_phoneNumber", "");
            json.put("_reserve1", "");
            json.put("_reserve2", "");
            json.put("_reserve3", "");
            json.put("_status", "Disabled");
            json.put("_logConfirmation", true);
            json.put("_autofailover", 0);
            json.put("_retries", 2);
            json.put("_retryduration", 10);
            }catch(Exception e){log.error("Exception caught :: ",e);}
        } else if (_type1 == editsettings.FAX && _preference1 == editsettings.PREFERENCE_TWO) {
            try { json.put("_classNameS", "");
            json.put("_ipS", "");
            json.put("_portS", "");
            json.put("_userIdS", "");
            json.put("_passwordS", "");
            json.put("_phoneNumberS", "");
            json.put("_reserve1S", "");
            json.put("_reserve2S", "");
            json.put("_reserve3S", "");
            json.put("_statusS", "Disabled");
            json.put("_logConfirmationS", true);
            json.put("_retriesS", 2);
            json.put("_retrydurationS", 10);
            }catch(Exception e){log.error("Exception caught :: ",e);}
        }
        return json;
    }

    private JSONObject SettingsWhenPresent(Object settingsObj) {
        JSONObject json = new JSONObject();
        if (settingsObj instanceof FaxSetting) {
            FaxSetting fax = (FaxSetting) settingsObj;
            if (fax.getPreference() == 1) {
                try { json.put("_className", fax.getClassName());
                json.put("_ip", fax.getIp());
                json.put("_port", fax.getPort());
                json.put("_userId", fax.getUserid());
                json.put("_password", fax.getPassword());
                json.put("_phoneNumber", fax.getPhoneNumber());
                json.put("_reserve1", fax.getReserve1());
                json.put("_reserve2", fax.getReserve2());
                json.put("_reserve3", fax.getReserve3());
                json.put("_status", fax.getStatus());
                json.put("_logConfirmation", fax.isLogConfirmation());
                json.put("_retries", fax.getRetrycount());
                json.put("_retryduration", fax.getRetryduration());
                json.put("_autofailover", fax.getAutofailover());
                }catch(Exception e){log.error("Exception caught :: ",e);}
            } else {
                try { json.put("_classNameS", fax.getClassName());
                json.put("_ipS", fax.getIp());
                json.put("_portS", fax.getPort());
                json.put("_userIdS", fax.getUserid());
                json.put("_passwordS", fax.getPassword());
                json.put("_phoneNumberS", fax.getPhoneNumber());
                json.put("_reserve1S", fax.getReserve1());
                json.put("_reserve2S", fax.getReserve2());
                json.put("_reserve3S", fax.getReserve3());
                json.put("_statusS", fax.getStatus());
                json.put("_logConfirmationS", fax.isLogConfirmation());
                json.put("_retriesS", fax.getRetrycount());
                json.put("_retrydurationS", fax.getRetryduration());
                }catch(Exception e){log.error("Exception caught :: ",e);}
            }
        }
        return json;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
//        String _type = request.getParameter("_type");
        String _preference = request.getParameter("_preference");
        int _type1 = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.FAX_SETTING;
        int _preference1 = Integer.parseInt(_preference);
        
        log.debug("channel is::"+channel.getName());
        log.debug("sessionid::"+sessionId);
        log.debug("preference is::"+_preference1);
        
        JSONObject json = null;//new JSONObject();
        PrintWriter out = response.getWriter();

        try {
            SettingsManagement sMngmt = new SettingsManagement();
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _type1, _preference1);
            if (settingsObj != null) {
                json = SettingsWhenPresent(settingsObj);
            } else {
                json = SettingsWhenEmpty(_type1, _preference1, settingsObj);
            }

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
            // TODO handle custom exceptions here
        }
        try {
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet started");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
