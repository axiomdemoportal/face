/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Billmanager;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Otptokens;
import com.mollatech.axiom.nucleus.db.Remoteaccess;
import com.mollatech.axiom.nucleus.db.Sessions;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.BillingManagment;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.BillingManagerSettings;
import com.mollatech.axiom.nucleus.settings.PasswordPolicySetting;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import com.mollatech.axiom.nucleus.settings.TokenSettings;
import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.hibernate.Session;

/**
 * Web application lifecycle listener.
 *
 * @author nilesh
 */
public class AxiomListner implements ServletContextListener {

    private static String channelid;
    private static Channels channel;
    private static int intervalCall;
    private static Timer timer;
    private static TokenSettings tokenObj = null;
    private static OTPTokenManagement otpObj = null;
    private static PasswordPolicySetting passwordSetting = null;
    private static OperatorsManagement oprObj = null;
    private static ChannelProfile channelprofileObj = null;
    private static RemoteAccessUtils rUtil = null;
    private static SessionManagement sessionObj = null;
    private static BillingManagerSettings billsetObj = null;
    private static BillingManagment billMngt = null;
    private static UserManagement userMngt = null;
    private static TemplateManagement templateMngt = null;
    private static SendNotification sendMngt = null;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAcess = suRemoteAcess.openSession();
        rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
        ServletContext ch = sce.getServletContext();
        String _channelName = ch.getContextPath();
        //System.out.println("Channel Core Name >>" + _channelName);
        //_channelName = "/ibank2core";
        _channelName = _channelName.replaceAll("/", "");
        if (_channelName.compareTo("core") != 0) {
            _channelName = _channelName.replaceAll("core", "");
        } else {
            _channelName = "face";
        }

        channel = cUtil.getChannel(_channelName);
        if (channel == null) {
            System.out.println("Channel Details could not be found>>" + _channelName);
            return;
        }
        channelid = channel.getChannelid();
        SettingsManagement setObj = new SettingsManagement();

        Object tokenobj = setObj.getSetting(channelid, SettingsManagement.Token, SettingsManagement.PREFERENCE_ONE);

        if (tokenobj != null) {
            tokenObj = (TokenSettings) tokenobj;
        }

        Object psswordobj = setObj.getSetting(channelid, SettingsManagement.PASSWORD_POLICY_SETTING, SettingsManagement.PREFERENCE_ONE);

        if (psswordobj != null) {
            passwordSetting = (PasswordPolicySetting) psswordobj;
        }

        // ChannelProfile channelprofileObj = null;
        Object channelpobj = setObj.getSetting(channel.getChannelid(), SettingsManagement.CHANNELPROFILE_SETTING, 1);

        if (channelpobj != null) {
            channelprofileObj = (ChannelProfile) channelpobj;
        }

        // Billing Manager
        Object billObj = setObj.getSetting(channel.getChannelid(), SettingsManagement.BILLING_MANAGER_SETTING, SettingsManagement.PREFERENCE_ONE);

        if (billObj != null) {
            billsetObj = (BillingManagerSettings) billObj;
        }

        otpObj = new OTPTokenManagement(channelid);
        oprObj = new OperatorsManagement();
        sessionObj = new SessionManagement();
        billMngt = new BillingManagment();
        userMngt = new UserManagement();
        templateMngt = new TemplateManagement();
        sendMngt = new SendNotification();
        String interval = LoadSettings.g_sSettings.getProperty("connector.status.check.time");
        if (interval != null) {
            intervalCall = new Integer(interval).intValue();
        } else {
            intervalCall = 300;
        }
        try {
            timer = new Timer();

            timer.schedule(new TimerTask() {
                public void run() {
                    String active = LoadSettings.g_sSettings.getProperty("reserved.1");
                    if (active == null || active.isEmpty() == true) {
                        System.out.println("system is not active yet!!!");
                    } else {
                        try {
                            Calendar current = null;
                            Date endDate = null;
                            Date lastaccess = new Date();
                            if (tokenObj != null) {
                                current = Calendar.getInstance();
                                current.setTime(lastaccess);
                                current.set(Calendar.AM_PM, Calendar.AM);
                                current.set(Calendar.HOUR, 0);
                                current.set(Calendar.HOUR, 0);
                                current.set(Calendar.MINUTE, 0);
                                current.set(Calendar.SECOND, 0);
                                current.set(Calendar.MILLISECOND, 0);
                                current.add(Calendar.DAY_OF_YEAR, -tokenObj.getOtpLockAfterXDays());
                                endDate = current.getTime();
                                current = null;
                                Otptokens[] arrOtpTokens = otpObj.getOtpObjByDuration(channelid, endDate);
                                endDate = null;
                                lastaccess = null;
                                if (arrOtpTokens != null) {
                                    for (int i = 0; i < arrOtpTokens.length; i++) {
                                        otpObj.ChangeStatus(channelid, arrOtpTokens[i].getUserid(), OTPTokenManagement.TOKEN_STATUS_SUSPENDED, arrOtpTokens[i].getCategory(), arrOtpTokens[i].getSubcategory());
                                    }
                                };
                            }

                            if (passwordSetting != null) {
                                lastaccess = new Date();
                                current = Calendar.getInstance();
                                current.setTime(lastaccess);
                                current.set(Calendar.AM_PM, Calendar.AM);
                                current.set(Calendar.HOUR, 0);
                                current.set(Calendar.HOUR, 0);
                                current.set(Calendar.MINUTE, 0);
                                current.set(Calendar.SECOND, 0);
                                current.set(Calendar.MILLISECOND, 0);
                                current.add(Calendar.DAY_OF_YEAR, -passwordSetting.passwordExpiryTime);
                                endDate = current.getTime();
                                Operators[] arrOpr = oprObj.getAllExpireOperators(channelid, endDate);
                                if (arrOpr != null) {
                                    for (int i = 0; i < arrOpr.length; i++) {
                                        if (arrOpr[i].getRoleid() != 1) {// role id 1 = sysadmin
                                            oprObj.ChangeOperatorStatus(channelid, arrOpr[i].getOperatorid(), OperatorsManagement.SUSPEND_STATUS);
                                        }
                                    }
                                }

                                //disable operator if not in use 
                                lastaccess = new Date();
                                current = Calendar.getInstance();
                                current.setTime(lastaccess);
                                current.set(Calendar.AM_PM, Calendar.AM);
                                current.set(Calendar.HOUR, 0);
                                current.set(Calendar.HOUR, 0);
                                current.set(Calendar.MINUTE, 0);
                                current.set(Calendar.SECOND, 0);
                                current.set(Calendar.MILLISECOND, 0);
                                current.add(Calendar.MONTH, -passwordSetting.enforcementDormancytime);
                                endDate = current.getTime();
                                arrOpr = null;
                                arrOpr = oprObj.getAllNotInUseOperators(channelid, endDate);
                                if (arrOpr != null) {
                                    for (int i = 0; i < arrOpr.length; i++) {
                                        if (arrOpr[i].getRoleid() != 1) {// role id 1 = sysadmin
                                            oprObj.ChangeOperatorStatus(channelid, arrOpr[i].getOperatorid(), OperatorsManagement.SUSPEND_STATUS);
                                        }
                                    }
                                }
                            }

                            if (channelprofileObj != null) {
                                if (channelprofileObj._multipleSession == 0) {//0 - inactive 
                                    Remoteaccess rObj = rUtil.getRemoteaccess4Channel(channelid);
                                    if (rObj == null) {
                                        return;
                                    }
                                    lastaccess = new Date();
                                    current = Calendar.getInstance();
                                    current.setTime(lastaccess);
                                    current.set(Calendar.AM_PM, Calendar.AM);
                                    current.set(Calendar.HOUR, 0);
                                    current.set(Calendar.HOUR, 0);
                                    current.add(Calendar.MINUTE, -rObj.getExpiryInMins());
                                    current.set(Calendar.SECOND, 0);
                                    current.set(Calendar.MILLISECOND, 0);
                                    endDate = current.getTime();
                                    Sessions[] arrSessions = sessionObj.getSessionByDuration(channelid, endDate);
                                    if (arrSessions != null) {
                                        for (int i = 0; i < arrSessions.length; i++) {
                                            sessionObj.CloseSession(arrSessions[i].getSessionid());
                                        }
                                    }

                                }

                            }
                            if (billsetObj != null) {
                                if (billsetObj.alert == true) {//0 - inactive 
                                    if (billsetObj.ballowBillingManagerSWOTP == true) {  // SW OTP Token
                                        if (billsetObj.iselectBillTypeSWOTP == 2) {//2- subscription
                                            lastaccess = new Date();
                                            current = Calendar.getInstance();
                                            current.setTime(lastaccess);
                                            current.set(Calendar.AM_PM, Calendar.AM);
                                            current.set(Calendar.HOUR, 0);
                                            current.set(Calendar.HOUR, 0);
                                            current.set(Calendar.MINUTE, 0);
                                            current.set(Calendar.SECOND, 0);
                                            current.set(Calendar.MILLISECOND, 0);
                                            current.add(Calendar.MONTH, -billsetObj.iselectBillTypeSWOTP);
                                            endDate = current.getTime();
                                            current = Calendar.getInstance();
                                            current.setTime(endDate);
                                            current.add(Calendar.WEEK_OF_MONTH, -billsetObj.iselectAlertBeforeSWOTP);
                                            endDate = current.getTime();
                                            Billmanager[] arrBill = billMngt.getBillRecordForDuration(channelid, BillingManagment.SOFTWARE_TOKEN, endDate);
                                            if (arrBill != null) {
                                                for (int i = 0; i < arrBill.length; i++) {
                                                    try {
                                                         billMngt.updateBillRecord(channelid, arrBill[i].getBillid());
                                                        AuthUser userObj = userMngt.getUser(channelid, arrBill[i].getUserid());
                                                        if (userObj.getEmail() != null) {
                                                            Templates templateObj = templateMngt.LoadbyName(channelid, TemplateNames.EMAIL_EDIT_USER_TEMPLATE);
                                                            if (templateObj != null) {
                                                                ByteArrayInputStream bais = null;
                                                                String message = null;
                                                                String subject = null;
                                                                bais = new ByteArrayInputStream(templateObj.getTemplatebody());
                                                                message = (String) TemplateUtils.deserializeFromObject(bais);

                                                                message = message.replaceAll("#name#", userObj.getUserName());
                                                                message = message.replaceAll("#channel#", channel.getName());
                                                                message = message.replaceAll("#datetime#", sdf.format(new Date()));
                                                                sendMngt.SendEmail(channelid,
                                                                        userObj.email, subject, message, null, null,
                                                                        null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));

                                                            }
                                                        }
                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (billsetObj.ballowBillingManagerHWOTP == true) {  // HW OTP Token
                                        if (billsetObj.iselectBillTypeHWOTP == 2) {//2- subscription
                                            lastaccess = new Date();
                                            current = Calendar.getInstance();
                                            current.setTime(lastaccess);
                                            current.set(Calendar.AM_PM, Calendar.AM);
                                            current.set(Calendar.HOUR, 0);
                                            current.set(Calendar.HOUR, 0);
                                            current.set(Calendar.MINUTE, 0);
                                            current.set(Calendar.SECOND, 0);
                                            current.set(Calendar.MILLISECOND, 0);
                                            current.add(Calendar.MONTH, -billsetObj.iselectBillTypeHWOTP);
                                            endDate = current.getTime();
                                            current = Calendar.getInstance();
                                            current.setTime(endDate);
                                            current.add(Calendar.WEEK_OF_MONTH, -billsetObj.iselectAlertBeforeHWOTP);
                                            endDate = current.getTime();
                                            Billmanager[] arrBill = billMngt.getBillRecordForDuration(channelid, BillingManagment.HARDWARE_TOKEN, endDate);
                                            if (arrBill != null) {
                                                for (int i = 0; i < arrBill.length; i++) {
                                                    try {
                                                        billMngt.updateBillRecord(channelid, arrBill[i].getBillid());
                                                        AuthUser userObj = userMngt.getUser(channelid, arrBill[i].getUserid());
                                                        if (userObj.getEmail() != null) {
                                                            Templates templateObj = templateMngt.LoadbyName(channelid, TemplateNames.EMAIL_EDIT_USER_TEMPLATE);
                                                            if (templateObj != null) {
                                                                ByteArrayInputStream bais = null;
                                                                String message = null;
                                                                String subject = null;
                                                                bais = new ByteArrayInputStream(templateObj.getTemplatebody());
                                                                message = (String) TemplateUtils.deserializeFromObject(bais);

                                                                message = message.replaceAll("#name#", userObj.getUserName());
                                                                message = message.replaceAll("#channel#", channel.getName());
                                                                message = message.replaceAll("#datetime#", sdf.format(new Date()));
                                                                sendMngt.SendEmail(channelid,
                                                                        userObj.email, subject, message, null, null,
                                                                        null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));

                                                            }
                                                        }
                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (billsetObj.ballowBillingManagerSWPKIOTP == true) {  // SW PKI Token
                                        if (billsetObj.iselectBillTypeSWPKIOTP == 2) {//2- subscription
                                            lastaccess = new Date();
                                            current = Calendar.getInstance();
                                            current.setTime(lastaccess);
                                            current.set(Calendar.AM_PM, Calendar.AM);
                                            current.set(Calendar.HOUR, 0);
                                            current.set(Calendar.HOUR, 0);
                                            current.set(Calendar.MINUTE, 0);
                                            current.set(Calendar.SECOND, 0);
                                            current.set(Calendar.MILLISECOND, 0);
                                            current.add(Calendar.MONTH, -billsetObj.iselectBillTypeSWPKIOTP);
                                            endDate = current.getTime();
                                            current = Calendar.getInstance();
                                            current.setTime(endDate);
                                            current.add(Calendar.WEEK_OF_MONTH, -billsetObj.iselectAlertBeforeSWPKIOTP);
                                            endDate = current.getTime();
                                            Billmanager[] arrBill = billMngt.getBillRecordForDuration(channelid, BillingManagment.SW_PKI_TOKEN, endDate);
                                            if (arrBill != null) {
                                                for (int i = 0; i < arrBill.length; i++) {
                                                    try {
                                                        billMngt.updateBillRecord(channelid, arrBill[i].getBillid()); 
                                                        AuthUser userObj = userMngt.getUser(channelid, arrBill[i].getUserid());
                                                        if (userObj.getEmail() != null) {
                                                            Templates templateObj = templateMngt.LoadbyName(channelid, TemplateNames.EMAIL_EDIT_USER_TEMPLATE);
                                                            if (templateObj != null) {
                                                                ByteArrayInputStream bais = null;
                                                                String message = null;
                                                                String subject = null;
                                                                bais = new ByteArrayInputStream(templateObj.getTemplatebody());
                                                                message = (String) TemplateUtils.deserializeFromObject(bais);

                                                                message = message.replaceAll("#name#", userObj.getUserName());
                                                                message = message.replaceAll("#channel#", channel.getName());
                                                                message = message.replaceAll("#datetime#", sdf.format(new Date()));
                                                                sendMngt.SendEmail(channelid,
                                                                        userObj.email, subject, message, null, null,
                                                                        null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));

                                                            }
                                                        }
                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (billsetObj.ballowBillingManagerHWPKIOTP == true) {  // HW PKI Token
                                        if (billsetObj.iselectBillTypeHWPKIOTP == 2) {//2- subscription
                                            lastaccess = new Date();
                                            current = Calendar.getInstance();
                                            current.setTime(lastaccess);
                                            current.set(Calendar.AM_PM, Calendar.AM);
                                            current.set(Calendar.HOUR, 0);
                                            current.set(Calendar.HOUR, 0);
                                            current.set(Calendar.MINUTE, 0);
                                            current.set(Calendar.SECOND, 0);
                                            current.set(Calendar.MILLISECOND, 0);
                                            current.add(Calendar.MONTH, -billsetObj.iselectBillTypeHWPKIOTP);
                                            endDate = current.getTime();
                                            current = Calendar.getInstance();
                                            current.setTime(endDate);
                                            current.add(Calendar.WEEK_OF_MONTH, -billsetObj.iselectAlertBeforeHWPKIOTP);
                                            endDate = current.getTime();
                                            Billmanager[] arrBill = billMngt.getBillRecordForDuration(channelid, BillingManagment.HW_PKI_TOKEN, endDate);
                                            if (arrBill != null) {
                                                for (int i = 0; i < arrBill.length; i++) {
                                                    try {
                                                         billMngt.updateBillRecord(channelid, arrBill[i].getBillid());
                                                        AuthUser userObj = userMngt.getUser(channelid, arrBill[i].getUserid());
                                                        if (userObj.getEmail() != null) {
                                                            Templates templateObj = templateMngt.LoadbyName(channelid, TemplateNames.EMAIL_EDIT_USER_TEMPLATE);
                                                            if (templateObj != null) {
                                                                ByteArrayInputStream bais = null;
                                                                String message = null;
                                                                String subject = null;
                                                                bais = new ByteArrayInputStream(templateObj.getTemplatebody());
                                                                message = (String) TemplateUtils.deserializeFromObject(bais);

                                                                message = message.replaceAll("#name#", userObj.getUserName());
                                                                message = message.replaceAll("#channel#", channel.getName());
                                                                message = message.replaceAll("#datetime#", sdf.format(new Date()));
                                                                sendMngt.SendEmail(channelid,
                                                                        userObj.email, subject, message, null, null,
                                                                        null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));

                                                            }
                                                        }
                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (billsetObj.ballowBillingManagerCertificate == true) {  // HW PKI Token
                                        if (billsetObj.iselectBillTypeCertificate == 2) {//2- subscription
                                            lastaccess = new Date();
                                            current = Calendar.getInstance();
                                            current.setTime(lastaccess);
                                            current.set(Calendar.AM_PM, Calendar.AM);
                                            current.set(Calendar.HOUR, 0);
                                            current.set(Calendar.HOUR, 0);
                                            current.set(Calendar.MINUTE, 0);
                                            current.set(Calendar.SECOND, 0);
                                            current.set(Calendar.MILLISECOND, 0);
                                            current.add(Calendar.MONTH, -billsetObj.iselectBillTypeCertificate);
                                            endDate = current.getTime();
                                            current = Calendar.getInstance();
                                            current.setTime(endDate);
                                            current.add(Calendar.WEEK_OF_MONTH, -billsetObj.iselectAlertBeforeCertificate);
                                            endDate = current.getTime();
                                            Billmanager[] arrBill = billMngt.getBillRecordForDuration(channelid, BillingManagment.CERTIFICATE, endDate);
                                            if (arrBill != null) {
                                                for (int i = 0; i < arrBill.length; i++) {
                                                    try {
                                                         billMngt.updateBillRecord(channelid, arrBill[i].getBillid());
                                                        AuthUser userObj = userMngt.getUser(channelid, arrBill[i].getUserid());
                                                        if (userObj.getEmail() != null) {
                                                            Templates templateObj = templateMngt.LoadbyName(channelid, TemplateNames.EMAIL_EDIT_USER_TEMPLATE);
                                                            if (templateObj != null) {
                                                                ByteArrayInputStream bais = null;
                                                                String message = null;
                                                                String subject = null;
                                                                bais = new ByteArrayInputStream(templateObj.getTemplatebody());
                                                                message = (String) TemplateUtils.deserializeFromObject(bais);

                                                                message = message.replaceAll("#name#", userObj.getUserName());
                                                                message = message.replaceAll("#channel#", channel.getName());
                                                                message = message.replaceAll("#datetime#", sdf.format(new Date()));
                                                                sendMngt.SendEmail(channelid,
                                                                        userObj.email, subject, message, null, null,
                                                                        null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));

                                                            }
                                                        }
                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (billsetObj.ballowBillingManagerOOBOTP == true) {  // HW PKI Token
                                        if (billsetObj.iselectBillTypeOOBOTP == 2) {//2- subscription
                                            lastaccess = new Date();
                                            current = Calendar.getInstance();
                                            current.setTime(lastaccess);
                                            current.set(Calendar.AM_PM, Calendar.AM);
                                            current.set(Calendar.HOUR, 0);
                                            current.set(Calendar.HOUR, 0);
                                            current.set(Calendar.MINUTE, 0);
                                            current.set(Calendar.SECOND, 0);
                                            current.set(Calendar.MILLISECOND, 0);
                                            current.add(Calendar.MONTH, -billsetObj.iselectBillTypeOOBOTP);
                                            endDate = current.getTime();
                                            current = Calendar.getInstance();
                                            current.setTime(endDate);
                                            current.add(Calendar.WEEK_OF_MONTH, -billsetObj.iselectAlertBeforeOOBOTP);
                                            endDate = current.getTime();
                                            Billmanager[] arrBill = billMngt.getBillRecordForDuration(channelid, BillingManagment.OOB_TOKEN, endDate);
                                            if (arrBill != null) {
                                                for (int i = 0; i < arrBill.length; i++) {
                                                    try {
                                                         billMngt.updateBillRecord(channelid, arrBill[i].getBillid());
                                                        AuthUser userObj = userMngt.getUser(channelid, arrBill[i].getUserid());
                                                        if (userObj.getEmail() != null) {
                                                            Templates templateObj = templateMngt.LoadbyName(channelid, TemplateNames.EMAIL_EDIT_USER_TEMPLATE);
                                                            if (templateObj != null) {
                                                                ByteArrayInputStream bais = null;
                                                                String message = null;
                                                                String subject = null;
                                                                bais = new ByteArrayInputStream(templateObj.getTemplatebody());
                                                                message = (String) TemplateUtils.deserializeFromObject(bais);

                                                                message = message.replaceAll("#name#", userObj.getUserName());
                                                                message = message.replaceAll("#channel#", channel.getName());
                                                                message = message.replaceAll("#datetime#", sdf.format(new Date()));
                                                                sendMngt.SendEmail(channelid,
                                                                        userObj.email, subject, message, null, null,
                                                                        null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));

                                                            }
                                                        }
                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, (60000), (intervalCall * 1000));
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            sChannel.close();
            suChannel.close();
            suRemoteAcess.close();
            sRemoteAcess.close();
        }

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        try {
            timer.purge();
            timer.cancel();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
