/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.otptokens;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Otptokens;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import com.mollatech.axiom.nucleus.db.operation.TokenLicenseDetails;
import com.mollatech.axiom.nucleus.db.operation.XmlParser;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class HardwareLicenseFileUpload extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(HardwareLicenseFileUpload.class.getName());

    final String itemType = "HOTPUPLOAD";
    final String itemTypeOp = "OPERATOR";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        log.info("is started :: ");
        PrintWriter out = response.getWriter();
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());
        String password = request.getParameter("_password");
        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_HARDWARE) != 0) {
            String result = "error";
            String message = "This feature is not available in this license!!!";
            JSONObject json = new JSONObject();
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        String strError = "";
        //String saveFile = "";
        String savepath = "";
        int failed = 0;
        int success = 0;
        String result = "success";

        String message = "File upload successful, " + success + " entries added and " + failed + " entries failed!!!";
        //  String counter  = "0";

        JSONObject json = new JSONObject();

        savepath = System.getProperty("catalina.home");
        if (savepath == null) {
            savepath = System.getenv("catalina.home");
        }
        savepath += System.getProperty("file.separator");
        savepath += "axiomv2-settings";
        savepath += System.getProperty("file.separator");
        savepath += "uploads";
        savepath += System.getProperty("file.separator");

        String strUniqueID = new String(request.getSession().getId());
        strUniqueID = strUniqueID.substring(0, 8);

        String optionalFileName = "";
        FileItem fileItem = null;
        String[] files = new String[1];	 // file names
        String dirName = savepath;
        int retValue = 0;

        int i = 0;

        AuditManagement audit = new AuditManagement();
        String xmlFileName = null;
        String passwordFileName = null;
        xmlFileName = (String) request.getSession().getAttribute("_otpXmlFliePath");
        passwordFileName = (String) request.getSession().getAttribute("_otpPasswordFliePath");
        if (xmlFileName == null && passwordFileName == null) {
            xmlFileName = request.getParameter("_otpXmlFliePath");
            passwordFileName = request.getParameter("_otpPasswordFliePath");
        }

        String _UnitId = request.getParameter("_UnitId");
        log.debug("_UnitId :: "+_UnitId);
        int unitID = 0;
        if (_UnitId != null) {
            unitID = Integer.parseInt(_UnitId);
        }

        if (unitID == 0 ) {
             result = "error";
             message = "The unit select is invalid!!";
             try { 
                json.put("_result", result);
                json.put("_message", message);
                json.put("_success", success);
            out.print(json);
            out.flush();
             } catch(Exception e ){
                 log.error("exception caught :: ",e);
             }
            return;
        }

//                            File saveTo = new File(dirName + xmlFileName);
        //saveFile = fileName;
        //String secret = null;
        AXIOMStatus axiom[] = null;
        try {

            OTPTokenManagement oManagement = new OTPTokenManagement(channel.getChannelid());
            int tokenCountCurrentDBState = oManagement.getTokenCountForLicenseCheck(sessionId, channel.getChannelid(), OTPTokenManagement.HARDWARE_TOKEN);
            int licensecountForHWToken = AxiomProtect.GetHardwareTokensAllowed();

            XmlParser xParser = new XmlParser();
            TokenLicenseDetails[] tlDetails = xParser.ParseXML(xmlFileName);

            int tokenCountInUplodFile = tlDetails.length;

//            System.out.println("licensecountForHWToken=" + licensecountForHWToken);
//            System.out.println("tokenCountCurrentDBState=" + tokenCountCurrentDBState);
//            System.out.println("tokenCountInUplodFile=" + tokenCountInUplodFile);

            if (licensecountForHWToken != -998) {
                if (licensecountForHWToken - tokenCountCurrentDBState < tokenCountInUplodFile) {
                    result = "error";
                    message = "Hardware Token import is not allowed as this import will cross the limit!!";
                    json.put("_result", result);
                    json.put("_message", message);
                    json.put("_success", success);
                    out.print(json);
                    out.flush();
                    return;
                }
            } else {
                //unlimited licenses allowed for hardware tokens.
            }
            byte[] passwordToFile = null;
            if(!password.equals(null) && !password.isEmpty()){
              passwordToFile = new BigInteger(password,16).toByteArray();  
            }else{
                File fpasswordFileName = new File(passwordFileName);
             passwordToFile = OTPTokenManagement.readFile(fpasswordFileName);
            }
            
            if (passwordToFile == null) {
                result = "error";
                message = "Password is empty/invalid!!";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_success", success);
                out.print(json);
                out.flush();
                return;
            }

            Otptokens[] otp = oManagement.getTokenByCatAndSubCat(channel.getChannelid(), OTPTokenManagement.HARDWARE_TOKEN, OTPTokenManagement.HW_MINI_TOKEN);
            String strSerial = "";
            HashSet<String> stringSet = new HashSet<String>();
            if (tlDetails != null) {
                for (int j = 0; j < tlDetails.length; j++) {
                    strSerial = strSerial + "," + tlDetails[j].strSerialNo;
                    stringSet.add(tlDetails[j].strSecret);
                }
            }

            if (otp != null) {
                for (int j = 0; j < otp.length; j++) {
                    if (strSerial.contains(otp[j].getSrno())) {
                        result = "error";
                        message = "ERROR: Token serial number:" + otp[j].getSrno() + " is already present!!!";
                        json.put("_result", result);
                        json.put("_message", message);
                        json.put("_success", success);
                        out.print(json);
                        out.flush();
                        return;
                    }
                }
            }
            //System.out.println("Token XML file password length is "+ passwordToFile.length);
            for (int j = 0; j < tlDetails.length; ++j) {
                for(String secret : stringSet){
                tlDetails[j].strSecret = oManagement.DecryptTokenSecretv2(secret, passwordToFile, tlDetails[j].strEncAlgo);
                System.out.println("Srno"+tlDetails[j].strSerialNo);
//                tlDetails[j].strMAC=oManagement.DecryptTokenSecretv2(tlDetails[j].strMAC, passwordToFile, tlDetails[j].strEncAlgo);
//                 System.out.println("STRMAC"+ tlDetails[j].strMAC);
            }}

            ArrayList<TokenLicenseDetails> successlist = new ArrayList<TokenLicenseDetails>();
            ArrayList<TokenLicenseDetails> faillist = new ArrayList<TokenLicenseDetails>();
            TokenLicenseDetails otptoken = new TokenLicenseDetails();
            axiom = oManagement.addHardwareTokenHWUPLOAD(sessionId, channel.getChannelid(), tlDetails, unitID);

            if (axiom == null) {

                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Upload Token Secret File", result, retValue, "OTP Management",
                        "", "Success Entries=" + success + "Failed Entries=" + failed,
                        "OTPTOKENS",
                        "-");
                result = "error";
                message = "Fail To Upload!!";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_success", success);
                out.print(json);
                out.flush();
                return;

            }

            for (int j = 0; j < axiom.length; j++) {
                TokenLicenseDetails otptoken1 = new TokenLicenseDetails();
                if (axiom[j].iStatus == 0) {

                    if (tlDetails[i].strOtpalgo.equals("urn:ietf:params:xml:ns:keyprov:pskc:totp")) {
                        otptoken1.strOtpalgo = "OATH TIME OTP";
                    } else {
                        otptoken1.strOtpalgo = "OATH EVENT OTP";
                    }

                    if (tlDetails[i].strEncAlgo == null) {
                        otptoken1.strEncAlgo = "plaintext";
                    } else {
                        otptoken1.strEncAlgo = tlDetails[j].strEncAlgo;
                    }
                    otptoken1.strLength = tlDetails[j].strLength;
                    otptoken1.strMAC = tlDetails[j].strMAC;
                    otptoken1.strManufacturer = tlDetails[j].strManufacturer;
                    //otptoken1.strSecret = tlDetails[j].strSecret;
                    otptoken1.strSecret = "********";
                    otptoken1.strSerialNo = tlDetails[j].strSerialNo;
                    otptoken1.strTimeInterval = tlDetails[j].strTimeInterval;
                    success++;
                    successlist.add(otptoken1);

                } else {
                    if (tlDetails[i].strOtpalgo.equals("urn:ietf:params:xml:ns:keyprov:pskc:totp")) {
                        otptoken1.strOtpalgo = "OATH TIME OTP";
                    } else {
                        otptoken1.strOtpalgo = "OATH EVENT OTP";
                    }

                    if (tlDetails[i].strEncAlgo == null) {
                        otptoken1.strEncAlgo = "plaintext";
                    } else {
                        otptoken1.strEncAlgo = tlDetails[j].strEncAlgo;
                    }
                    otptoken1.strLength = tlDetails[j].strLength;
                    otptoken1.strMAC = tlDetails[j].strMAC;
                    otptoken1.strManufacturer = tlDetails[j].strManufacturer;
                    //otptoken1.strSecret = tlDetails[j].strSecret;
                    otptoken1.strSecret = "********";
                    otptoken1.strSerialNo = tlDetails[j].strSerialNo;
                    otptoken1.strTimeInterval = tlDetails[j].strTimeInterval;

                    failed++;
                    result = "ERROR";
                    faillist.add(otptoken1);
                }
            }

            HttpSession session = request.getSession(true);
            session.setAttribute("_failedOTPENTRIES", faillist);
            session.setAttribute("_successOTPENTRIES", successlist);

            message = "Token Import Result is" + success + " entries successfully added and" + failed + " entries are duplicate/invalid";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                    "Upload HOTP Secret File", result, retValue, "OTP Management",
                    "",
                    message,
                    "OTPTOKENS",
                    operatorS.getOperatorid()
            );

        } catch (Exception e) {
            e.printStackTrace();
            log.error("exception caught :: ",e);
        }

        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_failed", failed);
            json.put("_success", success);

        } finally {
            out.print(json);
            out.flush();
             log.info("is ended :: ");
            return;
           
        }
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
