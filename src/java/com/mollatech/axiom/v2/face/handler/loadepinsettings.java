/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.PINDeliverySetting;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class loadepinsettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadepinsettings.class.getName());

    public static String smsUrl;
    public static String voiceUrl;
    public static String ussdUrl;
    public static String webUrl;

    private JSONObject SettingsWhenEmpty(int _type1) {
        log.info("Servlet Started");
        JSONObject json = new JSONObject();
        int iEPIN = SettingsManagement.Epin;

        if (_type1 == iEPIN) {
            try {
                json.put("_pinDeliveryType", 1);
                json.put("_channelType", 1);
                json.put("_channelType2", 2);
                json.put("_dayRestriction", 1);
                json.put("_timeFromInHour", 0);
                json.put("_timeToInHour", 24);
                json.put("_pinRequestCountDuration", 2);
                json.put("_pinRequestCount", -1);
                json.put("_operatorController", false);
                json.put("_duration", 5);
                json.put("_expiryTime", 10);
                json.put("_isAlertOperatorOnFailure", false);
                json.put("_PINSource", 1);
                json.put("_PINLength", 8);
                json.put("_PINValidity", 1440);
                json.put("_classname", "");
                json.put("_smstext", "");
                json.put("_smsurl", smsUrl);
                json.put("_voiceurl", voiceUrl);
                json.put("_ussdurl", ussdUrl);
                json.put("_weburl", webUrl);
                json.put("_ChallengeResponse", 1);
                json.put("_smsurlStatus", 1);
                json.put("_voiceurlStatus", 1);
                json.put("_ussdurlStatus", 1);
                json.put("_weburlStatus", 1);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
        }
        return json;
    }

    private JSONObject SettingsWhenPresent(Object settingsObj) {
        JSONObject json = new JSONObject();
        if (settingsObj instanceof PINDeliverySetting) {
            PINDeliverySetting epinObj = (PINDeliverySetting) settingsObj;

            try {
                json.put("_pinDeliveryType", epinObj.pinDeliveryType);
                json.put("_channelType", epinObj.channelType);
                json.put("_channelType2", epinObj.channelType2);
                json.put("_duration", epinObj.duration);
                json.put("_dayRestriction", epinObj.dayRestriction);
                json.put("_timeFromInHour", epinObj.timeFromInHour);
                json.put("_timeToInHour", epinObj.timeToInHour);
                json.put("_pinRequestCountDuration", epinObj.pinRequestCountDuration);
                json.put("_pinRequestCount", epinObj.pinRequestCount);
                json.put("_operatorController", epinObj.operatorController);
                json.put("_expiryTime", epinObj.expiryTime);
                json.put("_isAlertOperatorOnFailure", epinObj.isAlertOperatorOnFailure);
                json.put("_classname", epinObj.classname);
                json.put("_smstext", epinObj.smsText);
                //
                json.put("_smsurl", smsUrl);
                json.put("_voiceurl", voiceUrl);
                json.put("_ussdurl", ussdUrl);
                json.put("_weburl", webUrl);
                json.put("_ChallengeResponse", epinObj.challangeResponseSource);
                json.put("_smsurlStatus", epinObj.isSMSEnable);
                json.put("_voiceurlStatus", epinObj.isVoiceEnable);
                json.put("_ussdurlStatus", epinObj.isUSSDEnable);
                json.put("_weburlStatus", epinObj.isWebEnable);
                json.put("_PinSourceClassNameclassname", epinObj.classname);
                json.put("_ChallengeResponseclassname", epinObj.challangeResponseClassName);
                json.put("_PINSource", epinObj.PINSource);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
        }
        return json;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("channel is::"+channel.getName());
        log.debug("sessionid::"+sessionId);
        JSONObject json = null;
        PrintWriter out = response.getWriter();
        int port = request.getLocalPort();
        String server = request.getServerName();
        String channelname = request.getContextPath();

        if (request.isSecure() == true) {
            smsUrl = "https://".concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("/").concat("epinSMS");
            voiceUrl = "https://".concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("/").concat("epinIVRConfirm");
            ussdUrl = "https://".concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("/").concat("epinSMS");
            webUrl = "https://".concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("/").concat("ecopin").concat("/").concat("epinWebStart.jsp");

        } else {
            smsUrl = "http://".concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("/").concat("epinSMS");
            voiceUrl = "http://".concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("/").concat("epinIVRConfirm");
            ussdUrl = "http://".concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("/").concat("epinSMS");
            webUrl = "http://".concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("/").concat("ecopin").concat("/").concat("epinWebStart.jsp");

        }

//        smsUrl = "http://".concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("/").concat("epinSMS");
//        voiceUrl = "http://".concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("/").concat("epinIVRConfirm");
//         ussdUrl = "http://".concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("/").concat("epinSMS");
//        webUrl = "http://".concat(server).concat(":").concat(String.valueOf(port)).concat(channelname).concat("/").concat("ecopin").concat("/").concat("epinWebStart.jsp");
        try {
            SettingsManagement sMngmt = new SettingsManagement();
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), sMngmt.Epin, sMngmt.PREFERENCE_ONE);
            if (settingsObj != null) {

                json = SettingsWhenPresent(settingsObj);
            } else {
                json = SettingsWhenEmpty(sMngmt.Epin);
            }

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
            // TODO handle custom exceptions here
        }
        try {
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
