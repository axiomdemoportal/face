/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.UserSourceSettings;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class loadusersrcsettings extends HttpServlet {
    
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadusersrcsettings.class.getName());
    
    private JSONObject SettingsWhenEmpty(int _type1) {
        JSONObject json = new JSONObject();
        if (_type1 == editsettings.UserSource ) {
            try { json.put("_status",0);
            json.put("_ip","localhost");
            json.put("_port",3306);
            json.put("_userId","");
            json.put("_password","");
            json.put("_ssl",false);
            json.put("_driverNameJAR","");
            json.put("_className","");
            json.put("_reserve1","");
            json.put("_reserve2","");
            json.put("_reserve3","");
            json.put("_reserve3","");
            json.put("_databaseType","com.mysql.jdbc.Driver");
            json.put("_databaseName","");
            json.put("_tableName","");
            json.put("_algoType", 6);
            json.put("_passwordType", 3);
            json.put("_algoAttrReserve1","");
            json.put("_algoAttrReserve2","");
            json.put("_algoAttrReserve3","");
            json.put("_algoAttrReserve4","");
            json.put("_userValidityDays","1825");
            }catch(Exception e){}
        } 
        return json;
    }

    private JSONObject SettingsWhenPresent(Object settingsObj) {
        JSONObject json = new JSONObject();
        if (settingsObj instanceof UserSourceSettings) {
            UserSourceSettings epinObj = (UserSourceSettings) settingsObj;
            try { json.put("_status",epinObj.getStatus());
            json.put("_ip",epinObj.getIp());
            json.put("_port",epinObj.getPort());
            json.put("_userId",epinObj.getUserId());
            json.put("_password",epinObj.getPassword());
            json.put("_ssl",epinObj.isSsl());
            json.put("_driverNameJAR",epinObj.getDriverNameJAR());
            json.put("_className",epinObj.getClassName());
            json.put("_reserve1",epinObj.getReserve1());
            json.put("_reserve2",epinObj.getReserve2());
            json.put("_reserve3",epinObj.getReserve3());
            json.put("_databaseType",epinObj.getDatabaseType());
            json.put("_databaseName",epinObj.getDatabaseName());
            json.put("_tableName",epinObj.getTableName());
            json.put("_algoType", epinObj.getAlgoType());
            json.put("_passwordType", epinObj.getPasswordType());
            json.put("_algoAttrReserve1",epinObj.getAlgoAttrReserve1());
            json.put("_algoAttrReserve2",epinObj.getAlgoAttrReserve2());
            json.put("_algoAttrReserve3",epinObj.getAlgoAttrReserve3());
            json.put("_algoAttrReserve4",epinObj.getAlgoAttrReserve4());
            json.put("_channelId",epinObj.getChannelId());
            json.put("_databaseType",epinObj.getDbCategory()); // chaned by abhishek
            json.put("_userValidityDays",epinObj.getUserValidityDays());
            }catch(Exception e){}
        }
        return json;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        JSONObject json = null;
        PrintWriter out = response.getWriter();
        
        log.debug("loadusersrcsettings::channel is::"+channel.getName());
        log.debug("loadusersrcsettings::sessionid::"+sessionId);

        try {
            SettingsManagement sMngmt = new SettingsManagement();
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), editsettings.UserSource, 1);
            if (settingsObj != null) {
                json = SettingsWhenPresent(settingsObj);
            } else {
                json = SettingsWhenEmpty(editsettings.UserSource);
                json.put("_channelId",channel.getChannelid());
            }

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
            // TODO handle custom exceptions here
        }
        try {
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
