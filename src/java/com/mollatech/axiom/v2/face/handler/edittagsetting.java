/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.ContactTagsSetting;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class edittagsetting extends HttpServlet {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(edittagsetting.class.getName());
    final String itemtype = "SETTINGS";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        PrintWriter out = response.getWriter();

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        String OperatorID = operatorS.getOperatorid();

        //String istatus = request.getParameter("_statusContent");
        //int _istatus = Integer.parseInt(istatus);

        String _channelId = channel.getChannelid();
        SettingsManagement sMngmt = new SettingsManagement();
        int retValue = -1;
            log.debug("channel :: " + channel.getName());
            log.debug("getChannelid :: " + channel.getChannelid());
            log.debug("operatorS :: " + operatorS.getOperatorid());
            log.debug("sessionId :: " + sessionId);
            log.debug("remoteaccesslogin :: " + remoteaccesslogin);

        String result = "success";
        String message = "Tags Updated Successfully!!!";
        JSONObject json = new JSONObject();

        //String _preference = request.getParameter("_perferenceContent");
        int _iPreference = 1;//Integer.parseInt(_preference);

        //String _type = request.getParameter("_type");
        int _itype = SettingsManagement.CONTACT_TAGS;//Integer.parseInt(_type);

        Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _itype, _iPreference);

        ContactTagsSetting contentObj = null;
        boolean bAddSetting = false;

        if (settingsObj == null) {
            contentObj = new ContactTagsSetting();
            contentObj.setChannelId(_channelId);            
            bAddSetting = true;
        } else {
            contentObj = (ContactTagsSetting) settingsObj;
        }

        String _content = request.getParameter("_tagsList");
        log.debug("_content :: " + _content);
        String [] _contentsArray = _content.split(",");
        //if (_itype == com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.CONTACT_TAGS) {
        //    contentObj.setChannelId(_channelId);
        
//        ContactManagement cmObj = new ContactManagement();
//        retValue = cmObj.UpdateContatactTags(sessionId, _channelId, _content);
        
      //  if ( retValue == 0 ) {
            contentObj.setTags(_contentsArray);
    //    }
        
        AuditManagement audit = new AuditManagement();
        if (bAddSetting == true) {
            retValue = sMngmt.addSetting(sessionId, _channelId, _itype, _iPreference, contentObj);
            log.debug("addSetting :: " + retValue);

         String resultString = "ERROR";
         
           String[] arrTags = contentObj.getTags();
           String strTags = null;
           for(int i = 0; i< arrTags.length;i++){
               strTags +=arrTags[i]+","; 
           }
            if(retValue == 0){
                resultString = "Success";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                    "Add Tags Setting", resultString, retValue, "Setting Management",
                    "","New Tags ="+strTags,itemtype,OperatorID);
                 }else if(retValue != 0){
                       audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                               request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                    "Add Tags Setting", resultString, retValue, "Setting Management",
                    "","Failed to add Tags Setting..!!!",itemtype, OperatorID);
                 }

        } else {
             ContactTagsSetting oldTagObj = (ContactTagsSetting) sMngmt.getSetting(sessionId, _channelId, _itype, _iPreference);
            retValue = sMngmt.changeSetting(sessionId, _channelId, _itype, _iPreference, settingsObj, contentObj);
            log.debug("edittagsetting::changeSetting::" + retValue);

           String resultString = "ERROR";
            if (retValue == 0) {resultString = "SUCCESS";}

              String [] arroldTags = oldTagObj.getTags();
            String strOldTags = null;
           for(int i = 0; i< arroldTags.length;i++){
               strOldTags +=arroldTags[i]+","; 
           }
           
            String [] arrnewTags = contentObj.getTags();
            String strnewTags = null;
           for(int i = 0; i< arrnewTags.length;i++){
               strnewTags +=arrnewTags[i]+","; 
           }
            
            if(retValue == 0){
                 audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                         request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                    "Change Te Setting", resultString, retValue, "Setting Management",
                    "Old Tags ="+strOldTags, "New Tags ="+strnewTags,itemtype, 
                    channel.getChannelid());
            }
            else if(retValue != 0){
                 audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                         request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                    "Change TAG Setting", resultString, retValue, "Setting Management",
                    "Old Tags ="+strOldTags, "Failed To Edit Tags Settings", itemtype, 
                    channel.getChannelid());
            }
        }

        if (retValue != 0) {
            result = "error";
            //message = "Content Filter Gateway Settings Update Failed!!!";
            message = "Tags Update Failed!!!";
        }


        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch(Exception e){ log.error("Exception caught :: ",e);} finally {
            out.print(json);
            out.close();
        }
        log.info("Servlet ended");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
