/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.OOBEmailChannelSettings;
import com.mollatech.axiom.nucleus.settings.OOBMobileChannelSettings;
import com.mollatech.axiom.nucleus.settings.RadiusServerSettings;
import com.mollatech.axiom.nucleus.settings.RootCertificateSettings;
import com.mollatech.axiom.nucleus.settings.TokenSettings;
import com.mollatech.axiom.nucleus.settings.UserSourceSettings;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class loadsettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadsettings.class.getName());

    private JSONObject SettingsWhenEmpty(int _type1, int _preference1, Object settingsObj) {
        JSONObject json = new JSONObject();
        if (settingsObj == null) {
            if (_type1 == editsettings.SMS && _preference1 == editsettings.PREFERENCE_ONE) {
                //json.put("_preference", _preference);
                try { json.put("_className", "");
                json.put("_ip", "");
                json.put("_port", "");
                json.put("_userId", "");
                json.put("_password", "");
                json.put("_phoneNumber", "");
                json.put("_reserve1", "");
                json.put("_reserve2", "");
                json.put("_reserve3", "");
                json.put("_status", "Disabled");
                json.put("_logConfirmation", true);
                json.put("_autofailover", 0);
                json.put("_retries", 2);
                json.put("_retryduration", 10);
                }catch(Exception e){}
            } else if (_type1 == editsettings.SMS && _preference1 == editsettings.PREFERENCE_TWO) {
                //json.put("_preference", _preference);
                try { json.put("_classNameS", "");
                json.put("_ipS", "");
                json.put("_portS", "");
                json.put("_userIdS", "");
                json.put("_passwordS", "");
                json.put("_phoneNumberS", "");
                json.put("_reserve1S", "");
                json.put("_reserve2S", "");
                json.put("_reserve3S", "");
                json.put("_statusS", "Disabled");
                json.put("_logConfirmationS", true);
                json.put("_retriesS", 2);
                json.put("_retrydurationS",10);
                }catch(Exception e){}
            } else if (_type1 == editsettings.USSD && _preference1 == editsettings.PREFERENCE_ONE) {
                //json.put("_preference", _preference);
                try { json.put("_className", "");
                json.put("_ip", "");
                json.put("_port", "80");
                json.put("_userId", "");
                json.put("_password", "");
                json.put("_phoneNumber", "");
                json.put("_reserve1", "");
                json.put("_reserve2", "");
                json.put("_reserve3", "");
                json.put("_status", "Disabled");
                json.put("_logConfirmation", true);
                json.put("_autofailover", 0);
                json.put("_retries", 2);
                json.put("_retryduration", 10);
                }catch(Exception e){}
            } else if (_type1 == editsettings.USSD && _preference1 == editsettings.PREFERENCE_TWO) {
                //json.put("_preference", _preference);
                try { json.put("_classNameS", "");
                json.put("_ipS", "");
                json.put("_portS", "80");
                json.put("_userIdS", "");
                json.put("_passwordS", "");
                json.put("_phoneNumberS", "");
                json.put("_reserve1S", "");
                json.put("_reserve2S", "");
                json.put("_reserve3S", "");
                json.put("_statusS", "Disabled");
                json.put("_logConfirmationS", true);
                json.put("_retriesS", "2");
                json.put("_retrydurationS", "10");
                }catch(Exception e){}
            } else if (_type1 == editsettings.VOICE && _preference1 == editsettings.PREFERENCE_ONE) {
                //json.put("_preference", _preference);
                try { json.put("_className", "");
                json.put("_ip", "");
                json.put("_port", "80");
                json.put("_userId", "");
                json.put("_password", "");
                json.put("_phoneNumber", "");
                json.put("_reserve1", "");
                json.put("_reserve2", "");
                json.put("_reserve3", "");
                json.put("_status", "Disabled");
                json.put("_logConfirmation", true);
                json.put("_autofailover", 0);
                json.put("_retries", 2);
                json.put("_retryduration", 10);
                }catch(Exception e){}
            } else if (_type1 == editsettings.VOICE && _preference1 == editsettings.PREFERENCE_TWO) {
                //json.put("_preference", _preference);
                try { json.put("_classNameS", "");
                json.put("_ipS", "");
                json.put("_portS", "80");
                json.put("_userIdS", "");
                json.put("_passwordS", "");
                json.put("_phoneNumberS", "");
                json.put("_reserve1S", "");
                json.put("_reserve2S", "");
                json.put("_reserve3S", "");
                json.put("_statusS", "Disabled");
                json.put("_logConfirmationS", true);
                json.put("_retriesS",2);
                json.put("_retrydurationS", 10);
                }catch(Exception e){}
            }
            if (_type1 == editsettings.Email && _preference1 == editsettings.PREFERENCE_ONE) {
                //json.put("_channelId", "");
                try { json.put("_className", "");
                json.put("_fromEmail", "");
                json.put("_fromName", "");
                json.put("_password", "");
                json.put("_port", "");
                //json.put("_preference1", "");
                json.put("_reserve1", "");
                json.put("_reserve2", "");
                json.put("_reserve3", "");
                json.put("_smtpIp", "");
                json.put("_status", "");
                json.put("_userId", "");
                json.put("_authRequired", "");
                json.put("_ssl", "");
                json.put("_smtpIp", "");
                json.put("_autofailover", 0);
                json.put("_retries", 2);
                json.put("_retryduration", 10);
                }catch(Exception e){}
            } else if (_type1 == editsettings.Email && _preference1 == editsettings.PREFERENCE_TWO) {
                //json.put("_channelIdS", "");
                try { json.put("_classNameS", "");
                json.put("_fromEmailS", "");
                json.put("_fromNameS", "");
                json.put("_passwordS", "");
                json.put("_portS", "");
                json.put("_preferenceS", "");
                json.put("_reserveS", "");
                json.put("_reserveS", "");
                json.put("_reserveS", "");
                json.put("_smtpIpS", "");
                json.put("_statusS", "");
                json.put("_userIdS", "");
                json.put("_authRequiredS", "");
                json.put("_sslS", "");
                json.put("_smtpIpS", "");
                //json.put("_autofailoverS", "0");
                json.put("_retriesS", "2");
                json.put("_retrydurationS", "10");
                }catch(Exception e){}
            } else if (_type1 == editsettings.RADIUS) {
                try { json.put("_accountIp", "");
                json.put("_accountPort", "");
                json.put("_authIp", "");
                json.put("_authPort", "");
                json.put("_axiomServerIp", "");
                json.put("_axiomServerPort", "");
                //json.put("_channelId", "");
                json.put("_class", "");
                json.put("_ldapSearchPath", "");
                json.put("_ldapServerIp", "");
                json.put("_ldapServerPort", "");
                json.put("_radiusClientAuthtype", "");
                json.put("_radiusClientIp", "");
                json.put("_radiusClientSecretkey", "");
                json.put("_radiusClientsCount", "");
                json.put("_status", "");
                json.put("_accountEnabled", "");
                json.put("_authEnabled", "");
                json.put("_axiomValidate()", "");
                json.put("_ldapValidate()", "");
                }catch(Exception e){}
            } else if (_type1 == editsettings.RootConfiguration) {
                //json.put("_channelId", "");
                try { json.put("_channelType", "");
                json.put("_city", "");
                json.put("_class", "");
                json.put("_className", "");
                json.put("_country", "");
                json.put("_crlUrl", "");
                json.put("_csrUrl", "");
                json.put("_ip", "");
                json.put("_ocspUrlPath", "");
                json.put("_organization", "");
                json.put("_organizationUnit", "");
                json.put("_password", "");
                json.put("_port", "");
                json.put("_reserve1", "");
                json.put("_Reserve2", "");
                json.put("_Reserve3", "");
                json.put("_state", "");
                json.put("_status", "");
                json.put("_timestampUrlPath", "");
                json.put("_userId", "");
                json.put("_validityDays", "");
                json.put("_authRequired", "");
                json.put("_notificationEnabled", "");
                json.put("_ssl", "");
                }catch(Exception e){}
            } else if (_type1 == editsettings.Token) {
                try { json.put("_certificateCount", "0");
                //json.put("_channelId", "0");
                json.put("_class", "");
                json.put("_wTokenCount", "0");
                json.put("_oobTokenCount", "");
                json.put("_otpAllowed", "");
                json.put("_otpLengthHWToken", "");
                json.put("_otpLengthOOBToken", "");
                json.put("_otpLengthSWToken", "");
                json.put("_otpSteps", "");
                json.put("_pkiHWTokenCount", "");
                json.put("_pkiSWTokenCount", "");
                json.put("_sotpAllowed", "");
                json.put("_sotpLengthHWToken", "");
                json.put("_sotpLengthOOBToken", "");
                json.put("_sotpLengthSWToken", "");
                json.put("_CERTIFICATEEnabled", "");
                json.put("_HWOTPTokenEnabled", "");
                json.put("_HWPKITokenEnabled", "");
                json.put("_HWPKITokenEnabled", "");
                json.put("_OOBOTPTokenEnabled", "");
                json.put("_SWOTPTokenEnabled", "");
                json.put("_SWPKITokenEnabled", "");
                json.put("_bCERTIFICATEUnlimited", "");
                json.put("_bHWOTPTokenUnlimited", "");
                json.put("_bHWPKITokenUnlimited", "");
                json.put("_bOOBOTPTokenUnlimited", "");
                json.put("_SWOTPTokenUnlimited", "");
                json.put("_SWPKITokenUnlimited", "");
                }catch(Exception e){}
            } else if (_type1 == editsettings.UserSource) {
                try { json.put("_algoAttrReserve1", "");
                json.put("_algoAttrReserve2", "");
                json.put("_algoAttrReserve3", "");
                json.put("_algoAttrReserve4", "");
                json.put("_algoType", "");
                //json.put("_channelId", "");
                json.put("_class", "");
                json.put("_className", "");
                json.put("_databaseName", "");
                json.put("_databaseType", "");
                json.put("_driverNameJAR", "");
                json.put("_ip", "");
                json.put("_password", "");
                json.put("_passwordType", "");
                json.put("_port", "");
                json.put("_reserve1", "");
                json.put("_reserve2", "");
                json.put("_reserve3", "");
                json.put("_status", "");
                json.put("_tableName", "");
                json.put("_userId", "");
                json.put("_ssl", "");
                }catch(Exception e){}
            }
        }
        return json;
    }

    private JSONObject SettingsWhenPresent(Object settingsObj) {
        JSONObject json = new JSONObject();
        if (settingsObj instanceof OOBMobileChannelSettings) {

            OOBMobileChannelSettings mobile = (OOBMobileChannelSettings) settingsObj;
            if (mobile.getPreference() == 1) {
                //json.put("_channelId", mobile.getChannelId());
                //json.put("_preference", mobile.getPreference());
                try { json.put("_className", mobile.getClassName());
                json.put("_ip", mobile.getIp());
                json.put("_port", mobile.getPort());
                json.put("_userId", mobile.getUserid());
                json.put("_password", mobile.getPassword());
                json.put("_phoneNumber", mobile.getPhoneNumber());
                json.put("_reserve1", mobile.getReserve1());
                json.put("_reserve2", mobile.getReserve2());
                json.put("_reserve3", mobile.getReserve3());
                json.put("_status", mobile.getStatus());
                //json.put("_type", mobile.getType());
                json.put("_logConfirmation", mobile.isLogConfirmation());
                json.put("_retries", mobile.getRetrycount());
                json.put("_retryduration", mobile.getRetryduration());
                
                json.put("_autofailover", mobile.getAutofailover());
                }catch(Exception e){}

            } else {
                try { 
                json.put("_classNameS", mobile.getClassName());
                json.put("_ipS", mobile.getIp());
                json.put("_portS", mobile.getPort());
                json.put("_userIdS", mobile.getUserid());
                json.put("_passwordS", mobile.getPassword());
                json.put("_phoneNumberS", mobile.getPhoneNumber());                
                json.put("_preferenceS", mobile.getPreference());
                json.put("_reserve1S", mobile.getReserve1());
                json.put("_reserve2S", mobile.getReserve2());
                json.put("_reserve3S", mobile.getReserve3());
                json.put("_statusS", mobile.getStatus());
                //json.put("_typeS", mobile.getType());                
                json.put("_logConfirmationS", mobile.isLogConfirmation());
                json.put("_retriesS", mobile.getRetrycount());
                json.put("_retrydurationS", mobile.getRetryduration());
                }catch(Exception e){}
            }
        } else if (settingsObj instanceof OOBEmailChannelSettings) {
            OOBEmailChannelSettings email = (OOBEmailChannelSettings) settingsObj;

            if (email.getPreference() == 1) {
                //json.put("_channelId", email.getChannelId());
                try { 
                json.put("_className", email.getClassName());
                json.put("_fromEmail", email.getFromEmail());
                json.put("_fromName", email.getFromName());
                json.put("_password", email.getPassword());
                json.put("_port", email.getPort());
                json.put("_preference1", email.getPreference());
                json.put("_reserve1", email.getReserve1());
                json.put("_reserve2", email.getReserve2());
                json.put("_reserve3", email.getReserve3());
                json.put("_smtpIp", email.getSmtpIp());
                json.put("_status", email.getStatus());
                json.put("_userId", email.getUserId());
                json.put("_authRequired", email.isAuthRequired());
                json.put("_ssl", email.isSsl());
                json.put("_smtpIp", email.getSmtpIp());
                json.put("_autofailover", email.getAutofailover());
                json.put("_retries", email.getRetrycount());
                //json.put("_retryduration", email.getRetryduration());
                //json.put("_implementationJAR", email.getImplementationJAR());
                }catch(Exception e){}

            } else {

                //json.put("_channelIdS", email.getChannelId());
                try { 
                    json.put("_classNameS", email.getClassName());
                
                json.put("_fromEmailS", email.getFromEmail());
                json.put("_fromNameS", email.getFromName());
                //            json.put("_implementationJarS", email.getImplementationJAR());
                json.put("_passwordS", email.getPassword());
                json.put("_portS", email.getPort());
                json.put("_preferenceS", email.getPreference());
                json.put("_reserve1S", email.getReserve1());
                json.put("_reserve2S", email.getReserve2());
                json.put("_reserve3S", email.getReserve3());
                json.put("_smtpIpS", email.getSmtpIp());
                json.put("_statusS", email.getStatus());
                json.put("_userIdS", email.getUserId());
                json.put("_authRequiredS", email.isAuthRequired());
                json.put("_sslS", email.isSsl());
                json.put("_smtpIpS", email.getSmtpIp());

                json.put("_autofailover", email.getAutofailover());
                json.put("_retries", email.getRetrycount());
                //          json.put("_retryduration", email.getRetryduration());
                
                }catch(Exception e){}

            }
        } else if (settingsObj instanceof RadiusServerSettings) {
            RadiusServerSettings radius = (RadiusServerSettings) settingsObj;
            try { 
            json.put("_accountIp", radius.getAccountIp());
            json.put("_accountPort", radius.getAccountPort());
            json.put("_authIp", radius.getAuthIp());
            json.put("_authPort", radius.getAuthPort());
            json.put("_axiomServerIp", radius.getAxiomServerIp());
            json.put("_axiomServerPort", radius.getAxiomServerPort());
            json.put("_channelId", radius.getChannelId());
            json.put("_class", radius.getClass());
            json.put("_ldapSearchPath", radius.getLdapSearchPath());
            json.put("_ldapServerIp", radius.getLdapServerIp());
            json.put("_ldapServerPort", radius.getLdapServerPort());
            json.put("_radiusClientAuthtype", radius.getRadiusClientAuthtype());
            json.put("_radiusClientIp", radius.getRadiusClientIp());
            json.put("_radiusClientSecretkey", radius.getRadiusClientSecretkey());
            json.put("_radiusClientsCount", radius.getRadiusClientsCount());
            json.put("_status", radius.getStatus());
            json.put("_accountEnabled", radius.isAccountEnabled());
            json.put("_authEnabled", radius.isAuthEnabled());
            json.put("_axiomValidate()", radius.isAxiomValidate());
            json.put("_ldapValidate()", radius.isLdapValidate());
            }catch(Exception e){}
        } else if (settingsObj instanceof RootCertificateSettings) {
            try { 
            RootCertificateSettings certificate = (RootCertificateSettings) settingsObj;
            json.put("_channelId", certificate.getChannelId());
            json.put("_channelType", certificate.getChannelType());
            json.put("_class", certificate.getClass());
            json.put("_className", certificate.getClassName());
            json.put("_ip", certificate.getIp());
            json.put("_password", certificate.getPassword());
            json.put("_port", certificate.getPort());
            json.put("_reserve1", certificate.getReserve1());
            json.put("_Reserve2", certificate.getReserve2());
            json.put("_Reserve3", certificate.getReserve3());
            json.put("_status", certificate.getStatus());
            json.put("_userId", certificate.getUserId());
            json.put("_validityDays", certificate.getValidityDays());
            json.put("_notificationEnabled", certificate.isNotificationEnabled());
            }catch(Exception e){}
            
        } else if (settingsObj instanceof TokenSettings) {
            try { 
            TokenSettings token = (TokenSettings) settingsObj;
            json.put("_certificateCount", token.getCertificateCount());
            json.put("_channelId", token.getChannelId());
            json.put("_class", token.getClass());
            json.put("_wTokenCount", token.getHwTokenCount());
            json.put("_oobTokenCount", token.getOobTokenCount());
            json.put("_otpAllowed", token.getOtpAllowed());
            json.put("_otpLengthHWToken", token.getOtpLengthHWToken());
            json.put("_otpLengthOOBToken", token.getOtpLengthOOBToken());
            json.put("_otpLengthSWToken", token.getOtpLengthSWToken());
            json.put("_otpSteps", token.getOtpSteps());
            json.put("_pkiHWTokenCount", token.getPkiHWTokenCount());
            json.put("_pkiSWTokenCount", token.getPkiSWTokenCount());
//            json.put("_sotpAllowed", token.getSotpAllowed());
            json.put("_sotpLengthHWToken", token.getSotpLengthHWToken());
            json.put("_sotpLengthOOBToken", token.getSotpLengthOOBToken());
            json.put("_sotpLengthSWToken", token.getSotpLengthSWToken());
            json.put("_CERTIFICATEEnabled", token.isCERTIFICATEEnabled());
            json.put("_HWOTPTokenEnabled", token.isHWOTPTokenEnabled());
            json.put("_HWPKITokenEnabled", token.isHWPKITokenEnabled());
            json.put("_HWPKITokenEnabled", token.isHWPKITokenEnabled());
            json.put("_OOBOTPTokenEnabled", token.isOOBOTPTokenEnabled());
            json.put("_SWOTPTokenEnabled", token.isSWOTPTokenEnabled());
            json.put("_SWPKITokenEnabled", token.isSWPKITokenEnabled());
            json.put("_bCERTIFICATEUnlimited", token.isbCERTIFICATEUnlimited());
            json.put("_bHWOTPTokenUnlimited", token.isbHWOTPTokenUnlimited());
            json.put("_bHWPKITokenUnlimited", token.isbHWPKITokenUnlimited());
            json.put("_bOOBOTPTokenUnlimited", token.isbOOBOTPTokenUnlimited());
            json.put("_SWOTPTokenUnlimited", token.isbSWOTPTokenUnlimited());
            json.put("_SWPKITokenUnlimited", token.isbSWPKITokenUnlimited());
            }catch(Exception e){}
        } else if (settingsObj instanceof UserSourceSettings) {
            try { 
            UserSourceSettings source = (UserSourceSettings) settingsObj;
            json.put("_algoAttrReserve1", source.getAlgoAttrReserve1());
            json.put("_algoAttrReserve2", source.getAlgoAttrReserve2());
            json.put("_algoAttrReserve3", source.getAlgoAttrReserve3());
            json.put("_algoAttrReserve4", source.getAlgoAttrReserve4());
            json.put("_algoType", source.getAlgoType());
            json.put("_channelId", source.getChannelId());
            json.put("_class", source.getClass());
            json.put("_className", source.getClassName());
            json.put("_databaseName", source.getDatabaseName());
            json.put("_databaseType", source.getDatabaseType());
            json.put("_driverNameJAR", source.getDriverNameJAR());
            json.put("_ip", source.getIp());
            json.put("_password", source.getPassword());
            json.put("_passwordType", source.getPasswordType());
            json.put("_port", source.getPort());
            json.put("_reserve1", source.getReserve1());
            json.put("_reserve2", source.getReserve2());
            json.put("_reserve3", source.getReserve3());
            json.put("_status", source.getStatus());
            json.put("_tableName", source.getTableName());
            json.put("_userId", source.getUserId());
            json.put("_ssl", source.isSsl());
            }catch(Exception e){}
        }
        return json;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        String _type = request.getParameter("_type");
        String _preference = request.getParameter("_preference");
        int _type1 = Integer.parseInt(_type);
        int _preference1 = Integer.parseInt(_preference);
        
        log.debug("loadsettings::channel is::"+channel.getName());
        log.debug("loadsettings::sessionid::"+sessionId);
        log.debug("loadsettings::type is::"+_type1);
        log.debug("loadsettings::preference is::"+_preference1);

        JSONObject json = null;//new JSONObject();
        PrintWriter out = response.getWriter();

        try {
            SettingsManagement sMngmt = new SettingsManagement();
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _type1, _preference1);
            if (settingsObj != null) {
                json = SettingsWhenPresent(settingsObj);
            } else {
                json = SettingsWhenEmpty(_type1, _preference1, settingsObj);
            }

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
            // TODO handle custom exceptions here
        }
        try {
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
