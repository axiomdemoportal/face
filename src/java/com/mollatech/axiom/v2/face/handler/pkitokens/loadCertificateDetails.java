/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.pkitokens;

import com.mollatech.axiom.nucleus.db.Certificates;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.security.cert.CertificateException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class loadCertificateDetails extends HttpServlet {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadCertificateDetails.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public static final int CERT_STATUS_ACTIVE = 1;
    public static final int CERT_STATUS_REVOKED = -5;
    public static final int CERT_STATUS_EXPIRED = -10;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");

        response.setContentType("application/json");

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: " + channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: " + sessionId);
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String _user = request.getParameter("_uid");
        log.debug("_user :: " + _user);

        if (_user == null) {
            try {
                json.put("_result", "error");
                json.put("_message", "User id Not valid..!!!!");
            } catch (Exception e) {
                log.error("exception caught :: ", e);
            }
            out.print(json);
            out.flush();
            return;
        }

        CertificateManagement CObj = new CertificateManagement();
        javax.security.cert.X509Certificate certifts = null;

        Certificates userCertObj = CObj.getCertificate(sessionId, channel.getChannelid(), _user);

        //String certificate = CObj.GenerateCertificate(sessionId, channel.getChannelid(), _user);
        if (userCertObj != null) {
            byte[] certBytes = Base64.decode(userCertObj.getCertificate());
            try {
                certifts = javax.security.cert.X509Certificate.getInstance(certBytes);

                if (certifts == null) {
                    try {
                        json.put("_result", "error");
                        json.put("_message", "Certificate Decoding Failed!!!");
                    } catch (Exception e) {
                        log.error("exception caught :: ", e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
            } catch (Exception ex) {
                log.error("exception caught :: ", ex);
                try {
                    json.put("_result", "error");
                    json.put("_message", "Certificate Decoding Failed!!!");
                } catch (Exception e) {
                    log.error("Exception caught :: ", e);
                }
                out.print(json);
                out.flush();
                return;
            }
        }

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
            Date dLastUpdated = userCertObj.getExpirydatetime();
            String ExpireOn = "NA";
            if (dLastUpdated != null) {
                ExpireOn = sdf.format(dLastUpdated);
            }
            String notAfter = sdf.format(certifts.getNotAfter());
            String notBefore = sdf.format(certifts.getNotBefore());
            json.put("_srno", certifts.getSerialNumber());
            json.put("_algoname", certifts.getSigAlgName());
            json.put("_issuerdn", "\"" + certifts.getIssuerDN() + "\"");
            json.put("_notafter", ExpireOn);
            json.put("_notbefore", notBefore);
            json.put("_subjectdn", certifts.getSubjectDN());
            json.put("_version", certifts.getVersion());
            json.put("_subjectdn", "\"" + certifts.getSubjectDN() + "\"");
            json.put("_result", "success");

        } catch (Exception ex) {
            log.error("exception caught :: ", ex);
            // TODO handle custom exceptions here
            try {
                json.put("_result", "error");
                json.put("_message", ex.getMessage());
            } catch (Exception e) {
                log.error("exception caught :: ", e);
            }
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
