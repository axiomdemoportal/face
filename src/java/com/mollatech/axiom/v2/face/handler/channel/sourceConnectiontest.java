/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.axiom.v2.face.handler.channel;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class sourceConnectiontest extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(sourceConnectiontest.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         response.setContentType("application/json");
         log.info("is started :: ");

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel ::"+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId ::"+sessionId);
//        
//        String _type = request.getParameter("_type");
//        String _preference = request.getParameter("_preference");
//        String _testphone = null;
//        String _testmsg = null;
//
//        if (_preference.compareTo("1") == 0) {
//            _testphone = request.getParameter("_testphone");
//            _testmsg = request.getParameter("_testmsg");
//        } else if (_preference.compareTo("2") == 0) {
//            _testphone = request.getParameter("_testphoneS");
//            _testmsg = request.getParameter("_testmsgS");
//        }

//        int _type1 = Integer.parseInt(_type);
//        int _preference1 = Integer.parseInt(_preference);


        String result = "success";
        String message = "Connection sucessful!!!";

        boolean retValue = false;

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        try {
            OperatorsManagement sMngmt = new OperatorsManagement();
            retValue = sMngmt.TestConnection(sessionId, channel.getChannelid());
            log.debug("TestConnection::"+retValue);
            
        } catch (Exception ex) {
            log.error("exception caught :: ",ex);
          
        }

        if(retValue == true){
            result = "success";
            message = "Connection sucessful!!!";
        }else if(retValue == false){//Prefix Filter
            result = "error";
            message = "Connection Failed Check Settings!!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception ex) {
            log.error("exception caught :: ",ex);
            
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
