/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.PluginSetting;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech1
 */
public class editpluginsetting extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editpluginsetting.class.getName());

    final String itemtype = "SETTINGS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        try {
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");

            //audit parameter
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
            String OperatorID = operatorS.getOperatorid();

            String _channelId = channel.getChannelid();
            SettingsManagement sMngmt = new SettingsManagement();
            int retValue = -1;

            String result = "success";
            String message = "Plugins Updated Successfully!!!";
            JSONObject json = new JSONObject();
            
            log.debug("channel :: " + channel.getName());
            log.debug("getChannelid :: " + channel.getChannelid());
            log.debug("operatorS :: " + operatorS.getOperatorid());
            log.debug("sessionId :: " + sessionId);
            log.debug("remoteaccesslogin :: " + remoteaccesslogin);

            //String _preference = request.getParameter("_perferenceContent");
            int _iPreference = 1;//Integer.parseInt(_preference);

            //String _type = request.getParameter("_type");
            int _itype = SettingsManagement.PLUGIN_SETTING;//Integer.parseInt(_type);
            // String strType = String.valueOf(_itype); 
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _itype, _iPreference);

            PluginSetting pluginObj = null;
            boolean bAddSetting = false;

            if (settingsObj == null) {
                pluginObj = new PluginSetting();
                pluginObj.setChannelId(_channelId);

                bAddSetting = true;
            } else {
                pluginObj = (PluginSetting) settingsObj;
            }

            String _plugin = request.getParameter("_fileNames");
             log.debug("editpluginsetting::name of file is::" + _plugin);
            // String _plugin = (String) json.get("fileName");
            String[] _pluginArray = _plugin.split(",");
            if (_itype == com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PLUGIN_SETTING) {
                pluginObj.setChannelId(_channelId);
                pluginObj.setFileName(_pluginArray);
                //ipObj.setStatus(_istatus);
            }
            AuditManagement audit = new AuditManagement();
            if (bAddSetting == true) {

                String[] arrIp = pluginObj.getFileName();
                String strContent = null;
                for (int i = 0; i < arrIp.length; i++) {
                    strContent += arrIp[i] + ",";
                }

                retValue = sMngmt.addSetting(sessionId, _channelId, _itype, _iPreference, pluginObj);
                log.debug("retValue :: " + retValue);
                String resultString = "ERROR";

                if (retValue == 0) {

                    resultString = "SUCCESS";

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add Plugin", resultString, retValue, "Setting Management",
                            "", "New Content =" + strContent,
                            itemtype,_channelId);

                } else if (retValue != 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add Plugin", resultString, retValue, "Setting Management",
                            "", "Failed To Add Add Plugin List", itemtype,_channelId);

                }

            } else {
                PluginSetting pluginOldObj = (PluginSetting) sMngmt.getSetting(sessionId, _channelId, _itype, _iPreference);
                retValue = sMngmt.changeSetting(sessionId, _channelId, _itype, _iPreference, settingsObj, pluginObj);
                log.debug("retValue :: " + retValue);
                String resultString = "ERROR";

                String[] arroldContent = pluginOldObj.getFileName();
                String strOldPlugin = null;
                for (int i = 0; i < arroldContent.length; i++) {
                    strOldPlugin += arroldContent[i] + ",";
                }

                String[] arrnewIp = pluginObj.getFileName();
                String strnewPlugin = null;
                for (int i = 0; i < arrnewIp.length; i++) {
                    strnewPlugin += arrnewIp[i] + ",";
                }

                if (retValue == 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                             "Change Plugin", resultString, retValue, "Setting Management",
                            "Old Content =" + strOldPlugin, "New Content =" + strnewPlugin,
                            itemtype, _channelId);
                }

                if (retValue != 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                             "Change Plugin", resultString, retValue, "Setting Management",
                            "Old Content =" + strOldPlugin, "Failed To Change Plugin",
                            itemtype, _channelId);
                }

            }

            if (retValue != 0) {
                result = "error";
                //message = "Content Filter Gateway Settings Update Failed!!!";
                message = "Allowed Plugin List Update Failed!!!";
            }

            try {
                json.put("_result", result);
                json.put("_message", message);

            } finally {
                out.print(json);
                out.close();
            }
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        }
        log.info("Servlet ended");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
