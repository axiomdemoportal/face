/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.users;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class edituser extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(edituser.class.getName());

    final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;
    final int LOCKED_STATUS = -1;
    final int REMOVE_STATUS = -99;
    final String itemTypeAUTH = "AUTHORIZATION";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final String itemtype = "USERPASSWORD";
    int FAILED_TO_SEND_ALERT = 9;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String _userid = request.getParameter("_userIdE");
        log.debug("_userid :: "+_userid);
        String _user_name = request.getParameter("_NameE");
        log.debug("_user_name :: "+_user_name);
        String _user_email = request.getParameter("_EmailE");
        log.debug("_user_email :: "+_user_email);
        String _user_phone = request.getParameter("_PhoneE");
        log.debug("_user_phone :: "+_user_phone);
//        String _user_group = request.getParameter("_groupidE");

//        int groupid = 0;
//        if (_user_group != null) {
//            groupid = Integer.parseInt(_user_group);
//        }
        String result = "success";
        String message = "User updated sucessfully";

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        if (_userid == null || _user_name == null || _user_email == null || _user_phone == null) {
            result = "error";
            message = "Fill all Details!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        if (_user_name.isEmpty() == true || _user_email.isEmpty() == true || _user_phone.isEmpty() == true) {
            result = "error";
            message = "Fill all Details!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.USER_PASSWORD) != 0) {
            result = "error";
            message = "This feature is not available in this license!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        if (UtilityFunctions.isValidEmail(_user_email) == false) {
            result = "error";
            message = "Invalid email id!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        if (UtilityFunctions.isValidPhoneNumber(_user_phone) == false) {
            result = "error";
            message = "Invalid phone number!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        int retValue = -1;

        UserManagement uManagement = new UserManagement();
        AuditManagement audit = new AuditManagement();
        AuthUser olduser = uManagement.getUser(sessionId, channel.getChannelid(), _userid);
        int sts = olduser.getStatePassword();
        String passwordstatus = null;
        if (sts == SUSPEND_STATUS) {
            passwordstatus = "SUSPENDED";
        } else if (sts == ACTIVE_STATUS) {
            passwordstatus = "ACTIVE";

        } else if (sts == LOCKED_STATUS) {
            passwordstatus = "LOCKED";

        } else if (sts == REMOVE_STATUS) {
            passwordstatus = "REMOVED";

        }

        retValue = uManagement.EditUser(sessionId, channel.getChannelid(), _userid, _user_name, _user_phone, _user_email, olduser.groupid, "MSC Trustgate.com Sdn Bhd.", "140541211211", "Passport", "Business Development", "MY", "KL", "Mont Kiara", "Manager");
        String resultString = "Failure";

        if (retValue == 0) {
            resultString = "Success";
        }
        if (retValue == 0) {

            AuthUser newUser = uManagement.getUser(sessionId, channel.getChannelid(), _userid);

            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Edit User", resultString, retValue,
                    "User Management", "User ID=" + olduser.getUserId() + ",Name=" + olduser.getUserName()
                    + ",Phone=" + olduser.getPhoneNo() + ",Email=" + olduser.getEmail() + ",State=" + passwordstatus,
                    "User ID=" + newUser.getUserId() + ",Name=" + newUser.getUserName() + ",Phone=" + newUser.getPhoneNo()
                    + ",Email=" + newUser.getEmail() + ",State=" + newUser.getStatePassword(),
                    itemtype, _userid);

        } else if (retValue == -240) {
            AuthUser newUser = uManagement.getUser(sessionId, channel.getChannelid(), _userid);

            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Edit User", resultString, retValue,
                    "User Management", "User ID=" + olduser.getUserId() + ",Name=" + olduser.getUserName()
                    + ",Phone=" + olduser.getPhoneNo() + ",Email=" + olduser.getEmail() + ",State=" + passwordstatus,
                    "User ID=" + newUser.getUserId() + ",Name=" + newUser.getUserName() + ",Phone=" + newUser.getPhoneNo()
                    + ",Email=" + newUser.getEmail() + ",State=" + newUser.getStatePassword(),
                    itemtype, _userid);
            result = "error";
            message = "Duplicate EmailId not allowed";

        } else if (retValue == -242) {
            AuthUser newUser = uManagement.getUser(sessionId, channel.getChannelid(), _userid);

            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Edit User", resultString, retValue,
                    "User Management", "User ID=" + olduser.getUserId() + ",Name=" + olduser.getUserName()
                    + ",Phone=" + olduser.getPhoneNo() + ",Email=" + olduser.getEmail() + ",State=" + passwordstatus,
                    "User ID=" + newUser.getUserId() + ",Name=" + newUser.getUserName() + ",Phone=" + newUser.getPhoneNo()
                    + ",Email=" + newUser.getEmail() + ",State=" + newUser.getStatePassword(),
                    itemtype, _userid);
            result = "error";
            message = "Duplicate Phono not allowed";
        } else if (retValue == -241) {

            AuthUser newUser = uManagement.getUser(sessionId, channel.getChannelid(), _userid);

            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Edit User", resultString, retValue,
                    "User Management", "User ID=" + olduser.getUserId() + ",Name=" + olduser.getUserName()
                    + ",Phone=" + olduser.getPhoneNo() + ",Email=" + olduser.getEmail() + ",State=" + passwordstatus,
                    "User ID=" + newUser.getUserId() + ",Name=" + newUser.getUserName() + ",Phone=" + newUser.getPhoneNo()
                    + ",Email=" + newUser.getEmail() + ",State=" + newUser.getStatePassword(),
                    itemtype, _userid);
            result = "error";
            message = "Duplicate Userid not allowed";

        } else if (retValue == FAILED_TO_SEND_ALERT) {

            AuthUser newUser = uManagement.getUser(sessionId, channel.getChannelid(), _userid);

            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Edit User", resultString, retValue,
                    "User Management", "User ID=" + olduser.getUserId() + ",Name=" + olduser.getUserName()
                    + ",Phone=" + olduser.getPhoneNo() + ",Email=" + olduser.getEmail() + ",State=" + passwordstatus,
                    "User ID=" + newUser.getUserId() + ",Name=" + newUser.getUserName() + ",Phone=" + newUser.getPhoneNo()
                    + ",Email=" + newUser.getEmail() + ",State=" + newUser.getStatePassword(),
                    itemtype, _userid);

        } else {

            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Edit User", resultString, retValue,
                    "User Management", "User ID=" + olduser.getUserId() + ",Name=" + olduser.getUserName()
                    + ",Phone=" + olduser.getPhoneNo() + ",Email=" + olduser.getEmail() + ",State=" + passwordstatus,
                    "User ID=" + olduser.getUserId() + "User Name = " + olduser.getUserName()
                    + ",Phone= " + olduser.getPhoneNo() + ",Email=" + olduser.getEmail() + ",State" + passwordstatus,
                    itemtype, _userid);

            result = "error";
            message = "User updated failed!!";
            if (retValue == -6) {
                result = "error";
                message = "Duplicate Entries for User Name/Email/Phone) is not allowed!!!";
            }
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        if (retValue == 0) {
            result = "success";
            message = "User updated Successfully!!";
        } else if (retValue == FAILED_TO_SEND_ALERT) {
            result = "success";
            message = "User Updated successfully  but failed to send alert";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception e) {
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static Object createObject(Constructor constructor, Object[] arguments) {

        //System.out.println("Constructor: " + constructor.toString());
        Object object = null;

        try {
            object = constructor.newInstance(arguments);
            //System.out.println("Object: " + object.toString());
            return object;
        } catch (Exception e) {
            System.out.println(e);
            log.error("exception caught :: ",e);
        }
        return object;
    }
}
