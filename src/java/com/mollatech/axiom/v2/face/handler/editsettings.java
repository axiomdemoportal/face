/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

/**
 *
 * @author vikramsareen
 */
public class editsettings {
    public static final int SMS = 1;
    public static final int USSD = 2;
    public static final int VOICE = 3;
    public static final int Email = 4;
    public static final int RADIUS = 5;
    public static final int RootConfiguration = 6;
    public static final int Token = 7;
    public static final int UserSource = 8;
    public static final int EPIN = 9;
    public static final int FAX = 10;
    public static final int SOCIAL = 11;
        
    public static final int PREFERENCE_ONE = 1;   //primary
    public static final int PREFERENCE_TWO = 2;
    public static final int PREFERENCE_THREE = 3;

}
