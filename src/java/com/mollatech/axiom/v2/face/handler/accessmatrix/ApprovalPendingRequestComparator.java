/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.accessmatrix;

import com.mollatech.axiom.nucleus.db.Approvalsettings;
import java.util.Comparator;

/**
 *
 * @author pramodchaudhari
 */
public class ApprovalPendingRequestComparator implements Comparator<Approvalsettings>{

    @Override
    public int compare(Approvalsettings o1, Approvalsettings o2) {
       if(o1.getCreatedOn().compareTo(o2.getCreatedOn())>0)
       {
       return 1;
       
       }
       else
       {
       return -1;
       }
        
        
    }
    
}
