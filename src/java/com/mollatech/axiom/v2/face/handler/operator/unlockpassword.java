/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.operator;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import org.json.JSONException;
import org.json.JSONObject;

public class unlockpassword extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(unlockpassword.class.getName());

    public static int ACTIVE_OPERATOR = 1;
    public static int SUSPENDED_OPERATOR = 0;
    public static int LOCKED_OPERATOR = -1;
    public static int REMOVED_OPERATOR = -99;

final String itemTypeOp = "OPERATOR";    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("is started :: ");
        
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());

        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String _operId =request.getParameter("_oprid");
        log.debug("_operId :: "+_operId);
        
          Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
          log.debug("operator :: "+operator.getOperatorid());
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Unlock Password Successful!!!";
        String _value  = "0";

        int retValue = 0;
        
        OperatorsManagement oManagement = new OperatorsManagement();
        AuditManagement audit = new AuditManagement();
        Operators oprObj = oManagement.getOperatorById(channel.getChannelid(), _operId);
        int iOprStatus = oprObj.getStatus();
        if ( iOprStatus == LOCKED_OPERATOR) {
            retValue = oManagement.UnlockPassword(sessionId, channel.getChannelid(), _operId);
            log.debug("UnlockPassword :: "+retValue);
            String resultString = "ERROR";
            if(retValue == 0){
                  resultString = "SUCCESS";
                 audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                         request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                    "Unlock Password", resultString, retValue, 
                    "Operator Management",
                    "Locked Password","Unlock Password",itemTypeOp, 
                    _operId);
            }else if(retValue != 0){
                 audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                         request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                    "Unlock Password", resultString, retValue, "Operator Management",
                    "Locked Password","Failed to Unlocked Password",itemTypeOp,
                    _operId);
            }
            
        }
        else {
            result = "error";
            message = "Operator's Password is not locked!!!";
            
        }
        if (retValue != 0) {
            result = "error";
            message = "Status Update Failed!!!";
            _value = "0";
         }
        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);
        } catch (Exception ex) {
            Logger.getLogger(resendpassword.class.getName()).log(Level.SEVERE, null, ex);
            log.error("exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
        }
        
        log.info("is ended :: ");

    }
     // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
