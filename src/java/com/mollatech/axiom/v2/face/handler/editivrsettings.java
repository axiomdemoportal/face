package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.OOBMobileChannelSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class editivrsettings extends HttpServlet {
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editivrsettings.class.getName());

    //Static variables
    //public static final int IVR = 2;
    public static final int ACTIVE_STATUS = 1;
    public static final int SUSPENDED_STATUS = 1;
    public static final int PREFERENCE_ONE = 1;   //primary
    public static final int PREFERENCE_TWO = 2;
    final String itemtype = "SETTINGS";
//    private void PrintRequestParameters(HttpServletRequest req) {
//        Enumeration<String> paramNames = req.getParameterNames();
//        while (paramNames.hasMoreElements()) {
//            String paramName = paramNames.nextElement();
//            System.out.println(paramName + "=" + req.getParameter(paramName));
//        }
//    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //PrintRequestParameters(request);
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String OperatorID = operatorS.getOperatorid();

        String _channelId = channel.getChannelid();
        SettingsManagement sMngmt = new SettingsManagement();
        int retValue = -1;

        String result = "success";
        String message = "Voice Gateway Settings Update Successful!!!";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
                
            log.debug("editivrsettings::channel is::" + channel.getName());
            log.debug("editivrsettings::Operator Id is::" + operatorS.getOperatorid());
            log.debug("editivrsettings::SessionId::" + sessionId);
            log.debug("editivrsettings::RemoteAccessLogin::" + remoteaccesslogin);
            log.debug("editivrsettings::Channel Id is::" + channel.getChannelid());

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.GATEWAY_VOICE) != 0) {
            result = "error";
            message = "This feature is not available in this license!!!";

            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }

            out.print(json);
            out.flush();
            return;
        }

        String _className = null;
        String _ip = null;
        String _logConfirmation1 = null;
        String _password = null;
        String _phoneNo = null;
        String _port = null;
        Object reserve1 = null;
        Object reserve2 = null;
        Object reserve3 = null;
        String _status1 = null;
        String _userId = null;
        String _autofailover = null;
        String _retrycount = null;
        String _retryduration = null;

        String _preference = request.getParameter("_perferenceIVR");
        int _iPreference = Integer.parseInt(_preference);
        
        log.debug("editivrsettings::_preference::"+_iPreference);

        //request parameter for mobile primary
        //String _type1 = request.getParameter("_typeIVR");
        //int _type = Integer.parseInt(_type1);
        int _type = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.VOICE;
        if (_type == com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.VOICE) {
            if (_iPreference == com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE) {   //primary
                _className = request.getParameter("_classNameIVR");
                _ip = request.getParameter("_ipIVR");
                _logConfirmation1 = request.getParameter("_logConfirmationIVR");
                if (_logConfirmation1 == null) {
                    _logConfirmation1 = "false";
                } else {
                    _logConfirmation1 = "true";
                }
                _password = request.getParameter("_passwordIVR");
                _phoneNo = request.getParameter("_phoneNumberIVR");
                _port = request.getParameter("_portIVR");
                reserve1 = request.getParameter("_reserve1IVR");
                reserve2 = request.getParameter("_reserve2IVR");
                reserve3 = request.getParameter("_reserve3IVR");
                _status1 = request.getParameter("_statusIVR");
                _userId = request.getParameter("_userIdIVR");
                _autofailover = request.getParameter("_autofailoverIVR");
                _retrycount = request.getParameter("_retriesIVR");
                _retryduration = request.getParameter("_retrydurationIVR");
            } else if (_iPreference == PREFERENCE_TWO) {   //secondary
                //request parameter for mobile secondary
                _className = request.getParameter("_classNameIVRS");
                _ip = request.getParameter("_ipIVRS");
                _logConfirmation1 = request.getParameter("_logConfirmationIVRS");
                if (_logConfirmation1 == null) {
                    _logConfirmation1 = "false";
                } else {
                    _logConfirmation1 = "true";
                }
                _password = request.getParameter("_passwordIVRS");
                _phoneNo = request.getParameter("_phoneNumberIVRS");
                _port = request.getParameter("_portIVRS");
                reserve1 = request.getParameter("_reserve1IVRS");
                reserve2 = request.getParameter("_reserve2IVRS");
                reserve3 = request.getParameter("_reserve3IVRS");
                _status1 = request.getParameter("_statusIVRS");
                _userId = request.getParameter("_userIdIVRS");
                _retrycount = request.getParameter("_retriesIVRS");
                _retryduration = request.getParameter("_retrydurationIVRS");
            }

            if (_className == null && _ip == null && _port == null
                    && _className.isEmpty() == true && _ip.isEmpty() == true && _port.isEmpty() == true) {
                result = "error";
                message = "Please provide neccessary details!!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);

                } catch (Exception e) {
                } finally {
                    out.print(json);
                    out.flush();
                }

            }
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _type, _iPreference);

            OOBMobileChannelSettings mobile = null;
            boolean bAddSetting = false;

            if (settingsObj == null) {
                mobile = new OOBMobileChannelSettings();
                mobile.setChannelId(_channelId);
                mobile.setType(_type);
                mobile.setPreference(_iPreference);
                bAddSetting = true;
            } else {
                mobile = (OOBMobileChannelSettings) settingsObj;
            }

            mobile.setClassName(_className);
            mobile.setIp(_ip);
            if (_logConfirmation1 != null) {
                boolean _logConfirmation = Boolean.parseBoolean(_logConfirmation1);
                mobile.setLogConfirmation(_logConfirmation);
            }
            mobile.setPassword(_password);
            mobile.setPhoneNumber(_phoneNo);
            if (_port != null || _port.isEmpty() == true) {
                int _port1 = Integer.parseInt(_port);
                mobile.setPort(_port1);
            }
            mobile.setReserve1(reserve1);
            mobile.setReserve2(reserve2);
            mobile.setReserve3(reserve3);
            if (_status1 != null) {
                int _status = Integer.parseInt(_status1);
                mobile.setStatus(_status);
            }
            mobile.setUserid(_userId);
            if (_autofailover != null) {
                int _iAutofailover = Integer.parseInt(_autofailover);
                mobile.setAutofailover(_iAutofailover);
            }

            if (_retrycount != null) {
                int _iRetrycount = Integer.parseInt(_retrycount);
                mobile.setRetrycount(_iRetrycount);
            }

            if (_retryduration != null) {
                int _iRetryDuration = Integer.parseInt(_retryduration);
                mobile.setRetryduration(_iRetryDuration);
            }

            AuditManagement audit = new AuditManagement();

            if (bAddSetting == true) {
                retValue = sMngmt.addSetting(sessionId, _channelId, _type, _iPreference, mobile);
                log.debug("editivrsettings::addSetting::"+retValue);
                String resultString = "ERROR";
                if (retValue == 0) {
                    resultString = "SUCCESS";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add IVR Voice Setting", resultString, retValue, "Setting Management",
                            "", " ChannelId" + mobile.getChannelId() + " ClassName" + mobile.getClassName() + " ImplementationJAR"
                            + " Ip" + mobile.getIp() + " Password=*****" + " PhoneNumber" + mobile.getPhoneNumber() + " Userid" + mobile.getUserid()
                            + " Autofailover" + mobile.getAutofailover() + " Port" + mobile.getPort() + " Preference" + mobile.getPreference()
                            + " Reserve1" + mobile.getReserve1() + " Reserve2" + mobile.getReserve2() + " Reserve3" + mobile.getReserve3()
                            + " Retrycount" + mobile.getRetrycount() + " Status" + mobile.getStatus() + " Type" + mobile.getType()
                            + " LogConfirmation" + mobile.isLogConfirmation() + " Retryduration" + mobile.getRetryduration(),
                            itemtype, _channelId);
                } else if (retValue != 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add IVR Voice Setting", resultString, retValue, "Setting Management",
                            "", "Failed to add IVR settings", itemtype, _channelId);
                }
            } else {
                OOBMobileChannelSettings oldIvrObj = (OOBMobileChannelSettings) sMngmt.getSetting(sessionId, _channelId, _type, _iPreference);
                retValue = sMngmt.changeSetting(sessionId, _channelId, _type, _iPreference, settingsObj, mobile);
                log.debug("editivrsettings::changeSetting::"+retValue);
                String resultString = "ERROR";
                if (retValue == 0) {
                    resultString = "Success";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Edit IVR Voice Setting", resultString, retValue, "Setting Management",
                            "ChannelId" + oldIvrObj.getChannelId() + " ClassName" + oldIvrObj.getClassName()
                            + " Ip" + oldIvrObj.getIp() + " Password=*****" + " PhoneNumber" + oldIvrObj.getPhoneNumber() + " Userid" + oldIvrObj.getUserid()
                            + " Autofailover" + oldIvrObj.getAutofailover() + " Port" + oldIvrObj.getPort() + " Preference" + oldIvrObj.getPreference()
                            + " Reserve1" + oldIvrObj.getReserve1() + " Reserve2" + oldIvrObj.getReserve2() + " Reserve3" + oldIvrObj.getReserve3()
                            + " Retrycount" + oldIvrObj.getRetrycount() + " Status" + oldIvrObj.getStatus() + " Type" + oldIvrObj.getType()
                            + " LogConfirmation" + oldIvrObj.isLogConfirmation(),
                            "ChannelId" + mobile.getChannelId() + " ClassName" + mobile.getClassName() + " ImplementationJAR"
                            + " Ip" + mobile.getIp() + " Password=*****" + " PhoneNumber" + mobile.getPhoneNumber() + " Userid" + mobile.getUserid()
                            + " Autofailover" + mobile.getAutofailover() + " Port" + mobile.getPort() + " Preference" + mobile.getPreference()
                            + " Reserve1" + mobile.getReserve1() + " Reserve2" + mobile.getReserve2() + " Reserve3" + mobile.getReserve3()
                            + " Retrycount" + mobile.getRetrycount() + " Status" + mobile.getStatus() + " Type" + mobile.getType()
                            + " LogConfirmation" + mobile.isLogConfirmation(),
                            itemtype, _channelId);
                } else if (retValue != 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Edit IVR Voice Setting", resultString, retValue, "Setting Management",
                            "ChannelId" + oldIvrObj.getChannelId() + " ClassName" + oldIvrObj.getClassName()
                            + " Ip" + oldIvrObj.getIp() + " Password=*****" + " PhoneNumber" + oldIvrObj.getPhoneNumber() + " Userid" + oldIvrObj.getUserid()
                            + " Autofailover" + oldIvrObj.getAutofailover() + " Port" + oldIvrObj.getPort() + " Preference" + oldIvrObj.getPreference()
                            + " Reserve1" + oldIvrObj.getReserve1() + " Reserve2" + oldIvrObj.getReserve2() + " Reserve3" + oldIvrObj.getReserve3()
                            + " Retrycount" + oldIvrObj.getRetrycount() + " Status" + oldIvrObj.getStatus() + " Type" + oldIvrObj.getType()
                            + " LogConfirmation" + oldIvrObj.isLogConfirmation(),
                            "Failed To Edit IVR Voice Setting",
                            itemtype, _channelId);
                }

            }
        }

        if (retValue != 0) {
            result = "error";
            message = "IVR Voice Gateway Settings Update Failed!!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception e) {
        } finally {
            out.print(json);
            out.flush();
        }

    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";

    }// </editor-fold>
}
