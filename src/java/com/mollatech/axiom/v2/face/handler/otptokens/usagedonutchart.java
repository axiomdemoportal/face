/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.otptokens;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.face.common.donut;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nilesh
 */
public class usagedonutchart extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(usagedonutchart.class.getName());

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel"+channel.getName());
        String channelID = channel.getChannelid();
        log.debug("channelID :: "+channelID);
                
        try {
            String _type = request.getParameter("_changeCategory");
            log.debug("_type :: "+_type);
            String _startdate = request.getParameter("_startdate");
            log.debug("_startdate :: "+_startdate);
            String _enddate = request.getParameter("_enddate");
            log.debug("_enddate :: "+_enddate);
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date startDate = null;
            if (_startdate != null && !_startdate.isEmpty()) {
                startDate = (Date) formatter.parse(_startdate);
            }
            Date endDate = null;
            if (_enddate != null && !_enddate.isEmpty()) {
                endDate = (Date) formatter.parse(_enddate);
                endDate.setHours(23);
                endDate.setMinutes(59);
                endDate.setSeconds(59);
                
            }
            if (_type == null) {
                return;
            }
            AuditManagement auditObj = new AuditManagement();
            ArrayList<donut> sample = new ArrayList<donut>();
            if (_type.equals("VERIFY OTP")) {
                int success = auditObj.searchUsageCount(channel.getChannelid(), _type, 0, startDate, endDate);
                int expired = auditObj.searchUsageCount(channel.getChannelid(), _type, -9, startDate, endDate);
                int consume = auditObj.searchUsageCount(channel.getChannelid(), _type, -8, startDate, endDate);
                int notfound = auditObj.searchUsageCount(channel.getChannelid(), _type, -17, startDate, endDate);
                int locked = auditObj.searchUsageCount(channel.getChannelid(), _type, -22, startDate, endDate);
                int error = auditObj.searchUsageCount(channel.getChannelid(), _type, -1, startDate, endDate);
                sample.add(new donut(success, "Success"));
                sample.add(new donut(expired, "Expired"));
                sample.add(new donut(consume, "Failed"));
                sample.add(new donut(notfound, "Not Found"));
                sample.add(new donut(locked, "Locked"));
                sample.add(new donut(error, "ERROR"));
            }else if (_type.equals("VERIFY SIGNATURE OTP")) {
                int success = auditObj.searchUsageCount(channel.getChannelid(), _type, 0, startDate, endDate);
                int expired = auditObj.searchUsageCount(channel.getChannelid(), _type, -9, startDate, endDate);
                int internalerror = auditObj.searchUsageCount(channel.getChannelid(), _type, -2, startDate, endDate);
                int invalidsession = auditObj.searchUsageCount(channel.getChannelid(), _type, -14, startDate, endDate);
                int consume = auditObj.searchUsageCount(channel.getChannelid(), _type, -8, startDate, endDate);
                int notfound = auditObj.searchUsageCount(channel.getChannelid(), _type, -17, startDate, endDate);
                int locked = auditObj.searchUsageCount(channel.getChannelid(), _type, -22, startDate, endDate);
                int error = auditObj.searchUsageCount(channel.getChannelid(), _type, -1, startDate, endDate);
                sample.add(new donut(success, "Success"));
                sample.add(new donut(expired, "Expired"));
                sample.add(new donut(internalerror, "internalerror"));
                sample.add(new donut(invalidsession, "Invalid Session"));
                sample.add(new donut(consume, "Alredy Consume"));
                sample.add(new donut(notfound, "Not Found"));
                sample.add(new donut(locked, "Locked"));
                sample.add(new donut(error, "ERROR"));
            }else if (_type.equals("Verify Password")) {
                int success = auditObj.searchUsageCount(channel.getChannelid(), _type, 0, startDate, endDate);
                int expired = auditObj.searchUsageCount(channel.getChannelid(), _type, -9, startDate, endDate);
                int consume = auditObj.searchUsageCount(channel.getChannelid(), _type, -8, startDate, endDate);
                int notfound = auditObj.searchUsageCount(channel.getChannelid(), _type, -17, startDate, endDate);
                int locked = auditObj.searchUsageCount(channel.getChannelid(), _type, -22, startDate, endDate);
                int error = auditObj.searchUsageCount(channel.getChannelid(), _type, -1, startDate, endDate);
                sample.add(new donut(success, "Success"));
                sample.add(new donut(expired, "Expired"));
                sample.add(new donut(consume, "Alredy Consume"));
                sample.add(new donut(notfound, "Not Found"));
                sample.add(new donut(locked, "Locked"));
                sample.add(new donut(error, "ERROR"));
            }else if (_type.equals("ChallengeResponse Management")) {
                int success = auditObj.searchUsageCount(channel.getChannelid(), _type, 0, startDate, endDate);
                int error = auditObj.searchUsageCount(channel.getChannelid(), _type, -1, startDate, endDate);
                sample.add(new donut(success, "Success"));
                sample.add(new donut(error, "ERROR"));
            }else if (_type.equals("PKI Sign Data")) {
                int success = auditObj.searchUsageCount(channel.getChannelid(), _type, 0, startDate, endDate);
                int error = auditObj.searchUsageCount(channel.getChannelid(), _type, -1, startDate, endDate);
                sample.add(new donut(success, "Success"));
                sample.add(new donut(error, "ERROR"));
            }else if (_type.equals("OcrDocumentation")) {
                int success = auditObj.searchUsageCountForHdfc(channel.getChannelid(), _type, 0, startDate, endDate);
                int error = auditObj.searchUsageCountForHdfc(channel.getChannelid(), _type, -1, startDate, endDate);
                sample.add(new donut(success, "Success"));
                sample.add(new donut(error, "ERROR"));
            }
            Gson gson = new Gson();

            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<donut>>() {
            }.getType());

            JsonArray jsonArray = element.getAsJsonArray();
            out.print(jsonArray);
           
        } catch (Exception ex) {
            log.error("exception caught :: ",ex);
        }finally{
             out.close();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
