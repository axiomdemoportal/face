/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.axiom.v2.face.handler.group;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.GroupManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ISOManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Manoj Sherkhane
 */
public class makeItDefault extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(makeItDefault.class.getName());
public static final int SUCCESS = 0;
    public static final int FAILURE = -1;
     final String itemType = "GROUPS";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         response.setContentType("application/json");
         
         log.info("is started :: ");

        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Group set as Default successfully!!!";

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);

        JSONObject json = new JSONObject();
        int retValue = 0;
        String _unitId = request.getParameter("_unitIdS");
        String _oldunitNameE = request.getParameter("_oldGroupNameS");
        log.debug("_oldunitNameE :: "+_oldunitNameE);
        
        int ioldunitId = Integer.parseInt(_unitId);
        log.debug("_unitId :: "+ioldunitId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getChannelid());
        GroupManagement management = new GroupManagement();
       
       int res = management.makeItDefault(sessionId, channel.getChannelid(), ioldunitId);
       log.debug("makeItDefault :: "+res);
       
       if(res == SUCCESS){
            result = "success";
            message = "Group set as default successfully!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
           
       }else {
            result = "error";
            message = "failed to make group as default!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        AuditManagement audit = new AuditManagement();

//        retValue = unMngt.EditUnits(sessionId, channel.getChannelid(),iunitId, _unit_name);
        String resultString = "ERROR";

        if (retValue == 0) {
            resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                    "Mark as Default", resultString, retValue, "Group Management",
                    "Name=" + _oldunitNameE,
                    "Default Group = " + _oldunitNameE,
                    itemType, channel.getChannelid()
            );

        }
        else  {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                    "Mark as Default", resultString, retValue, "Group Management",
                    "Name=" + _oldunitNameE,
                    "Failed To Make group as default",
                    itemType, channel.getChannelid()
                    );
            result = "error";

        }

        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            
           log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            out.close();
        }
        
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
