/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.otptokens;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Otptokens;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.AuthorizationManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.connector.access.controller.ApprovalSetting;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class changetokentype extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(changetokentype.class.getName());
    
    public static final int HW_MINI_TOKEN = 1;
    public static final int HW_CR_TOKEN = 2;
    final int SW_PC_TOKEN = 3;

    final int SOFTWARE_TOKEN = 1;
    final int SW_WEB_TOKEN = 1;
    final int SW_MOBILE_TOKEN = 2;
    final int HARDWARE_TOKEN = 2;
    final int OOB_TOKEN = 3;
    final int OOB__SMS_TOKEN = 1;
    final int OOB__VOICE_TOKEN = 2;
    final int OOB__USSD_TOKEN = 3;
    final int OOB__EMAIL_TOKEN = 4;
    final int OOB__PUSH_TOKEN = 5;
    final String itemType = "OTPTOKENS";
    public static final int TOKEN_STATUS_UNASSIGNED = -10;
    final String itemTypeAUTH = "AUTHORIZATION";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String _serialnumber = request.getParameter("_serialnumber");
        log.debug("_serialnumber :: "+_serialnumber);
        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS);

        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String _userid = request.getParameter("_userid");
        log.debug("_userid :: "+_userid);
        String _subcategory = request.getParameter("_subcategory");
        log.debug("_subcategory :: "+_subcategory);
        int subCategory = 0;
        if (_subcategory != null) {
            subCategory = Integer.parseInt(_subcategory);
        }

        String _category = request.getParameter("_category");
        log.debug("_category :: "+_category);
        int category = 0;
        if (_category != null) {
            category = Integer.parseInt(_category);
        }
        String _value = "";
        String result = "success";
        String _message = "Token Type Changed Successfully!!!";

        String message = null;

        if (subCategory == OOB__SMS_TOKEN) {
            _value = "SMS";
       
        } else if (subCategory == OOB__VOICE_TOKEN) {
            _value = "VOICE";
               
        } else if (subCategory == OOB__USSD_TOKEN) {
            _value = "USSD";
             
        } else if (subCategory == OOB__EMAIL_TOKEN) {
            _value = "EMAIL";
             
        } else if (subCategory == OOB__PUSH_TOKEN) {
            _value = "PUSH";             
        }
        
        OTPTokenManagement oManagement = new OTPTokenManagement(channel.getChannelid());
        int istatus = oManagement.getStatus(sessionId, channel.getChannelid(), _userid, category, subCategory);
        log.debug("getStatus :: "+istatus);
  
        if (istatus == TOKEN_STATUS_UNASSIGNED) {
            result = "error";
            _message = "Token Type couldn't be changed as token is not assigned!!!";
            try {json.put("_result", result);
            json.put("_message", _message);
            }catch(Exception e){
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;

        }



        if (_userid == null) {
            result = "error";
            _message = "User not available in system!!!";
            try { json.put("_result", result);
            json.put("_message", _message);
            }catch(Exception e){
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }

        int retValue = -1;


        AuditManagement audit = new AuditManagement();

        Otptokens otpObj = oManagement.getOtpObjByUserId(sessionId, channel.getChannelid(), _userid, category);
           String strNewCategory = "";
        String strNewSubCategory = "";
        if (category == SOFTWARE_TOKEN) {
            strNewCategory = "SOFTWARE_TOKEN";
            if (subCategory == SW_WEB_TOKEN) {
                strNewSubCategory = "SW_WEB_TOKEN";
            } else if (subCategory == SW_MOBILE_TOKEN) {
                strNewSubCategory = "SW_MOBILE_TOKEN";
            } else if (subCategory == SW_PC_TOKEN) {
                strNewSubCategory = "SW_PC_TOKEN";
            }

        } else if (category == HARDWARE_TOKEN) {
            strNewCategory = "HARDWARE_TOKEN";
            if (subCategory == HW_MINI_TOKEN) {
                strNewSubCategory = "HW_MINI_TOKEN";
            } else if (subCategory == HW_CR_TOKEN) {
                strNewSubCategory = "HW_CR_TOKEN";
            }
        } else if (category == OOB_TOKEN) {
            strNewCategory = "OOB_TOKEN";
            if (subCategory == OOB__SMS_TOKEN) {
                strNewSubCategory = "OOB__SMS_TOKEN";
            } else if (subCategory == OOB__USSD_TOKEN) {
                strNewSubCategory = "OOB__USSD_TOKEN";
            } else if (subCategory == OOB__VOICE_TOKEN) {
                strNewSubCategory = "OOB__VOICE_TOKEN";
            } else if (subCategory == OOB__EMAIL_TOKEN) {
                strNewSubCategory = "OOB__EMAIL_TOKEN";
            } else if (subCategory == OOB__PUSH_TOKEN) {
                strNewSubCategory = "OOB__PUSH_TOKEN";
            }
        }
           UserManagement uMngt = new UserManagement();
         AuthUser uObj = uMngt.getUser(sessionId, channel.getChannelid(), _userid);
       
        String _approvalId = request.getParameter("_approvalId");
         SettingsManagement sMngmt = new SettingsManagement();
        Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
        ChannelProfile chSettingObj = null;
        if (settingsObj != null) {
            chSettingObj = (ChannelProfile) settingsObj;
        }
        String _unitId = request.getParameter("_unitId");
        log.debug("_unitId :: "+_unitId);
        if (_unitId != null) {
            if (chSettingObj != null) {

                if (chSettingObj.authorizationunit == 1) {
                    int iUnitId = Integer.parseInt(_unitId);
                    if (iUnitId != operatorS.getUnits()) {
                        result = "error";
                        message = "Sorry, Must be marked by same branch operator!!!";
                        try {
                            json.put("_result", result);
                            json.put("_message", message);
                        } catch (Exception e) {
                           log.error("exception caught :: ",e);
                        }
                        out.print(json);
                        out.flush();
                        return;
                    }
                }
            }
        }
         OperatorsManagement oprMngt = new OperatorsManagement();
     
        AuthorizationManagement auth = new AuthorizationManagement();
        String strOpName = "-";
        String struserName = "-";
        String strAction = "-";
        int iapprovalID = -1;
//        ApprovalSetting approvalSetting = null;
        if (_approvalId != null) {
                if (operatorS.getRoleid() != 6) {
                if (operatorS.getRoleid() >= 3) {
                    result = "error";
                    message = "Sorry, But you don't have permissions for this action!!!";
                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("exception caught :: ",e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
            }

          String _markerID = request.getParameter("_markerID");
          log.debug("_markerID :: "+_markerID);
             Operators op = oprMngt.getOperatorById(channel.getChannelid(), _markerID);
            if (op != null) {
                strOpName = op.getName();
            }

            if (uObj != null) {
                struserName = uObj.getUserName();
            }

        }
            retValue = oManagement.ChangeTokenType(sessionId, channel.getChannelid(), _userid, category, subCategory);
            log.debug("ChangeTokenType :: "+retValue);



        
        int iCategory = otpObj.getCategory();
        int isubCategory = otpObj.getSubcategory();
         String strCategory = "";
        String strSubCategory = "";
        if (iCategory == SOFTWARE_TOKEN) {
            strCategory = "SOFTWARE_TOKEN";
            if (isubCategory == SW_WEB_TOKEN) {
                strSubCategory = "SW_WEB_TOKEN";
            } else if (isubCategory == SW_MOBILE_TOKEN) {
                strSubCategory = "SW_MOBILE_TOKEN";
            } else if (isubCategory == SW_PC_TOKEN) {
                strSubCategory = "SW_PC_TOKEN";
            }

        } else if (iCategory == HARDWARE_TOKEN) {
            strCategory = "HARDWARE_TOKEN";
            if (isubCategory == HW_MINI_TOKEN) {
                strSubCategory = "HW_MINI_TOKEN";
            } else if (isubCategory == HW_CR_TOKEN) {
                strSubCategory = "HW_CR_TOKEN";
            }
        } else if (iCategory == OOB_TOKEN) {
            strCategory = "OOB_TOKEN";
            if (isubCategory == OOB__SMS_TOKEN) {
                strSubCategory = "OOB__SMS_TOKEN";
            } else if (isubCategory == OOB__USSD_TOKEN) {
                strSubCategory = "OOB__USSD_TOKEN";
            } else if (isubCategory == OOB__VOICE_TOKEN) {
                strSubCategory = "OOB__VOICE_TOKEN";
            } else if (isubCategory == OOB__EMAIL_TOKEN) {
                strSubCategory = "OOB__EMAIL_TOKEN";
            } else if (isubCategory == OOB__PUSH_TOKEN) {
                strSubCategory = "OOB__PUSH_TOKEN";
            }
        }
        

        String resultString = "ERROR";
        
        
    
        if (retValue == 0) {
            resultString = "SUCCESS";
            String entity = "User phone no:"+ uObj.getPhoneNo()+"User Name:"+ uObj.getUserName();    
            audit.AddAuditTrailWithEntity(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Change Token Type", resultString, retValue,
                    "Token Management", "Category = " + strCategory + " Sub Category =" + strSubCategory,
                    "Category = " + strNewCategory + " Sub Category =" + strNewSubCategory, itemType,entity,
                    _userid);
   


            if (category == OTPTokenManagement.OOB_TOKEN) {
//                AuthUser user = null;
//                UserManagement userObj = new UserManagement();
//                user = userObj.getUser(sessionId, channel.getChannelid(), _userid);
                Templates templates = null;
                ByteArrayInputStream bais = null;
                String subject = null;
                TemplateManagement tManagement = new TemplateManagement();

                if (subCategory != OTPTokenManagement.OOB__EMAIL_TOKEN) {
                    templates = tManagement.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.MOBILE_OOB_OTP_CHANGE_TOKEN_TEMPLATE);
                    bais = new ByteArrayInputStream(templates.getTemplatebody());
                    message = (String) TemplateUtils.deserializeFromObject(bais);
                } else {
                    templates = tManagement.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.EMAIL_OOB_OTP_CHANGE_TOKEN_TEMPLATE);
                    bais = new ByteArrayInputStream(templates.getTemplatebody());
                    message = (String) TemplateUtils.deserializeFromObject(bais);
                    subject = templates.getSubject();
                }

                if (subCategory != OTPTokenManagement.OOB__EMAIL_TOKEN) {
                    //added for template based messages
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                    if (message != null) {
                        Date d = new Date();

                        message = message.replaceAll("#name#", uObj.getUserName());
                        message = message.replaceAll("#channel#", channel.getName());
                        message = message.replaceAll("#datetime#", sdf.format(d));
                        if (subCategory == OTPTokenManagement.OOB__SMS_TOKEN) {
                            message = message.replaceAll("#tokentype#", "SMS");
                        } else if (subCategory == OTPTokenManagement.OOB__USSD_TOKEN) {
                            message = message.replaceAll("#tokentype#", "USSD");
                        } else if (subCategory == OTPTokenManagement.OOB__VOICE_TOKEN) {
                            message = message.replaceAll("#tokentype#", "Voice");
                        }
                    }
                } else if (subCategory == OTPTokenManagement.OOB__EMAIL_TOKEN) {
                    //added for template based messages
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                    if (message != null) {
                        Date d = new Date();

                        message = message.replaceAll("#name#", uObj.getUserName());
                        message = message.replaceAll("#channel#", channel.getName());
                        message = message.replaceAll("#datetime#", sdf.format(d));
                        message = message.replaceAll("#tokentype#", "EMAIL");
                        message = message.replaceAll("#email#", uObj.getEmail());
                        subject = subject.replaceAll("#name#", uObj.getUserName());
                        subject = subject.replaceAll("#channel#", channel.getName());
                        subject = subject.replaceAll("#datetime#", sdf.format(d));
                        subject = subject.replaceAll("#tokentype#", "EMAIL");
                    }

                }

                SendNotification send = new SendNotification();
                //     AXIOMStatus axiomStatus = send.SendOnMobile(channel.getChannelid(), user.getPhoneNo(), templatebody, 1, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));

                if (message != null && uObj != null) {
                    Date d = new Date();

                    if (category == SOFTWARE_TOKEN) {
                        if (uObj.phoneNo != null) {
                            if(templates.getStatus() == tManagement.ACTIVE_STATUS){
                            send.SendOnMobileNoWaiting(channel.getChannelid(), uObj.phoneNo, message, SendNotification.SMS, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }
                        }
                    } else {
                        if (subCategory == OOB__SMS_TOKEN || subCategory == OOB__VOICE_TOKEN || subCategory == OOB__USSD_TOKEN) {
                            if (uObj.phoneNo != null) {
                                if(templates.getStatus() == tManagement.ACTIVE_STATUS){
                                send.SendOnMobileNoWaiting(channel.getChannelid(), uObj.phoneNo, message, subCategory, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                            }
                        } else {
                            if (uObj.email != null) {
                                if(templates.getStatus() ==tManagement.ACTIVE_STATUS){
                                send.SendEmail(channel.getChannelid(), uObj.email, subject, message, null, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                            }
                        }
                    }
                }

            }
        } else  {
            String entity = "User phone no:"+ uObj.getPhoneNo()+"User Name:"+ uObj.getUserName();    
            audit.AddAuditTrailWithEntity(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Change Token Type", resultString,
                    retValue, "Token Management", "Category = " + strCategory + " Sub Category =" + strSubCategory, "",
                    itemType,entity, _userid);
            result = "error";
            _message = "Token Type change request failed!!!";
            try { json.put("_result", result);
            json.put("_message", _message);
            }catch(Exception e){
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        try {
            json.put("_result", result);
            json.put("_message", _message);
            json.put("_value", _value);

        }catch(Exception e){
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
        }
         log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static Object createObject(Constructor constructor, Object[] arguments) {

        //System.out.println("Constructor: " + constructor.toString());
        Object object = null;

        try {
            object = constructor.newInstance(arguments);
            //System.out.println("Object: " + object.toString());
            return object;
        } catch (Exception e) {
            log.error("exception caught :: ",e);
        }
       
        return object;
        
    }
}
