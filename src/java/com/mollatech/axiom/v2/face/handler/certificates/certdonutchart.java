/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.certificates;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.face.common.donut;
import com.mollatech.axiom.nucleus.db.Certificates;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mollatech1
 */
public class certdonutchart extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(certdonutchart.class.getName());

    final int iActive = 1;
    final int iRevoked = -5;
    final int iExpired = -10;
    final int iGoing_To_Expire = -1;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("is started :: ");
        PrintWriter out = response.getWriter();
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel ::" + channel.getName());
        String channelID = channel.getChannelid();
        log.debug("channelID ::"+channelID);
        String _startdate = request.getParameter("_startdate");
        log.debug("_startdate ::" + _startdate);
        String _enddate   = request.getParameter("_enddate");
        log.debug("_enddate::"+_enddate);
        

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date startDate = null;
        Date endDate = null;
        Certificates[] arrCobj = null;
        try {
            if (_startdate != null && !_startdate.isEmpty()) {
            startDate = (Date) formatter.parse(_startdate);
            }
            
            if (_enddate != null && !_enddate.isEmpty()) {
                endDate = (Date) formatter.parse(_enddate);
            }
            CertificateManagement cObj = new CertificateManagement();
            int Active = cObj.getCertCountByStatusV2(channelID, iActive,startDate,endDate);
            log.debug("getCertCountByStatusV2::"+Active);
            int Revoked = cObj.getCertCountByStatusV2(channelID, iRevoked,startDate,endDate);
            log.debug("getCertCountByStatusV2::"+Revoked);
            int Expired = cObj.getCertCountByStatusV2(channelID, iExpired,startDate,endDate);
            log.debug("getCertCountByStatusV2::"+Expired);
            //new change
            //int Going_To_Expire = cObj.getCertCountByStatus(channelID, iGoing_To_Expire);
            arrCobj = cObj.getExpireSoonCertificate(channelID);
            int Going_To_Expire = 0;
            if(arrCobj != null){
                Going_To_Expire = arrCobj.length;
            }
            ArrayList<donut> sample = new ArrayList<donut>();
            sample.add(new donut(Active, "Active"));
            sample.add(new donut(Revoked, "Revoked"));
            sample.add(new donut(Expired, "Expired"));
            sample.add(new donut(Going_To_Expire, "Going To Expire"));

//            for (int i = 0; i < sample.size(); i++) {
//                System.out.println(sample.get(i));
//            }
            Gson gson = new Gson();

            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<donut>>() {
            }.getType());

            JsonArray jsonArray = element.getAsJsonArray();
            //response.setContentType("application/json");
            out.print(jsonArray);


        }catch(Exception e){
            
            log.error("exception caught :: ",e);
        }finally {
            out.close();
        }
         log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
