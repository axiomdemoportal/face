package com.mollatech.axiom.v2.face.handler.operator.errormessages;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Errormessages;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.ErrorMessageUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ErrorMessageManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class changeemessagestatus extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(changeemessagestatus.class.getName());
    final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;
    final String itemType = "ERRORS";

   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         response.setContentType("application/json");
         
         log.info("is started :: ");

        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Error Message updated successfully!!!";
        String _value = "Active";
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        

        String operatorId = operatorS.getOperatorid();
        log.debug("operatorId :: "+operatorId);

        JSONObject json = new JSONObject();
        int retValue = -1;
        String _tid = request.getParameter("_templateid");
        int _templateid = Integer.valueOf(_tid);
        log.debug("_templateid :: "+_templateid);
        String _status = request.getParameter("_status");
        int status = Integer.parseInt(_status);
         log.debug("status :: "+status);

        Errormessages checkObj = null;

        if (_tid == null || _status == null) {
            result = "error";
            message = "Invalid Error Message Details!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        if (status == ACTIVE_STATUS) {
            _value = "Active ";
        } else if (status == SUSPEND_STATUS) {
            _value = "Suspended ";
        }
        SessionFactoryUtil suTem = new SessionFactoryUtil(SessionFactoryUtil.errormessages);
        Session sTem = suTem.openSession();
        ErrorMessageUtils tmpU = new ErrorMessageUtils(suTem, sTem);
        Errormessages tObj = tmpU.loadtemplate(sessionId, _templateid);
        sTem.close();
        suTem.close();

        message = _value + message;

        ErrorMessageManagement tManagement = new ErrorMessageManagement();
        AuditManagement audit = new AuditManagement();

        retValue = tManagement.ChangeStatus(sessionId, channel.getChannelid(), _templateid, status);

        String resultString = "ERROR";
        if (retValue == 0) {
            resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), 
                    "Change Status", resultString, retValue,
                    "Error Message Management",
                    "Old Status=" + tObj.getStatus(), "New Status=" + _value,
                    itemType,
                    _tid);
        }

         else if (retValue != 0) { 
              audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), 
                    "Change Status", resultString, retValue,
                    "Error Message Management",
                    "Old Status=" + tObj.getStatus(), "New Status=" + _value,
                    itemType, 
                    _tid);
           
            result = "error";
            message = "Status update failed!!!";
        }

        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);

        } catch (Exception e) {
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
        }
        
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
