/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.certificates;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.RootCertificateSettings;
import static com.mollatech.axiom.v2.face.handler.certificates.savepfxdetails.log;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author pramodchaudhari
 */
public class getcaConnectorCert extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            int type=0;
           if(request.getParameter("type")!=null)
           {
         
           type=Integer.parseInt(request.getParameter("type"));
           }
            log.info("is started :: ");
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel::" + channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId::" + sessionId);
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("remoteaccesslogin::" + remoteaccesslogin);
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operatorS::" + operatorS.getOperatorid());

            AuditManagement audit = new AuditManagement();
            JSONObject json = new JSONObject();
            SettingsManagement sManagement = new SettingsManagement();
            RootCertificateSettings settingsObj=null;
            if(type==1){
             settingsObj = (RootCertificateSettings) sManagement.getSetting(sessionId, channel.getChannelid(), SettingsManagement.RootConfiguration, SettingsManagement.PREFERENCE_ONE);
            }
            else
            { 
           settingsObj = (RootCertificateSettings) sManagement.getSetting(sessionId, channel.getChannelid(), SettingsManagement.RootConfiguration, SettingsManagement.PREFERENCE_TWO);
            
            }
           // RootCertificateSettings certSetting = settingsObj;
            
//            settingsObj = new RootCertificateSettings();
            if (settingsObj != null) {
                try {
                    KeyStore keystore = KeyStore.getInstance("pkcs12", "BC");
                    System.out.println("Reserv1"+(String) settingsObj.getReserve1());
                    System.out.println("Reserv2"+(String) settingsObj.getReserve2());
                    System.out.println("Reserv3"+(String) settingsObj.getReserve3());
                    keystore.load(new FileInputStream((String) settingsObj.getReserve1()), ((String) settingsObj.getReserve2()).toCharArray());
                    Certificate cert = keystore.getCertificate((String) settingsObj.getReserve3());
                    javax.security.cert.X509Certificate certifts = javax.security.cert.X509Certificate.getInstance(cert.getEncoded());
                    if (certifts != null) {
                        json.put("_result", "success");
                        json.put("_srnoCertifiacte",""+ certifts.getSerialNumber());
                        json.put("_algoname",""+ certifts.getSigAlgName());
                        json.put("_issuerdn",""+ certifts.getIssuerDN());
                        json.put("_notafter", ""+certifts.getNotAfter());
                        json.put("_notbefore", ""+certifts.getNotBefore());
                        json.put("_subjectdn",""+ certifts.getSubjectDN());
                        json.put("_version", ""+certifts.getVersion());
                        out.print(json);
                        return;
                    } else {
                        json.put("_result", "errror");
                        json.put("_message", "No file Uploaded");
                        out.print(json);
                        return;
                    }

                } catch (Exception ex) {
                       ex.printStackTrace();
                        json.put("_result", "errror");
                        json.put("_message", "Uploaded file format not correct..!!!");
                        out.print(json);
                        return;
                   
                }
            } else {
                try {
                    json.put("_result", "errror");
                } catch (JSONException ex) {
                    Logger.getLogger(getcaConnectorCert.class.getName()).log(Level.SEVERE, null, ex);
                }
                json.put("_message", "Please add settings");

                out.print(json);
                return;

            }

        } catch (JSONException ex) {
            Logger.getLogger(getcaConnectorCert.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
