package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.RootCertificateSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class editcasettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editcasettings.class.getName());

    //Static variables
    public static final int CertificateSettings = 6;
    public static final int PREFERENCE_ONE = 1;   //primary
    final String itemtype = "SETTINGS";

    private void PrintRequestParameters(HttpServletRequest req) {
        Enumeration<String> paramNames = req.getParameterNames();
        while (paramNames.hasMoreElements()) {
            String paramName = paramNames.nextElement();
            //System.out.println(paramName + "=" + req.getParameter(paramName));
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        PrintRequestParameters(request);
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        String OperatorID = operatorS.getOperatorid();

        String _channelId = channel.getChannelid();
        SettingsManagement sMngmt = new SettingsManagement();
        int retValue = -1;
        
            log.debug("channel :: " + channel.getName());
            log.debug("operatorS :: " + operatorS.getOperatorid());
            log.debug("sessionId :: " + sessionId);
            log.debug("remoteaccesslogin :: " + remoteaccesslogin);
            log.debug(" getChannelid :: " +channel.getChannelid());

        String result = "success";
        String message = "Certificate Settings Update Successful!!!";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        String _className = null;
        String _ip = null;
        String _port = null;
        String _userId = null;
        String _password = null;
        String _validityDays = null;
        String _keyLength = null;
        Object reserve1 = null;
        Object reserve2 = null;
        Object reserve3 = null;
        String OCSPUrl = null;
        String CRL = null;
        String CAEmailides = null;
        String crlUserid = null;
        String crlPassword = null;
        String _crlPoolTime = null;
        String _statuskyc = null;
        String _alertsource = null;
        int crlPoolTime = 6;
        String _status_certificate = null;

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.PKI_CERTIFICATE) != 0) {
            result = "error";
            message = "This feature is not available in this license!!!";

            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }

            out.print(json);
            out.flush();
            return;
        }

        String _preference = request.getParameter("_perference");
        
        int _iPreference = Integer.parseInt(_preference);
        
        log.debug("_preference :: " + _preference);

        //request parameter for mobile primary
        String _type1 = request.getParameter("_type");
        int _type = Integer.parseInt(_type1);
        log.debug("_type1 :: " + _type1);

        if (_type == CertificateSettings) {
            if (_iPreference == PREFERENCE_ONE) {   //primary

                _className = request.getParameter("_className");
                log.debug("_className :: "+ _className);
                _ip = request.getParameter("_ip");
                log.debug("_ip :: "+ _ip);
                _password = request.getParameter("_password");
                log.debug("_password :: "+ _password);
                _port = request.getParameter("_port");
                log.debug("_port :: "+ _port);
                reserve1 = request.getParameter("_reserve1");
                log.debug("reserve1 :: "+ reserve1);
                reserve2 = request.getParameter("_reserve2");
                log.debug("reserve2 :: "+ reserve2);
                reserve3 = request.getParameter("_reserve3");
                log.debug("reserve3 :: "+ reserve3);
                _userId = request.getParameter("_userId");
                log.debug("_userId :: "+ _userId);
                _validityDays = request.getParameter("_validityDays");
                log.debug("_validityDays :: "+ _validityDays);
                _keyLength = request.getParameter("_keyLength");
                log.debug("_keyLength :: "+ _keyLength);
                CRL = request.getParameter("_crllink");
                log.debug("CRL :: "+ CRL);
                crlUserid = request.getParameter("_crlusername");
                log.debug("crlUserid :: "+ crlUserid);
                crlPassword = request.getParameter("_crlpassword");
                log.debug("crlPassword :: "+ crlPassword);
                OCSPUrl = request.getParameter("ocspurl");
                log.debug("OCSPUrl :: "+ OCSPUrl);
                CAEmailides = request.getParameter("_tagsList");
                log.debug("CAEmailides :: "+ CAEmailides);
                _crlPoolTime = request.getParameter("_pulledtime");
                log.debug("_crlPoolTime :: "+ _crlPoolTime);
                _statuskyc = request.getParameter("_statuskyc");
                log.debug("_statuskyc :: "+ _statuskyc);
                _alertsource = request.getParameter("_alertsource");
                log.debug("_alertsource :: "+ _alertsource);
                _status_certificate = request.getParameter("_statusCACERT");
                log.debug("_status_certificate :: "+ _status_certificate);
            } else {
                _className = request.getParameter("_classNameTP");
                log.debug("_className :: "+ _className);
                _ip = request.getParameter("_ipTP");
                log.debug("_ip :: "+ _ip);
                _password = request.getParameter("_passwordTP");
                log.debug("_password :: "+ _password);
                _port = request.getParameter("_portTP");
                log.debug("_port :: "+ _port);
                reserve1 = request.getParameter("_reserve1TP");
                log.debug("reserve1 :: "+ reserve1);
                reserve2 = request.getParameter("_reserve2TP");
                log.debug("reserve2 :: "+ reserve2);
                reserve3 = request.getParameter("_reserve3TP");
                log.debug("reserve3 :: "+ reserve3);
                _userId = request.getParameter("_userIdTP");
                log.debug("_userId :: "+ _userId);
                _validityDays = request.getParameter("_validityDaysTP");
                log.debug("_validityDays :: "+ _validityDays);
                _keyLength = request.getParameter("_keyLengthTP");
                log.debug("_keyLength :: "+ _keyLength);
                CRL = request.getParameter("_crllinkTP");
                log.debug("CRL :: "+ CRL);
                crlUserid = request.getParameter("_crlusernameTP");
                log.debug("crlUserid :: "+ crlUserid);
                crlPassword = request.getParameter("_crlpasswordTP");
                log.debug("crlPassword :: "+ crlPassword);
                OCSPUrl = request.getParameter("ocspurlTP");
                log.debug("OCSPUrl :: "+ OCSPUrl);
                CAEmailides = request.getParameter("_tagsListTP");
                log.debug("CAEmailides :: "+ CAEmailides);
                _crlPoolTime = request.getParameter("_pulledtimeTP");
                log.debug("_crlPoolTime :: "+ _crlPoolTime);
                _statuskyc = request.getParameter("_statuskycTP");
                log.debug("_statuskyc :: "+ _statuskyc);
                _alertsource = request.getParameter("_alertsourceTP");
                log.debug("_alertsource :: "+ _alertsource);
                _status_certificate = request.getParameter("_statusCACERTTP");
                log.debug("_status_certificate :: "+ _status_certificate);
            }
            
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _type, _iPreference);

            RootCertificateSettings certificate = null;
            boolean bAddSetting = false;

            if (settingsObj == null) {
                certificate = new RootCertificateSettings();
                certificate.setChannelId(_channelId);

                bAddSetting = true;
            } else {
                certificate = (RootCertificateSettings) settingsObj;

            }

            if (_statuskyc != null) {
                int statuskyc = Integer.parseInt(_statuskyc);
                certificate.setKycStatus(statuskyc);
            }
            if (_alertsource != null) {
                int alertsource = Integer.parseInt(_alertsource);
                certificate.setKycalert(alertsource);
            }

            if (_status_certificate != null) {
                int status_certificate = Integer.parseInt(_status_certificate);
                certificate.setStatus(status_certificate);
            }

            certificate.setClassName(_className);
            certificate.setIp(_ip);

            certificate.setPassword(_password);
            if (_port != null || _port.isEmpty() == true) {
                int _port1 = Integer.parseInt(_port);
                certificate.setPort(_port1);
            }

            if (_crlPoolTime != null || _crlPoolTime.isEmpty() == true) {
                crlPoolTime = Integer.parseInt(_crlPoolTime);
                certificate.crlPoolTime = crlPoolTime;
            }
            certificate.CAEmailides = CAEmailides;
            certificate.CRL = CRL;
            certificate.OCSPUrl = OCSPUrl;
            certificate.crlPassword = crlPassword;
            certificate.crlUserid = crlUserid;
            certificate.setReserve1(reserve1);
            certificate.setReserve2(reserve2);
            certificate.setReserve3(reserve3);
            certificate.setUserId(_userId);
            if (_validityDays != null) {
                int _Duration = Integer.parseInt(_validityDays);
                certificate.setValidityDays(_Duration);
            }
            if (_keyLength != null) {
                int _keylength = Integer.parseInt(_keyLength);
                certificate.setKeyLength(_keylength);
            }

            AuditManagement audit = new AuditManagement();
            if (bAddSetting == true) {
                retValue = sMngmt.addSetting(sessionId, _channelId, _type, _iPreference, certificate);
                log.debug("addSetting :: " +retValue);

                String resultString = "ERROR";
                if (retValue == 0) {
                    resultString = "SUCCESS";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add Certificate Authority Settings", resultString, retValue, "Setting Management",
                            "", "ChannelID=" + certificate.getChannelId() + "Class Name=" + certificate.getClassName()
                            + "Status=" + certificate.getStatus()
                            + "IP=" + certificate.getIp() + "Password=***" + "User ID=" + certificate.getUserId()
                            + "Port=" + certificate.getPort()
                            + "Reserve1=" + certificate.getReserve1() + "Reserve2" + certificate.getReserve2() + "Reserve3=" + certificate.getReserve3()
                            + "Status=" + certificate.getStatus() + "CAEmailides =" + certificate.CAEmailides
                            + "CRL link=" + certificate.CRL + "OCSPUrl= "
                            + certificate.OCSPUrl + "crlPassword= "
                            + certificate.crlPassword + "crlUserid= "
                            + certificate.crlUserid + "crlPoolTime= "
                            + certificate.crlPoolTime,
                            itemtype, channel.getChannelid());
                } else if (retValue != 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add Certificate Authority Settings", resultString, retValue, "Setting Management",
                            "", "Failed To Add Certificate Authority Settings", itemtype,
                            OperatorID);
                }
            } else {
                RootCertificateSettings oldmobileObj = (RootCertificateSettings) sMngmt.getSetting(sessionId, _channelId, _type, _iPreference);
                retValue = sMngmt.changeSetting(sessionId, _channelId, _type, _iPreference, settingsObj, certificate);
                log.debug("changeSetting :: " +retValue);

                String resultString = "ERROR";

                if (retValue == 0) {
                    resultString = "SUCCESS";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change Certificate Authority Settings", resultString, retValue, "Setting Management",
                            "ChannelID=" + oldmobileObj.getChannelId() + " Class Name=" + oldmobileObj.getClassName()
                            + "Status=" + certificate.getStatus()
                            + " IP=" + oldmobileObj.getIp() + " Password=***" + " User ID=" + oldmobileObj.getUserId()
                            + " Port=" + oldmobileObj.getPort()
                            + " Reserve1=" + oldmobileObj.getReserve1() + " Reserve2" + oldmobileObj.getReserve2() + " Reserve3=" + oldmobileObj.getReserve3()
                            + " Status=" + oldmobileObj.getStatus(),
                            " ChannelID=" + certificate.getChannelId() + " Class Name=" + certificate.getClassName() + " ImplementationJar="
                            + " IP=" + certificate.getIp() + " Password=***" + " Phone Number=" + "" + " User ID=" + certificate.getUserId()
                            + " Port=" + certificate.getPort()
                            + " Reserve1=" + certificate.getReserve1() + " Reserve2" + certificate.getReserve2() + " Reserve3=" + certificate.getReserve3() + "CAEmailides =" + certificate.CAEmailides
                            + "CRL link=" + certificate.CRL + "OCSPUrl= "
                            + certificate.OCSPUrl + "crlPassword= "
                            + certificate.crlPassword + "crlUserid= "
                            + certificate.crlUserid + "crlPoolTime= "
                            + certificate.crlPoolTime,
                            itemtype, channel.getChannelid());
                } else if (retValue != 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change Certificate Authority Settings", resultString, retValue, "Setting Management",
                            "ChannelID=" + oldmobileObj.getChannelId() + " Class Name=" + oldmobileObj.getClassName()
                            + "Status=" + certificate.getStatus()
                            + " IP=" + oldmobileObj.getIp() + " Password=***" + " Phone Number=" + "" + " User ID=" + oldmobileObj.getUserId()
                            + " Port=" + oldmobileObj.getPort()
                            + " Reserve1=" + oldmobileObj.getReserve1() + " Reserve2" + oldmobileObj.getReserve2() + " Reserve3=" + oldmobileObj.getReserve3()
                            + " RetryCount=" + "" + " Status=" + oldmobileObj.getStatus() + "CAEmailides =" + certificate.CAEmailides
                            + "CRL link=" + certificate.CRL + "OCSPUrl= "
                            + certificate.OCSPUrl + "crlPassword= "
                            + certificate.crlPassword + "crlUserid= "
                            + certificate.crlUserid + "crlPoolTime= "
                            + certificate.crlPoolTime,
                            "Failed To Edit Certificate Authority Settings", itemtype, channel.getChannelid());
                }

            }
        }

        if (retValue != 0) {
            result = "error";
            message = "Certificate Authority Settings Update Failed!!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }

    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";

    }// </editor-fold>
}
