package com.mollatech.axiom.v2.face.handler.operator;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author nilesh
 */
public class grantAccessOperator extends HttpServlet {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(grantAccessOperator.class.getName());

    final String itemTypeOp = "OPERATOR";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        log.info("is started :: ");
        try {
            response.setContentType("application/json");
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: " + channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("channel :: " + sessionId);
            Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operator :: " + operator.getOperatorid());
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("remoteaccesslogin :: " + remoteaccesslogin);

            String _oprAccessId = request.getParameter("_oprAccessId");
            log.debug("_oprAccessId :: " + _oprAccessId);
            String _accessStartDate = request.getParameter("_accessStartDate");
            log.debug("_accessStartDate :: " + _accessStartDate);

            String _accessEndDate = request.getParameter("_accessEndDate");
            log.debug("_accessEndDate :: " + _accessEndDate);

            JSONObject json = new JSONObject();
            PrintWriter out = response.getWriter();
            String result = "success";
            String message = "Access Update Successful!!!";
            if (_oprAccessId == null || _accessStartDate == null || _accessEndDate == null) {
                result = "error";
                message = "Access Update Failed as invalid/empty data!!!";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }

            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date startDate = null;
            if (_accessStartDate != null && !_accessStartDate.isEmpty()) {
                startDate = (Date) formatter.parse(_accessStartDate);
            }
            Date endDate = null;
            if (_accessEndDate != null && !_accessEndDate.isEmpty()) {
                endDate = (Date) formatter.parse(_accessEndDate);
            }

            if (endDate.after(startDate) == false) {
                result = "error";
                message = "End date cannot be before Start Date!!!";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }

            OperatorsManagement oprMnt = new OperatorsManagement();
            Operators oldOpr = oprMnt.getOperatorById(sessionId, _oprAccessId);
            int retValue = oprMnt.ChangeAccess(sessionId, channel.getChannelid(), _oprAccessId, startDate, endDate);
            log.debug("ChangeAccess :: " + retValue);
            AuditManagement audit = new AuditManagement();
            String resultString = "ERROR";
            Date oldstartDate = null;
            Date oldendDate = null;
            if (oldOpr != null) {
                oldstartDate = oldOpr.getAccessFrom();
                oldendDate = oldOpr.getAccessTill();
            }
            if (retValue == 0) {
                resultString = "SUCCESS";

                audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                        "Change Operator Access", resultString, retValue, "Operator Management",
                        "Access From = " + oldstartDate + " Access Till =" + oldendDate,
                        "Access From = " + startDate + " Access Till =" + endDate,
                        itemTypeOp,
                        operator.getOperatorid());
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            } else {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operator.getName(), new Date(),
                        "Change Operator Access", resultString, retValue, "Operator Management",
                        "Access From = " + oldstartDate + " Access Till =" + oldendDate, "Failed to Set access",
                        itemTypeOp,
                        operator.getOperatorid());
                result = "error";
                message = "Access Update Failed!!!";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }

        } catch (Exception ex) {
            log.error("exception caught :: ", ex);
        }

        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
