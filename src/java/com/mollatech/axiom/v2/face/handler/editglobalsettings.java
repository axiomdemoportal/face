/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class editglobalsettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editglobalsettings.class.getName());

    final String itemtype = "SETTINGS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String OperatorID = operatorS.getOperatorid();

        String _channelId = channel.getChannelid();
        SettingsManagement sMngmt = new SettingsManagement();
        int retValue = -1;

        String result = "success";
        String message = "Global Settings Update Successful!!!";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
       

        String _slowPaceMobile = request.getParameter("_slowPaceMobile");
        String _normalPaceMobile = request.getParameter("_normalPaceMobile");
        String _fastPaceMobile = request.getParameter("_fastPaceMobile");
        String _hyperPaceMobile = request.getParameter("_hyperPaceMobile");
        String _slowPaceEmail = request.getParameter("_slowPaceEmail");
        String _normalPaceEmail = request.getParameter("_normalPaceEmail");
        String _fastPaceEmail = request.getParameter("_fastPaceEmail");
        String _hyperPaceEmail = request.getParameter("_hyperPaceEmail");
        String _FXListner = request.getParameter("_FXListner");
        String _vduration = request.getParameter("_vduration");
        String _voiceSetting = request.getParameter("_voiceSetting");
        String strPercentage = request.getParameter("_weigtage");
        String _answersattempts = request.getParameter("_answersattempts");
        
            log.debug("editglobalsettings::channel is::" + channel.getName());
            log.debug("editglobalsettings::Operator Id is::" + operatorS.getOperatorid());
            log.debug("editglobalsettings::SessionId::" + sessionId);
            log.debug("editglobalsettings::RemoteAccessLogin::" + remoteaccesslogin);
            log.debug("editglobalsettings::Channel Id is::" + channel.getChannelid());

            log.debug("editglobalsettings::slow pace mobile is::" + _slowPaceMobile);
            log.debug("editglobalsettings::Normal pace mobile is::" + _normalPaceMobile);
            log.debug("editglobalsettings::Fast pace mobile::" + _fastPaceMobile);
            log.debug("editglobalsettings::Hyper pace mobile::" + _hyperPaceMobile);
            log.debug("editglobalsettings::slow pace email is::" + _slowPaceEmail);
            log.debug("editglobalsettings::normal pace mail is::" + _normalPaceEmail);
            log.debug("editglobalsettings::fast pace email is::" + _fastPaceEmail);
            log.debug("editglobalsettings::hyper pace email::" + _hyperPaceEmail);
            log.debug("editglobalsettings::FXL listener::" + _FXListner);
            log.debug("editglobalsettings::vduration is::" + _vduration);
            log.debug("editglobalsettings::voice setting::" + _voiceSetting);
            log.debug("editglobalsettings::Percentage::" + strPercentage);
            log.debug("editglobalsettings::answersattempts is::" + _answersattempts);

            
        int iType = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.GlobalSettings;
        int iPreference = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE;

        //   String strType = String.valueOf(iType);
        Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), iType, iPreference);

        GlobalChannelSettings globalObj = null;
        boolean bAddSetting = false;

        if (settingsObj == null) {
            globalObj = new GlobalChannelSettings();
            bAddSetting = true;
        } else {
            globalObj = (GlobalChannelSettings) settingsObj;
        }

        if (strPercentage != null) {
            int istrPercentage = Integer.parseInt(strPercentage);
            globalObj.weightageForCR = istrPercentage;
        }
        if (_answersattempts != null) {
            int ianswersattempts = Integer.parseInt(_answersattempts);
            globalObj.answersattempts = ianswersattempts;
        }

        globalObj._fastPaceEmail = Integer.valueOf(_fastPaceEmail).intValue();
        globalObj._fastPaceMobile = Integer.valueOf(_fastPaceMobile).intValue();
        globalObj._hyperPaceEmail = Integer.valueOf(_hyperPaceEmail).intValue();
        globalObj._hyperPaceMobile = Integer.valueOf(_hyperPaceMobile).intValue();
        globalObj._normalPaceEmail = Integer.valueOf(_normalPaceEmail).intValue();
        globalObj._normalPaceMobile = Integer.valueOf(_normalPaceMobile).intValue();
        globalObj._slowPaceEmail = Integer.valueOf(_slowPaceEmail).intValue();
        globalObj._slowPaceMobile = Integer.valueOf(_slowPaceMobile).intValue();

        globalObj._FXListner = _FXListner;
        globalObj._vduration = _vduration;
        globalObj._voiceSetting = _voiceSetting;

        AuditManagement audit = new AuditManagement();
        if (bAddSetting == true) {
            retValue = sMngmt.addSetting(sessionId, _channelId, iType, iPreference, globalObj);
            log.debug("editglobalsettings::addSetting::"+retValue);
            String resultString = "ERROR";
            if (retValue == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add Channel Global Settings", resultString, retValue, "Setting Management",
                        "", "FXListner=" + globalObj._FXListner + ",fastPaceEmail=" + globalObj._fastPaceEmail + ",hyperPaceEmail=" + globalObj._hyperPaceEmail
                        + ",hyperPaceMobile=" + globalObj._hyperPaceMobile + ",normalPaceEmail=" + globalObj._normalPaceEmail + ",normalPaceMobile=" + globalObj._normalPaceMobile
                        + ",slowPaceEmail=" + globalObj._slowPaceEmail + ",slowPaceMobile=" + globalObj._slowPaceMobile
                        +"Weightage ="+globalObj.weightageForCR+"Answer Attempts ="+globalObj.answersattempts,
                        itemtype, _channelId);
            } else if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add Channel Global Settings", resultString, retValue, "Setting Management",
                        "", "Failed to Add Channel Global Settings",
                        itemtype, _channelId);
            }

        } else {
            GlobalChannelSettings oldglobalObj = (GlobalChannelSettings) sMngmt.getSetting(sessionId, _channelId, iType, iPreference);
            retValue = sMngmt.changeSetting(sessionId, _channelId, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.GlobalSettings, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE, settingsObj, globalObj);
            log.debug("editglobalsettings::changeSetting::"+retValue);
            
            String resultString = "ERROR";
            if (retValue == 0) {

                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Change Channel Global Settings", resultString, retValue, "Setting Management",
                        "FXListner=" + oldglobalObj._FXListner + ",fastPaceEmail=" + oldglobalObj._fastPaceEmail + ",hyperPaceEmail=" + oldglobalObj._hyperPaceEmail
                        + ",hyperPaceMobile=" + oldglobalObj._hyperPaceMobile + ",normalPaceEmail=" + oldglobalObj._normalPaceEmail + ",normalPaceMobile=" + oldglobalObj._normalPaceMobile
                        + ",slowPaceEmail=" + oldglobalObj._slowPaceEmail + ",slowPaceMobile=" + oldglobalObj._slowPaceMobile
                        +"Weightage ="+oldglobalObj.weightageForCR+"Answer Attempts ="+oldglobalObj.answersattempts
                        ,
                        "FXListner=" + globalObj._FXListner + ",fastPaceEmail=" + globalObj._fastPaceEmail + ",hyperPaceEmail=" + globalObj._hyperPaceEmail
                        + ",hyperPaceMobile=" + globalObj._hyperPaceMobile + ",normalPaceEmail=" + globalObj._normalPaceEmail + ",normalPaceMobile=" + globalObj._normalPaceMobile
                        + ",slowPaceEmail=" + globalObj._slowPaceEmail + ",slowPaceMobile=" + globalObj._slowPaceMobile
                        +"Weightage ="+globalObj.weightageForCR+"Answer Attempts ="+globalObj.answersattempts,
                        itemtype, _channelId);
            } else if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Change Channel Global Settings", resultString, retValue, "Setting Management",
                        "FXListner=" + oldglobalObj._FXListner + ",fastPaceEmail=" + oldglobalObj._fastPaceEmail + ",hyperPaceEmail=" + oldglobalObj._hyperPaceEmail
                        + ",hyperPaceMobile=" + oldglobalObj._hyperPaceMobile + ",normalPaceEmail=" + oldglobalObj._normalPaceEmail + ",normalPaceMobile=" + oldglobalObj._normalPaceMobile
                        + ",slowPaceEmail=" + oldglobalObj._slowPaceEmail + ",slowPaceMobile=" + oldglobalObj._slowPaceMobile
                        +"Weightage ="+oldglobalObj.weightageForCR+"Answer Attempts ="+oldglobalObj.answersattempts,
                        "Failed To Edit Channel Global Settings",
                        itemtype, _channelId);
            }
        }

        if (retValue != 0) {
            result = "error";
            message = "Channel Global Settings Update Failed!!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch(Exception e){log.error("Exception caught :: ",e);}finally {
            out.print(json);
            out.flush();
        }

    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
