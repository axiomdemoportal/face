package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.UserSourceSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class editusersrcsettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editusersrcsettings.class.getName());

    //Static variables
    //public static final int SMS = 1;
    public static final int ACTIVE_STATUS = 1;
    public static final int SUSPENDED_STATUS = 1;
    public static final int PREFERENCE_ONE = 1;   //primary
    public static final int PREFERENCE_TWO = 2;
    final String itemtype = "SETTINGS";

//    private void PrintRequestParameters(HttpServletRequest req) {
//        Enumeration<String> paramNames = req.getParameterNames();
//        while (paramNames.hasMoreElements()) {
//            String paramName = paramNames.nextElement();
//            System.out.println(paramName + "=" + req.getParameter(paramName));
//        }
//    }
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        //PrintRequestParameters(request);
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String OperatorID = operatorS.getOperatorid();

        String _channelId = channel.getChannelid();
        SettingsManagement sMngmt = new SettingsManagement();
        int retValue = -1;
        
            log.debug("channel :: " + channel.getName());
            log.debug("getChannelid :: " + channel.getChannelid());
            log.debug("operatorS :: " + operatorS.getOperatorid());
            log.debug("sessionId :: " + sessionId);
            log.debug("remoteaccesslogin :: " + remoteaccesslogin);

        String result = "success";
        String message = "User Source Settings Update Successful!!!";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        int status = Integer.valueOf(request.getParameter("_statusUS")).intValue();
        String channelId = _channelId;
        String ip = request.getParameter("_ipUS");
        int port = Integer.valueOf(request.getParameter("_portUS")).intValue();
        String userId = request.getParameter("_userIdUS");
        String password = request.getParameter("_passwordUS");
        String driverName="";
        boolean ssl = Boolean.valueOf(request.getParameter("_sslUS")).booleanValue();
        String driverNameJAR = "NA";
        String className = request.getParameter("_classNameUS");
        Object reserve1 = (Object) request.getParameter("_reserve1US");
        Object reserve2 = (Object) request.getParameter("_reserve2US");
        Object reserve3 = (Object) request.getParameter("_reserve3US");
        //String databaseType = request.getParameter("_databaseTypeUS");
        
        String databaseType1 = request.getParameter("_databaseTypeUS");
        String databaseType = databaseType1;  //"com.mysql.jdbc.Driver";
        String databaseName = request.getParameter("_databaseNameUS");
        String tableName = request.getParameter("_tableNameUS");
        int algoType = Integer.valueOf(request.getParameter("_algoTypeUS")).intValue();
        int passwordType = Integer.valueOf(request.getParameter("_passwordTypeUS")).intValue();
        String algoAttrReserve1 = request.getParameter("_algoAttrReserve1US");
        String algoAttrReserve2 = request.getParameter("_algoAttrReserve2US");
        String algoAttrReserve3 = request.getParameter("_algoAttrReserve3US");
        String algoAttrReserve4 = request.getParameter("_algoAttrReserve4US");

        log.debug("ip :: " + ip);
        log.debug("userId :: " + userId);
        log.debug("password :: " + password);
        log.debug("databaseType1 :: " + databaseType1);
        log.debug("databaseType :: " + databaseType);
        log.debug("databaseName :: " + databaseName);
        log.debug("tableName :: " + tableName);
        log.debug("algoAttrReserve1 :: " + algoAttrReserve1);
        log.debug("algoAttrReserve2 :: " + algoAttrReserve2);
        log.debug("algoAttrReserve3 :: " + algoAttrReserve3);
        log.debug("algoAttrReserve4 :: " + algoAttrReserve4);

         if (databaseType1 != null){
            if(databaseType1.equalsIgnoreCase("Mysql")){
                driverName = "com.mysql.jdbc.Driver";
            }else if(databaseType1.equalsIgnoreCase("Oracle")){
                driverName = "oracle.jdbc.OracleDriver";
            }
        }
        
        
        int iuserValidityDays = Integer.valueOf(request.getParameter("_userValidityDays"));

        Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.UserSource, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE);

        UserSourceSettings usObj = null;
        boolean bAddSetting = false;

        if (settingsObj == null) {
            usObj = new UserSourceSettings();
            bAddSetting = true;
        } else {
            usObj = (UserSourceSettings) settingsObj;
        }

        usObj.status = status;
        usObj.channelId = channelId;
        usObj.ip = ip;
        usObj.port = port;
        usObj.userId = userId;
        usObj.password = password;
        usObj.ssl = ssl;
        usObj.driverNameJAR = driverNameJAR;
        usObj.className = className;
        usObj.reserve1 = reserve1;
        usObj.reserve2 = reserve2;
        usObj.reserve3 = reserve3;
        usObj.databaseType = driverName;
        usObj.databaseName = databaseName;
        usObj.tableName = tableName;
        usObj.algoType = algoType;
        usObj.passwordType = passwordType;
        usObj.algoAttrReserve1 = algoAttrReserve1;
        usObj.algoAttrReserve2 = algoAttrReserve2;
        usObj.algoAttrReserve3 = algoAttrReserve3;
        usObj.algoAttrReserve4 = algoAttrReserve4;
        usObj.userValidityDays = iuserValidityDays;
        usObj.dbCategory =  databaseType;

        AuditManagement audit = new AuditManagement();

        if (bAddSetting == true) {
            retValue = sMngmt.addSetting(sessionId, _channelId, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.UserSource, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE, usObj);
            log.debug("addSetting::" + retValue);
            String resultString = "ERROR";
            if (retValue == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add User Source Setting", resultString, retValue, "Setting Management",
                        "", "Algo.AttributeReserve1=" + usObj.getAlgoAttrReserve1() + "Algo.AttributeReserve2=" + usObj.getAlgoAttrReserve2()
                        + "Algo.AttributeReserve3=" + usObj.getAlgoAttrReserve3() + "Algo.AttributeReserve4=" + usObj.getAlgoAttrReserve4()
                        + "ChannelID=" + usObj.getChannelId() + "ClassName=" + usObj.getClassName() + "DatabaseName=" + usObj.getDatabaseName()
                        + "DatabaseType=" + usObj.getDatabaseType() + "DriverNameJAR=" + usObj.getDriverNameJAR() + "Ip=" + usObj.getIp()
                        + "Password=*****" + "TableName=" + usObj.getTableName() + "UserID=" + usObj.getUserId() + "Algorithm Type=" + usObj.getAlgoType()
                        + "PasswordType=" + usObj.getPasswordType() + "Port=" + usObj.getPort() + "Reserve1=" + usObj.getReserve1() + "Reserve2=" + usObj.getReserve2()
                        + "Status=" + usObj.getStatus() + "SSL=" + usObj.isSsl(),
                        itemtype, channel.getChannelid());

            } else if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add User Source Settings", resultString, retValue, "Setting Management",
                        "", "Failed To Add User Source Settings",
                        itemtype, channel.getChannelid());
            }

        } else {
            UserSourceSettings oldUserSourceObj = (UserSourceSettings) sMngmt.getSetting(sessionId, channelId, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.UserSource, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE);
            retValue = sMngmt.changeSetting(sessionId, _channelId, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.UserSource, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE, settingsObj, usObj);
            log.debug("changeSetting::" + retValue);
            String resultString = "ERROR";

            if (retValue == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Change User Source Setting", resultString, retValue, "Setting Management",
                        "Algo.AttributeReserve1=" + oldUserSourceObj.getAlgoAttrReserve1() + " Algo.AttributeReserve2=" + oldUserSourceObj.getAlgoAttrReserve2()
                        + " Algo.AttributeReserve3=" + oldUserSourceObj.getAlgoAttrReserve3() + " Algo.AttributeReserve4=" + oldUserSourceObj.getAlgoAttrReserve4()
                        + " ChannelID=" + oldUserSourceObj.getChannelId() + " ClassName=" + oldUserSourceObj.getClassName() + " DatabaseName=" + oldUserSourceObj.getDatabaseName()
                        + " DatabaseType=" + oldUserSourceObj.getDatabaseType() + " DriverNameJAR=" + oldUserSourceObj.getDriverNameJAR() + " Ip=" + oldUserSourceObj.getIp()
                        + " Password=*****" + " TableName=" + oldUserSourceObj.getTableName() + " UserID=" + oldUserSourceObj.getUserId() + " Algorithm Type=" + oldUserSourceObj.getAlgoType()
                        + " PasswordType=" + usObj.getPasswordType() + " Port=" + oldUserSourceObj.getPort() + " Reserve1=" + oldUserSourceObj.getReserve1() + " Reserve2=" + oldUserSourceObj.getReserve2()
                        + " Status=" + oldUserSourceObj.getStatus() + " SSL=" + oldUserSourceObj.isSsl(),
                        "Algo.AttributeReserve1=" + usObj.getAlgoAttrReserve1() + "Algo.AttributeReserve2=" + usObj.getAlgoAttrReserve2()
                        + "Algo.AttributeReserve3=" + usObj.getAlgoAttrReserve3() + "Algo.AttributeReserve4=" + usObj.getAlgoAttrReserve4()
                        + "ChannelID=" + usObj.getChannelId() + "ClassName=" + usObj.getClassName() + "DatabaseName=" + usObj.getDatabaseName()
                        + "DatabaseType=" + usObj.getDatabaseType() + "DriverNameJAR=" + usObj.getDriverNameJAR() + "Ip=" + usObj.getIp()
                        + "Password=*****" + "TableName=" + usObj.getTableName() + "UserID=" + usObj.getUserId() + "Algorithm Type=" + usObj.getAlgoType()
                        + "PasswordType=" + usObj.getPasswordType() + "Port=" + usObj.getPort() + "Reserve1=" + usObj.getReserve1() + "Reserve2=" + usObj.getReserve2()
                        + "Status=" + usObj.getStatus() + "SSL=" + usObj.isSsl(),
                        itemtype, channel.getChannelid());
            } else if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Change User Source Setting", resultString, retValue, "Setting Management",
                        "Algo.AttributeReserve1=" + oldUserSourceObj.getAlgoAttrReserve1() + " Algo.AttributeReserve2=" + oldUserSourceObj.getAlgoAttrReserve2()
                        + " Algo.AttributeReserve3=" + oldUserSourceObj.getAlgoAttrReserve3() + " Algo.AttributeReserve4=" + oldUserSourceObj.getAlgoAttrReserve4()
                        + " ChannelID=" + oldUserSourceObj.getChannelId() + " ClassName=" + oldUserSourceObj.getClassName() + " DatabaseName=" + oldUserSourceObj.getDatabaseName()
                        + " DatabaseType=" + oldUserSourceObj.getDatabaseType() + " DriverNameJAR=" + oldUserSourceObj.getDriverNameJAR() + " Ip=" + oldUserSourceObj.getIp()
                        + " Password=*****" + " TableName=" + oldUserSourceObj.getTableName() + " UserID=" + oldUserSourceObj.getUserId() + " Algorithm Type=" + oldUserSourceObj.getAlgoType()
                        + " PasswordType=" + usObj.getPasswordType() + " Port=" + oldUserSourceObj.getPort() + " Reserve1=" + oldUserSourceObj.getReserve1() + " Reserve2=" + oldUserSourceObj.getReserve2()
                        + " Status=" + oldUserSourceObj.getStatus() + " SSL=" + oldUserSourceObj.isSsl(),
                        "Failed To Edit User Source Settings", itemtype, 
                        channel.getChannelid());

            }

        }

        if (retValue != 0) {
            result = "error";
            message = "User Source Settings Update Failed!!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception e) { log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }

    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";

    }// </editor-fold>
}
