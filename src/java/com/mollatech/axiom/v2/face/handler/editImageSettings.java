/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.ImageSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author nilesh
 */
public class editImageSettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editImageSettings.class.getName());


    final String itemtype = "SETTINGS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //      PrintRequestParameters(request);
        log.info("Servlet started");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String OperatorID = operatorS.getOperatorid();

        String _channelId = channel.getChannelid();
        SettingsManagement sMngmt = new SettingsManagement();
        int retValue = -1;

        String result = "success";
        String message = "Secure Phrase Settings Update Successful!!!";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        int _iPreference = 1;
        
            log.debug("channel is::" + channel.getName());
            log.debug("Operator Id is::" + operatorS.getOperatorid());
            log.debug("SessionId::" + sessionId);
            log.debug("RemoteAccessLogin::" + remoteaccesslogin);
            log.debug("Channel Id is::" + channel.getChannelid());
        
//        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.IMAGE_AUTH) != 0) {
//            result = "error";
//            message = "This feature is not available in this license!!!";
//
//            try {
//                json.put("_result", result);
//                json.put("_message", message);
//            } catch (Exception e) {
//               log.error("Exception caught :: ",e);
//            }
//
//            out.print(json);
//            out.flush();
//            return;
//        }
        int _type = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.IMAGE_SETTINGS;
        //  String strType = String.valueOf(_type);
        Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), _type, _iPreference);

        ImageSettings imageObj = null;
        boolean bAddSetting = false;

        if (settingsObj == null) {
            imageObj = new ImageSettings();
            imageObj._channelID = _channelId;
            bAddSetting = true;
        } else {
            imageObj = (ImageSettings) settingsObj;
        }
        imageObj._channelID = _channelId;
        String _sweetSpotDeviation = request.getParameter("_sweetSpotDeviation");
        log.debug("_sweetSpotDeviation ::" + _sweetSpotDeviation);
        
        int isweetSpotDeviation = 0;
        if (_sweetSpotDeviation != null) {
            isweetSpotDeviation = Integer.valueOf(_sweetSpotDeviation);
        }
        imageObj._sweetSpotDeviation = isweetSpotDeviation;

        String _xDeviation = request.getParameter("_xDeviation");
        log.debug("_xDeviation ::" + _xDeviation);
        
        int ixDeviation = 0;
        if (_xDeviation != null) {
            ixDeviation = Integer.valueOf(_xDeviation);
        }
        imageObj._xDeviation = ixDeviation;
        String _yDeviation = request.getParameter("_yDeviation");
        log.debug("_yDeviation::" + _yDeviation);
        int iyDeviation = 0;
        if (_yDeviation != null) {
            iyDeviation = Integer.valueOf(_yDeviation);
        }
        imageObj._yDeviation = iyDeviation;
        String _geoCheck = request.getParameter("_geoCheck");
        log.debug("_geoCheck ::" + _geoCheck);
        int igeoCheck = 0;
        if (_geoCheck != null) {
            igeoCheck = Integer.valueOf(_geoCheck);
        }
        imageObj._geoCheck = igeoCheck;
        String _deviceProfile = request.getParameter("_deviceProfile");
        log.debug("_deviceProfile ::" + _deviceProfile);
        int ideviceProfile = 0;
        if (_deviceProfile != null) {
            ideviceProfile = Integer.valueOf(_deviceProfile);
        }
        imageObj._deviceProfile = ideviceProfile;
        String _sweetSpot = request.getParameter("_sweetSpot");
        log.debug("_sweetSpot::" + _sweetSpot);
        int isweetSpot = 0;
        if (_sweetSpot != null) {
            isweetSpot = Integer.valueOf(_sweetSpot);
        }
        imageObj._sweetSpot = isweetSpot;
        String _honeyTrap = request.getParameter("_honeyTrap");
        log.debug("_honeyTrap::" + _honeyTrap);
        int ihoneyTrap = 0;
        if (_honeyTrap != null) {
            ihoneyTrap = Integer.valueOf(_honeyTrap);
        }
        imageObj._honeyTrap = ihoneyTrap;

        AuditManagement audit = new AuditManagement();
        if (bAddSetting == true) {
            retValue = sMngmt.addSetting(sessionId, _channelId, _type, _iPreference, imageObj);
            log.debug("addSetting :: " + retValue);
            String resultString = "ERROR";
            if (retValue == 0) {
                resultString = "SUCCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add Secure Phrase Settings", resultString, retValue, "Setting Management",
                        "", "Sweetspot = " + isweetSpotDeviation + " x-Devication" + ixDeviation
                        + " y-Deviation" + iyDeviation + " Device Profile =" + ideviceProfile + " Geo Check=" + igeoCheck
                        + " Honey Trap=" + ihoneyTrap,
                        itemtype, channel.getChannelid());
            } else if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add Secure Phrase Settings", resultString, retValue, "Setting Management",
                        "", "Failed to Secure Phrase Settings",
                        itemtype, channel.getChannelid());
            }

        } else {
            ImageSettings oldimagebj = (ImageSettings) sMngmt.getSetting(sessionId, _channelId, _type, _iPreference);
            retValue = sMngmt.changeSetting(sessionId, _channelId, _type, _iPreference, settingsObj, imageObj);
            log.debug("changeSetting::" + retValue);
            
            String resultString = "ERROR";

            if (retValue == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Edit Secure Phrase Settings", resultString, retValue, "Setting Management",
                        "Sweetspot = " + oldimagebj._sweetSpotDeviation + " x-Devication" + oldimagebj._xDeviation
                        + " y-Deviation" + oldimagebj._yDeviation + " Device Profile =" + oldimagebj._deviceProfile
                        + " Geo Check=" + oldimagebj._geoCheck + " Honey Trap=" + oldimagebj._honeyTrap,
                        "Sweetspot = " + isweetSpotDeviation + " x-Devication" + ixDeviation
                        + " y-Deviation" + iyDeviation + " Device Profile =" + ideviceProfile + " Geo Check=" + igeoCheck
                        + " Honey Trap=" + ihoneyTrap,
                        itemtype, channel.getChannelid());

            } else if (retValue != 0) {

                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add Secure Phrase Settings", resultString, retValue, "Setting Management",
                        "Sweetspot = " + oldimagebj._sweetSpotDeviation + " x-Devication" + oldimagebj._xDeviation
                        + " y-Deviation" + oldimagebj._yDeviation + " Device Profile =" + oldimagebj._deviceProfile
                        + " Geo Check=" + oldimagebj._geoCheck + " Honey Trap=" + oldimagebj._honeyTrap,
                        "Failed To change Secure Phrase Settings", itemtype, channel.getChannelid());
            }

        }

        if (retValue != 0) {
            result = "error";
            message = "Secure Phrase Settings Update Failed!!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
