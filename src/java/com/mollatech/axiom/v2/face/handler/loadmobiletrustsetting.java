/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.management.MobileTrustManagment;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.MobileTrustSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;
import org.json.JSONArray;


/**
 *
 * @author mollatech1
 */
public class loadmobiletrustsetting extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadmobiletrustsetting.class.getName());
    
  public static final int ANDROID = 1;
    public static final int IOS = 2;
    public static final int WEB = 3;
    private JSONObject SettingsWhenEmpty(int _type1) {
        JSONObject json = new JSONObject();
        int iglobal = SettingsManagement.MOBILETRUST_SETTING;

        if (_type1 == iglobal) {
            try { json.put("_CountryName", "India");
            json.put("_ExpiryMin", 1);
            json.put("_Backup", true);
            json.put("_SilentCall", true);
            json.put("_SelfDestructEnable", true);
            json.put("_SelfDestructAttempts", 3);
            json.put("_licenseforAndroid", "Not Initialized!!!!");
            json.put("_licenseforIphone", "Not Initialized!!!!");
            json.put("_timeStamp", true);
            json.put("_deviceTracking", true);
            json.put("_geoFencing", true);
            json.put("_hashalgo", "SHA1");
            json.put("_gatewayTypeM", 1);
            json.put("_alertAttemptM", 1);
            json.put("_templateNameM",1);
            json.put("_allowAlertM",true);
            json.put("_allowAlertForM",1);
             json.put("_blackListedCountries", "");
            json.put("_isBlackListed", true);
  
            
            }catch(Exception ex){
                log.error("Exception caught :: ",ex);
            }
        }
        return json;
    }

    private JSONObject SettingsWhenPresent(Object settingsObj) {
        JSONObject json = new JSONObject();
        if (settingsObj instanceof MobileTrustSettings) {
            MobileTrustSettings globalObj = (MobileTrustSettings) settingsObj;
            try { json.put("_CountryName", globalObj.homeCountryCode);
            json.put("_ExpiryMin", globalObj.expiryTimeStampInMins);
            json.put("_Backup", globalObj.isKeyBackupEnable);
            json.put("_SilentCall", globalObj.bSilentCall);
            json.put("_SelfDestructEnable", globalObj.selfdestructalertENABLED);
            json.put("_SelfDestructAttempts", globalObj.selfDestructAttemps);
            json.put("_selfDestructURl", globalObj.selfdestructalertURL);
            json.put("_hashalgo", globalObj.hashingAlgo);
            }catch(Exception ex){
                log.error("Exception caught :: ",ex);
            }
            MobileTrustManagment mtm = new MobileTrustManagment();
            String licensekeyforAndroid = mtm.makelicensekey(_channelid,ANDROID);
            String licensekeyforIphone = mtm.makelicensekey(_channelid,IOS);
            String licensekeyforWEB = mtm.makelicensekey(_channelid,WEB);
            //System.out.println("the json license key is " + licensekeyforAndroid);
//            if(licensekeyforAndroid != null){
//            String b64licensekeyforAndroid = new String(Base64.encode(licensekeyforAndroid.getBytes()));
//             json.put("_licenseforAndroid", b64licensekeyforAndroid);
//            }else{
//                json.put("_licenseforAndroid", "Not Initialized!!!!");
//            }
            //System.out.println("the base 64 json license key is " + b64licensekeyforAndroid);
            try { 
                 if(licensekeyforAndroid != null){
            String b64licensekeyforAndroid = new String(Base64.encode(licensekeyforAndroid.getBytes()));
             json.put("_licenseforAndroid", b64licensekeyforAndroid);
            }else{
                json.put("_licenseforAndroid", "Not Initialized!!!!");
            }
//                json.put("_licenseforAndroid", b64licensekeyforAndroid);
            //System.out.println("the json license key is " + licensekeyforIphone);
               
          
            //System.out.println("the base 64 json license key is " + b64licensekeyforIphone);
//            json.put("_licenseforIphone", b64licensekeyforIphone);
                
                 if(licensekeyforIphone != null){
            String b64licensekeyforIphone = new String(Base64.encode(licensekeyforIphone.getBytes()));
             json.put("_licenseforIphone", b64licensekeyforIphone);
                }else{
                    json.put("_licenseforIphone", "Not Initialized!!!!");
                }
                 
                       if(licensekeyforWEB != null){
            String b64licensekeyforWEB = new String(Base64.encode(licensekeyforWEB.getBytes()));
             json.put("_licenseforWEB", b64licensekeyforWEB);
                }else{
                    json.put("_licenseforWEB", "Not Initialized!!!!");
                }
            
            json.put("_timeStamp", globalObj.bTimestamp);
            json.put("_deviceTracking", globalObj.bDeviceTracking);
            json.put("_geoFencing", globalObj.bGeoFencing);
            
            json.put("_gatewayTypeM", globalObj.mobileAlertVia);
            json.put("_alertAttemptM", globalObj.mobileAlertAttempts);
//            json.put("_templateNameM",globalObj.m);
            json.put("_allowAlertM",globalObj.mobileAlert);
            json.put("_allowAlertForM",globalObj.mobileAlertFor);
            json.put("_selfDestructURlM",globalObj.selfdestructalertURLWEB);
             json.put("_blackListedCountries", globalObj.listOfBlackListedCountries);
            json.put("_isBlackListed", globalObj.systemCountryCheck);
           List<String> allowedDomains=new ArrayList<String>();
           if(globalObj.listofAllowedDomainnames!=null){
           for(int i=0;i<globalObj.listofAllowedDomainnames.length;i++)
           {
               allowedDomains.add(globalObj.listofAllowedDomainnames[i]);
           }
           json.put("_allowedDomainnames",new JSONArray(allowedDomains));
           }else{
            json.put("_allowedDomainnames","");
           }
            
            }catch(Exception ex){
                log.error("Exception caught :: ",ex);
            }

        }
        return json;
    }
    private String _channelid;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("loadmobiletrustsetting::channel is::"+channel.getName());
        log.debug("loadmobiletrustsetting::sessionid::"+sessionId);
        JSONObject json = null;
        PrintWriter out = response.getWriter();

        _channelid = channel.getChannelid();

        try {
            SettingsManagement sMngmt = new SettingsManagement();
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), sMngmt.MOBILETRUST_SETTING, sMngmt.PREFERENCE_ONE);
            if (settingsObj != null) {
                json = SettingsWhenPresent(settingsObj);
            } else {
                json = SettingsWhenEmpty(sMngmt.MOBILETRUST_SETTING);
            }

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
            // TODO handle custom exceptions here
        }
        try {
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
