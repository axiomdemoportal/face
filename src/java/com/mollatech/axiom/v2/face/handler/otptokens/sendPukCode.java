/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.otptokens;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

public class sendPukCode extends HttpServlet {
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(sendPukCode.class.getName());
final String itemType = "OTPTOKENS";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");
        try {
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel ::"+channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId :: "+sessionId);

            //audit parameter
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("remoteaccesslogin :: "+remoteaccesslogin);
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operatorS :: "+operatorS.getOperatorid());
            String _userid = request.getParameter("_useridPUK");
            log.debug("_userid :: "+_userid);
            String _pukcode = request.getParameter("_pukcode");
            log.debug("_pukcode :: "+_pukcode);
            String _sendVia = request.getParameter("_sendVia");
            log.debug("_sendVia :: "+_sendVia);
            String _pukExpiry = request.getParameter("_pukExpiry");
            log.debug("_pukExpiry :: "+_pukExpiry);
            System.out.println("Puk Expiry"+_pukExpiry);
            JSONObject json = new JSONObject();
            PrintWriter out = response.getWriter();
            if(_userid == null || _pukcode == null || _pukExpiry == null){
                String result = "error";
                String message = "PUK code send failed!!!";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }if(_userid.isEmpty() == true || _pukcode.isEmpty() == true || _pukExpiry.isEmpty() == true){
                String result = "error";
                String message = "PUK code send failed!!!";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }
            
            if (_sendVia == null) {
                _sendVia = "EMAIL";
            }
            int expiry = 0;
            if (_pukExpiry != null) {
                expiry = Integer.parseInt(_pukExpiry);
            }
            UserManagement uMngt = new UserManagement();
            AuthUser user = uMngt.getUser(sessionId, channel.getChannelid(), _userid);
            if(user == null){
                String result = "error";
                String message = "User Not Found!!!";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }
            Templates temp = null;
            TemplateManagement tObj = new TemplateManagement();
            SendNotification send = new SendNotification();
            AXIOMStatus status = new AXIOMStatus();
            final String strFAILED = "FAILED";
            final int FAILED = -1;
            status.iStatus = FAILED;
            status.strStatus = strFAILED;
            if (_sendVia.equals("EMAIL")) {
                temp = tObj.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.EMAIL_PUK_CODE);
                if (temp.getStatus() == tObj.ACTIVE_STATUS) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                    Date d = new Date();
                    ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
                    String tmessage = (String) UtilityFunctions.deserializeFromObject(bais);
                    tmessage = tmessage.replaceAll("#name#", user.getUserName());
                    tmessage = tmessage.replaceAll("#channel#", channel.getName());
                    tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                    tmessage = tmessage.replaceAll("#pukcode#", _pukcode);
                    Calendar current = Calendar.getInstance();
                    current.setTime(d);
                    current.set(Calendar.AM_PM, Calendar.AM);
                    current.add(Calendar.SECOND, expiry);
                    Date _pukExpirytime = current.getTime();
                    tmessage = tmessage.replaceAll("#expiry#", "" + _pukExpirytime);
                    status = send.SendEmail(channel.getChannelid(), user.getEmail(), temp.getSubject(), tmessage, null, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                }
            } else {
                temp = tObj.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.MOBILE_PUK_CODE);
                if (temp.getStatus() == tObj.ACTIVE_STATUS) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                    Date d = new Date();
                    ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
                    String tmessage = (String) UtilityFunctions.deserializeFromObject(bais);
                    tmessage = tmessage.replaceAll("#name#", user.getUserName());
                    tmessage = tmessage.replaceAll("#channel#", channel.getName());
                    tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                    tmessage = tmessage.replaceAll("#pukcode#", _pukcode);
                    Calendar current = Calendar.getInstance();
                    current.setTime(d);
                    current.set(Calendar.AM_PM, Calendar.AM);
                    current.add(Calendar.SECOND, expiry);
                    Date _pukExpirytime = current.getTime();
                    
                    tmessage = tmessage.replaceAll("#expiry#", "" + _pukExpirytime);
                    //System.out.println("user.getPhoneNo() ::"+user.getPhoneNo());
                    status = send.SendOnMobileNoWaiting(channel.getChannelid(), user.getPhoneNo(), tmessage, 1, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                }
            }

            AuditManagement audit = new AuditManagement();
            if (status.iStatus == 0 || status.iStatus == 2) {
                 String resultString = "Success";
                        audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                                request.getRemoteAddr(), channel.getName(),
                                remoteaccesslogin, operatorS.getName(), new Date(), "Send PUK Code to "+user.getUserName(),
                                resultString, status.iStatus ,
                                "OTP Management", " PUK code send successfully to " + user.getUserName(),
                                "Sent successfully!!!",
                                itemType, _userid);
                String result = "success";
                String message = "PUK code sent successfully!!!";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
                
            }else{
                String resultString = "error";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                                request.getRemoteAddr(), channel.getName(),
                                remoteaccesslogin, operatorS.getName(), new Date(), "Send PUK Code to "+user.getUserName(),
                                resultString, status.iStatus ,
                                "OTP Management", "PUK code send failed to" + user.getUserName(),
                                "Send failed!!!",
                                itemType, _userid);
                String result = "error";
                String message = "PUK code sent failed!!!";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            }

        } catch (Exception ex) {
            log.error("exception caught ::",ex);
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
