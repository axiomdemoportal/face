/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.group;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.GroupManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author ashish
 */
public class ChangeGroupStatus extends HttpServlet {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ChangeGroupStatus.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private int Status = -1;
    AuditManagement audit = new AuditManagement();
    final String itemType = "GROUPS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("is started :: ");
        
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        String operatorId = operatorS.getOperatorid();
        log.debug("operatorId :: "+operatorId);
        String _status = request.getParameter("_status");
        log.debug("_status :: "+_status);
        String _unitId = request.getParameter("_unitId");
        log.debug("_unitId :: "+_unitId);
        
        if (!_status.equals("")) {
            Status = Integer.parseInt(_status);
        }
        GroupManagement management = new GroupManagement();
        String resultString = "ERROR";
        int result = management.changeGroupStatus(sessionId, channel.getChannelid(), _unitId, Status);
        log.debug("changeGroupStatus :: "+result);
        if (result == 0) {
            resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), 
                    "Change Group Status", resultString, result,
                    "Group Management", "", "Group Name=" + _unitId + "Group Status" + Status + "created on =" + new Date()
                    + "Updated On = " + new Date(), 
                    itemType, channel.getChannelid());
        } else {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), 
                    "Change Group Status", resultString, result,
                    "Group Management", "", "Failed To Change Group Status",
                    itemType, channel.getChannelid());
        }
        String _result="";
        String _message="Status Updated Successfully";
        if (result == 0) {
            _result = "success";
        } else {
            _result = "error";
            _message = "Group Addition Failed";

        }
        try {
            json.put("_result", _result);
            json.put("_message", _message);
        } catch (Exception e) {
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();

        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
