/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.pkitokens;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.db.Audit;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.v2.face.handler.users.getuseraudits;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mollatech1
 */
public class pkitokensaudit extends HttpServlet {
    
     static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(pkitokensaudit.class.getName());

    private static int PDF_TYPE = 0;
    private static int CSV_TYPE = 1;
    private static final int BUFSIZE = 4096;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");

        try {

            //response.setContentType("application/json");
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId :: "+sessionId);
            Operators operartorObj = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operartorObj :: "+operartorObj);
            String _op_duration = request.getParameter("_duration");
            log.debug("_op_duration :: "+_op_duration);
            String _userid = request.getParameter("_userid");
            log.debug("_userid :: "+_userid);
            String _format = request.getParameter("_format");
            log.debug("_format :: "+_format);
           String _itemType = request.getParameter("_itemType");
           log.debug("_itemType :: "+_itemType);

            int iFormat = PDF_TYPE;
            // if ( _format.compareTo("pdf") == 0 )
            //  iFormat =  PDF_TYPE;

            String filepath = null;
            //JSONObject json = new JSONObject();
            //PrintWriter out = response.getWriter();
            //System.out.println("In ListOperator");
            //String result = "success";
            //String message = "Audit Report Generated Successfully!!!";

            if (_userid == null || _op_duration == null
                    || _userid.length() <= 0 || _op_duration.length() <= 0) {
//                result = "error";
//                message = "Invalid parameters !!!";
//                json.put("_result", result);
//                json.put("_message", message);
//                out.print(json);
//                out.flush();
                return;
            }

            try {
                try {
                    AuditManagement aManagement = new AuditManagement();
                    UserManagement user = new UserManagement();
                    AuthUser auser = user.getUser(sessionId, channel.getChannelid(), _userid);
                    Audit[] audit = aManagement.getUserAuditrail(sessionId, channel.getChannelid(), _userid, _itemType, _op_duration);
                    String strUser = "";
                    if (auser.getEmail() != null) {
                        strUser = auser.getEmail();
                    }
                    filepath = aManagement.generateReport(iFormat, auser.getUserName() + "[" + strUser + "]",
                            audit, _op_duration, channel.getName(), channel.getChannelid());
                    audit = null;
                } catch (Exception e) {
                    log.error("Exception caught :: ",e);
                }

                //filepath = "C:\\Users\\Vikram\\Desktop\\A3R2QL-cambodia.pdf";
                File file = new File(filepath);
                int length = 0;
                ServletOutputStream outStream = response.getOutputStream();
                ServletContext context = getServletConfig().getServletContext();
                String mimetype = context.getMimeType(filepath);

                // sets response content type
                if (mimetype == null) {
                    mimetype = "application/octet-stream";
                }
                response.setContentType(mimetype);
                response.setContentLength((int) file.length());
                String fileName = (new File(filepath)).getName();

                // sets HTTP header
                response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

                byte[] byteBuffer = new byte[BUFSIZE];
                DataInputStream in = new DataInputStream(new FileInputStream(file));

                // reads the file's bytes and writes them to the response stream
                while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                    outStream.write(byteBuffer, 0, length);
                }

                in.close();
                outStream.close();

                file.delete();

            } catch (Exception ex) {
                // TODO handle custom exceptions here
                log.error("exception caught :: ",ex);
            }

        } catch (Exception ex) {
            Logger.getLogger(getuseraudits.class.getName()).log(Level.SEVERE, null, ex);
            log.error("exception caught :: ",ex);
        }
        log.info("is ended :: ");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
