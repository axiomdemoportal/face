package com.mollatech.axiom.v2.face.handler.operator.template;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
import com.mollatech.axiom.nucleus.settings.OOBMobileChannelSettings;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class edittemplates extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(edittemplates.class.getName());

    final String itemType = "MESSAGES";
    final int BLOCKED = -121;
    final int SMS = 1;
    final int USSD = 2;
    final int VOICE = 3;
    final int EMAIL = 4;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        
        log.info("is started :: ");

        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Template updated successfully!!!";

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String operatorId = operatorS.getOperatorid();
        log.debug("operatorId :: "+operatorId);

        JSONObject json = new JSONObject();

        String _tid = request.getParameter("idMessageTemplateId");
        int _templateid = Integer.valueOf(_tid);
        log.debug("_templateid :: "+_templateid);
        String _templatename = request.getParameter("_templateM_name");
        log.debug("_templatename :: "+_templatename);
        String _templatebody = request.getParameter("_templateM_body");
        log.debug("_templatebody :: "+_templatebody);
        String _templateVariables = request.getParameter("_templateM_variable");
        log.debug("_templateVariables :: "+_templateVariables);

        if (_templatename == null || _templatebody == null || _templateVariables == null
                || _templatename.length() == 0 || _templatebody.length() == 0 || _templateVariables.length() == 0) {
            result = "error";
            message = "Invalid Template Details!!";
            try { json.put("_result", result);
            json.put("_message", message);}catch(Exception e){}
            out.print(json);
            out.flush();
            return;
        }
        int retValue = 0;
        Templates checkObj = null;
        TemplateManagement tManagement = new TemplateManagement();
        AuditManagement audit = new AuditManagement();
        checkObj = tManagement.CheckTemplate(sessionId, channel.getChannelid(), _templatename);

        if (checkObj != null && checkObj.getTemplateid() != _templateid) {
            result = "error";
            message = "Duplicate Name!!";
            try { json.put("_result", result);
            json.put("_message", message);}catch(Exception e){
                
            log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;

        }

        boolean _allow = false;

        if (checkObj.getType() == SMS) {
            try {
                SettingsManagement sMngmt = new SettingsManagement();
                Object settingsObjP = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.SMS, SettingsManagement.PREFERENCE_ONE);
                Object settingsObjS = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.SMS, SettingsManagement.PREFERENCE_TWO);

                OOBMobileChannelSettings mobileP = (OOBMobileChannelSettings) settingsObjP;
                OOBMobileChannelSettings mobileS = (OOBMobileChannelSettings) settingsObjS;

                if (mobileP.getMessageLength() == 1 || mobileS.getMessageLength() == 1) {
                    _allow = true;
                }

            } catch (Exception ex) {
                log.error("exception caught :: ",ex);
            }

            if (_templatebody.length() >= 160 && _allow == false) {
                result = "error";
                message = "Template Body Length must be less than 160 characters!!!";
                try { json.put("_result", result);
                json.put("_message", message);}catch(Exception e){
                    log.error("exception caught :: ",e);
                
                }
                out.print(json);
                out.flush();
                return;
            }
        }

        Templates oldTObj = tManagement.LoadTemplate(sessionId, channel.getChannelid(), _templateid);
        SettingsManagement sMngmt = new SettingsManagement();
        SendNotification send = new SendNotification();

        Object obj = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.GlobalSettings, 1);

        if (obj != null) {
            GlobalChannelSettings cObj = (GlobalChannelSettings) obj;
            String strword = sMngmt.checkcontent( channel.getChannelid(), _templatebody,cObj);
            if (strword != null && cObj.contentstatus == 0) {

                if (cObj.contentalertstatus == 0) {

                    retValue = BLOCKED;
                }

                retValue = BLOCKED;

            } else {
                retValue = tManagement.EditTemplate(sessionId, channel.getChannelid(), _templateid, _templatename, _templatebody, _templateVariables, null);
                log.debug("EditTemplate :: "+retValue);
            }
        } else {

            retValue = tManagement.EditTemplate(sessionId, channel.getChannelid(), _templateid, _templatename, _templatebody, _templateVariables, null);
            log.debug("EditTemplate :: "+retValue);
        }
        byte[] bTemplate = oldTObj.getTemplatebody();
        String tName = oldTObj.getTemplatename();
        String tVariables = oldTObj.getTemplatevariables();

        String tBody = new String(bTemplate, "UTF-8");
        String resultString = "ERROR";

        if (retValue == 0) {
             resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), 
                    "Edit Template", resultString, retValue,
                    "Template Management", "Template Name =" + tName + "Template Variables =" + tVariables + "Template Body =" + tBody,
                    "Template Name =" + _templatename + "Template Variables =" + _templateVariables + "Template Body =" + _templatebody,
                    itemType, 
                    "" + _templateid);
            
             result = "success";
             message = "Template updated successfully!!!";

        } else if (retValue == BLOCKED) {
            resultString = "ERROR";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), 
                    "Edit Template", resultString, retValue,
                    "Template Management", "", 
                    "Template Blocked by Content Filter",
                    itemType, 
                    "" + _templateid);
            result = "error";
            message = "Template blocked by Content Filter!!!";
        } else {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(),
                    channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Edit Template", resultString, retValue,
                    "Template Management", " Name =" + tName + " Variables =" + tVariables + " Body =" + tBody,
                    "Failed To Edit Template",
                    itemType, 
                    "" + _templateid);
            result = "error";
            message = "Template Update Failed!!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception ex) {
            
            log.error("exception caught :: ",ex);
            
        } finally {
            out.print(json);
            out.flush();
        }
        
        log.info("is ended :: ");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
