/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class loadPushWatch extends HttpServlet {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(loadPushWatch.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String UserGroup = request.getParameter("_groupId");

        String PushType = request.getParameter("_type");
        log.info("loadPushWatch started");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        log.debug("channel :: " + channel.getName());
        log.debug("sessionId :: " + sessionId);
        log.debug("UserGroup :: " + UserGroup);
        log.debug("PushType :: " + PushType);
        String[] files = new String[1];
        String result;
        String message;
        response.setContentType("text/html;charset=UTF-8");
        //Upload Certificate
        String savepath;
        savepath = System.getProperty("catalina.home");
        if (savepath == null) {
            savepath = System.getenv("catalina.home");
        }
        savepath += System.getProperty("file.separator");
        savepath += "axiomv2-settings";
        savepath += System.getProperty("file.separator");
        savepath += "uploads";
        savepath += System.getProperty("file.separator");
        String optionalFileName = "";
        FileItem fileItem = null;

        String dirName = savepath;
        int i = 0;
        if (ServletFileUpload.isMultipartContent(request)) {
            try {
                ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                List fileItemsList = servletFileUpload.parseRequest(request);
                Iterator it = fileItemsList.iterator();
                while (it.hasNext()) {
                    FileItem fileItemTemp = (FileItem) it.next();
                    if (fileItemTemp.isFormField()) {
                        if (fileItemTemp.getFieldName().equals("filename")) {
                            optionalFileName = fileItemTemp.getString();
                        } else {
                            System.out.println("Additional fields in file upload form is  " + fileItemTemp.getFieldName());
                        }
                    } else {
                        fileItem = fileItemTemp;

                    }
                }
                if (fileItem != null) {
                    String fileName = fileItem.getName();
                    if (fileItem.getSize() == 0) {
                        String strError = "Please Select File To Upload...!!!";
                        result = "error";
                        try {
                            json.put("result", result);
                            json.put("message", strError);
                        } catch (JSONException e) {
                            log.error("Exception caught :: ", e);
                        }
                        //out.print(json);
                        out.print("{result:'" + result + "',message:'" + strError + "'}");
                        out.flush();
                        return;
                    }
                    if (fileItem.getSize() == 0) {
                        String strError = "Please Select File To Upload...!!!";
                        result = "error";
                        try {
                            json.put("result", result);
                            json.put("message", strError);
                        } catch (JSONException e) {
                            log.error("Exception caught :: ", e);
                        }
                        //out.print(json);
                        out.print("{result:'" + result + "',message:'" + strError + "'}");
                        out.flush();
                        return;
                    }
                    if (fileItem.getSize() > 0 && fileItem.getSize() < 1024000 * 5) { // size cannot be more than 65Kb. We want it light.
                        if (optionalFileName.trim().equals("")) {
                            fileName = FilenameUtils.getName(fileName);
                        } else {
                            fileName = optionalFileName;
                        }
                        files[i++] = dirName + fileName;
                        File saveTo = new File(dirName + fileName);
                        fileItem.write(saveTo);
                        request.getSession().setAttribute("dirName", dirName);
                        request.getSession().setAttribute("fileName", fileName);
                         result = "success";
                        message = "File Uploaded Successfilly";
                        json.put("_result", result);
                        json.put("_message", message);
                        out.print(json);
                        
                        out.flush();
                        
                    } else {
                        result = "error";
                        message = "Error: " + fileName + " size is more than 5MB. Please upload lighter files.";
                        json.put("_result", result);
                        json.put("_message", message);
                        out.print(json);
                        out.flush();
                       
                    }
                }

            } catch (Exception ex) {
                log.error("Exception caught :: ", ex);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
