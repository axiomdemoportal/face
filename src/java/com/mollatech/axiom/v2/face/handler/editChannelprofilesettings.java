/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class editChannelprofilesettings extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editChannelprofilesettings.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final String itemtype = "SETTINGS";
    final int yes = 1;
    final int no = 0;
    final int SMS = 1;
    final int EMAIL = 2;
    final int VOICE = 3;
    final int USSD = 4;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String OperatorID = operatorS.getOperatorid();

        String _channelId = channel.getChannelid();
        SettingsManagement sMngmt = new SettingsManagement();
        int retValue = -1;

        String result = "success";
        String message = "Channel Settings Update Successful!!!";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        String _checkuser = request.getParameter("_checkuser");
        String _alertuser = request.getParameter("_alertuser");
        String _edituser = request.getParameter("_edituser");
        String _deleteuser = request.getParameter("_deleteuser");
        String _tokenload = request.getParameter("_tokenload");
        String _alertmedia = request.getParameter("_alertmedia");
        String _softwaretype = request.getParameter("_softwaretype");
//        String _pfxpath = request.getParameter("_pfxpath");
//        String _pfxpassword = request.getParameter("_pfxpassword");
        String _cleanuppath = request.getParameter("_cleanuppath");
        String _rssarchive = request.getParameter("_rssarchive");
        String _uploads = request.getParameter("_uploads");

        String _authorizer = request.getParameter("_authorizer");
        String _authorizerPendingDuration = request.getParameter("_Authorizer_PeningDuration");

        String _resetuser = request.getParameter("_resetuser");
        String _connectorstatus = request.getParameter("_connectorstatus");
        String _cleanupdays = request.getParameter("_cleanupdays");

        String _otpspecification = request.getParameter("_otpspecification");
        String _certspecification = request.getParameter("_certspecification");
        String _ocraspecification = request.getParameter("_ocraspecification");
        String _locationclassName = request.getParameter("_locationclassName");
        String _Authorizer_Units = request.getParameter("_Authorizer_Units");

        String _multipleSession = request.getParameter("_multipleSession");
        String _serverRestriction = request.getParameter("_serverRestriction");
        String _dayRestriction = request.getParameter("_dayrestriction");
        String _timerange = request.getParameter("_timerange");
        String _timefromampm = request.getParameter("_timefromampm");
        String _totimerange = request.getParameter("_totimerange");
        String _timetoampm = request.getParameter("_timetoampm");
        String _sweetSpotDeviation = request.getParameter("_sweetSpotDeviation");
        String _xDeviation = request.getParameter("_xDeviation");
        String _yDeviation = request.getParameter("_yDeviation");
        String _UnitAutherization= request.getParameter("UnitAutherization");
        
            log.debug("editChannelprofilesettings::channel is::" + channel.getName());
            log.debug("editChannelprofilesettings::Operator Id is::" + operatorS.getOperatorid());
            log.debug("editChannelprofilesettings::SessionId::" + sessionId);
            log.debug("editChannelprofilesettings::RemoteAccessLogin::" + remoteaccesslogin);
            log.debug("editChannelprofilesettings::Channel Id is::" + channel.getChannelid());
            log.debug("editChannelprofilesettings::To check the users::"+ _checkuser);
            log.debug("editChannelprofilesettings::alert users::"+ _alertuser);
            log.debug("editChannelprofilesettings::To edit users::"+ _edituser);
            log.debug("editChannelprofilesettings::To delete users::" + _deleteuser);
            log.debug("editChannelprofilesettings::tokenload is::" + _tokenload);
            log.debug("editChannelprofilesettings::media is::" + _alertmedia);
            log.debug("editChannelprofilesettings::Type of software is::"+ _softwaretype);
            log.debug("editChannelprofilesettings::path is::" + _cleanuppath);
            log.debug("editChannelprofilesettings::reserchieve is::" + _rssarchive);
            log.debug("editChannelprofilesettings::uploaded file is is::" + _uploads);
            log.debug("editChannelprofilesettings::Authorizer::" + _authorizer);
            log.debug("editChannelprofilesettings::pending Duration is::" + _authorizerPendingDuration);
            log.debug("editChannelprofilesettings::Resetuser is::" +_resetuser);
            log.debug("editChannelprofilesettings::connector status is::" +_connectorstatus);
            log.debug("editChannelprofilesettings::cleanup days are::" +_cleanupdays);
            log.debug("editChannelprofilesettings::OTP specification is::" + _otpspecification);
            log.debug("editChannelprofilesettings::Certificate specification is::"+ _certspecification);
            log.debug("editChannelprofilesettings::OCR specification is::" + _ocraspecification);
            log.debug("editChannelprofilesettings::name of class location is::" + _locationclassName);
            log.debug("editChannelprofilesettings::Authorized unit is::" + _Authorizer_Units);
            log.debug("editChannelprofilesettings::multiplesession is::" + _multipleSession);
            log.debug("editChannelprofilesettings::Server Restriction is::" + _serverRestriction);
            log.debug("editChannelprofilesettings::Day Restriction is::" + _dayRestriction);
            log.debug("editChannelprofilesettings::range of time is::" + _timerange);
            log.debug("editChannelprofilesettings::From am to pm time is::"+ _timefromampm);
            log.debug("editChannelprofilesettings::Time Range is::" + _totimerange);
            log.debug("editChannelprofilesettings::Time to am and pm is::" + _timetoampm);
            log.debug("editChannelprofilesettings::Sweet spot Deviation is::" + _sweetSpotDeviation);
            log.debug("editChannelprofilesettings::X Deviation is::" + _xDeviation);
            log.debug("editChannelprofilesettings::Y Deviation is::" + _yDeviation);
            log.debug("editChannelprofilesettings::Unit AuthenticationType is::" + _UnitAutherization);

        int _dayRes = -1;
        int _timeFromIn = -1;
        int _timeTo = 0;
        int _timeFromampm = 0;
        int _timeToampm = -1;
        int imultipleSession = -1;
        int isweetSpotDeviation = -1;
        int ixDeviation = -1;
        int iyDeviation = -1;

        int iserverRestriction = -1;

        if (_multipleSession != null) {
            imultipleSession = Integer.parseInt(_multipleSession);
        }
        if (_serverRestriction != null) {
            iserverRestriction = Integer.parseInt(_serverRestriction);
        }
        if (_dayRestriction != null) {
            _dayRes = Integer.parseInt(_dayRestriction);
        }
        if (_timerange != null&&_timerange.isEmpty()) {
            _timeFromIn = Integer.parseInt(_timerange);
        }

        if (_timefromampm != null && _timefromampm.isEmpty()) {
            _timeFromampm = Integer.parseInt(_timefromampm);
        }

        if (_totimerange != null) {
            _timeTo = Integer.parseInt(_totimerange);
        }
         if (_timeTo < 0 && _timeFromampm < 0) {
            try {
                result = "error";
                message = "Time Range can not be greater than 12!!!";
                json.put("_result", result);
                json.put("_message", message);
                 out.print(json);
                out.flush();
                return;
            } catch (Exception ex) {
                log.error("Exception caught :: ",ex);
            } 
        }
        
        

        if (_timetoampm != null) {
            _timeToampm = Integer.parseInt(_timetoampm);
        }
        if (_timeTo > 12 && _timeFromIn > 12) {
            try {
                result = "error";
                message = "Time Range can not be greater than 12!!!";
                json.put("_result", result);
                json.put("_message", message);
                 out.print(json);
                out.flush();
                return;
            } catch (Exception ex) {
                log.error("Exception caught :: ",ex);
            } 
        }

        int iType = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.CHANNELPROFILE_SETTING;
        int iPreference = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE;

        //   String strType = String.valueOf(iType);
        Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), iType, iPreference);

        ChannelProfile channelprofileObj = null;
        boolean bAddSetting = false;

        if (settingsObj == null) {
            channelprofileObj = new ChannelProfile();
            bAddSetting = true;
        } else {
            channelprofileObj = (ChannelProfile) settingsObj;
        }

        channelprofileObj._dayRestriction = _dayRes;
        channelprofileObj._timeFromInHour = _timeFromIn;
        channelprofileObj._timeToInHour = _timeTo;
        channelprofileObj._timfromampm = _timeFromampm;
        channelprofileObj._timetoampm = _timeToampm;
        channelprofileObj._multipleSession = imultipleSession;
        channelprofileObj._serverRestriction = iserverRestriction;
          if(_UnitAutherization!=null||!_UnitAutherization.isEmpty())
          {
              if(_UnitAutherization.equals("1"))
          channelprofileObj._HardWareTokenUnitAutherization=true;
              else
          channelprofileObj._HardWareTokenUnitAutherization=false;
                  
          }
//        channelprofileObj._userValidityDays=iuserValidityDays;
        if (_sweetSpotDeviation != null) {
            isweetSpotDeviation = Integer.parseInt(_sweetSpotDeviation);
        }
        if (_xDeviation != null) {
            ixDeviation = Integer.parseInt(_xDeviation);
        }
        if (_sweetSpotDeviation != null) {
            iyDeviation = Integer.parseInt(_yDeviation);
        }

        if (Integer.parseInt(_Authorizer_Units) == ChannelProfile._AUTHARIZATIONBYSAMEUNIT) {
            channelprofileObj.authorizationunit = ChannelProfile._AUTHARIZATIONBYSAMEUNIT;
        } else if (Integer.parseInt(_Authorizer_Units) == ChannelProfile._AUTHARIZATIONBYDIFFRENTUNIT) {
            channelprofileObj.authorizationunit = ChannelProfile._AUTHARIZATIONBYDIFFRENTUNIT;
        }
        if (Integer.parseInt(_authorizer) == ChannelProfile._AUTHARIZATIONSTATUSTOACTIVE) {
            channelprofileObj.authorizationStatus = ChannelProfile._AUTHARIZATIONSTATUSTOACTIVE;
        } else if (Integer.parseInt(_authorizer) == ChannelProfile._AUTHARIZATIONSTATUSTOINACTIVE) {
            channelprofileObj.authorizationStatus = ChannelProfile._AUTHARIZATIONSTATUSTOINACTIVE;
        }
        if (_authorizerPendingDuration != null&&_authorizerPendingDuration.isEmpty()==false) {
            channelprofileObj.authorizationDuration = Integer.parseInt(_authorizerPendingDuration);
        }
        if (Integer.parseInt(_resetuser) == ChannelProfile._RESETUSERTOACTIVE) {
            channelprofileObj.resetUser = ChannelProfile._RESETUSERTOACTIVE;
        } else if (Integer.parseInt(_resetuser) == ChannelProfile._RESETUSERTOUNASSIGN) {
            channelprofileObj.resetUser = ChannelProfile._RESETUSERTOUNASSIGN;
        }

        if (Integer.parseInt(_connectorstatus) == ChannelProfile.CONNECTORSTATUSFIVEMINS) {
            channelprofileObj.connectorStatus = ChannelProfile.CONNECTORSTATUSFIVEMINS;
        } else if (Integer.parseInt(_connectorstatus) == ChannelProfile.CONNECTORSTATUSTENMINS) {
            channelprofileObj.connectorStatus = ChannelProfile.CONNECTORSTATUSTENMINS;
        } else if (Integer.parseInt(_connectorstatus) == ChannelProfile.CONNECTORSTATUSTHIRTYMINS) {
            channelprofileObj.connectorStatus = ChannelProfile.CONNECTORSTATUSTHIRTYMINS;
        } else if (Integer.parseInt(_connectorstatus) == ChannelProfile.CONNECTORSTATUSONEHOUR) {
            channelprofileObj.connectorStatus = ChannelProfile.CONNECTORSTATUSONEHOUR;
        } else if (Integer.parseInt(_connectorstatus) == ChannelProfile.CONNECTORSTATUSTHREEHOUR) {
            channelprofileObj.connectorStatus = ChannelProfile.CONNECTORSTATUSTHREEHOUR;
        }

        if (Integer.parseInt(_cleanupdays) == ChannelProfile.CLEANUPDAYSNINETY) {
            channelprofileObj.cleanupdays = ChannelProfile.CLEANUPDAYSNINETY;
        } else if (Integer.parseInt(_cleanupdays) == ChannelProfile.CLEANUPDAYSONEEIGHTY) {
            channelprofileObj.cleanupdays = ChannelProfile.CLEANUPDAYSONEEIGHTY;
        } else if (Integer.parseInt(_cleanupdays) == ChannelProfile.CLEANUPDAYSTWOSEVENTY) {
            channelprofileObj.cleanupdays = ChannelProfile.CLEANUPDAYSTWOSEVENTY;
        } else if (Integer.parseInt(_cleanupdays) == ChannelProfile.CLEANUPDAYSTHREESIXTYFOUR) {
            channelprofileObj.cleanupdays = ChannelProfile.CLEANUPDAYSTHREESIXTYFOUR;
        } else if (Integer.parseInt(_cleanupdays) == ChannelProfile.CLEANUPDAYSSEVENTWENYEIGHT) {
            channelprofileObj.cleanupdays = ChannelProfile.CLEANUPDAYSSEVENTWENYEIGHT;
        }

        if (Integer.parseInt(_checkuser) == ChannelProfile._CHECKUSERBYNAME) {
            channelprofileObj.checkUser = ChannelProfile._CHECKUSERBYNAME;
        } else if (Integer.parseInt(_checkuser) == ChannelProfile._CHECKUSERBYEMAILPHONEUSERID) {
            channelprofileObj.checkUser = ChannelProfile._CHECKUSERBYEMAILPHONEUSERID;
        }

        if (Integer.parseInt(_alertuser) == yes) {
            channelprofileObj.alertUSer = ChannelProfile._USERALERT;
        } else if (Integer.parseInt(_alertuser) == ChannelProfile._NOUSERALERT) {
            channelprofileObj.alertUSer = ChannelProfile._NOUSERALERT;
        }

        if (Integer.parseInt(_deleteuser) == yes) {
            channelprofileObj.deleteUser = true;
        } else {
            channelprofileObj.deleteUser = false;
        }

        if (Integer.parseInt(_edituser) == yes) {
            channelprofileObj.editUser = true;
        } else {
            channelprofileObj.editUser = false;
        }

        if (Integer.parseInt(_tokenload) == yes) {
            channelprofileObj.tokensettingload = ChannelProfile.tokensettingloadyes;
        } else {
            channelprofileObj.tokensettingload = ChannelProfile.tokensettingloadno;
        }

        if (Integer.parseInt(_alertmedia) == SMS) {
            channelprofileObj.alertmedia = ChannelProfile._ALERTMEDIASMS;
        } else if (Integer.parseInt(_alertmedia) == EMAIL) {
            channelprofileObj.alertmedia = ChannelProfile._ALERTMEDIAEMAIL;
        } else if (Integer.parseInt(_alertmedia) == VOICE) {
            channelprofileObj.alertmedia = ChannelProfile._ALERTMEDIAVOICE;
        } else {
            channelprofileObj.alertmedia = ChannelProfile._ALERTMEDIAUSSD;

        }
        if (Integer.parseInt(_softwaretype) == yes) {
            channelprofileObj.swotptype = ChannelProfile._SWOTPTYPESIMPLE;
        } else {
            channelprofileObj.swotptype = ChannelProfile._SWOTPTYPEMOBILE;
        }

//        channelprofileObj.pfxpassword = _pfxpassword;
//        channelprofileObj.pfxfilepath = _pfxpath;
        channelprofileObj.bulkemailattachment = _uploads;
        channelprofileObj.cleanuppath = _cleanuppath;
        channelprofileObj.remotesignarchive = _rssarchive;

        channelprofileObj.locationclassname = _locationclassName;

        channelprofileObj.otpspecification = _otpspecification;
        channelprofileObj.certspecification = _certspecification;
        channelprofileObj.signspecification = _ocraspecification;
//        channelprofileObj._sweetSpotDeviation = isweetSpotDeviation;
//        channelprofileObj._xDeviation = ixDeviation;
//        channelprofileObj._yDeviation = iyDeviation;

        AuditManagement audit = new AuditManagement();
        if (bAddSetting == true) {
            retValue = sMngmt.addSetting(sessionId, _channelId, iType, iPreference, channelprofileObj);
            log.debug("editChannelprofilesettings::addSetting::" + retValue);
            String resultString = "ERROR";
            if (retValue == 0) {
                resultString = "SUCCESS";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "ADD Channel Profile Settings", resultString, retValue, "Setting Management",
                        "", "Checkuser=" + channelprofileObj.checkUser + ",AlertUser=" + channelprofileObj.alertUSer + ",edituser=" + channelprofileObj.editUser
                        + ",deleteuser=" + channelprofileObj.deleteUser + ",alertMedia=" + channelprofileObj.alertmedia + ",softwaretokentype=" + channelprofileObj.swotptype
                        + ",TokenSettingload=" + channelprofileObj.tokensettingload + ",cleanuppath =" + channelprofileObj.cleanuppath + ",resetUser =" + channelprofileObj.resetUser + ",cleanupDays =" + channelprofileObj.cleanupdays + ",connectorStatus =" + channelprofileObj.connectorStatus + "bulkemailattachment =" + channelprofileObj.bulkemailattachment + "remotesignarchive =" + channelprofileObj.remotesignarchive + "otpspecification =" + channelprofileObj.otpspecification
                        + "certspecification =" + channelprofileObj.certspecification + "signspecification =" + channelprofileObj.signspecification + "locationclassname =" + channelprofileObj.locationclassname
                        + "Authorization Status" + channelprofileObj.authorizationStatus + "Authorization Duration =" + channelprofileObj.authorizationDuration
                        + "multiple session=" + channelprofileObj._multipleSession + "Day Restriction=" + _dayRes + "Time From =" + _timeFromIn + "Time To=" + _timeTo + "_time from Am To Pm=" + _timefromampm + "Time to am to pm=" + _timetoampm,
                        itemtype, channel.getChannelid());
            } else if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add Channel Profile Settings", resultString, retValue, "Setting Management",
                        "", "Failed to Add Channel Profile Setting...!",
                        itemtype, channel.getChannelid());
            }

        } else {
            ChannelProfile oldglobalObj = (ChannelProfile) sMngmt.getSetting(sessionId, _channelId, iType, iPreference);
            retValue = sMngmt.changeSetting(sessionId, _channelId, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.CHANNELPROFILE_SETTING, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE, settingsObj, channelprofileObj);
            log.debug("editChannelprofilesettings::changeSetting::" + retValue);
            
            String resultString = "ERROR";
            if (retValue == 0) {

                resultString = "SUCCESS";

                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Change Channel Profile Settings", resultString, retValue, "Setting Management",
                        "Checkuser=" + oldglobalObj.checkUser + ",AlertUser=" + oldglobalObj.alertUSer + ",edituser=" + oldglobalObj.editUser
                        + ",deleteuser=" + oldglobalObj.deleteUser + ",alertMedia=" + oldglobalObj.alertmedia + ",softwaretokentype=" + oldglobalObj.swotptype
                        + ",TokenSettingload=" + oldglobalObj.tokensettingload
                        + "cleanuppath =" + oldglobalObj.cleanuppath + ",resetUser =" + oldglobalObj.resetUser + ",cleanupDays =" + oldglobalObj.cleanupdays + ",connectorStatus =" + oldglobalObj.connectorStatus + "bulkemailattachment =" + oldglobalObj.bulkemailattachment + "remotesignarchive =" + oldglobalObj.remotesignarchive + "otpspecification =" + oldglobalObj.otpspecification
                        + "certspecification =" + oldglobalObj.certspecification + "signspecification =" + oldglobalObj.signspecification + "locationclassname =" + oldglobalObj.locationclassname
                        + "Authorization Status" + oldglobalObj.authorizationStatus + "Authorization Duration =" + oldglobalObj.authorizationDuration
                        + "Day Restriction=" + oldglobalObj._dayRestriction + "Time From =" + oldglobalObj._timeFromInHour + "Time To=" + oldglobalObj._timeToInHour
                        + "_time from Am To Pm=" + oldglobalObj._timfromampm + "Time to am to pm=" + oldglobalObj._timeToInHour,
                        "Checkuser=" + channelprofileObj.checkUser + ",AlertUser=" + channelprofileObj.alertUSer + ",edituser=" + channelprofileObj.editUser
                        + ",deleteuser=" + channelprofileObj.deleteUser + ",alertMedia=" + channelprofileObj.alertmedia + ",softwaretokentype=" + channelprofileObj.swotptype
                        + ",TokenSettingload=" + channelprofileObj.tokensettingload + ",cleanuppath =" + channelprofileObj.cleanuppath + ",resetUser =" + channelprofileObj.resetUser + ",cleanupDays =" + channelprofileObj.cleanupdays + ",connectorStatus =" + channelprofileObj.connectorStatus + "bulkemailattachment =" + channelprofileObj.bulkemailattachment + "remotesignarchive =" + channelprofileObj.remotesignarchive + "otpspecification =" + channelprofileObj.otpspecification
                        + "certspecification =" + channelprofileObj.certspecification + "signspecification =" + channelprofileObj.signspecification + "locationclassname =" + channelprofileObj.locationclassname
                        + "Authorization Status" + channelprofileObj.authorizationStatus + "Authorization Duration =" + channelprofileObj.authorizationDuration
                        + "Day Restriction=" + _dayRes + "Time From =" + _timeFromIn + "Time To=" + _timeTo + "_time from Am To Pm=" + _timefromampm + "Time to am to pm=" + _timetoampm,
                        itemtype, channel.getChannelid());

            } else if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Change Channel Profile Settings", resultString, retValue, "Setting Management",
                        "Checkuser=" + oldglobalObj.checkUser + ",AlertUser=" + oldglobalObj.alertUSer + ",edituser=" + oldglobalObj.editUser
                        + ",deleteuser=" + oldglobalObj.deleteUser + ",alertMedia=" + oldglobalObj.alertmedia + ",softwaretokentype=" + oldglobalObj.swotptype
                        + ",TokenSettingload=" + oldglobalObj.tokensettingload + ",cleanuppath =" + oldglobalObj.cleanuppath + ",resetUser =" + oldglobalObj.resetUser + ",cleanupDays =" + oldglobalObj.cleanupdays + ",connectorStatus =" + oldglobalObj.connectorStatus + "bulkemailattachment =" + oldglobalObj.bulkemailattachment + "remotesignarchive =" + oldglobalObj.remotesignarchive + "otpspecification =" + oldglobalObj.otpspecification
                        + "certspecification =" + oldglobalObj.certspecification + "signspecification =" + oldglobalObj.signspecification + "locationclassname =" + oldglobalObj.locationclassname
                        + "Authorization Status" + oldglobalObj.authorizationStatus + "Authorization Duration =" + oldglobalObj.authorizationDuration
                        + "Day Restriction=" + oldglobalObj._dayRestriction + "Time From =" + oldglobalObj._timeFromInHour + "Time To=" + oldglobalObj._timeToInHour
                        + "_time from Am To Pm=" + oldglobalObj._timfromampm + "Time to am to pm=" + oldglobalObj._timeToInHour,
                        "Failed To Change Channel Profile Setting...!",
                        itemtype, channel.getChannelid());
            }
        }

        if (retValue == 0) {
            result = "success";
            message = "Channel Profile Settings Update Successful. For Consistency, Please Restart Interface Service!!!";
        } else if (retValue == -2) {
            result = "error";
            message = "Your Session is Expired ...!!!";
        } else {
            result = "error";
            message = "Channel Profile Settings Update Failed!!!";
        }
//        if (retValue != 0) {
//            result = "error";
//            message = "Channel Profile Settings Update Failed!!!";
//        }
        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
