/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.users;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.internal.handler.user.source.AxiomUser;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
public class edituserdetails extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(edituserdetails.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final String itemtype = "USERPASSWORD";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("is started :: ");
        String _userid = request.getParameter("_userIdD");
        log.debug("_userid :: "+_userid);
        String _user_name = request.getParameter("_NameD");
        log.debug("_user_name :: "+_user_name);
        String _user_country = request.getParameter("_CountryD");
        log.debug("_user_country :: "+_user_country);
        String _user_location = request.getParameter("_LocationD");
        log.debug("_user_location :: "+_user_location);
        String _user_street = request.getParameter("_StreetD");
        log.debug("_user_street :: "+_user_street);
        String _user_designation = request.getParameter("_DesignationD");
        log.debug("_user_designation :: "+_user_designation);
        String _user__OrganisationD = request.getParameter("_OrganisationD");
        log.debug("_user__OrganisationD :: "+_user__OrganisationD);
        String _user__OrganisationUnitD = request.getParameter("_OrganisationUnitD");
        log.debug("_user__OrganisationUnitD :: "+_user__OrganisationUnitD);
         Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
         log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());
        
        String result = null;
        String message = null;
        
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        
        if (_userid == null || _user_name == null || _user_name.isEmpty() == true) {
            result = "error";
            message = "Fill all Details!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
               log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        int retValue = -1;
        
        UserManagement uMngt = new UserManagement();
        AuditManagement audit = new AuditManagement();
        AuthUser olduser = uMngt.getUser(sessionId, channel.getChannelid(), _userid);
        retValue = uMngt.EditUserDetails(sessionId,channel.getChannelid(),_userid, _user_name, _user__OrganisationD,_user__OrganisationUnitD, _user_country, _user_location,_user_street, _user_designation);
        log.debug("EditUserDetails :: "+retValue);
        String resultString = "Failure";
        if (retValue == 0) {
            resultString = "Success";
        }
        if (retValue == 0) {
            result = "success";
            message = "User updated Successfully!!";
             AuthUser newUser = uMngt.getUser(sessionId, channel.getChannelid(), _userid);
             audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Edit User Details", resultString, retValue,
                    "User Management", "User ID=" + olduser.getUserId() + ",Name=" + olduser.getUserName()
                    + ",Phone=" + olduser.getPhoneNo() + ",Email=" + olduser.getEmail() + ",Country=" + olduser.country
                    + ",Designation=" + olduser.designation+ ",IdType=" + olduser.idType+ ",Location=" + olduser.location
                    + ",Organisation=" + olduser.organisation+ ",Organisation=" + olduser.organisationUnit+ ",street=" + olduser.street
                     + ",userIdentity=" + olduser.userIdentity,
                    "User ID=" + newUser.getUserId() + ",Name=" + newUser.getUserName()
                    + ",Phone=" + newUser.getPhoneNo() + ",Email=" + newUser.getEmail() + ",Country=" + newUser.country
                    + ",Designation=" + newUser.designation+ ",IdType=" + newUser.idType+ ",Location=" + newUser.location
                    + ",Organisation=" + newUser.organisation+ ",Organisation=" + newUser.organisationUnit+ ",street=" + newUser.street
                     + ",userIdentity=" + newUser.userIdentity,
                    itemtype, _userid);
        }
        else{
            result = "error";
            message = "User updated failed!!";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Edit User Details", resultString, retValue,
                    "User Management", "User ID=" + olduser.getUserId() + ",Name=" + olduser.getUserName()
                    + ",Phone=" + olduser.getPhoneNo() + ",Email=" + olduser.getEmail() + ",Country=" + olduser.country
                    + ",Designation=" + olduser.designation+ ",IdType=" + olduser.idType+ ",Location=" + olduser.location
                    + ",Organisation=" + olduser.organisation+ ",Organisation=" + olduser.organisationUnit+ ",street=" + olduser.street
                     + ",userIdentity=" + olduser.userIdentity,
                   "Failed to update user ="+_userid,
                    itemtype, _userid);
        }
        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception e) {
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("is ended :: ");
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
