package com.mollatech.axiom.v2.face.handler.otptokens;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Otptokens;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.AuthorizationManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.RegistrationCodeTrailManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class sendregistration extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(sendregistration.class.getName());

    final String itemType = "OTPTOKENS";
    final String itemTypeAUTH = "AUTHORIZATION";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("is started :: ");

        final String strFAILED = "FAILED";
        final int FAILED = -1;
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("channel :: "+channel.getName());
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("sessionId :: "+sessionId);

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        log.debug("remoteaccesslogin :: "+remoteaccesslogin);
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("operatorS :: "+operatorS.getOperatorid());
        String _userid = request.getParameter("_userid");
        log.debug("_userid :: "+_userid);
        String _subcategory = request.getParameter("_subcategory");
        log.debug("_subcategory :: "+_subcategory);
        String _category = request.getParameter("_category"); 
        log.debug("_category :: "+_category);
        String _newvalue = request.getParameter("newvalue");
        log.debug("new value = "+_newvalue);
        String _oldvalue = request.getParameter("oldvalue");
        log.debug("oldvalue = "+_oldvalue);
        //added by vikram = 12/09/2016
        String _type = request.getParameter("_type"); 
        log.debug("_type :: "+_type);
        if ( _type == null || _type.isEmpty() == true || _type.compareToIgnoreCase("email") != 0){
            _type = "mobile"; 
        }
        //end of addition
        
        
        int subCategory = 0;
        if (_subcategory != null) {
            subCategory = Integer.parseInt(_subcategory);
        }
        
        int category = 0;
        if (_category != null) {
            category = Integer.parseInt(_category);
        }
        String result = "success";
        String message = "Registration send on your mobile....";
        String strPassword = null;
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        
        if (_userid == null) {
            result = "error";
            message = "Userid not sent ....";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
        int retValue = -1;
        String registrationCode = null;
        OTPTokenManagement oManagement = new OTPTokenManagement(channel.getChannelid());
        AuditManagement audit = new AuditManagement();

        registrationCode = oManagement.generateRegistrationCode(sessionId, channel.getChannelid(), _userid, category);
        log.debug("generateRegistrationCode :: "+registrationCode);
        
        
        Otptokens otp = oManagement.getOtpObjByUserId(sessionId, channel.getChannelid(), _userid, category);
        
        if (otp == null) {
            result = "error";
            message = "OTP token not found/assigned!!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;
        }
       
        //SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm");
       SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
       
        String strExpiry = sdf1.format(otp.getExpirytime());
        String resultString = "ERROR";

        AXIOMStatus status = new AXIOMStatus();
        status.iStatus = FAILED;
        status.strStatus = strFAILED;
        String _approvalId = null;
        AuthUser user = null;
        OperatorsManagement oprMngt = new OperatorsManagement();
        UserManagement uMngt = new UserManagement();
         user = uMngt.getUser(sessionId, channel.getChannelid(), _userid);
        AuthorizationManagement auth = new AuthorizationManagement();
        String strOpName = "-";
        String struserName = "-";
        String strAction = "-";
        int iapprovalID = -1;
        if (registrationCode != null) {
           
            Templates temp = null;
            TemplateManagement tObj = new TemplateManagement();
            temp = tObj.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.MOBILE_SOFTWARE_TOKEN_REGISTER_TEMPLATE);

            if (temp.getStatus() == tObj.ACTIVE_STATUS) {
                ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                Date d = new Date();
                String tmessage = (String) UtilityFunctions.deserializeFromObject(bais);
                tmessage = tmessage.replaceAll("#name#", user.getUserName());
                tmessage = tmessage.replaceAll("#channel#", channel.getName());
                tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));

                tmessage = tmessage.replaceAll("#expiry#", strExpiry);
                if (otp.getSubcategory() == OTPTokenManagement.SW_WEB_TOKEN) {
                    tmessage = tmessage.replaceAll("#tokentype#", "WEB");

                }
                if (otp.getSubcategory() == OTPTokenManagement.SW_MOBILE_TOKEN) {
                    tmessage = tmessage.replaceAll("#tokentype#", "MOBILE");
                    String strSWOTPType = LoadSettings.g_sSettings.getProperty("sw.otp.type");
                    log.debug("strSWOTPType"+strSWOTPType);
                    System.out.println("strSWOTPType ##"+strSWOTPType);
                    if (strSWOTPType != null && strSWOTPType.compareToIgnoreCase("simple") == 0) { 
                        // we are forcing it to be time based for mobile token.
                        registrationCode = "1" + registrationCode;
                    }
                }
                if (otp.getSubcategory() == OTPTokenManagement.SW_PC_TOKEN) {
                    tmessage = tmessage.replaceAll("#tokentype#", "PC");
                }

                tmessage = tmessage.replaceAll("#regcode#", registrationCode);
                

                SendNotification send = new SendNotification();
                _approvalId = request.getParameter("_approvalId");
                log.debug("_approvalId :: "+_approvalId);
                SettingsManagement sMngmt = new SettingsManagement();

                Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                ChannelProfile chSettingObj = null;
                if (settingsObj != null) {
                    chSettingObj = (ChannelProfile) settingsObj;
                }
                String _unitId = request.getParameter("_unitId");
                log.debug("_unitId :: "+_unitId);
                if (_unitId != null) {
                    if (chSettingObj != null) {

                        if (chSettingObj.authorizationunit == 1) {

                            int iUnitId = Integer.parseInt(_unitId);
                            if (iUnitId != operatorS.getUnits()) {
                                result = "error";
                                message = "Sorry, Must be marked by same branch operator!!!";
                                try {
                                    json.put("_result", result);
                                    json.put("_message", message);
                                } catch (Exception e) {
                                    log.error("exception caught :: ",e);
                                }
                                out.print(json);
                                out.flush();
                                return;
                            }
                        }
                    }
                }

                if (_approvalId != null) {
                  if (operatorS.getRoleid() != 6) {
                if (operatorS.getRoleid() >= 3) {
                    result = "error";
                    message = "Sorry, But you don't have permissions for this action!!!";
                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                    } catch (Exception e) {
                        log.error("exception caught :: ",e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
            }
//                    iapprovalID = Integer.parseInt(_approvalId);

                    String _markerID = request.getParameter("_markerID");
                    Operators op = oprMngt.getOperatorById(channel.getChannelid(), _markerID);
                    if (op != null) {
                        strOpName = op.getName();
                    }
                    if (user != null) {
                        struserName = user.getUserName();
                    }

                }
                
                
        

                //added by vikram = 12/09/2016
                if ( _type.compareToIgnoreCase("mobile") == 0 )    {
                    status = send.SendOnMobileNoWaiting(channel.getChannelid(), user.phoneNo, 
                            tmessage, 1, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                }
                else if ( _type.compareToIgnoreCase("email") == 0 )    {
                    message="Registration send on your Email....";
                        status = send.SendEmail(channel.getChannelid(), user.email, "Registation Code For Token", 
                                tmessage, null, null, null, null,  
                                Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));    
                }

                //end of addition

//                }
                //end of authorization
//                status = send.SendOnMobileNoWaiting(channel.getChannelid(), user.getPhoneNo(), tmessage, 1, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
            }
            if (status != null) {
                if (status.iStatus == SendNotification.PENDING||status.iStatus==SendNotification.SENT) {
                    RegistrationCodeTrailManagement rManagement = new RegistrationCodeTrailManagement();
                    String registrationcode = UtilityFunctions.Bas64SHA1(registrationCode);
                    rManagement.addRegCodeTrail(channel.getChannelid(), user.getUserId(), registrationcode, RegistrationCodeTrailManagement.SENT, RegistrationCodeTrailManagement.OTPTOKEN);

                    if (_approvalId != null) {
                        //int res = auth.removeAuthorizationRequest(sessionId, channel.getChannelid(), iapprovalID);
                         int res = auth.removeAuthorizationRequest(sessionId, channel.getChannelid(),operatorS.getOperatorid(), _approvalId,_newvalue,_oldvalue);
                        if (res == 0) {
                            resultString = "SUCCESS";
                            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                                    request.getRemoteAddr(), channel.getName(),
                                    remoteaccesslogin, operatorS.getName(), new Date(), 
                                    "Remove Authorization Request",
                                    resultString, retValue,
                                    "Authorization Management", "Action =" + strAction + ",Action Marked By=" + strOpName
                                    + ",Action Marked On=" + struserName, "Removed successfully!!!",
                                    itemTypeAUTH, _userid);
                        }
                    }
                } else if (status.iStatus == AuthorizationManagement.RETURN_AUTORIZATION_RESULT) {
                    
//                    if (_approvalId != null) {

                        resultString = "SUCCESS";
                        audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                                request.getRemoteAddr(), channel.getName(),
                                remoteaccesslogin, operatorS.getName(), new Date(), "Add Authorization Request :Send Registration Code",
                                resultString, retValue,
                                "Authorization Management", "Cert send successfully to " + user.getUserName(),
                                "Added successfully!!!",
                                itemTypeAUTH, _userid);

//                    }

                    result = "success";
                    message = "Request is pending for approval!!!";
                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                     } catch (Exception ex) {
               log.error("exception caught :: ",ex);
            }
                    
                    out.print(json);
                    out.flush();
                    return;
                }
            }
            retValue = 0;
            resultString = "SUCCESS";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), 
                    "Generate Registration Code",
                    resultString, 0, 
                    "Token Management", "", 
                    "Registration Code = ******" /*strRegCode*/, itemType, 
                    _userid);            
        } else if (registrationCode == null) {
            resultString = "ERROR";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                    request.getRemoteAddr(), channel.getName(),
                    remoteaccesslogin, operatorS.getName(), new Date(), "Generate Registration Code",
                    resultString,
                    -1, "Token Management", "", "Failed To generate Registration Code",
                    itemType, _userid);

        }
        
        
       else if (status.iStatus != SendNotification.PENDING||status.iStatus != SendNotification.SENT) { // -2 = pending
            result = "error";
            message = "Failed to send registration code!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                log.error("exception caught :: ",e);
            }
            out.print(json);
            out.flush();
            return;

        }
        

        try {
            
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            log.error("exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
        }
        log.info("is ended :: ");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
