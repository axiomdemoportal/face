/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class terminatesession extends HttpServlet {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(terminatesession.class.getName());

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


        response.setContentType("application/json");

        String _sessionId = request.getParameter("_sessionId");
        log.debug("terminatesession::sessionid::"+_sessionId);
        
        
        _sessionId = _sessionId.trim();
        _sessionId = _sessionId.replaceAll(" ", "");
        _sessionId = _sessionId.replaceAll("\n", "");
        
        
        
        String result = "success";
        String message = "Session terminated successfully....";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        log.debug("terminatesession::Operator Id is::"+operator.getOperatorid());
        
         
        if (_sessionId == null) {
            result = "error";
            message = "Invalid/Empty Session ID!!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){log.error("Exception caught :: ",e);}
            out.print(json);
            out.flush();
            return;
        }
        
        int retValue = -1;
        String sessionid = request.getParameter("_sessionId");

        SessionManagement sm = new SessionManagement();
        
        retValue = sm.CloseSession(sessionid);

        if (retValue == 0) {
            result = "success";
            message = "The session successfully closed!!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){log.error("Exception caught :: ",e);}
            out.print(json);
            out.flush();
            return;

        }

       if (retValue != 0) {
            result = "error";
            message = "The session couldn't be terminate, please try again !!!";
            try { json.put("_result", result);
            json.put("_message", message);
            }catch(Exception e){log.error("Exception caught :: ",e);}
            out.print(json);
            out.flush();
            return;
        }
        try {
            json.put("_result", result);
            json.put("_message", message);

        }catch(Exception e){log.error("Exception caught :: ",e);} finally {
            out.print(json);
            out.flush();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
