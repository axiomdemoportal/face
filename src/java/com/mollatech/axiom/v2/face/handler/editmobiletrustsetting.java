/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.MobileTrustManagment;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.settings.MobileTrustSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bouncycastle.util.encoders.Base64;
import org.json.simple.JSONObject;

/**
 *
 * @author mollatech1
 */
public class editmobiletrustsetting extends HttpServlet {
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editmobiletrustsetting.class.getName());

    final String itemtype = "SETTINGS";
    public static final int ANDROID = 1;
    public static final int IOS = 2;
    public static final int WEB = 3;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");

        //audit parameter
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");

        String OperatorID = operatorS.getOperatorid();

        String _channelId = channel.getChannelid();
        SettingsManagement sMngmt = new SettingsManagement();
        int retValue = -1;

        String result = "success";
        String message = "MobileTrust Settings Update Successful!!!";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

            log.debug("channel :: " + channel.getName());
            log.debug("operatorS :: " + operatorS.getOperatorid());
            log.debug("sessionId :: " + sessionId);
            log.debug("remoteaccesslogin :: " + remoteaccesslogin);
            log.debug("channel :: " + channel.getChannelid());
      
//        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.MOBILETRUST) != 0) {
//            result = "error";
//            message = "This feature is not available in this license!!!";
//
//            try {
//                json.put("_result", result);
//                json.put("_message", message);
//            } catch (Exception e) {
//                log.error("Exception caught :: ",e);
//            }
//
//            out.print(json);
//            out.flush();
//            return;
//        }

        //String CountryName[] = request.getParameterValues("_CountryName");
        //String _CountryName = request.getParameter("_CountryName");
        String _CountryName = request.getParameter("_CountryName");
        log.debug("_CountryName :: "+_CountryName);
        String _ExpiryMin = request.getParameter("_ExpiryMin");
        log.debug("_ExpiryMin :: "+_ExpiryMin);
        int ExpiryMin = 0;
        if (_ExpiryMin != null) {
            ExpiryMin = Integer.parseInt(_ExpiryMin);
        }
        String _Backup = request.getParameter("_Backup");
        
        log.debug("_Backup :: "+_Backup);
        String _hashalgo = request.getParameter("_hashalgo");
        log.debug("HashAlgo"+_hashalgo);
        String _SilentCall = request.getParameter("_SilentCall");
        log.debug("_SilentCall :: "+_SilentCall);
        String _SelfDestructEnable = request.getParameter("_SelfDestructEnable");
        log.debug("_SelfDestructEnable :: "+_SelfDestructEnable);
        String _SelfDestructAttempts = request.getParameter("_SelfDestructAttempts");
        log.debug("_SelfDestructAttempts :: "+_SelfDestructAttempts);
        int SelfDestructAttempts = 0;
        if (_SelfDestructAttempts != null) {
            SelfDestructAttempts = Integer.parseInt(_SelfDestructAttempts);
        }
//           if(request.getParameter("_selfDestructURl")==null||request.getParameter("_selfDestructURl").isEmpty())
//           {
//               try{
//                   result="error";
//                   message="SelfDestruct url can not be null";
//                 json.put("_result", result);
//             json.put("_message", message); 
//              out.print(json);
//               out.flush();
//            return;
//               }catch(Exception ex)
//               {
//                   log.error("Exception caught :: ",ex);
//               }
//               
//           }
        String _selfDestructURl = request.getParameter("_selfDestructURl");

        String _timeStamp = request.getParameter("_timeStamp");
        String _deviceTracking = request.getParameter("_deviceTracking");
        String _geoFencing = request.getParameter("_geoFencing");

        String _allowAlert = request.getParameter("_allowAlertM");
        String _gatewayType = request.getParameter("_gatewayTypeM");
        String _alertAttempt = request.getParameter("_alertAttemptM");
        String _templateName = request.getParameter("_templateName");
        String _allowAlertFor = request.getParameter("_allowAlertForM");
        String _selfDestructURlM = request.getParameter("_selfDestructURlM");
        String _type = request.getParameter("_type");
        String _allowedDomainNames= request.getParameter("_arrallowedDomainNames");
        
        log.debug("_selfDestructURl :: "+_selfDestructURl);
        log.debug("_timeStamp :: "+_timeStamp);
        log.debug("_deviceTracking :: "+_deviceTracking);
        log.debug("_geoFencing :: "+_geoFencing);
        log.debug("_allowAlert :: "+_allowAlert);
        log.debug("_gatewayType :: "+_gatewayType);
        log.debug("_alertAttempt :: "+_alertAttempt);
        log.debug("_templateName :: "+_templateName);
        log.debug("_allowAlertFor :: "+_allowAlertFor);
        log.debug("_selfDestructURlM :: "+_selfDestructURlM);
        log.debug("_type :: "+_type);
        log.debug("_allowedDomainNames :: "+_allowedDomainNames);
        
        
        
        
        String[] allowedDomain=null;
        
        if (_allowedDomainNames != null) {
            allowedDomain = _allowedDomainNames.split(",");
        }
        
        
        String _isBlackListedCoutriesCheckEnable = request.getParameter("_isBlackListed");

        String[] _blacListedCountries = request.getParameterValues("_blackListedCountries");
        
        log.debug("_isBlackListedCoutriesCheckEnable :: " +_isBlackListedCoutriesCheckEnable);
        log.debug("_blacListedCountries :: " +_blacListedCountries);

        String strBlockForeignCountry = "";
        if (_blacListedCountries != null) {
            for (int i = 0; i < _blacListedCountries.length; i++) {
                strBlockForeignCountry = strBlockForeignCountry + _blacListedCountries[i] + ",";
            }
        }
        boolean isBlackListed = false;
        if (_isBlackListedCoutriesCheckEnable != null) {
        if (_isBlackListedCoutriesCheckEnable.equals("1")) {
            isBlackListed = true;
        }
        }
        
        int iType = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.MOBILETRUST_SETTING;
        int iPreference = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE;

        //   String strType = String.valueOf(iType);
        Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), iType, iPreference);

        MobileTrustSettings mobileObj = null;
        boolean bAddSetting = false;

        if (settingsObj == null) {
            mobileObj = new MobileTrustSettings();
            bAddSetting = true;
        } else {
            mobileObj = (MobileTrustSettings) settingsObj;
        }
        boolean allowAlert = false;
        int ialertAttempt = 0;
        int itemplateId = 0;
        int igatewayType = 0;
        int iAlertFor = 0;
        boolean backup = false;
        boolean SelfDestructEnable = false;
        boolean SilentCall = false;
        boolean timestamp = false;
        boolean deviceTracking = false;
        boolean geoFencing = false;
        if (_type.equals("0")) {

            if (_allowAlert.equals("1")) {
                allowAlert = true;
            }
            if (_alertAttempt != null) {
                ialertAttempt = Integer.parseInt(_alertAttempt);
            }

//            if (_templateName != null) {
//                itemplateId = Integer.parseInt(_templateName);
//            }

            if (_gatewayType != null) {
                igatewayType = Integer.parseInt(_gatewayType);
            }

            if (_allowAlertFor != null) {
                iAlertFor = Integer.parseInt(_allowAlertFor);
            }

            if (_Backup.equals("1")) {
                backup = true;
            }

            if (_SelfDestructEnable.equals("1")) {
                SelfDestructEnable = true;
            }

            if (_SilentCall.equals("1")) {
                SilentCall = true;
            }

            if (_timeStamp.equals("1")) {
                timestamp = true;
            }

            if (_deviceTracking.equals("1")) {
                deviceTracking = true;
            }

            if (_geoFencing.equals("1")) {
                geoFencing = true;
            }
            mobileObj.bSilentCall = SilentCall;
            mobileObj.expiryTimeStampInMins = ExpiryMin;
            mobileObj.homeCountryCode = _CountryName;
            mobileObj.isKeyBackupEnable = backup;
            mobileObj.selfDestructAttemps = SelfDestructAttempts;
            mobileObj.selfdestructalertENABLED = SelfDestructEnable;
//            mobileObj.selfdestructalertURL = _selfDestructURl;
            mobileObj.bTimestamp = timestamp;
            mobileObj.bDeviceTracking = deviceTracking;
            mobileObj.bGeoFencing = geoFencing;
            mobileObj.mobileAlert = allowAlert;
            mobileObj.mobileAlertVia = igatewayType;
            mobileObj.mobileAlertAttempts = ialertAttempt;
            if(_hashalgo.equals("")){
             mobileObj.hashingAlgo="SHA1";
            }else
            {
              mobileObj.hashingAlgo=_hashalgo;
            }
            
           
            //changed by vikram
            TemplateManagement tempObj = new TemplateManagement();
            Templates tObj =  tempObj.LoadbyName(_channelId, _templateName);
            mobileObj.mobileAlertTemplateID = tObj.getTemplateid();
            //end of change 
            mobileObj.mobileAlertFor = iAlertFor;
            mobileObj.systemCountryCheck = isBlackListed;
          
           
        if (strBlockForeignCountry != null ) {
        mobileObj.listOfBlackListedCountries = strBlockForeignCountry;
        
        }
        } else if (_type.equals("1")) {
            mobileObj.selfdestructalertURL = _selfDestructURl;
        } else if (_type.equals("2")) {
            mobileObj.selfdestructalertURLWEB = _selfDestructURlM;
             mobileObj.listofAllowedDomainnames=allowedDomain;
        }
      
       
        AuditManagement audit = new AuditManagement();
        if (bAddSetting == true) {
            retValue = sMngmt.addSetting(sessionId, _channelId, iType, iPreference, mobileObj);
            log.debug("editmobiletrustsetting::addSetting::" +retValue);
            String resultString = "ERROR";
            if (retValue == 0) {
                resultString = "SUCCESS";
                if (_type.equals("0")) {


                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add Mobile Trust Setting", resultString, retValue, "Setting Management",
                            "", "HomeCountryCode =" + mobileObj.homeCountryCode + "SelfdestructalertURL=" + mobileObj.selfdestructalertURL + "SilentCall =" + mobileObj.bSilentCall
                            + "ExpiryTimeStampInMins=" + mobileObj.expiryTimeStampInMins + "KeyBackupEnable=" + mobileObj.isKeyBackupEnable + "SelfDestructAttemps=" + mobileObj.selfDestructAttemps
                            + "SelfdestructalertENABLED=" + mobileObj.selfdestructalertENABLED
                            + "Timestamp = " + timestamp + ",Device Tacking=" + deviceTracking + ",Geofencing=" + geoFencing
                            + "Password Alert=" + allowAlert + "Alert via=" + igatewayType + "Password Attempts=" + ialertAttempt
                            + "Template Id =" + itemplateId + "Alert For =" + iAlertFor + "Self Destruct URL Web=" + _selfDestructURlM
                            + mobileObj.systemCountryCheck + mobileObj.listOfBlackListedCountries,
                            itemtype, _channelId);
                } else if (_type.equals("1")) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Mobile Trust Setting", resultString, retValue, "Setting Management",
                            "", "SelfdestructalertURL=" + mobileObj.selfdestructalertURL+ mobileObj.systemCountryCheck + mobileObj.listOfBlackListedCountries,
                            itemtype, _channelId);
                } else if (_type.equals("2")) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Mobile Trust Setting", resultString, retValue, "Setting Management",
                            "", "SelfdestructalertURL=" + mobileObj.selfdestructalertURLWEB+ mobileObj.systemCountryCheck + mobileObj.listOfBlackListedCountries,
                            itemtype, _channelId);
                }

            } else if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Add Mobile Trust Setting", resultString, retValue, "Setting Management",
                        "", "Failed to Add Mobile Trust Setting",
                        itemtype, _channelId);
            }

        } else {
            MobileTrustSettings oldmobileObj = (MobileTrustSettings) sMngmt.getSetting(sessionId, _channelId, iType, iPreference);
            retValue = sMngmt.changeSetting(sessionId, _channelId, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.MOBILETRUST_SETTING, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE, settingsObj, mobileObj);
            log.debug("editmobiletrustsetting::changeSetting::" +retValue);
            String resultString = "ERROR";
            if (retValue == 0) {

                resultString = "SUCCESS";
                if (_type.equals("0")) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change Mobile Trust Setting", resultString, retValue, "Setting Management",
                            "HomeCountryCode =" + oldmobileObj.homeCountryCode + "SelfdestructalertURL=" + oldmobileObj.selfdestructalertURL + "SilentCall =" + oldmobileObj.bSilentCall
                            + "ExpiryTimeStampInMins=" + oldmobileObj.expiryTimeStampInMins + "KeyBackupEnable=" + oldmobileObj.isKeyBackupEnable + "SelfDestructAttemps=" + oldmobileObj.selfDestructAttemps
                            + "SelfdestructalertENABLED=" + oldmobileObj.selfdestructalertENABLED
                            + "Timestamp = " + oldmobileObj.bTimestamp + ",Device Tacking=" + oldmobileObj.bDeviceTracking + ",Geofencing=" + oldmobileObj.bGeoFencing
                            + "Password Alert=" + oldmobileObj.mobileAlert + "Alert via=" + oldmobileObj.mobileAlertVia + "Password Attempts=" + oldmobileObj.mobileAlertAttempts
                            + "Template Id =" + oldmobileObj.mobileAlertTemplateID + "Alert For =" + oldmobileObj.mobileAlertFor
                            + "Self Destruct URL Web=" + oldmobileObj.selfdestructalertURLWEB+ oldmobileObj.systemCountryCheck + oldmobileObj.listOfBlackListedCountries,
                            "HomeCountryCode =" + mobileObj.homeCountryCode + "SelfdestructalertURL=" + mobileObj.selfdestructalertURL + "SilentCall =" + mobileObj.bSilentCall
                            + "ExpiryTimeStampInMins=" + mobileObj.expiryTimeStampInMins + "KeyBackupEnable=" + mobileObj.isKeyBackupEnable + "SelfDestructAttemps=" + mobileObj.selfDestructAttemps
                            + "SelfdestructalertENABLED=" + mobileObj.selfdestructalertENABLED
                            + "Timestamp = " + timestamp + ",Device Tacking=" + deviceTracking + ",Geofencing=" + geoFencing
                            + "Password Alert=" + allowAlert + "Alert via=" + igatewayType + "Password Attempts=" + ialertAttempt
                            + "Template Id =" + itemplateId + "Alert For =" + iAlertFor+ mobileObj.systemCountryCheck + mobileObj.listOfBlackListedCountries,
                            itemtype, _channelId);
                }
                if (_type.equals("1")) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change Mobile Trust Setting", resultString, retValue, "Setting Management",
                            "HomeCountryCode =" + oldmobileObj.homeCountryCode + "SelfdestructalertURL=" + oldmobileObj.selfdestructalertURL + "SilentCall =" + oldmobileObj.bSilentCall
                            + "ExpiryTimeStampInMins=" + oldmobileObj.expiryTimeStampInMins + "KeyBackupEnable=" + oldmobileObj.isKeyBackupEnable + "SelfDestructAttemps=" + oldmobileObj.selfDestructAttemps
                            + "SelfdestructalertENABLED=" + oldmobileObj.selfdestructalertENABLED
                            + "Timestamp = " + oldmobileObj.bTimestamp + ",Device Tacking=" + oldmobileObj.bDeviceTracking + ",Geofencing=" + oldmobileObj.bGeoFencing
                            + "Password Alert=" + oldmobileObj.mobileAlert + "Alert via=" + oldmobileObj.mobileAlertVia + "Password Attempts=" + oldmobileObj.mobileAlertAttempts
                            + "Template Id =" + oldmobileObj.mobileAlertTemplateID + "Alert For =" + oldmobileObj.mobileAlertFor
                            + "Self Destruct URL Web=" + oldmobileObj.selfdestructalertURLWEB + oldmobileObj.systemCountryCheck +  oldmobileObj.listOfBlackListedCountries,
                            "self destruct url =" + _selfDestructURl,
                            itemtype, _channelId);
                }
                if (_type.equals("2")) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "Change Mobile Trust Setting", resultString, retValue, "Setting Management",
                            "HomeCountryCode =" + oldmobileObj.homeCountryCode + "SelfdestructalertURL=" + oldmobileObj.selfdestructalertURL + "SilentCall =" + oldmobileObj.bSilentCall
                            + "ExpiryTimeStampInMins=" + oldmobileObj.expiryTimeStampInMins + "KeyBackupEnable=" + oldmobileObj.isKeyBackupEnable + "SelfDestructAttemps=" + oldmobileObj.selfDestructAttemps
                            + "SelfdestructalertENABLED=" + oldmobileObj.selfdestructalertENABLED
                            + "Timestamp = " + oldmobileObj.bTimestamp + ",Device Tacking=" + oldmobileObj.bDeviceTracking + ",Geofencing=" + oldmobileObj.bGeoFencing
                            + "Password Alert=" + oldmobileObj.mobileAlert + "Alert via=" + oldmobileObj.mobileAlertVia + "Password Attempts=" + oldmobileObj.mobileAlertAttempts
                            + "Template Id =" + oldmobileObj.mobileAlertTemplateID + "Alert For =" + oldmobileObj.mobileAlertFor
                            + "Self Destruct URL Web=" + oldmobileObj.systemCountryCheck +  oldmobileObj.selfdestructalertURLWEB+  oldmobileObj.listOfBlackListedCountries,
                            "self destruct web =" + _selfDestructURlM,
                            itemtype, _channelId);
                }

            } else if (retValue != 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                        request.getRemoteAddr(),
                        channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                        "Change Mobile Trust Setting", resultString, retValue, "Setting Management",
                        "HomeCountryCode =" + oldmobileObj.homeCountryCode + "SelfdestructalertURL=" + oldmobileObj.selfdestructalertURL + "SilentCall =" + oldmobileObj.bSilentCall
                        + "ExpiryTimeStampInMins=" + oldmobileObj.expiryTimeStampInMins + "KeyBackupEnable=" + oldmobileObj.isKeyBackupEnable + "SelfDestructAttemps=" + oldmobileObj.selfDestructAttemps
                        + "SelfdestructalertENABLED=" + oldmobileObj.selfdestructalertENABLED
                        + "Timestamp = " + oldmobileObj.bTimestamp + ",Device Tacking=" + oldmobileObj.bDeviceTracking + ",Geofencing=" + oldmobileObj.bGeoFencing
                        + "Password Alert=" + oldmobileObj.mobileAlert + "Alert via=" + oldmobileObj.mobileAlertVia + "Password Attempts=" + oldmobileObj.mobileAlertAttempts
                        + "Template Id =" + oldmobileObj.mobileAlertTemplateID + "Alert For =" + oldmobileObj.mobileAlertFor + "Self Destruct URL Web=" + oldmobileObj.selfdestructalertURLWEB,
                        "Failed To Edit Mobile Trust Setting...!",
                        itemtype, _channelId);
            }
        }

        if (retValue != 0) {
            result = "error";
            message = " Mobile Trust Settings Update Failed!!!";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
            if (retValue == 0) {
                MobileTrustManagment mtm = new MobileTrustManagment();
                String licensekeyforAndroid = mtm.makelicensekey(_channelId, ANDROID);
                String b64licensekeyforAndroid = new String(Base64.encode(licensekeyforAndroid.getBytes()));
                json.put("_licenseforAndroid", b64licensekeyforAndroid);
                String licensekeyforIphone = mtm.makelicensekey(_channelId, IOS);
                String b64licensekeyforIphone = new String(Base64.encode(licensekeyforIphone.getBytes()));
                json.put("_licenseforIphone", b64licensekeyforIphone);
                String licensekeyforWEB = mtm.makelicensekey(_channelId, WEB);
                if (licensekeyforWEB != null) {
                    String b64licensekeyforWEB = new String(Base64.encode(licensekeyforWEB.getBytes()));
                    json.put("_licenseWebTRUST", b64licensekeyforWEB);
                }
            }
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }

    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
