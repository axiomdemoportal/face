    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.radius;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author bluebricks
 */
@WebService(name = "CoreRadiusService")
public interface CoreRadiusServiceInterface {

    @WebMethod

    public RadiusClientResponse OpenSession(@WebParam(name = "channelid") String channelid,
            @WebParam(name = "loginpassword") String loginpassword,
            @WebParam(name = "loginid") String loginid);

    @WebMethod

    public RadiusClientResponse AddRadiusClient(@WebParam(name = "Sessionid") String sessionId,
            @WebParam(name = "Displayname") String displayname,
            @WebParam(name = "authenticationtype") String authenticationtype,
            @WebParam(name = "rClientIP") String rClientIP,
            @WebParam(name = "radiusClientSecretkey") String radiusClientSecretkey,
            @WebParam(name = "dayrestriction") String dayrestriction,
            @WebParam(name = "timerange") String timerange,
            @WebParam(name = "timefromampm") String timefromampm,
            @WebParam(name = "totimerange") String totimerange,
            @WebParam(name = "timetoampm") String timetoampm);

    @WebMethod

    public RadiusClientResponse EditRadiusClient(@WebParam(name = "Sessionid") String sessionId,
            @WebParam(name = "SettingID") String SettingID,
            @WebParam(name = "authenticationtype") String authenticationtype,
            @WebParam(name = "rClientIP") String rClientIP,
            @WebParam(name = "Status") String status,
            @WebParam(name = "radiusClientSecretkey") String radiusClientSecretkey,
            @WebParam(name = "dayrestriction") String dayrestriction,
            @WebParam(name = "timerange") String timerange,
            @WebParam(name = "timefromampm") String timefromampm,
            @WebParam(name = "totimerange") String totimerange,
            @WebParam(name = "timetoampm") String timetoampm);

    @WebMethod

    public RadiusClientResponse RemoveRadiusClient(@WebParam(name = "Sessionid") String sessionId,
            @WebParam(name = "SettingID") String SettingID);

    @WebMethod
    public RadiusClientResponse CloseSession(@WebParam(name = "Sessionid") String sessionid);

    @WebMethod
    public RadiusClientResp GetRadiusClient(@WebParam(name = "Sessionid") String sessionid,
            @WebParam(name = "SettingID") String SettingID);

}
