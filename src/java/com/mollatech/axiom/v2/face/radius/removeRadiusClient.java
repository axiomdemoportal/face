/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.radius;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.AxiomRadiusClient;
import com.mollatech.axiom.nucleus.settings.AxiomRadiusConfiguration;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class removeRadiusClient extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(removeRadiusClient.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final String itemtype = "SETTINGS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        String result = "success";
        String message = "Radius Setting Update Successful!!!";
        String resultString = "Failure";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        try {

            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId :: "+sessionId);
            //audit parameter
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("remoteaccesslogin :: "+remoteaccesslogin);
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operatorS :: "+operatorS.getName());
            String OperatorID = operatorS.getOperatorid();
            String _channelId = channel.getChannelid();
            SettingsManagement sMngmt = new SettingsManagement();
            int retValue = -1;

            int _srno = Integer.parseInt(request.getParameter("srno"));
            log.debug("_srno :: "+_srno);
            int iType = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.Radius;
            log.debug("iType :: "+iType);
            int iPreference = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE;
            log.debug("iPreference :: "+iPreference);
            //   String strType = String.valueOf(iType);
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), iType, iPreference);

            AxiomRadiusConfiguration RadiusServerObj = null;
            boolean bAddSetting = false;
            if (settingsObj == null) {
                RadiusServerObj = new AxiomRadiusConfiguration();

            } else {
                RadiusServerObj = (AxiomRadiusConfiguration) settingsObj;
                RadiusServerObj.setAccountEnabled(RadiusServerObj.isAccountEnabled());
                RadiusServerObj.setAccountIp(RadiusServerObj.getAccountIp());
                RadiusServerObj.setAccountPort(RadiusServerObj.getAccountPort());
                RadiusServerObj.setAuthEnabled(RadiusServerObj.isAuthEnabled());
                RadiusServerObj.setAuthIp(RadiusServerObj.getAuthIp());
                RadiusServerObj.setAuthPort(RadiusServerObj.getAuthPort());
                RadiusServerObj.setLdapServerUsername(RadiusServerObj.getLdapServerUsername());
                RadiusServerObj.setLdapServerPassword(RadiusServerObj.getLdapServerPassword());
                RadiusServerObj.setLdapSearchInitial(RadiusServerObj.getLdapSearchInitial());
                RadiusServerObj.setLdapSearchPath(RadiusServerObj.getLdapSearchPath());
                RadiusServerObj.setLdapServerIp(RadiusServerObj.getLdapServerIp());
                RadiusServerObj.setLdapServerPort(RadiusServerObj.getLdapServerPort());
                RadiusServerObj.setLdapValidate(RadiusServerObj.isLdapValidate());
                RadiusServerObj.setAxiomValidate(RadiusServerObj.isAxiomValidate());

                if (RadiusServerObj.getRadiusClient() == null) {

                } else {

                    // 
                    AxiomRadiusClient[] aradiusClient = new AxiomRadiusClient[RadiusServerObj.getRadiusClient().length];
                    List<AxiomRadiusClient> radList = new ArrayList<AxiomRadiusClient>();

                    aradiusClient = RadiusServerObj.getRadiusClient();

                    int j = 0;
                    for (int i = 0; i < RadiusServerObj.getRadiusClient().length; i++) {
                        if (i != _srno - 1) {

                            AxiomRadiusClient radiusClientObj = new AxiomRadiusClient();
                            radiusClientObj.setCreationDate(aradiusClient[i].getCreationDate());
                            radiusClientObj.setLastUpdateDate(aradiusClient[i].getLastUpdateDate());
                            radiusClientObj.setRadiusClientAuthtype(aradiusClient[i].getRadiusClientAuthtype());
                            radiusClientObj.setRadiusClientDisplayname(aradiusClient[i].getRadiusClientDisplayname());
                            radiusClientObj.setRadiusClientSecretkey(aradiusClient[i].getRadiusClientSecretkey());
                            radiusClientObj.setStatus(aradiusClient[i].getStatus());
                            radiusClientObj.setRadiusClientIp(aradiusClient[i].getRadiusClientIp());

                            radiusClientObj.setDayRestriction(aradiusClient[i].getDayRestriction());
                            radiusClientObj.setTimeFromInHour(aradiusClient[i].getTimeFromInHour());
                            radiusClientObj.setTimeToInHour(aradiusClient[i].getTimeFromInHour());

                            radiusClientObj.setTimetoampm(aradiusClient[i].getTimetoampm());
                            radiusClientObj.setTimfromampm(aradiusClient[i].getTimfromampm());

                            radList.add(radiusClientObj);

                        }
                    }

                    AxiomRadiusClient[] radiusClient = new AxiomRadiusClient[radList.size()];
                    for (int i = 0; i < radList.size(); i++) {
                        radiusClient[i] = radList.get(i);
                    }

                    RadiusServerObj.setRadiusClient(radiusClient);
                }

            }
            AuditManagement audit = new AuditManagement();
            if (bAddSetting == true) {

            } else {
                //AxiomRadiusConfiguration oldglobalObj = (AxiomRadiusConfiguration) sMngmt.getSetting(sessionId, _channelId, iType, iPreference);
                retValue = sMngmt.changeSetting(sessionId, _channelId, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.Radius,
                        com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE, 
                        settingsObj, RadiusServerObj);
                log.debug("retValue :: "+retValue);

                if (retValue == 0) {
                    result = "SUCCESS";
                    message = "Radius Client removed successfully.";
                    resultString = "SUCCESS";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), 
                            "Remove Radius Client", resultString, retValue,
                             "Radius Management", "", 
                             message,
                            itemtype, String.valueOf(_srno));

                } else if (retValue == -2) {
                    result = "error";
                    message = "Your Session has expired.";
                    resultString = "Failure";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), 
                            "Remove Radius Client", resultString, retValue,
                             "Radius Management", "", 
                             message,
                            itemtype, String.valueOf(_srno));
                } else {

                    result = "error";
                    message = "Radius Client could not be removed.";
                    resultString = "ERROR";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), 
                            "Remove Radius Client", resultString, retValue,
                            "Radius Management", 
                            "", 
                            message,
                            itemtype, String.valueOf(_srno)
                    );
                }
            }
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        }
        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
