/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.radius;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Sessions;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.RemoteAccessManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.AxiomRadiusClient;
import com.mollatech.axiom.nucleus.settings.AxiomRadiusConfiguration;
import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import org.hibernate.Session;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
@WebService(serviceName = "AxiomRadiusInterfaceImpl")
public class AxiomRadiusInterfaceImpl implements CoreRadiusServiceInterface {

    final String itemtype = "SETTINGS";
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(editRadiusClient.class.getName());
    @Resource
    WebServiceContext wsContext;

    public RadiusClientResponse OpenSession(@WebParam(name = "channelid") String channelid, @WebParam(name = "loginpassword") String loginpassword, @WebParam(name = "loginid") String loginid) {
        RadiusClientResponse response = new RadiusClientResponse();
        if (channelid != null && !channelid.isEmpty() && !channelid.endsWith("?") && loginpassword != null
                && !loginpassword.isEmpty() && loginid != null && !loginid.isEmpty()) {
            try {

                String strDebug = null;
                SettingsManagement setManagement = new SettingsManagement();
                try {

                    ChannelProfile channelprofileObj = null;
                    Object channelpobj = setManagement.getSettingInner(channelid, SettingsManagement.CHANNELPROFILE_SETTING, 1);

                    if (channelpobj == null) {
                        LoadSettings.LoadChannelProfile(channelprofileObj);
                    } else {
                        channelprofileObj = (ChannelProfile) channelpobj;
                        LoadSettings.LoadChannelProfile(channelprofileObj);
                    }
                    strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                    if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                        System.out.println("OpenSession::loginpassword::" + loginpassword);
                        System.out.println("OpenSession::channelid::" + channelid);
                        System.out.println("OpenSession::loginid::" + loginid);
                    }
                } catch (Exception ex) {
                }

                int iResult = AxiomProtect.ValidateLicense();
                if (iResult != 0) {
                    System.out.println("OpenSession::License Error::" + iResult);
                    return null;
                }

                ChannelManagement cManagement = new ChannelManagement();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {

                }

                MessageContext mc = wsContext.getMessageContext();
                HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
                //System.out.println("Client IP = " + req.getRemoteAddr());
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            SessionFactoryUtil suOperators = new SessionFactoryUtil(SessionFactoryUtil.operators);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                return null;
                            }

                            suTemplate.close();
                            sTemplate.close();
                            return null;
                        }
                        return null;
                    }
                }

//            int result = setManagement.checkIP(channelid, req.getRemoteAddr());
//            if (checkIp != 1) {
//                return null;
//            }
                String resultStr = "Failure";
                int retValue = -1;
                SessionManagement sManagement = new SessionManagement();
                String sessionId = sManagement.OpenSessionForWS(channelid, loginid, loginpassword, req.getSession().getId());
                if (sessionId == null) {
                    response.errorcode = -3;
                    response.error = "Failed";
                    response.rsesult = "Enter Valid Credentials";
                    return response;
                }
                AuditManagement audit = new AuditManagement();
                if (sessionId != null) {
                    retValue = 0;
                    resultStr = "Success";
                    audit.AddAuditTrail(sessionId, channelid, loginid,
                            req.getRemoteAddr(),
                            channel.getName(), loginid,
                            loginid, new Date(), "Open Session", resultStr, retValue,
                            "Login", "", "Open Session successfully with Session Id =" + sessionId, "SESSION",
                            loginid);

                } else if (sessionId == null) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), loginid, req.getRemoteAddr(), channel.getName(), loginid,
                            loginid, new Date(), "Open Session", resultStr, retValue,
                            "Login", "", "Failed To Open Session", "SESSION",
                            loginid);

                }
                response.errorcode = 0;
                response.error = "Success";
                response.rsesult = sessionId;
                return response;
            } catch (Exception e) {
                response.errorcode = -2;
                response.error = "Failed";
                response.rsesult = "Enter Valid ChannelID";
                return response;
            }
        }
        response.errorcode = -1;
        response.error = "Failed";
        response.rsesult = "Enter Valid Details";
        return response;
    }

    @Override
    public RadiusClientResponse AddRadiusClient(@WebParam(name = "Sessionid") String sessionId,
            @WebParam(name = "Displayname") String displayname,
            @WebParam(name = "authenticationtype") String authenticationtype,
            @WebParam(name = "rClientIP") String rClientIP,
            @WebParam(name = "radiusClientSecretkey") String radiusClientSecretkey,
            @WebParam(name = "dayrestriction") String dayrestriction,
            @WebParam(name = "timerange") String timerange,
            @WebParam(name = "timefromampm") String timefromampm,
            @WebParam(name = "totimerange") String totimerange,
            @WebParam(name = "timetoampm") String timetoampm) {
        RadiusClientResponse response = new RadiusClientResponse();

        if (sessionId != null && !sessionId.isEmpty() && !sessionId.endsWith("?")
                && displayname != null && !displayname.isEmpty() && !displayname.endsWith("?")
                && authenticationtype != null && !authenticationtype.isEmpty() && !authenticationtype.endsWith("?")
                && rClientIP != null && !rClientIP.isEmpty() && !rClientIP.endsWith("?")
                && radiusClientSecretkey != null && !radiusClientSecretkey.isEmpty() && !radiusClientSecretkey.endsWith("?")
                && dayrestriction != null && !dayrestriction.isEmpty() && !dayrestriction.endsWith("?")
                && timerange != null && !timerange.isEmpty() && !timerange.endsWith("?")
                && timefromampm != null && !timefromampm.isEmpty() && !timefromampm.endsWith("?")
                && totimerange != null && !totimerange.isEmpty() && !totimerange.endsWith("?")
                && timetoampm != null && !timetoampm.isEmpty() && !timetoampm.endsWith("?")) {
            String result = "success";
            String message = "Radius Setting Update Successful!!!";
            String resultString = "ERROR";
            JSONObject json = new JSONObject();

            try {

                SessionManagement sManagement = new SessionManagement();
                Sessions session = sManagement.getSessionById(sessionId);
                String channelid = session.getChannelid();
                ChannelManagement channelmagnt = new ChannelManagement();
                Channels channel = channelmagnt.getChannelByID(channelid);
                log.debug("channel :: " + channel.getName());
                //audit parameter

                RemoteAccessManagement rmeaccmngt = new RemoteAccessManagement();

                String remoteaccesslogin = session.getLoginid();
                log.debug("remoteaccesslogin :: " + remoteaccesslogin);
                OperatorsManagement oprmngt = new OperatorsManagement();

                String operatorS = session.getLoginid();

                String OperatorID = session.getLoginid();
                String _channelId = channel.getChannelid();
                SettingsManagement sMngmt = new SettingsManagement();
                String retValue = null;
                Validate dec = new Validate();
                boolean ipaddr = dec.isValidIP(rClientIP);
                if (ipaddr == false) {
                    response.errorcode = -5;
                    response.error = "Failed";
                    response.rsesult = "Enter Valid ClientIP";
                    return response;
                }
                String _displayname = displayname;
                log.debug("_displayname :: " + _displayname);
                if (authenticationtype.equals("otp") || authenticationtype.equals("password") || authenticationtype.equals("both")) {
                    String authenticationtyp = authenticationtype;
                } else {
                    response.errorcode = -55;
                    response.error = "Failed";
                    response.rsesult = "Enter Valid Authentication Type i.e. 'otp','password' Or 'password+otp'.";
                    return response;
                }
                log.debug("authenticationtype :: " + authenticationtype);
                String _radiusClientIp = rClientIP;
                log.debug("_radiusClientIp :: " + _radiusClientIp);
                String _radiusClientSecretkey = radiusClientSecretkey;
                log.debug("_radiusClientSecretkey :: " + _radiusClientSecretkey);
                String _dayRestriction;
                if (dayrestriction.equals("1") || dayrestriction.equals("2")) {
                    _dayRestriction = dayrestriction;
                } else {
                    response.errorcode = -56;
                    response.error = "Failed";
                    response.rsesult = "Enter Valid DayRestriction Type i.e. '1 (Weekdays Only)','2(Whole week (including weekend))'.";
                    return response;
                }

                log.debug("_dayRestriction :: " + _dayRestriction);
                String _timerange = timerange;
                log.debug("_timerange :: " + _timerange);
                String _timefromampm = timefromampm;
                log.debug("_timefromampm :: " + _timefromampm);
                String _totimerange = totimerange;
                log.debug("_totimerange :: " + _totimerange);
                String _timetoampm = timetoampm;
                log.debug("_timetoampm :: " + _timetoampm);
                int _dayRes = -1;
                int _timeFromIn = -1;
                int _timeTo = -1;
                int _timeFromampm = -1;
                int _timeToampm = -1;

                if (_dayRestriction != null) {
                    _dayRes = Integer.parseInt(_dayRestriction);
                }
                if (_timerange != null) {
                    _timeFromIn = Integer.parseInt(_timerange);
                }

                if (_timefromampm != null) {
                    _timeFromampm = Integer.parseInt(_timefromampm);
                }

                if (_totimerange != null) {
                    _timeTo = Integer.parseInt(_totimerange);
                }

                if (_timetoampm != null) {
                    _timeToampm = Integer.parseInt(_timetoampm);
                }

                int iType = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.Radius;
                log.debug("iType :: " + iType);
                int iPreference = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE;
                log.debug("iPreference :: " + iPreference);
                //   String strType = String.valueOf(iType);
                Object settingsObj = sMngmt.getdisplay(displayname, sessionId, _channelId, iType, iPreference);
                if (settingsObj != null) {
                    response.errorcode = -25;
                    response.error = "Failed";
                    response.rsesult = "Enter Unique Display Name";
                    return response;
                }
                AxiomRadiusConfiguration RadiusServerObj = null;
                boolean bAddSetting = false;
                AxiomRadiusClient nradiusClient = new AxiomRadiusClient();

                nradiusClient.setCreationDate(new Date().getTime());
                nradiusClient.setLastUpdateDate(new Date().getTime());
                nradiusClient.setStatus(1);
                nradiusClient.setRadiusClientSecretkey(_radiusClientSecretkey);
                nradiusClient.setRadiusClientDisplayname(_displayname);
                nradiusClient.setRadiusClientIp(_radiusClientIp);
                nradiusClient.setRadiusClientAuthtype(authenticationtype);

                nradiusClient.setDayRestriction(_dayRes);
                nradiusClient.setTimeFromInHour(_timeFromIn);
                nradiusClient.setTimeToInHour(_timeTo);
                nradiusClient.setTimetoampm(_timeToampm);
                nradiusClient.setTimfromampm(_timeFromampm);

                if (settingsObj == null) {
                    RadiusServerObj = new AxiomRadiusConfiguration();
                } else {
                    RadiusServerObj = (AxiomRadiusConfiguration) settingsObj;
                    RadiusServerObj.setAccountEnabled(RadiusServerObj.isAccountEnabled());
                    RadiusServerObj.setAccountIp(RadiusServerObj.getAccountIp());
                    RadiusServerObj.setAccountPort(RadiusServerObj.getAccountPort());
                    RadiusServerObj.setAuthEnabled(RadiusServerObj.isAuthEnabled());
                    RadiusServerObj.setAuthIp(RadiusServerObj.getAuthIp());
                    RadiusServerObj.setAuthPort(RadiusServerObj.getAuthPort());
                    RadiusServerObj.setLdapServerUsername(RadiusServerObj.getLdapServerUsername());
                    RadiusServerObj.setLdapServerPassword(RadiusServerObj.getLdapServerPassword());
                    RadiusServerObj.setLdapSearchPath(RadiusServerObj.getLdapSearchPath());
                    RadiusServerObj.setLdapSearchInitial(RadiusServerObj.getLdapSearchInitial());
                    RadiusServerObj.setLdapServerIp(RadiusServerObj.getLdapServerIp());
                    RadiusServerObj.setLdapServerPort(RadiusServerObj.getLdapServerPort());
                    RadiusServerObj.setLdapValidate(RadiusServerObj.isLdapValidate());
                    RadiusServerObj.setAxiomValidate(RadiusServerObj.isAxiomValidate());

                    if (RadiusServerObj.getRadiusClient() == null) {
                        AxiomRadiusClient[] radiusClient = new AxiomRadiusClient[1];
                        radiusClient[0] = new AxiomRadiusClient();
                        radiusClient[0] = nradiusClient;
                        RadiusServerObj.setRadiusClient(radiusClient);

                    } else {
                        int count = 0;
                        AxiomRadiusClient[] radiusClient = new AxiomRadiusClient[RadiusServerObj.getRadiusClient().length + 1];
                        AxiomRadiusClient[] aradiusClient = new AxiomRadiusClient[RadiusServerObj.getRadiusClient().length + 1];
                        aradiusClient = RadiusServerObj.getRadiusClient();
                        int i = 0;
                        for (i = 0; i < RadiusServerObj.getRadiusClient().length; i++) {
                            radiusClient[i] = new AxiomRadiusClient();
                            radiusClient[i].setCreationDate(aradiusClient[i].getCreationDate());
                            radiusClient[i].setLastUpdateDate(aradiusClient[i].getLastUpdateDate());
                            radiusClient[i].setRadiusClientAuthtype(aradiusClient[i].getRadiusClientAuthtype());
                            radiusClient[i].setRadiusClientDisplayname(aradiusClient[i].getRadiusClientDisplayname());
                            radiusClient[i].setRadiusClientSecretkey(aradiusClient[i].getRadiusClientSecretkey());
                            radiusClient[i].setRadiusClientIp(aradiusClient[i].getRadiusClientIp());
                            radiusClient[i].setStatus(aradiusClient[i].getStatus());

                            radiusClient[i].setDayRestriction(aradiusClient[i].getDayRestriction());
                            radiusClient[i].setTimeFromInHour(aradiusClient[i].getTimeFromInHour());
                            radiusClient[i].setTimfromampm(aradiusClient[i].getTimfromampm());
                            radiusClient[i].setTimeToInHour(aradiusClient[i].getTimeFromInHour());
                            radiusClient[i].setTimetoampm(aradiusClient[i].getTimetoampm());

                        }
                        radiusClient[i] = new AxiomRadiusClient();
                        radiusClient[i] = nradiusClient;

                        RadiusServerObj.setRadiusClient(radiusClient);

                    }

                }
                AuditManagement audit = new AuditManagement();

                retValue = sMngmt.addSettings(sessionId, _channelId, iType, iPreference, nradiusClient);
                if (retValue == null) {
                    response.errorcode = -6;
                    response.error = "Failed";
                    response.rsesult = "Enter Valid SessionID";
                    return response;
                }
                resultString = "Failure";
                if (retValue != null) {

                    resultString = "SUCCESS";
//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
//                            request.getRemoteAddr(),
//                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
//                            "ChangeRadiusClient", resultString, retValue, "Setting Management",
//                            "", "_displayname=" + nradiusClient.getRadiusClientDisplayname() + ",authenticationtype=" + nradiusClient.getRadiusClientAuthtype() + ",_rClientIP=" + nradiusClient.getRadiusClientIp()
//                            + ",_radiusClientSecretkey=" + nradiusClient.getRadiusClientSecretkey(),
//                            itemtype, OperatorID);

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), OperatorID, remoteaccesslogin, channel.getName(),
                            remoteaccesslogin, OperatorID, new Date(),
                            "Add Radius Client", resultString, 0,
                            "Radius Management", "",
                            message
                            + "_displayname=" + _displayname + ",authenticationtype=" + authenticationtype
                            + ",_rClientIP=" + _radiusClientIp,
                            itemtype,
                            "-");

                } else {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), OperatorID, remoteaccesslogin, channel.getName(),
                            remoteaccesslogin, OperatorID, new Date(),
                            "Add Radius Client", resultString, 0,
                            "Radius Management", "",
                            message
                            + "_displayname=" + _displayname + ",authenticationtype=" + authenticationtype
                            + ",_rClientIP=" + _radiusClientIp,
                            itemtype,
                            "-");
                }

                response.settingID = retValue;
                response.errorcode = 0;
                response.error = "Success";
                response.rsesult = "Radius Setting Add Successful!!!";
                return response;
            } catch (Exception ex) {
                response.errorcode = -6;
                response.error = "Failed";
                response.rsesult = "Enter Valid SessionID";
                return response;
            }
        }
        response.errorcode = -1;
        response.error = "Failed";
        response.rsesult = "Enter Valid Details";
        return response;
    }

    @Override
    public RadiusClientResponse EditRadiusClient(@WebParam(name = "Sessionid") String sessionId,
            @WebParam(name = "SettingID") String SettingID,
            @WebParam(name = "authenticationtype") String authenticationtype,
            @WebParam(name = "rClientIP") String rClientIP,
            @WebParam(name = "Status") String status,
            @WebParam(name = "radiusClientSecretkey") String radiusClientSecretkey,
            @WebParam(name = "dayrestriction") String dayrestriction,
            @WebParam(name = "timerange") String timerange,
            @WebParam(name = "timefromampm") String timefromampm,
            @WebParam(name = "totimerange") String totimerange,
            @WebParam(name = "timetoampm") String timetoampm) {
        RadiusClientResponse response = new RadiusClientResponse();
        if (sessionId != null && !sessionId.isEmpty() && !sessionId.endsWith("?")
                && SettingID != null && !SettingID.isEmpty() && !SettingID.endsWith("?")
                && status != null && !status.isEmpty() && !status.endsWith("?")
                && authenticationtype != null && !authenticationtype.isEmpty() && !authenticationtype.endsWith("?")
                && rClientIP != null && !rClientIP.isEmpty() && !rClientIP.endsWith("?")
                && radiusClientSecretkey != null && !radiusClientSecretkey.isEmpty() && !radiusClientSecretkey.endsWith("?")
                && dayrestriction != null && !dayrestriction.isEmpty() && !dayrestriction.endsWith("?")
                && timerange != null && !timerange.isEmpty() && !timerange.endsWith("?")
                && timefromampm != null && !timefromampm.isEmpty() && !timefromampm.endsWith("?")
                && totimerange != null && !totimerange.isEmpty() && !totimerange.endsWith("?")
                && timetoampm != null && !timetoampm.isEmpty() && !timetoampm.endsWith("?")) {
            String result = "success";
            String message = "Radius Setting Update Successful!!!";
            String resultString = "ERROR";
            JSONObject json = new JSONObject();

            try {
                Validate dec = new Validate();
                boolean ipaddr = dec.isValidIP(rClientIP);
                if (ipaddr == false) {
                    response.errorcode = -5;
                    response.error = "Failed";
                    response.rsesult = "Enter Valid ClientIP";
                    return response;
                }
                SessionManagement sManagement = new SessionManagement();
                Sessions session = sManagement.getSessionById(sessionId);
                String channelid = session.getChannelid();

                ChannelManagement channelmagnt = new ChannelManagement();
                Channels channel = channelmagnt.getChannelByID(channelid);

                log.debug("channel :: " + channel.getName());
                //audit parameter
                RemoteAccessManagement rmeaccmngt = new RemoteAccessManagement();
                String remoteaccesslogin = session.getLoginid();
                log.debug("remoteaccesslogin :: " + remoteaccesslogin);
                OperatorsManagement oprmngt = new OperatorsManagement();
                String operatorS = session.getLoginid();
                String OperatorID = session.getLoginid();
                String _channelId = channel.getChannelid();
                SettingsManagement sMngmt = new SettingsManagement();
                int retValue = -1;

                if (status == null || status.isEmpty()) {
                    try {
                        response.error = "error";
                        response.rsesult = "Status can not be null!!!";
                        return response;
                    } catch (Exception ex) {
                        log.error("Exception caught :: ", ex);
                    }
                }

                if (rClientIP == null || rClientIP.isEmpty()) {
                    try {
                        response.error = "error";
                        response.rsesult = "Enter Client Ip!!!";
                        return response;
                    } catch (Exception ex) {
                        log.error("Exception caught :: ", ex);
                    }
                }

                if (radiusClientSecretkey == null || radiusClientSecretkey.isEmpty()) {
                    try {
                        response.error = "error";
                        response.rsesult = "Enter Client Secrert Key!!!";
                        return response;
                    } catch (Exception ex) {
                        log.error("Exception caught :: ", ex);
                    }
                }

                if (timerange == null || timerange.isEmpty()) {
                    try {
                        response.error = "error";
                        response.rsesult = "Enter Restriction time from !!!";
                        return response;
                    } catch (Exception ex) {
                        log.error("Exception caught :: ", ex);
                    }
                }
                if (totimerange == null || totimerange.isEmpty()) {
                    try {
                        response.error = "error";
                        response.rsesult = "Enter Restriction time to !!!";
                        return response;
                    } catch (Exception ex) {
                        log.error("Exception caught :: ", ex);
                    }
                }

                int _srno = Integer.parseInt(SettingID);
                if (authenticationtype.equals("otp") || authenticationtype.equals("password") || authenticationtype.equals("both")) {
                    String authenticationtyp = authenticationtype;
                } else {
                    response.errorcode = -55;
                    response.error = "Failed";
                    response.rsesult = "Enter Valid Authentication Type i.e. 'otp','password' Or 'password+otp'.";
                    return response;
                }
                log.debug("authenticationtype :: " + authenticationtype);
                String _status = status;
                log.debug("_status :: " + _status);
                int stat;
                try {
                    stat = Integer.parseInt(_status);
                } catch (Exception e) {
                    response.error = "error";
                    response.rsesult = "Enter Numeric Status (i.e. 1 OR 0)";
                    return response;
                }
                String _radiusClientIp = rClientIP;
                log.debug("_radiusClientIp :: " + _radiusClientIp);
                String _radiusClientSecretkey = radiusClientSecretkey;
                log.debug("_radiusClientSecretkey :: " + _radiusClientSecretkey);
                String _dayRestriction;
                if (dayrestriction.equals("1") || dayrestriction.equals("2")) {
                    _dayRestriction = dayrestriction;
                } else {
                    response.errorcode = -56;
                    response.error = "Failed";
                    response.rsesult = "Enter Valid DayRestriction Type i.e. '1 (Weekdays Only)','2(Whole week (including weekend))'.";
                    return response;
                }
                log.debug("_dayRestriction :: " + _dayRestriction);
                String _timerange = timerange;
                log.debug("_timerange :: " + _timerange);
                String _timefromampm = timetoampm;
                log.debug("_timefromampm :: " + _timefromampm);
                String _totimerange = totimerange;
                log.debug("_totimerange :: " + _totimerange);
                String _timetoampm = timetoampm;
                log.debug("_timetoampm :: " + _timetoampm);

                int _dayRes = -1;
                int _timeFromIn = -1;
                int _timeTo = -1;
                int _timeFromampm = -1;
                int _timeToampm = -1;

                if (_dayRestriction != null) {
                    _dayRes = Integer.parseInt(_dayRestriction);
                }
                if (_timerange != null) {
                    _timeFromIn = Integer.parseInt(_timerange);
                }

                if (_timefromampm != null) {
                    _timeFromampm = Integer.parseInt(_timefromampm);
                }

                if (_totimerange != null) {
                    _timeTo = Integer.parseInt(_totimerange);
                }

                if (_timetoampm != null) {
                    _timeToampm = Integer.parseInt(_timetoampm);
                }

                int iType = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.Radius;
                log.debug("iType :: " + iType);
                int iPreference = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE;
                log.debug("iPreference :: " + iPreference);
                //   String strType = String.valueOf(iType);

                Object settingsObj = sMngmt.getSettingV2(SettingID, sessionId, _channelId, iType, iPreference);
                if (settingsObj == null) {
                    response.errorcode = -16;
                    response.error = "Failed";
                    response.rsesult = "Enter valid Display Name";
                    return response;
                }
                AxiomRadiusClient client = (AxiomRadiusClient) settingsObj;
                // System.out.println("hiiiiiiiiiiiiiiiii >> " + client.getRadiusClientDisplayname());
                AxiomRadiusConfiguration RadiusServerObj = null;
                boolean bAddSetting = false;
                AxiomRadiusClient nradiusClient = new AxiomRadiusClient();

                nradiusClient.setCreationDate(new Date().getTime());
                nradiusClient.setLastUpdateDate(new Date().getTime());
                if (stat == 0 || stat == 1) {
                    nradiusClient.setStatus(stat);
                } else {
                    response.error = "error";
                    response.rsesult = "Enter Valid Status (i.e. 1 OR 0)";
                    return response;
                }

                nradiusClient.setRadiusClientSecretkey(_radiusClientSecretkey);
                nradiusClient.setRadiusClientDisplayname(client.getRadiusClientDisplayname());
                nradiusClient.setRadiusClientIp(_radiusClientIp);
                nradiusClient.setRadiusClientAuthtype(authenticationtype);
                nradiusClient.setDayRestriction(_dayRes);
                nradiusClient.setTimeFromInHour(_timeFromIn);
                nradiusClient.setTimeToInHour(_timeTo);
                nradiusClient.setTimetoampm(_timeToampm);
                nradiusClient.setTimfromampm(_timeFromampm);

//                if (settingsObj == null) {
//                    RadiusServerObj = new AxiomRadiusConfiguration();
//
//                } else {
//                    RadiusServerObj = (AxiomRadiusConfiguration) settingsObj;
//                    RadiusServerObj.setAccountEnabled(RadiusServerObj.isAccountEnabled());
//                    RadiusServerObj.setAccountIp(RadiusServerObj.getAccountIp());
//                    RadiusServerObj.setAccountPort(RadiusServerObj.getAccountPort());
//                    RadiusServerObj.setAuthEnabled(RadiusServerObj.isAuthEnabled());
//                    RadiusServerObj.setAuthIp(RadiusServerObj.getAuthIp());
//                    RadiusServerObj.setAuthPort(RadiusServerObj.getAuthPort());
//                    RadiusServerObj.setLdapServerUsername(RadiusServerObj.getLdapServerUsername());
//                    RadiusServerObj.setLdapServerPassword(RadiusServerObj.getLdapServerPassword());
//                    RadiusServerObj.setLdapSearchInitial(RadiusServerObj.getLdapSearchInitial());
//                    RadiusServerObj.setLdapSearchPath(RadiusServerObj.getLdapSearchPath());
//                    RadiusServerObj.setLdapServerIp(RadiusServerObj.getLdapServerIp());
//                    RadiusServerObj.setLdapServerPort(RadiusServerObj.getLdapServerPort());
//                    RadiusServerObj.setLdapValidate(RadiusServerObj.isLdapValidate());
//                    RadiusServerObj.setAxiomValidate(RadiusServerObj.isAxiomValidate());
//
//                    if (RadiusServerObj.getRadiusClient() == null) {
//                        AxiomRadiusClient[] radiusClient = new AxiomRadiusClient[1];
//                        radiusClient[0] = new AxiomRadiusClient();
//                        radiusClient[0] = nradiusClient;
//                        RadiusServerObj.setRadiusClient(radiusClient);
//
//                    } else {
//
//                        AxiomRadiusClient[] radiusClient = new AxiomRadiusClient[RadiusServerObj.getRadiusClient().length];
//                        AxiomRadiusClient[] aradiusClient = new AxiomRadiusClient[RadiusServerObj.getRadiusClient().length];
//                        aradiusClient = RadiusServerObj.getRadiusClient();
//                        int i = 0;
//                        for (i = 0; i < RadiusServerObj.getRadiusClient().length; i++) {
//                            if (i == _srno - 1) {
//                                radiusClient[i] = new AxiomRadiusClient();
//                                radiusClient[i] = nradiusClient;
//
//                            } else {
//                                radiusClient[i] = new AxiomRadiusClient();
//                                radiusClient[i].setCreationDate(aradiusClient[i].getCreationDate());
//                                radiusClient[i].setLastUpdateDate(aradiusClient[i].getLastUpdateDate());
//                                radiusClient[i].setRadiusClientAuthtype(aradiusClient[i].getRadiusClientAuthtype());
//                                radiusClient[i].setRadiusClientDisplayname(aradiusClient[i].getRadiusClientDisplayname());
//                                radiusClient[i].setRadiusClientSecretkey(aradiusClient[i].getRadiusClientSecretkey());
//                                radiusClient[i].setStatus(aradiusClient[i].getStatus());
//                                radiusClient[i].setRadiusClientIp(aradiusClient[i].getRadiusClientIp());
//                                radiusClient[i].setDayRestriction(aradiusClient[i].getDayRestriction());
//                                radiusClient[i].setTimeFromInHour(aradiusClient[i].getTimeFromInHour());
//                                radiusClient[i].setTimeToInHour(aradiusClient[i].getTimeFromInHour());
//                                radiusClient[i].setTimetoampm(aradiusClient[i].getTimetoampm());
//                                radiusClient[i].setTimfromampm(aradiusClient[i].getTimfromampm());
//
//                            }
//                        }
//
//                        RadiusServerObj.setRadiusClient(radiusClient);
//                    }
//
//                }
                AuditManagement audit = new AuditManagement();
                if (bAddSetting == true) {

                } else {

                    //RadiusServerObj
                    //oldglobalObj
                    AxiomRadiusConfiguration oldglobalObj = (AxiomRadiusConfiguration) sMngmt.getSetting(sessionId, _channelId, iType, iPreference);
                    retValue = sMngmt.changeSettin(SettingID, sessionId, _channelId, iType, iPreference, oldglobalObj, nradiusClient);

                    if (retValue == 0) {
                        result = "success";
                        message = "Radius Client Successfully updated";
                        resultString = "SUCCESS";

                        audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS, OperatorID, channel.getName(),
                                remoteaccesslogin, operatorS, new Date(),
                                "Change Radius Client", resultString, retValue,
                                "Radius Management", "",
                                message
                                + "_displayname=" + nradiusClient.getRadiusClientDisplayname() + ",authenticationtype=" + nradiusClient.getRadiusClientAuthtype()
                                + ",_rClientIP=" + nradiusClient.getRadiusClientIp()
                                + ",_radiusClientSecretkey=#####" //+ nradiusClient.getRadiusClientSecretkey()                              
                                ,
                                itemtype, String.valueOf(_srno));

//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
//                            request.getRemoteAddr(),
//                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
//                            "ChangeRadiusClient", resultString, retValue, "Setting Management",
//                            "", "_displayname=" + nradiusClient.getRadiusClientDisplayname() + ",authenticationtype=" + nradiusClient.getRadiusClientAuthtype() + ",_rClientIP=" + nradiusClient.getRadiusClientIp()
//                            + ",_radiusClientSecretkey=" + nradiusClient.getRadiusClientSecretkey()+ "_displayname=" + nradiusClient.getRadiusClientDisplayname() + ",authenticationtype=" + nradiusClient.getRadiusClientAuthtype() + ",_rClientIP=" + nradiusClient.getRadiusClientIp()
//                            + ",_radiusClientSecretkey=" + nradiusClient.getRadiusClientSecretkey() ,
//                            itemtype, OperatorID);
                    } else if (retValue == -2) {
                        result = "error";
                        message = "Your Session is Expired ...!!!";
//                     resultString = "Success";
//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
//                            request.getRemoteAddr(),
//                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
//                            "ChangeRadiusClient", resultString, retValue, "Setting Management",
//                            "", "_displayname=" + nradiusClient.getRadiusClientDisplayname() + ",authenticationtype=" + nradiusClient.getRadiusClientAuthtype() + ",_rClientIP=" + nradiusClient.getRadiusClientIp()
//                            + ",_radiusClientSecretkey=" + nradiusClient.getRadiusClientSecretkey() ,
//                            itemtype, OperatorID);
                    } else {

                        result = "error";
                        message = "Radius Client updatation Failure...!!!";
                        resultString = "ERROR";

                        audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS, OperatorID, channel.getName(),
                                remoteaccesslogin, operatorS, new Date(),
                                "Change Radius Client", resultString, retValue,
                                "Radius Management", "",
                                message
                                + "_displayname=" + nradiusClient.getRadiusClientDisplayname() + ",authenticationtype=" + nradiusClient.getRadiusClientAuthtype()
                                + ",_rClientIP=" + nradiusClient.getRadiusClientIp()
                                + ",_radiusClientSecretkey=#####" //+ nradiusClient.getRadiusClientSecretkey()                              
                                ,
                                itemtype, String.valueOf(_srno));

//                    resultString = "Success";
//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
//                            request.getRemoteAddr(),
//                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
//                            "ChangeRadiusClient", resultString, retValue, "Setting Management",
//                            "", "_displayname=" + nradiusClient.getRadiusClientDisplayname() + ",authenticationtype=" + nradiusClient.getRadiusClientAuthtype() + ",_rClientIP=" + nradiusClient.getRadiusClientIp()
//                            + ",_radiusClientSecretkey=" + nradiusClient.getRadiusClientSecretkey() ,
//                            itemtype, OperatorID);
                    }
                }
                response.errorcode = 0;
                response.error = "Success";
                response.rsesult = "Radius Setting Update Successful!!!";
                return response;
            } catch (Exception ex) {
                response.errorcode = -6;
                response.error = "Failed";
                response.rsesult = "Enter Valid SessionID";
                return response;
            }

        }
        response.errorcode = -1;
        response.error = "Failed";
        response.rsesult = "Enter Valid Details";
        return response;
    }

    @Override
    public RadiusClientResponse RemoveRadiusClient(@WebParam(name = "sessionId") String sessionId,
            @WebParam(name = "SettingID") String SettingID
    ) {
        RadiusClientResponse response = new RadiusClientResponse();
        if (SettingID != null & !SettingID.isEmpty() && sessionId != null && !sessionId.isEmpty()) {
            String result = "success";
            String message = "Radius Setting Update Successful!!!";
            String resultString = "Failure";
            JSONObject json = new JSONObject();

            try {

                SessionManagement sManagement = new SessionManagement();
                Sessions session = sManagement.getSessionById(sessionId);
                String channelid = session.getChannelid();

                ChannelManagement channelmagnt = new ChannelManagement();
                Channels channel = channelmagnt.getChannelByID(channelid);

                log.debug("channel :: " + channel.getName());
                //audit parameter
                RemoteAccessManagement rmeaccmngt = new RemoteAccessManagement();
                String remoteaccesslogin = session.getLoginid();
                log.debug("remoteaccesslogin :: " + remoteaccesslogin);
                OperatorsManagement oprmngt = new OperatorsManagement();
                String operatorS = session.getLoginid();
                String OperatorID = session.getLoginid();
                String _channelId = channel.getChannelid();
                SettingsManagement sMngmt = new SettingsManagement();
                int retValue = -1;

                int _srno = Integer.parseInt(SettingID);
                log.debug("_srno :: " + _srno);
                int iType = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.Radius;
                log.debug("iType :: " + iType);
                int iPreference = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE;
                log.debug("iPreference :: " + iPreference);
                //   String strType = String.valueOf(iType);
                Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), iType, iPreference);

                AxiomRadiusConfiguration RadiusServerObj = null;
                boolean bAddSetting = false;
                if (settingsObj == null) {
                    RadiusServerObj = new AxiomRadiusConfiguration();

                } else {
                    RadiusServerObj = (AxiomRadiusConfiguration) settingsObj;
                    RadiusServerObj.setAccountEnabled(RadiusServerObj.isAccountEnabled());
                    RadiusServerObj.setAccountIp(RadiusServerObj.getAccountIp());
                    RadiusServerObj.setAccountPort(RadiusServerObj.getAccountPort());
                    RadiusServerObj.setAuthEnabled(RadiusServerObj.isAuthEnabled());
                    RadiusServerObj.setAuthIp(RadiusServerObj.getAuthIp());
                    RadiusServerObj.setAuthPort(RadiusServerObj.getAuthPort());
                    RadiusServerObj.setLdapServerUsername(RadiusServerObj.getLdapServerUsername());
                    RadiusServerObj.setLdapServerPassword(RadiusServerObj.getLdapServerPassword());
                    RadiusServerObj.setLdapSearchInitial(RadiusServerObj.getLdapSearchInitial());
                    RadiusServerObj.setLdapSearchPath(RadiusServerObj.getLdapSearchPath());
                    RadiusServerObj.setLdapServerIp(RadiusServerObj.getLdapServerIp());
                    RadiusServerObj.setLdapServerPort(RadiusServerObj.getLdapServerPort());
                    RadiusServerObj.setLdapValidate(RadiusServerObj.isLdapValidate());
                    RadiusServerObj.setAxiomValidate(RadiusServerObj.isAxiomValidate());

                    if (RadiusServerObj.getRadiusClient() == null) {

                    } else {

                        // 
                        AxiomRadiusClient[] aradiusClient = new AxiomRadiusClient[RadiusServerObj.getRadiusClient().length];
                        List<AxiomRadiusClient> radList = new ArrayList<AxiomRadiusClient>();

                        aradiusClient = RadiusServerObj.getRadiusClient();

                        int j = 0;
                        for (int i = 0; i < RadiusServerObj.getRadiusClient().length; i++) {
                            if (i != _srno - 1) {

                                AxiomRadiusClient radiusClientObj = new AxiomRadiusClient();
                                radiusClientObj.setCreationDate(aradiusClient[i].getCreationDate());
                                radiusClientObj.setLastUpdateDate(aradiusClient[i].getLastUpdateDate());
                                radiusClientObj.setRadiusClientAuthtype(aradiusClient[i].getRadiusClientAuthtype());
                                radiusClientObj.setRadiusClientDisplayname(aradiusClient[i].getRadiusClientDisplayname());
                                radiusClientObj.setRadiusClientSecretkey(aradiusClient[i].getRadiusClientSecretkey());
                                radiusClientObj.setStatus(aradiusClient[i].getStatus());
                                radiusClientObj.setRadiusClientIp(aradiusClient[i].getRadiusClientIp());

                                radiusClientObj.setDayRestriction(aradiusClient[i].getDayRestriction());
                                radiusClientObj.setTimeFromInHour(aradiusClient[i].getTimeFromInHour());
                                radiusClientObj.setTimeToInHour(aradiusClient[i].getTimeFromInHour());

                                radiusClientObj.setTimetoampm(aradiusClient[i].getTimetoampm());
                                radiusClientObj.setTimfromampm(aradiusClient[i].getTimfromampm());

                                radList.add(radiusClientObj);

                            }
                        }

                        AxiomRadiusClient[] radiusClient = new AxiomRadiusClient[radList.size()];
                        for (int i = 0; i < radList.size(); i++) {
                            radiusClient[i] = radList.get(i);
                        }

                        RadiusServerObj.setRadiusClient(radiusClient);
                    }

                }
                AuditManagement audit = new AuditManagement();
                if (bAddSetting == true) {

                } else {

                    //AxiomRadiusConfiguration oldglobalObj = (AxiomRadiusConfiguration) sMngmt.getSetting(sessionId, _channelId, iType, iPreference);
                    retValue = sMngmt.removeSettings(SettingID, sessionId, _channelId, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.Radius, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE, settingsObj);

                    log.debug("retValue :: " + retValue);

                    if (retValue == 0) {
                        result = "SUCCESS";
                        message = "Radius Client removed successfully.";
                        resultString = "SUCCESS";
                        audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS, OperatorID, channel.getName(),
                                remoteaccesslogin, operatorS, new Date(),
                                "Remove Radius Client", resultString, retValue,
                                "Radius Management", "",
                                message,
                                itemtype, String.valueOf(_srno));

                    } else if (retValue == -2) {
                        response.errorcode = -2;
                        response.error = "Failed";
                        response.rsesult = "Records not Found";
                        return response;
                    } else {

                        result = "error";
                        message = "Radius Client could not be removed.";
                        resultString = "ERROR";
                        audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS, OperatorID, channel.getName(),
                                remoteaccesslogin, operatorS, new Date(),
                                "Remove Radius Client", resultString, retValue,
                                "Radius Management",
                                "",
                                message,
                                itemtype, String.valueOf(_srno)
                        );
                    }
                }
            } catch (Exception ex) {
                response.errorcode = -1;
                response.error = "Failed";
                response.rsesult = "Enter Valid Inputs";
                return response;
            }
            response.errorcode = 0;
            response.error = "Success";
            response.rsesult = "Radius Client removed successfully.";
            return response;
        }
        response.errorcode = -1;
        response.error = "Failed";
        response.rsesult = "Enter Valid Inputs";
        return response;
    }

    @Override
    public RadiusClientResponse CloseSession(@WebParam(name = "Sessionid") String sessionid
    ) {
        RadiusClientResponse response = new RadiusClientResponse();
        if (sessionid != null && !sessionid.isEmpty()) {
            try {

                String strDebug = null;
                try {
                    strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                    if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                        System.out.println("CloseSession::sessionid::" + sessionid);
                    }
                } catch (Exception ex) {
                }

                int iResult = AxiomProtect.ValidateLicense();
                if (iResult != 0) {
                    RadiusClientResponse aStatus = new RadiusClientResponse();
                    aStatus.error = "Licence is invalid";
                    aStatus.errorcode = -100;
                    return aStatus;
                }

                SessionManagement sManagement = new SessionManagement();
                Sessions session = sManagement.getSessionById(sessionid);
                MessageContext mc = wsContext.getMessageContext();
                HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
                RadiusClientResponse aStatus = new RadiusClientResponse();
                aStatus.error = "ERROR";
                aStatus.errorcode = -2;

                if (session != null) {

                    //System.out.println("Client IP = " + req.getRemoteAddr());        
                    SettingsManagement setManagement = new SettingsManagement();
//                int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
//                if (result != 1) {
//                    aStatus.error = "INVALID IP REQUEST";
//                    aStatus.errorcode = -8;
//                    //  return aStatus;
//                }
                    String channelid = session.getChannelid();
                    ChannelManagement cManagement = new ChannelManagement();
                    Channels channel = cManagement.getChannelByID(channelid);
                    if (channel == null) {
                        return null;
                    }
                    Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                    if (ipobj != null) {
                        GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                        int checkIp = 1;
                        if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                            checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                        } else {
                            checkIp = 1;
                        }
                        if (iObj.ipstatus == 0 && checkIp != 1) {
                            if (iObj.ipalertstatus == 0) {
                                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                                Session sTemplate = suTemplate.openSession();
                                TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                                Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                                OperatorsManagement oManagement = new OperatorsManagement();
                                Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                                if (aOperator != null) {
                                    String[] emailList = new String[aOperator.length - 1];
                                    for (int i = 1; i < aOperator.length; i++) {
                                        emailList[i - 1] = aOperator[i].getEmailid();
                                    }
                                    if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                        ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                        String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                        String strsubject = templatesObj.getSubject();
                                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                        if (strmessageBody != null) {
                                            // Date date = new Date();
                                            strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                            strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                            strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                            strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                            strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                        }

                                        SendNotification send = new SendNotification();
                                        AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                                emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                    }

                                    suTemplate.close();
                                    sTemplate.close();
                                    aStatus.error = "INVALID IP REQUEST";
                                    aStatus.errorcode = -8;
                                    return aStatus;
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                    }
                    int retValue = sManagement.CloseSession(sessionid);

                    aStatus.error = "ERROR";
                    aStatus.errorcode = retValue;
                    if (retValue == 0) {
                        aStatus.error = "SUCCESS";
                        aStatus.errorcode = 0;
                    }

                } else if (session == null) {
                    return aStatus;
                }

                ChannelManagement cManagement = new ChannelManagement();
                Channels channel = cManagement.getChannelByID(session.getChannelid());
                if (channel == null) {
                    return aStatus;
                }
                AuditManagement audit = new AuditManagement();
                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                        session.getLoginid(), new Date(), "Close Session", aStatus.error, aStatus.errorcode,
                        "Login", "", "Close Session successfully with Session Id =" + sessionid, "SESSION",
                        session.getLoginid());
                return aStatus;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        response.errorcode = -1;
        response.error = "Failed";
        response.rsesult = "Enter Valid Details";
        return response;
    }

    @Override
    public RadiusClientResp GetRadiusClient(@WebParam(name = "Sessionid") String sessionid,
            @WebParam(name = "SettingID") String SettingID) {
        RadiusClientResp response = new RadiusClientResp();
        if (sessionid != null && !sessionid.isEmpty()
                && SettingID != null && !SettingID.isEmpty()) {

            SessionManagement sManagement = new SessionManagement();
            Sessions session = sManagement.getSessionById(sessionid);
            if (session == null) {
                response.error = "-51";
                response.errorcode = "Enter Valid Session ID";
                return response;
            }
            String channelid = session.getChannelid();

            ChannelManagement channelmagnt = new ChannelManagement();
            Channels channel = channelmagnt.getChannelByID(channelid);

            log.debug("channel :: " + channel.getName());
            //audit parameter
            String remoteaccesslogin = session.getLoginid();
            log.debug("remoteaccesslogin :: " + remoteaccesslogin);
            SettingsManagement sMngmt = new SettingsManagement();
            int retValue = -1;

            //   String strType = String.valueOf(iType);
            int iType = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.Radius;
            log.debug("iType :: " + iType);
            int iPreference = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE;
            log.debug("iPreference :: " + iPreference);

            Object settingsObj = sMngmt.getSettingV2(SettingID, sessionid, channelid, iType, iPreference);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            if (settingsObj == null) {
                response.errorcode = "-17";
                response.error = "Record not Found";
                return response;

            }
            System.out.println("settingsObj" + settingsObj);
            AxiomRadiusClient client = (AxiomRadiusClient) settingsObj;
            response.displayname = client.getRadiusClientDisplayname();
            response.authenticationtype = client.getRadiusClientAuthtype();
            response.clientIp = client.getRadiusClientIp();
            response.Status = client.getStatus();
            response.dayrestriction = client.getDayRestriction();
            response.Createdon = sdf.format(client.getCreationDate());
            response.lastupdate = sdf.format(client.getLastUpdateDate());
            response.TimeFromInHour = client.getTimeFromInHour();
            response.Timfromampm = client.getTimfromampm();
            response.TimeToInHour = client.getTimeToInHour();
            response.Timetoampm = client.getTimetoampm();
            return response;
        }
        response.errorcode = "-1";
        response.error = "Enter Valid Details";
        return response;
    }

}
