/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.radius;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.AxiomRadiusClient;
import com.mollatech.axiom.nucleus.settings.AxiomRadiusConfiguration;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class changeClientStatus extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(changeClientStatus.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final int ACTIVE_STATUS = 1;
    final int SUSPEND_STATUS = 0;
    final String itemtype = "SETTINGS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        String result = "success";
        String message = "Radius Setting Update Successful!!!";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String _value = "Active";
        String resultString = "Failure";
        String oldstatus="";
        try {

            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: "+channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId :: "+sessionId);
            //audit parameter
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("remoteaccesslogin :: "+remoteaccesslogin);
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operatorS :: "+operatorS.getName());
            //String OperatorID = operatorS.getOperatorid();
            String _channelId = channel.getChannelid();
            SettingsManagement sMngmt = new SettingsManagement();
            int retValue = -1;

            int _srno = Integer.parseInt(request.getParameter("srno"));
            log.debug("_srno :: "+_srno);
            int _status = Integer.parseInt(request.getParameter("_status"));
            log.debug("_status :: "+_status);
            if (_status == ACTIVE_STATUS) {
                _value = "Active ";
            } else if (_status == SUSPEND_STATUS) {
                _value = "Suspended ";
            }

            message = _value + message;
            int iType = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.Radius;
            log.debug("iType :: "+iType);
            int iPreference = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE;
            log.debug("iPreference :: "+iPreference);
            //   String strType = String.valueOf(iType);
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), iType, iPreference);

            AxiomRadiusConfiguration RadiusServerObj = null;
            boolean bAddSetting = false;
            AxiomRadiusClient nradiusClient = new AxiomRadiusClient();

            if (settingsObj == null) {
                RadiusServerObj = new AxiomRadiusConfiguration();

            } else {
                RadiusServerObj = (AxiomRadiusConfiguration) settingsObj;
                RadiusServerObj.setAccountEnabled(RadiusServerObj.isAccountEnabled());
                RadiusServerObj.setAccountIp(RadiusServerObj.getAccountIp());
                RadiusServerObj.setAccountPort(RadiusServerObj.getAccountPort());
                RadiusServerObj.setAuthEnabled(RadiusServerObj.isAuthEnabled());
                RadiusServerObj.setAuthIp(RadiusServerObj.getAuthIp());
                RadiusServerObj.setAuthPort(RadiusServerObj.getAuthPort());
                RadiusServerObj.setLdapServerUsername(RadiusServerObj.getLdapServerUsername());
                RadiusServerObj.setLdapServerPassword(RadiusServerObj.getLdapServerPassword());
                RadiusServerObj.setLdapSearchInitial(RadiusServerObj.getLdapSearchInitial());
                RadiusServerObj.setLdapSearchPath(RadiusServerObj.getLdapSearchPath());
                RadiusServerObj.setLdapServerIp(RadiusServerObj.getLdapServerIp());
                RadiusServerObj.setLdapServerPort(RadiusServerObj.getLdapServerPort());
                RadiusServerObj.setLdapValidate(RadiusServerObj.isLdapValidate());
                RadiusServerObj.setAxiomValidate(RadiusServerObj.isAxiomValidate());

                if (RadiusServerObj.getRadiusClient() == null) {
                    AxiomRadiusClient[] radiusClient = new AxiomRadiusClient[1];
                    radiusClient[0] = new AxiomRadiusClient();
                    radiusClient[0] = nradiusClient;
                    RadiusServerObj.setRadiusClient(radiusClient);

                } else {

                    AxiomRadiusClient[] radiusClient = new AxiomRadiusClient[RadiusServerObj.getRadiusClient().length];
                    AxiomRadiusClient[] aradiusClient = new AxiomRadiusClient[RadiusServerObj.getRadiusClient().length];
                    aradiusClient = RadiusServerObj.getRadiusClient();
                    int i = 0;
                    for (i = 0; i < RadiusServerObj.getRadiusClient().length; i++) {
                        if (i == _srno - 1) {
                            if(radiusClient[i].getStatus()==1){
                            oldstatus="ACTIVE";
                            }
                            else{
                                oldstatus="SUSPENDED";
                            }
                                                    
                            radiusClient[i] = new AxiomRadiusClient();
                            radiusClient[i] = nradiusClient;
                            radiusClient[i].setCreationDate(aradiusClient[i].getCreationDate());
                            radiusClient[i].setLastUpdateDate(aradiusClient[i].getLastUpdateDate());
                            radiusClient[i].setRadiusClientAuthtype(aradiusClient[i].getRadiusClientAuthtype());
                            radiusClient[i].setRadiusClientDisplayname(aradiusClient[i].getRadiusClientDisplayname());
                            radiusClient[i].setRadiusClientSecretkey(aradiusClient[i].getRadiusClientSecretkey());
                            radiusClient[i].setStatus(_status);
                            
                            radiusClient[i].setDayRestriction(aradiusClient[i].getDayRestriction());
                            radiusClient[i].setTimeFromInHour(aradiusClient[i].getTimeFromInHour());
                            radiusClient[i].setTimeToInHour(aradiusClient[i].getTimeToInHour());

                        } else {
                            radiusClient[i] = new AxiomRadiusClient();
                            radiusClient[i].setCreationDate(aradiusClient[i].getCreationDate());
                            radiusClient[i].setLastUpdateDate(aradiusClient[i].getLastUpdateDate());
                            radiusClient[i].setRadiusClientAuthtype(aradiusClient[i].getRadiusClientAuthtype());
                            radiusClient[i].setRadiusClientDisplayname(aradiusClient[i].getRadiusClientDisplayname());
                            radiusClient[i].setRadiusClientSecretkey(aradiusClient[i].getRadiusClientSecretkey());
                            radiusClient[i].setStatus(aradiusClient[i].getStatus());
                            
                            radiusClient[i].setDayRestriction(aradiusClient[i].getDayRestriction());
                            radiusClient[i].setTimeFromInHour(aradiusClient[i].getTimeFromInHour());
                            radiusClient[i].setTimeToInHour(aradiusClient[i].getTimeToInHour());
                            radiusClient[i].setTimetoampm(aradiusClient[i].getTimetoampm()); 
                            radiusClient[i].setTimfromampm(aradiusClient[i].getTimfromampm());
                            
                        }
                    }
                    RadiusServerObj.setRadiusClient(radiusClient);
                }

            }
            AuditManagement audit = new AuditManagement();
            if (bAddSetting == true) {
                retValue = sMngmt.addSetting(sessionId, _channelId, iType, iPreference, RadiusServerObj);
                log.debug("retValue :: "+retValue);
                if (retValue == 0) {
                    resultString = "SUCCESS";
                    result = "success";
                    message = "Radius Client Status Changed Successfully!";
                                        
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), 
                            "Change Radius Client", resultString, retValue,
                             "Radius Management", "", 
                             message +                                 
                            "Radius Client (" + _srno + ") changed to status=" + _status ,
                            itemtype, 
                            String.valueOf(_srno));
                     

                } 
                
                
                
//                else if (retValue == -2) {
//                    resultString = "Failure";
//                    result = "error";
//                    message = "Your Session is Expired ...!!!";
//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
//                            remoteaccesslogin, operatorS.getName(), new Date(), "Change Status", resultString, retValue,
//                            "User Management", "Old Status=" + oldstatus, "New Status=" + _value,
//                            itemtype, String.valueOf(_srno));
//                } 
                else {
                    resultString = "ERROR";
                    result = "error";
                    message = "Radius Settings Addition Failure...!!!";
//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
//                            remoteaccesslogin, operatorS.getName(), new Date(), "Change Status", resultString, retValue,
//                            "User Management", "Old Status=" + oldstatus, "New Status=" + _value,
//                            itemtype, String.valueOf(_srno));
                    
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), 
                            "Change Radius Client", resultString, retValue,
                             "Radius Management", "", 
                             message +                                 
                            "Radius Client (" + _srno + ") changed to status=" + _status ,
                            itemtype, 
                            String.valueOf(_srno));
                }
            } else {
                AxiomRadiusConfiguration oldglobalObj = (AxiomRadiusConfiguration) sMngmt.getSetting(sessionId, _channelId, iType, iPreference);
                retValue = sMngmt.changeSetting(sessionId, _channelId, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.Radius, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE, settingsObj, RadiusServerObj);

                if (retValue == 0) {
                    result = "success";
                    //message = "Radius Client updatation Successfull...!!!";
                    message = "Radius Client Status Changed Successfully!";
                    resultString = "SUCCESS";
//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
//                            remoteaccesslogin, operatorS.getName(), new Date(), "Change Status", resultString, retValue,
//                            "Radius Client", "Old Status=" + oldstatus, "New Status=" + _value,
//                            itemtype,  String.valueOf(_srno));
                    
                      audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), 
                            "Change Radius Client", resultString, retValue,
                             "Radius Management", "", 
                             message +                                 
                            "Radius Client (" + _srno + ") changed to status=" + _status ,
                            itemtype, 
                            String.valueOf(_srno));

                } 
                else {

                    result = "error";
                    message = "Radius Client Addition Failure...!!!";
                    resultString = "ERROR";
//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
//                            remoteaccesslogin, operatorS.getName(), new Date(), "Change Status", resultString, retValue,
//                            "Radius Client", "Old Status=" + oldstatus, "New Status=" + _value,
//                            itemtype,  String.valueOf(_srno));
                    
                      audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(), 
                            "Change Radius Client", resultString, retValue,
                             "Radius Management", "", 
                             message +                                 
                            "Radius Client (" + _srno + ") changed to status=" + _status ,
                            itemtype, 
                            String.valueOf(_srno));
                }
            }
        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
