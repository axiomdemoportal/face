/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.radius;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.AxiomRadiusClient;
import com.mollatech.axiom.nucleus.settings.AxiomRadiusConfiguration;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ideasventure
 */
public class addRadiusClient extends HttpServlet {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(addRadiusClient.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final String itemtype = "SETTINGS";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        String result = "success";
        String message = "Radius Setting Update Successful!!!";
        String resultString = "ERROR";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        try {

            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            log.debug("channel :: " + channel.getName());
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            log.debug("sessionId :: " + sessionId);
            //audit parameter
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            log.debug("remoteaccesslogin :: " + remoteaccesslogin);
            Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
            log.debug("operatorS :: " + operatorS.getName());
            String OperatorID = operatorS.getOperatorid();
            String _channelId = channel.getChannelid();
            SettingsManagement sMngmt = new SettingsManagement();
            int retValue = -1;

            String _displayname = request.getParameter("_displayname");
            log.debug("_displayname :: " + _displayname);
            String authenticationtype = request.getParameter("authenticationtype");
            log.debug("authenticationtype :: " + authenticationtype);
            String _radiusClientIp = request.getParameter("_rClientIP");
            log.debug("_radiusClientIp :: " + _radiusClientIp);
            String _radiusClientSecretkey = request.getParameter("_radiusClientSecretkey");
            log.debug("_radiusClientSecretkey :: " + _radiusClientSecretkey);
            String _dayRestriction = request.getParameter("_dayrestriction");
            log.debug("_dayRestriction :: " + _dayRestriction);
            String _timerange = request.getParameter("_timerange");
            log.debug("_timerange :: " + _timerange);
            String _timefromampm = request.getParameter("_timefromampm");
            log.debug("_timefromampm :: " + _timefromampm);
            String _totimerange = request.getParameter("_totimerange");
            log.debug("_totimerange :: " + _totimerange);
            String _timetoampm = request.getParameter("_timetoampm");
            log.debug("_timetoampm :: " + _timetoampm);
            int _dayRes = -1;
            int _timeFromIn = -1;
            int _timeTo = -1;
            int _timeFromampm = -1;
            int _timeToampm = -1;

//            if (_dayRestriction != null) {
//                _dayRes = Integer.parseInt(_dayRestriction);
//            }
//            if (_timerange != null) {
//                _timeFromIn = Integer.parseInt(_timerange);
//            }
//
//            if (_timefromampm != null) {
//                _timeFromampm = Integer.parseInt(_timefromampm);
//            }
//
//             if (_totimerange != null) {
//                _timeTo = Integer.parseInt(_totimerange);
//            }
//
//            if (_timetoampm != null) {
//                _timeToampm = Integer.parseInt(_timetoampm);
//            }
            int iType = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.Radius;
            log.debug("iType :: " + iType);
            int iPreference = com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE;
            log.debug("iPreference :: " + iPreference);
            //   String strType = String.valueOf(iType);
            Object settingsObj = sMngmt.getSetting(sessionId, channel.getChannelid(), iType, iPreference);

            AxiomRadiusConfiguration RadiusServerObj = null;
            boolean bAddSetting = false;
            AxiomRadiusClient nradiusClient = new AxiomRadiusClient();

            nradiusClient.setCreationDate(new Date().getTime());
            nradiusClient.setLastUpdateDate(new Date().getTime());
            nradiusClient.setStatus(1);
            nradiusClient.setRadiusClientSecretkey(_radiusClientSecretkey);
            nradiusClient.setRadiusClientDisplayname(_displayname);
            nradiusClient.setRadiusClientIp(_radiusClientIp);
            nradiusClient.setRadiusClientAuthtype(authenticationtype);

            nradiusClient.setDayRestriction(_dayRes);
            nradiusClient.setTimeFromInHour(_timeFromIn);
            nradiusClient.setTimeToInHour(_timeTo);
            nradiusClient.setTimetoampm(_timeToampm);
            nradiusClient.setTimfromampm(_timeFromampm);

            if (settingsObj == null) {
                RadiusServerObj = new AxiomRadiusConfiguration();
            } else {
                RadiusServerObj = (AxiomRadiusConfiguration) settingsObj;
                RadiusServerObj.setAccountEnabled(RadiusServerObj.isAccountEnabled());
                RadiusServerObj.setAccountIp(RadiusServerObj.getAccountIp());
                RadiusServerObj.setAccountPort(RadiusServerObj.getAccountPort());
                RadiusServerObj.setAuthEnabled(RadiusServerObj.isAuthEnabled());
                RadiusServerObj.setAuthIp(RadiusServerObj.getAuthIp());
                RadiusServerObj.setAuthPort(RadiusServerObj.getAuthPort());
                RadiusServerObj.setLdapServerUsername(RadiusServerObj.getLdapServerUsername());
                RadiusServerObj.setLdapServerPassword(RadiusServerObj.getLdapServerPassword());
                RadiusServerObj.setLdapSearchPath(RadiusServerObj.getLdapSearchPath());
                RadiusServerObj.setLdapSearchInitial(RadiusServerObj.getLdapSearchInitial());
                RadiusServerObj.setLdapServerIp(RadiusServerObj.getLdapServerIp());
                RadiusServerObj.setLdapServerPort(RadiusServerObj.getLdapServerPort());
                RadiusServerObj.setLdapValidate(RadiusServerObj.isLdapValidate());
                RadiusServerObj.setAxiomValidate(RadiusServerObj.isAxiomValidate());

                if (RadiusServerObj.getRadiusClient() == null) {
                    AxiomRadiusClient[] radiusClient = new AxiomRadiusClient[1];
                    radiusClient[0] = new AxiomRadiusClient();
                    radiusClient[0] = nradiusClient;
                    RadiusServerObj.setRadiusClient(radiusClient);

                } else {
                    int count = 0;
                    AxiomRadiusClient[] radiusClient = new AxiomRadiusClient[RadiusServerObj.getRadiusClient().length + 1];
                    AxiomRadiusClient[] aradiusClient = new AxiomRadiusClient[RadiusServerObj.getRadiusClient().length + 1];
                    aradiusClient = RadiusServerObj.getRadiusClient();
                    int i = 0;
                    for (i = 0; i < RadiusServerObj.getRadiusClient().length; i++) {
                        radiusClient[i] = new AxiomRadiusClient();
                        radiusClient[i].setCreationDate(aradiusClient[i].getCreationDate());
                        radiusClient[i].setLastUpdateDate(aradiusClient[i].getLastUpdateDate());
                        radiusClient[i].setRadiusClientAuthtype(aradiusClient[i].getRadiusClientAuthtype());
                        radiusClient[i].setRadiusClientDisplayname(aradiusClient[i].getRadiusClientDisplayname());
                        radiusClient[i].setRadiusClientSecretkey(aradiusClient[i].getRadiusClientSecretkey());
                        radiusClient[i].setRadiusClientIp(aradiusClient[i].getRadiusClientIp());
                        radiusClient[i].setStatus(aradiusClient[i].getStatus());

                        radiusClient[i].setDayRestriction(aradiusClient[i].getDayRestriction());
                        radiusClient[i].setTimeFromInHour(aradiusClient[i].getTimeFromInHour());
                        radiusClient[i].setTimfromampm(aradiusClient[i].getTimfromampm());
                        radiusClient[i].setTimeToInHour(aradiusClient[i].getTimeFromInHour());
                        radiusClient[i].setTimetoampm(aradiusClient[i].getTimetoampm());

                    }
                    radiusClient[i] = new AxiomRadiusClient();
                    radiusClient[i] = nradiusClient;

                    RadiusServerObj.setRadiusClient(radiusClient);

                }

            }
            AuditManagement audit = new AuditManagement();
            if (bAddSetting == true) {
                retValue = sMngmt.addSetting(sessionId, _channelId, iType, iPreference, RadiusServerObj);

                if (retValue == 0) {
                    result = "success";
                    message = "Radius Settings Addition Successful...!!!";
                    resultString = "Failure";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "ChangeRadiusClient", resultString, retValue, "Setting Management",
                            "", "_displayname=" + nradiusClient.getRadiusClientDisplayname() + ",authenticationtype=" + nradiusClient.getRadiusClientAuthtype() + ",_rClientIP=" + nradiusClient.getRadiusClientIp()
                            + ",_radiusClientSecretkey=" + nradiusClient.getRadiusClientSecretkey(),
                            itemtype, OperatorID);

                } else if (retValue == -2) {
                    result = "error";
                    message = "Your Session is Expired ...!!!";
                    resultString = "Failure";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "ChangeRadiusClient", resultString, retValue, "Setting Management",
                            "", "_displayname=" + nradiusClient.getRadiusClientDisplayname() + ",authenticationtype=" + nradiusClient.getRadiusClientAuthtype() + ",_rClientIP=" + nradiusClient.getRadiusClientIp()
                            + ",_radiusClientSecretkey=" + nradiusClient.getRadiusClientSecretkey(),
                            itemtype, OperatorID);
                } else {

                    result = "error";
                    message = "Radius Settings Addition Failure...!!!";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
                            request.getRemoteAddr(),
                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
                            "ChangeRadiusClient", resultString, retValue, "Setting Management",
                            "", "_displayname=" + nradiusClient.getRadiusClientDisplayname() + ",authenticationtype=" + nradiusClient.getRadiusClientAuthtype() + ",_rClientIP=" + nradiusClient.getRadiusClientIp()
                            + ",_radiusClientSecretkey=" + nradiusClient.getRadiusClientSecretkey(),
                            itemtype, OperatorID);
                }

            } else if (bAddSetting == false) {
                AxiomRadiusConfiguration oldglobalObj = (AxiomRadiusConfiguration) sMngmt.getSetting(sessionId, _channelId, iType, iPreference);
                retValue = sMngmt.changeSetting(sessionId, _channelId, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.Radius, com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement.PREFERENCE_ONE, settingsObj, RadiusServerObj);
                resultString = "Failure";
                if (retValue == 0) {

                    resultString = "SUCCESS";
//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(),
//                            request.getRemoteAddr(),
//                            channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(),
//                            "ChangeRadiusClient", resultString, retValue, "Setting Management",
//                            "", "_displayname=" + nradiusClient.getRadiusClientDisplayname() + ",authenticationtype=" + nradiusClient.getRadiusClientAuthtype() + ",_rClientIP=" + nradiusClient.getRadiusClientIp()
//                            + ",_radiusClientSecretkey=" + nradiusClient.getRadiusClientSecretkey(),
//                            itemtype, OperatorID);

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add Radius Client", resultString, retValue,
                            "Radius Management", "",
                            message
                            + "_displayname=" + _displayname + ",authenticationtype=" + authenticationtype
                            + ",_rClientIP=" + _radiusClientIp,
                            itemtype,
                            "-");

                } else {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(),
                            remoteaccesslogin, operatorS.getName(), new Date(),
                            "Add Radius Client", resultString, retValue,
                            "Radius Management", "",
                            message
                            + "_displayname=" + _displayname + ",authenticationtype=" + authenticationtype
                            + ",_rClientIP=" + _radiusClientIp,
                            itemtype,
                            "-");
                }

            }

        } catch (Exception ex) {
            log.error("Exception caught :: ", ex);
        }
        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception ex) {
            log.error("Exception caught :: ", ex);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
