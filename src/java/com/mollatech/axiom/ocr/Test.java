package com.mollatech.axiom.ocr;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;

public class Test {

public static void main(String [] args) throws IOException{
PDDocument doc = PDDocument.load(new File("/home/bluebricks/Documents/similar_PDF/20942526_7577.pdf"));
List<PDPage>pages =  doc.getDocumentCatalog().getAllPages();
PDPage page = pages.get(0);
BufferedImage image =page.convertToImage();
File outputfile = new File("/home/bluebricks/Documents/similar_PDF/image.jpg");
ImageIO.write(image, "jpg", outputfile);
doc.close();

}
}