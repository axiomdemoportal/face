/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.ocr;

import com.mollatech.axiom.nucleus.db.ApOcrresult;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.DocumentTemplate;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.DocsTemplatesManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OcrResultManagement;
//import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class SendTestdata extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String result = null;
        String message = null;
        JSONObject json = new JSONObject();
        try {
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            response.setContentType("application/json");
            String templateName = request.getParameter("templateName1");
            String documentName = request.getParameter("docsname");
            try {
                DocsTemplatesManagement docObj = new DocsTemplatesManagement();
                String savepath = "";
                savepath = System.getProperty("catalina.home");
                if (savepath == null) {
                    savepath = System.getenv("catalina.home");
                }
                savepath += System.getProperty("file.separator");
                savepath += "axiomv2-settings";
                savepath += System.getProperty("file.separator");
                savepath += "ocrpdfupload/Upload for result";
                savepath += System.getProperty("file.separator");
                savepath += documentName + ".pdf";
                if (!Files.exists(Paths.get(savepath))) {
                    result = "error";
                    message = "Document does not exist.";
                    json.put("result", result);
                    json.put("message", message);
                    return;
                }
                System.out.println(">>>>>>>>>>>> " + savepath);
                InputStream finput = new FileInputStream(savepath);
                byte[] bytes = IOUtils.toByteArray(finput);
                String encodedImage = new String(Base64.encode(bytes));
                DocumentTemplate docsDetails = docObj.editDocumentDetails(templateName, "DxJUGvzOF1+zS7BaeclbdudlSIs=");
                if (docsDetails == null) {
                    result = "error";
                    message = "Template does not exist.";
                    json.put("result", result);
                    json.put("message", message);
                    return;
                }
                String fieldCordinate = docsDetails.getTemplateDetails();
                String keyCordinates = docsDetails.getTemplateKeyValue();
                byte[] byteimage = docsDetails.getProcessImage();
                String url = "http://128.199.222.136:5000/todo/api/v1.0/tasks";
//                String url = "http://192.168.0.114:5000/todo/api/v1.0/tasks";
                JSONObject jsonObj = new JSONObject();
                JSONObject jo = new JSONObject();
                jsonObj.put("methodname", "processocr");
                jsonObj.put("templatejson", fieldCordinate);
                jsonObj.put("keycordinates", keyCordinates);
                jsonObj.put("imageforocr", encodedImage);
                jsonObj.put("imageid", documentName);
                jsonObj.put("docextension", "pdf");
                jsonObj.put("ocr_template", docsDetails.getDocType());
                System.out.println("Field co-ordinates == " + fieldCordinate);
                System.out.println("keycordinates == " + jo.toString());
                System.out.println("imageforocr == " + encodedImage);
                RestCallingWrapper callService = new RestCallingWrapper();

                result = callService.callMyService(jsonObj.toString(), url);
                System.out.println(">>>>> " + result);
                JSONObject resultObj;
                resultObj = new JSONObject(result);
                JSONObject resultString = (JSONObject) resultObj.get("task");
                String finalData = resultString.get("data").toString();
                String resultcode = resultString.get("Result").toString();
                String errormessage = resultString.get("errorMessage").toString();
                String ocrdetails = finalData.replace("/^\\ /", "");
                long time = System.currentTimeMillis();
                String docid = sessionId + time;
                HttpSession session = request.getSession(true);
                session.setAttribute("docid", docid);
                int res = -1;
                if (Integer.parseInt(resultcode) == 0) {
                    OcrResultManagement ocrMgmt = new OcrResultManagement();
                    ApOcrresult ocrDetails = new ApOcrresult();
                    ocrDetails.setChannelId(channel.getChannelid());
                    ocrDetails.setTemplateName(templateName);
                    ocrDetails.setTestimageofpdf(bytes);
                    ocrDetails.setResult(finalData);
                    ocrDetails.setCreatedOn(new Date());
                    ocrDetails.setDocumentid(docid);
                    res = ocrMgmt.addDocumentDetails(ocrDetails);
                }
                AuditManagement audit = new AuditManagement();
                if (res == 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin, operator.getName(), new Date(), "Extract Ocr", "Ocr Field Added,Extract Ocr Saved Successfully", 0, "", "", "Extract Ocr Saved Successfully", "OcrDocumentation", remoteaccesslogin);
                } else {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin, operator.getName(), new Date(), "Extract Ocr", "Failed To Add Extract data.", -1, "", "", "Extracted Ocr Failed To Add" + sessionId, "OcrDocumentation", remoteaccesslogin);
                }
                System.out.println("-----------------------");
                json.put("result", ocrdetails);
                json.put("resultcode", ocrdetails);
                json.put("errormessage", ocrdetails);
            } catch (Exception ex) {
                ex.printStackTrace();
//                Logger.getLogger(SendData.class.getName()).log(Level.SEVERE, null, ex);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
