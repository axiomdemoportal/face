package com.mollatech.axiom.ocr;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.DocumentTemplate;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.DocsTemplatesManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class Adddocdata extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        String fieldname = request.getParameter("fieldname");
        Operators operator = (Operators) request.getSession().getAttribute("_apOprDetail");
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        String templatename = request.getParameter("templateName");
        if (templatename == null) {
            templatename = (String) request.getSession().getAttribute("templateName");
        
            
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        String datatype = request.getParameter("datatype");
        JSONObject json = new JSONObject();
        String result = null;
        String message = null;
        int res = -1;
        PrintWriter out = response.getWriter();
        String x1 = request.getParameter("x1");
        String y1 = request.getParameter("y1");
        String x2 = request.getParameter("x2");
        String y2 = request.getParameter("y2");
        String w = request.getParameter("w");
        String ocrtype = request.getParameter("ocrt_type");
        JSONObject main = new JSONObject();
        JSONObject jobject = new JSONObject();
        try {
            DocsTemplatesManagement docObj = new DocsTemplatesManagement();
            DocumentTemplate docsDetails = docObj.getDocumentDetails(templatename, channel.getChannelid());

            if (docsDetails != null && null != docsDetails.getTemplateDetails()) {
                JSONObject ocrArr = new JSONObject(docsDetails.getTemplateDetails());
                jobject.put("x1", x1);
                jobject.put("y1", y1);
                jobject.put("x2", x2);
                jobject.put("y2", y2);
                jobject.put("w", w);
                jobject.put("_datatype", datatype);
                jobject.put("ocrt_type", ocrtype);
                ocrArr.put(fieldname, jobject);
                docsDetails.setOperatorId(operator.getOperatorid());
                docsDetails.setTemplateDetails(ocrArr.toString());
                docsDetails.setUpdatedOn(new Date());
            } else {
                JSONObject ocrArr = new JSONObject();
                jobject.put("x1", x1);
                jobject.put("y1", y1);
                jobject.put("x2", x2);
                jobject.put("y2", y2);
                jobject.put("w", w);
                jobject.put("_datatype", datatype);
                jobject.put("ocrt_type", ocrtype);
                ocrArr.put(fieldname, jobject);
                docsDetails.setOperatorId(operator.getOperatorid());
                docsDetails.setTemplateDetails(ocrArr.toString());
                docsDetails.setUpdatedOn(new Date());
            }

            DocsTemplatesManagement tempObj = new DocsTemplatesManagement();
            res = tempObj.updateDocumentDetails(docsDetails);
        } catch (Exception e) {
            e.printStackTrace();
            message = "Please try again.";
            result = "error";
            try {
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
            } catch (JSONException ex) {
                Logger.getLogger(Adddocdata.class.getName()).log(Level.SEVERE, null, ex);
            }

            return;
        }
        AuditManagement audit = new AuditManagement();
        if (res == 0) {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),  request.getRemoteAddr(), channel.getName(),remoteaccesslogin, operator.getName(), new Date(), "Create template", "Ocr Field Added", 0, "", "", "Ocr field added" + sessionId, "OcrDocumentation", remoteaccesslogin);
            message = "Added suceessfully.";
            result = "success";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (JSONException ex) {
                Logger.getLogger(Adddocdata.class.getName()).log(Level.SEVERE, null, ex);
            }
            out.print(json);
            out.flush();
            return;

        } else {
            audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),  request.getRemoteAddr(), channel.getName(),remoteaccesslogin, operator.getName(), new Date(), "Create template", "Ocr Field Added", -1, "", "", "Failed To Add Ocr field" + sessionId, "OcrDocumentation", remoteaccesslogin);
            message = "Please try again.";
            result = "error";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (JSONException ex) {
                Logger.getLogger(Adddocdata.class.getName()).log(Level.SEVERE, null, ex);
            }
            out.print(json);
            out.flush();
            return;
        }
    }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
