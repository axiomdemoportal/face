/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.ocr;

import com.mollatech.axiom.nucleus.db.connector.management.DocsTemplatesManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class DeleteTemplate extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        response.setContentType("application/json");
        String tempId = request.getParameter("templateId");
        int _tmplateId = -1;
        String _result = null;
        String _message = null;
        String _url = null;
        if(tempId != null){
        _tmplateId = Integer.parseInt(tempId);
        }
        int result = -1;
        com.mollatech.axiom.nucleus.db.Channels channel = (com.mollatech.axiom.nucleus.db.Channels) request.getSession().getAttribute("_apSChannelDetails");
        DocsTemplatesManagement templateObj = new DocsTemplatesManagement();
        result = templateObj.deletetemplate(_tmplateId, channel.getChannelid());
        if(result == 0){
            _result = "success";
            _message = "Template deleted successfully";
            try {
                json.put("_result", _result);
                json.put("_message", _message);
                json.put("_url", "templatereport.jsp");
            } catch (Exception ex) {
                Logger.getLogger(DeleteTemplate.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }else{
            _result = "error";
            _message = "Unable to delete Template";
            try {
                json.put("_result", _result);
                json.put("_message", _message);
            } catch (Exception ex) {
                Logger.getLogger(DeleteTemplate.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
