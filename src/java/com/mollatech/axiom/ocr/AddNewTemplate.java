/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.ocr;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.DocumentTemplate;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.DocsTemplatesManagement;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class AddNewTemplate extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        try {
            JSONObject json = new JSONObject();
            String sessionId = (String) request.getSession().getAttribute("_apSessionID");
            String templateName = request.getParameter("_templatename");
            HttpSession session = request.getSession(true);
            session.setAttribute("templateName", templateName);
            String templatetype = request.getParameter("templateType");
            String base64image = (String) request.getSession().getAttribute("preprocessImage");
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            String docname = request.getParameter("docsName");
            Operators operator = (com.mollatech.axiom.nucleus.db.Operators) request.getSession().getAttribute("_apOprDetail");
            Channels channel = (com.mollatech.axiom.nucleus.db.Channels) request.getSession().getAttribute("_apSChannelDetails");
            byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(base64image);
            BufferedImage img = ImageIO.read(new ByteArrayInputStream(imageBytes));
            System.out.println("width == " + img.getWidth());
            System.out.println("height == " + img.getHeight());
            String result = null;
            String message = null;
            DocsTemplatesManagement tempObj = new DocsTemplatesManagement();
            DocumentTemplate templateData = tempObj.getDocumentDetails(templateName, channel.getChannelid());
            if (templateData != null) {
                message = "Template name already exist..";
                result = "error";
                json.put("_result", result);
                json.put("_message", message);
                out.print(json);
                out.flush();
                return;
            } else {
                DocumentTemplate docsObj = new DocumentTemplate();
                docsObj.setTemplateName(templateName);
                docsObj.setWidthInPixel(Integer.toString(img.getWidth()));
                docsObj.setHeightInpixel(Integer.toString(img.getHeight()));
                docsObj.setProcessImage(imageBytes);
                docsObj.setDocsImageName(docname);
                docsObj.setOperatorId(operator.getOperatorid());
                docsObj.setChannelid(channel.getChannelid());
                docsObj.setDocType(templatetype);
                docsObj.setCreatedOn(new Date());
                docsObj.setUpdatedOn(new Date());
                int res = new DocsTemplatesManagement().addDocumentDetails(docsObj);
                AuditManagement audit = new AuditManagement();
                if (res == 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),  request.getRemoteAddr(), channel.getName(),remoteaccesslogin, operator.getName(), new Date(), "Create template", "Template created successfully", 0, "", "", "Template Created successfully" + sessionId, "OcrDocumentation", remoteaccesslogin);
                    message = "Template added successfully..";
                    result = "success";
                    String url = "addTemplateDetails.jsp?templateName=" + templateName;
                    json.put("_result", result);
                    json.put("_message", message);
                    json.put("_url", url);
                    out.print(json);
                    out.flush();
                    return;
                } else {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),  request.getRemoteAddr(), channel.getName(),remoteaccesslogin, operator.getName(), new Date(), "Create template", "Failed to Create Template", -1, "", "", "Create Template successfully with Session Id =" + sessionId, "OcrDocumentation", remoteaccesslogin);
//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), operator.getOperatorid(),  request.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "Create template", aStatus.error, aStatus.errorcode, "", "", "Create Template successfully with Session Id =" + sessionid, "OcrDocumentation", session.getLoginid());
                    message = "Failed to add Template..";
                    result = "error";
                    json.put("_result", result);
                    json.put("_message", message);
                    out.print(json);
                    out.flush();
                    return;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
