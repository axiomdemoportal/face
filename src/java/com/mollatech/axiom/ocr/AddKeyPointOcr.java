/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.ocr;

import com.mollatech.axiom.nucleus.db.DocumentTemplate;
import com.mollatech.axiom.nucleus.db.connector.management.DocsTemplatesManagement;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class AddKeyPointOcr extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        String fieldname = request.getParameter("fieldname");
        String templateDocsName = request.getParameter("templateDocsName");
        com.mollatech.axiom.nucleus.db.Operators operator = (com.mollatech.axiom.nucleus.db.Operators) request.getSession().getAttribute("_apOprDetail");
        com.mollatech.axiom.nucleus.db.Channels channel = (com.mollatech.axiom.nucleus.db.Channels) request.getSession().getAttribute("_apSChannelDetails");
        String templatename = request.getParameter("templatename");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
         try {
             HttpSession session = request.getSession(true);
        String base64image = (String) request.getSession().getAttribute("preprocessImage");
        byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(base64image);
        BufferedImage img = ImageIO.read(new ByteArrayInputStream(imageBytes));
        String result = null;
        String message = null;
        int res;
        res = 1;
        String x1 = request.getParameter("x1");
        String y1 = request.getParameter("y1");
        String x2 = request.getParameter("x2");
        String y2 = request.getParameter("y2");
        String w = request.getParameter("w");
        String h = request.getParameter("h");
            String url = null;
            DocumentTemplate docsDetails = new DocumentTemplate();
            JSONObject Obj = new JSONObject();
            Obj.put("x1", x1);
            Obj.put("y1", y1);
            Obj.put("x2", x2);
            Obj.put("y2", y2);
            Obj.put("w", w);
            Obj.put("h", h);
            Obj.put("fieldname", fieldname);
            docsDetails.setChannelid(channel.getChannelid());
            docsDetails.setTemplateName(templateDocsName);
            docsDetails.setOperatorId(operator.getOperatorid());
            docsDetails.setTemplateKeyValue(Obj.toString());
            docsDetails.setDocsImageName(templatename);
            docsDetails.setProcessImage(imageBytes);
            docsDetails.setCreatedOn(new Date());
            docsDetails.setUpdatedOn(new Date());
            docsDetails.setHeightInpixel(Integer.toString(img.getHeight()));
            docsDetails.setWidthInPixel(Integer.toString(img.getWidth()));
            DocsTemplatesManagement tempObj = new DocsTemplatesManagement();
            res = tempObj.addDocumentDetails(docsDetails);
            url = "./addTemplateDetails.jsp?_templateName=" + templateDocsName;
            session.setAttribute("DocsName", templateDocsName);
            if (res == 0) {
                message = "Added suceessfully.";
                result = "success";
            } else {
                message = "Please try again.";
                result = "error";
            }
            json.put("_result", result);
            json.put("_message", message);
            json.put("url", url);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
//            out.print(json);
            out.print(json);
            out.flush();
            return;
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
