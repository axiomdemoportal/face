/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.ocr;

import com.mollatech.axiom.nucleus.db.DocumentTemplate;
import com.mollatech.axiom.nucleus.db.connector.management.DocsTemplatesManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.channels.Channels;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class DeleteTemplateField extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        int res = -1;
        try {
            com.mollatech.axiom.nucleus.db.Channels channel = (com.mollatech.axiom.nucleus.db.Channels) request.getSession().getAttribute("_apSChannelDetails");
            String cordinateName = request.getParameter("_cordinateName");
            String templateName = request.getParameter("templatename");
            String x1 = request.getParameter("x1");
            String y1 = request.getParameter("y1");
            String x2 = request.getParameter("x2");
            String y2 = request.getParameter("y2");
            DocsTemplatesManagement docObj = new DocsTemplatesManagement();
            DocumentTemplate docsDetails = docObj.editDocumentDetails(templateName, channel.getChannelid());
            if (docsDetails != null) {
                if (docsDetails != null && docsDetails.getTemplateKeyValue() != null) {
                    docsDetails.getTemplateDetails();
                    JSONObject jsonObj = null;
                    JSONObject main = new JSONObject();
                    jsonObj = new JSONObject(docsDetails.getTemplateDetails());
                    Iterator<?> keys = jsonObj.keys();
                    while (keys.hasNext()) {
                        String key = (String) keys.next();
                        System.out.println("object == " + key);
                        System.out.println("object data == " + jsonObj.get(key));
                        if (key.equals(cordinateName)) {
                        }else{
                            main.put(key, jsonObj.get(key));
                        }
                    }
                    docsDetails.setTemplateDetails(main.toString());
                    DocsTemplatesManagement tempObj = new DocsTemplatesManagement();
                    res = tempObj.updateDocumentDetails(docsDetails);
                }
                if (res == 0) {
                    json.put("_result", "success");
                    json.put("_message", "Template field deleted successful.");
                }else{
                    json.put("_result", "error");
                    json.put("_message", "Template field not deleted.");
                }
            }else{
                    json.put("_result", "error");
                    json.put("_message", "Template field not deleted.");
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
