/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.ocr;

import com.mollatech.axiom.nucleus.db.ApTemplateJsonStructure;
import com.mollatech.axiom.nucleus.db.DocumentTemplate;
import com.mollatech.axiom.nucleus.db.connector.management.DocsTemplatesManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateTableToJsonManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class addColumnFromTable extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        try {
            String templateName = request.getParameter("templatename");
            String columnName = request.getParameter("_columnName");
            com.mollatech.axiom.nucleus.db.Channels channel = (com.mollatech.axiom.nucleus.db.Channels) request.getSession().getAttribute("_apSChannelDetails");
            DocsTemplatesManagement docsObj = new DocsTemplatesManagement();
            DocumentTemplate docsDetails = docsObj.getDocumentDetails(templateName, channel.getChannelid());
            int res = -1;
            if (docsDetails == null) {
                try {
                    json.put("_result", "error");
                    json.put("_message", "Template not found.");
                    return;
                } catch (JSONException ex) {
                    ex.printStackTrace();
                } finally {
                    out.print(json);
                    out.flush();
                }
            }
            TemplateTableToJsonManagement tableObj = new TemplateTableToJsonManagement();
            ApTemplateJsonStructure jsonObj = tableObj.getDocumentDetailsByName(templateName, channel.getChannelid());
            if (jsonObj != null) {
                jsonObj.setTemplateColumn(jsonObj.getTemplateColumn() + "," + columnName);
                jsonObj.setTemplateName(templateName);
                jsonObj.setCreatedOn(new Date());
                jsonObj.setUpdatedOn(new Date());
                jsonObj.setTemplateId(Integer.toString(docsDetails.getTemplateId()));
                res = tableObj.updateDocumentDetails(jsonObj);
            } else {
                ApTemplateJsonStructure jsonObj1 = new ApTemplateJsonStructure();
                jsonObj1.setTemplateColumn(columnName);
                jsonObj1.setTemplateName(templateName);
                jsonObj1.setCreatedOn(new Date());
                jsonObj1.setUpdatedOn(new Date());
                jsonObj1.setTemplateId(Integer.toString(docsDetails.getTemplateId()));
                res = tableObj.addDocumentDetails(jsonObj1);
            }
            if (res == 0) {
                json.put("_result", "success");
                json.put("_message", "Column added successfully.");
                out.print(json);
                out.flush();
                return;
            } else {
                json.put("_result", "error");
                json.put("_message", "Unable to add column please try again.");
                out.print(json);
                out.flush();
                return;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
