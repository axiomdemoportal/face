///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.mollatech.axiom.ocr;
//
//import com.mollatech.axiom.document.PreprocessImage;
//import com.mollatech.axiom.nucleus.db.DocumentTemplate;
//import com.mollatech.axiom.nucleus.db.connector.management.DocsTemplatesManagement;
////import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
//import java.awt.image.BufferedImage;
//import java.io.ByteArrayInputStream;
//import java.io.ByteArrayOutputStream;
//import java.io.File;
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.imageio.ImageIO;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import org.bouncycastle.util.encoders.Base64;
//import org.json.JSONObject;
//
///**
// *
// * @author bluebricks
// */
//public class SendData extends HttpServlet {
//
//    /**
//     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
//     * methods.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        PrintWriter out = response.getWriter();
//        JSONObject json = new JSONObject();
//        try {
//            response.setContentType("application/json");
////        String templateName = request.getParameter("templateName");
//            String templateName = "image1003";
////        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
//            DocsTemplatesManagement docObj = new DocsTemplatesManagement();
//            String savepath = "";
//            savepath = System.getProperty("catalina.home");
//            if (savepath == null) {
//                savepath = System.getenv("catalina.home");
//            }
//            savepath += System.getProperty("file.separator");
//            savepath += "axiomv2-settings";
//            savepath += System.getProperty("file.separator");
//            savepath += "ocrpdfupload";
//            savepath += System.getProperty("file.separator");
//            savepath += "Converted_PdfFiles_to_Image";
//            savepath += System.getProperty("file.separator");
//            savepath += templateName + ".png";
//            String Image = null;
//            System.out.println(">>>>>>>>>>>> " + savepath);
//            BufferedImage image = ImageIO.read(new File(savepath));
//            ByteArrayOutputStream baos = new ByteArrayOutputStream();
//            ImageIO.write(image, "png", baos);
//            byte[] res = baos.toByteArray();
//            String encodedImage = new String(Base64.encode(baos.toByteArray()));
//            //////Preprocess Image
//            String result = null;
//            PreprocessImage preObj = new PreprocessImage();
//            result = preObj.callPreprocessingService(encodedImage, templateName + ".png");
//            JSONObject resultObj = new JSONObject(result);
//            JSONObject resultString = (JSONObject) resultObj.get("task");
//            String finalImage = resultString.get("Result").toString();
//            byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(finalImage);
//            BufferedImage img = ImageIO.read(new ByteArrayInputStream(imageBytes));
//            ResizeImage resizeImage = new ResizeImage();
//            ////Resize Image
//            BufferedImage resized = resizeImage.resize(img, img.getHeight(), img.getWidth());
//            DocumentTemplate docsDetails = docObj.editDocumentDetails(templateName + ".pdf", "DxJUGvzOF1+zS7BaeclbdudlSIs=");
//            String fieldCordinate = docsDetails.getTemplateDetails();
//            byte[] byteimage = docsDetails.getProcessImage();
//            String url = "http://192.168.0.118:5000/todo/api/v1.0/tasks";
//            JSONObject jsonObj = new JSONObject();
//            JSONObject jo = new JSONObject();
//            jo.put("keyname", "DBS Bank Ltd");
//            jsonObj.put("methodname", "processocr");
//            jsonObj.put("templatejson", fieldCordinate);
//            jsonObj.put("keycordinates", jo.toString());
//            jsonObj.put("imageforocr", encodedImage);
//            jsonObj.put("imageid", templateName);
//            System.out.println("Field co-ordinates == " + fieldCordinate);
//            System.out.println("keycordinates == " + jo.toString());
//            System.out.println("imageforocr == " + encodedImage);
//            RestCallingWrapper callService = new RestCallingWrapper();
//            String extractResult = null;
//            result = callService.callMyService(jsonObj.toString(), url);
//            System.out.println(">>>>> " + extractResult);
//        } catch (Exception ex) {
//            Logger.getLogger(SendData.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
//    /**
//     * Handles the HTTP <code>GET</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /**
//     * Handles the HTTP <code>POST</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /**
//     * Returns a short description of the servlet.
//     *
//     * @return a String containing servlet description
//     */
//    @Override
//    public String getServletInfo() {
//        return "Short description";
//    }// </editor-fold>
//
//}
