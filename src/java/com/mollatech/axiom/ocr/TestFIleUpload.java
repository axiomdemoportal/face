/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.ocr;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.docs.upload.TestImageCreate;
import com.mollatech.license.registerlicense;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class TestFIleUpload extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String strError = "";
        String saveFile = "";
        String savepath = "";
        String result = "success";
        String message = "File Upload sucessfully";

        JSONObject json = new JSONObject();

        savepath = System.getProperty("catalina.home");
        if (savepath == null) {
            savepath = System.getenv("catalina.home");
        }
        savepath += System.getProperty("file.separator");
        savepath += "axiomv2-settings";
        savepath += System.getProperty("file.separator");
        savepath += "ocrpdfupload/Upload for result";
        savepath += System.getProperty("file.separator");
        String optionalFileName = "";
        FileItem fileItem = null;
        String[] files = new String[1];	 // file names
        String dirName = savepath;
        int retValue = 0;
        String fileName = null;
        int i = 0;
        if (ServletFileUpload.isMultipartContent(request)) {
            try {
                ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                List fileItemsList = servletFileUpload.parseRequest(request);
                Iterator it = fileItemsList.iterator();
                while (it.hasNext()) {
                    FileItem fileItemTemp = (FileItem) it.next();
                    if (fileItemTemp.isFormField()) {
                        if (fileItemTemp.getFieldName().equals("filename")) {
                            optionalFileName = fileItemTemp.getString();
                        } else {
                            System.out.println("Additional fields in file upload form is  " + fileItemTemp.getFieldName());
                        }
                    } else {
                        fileItem = fileItemTemp;
                    }
                    if (fileItem != null) {
                        fileName = fileItem.getName();
                        if (!fileName.endsWith(".pdf")) {
                            strError = "Please Select Pdf File To Upload...!!!";
                            result = "error";
                            try {
                                json.put("result", result);
                                json.put("message", strError);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            //out.print(json);
                            out.print(json);
                            out.flush();
                            return;
                        }
                        if (fileItem.getSize() == 0) {
                            strError = "Please Select File To Upload...!!!";
                            result = "error";
                            try {
                                json.put("result", result);
                                json.put("message", strError);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            //out.print(json);
                            out.print(json);
                            out.flush();
                            return;
                        }

                        if (fileItem.getSize() > 0 && fileItem.getSize() < 1024000 * 5) {
                            // size cannot be more than 65Kb. We want it light.
                            if (optionalFileName.trim().equals("")) {
                                fileName = FilenameUtils.getName(fileName);
                            } else {
                                fileName = optionalFileName;
                            }
                            files[i++] = dirName + fileName;
                            File saveTo = new File(dirName + fileName);

                            saveFile = fileName;

                            AXIOMStatus axiom[] = null;
                            try {
                                fileItem.write(saveTo);
                                HttpSession session = request.getSession(true);

                                session.setAttribute("_templatefilePath", saveTo.getAbsolutePath());

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            strError = "Error: " + fileName + " size is more than 5MB. Please upload correct file.";
                            result = "error";
                            try {
                                json.put("result", result);
                                json.put("message", strError);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            //out.print(json);
                            out.print("{result:'" + result + "',message:'" + strError + "'}");
                            out.flush();
                            return;
                        }
                    } else {
                        result = "error";
                        message = "Error: No file present...";
                        try {
                            json.put("result", result);
                            json.put("message", message);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //out.print(json);
                        out.print(json);
                        out.flush();
                        return;
                    }
                }
            } catch (FileUploadException ex) {
                Logger.getLogger(registerlicense.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            result = "error";
            message = "Error: Form Post is invalid!!!";
            try {
                json.put("result", result);
                json.put("message", message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        String _templatefilePath = (String) request.getSession().getAttribute("_templatefilePath");
        if (_templatefilePath != null) {
            result = "success";
            message = "File Upload sucessfully";
            try {
                json.put("result", result);
                json.put("message", message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;

        } else {
            result = "error";
            message = "File uploading failed.";
            try {
                json.put("result", result);
                json.put("message", message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
