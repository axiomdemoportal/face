/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.ocr;

import com.mollatech.axiom.nucleus.db.ApTemplateJsonStructure;
import com.mollatech.axiom.nucleus.db.DocumentTemplate;
import com.mollatech.axiom.nucleus.db.connector.management.DocsTemplatesManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateTableToJsonManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class AddTableStructure extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        String templateName = request.getParameter("templatename");
        PrintWriter out = response.getWriter();
        
        JSONArray tableArray = new JSONArray();
        JSONObject reversejson = new JSONObject();
        String columnDetails[];
        try{
        com.mollatech.axiom.nucleus.db.Channels channel = (com.mollatech.axiom.nucleus.db.Channels) request.getSession().getAttribute("_apSChannelDetails");
        TemplateTableToJsonManagement tableObj = new TemplateTableToJsonManagement();
        ApTemplateJsonStructure getObj = tableObj.getDocumentDetailsByName(templateName, channel.getChannelid());
        LinkedHashMap<String, String> jsonOrderedMap = new LinkedHashMap<String, String>();
        if (getObj != null) {
            columnDetails = getObj.getTemplateColumn().split(",");
            for(int i = 0; i < columnDetails.length; i++){
                String rowdata = request.getParameter("row"+i);
                String columndata = request.getParameter("column"+i);
                if(rowdata == null){
                    rowdata = "";
                }
                jsonOrderedMap.put(columndata, rowdata);
                
            }
            JSONObject json = new JSONObject(jsonOrderedMap);
            tableArray.put(json);
            if(getObj.getTableJsonDetails() != null){
                JSONArray getArray = new JSONArray(getObj.getTableJsonDetails());
                for(int i = 0; i < getArray.length(); i++){
                    JSONObject objects = getArray.getJSONObject(i);
                    tableArray.put(objects);
                }
            }
            getObj.setTableJsonDetails(tableArray.toString());
            getObj.setUpdatedOn(new Date());
            int temp = tableObj.updateDocumentDetails(getObj);
            if(temp == 0){
                reversejson.put("_result", "success");
                reversejson.put("_message", "Row data added successfully..");
                out.print(reversejson);
                out.flush();
                return;
            }else{
                reversejson.put("_result", "error");
                reversejson.put("_message", "Failed to add row data..");
                out.print(reversejson);
                out.flush();
                return;
            }
        }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet
        (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            processRequest(request, response);
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost
        (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            processRequest(request, response);
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo
        
            () {
        return "Short description";
        }// </editor-fold>

    }
