/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.ocr;

/**
 *
 * @author bluebricks
 */
import java.awt.*;
import java.awt.image.BufferedImage;

public class ResizeImage {

//    public static void main(String... args) throws IOException {
//
//        File input = new File("/tmp/duke.png");
//        BufferedImage image = ImageIO.read(input);
//
//        BufferedImage resized = resize(image, 500, 500);
//
//        File output = new File("/tmp/duke-resized-500x500.png");
//        ImageIO.write(resized, "png", output);
//
//    }

    public static BufferedImage resize(BufferedImage img, int height, int width) {
        Image tmp = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = resized.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();
        return resized;
    }

}
