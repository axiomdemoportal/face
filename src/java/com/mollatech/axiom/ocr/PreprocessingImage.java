/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.ocr;

import com.mollatech.axiom.nucleus.db.DocumentTemplate;
import com.mollatech.axiom.nucleus.db.connector.management.DocsTemplatesManagement;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.channels.Channels;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import java.util.Date;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class PreprocessingImage extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        response.setContentType("application/json");
        
        com.mollatech.axiom.nucleus.db.Channels channel = (com.mollatech.axiom.nucleus.db.Channels) request.getSession().getAttribute("_apSChannelDetails");
        String templatename = request.getParameter("fileName");
        String tempName[] = templatename.split("\\.");
        String savepath = "";
        savepath = System.getProperty("catalina.home");
        if (savepath == null) {
            savepath = System.getenv("catalina.home");
        }
        try {
            savepath += System.getProperty("file.separator");
            savepath += "axiomv2-settings";
            savepath += System.getProperty("file.separator");
            savepath += "ocrpdfupload";
            savepath += System.getProperty("file.separator");
            savepath += "Converted_PdfFiles_to_Image";
            savepath += System.getProperty("file.separator");
            savepath += templatename + ".png";
            String Image = null;
            System.out.println(">>>>>>>>>>>> " + savepath);
            BufferedImage image = ImageIO.read(new File(savepath));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(image, "png", baos);
            byte[] res = baos.toByteArray();
            String encodedImage = new String( Base64.encode(baos.toByteArray()));
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("methodname", "preprocessimage");
            jsonObj.put("rawimage", encodedImage);
            jsonObj.put("docid", templatename);
            String url = "http://192.168.0.117:5000/todo/api/v1.0/tasks";
            RestCallingWrapper callService = new RestCallingWrapper();
            String result = null;
            result = callService.callMyService(jsonObj.toString(), url);
            JSONObject resultObj = new JSONObject(result);
            JSONObject resultString = (JSONObject) resultObj.get("task");
            Image = resultString.get("Result").toString();
            int responce = -1;
            if (Image != null && !Image.isEmpty()) {
                DocsTemplatesManagement docObj = new DocsTemplatesManagement();
                DocumentTemplate docsDetails  = new DocumentTemplate();
                docsDetails.setTemplateName(templatename+".pdf");
                docsDetails.setProcessImage(Image.getBytes());
                docsDetails.setCreatedOn(new Date());
                docsDetails.setChannelid(channel.getChannelid());
//                DocumentTemplate docsDetails = docObj.editDocumentDetails(templatename+".pdf", channel.getChannelid());
                DocsTemplatesManagement tempObj = new DocsTemplatesManagement();
//                responce = tempObj.addDocumentDetails(docsDetails);
            }
             String _result = null;
            System.out.println("===================================================");
            System.out.println("Image = " + Image);
            if(responce == 0){
                _result = "success";
            }
            json.put("_result", _result);
            json.put("Image", Image);
            json.put("_url", "./tutorial2.jsp?fileName="+templatename+".pdf");
            
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
