
package com.mollatech.axiom.ocr;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *
 * @author bluebricks
 */
public class RestCallingWrapper {

    public String callMyService(String dataToPost, String urlTo) {
        try {
            URL url = new URL(urlTo);
//            System.out.println("com.mollatech.axiom.v2.core.ekycFbio.RestCallingWrapper.callMyService()");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            OutputStream os = conn.getOutputStream();
            os.write(dataToPost.getBytes());
            os.flush();
         
            if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) { 
                
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode()); 
               
            } 
            
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            String output;
            String outputToreturn = "";
            while ((output = br.readLine()) != null) {
                outputToreturn = outputToreturn + output;
            }
            conn.disconnect();
            return outputToreturn;
        } catch (IOException | RuntimeException ex) {
            ex.printStackTrace();
            
            return "connection refused";
        }
    }

}