/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.ocr;

import com.mollatech.axiom.nucleus.db.DocumentTemplate;
import com.mollatech.axiom.nucleus.db.connector.management.DocsTemplatesManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
public class EditTemplate extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        int res = -1;
        try {
            String cordinateName = request.getParameter("_cordinateName");
            String templateName = request.getParameter("templatename");
            String x1 = request.getParameter("x1");
            String y1 = request.getParameter("y1");
            String x2 = request.getParameter("x2");
            String y2 = request.getParameter("y2");
            String w = request.getParameter("w");
            com.mollatech.axiom.nucleus.db.Channels channel = (com.mollatech.axiom.nucleus.db.Channels) request.getSession().getAttribute("_apSChannelDetails");
            DocsTemplatesManagement docObj = new DocsTemplatesManagement();
            DocumentTemplate docsDetails = docObj.editDocumentDetails(templateName, channel.getChannelid());
            if (docsDetails != null) {
                String keyvalue = docsDetails.getTemplateKeyValue();
                JSONObject jsonObjct = new JSONObject(keyvalue);
                int keyX1 = Integer.parseInt(jsonObjct.get("x1").toString());
                int keyY1 = Integer.parseInt(jsonObjct.get("y1").toString());
                int keyX2 = Integer.parseInt(jsonObjct.get("x2").toString());
                int keyY2 = Integer.parseInt(jsonObjct.get("y2").toString());
                int x1diff = Integer.parseInt(x1) - keyX1;
                int y1diff = Integer.parseInt(y1) - keyY1;
                int x2diff = Integer.parseInt(x2) - keyX2;
                int y2diff = Integer.parseInt(y2) - keyY2;

                if (docsDetails != null && docsDetails.getTemplateKeyValue() != null) {
                    docsDetails.getTemplateDetails();
                    JSONObject jsonObj = null;
                    JSONObject newJsonObj = null;
                    JSONObject main = new JSONObject();

                    jsonObj = new JSONObject(docsDetails.getTemplateDetails());
                    Iterator<?> keys = jsonObj.keys();
                    while (keys.hasNext()) {
                        String key = (String) keys.next();
                        System.out.println("object == " + key);
                        System.out.println("Shailendra == " + jsonObj.get(key));
                        main.put(key, jsonObj.get(key));
                        if (key.equals(cordinateName)) {
                            JSONObject jObj;
                            jObj = new JSONObject(jsonObj.get(key).toString());
                            jObj.put("x1", x1);
                            jObj.put("x2", x2);
                            jObj.put("y1", y1);
                            jObj.put("y2", y2);
                            jObj.put("w", w);
                            jObj.put("x1diff", x1diff);
                            jObj.put("y1diff", y1diff);
                            jObj.put("x2diff", x2diff);
                            jObj.put("y2diff", y2diff);
                            main.put(cordinateName, jObj);

                        }
                    }
                    docsDetails.setTemplateDetails(main.toString());
                    docsDetails.setUpdatedOn(new Date());
                    DocsTemplatesManagement tempObj = new DocsTemplatesManagement();
                    res = tempObj.updateDocumentDetails(docsDetails);
                }
                if (res == 0) {
                    json.put("_result", "success");
                    json.put("_message", "Template updated successful.");
                } else {
                    json.put("_result", "error");
                    json.put("_message", "Template not updated.");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
