/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.utils.logs;



/**
 *
 * @author Ideasventure
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipFiles {

    List<String> fileList;
    private static  String OUTPUT_ZIP_FILE = null; 
    private static  String SOURCE_FOLDER = null; 

  public  ZipFiles() {
        fileList = new ArrayList<String>();
    }

   
    public void zipIt(String zipFile) {
        byte[] buffer = new byte[1024];
        String source = "";
        try {
            try {
                source = SOURCE_FOLDER.substring(SOURCE_FOLDER.lastIndexOf("\\") + 1, SOURCE_FOLDER.length());
            } catch (Exception e) {
                source = SOURCE_FOLDER;
            }
            FileOutputStream fos = new FileOutputStream(zipFile);
            ZipOutputStream zos = new ZipOutputStream(fos);
            

           // System.out.println("Output to Zip : " + zipFile);

            for (String file : this.fileList) {

            //    System.out.println("File Added : " + file);
                ZipEntry ze = new ZipEntry(source + File.separator + file);
                zos.putNextEntry(ze);

                FileInputStream in =
                        new FileInputStream(SOURCE_FOLDER + File.separator + file);

                int len;
                while ((len = in.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }

                in.close();
            }

            zos.closeEntry();
            //remember close it
            zos.close();

         //   System.out.println("Folder successfully compressed");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void generateFileList(File node) {

        //add file only
        if (node.isFile()) {
            fileList.add(generateZipEntry(node.toString()));

        }

        if (node.isDirectory()) {
            String[] subNote = node.list();
            for (String filename : subNote) {
                generateFileList(new File(node, filename));
            }
        }

    }
     public String compresszip(String sourcepath,String destination) {
        SOURCE_FOLDER=sourcepath;
        OUTPUT_ZIP_FILE=destination;
       // ZipFiles appZip = new ZipFiles();
        generateFileList(new File(SOURCE_FOLDER));
        zipIt(OUTPUT_ZIP_FILE);
        return OUTPUT_ZIP_FILE;
    }

    private String generateZipEntry(String file) {
        return file.substring(SOURCE_FOLDER.length() + 1, file.length());
    }
    
    public static String zipFile(File inputFile, String zipFilePath) {
        try {

            // Wrap a FileOutputStream around a ZipOutputStream
            // to store the zip stream to a file. Note that this is
            // not absolutely necessary
            FileOutputStream fileOutputStream = new FileOutputStream(zipFilePath);
            ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream);

            // a ZipEntry represents a file entry in the zip archive
            // We name the ZipEntry after the original file's name
            ZipEntry zipEntry = new ZipEntry(inputFile.getName());
            zipOutputStream.putNextEntry(zipEntry);

            FileInputStream fileInputStream = new FileInputStream(inputFile);
            byte[] buf = new byte[1024];
            int bytesRead;

            // Read the input file by chucks of 1024 bytes
            // and write the read bytes to the zip stream
            while ((bytesRead = fileInputStream.read(buf)) > 0) {
                zipOutputStream.write(buf, 0, bytesRead);
            }

            // close ZipEntry to store the stream to the file
            zipOutputStream.closeEntry();

            zipOutputStream.close();
            fileOutputStream.close();

//            System.out.println("Regular file :" + inputFile.getCanonicalPath()+" is zipped to archive :"+zipFilePath);
          return zipFilePath;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }
}