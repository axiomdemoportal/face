/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.utils.logs;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class downloadlogs extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(downloadlogs.class.getName());
    private static final int BUFSIZE = 4096;
    
     private void addToZipFile(String fileName, ZipOutputStream zos) throws FileNotFoundException, IOException {
        
        //System.out.println("Writing '" + fileName + "' to zip file");

        File file = new File(fileName);
        FileInputStream fis = new FileInputStream(file);
//        ZipEntry zipEntry = new ZipEntry(fileName);  taking full folder path in zip file
        ZipEntry zipEntry = new ZipEntry(file.getName());  // only taking files
        zos.putNextEntry(zipEntry);

        byte[] bytes = new byte[2048];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zos.write(bytes, 0, length);
        }

        zos.closeEntry();
        fis.close();
    }

     protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                  log.info("Servlet started");
        try {
//            boolean currentdate = false, catalinafile = false, logfile = false;
//            String date = request.getParameter("_date");
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//            Date current = new Date();
//            String dateCurrent = sdf.format(current);
//            if (date.equals(dateCurrent)) {
//                currentdate = true;
//            }
//            String[] d = date.split("-");
//            String sep = System.getProperty("file.separator");
//            String usrhome = System.getProperty("catalina.home");
//            if (usrhome == null) {
//                usrhome = System.getenv("catalina.home");
//            }
//            
//            String day = d[2];
//            String month = d[1];
//            String year = d[0];
//            
//            
//            
//            String filepath = usrhome + sep + "axiomv2-logs" + sep + "AxiomProtect." + year + "-" + month + "-" + day + "_" + current.getTime() + ".zip";
//            FileOutputStream fos = new FileOutputStream(filepath);
//            ZipOutputStream zos = new ZipOutputStream(fos);
//            String FACELogFile = null;
//            String CORELogFile = null;
//            String CatalinaLogFile = usrhome + sep + "logs" + sep + "catalina." + year + "-" + month + "-" + day + ".log";
//            String LocalhostLogFile = usrhome + sep + "logs" + sep + "localhost." + year + "-" + month + "-" + day + ".log";
//            String localhost_access_logLogFile = usrhome + sep + "logs" + sep + "localhost_access_log." + year + "-" + month + "-" + day + ".txt";
//            String managerLogFile = usrhome + sep + "logs" + sep + "manager." + year + "-" + month + "-" + day + ".log";
//            
//            //System.out.println("CatalinaLogFile :" + usrhome + sep + "logs" + sep + "catalina." + year + "-" + month + "-" + day + ".log");
//            if (currentdate == false) {
//                FACELogFile = usrhome + sep + "axiomv2-logs" + sep + "face.log." + year + "-" + month + "-" + day;
//                CORELogFile = usrhome + sep + "axiomv2-logs" + sep + "core.log." + year + "-" + month + "-" + day;                
//            } else {
//                FACELogFile = usrhome + sep + "axiomv2-logs" + sep + "face.log";
//                CORELogFile = usrhome + sep + "axiomv2-logs" + sep + "core.log";
//                
//                //System.out.println("AxiomLogFile :" + usrhome + sep + "axiomv2-logs" + sep + "logs.log");
//            }
//            File f = new File(FACELogFile);
//            if (f.exists() && !f.isDirectory()) {
//                addToZipFile(FACELogFile, zos);
//            }
//            
//            f = new File(CORELogFile);
//            if (f.exists() && !f.isDirectory()) {
//                addToZipFile(CORELogFile, zos);
//            }
//            
//            f = new File(CatalinaLogFile);
//            if (f.exists() && !f.isDirectory()) {
//                addToZipFile(CatalinaLogFile, zos);                
//            }
//            
//            f = new File(LocalhostLogFile);
//            if (f.exists() && !f.isDirectory()) {
//                addToZipFile(LocalhostLogFile, zos);
//                
//            }
//            
//            f = new File(localhost_access_logLogFile);
//            if (f.exists() && !f.isDirectory()) {
//                addToZipFile(localhost_access_logLogFile, zos);
//                
//            }
//            
//            f = new File(managerLogFile);
//            if (f.exists() && !f.isDirectory()) {
//                addToZipFile(managerLogFile, zos);
//                
//            }
//            
//            zos.close();            
//            fos.close();
            
            String filepath = (String) request.getSession().getAttribute("_apDownloadLogsZipFilePath"); 
            log.debug("filepath ::" + filepath);
            File file = new File(filepath);

            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(filepath);

            // sets response content type
            if (mimetype == null) {
                mimetype = "application/octet-stream";
            }
            response.setContentType(mimetype);
            response.setContentLength((int) file.length());
            String fileName = (new File(filepath)).getName();

            // sets HTTP header
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(file));

            // reads the file's bytes and writes them to the response stream
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }
            in.close();
            outStream.close();
            file.delete();
        } catch (FileNotFoundException e) {
            log.error("Exception caught :: ",e);
        } catch (IOException e) {
            log.error("Exception caught :: ",e);
        }
        log.info("Servlet ended");
    }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.mollatech.utils.logs;
//
//import com.mollatech.axiom.common.utils.ZipFiles;
//import com.mollatech.axiom.nucleus.crypto.LoadSettings;
//
//import com.mollatech.axiom.v2.face.handler.operator.getopraudits;
//import java.io.DataInputStream;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.util.Date;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.servlet.ServletContext;
//import javax.servlet.ServletException;
//import javax.servlet.ServletOutputStream;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
///**
// *
// * @author mollatech1
// */
//public class downloadlogs extends HttpServlet {
//
//    private static final int BUFSIZE = 4096;
//
//    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//
//        String f = LoadSettings.g_strPath;
//        String sep = System.getProperty("file.separator");
//        f += ".." + sep + "logs";
//        String f1 = LoadSettings.g_strPath;
//        Date d = new Date();
//        f1 += "temp" + sep + d.getTime() + ".zip";
//        //System.out.println("log folder " + f);
//        //System.out.println("temp folder " + f1);
//        
//        try {
//            try {
//
//                String filepath = null;
//                ZipFiles zip = new ZipFiles();
//                filepath = zip.compresszip(f, f1);
//
//                File file = new File(filepath);
//                int length = 0;
//                ServletOutputStream outStream = response.getOutputStream();
//                ServletContext context = getServletConfig().getServletContext();
//                String mimetype = context.getMimeType(filepath);
//
//                // sets response content type
//                if (mimetype == null) {
//                    // mimetype = "application/octet-stream";
//                    mimetype = "application/zip";
//                }
//                response.setContentType(mimetype);
//                response.setContentLength((int) file.length());
//                String fileName = (new File(filepath)).getName();
//
//                // sets HTTP header
//                response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
//
//                byte[] byteBuffer = new byte[BUFSIZE];
//                DataInputStream in = new DataInputStream(new FileInputStream(file));
//
//                // reads the file's bytes and writes them to the response stream
//                while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
//                    outStream.write(byteBuffer, 0, length);
//                }
//
//                in.close();
//                outStream.close();
//                file.delete();
//
//            } catch (Exception ex) {
//                // TODO handle custom exceptions here
//                log.error("Exception caught :: ",ex);
//            }
//
//
//        } catch (Exception ex) {
//            Logger.getLogger(getopraudits.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            //  out.close();
//        }
//    }
//
//    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
//    /**
//     * Handles the HTTP
//     * <code>GET</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /**
//     * Handles the HTTP
//     * <code>POST</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /**
//     * Returns a short description of the servlet.
//     *
//     * @return a String containing servlet description
//     */
//    @Override
//    public String getServletInfo() {
//        return "Short description";
//    }// </editor-fold>
//}
