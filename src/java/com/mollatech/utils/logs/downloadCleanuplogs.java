/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.utils.logs;

import com.mollatech.axiom.nucleus.crypto.LoadSettings;

import com.mollatech.axiom.v2.face.handler.operator.getopraudits;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mollatech1
 */
public class downloadCleanuplogs extends HttpServlet {

     static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(downloadCleanuplogs.class.getName());
    private static final int BUFSIZE = 4096;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
       String f=LoadSettings.g_sSettings.getProperty("cleanup.log");
        try {
            try {
                File file = new File(f);
                int length = 0;
                ServletOutputStream outStream = response.getOutputStream();
                ServletContext context = getServletConfig().getServletContext();
                String mimetype = context.getMimeType(f);

                // sets response content type
                if (mimetype == null) {
                   mimetype = "application/zip";
                }
                response.setContentType(mimetype);
                response.setContentLength((int) file.length());
                String fileName = "cleanup.log"; 

                // sets HTTP header
                response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

                byte[] byteBuffer = new byte[BUFSIZE];
                DataInputStream in = new DataInputStream(new FileInputStream(file));

                // reads the file's bytes and writes them to the response stream
                while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                    outStream.write(byteBuffer, 0, length);
                }

                in.close();
                outStream.close();
//                file.delete();

            } catch (Exception ex) {
                // TODO handle custom exceptions here
                log.error("Exception caught :: ",ex);
            }


        } catch (Exception ex) {
            log.error("Exception caught::" + ex);
        } finally {
            //  out.close();
            log.info("Servlet ended");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
