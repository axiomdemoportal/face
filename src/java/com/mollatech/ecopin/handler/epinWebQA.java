/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.ecopin.handler;

import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Epinsessions;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;

import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.PINDeliverySetting;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import com.mollatech.ecopin.management.EPINManagement;
import com.mollatech.ecopin.management.EPINThread;
import com.mollatech.axiom.connector.epin.QuestionsAndAnswers;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class epinWebQA extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(epinSMS.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final int START = 0;
    final int WAITING = 1;
    final int STOP = 2;
    final int RUNNING = 1;
    final int EXPIRED = -1;
    final int USERID = 1;
    final int PHONENUMBER = 2;
    final int EMAILID = 3;
    final int SMS = 1;
    final int USSD = 2;
    final int VOICE = 3;
    final int EMAIL = 4;
    final int SUCCESS = 0;
    final int FAILED = -1;
    final int EPIN = 9;
    int iType;
    final int VALIDATION = 5;
    final int FAILEDTOVALIDATE = -4;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        log.info("Servlet started");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "EPIN sent sucessfully!!!";

        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        log.debug("epinSMS::channel::"+channel);
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        log.debug("epinSMS::sessionId::"+sessionId);
        AuthUser aUser = (AuthUser) request.getSession().getAttribute("_apUserObj");
        log.debug("epinSMS::aUser::"+aUser);
        // QuestionsAndAnswers QandAObj =(QuestionsAndAnswers)request.getSession().getAttribute("_apQuesAndAns");
        String eSessionId = (String) request.getSession().getAttribute("_apEPINSessionId");
        log.debug("epinSMS::eSessionId::"+eSessionId);
        SettingsManagement setManagement = new SettingsManagement();
        EPINManagement eManagement = new EPINManagement();
        int countOfCorrectAns = 0;

        Epinsessions eSessions = eManagement.getEpinSessionBySessionId(eSessionId);
        //Epinsessions eSessions = eManagement.getValidateSessions(aUser.userId, channel.getChannelid());

        QuestionsAndAnswers QandAObj = null;
        //  QuestionAndAnswer[] queArray = eManagement.GetValidationQuestionsAndAnswers(sessionId, channel.getChannelid(), aUser.userId);
        //QuestionsAndAnswers QandAObj = new QuestionsAndAnswers(queArray);
        if (eSessions != null) {
            QandAObj = eManagement.getQuestionsAndAnswer(eSessions.getEpinsessionid());
        }

        PINDeliverySetting ePin = (PINDeliverySetting) setManagement.getSetting(sessionId, channel.getChannelid(), EPIN, 1);
        int retValue = 0;

        try {
            if (QandAObj != null) {
                for (int i = 0; i < QandAObj.questionsAndAnswerses.length; i++) {
                    QandAObj.questionsAndAnswerses[i].answerbyUser = request.getParameter("answers" + i);
                    QandAObj.questionsAndAnswerses[i].iCurrent = i + 1;

                    //if (QandAObj.questionsAndAnswerses[i].answerbyUser == null || _userid == null) {
                    if (QandAObj.questionsAndAnswerses[i].answerbyUser == null) {
                        result = "error";
                        message = "Invalid Parameters!!!";
                        json.put("_result", result);
                        json.put("_message", message);
//                        out.print(json);
//                        out.flush();
                        return;
                    }
                }
                eManagement.updateEPINSession(eSessionId, aUser.userId, channel.getChannelid(), aUser.phoneNo, aUser.email, QandAObj, WAITING);

            } else {
                result = "error";
                message = "Epin Time has expired!!!";
                json.put("_result", result);
                json.put("_message", message);
//                out.print(json);
//                out.flush();
                return;
            }

            QandAObj = eManagement.getQuestionsAndAnswer(eSessions.getEpinsessionid());
            if (QandAObj == null) {
                eManagement.updateEPINSession(eSessionId, aUser.userId, channel.getChannelid(), aUser.phoneNo, aUser.email, QandAObj, EXPIRED);
                result = "error";
                message = "Epin Time has expired!!!";
                json.put("_result", result);
                json.put("_message", message);
//                out.print(json);
//                out.flush();
                return;
            } else {
                for (int i = 0; i < QandAObj.questionsAndAnswerses.length; i++) {
                    if (QandAObj.questionsAndAnswerses[i].bValidateAtAxiom == true) {
                        if (QandAObj.questionsAndAnswerses[i].bEnforceCaseSensitive == true) {
                            if (QandAObj.questionsAndAnswerses[i].answerToValidate.equals(QandAObj.questionsAndAnswerses[i].answerbyUser)) {
                                QandAObj.questionsAndAnswerses[i].bValidationResult = true;
                                countOfCorrectAns++;
                            }
                        } else {
                            if (QandAObj.questionsAndAnswerses[i].answerToValidate.equalsIgnoreCase(QandAObj.questionsAndAnswerses[i].answerbyUser)) {
                                QandAObj.questionsAndAnswerses[i].bValidationResult = true;
                                countOfCorrectAns++;
                            }
                        }
                    } else {
                        countOfCorrectAns = eManagement.ValidateUserDetails(sessionId, channel.getChannelid(), aUser.getUserId(), QandAObj.questionsAndAnswerses);
                        break;
                    }
                }
            }
            // QandAObj = eManagement.getQuestionsAndAnswer(eSessions.getEpinsessionid());
            if (QandAObj != null) {
                if (countOfCorrectAns == QandAObj.questionsAndAnswerses.length) {

                    if (ePin.operatorController == EPINManagement.OPERATOR_CONTROLLED || ePin.operatorController == EPINManagement.DUAL_OPERATOR_CONTROLLED) {
                        OperatorsManagement oManagement = new OperatorsManagement();
                        Operators[] oper = oManagement.getAdminOperator(channel.getChannelid());
                        TemplateManagement tManagement = new TemplateManagement();
                        Templates templates = tManagement.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.EMAIL_ECOPIN_OPERATOR_PIN_DELIVERY_REQUEST_TEMPLATE);
                       if(templates.getStatus() == tManagement.ACTIVE_STATUS){
                        ByteArrayInputStream bais = new ByteArrayInputStream(templates.getTemplatebody());
                        String tmessage = (String) TemplateUtils.deserializeFromObject(bais);
                        String subject = templates.getSubject();
                        SendNotification send = new SendNotification();
                        String[] emailList = new String[oper.length - 1];

                        for (int i = 1; i < oper.length; i++) {
                            emailList[i-1] = oper[i].getEmailid();
                        }
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                        if (message != null) {
                            Date d = new Date();
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(d);
                            cal.add(Calendar.MINUTE, ePin.expiryTime);
                            Date exipryDate = cal.getTime();
                            tmessage = tmessage.replaceAll("#name#", oper[0].getName());
                            tmessage = tmessage.replaceAll("#channel#", channel.getName());
                            tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                            tmessage = tmessage.replaceAll("#email#", oper[0].getEmailid());
                            tmessage = tmessage.replaceAll("#username#", aUser.userName);
                            tmessage = tmessage.replaceAll("#expiry#", sdf.format(exipryDate));
                        }

                        if (subject != null) {
                            Date d = new Date();
                            subject = subject.replaceAll("#channel#", channel.getName());
                            subject = subject.replaceAll("#datetime#", sdf.format(d));
                        }
                        send.SendEmail(channel.getChannelid(), oper[0].getEmailid(), subject, tmessage, emailList, null, null, null, 3);
                       }
                        if (ePin.operatorController == EPINManagement.OPERATOR_CONTROLLED) {
                            eManagement.isOperatorControlled(eSessions.getUserid(), channel.getChannelid(), eSessions.getPhone(), eSessions.getEmailid(), new Date(), ePin);
                        } else if (ePin.operatorController == EPINManagement.DUAL_OPERATOR_CONTROLLED) {
                            eManagement.isDualOperatorControlled(eSessions.getUserid(), channel.getChannelid(), eSessions.getPhone(), eSessions.getEmailid(), new Date(), ePin);
                        }
                        json.put("_result", "success");
                        json.put("_message", "Your EPIN is secure so it'll deliver by operator only,Thank you!");

                        QandAObj = null;
                        eManagement.updateEPINSession(eSessionId, aUser.userId, channel.getChannelid(), aUser.phoneNo, aUser.email, QandAObj, STOP);
                        return;

                    } else {

                        String strEPin = eManagement.GeneratePIN(channel.getChannelid(),ePin, aUser.userId);
                        if (strEPin != null) {
                            EPINThread eThread = new EPINThread(sessionId, channel.getChannelid(), ePin, aUser.phoneNo, aUser.email, strEPin, aUser.userId, null, null, new Date());
                            Thread t = new Thread(eThread);
                            // EPINThread eThread = new EPINThread(channel.getChannelid(), ePin, "+919049105669", aUser.email, "Your PIN is", strEPin, aUser.userId, null, new Date());
                            //eThread.start();
                            t.start();
                            QandAObj = null;
                            json.put("_result", result);
                            json.put("_message", message);
                            eManagement.updateEPINSession(eSessionId, aUser.userId, channel.getChannelid(), aUser.phoneNo, aUser.email, QandAObj, STOP);
                            return;
                        }
                    }

                } else {

                    eManagement.updateEPINSession(eSessionId, aUser.userId, channel.getChannelid(), aUser.phoneNo, aUser.email, QandAObj, STOP);
                    if (QandAObj.questionsAndAnswerses[QandAObj.questionsAndAnswerses.length - 1].answerbyUser != null) {
                        if (ePin.operatorController == EPINManagement.OPERATOR_CONTROLLED) {
                            eManagement.AddEPINTracker(channel.getChannelid(), null, null, EPINManagement.SINGLE_OPERATOR_CONTROLLED, eSessions.getUserid(), eSessions.getPhone(), eSessions.getEmailid(), VALIDATION, FAILEDTOVALIDATE, new Date(), null, ePin.expiryTime, null);
                        } else if (ePin.operatorController == EPINManagement.DUAL_OPERATOR_CONTROLLED) {
                            eManagement.AddEPINTracker(channel.getChannelid(), null, null, EPINManagement.PENDING_FOR_OPERATORS, eSessions.getUserid(), eSessions.getPhone(), eSessions.getEmailid(), VALIDATION, FAILEDTOVALIDATE, new Date(), null, ePin.expiryTime, null);
                        } else {
                            eManagement.AddEPINTracker(channel.getChannelid(), null, null, EPINManagement.NO_OPERATOR_CONTROLLED, eSessions.getUserid(), eSessions.getPhone(), eSessions.getEmailid(), VALIDATION, FAILEDTOVALIDATE, new Date(), null, ePin.expiryTime, null);
                        }

                    }
                    json.put("_result", "error");
                    json.put("_message", "Sorry your response are incorrect!!!");
//                    out.print(json);
//                    out.flush();
                    QandAObj = null;
                    return;
                }

            }

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
            
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
        }
    }
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
