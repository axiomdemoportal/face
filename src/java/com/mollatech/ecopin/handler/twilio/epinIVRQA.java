/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.ecopin.handler.twilio;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Epinsessions;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;

import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.PINDeliverySetting;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import com.mollatech.ecopin.management.EPINManagement;
import com.mollatech.ecopin.management.EPINThread;
import com.mollatech.axiom.connector.epin.QuestionsAndAnswers;
import com.twilio.sdk.verbs.Gather;
import com.twilio.sdk.verbs.Say;
import com.twilio.sdk.verbs.TwiMLResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;

/**
 *
 * @author mollatech2
 */
public class epinIVRQA extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(epinIVRQA.class.getName());
    final int USERID = 1;
    final int PHONENUMBER = 2;
    final int EMAILID = 3;
    final int SMS = 1;
    final int USSD = 2;
    final int VOICE = 3;
    final int EMAIL = 4;
    final int SUCCESS = 0;
    final int FAILED = -1;
    final int EPIN = 9;
    int iType;
    final int START = 0;
    final int WAITING = 1;
    final int STOP = 2;
    final int RUNNING = 1;
    final int EXPIRED = -1;
    final int VALIDATION = 5;
    final int FAILEDTOVALIDATE = -4;
    
    

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        String eSessionId = request.getParameter("eSessionId");
        log.debug("epinIVRQA::eSessionId::"+eSessionId);
         String sessionId = request.getParameter("sessionid");
        log.debug("epinIVRQA::sessionId::"+sessionId);
        
        String _channelName = this.getServletContext().getContextPath();
        _channelName = _channelName.replaceAll("/", "");

        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);

        Channels channel = cUtil.getChannel(_channelName);
        log.debug("epinIVRQA::Channel ::"+channel);
        if (channel == null) {
            sChannel.close();
            suChannel.close();
            return;
        }

        //digits = digits.trim();

        //SessionManagement sManagement = null;
        
        sChannel.close();
        suChannel.close();
        //end of addition

        
        //SessionManagement sManagement = new SessionManagement();
        String channelId = channel.getChannelid();
        
        //String channelId = "b6BK5gdwIWqSue/8VSJOuuIiFNc=";
        SettingsManagement setManagement = new SettingsManagement();

        EPINManagement eManagement = new EPINManagement();
        String digits = request.getParameter("Digits");
        log.debug("epinIVRQA::digits::"+digits);
        String from = "+" + request.getParameter("From");
        log.debug("epinIVRQA::from::"+from);

        TwiMLResponse twiml = new TwiMLResponse();
        QuestionsAndAnswers QandAObj = null;
        AXIOMStatus status = null;
        String message = null;
        Say say = null;
        PINDeliverySetting ePin = null;
        int countOfCorrectAns = 0;
        Gather gather = new Gather();
        
        //SendNotification send = new SendNotification();
        //  Epinsessions eSessions = eManagement.getEpinSessionsByType(from, channelId, PHONENUMBER);
        Epinsessions eSessions = eManagement.getEpinSessionBySessionId(eSessionId);
        
        //Epinsessions eSessions = eManagement.getEpinSessionsByType(from, channelId, PHONENUMBER);
        if (eSessions.getSessionId() != null) {
            ePin = (PINDeliverySetting) setManagement.getSetting(eSessions.getSessionId(), channelId, EPIN, 1);
        }

        if (digits != null && digits.equals("1")) {
            //   Epinsessions eSessions = eManagement.getValidateSessions(aUser.userId, channel.getChannelid());
            //  Epinsessions eSessions = eManagement.getValidateSessions(aUser.userId, channelId);
            if (eSessions != null) {
                QandAObj = eManagement.getQuestionsAndAnswer(eSessions.getEpinsessionid());
                if (QandAObj == null) {
                    // eManagement.updateEPINSession(aUser.userId, channel.getChannelid(), aUser.phoneNo, aUser.email, QandAObj);
                    eManagement.updateEPINSession(eSessionId,eSessions.getUserid(), channelId, eSessions.getPhone(), eSessions.getEmailid(), QandAObj, EXPIRED);
                } else {
                    //gather.setAction("/epinque");
                    gather.setAction("/epinIVRQA?eSessionId="+eSessionId);                          
                        
                    gather.setNumDigits(4);
                    gather.setMethod("POST");
                    say = new Say(QandAObj.questionsAndAnswerses[0].question);
                    //    if (status.iStatus == 0 && status.strStatus.equals("completed")) {
                    QandAObj.questionsAndAnswerses[0].iCurrent = 1;
                    // eManagement.updateEPINSession(aUser.userId, channel.getChannelid(), aUser.phoneNo, aUser.email, QandAObj);
                    eManagement.updateEPINSession(eSessionId,eSessions.getUserid(), channelId, eSessions.getPhone(), eSessions.getEmailid(), QandAObj, WAITING);
                    // }

                    try {
                        gather.append(say);
                        twiml.append(gather);
                    } catch (Exception e) {
                        log.error("Exception caught :: ",e);
                    }

                }

            }
        } else if (digits != null && digits.length() == 4) {
            //Epinsessions eSessions = eManagement.getValidateSessions(aUser.userId, channel.getChannelid());
//            Epinsessions eSessions = eManagement.getValidateSessions(aUser.userId, channelId);
            if (eSessions != null) {
                QandAObj = eManagement.getQuestionsAndAnswer(eSessions.getEpinsessionid());
                if (QandAObj == null) {
                    //  eManagement.updateEPINSession(aUser.userId, channel.getChannelid(), aUser.phoneNo, aUser.email, QandAObj);
                    eManagement.updateEPINSession(eSessionId,eSessions.getUserid(), channelId, eSessions.getPhone(), eSessions.getEmailid(), QandAObj, EXPIRED);
                } else {
                    for (int i = 0; i < QandAObj.questionsAndAnswerses.length; i++) {
                        if (QandAObj.questionsAndAnswerses[i].answerbyUser == null) {
                            QandAObj.questionsAndAnswerses[i].answerbyUser = digits;

                            if (i < QandAObj.questionsAndAnswerses.length - 1) {
                                //   if (status.iStatus == 0 && status.strStatus.equals("completed")) {
                                gather.setAction("/epinIVRQA?eSessionId="+eSessionId);                          
                                gather.setNumDigits(4);
                                gather.setMethod("POST");
                                say = new Say(QandAObj.questionsAndAnswerses[i + 1].question);
                                QandAObj.questionsAndAnswerses[i + 1].iCurrent = QandAObj.questionsAndAnswerses[i].iCurrent + 1;
                                   try {
                                    gather.append(say);
                                    twiml.append(gather);
                                } catch (Exception e) {
                                    log.error("Exception caught :: ",e);
                                }
                             
                            }

                            if (QandAObj.questionsAndAnswerses[i].answerbyUser != null) {
                                // eManagement.updateEPINSession(aUser.userId, channel.getChannelid(), aUser.phoneNo, aUser.email, QandAObj);
                                eManagement.updateEPINSession(eSessionId,eSessions.getUserid(), channelId, eSessions.getPhone(), eSessions.getEmailid(), QandAObj, WAITING);
                                break;
                            }
                            //   }

                        }

                    }



                }

            }
        } else if (digits != null && digits.equals("0")) {
            response.sendRedirect("/epinIVR");
        } else {
            say = new Say("You have pressed invalid key.");
            try {
                twiml.append(say);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
        }
        
        
        QandAObj = eManagement.getQuestionsAndAnswer(eSessions.getEpinsessionid());
        if (QandAObj == null) {
            eManagement.updateEPINSession(eSessionId,eSessions.getUserid(), channelId, eSessions.getPhone(), eSessions.getEmailid(), QandAObj, EXPIRED);
        } else {
            for (int i = 0; i < QandAObj.questionsAndAnswerses.length; i++) {
                if (QandAObj.questionsAndAnswerses[i].bValidateAtAxiom == true) {
                    if (QandAObj.questionsAndAnswerses[i].bEnforceCaseSensitive == true) {
                        if (QandAObj.questionsAndAnswerses[i].answerToValidate.equals(QandAObj.questionsAndAnswerses[i].answerbyUser)) {
                            QandAObj.questionsAndAnswerses[i].bValidationResult = true;
                            countOfCorrectAns++;
                        }
                    } else {
                        if (QandAObj.questionsAndAnswerses[i].answerToValidate.equalsIgnoreCase(QandAObj.questionsAndAnswerses[i].answerbyUser)) {
                            QandAObj.questionsAndAnswerses[i].bValidationResult = true;
                            countOfCorrectAns++;
                        }
                    }
                } else {
//                        countOfCorrectAns = eManagement.ValidateUserDetails(sessionId, channelId, aUser.getUserId(), QandAObj.questionsAndAnswerses);
                    countOfCorrectAns = eManagement.ValidateUserDetails(eSessions.getSessionId(), channelId, eSessions.getUserid(), QandAObj.questionsAndAnswerses);
                    break;
                }
            }
        }
        // QandAObj = eManagement.getQuestionsAndAnswer(eSessions.getEpinsessionid());
        if (QandAObj != null) {
            if (countOfCorrectAns == QandAObj.questionsAndAnswerses.length) {

                if (ePin.operatorController == EPINManagement.OPERATOR_CONTROLLED || ePin.operatorController == EPINManagement.DUAL_OPERATOR_CONTROLLED) {
                    OperatorsManagement oManagement = new OperatorsManagement();
                    UserManagement uManagement = new UserManagement();
                    AuthUser aUser = uManagement.CheckUserByType(sessionId, channelId, from, PHONENUMBER);
                        Operators[] oper = oManagement.getAdminOperator(channelId);
                        TemplateManagement tManagement = new TemplateManagement();
                        Templates templates = tManagement.LoadbyName(sessionId, channelId, TemplateNames.EMAIL_ECOPIN_OPERATOR_PIN_DELIVERY_FAILED_TEMPLATE);
                   if(templates.getStatus() == tManagement.ACTIVE_STATUS){
                        ByteArrayInputStream bais = new ByteArrayInputStream(templates.getTemplatebody());
                        String tmessage = (String) TemplateUtils.deserializeFromObject(bais);
                        String subject = templates.getSubject();
                        SendNotification send = new SendNotification();
                          String[] emailList = new String[oper.length - 1];

                        for (int i = 1; i < oper.length; i++) {
                            emailList[i] = oper[i].getEmailid();
                        }
                                SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                                if (message != null) {
                                    Date d = new Date();
                                    Calendar cal = Calendar.getInstance();
                                    cal.setTime(d);
                                    cal.add(Calendar.MINUTE, ePin.expiryTime);
                                    Date exipryDate = cal.getTime();
                                    tmessage = tmessage.replaceAll("#name#", oper[0].getName());
                                    tmessage = tmessage.replaceAll("#channel#", channel.getName());
                                    tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                                    tmessage = tmessage.replaceAll("#email#", oper[0].getEmailid());
                                    tmessage = tmessage.replaceAll("#username#", aUser.userName);
                                    tmessage = tmessage.replaceAll("#expiry#", sdf.format(exipryDate));
                                }

                                if (subject != null) {
                                    Date d = new Date();
                                    subject = subject.replaceAll("#channel#", channel.getName());
                                    subject = subject.replaceAll("#datetime#", sdf.format(d));
                                }
                                
                                send.SendEmail(channelId, oper[0].getEmailid(), subject, tmessage, emailList, null, null, null, 3);
                            
                   }
                    if(ePin.operatorController == EPINManagement.OPERATOR_CONTROLLED){
                        eManagement.isOperatorControlled(eSessions.getUserid(), channelId, eSessions.getPhone(), eSessions.getEmailid(), new Date(), ePin);
                        }else if(ePin.operatorController == EPINManagement.DUAL_OPERATOR_CONTROLLED){
                             eManagement.isOperatorControlled(eSessions.getUserid(), channelId, eSessions.getPhone(), eSessions.getEmailid(), new Date(), ePin);
                        }
                    message = "Your pin is securely generated so it will get approved by operator.Thank you!";
                    say = new Say(message);
                    QandAObj = null;
                    eManagement.updateEPINSession(eSessionId,eSessions.getUserid(), channelId, eSessions.getPhone(), eSessions.getEmailid(), QandAObj, STOP);

                } else {

                    String strEPin = eManagement.GeneratePIN(channel.getChannelid(),ePin, eSessions.getUserid());
                    if (strEPin != null) {
                        //EPINThread eThread = new EPINThread(channelId, ePin, aUser.getPhone(), aUser.getEmail(), "Your PIN is ",strEPin);
                        EPINThread eThread = new EPINThread(sessionId,channelId, ePin, eSessions.getPhone(), eSessions.getEmailid(), strEPin, eSessions.getUserid(), null,null, new Date());
                        //eThread.run();
                        Thread t = new Thread(eThread);
                        t.start();
                        say = new Say("Your pin will be delivered shortly. Thank you for using our service.");
                        //say = new Say(LoadTemplateForSetting("ecopin.question.start"));
                        QandAObj = null;
                        eManagement.updateEPINSession(eSessionId,eSessions.getUserid(), channelId, eSessions.getPhone(), eSessions.getEmailid(), QandAObj, STOP);
                    }
                }

                try {

                    twiml.append(say);
                } catch (Exception e) {
                    log.error("Exception caught :: ",e);
                  
                }


            }else{
                  eManagement.updateEPINSession(eSessionId, eSessions.getUserid(), channel.getChannelid(), eSessions.getPhone(), eSessions.getEmailid(), QandAObj, STOP);
                if(QandAObj.questionsAndAnswerses[QandAObj.questionsAndAnswerses.length -1].answerbyUser != null){
                    if(ePin.operatorController == EPINManagement.OPERATOR_CONTROLLED){
                              eManagement.AddEPINTracker(channel.getChannelid(), null,null,EPINManagement.SINGLE_OPERATOR_CONTROLLED, eSessions.getUserid(), eSessions.getPhone(), eSessions.getEmailid(), VALIDATION, FAILEDTOVALIDATE, new Date(), null, ePin.expiryTime,null);
                         }else if(ePin.operatorController == EPINManagement.DUAL_OPERATOR_CONTROLLED){
                              eManagement.AddEPINTracker(channel.getChannelid(), null,null,EPINManagement.PENDING_FOR_OPERATORS, eSessions.getUserid(), eSessions.getPhone(), eSessions.getEmailid(), VALIDATION, FAILEDTOVALIDATE, new Date(), null, ePin.expiryTime,null);
                         }else{
                              eManagement.AddEPINTracker(channel.getChannelid(), null,null,EPINManagement.NO_OPERATOR_CONTROLLED, eSessions.getUserid(), eSessions.getPhone(), eSessions.getEmailid(), VALIDATION, FAILEDTOVALIDATE, new Date(), null, ePin.expiryTime,null);
                         }
                }
            }

        }



        response.setContentType("application/xml");
        response.getWriter().print(twiml.toXML());
        log.info("Servlet ended");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
