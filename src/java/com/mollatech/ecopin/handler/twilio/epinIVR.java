/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.ecopin.handler.twilio;

import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.ecopin.management.EPINManagement;
import com.twilio.sdk.verbs.Gather;
import com.twilio.sdk.verbs.Say;
import com.twilio.sdk.verbs.TwiMLResponse;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
public class epinIVR extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(epinIVR.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        TwiMLResponse twiml = new TwiMLResponse();

        String smsid = request.getParameter("SmsSid");
        log.debug("epinIVR::smsid::"+smsid);
        String accountSid = request.getParameter("AccountSid");
         log.debug("epinIVR::accountSid::"+accountSid);
        String from = "+" + request.getParameter("From");
        log.debug("epinIVR::from::"+from);
        String to = request.getParameter("To");
        log.debug("epinIVR::to::"+to);
        String body = request.getParameter("Body");
        log.debug("epinIVR::body::"+body);
        String fromCity = request.getParameter("FromCity");
        log.debug("epinIVR::fromCity::"+fromCity);
        String fromState = request.getParameter("FromState");
        log.debug("epinIVR::fromState::"+fromState);
        String fromZip = request.getParameter("FromZip");
        log.debug("epinIVR::fromZip::"+fromZip);
        String FromCountry = request.getParameter("FromCountry");
        log.debug("epinIVR::FromCountry::"+FromCountry);
        String toCity = request.getParameter("ToCity");
        log.debug("epinIVR::toCity::"+toCity);
        String toState = request.getParameter("ToState");
        log.debug("epinIVR::toState::"+toState);
        String toZip = request.getParameter("ToZip");
        log.debug("epinIVR::toZip::"+toZip);
        String toCountry = request.getParameter("ToCountry");
        log.debug("epinIVR::toCountry::"+toCountry);
        String user_pushed = request.getParameter("Digits");
        log.debug("epinIVR::user_pushed::"+user_pushed);
//        System.out.println(smsid);
//        System.out.println(accountSid);
//        System.out.println(from);
//        System.out.println(to);
//        System.out.println(body);
//        System.out.println(fromCity);
//        System.out.println(fromState);
//        System.out.println(fromZip);
//        System.out.println(FromCountry);
//        System.out.println(toCity);
//        System.out.println(toState);
//        System.out.println(toZip);
//        System.out.println(toCountry);
//        System.out.println(user_pushed);
//        

        String _channelName = this.getServletContext().getContextPath();
        _channelName = _channelName.replaceAll("/", "");

        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);

        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAcess = suRemoteAcess.openSession();
        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);

        Channels channel1 = cUtil.getChannel(_channelName);
        if (channel1 == null) {
            Say say = new Say("Sorry for you inconvience, Internal Configuration error ocuur.");
            try {
                twiml.append(say);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            return;
        }

        String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel1.getChannelid());
        if (credentialInfo == null) {
            Say say = new Say("Sorry for you inconvience, Internal Configuration error ocuur.");
            try {
                twiml.append(say);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            return;
        }

        request.getSession().setAttribute("_apSessionRemoteAccessLoginID", credentialInfo[0]);

        SessionManagement sManagement = new SessionManagement();
        String sessionId1 = sManagement.OpenSession(channel1.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());

        if (sessionId1 == null) {
            Say say = new Say("Sorry for you inconvience, Internal Configuration error ocuur.");
            try {
                twiml.append(say);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            return;
        }

        sRemoteAcess.close();
        suRemoteAcess.close();
        sChannel.close();
        suChannel.close();

        request.getSession().setAttribute("_apSessionID", sessionId1);
        request.getSession().setAttribute("_apSChannelDetails", channel1);

        EPINManagement eManagement = new EPINManagement();
        int result = eManagement.IsChannelURLACtive(sessionId1, channel1.getChannelid(), EPINManagement.VOICE);

        if (result != 0) {
            Say say = new Say("Sorry for you inconvience, Voice epin request url is disabled.");
            try {
                twiml.append(say);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
            return;
        }
        if (result == 0) {
            Gather gather = new Gather();
            gather.setAction("/epinIVRConfirm");
            gather.setNumDigits(1);
            gather.setMethod("POST");
            Say sayInGather = new Say("Welcome to Axiom "
                    + "Here you can change your Pin"
                    + "press 1 for Epin Request. "
                    + "press 0 for Again Listen Menu.");
//        if (user_pushed != null) {
//            if (user_pushed.equals("0")) {
//            } else if (user_pushed.equals("1")) {
//                RequestDispatcher dispatcher = request.getRequestDispatcher("./epiniverquetions");
//                dispatcher.forward(request, response);
//            }
//        }

            try {
                gather.append(sayInGather);

                twiml.append(gather);

            } catch (Exception e) {
                log.error("Exception caught :: ",e);
            }
        }

        response.setContentType("application/xml");
        response.getWriter().print(twiml.toXML());
        log.info("Servlet ended");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
