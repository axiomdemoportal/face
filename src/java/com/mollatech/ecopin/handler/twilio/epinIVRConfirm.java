/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.ecopin.handler.twilio;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.connector.user.QuestionAndAnswer;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Epinsessions;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;

import com.mollatech.axiom.nucleus.settings.OOBMobileChannelSettings;
import com.mollatech.axiom.nucleus.settings.PINDeliverySetting;
import com.mollatech.ecopin.management.EPINManagement;
import com.mollatech.axiom.connector.epin.QuestionsAndAnswers;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.resource.factory.SmsFactory;
import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.verbs.Gather;
import com.twilio.sdk.verbs.Say;
import com.twilio.sdk.verbs.TwiMLResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;

/**
 *
 * @author mollatech2
 */
public class epinIVRConfirm extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(epinIVRConfirm.class.getName());
    final int USERID = 1;
    final int PHONENUMBER = 2;
    final int EMAILID = 3;
    final int SMS = 1;
    final int USSD = 2;
    final int VOICE = 3;
    final int EMAIL = 4;
    final int SUCCESS = 0;
    final int FAILED = -1;
    final int EPIN = 9;
    int iType;
    final int START = 0;
    final int WAITING = 1;
    final int STOP = 2;
    final int RUNNING = 1;
    final int EXPIRED = -1;
    public static final int ALLOWED = 3;
    public static final int NOT_ALLOWED = -3;
    public static final int POLICY_CHECK = 4;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String smsid = request.getParameter("SmsSid");
        log.debug("epinIVRConfirm::smsid::"+smsid);
        String accountSid = request.getParameter("AccountSid");
        log.debug("epinIVRConfirm::accountSid::"+accountSid);
        String from = request.getParameter("From");
        log.debug("epinIVRConfirm::from::"+from);
        String to = request.getParameter("To");
        log.debug("epinIVRConfirm::to::"+to);
        String body = request.getParameter("Body");
        log.debug("epinIVRConfirm::body::"+body);
        String fromCity = request.getParameter("FromCity");
        log.debug("epinIVRConfirm::fromCity::"+fromCity);
        String fromState = request.getParameter("FromState");
        log.debug("epinIVRConfirm::fromState::"+fromState);
        String fromZip = request.getParameter("FromZip");
        log.debug("epinIVRConfirm::fromZip::"+fromZip);
        String FromCountry = request.getParameter("FromCountry");
        log.debug("epinIVRConfirm::FromCountry::"+FromCountry);
        String toCity = request.getParameter("ToCity");
        log.debug("epinIVRConfirm::toCity::"+toCity);
        String toState = request.getParameter("ToState");
        log.debug("epinIVRConfirm::toState::"+toState);
        String toZip = request.getParameter("ToZip");
        log.debug("epinIVRConfirm::toZip::"+toZip);
        String toCountry = request.getParameter("ToCountry");
        log.debug("epinIVRConfirm::toCountry::"+toCountry);
        String digits = request.getParameter("Digits");
        log.debug("epinIVRConfirm::digits::"+digits);

        //added on 10th aug 2013
        String _channelName = this.getServletContext().getContextPath();
        _channelName = _channelName.replaceAll("/", "");

        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);

        Channels channel = cUtil.getChannel(_channelName);
        if (channel == null) {
            sChannel.close();
            suChannel.close();
            return;
        }

        digits = digits.trim();

        //SessionManagement sManagement = null;
        sChannel.close();
        suChannel.close();
        //end of addition

        //String channelId = "b6BK5gdwIWqSue/8VSJOuuIiFNc=";
        String channelId = channel.getChannelid();

        String sessionId = null;
        AuthUser aUser = null;
        QuestionsAndAnswers QandAObj = null;
        int countOfCorrectAns = 0;
        PINDeliverySetting ePin = null;
        Object obj = null;
        OOBMobileChannelSettings mobile = null;
        int retValue = 0;
        Say say = null;
        Gather gather = null;
        Say sayGather = null;
        TwiMLResponse twiml = new TwiMLResponse();

        String message = null;
        AXIOMStatus status = null;
        EPINManagement eManagement = new EPINManagement();
        SettingsManagement setManagement = new SettingsManagement();
        SessionManagement sManagement = new SessionManagement();
        //  Epinsessions eSessions = eManagement.getEpinSessionsByType(from, channelId, PHONENUMBER);
        //Epinsessions eSessions = eManagement.getEpinSessionsByType(from, channelId, PHONENUMBER);

        Epinsessions eSessions = null;
        String eSessionId = null;

        Epinsessions[] eSessionsList = eManagement.getEpinSessionsByType(from, channelId, PHONENUMBER);
        if (eSessionsList != null) {
            for (int i = 0; i < eSessionsList.length; i++) {
                if (eSessionsList[i].getStatus() == STOP || eSessionsList[i].getStatus() == EXPIRED) {
                    //continue;
                } else {
                    eSessions = eSessionsList[i];
                    eSessionId = eSessions.getEpinsessionid();
                    break;
                }
            }
        }

        if (eSessions == null || digits.equalsIgnoreCase("1") == true) {

            SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
            Session sRemoteAcess = suRemoteAcess.openSession();
            RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);

            String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
            if (credentialInfo == null) {
                sRemoteAcess.close();
                suRemoteAcess.close();
                sChannel.close();
                suChannel.close();
                return;
            }
            sManagement = new SessionManagement();
            sessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1],request.getSession().getId());

            if (sessionId == null) {
                sRemoteAcess.close();
                suRemoteAcess.close();
                sChannel.close();
                suChannel.close();
                return;
            }

            sRemoteAcess.close();
            suRemoteAcess.close();
              UserManagement uManagement = new UserManagement();
            aUser = uManagement.CheckUserByType(sessionId, channelId, from, PHONENUMBER);

            ePin = (PINDeliverySetting) setManagement.getSetting(sessionId, channelId, EPIN, 1);

            retValue = eManagement.EnforcePINPolicy(sessionId, null, aUser.userId, channelId, aUser.phoneNo, aUser.email, new Date(), ePin);

            eSessions = null;
        } else if (sessionId == null && eSessions != null) {

            ePin = (PINDeliverySetting) setManagement.getSetting(eSessions.getSessionId(), channelId, EPIN, 1);

        }

        if (digits != null && digits.equalsIgnoreCase("1") == true) {

            if (retValue == SUCCESS) {

                if (eSessions == null) {
                    QuestionAndAnswer[] queArray = eManagement.GetValidationQuestionsAndAnswers(sessionId, channelId, aUser.userId);
                    QandAObj = new QuestionsAndAnswers(queArray);
                    if (QandAObj.questionsAndAnswerses == null || QandAObj.questionsAndAnswerses.length <= 0) {
                        //user not found
                    } else {

                        eSessionId = eManagement.addEPINSession(aUser.userId, channelId, aUser.phoneNo, aUser.email, QandAObj, new Date(), new Date(), sessionId);
                        gather = new Gather();

                        gather.setAction("/epinIVRQA?eSessionId=" + eSessionId + "&sessionid=" + sessionId);
                        //gather.setAction("/epinIVRQA");

                        gather.setNumDigits(1);
                        gather.setMethod("POST");
                        sayGather = new Say("for verification we need to ask you few questions."
                                + "To proceed further press 1."
                                + "Otherwise press 0 to stop.");

//                    say = new Say(queArray[0].question);
////                    message = "Question 1\n" + queArray[0].question;
////                    status = send.SendOnMobile(channelId, aUser.phoneNo, message, VOICE);
//                    if (status.iStatus == 0 && status.strStatus.equals("completed")) {
//                        queArray[0].iCurrent = 1;
//                        eManagement.updateEPINSession(aUser.userId, channelId, aUser.phoneNo, aUser.email, QandAObj);
//                    }
                        try {
                            gather.append(sayGather);

                            twiml.append(gather);

                        } catch (Exception e) {
                            log.error("Exception caught :: ",e);
                        }
                    }
                    log.info("Servlet ended");
                }
//                else if (eSessions != null) {
//                    QuestionAndAnswer[] queArray = eManagement.GetValidationQuestionsAndAnswers(eSessions.getSessionId(), channelId, eSessions.getUserid());
//                    QandAObj = new QuestionsAndAnswers(queArray);
//
//                    if (QandAObj.questionsAndAnswerses == null || QandAObj.questionsAndAnswerses.length <= 0) {
//                        //user not found
//                    } else {
//                        eManagement.enableExpirySession(eSessions.getUserid(), channelId, eSessions.getPhone(), eSessions.getEmailid(), QandAObj, START, new Date(), new Date());
//                        gather = new Gather();
//                        gather.setAction("/epinque");
//                        gather.setNumDigits(1);
//                        gather.setMethod("POST");
//                        sayGather = new Say("for verification we need to asked u some Questions."
//                                + "for Proceed further press 1."
//                                + "press 0 to go back to previous menu.");
//                        try {
//                            gather.append(sayGather);
//
//                            twiml.append(gather);
//
//                        } catch (Exception e) {
//                            log.error("Exception caught :: ",e);
//                        }
//
//                    }
//                }
            } else {
                eManagement.AddEPINTracker(channelId, null,null,EPINManagement.NO_OPERATOR_CONTROLLED, aUser.userId, from, aUser.email, POLICY_CHECK, NOT_ALLOWED, new Date(), null, ePin.expiryTime,null);
                say = new Say("You Are not Allowed to Request For Eco pin");
                try {
                    twiml.append(say);
                } catch (Exception e) {
                    log.error("Exception caught :: ",e);
                    log.error("epinIVRConfirm::Exception is::",e);
                }
            }
        } else if (digits != null && digits.equals("0")) {
            response.sendRedirect("/epinIVR");
        } else {
            say = new Say("You have pressed invalid key.");
            try {
                twiml.append(say);
            } catch (Exception e) {
                log.error("Exception caught :: ",e);
                log.error("epinIVRConfirm::Exception is::",e);
            }
        }

        response.setContentType("application/xml");
        response.getWriter().print(twiml.toXML());

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
