/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.ecopin.handler;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.epin.QuestionsAndAnswers;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.connector.user.QuestionAndAnswer;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Epinsessions;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.OOBMobileChannelSettings;
import com.mollatech.axiom.nucleus.settings.PINDeliverySetting;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import com.mollatech.ecopin.management.EPINManagement;
import com.mollatech.ecopin.management.EPINThread;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;

/**
 *
 * @author mollatech1
 */
public class epinSMS extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(epinSMS.class.getName());
    public static final int USERID = 1;
    public static final int PHONENUMBER = 2;
    public static final int EMAILID = 3;
    final int SMS = 1;
    final int USSD = 2;
    final int VOICE = 3;
    final int EMAIL = 4;
    final int SUCCESS = 0;
    final int FAILED = -1;
    final int EPIN = 9;
    int iType;
    public static final int START = 0;
    public static final int WAITING = 1;
    public static final int STOP = 2;
    public static final int RUNNING = 1;
    public static final int EXPIRED = -1;
    public static final int ALLOWED = 6;
    public static final int NOT_ALLOWED = -6;
    public static final int POLICY_CHECK = 4;
    final int VALIDATION = 5;
    final int FAILEDTOVALIDATE = -4;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            log.info("Servlet Started");
//         Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
//        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        //TwiMLResponse twiml = new TwiMLResponse();
        String smsid = request.getParameter("SmsSid");
        log.debug("epinSMS::smsid::"+smsid);
        String accountSid = request.getParameter("AccountSid");
        log.debug("epinSMS::accountSid::"+accountSid);
        String from = request.getParameter("From");
        log.debug("epinSMS::from::"+from);
        String to = request.getParameter("To");
        log.debug("epinSMS::to::"+to);
        String body = request.getParameter("Body");
        log.debug("epinSMS::body::"+body);
        String fromCity = request.getParameter("FromCity");
        log.debug("epinSMS::fromCity::"+fromCity);
        String fromState = request.getParameter("FromState");
        log.debug("epinSMS::fromState::"+fromState);
        String fromZip = request.getParameter("FromZip");
        log.debug("epinSMS::fromZip::"+fromZip);
        String FromCountry = request.getParameter("FromCountry");
        log.debug("epinSMS::FromCountry::"+FromCountry);
        String toCity = request.getParameter("ToCity");
        log.debug("epinSMS::toCity::"+toCity);
        String toState = request.getParameter("ToState");
        log.debug("epinSMS::toState::"+toState);
        String toZip = request.getParameter("ToZip");
        log.debug("epinSMS::toZip::"+toZip);
        String toCountry = request.getParameter("ToCountry");
        log.debug("epinSMS::toCountry::"+toCountry);
//        System.out.println(smsid);
//        System.out.println(accountSid);
//        System.out.println(from);
//        System.out.println(to);
//        System.out.println(body);
//        System.out.println(fromCity);
//        System.out.println(fromState);
//        System.out.println(fromZip);
//        System.out.println(FromCountry);
//        System.out.println(toCity);
//        System.out.println(toState);
//        System.out.println(toZip);
//        System.out.println(toCountry);
        //added on 10th aug 2013
        String _channelName = this.getServletContext().getContextPath();
        _channelName = _channelName.replaceAll("/", "");

        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);

        Channels channel = cUtil.getChannel(_channelName);
        if (channel == null) {
            sChannel.close();
            suChannel.close();
            return;
        }

        body = body.trim();

        SessionManagement sManagement = null;
        String sessionId = null;

        sChannel.close();
        suChannel.close();
        //end of addition

        String channelId = channel.getChannelid();

        AuthUser aUser = null;
        QuestionsAndAnswers QandAObj = null;
        int countOfCorrectAns = 0;
        PINDeliverySetting ePin = null;
        Object obj = null;
        OOBMobileChannelSettings mobile = null;
        int retValue = 0;

        String message = null;
        AXIOMStatus status = null;
        EPINManagement eManagement = new EPINManagement();
        SettingsManagement setManagement = new SettingsManagement();

        Epinsessions eSessions = null;

        String eSessionId = null;

        Epinsessions[] eSessionsList = eManagement.getEpinSessionsByType(from, channelId, PHONENUMBER);
        if (eSessionsList != null) {
            for (int i = 0; i < eSessionsList.length; i++) {
                if (eSessionsList[i].getStatus() == STOP || eSessionsList[i].getStatus() == EXPIRED) {
                    //continue;
                } else {
                    eSessions = eSessionsList[i];
                    eSessionId = eSessions.getEpinsessionid();
                    break;
                }
            }
        }
        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAcess = suRemoteAcess.openSession();
        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);

        String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
        if (credentialInfo == null) {
            sRemoteAcess.close();
            suRemoteAcess.close();
            sChannel.close();
            suChannel.close();
            return;
        }
        sManagement = new SessionManagement();
        sessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());

        if (sessionId == null) {
            sRemoteAcess.close();
            suRemoteAcess.close();
            sChannel.close();
            suChannel.close();
            return;
        }

        sRemoteAcess.close();
        suRemoteAcess.close();

        int result = eManagement.IsChannelURLACtive(sessionId, channelId, iType);
        if (result == 0) {

            ePin = (PINDeliverySetting) setManagement.getSetting(sessionId, channelId, EPIN, 1);

            //Epinsessions []  eSessions = eManagement.getEpinSessionsByType(from, channelId, PHONENUMBER);
            if (eSessions == null || body.equals(ePin.smsText) == true) {

                //sessionId = sManagement.OpenSession(channelId, "manoj", "manoj");
//            SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
//            Session sRemoteAcess = suRemoteAcess.openSession();
//            RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
//
//            String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
//            if (credentialInfo == null) {
//                sRemoteAcess.close();
//                suRemoteAcess.close();
//                sChannel.close();
//                suChannel.close();
//                return;
//            }
//            sManagement = new SessionManagement();
//            sessionId = sManagement.OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1]);
//
//            if (sessionId == null) {
//                sRemoteAcess.close();
//                suRemoteAcess.close();
//                sChannel.close();
//                suChannel.close();
//                return;
//            }
//
//            sRemoteAcess.close();
//            suRemoteAcess.close();
                UserManagement uManagement = new UserManagement();
                aUser = uManagement.CheckUserByType(sessionId, channelId, from, PHONENUMBER);

//            ePin = (PINDeliverySetting) setManagement.getSetting(sessionId, channelId, EPIN, 1);
                retValue = eManagement.EnforcePINPolicy(sessionId, null, aUser.userId, channelId, aUser.phoneNo, aUser.email, new Date(), ePin);

                eSessions = null;
            } else if (sessionId == null && eSessions != null) {

                ePin = (PINDeliverySetting) setManagement.getSetting(eSessions.getSessionId(), channelId, EPIN, 1);

            }

//        if (sessionId != null && eSessions == null) {
//
//            ePin = (PINDeliverySetting) setManagement.getSetting(sessionId, channelId, EPIN, 1);
//            if (ePin == null) {
//                return;
//            }
//
//            obj = setManagement.getSetting(sessionId, channelId, SMS, 1);
//
//            if (obj == null) {
//                return;
//            }
//
//            retValue = eManagement.EnforcePINPolicy(sessionId, null, aUser.userId, channelId, aUser.phoneNo, aUser.email, new Date(), ePin);
//
//
//        } else if (sessionId == null && eSessions != null) {
//
//            ePin = (PINDeliverySetting) setManagement.getSetting(eSessions.getSessionId(), channelId, EPIN, 1);
//            if (ePin == null) {
//                return;
//            }
//
//            obj = setManagement.getSetting(eSessions.getSessionId(), channelId, SMS, 1);
//
//            if (obj == null) {
//                return;
//            }
//
//
//            if (eSessions.getStatus() == STOP || eSessions.getStatus() == EXPIRED) {
//
//                sManagement.UpdateSession(eSessions.getSessionId());
//
//                retValue = eManagement.EnforcePINPolicy(sessionId, null, eSessions.getUserid(), channelId, eSessions.getPhone(), eSessions.getEmailid(), new Date(), ePin);
//            }
//        }
//        if (obj != null) {
//            mobile = (OOBMobileChannelSettings) obj;
//        }
//        if (mobile != null) {
//            String ACCOUNTSID = mobile.getTwoWayuserid();
//            String AUTHTOKEN = mobile.getTwoWayPassword();
//
////            TwilioRestClient client = new TwilioRestClient(ACCOUNTSID, AUTHTOKEN);
////            Account acct = client.getAccount();
////            SmsFactory smsFactory = acct.getSmsFactory();
//            Map<String, String> params = new HashMap<String, String>();
//            //  params.put("SmsUrl", mobile.getTwoWayServerUrl());
//            //  params.put("SmsUrl", "http://twimlets.com/echo?Twiml=%3CResponse%3E%3CSms%3EHello+vikram%2C+thanks+for+the+message%21%3C%2FSms%3E%3C%2FResponse%3E");
//
//        }
            if (body.equalsIgnoreCase("PIN") == true) {

                if (retValue == SUCCESS) {
                    if (eSessions == null) {
                        QuestionAndAnswer[] queArray = eManagement.GetValidationQuestionsAndAnswers(sessionId, channelId, aUser.userId);
                        QandAObj = new QuestionsAndAnswers(queArray);

                        if (QandAObj.questionsAndAnswerses == null || QandAObj.questionsAndAnswerses.length <= 0) {
                            return;
                        } else {
                            Calendar cal = Calendar.getInstance();
                            cal.add(Calendar.MINUTE, ePin.expiryTime);
                            Date expiryDate = cal.getTime();
                            eSessionId = eManagement.addEPINSession(aUser.userId, channelId, aUser.phoneNo, aUser.email, QandAObj, new Date(), expiryDate, sessionId);

                            message = "Question 1:\n" + QandAObj.questionsAndAnswerses[0].question;

                            //needs to be threaded
                            SendNotification send = new SendNotification();
                            status = send.SendOnMobile(channelId, aUser.phoneNo, message, SMS, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));

                            if (status.iStatus == 0 && status.strStatus.equals("completed")) {
                                QandAObj.questionsAndAnswerses[0].iCurrent = 1;
                                eManagement.updateEPINSession(eSessionId, aUser.userId, channelId, aUser.phoneNo, aUser.email, QandAObj, WAITING);
                            }
                        }
                    }
//                else if (eSessions != null) {
//
//                    QuestionAndAnswer[] queArray = eManagement.GetValidationQuestionsAndAnswers(eSessions.getSessionId(), channelId, eSessions.getUserid());
//                    QandAObj = new QuestionsAndAnswers(queArray);
//
//                    if (QandAObj.questionsAndAnswerses == null || QandAObj.questionsAndAnswerses.length <= 0) {
//                        return;
//                    } else {
//
//                        eManagement.enableExpirySession(eSessions.getUserid(), channelId, eSessions.getPhone(), eSessions.getEmailid(), QandAObj, START, new Date(), new Date());
//
//                        message = "Question 1\n" + QandAObj.questionsAndAnswerses[0].question;
//
//                        status = send.SendOnMobile(channelId, eSessions.getPhone(), message, SMS);
//
//                        if (status.iStatus == 0 && status.strStatus.equals("completed")) {
//                            QandAObj.questionsAndAnswerses[0].iCurrent = 1;
//                            eManagement.updateEPINSession(eSessions.getUserid(), channelId, eSessions.getPhone(), eSessions.getEmailid(), QandAObj, WAITING);
//                        }
//                    }
//                }
                } else {
                    eManagement.AddEPINTracker(channelId, null, null, EPINManagement.NO_OPERATOR_CONTROLLED, aUser.userId, from, aUser.email, POLICY_CHECK, NOT_ALLOWED, new Date(), null, ePin.expiryTime, null);
                }
            } else if (eSessions != null) {
                QandAObj = eManagement.getQuestionsAndAnswer(eSessions.getEpinsessionid());
                if (QandAObj == null) {
                    eManagement.updateEPINSession(eSessionId, eSessions.getUserid(), channelId, eSessions.getPhone(), eSessions.getEmailid(), QandAObj, EXPIRED);
                } else {
                    for (int i = 0; i < QandAObj.questionsAndAnswerses.length; i++) {
                        if (QandAObj.questionsAndAnswerses[i].answerbyUser == null) {
                            QandAObj.questionsAndAnswerses[i].answerbyUser = body;

                            if (i < QandAObj.questionsAndAnswerses.length - 1) {
                                message = "Question " + (i + 2) + ": \n" + QandAObj.questionsAndAnswerses[i + 1].question;

                                SendNotification send = new SendNotification();
                                status = send.SendOnMobile(channelId, eSessions.getPhone(), message, SMS, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));

                                if (status.iStatus == 0 && status.strStatus.equals("completed")) {
                                    QandAObj.questionsAndAnswerses[i + 1].iCurrent = QandAObj.questionsAndAnswerses[i].iCurrent + 1;
                                }
                            }

                            if (QandAObj.questionsAndAnswerses[i].answerbyUser != null) {
                                eManagement.updateEPINSession(eSessionId, eSessions.getUserid(), channelId, eSessions.getPhone(), eSessions.getEmailid(), QandAObj, WAITING);
                                break;
                            }

                        }
                    }
                }

                // QandAObj = eManagement.getQuestionsAndAnswer(eSessions.getEpinsessionid());
                if (QandAObj == null) {
                    eManagement.updateEPINSession(eSessionId, eSessions.getUserid(), channelId, eSessions.getPhone(), eSessions.getEmailid(), QandAObj, EXPIRED);
                } else {
                    for (int i = 0; i < QandAObj.questionsAndAnswerses.length; i++) {
                        if (QandAObj.questionsAndAnswerses[i].bValidateAtAxiom == true) {
                            if (QandAObj.questionsAndAnswerses[i].bEnforceCaseSensitive == true) {
                                if (QandAObj.questionsAndAnswerses[i].answerToValidate.equals(QandAObj.questionsAndAnswerses[i].answerbyUser)) {
                                    QandAObj.questionsAndAnswerses[i].bValidationResult = true;
                                    countOfCorrectAns++;
                                }
                            } else {
                                if (QandAObj.questionsAndAnswerses[i].answerToValidate.equalsIgnoreCase(QandAObj.questionsAndAnswerses[i].answerbyUser)) {
                                    QandAObj.questionsAndAnswerses[i].bValidationResult = true;
                                    countOfCorrectAns++;
                                }
                            }
                        } else {
                            countOfCorrectAns = eManagement.ValidateUserDetails(eSessions.getSessionId(), channelId, eSessions.getUserid(), QandAObj.questionsAndAnswerses);
                            break;
                        }
                    }
                }
                QandAObj = eManagement.getQuestionsAndAnswer(eSessions.getEpinsessionid());
                if (QandAObj != null) {
                    if (countOfCorrectAns == QandAObj.questionsAndAnswerses.length) {

                        if (ePin.operatorController == EPINManagement.OPERATOR_CONTROLLED || ePin.operatorController == EPINManagement.DUAL_OPERATOR_CONTROLLED) {
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] oper = oManagement.getAdminOperator(channelId);
                            TemplateManagement tManagement = new TemplateManagement();
                            Templates templates = tManagement.LoadbyName(sessionId, channelId, TemplateNames.EMAIL_ECOPIN_OPERATOR_PIN_DELIVERY_FAILED_TEMPLATE);
                          if(templates.getStatus() == tManagement.ACTIVE_STATUS){
                            ByteArrayInputStream bais = new ByteArrayInputStream(templates.getTemplatebody());
                            String tmessage = (String) TemplateUtils.deserializeFromObject(bais);
                            String subject = templates.getSubject();
                            SendNotification send = new SendNotification();
                           String[] emailList = new String[oper.length - 1];

                        for (int i = 1; i < oper.length; i++) {
                            emailList[i-1] = oper[i].getEmailid();
                        }
                                SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                                if (message != null) {
                                    Date d = new Date();
                                    Calendar cal = Calendar.getInstance();
                                    cal.setTime(d);
                                    cal.add(Calendar.MINUTE, ePin.expiryTime);
                                    Date exipryDate = cal.getTime();
                                    tmessage = tmessage.replaceAll("#name#", oper[0].getName());
                                    tmessage = tmessage.replaceAll("#channel#", channel.getName());
                                    tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                                    tmessage = tmessage.replaceAll("#email#", oper[0].getEmailid());
                                    tmessage = tmessage.replaceAll("#username#", aUser.userName);
                                    tmessage = tmessage.replaceAll("#expiry#", sdf.format(exipryDate));
                                }

                                if (subject != null) {
                                    Date d = new Date();
                                    subject = subject.replaceAll("#channel#", channel.getName());
                                    subject = subject.replaceAll("#datetime#", sdf.format(d));
                                }
                                
                                send.SendEmail(channelId, oper[0].getEmailid(), subject, tmessage, emailList, null, null, null, 3);
                            
                            if (ePin.operatorController == EPINManagement.OPERATOR_CONTROLLED) {
                                eManagement.isOperatorControlled(eSessions.getUserid(), channelId, eSessions.getPhone(), eSessions.getEmailid(), new Date(), ePin);
                            } else if (ePin.operatorController == EPINManagement.DUAL_OPERATOR_CONTROLLED) {
                                eManagement.isOperatorControlled(eSessions.getUserid(), channelId, eSessions.getPhone(), eSessions.getEmailid(), new Date(), ePin);
                            }
                            message = "Thanks for your response, our operator shall get back to you shortly!!!";

                            status = send.SendOnMobile(channelId, eSessions.getPhone(), message, SMS, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            eManagement.updateEPINSession(eSessionId, eSessions.getUserid(), channelId, eSessions.getPhone(), eSessions.getEmailid(), QandAObj, STOP);
                          }
                        } else {

                            String strEPin = eManagement.GeneratePIN(channelId, ePin, eSessions.getUserid());
                            if (strEPin != null) {
                                //EPINThread eThread = new EPINThread(channelId, ePin, from , aUser.getEmail(), "Your PIN is ",strEPin);
                                EPINThread eThread = new EPINThread(sessionId, channelId, ePin, from, eSessions.getEmailid(), strEPin, eSessions.getUserid(), null, null, new Date());
                                Thread t = new Thread(eThread);
                                t.start();
                                QandAObj = null;

                                eManagement.updateEPINSession(eSessionId, eSessions.getUserid(), channelId, eSessions.getPhone(), eSessions.getEmailid(), QandAObj, STOP);

                            }
                        }

                    } else {
                          eManagement.updateEPINSession(eSessionId, aUser.userId, channel.getChannelid(), aUser.phoneNo, aUser.email, QandAObj, STOP);
                        if (QandAObj.questionsAndAnswerses[QandAObj.questionsAndAnswerses.length - 1].answerbyUser != null) {
                           if(ePin.operatorController == EPINManagement.OPERATOR_CONTROLLED){
                              eManagement.AddEPINTracker(channel.getChannelid(), null,null,EPINManagement.SINGLE_OPERATOR_CONTROLLED, eSessions.getUserid(), eSessions.getPhone(), eSessions.getEmailid(), VALIDATION, FAILEDTOVALIDATE, new Date(), null, ePin.expiryTime,null);
                         }else if(ePin.operatorController == EPINManagement.DUAL_OPERATOR_CONTROLLED){
                              eManagement.AddEPINTracker(channel.getChannelid(), null,null,EPINManagement.PENDING_FOR_OPERATORS, eSessions.getUserid(), eSessions.getPhone(), eSessions.getEmailid(), VALIDATION, FAILEDTOVALIDATE, new Date(), null, ePin.expiryTime,null);
                         }else{
                              eManagement.AddEPINTracker(channel.getChannelid(), null,null,EPINManagement.NO_OPERATOR_CONTROLLED, eSessions.getUserid(), eSessions.getPhone(), eSessions.getEmailid(), VALIDATION, FAILEDTOVALIDATE, new Date(), null, ePin.expiryTime,null);
                         }
                        }
                    }

                }
            }

            response.setContentType("application/xml");
            //response.getWriter().print(twiml.toXML());
            response.getWriter().print("");
            log.info("Servlet ended");
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
