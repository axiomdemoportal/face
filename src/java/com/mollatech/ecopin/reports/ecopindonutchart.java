/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.ecopin.reports;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.axiom.face.common.bar;
import com.mollatech.axiom.face.common.donut;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.ecopin.management.EPINManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ecopindonutchart extends HttpServlet {
static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ecopindonutchart.class.getName());
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String channelID = channel.getChannelid();
        log.debug("ecopindonutchart::channelID::"+channelID);
        String _userID = request.getParameter("_userID");
        log.debug("ecopindonutchart::_userID::"+_userID);
        String _startdate = request.getParameter("_startDate");
        log.debug("ecopindonutchart::_startdate::"+_startdate);
        String _enddate = request.getParameter("_endDate");
        log.debug("ecopindonutchart::_enddate::"+_enddate);
        String _type = request.getParameter("_type");
        log.debug("ecopindonutchart::_type::"+_type);
        String _reporttype = request.getParameter("_reporttype");
        log.debug("ecopindonutchart::_reporttype::"+_reporttype);
        try {
            int iType = 000;
            if (_type != null) {
                iType = Integer.parseInt(_type);
            }
            int ireporttype = 000;
            if (_reporttype != null) {
                ireporttype = Integer.parseInt(_reporttype);
            }
            DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            Date startDate = null;
            if (_startdate != null && !_startdate.isEmpty()) {
                startDate = (Date) formatter.parse(_startdate);
            }
            Date endDate = null;
            if (_enddate != null && !_enddate.isEmpty()) {
                endDate = (Date) formatter.parse(_enddate);
            }

            EPINManagement oObj = new EPINManagement();
            ArrayList<donut> sample = new ArrayList<donut>();
            if (ireporttype == 2) {
                if (iType == EPINManagement.OPERATOR_CONTROLLED) {
                    int approval = oObj.searchSystemEPINCount(channelID, iType, EPINManagement.APPROVED, startDate, endDate);
                    log.debug("ecopindonutchart::searchSystemEPINCount::"+approval);
                    
                    int rejected = oObj.searchSystemEPINCount(channelID, iType, EPINManagement.REJECTED, startDate, endDate);
                    log.debug("ecopindonutchart::searchSystemEPINCount::"+rejected);
                    
                    sample.add(new donut(approval, "Delivered"));
                    sample.add(new donut(rejected, "Rejected"));
                } else if (iType == EPINManagement.DUAL_OPERATOR_CONTROLLED) {
                    int approval = oObj.searchSystemEPINCount(channelID, iType, EPINManagement.APPROVED, startDate, endDate);
                    log.debug("ecopindonutchart::searchSystemEPINCount::"+approval);
                    
                    int rejected = oObj.searchSystemEPINCount(channelID, iType, EPINManagement.REJECTED, startDate, endDate);
                    log.debug("ecopindonutchart::searchSystemEPINCount::"+rejected);
                    
                    sample.add(new donut(approval, "Delivered"));
                    sample.add(new donut(rejected, "Rejected"));
                } else if (iType == EPINManagement.DELIVERY) {
                    int failed = oObj.searchSystemEPINCount(channelID, iType, EPINManagement.FAILED, startDate, endDate);
                    log.debug("ecopindonutchart::searchSystemEPINCount::"+failed);
                    
                    int approval = oObj.searchSystemEPINCount(channelID, iType, EPINManagement.APPROVED, startDate, endDate);
                    log.debug("ecopindonutchart::searchSystemEPINCount::"+approval);
                    
                    int rejected = oObj.searchSystemEPINCount(channelID, iType, EPINManagement.REJECTED, startDate, endDate);
                    log.debug("ecopindonutchart::searchSystemEPINCount::"+rejected);
                    
                    sample.add(new donut(failed, "Not delivered"));
                    sample.add(new donut(approval, "Delivered"));
                    sample.add(new donut(rejected, "Rejected"));
                } else if (iType == EPINManagement.POLICY_CHECK) {
//                int allowed = oObj.searchEPINCount(channelID, iType, EPINManagement.ALLOWED, _searchtext, startDate, endDate);
                    int notallowed = oObj.searchSystemEPINCount(channelID, iType, EPINManagement.NOT_ALLOWED, startDate, endDate);
                    log.debug("ecopindonutchart::searchSystemEPINCount::"+notallowed);
                    
                    int approval = oObj.searchSystemEPINCount(channelID, iType, EPINManagement.APPROVED, startDate, endDate);
                    log.debug("ecopindonutchart::searchSystemEPINCount::"+approval);
                    
                    int rejected = oObj.searchSystemEPINCount(channelID, iType, EPINManagement.REJECTED, startDate, endDate);
                    log.debug("ecopindonutchart::searchSystemEPINCount::"+rejected);
                    
                    int failed = oObj.searchSystemEPINCount(channelID, iType, EPINManagement.FAILED, startDate, endDate);
                    log.debug("ecopindonutchart::searchSystemEPINCount::"+failed);
                    
                    sample.add(new donut(notallowed, "Dropped"));
                    sample.add(new donut(approval, "Delivered"));
                    sample.add(new donut(rejected, "Rejected"));
                    sample.add(new donut(failed, "Not delivered"));

                } else if (iType == EPINManagement.VALIDATION) {
                    int approval = oObj.searchSystemEPINCount(channelID, iType, EPINManagement.APPROVED, startDate, endDate);
                    log.debug("ecopindonutchart::searchSystemEPINCount::"+approval);
                    
                    int failedtovalidate = oObj.searchSystemEPINCount(channelID, iType, EPINManagement.FAILEDTOVALIDATE, startDate, endDate);
                    log.debug("ecopindonutchart::searchSystemEPINCount::"+failedtovalidate);
                    
                    int expired = oObj.searchSystemEPINCount(channelID, iType, EPINManagement.EXPIRED, startDate, endDate);
                    log.debug("ecopindonutchart::searchSystemEPINCount::"+expired);
                    
                    sample.add(new donut(approval, "Delivered"));
                    sample.add(new donut(failedtovalidate, "Invalid"));
                    sample.add(new donut(expired, "Expired"));
                }
            } else {
                if (iType == EPINManagement.OPERATOR_CONTROLLED) {
                    int approval = oObj.searchEPINCount(channelID, iType, EPINManagement.APPROVED, _userID, startDate, endDate);
                    log.debug("ecopindonutchart::searchEPINCount::"+approval);
                    
                    int rejected = oObj.searchEPINCount(channelID, iType, EPINManagement.REJECTED, _userID, startDate, endDate);
                    log.debug("ecopindonutchart::searchEPINCount::"+rejected);
                    
                    sample.add(new donut(approval, "Delivered"));
                    sample.add(new donut(rejected, "Rejected"));
                }  else if (iType == EPINManagement.DUAL_OPERATOR_CONTROLLED) {
                    int approval = oObj.searchEPINCount(channelID, iType, EPINManagement.APPROVED, _userID, startDate, endDate);
                    log.debug("ecopindonutchart::searchEPINCount::"+approval);
                    
                    int rejected = oObj.searchEPINCount(channelID, iType, EPINManagement.REJECTED, _userID, startDate, endDate);
                    log.debug("ecopindonutchart::searchEPINCount::"+rejected);
                    
                    sample.add(new donut(approval, "Delivered"));
                    sample.add(new donut(rejected, "Rejected"));
                } else if (iType == EPINManagement.DELIVERY) {
                    int failed = oObj.searchEPINCount(channelID, iType, EPINManagement.FAILED, _userID, startDate, endDate);
                    log.debug("ecopindonutchart::searchEPINCount::"+failed);
                    
                    int approval = oObj.searchEPINCount(channelID, iType, EPINManagement.APPROVED, _userID, startDate, endDate);
                    log.debug("ecopindonutchart::searchEPINCount::"+approval);
                    
                    int rejected = oObj.searchEPINCount(channelID, iType, EPINManagement.REJECTED, _userID, startDate, endDate);
                    log.debug("ecopindonutchart::searchEPINCount::"+rejected);
                    
                    sample.add(new donut(failed, "Not delivered"));
                    sample.add(new donut(approval, "Delivered"));
                    sample.add(new donut(rejected, "Rejected"));
                } else if (iType == EPINManagement.POLICY_CHECK) {
//                int allowed = oObj.searchEPINCount(channelID, iType, EPINManagement.ALLOWED, _searchtext, startDate, endDate);
                    int notallowed = oObj.searchEPINCount(channelID, iType, EPINManagement.NOT_ALLOWED, _userID, startDate, endDate);
                    log.debug("ecopindonutchart::searchEPINCount::"+notallowed);
                    
                    int approval = oObj.searchEPINCount(channelID, iType, EPINManagement.APPROVED, _userID, startDate, endDate);
                    log.debug("ecopindonutchart::searchEPINCount::"+approval);
                    
                    int rejected = oObj.searchEPINCount(channelID, iType, EPINManagement.REJECTED, _userID, startDate, endDate);
                    log.debug("ecopindonutchart::searchEPINCount::"+rejected);
                    
                    int failed = oObj.searchEPINCount(channelID, iType, EPINManagement.FAILED, _userID, startDate, endDate);
                    log.debug("ecopindonutchart::searchEPINCount::"+failed);
                    
                    sample.add(new donut(notallowed, "Dropped"));
                    sample.add(new donut(approval, "Delivered"));
                    sample.add(new donut(rejected, "Rejected"));
                    sample.add(new donut(failed, "Not delivered"));

                } else if (iType == EPINManagement.VALIDATION) {
                    int approval = oObj.searchEPINCount(channelID, iType, EPINManagement.APPROVED, _userID, startDate, endDate);
                    log.debug("ecopindonutchart::searchEPINCount::"+approval);
                    
                    int failedtovalidate = oObj.searchEPINCount(channelID, iType, EPINManagement.FAILEDTOVALIDATE, _userID, startDate, endDate);
                    log.debug("ecopindonutchart::searchEPINCount::"+failedtovalidate);
                    
                    int expired = oObj.searchEPINCount(channelID, iType, EPINManagement.EXPIRED, _userID, startDate, endDate);
                    log.debug("ecopindonutchart::searchEPINCount::"+expired);
                    sample.add(new donut(approval, "Delivered"));
                    sample.add(new donut(failedtovalidate, "Invalid"));
                    sample.add(new donut(expired, "Expired"));
                }
            }

            for (int i = 0; i < sample.size(); i++) {
                //  System.out.println(sample.get(i));
            }
            Gson gson = new Gson();

            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<donut>>() {
            }.getType());

            JsonArray jsonArray = element.getAsJsonArray();

            out.print(jsonArray);

        } catch (Exception ex) {
            log.error("Exception caught :: ",ex);
        } finally {
            out.close();
            log.info("Servlet ended");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
